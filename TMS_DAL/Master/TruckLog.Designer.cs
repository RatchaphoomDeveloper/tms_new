﻿namespace TMS_DAL.Master
{
    partial class TruckLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdTruckAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdOldDataTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdOldCapacity = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckInsurance = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckByTruckId = new System.Data.OracleClient.OracleCommand();

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdTruckAdd;
        private System.Data.OracleClient.OracleCommand cmdOldDataTruck;
        private System.Data.OracleClient.OracleCommand cmdOldCapacity;
        private System.Data.OracleClient.OracleCommand cmdTruckInsurance;
        private System.Data.OracleClient.OracleCommand cmdTruckByTruckId;
    }
}
