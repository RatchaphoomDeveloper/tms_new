﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class QuestionnaireDAL : OracleConnectionDAL
    {
        public QuestionnaireDAL()
        {
            InitializeComponent();
        }

        public QuestionnaireDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region + Instance +
        private static QuestionnaireDAL _instance;
        public static QuestionnaireDAL Instance
        {
            get
            {
                _instance = new QuestionnaireDAL();
                return _instance;
            }
        }
        #endregion
    }
}
