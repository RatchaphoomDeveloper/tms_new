﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;

namespace TMS_DAL.Master
{
    public partial class TopicDAL : OracleConnectionDAL
    {
        #region TopicDAL
        public TopicDAL()
        {
            InitializeComponent();
        }

        public TopicDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #endregion

        #region TopicSelect
        public DataTable TopicSelect(string Condition)
        {
            string Query = cmdTopicSelect.CommandText;
            try
            {
                cmdTopicSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdTopicSelect, "dtTopic");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTopicSelect.CommandText = Query;
            }
        }
        #endregion

        #region TopicSelect
        public DataTable TopicSelect(string TOPIC_NAME, string CACTIVE, string TYPE)
        {
            try
            {
                cmdTopic.CommandText = "SELECT TOPIC_ID, TOPIC_NAME,ISACTIVE,TYPE FROM M_TOPIC WHERE (ISACTIVE = :CACTIVE OR ((:CACTIVE IS NULL OR :CACTIVE = '') AND 0=0)) AND (TYPE = :TYPE OR ((:TYPE IS NULL OR :TYPE = '') AND 0=0)) AND TOPIC_NAME LIKE :TOPIC_NAME";
                cmdTopic.Parameters.Clear();
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_NAME",
                    Value = "%" + TOPIC_NAME + "%",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TYPE",
                    Value = TYPE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdTopic, "cmdTopic");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TopicCheckValidate
        public bool TopicCheckValidate(int TOPIC_ID, string TOPIC_NAME, string TYPE)
        {
            try
            {
                cmdTopic.CommandText = "SELECT COUNT(TOPIC_ID) COUNTID FROM M_TOPIC WHERE (ISACTIVE = 1) AND TOPIC_NAME = :TOPIC_NAME AND TYPE = :TYPE AND TOPIC_ID != :TOPIC_ID";
                cmdTopic.Parameters.Clear();
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_NAME",
                    Value = TOPIC_NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TYPE",
                    Value = TYPE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_ID",
                    Value = TOPIC_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdTopic, "cmdTopic");
                if (dt.Rows.Count > 0 && int.Parse(dt.Rows[0]["COUNTID"] + string.Empty) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TopicInsert
        public bool TopicInsert(string TOPIC_NAME, string CACTIVE, string TYPE, string USERID)
        {
            try
            {
                cmdTopic.CommandText = @"INSERT INTO M_TOPIC(TOPIC_NAME, ISACTIVE,TYPE,CREATE_BY ,CREATE_DATETIME
      ,UPDATE_BY
      ,UPDATE_DATETIME) 
VALUES (:TOPIC_NAME, :CACTIVE, :TYPE,:USERID,SYSDATE,:USERID,SYSDATE) RETURNING TOPIC_ID INTO :TOPIC_ID";
                #region Parameter
                cmdTopic.Parameters.Clear();
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_NAME",
                    Value = TOPIC_NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TYPE",
                    Value = TYPE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "USERID",
                    Value = USERID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "TOPIC_ID",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                #endregion

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdTopic);
                if (row > 0)
                {
                    int TOPIC_ID = int.Parse(cmdTopic.Parameters["TOPIC_ID"].Value + string.Empty);
                    cmdTopic.CommandText = "INSERT INTO M_REQUIRE_LEVEL_DATA(DETAIL, PARENTID,MREQUIRELEVELID,REALID,CACTIVE) VALUES (:DETAIL, 0,1,:REALID,:CACTIVE)";
                    cmdTopic.Parameters.Clear();

                    cmdTopic.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "REALID",
                        Value = TOPIC_ID,
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdTopic.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "CACTIVE",
                        Value = CACTIVE,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    cmdTopic.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "DETAIL",
                        Value = TOPIC_NAME,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    row = dbManager.ExecuteNonQuery(cmdTopic);
//                    if (row >= 0)
//                    {
//                        cmdTopic.CommandText = "INSERT INTO M_COMPLAIN_REQUIRE_FIELD( " +
//   "COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,CREATE_DATETIME,UPDATE_BY,UPDATE_DATETIME,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION) " +
//"SELECT :TOPIC_ID AS COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,SYSDATE,UPDATE_BY,SYSDATE,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION FROM M_COMPLAIN_REQUIRE_FIELD WHERE FIELD_TYPE = :TYPE AND COMPLAIN_TYPE_ID = 0";
//                        cmdTopic.Parameters.Clear();

//                        cmdTopic.Parameters.Add(new OracleParameter()
//                        {
//                            Direction = ParameterDirection.Input,
//                            ParameterName = "TOPIC_ID",
//                            Value = TOPIC_ID,
//                            OracleType = OracleType.Number,
//                            IsNullable = true,
//                        });
//                        cmdTopic.Parameters.Add(new OracleParameter()
//                        {
//                            Direction = ParameterDirection.Input,
//                            ParameterName = "TYPE",
//                            Value = TYPE,
//                            OracleType = OracleType.Number,
//                            IsNullable = true,
//                        });
//                        row = dbManager.ExecuteNonQuery(cmdTopic);
//                        if (row >= 0)
//                        {
//                            cmdTopic.CommandText = "INSERT INTO M_COMPLAIN_REQUEST_FILE(" +
//  "COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,CREATE_DATETIME,CREATE_BY,UPDATE_BY,UPDATE_DATETIME,REQUEST_TYPE) " +
//  "SELECT :TOPIC_ID COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,SYSDATE,CREATE_BY,UPDATE_BY,SYSDATE,REQUEST_TYPE FROM M_COMPLAIN_REQUEST_FILE" +
//   " WHERE REQUEST_TYPE = :TYPE AND COMPLAIN_TYPE_ID = 0";
//                            cmdTopic.Parameters.Clear();
//                            cmdTopic.Parameters.Add(new OracleParameter()
//                            {
//                                Direction = ParameterDirection.Input,
//                                ParameterName = "TOPIC_ID",
//                                Value = TOPIC_ID,
//                                OracleType = OracleType.Number,
//                                IsNullable = true,
//                            });
//                            cmdTopic.Parameters.Add(new OracleParameter()
//                            {
//                                Direction = ParameterDirection.Input,
//                                ParameterName = "TYPE",
//                                Value = TYPE,
//                                OracleType = OracleType.Number,
//                                IsNullable = true,
//                            });
//                            dbManager.ExecuteNonQuery(cmdTopic);
//                            isRes = true;
//                        }

//                    }
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region TopicUpdate
        public bool TopicUpdate(int TOPIC_ID, string TOPIC_NAME, string CACTIVE, string TYPE, string USERID)
        {
            try
            {
                cmdTopic.CommandText = @"UPDATE M_TOPIC SET TOPIC_NAME = :TOPIC_NAME,ISACTIVE = :CACTIVE,TYPE = :TYPE,UPDATE_BY = :USERID
      ,UPDATE_DATETIME = sysdate WHERE TOPIC_ID = :TOPIC_ID";
                cmdTopic.Parameters.Clear();

                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_NAME",
                    Value = TOPIC_NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TYPE",
                    Value = TYPE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_ID",
                    Value = TOPIC_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "USERID",
                    Value = USERID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdTopic);
                if (row > 0)
                {
                    cmdTopic.CommandText = "UPDATE M_REQUIRE_LEVEL_DATA " +
                      "SET DETAIL = :DETAIL " +
                      ",CACTIVE = :CACTIVE " +
                      "WHERE REALID = :REALID AND MREQUIRELEVELID = 1 AND PARENTID = 0";
                    cmdTopic.Parameters.Clear();
                    cmdTopic.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "REALID",
                        Value = TOPIC_ID,
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdTopic.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "DETAIL",
                        Value = TOPIC_NAME,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    cmdTopic.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "CACTIVE",
                        Value = CACTIVE,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    row = dbManager.ExecuteNonQuery(cmdTopic);
                    if (row == 0)
                    {
                        #region Insert M_REQUIRE_LEVEL_DATA
                        cmdTopic.CommandText = "INSERT INTO M_REQUIRE_LEVEL_DATA(DETAIL, PARENTID,MREQUIRELEVELID,REALID,CACTIVE) VALUES (:DETAIL, 0,1,:REALID,:CACTIVE)";
                        cmdTopic.Parameters.Clear();

                        cmdTopic.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "REALID",
                            Value = TOPIC_ID,
                            OracleType = OracleType.Number,
                            IsNullable = true,
                        });
                        cmdTopic.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "CACTIVE",
                            Value = CACTIVE,
                            OracleType = OracleType.VarChar,
                            IsNullable = true,
                        });
                        cmdTopic.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "DETAIL",
                            Value = TOPIC_NAME,
                            OracleType = OracleType.VarChar,
                            IsNullable = true,
                        });
                        row = dbManager.ExecuteNonQuery(cmdTopic);
                        #endregion
                    }
//                    cmdTopic.CommandText = "UPDATE M_COMPLAIN_REQUIRE_FIELD " +
//                  "SET ISACTIVE = :CACTIVE " +
//                  "WHERE COMPLAIN_TYPE_ID = :PERSON_TYPE AND  FIELD_TYPE = :TYPE";
//                    cmdTopic.Parameters.Clear();
//                    cmdTopic.Parameters.Add(new OracleParameter()
//                    {
//                        Direction = ParameterDirection.Input,
//                        ParameterName = "PERSON_TYPE",
//                        Value = TOPIC_ID,
//                        OracleType = OracleType.Number,
//                        IsNullable = true,
//                    });
//                    cmdTopic.Parameters.Add(new OracleParameter()
//                    {
//                        Direction = ParameterDirection.Input,
//                        ParameterName = "CACTIVE",
//                        Value = CACTIVE,
//                        OracleType = OracleType.VarChar,
//                        IsNullable = true,
//                    });
//                    cmdTopic.Parameters.Add(new OracleParameter()
//                    {
//                        Direction = ParameterDirection.Input,
//                        ParameterName = "TYPE",
//                        Value = TYPE,
//                        OracleType = OracleType.VarChar,
//                        IsNullable = true,
//                    });
//                    row = dbManager.ExecuteNonQuery(cmdTopic);
//                    if (row == 0)
//                    {
//                        #region Insert M_COMPLAIN_REQUIRE_FIELD
//                        cmdTopic.CommandText = "INSERT INTO M_COMPLAIN_REQUIRE_FIELD( " +
//   "COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,CREATE_DATETIME,UPDATE_BY,UPDATE_DATETIME,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION) " +
//"SELECT :TOPIC_ID AS COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,SYSDATE,UPDATE_BY,SYSDATE,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION FROM M_COMPLAIN_REQUIRE_FIELD WHERE FIELD_TYPE = :TYPE AND COMPLAIN_TYPE_ID = 0";
//                        cmdTopic.Parameters.Clear();

//                        cmdTopic.Parameters.Add(new OracleParameter()
//                        {
//                            Direction = ParameterDirection.Input,
//                            ParameterName = "TOPIC_ID",
//                            Value = TOPIC_ID,
//                            OracleType = OracleType.Number,
//                            IsNullable = true,
//                        });
//                        cmdTopic.Parameters.Add(new OracleParameter()
//                        {
//                            Direction = ParameterDirection.Input,
//                            ParameterName = "TYPE",
//                            Value = TYPE,
//                            OracleType = OracleType.Number,
//                            IsNullable = true,
//                        });
//                        row = dbManager.ExecuteNonQuery(cmdTopic);
//                        #endregion
//                    }

//                    cmdTopic.CommandText = "UPDATE M_COMPLAIN_REQUEST_FILE " +
//                  "SET ISACTIVE = :CACTIVE " +
//                  "WHERE COMPLAIN_TYPE_ID = :PERSON_TYPE AND  REQUEST_TYPE = :TYPE";
//                    cmdTopic.Parameters.Clear();
//                    cmdTopic.Parameters.Add(new OracleParameter()
//                    {
//                        Direction = ParameterDirection.Input,
//                        ParameterName = "PERSON_TYPE",
//                        Value = TOPIC_ID,
//                        OracleType = OracleType.Number,
//                        IsNullable = true,
//                    });
//                    cmdTopic.Parameters.Add(new OracleParameter()
//                    {
//                        Direction = ParameterDirection.Input,
//                        ParameterName = "CACTIVE",
//                        Value = CACTIVE,
//                        OracleType = OracleType.VarChar,
//                        IsNullable = true,
//                    });
//                    cmdTopic.Parameters.Add(new OracleParameter()
//                    {
//                        Direction = ParameterDirection.Input,
//                        ParameterName = "TYPE",
//                        Value = TYPE,
//                        OracleType = OracleType.VarChar,
//                        IsNullable = true,
//                    });
//                    row = dbManager.ExecuteNonQuery(cmdTopic);
//                    if (row == 0)
//                    {
//                        cmdTopic.CommandText = "INSERT INTO M_COMPLAIN_REQUEST_FILE(" +
//  "COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,CREATE_DATETIME,CREATE_BY,UPDATE_BY,UPDATE_DATETIME,REQUEST_TYPE) " +
//  "SELECT :TOPIC_ID COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,SYSDATE,CREATE_BY,UPDATE_BY,SYSDATE,REQUEST_TYPE FROM M_COMPLAIN_REQUEST_FILE" +
//   " WHERE REQUEST_TYPE = :TYPE AND COMPLAIN_TYPE_ID = 0";
//                        cmdTopic.Parameters.Clear();
//                        cmdTopic.Parameters.Add(new OracleParameter()
//                        {
//                            Direction = ParameterDirection.Input,
//                            ParameterName = "TOPIC_ID",
//                            Value = TOPIC_ID,
//                            OracleType = OracleType.Number,
//                            IsNullable = true,
//                        });
//                        cmdTopic.Parameters.Add(new OracleParameter()
//                        {
//                            Direction = ParameterDirection.Input,
//                            ParameterName = "TYPE",
//                            Value = TYPE,
//                            OracleType = OracleType.Number,
//                            IsNullable = true,
//                        });
//                        dbManager.ExecuteNonQuery(cmdTopic);
//                    }
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion


        #region ComplainTypeSelect
        public DataTable ComplainTypeSelect(int TOPIC_ID, string COMPLAIN_TYPE_NAME, string CACTIVE)
        {
            try
            {
                cmdTopic.CommandText = "SELECT M_COMPLAIN_TYPE.* FROM M_COMPLAIN_TYPE " +
                "LEFT JOIN M_TOPIC ON M_TOPIC.TOPIC_ID = M_COMPLAIN_TYPE.TOPIC_ID " +
                "WHERE (M_COMPLAIN_TYPE.TOPIC_ID = :TOPIC_ID OR (:TOPIC_ID = 0 AND 0=0)) " +
                "AND M_COMPLAIN_TYPE.COMPLAIN_TYPE_NAME LIKE :COMPLAIN_TYPE_NAME  " +
                "AND (M_COMPLAIN_TYPE.CACTIVE = :CACTIVE OR ((:CACTIVE IS NULL OR  :CACTIVE = '') AND 0=0)) " +
                "AND M_TOPIC.CACTIVE ='1'";
                cmdTopic.Parameters.Clear();
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_ID",
                    Value = TOPIC_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "COMPLAIN_TYPE_NAME",
                    Value = "%" + COMPLAIN_TYPE_NAME + "%",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdTopic, "cmdTopic");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ComplainTypeCheckValidate
        public bool ComplainTypeCheckValidate(int COMPLAIN_TYPE_ID, string COMPLAIN_TYPE_NAME)
        {
            try
            {
                cmdTopic.CommandText = "SELECT COUNT(ID) COUNTID FROM M_COMPLAIN_TYPE WHERE (CACTIVE = 1) AND COMPLAIN_TYPE_NAME = :COMPLAIN_TYPE_NAME AND COMPLAIN_TYPE_ID != :COMPLAIN_TYPE_ID";
                cmdTopic.Parameters.Clear();
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "COMPLAIN_TYPE_NAME",
                    Value = COMPLAIN_TYPE_NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "COMPLAIN_TYPE_ID",
                    Value = COMPLAIN_TYPE_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdTopic, "cmdTopic");
                if (dt.Rows.Count > 0 && int.Parse(dt.Rows[0]["COUNTID"] + string.Empty) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ComplainTypeInsert
        public bool ComplainTypeInsert(int TOPIC_ID, string COMPLAIN_TYPE_NAME, string CACTIVE, string LOCK_DRIVER, string IS_ADD_MULTIPLE_CAR, string USERID)
        {
            try
            {
                cmdTopic.CommandText = @"INSERT INTO M_COMPLAIN_TYPE(TOPIC_ID,COMPLAIN_TYPE_NAME, CACTIVE,LOCK_DRIVER,IS_ADD_MULTIPLE_CAR,CREATE_BY ,CREATE_DATETIME
      ,UPDATE_BY
      ,UPDATE_DATETIME) VALUES (:TOPIC_ID,:COMPLAIN_TYPE_NAME, :CACTIVE,:LOCK_DRIVER,:IS_ADD_MULTIPLE_CAR,:USERID ,sysdate
      ,:USERID
      ,sysdate)";
                cmdTopic.Parameters.Clear();
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_ID",
                    Value = TOPIC_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "COMPLAIN_TYPE_NAME",
                    Value = COMPLAIN_TYPE_NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LOCK_DRIVER",
                    Value = LOCK_DRIVER,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IS_ADD_MULTIPLE_CAR",
                    Value = IS_ADD_MULTIPLE_CAR,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "USERID",
                    Value = USERID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdTopic);
                if (row > 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region ComplainTypeUpdate
        public bool ComplainTypeUpdate(int COMPLAIN_TYPE_ID, int TOPIC_ID, string COMPLAIN_TYPE_NAME, string CACTIVE, string LOCK_DRIVER, string IS_ADD_MULTIPLE_CAR, string USERID)
        {
            try
            {
                cmdTopic.CommandText = @"UPDATE M_COMPLAIN_TYPE 
SET COMPLAIN_TYPE_NAME = :COMPLAIN_TYPE_NAME
,CACTIVE = :CACTIVE 
,TOPIC_ID = :TOPIC_ID 
,LOCK_DRIVER = :LOCK_DRIVER 
,IS_ADD_MULTIPLE_CAR = :IS_ADD_MULTIPLE_CAR 
,UPDATE_BY = :USERID 
,UPDATE_DATETIME = sysdate 
WHERE COMPLAIN_TYPE_ID = :COMPLAIN_TYPE_ID";
                cmdTopic.Parameters.Clear();
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "COMPLAIN_TYPE_ID",
                    Value = COMPLAIN_TYPE_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "TOPIC_ID",
                    Value = TOPIC_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "COMPLAIN_TYPE_NAME",
                    Value = COMPLAIN_TYPE_NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LOCK_DRIVER",
                    Value = LOCK_DRIVER,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IS_ADD_MULTIPLE_CAR",
                    Value = IS_ADD_MULTIPLE_CAR,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdTopic.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "USERID",
                    Value = USERID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdTopic);
                if (row >= 0)
                {
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        public DataTable TopicSelectAllDAL(string Condition)
        {
            string Query = cmdTopicSelectAll.CommandText;
            try
            {
                dbManager.Open();
                cmdTopicSelectAll.CommandText = @"SELECT TOPIC_ID, TOPIC_NAME, CASE WHEN ISACTIVE = 0 THEN 'InActive' ELSE 'Active' END AS ISACTIVE
                                                       , CASE WHEN TYPE = 'CHECKTRUCK' THEN 'Surprise Check' WHEN TYPE = 'COMPLAIN' THEN 'Complain' WHEN TYPE = 'TRUCK' THEN 'Truck' END AS TYPE
                                                       , TYPE AS TYPE_VALUE, ISACTIVE AS ISACTIVE_VALUE
                                                  FROM M_TOPIC
                                                  WHERE 1=1" + Condition +
                                                  " ORDER BY TYPE";

                return dbManager.ExecuteDataTable(cmdTopicSelectAll, "cmdTopicSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdTopicSelectAll.CommandText = Query;
            }
        }

        public void TopicAddDAL(string TopicName, int IsActive, int UserID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdTopicCheck.CommandText = @"SELECT COUNT(*) AS COUNT
                                              FROM M_TOPIC
                                              WHERE TOPIC_NAME =:I_TOPIC_NAME AND TYPE ='COMPLAIN'";

                cmdTopicCheck.Parameters["I_TOPIC_NAME"].Value = TopicName;
                DataTable dt = dbManager.ExecuteDataTable(cmdTopicCheck, "cmdTopicCheck");
                if ((dt.Rows.Count > 0) && (int.Parse(dt.Rows[0]["COUNT"].ToString()) > 0))
                    throw new Exception("ข้อมูล ประเภทร้องเรียน ซ้ำกับในระบบ");

                cmdTopicAdd.CommandText = @"INSERT INTO M_TOPIC (TOPIC_NAME, ISACTIVE, TYPE, CREATE_BY)
                                            VALUES (:I_TOPIC_NAME, :I_ISACTIVE, 'COMPLAIN', :I_CREATE_BY)";

                cmdTopicAdd.Parameters["I_TOPIC_NAME"].Value = TopicName;
                cmdTopicAdd.Parameters["I_ISACTIVE"].Value = IsActive;
                cmdTopicAdd.Parameters["I_CREATE_BY"].Value = UserID;

                dbManager.ExecuteNonQuery(cmdTopicAdd);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void TopicAddDetailDAL(int TopicID, string ComplainTypeName, int IsActive, int UserID, int LockDriver, int IsAddMultipleCar)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainTypeCheck.CommandText = @"SELECT COUNT(*) AS COUNT
                                                     FROM M_COMPLAIN_TYPE
                                                     WHERE TOPIC_ID =:I_TOPIC_ID AND COMPLAIN_TYPE_NAME =:I_COMPLAIN_TYPE_NAME";

                cmdComplainTypeCheck.Parameters["I_TOPIC_ID"].Value = TopicID;
                cmdComplainTypeCheck.Parameters["I_COMPLAIN_TYPE_NAME"].Value = ComplainTypeName;
                DataTable dt = dbManager.ExecuteDataTable(cmdComplainTypeCheck, "cmdComplainTypeCheck");
                if ((dt.Rows.Count > 0) && (int.Parse(dt.Rows[0]["COUNT"].ToString()) > 0))
                    throw new Exception("ข้อมูล หัวข้อ ซ้ำกับในระบบ");

                cmdComplainTypeInsert.CommandText = @"INSERT INTO M_COMPLAIN_TYPE (TOPIC_ID, ISACTIVE, COMPLAIN_TYPE_NAME, CREATE_BY, LOCK_DRIVER, IS_ADD_MULTIPLE_CAR)
                                                      VALUES (:I_TOPIC_ID, :I_ISACTIVE, :I_COMPLAIN_TYPE_NAME, :I_CREATE_BY, :I_LOCK_DRIVER, :I_IS_ADD_MULTIPLE_CAR)";

                cmdComplainTypeInsert.Parameters["I_TOPIC_ID"].Value = TopicID;
                cmdComplainTypeInsert.Parameters["I_ISACTIVE"].Value = IsActive;
                cmdComplainTypeInsert.Parameters["I_COMPLAIN_TYPE_NAME"].Value = ComplainTypeName;
                cmdComplainTypeInsert.Parameters["I_CREATE_BY"].Value = UserID;
                cmdComplainTypeInsert.Parameters["I_LOCK_DRIVER"].Value = LockDriver;
                cmdComplainTypeInsert.Parameters["I_IS_ADD_MULTIPLE_CAR"].Value = IsAddMultipleCar;

                dbManager.ExecuteNonQuery(cmdComplainTypeInsert);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void TopicUpdateDAL(int TopicID, int Topic_Active, int ComplainID, string ComplainTypeName, int Complain_Active, int UserID, int LockDriver, int IsAddMultipleCar)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdTopicUpdate.CommandText = @"UPDATE M_TOPIC
                                               SET ISACTIVE =:I_TOPIC_ACTIVE, UPDATE_BY =:I_USER_ID" +
                                             " WHERE TOPIC_ID =:I_TOPIC_ID";

                cmdTopicUpdate.Parameters["I_TOPIC_ID"].Value = TopicID;
                cmdTopicUpdate.Parameters["I_TOPIC_ACTIVE"].Value = Topic_Active;
                cmdTopicUpdate.Parameters["I_USER_ID"].Value = UserID;

                dbManager.ExecuteNonQuery(cmdTopicUpdate);

                if (ComplainID != 0)
                {
                    cmdComplainTypeUpdate.CommandText = @"UPDATE M_COMPLAIN_TYPE
                                                          SET COMPLAIN_TYPE_NAME =:I_COMPLAIN_TYPE_NAME, ISACTIVE =:I_ISACTIVE, UPDATE_BY =:I_CREATE_BY, LOCK_DRIVER =:I_LOCK_DRIVER, IS_ADD_MULTIPLE_CAR =:I_IS_ADD_MULTIPLE_CAR" +
                                                        " WHERE COMPLAIN_TYPE_ID =:I_COMPLAIN_TYPE_ID";

                    cmdComplainTypeUpdate.Parameters["I_ISACTIVE"].Value = Complain_Active;
                    cmdComplainTypeUpdate.Parameters["I_COMPLAIN_TYPE_ID"].Value = ComplainID;
                    cmdComplainTypeUpdate.Parameters["I_COMPLAIN_TYPE_NAME"].Value = ComplainTypeName;
                    cmdComplainTypeUpdate.Parameters["I_CREATE_BY"].Value = UserID;
                    cmdComplainTypeUpdate.Parameters["I_LOCK_DRIVER"].Value = LockDriver;
                    cmdComplainTypeUpdate.Parameters["I_IS_ADD_MULTIPLE_CAR"].Value = IsAddMultipleCar;

                    dbManager.ExecuteNonQuery(cmdComplainTypeUpdate);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static TopicDAL _instance;
        public static TopicDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new TopicDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}