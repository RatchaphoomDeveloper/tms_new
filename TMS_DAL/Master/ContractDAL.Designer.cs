﻿namespace TMS_DAL.Master
{
    partial class ContractDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractDAL));
            this.cmdTTERMINALSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTransportTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdContractTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdProductTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdVEHSelectByPagesize = new System.Data.OracleClient.OracleCommand();
            this.cmdTUSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdGUARANTEETYPESelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCONTRACTSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCONTRACTAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdCONTRACTAttachInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdCONTRACTEdit = new System.Data.OracleClient.OracleCommand();
            this.cmdMContractSelectByPagesize = new System.Data.OracleClient.OracleCommand();
            this.cmdConTractListSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdConTractSelectByVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckInContract = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckContractExpire = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverContractExpire = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateContractExpire = new System.Data.OracleClient.OracleCommand();
            this.cmdSpotDeliverySelect = new System.Data.OracleClient.OracleCommand();
            this.cmdAfterCONTRACTSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdContractDetail = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTTERMINALSelect
            // 
            this.cmdTTERMINALSelect.CommandText = resources.GetString("cmdTTERMINALSelect.CommandText");
            // 
            // cmdTransportTypeSelect
            // 
            this.cmdTransportTypeSelect.CommandText = "SELECt STRANTYPEID ,STRANTYPENAME FROM TTRANSPORTTYPE";
            // 
            // cmdContractTypeSelect
            // 
            this.cmdContractTypeSelect.CommandText = "SELECt * FROM TCONTRACTTYPE";
            // 
            // cmdProductTypeSelect
            // 
            this.cmdProductTypeSelect.CommandText = "SELECT SPRODUCTTYPEID,SPRODUCTTYPENAME FROM TPRODUCTTYPE WHERE ACTIVE = 1";
            // 
            // cmdVEHSelectByPagesize
            // 
            this.cmdVEHSelectByPagesize.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILLTER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("STARTINDEX", System.Data.OracleClient.OracleType.Int32),
            new System.Data.OracleClient.OracleParameter("ENDINDEX", System.Data.OracleClient.OracleType.Int32),
            new System.Data.OracleClient.OracleParameter("STRANSPORTID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdTUSelect
            // 
            this.cmdTUSelect.CommandText = "SELECT STRUCKID As STRAILERID,SHEADREGISTERNO As TU_NO FROM TTRUCK WHERE SCARTYPE" +
    "ID=\'4\' AND SHEADID=:VEH_ID";
            this.cmdTUSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("VEH_ID", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdGUARANTEETYPESelect
            // 
            this.cmdGUARANTEETYPESelect.CommandText = "SELECT * FROM TGUARANTEESTYPE WHERE NVL( CACTIVE,\'1\')=\'1\'";
            // 
            // cmdCONTRACTSelect
            // 
            this.cmdCONTRACTSelect.CommandText = "USP_T_CONTRACT_SELECT";
            this.cmdCONTRACTSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("O_GUARANTEE", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_INFORMATION", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_PLANT", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_GPRODUCT", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_TRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_UPLOAD", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdCONTRACTAdd
            // 
            this.cmdCONTRACTAdd.CommandText = "USP_T_CONTRACT_ADD";
            this.cmdCONTRACTAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CONTRACTID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MODIFY_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTTYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTNO", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTNONEW", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_VENDOR", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_STARTDATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_ENDDATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTTRUCK", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_USERID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_PROCUREMENT", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_MAINPLANT", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_PLNT", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_RENEWFROM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_GUARANTEE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTPLANT", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_GPRODUCT", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_TRUCK", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_GROUPSID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_NTRUCKCONTRACT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_NTRUCKRESERVE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdImportFileDelete
            // 
            this.cmdImportFileDelete.CommandText = "DELETE FROM F_UPLOAD WHERE REF_STR =: I_DOC_ID";
            this.cmdImportFileDelete.Connection = this.OracleConn;
            this.cmdImportFileDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdCONTRACTAttachInsert
            // 
            this.cmdCONTRACTAttachInsert.CommandText = resources.GetString("cmdCONTRACTAttachInsert.CommandText");
            this.cmdCONTRACTAttachInsert.Connection = this.OracleConn;
            this.cmdCONTRACTAttachInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500)});
            // 
            // cmdCONTRACTEdit
            // 
            this.cmdCONTRACTEdit.CommandText = "USP_T_CONTRACT_Edit";
            this.cmdCONTRACTEdit.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CONTRACTID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MODIFY_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTTYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTNO", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTNONEW", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_VENDOR", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_STARTDATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_ENDDATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTTRUCK", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_USERID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_PROCUREMENT", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_MAINPLANT", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_PLNT", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_RENEWFROM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_GUARANTEE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CONTRACTPLANT", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_GPRODUCT", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_TRUCK", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_GROUPSID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_NTRUCKCONTRACT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_NTRUCKRESERVE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdMContractSelectByPagesize
            // 
            this.cmdMContractSelectByPagesize.CommandText = "SELECT ID,NAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY ID DESC) AS RN , ID,NAME FR" +
    "OM M_CONTRACT MC WHERE CACTIVE IN (\'1\') AND NAME LIKE :fillter) WHERE RN BETWEEN" +
    " :startindex AND :endindex";
            this.cmdMContractSelectByPagesize.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("fillter", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("startIndex", System.Data.OracleClient.OracleType.Int32),
            new System.Data.OracleClient.OracleParameter("endIndex", System.Data.OracleClient.OracleType.Int32)});
            // 
            // cmdConTractListSelect
            // 
            this.cmdConTractListSelect.CommandText = resources.GetString("cmdConTractListSelect.CommandText");
            this.cmdConTractListSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("O_GUARANTEE", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_INFORMATION", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_PLANT", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_GPRODUCT", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("O_TRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdCheckTruckInContract
            // 
            this.cmdCheckTruckInContract.CommandText = "FC_CHECK_TRUCK_INCONTRACT";
            this.cmdCheckTruckInContract.Connection = this.OracleConn;
            this.cmdCheckTruckInContract.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_STRUCKID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("VRETURN", System.Data.OracleClient.OracleType.Number, 0, System.Data.ParameterDirection.ReturnValue, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdAfterCONTRACTSelect
            // 
            this.cmdAfterCONTRACTSelect.CommandText = "USP_M_AFTER_CONTRACT_SELECT";
            this.cmdAfterCONTRACTSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_STRUCKID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdContractDetail
            // 
            this.cmdContractDetail.CommandText = "SELECt * FROM VW_M_CONTRACT_DETAIL_SELECT WHERE 1=1 ";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdTTERMINALSelect;
        private System.Data.OracleClient.OracleCommand cmdTransportTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdContractTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdProductTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdVEHSelectByPagesize;
        private System.Data.OracleClient.OracleCommand cmdTUSelect;
        private System.Data.OracleClient.OracleCommand cmdGUARANTEETYPESelect;
        private System.Data.OracleClient.OracleCommand cmdCONTRACTSelect;
        private System.Data.OracleClient.OracleCommand cmdCONTRACTAdd;
        private System.Data.OracleClient.OracleCommand cmdImportFileDelete;
        private System.Data.OracleClient.OracleCommand cmdCONTRACTAttachInsert;
        private System.Data.OracleClient.OracleCommand cmdCONTRACTEdit;
        private System.Data.OracleClient.OracleCommand cmdMContractSelectByPagesize;
        private System.Data.OracleClient.OracleCommand cmdConTractListSelect;
        private System.Data.OracleClient.OracleCommand cmdConTractSelectByVendor;
		private System.Data.OracleClient.OracleCommand cmdCheckTruckInContract;
        private System.Data.OracleClient.OracleCommand cmdTruckContractExpire;
        private System.Data.OracleClient.OracleCommand cmdDriverContractExpire;
        private System.Data.OracleClient.OracleCommand cmdUpdateContractExpire;
        private System.Data.OracleClient.OracleCommand cmdSpotDeliverySelect;
        private System.Data.OracleClient.OracleCommand cmdAfterCONTRACTSelect;
        private System.Data.OracleClient.OracleCommand cmdContractDetail;
    }
}
