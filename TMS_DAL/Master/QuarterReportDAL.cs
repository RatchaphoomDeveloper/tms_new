﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class QuarterReportDAL : OracleConnectionDAL
    {
        #region QuarterReportDAL
        public QuarterReportDAL()
        {
            InitializeComponent();
        }

        public QuarterReportDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion
        
        #region + Instance +
        private static QuarterReportDAL _instance;
        public static QuarterReportDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new QuarterReportDAL();
                //}
                return _instance;
            }
        }
        #endregion

        #region GetData
        public DataTable GetData(string I_YEAR, string I_SVENDORID, string I_SPROCESSID, int I_FIX)
        {
            try
            {
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = "USP_M_QUARTERY_REPORT";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    OracleType = OracleType.VarChar,
                    Value = I_YEAR,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    OracleType = OracleType.VarChar,
                    Value = I_SVENDORID,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SPROCESSID",
                    OracleType = OracleType.VarChar,
                    Value = I_SPROCESSID,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIX",
                    OracleType = OracleType.Number,
                    Value = I_FIX,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdQuarterReport, "cmdQuarterReport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetDataVendorConfig
        public DataTable GetDataVendorConfig(string I_YEAR, string I_SVENDORID, string I_SPROCESSID, int I_FIX)
        {
            try
            {
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = "USP_M_QUARTERY_CONFIG_REPORT";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    OracleType = OracleType.VarChar,
                    Value = I_YEAR,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    OracleType = OracleType.VarChar,
                    Value = I_SVENDORID,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SPROCESSID",
                    OracleType = OracleType.VarChar,
                    Value = I_SPROCESSID,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIX",
                    OracleType = OracleType.Number,
                    Value = I_FIX,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdQuarterReport, "cmdQuarterReport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetVendor
        public DataTable GetVendor(string condition)
        {
            string Query = cmdGetVendorContract.CommandText.ToString();
            try
            {
                return dbManager.ExecuteDataTable(cmdGetVendorContract, "cmdGetVendorContract");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetSPROCESSID
        public DataTable GetSPROCESSID()
        {
            
            try
            {
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = "USP_T_SPROCESSID_SELECT";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdQuarterReport, "cmdQuarterReport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Save
        public bool Save(string I_ID, string I_YEAR, string I_QUARTER, string I_SVENDORID
            , string I_DOCNO, string I_DATESTR, string I_USERID, DataTable I_DATA)
        {
            try
            {
                DataSet ds = new DataSet("DS");
                I_DATA.TableName = "DT";
                ds.Tables.Add(I_DATA.Copy());
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = @"USP_M_QUARTER_SAVE";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = string.IsNullOrEmpty(I_YEAR) ? 0 : int.Parse(I_YEAR),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_QUARTER",
                    Value = string.IsNullOrEmpty(I_QUARTER) ? 0 : int.Parse(I_QUARTER),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DOCNO",
                    Value = I_DOCNO,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATESTR",
                    Value = I_DATESTR,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = I_USERID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATA",
                    Value = ds.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                 });
                
                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdQuarterReport);
                if (row >= 0)
                {
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region SaveLog
        public bool SaveLog(string I_YEARS, string I_QUARTER, string I_SVENDORID, string I_LOGVERSION, string I_USERID)
        {
            try
            {

                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = @"USP_M_QUARTER_LOG_SAVE";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEARS",
                    Value = string.IsNullOrEmpty(I_YEARS) ? 0 : int.Parse(I_YEARS),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_QUARTER",
                    Value = string.IsNullOrEmpty(I_QUARTER) ? 0 : int.Parse(I_QUARTER),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_LOGVERSION",
                    Value = I_LOGVERSION,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = I_USERID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });


                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdQuarterReport);
                if (row >= 0)
                {
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region Config Quarter
        #region ConfigQuarterSave
        public bool ConfigQuarterSave(string I_ID, string I_YEARS, string I_QUARTER, string I_USERID, DataTable dtTitle)
        {
            try
            {
                DataSet dsTitle = new DataSet("DS");
                DataTable dtI = dtTitle.Copy();
                dtI.TableName = "DT";
                dsTitle.Tables.Add(dtI);
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = "USP_M_CONFIG_QUARTER_SAVE";
                cmdQuarterReport.Parameters.Clear();

                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = !string.IsNullOrEmpty(I_ID) ? int.Parse(I_ID) : 0,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEARS",
                    Value = !string.IsNullOrEmpty(I_YEARS) ? int.Parse(I_YEARS) : 0,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_QUARTER",
                    Value = !string.IsNullOrEmpty(I_QUARTER) ? int.Parse(I_QUARTER) : 0,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = !string.IsNullOrEmpty(I_USERID) ? int.Parse(I_USERID) : 0,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TITLE",
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                    Value = dsTitle.GetXml()
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdQuarterReport);
                isRes = true;
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region ConfigQuarterSelect
        public DataTable ConfigQuarterSelect(string I_YEARS, string I_QUARTER)
        {

            try
            {
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = "USP_M_CONFIG_QUARTER_SELECT";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEARS",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                    Value = !string.IsNullOrEmpty(I_YEARS) ? int.Parse(I_YEARS) : 0,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_QUARTER",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                    Value = !string.IsNullOrEmpty(I_QUARTER) ? int.Parse(I_QUARTER) : 0,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdQuarterReport, "cmdQuarterReport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region Vendor
        public DataTable GetDataQuarterByVendor(string I_YEAR, string I_SVENDORID, string I_QUARTER, string I_CACTIVE)
        {
            try
            {
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = "USP_M_QUARTER_VENDOR_SELECT";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_YEAR) ? int.Parse(I_YEAR) : 0,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    OracleType = OracleType.VarChar,
                    Value = I_SVENDORID,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_QUARTER",
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_QUARTER) ? int.Parse(I_QUARTER) : 0,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_CACTIVE) ? int.Parse(I_CACTIVE) : -1,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdQuarterReport, "cmdQuarterReport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetDataQuarterLog(string I_YEAR, string I_SVENDORID, string I_QUARTER, string I_LOGVERSION)
        {
            try
            {
                cmdQuarterReport.CommandType = CommandType.StoredProcedure;
                cmdQuarterReport.CommandText = "USP_M_QUARTER_LOG_SELECT";
                cmdQuarterReport.Parameters.Clear();
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_YEAR) ? int.Parse(I_YEAR) : 0,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    OracleType = OracleType.VarChar,
                    Value = I_SVENDORID,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_QUARTER",
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_QUARTER) ? int.Parse(I_QUARTER) : 0,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_LOGVERSION",
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_LOGVERSION) ? int.Parse(I_LOGVERSION) : 0,
                    IsNullable = true,
                });
                cmdQuarterReport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdQuarterReport, "cmdQuarterReport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
