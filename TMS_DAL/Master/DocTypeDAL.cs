﻿using System;
using System.ComponentModel;
using System.Data;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class DocTypeDAL : OracleConnectionDAL
    {
        public DocTypeDAL()
        {
            InitializeComponent();
        }

        public DocTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable DocTypeSelect(string Condition)
        {
            try
            {
                cmdDocTypeSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdDocTypeSelect, "cmdDocTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DocTypeDAL _instance;
        public static DocTypeDAL Instance
        {
            get
            {
                // if (_instance == null)
                //{
                _instance = new DocTypeDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
