﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;

namespace TMS_DAL.Master
{
    public partial class EmailTemplateDAL : OracleConnectionDAL
    {
        public EmailTemplateDAL()
        {
            InitializeComponent();
        }

        public EmailTemplateDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable EmailTemplateSelectAllDAL(string Condition)
        {
            string Query = cmdEmailTemplateSelectAll.CommandText;
            try
            {
                dbManager.Open();

                cmdEmailTemplateSelectAll.CommandText = @"SELECT TEMPLATE_ID, TEMPLATE_NAME, SUBJECT, BODY, MAIL_PARAMETER, M_EMAIL_TYPE.EMAIL_TYPE_ID, M_EMAIL_TYPE.EMAIL_TYPE_NAME
                                                         FROM M_EMAIL_TEMPLATE LEFT JOIN M_EMAIL_TYPE ON M_EMAIL_TEMPLATE.EMAIL_TYPE_ID = M_EMAIL_TYPE.EMAIL_TYPE_ID
                                                         WHERE 1=1";

                cmdEmailTemplateSelectAll.CommandText += Condition;
                cmdEmailTemplateSelectAll.CommandText += " ORDER BY M_EMAIL_TYPE.EMAIL_TYPE_NAME";

                return dbManager.ExecuteDataTable(cmdEmailTemplateSelectAll, "cmdEmailTemplateSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdEmailTemplateSelectAll.CommandText = Query;
            }
        }

        public void EmailTemplateUpdateDAL(string EmailType, int TemplateID, string TemplateName, string Subject, string Body)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdEmailTemplateUpdate.CommandText = @"UPDATE M_EMAIL_TEMPLATE
                                                       SET EMAIL_TYPE_ID =:I_EMAIL_TYPE_ID, TEMPLATE_NAME =:I_TEMPLATE_NAME, SUBJECT =:I_SUBJECT, BODY =:I_BODY
                                                       WHERE TEMPLATE_ID =:I_TEMPLATE_ID";

                cmdEmailTemplateUpdate.Parameters["I_EMAIL_TYPE_ID"].Value = EmailType;
                cmdEmailTemplateUpdate.Parameters["I_TEMPLATE_ID"].Value = TemplateID;
                cmdEmailTemplateUpdate.Parameters["I_TEMPLATE_NAME"].Value = TemplateName;
                cmdEmailTemplateUpdate.Parameters["I_SUBJECT"].Value = Subject;
                cmdEmailTemplateUpdate.Parameters["I_BODY"].Value = Body;

                dbManager.ExecuteNonQuery(cmdEmailTemplateUpdate);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable EmailTemplateSelectDAL(int TemplateID)
        {
            try
            {
                dbManager.Open();
                cmdEmailTemplateSelect.Parameters["I_TEMPLATE_ID"].Value = TemplateID;
                return dbManager.ExecuteDataTable(cmdEmailTemplateSelect, "cmdEmailTemplateSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable EmailTemplateSelectTruckDAL(int TemplateID)
        {
            try
            {
                dbManager.Open();
                cmdEmailTempplateSelectTruck.Parameters["I_TEMPLATE_ID"].Value = TemplateID;
                return dbManager.ExecuteDataTable(cmdEmailTempplateSelectTruck, "cmdEmailTempplateSelectTruck");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable EmailScheduleSelectAllDAL(string Condition)
        {
            string Query = cmdEmailScheduleSelectAll.CommandText;
            try
            {
                dbManager.Open();

                cmdEmailScheduleSelectAll.CommandText = @"SELECT M_EMAIL_TEMPLATE.TEMPLATE_ID, M_EMAIL_SCHEDULE.SCHEDULE_ID, M_EMAIL_TEMPLATE.TEMPLATE_NAME, M_EMAIL_SCHEDULE.SCHEDULE_NO, M_EMAIL_SCHEDULE.SCHEDULE_DAY
                                                               , CASE WHEN (M_EMAIL_SCHEDULE.ISACTIVE = 0) THEN 'Not Active' ELSE 'Active' END AS ISACTIVE, M_EMAIL_SCHEDULE.ISACTIVE AS ISACTIVE_VALUE
                                                               , M_EMAIL_TYPE.EMAIL_TYPE_ID, M_EMAIL_TYPE.EMAIL_TYPE_NAME
                                                          FROM M_EMAIL_TEMPLATE INNER JOIN M_EMAIL_SCHEDULE ON M_EMAIL_TEMPLATE.TEMPLATE_ID = M_EMAIL_SCHEDULE.TEMPLATE_ID
                                                                                LEFT JOIN M_EMAIL_TYPE ON M_EMAIL_TEMPLATE.EMAIL_TYPE_ID = M_EMAIL_TYPE.EMAIL_TYPE_ID
                                                          WHERE 1=1" + Condition
                                                          + " ORDER BY M_EMAIL_TEMPLATE.TEMPLATE_NAME, M_EMAIL_SCHEDULE.SCHEDULE_NO";

                return dbManager.ExecuteDataTable(cmdEmailScheduleSelectAll, "cmdEmailScheduleSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdEmailScheduleSelectAll.CommandText = Query;
            }
        }

        public void EmailScheduleUpdateDAL(int ScheduleID, int TemplateID, int ScheduleNo, int ScheduleDay, int IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                if (ScheduleID == 0)                                    //Insert
                {
                    cmdEmailScheduleCheck.CommandText = @"SELECT COUNT(*) AS COUNT
                                                          FROM M_EMAIL_SCHEDULE
                                                          WHERE TEMPLATE_ID =:I_TEMPLATE_ID
                                                          AND SCHEDULE_DAY =:I_SCHEDULE_DAY";

                    cmdEmailScheduleCheck.Parameters["I_TEMPLATE_ID"].Value = TemplateID;
                    cmdEmailScheduleCheck.Parameters["I_SCHEDULE_DAY"].Value = ScheduleDay;
                    DataTable dt = dbManager.ExecuteDataTable(cmdEmailScheduleCheck, "cmdEmailScheduleCheck");
                    if ((dt.Rows.Count > 0) && (int.Parse(dt.Rows[0]["COUNT"].ToString()) > 0))
                        throw new Exception("ข้อมูลซ้ำกับที่มีอยู่ในระบบ");

                    cmdEmailScheduleInsert.CommandText = @"INSERT INTO M_EMAIL_SCHEDULE (TEMPLATE_ID, SCHEDULE_NO, SCHEDULE_DAY, ISACTIVE, CREATE_BY)
                                                           VALUES (:I_TEMPLATE_ID, :I_SCHEDULE_NO, :I_SCHEDULE_DAY, :I_ISACTIVE, :I_CREATE_BY)";

                    cmdEmailScheduleInsert.Parameters["I_TEMPLATE_ID"].Value = TemplateID;
                    cmdEmailScheduleInsert.Parameters["I_SCHEDULE_NO"].Value = ScheduleNo;
                    cmdEmailScheduleInsert.Parameters["I_SCHEDULE_DAY"].Value = ScheduleDay;
                    cmdEmailScheduleInsert.Parameters["I_ISACTIVE"].Value = IsActive;
                    cmdEmailScheduleInsert.Parameters["I_CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdEmailScheduleInsert);
                }
                else
                {
                    cmdEmailScheduleUpdate.CommandText = @"UPDATE M_EMAIL_SCHEDULE
                                                           SET SCHEDULE_NO =:I_SCHEDULE_NO, SCHEDULE_DAY =:I_SCHEDULE_DAY, ISACTIVE =:I_ISACTIVE, UPDATE_BY =:I_CREATE_BY, UPDATE_DATETIME = SYSDATE
                                                           WHERE SCHEDULE_ID =:I_SCHEDULE_ID";

                    cmdEmailScheduleUpdate.Parameters["I_SCHEDULE_ID"].Value = ScheduleID;
                    cmdEmailScheduleUpdate.Parameters["I_SCHEDULE_NO"].Value = ScheduleNo;
                    cmdEmailScheduleUpdate.Parameters["I_SCHEDULE_DAY"].Value = ScheduleDay;
                    cmdEmailScheduleUpdate.Parameters["I_ISACTIVE"].Value = IsActive;
                    cmdEmailScheduleUpdate.Parameters["I_CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdEmailScheduleUpdate);
                }
                
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable EmailTypeSelectDAL(string Condition)
        {
            string Query = cmdEmailTemplateSelectAll.CommandText;
            try
            {
                dbManager.Open();

                cmdEmailTypeSelect.CommandText = @"SELECT EMAIL_TYPE_ID, EMAIL_TYPE_NAME
                                                   FROM M_EMAIL_TYPE
                                                   WHERE 1=1";

                cmdEmailTypeSelect.CommandText += Condition;
                cmdEmailTypeSelect.CommandText += " ORDER BY M_EMAIL_TYPE.EMAIL_TYPE_NAME";

                return dbManager.ExecuteDataTable(cmdEmailTypeSelect, "cmdEmailTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdEmailTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static EmailTemplateDAL _instance;
        public static EmailTemplateDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new EmailTemplateDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}