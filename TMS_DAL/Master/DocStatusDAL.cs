﻿using System;
using System.ComponentModel;
using System.Data;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class DocStatusDAL : OracleConnectionDAL
    {
        public DocStatusDAL()
        {
            InitializeComponent();
        }

        public DocStatusDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable DocStatusSelect()
        {
            try
            {
                return dbManager.ExecuteDataTable(cmdDocStatusSelect, "cmdDocStatusSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DocStatusDAL _instance;
        public static DocStatusDAL Instance
        {
            get
            {
               // if (_instance == null)
                //{
                    _instance = new DocStatusDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}