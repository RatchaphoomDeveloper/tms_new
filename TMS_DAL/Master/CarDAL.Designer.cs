﻿namespace TMS_DAL.Master
{
    partial class CarDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdConfigSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarsap = new System.Data.OracleClient.OracleCommand();
            this.cmdCartype = new System.Data.OracleClient.OracleCommand();
            this.cmdupdateTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdFunctionGetID = new System.Data.OracleClient.OracleCommand();
            this.cmdinsert = new System.Data.OracleClient.OracleCommand();
            this.cmdFucntionGenIdDoc = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectData = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdDeleteFileInsurance = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateSemi = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateCapacity = new System.Data.OracleClient.OracleCommand();
            this.cmdloadband = new System.Data.OracleClient.OracleCommand();
            this.cmdloadGps = new System.Data.OracleClient.OracleCommand();
            this.cmdDataInfo = new System.Data.OracleClient.OracleCommand();
            this.cmdBlacklistData = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateApprove = new System.Data.OracleClient.OracleCommand();
            this.cmdRowblacklist = new System.Data.OracleClient.OracleCommand();
            this.cmdSavecomment = new System.Data.OracleClient.OracleCommand();
            this.cmdFindHeadBySemi = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckCarApprove = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckVendorName = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectGenClassgrp = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateClassGrp = new System.Data.OracleClient.OracleCommand();
            this.cmdGenClassgrp = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckAgeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdGetDocMV = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckAgeExpire = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateTruckExpire = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateTruckExpire2 = new System.Data.OracleClient.OracleCommand();
            this.cmdDelTruckForInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdCancelTruckAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdConfigSelect
            // 
            this.cmdConfigSelect.CommandText = "SELECT 0 As sID,NULL As ROUTE,\'- Route -\' As DESCRIPTION FROM DUAL UNION SELECT R" +
    "OW_NUMBER() OVER(ORDER BY ROUTE) As sID,ROUTE,DESCRIPTION FROM LSTROUTE WHERE ls" +
    "troute.cactive = \'1\'";
            // 
            // cmdCarsap
            // 
            this.cmdCarsap.CommandText = "SELECT 0 As OID,NULL As TYPE_ID,\'- เลือก -\' As TYPE_NAME FROM DUAL UNION SELECT R" +
    "OWNUM As OID,TYPE_ID,TYPE_NAME FROM LSTVEH_TYPE where lstveh_type.cactive =\'1\'";
            // 
            // cmdCartype
            // 
            this.cmdCartype.CommandText = "SELECT c.config_name , c.config_value FROM c_config c WHERE config_type=\'VICHICLE" +
    "\'";
            // 
            // cmdupdateTruck
            // 
            this.cmdupdateTruck.CommandText = "USP_M_TTRUCK_UPDATE";
            this.cmdupdateTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TRUCKID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("TYPECAR", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_HEAD", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("Doc", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("Insurance", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("Oilcapacity", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("blacklist", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("OutputValue", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("DocTPN", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdFunctionGetID
            // 
            this.cmdFunctionGetID.CommandText = "SELECT FC_GENID_TTRUCK(0) FROM DUAL";
            // 
            // cmdinsert
            // 
            this.cmdinsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TYPECAR", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_HEAD", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("Doc", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("Insurance", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("Oilcapacity", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("OutputValue", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("DocTPN", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdFucntionGenIdDoc
            // 
            this.cmdFucntionGenIdDoc.CommandText = "SELECT DOCID FROM (SELECT DOCID+0 As DOCID FROM ";
            // 
            // cmdImportFileDelete
            // 
            this.cmdImportFileDelete.CommandText = "DELETE FROM F_UPLOAD WHERE REF_STR =: I_DOC_ID";
            this.cmdImportFileDelete.Connection = this.OracleConn;
            this.cmdImportFileDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdDeleteFileInsurance
            // 
            this.cmdDeleteFileInsurance.CommandText = "DELETE FROM TTRUCK_INSURANCE WHERE STRUCKID =: STRUCKID";
            this.cmdDeleteFileInsurance.Connection = this.OracleConn;
            this.cmdDeleteFileInsurance.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdUpdateCapacity
            // 
            this.cmdUpdateCapacity.CommandText = "USP_M_TTRUCK_UPDATE_NTOTAL";
            this.cmdUpdateCapacity.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TRUCKID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("NTOTAL", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_RETRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdloadband
            // 
            this.cmdloadband.CommandText = "SELECT brandid,brandname FROM ttruck_brand WHERE cactive=\'1\'";
            this.cmdloadband.Connection = this.OracleConn;
            this.cmdloadband.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdloadGps
            // 
            this.cmdloadGps.CommandText = "SELECT GPSID,GPSNAME FROM TTRUCK_GPS WHERE CACTIVE=\'1\'";
            this.cmdloadGps.Connection = this.OracleConn;
            this.cmdloadGps.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdUpdateApprove
            // 
            this.cmdUpdateApprove.CommandText = "UPDATE TTRUCK SET CACTIVE=\'Y\',LOGDATA = \'1\' WHERE STRUCKID=:STRUCKID";
            // 
            // cmdRowblacklist
            // 
            this.cmdRowblacklist.CommandText = "SELECT COUNT(*) FROM TTRUCK_BLACKLIST WHERE TYPE=\'D\' AND SHEADREGISTERNO=:SHEADRE" +
    "GISTERNO";
            this.cmdRowblacklist.Connection = this.OracleConn;
            this.cmdRowblacklist.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdSavecomment
            // 
            this.cmdSavecomment.CommandText = "UPDATE TTRUCK SET COMMENTDATA=:COMMENTDATA WHERE STRUCKID=:STRUCKID";
            // 
            // cmdFindHeadBySemi
            // 
            this.cmdFindHeadBySemi.CommandText = "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRAILERREGISTERNO=:STRAILERREGISTERNO";
            // 
            // cmdCheckCarApprove
            // 
            this.cmdCheckCarApprove.CommandText = "SELECT * FROM TTRUCK T WHERE VIEHICLE_TYPE IN (\'34\',\'00\') AND T.STRUCKID = :STRUC" +
    "KID";
            // 
            // cmdTruckVendorName
            // 
            this.cmdTruckVendorName.CommandText = "SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID=:SVENDORID";
            // 
            // cmdSelectGenClassgrp
            // 
            this.cmdSelectGenClassgrp.CommandText = "SELECT T.SCARTYPEID,TRUCK_CATEGORY,NSHAFTDRIVEN,DREGISTER,NTOTALCAPACITY FROM TTR" +
    "UCK T WHERE SHEADREGISTERNO=:SHEADREGISTERNO";
            // 
            // cmdUpdateClassGrp
            // 
            this.cmdUpdateClassGrp.CommandText = "UPDATE TTRUCK SET CLASSGRP=:CLASSGRP WHERE SHEADREGISTERNO=:SHEADREGISTERNO";
            // 
            // cmdGenClassgrp
            // 
            this.cmdGenClassgrp.CommandText = "USP_M_GENERATE_CLASSGRP";
            this.cmdGenClassgrp.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SHEADREGISTERNO", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_DOCUMENT", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdGetDocMV
            // 
            this.cmdGetDocMV.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdTruckAgeExpire
            // 
            this.cmdTruckAgeExpire.CommandText = "USP_M_TRUCK_AGE_EXPIRE";
            this.cmdTruckAgeExpire.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdUpdateTruckExpire
            // 
            this.cmdUpdateTruckExpire.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_EXPIRE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdUpdateTruckExpire2
            // 
            this.cmdUpdateTruckExpire2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_STRUCKID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdDelTruckForInsert
            // 
            this.cmdDelTruckForInsert.CommandText = "USP_M_TTRUCK_DELETE_FOR_INSERT";
            this.cmdDelTruckForInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TRUCKID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdCancelTruckAdd
            // 
            this.cmdCancelTruckAdd.CommandText = "USP_M_TTRUCK_CANCEL";
            this.cmdCancelTruckAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdConfigSelect;
        private System.Data.OracleClient.OracleCommand cmdCarsap;
        private System.Data.OracleClient.OracleCommand cmdCartype;
        private System.Data.OracleClient.OracleCommand cmdupdateTruck;
        private System.Data.OracleClient.OracleCommand cmdFunctionGetID;
        private System.Data.OracleClient.OracleCommand cmdinsert;
        private System.Data.OracleClient.OracleCommand cmdFucntionGenIdDoc;
        private System.Data.OracleClient.OracleCommand cmdSelectData;
        private System.Data.OracleClient.OracleCommand cmdImportFileDelete;
        private System.Data.OracleClient.OracleCommand cmdDeleteFileInsurance;
        private System.Data.OracleClient.OracleCommand cmdUpdateSemi;
        private System.Data.OracleClient.OracleCommand cmdUpdateCapacity;
        private System.Data.OracleClient.OracleCommand cmdloadband;
        private System.Data.OracleClient.OracleCommand cmdloadGps;
        private System.Data.OracleClient.OracleCommand cmdDataInfo;
        private System.Data.OracleClient.OracleCommand cmdBlacklistData;
        private System.Data.OracleClient.OracleCommand cmdUpdateApprove;
        private System.Data.OracleClient.OracleCommand cmdRowblacklist;
        private System.Data.OracleClient.OracleCommand cmdSavecomment;
        private System.Data.OracleClient.OracleCommand cmdFindHeadBySemi;
        private System.Data.OracleClient.OracleCommand cmdCheckCarApprove;
        private System.Data.OracleClient.OracleCommand cmdTruckVendorName;
        private System.Data.OracleClient.OracleCommand cmdSelectGenClassgrp;
        private System.Data.OracleClient.OracleCommand cmdUpdateClassGrp;
        private System.Data.OracleClient.OracleCommand cmdGenClassgrp;
        private System.Data.OracleClient.OracleCommand cmdTruckAgeSelect;
        private System.Data.OracleClient.OracleCommand cmdGetDocMV;
        private System.Data.OracleClient.OracleCommand cmdTruckAgeExpire;
        private System.Data.OracleClient.OracleCommand cmdUpdateTruckExpire;
        private System.Data.OracleClient.OracleCommand cmdUpdateTruckExpire2;
        private System.Data.OracleClient.OracleCommand cmdDelTruckForInsert;
        private System.Data.OracleClient.OracleCommand cmdCancelTruckAdd;
    }
}
