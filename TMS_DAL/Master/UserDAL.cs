﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;
using System.Web.UI.WebControls;

namespace TMS_DAL.Master
{
    public partial class UserDAL : OracleConnectionDAL
    {
        public UserDAL()
        {
            InitializeComponent();
        }

        public UserDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UserSelectDAL(string Condition)
        {
            string Query = cmdUser.CommandText;
            try
            {
                cmdUser.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUser, "cmdUser");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUser.CommandText = Query;
            }
        }

        public DataTable TeamSelectDAL(string Condition)
        {
            string Query = cmdTeam.CommandText;
            try
            {
                cmdTeam.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdTeam, "cmdTeam");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTeam.CommandText = Query;
            }
        }

        public DataTable UserSelectDALWithDocDate(string Condition, string Field, string Join, string Rownum)
        {
            string Query = cmdUserWithDocDate.CommandText;
            Query = Query.Replace("{V_FIELD}", Field);
            Query = Query.Replace("{V_JOIN}", Join);
            Query = Query.Replace("{V_CONDITION}", Condition);
            Query = Query.Replace("{V_ROWNUM}", Rownum);
            cmdUserWithDocDate.CommandText = Query;
            try
            {
                dbManager.Open();
                //cmdUserWithDocDate.Parameters["V_UPLOAD_TYPE"].Value = queryDocType;   

                return dbManager.ExecuteDataTable(cmdUserWithDocDate, "cmdUserWithDocDate");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserWithDocDate.CommandText = Query + Condition;
            }
        }
        public DataTable UserSelectDALOnlyDocDate()
        {
            string Query = cmdUserOnlyDocDate.CommandText;
            try
            {
                return dbManager.ExecuteDataTable(cmdUserOnlyDocDate, "cmdUserOnlyDocDate");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserOnlyDocDate.CommandText = Query;
            }
        }

        public DataTable SoldToSelectDAL(string Search)
        {

            try
            {
                DataTable dt = new DataTable();
                cmdSoldToSelect.CommandText = @"SELECT SHIP_ID,SOLD_ID,SHIP_NAME,SOLD_NAME
                                                FROM M_SOLD_TO
                                                WHERE SOLD_ID LIKE :I_SEARCH
                                                OR SHIP_ID LIKE :I_SEARCH";

                cmdSoldToSelect.Parameters["I_SEARCH"].Value = Search;

                dt = dbManager.ExecuteDataTable(cmdSoldToSelect, "cmdSoldToSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserInsertDAL(int SUID, int CGROUP, string SVENDORID, string FIRSTNAME, string LASTNAME, string POSITION, string TEL, string USERNAME, string PASSWORD, string OLDPASSWORD, string EMAIL, int CACTIVE, int IS_CONTRACT, int CREATER, int DEPARTMENT, int DIVISION, int UserGroupID, string SALES_DISTRICT = "")
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdUserInsert.CommandText = "USP_M_USER_INSERT";
                cmdUserInsert.CommandType = CommandType.StoredProcedure;

                cmdUserInsert.Parameters["I_SUID"].Value = SUID;
                cmdUserInsert.Parameters["I_CGROUP"].Value = CGROUP;
                cmdUserInsert.Parameters["I_SVENDORID"].Value = SVENDORID;
                cmdUserInsert.Parameters["I_FIRSTNAME"].Value = FIRSTNAME;
                cmdUserInsert.Parameters["I_LASTNAME"].Value = LASTNAME;
                cmdUserInsert.Parameters["I_POSITION"].Value = POSITION;
                cmdUserInsert.Parameters["I_TEL"].Value = TEL;
                cmdUserInsert.Parameters["I_USERNAME"].Value = USERNAME;
                cmdUserInsert.Parameters["I_PASSWORD"].Value = PASSWORD;
                cmdUserInsert.Parameters["I_OLDPASSWORD"].Value = OLDPASSWORD;
                cmdUserInsert.Parameters["I_EMAIL"].Value = EMAIL;
                cmdUserInsert.Parameters["I_CACTIVE"].Value = CACTIVE;
                cmdUserInsert.Parameters["I_IS_CONTRACT"].Value = IS_CONTRACT;
                cmdUserInsert.Parameters["I_CREATER"].Value = CREATER;
                cmdUserInsert.Parameters["I_DEPARTMENT_ID"].Value = DEPARTMENT;
                cmdUserInsert.Parameters["I_DIVISION_ID"].Value = DIVISION;
                cmdUserInsert.Parameters["I_USERGROUP_ID"].Value = UserGroupID;
                cmdUserInsert.Parameters["I_SALES_DISTRICT"].Value = SALES_DISTRICT;

                dt = dbManager.ExecuteDataTable(cmdUserInsert, "cmdUserInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable StatusSelectDAL(string Condition)
        {
            string Query = cmdStatus.CommandText;
            try
            {
                cmdStatus.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdStatus, "cmdStatus");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdStatus.CommandText = Query;
            }
        }
        public DataTable SaleDistSelectDAL(string Condition)
        {
            string Query = cmdSaleDist.CommandText;
            try
            {
                cmdSaleDist.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSaleDist, "cmdSaleDist");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSaleDist.CommandText = Query;
            }
        }

        public DataTable CGroupFromUserGroupID(string Condition)
        {
            string Query = cmdCGroup.CommandText;
            try
            {
                cmdCGroup.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdCGroup, "cmdCGroup");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCGroup.CommandText = Query;
            }
        }
        public DataTable GetQueryDataDAL(string Condition)
        {
            string Query = cmdCGroup.CommandText;
            try
            {
                cmdCGroup.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdCGroup, "cmdCGroup");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCGroup.CommandText = Query;
            }
        }

        public DataTable DocExpireDAL(int TEMPLATE_ID)
        {
            try
            {
                DataTable dtResult = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                //"Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.17.15.39)(PORT = 1526)))(CONNECT_DATA =(SID = tstaging)(SERVER = DEDICATED)));User ID=TMSIVUAT;Password=Tmsivuat123;Unicode=True;"
                cmdDocExpire.CommandText = "USP_M_DOC_EXPIRE_SYSDATE";
                cmdDocExpire.CommandType = CommandType.StoredProcedure;

                cmdDocExpire.Parameters["I_TEMPLATE_ID"].Value = TEMPLATE_ID;
                dtResult = dbManager.ExecuteDataTable(cmdDocExpire, "cmdDocExpire");
                dbManager.CommitTransaction();
                return dtResult;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        public DataTable DocExpireAndInactiveDAL(int TEMPLATE_ID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dtResult = new DataTable();
                cmdDocExpire.CommandText = "USP_M_DOC_EXPIRE_AND_INACTIVE";
                cmdDocExpire.CommandType = CommandType.StoredProcedure;
                cmdDocExpire.Parameters["I_TEMPLATE_ID"].Value = TEMPLATE_ID;
                dtResult = dbManager.ExecuteDataTable(cmdDocExpire, "cmdDocExpire");
                dbManager.CommitTransaction();
                return dtResult;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }
        #region + Instance +
        private static UserDAL _instance;
        public static UserDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new UserDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
