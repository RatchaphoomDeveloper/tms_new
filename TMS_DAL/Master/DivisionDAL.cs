﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class DivisionDAL : OracleConnectionDAL
    {
        public DivisionDAL()
        {
            InitializeComponent();
        }

        public DivisionDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable DivisionSelectDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdDivisionSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdDivisionSelect, "cmdDivisionSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable DivisionInsertDAL(int DIVISION_ID, string DIVISION_NAME, string DOC_PREFIX, int DEPARTMENT_ID, int CACTIVE, int CREATER)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdDivisionInsert.CommandText = "USP_M_DIVISION_INSERT";
                cmdDivisionInsert.CommandType = CommandType.StoredProcedure;

                cmdDivisionInsert.Parameters["I_DIVISION_ID"].Value = DIVISION_ID;
                cmdDivisionInsert.Parameters["I_DIVISION_NAME"].Value = DIVISION_NAME;
                cmdDivisionInsert.Parameters["I_DOC_PREFIX"].Value = DOC_PREFIX;
                cmdDivisionInsert.Parameters["I_DEPARTMENT_ID"].Value = DEPARTMENT_ID;
                cmdDivisionInsert.Parameters["I_CACTIVE"].Value = CACTIVE;
                cmdDivisionInsert.Parameters["I_CREATER"].Value = CREATER;

                dt = dbManager.ExecuteDataTable(cmdDivisionInsert, "cmdDivisionInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static DivisionDAL _instance;
        public static DivisionDAL Instance
        {
            get
            {
                _instance = new DivisionDAL();
                return _instance;
            }
        }
        #endregion
    }
}
