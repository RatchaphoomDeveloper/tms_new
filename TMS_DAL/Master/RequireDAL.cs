﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class RequireDAL : OracleConnectionDAL
    {
        #region RequireDAL
        public RequireDAL()
        {
            InitializeComponent();
        }

        public RequireDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region MenuSelect
        public DataTable MenuSelect()
        {
            try
            {
                return dbManager.ExecuteDataTable(cmdMenuSelect, "cmdMenuSelect");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireLevelSelect
        public DataTable RequireLevelSelect(string FIELD_TYPE)
        {
            try
            {
                cmdRequireLevelSelect.Parameters.Clear();
                cmdRequireLevelSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter() {
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    Direction = ParameterDirection.InputOutput,
                    ParameterName = "FIELD_TYPE",
                    Value = FIELD_TYPE,
                });
                return dbManager.ExecuteDataTable(cmdRequireLevelSelect, "cmdRequireLevelSelect");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireLevelDataSelect
        public DataTable RequireLevelDataSelect(string MREQUIRELEVELID, string PARENTID)
        {
            try
            {
                cmdRequireLevelDataSelect.Parameters.Clear();
                cmdRequireLevelDataSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    Direction = ParameterDirection.InputOutput,
                    ParameterName = "MREQUIRELEVELID",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELID) ? 0 : int.Parse(MREQUIRELEVELID),
                });
                cmdRequireLevelDataSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    Direction = ParameterDirection.InputOutput,
                    ParameterName = "PARENTID",
                    Value = string.IsNullOrEmpty(PARENTID) ? 0 : int.Parse(PARENTID),
                });
                return dbManager.ExecuteDataTable(cmdRequireLevelDataSelect, "cmdRequireLevelDataSelect");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireSelect
        public DataTable RequireSelect(string Level1, string Level2, string Level3, string Level4, string MREQUIRELEVELIDLEVEL1, string MREQUIRELEVELIDLEVEL2, string MREQUIRELEVELIDLEVEL3, string MREQUIRELEVELIDLEVEL4, string FIELD_TYPE, int LevelCount)
        {
            try
            {
                string level = Level1, mrequirelevelidlevel = MREQUIRELEVELIDLEVEL1;

                switch (LevelCount)
                {
                    case 4:
                        Level1 = Level4;
                        Level4 = level;
                        level = Level2;
                        Level2 = Level3;
                        Level3 = Level2;

                        MREQUIRELEVELIDLEVEL1 = MREQUIRELEVELIDLEVEL4;
                        MREQUIRELEVELIDLEVEL4 = mrequirelevelidlevel;
                        mrequirelevelidlevel = MREQUIRELEVELIDLEVEL2;
                        MREQUIRELEVELIDLEVEL2 = MREQUIRELEVELIDLEVEL3;
                        MREQUIRELEVELIDLEVEL3 = MREQUIRELEVELIDLEVEL2;
                        break;
                    case 3 :
                        Level1 = Level3;
                        Level3 = level;
                        MREQUIRELEVELIDLEVEL1 = MREQUIRELEVELIDLEVEL3;
                        MREQUIRELEVELIDLEVEL3 = mrequirelevelidlevel;
                        break;
                    case 2:
                        Level1 = Level2;
                        Level2 = level;
                        MREQUIRELEVELIDLEVEL1 = MREQUIRELEVELIDLEVEL2;
                        MREQUIRELEVELIDLEVEL2 = mrequirelevelidlevel;
                        break;
                    default:
                        break;
                }
                cmdRequireSelect.CommandText = @"SELECT M_COMPLAIN_REQUIRE_FIELD.M_COMPLAIN_REQUIRE_FIELD 
  ,M_COMPLAIN_REQUIRE_FIELD.DESCRIPTION 
  ,M_COMPLAIN_REQUIRE_FIELD.REQUIRE_TYPE
  ,LEVEL1.ID ID1,LEVEL1.DETAIL DETAIL1 
  ,LEVEL2.ID ID2,LEVEL2.DETAIL DETAIL2 
  ,LEVEL3.ID ID3,LEVEL3.DETAIL DETAIL3 
  ,LEVEL4.ID ID4,LEVEL4.DETAIL DETAIL4 
  ,M_REQUIRE_LEVEL1.DETAIL HEADER1 
  ,M_REQUIRE_LEVEL2.DETAIL HEADER2 
  ,M_REQUIRE_LEVEL3.DETAIL HEADER3 
  ,M_REQUIRE_LEVEL4.DETAIL HEADER4 
  FROM M_COMPLAIN_REQUIRE_FIELD 
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL1 ON M_COMPLAIN_REQUIRE_FIELD.COMPLAIN_TYPE_ID  = LEVEL1.REALID 
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL1 ON LEVEL1.MREQUIRELEVELID = M_REQUIRE_LEVEL1.ID  
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL2 ON LEVEL1.PARENTID = LEVEL2.ID 
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL2 ON LEVEL2.MREQUIRELEVELID = M_REQUIRE_LEVEL2.ID 
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL3 ON LEVEL2.PARENTID = LEVEL3.ID 
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL3 ON LEVEL3.MREQUIRELEVELID = M_REQUIRE_LEVEL3.ID 
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL4 ON LEVEL3.PARENTID = LEVEL4.ID 
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL4 ON LEVEL4.MREQUIRELEVELID = M_REQUIRE_LEVEL4.ID 
  WHERE (LEVEL1.ID = :LEVEL1ID OR (:LEVEL1ID  = 0 AND 0 = 0)) 
  AND (LEVEL2.ID = :LEVEL2ID OR (:LEVEL2ID  = 0 AND 0 = 0)) 
  AND (LEVEL3.ID = :LEVEL3ID OR (:LEVEL3ID  = 0 AND 0 = 0))  
  AND (LEVEL4.ID = :LEVEL1ID OR (:LEVEL4ID  = 0 AND 0 = 0)) 
  AND M_COMPLAIN_REQUIRE_FIELD.FIELD_TYPE = :FIELD_TYPE 
  AND (M_REQUIRE_LEVEL1.id = :MREQUIRELEVELIDLEVEL1 OR (:MREQUIRELEVELIDLEVEL1  = 0 AND 0 = 0))
  AND (M_REQUIRE_LEVEL2.id = :MREQUIRELEVELIDLEVEL2 OR (:MREQUIRELEVELIDLEVEL2  = 0 AND 0 = 0)) 
  AND (M_REQUIRE_LEVEL3.id = :MREQUIRELEVELIDLEVEL3 OR (:MREQUIRELEVELIDLEVEL3  = 0 AND 0 = 0)) 
  AND (M_REQUIRE_LEVEL4.id = :MREQUIRELEVELIDLEVEL4 OR (:MREQUIRELEVELIDLEVEL4  = 0 AND 0 = 0)) 
  ORDER BY M_COMPLAIN_REQUIRE_FIELD.M_COMPLAIN_REQUIRE_FIELD";
                cmdRequireSelect.Parameters.Clear();
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "FIELD_TYPE",
                    Value = FIELD_TYPE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL1ID",
                    Value = string.IsNullOrEmpty(Level1) ? 0 : int.Parse(Level1),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL2ID",
                    Value = string.IsNullOrEmpty(Level2) ? 0 : int.Parse(Level2),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL3ID",
                    Value = string.IsNullOrEmpty(Level3) ? 0 : int.Parse(Level3),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL4ID",
                    Value = string.IsNullOrEmpty(Level4) ? 0 : int.Parse(Level4),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL1",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL1) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL1),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL2",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL2) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL2),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL3",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL3) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL3),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL4",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL4) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL4),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdRequireSelect, "cmdRequireSelect");
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireFileSelect
        public DataTable RequireFileSelect(string Level1, string Level2, string Level3, string Level4, string MREQUIRELEVELIDLEVEL1, string MREQUIRELEVELIDLEVEL2, string MREQUIRELEVELIDLEVEL3, string MREQUIRELEVELIDLEVEL4, string FIELD_TYPE, int LevelCount)
        {
            try
            {
                string level = Level1, mrequirelevelidlevel = MREQUIRELEVELIDLEVEL1;

                switch (LevelCount)
                {
                    case 4:
                        Level1 = Level4;
                        Level4 = level;
                        level = Level2;
                        Level2 = Level3;
                        Level3 = Level2;

                        MREQUIRELEVELIDLEVEL1 = MREQUIRELEVELIDLEVEL4;
                        MREQUIRELEVELIDLEVEL4 = mrequirelevelidlevel;
                        mrequirelevelidlevel = MREQUIRELEVELIDLEVEL2;
                        MREQUIRELEVELIDLEVEL2 = MREQUIRELEVELIDLEVEL3;
                        MREQUIRELEVELIDLEVEL3 = MREQUIRELEVELIDLEVEL2;
                        break;
                    case 3:
                        Level1 = Level3;
                        Level3 = level;
                        MREQUIRELEVELIDLEVEL1 = MREQUIRELEVELIDLEVEL3;
                        MREQUIRELEVELIDLEVEL3 = mrequirelevelidlevel;
                        break;
                    case 2:
                        Level1 = Level2;
                        Level2 = level;
                        MREQUIRELEVELIDLEVEL1 = MREQUIRELEVELIDLEVEL2;
                        MREQUIRELEVELIDLEVEL2 = mrequirelevelidlevel;
                        break;
                    default:
                        break;
                }
                cmdRequireFileSelect.CommandType = CommandType.Text;
                cmdRequireFileSelect.CommandText = @"SELECT  M_COMPLAIN_REQUEST_FILE.REQUEST_ID,M_COMPLAIN_REQUEST_FILE.ISACTIVE,M_UPLOAD_TYPE.UPLOAD_ID, M_UPLOAD_TYPE.UPLOAD_NAME, M_UPLOAD_TYPE.EXTENTION, M_UPLOAD_TYPE.MAX_FILE_SIZE,LEVEL1.ID ID1,LEVEL1.DETAIL DETAIL1 ,LEVEL2.ID ID2,LEVEL2.DETAIL DETAIL2 ,LEVEL3.ID ID3,LEVEL3.DETAIL DETAIL3 ,LEVEL4.ID ID4,LEVEL4.DETAIL DETAIL4 ,M_REQUIRE_LEVEL1.DETAIL HEADER1 ,M_REQUIRE_LEVEL2.DETAIL HEADER2 ,M_REQUIRE_LEVEL3.DETAIL HEADER3 ,M_REQUIRE_LEVEL4.DETAIL HEADER4 
  FROM M_COMPLAIN_REQUEST_FILE  
  LEFT JOIN M_UPLOAD_TYPE ON M_COMPLAIN_REQUEST_FILE.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID  
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL1 ON M_COMPLAIN_REQUEST_FILE.COMPLAIN_TYPE_ID  = LEVEL1.REALID  
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL1 ON LEVEL1.MREQUIRELEVELID = M_REQUIRE_LEVEL1.ID   
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL2 ON LEVEL1.PARENTID = LEVEL2.ID  
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL2 ON LEVEL2.MREQUIRELEVELID = M_REQUIRE_LEVEL2.ID  
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL3 ON LEVEL2.PARENTID = LEVEL3.ID  
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL3 ON LEVEL3.MREQUIRELEVELID = M_REQUIRE_LEVEL3.ID  
  LEFT JOIN M_REQUIRE_LEVEL_DATA LEVEL4 ON LEVEL3.PARENTID = LEVEL4.ID  
  LEFT JOIN M_REQUIRE_LEVEL M_REQUIRE_LEVEL4 ON LEVEL4.MREQUIRELEVELID = M_REQUIRE_LEVEL4.ID  
  WHERE (LEVEL1.ID = :LEVEL1ID OR (:LEVEL1ID  = 0 AND 0 = 0))  
  AND (LEVEL2.ID = :LEVEL2ID OR (:LEVEL2ID  = 0 AND 0 = 0))  
  AND (LEVEL3.ID = :LEVEL3ID OR (:LEVEL3ID  = 0 AND 0 = 0))   
  AND (LEVEL4.ID = :LEVEL1ID OR (:LEVEL4ID  = 0 AND 0 = 0))  
  AND M_UPLOAD_TYPE.UPLOAD_TYPE  = :FIELD_TYPE  
  AND (M_REQUIRE_LEVEL1.id = :MREQUIRELEVELIDLEVEL1 OR (:MREQUIRELEVELIDLEVEL1  = 0 AND 0 = 0))  
  AND (M_REQUIRE_LEVEL2.id = :MREQUIRELEVELIDLEVEL2 OR (:MREQUIRELEVELIDLEVEL2  = 0 AND 0 = 0))  
  AND (M_REQUIRE_LEVEL3.id = :MREQUIRELEVELIDLEVEL3 OR (:MREQUIRELEVELIDLEVEL3  = 0 AND 0 = 0))  
  AND (M_REQUIRE_LEVEL4.id = :MREQUIRELEVELIDLEVEL4 OR (:MREQUIRELEVELIDLEVEL4  = 0 AND 0 = 0)) 
    ORDER BY M_UPLOAD_TYPE.UPLOAD_ID";
                cmdRequireFileSelect.Parameters.Clear();
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "FIELD_TYPE",
                    Value = FIELD_TYPE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL1ID",
                    Value = string.IsNullOrEmpty(Level1) ? 0 : int.Parse(Level1),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL2ID",
                    Value = string.IsNullOrEmpty(Level2) ? 0 : int.Parse(Level2),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL3ID",
                    Value = string.IsNullOrEmpty(Level3) ? 0 : int.Parse(Level3),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "LEVEL4ID",
                    Value = string.IsNullOrEmpty(Level4) ? 0 : int.Parse(Level4),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL1",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL1) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL1),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL2",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL2) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL2),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL3",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL3) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL3),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireFileSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "MREQUIRELEVELIDLEVEL4",
                    Value = string.IsNullOrEmpty(MREQUIRELEVELIDLEVEL4) ? 0 : int.Parse(MREQUIRELEVELIDLEVEL4),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdRequireFileSelect, "cmdRequireFileSelect");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireSave
        public bool RequireSave(string M_COMPLAIN_REQUIRE_FIELD,string ISACTIVE, string REQUIRE_TYPE)
        {
            try
            {
                cmdRequireSave.CommandText = @"UPDATE M_COMPLAIN_REQUIRE_FIELD SET ISACTIVE = :ISACTIVE, REQUIRE_TYPE = :REQUIRE_TYPE  WHERE M_COMPLAIN_REQUIRE_FIELD = :M_COMPLAIN_REQUIRE_FIELD";
                cmdRequireSave.Parameters.Clear();
                cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "M_COMPLAIN_REQUIRE_FIELD",
                    Value = string.IsNullOrEmpty(M_COMPLAIN_REQUIRE_FIELD) ? 0 : int.Parse(M_COMPLAIN_REQUIRE_FIELD),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ISACTIVE",
                    Value = string.IsNullOrEmpty(ISACTIVE) ? 0 : int.Parse(ISACTIVE),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "REQUIRE_TYPE",
                    Value = string.IsNullOrEmpty(REQUIRE_TYPE) ? 0 : int.Parse(REQUIRE_TYPE),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdRequireSave);
                if (row >= 0)
                {
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region RequireFileSave
        public bool RequireFileSave(string REQUEST_ID, string ISACTIVE, string UPLOAD_NAME, string EXTENTION, string MAX_FILE_SIZE, string UPLOAD_ID)
        {
            try
            {
                cmdRequireSave.CommandText = "UPDATE M_COMPLAIN_REQUEST_FILE  SET ISACTIVE = :ISACTIVE  WHERE REQUEST_ID = :REQUEST_ID";
                cmdRequireSave.Parameters.Clear();

                cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "REQUEST_ID",
                    Value = string.IsNullOrEmpty(REQUEST_ID) ? 0 : int.Parse(REQUEST_ID),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ISACTIVE",
                    Value = string.IsNullOrEmpty(ISACTIVE) ? 0 : int.Parse(ISACTIVE),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdRequireSave);
                if (row >= 0)
                {
                    cmdRequireSave.CommandText = "UPDATE M_UPLOAD_TYPE  SET UPLOAD_NAME = :UPLOAD_NAME,EXTENTION = :EXTENTION,MAX_FILE_SIZE = :MAX_FILE_SIZE  WHERE UPLOAD_ID = :UPLOAD_ID";
                    cmdRequireSave.Parameters.Clear();

                    cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "UPLOAD_NAME",
                        Value = UPLOAD_NAME,
                        OracleType = System.Data.OracleClient.OracleType.VarChar,
                        IsNullable = true,
                    });
                    cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "EXTENTION",
                        Value = EXTENTION,
                        OracleType = System.Data.OracleClient.OracleType.VarChar,
                        IsNullable = true,
                    });
                    cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "MAX_FILE_SIZE",
                        Value = MAX_FILE_SIZE,
                        OracleType = System.Data.OracleClient.OracleType.VarChar,
                        IsNullable = true,
                    });
                    cmdRequireSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "UPLOAD_ID",
                        Value = string.IsNullOrEmpty(UPLOAD_ID) ? 0 : int.Parse(UPLOAD_ID),
                        OracleType = System.Data.OracleClient.OracleType.Number,
                        IsNullable = true,
                    });
                    row = dbManager.ExecuteNonQuery(cmdRequireSave);
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region RequireDelete
        public bool RequireDelete(string M_COMPLAIN_REQUIRE_FIELD)
        {
            try
            {
                cmdRequireDelete.Parameters.Clear();

                cmdRequireDelete.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "M_COMPLAIN_REQUIRE_FIELD",
                    Value = string.IsNullOrEmpty(M_COMPLAIN_REQUIRE_FIELD) ? 0 : int.Parse(M_COMPLAIN_REQUIRE_FIELD),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdRequireDelete);
                if (row >= 0)
                {
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region FieldSelect
        public DataTable FieldSelect(string I_FIELD_TYPE)
        {
            try
            {
                cmdRequire.CommandType = CommandType.StoredProcedure;
                cmdRequire.CommandText = @"USP_M_FIELD_SELECT";

                cmdRequire.Parameters.Clear();
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIELD_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_FIELD_TYPE,
                });
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdRequire, "cmdRequire");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireFieldSelect
        public DataTable RequireFieldSelect(string I_FIELD_TYPE, string I_FIELD_REFERENCE)
        {
            try
            {
                cmdRequire.CommandType = CommandType.StoredProcedure;
                cmdRequire.CommandText = @"USP_M_FIELD_REQUIRE_SELECT";

                cmdRequire.Parameters.Clear();
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIELD_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_FIELD_TYPE,
                });
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIELD_REFERENCE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_FIELD_REFERENCE) ? (object)DBNull.Value : (object)int.Parse(I_FIELD_REFERENCE),
                });
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdRequire, "cmdRequire");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        public DataTable RequireSelectTemplateDAL(string FieldType)
        {
            string Query = cmdRequireSelectTemplate.CommandText;
            try
            {
                dbManager.Open();

                cmdRequireSelectTemplate.CommandText = @"SELECT * FROM M_REQUIRE_FIELD WHERE FIELD_TYPE =:I_FIELD_TYPE";

                cmdRequireSelectTemplate.Parameters["I_FIELD_TYPE"].Value = FieldType;

                DataTable dt = dbManager.ExecuteDataTable(cmdRequireSelectTemplate, "cmdRequireSelectTemplate");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdRequireSelectTemplate.CommandText = Query;
            }
        }

        public void RequireInsertDAL(DataTable dtFieldFinal, DataTable dtFileFinal, string ComplainTypeID, DataTable dtComplainConfig, DataTable dtMonthlyReportConfig)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                #region + Require Field +
                if (dtFieldFinal.Rows.Count > 0)
                {
                    cmdRequireClear.CommandText = @"DELETE FROM M_COMPLAIN_REQUIRE_FIELD
                                                WHERE FIELD_TYPE =:I_FIELD_TYPE
                                                AND COMPLAIN_TYPE_ID =:I_COMPLAIN_TYPE_ID";

                    cmdRequireClear.Parameters["I_FIELD_TYPE"].Value = dtFieldFinal.Rows[0]["FIELD_TYPE"].ToString();
                    cmdRequireClear.Parameters["I_COMPLAIN_TYPE_ID"].Value = dtFieldFinal.Rows[0]["COMPLAIN_TYPE_ID"].ToString();

                    dbManager.ExecuteNonQuery(cmdRequireClear);

                    cmdRequireInsert.CommandText = @"INSERT INTO M_COMPLAIN_REQUIRE_FIELD (COMPLAIN_TYPE_ID, CONTROL_ID, REQUIRE_TYPE, ISACTIVE, CREATE_BY, CONTROL_ID_REQ, FIELD_TYPE, DESCRIPTION)
                                                 VALUES (:I_COMPLAIN_TYPE_ID, :I_CONTROL_ID, :I_REQUIRE_TYPE, :I_ISACTIVE, :I_CREATE_BY, :I_CONTROL_ID_REQ, :I_FIELD_TYPE, :I_DESCRIPTION)";

                    for (int i = 0; i < dtFieldFinal.Rows.Count; i++)
                    {
                        cmdRequireInsert.Parameters["I_COMPLAIN_TYPE_ID"].Value = dtFieldFinal.Rows[i]["COMPLAIN_TYPE_ID"].ToString();
                        cmdRequireInsert.Parameters["I_CONTROL_ID"].Value = dtFieldFinal.Rows[i]["CONTROL_ID"].ToString();
                        cmdRequireInsert.Parameters["I_REQUIRE_TYPE"].Value = dtFieldFinal.Rows[i]["REQUIRE_TYPE"].ToString();
                        cmdRequireInsert.Parameters["I_ISACTIVE"].Value = dtFieldFinal.Rows[i]["ISACTIVE"].ToString();
                        cmdRequireInsert.Parameters["I_CREATE_BY"].Value = dtFieldFinal.Rows[i]["CREATE_BY"].ToString();
                        cmdRequireInsert.Parameters["I_CONTROL_ID_REQ"].Value = dtFieldFinal.Rows[i]["CONTROL_ID_REQ"].ToString();
                        cmdRequireInsert.Parameters["I_FIELD_TYPE"].Value = dtFieldFinal.Rows[i]["FIELD_TYPE"].ToString();
                        cmdRequireInsert.Parameters["I_DESCRIPTION"].Value = dtFieldFinal.Rows[i]["DESCRIPTION"].ToString();

                        dbManager.ExecuteNonQuery(cmdRequireInsert);
                    }
                }
                #endregion

                #region + Request File +
                if (dtFileFinal.Rows.Count > 0)
                {
                    cmdRequestFileClear.CommandText = @"DELETE FROM M_COMPLAIN_REQUEST_FILE
                                                        WHERE REQUEST_TYPE =:I_REQUEST_TYPE
                                                        AND COMPLAIN_TYPE_ID =:I_COMPLAIN_TYPE_ID";

                    cmdRequestFileClear.Parameters["I_REQUEST_TYPE"].Value = dtFileFinal.Rows[0]["REQUEST_TYPE"].ToString();
                    cmdRequestFileClear.Parameters["I_COMPLAIN_TYPE_ID"].Value = dtFileFinal.Rows[0]["COMPLAIN_TYPE_ID"].ToString();

                    dbManager.ExecuteNonQuery(cmdRequestFileClear);

                    cmdRequestFileInsert.CommandText = @"INSERT INTO M_COMPLAIN_REQUEST_FILE (COMPLAIN_TYPE_ID, UPLOAD_ID, ISACTIVE, CREATE_BY, REQUEST_TYPE)
                                                         VALUES (:I_COMPLAIN_TYPE_ID, :I_UPLOAD_ID, :I_ISACTIVE, :I_CREATE_BY, :I_REQUEST_TYPE)";

                    for (int i = 0; i < dtFileFinal.Rows.Count; i++)
                    {
                        cmdRequestFileInsert.Parameters["I_COMPLAIN_TYPE_ID"].Value = dtFileFinal.Rows[i]["COMPLAIN_TYPE_ID"].ToString();
                        cmdRequestFileInsert.Parameters["I_UPLOAD_ID"].Value = dtFileFinal.Rows[i]["UPLOAD_ID"].ToString();
                        cmdRequestFileInsert.Parameters["I_ISACTIVE"].Value = dtFileFinal.Rows[i]["ISACTIVE"].ToString();
                        cmdRequestFileInsert.Parameters["I_CREATE_BY"].Value = dtFileFinal.Rows[i]["CREATE_BY"].ToString();
                        cmdRequestFileInsert.Parameters["I_REQUEST_TYPE"].Value = dtFileFinal.Rows[i]["REQUEST_TYPE"].ToString();

                        dbManager.ExecuteNonQuery(cmdRequestFileInsert);
                    }
                }
                #endregion

                #region + Complain Config +
                if (dtComplainConfig.Rows.Count > 0)
                {
                    cmdComplainConfigUpdate.CommandText = @"UPDATE M_COMPLAIN_TYPE
                                                            SET LOCK_DRIVER =:I_LOCK_DRIVER, IS_ADD_MULTIPLE_CAR =:I_IS_ADD_MULTIPLE_CAR
                                                            WHERE COMPLAIN_TYPE_ID =:I_COMPLAIN_TYPE_ID";

                    cmdComplainConfigUpdate.Parameters["I_COMPLAIN_TYPE_ID"].Value = ComplainTypeID;
                    cmdComplainConfigUpdate.Parameters["I_LOCK_DRIVER"].Value = dtComplainConfig.Rows[0]["LOCK_DRIVER"].ToString();
                    cmdComplainConfigUpdate.Parameters["I_IS_ADD_MULTIPLE_CAR"].Value = dtComplainConfig.Rows[0]["IS_ADD_MULTIPLE_CAR"].ToString();

                    dbManager.ExecuteNonQuery(cmdComplainConfigUpdate);
                }
                #endregion

                #region + Monthly Report Config +
                if (dtMonthlyReportConfig.Rows.Count > 0)
                {
                    cmdMonthlyReportConfigUpload.CommandText = @"UPDATE M_MONTH_REPORT_REQUIRE
                                                                 SET START_UPLOAD_DAY =:I_START_UPLOAD_DAY, END_UPLOAD_DAY =:I_END_UPLOAD_DAY";

                    cmdMonthlyReportConfigUpload.Parameters["I_START_UPLOAD_DAY"].Value = dtMonthlyReportConfig.Rows[0]["START_UPLOAD_DAY"].ToString();
                    cmdMonthlyReportConfigUpload.Parameters["I_END_UPLOAD_DAY"].Value = dtMonthlyReportConfig.Rows[0]["END_UPLOAD_DAY"].ToString();

                    dbManager.ExecuteNonQuery(cmdMonthlyReportConfigUpload);
                }
                #endregion
                

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static RequireDAL _instance;
        public static RequireDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new RequireDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
