﻿namespace TMS_DAL.Master
{
    partial class KPIDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KPIDAL));
            this.cmdGetVendorContract = new System.Data.OracleClient.OracleCommand();
            this.cmdSaveReportKPI = new System.Data.OracleClient.OracleCommand();
            this.cmdGetFormreport = new System.Data.OracleClient.OracleCommand();
            this.cmdGetreportById = new System.Data.OracleClient.OracleCommand();
            this.cmdEditReportKPI = new System.Data.OracleClient.OracleCommand();
            this.cmdKpireport = new System.Data.OracleClient.OracleCommand();
            this.cmdSaveKpiConfig = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectYearConfig = new System.Data.OracleClient.OracleCommand();
            this.cmdAllContract = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIByYearMonth = new System.Data.OracleClient.OracleCommand();
            this.cmdManualInSert = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISelectForEdit = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISaveValueA = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISaveValueB = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIEvaluationSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdLoadMethod = new System.Data.OracleClient.OracleCommand();
            this.cmdLoadFormula = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIFormSave = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIFormSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIMappingFormSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISaveTransaction = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIFindSum = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISelectListHeader = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISelectListDetail = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIMappingFormSelect2 = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIExportSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKPIExportFormSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKPISaveTransactionBatch = new System.Data.OracleClient.OracleCommand();
            this.cmdKPICheck = new System.Data.OracleClient.OracleCommand();
            this.cmdGetTotalTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdGetTotalAccident = new System.Data.OracleClient.OracleCommand();
            this.cmdGetTotalIVMS = new System.Data.OracleClient.OracleCommand();
            this.cmdGetShipmentCostData = new System.Data.OracleClient.OracleCommand();
            this.cmdKPILastUpdateSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdGetVendorContract
            // 
            this.cmdGetVendorContract.CommandText = "SELECT  V.SABBREVIATION, C.SCONTRACTNO FROM TCONTRACT C ,TVENDOR V WHERE C.SVENDO" +
    "RID = V.SVENDORID AND C.CACTIVE=\'Y\'  AND V.INUSE=\'1\' :CONDITION ORDER BY V.SABBR" +
    "EVIATION";
            // 
            // cmdSaveReportKPI
            // 
            this.cmdSaveReportKPI.CommandText = "USP_M_KPI_INSERT";
            this.cmdSaveReportKPI.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("KPI_EVA_HEAD_CLOB", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("KPI_EVA_EDTAIL_CLOB", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("OutputValue", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdGetFormreport
            // 
            this.cmdGetFormreport.CommandText = "SELECT K.KPI_EVA_HEAD_ID,K.NAME,K.AMOUNT,K.STATUS,T.SFIRSTNAME,K.UPDATE_DATE FROM" +
    " KPI_EVA_HEAD K,TUSER T WHERE K.UPDATE_BY=T.SUID ORDER BY K.UPDATE_DATE DESC";
            // 
            // cmdGetreportById
            // 
            this.cmdGetreportById.CommandText = resources.GetString("cmdGetreportById.CommandText");
            // 
            // cmdEditReportKPI
            // 
            this.cmdEditReportKPI.CommandText = "USP_M_KPI_EDIT";
            this.cmdEditReportKPI.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("KPI_EVA_ID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("KPI_EVA_HEAD_CLOB", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("KPI_EVA_EDTAIL_CLOB", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("OutputValue", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdKpireport
            // 
            this.cmdKpireport.CommandText = "SELECT C.KPI_EVA_HEAD_ID  val,C.NAME  Text FROM KPI_EVA_HEAD C WHERE STATUS=\'1\'";
            // 
            // cmdSaveKpiConfig
            // 
            this.cmdSaveKpiConfig.CommandText = "USP_M_KPI_CONFIG";
            this.cmdSaveKpiConfig.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("KPI_EVA_YEAR", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("KPI_EVA_CONFIG_CLOB", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("OutputValue", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdSelectYearConfig
            // 
            this.cmdSelectYearConfig.CommandText = "SELECT KPI_EVA_HEAD_ID,YEAR,MONTH,LOCKDATA FROM KPI_CONFIG WHERE YEAR=:YEAR";
            // 
            // cmdAllContract
            // 
            this.cmdAllContract.CommandText = "SELECT SCONTRACTID ,SCONTRACTNO FROM TCONTRACT WHERE CACTIVE=\'Y\'";
            // 
            // cmdKPIByYearMonth
            // 
            this.cmdKPIByYearMonth.CommandText = resources.GetString("cmdKPIByYearMonth.CommandText");
            // 
            // cmdManualInSert
            // 
            this.cmdManualInSert.CommandText = "USP_M_KPI_MANUAL";
            this.cmdManualInSert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("KPI_SCORE_DELETE_CLOB", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("KPI_SCORE_CLOB", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("OutputValue", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdKPISelectForEdit
            // 
            this.cmdKPISelectForEdit.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_FORMULA_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPISaveValueA
            // 
            this.cmdKPISaveValueA.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_FORMULA_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VALIABLE_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_VALUE", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPISaveValueB
            // 
            this.cmdKPISaveValueB.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_FORMULA_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VALIABLE_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_VALUE", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPIEvaluationSelect
            // 
            this.cmdKPIEvaluationSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_FORMULA_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VALIABLE_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_VALUE", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPIFormSave
            // 
            this.cmdKPIFormSave.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_FORM_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_DETAIL", System.Data.OracleClient.OracleType.VarChar, 1000),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_XML", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPIFormSelect
            // 
            this.cmdKPIFormSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPIMappingFormSelect
            // 
            this.cmdKPIMappingFormSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPISaveTransaction
            // 
            this.cmdKPISaveTransaction.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DATA", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPIFindSum
            // 
            this.cmdKPIFindSum.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_FORMULA_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_TOPIC_DETAIL_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPISelectListHeader
            // 
            this.cmdKPISelectListHeader.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4)});
            // 
            // cmdKPISelectListDetail
            // 
            this.cmdKPISelectListDetail.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPIMappingFormSelect2
            // 
            this.cmdKPIMappingFormSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPIExportFormSelect
            // 
            this.cmdKPIExportFormSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPISaveTransactionBatch
            // 
            this.cmdKPISaveTransactionBatch.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DATA", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPICheck
            // 
            this.cmdKPICheck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_HEADER_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetTotalTruck
            // 
            this.cmdGetTotalTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetTotalAccident
            // 
            this.cmdGetTotalAccident.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdGetTotalIVMS
            // 
            this.cmdGetTotalIVMS.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetShipmentCostData
            // 
            this.cmdGetShipmentCostData.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKPILastUpdateSelect
            // 
            this.cmdKPILastUpdateSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_MONTH", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdGetVendorContract;
        private System.Data.OracleClient.OracleCommand cmdSaveReportKPI;
        private System.Data.OracleClient.OracleCommand cmdGetFormreport;
        private System.Data.OracleClient.OracleCommand cmdGetreportById;
        private System.Data.OracleClient.OracleCommand cmdEditReportKPI;
        private System.Data.OracleClient.OracleCommand cmdKpireport;
        private System.Data.OracleClient.OracleCommand cmdSaveKpiConfig;
        private System.Data.OracleClient.OracleCommand cmdSelectYearConfig;
        private System.Data.OracleClient.OracleCommand cmdAllContract;
        private System.Data.OracleClient.OracleCommand cmdKPIByYearMonth;
        private System.Data.OracleClient.OracleCommand cmdManualInSert;
        private System.Data.OracleClient.OracleCommand cmdKPISelect;
        private System.Data.OracleClient.OracleCommand cmdKPISelectForEdit;
        private System.Data.OracleClient.OracleCommand cmdKPISaveValueA;
        private System.Data.OracleClient.OracleCommand cmdKPISaveValueB;
        private System.Data.OracleClient.OracleCommand cmdKPIEvaluationSelect;
        private System.Data.OracleClient.OracleCommand cmdLoadMethod;
        private System.Data.OracleClient.OracleCommand cmdLoadFormula;
        private System.Data.OracleClient.OracleCommand cmdKPIFormSave;
        private System.Data.OracleClient.OracleCommand cmdKPIFormSelect;
        private System.Data.OracleClient.OracleCommand cmdKPIMappingFormSelect;
        private System.Data.OracleClient.OracleCommand cmdKPISaveTransaction;
        private System.Data.OracleClient.OracleCommand cmdKPIFindSum;
        private System.Data.OracleClient.OracleCommand cmdKPISelectListHeader;
        private System.Data.OracleClient.OracleCommand cmdKPISelectListDetail;
        private System.Data.OracleClient.OracleCommand cmdKPIMappingFormSelect2;
        private System.Data.OracleClient.OracleCommand cmdKPIExportSelect;
        private System.Data.OracleClient.OracleCommand cmdKPIExportFormSelect;
        private System.Data.OracleClient.OracleCommand cmdKPISaveTransactionBatch;
        private System.Data.OracleClient.OracleCommand cmdKPICheck;
        private System.Data.OracleClient.OracleCommand cmdGetTotalTruck;
        private System.Data.OracleClient.OracleCommand cmdGetTotalAccident;
        private System.Data.OracleClient.OracleCommand cmdGetTotalIVMS;
        private System.Data.OracleClient.OracleCommand cmdGetShipmentCostData;
        private System.Data.OracleClient.OracleCommand cmdKPILastUpdateSelect;
    }
}
