﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class KnowledgeTypeDAL : OracleConnectionDAL
    {
        public KnowledgeTypeDAL()
        {
            InitializeComponent();
        }

        public KnowledgeTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable KnowledgeSelectDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKnowledgeTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKnowledgeTypeSelect, "cmdKnowledgeTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable KnowledgeInsertDAL(int KNOWLEDGE_TYPE_ID, string KNOWLEDGE_TYPE_NAME, int CACTIVE, int CREATER)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdKnowledgeTypeInsert.CommandText = "USP_M_KNOWLEDGE_TYPE_INSERT";
                cmdKnowledgeTypeInsert.CommandType = CommandType.StoredProcedure;

                cmdKnowledgeTypeInsert.Parameters["I_KNOWLEDGE_TYPE_ID"].Value = KNOWLEDGE_TYPE_ID;
                cmdKnowledgeTypeInsert.Parameters["I_KNOWLEDGE_TYPE_NAME"].Value = KNOWLEDGE_TYPE_NAME;
                cmdKnowledgeTypeInsert.Parameters["I_CACTIVE"].Value = CACTIVE;
                cmdKnowledgeTypeInsert.Parameters["I_CREATER"].Value = CREATER;

                dt = dbManager.ExecuteDataTable(cmdKnowledgeTypeInsert, "cmdKnowledgeTypeInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static KnowledgeTypeDAL _instance;
        public static KnowledgeTypeDAL Instance
        {
            get
            {
                _instance = new KnowledgeTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}
