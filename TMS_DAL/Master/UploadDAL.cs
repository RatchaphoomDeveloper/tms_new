﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;
using System.Web.UI.WebControls;
using dbManager;

namespace TMS_DAL.Master
{
    public partial class UploadDAL : OracleConnectionDAL
    {
        public UploadDAL()
        {
            InitializeComponent();
        }

        public UploadDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UploadRequestFileDAL(string Condition)
        {
            try
            {
                cmdUploadRequestSelect.CommandText = @"SELECT M_REQUEST_FILE.UPLOAD_TYPE, M_UPLOAD_TYPE.UPLOAD_ID, M_UPLOAD_TYPE.UPLOAD_NAME, M_UPLOAD_TYPE.EXTENTION, M_UPLOAD_TYPE.MAX_FILE_SIZE, M_UPLOAD_TYPE.DOWNLOAD_FORM
                                                       FROM  M_REQUEST_FILE INNER JOIN M_UPLOAD_TYPE ON M_REQUEST_FILE.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID
                                                       WHERE (M_UPLOAD_TYPE.ISACTIVE = 1)";

                cmdUploadRequestSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUploadRequestSelect, "cmdUploadRequestSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UploadExpireAdd(string DocID, string UploadType, DataTable dtUpload, int CreateBy, DBManager db)
        {
            try
            {
                cmdUploadDelete.Parameters["I_DOC_ID"].Value = DocID;
                cmdUploadDelete.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                db.ExecuteNonQuery(cmdUploadDelete);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdUpload2Add.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdUpload2Add.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdUpload2Add.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdUpload2Add.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdUpload2Add.Parameters["REF_INT"].Value = 0;
                    cmdUpload2Add.Parameters["REF_STR"].Value = DocID;
                    cmdUpload2Add.Parameters["ISACTIVE"].Value = "1";
                    cmdUpload2Add.Parameters["CREATE_BY"].Value = CreateBy;
                    cmdUpload2Add.Parameters["TYPE_DOCUMENT"].Value = "2";
                    cmdUpload2Add.Parameters["DOC_NUMBER"].Value = dtUpload.Rows[i]["DOC_NUMBER"].ToString();
                    cmdUpload2Add.Parameters["START_DATE"].Value = dtUpload.Rows[i]["START_DATE"].ToString();
                    cmdUpload2Add.Parameters["STOP_DATE"].Value = dtUpload.Rows[i]["STOP_DATE"].ToString();
                    cmdUpload2Add.Parameters["YEAR"].Value = dtUpload.Rows[i]["YEAR"].ToString();

                    db.ExecuteNonQuery(cmdUpload2Add);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UploadAdd(string DocID, string UploadType, DataTable dtUpload, int CreateBy, DBManager db)
        {
            try
            {
                cmdUploadDelete.Parameters["I_DOC_ID"].Value = DocID;
                cmdUploadDelete.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                db.ExecuteNonQuery(cmdUploadDelete);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdUploadAdd.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdUploadAdd.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdUploadAdd.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdUploadAdd.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdUploadAdd.Parameters["REF_INT"].Value = 0;
                    cmdUploadAdd.Parameters["REF_STR"].Value = DocID;
                    cmdUploadAdd.Parameters["ISACTIVE"].Value = "1";
                    cmdUploadAdd.Parameters["CREATE_BY"].Value = CreateBy;
                    cmdUploadAdd.Parameters["TYPE_DOCUMENT"].Value = "1";

                    db.ExecuteNonQuery(cmdUploadAdd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadSelect(string RefStr, string UploadType, string Condition)
        {
            string Query = cmdUploadSelect.CommandText;
            try
            {
                cmdUploadSelect.CommandText += Condition;

                cmdUploadSelect.Parameters["I_REF_STR"].Value = RefStr;
                cmdUploadSelect.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                DataTable dt = dbManager.ExecuteDataTable(cmdUploadSelect, "cmdUploadSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUploadSelect.CommandText = Query;
            }
        }

        public DataTable Upload2Select(string RefStr, string UploadType, string Condition)
        {
            string Query = cmdUpload2Select.CommandText;
            try
            {
                cmdUpload2Select.CommandText += Condition;

                cmdUpload2Select.Parameters["I_REF_STR"].Value = RefStr;
                cmdUpload2Select.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                DataTable dt = dbManager.ExecuteDataTable(cmdUpload2Select, "cmdUpload2Select");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUpload2Select.CommandText = Query;
            }
        }

        #region + Instance +
        private static UploadDAL _instance;
        public static UploadDAL Instance
        {
            get
            {
                _instance = new UploadDAL();
                return _instance;
            }
        }
        #endregion
    }
}
