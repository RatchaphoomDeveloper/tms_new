﻿namespace TMS_DAL.Master
{
    partial class UploadTypeDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadTypeDAL));
            this.cmdUploadTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeSelect2 = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeSelect2_Doc = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeSelectAll = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeSelectSearch = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeCheck = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeUpdate3 = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeSelect3 = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdUploadTypeSelect
            // 
            this.cmdUploadTypeSelect.CommandText = " SELECT UPLOAD_ID, UPLOAD_NAME, EXTENTION, MAX_FILE_SIZE  FROM M_UPLOAD_TYPE   WH" +
    "ERE ISACTIVE = 1 AND UPLOAD_TYPE =:V_UPLOAD_TYPE ORDER BY UPLOAD_ID";
            this.cmdUploadTypeSelect.Connection = this.OracleConn;
            this.cmdUploadTypeSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdUploadTypeSelect2
            // 
            this.cmdUploadTypeSelect2.CommandText = resources.GetString("cmdUploadTypeSelect2.CommandText");
            this.cmdUploadTypeSelect2.Connection = this.OracleConn;
            this.cmdUploadTypeSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Char, 1)});
            // 
            // cmdUploadTypeSelect2_Doc
            // 
            this.cmdUploadTypeSelect2_Doc.CommandText = resources.GetString("cmdUploadTypeSelect2_Doc.CommandText");
            this.cmdUploadTypeSelect2_Doc.Connection = this.OracleConn;
            this.cmdUploadTypeSelect2_Doc.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Char, 1)});
            // 
            // cmdUploadTypeSelectAll
            // 
            this.cmdUploadTypeSelectAll.Connection = this.OracleConn;
            this.cmdUploadTypeSelectAll.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdUploadTypeSelectSearch
            // 
            this.cmdUploadTypeSelectSearch.Connection = this.OracleConn;
            // 
            // cmdUploadTypeInsert
            // 
            this.cmdUploadTypeInsert.Connection = this.OracleConn;
            this.cmdUploadTypeInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_EXTENTION", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_MAX_FILE_SIZE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_IS_VENDOR_DOWNLOAD", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_CHECK_UPLOAD_END_DATE_IND", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REQUIRE_DOC_ALERT_TYPE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdUploadTypeCheck
            // 
            this.cmdUploadTypeCheck.Connection = this.OracleConn;
            this.cmdUploadTypeCheck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdUploadTypeUpdate
            // 
            this.cmdUploadTypeUpdate.Connection = this.OracleConn;
            this.cmdUploadTypeUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_EXTENTION", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_MAX_FILE_SIZE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_IS_VENDOR_DOWNLOAD", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_CHECK_UPLOAD_END_DATE_IND", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REQUIRE_DOC_ALERT_TYPE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdUploadTypeUpdate3
            // 
            this.cmdUploadTypeUpdate3.Connection = this.OracleConn;
            this.cmdUploadTypeUpdate3.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_EXTENTION", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_MAX_FILE_SIZE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_IS_VENDOR_DOWNLOAD", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_CHECK_UPLOAD_END_DATE_IND", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REQUIRE_DOC_ALERT_TYPE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SPECIAL_FLAG_VALUE", System.Data.OracleClient.OracleType.VarChar, 200)});
            // 
            // cmdUploadTypeSelect3
            // 
            this.cmdUploadTypeSelect3.CommandText = resources.GetString("cmdUploadTypeSelect3.CommandText");
            this.cmdUploadTypeSelect3.Connection = this.OracleConn;
            this.cmdUploadTypeSelect3.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("V_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Char, 1)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdUploadTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeSelect2;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeSelect2_Doc; 
        private System.Data.OracleClient.OracleCommand cmdUploadTypeSelectAll;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeSelectSearch;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeInsert;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeCheck;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeUpdate;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeUpdate3;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeSelect3;
    }
}
