﻿namespace TMS_DAL.Master
{
    partial class RequireDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RequireDAL));
            this.cmdRequireSelectByCOMPLAIN = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireSave = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdMenuSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireLevelSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireLevelDataSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireFileSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireFileSave = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireSelectTemplate = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdRequireClear = new System.Data.OracleClient.OracleCommand();
            this.cmdRequestFileClear = new System.Data.OracleClient.OracleCommand();
            this.cmdRequestFileInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdMonthlyReportConfigUpload = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainConfigUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdRequire = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdRequireSelectByCOMPLAIN
            // 
            this.cmdRequireSelectByCOMPLAIN.CommandText = resources.GetString("cmdRequireSelectByCOMPLAIN.CommandText");
            this.cmdRequireSelectByCOMPLAIN.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("WORKGROUPID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("GROUPSID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("SVENDORID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdRequireDelete
            // 
            this.cmdRequireDelete.CommandText = "UPDATE M_COMPLAIN_REQUIRE_FIELD  SET ISACTIVE = \'0\'  WHERE M_COMPLAIN_REQUIRE_FIE" +
    "LD = :M_COMPLAIN_REQUIRE_FIELD";
            this.cmdRequireDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("WORKGROUPID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("GROUPSID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("SVENDORID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdRequireSave
            // 
            this.cmdRequireSave.CommandText = "UPDATE M_COMPLAIN_REQUIRE_FIELD SET ISACTIVE = :ISACTIVE, REQUIRE_TYPE = :REQUIRE" +
    "_TYPE  WHERE M_COMPLAIN_REQUIRE_FIELD = :M_COMPLAIN_REQUIRE_FIELD";
            // 
            // cmdMenuSelect
            // 
            this.cmdMenuSelect.CommandText = "SELECT FIELD_TYPE,SMENUNAME FROM TMENU WHERE FIELD_TYPE IS NOT NULL";
            // 
            // cmdRequireLevelSelect
            // 
            this.cmdRequireLevelSelect.CommandText = "SELECT * FROM M_REQUIRE_LEVEL WHERE FIELD_TYPE = :FIELD_TYPE";
            // 
            // cmdRequireLevelDataSelect
            // 
            this.cmdRequireLevelDataSelect.CommandText = "SELECT * FROM M_REQUIRE_LEVEL_DATA WHERE MREQUIRELEVELID = :MREQUIRELEVELID AND P" +
    "ARENTID = :PARENTID AND CACTIVE = \'1\'";
            // 
            // cmdRequireSelectTemplate
            // 
            this.cmdRequireSelectTemplate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_FIELD_TYPE", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdRequireInsert
            // 
            this.cmdRequireInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTROL_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REQUIRE_TYPE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTROL_ID_REQ", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_FIELD_TYPE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DESCRIPTION", System.Data.OracleClient.OracleType.VarChar, 200)});
            // 
            // cmdRequireClear
            // 
            this.cmdRequireClear.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_FIELD_TYPE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdRequestFileClear
            // 
            this.cmdRequestFileClear.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_REQUEST_TYPE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdRequestFileInsert
            // 
            this.cmdRequestFileInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REQUEST_TYPE", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdMonthlyReportConfigUpload
            // 
            this.cmdMonthlyReportConfigUpload.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_START_UPLOAD_DAY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_END_UPLOAD_DAY", System.Data.OracleClient.OracleType.Number, 50)});
            // 
            // cmdComplainConfigUpdate
            // 
            this.cmdComplainConfigUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_LOCK_DRIVER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ADD_MULTIPLE_CAR", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdRequire
            // 
            this.cmdRequire.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdRequireSelectByCOMPLAIN;
        private System.Data.OracleClient.OracleCommand cmdRequireDelete;
        private System.Data.OracleClient.OracleCommand cmdRequireSave;
        private System.Data.OracleClient.OracleCommand cmdRequireSelect;
        private System.Data.OracleClient.OracleCommand cmdMenuSelect;
        private System.Data.OracleClient.OracleCommand cmdRequireLevelSelect;
        private System.Data.OracleClient.OracleCommand cmdRequireLevelDataSelect;
        private System.Data.OracleClient.OracleCommand cmdRequireFileSelect;
        private System.Data.OracleClient.OracleCommand cmdRequireFileSave;
        private System.Data.OracleClient.OracleCommand cmdRequireSelectTemplate;
        private System.Data.OracleClient.OracleCommand cmdRequireInsert;
        private System.Data.OracleClient.OracleCommand cmdRequireClear;
        private System.Data.OracleClient.OracleCommand cmdRequestFileClear;
        private System.Data.OracleClient.OracleCommand cmdRequestFileInsert;
        private System.Data.OracleClient.OracleCommand cmdMonthlyReportConfigUpload;
        private System.Data.OracleClient.OracleCommand cmdComplainConfigUpdate;
        private System.Data.OracleClient.OracleCommand cmdRequire;
    }
}
