﻿namespace TMS_DAL.Master
{
    partial class VendorDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VendorDAL));
            this.cmdPositionSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTitleSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdStatusCancelSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdStatusSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTVendorSapSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdEMPSAPID_Update = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorAttachInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorAttachInsert2 = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorSelectNameCar = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectNameVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorSave = new System.Data.OracleClient.OracleCommand();
            this.cmdEmployeeHistory = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorSaveChangeVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorCheckBeforeSave = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorSelectDAL = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorUpdateVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorP4Select = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorP4Insert = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorP4Update = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorPKEditOrApprove = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorPKSaveDraft = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorPKSaveApprove = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorRequestFile = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorRequestFile_Doc = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorUpdateInuseBySemployeeid = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorUpdateInActiveBySemployeeid = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorUpdateSAPInActiveBySemployeeid = new System.Data.OracleClient.OracleCommand();
            this.cmdLogInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdLogDriverJobInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdPositionSelectAll = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdGetSpecialCC_TeamStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverSelectWeb = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorP4Insert_Spot = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorP4Update_Spot = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorPKSaveDraft_Spot = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorPKSaveApprove_Spot = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdPositionSelect
            // 
            this.cmdPositionSelect.CommandText = "SELECT * FROM TEMPLOYEETYPES WHERE CACTIVE =\'1\'";
            // 
            // cmdTitleSelect
            // 
            this.cmdTitleSelect.CommandText = "SELECT title_id,title_name FROM m_titelname";
            // 
            // cmdStatusCancelSelect
            // 
            this.cmdStatusCancelSelect.CommandText = "SELECT STATUS_IDCANCEL, STATUS_TYPECANCEL FROM TEMPLOYEESTATUSCANCEL";
            // 
            // cmdStatusSelect
            // 
            this.cmdStatusSelect.CommandText = "SELECT STATUS_ID, STATUS_TYPE FROM TEMPLOYEESTATUS";
            // 
            // cmdVendorSelect
            // 
            this.cmdVendorSelect.CommandText = resources.GetString("cmdVendorSelect.CommandText");
            this.cmdVendorSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdTVendorSapSelect
            // 
            this.cmdTVendorSapSelect.CommandText = "SELECT SVENDORID,SABBREVIATION FROM TVENDOR WHERE  CCATEGORY = \'C\' AND INUSE = \'1" +
    "\' ORDER BY SVENDORID";
            // 
            // cmdEMPSAPID_Update
            // 
            this.cmdEMPSAPID_Update.CommandText = "USP_T_EMPSAPID_UPDATE";
            this.cmdEMPSAPID_Update.Connection = this.OracleConn;
            this.cmdEMPSAPID_Update.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdVendorAttachInsert
            // 
            this.cmdVendorAttachInsert.CommandText = resources.GetString("cmdVendorAttachInsert.CommandText");
            this.cmdVendorAttachInsert.Connection = this.OracleConn;
            this.cmdVendorAttachInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500)});
            // 
            // cmdVendorAttachInsert2
            // 
            this.cmdVendorAttachInsert2.CommandText = resources.GetString("cmdVendorAttachInsert2.CommandText");
            this.cmdVendorAttachInsert2.Connection = this.OracleConn;
            this.cmdVendorAttachInsert2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("START_DATE", System.Data.OracleClient.OracleType.NVarChar),
            new System.Data.OracleClient.OracleParameter("STOP_DATE", System.Data.OracleClient.OracleType.NVarChar),
            new System.Data.OracleClient.OracleParameter("DOC_NUMBER", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdImportFileDelete
            // 
            this.cmdImportFileDelete.CommandText = "DELETE FROM F_UPLOAD WHERE REF_STR =: I_DOC_ID";
            this.cmdImportFileDelete.Connection = this.OracleConn;
            this.cmdImportFileDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdVendorSelectNameCar
            // 
            this.cmdVendorSelectNameCar.CommandText = "SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID" +
    ") AS RN , SVENDORID , SABBREVIATION FROM TVENDOR WHERE cactive = \'1\' AND ccatego" +
    "ry = \'C\' AND TVENDOR.INUSE=\'1\') ";
            // 
            // cmdSelectNameVendor
            // 
            this.cmdSelectNameVendor.CommandText = "SELECT SABBREVIATION FROM TVENDOR WHERE SABBREVIATION LIKE \'%\' || :SABBREVIATION " +
    "|| \'%\'";
            // 
            // cmdVendorSave
            // 
            this.cmdVendorSave.CommandText = "USP_T_VENDOR_SAVE";
            this.cmdVendorSave.Connection = this.OracleConn;
            this.cmdVendorSave.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdVendorSaveChangeVendor
            // 
            this.cmdVendorSaveChangeVendor.CommandText = "USP_T_VENDOR_SAVECHANGEVENDOR";
            this.cmdVendorSaveChangeVendor.Connection = this.OracleConn;
            this.cmdVendorSaveChangeVendor.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdVendorCheckBeforeSave
            // 
            this.cmdVendorCheckBeforeSave.CommandText = "USP_T_VENDOR_CHECKBEFORESAVE";
            this.cmdVendorCheckBeforeSave.Connection = this.OracleConn;
            // 
            // cmdVendorInsert
            // 
            this.cmdVendorInsert.CommandText = "USP_T_VENDOR_INSERT";
            this.cmdVendorInsert.Connection = this.OracleConn;
            this.cmdVendorInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdVendorSelectDAL
            // 
            this.cmdVendorSelectDAL.CommandText = "SELECT * FROM VW_M_VENDOR_SELECT WHERE 1=1";
            // 
            // cmdVendorUpdate
            // 
            this.cmdVendorUpdate.CommandText = "USP_T_VENDOR_UPDATE";
            this.cmdVendorUpdate.Connection = this.OracleConn;
            this.cmdVendorUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdVendorUpdateVendor
            // 
            this.cmdVendorUpdateVendor.CommandText = "USP_T_VENDOR_UPDATEVENDOR";
            this.cmdVendorUpdateVendor.Connection = this.OracleConn;
            this.cmdVendorUpdateVendor.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdVendorP4Select
            // 
            this.cmdVendorP4Select.CommandText = "USP_T_VENDOR_P4_SELETE";
            this.cmdVendorP4Select.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdVendorP4Insert
            // 
            this.cmdVendorP4Insert.CommandText = "USP_T_VENDOR_P4_INSERT";
            this.cmdVendorP4Insert.Connection = this.OracleConn;
            this.cmdVendorP4Insert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdVendorP4Update
            // 
            this.cmdVendorP4Update.CommandText = "USP_T_VENDOR_P4_UPDATE";
            this.cmdVendorP4Update.Connection = this.OracleConn;
            this.cmdVendorP4Update.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REQ_ID", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdVendorPKEditOrApprove
            // 
            this.cmdVendorPKEditOrApprove.CommandText = "USP_T_VENDOR_PK_EDIT_OR_NOAP";
            // 
            // cmdVendorPKSaveDraft
            // 
            this.cmdVendorPKSaveDraft.CommandText = "USP_T_VENDOR_PK_SAVEDRAFT";
            this.cmdVendorPKSaveDraft.Connection = this.OracleConn;
            this.cmdVendorPKSaveDraft.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REQ_ID", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdVendorPKSaveApprove
            // 
            this.cmdVendorPKSaveApprove.CommandText = "USP_T_VENDOR_PK_SAVEAPPROVE";
            this.cmdVendorPKSaveApprove.Connection = this.OracleConn;
            this.cmdVendorPKSaveApprove.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REQ_ID", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdVendorRequestFile
            // 
            this.cmdVendorRequestFile.CommandText = resources.GetString("cmdVendorRequestFile.CommandText");
            // 
            // cmdVendorRequestFile_Doc
            // 
            this.cmdVendorRequestFile_Doc.CommandText = resources.GetString("cmdVendorRequestFile_Doc.CommandText");
            // 
            // cmdVendorUpdateInuseBySemployeeid
            // 
            this.cmdVendorUpdateInuseBySemployeeid.CommandText = "UPDATE TEMPLOYEE SET INUSE = :INUSE,DUPDATE = SYSDATE WHERE SEMPLOYEEID = :SEMPLO" +
    "YEEID";
            this.cmdVendorUpdateInuseBySemployeeid.Connection = this.OracleConn;
            // 
            // cmdVendorUpdateInActiveBySemployeeid
            // 
            this.cmdVendorUpdateInActiveBySemployeeid.CommandText = "UPDATE TEMPLOYEE SET CACTIVE = : ICACTIVE, SUPDATE = :IUPDATEBY , DUPDATE = SYSDA" +
    "TE WHERE SEMPLOYEEID = :SEMPLOYEEID";
            this.cmdVendorUpdateInActiveBySemployeeid.Connection = this.OracleConn;
            // 
            // cmdVendorUpdateSAPInActiveBySemployeeid
            // 
            this.cmdVendorUpdateSAPInActiveBySemployeeid.CommandText = "UPDATE TEMPLOYEE_SAP SET DRVSTATUS = :IDRVSTATUS, SUPDATE = :IUPDATEBY , DUPDATE " +
    "= SYSDATE WHERE SEMPLOYEEID = :SEMPLOYEEID";
            this.cmdVendorUpdateSAPInActiveBySemployeeid.Connection = this.OracleConn;
            // 
            // cmdLogInsert
            // 
            this.cmdLogInsert.Connection = this.OracleConn;
            // 
            // cmdLogDriverJobInsert
            // 
            this.cmdLogDriverJobInsert.Connection = this.OracleConn;
            // 
            // cmdPositionSelectAll
            // 
            this.cmdPositionSelectAll.CommandText = "SELECT * FROM TEMPLOYEETYPES WHERE 1=1";
            // 
            // cmdDriverSelect
            // 
            this.cmdDriverSelect.CommandText = resources.GetString("cmdDriverSelect.CommandText");
            this.cmdDriverSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdGetSpecialCC_TeamStatus
            // 
            this.cmdGetSpecialCC_TeamStatus.CommandText = "SELECT SEMAIL AS CCEMAIL FROM TUSER WHERE CACTIVE=1 AND IS_CONTRACT IN (1)";
            // 
            // cmdDriverSelectWeb
            // 
            this.cmdDriverSelectWeb.CommandText = "SELECT * FROM VW_M_DRIVER_DETAIL_WEB WHERE PERS_CODE = :I_PERS_CODE";
            this.cmdDriverSelectWeb.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdVendorP4Insert_Spot
            // 
            this.cmdVendorP4Insert_Spot.CommandText = "USP_T_VENDOR_P4_INSERT_SPOT";
            this.cmdVendorP4Insert_Spot.Connection = this.OracleConn;
            this.cmdVendorP4Insert_Spot.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdVendorP4Update_Spot
            // 
            this.cmdVendorP4Update_Spot.CommandText = "USP_T_VENDOR_P4_UPDATE_SPOT";
            this.cmdVendorP4Update_Spot.Connection = this.OracleConn;
            this.cmdVendorP4Update_Spot.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REQ_ID", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdVendorPKSaveDraft_Spot
            // 
            this.cmdVendorPKSaveDraft_Spot.CommandText = "USP_T_VENDOR_PK_SAVEDRAFT_SPOT";
            this.cmdVendorPKSaveDraft_Spot.Connection = this.OracleConn;
            this.cmdVendorPKSaveDraft_Spot.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REQ_ID", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdVendorPKSaveApprove_Spot
            // 
            this.cmdVendorPKSaveApprove_Spot.CommandText = "USP_T_VENDOR_PK_SAVEAPPROVE_S";
            this.cmdVendorPKSaveApprove_Spot.Connection = this.OracleConn;
            this.cmdVendorPKSaveApprove_Spot.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_FNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DCREATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_PERS_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CARRIER", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_LICENSE_NO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STRANS_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPTPYE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DBIRTHDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STEL", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STEL2", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SMAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SMAIL2", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_BEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_PERSONEL_EXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CAUSEOVER", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SFILENAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SSYSFILENAME", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_SPATH", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SDRIVERNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEBEGIN", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_DDRIVEEXPIRE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_BANSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAP", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CANCELSTATUS", System.Data.OracleClient.OracleType.VarChar, 28),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCANCEL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_CAUSESAPCOMMIT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_WORKDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_NOTFILL", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_TNAME", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_NUMACCIDENT", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTSTARTDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ACCIDENTENDDATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_SETTLETO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REQ_ID", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_EMPSAPID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdPositionSelect;
        private System.Data.OracleClient.OracleCommand cmdTitleSelect;
        private System.Data.OracleClient.OracleCommand cmdStatusCancelSelect;
        private System.Data.OracleClient.OracleCommand cmdStatusSelect;
        private System.Data.OracleClient.OracleCommand cmdVendorSelect;
        private System.Data.OracleClient.OracleCommand cmdVendorSelectDAL;
        private System.Data.OracleClient.OracleCommand cmdTVendorSapSelect;
        private System.Data.OracleClient.OracleCommand cmdEMPSAPID_Update;
        private System.Data.OracleClient.OracleCommand cmdVendorAttachInsert;
        private System.Data.OracleClient.OracleCommand cmdVendorAttachInsert2;
        private System.Data.OracleClient.OracleCommand cmdImportFileDelete;
        private System.Data.OracleClient.OracleCommand cmdVendorSelectNameCar;
        private System.Data.OracleClient.OracleCommand cmdSelectNameVendor;
        private System.Data.OracleClient.OracleCommand cmdVendorSave;
        private System.Data.OracleClient.OracleCommand cmdEmployeeHistory;
        private System.Data.OracleClient.OracleCommand cmdVendorSaveChangeVendor;
        private System.Data.OracleClient.OracleCommand cmdVendorCheckBeforeSave;
        private System.Data.OracleClient.OracleCommand cmdVendorInsert;
        private System.Data.OracleClient.OracleCommand cmdVendorUpdate;
        private System.Data.OracleClient.OracleCommand cmdVendorUpdateVendor;
        private System.Data.OracleClient.OracleCommand cmdVendorP4Select;
        private System.Data.OracleClient.OracleCommand cmdVendorP4Insert;
        private System.Data.OracleClient.OracleCommand cmdVendorP4Update;
        private System.Data.OracleClient.OracleCommand cmdVendorPKEditOrApprove;
        private System.Data.OracleClient.OracleCommand cmdVendorPKSaveDraft;
        private System.Data.OracleClient.OracleCommand cmdVendorPKSaveApprove;
        private System.Data.OracleClient.OracleCommand cmdVendorRequestFile;
        private System.Data.OracleClient.OracleCommand cmdVendorRequestFile_Doc;
        private System.Data.OracleClient.OracleCommand cmdVendorUpdateInuseBySemployeeid;
        private System.Data.OracleClient.OracleCommand cmdVendorUpdateInActiveBySemployeeid;
        private System.Data.OracleClient.OracleCommand cmdVendorUpdateSAPInActiveBySemployeeid;
        private System.Data.OracleClient.OracleCommand cmdLogInsert;
        private System.Data.OracleClient.OracleCommand cmdLogDriverJobInsert;
        private System.Data.OracleClient.OracleCommand cmdVendor;
        private System.Data.OracleClient.OracleCommand cmdPositionSelectAll;
        private System.Data.OracleClient.OracleCommand cmdDriverSelect;
        private System.Data.OracleClient.OracleCommand cmdGetSpecialCC_TeamStatus;
        private System.Data.OracleClient.OracleCommand cmdDriverSelectWeb;
        private System.Data.OracleClient.OracleCommand cmdVendorP4Insert_Spot;
        private System.Data.OracleClient.OracleCommand cmdVendorP4Update_Spot;
        private System.Data.OracleClient.OracleCommand cmdVendorPKSaveDraft_Spot;
        private System.Data.OracleClient.OracleCommand cmdVendorPKSaveApprove_Spot;
    }
}
