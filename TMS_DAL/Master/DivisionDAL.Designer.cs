﻿namespace TMS_DAL.Master
{
    partial class DivisionDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdDivisionSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdDivisionInsert = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdDivisionSelect
            // 
            this.cmdDivisionSelect.CommandText = "SELECT * FROM VW_M_DIVISION_SELECT WHERE 1=1 ";
            // 
            // cmdDivisionInsert
            // 
            this.cmdDivisionInsert.Connection = this.OracleConn;
            this.cmdDivisionInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DIVISION_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DIVISION_NAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_DOC_PREFIX", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_DEPARTMENT_ID", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdDivisionSelect;
        private System.Data.OracleClient.OracleCommand cmdDivisionInsert;
    }
}
