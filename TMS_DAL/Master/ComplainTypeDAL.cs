﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;

namespace TMS_DAL.Master
{
    public partial class ComplainTypeDAL : OracleConnectionDAL
    {
        public ComplainTypeDAL()
        {
            InitializeComponent();
        }

        public ComplainTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable ComplainTypeSelect(string Condition)
        {
            string Query = cmdComplainTypeSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainTypeSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdComplainTypeSelect, "dtComplain");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainTypeSelect.CommandText = Query;
            }
        }

        public DataTable ComplainTypeSelectAllDAL(string Condition)
        {
            string Query = cmdComplainTypeSelectAll.CommandText;
            try
            {
                dbManager.Open();

                cmdComplainTypeSelectAll.CommandText = @"SELECT COMPLAIN_TYPE_ID, COMPLAIN_TYPE_NAME, LOCK_DRIVER, IS_ADD_MULTIPLE_CAR, M_TOPIC.ISACTIVE AS TOPIC_ACTIVE, M_COMPLAIN_TYPE.ISACTIVE AS COMPLAIN_ACTIVE
                                                              , M_COMPLAIN_TYPE.LOCK_DRIVER, M_COMPLAIN_TYPE.IS_ADD_MULTIPLE_CAR
                                                         FROM M_COMPLAIN_TYPE INNER JOIN M_TOPIC ON M_COMPLAIN_TYPE.TOPIC_ID = M_TOPIC.TOPIC_ID
                                                         WHERE 1=1";

                cmdComplainTypeSelectAll.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdComplainTypeSelectAll, "cmdComplainTypeSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainTypeSelectAll.CommandText = Query;
            }
        }

        #region + Instance +
        private static ComplainTypeDAL _instance;
        public static ComplainTypeDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ComplainTypeDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}