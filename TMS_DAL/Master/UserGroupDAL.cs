﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;
using System.Web.UI.WebControls;

namespace TMS_DAL.Master
{
    public partial class UserGroupDAL : OracleConnectionDAL
    {
        public UserGroupDAL()
        {
            InitializeComponent();
        }

        public UserGroupDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UserGroupSelectDAL(string Condition)
        {
            string Query = cmdUserGroup.CommandText;
            try
            {
                cmdUserGroup.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUserGroup, "cmdUserGroup");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserGroup.CommandText = Query;
            }
        }

        public DataTable DepartmentSelectDAL(string Condition)
        {
            string Query = cmdDepartment.CommandText;
            try
            {
                cmdDepartment.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdDepartment, "cmdDepartment");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdDepartment.CommandText = Query;
            }
        }

        #region + Instance +
        private static UserGroupDAL _instance;
        public static UserGroupDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new UserGroupDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
