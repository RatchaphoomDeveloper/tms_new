﻿using System;
using System.ComponentModel;
using System.Data;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class DashboardDAL : OracleConnectionDAL
    {
        public DashboardDAL()
        {
            InitializeComponent();
        }

        public DashboardDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable DriverLogSelectDAL()
        {
            try
            {
                return dbManager.ExecuteDataTable(cmdDriverLogSelect, "cmdDriverLogSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DashboardDAL _instance;
        public static DashboardDAL Instance
        {
            get
            {
                // if (_instance == null)
                //{
                _instance = new DashboardDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}