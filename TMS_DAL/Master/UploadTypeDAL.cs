﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;

namespace TMS_DAL.Master
{
    public partial class UploadTypeDAL : OracleConnectionDAL
    {
        public UploadTypeDAL()
        {
            InitializeComponent();
        }

        public UploadTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UploadTypeSelectDAL(string UploadType)
        {
            try
            {
                dbManager.Open();
                cmdUploadTypeSelect.Parameters["V_UPLOAD_TYPE"].Value = UploadType;

                return dbManager.ExecuteDataTable(cmdUploadTypeSelect, "cmdUploadTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable UploadTypeSelectDAL2(string UploadType,string UploadTypeIND)
        {
            try
            {
                dbManager.Open();
                cmdUploadTypeSelect2.Parameters["V_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeSelect2.Parameters["V_UPLOAD_TYPE_IND"].Value = UploadTypeIND;

                return dbManager.ExecuteDataTable(cmdUploadTypeSelect2, "cmdUploadTypeSelect2");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable UploadTypeSelectDAL3(string UploadType, string UploadTypeIND)
        {
            try
            {
                dbManager.Open();
                cmdUploadTypeSelect3.Parameters["V_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeSelect3.Parameters["V_UPLOAD_TYPE_IND"].Value = UploadTypeIND;

                return dbManager.ExecuteDataTable(cmdUploadTypeSelect3, "cmdUploadTypeSelect3");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable UploadTypeSelectDAL2_Doc(string UploadType, string UploadTypeIND)
        {
            try
            {
                dbManager.Open();
                cmdUploadTypeSelect2_Doc.Parameters["V_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeSelect2_Doc.Parameters["V_UPLOAD_TYPE_IND"].Value = UploadTypeIND;

                return dbManager.ExecuteDataTable(cmdUploadTypeSelect2_Doc, "cmdUploadTypeSelect2_Doc");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable UploadTypeSelectAllDAL(string Condition)
        {
            
            try
            {
                dbManager.Open();

                cmdUploadTypeSelectAll.CommandText = @"SELECT UPLOAD_ID
                                                             , CASE WHEN UPLOAD_TYPE = 'COMPLAIN' THEN 'Complain (เอกสารหลักฐาน)'
                                                                    WHEN UPLOAD_TYPE = 'COMPLAIN_COST' THEN 'Complain (ค่าปรับ)'
                                                                    WHEN UPLOAD_TYPE = 'COMPLAIN_APPEAL' THEN 'Complain (อุทธรณ์)' 
                                                                    WHEN UPLOAD_TYPE = 'COMPLAIN_SCORE' THEN 'Complain (เอกสารพิจารณาโทษ)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_REQ' THEN 'Complain (ขอเอกสารเพิ่มเติม)'
                                                                    WHEN UPLOAD_TYPE = 'CONTRACT' THEN 'Contract'
                                                                    WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_1' THEN 'รายงานผู้ขนส่ง (ประจำเดือน)'
                                                                    WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_3' THEN 'รายงานผู้ขนส่ง (ประจำ 3 เดือน)'
                                                                    WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_6' THEN 'รายงานผู้ขนส่ง (ประจำ 6 เดือน)'
                                                                    WHEN UPLOAD_TYPE = 'TRUCK' THEN 'Truck'
                                                                    WHEN UPLOAD_TYPE = 'KM' THEN 'KM'
                                                                    WHEN UPLOAD_TYPE = 'ACCIDENTTAB3VENDOR' THEN 'Accident Tab3(ผู้ขนส่ง)'
                                                                    WHEN UPLOAD_TYPE = 'ACCIDENTTAB2' THEN 'Accident Tab2'
                                                                    WHEN UPLOAD_TYPE = 'ACCIDENT_COST' THEN 'Accident Tab4(ค่าปรับ)'
                                                                    WHEN UPLOAD_TYPE = 'ACCIDENT_SCORE' THEN 'Accident Tab4(คะแนน)'
                                                                    WHEN UPLOAD_TYPE = 'AUDIT_FILE' THEN 'ประเมินผลการทำงาน'
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANT' THEN 'อุทธรณ์' 
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANTPTT' THEN 'อุทธรณื (ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANTPTT_CHANGE' THEN 'อุทธรณื (ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANT_PTTDOC' THEN 'อุทธรณ์ (เอกสาร ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'T_CHECK_CONTRACT_FILE' THEN 'เอกสารตรวจรถเข้าสัญญา'
                                                                                 WHEN UPLOAD_TYPE = 'T_CHECK_CONTRACT_FILE_EXPIRED' THEN 'เอกสารตรวจรถเข้าสัญญา (หมดอายุ)'
                                                                    WHEN UPLOAD_TYPE = 'ANNUAL_REPORT_FILE' THEN 'ผลประเมินประจำปี'
                                                                    WHEN UPLOAD_TYPE = 'VENDOR' THEN 'พขร.'END AS UPLOAD_TYPE
                                                             , UPLOAD_TYPE AS UPLOAD_TYPE_VALUE
                                                             , UPLOAD_NAME, MAX_FILE_SIZE, EXTENTION, CASE WHEN ISACTIVE = 0 THEN 'InActive' ELSE 'Active' END AS ISACTIVE
                                                             , NVL(IS_VENDOR_DOWNLOAD, 0) AS IS_VENDOR_DOWNLOAD
                                                             , ISACTIVE AS ISACTIVE_VALUE
                                                             , NVL(IS_CHECK_UPLOAD_END_DATE_IND,0) AS IS_CHECK_UPLOAD_END_DATE_IND , REQUIRE_DOC_ALERT_TYPE , NVL(UPLOAD_TYPE_IND,0) UPLOAD_TYPE_IND
                                                             , M_UPLOAD_TYPE.SPECIAL_FLAG, M_UPLOAD_TYPE.SPECIAL_FLAG_VALUE
                                                    FROM M_UPLOAD_TYPE WHERE 1=1" + Condition +
                                                       " ORDER BY UPLOAD_TYPE";

                return dbManager.ExecuteDataTable(cmdUploadTypeSelectAll, "cmdUploadTypeSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable UploadTypeSelectSearchDAL(string Condition)
        {

            try
            {
                dbManager.Open();

                cmdUploadTypeSelectSearch.CommandText = @"SELECT DISTINCT CASE WHEN UPLOAD_TYPE = 'COMPLAIN' THEN 'Complain (เอกสารหลักฐาน)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_COST' THEN 'Complain (ค่าปรับ)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_SCORE' THEN 'Complain (เอกสารพิจารณาโทษ)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_REQ' THEN 'Complain (ขอเอกสารเพิ่มเติม)'
                                                                                 WHEN UPLOAD_TYPE = 'CONTRACT' THEN 'Contract'
                                                                                 WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_1' THEN 'รายงานผู้ขนส่ง (ประจำเดือน)'
                                                                                 WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_3' THEN 'รายงานผู้ขนส่ง (ประจำ 3 เดือน)'
                                                                                 WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_6' THEN 'รายงานผู้ขนส่ง (ประจำ 6 เดือน)'
                                                                                 WHEN UPLOAD_TYPE = 'TRUCK' THEN 'Truck'
                                                                                 WHEN UPLOAD_TYPE = 'VENDOR' THEN 'พขร.'
                                                                                 WHEN UPLOAD_TYPE = 'KM' THEN 'KM'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENTTAB3VENDOR' THEN 'Accident Tab3(ผู้ขนส่ง)'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENTTAB2' THEN 'Accident Tab2'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENT_COST' THEN 'Accident Tab4(ค่าปรับ)'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENT_SCORE' THEN 'Accident Tab4(คะแนน)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_APPEAL' THEN 'Complain (อุทธรณ์)' 
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANT' THEN 'อุทธรณ์' 
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANTPTT' THEN 'อุทธรณื (ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANTPTT_CHANGE' THEN 'อุทธรณื (ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANT_PTTDOC' THEN 'อุทธรณ์ (เอกสาร ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'T_CHECK_CONTRACT_FILE' THEN 'เอกสารตรวจรถเข้าสัญญา'
                                                                                 WHEN UPLOAD_TYPE = 'T_CHECK_CONTRACT_FILE_EXPIRED' THEN 'เอกสารตรวจรถเข้าสัญญา (หมดอายุ)'
                                                                                 WHEN UPLOAD_TYPE = 'AUDIT_FILE' THEN 'ประเมินผลการทำงาน' 
                                                                                 WHEN UPLOAD_TYPE = 'ANNUAL_REPORT_FILE' THEN 'ผลประเมินประจำปี' END AS UPLOAD_TYPE
                                                                              , UPLOAD_TYPE AS UPLOAD_TYPE_VALUE
                                                                              FROM M_UPLOAD_TYPE WHERE 1=1 " + Condition +
                                                                              " ORDER BY UPLOAD_TYPE";

                return dbManager.ExecuteDataTable(cmdUploadTypeSelectSearch, "cmdUploadTypeSelectSearch");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable UploadTypeSelectSearchDAL_Full(string Condition)
        {

            try
            {
                dbManager.Open();

                cmdUploadTypeSelectSearch.CommandText = @"SELECT DISTINCT CASE WHEN UPLOAD_TYPE = 'COMPLAIN' THEN 'Complain (เอกสารหลักฐาน)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_COST' THEN 'Complain (ค่าปรับ)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_SCORE' THEN 'Complain (เอกสารพิจารณาโทษ)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_REQ' THEN 'Complain (ขอเอกสารเพิ่มเติม)'
                                                                                 WHEN UPLOAD_TYPE = 'CONTRACT' THEN 'Contract'
                                                                                 WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_1' THEN 'รายงานผู้ขนส่ง (ประจำเดือน)'
                                                                                 WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_3' THEN 'รายงานผู้ขนส่ง (ประจำ 3 เดือน)'
                                                                                 WHEN UPLOAD_TYPE = 'REPORT_MONTHLY_6' THEN 'รายงานผู้ขนส่ง (ประจำ 6 เดือน)'
                                                                                 WHEN UPLOAD_TYPE = 'TRUCK' THEN 'Truck'
                                                                                 WHEN UPLOAD_TYPE = 'VENDOR' THEN 'พขร.'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENTTAB3VENDOR' THEN 'Accident Tab3(ผู้ขนส่ง)'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENTTAB2' THEN 'Accident Tab2'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENT_COST' THEN 'Accident Tab4(ค่าปรับ)'
                                                                                 WHEN UPLOAD_TYPE = 'ACCIDENT_SCORE' THEN 'Accident Tab4(คะแนน)'
                                                                                 WHEN UPLOAD_TYPE = 'COMPLAIN_APPEAL' THEN 'Complain (อุทธรณ์)' 
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANT' THEN 'อุทธรณ์' 
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANTPTT' THEN 'อุทธรณื (ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANTPTT_CHANGE' THEN 'อุทธรณื (ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'SUPPLIANT_PTTDOC' THEN 'อุทธรณ์ (เอกสาร ปตท.)'
                                                                                 WHEN UPLOAD_TYPE = 'T_CHECK_CONTRACT_FILE' THEN 'เอกสารตรวจรถเข้าสัญญา'
                                                                                 WHEN UPLOAD_TYPE = 'T_CHECK_CONTRACT_FILE_EXPIRED' THEN 'เอกสารตรวจรถเข้าสัญญา (หมดอายุ)'
                                                                                 WHEN UPLOAD_TYPE = 'AUDIT_FILE' THEN 'ประเมินผลการทำงาน' 
                                                                                 WHEN UPLOAD_TYPE = 'ANNUAL_REPORT_FILE' THEN 'ผลประเมินประจำปี' END AS UPLOAD_TYPE
                                                                               , UPLOAD_TYPE AS UPLOAD_TYPE_VALUE , IS_CHECK_UPLOAD_END_DATE_IND,  REQUIRE_DOC_ALERT_TYPE , NVL(UPLOAD_TYPE_IND,0) UPLOAD_TYPE_IND
                                                                               FROM M_UPLOAD_TYPE WHERE 1=1 " + Condition +
                                                                              " ORDER BY UPLOAD_TYPE";

                return dbManager.ExecuteDataTable(cmdUploadTypeSelectSearch, "cmdUploadTypeSelectSearch");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UploadTypeInsertDAL(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUploadTypeCheck.CommandText = @"SELECT COUNT(*) AS COUNT
                                                    FROM M_UPLOAD_TYPE
                                                    WHERE UPLOAD_NAME =:I_UPLOAD_NAME AND UPLOAD_TYPE =:I_UPLOAD_TYPE";

                cmdUploadTypeCheck.Parameters["I_UPLOAD_NAME"].Value = UploadTypeName;
                cmdUploadTypeCheck.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                DataTable dt = dbManager.ExecuteDataTable(cmdUploadTypeCheck, "cmdUploadTypeCheck");
                if ((dt.Rows.Count > 0) && (int.Parse(dt.Rows[0]["COUNT"].ToString()) > 0))
                    throw new Exception("ข้อมูล ประเภทไฟล์อัพโหลด ซ้ำกับในระบบ");

                cmdUploadTypeInsert.CommandText = @"INSERT INTO M_UPLOAD_TYPE (UPLOAD_NAME, EXTENTION, MAX_FILE_SIZE, UPLOAD_TYPE, IS_VENDOR_DOWNLOAD, ISACTIVE, CREATE_BY)
                                                    VALUES (:I_UPLOAD_NAME, :I_EXTENTION, :I_MAX_FILE_SIZE, :I_UPLOAD_TYPE, :I_IS_VENDOR_DOWNLOAD, :I_ISACTIVE, :I_CREATE_BY)";

                cmdUploadTypeInsert.Parameters["I_UPLOAD_NAME"].Value = UploadTypeName;
                cmdUploadTypeInsert.Parameters["I_EXTENTION"].Value = Extention;
                cmdUploadTypeInsert.Parameters["I_MAX_FILE_SIZE"].Value = MaxFileSize;
                cmdUploadTypeInsert.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeInsert.Parameters["I_IS_VENDOR_DOWNLOAD"].Value = VendorDownload;
                cmdUploadTypeInsert.Parameters["I_ISACTIVE"].Value = IsActive;
                cmdUploadTypeInsert.Parameters["I_CREATE_BY"].Value = UserID;

                dbManager.ExecuteNonQuery(cmdUploadTypeInsert);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UploadTypeUpdateDAL(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUploadTypeUpdate.CommandText = @"UPDATE M_UPLOAD_TYPE
                                                    SET EXTENTION =:I_EXTENTION, MAX_FILE_SIZE =:I_MAX_FILE_SIZE, ISACTIVE =:I_ISACTIVE, IS_VENDOR_DOWNLOAD =:I_IS_VENDOR_DOWNLOAD, UPDATE_BY =:I_CREATE_BY, UPDATE_DATETIME = SYSDATE
                                                    WHERE UPLOAD_TYPE =:I_UPLOAD_TYPE
                                                    AND UPLOAD_NAME =:I_UPLOAD_NAME";

                cmdUploadTypeUpdate.Parameters["I_UPLOAD_NAME"].Value = UploadTypeName;
                cmdUploadTypeUpdate.Parameters["I_EXTENTION"].Value = Extention;
                cmdUploadTypeUpdate.Parameters["I_MAX_FILE_SIZE"].Value = MaxFileSize;
                cmdUploadTypeUpdate.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeUpdate.Parameters["I_IS_VENDOR_DOWNLOAD"].Value = VendorDownload;
                cmdUploadTypeUpdate.Parameters["I_ISACTIVE"].Value = IsActive;
                cmdUploadTypeUpdate.Parameters["I_CREATE_BY"].Value = UserID;

                dbManager.ExecuteNonQuery(cmdUploadTypeUpdate);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UploadTypeInsertDAL2(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID, int IsUploadTypeIND, int IsCheckEndDateIND, int RequireDocAlertIND)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUploadTypeCheck.CommandText = @"SELECT COUNT(*) AS COUNT
                                                    FROM M_UPLOAD_TYPE
                                                    WHERE UPLOAD_NAME =:I_UPLOAD_NAME AND UPLOAD_TYPE =:I_UPLOAD_TYPE";

                cmdUploadTypeCheck.Parameters["I_UPLOAD_NAME"].Value = UploadTypeName;
                cmdUploadTypeCheck.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                DataTable dt = dbManager.ExecuteDataTable(cmdUploadTypeCheck, "cmdUploadTypeCheck");
                if ((dt.Rows.Count > 0) && (int.Parse(dt.Rows[0]["COUNT"].ToString()) > 0))
                    throw new Exception("ข้อมูล ประเภทไฟล์อัพโหลด ซ้ำกับในระบบ");

                cmdUploadTypeInsert.CommandText = @"INSERT INTO M_UPLOAD_TYPE (UPLOAD_NAME, EXTENTION, MAX_FILE_SIZE, UPLOAD_TYPE, IS_VENDOR_DOWNLOAD, ISACTIVE, CREATE_BY,UPLOAD_TYPE_IND,IS_CHECK_UPLOAD_END_DATE_IND,REQUIRE_DOC_ALERT_TYPE)
                                                    VALUES (:I_UPLOAD_NAME, :I_EXTENTION, :I_MAX_FILE_SIZE, :I_UPLOAD_TYPE
                                                                , :I_IS_VENDOR_DOWNLOAD, :I_ISACTIVE, :I_CREATE_BY, :I_UPLOAD_TYPE_IND, :I_IS_CHECK_UPLOAD_END_DATE_IND, :I_REQUIRE_DOC_ALERT_TYPE)";

                cmdUploadTypeInsert.Parameters["I_UPLOAD_NAME"].Value = UploadTypeName;
                cmdUploadTypeInsert.Parameters["I_EXTENTION"].Value = Extention;
                cmdUploadTypeInsert.Parameters["I_MAX_FILE_SIZE"].Value = MaxFileSize;
                cmdUploadTypeInsert.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeInsert.Parameters["I_IS_VENDOR_DOWNLOAD"].Value = VendorDownload;
                cmdUploadTypeInsert.Parameters["I_ISACTIVE"].Value = IsActive;
                cmdUploadTypeInsert.Parameters["I_CREATE_BY"].Value = UserID;
                cmdUploadTypeInsert.Parameters["I_UPLOAD_TYPE_IND"].Value = IsUploadTypeIND;
                cmdUploadTypeInsert.Parameters["I_IS_CHECK_UPLOAD_END_DATE_IND"].Value = IsCheckEndDateIND;
                cmdUploadTypeInsert.Parameters["I_REQUIRE_DOC_ALERT_TYPE"].Value = RequireDocAlertIND;

                dbManager.ExecuteNonQuery(cmdUploadTypeInsert);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UploadTypeUpdateDAL2(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID, int IsUploadTypeIND, int IsCheckEndDateIND, int RequireDocAlertIND)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUploadTypeUpdate.CommandText = @"UPDATE M_UPLOAD_TYPE
                                                    SET EXTENTION =:I_EXTENTION, MAX_FILE_SIZE =:I_MAX_FILE_SIZE, ISACTIVE =:I_ISACTIVE, IS_VENDOR_DOWNLOAD =:I_IS_VENDOR_DOWNLOAD
                                                    , UPDATE_BY =:I_CREATE_BY, UPDATE_DATETIME = SYSDATE , UPLOAD_TYPE_IND=:I_UPLOAD_TYPE_IND, IS_CHECK_UPLOAD_END_DATE_IND=: I_IS_CHECK_UPLOAD_END_DATE_IND
                                                    , REQUIRE_DOC_ALERT_TYPE = :I_REQUIRE_DOC_ALERT_TYPE
                                                    WHERE UPLOAD_TYPE =:I_UPLOAD_TYPE
                                                    AND UPLOAD_NAME =:I_UPLOAD_NAME";

                cmdUploadTypeUpdate.Parameters["I_UPLOAD_NAME"].Value = UploadTypeName;
                cmdUploadTypeUpdate.Parameters["I_EXTENTION"].Value = Extention;
                cmdUploadTypeUpdate.Parameters["I_MAX_FILE_SIZE"].Value = MaxFileSize;
                cmdUploadTypeUpdate.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeUpdate.Parameters["I_IS_VENDOR_DOWNLOAD"].Value = VendorDownload;
                cmdUploadTypeUpdate.Parameters["I_ISACTIVE"].Value = IsActive;
                cmdUploadTypeUpdate.Parameters["I_CREATE_BY"].Value = UserID;
                cmdUploadTypeUpdate.Parameters["I_UPLOAD_TYPE_IND"].Value = IsUploadTypeIND;
                cmdUploadTypeUpdate.Parameters["I_IS_CHECK_UPLOAD_END_DATE_IND"].Value = IsCheckEndDateIND;
                cmdUploadTypeUpdate.Parameters["I_REQUIRE_DOC_ALERT_TYPE"].Value = RequireDocAlertIND;
                
                dbManager.ExecuteNonQuery(cmdUploadTypeUpdate);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UploadTypeUpdateDAL3(string UploadType, string UploadTypeName, string Extention, string MaxFileSize, int VendorDownload, int IsActive, int UserID, int IsUploadTypeIND, int IsCheckEndDateIND, int RequireDocAlertIND, string SpecialFlagValue)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUploadTypeUpdate3.CommandText = @"UPDATE M_UPLOAD_TYPE
                                                    SET EXTENTION =:I_EXTENTION, MAX_FILE_SIZE =:I_MAX_FILE_SIZE, ISACTIVE =:I_ISACTIVE, IS_VENDOR_DOWNLOAD =:I_IS_VENDOR_DOWNLOAD
                                                    , UPDATE_BY =:I_CREATE_BY, UPDATE_DATETIME = SYSDATE , UPLOAD_TYPE_IND=:I_UPLOAD_TYPE_IND, IS_CHECK_UPLOAD_END_DATE_IND=: I_IS_CHECK_UPLOAD_END_DATE_IND
                                                    , REQUIRE_DOC_ALERT_TYPE = :I_REQUIRE_DOC_ALERT_TYPE
                                                    , SPECIAL_FLAG_VALUE =:I_SPECIAL_FLAG_VALUE
                                                    WHERE UPLOAD_TYPE =:I_UPLOAD_TYPE
                                                    AND UPLOAD_NAME =:I_UPLOAD_NAME";

                cmdUploadTypeUpdate3.Parameters["I_UPLOAD_NAME"].Value = UploadTypeName;
                cmdUploadTypeUpdate3.Parameters["I_EXTENTION"].Value = Extention;
                cmdUploadTypeUpdate3.Parameters["I_MAX_FILE_SIZE"].Value = MaxFileSize;
                cmdUploadTypeUpdate3.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeUpdate3.Parameters["I_IS_VENDOR_DOWNLOAD"].Value = VendorDownload;
                cmdUploadTypeUpdate3.Parameters["I_ISACTIVE"].Value = IsActive;
                cmdUploadTypeUpdate3.Parameters["I_CREATE_BY"].Value = UserID;
                cmdUploadTypeUpdate3.Parameters["I_UPLOAD_TYPE_IND"].Value = IsUploadTypeIND;
                cmdUploadTypeUpdate3.Parameters["I_IS_CHECK_UPLOAD_END_DATE_IND"].Value = IsCheckEndDateIND;
                cmdUploadTypeUpdate3.Parameters["I_REQUIRE_DOC_ALERT_TYPE"].Value = RequireDocAlertIND;

                if (string.Equals(SpecialFlagValue, string.Empty))
                    cmdUploadTypeUpdate3.Parameters["I_SPECIAL_FLAG_VALUE"].Value = DBNull.Value;
                else
                    cmdUploadTypeUpdate3.Parameters["I_SPECIAL_FLAG_VALUE"].Value = SpecialFlagValue;

                dbManager.ExecuteNonQuery(cmdUploadTypeUpdate3);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static UploadTypeDAL _instance;
        public static UploadTypeDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new UploadTypeDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}