﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;

namespace TMS_DAL.Master
{
    public partial class WarehouseDAL : OracleConnectionDAL
    {
        public WarehouseDAL()
        {
            InitializeComponent();
        }

        public WarehouseDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable WarehouseSelectDAL()
        {
            try
            {
                dbManager.Open();
//                cmdWarehouseSelect.CommandText = @"SELECT STERMINALID, SABBREVIATION AS STERMINALNAME
//                                                   FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE '5%' OR T.STERMINALID LIKE '8%'))
//                                                   WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";
                cmdWarehouseSelect.CommandText = @"SELECT STERMINALID, SABBREVIATION AS STERMINALNAME
                                                   FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE 'H%' OR T.STERMINALID LIKE 'K%'))
                                                   WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";

                return dbManager.ExecuteDataTable(cmdWarehouseSelect, "cmdWarehouseSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static WarehouseDAL _instance;
        public static WarehouseDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new WarehouseDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}