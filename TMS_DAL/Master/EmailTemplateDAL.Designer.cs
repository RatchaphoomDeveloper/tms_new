﻿namespace TMS_DAL.Master
{
    partial class EmailTemplateDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdEmailTemplateSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailTempplateSelectTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailTemplateSelectAll = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailTemplateUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailScheduleSelectAll = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailScheduleUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailScheduleCheck = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailScheduleInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdEmailTypeSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdEmailTemplateSelect
            // 
            this.cmdEmailTemplateSelect.CommandText = "SELECT TEMPLATE_ID, TEMPLATE_NAME, SUBJECT, BODY FROM M_EMAIL_TEMPLATE WHERE TEMP" +
    "LATE_ID =: I_TEMPLATE_ID";
            this.cmdEmailTemplateSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdEmailTempplateSelectTruck
            // 
            this.cmdEmailTempplateSelectTruck.CommandText = "SELECT TEMPLATE_ID, TEMPLATE_NAME, SUBJECT, BODY FROM M_EMAIL_TEMP WHERE TEMPLATE" +
    "_ID =: I_TEMPLATE_ID";
            this.cmdEmailTempplateSelectTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdEmailTemplateSelectAll
            // 
            this.cmdEmailTemplateSelectAll.CommandText = "SELECT TEMPLATE_ID, TEMPLATE_NAME, SUBJECT, BODY, MAIL_PARAMETER FROM M_EMAIL_TEM" +
    "PLATE WHERE 1=1";
            this.cmdEmailTemplateSelectAll.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdEmailTemplateUpdate
            // 
            this.cmdEmailTemplateUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_EMAIL_TYPE_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_NAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SUBJECT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_BODY", System.Data.OracleClient.OracleType.VarChar, 4000)});
            // 
            // cmdEmailScheduleUpdate
            // 
            this.cmdEmailScheduleUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_NO", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_DAY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdEmailScheduleCheck
            // 
            this.cmdEmailScheduleCheck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_DAY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdEmailScheduleInsert
            // 
            this.cmdEmailScheduleInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_EMAIL_TYPE_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_NO", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_DAY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdEmailTypeSelect
            // 
            this.cmdEmailTypeSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_NO", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCHEDULE_DAY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdEmailTemplateSelect;
        private System.Data.OracleClient.OracleCommand cmdEmailTempplateSelectTruck;
        private System.Data.OracleClient.OracleCommand cmdEmailTemplateSelectAll;
        private System.Data.OracleClient.OracleCommand cmdEmailTemplateUpdate;
        private System.Data.OracleClient.OracleCommand cmdEmailScheduleSelectAll;
        private System.Data.OracleClient.OracleCommand cmdEmailScheduleUpdate;
        private System.Data.OracleClient.OracleCommand cmdEmailScheduleCheck;
        private System.Data.OracleClient.OracleCommand cmdEmailScheduleInsert;
        private System.Data.OracleClient.OracleCommand cmdEmailTypeSelect;
    }
}
