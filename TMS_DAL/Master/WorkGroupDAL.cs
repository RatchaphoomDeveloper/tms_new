﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class WorkGroupDAL : OracleConnectionDAL
    {
        #region WorkGroupDAL
        public WorkGroupDAL()
        {
            InitializeComponent();
        }

        public WorkGroupDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region WorkGroupSelect
        public DataTable WorkGroupSelect(string NAME, string CACTIVE)
        {
            try
            {
                cmdWorkGroup.CommandText = "SELECT ID, NAME,CACTIVE FROM M_WORKGROUP WHERE (CACTIVE = :CACTIVE OR ((:CACTIVE IS NULL OR :CACTIVE = '') AND 0=0)) AND NAME LIKE :NAME";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = "%" + NAME + "%",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdWorkGroup, "cmdWorkGroup");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region WorkGroupCheckValidate
        public bool WorkGroupCheckValidate(int ID, string NAME)
        {
            try
            {
                cmdWorkGroup.CommandText = "SELECT COUNT(ID) COUNTID FROM M_WORKGROUP WHERE (CACTIVE = 1) AND NAME = :NAME AND ID != :ID";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ID",
                    Value = ID,
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdWorkGroup, "cmdWorkGroup");
                if (dt.Rows.Count > 0 && int.Parse(dt.Rows[0]["COUNTID"] + string.Empty) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region WorkGroupInsert
        public bool WorkGroupInsert(string NAME, string CACTIVE)
        {
            try
            {
                cmdWorkGroup.CommandText = "INSERT INTO M_WORKGROUP(NAME, CACTIVE) VALUES (:NAME, :CACTIVE)";
                cmdWorkGroup.Parameters.Clear();

                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdWorkGroup);
                if (row > 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region WorkGroupUpdate
        public bool WorkGroupUpdate(int ID, string NAME, string CACTIVE)
        {
            try
            {
                cmdWorkGroup.CommandText = "UPDATE M_WORKGROUP SET NAME = :NAME,CACTIVE = :CACTIVE WHERE ID = :ID";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ID",
                    Value = ID,
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdWorkGroup);
                if (row >= 0)
                {
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion


        #region GroupSelect
        public DataTable GroupSelect(int WORKGROUPID, string NAME, string CACTIVE)
        {
            try
            {
                cmdWorkGroup.CommandText = "SELECT M_GROUPS.* FROM M_GROUPS " +
                "LEFT JOIN M_WORKGROUP ON M_GROUPS.WORKGROUPID = M_WORKGROUP.ID " +
                "WHERE (M_WORKGROUP.ID = :WORKGROUPID OR (:WORKGROUPID = 0 AND 0=0)) " +
                "AND M_GROUPS.NAME LIKE :NAME  " +
                "AND (M_GROUPS.CACTIVE = :CACTIVE OR ((:CACTIVE IS NULL OR  :CACTIVE = '') AND 0=0)) " +
                "AND M_WORKGROUP.CACTIVE ='1'";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "WORKGROUPID",
                    Value = WORKGROUPID,
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = "%" + NAME + "%",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdWorkGroup, "cmdWorkGroup");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupCheckValidate
        public bool GroupCheckValidate(int ID, string NAME)
        {
            try
            {
                cmdWorkGroup.CommandText = "SELECT COUNT(ID) COUNTID FROM M_GROUPS WHERE (CACTIVE = 1) AND NAME = :NAME AND ID != :ID";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ID",
                    Value = ID,
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdWorkGroup, "cmdWorkGroup");
                if (dt.Rows.Count > 0 && int.Parse(dt.Rows[0]["COUNTID"] + string.Empty) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupInsert
        public bool GroupInsert(int WORKGROUPID, string NAME, string CACTIVE)
        {
            try
            {
                cmdWorkGroup.CommandText = "INSERT INTO M_GROUPS(WORKGROUPID,NAME, CACTIVE) VALUES (:WORKGROUPID,:NAME, :CACTIVE)";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "WORKGROUPID",
                    Value = WORKGROUPID,
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdWorkGroup);
                if (row > 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region GroupUpdate
        public bool GroupUpdate(int ID, int WORKGROUPID, string NAME, string CACTIVE)
        {
            try
            {
                cmdWorkGroup.CommandText = "UPDATE M_GROUPS SET NAME = :NAME,CACTIVE = :CACTIVE ,WORKGROUPID = :WORKGROUPID WHERE ID = :ID";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ID",
                    Value = ID,
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "WORKGROUPID",
                    Value = WORKGROUPID,
                    OracleType = System.Data.OracleClient.OracleType.Number,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdWorkGroup);
                if (row >= 0)
                {
                    isRes = true;
                }
                return isRes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        public DataTable VEHICLE_STATUS()
        {
            try
            {
                cmdWorkGroup.CommandType = CommandType.StoredProcedure;
                cmdWorkGroup.CommandText = "ivms.VEHICLE_STATUS@ivms";
                cmdWorkGroup.Parameters.Clear();
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "License",
                    Value = "H105",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "splantcode",
                    Value = "H105",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "carriercode",
                    Value = "",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "contractnum",
                    Value = "",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                cmdWorkGroup.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "RETVAL",
                    OracleType = System.Data.OracleClient.OracleType.Cursor,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdWorkGroup, "cmdWorkGroup");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static WorkGroupDAL _instance;
        public static WorkGroupDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new WorkGroupDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
