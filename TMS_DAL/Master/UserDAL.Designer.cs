﻿namespace TMS_DAL.Master
{
    partial class UserDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDAL));
            this.cmdUser = new System.Data.OracleClient.OracleCommand();
            this.cmdStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdUserInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdCGroup = new System.Data.OracleClient.OracleCommand();
            this.cmdUserWithDocDate = new System.Data.OracleClient.OracleCommand();
            this.cmdUserOnlyDocDate = new System.Data.OracleClient.OracleCommand();
            this.cmdDocExpire = new System.Data.OracleClient.OracleCommand();
            this.cmdDocExpireAndInactive = new System.Data.OracleClient.OracleCommand();
            this.cmdSoldToSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTeam = new System.Data.OracleClient.OracleCommand();
            this.cmdSaleDist = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdUser
            // 
            this.cmdUser.CommandText = " SELECT * FROM VW_M_USER_SELECT  WHERE 1=1";
            // 
            // cmdStatus
            // 
            this.cmdStatus.CommandText = "SELECT * FROM M_STATUS WHERE 1=1 ";
            // 
            // cmdUserInsert
            // 
            this.cmdUserInsert.Connection = this.OracleConn;
            this.cmdUserInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SUID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CGROUP", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_FIRSTNAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_LASTNAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_POSITION", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_TEL", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_USERNAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_PASSWORD", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_OLDPASSWORD", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_EMAIL", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_CONTRACT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DEPARTMENT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DIVISION_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SALES_DISTRICT", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdCGroup
            // 
            this.cmdCGroup.CommandText = " SELECT M_USERGROUP.IS_ADMIN, M_USERGROUP.USERGROUP_ID  FROM M_USERGROUP   WHERE " +
    "1=1";
            // 
            // cmdUserWithDocDate
            // 
            this.cmdUserWithDocDate.CommandText = resources.GetString("cmdUserWithDocDate.CommandText");
            // 
            // cmdUserOnlyDocDate
            // 
            this.cmdUserOnlyDocDate.CommandText = " SELECT DISTINCT UPLOAD_ID,UPLOAD_NAME FROM M_UPLOAD_TYPE WHERE UPLOAD_TYPE_IND =" +
    " 1 AND UPLOAD_TYPE = \'VENDOR\' ORDER BY UPLOAD_ID";
            // 
            // cmdDocExpire
            // 
            this.cmdDocExpire.Connection = this.OracleConn;
            this.cmdDocExpire.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdDocExpireAndInactive
            // 
            this.cmdDocExpireAndInactive.Connection = this.OracleConn;
            this.cmdDocExpireAndInactive.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdSoldToSelect
            // 
            this.cmdSoldToSelect.Connection = this.OracleConn;
            this.cmdSoldToSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEARCH", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdTeam
            // 
            this.cmdTeam.CommandText = " SELECT * FROM VW_M_TEAM_SELECT  WHERE 1=1";
            // 
            // cmdSaleDist
            // 
            this.cmdSaleDist.CommandText = " SELECT * FROM VW_M_SALE_DIST_SELECT  WHERE 1=1";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdUser;
        private System.Data.OracleClient.OracleCommand cmdStatus;
        private System.Data.OracleClient.OracleCommand cmdCGroup;
        private System.Data.OracleClient.OracleCommand cmdUserInsert;
        private System.Data.OracleClient.OracleCommand cmdUserWithDocDate;
        private System.Data.OracleClient.OracleCommand cmdUserOnlyDocDate;
        private System.Data.OracleClient.OracleCommand cmdDocExpire;
        private System.Data.OracleClient.OracleCommand cmdDocExpireAndInactive;
        private System.Data.OracleClient.OracleCommand cmdSoldToSelect;
        private System.Data.OracleClient.OracleCommand cmdTeam;
        private System.Data.OracleClient.OracleCommand cmdSaleDist;

        
    }
}
