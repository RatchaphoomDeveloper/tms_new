﻿using dbManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class MaintainDAL : OracleConnectionDAL
	{
		#region MaintainDAL
		public MaintainDAL()
        {
            InitializeComponent();
        }

		public MaintainDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

		#region MaintainSelect
		public DataTable MaintainSelect()
        {
            cmdMaintainSelect.CommandType = CommandType.StoredProcedure;
			//cmdMaintainSelect.Parameters.Clear();
			DataTable dt = dbManager.ExecuteDataTable(cmdMaintainSelect, "MAINTAIN");
            return dt;
        }
        #endregion

		#region MaintainSave
		public DataTable MaintainSave(string I_USERID,DataTable MaintainDT)
        {
			try
			{
				DataTable dtMaintain = null;
				DataTable dt = null;
				DataSet ds = new DataSet("ds");
				dtMaintain = MaintainDT.Copy();
				dtMaintain.TableName = "dt";
				ds.Tables.Add(dtMaintain);

				dbManager.Open();
				dbManager.BeginTransaction();
				cmdMaintainSave.CommandType = CommandType.StoredProcedure;
				cmdMaintainSave.Parameters["I_USERID"].Value = I_USERID;
				cmdMaintainSave.Parameters["DATAHANDLE"].Value = ds.GetXml();
				//dt = dbManager.ExecuteDataTable(cmdMaintainSave, "cmdMaintainSave");
				dbManager.ExecuteNonQuery(cmdMaintainSave);
				dbManager.CommitTransaction();
				return dt;
			}
			catch(Exception ex)
			{
				dbManager.RollbackTransaction();
				throw new Exception(ex.Message);
			}
            finally
            {
                dbManager.Close();
            }

        }
        #endregion
        
        #region + Instance +
		private static MaintainDAL _instance;
		public static MaintainDAL Instance
        {
            get
            {
				_instance = new MaintainDAL();                
                return _instance;
            }
        }
        #endregion
    }
}
