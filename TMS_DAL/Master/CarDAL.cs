﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;
using System.Web.UI.WebControls;
using TMS_Entity.CarEntity;


namespace TMS_DAL.Master
{

    public partial class CarDAL : OracleConnectionDAL
    {
        public CarDAL()
        {
            InitializeComponent();

        }

        public CarDAL(IContainer container)
        {
            container.Add(this);
            InitializeComponent();

        }

        #region LoadData
        public DataTable InitalDataRoute()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdConfigSelect, "cmdConfigSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable InitalCarTypeSap()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdCarsap, "cmdCarsap");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable InitalCarType()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdCartype, "cmdCartype");
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public DataTable InitalBand()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdloadband, "cmdloadband");

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        public DataTable InitalGps()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdloadGps, "cmdloadGps");

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region GET TRUCKID INITIAL
        public string GenTRUKCID()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                DataTable _dt = dbManager.ExecuteDataTable(cmdFunctionGetID, "cmdFunctionGetID");
                if (_dt.Rows.Count >= 1)
                {
                    string _return = _dt.Rows[0][0].ToString();
                    _dt.Dispose();
                    return _return;
                }
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                throw;
            }
            finally
            {
                dbManager.Close();
            }



        }
        #endregion

        #region Data หน้าแรก
        public DataTable initalData()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();

                string Strsql = @"SELECT TH.LDUPDATE,
  TH.STRUCKID,
  TH.SHEADREGISTERNO,
  TH.SCHASIS,
  TH.SCARTYPEID ,
  TT.SCARCATEGORY ,
  TC.NCAPACITY AS NTOTALCAPACITY ,
  TH.CACTIVE ,
  NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
  C.SCONTRACTID,
  C.SVENDORID ,
  C.SCONTRACTNO,
  TH.ISUSE
FROM
  (SELECT
    CASE
      WHEN TH.DCREATE!=TH.DUPDATE
      THEN 1
      ELSE 0
    END LDUPDATE ,
    TH.STRUCKID,
    TH.SHEADREGISTERNO,
    TH.SCHASIS,
    TH.SCARTYPEID ,
    NVL(TH.CACTIVE,'Y') AS CACTIVE,
    TH.DUPDATE,
    TH.STRAILERID,
    TH.DCREATE
  FROM TTRUCK TH
  WHERE TH.SCARTYPEID IN ('0','3','4')
  )TH
LEFT JOIN TTRUCKTYPE TT
ON TH.SCARTYPEID=TT.SCARTYPEID
LEFT JOIN TCONTRACT_TRUCK CT
ON TH.STRUCKID              =CT.STRUCKID
AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
LEFT JOIN TCONTRACT C
ON CT.SCONTRACTID=C.SCONTRACTID
LEFT JOIN
  (SELECT TC.STRUCKID,
    SUM(TC.NCAPACITY) AS NCAPACITY
  FROM
    (SELECT STRUCKID,
      NCOMPARTNO,
      MAX(NPANLEVEL) AS NPANLEVEL ,
      MAX(NCAPACITY) AS NCAPACITY
    FROM TTRUCK_COMPART
      --WHERE STRUCKID = 'TR5500628'
    GROUP BY STRUCKID,
      NCOMPARTNO
    )TC
  GROUP BY TC.STRUCKID
  )TC
ON TC.STRUCKID =
  CASE
    WHEN TH.SCARTYPEID = '0'
    THEN TH.STRUCKID
    
  END
WHERE LOGDATA ='1'
ORDER BY TH.STRUCKID DESC";
                cmdDataInfo.CommandText = Strsql;
                return dbManager.ExecuteDataTable(cmdDataInfo, "cmdDataInfo");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }
        #region Data Blacklist
        public DataTable DataBlacklist(string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                string strSql = @"SELECT REMARK,DBLACKLIST_START,DBLACKLIST_END,SHEADREGISTERNO, CASE  
  WHEN TTRUCK_BLACKLIST.TYPE = 'N' THEN 'ระงับ'
  WHEN TTRUCK_BLACKLIST.TYPE = 'D' THEN 'ยกเลิก'  
  END as TYPE FROM TTRUCK_BLACKLIST
  WHERE SHEADREGISTERNO=:SHEADREGISTERNO";
                cmdBlacklistData.CommandText = strSql;
                cmdBlacklistData.Parameters.Clear();
                cmdBlacklistData.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdBlacklistData, "cmdBlacklistData");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion
        #region หาจำนวน Row Blacklist
        public string DataRowBlacklist(string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                string strSql = @"SELECT CASE  
              WHEN TTRUCK_BLACKLIST.TYPE = 'N' THEN 'N'
              WHEN TTRUCK_BLACKLIST.TYPE = 'D' THEN 'D'  
              END as TYPE FROM TTRUCK_BLACKLIST
              WHERE SHEADREGISTERNO=:SHEADREGISTERNO";
                cmdBlacklistData.CommandText = strSql;
                cmdBlacklistData.Parameters.Clear();
                cmdBlacklistData.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteScalar(cmdBlacklistData).ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion
        #endregion

        #region GET GenId INITIAL
        public string GenidDoc()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdFucntionGenIdDoc.CommandText = @"SELECT MAX(DOCID)+1 As DOCID FROM TTRUCK_INSUREDOC WHERE ROWNUM <= 1";
                DataTable _dt = dbManager.ExecuteDataTable(cmdFucntionGenIdDoc, "cmdFucntionGenIdDoc");
                if (_dt.Rows.Count >= 1)
                {
                    string _return = _dt.Rows[0][0].ToString();
                    _dt.Dispose();
                    return _return;
                }
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion

        #region SELECT Data
        #region Select Header
        public DataTable SelectDataHeader(string STRUCKID)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                string strSql;
                strSql = @"SELECT NSLOT,SHEADREGISTERNO,SOWNER_NAME,SCARTYPEID,STRANSPORTID,ROUTE,SENGINE,TRUCK_CATEGORY,SLOADING_METHOD,SMODEL,SBRAND,TTRUCK_BRAND.BRANDID,SCHASIS,SVIBRATION,NWHEELS,MATERIALOFPRESSURE,NSHAFTDRIVEN,RWIGHTDRIVEN,POWERMOVER,NWEIGHT,PUMPPOWER_TYPE,VOL_UOM,GPS_SERVICE_PROVIDER,VALVETYPE,FUELTYPE,PUMPPOWER,ISUSE,
DREGISTER ,STANK_MAKER,STANK_MATERAIL,SCAR_NUM,PLACE_WATER_MEASURE,SLAST_REQ_ID,DPREV_SERV,DWATEREXPIRE,TTRUCK.CACTIVE, LOGDATA,COMMENTDATA, NVL(SPOT_STATUS,0) AS SPOT_STATUS FROM TTRUCK INNER JOIN TTRUCK_BRAND ON TTRUCK.SBRAND = TTRUCK_BRAND.BRANDID OR TTRUCK.SMODEL = TTRUCK_BRAND.BRANDID OR TTRUCK.SBRAND = TTRUCK_BRAND.BRANDNAME WHERE STRUCKID=:STRUCKID";
                cmdSelectData.CommandText = strSql;
                cmdSelectData.Parameters.Clear();
                cmdSelectData.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                return dbManager.ExecuteDataTable(cmdSelectData, "cmdSelectData");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }




        }
        #endregion
        #region Select Insureance
        public DataTable SelectDataInsurance(string STRUCKID)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                DataTable ValueData = new DataTable();
                string strSql;
                strSql = @"SELECT SCOMPANY,SDETAIL,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,START_DURATION,END_DURATION FROM TTRUCK_INSURANCE WHERE STRUCKID=:STRUCKID";
                cmdSelectData.CommandText = strSql;
                cmdSelectData.Parameters.Clear();
                cmdSelectData.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                ValueData = dbManager.ExecuteDataTable(cmdSelectData, "cmdSelectData");

                return ValueData;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

        }
        #endregion
        #region Select Compart
        public DataTable SelectDataCompart(string STRUCKID)
        {
            try
            {
                DataTable ValueData = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                string strSql;
                strSql = @"SELECT NCOMPARTNO,NPANLEVEL,NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID=:STRUCKID ORDER BY NCOMPARTNO ASC";
                cmdSelectData.CommandText = strSql;
                cmdSelectData.Parameters.Clear();
                cmdSelectData.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                ValueData = dbManager.ExecuteDataTable(cmdSelectData, "cmdSelectData");

                return ValueData;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message); ;
            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion
        #region SELECT ทะเบียนหางไปหาหัว
        public string SelectFindHeadBySemi(string SHEADREGISTERNO)
        {
            try
            {
                DataTable Dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdFindHeadBySemi.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                Dt = dbManager.ExecuteDataTable(cmdFindHeadBySemi, "cmdFindHeadBySemi");
                return Dt.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion
        #region Select เจ้าของรถ
        public DataTable DataVendorName(String VendorId)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdTruckVendorName.Parameters.Clear();
                cmdTruckVendorName.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorId;
                return dbManager.ExecuteDataTable(cmdTruckVendorName, "cmdTruckVendorName");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion
        #region Select GenClassGrp //contract_add.aspx
        public DataTable GetClassGrp(string SHEADREGISTERNO)
        {
            try
            {
                DataTable Dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdSelectGenClassgrp.CommandText = "SELECT T.SCARTYPEID,T.TRUCK_CATEGORY,CASE WHEN (NVL(T.SCARTYPEID,0) = 0) THEN NVL(T.NSHAFTDRIVEN,0) ELSE NVL(TL.NSHAFTDRIVEN,0) END AS NSHAFTDRIVEN,T.DREGISTER,CASE WHEN (NVL(T.SCARTYPEID,0) = 0) THEN NVL(T.NTOTALCAPACITY,0) ELSE NVL(TL.NTOTALCAPACITY,0) END AS NTOTALCAPACITY FROM TTRUCK T LEFT JOIN TTRUCK TL ON T.STRAILERID = TL.STRUCKID WHERE T.SHEADREGISTERNO= :SHEADREGISTERNO";
                cmdSelectGenClassgrp.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdSelectGenClassgrp, "cmdSelectGenClassgrp");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }

        }

        #endregion
        #endregion

        #region INSERT DATA
        public DataTable SaveDataTruck(string CarType, DataTable Truck, DataTable Doc, DataTable DocTPN, DataTable Insurance, DataTable Oilcapacity, string SpotStatus)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdinsert.CommandText = "USP_M_TTRUCK_INSERT";
                cmdinsert.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet("ds");
                Truck.TableName = "dt";
                ds.Tables.Add(Truck);
                cmdinsert.Parameters["TYPECAR"].Value = CarType.ToString();
                cmdinsert.Parameters["I_TRUCK_HEAD"].Value = ds.GetXml();
                //เอกสาร ที่ไม่มีวันหมดอายุ
                DataSet doc_ds = new DataSet("doc_ds");
                Doc.TableName = "doc";
                doc_ds.Tables.Add(Doc.Copy());
                cmdinsert.Parameters["Doc"].Value = doc_ds.GetXml();
                //เอกสาร ที่มีวันหมดอายุ
                DataSet doc_tpn_ds = new DataSet("doc_tpn_ds");
                DocTPN.TableName = "doctpn";
                doc_tpn_ds.Tables.Add(DocTPN.Copy());
                cmdinsert.Parameters["DocTPN"].Value = doc_tpn_ds.GetXml();
                //บริษัทประกัน
                DataSet insurance_ds = new DataSet("insurance_ds");
                Insurance.TableName = "insurance";
                insurance_ds.Tables.Add(Insurance.Copy());
                cmdinsert.Parameters["Insurance"].Value = insurance_ds.GetXml();
                //ช่องเติม
                DataSet capacity_ds = new DataSet("capacity_ds");
                Oilcapacity.TableName = "oilcapacity";
                capacity_ds.Tables.Add(Oilcapacity.Copy());
                cmdinsert.Parameters["Oilcapacity"].Value = capacity_ds.GetXml();

                cmdinsert.Parameters["I_SPOT_STATUS"].Value = SpotStatus;


                //ประมวลผล
                DataTable dt = dbManager.ExecuteDataTable(cmdinsert, "cmdinsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);


            }
            finally
            {
                dbManager.Close();
            }

        }


        #endregion

        #region Update Data
        public DataTable UpdateDataTruck(string TtruckId, string CarType, DataTable Truck, DataTable Doc, DataTable DocTPN, DataTable Insurance, DataTable Oilcapacity, string SpotStatus)
        {

            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();

            try
            {
                cmdupdateTruck.CommandText = "USP_M_TTRUCK_UPDATE";
                cmdupdateTruck.CommandType = CommandType.StoredProcedure;
                cmdupdateTruck.Parameters["TRUCKID"].Value = TtruckId;
                DataSet ds = new DataSet("ds");
                Truck.TableName = "dt";
                ds.Tables.Add(Truck.Copy());
                cmdupdateTruck.Parameters["TYPECAR"].Value = CarType.ToString();
                cmdupdateTruck.Parameters["I_TRUCK_HEAD"].Value = ds.GetXml();
                
                //เอกสารอัพโหลด
                DataSet doc_ds = new DataSet("doc_ds");
                Doc.TableName = "doc";
                doc_ds.Tables.Add(Doc.Copy());
                cmdupdateTruck.Parameters["Doc"].Value = doc_ds.GetXml();

                //เอกสารอัพโหลด TPN
                DataSet doc_tpn_ds = new DataSet("doc_tpn_ds");
                DocTPN.TableName = "doctpn";
                doc_tpn_ds.Tables.Add(DocTPN.Copy());
                cmdupdateTruck.Parameters["DocTPN"].Value = doc_tpn_ds.GetXml();

                cmdupdateTruck.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                //บริษัทประกัน
                DataSet insurance_ds = new DataSet("insurance_ds");
                Insurance.TableName = "insurance";
                insurance_ds.Tables.Add(Insurance);
                cmdupdateTruck.Parameters["Insurance"].Value = insurance_ds.GetXml();
                //ช่องเติม
                DataSet capacity_ds = new DataSet("capacity_ds");
                Oilcapacity.TableName = "oilcapacity";
                capacity_ds.Tables.Add(Oilcapacity);
                cmdupdateTruck.Parameters["Oilcapacity"].Value = capacity_ds.GetXml();
                //ประมวลผล
                DataTable dt = dbManager.ExecuteDataTable(cmdupdateTruck, "cmdupdateTruck");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);


            }
            finally
            {
                dbManager.Close();

            }
        }
        // APPROVE
        public void UpdatateApprove(string TTRUCKID)
        {

            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                cmdUpdateApprove.Parameters.Clear();
                cmdUpdateApprove.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = TTRUCKID;
                //ประมวลผล
                dbManager.ExecuteNonQuery(cmdUpdateApprove);
                dbManager.CommitTransaction();


            }
            catch (Exception ex)
            {

                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region UPDATE ClassGrp
        public void UpDateClassgrp(string Classgrp, string SHEADREGISTERNO)
        {

            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdUpdateClassGrp.Parameters.Add(":CLASSGRP", OracleType.VarChar).Value = Classgrp;
                cmdUpdateClassGrp.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                dbManager.ExecuteNonQuery(cmdUpdateClassGrp);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                throw;
            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion

        #region Update NTOTAL
        public void UpdateNTotal(string TtruckId, int nTotal)
        {

            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                //cmdUpdateCapacity.Parameters.Clear();
                cmdUpdateCapacity.CommandText = "USP_M_TTRUCK_UPDATE_NTOTAL";
                cmdUpdateCapacity.CommandType = CommandType.StoredProcedure;
                cmdUpdateCapacity.Parameters["TRUCKID"].Value = TtruckId;
                cmdUpdateCapacity.Parameters["NTOTAL"].Value = nTotal;
                //cmdUpdateCapacity.Parameters.Add("O_RETRUCK", OracleType.VarChar).Value = nTotal;
                //ประมวลผล
                dbManager.ExecuteNonQuery(cmdUpdateCapacity);
                dbManager.CommitTransaction();


            }
            catch (Exception ex)
            {

                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region CreateClassgrp
        public DataTable CreateClassgrp(string TtruckId, string nTotal)
        {

            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                cmdGenClassgrp.CommandText = "USP_M_GENERATE_CLASSGRP";
                cmdGenClassgrp.CommandType = CommandType.StoredProcedure;
                cmdGenClassgrp.Parameters["I_SHEADREGISTERNO"].Value = TtruckId;
                cmdGenClassgrp.Parameters["I_DOCUMENT"].Value = nTotal;
                DataTable dt = dbManager.ExecuteDataTable(cmdGenClassgrp, "cmdGenClassgrp");
                dbManager.CommitTransaction();
                return dt;

            }
            catch (Exception ex)
            {

                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region TruckAgeExpire
        public DataTable TruckAgeExpireDAL(int Template_ID)
        {
            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                cmdTruckAgeExpire.CommandText = "USP_M_TRUCK_AGE_EXPIRE";
                cmdTruckAgeExpire.CommandType = CommandType.StoredProcedure;
                cmdTruckAgeExpire.Parameters["I_TEMPLATE_ID"].Value = Template_ID;
                DataTable dt = dbManager.ExecuteDataTable(cmdTruckAgeExpire, "cmdTruckAgeExpire");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {

                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region TruckAgeExpireSysdate
        public DataTable TruckAgeExpireSysdateDAL(int Template_ID)
        {
            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                cmdTruckAgeExpire.CommandText = "USP_M_TRUCK_AGE_EXPIRE_SYSDATE";
                cmdTruckAgeExpire.CommandType = CommandType.StoredProcedure;
                cmdTruckAgeExpire.Parameters["I_TEMPLATE_ID"].Value = Template_ID;
                DataTable dt = dbManager.ExecuteDataTable(cmdTruckAgeExpire, "cmdTruckAgeExpire");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region TruckDocExpire
        public DataTable TruckDocExpireDAL(int Template_ID)
        {
            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                cmdTruckAgeExpire.CommandText = "USP_M_TRUCK_DOC_EXPIRE";
                cmdTruckAgeExpire.CommandType = CommandType.StoredProcedure;
                cmdTruckAgeExpire.Parameters["I_TEMPLATE_ID"].Value = Template_ID;
                DataTable dt = dbManager.ExecuteDataTable(cmdTruckAgeExpire, "cmdTruckAgeExpire");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region TruckDocExpireSysdate
        public DataTable TruckDocExpireSysdateDAL(int Template_ID)
        {
            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                cmdTruckAgeExpire.CommandText = "USP_M_TRUCK_DOC_EXPIRE_SYSDATE";
                cmdTruckAgeExpire.CommandType = CommandType.StoredProcedure;
                cmdTruckAgeExpire.Parameters["I_TEMPLATE_ID"].Value = Template_ID;
                DataTable dt = dbManager.ExecuteDataTable(cmdTruckAgeExpire, "cmdTruckAgeExpire");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region UpdateTruckAgeExpire
        public DataTable UpdateTruckAgeExpireDAL(DataTable ttruck)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();

                cmdUpdateTruckExpire.CommandText = "USP_M_TRUCK_AGE_EXPIRE_UPDATE";
                cmdUpdateTruckExpire.CommandType = CommandType.StoredProcedure;
                DataSet truck_ds = new DataSet("truck_ds");
                ttruck.TableName = "truck";
                truck_ds.Tables.Add(ttruck.Copy());
                cmdUpdateTruckExpire.Parameters["I_TRUCK_EXPIRE"].Value = truck_ds.GetXml();

                DataTable dt = dbManager.ExecuteDataTable(cmdUpdateTruckExpire, "cmdUpdateTruckExpire");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region UpdateTruckExpire
        public DataTable UpdateTruckExpireDAL(string struckid)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();

                cmdUpdateTruckExpire2.CommandText = "USP_M_TRUCK_EXPIRE_UPDATE";
                cmdUpdateTruckExpire2.CommandType = CommandType.StoredProcedure;

                cmdUpdateTruckExpire2.Parameters["I_STRUCKID"].Value = struckid;

                DataTable dt = dbManager.ExecuteDataTable(cmdUpdateTruckExpire2, "cmdUpdateTruckExpire2");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region DeleteTruck
        public DataTable DeleteTruckDAL(string TRUCKID)
        {
            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                cmdDelTruckForInsert.CommandText = "USP_M_TTRUCK_DELETE_FOR_INSERT";
                cmdDelTruckForInsert.CommandType = CommandType.StoredProcedure;
                cmdDelTruckForInsert.Parameters["TRUCKID"].Value = TRUCKID;
                DataTable dt = dbManager.ExecuteDataTable(cmdDelTruckForInsert, "cmdDelTruckForInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        //SaveComment
        #region SaveComment

        public void SaveComment(string TTRUCKID, string COMMENT)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdSavecomment.Parameters.Clear();
                cmdSavecomment.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = TTRUCKID;
                cmdSavecomment.Parameters.Add(":COMMENTDATA", OracleType.VarChar).Value = COMMENT;
                //ประมวลผล
                dbManager.ExecuteNonQuery(cmdSavecomment);
                dbManager.CommitTransaction();


            }
            catch (Exception ex)
            {

                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region Check Email
        public DataTable CheckAlterEmal(string STRUCKID)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdCheckCarApprove.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                //ประมวลผล
                return dbManager.ExecuteDataTable(cmdCheckCarApprove, "cmdCheckCarApprove");


            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region + Instance +
        private static CarDAL _instance;
        public static CarDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new CarDAL();
                //}
                return _instance;
            }
        }
        #endregion

        public DataTable TruckAgeSelect(string Condition)
        {
            try
            {
                cmdTruckAgeSelect.CommandText = @"SELECT DISTINCT * FROM VW_M_TRUCK_AGE WHERE 1=1 ";
                cmdTruckAgeSelect.CommandText += Condition;

                DataTable dt = dbManager.ExecuteDataTable(cmdTruckAgeSelect, "cmdTruckAgeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void TruckCancelDAL(string I_TRUCK)
        {
            if (dbManager.Connection.State != ConnectionState.Open)
                dbManager.Open();
            dbManager.BeginTransaction();
            try
            {
                //cmdUpdateCapacity.Parameters.Clear();
                cmdCancelTruckAdd.CommandText = "USP_M_TTRUCK_CANCEL";
                cmdCancelTruckAdd.CommandType = CommandType.StoredProcedure;
                cmdCancelTruckAdd.Parameters["I_TRUCK"].Value = I_TRUCK;
                //cmdUpdateCapacity.Parameters.Add("O_RETRUCK", OracleType.VarChar).Value = nTotal;
                //ประมวลผล
                dbManager.ExecuteNonQuery(cmdCancelTruckAdd);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {

                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable GetDocMVDAL(string TruckID)
        {
            try
            {
                cmdGetDocMV.CommandText = @"SELECT TMP.*
                                                 , TBL_REQDOC.DOC_TYPE, TBL_REQDOC.FILE_PATH || TBL_REQDOC.FILE_SYSNAME AS FULLPATH
                                                 , CASE WHEN TBL_REQDOC.DOC_TYPE = '0006' THEN 'เอกสารใบเทียบแป้น' ELSE '' END AS UPLOAD_NAME
                                                 , TBL_REQDOC.FILE_NAME AS FILENAME_USER
                                            FROM
                                            (
                                                SELECT TMP3.*
                                                FROM
                                                (
                                                    SELECT TMP2.*
                                                    FROM
                                                    (
                                                        SELECT DISTINCT TTRUCK.SCAR_NUM AS DOC_NUMBER
                                                             , TO_CHAR(TTRUCK.DLAST_SERV, 'DD/MM/YYYY') AS START_DATE, TO_CHAR(TTRUCK.DWATEREXPIRE, 'DD/MM/YYYY') AS STOP_DATE
                                                             , (
                                                                   SELECT TMP.REQUEST_ID
                                                                   FROM 
                                                                   (
                                                                        SELECT DISTINCT REQUEST_ID
                                                                        FROM TBL_TIME_INNER_CHECKINGS 
                                                                        WHERE (STRUCKID =:I_TRUCK_ID OR STRUCKID = (SELECT STRUCKID FROM TTRUCK WHERE STRAILERID =:I_TRUCK_ID))
                                                                        ORDER BY REQUEST_ID DESC
                                                                   )TMP WHERE ROWNUM = 1
                                                                ) AS REQUEST_ID
                                                        FROM TTRUCK 
                                                        WHERE (TTRUCK.STRUCKID =:I_TRUCK_ID OR TTRUCK.STRUCKID = (SELECT TTRUCK.STRUCKID FROM TTRUCK WHERE STRAILERID =:I_TRUCK_ID))
                                                    ) TMP2
                                                    ORDER BY STOP_DATE
                                                ) TMP3
                                                WHERE ROWNUM = 1
                                            ) TMP INNER JOIN TBL_REQDOC ON TMP.REQUEST_ID = TBL_REQDOC.REQUEST_ID
                                            WHERE TBL_REQDOC.DOC_TYPE = '0006'";

                cmdGetDocMV.Parameters["I_TRUCK_ID"].Value = TruckID;

                DataTable dt = dbManager.ExecuteDataTable(cmdGetDocMV, "cmdGetDocMV");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
    }

}
