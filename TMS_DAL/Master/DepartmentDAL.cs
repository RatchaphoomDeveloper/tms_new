﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class DepartmentDAL : OracleConnectionDAL
    {
        public DepartmentDAL()
        {
            InitializeComponent();
        }

        public DepartmentDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable DepartmentSelectDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdDepartmentSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdDepartmentSelect, "cmdDepartmentSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable DepartmentInsertDAL(int DEPARTMENT_ID, string DEPARTMENT_NAME, int CACTIVE, int CREATER)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdDepartmentInsert.CommandText = "USP_M_DEPARTMENT_INSERT";
                cmdDepartmentInsert.CommandType = CommandType.StoredProcedure;

                cmdDepartmentInsert.Parameters["I_DEPARTMENT_ID"].Value = DEPARTMENT_ID;
                cmdDepartmentInsert.Parameters["I_DEPARTMENT_NAME"].Value = DEPARTMENT_NAME;
                cmdDepartmentInsert.Parameters["I_CACTIVE"].Value = CACTIVE;
                cmdDepartmentInsert.Parameters["I_CREATER"].Value = CREATER;

                dt = dbManager.ExecuteDataTable(cmdDepartmentInsert, "cmdDepartmentInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static DepartmentDAL _instance;
        public static DepartmentDAL Instance
        {
            get
            {
                _instance = new DepartmentDAL();
                return _instance;
            }
        }
        #endregion
    }
}
