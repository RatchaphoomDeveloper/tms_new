﻿namespace TMS_DAL.Master
{
	partial class MaintainDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.cmdMaintainSelect = new System.Data.OracleClient.OracleCommand();
			this.cmdMaintainSave = new System.Data.OracleClient.OracleCommand();
			// 
			// cmdMaintainSelect
			// 
			this.cmdMaintainSelect.CommandText = "USP_M_MAINTAIN_SELECT";
			this.cmdMaintainSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
			// 
			// cmdMaintainSave
			// 
			this.cmdMaintainSave.CommandText = "USP_M_MAINTAIN_SAVE";
			this.cmdMaintainSave.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERID", System.Data.OracleClient.OracleType.NVarChar, 255),
            new System.Data.OracleClient.OracleParameter("DATAHANDLE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("O_RETRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

		private System.Data.OracleClient.OracleCommand cmdMaintainSelect;
		private System.Data.OracleClient.OracleCommand cmdMaintainSave;
    }
}
