﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class PositionDAL : OracleConnectionDAL
    {
        #region PositionDAL
        public PositionDAL()
        {
            InitializeComponent();
        }

        public PositionDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region PositionSelect
        public DataTable PositionSelect(string PERSON_TYPE_DESC, string CACTIVE)
        {
            try
            {
                cmdPositionSelect.CommandText = "SELECT PERSON_TYPE, PERSON_TYPE_DESC,CACTIVE,AGEMIN,AGEMAX FROM TEMPLOYEETYPES WHERE (CACTIVE = :CACTIVE OR ((:CACTIVE IS NULL OR :CACTIVE = '') AND 0=0)) AND PERSON_TYPE_DESC LIKE :PERSON_TYPE_DESC";
                cmdPositionSelect.Parameters.Clear();
                cmdPositionSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE_DESC",
                    Value = "%" + PERSON_TYPE_DESC + "%",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdPositionSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdPositionSelect, "cmdPositionSelect");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PositionCheckValidate
        public bool PositionCheckValidate(int PERSON_TYPE, string PERSON_TYPE_DESC)
        {
            try
            {
                cmdPositionSelect.CommandText = "SELECT COUNT(PERSON_TYPE) COUNTPERSON_TYPE FROM TEMPLOYEETYPES WHERE (CACTIVE = 1) AND PERSON_TYPE_DESC = :PERSON_TYPE_DESC AND PERSON_TYPE != :PERSON_TYPE";
                cmdPositionSelect.Parameters.Clear();
                cmdPositionSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE_DESC",
                    Value = PERSON_TYPE_DESC,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdPositionSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE",
                    Value = PERSON_TYPE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdPositionSelect, "cmdPositionSelect");
                if (dt.Rows.Count > 0 && int.Parse(dt.Rows[0]["COUNTPERSON_TYPE"] + string.Empty) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PositionInsert
        public bool PositionInsert(string PERSON_TYPE_DESC, string CACTIVE, int? AGEMIN, int? AGEMAX)
        {
            try
            {
                cmdPositionInsert.CommandText = "INSERT INTO TEMPLOYEETYPES(PERSON_TYPE_DESC, CACTIVE,AGEMIN,AGEMAX) VALUES (:PERSON_TYPE_DESC, :CACTIVE,:AGEMIN,:AGEMAX)  RETURNING PERSON_TYPE INTO :PERSON_TYPE";
                cmdPositionInsert.Parameters.Clear();

                cmdPositionInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE_DESC",
                    Value = PERSON_TYPE_DESC,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdPositionInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "AGEMIN",
                    Value = AGEMIN ?? (object)DBNull.Value,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdPositionInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "AGEMAX",
                    Value = AGEMAX ?? (object)DBNull.Value,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdPositionInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdPositionInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "PERSON_TYPE",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdPositionInsert);
                if (row >= 0)
                {
                    int PERSON_TYPE = int.Parse(cmdPositionInsert.Parameters["PERSON_TYPE"].Value + string.Empty);
                    cmdPositionInsert.CommandText = "INSERT INTO M_REQUIRE_LEVEL_DATA(DETAIL, PARENTID,MREQUIRELEVELID,REALID,CACTIVE) VALUES (:DETAIL, 0,3,:REALID,:CACTIVE)";
                    cmdPositionInsert.Parameters.Clear();

                    cmdPositionInsert.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "REALID",
                        Value = PERSON_TYPE,
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdPositionInsert.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "CACTIVE",
                        Value = CACTIVE,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    cmdPositionInsert.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "DETAIL",
                        Value = PERSON_TYPE_DESC,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    row = dbManager.ExecuteNonQuery(cmdPositionInsert);
                    if (row >= 0)
                    {
                        cmdPositionInsert.CommandText = "INSERT INTO M_COMPLAIN_REQUIRE_FIELD( "+
   "COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,CREATE_DATETIME,UPDATE_BY,UPDATE_DATETIME,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION) "+
"SELECT :PERSON_TYPE AS COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,SYSDATE,UPDATE_BY,SYSDATE,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION FROM M_COMPLAIN_REQUIRE_FIELD WHERE FIELD_TYPE = 'VENDOR' AND COMPLAIN_TYPE_ID = 0";
                        cmdPositionInsert.Parameters.Clear();

                        cmdPositionInsert.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "PERSON_TYPE",
                            Value = PERSON_TYPE,
                            OracleType = OracleType.Number,
                            IsNullable = true,
                        });
                        row = dbManager.ExecuteNonQuery(cmdPositionInsert);
                        if (row >= 0)
                        {
                            cmdPositionInsert.CommandText = "INSERT INTO M_COMPLAIN_REQUEST_FILE(" +
  "COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,CREATE_DATETIME,CREATE_BY,UPDATE_BY,UPDATE_DATETIME,REQUEST_TYPE) " +
  "SELECT :PERSON_TYPE COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,SYSDATE,CREATE_BY,UPDATE_BY,SYSDATE,REQUEST_TYPE FROM M_COMPLAIN_REQUEST_FILE" +
   " WHERE REQUEST_TYPE = 'VENDOR' AND COMPLAIN_TYPE_ID = 0";
                            cmdPositionInsert.Parameters.Clear();

                            cmdPositionInsert.Parameters.Add(new OracleParameter()
                            {
                                Direction = ParameterDirection.Input,
                                ParameterName = "PERSON_TYPE",
                                Value = PERSON_TYPE,
                                OracleType = OracleType.Number,
                                IsNullable = true,
                            });
                            dbManager.ExecuteNonQuery(cmdPositionInsert);
                            isRes = true;
                        }
                        
                    }
                    
                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region PositionUpdate
        public bool PositionUpdate(int PERSON_TYPE, string PERSON_TYPE_DESC, string CACTIVE, int? AGEMIN, int? AGEMAX)
        {
            try
            {
                cmdPositionUpdate.CommandText = @"UPDATE TEMPLOYEETYPES 
SET PERSON_TYPE_DESC = :PERSON_TYPE_DESC
,AGEMIN = :AGEMIN 
,AGEMAX = :AGEMAX 
,CACTIVE = :CACTIVE 
WHERE PERSON_TYPE = :PERSON_TYPE";
                cmdPositionUpdate.Parameters.Clear();
                cmdPositionUpdate.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE",
                    Value = PERSON_TYPE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdPositionUpdate.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE_DESC",
                    Value = PERSON_TYPE_DESC,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdPositionUpdate.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "AGEMIN",
                    Value = AGEMIN ?? (object)DBNull.Value,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdPositionUpdate.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "AGEMAX",
                    Value = AGEMAX ?? (object)DBNull.Value,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdPositionUpdate.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdPositionUpdate);
                if (row >= 0)
                {
                    cmdPositionUpdate.CommandText = "UPDATE M_REQUIRE_LEVEL_DATA " +
                      "SET DETAIL = :DETAIL " +
                      ",CACTIVE = :CACTIVE " +
                      "WHERE REALID = :REALID AND MREQUIRELEVELID = 3 AND PARENTID = 0";
                    cmdPositionUpdate.Parameters.Clear();
                    cmdPositionUpdate.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "REALID",
                        Value = PERSON_TYPE,
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdPositionUpdate.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "DETAIL",
                        Value = PERSON_TYPE_DESC,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    cmdPositionUpdate.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "CACTIVE",
                        Value = CACTIVE,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    row = dbManager.ExecuteNonQuery(cmdPositionUpdate);
                    if (row >= 0)
                    {
                        cmdPositionUpdate.CommandText = "UPDATE M_COMPLAIN_REQUIRE_FIELD " +
                      "SET ISACTIVE = :CACTIVE " +
                      "WHERE COMPLAIN_TYPE_ID = :PERSON_TYPE AND  FIELD_TYPE = 'VENDOR'";
                        cmdPositionUpdate.Parameters.Clear();
                        cmdPositionUpdate.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "PERSON_TYPE",
                            Value = PERSON_TYPE,
                            OracleType = OracleType.Number,
                            IsNullable = true,
                        });
                        cmdPositionUpdate.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "CACTIVE",
                            Value = CACTIVE,
                            OracleType = OracleType.VarChar,
                            IsNullable = true,
                        });
                        row = dbManager.ExecuteNonQuery(cmdPositionUpdate);
                        if(row >= 0){
                            cmdPositionUpdate.CommandText = "UPDATE M_COMPLAIN_REQUEST_FILE " +
                      "SET ISACTIVE = :CACTIVE " +
                      "WHERE COMPLAIN_TYPE_ID = :PERSON_TYPE AND  REQUEST_TYPE = 'VENDOR'";
                            cmdPositionUpdate.Parameters.Clear();
                            cmdPositionUpdate.Parameters.Add(new OracleParameter()
                            {
                                Direction = ParameterDirection.Input,
                                ParameterName = "PERSON_TYPE",
                                Value = PERSON_TYPE,
                                OracleType = OracleType.Number,
                                IsNullable = true,
                            });
                            cmdPositionUpdate.Parameters.Add(new OracleParameter()
                            {
                                Direction = ParameterDirection.Input,
                                ParameterName = "CACTIVE",
                                Value = CACTIVE,
                                OracleType = OracleType.VarChar,
                                IsNullable = true,
                            });
                            row = dbManager.ExecuteNonQuery(cmdPositionUpdate);
                        }
                        isRes = true;
                    }
                    
                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region + Instance +
        private static PositionDAL _instance;
        public static PositionDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new PositionDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
