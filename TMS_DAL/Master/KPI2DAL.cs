﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class KPI2DAL : OracleConnectionDAL
    {
        #region + Instance +
        private static KPI2DAL _instance;
        public static KPI2DAL Instance
        {
            get
            {
                _instance = new KPI2DAL();
                return _instance;
            }
        }
        #endregion

        #region Component
        public KPI2DAL()
        {
            InitializeComponent();
        }

        public KPI2DAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region KPIScoreSelect
        public DataSet KPIScoreSelect(string I_YEAR, string I_MONTH, string I_SVENDORID, string I_SCONTRACTID)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;
                cmdKPI.CommandText = @"USP_T_KPI_SCORE_SELECT";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_MONTH",
                    Value = I_MONTH,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });

                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = string.IsNullOrEmpty(I_SVENDORID) ? 0 : int.Parse(I_SVENDORID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = string.IsNullOrEmpty(I_SCONTRACTID) ? 0 : int.Parse(I_SCONTRACTID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CONTRACT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_KPI",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataSet ds = dbManager.ExecuteDataSet(cmdKPI);
                ds.Tables[0].TableName = "CONTRACT";
                ds.Tables[1].TableName = "KPI";

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MonthSelect
        public DataTable MonthSelect()
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;
                cmdKPI.CommandText = @"USP_M_MONTH_SELECT";
                #region Parameter
                cmdKPI.Parameters.Clear();
                
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataTable dt = dbManager.ExecuteDataTable(cmdKPI, "cmdKPI");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPIMappingFormSelect
        public DataSet KPIMappingFormSelect(string I_YEAR)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;
                cmdKPI.CommandText = @"USP_T_KPI_MAPPING_FORM_SELECT";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_MONTH",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_TOPIC",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_MAPPING",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataSet ds = dbManager.ExecuteDataSet(cmdKPI);
                ds.Tables[0].TableName = "MONTH";
                ds.Tables[1].TableName = "TOPIC";
                ds.Tables[2].TableName = "MAPPING";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckKPIMappingForm
        public DataTable CheckKPIMappingForm(string I_WHERE)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;

                #region USP_T_KPI_MAPPING_FORM_CHECK
                cmdKPI.CommandText = @"USP_T_KPI_MAPPING_FORM_CHECK";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_WHERE",
                    Value = I_WHERE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });


                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdKPI, "cmdKPI");
                dbManager.CommitTransaction();
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region SaveKPIMappingForm
        public DataTable SaveKPIMappingForm(string I_YEAR, string I_USERID, DataSet dsData, DataSet dsDataCheck)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;

                #region USP_T_KPI_MAPPING_FORM_SAVE
                cmdKPI.CommandText = @"USP_T_KPI_MAPPING_FORM_SAVE";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsData.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });

                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLECHECK",
                    Value = dsDataCheck.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });

                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdKPI, "cmdKPI");
                dbManager.CommitTransaction();
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region KPIStatus
        public DataTable KPIStatus(string I_TYPE)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;
                cmdKPI.CommandText = @"USP_M_STATUS_SELECT";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TYPE",
                    Value = I_TYPE,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });

                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataTable dt = dbManager.ExecuteDataTable(cmdKPI, "cmdKPI");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPIResultSelect
        public DataTable KPIResultSelect(string I_YEAR, string I_MONTH, string I_VENDORID, string I_CONTRACTID, string I_STATUS)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;
                cmdKPI.CommandText = @"USP_T_KPI_RESULT_SELECT";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_MONTH",
                    Value = string.IsNullOrEmpty(I_MONTH) ? 0 : int.Parse(I_MONTH),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDORID",
                    Value = I_VENDORID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACTID",
                    Value = string.IsNullOrEmpty(I_CONTRACTID) ? 0 : int.Parse(I_CONTRACTID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    Value = I_STATUS,
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataTable dt = dbManager.ExecuteDataTable(cmdKPI, "cmdKPI");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UpdateKPIStatus
        public bool UpdateKPIStatus(string I_USERID, int I_STATUS_ID,DataSet ds)
        {
            try
            {
                bool isRes = false;
                cmdKPI.CommandType = CommandType.StoredProcedure;

                #region USP_T_KPI_STATUS_UPDATE
                cmdKPI.CommandText = @"USP_T_KPI_STATUS_UPDATE";
                #region Parameter
                cmdKPI.Parameters.Clear();
                
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });

                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS_ID",
                    Value = I_STATUS_ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATA",
                    Value = ds.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                dbManager.ExecuteNonQuery(cmdKPI);
                dbManager.CommitTransaction();
                isRes = true;
                #endregion
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region KPISummarySelect
        public DataTable KPISummarySelect(string I_YEAR, string I_VENDORID, string I_CONTRACTID)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;
                cmdKPI.CommandText = @"USP_T_KPI_SUMMARY_SELECT";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDORID",
                    Value = I_VENDORID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACTID",
                    Value = string.IsNullOrEmpty(I_CONTRACTID) ? 0 : int.Parse(I_CONTRACTID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataTable dt = dbManager.ExecuteDataTable(cmdKPI, "cmdKPI");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region KPIIndexSelect
        public DataSet KPIIndexSelect(string I_YEAR)
        {
            try
            {
                cmdKPI.CommandType = CommandType.StoredProcedure;
                cmdKPI.CommandText = @"USP_T_KPI_INDEX_SELECT";
                #region Parameter
                cmdKPI.Parameters.Clear();
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });

                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_TOTAL",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdKPI.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataSet ds = dbManager.ExecuteDataSet(cmdKPI);
                ds.Tables[0].TableName = "TOTAL";
                ds.Tables[1].TableName = "DATA";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
