﻿namespace TMS_DAL.Master
{
    partial class ContractTiedDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractTiedDAL));
            this.cmdWorkGroupSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdGroupSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdMContractSelectByPagesize = new System.Data.OracleClient.OracleCommand();
            this.cmdMContractSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdMContractSave = new System.Data.OracleClient.OracleCommand();
            this.cmdMContractDelete = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdWorkGroupSelect
            // 
            this.cmdWorkGroupSelect.CommandText = "SELECT ID,NAME   FROM M_WORKGROUP WHERE CACTIVE = \'1\'";
            // 
            // cmdGroupSelect
            // 
            this.cmdGroupSelect.CommandText = "SELECT ID,NAME   FROM M_GROUPS WHERE CACTIVE = \'1\' AND WORKGROUPID = :WorkGroupID" +
    "";
            // 
            // cmdMContractSelectByPagesize
            // 
            this.cmdMContractSelectByPagesize.CommandText = "SELECT ID,NAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY ID DESC) AS RN , ID,NAME FR" +
    "OM M_CONTRACT MC WHERE CACTIVE IN (\'1\',\'2\') AND NAME LIKE :fillter) WHERE RN BET" +
    "WEEN :startindex AND :endindex";
            this.cmdMContractSelectByPagesize.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("fillter", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("startIndex", System.Data.OracleClient.OracleType.Int32),
            new System.Data.OracleClient.OracleParameter("endIndex", System.Data.OracleClient.OracleType.Int32)});
            // 
            // cmdMContractSelect
            // 
            this.cmdMContractSelect.CommandText = resources.GetString("cmdMContractSelect.CommandText");
            this.cmdMContractSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("WORKGROUPID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("GROUPSID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("SVENDORID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdMContractSave
            // 
            this.cmdMContractSave.CommandText = "USP_M_CONTRACT_SAVE";
            // 
            // cmdMContractDelete
            // 
            this.cmdMContractDelete.CommandText = "USP_M_CONTRACT_DELETE";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdWorkGroupSelect;
        private System.Data.OracleClient.OracleCommand cmdGroupSelect;
        private System.Data.OracleClient.OracleCommand cmdMContractSelectByPagesize;
        private System.Data.OracleClient.OracleCommand cmdMContractSelect;
        private System.Data.OracleClient.OracleCommand cmdMContractSave;
        private System.Data.OracleClient.OracleCommand cmdMContractDelete;
    }
}
