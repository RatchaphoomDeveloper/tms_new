﻿namespace TMS_DAL.Master
{
    partial class ComplainTypeDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComplainTypeDAL));
            this.cmdComplainTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainTypeSelectAll = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdComplainTypeSelect
            // 
            this.cmdComplainTypeSelect.CommandText = resources.GetString("cmdComplainTypeSelect.CommandText");
            // 
            // cmdComplainTypeSelectAll
            // 
            this.cmdComplainTypeSelectAll.CommandText = resources.GetString("cmdComplainTypeSelectAll.CommandText");

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdComplainTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdComplainTypeSelectAll;
    }
}
