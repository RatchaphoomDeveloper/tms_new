﻿namespace TMS_DAL.Master
{
    partial class VehicleDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehicleDAL));
            this.cmdloadTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdloadContrat = new System.Data.OracleClient.OracleCommand();
            this.cmdloadHeadCar = new System.Data.OracleClient.OracleCommand();
            this.cmdloadsemi = new System.Data.OracleClient.OracleCommand();
            this.cmdVehicleCar = new System.Data.OracleClient.OracleCommand();
            this.cmdinital = new System.Data.OracleClient.OracleCommand();
            this.cmdVehicleInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdgetVeh_text = new System.Data.OracleClient.OracleCommand();
            this.cmdclassgrp = new System.Data.OracleClient.OracleCommand();
            this.cmdVehicleSelect_Edit = new System.Data.OracleClient.OracleCommand();
            this.cmdVehicleEdit = new System.Data.OracleClient.OracleCommand();
            this.cmdHaveVehicle = new System.Data.OracleClient.OracleCommand();
            this.cmdloadSemiCarVehicle = new System.Data.OracleClient.OracleCommand();
            this.cmdVehicleChange = new System.Data.OracleClient.OracleCommand();
            this.cmdLoadHeadVehicle = new System.Data.OracleClient.OracleCommand();
            this.cmdLoadTruckVehicle = new System.Data.OracleClient.OracleCommand();
            this.cmdloadHeadVehicleCancel = new System.Data.OracleClient.OracleCommand();
            this.cmdLoadTruckVehicleCancel = new System.Data.OracleClient.OracleCommand();
            this.cmdLoadSemiCarVehicleCancel = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateVehicle = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateVehicle_DWaterExpired_Head = new System.Data.OracleClient.OracleCommand();
            this.cmdDocrunning = new System.Data.OracleClient.OracleCommand();
            this.cmdTblReqByTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdRequestRunning = new System.Data.OracleClient.OracleCommand();
            this.cmdClassGroupSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdloadTruck
            // 
            this.cmdloadTruck.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE CACTIVE =\'Y\' AND SCARTYPEID=\'0\'" +
    " AND VIEHICLE_TYPE=\'99\' AND STRANSPORTID=:STRANSPORTID AND NVL(SPOT_STATUS,0)=:I" +
    "_SPOT_STATUS";
            // 
            // cmdloadContrat
            // 
            this.cmdloadContrat.CommandText = "SELECT SCONTRACTID,SCONTRACTNO FROM TCONTRACT WHERE CACTIVE =\'Y\' AND SVENDORID=:v" +
    "endorid";
            this.cmdloadContrat.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter(":vendorid", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdloadHeadCar
            // 
            this.cmdloadHeadCar.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE CACTIVE =\'Y\' AND SCARTYPEID=\'3\'" +
    " AND VIEHICLE_TYPE=\'99\' AND STRANSPORTID=:STRANSPORTID AND NVL(SPOT_STATUS,0)=:I" +
    "_SPOT_STATUS";
            // 
            // cmdloadsemi
            // 
            this.cmdloadsemi.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE CACTIVE =\'Y\' AND SCARTYPEID=\'4\'" +
    " AND VIEHICLE_TYPE=\'99\' AND STRANSPORTID=:STRANSPORTID AND NVL(SPOT_STATUS,0)=:I" +
    "_SPOT_STATUS";
            // 
            // cmdVehicleCar
            // 
            this.cmdVehicleCar.CommandText = "SELECT config_value,config_name FROM C_CONFIG WHERE config_type = \'VICHICLE_CAR\' " +
    "and isactive = \'1\'";
            // 
            // cmdinital
            // 
            this.cmdinital.CommandText = "SELECT u.cgroup,t.scontractno FROM tuser u,tcontract t WHERE u.svendorid=t.svendo" +
    "rid AND u.suid=:UID";
            this.cmdinital.Connection = this.OracleConn;
            this.cmdinital.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter(":UID", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdVehicleInsert
            // 
            this.cmdVehicleInsert.CommandText = "UPS_M_VECHICLE_INSERT";
            this.cmdVehicleInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_RETRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("TYPEVALUE", System.Data.OracleClient.OracleType.Int32),
            new System.Data.OracleClient.OracleParameter("DATAHANDLE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_CLASS_GROUP", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdgetVeh_text
            // 
            this.cmdgetVeh_text.CommandText = "SELECT NCAPACITY,VEH_TEXT FROM TTRUCK_COMPART  WHERE NPANLEVEL = \'1\' AND STRUCKID" +
    "=:STRUCKID ";
            // 
            // cmdclassgrp
            // 
            this.cmdclassgrp.CommandText = "SELECT CLASSFGROUP,DESCRIPTION FROM VEH_CLASS_GROUP WHERE DESCRIPTION IS NOT NULL" +
    "";
            // 
            // cmdVehicleSelect_Edit
            // 
            this.cmdVehicleSelect_Edit.CommandText = resources.GetString("cmdVehicleSelect_Edit.CommandText");
            // 
            // cmdVehicleEdit
            // 
            this.cmdVehicleEdit.CommandText = "USP_M_VECHICLE_EDIT";
            this.cmdVehicleEdit.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_RETRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("TYPEVALUE", System.Data.OracleClient.OracleType.Int32),
            new System.Data.OracleClient.OracleParameter("DATAHANDLE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_SPOT_STATUS", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_CLASS_GROUP", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdHaveVehicle
            // 
            this.cmdHaveVehicle.CommandText = "SELECT COUNT(*) FROM TTRUCK WHERE VIEHICLE_TYPE IN(\'00\',\'34\') AND SHEADREGISTERNO" +
    "=:SHEADREGISTERNO";
            // 
            // cmdloadSemiCarVehicle
            // 
            this.cmdloadSemiCarVehicle.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE CACTIVE =\'Y\' AND VIEHICLE_TYPE=" +
    "\'99\' AND SCARTYPEID=\'4\' OR (STRANSPORTID=:STRANSPORTID AND SHEADREGISTERNO=:SHEA" +
    "DREGISTERNO)";
            // 
            // cmdVehicleChange
            // 
            this.cmdVehicleChange.CommandText = "USP_M_VECHICLE_CHANGE";
            this.cmdVehicleChange.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("DATAHANDLE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("O_RETRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdLoadHeadVehicle
            // 
            this.cmdLoadHeadVehicle.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE CACTIVE =\'Y\' AND SCARTYPEID=\'3\'" +
    " AND STRANSPORTID=:STRANSPORTID AND SHEADREGISTERNO=:SHEADREGISTERNO";
            // 
            // cmdLoadTruckVehicle
            // 
            this.cmdLoadTruckVehicle.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE CACTIVE =\'Y\' AND SCARTYPEID=\'0\'" +
    " AND STRANSPORTID=:STRANSPORTID AND SHEADREGISTERNO=:SHEADREGISTERNO";
            // 
            // cmdloadHeadVehicleCancel
            // 
            this.cmdloadHeadVehicleCancel.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE VIEHICLE_TYPE=\'34\' AND SCARTYPE" +
    "ID =\'3\' OR (STRANSPORTID   =:STRANSPORTID AND SHEADREGISTERNO=:SHEADREGISTERNO)";
            // 
            // cmdLoadTruckVehicleCancel
            // 
            this.cmdLoadTruckVehicleCancel.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE VIEHICLE_TYPE=\'00\' AND SCARTYPE" +
    "ID=\'0\' AND STRANSPORTID=:STRANSPORTID AND SHEADREGISTERNO=:SHEADREGISTERNO";
            // 
            // cmdLoadSemiCarVehicleCancel
            // 
            this.cmdLoadSemiCarVehicleCancel.CommandText = "SELECT STRUCKID,SHEADREGISTERNO FROM ttruck WHERE  VIEHICLE_TYPE=\'34\' AND SCARTYP" +
    "EID=\'4\' OR (STRANSPORTID=:STRANSPORTID AND SHEADREGISTERNO=:SHEADREGISTERNO)";
            // 
            // cmdUpdateVehicle
            // 
            this.cmdUpdateVehicle.CommandText = "USP_M_VECHICLE_STATUS";
            this.cmdUpdateVehicle.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("DATASTATUS", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("DATABLACKLIST", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("O_RETRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdUpdateVehicle_DWaterExpired_Head
            // 
            this.cmdUpdateVehicle_DWaterExpired_Head.CommandText = "USP_M_VECHICLE_DWATEREXPIRED";
            this.cmdUpdateVehicle_DWaterExpired_Head.Connection = this.OracleConn;
            this.cmdUpdateVehicle_DWaterExpired_Head.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_RETRUCK", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("P_STRUCKID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("P_ISCHANGE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdDocrunning
            // 
            this.cmdDocrunning.CommandText = "USP_M_DOCRUNNING";
            this.cmdDocrunning.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdTblReqByTruck
            // 
            this.cmdTblReqByTruck.CommandText = "USP_M_CHECKTBL_REQ_BY_TTRUCK";
            this.cmdTblReqByTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_ID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdRequestRunning
            // 
            this.cmdRequestRunning.CommandText = "USP_M_MV_GENERATE1";
            this.cmdRequestRunning.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("REQTYPE_ID1", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR_REQUEST_ID", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdloadTruck;
        private System.Data.OracleClient.OracleCommand cmdloadContrat;
        private System.Data.OracleClient.OracleCommand cmdloadHeadCar;
        private System.Data.OracleClient.OracleCommand cmdloadsemi;
        private System.Data.OracleClient.OracleCommand cmdVehicleCar;
        private System.Data.OracleClient.OracleCommand cmdinital;
        private System.Data.OracleClient.OracleCommand cmdVehicleInsert;
        private System.Data.OracleClient.OracleCommand cmdgetVeh_text;
        private System.Data.OracleClient.OracleCommand cmdclassgrp;
        private System.Data.OracleClient.OracleCommand cmdVehicleSelect_Edit;
        private System.Data.OracleClient.OracleCommand cmdVehicleEdit;
        private System.Data.OracleClient.OracleCommand cmdHaveVehicle;
        private System.Data.OracleClient.OracleCommand cmdloadSemiCarVehicle;
        private System.Data.OracleClient.OracleCommand cmdVehicleChange;
        private System.Data.OracleClient.OracleCommand cmdLoadHeadVehicle;
        private System.Data.OracleClient.OracleCommand cmdLoadTruckVehicle;
        private System.Data.OracleClient.OracleCommand cmdloadHeadVehicleCancel;
        private System.Data.OracleClient.OracleCommand cmdLoadTruckVehicleCancel;
        private System.Data.OracleClient.OracleCommand cmdLoadSemiCarVehicleCancel;
        private System.Data.OracleClient.OracleCommand cmdUpdateVehicle;
		private System.Data.OracleClient.OracleCommand cmdUpdateVehicle_DWaterExpired_Head;
        private System.Data.OracleClient.OracleCommand cmdDocrunning;
        private System.Data.OracleClient.OracleCommand cmdTblReqByTruck;
        private System.Data.OracleClient.OracleCommand cmdRequestRunning;
        private System.Data.OracleClient.OracleCommand cmdClassGroupSelect;
    }
}
