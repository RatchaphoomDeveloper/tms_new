﻿namespace TMS_DAL.Master
{
    partial class UserGroupDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdUserGroup = new System.Data.OracleClient.OracleCommand();
            this.cmdDepartment = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdUserGroup
            // 
            this.cmdUserGroup.CommandText = "SELECT * FROM M_USERGROUP WHERE IS_ACTIVE = 1";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdUserGroup;
        private System.Data.OracleClient.OracleCommand cmdDepartment;
    }
}
