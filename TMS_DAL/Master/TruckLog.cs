﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;

namespace TMS_DAL.Master
{
    public partial class TruckLog : OracleConnectionDAL
    {
        public TruckLog()
        {
            InitializeComponent();
        }

        public TruckLog(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #region SaveLog        
            public void SaveLog(DataTable DataLog)
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                try
                {
                    cmdTruckAdd.CommandText = @"INSERT INTO TTRUCK_LOG (TYPE,HEADDER,REGISNO,USER_ID,NEWDATA,OLDDATA) VALUES (:TYPE,:HEADDER,:REGISNO,:USER_ID,:NEWDATA,:OLDDATA)";
                    cmdTruckAdd.Parameters.Clear();
                    cmdTruckAdd.Parameters.Add(":TYPE", OracleType.VarChar).Value = "T";
                    cmdTruckAdd.Parameters.Add(":HEADDER", OracleType.VarChar).Value = DataLog.Rows[0]["HEADDER"].ToString();
                    cmdTruckAdd.Parameters.Add(":REGISNO", OracleType.VarChar).Value = DataLog.Rows[0]["REGISNO"].ToString();
                    cmdTruckAdd.Parameters.Add(":USER_ID", OracleType.VarChar).Value = DataLog.Rows[0]["USER_ID"].ToString();
                    cmdTruckAdd.Parameters.Add(":NEWDATA", OracleType.VarChar).Value = DataLog.Rows[0]["NEWDATA"].ToString();
                    cmdTruckAdd.Parameters.Add(":OLDDATA", OracleType.VarChar).Value = DataLog.Rows[0]["OLDDATA"].ToString();
                    //ประมวลผล
                    dbManager.ExecuteNonQuery(cmdTruckAdd);
                    dbManager.CommitTransaction();
                }
                catch (Exception ex)
                {

                    dbManager.RollbackTransaction();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    dbManager.Close();
                }
            }
        #endregion
        #region GetOldDataTruck
            public DataTable GetOldDataTruck(string regisno) 
            {
                cmdOldDataTruck.CommandText = @"SELECT * FROM TTRUCK_HIS WHERE SHEADREGISTERNO='" + regisno + "' ORDER BY NITEM DESC";
                    return dbManager.ExecuteDataTable(cmdOldDataTruck, "cmdOldDataTruck");
                 
            }

        #endregion
        #region GetOldDataTruckInsurance
            public DataTable GetOldDataTruckInsurance(string STTRUCKID)
        {
            cmdTruckInsurance.CommandText = @"SELECT SCOMPANY,START_DURATION,END_DURATION,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL FROM TTRUCK_INSURANCE_HIS WHERE STRUCKID='" + STTRUCKID + "' ORDER BY NITEM DESC";
            return dbManager.ExecuteDataTable(cmdTruckInsurance, "cmdTruckInsurance");

        }

        #endregion
        #region GetOldDataCapacity
        public DataTable GetOldDataCapacity(string STTRUCKID)
        {
            cmdOldCapacity.CommandText = @"SELECT NCOMPARTNO,NPANLEVEL,NCAPACITY FROM TTRUCK_COMPART_HIS WHERE
            NITEM =(SELECT MAX(NITEM) FROM TTRUCK_COMPART_HIS WHERE STRUCKID='" + STTRUCKID + "') AND STRUCKID='" + STTRUCKID + "' ORDER BY NCOMPARTNO";
            return dbManager.ExecuteDataTable(cmdOldCapacity, "cmdOldCapacity");

        }

        #endregion
        #region ค้นหาทะเบียนรถโดย ID
        public DataTable GetRegisNoByTruckId(string STTRUCKID)
        {
            cmdTruckByTruckId.CommandText = @"SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STTRUCKID + "'";
            return dbManager.ExecuteDataTable(cmdTruckByTruckId, "cmdTruckByTruckId");

        }

        #endregion
            #region + Instance +
            private static TruckLog _instance;
        public static TruckLog Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new TruckLog();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
