﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;
using System.Web.UI.WebControls;

namespace TMS_DAL.Master
{

    public partial class VehicleDAL : OracleConnectionDAL
    {

        public VehicleDAL()
        {
            InitializeComponent();
        }

        public VehicleDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #region LoadData
        
        
        //โหลดข้อมูล
        public DataTable InitalDataInital()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdinital, "cmdinital");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable InitalDataHeadCar(string vendorId, string SpotStatus)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdloadHeadCar.Parameters.Clear();
                cmdloadHeadCar.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = vendorId;

                cmdloadHeadCar.Parameters.Add(":I_SPOT_STATUS", OracleType.VarChar).Value = SpotStatus;

                return dbManager.ExecuteDataTable(cmdloadHeadCar, "cmdloadHeadCar");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable InitalSelectEdit(string STRUCKID)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdVehicleSelect_Edit.Parameters.Clear();
                cmdVehicleSelect_Edit.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                return dbManager.ExecuteDataTable(cmdVehicleSelect_Edit, "cmdVehicleSelect_Edit");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
            

        }

        //โหลดสัญญา
        public DataTable InitalDataContrat(string CGROUP, string vendorId, string SpotStatus)
        {
            string Strsql = "";
            try
            {
                string tmp = string.Empty;
                if (SpotStatus == "0")
                    tmp = "N";
                else
                    tmp = "Y";

                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                switch (CGROUP.ToString())
                {
                    case "1":
                        Strsql = @"SELECT SCONTRACTID,SCONTRACTNO FROM TCONTRACT WHERE CACTIVE ='Y' AND svendorid=:vendorid AND CSPACIALCONTRAC =:I_SPOT_STATUS";
                        cmdloadContrat.CommandText = Strsql;
                        cmdloadContrat.Parameters.Clear();
                        cmdloadContrat.Parameters.Add(":vendorid", OracleType.VarChar).Value = vendorId;
                        cmdloadContrat.Parameters.Add(":I_SPOT_STATUS", OracleType.VarChar).Value = tmp;

                        break;
                    case "3":
                        Strsql = @"SELECT SCONTRACTID,SCONTRACTNO FROM TCONTRACT WHERE CACTIVE ='Y' AND svendorid=:vendorid AND CSPACIALCONTRAC =:I_SPOT_STATUS";
                        cmdloadContrat.CommandText = Strsql;
                        cmdloadContrat.Parameters.Clear();
                        cmdloadContrat.Parameters.Add(":vendorid", OracleType.VarChar).Value = vendorId;
                        cmdloadContrat.Parameters.Add(":I_SPOT_STATUS", OracleType.VarChar).Value = tmp;
                        break;
                    default:
                        Strsql = @"SELECT SCONTRACTID,SCONTRACTNO FROM TCONTRACT WHERE CACTIVE ='Y' AND svendorid=:vendorid AND CSPACIALCONTRAC =:I_SPOT_STATUS";
                        cmdloadContrat.CommandText = Strsql;
                        cmdloadContrat.Parameters.Clear();
                        cmdloadContrat.Parameters.Add(":vendorid", OracleType.VarChar).Value = vendorId;
                        cmdloadContrat.Parameters.Add(":I_SPOT_STATUS", OracleType.VarChar).Value = tmp;
                        break;


                }
                return dbManager.ExecuteDataTable(cmdloadContrat, "cmdloadContrat");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        // เช็ตว่ารถคันนั้นเคยส่งเข้า Sap หรือไม่
        public DataTable HaveVehicle(string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdHaveVehicle.Parameters.Clear();
                cmdHaveVehicle.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdHaveVehicle, "cmdHaveVehicle");
            }
            catch (Exception ex)
            {
                throw new Exception();
                throw;
            }
            finally
            {
                dbManager.Close();
            }
            
        }
        //โหลดสัญญา
        public DataTable InitalDataTruck( string VendorId, string SpotStatus)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdloadTruck.Parameters.Clear();
                cmdloadTruck.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;

                cmdloadTruck.Parameters.Add(":I_SPOT_STATUS", OracleType.VarChar).Value = SpotStatus;

                return dbManager.ExecuteDataTable(cmdloadTruck, "cmdloadTruck");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        //โหลด Classgrp
        public DataTable InitalDataClassgrp()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdclassgrp, "cmdclassgrp");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable InitalDataSemi(string VendorId, string SpotStatus)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdloadsemi.Parameters.Clear();
                cmdloadsemi.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;

                cmdloadsemi.Parameters.Add(":I_SPOT_STATUS", OracleType.VarChar).Value = SpotStatus;

                return dbManager.ExecuteDataTable(cmdloadsemi, "cmdloadsemi");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable GENERATE_DOC_MVDAL(string I_DOC_ID, string I_SCREATE)
        {
            try
            {
                cmdDocrunning.CommandType = CommandType.StoredProcedure;
                cmdDocrunning.Parameters["I_DOC_ID"].Value = I_DOC_ID;
                cmdDocrunning.Parameters["I_SCREATE"].Value = I_SCREATE;
                DataTable dt = dbManager.ExecuteDataTable(cmdDocrunning, "cmdDocrunning");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable CHECKTBL_REQ_BY_TTRUCKDAL(string I_ID)
        {
            try
            {
                cmdTblReqByTruck.CommandType = CommandType.StoredProcedure;
                cmdTblReqByTruck.Parameters["I_ID"].Value = I_ID;
                DataTable dt = dbManager.ExecuteDataTable(cmdTblReqByTruck, "cmdTblReqByTruck");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        //โหลดรถเดี่ยวMap แล้วมาแสดง
        public DataTable InitalDataTruckVehicle(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdLoadTruckVehicle.Parameters.Clear();
                cmdLoadTruckVehicle.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;
                cmdLoadTruckVehicle.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdLoadTruckVehicle, "cmdLoadTruckVehicle");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        //โหลดหัวMap แล้วมาแสดง
        public DataTable InitalDataHeadVehicle(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdLoadHeadVehicle.Parameters.Clear();
                cmdLoadHeadVehicle.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;
                cmdLoadHeadVehicle.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdLoadHeadVehicle, "cmdLoadHeadVehicle");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        //โหลดหางลากมาสสลับหางลาก
        public DataTable InitalDataSemiVehicle(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdloadSemiCarVehicle.Parameters.Clear();
                cmdloadSemiCarVehicle.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;
                cmdloadSemiCarVehicle.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdloadSemiCarVehicle, "cmdloadSemiCarVehicle");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #region โหลดที่ยกเลิกการแมพรถมาแสดง
        //โหลดรถเดี่ยวMap แล้วมาแสดง
        public DataTable InitalDataTruckVehicleCancel(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdLoadTruckVehicleCancel.Parameters.Clear();
                cmdLoadTruckVehicleCancel.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;
                cmdLoadTruckVehicleCancel.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdLoadTruckVehicleCancel, "cmdLoadTruckVehicle");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        //โหลดหัวMap แล้วมาแสดง
        public DataTable InitalDataHeadVehicleCancel(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdloadHeadVehicleCancel.Parameters.Clear();
                cmdloadHeadVehicleCancel.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;
                cmdloadHeadVehicleCancel.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdloadHeadVehicleCancel, "cmdloadHeadVehicleCancel");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        //โหลดหางลากมาสสลับหางลาก
        public DataTable InitalDataSemiVehicleCancel(string VendorId, string SHEADREGISTERNO)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdLoadSemiCarVehicleCancel.Parameters.Clear();
                cmdLoadSemiCarVehicleCancel.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = VendorId;
                cmdLoadSemiCarVehicleCancel.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = SHEADREGISTERNO;
                return dbManager.ExecuteDataTable(cmdLoadSemiCarVehicleCancel, "cmdLoadSemiCarVehicleCancel");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        public DataTable InitalDataVehicleCar()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdVehicleCar, "cmdVehicleCar");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable GetVeh_text(string STTRUCKID)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdgetVeh_text.Parameters.Clear();
                cmdgetVeh_text.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STTRUCKID;
                return dbManager.ExecuteDataTable(cmdgetVeh_text, "cmdgetVeh_text");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                throw;
            }
            finally
            {
                dbManager.Close();
            }
            
        }

        public DataTable GENERATE_REQ_MVDAL(string ReqType_ID)
        {
            try
            {
                cmdRequestRunning.CommandType = CommandType.StoredProcedure;
                cmdRequestRunning.Parameters["REQTYPE_ID1"].Value = ReqType_ID;
                DataTable dt = dbManager.ExecuteDataTable(cmdRequestRunning, "cmdRequestRunning");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion


        #region SaveData
        public DataTable VehicleInsert(string Cartype, DataTable DataToDatatable, string SpotStatus, string ClassGroup, int ContractID)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();

                cmdVehicleInsert.CommandType = CommandType.StoredProcedure;

                DataSet ds = new DataSet("ds");
                DataToDatatable.TableName = "dt";
                ds.Tables.Add(DataToDatatable);
                cmdVehicleInsert.Parameters["TYPEVALUE"].Value = int.Parse(Cartype);
                cmdVehicleInsert.Parameters["DATAHANDLE"].Value = ds.GetXml();

                cmdVehicleInsert.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                if (string.Equals(ClassGroup, string.Empty))
                    cmdVehicleInsert.Parameters["I_CLASS_GROUP"].Value = DBNull.Value;
                else
                    cmdVehicleInsert.Parameters["I_CLASS_GROUP"].Value = ClassGroup;

                cmdVehicleInsert.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVehicleInsert, "cmdVehicleInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region ChANGEVehicle
        public DataTable VehicleChange(string Cartype, DataTable DataToDatatable)
        {
            try
            {
				string typecar = DataToDatatable.Rows[0].Field<string>("typecar");
				string struckid = DataToDatatable.Rows[0].Field<string>("STRUCKID");

                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();

                cmdVehicleChange.CommandType = CommandType.StoredProcedure;

                DataSet ds = new DataSet("ds");
                DataToDatatable.TableName = "dt";
                ds.Tables.Add(DataToDatatable);
                cmdVehicleChange.Parameters["DATAHANDLE"].Value = ds.GetXml();
                DataTable dt = dbManager.ExecuteDataTable(cmdVehicleChange, "cmdVehicleChange");

				/*UPDATE Set null to DWATEREXPIRE on TTRUCK Head*/
				if(typecar == "1")
				{
					cmdUpdateVehicle_DWaterExpired_Head.CommandType = CommandType.StoredProcedure;
					cmdUpdateVehicle_DWaterExpired_Head.Parameters["P_STRUCKID"].Value = struckid;
					cmdUpdateVehicle_DWaterExpired_Head.Parameters["P_ISCHANGE"].Value = 1;
				 	int result = dbManager.ExecuteNonQuery(cmdUpdateVehicle_DWaterExpired_Head);
				}

                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region EditData
        public DataTable VehicleEdit(string Cartype, DataTable DataToDatatable, string SpotStatus, string ClassGroup, int ContractID)
        {
            try
            {
				string typecar = DataToDatatable.Rows[0].Field<string>("typecar");
				string struckid = DataToDatatable.Rows[0].Field<string>("STRUCKID");
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();

                cmdVehicleEdit.CommandType = CommandType.StoredProcedure;

                DataSet ds = new DataSet("ds");
                DataToDatatable.TableName = "dt";
                ds.Tables.Add(DataToDatatable);
                cmdVehicleEdit.Parameters["TYPEVALUE"].Value = int.Parse(Cartype);
                cmdVehicleEdit.Parameters["DATAHANDLE"].Value = ds.GetXml();

                cmdVehicleEdit.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                if (string.Equals(ClassGroup, string.Empty))
                    cmdVehicleEdit.Parameters["I_CLASS_GROUP"].Value = DBNull.Value;
                else
                    cmdVehicleEdit.Parameters["I_CLASS_GROUP"].Value = ClassGroup;

                cmdVehicleEdit.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVehicleEdit, "cmdVehicleEdit");
				/*UPDATE Set null to DWATEREXPIRE on TTRUCK Head*/
				if(typecar == "1")
				{
					cmdUpdateVehicle_DWaterExpired_Head.CommandType = CommandType.StoredProcedure;
					cmdUpdateVehicle_DWaterExpired_Head.Parameters["P_STRUCKID"].Value = struckid;
					cmdUpdateVehicle_DWaterExpired_Head.Parameters["P_ISCHANGE"].Value = 0;
					int result = dbManager.ExecuteNonQuery(cmdUpdateVehicle_DWaterExpired_Head);
				}
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region SetStatus
        public DataTable SetStatusTms(DataTable Datastatus, DataTable DataBlacklist)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();

                cmdUpdateVehicle.CommandType = CommandType.StoredProcedure;

                DataSet ds = new DataSet("ds");
                Datastatus.TableName = "dt";
                ds.Tables.Add(Datastatus);
                //เอกสารประกัน
                DataSet doc_ds = new DataSet("blacklist_ds");
                DataBlacklist.TableName = "blacklist";
                doc_ds.Tables.Add(DataBlacklist);
                cmdUpdateVehicle.Parameters["DATABLACKLIST"].Value = doc_ds.GetXml();
                cmdUpdateVehicle.Parameters["DATASTATUS"].Value = ds.GetXml();
                DataTable dt = dbManager.ExecuteDataTable(cmdUpdateVehicle, "cmdUpdateVehicle");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message) ;
            }
            finally
            {
                dbManager.Close();
            }
            
        }
        #endregion
        #region + Instance +
        private static VehicleDAL _instance;
        public static VehicleDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new VehicleDAL();
                //}
                return _instance;
            }
        }
        #endregion

        public DataTable ClassGroupSelectDAL(string Condition)
        {
            try
            {
                cmdClassGroupSelect.CommandText = "SELECT * FROM M_CLASS_GROUP WHERE 1=1";

                cmdClassGroupSelect.CommandText += Condition;

                DataTable dt = dbManager.ExecuteDataTable(cmdClassGroupSelect, "cmdClassGroupSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
   
}
