﻿namespace TMS_DAL.Master
{
    partial class ProductTagDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdProductTagHeadInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdProductTagHeadSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdProductTagItemInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdProductTagItemSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdProductTagHeadInsert
            // 
            this.cmdProductTagHeadInsert.Connection = this.OracleConn;
            this.cmdProductTagHeadInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_TAG_LVL1_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_KM_TAG_LVL1_NAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdProductTagHeadSelect
            // 
            this.cmdProductTagHeadSelect.CommandText = "SELECT * FROM VW_M_KM_TAG_LVL1_SELECT WHERE 1=1 ";
            // 
            // cmdProductTagItemInsert
            // 
            this.cmdProductTagItemInsert.Connection = this.OracleConn;
            this.cmdProductTagItemInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_TAG_LVL2_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_KM_TAG_LVL2_NAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_KM_TAG_LVL1_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdProductTagItemSelect
            // 
            this.cmdProductTagItemSelect.CommandText = "SELECT * FROM VW_M_KM_TAG_LVL2_SELECT WHERE 1=1 ";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdProductTagHeadInsert;
        private System.Data.OracleClient.OracleCommand cmdProductTagHeadSelect;
        private System.Data.OracleClient.OracleCommand cmdProductTagItemInsert;
        private System.Data.OracleClient.OracleCommand cmdProductTagItemSelect;
    }
}
