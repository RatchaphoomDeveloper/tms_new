﻿namespace TMS_DAL.Master
{
    partial class PositionDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdPositionSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdPositionInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdPositionUpdate = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdPositionUpdate
            // 
            this.cmdPositionUpdate.CommandText = "UPDATE TEMPLOYEETYPES SET PERSON_TYPE_DESC = :PERSON_TYPE_DESC,CACTIVE = :CACTIVE" +
    " WHERE PERSON_TYPE = :PERSON_TYPE";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdPositionSelect;
        private System.Data.OracleClient.OracleCommand cmdPositionInsert;
        private System.Data.OracleClient.OracleCommand cmdPositionUpdate;
    }
}
