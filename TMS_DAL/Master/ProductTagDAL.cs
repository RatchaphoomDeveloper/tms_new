﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class ProductTagDAL : OracleConnectionDAL
    {
        public ProductTagDAL()
        {
            InitializeComponent();
        }

        public ProductTagDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable ProductTagHeadSelectDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdProductTagHeadSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdProductTagHeadSelect, "cmdProductTagHeadSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ProductTagItemSelectDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdProductTagItemSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdProductTagItemSelect, "cmdProductTagItemSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ProductTagHeadInsertDAL(int KM_TAG_LVL1_ID, string KM_TAG_LVL1_NAME, int CACTIVE, int CREATER)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdProductTagHeadInsert.CommandText = "USP_M_KM_TAG_LVL1_INSERT";
                cmdProductTagHeadInsert.CommandType = CommandType.StoredProcedure;

                cmdProductTagHeadInsert.Parameters["I_KM_TAG_LVL1_ID"].Value = KM_TAG_LVL1_ID;
                cmdProductTagHeadInsert.Parameters["I_KM_TAG_LVL1_NAME"].Value = KM_TAG_LVL1_NAME;
                cmdProductTagHeadInsert.Parameters["I_CACTIVE"].Value = CACTIVE;
                cmdProductTagHeadInsert.Parameters["I_CREATER"].Value = CREATER;

                dt = dbManager.ExecuteDataTable(cmdProductTagHeadInsert, "cmdProductTagHeadInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ProductTagItemInsertDAL(int KM_TAG_LVL2_ID, string KM_TAG_LVL2_NAME,int KM_TAG_LVL1_ID, int CACTIVE, int CREATER)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdProductTagItemInsert.CommandText = "USP_M_KM_TAG_LVL2_INSERT";
                cmdProductTagItemInsert.CommandType = CommandType.StoredProcedure;

                cmdProductTagItemInsert.Parameters["I_KM_TAG_LVL2_ID"].Value = KM_TAG_LVL2_ID;
                cmdProductTagItemInsert.Parameters["I_KM_TAG_LVL2_NAME"].Value = KM_TAG_LVL2_NAME;
                cmdProductTagItemInsert.Parameters["I_KM_TAG_LVL1_ID"].Value = KM_TAG_LVL1_ID;
                cmdProductTagItemInsert.Parameters["I_CACTIVE"].Value = CACTIVE;
                cmdProductTagItemInsert.Parameters["I_CREATER"].Value = CREATER;

                dt = dbManager.ExecuteDataTable(cmdProductTagItemInsert, "cmdProductTagItemInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static ProductTagDAL _instance;
        public static ProductTagDAL Instance
        {
            get
            {
                _instance = new ProductTagDAL();
                return _instance;
            }
        }
        #endregion
    }
}
