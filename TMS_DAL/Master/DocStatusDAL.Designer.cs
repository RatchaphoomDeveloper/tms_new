﻿namespace TMS_DAL.Master
{
    partial class DocStatusDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdDocStatusSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdDocStatusSelect
            // 
            this.cmdDocStatusSelect.CommandText = "SELECT DOC_STATUS_ID, DOC_STATUS_NAME FROM M_DOC_STATUS WHERE ISACTIVE = 1 ORDER " +
    "BY ROW_ORDER";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdDocStatusSelect;
    }
}
