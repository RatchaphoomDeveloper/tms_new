﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class MenuDAL : OracleConnectionDAL
    {
        #region MenuDAL
        public MenuDAL()
        {
            InitializeComponent();
        }

        public MenuDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region MenuSelect
        public DataTable MenuSelect(string PRM)
        {
            try
            {
                cmdMenu.CommandText = @"SELECT SMENUID
                                          ,SMENUORDER
                                          ,SMENUNAME
                                          ,SMENULINK
                                          ,PRMSDISABLE
                                          ,PRMSVIEW
                                          ,PRMSEDIT
                                      FROM TMENU TM  
                                      WHERE  ( PRMSDISABLE  LIKE '%'|| :PRM ||'%') AND SMENUHEAD IS NULL  AND CACTIVE = '1'  
                                      ORDER BY SMENUORDER";
                cmdMenu.Parameters.Clear();
                cmdMenu.Parameters.Add(new OracleParameter() {ParameterName = "PRM", Value = PRM });
                return dbManager.ExecuteDataTable(cmdMenu, "cmdMenu");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable MenuSelect2(string cGroup)
        {
            try
            {
                cmdMenu.CommandText = @"SELECT DISTINCT MM.MENU_ID 
                                          ,SMENUORDER
                                          ,MM.MENU_NAME
                                          ,MM.MENU_URL
                                     FROM TMENU TM                                     
                                      JOIN M_MENU MM ON UPPER(TM.SMENULINK) = UPPER(MM.MENU_URL)
                                      JOIN M_AUTHORIZATION MA ON MA.MENU_ID = MM.MENU_ID AND MA.VISIBLE = 1 AND MA.USERGROUP_ID = ''|| :CGroup ||''
                                      AND SMENUHEAD IS NULL  AND CACTIVE = '1'  
                                      ORDER BY SMENUORDER,MM.MENU_ID  ";
                cmdMenu.Parameters.Clear();
                cmdMenu.Parameters.Add(new OracleParameter() { ParameterName = "CGroup", Value = cGroup });
                //cmdMenu.Parameters.Add(new OracleParameter() { ParameterName = "PRM", Value = PRM }); //WHERE  ( PRMSDISABLE  LIKE '%'|| :PRM ||'%') 

                return dbManager.ExecuteDataTable(cmdMenu, "cmdMenu");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static MenuDAL _instance;
        public static MenuDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new MenuDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
