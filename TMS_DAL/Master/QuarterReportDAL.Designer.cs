﻿namespace TMS_DAL.Master
{
    partial class QuarterReportDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdGetData = new System.Data.OracleClient.OracleCommand();
            this.cmdGetVendorContract = new System.Data.OracleClient.OracleCommand();
            this.cmdQuarterReport = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdGetData
            // 
            this.cmdGetData.CommandText = "SELECT  V.SABBREVIATION, C.SCONTRACTNO FROM TCONTRACT C ,TVENDOR V WHERE C.SVENDO" +
    "RID = V.SVENDORID AND C.CACTIVE=\'Y\'  AND V.INUSE=\'1\' ORDER BY V.SABBREVIATION";
            // 
            // cmdGetVendorContract
            // 
            this.cmdGetVendorContract.CommandText = "SELECT  V.SABBREVIATION, C.SCONTRACTNO FROM TCONTRACT C ,TVENDOR V WHERE C.SVENDO" +
    "RID = V.SVENDORID AND C.CACTIVE=\'Y\'  AND V.INUSE=\'1\' AND V.SVENDORID=\'0010001356" +
    "\' ORDER BY V.SABBREVIATION";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdGetData;
        private System.Data.OracleClient.OracleCommand cmdGetVendorContract;
        private System.Data.OracleClient.OracleCommand cmdQuarterReport;
    }
}
