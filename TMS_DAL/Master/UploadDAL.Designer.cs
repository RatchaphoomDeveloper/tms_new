﻿namespace TMS_DAL.Master
{
    partial class UploadDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadDAL));
            this.cmdUploadRequestSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUpload2Select = new System.Data.OracleClient.OracleCommand();
            this.cmdUpload2Add = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdUploadRequestSelect
            // 
            this.cmdUploadRequestSelect.CommandText = resources.GetString("cmdUploadRequestSelect.CommandText");
            this.cmdUploadRequestSelect.Connection = this.OracleConn;
            // 
            // cmdUploadAdd
            // 
            this.cmdUploadAdd.CommandText = resources.GetString("cmdUploadAdd.CommandText");
            this.cmdUploadAdd.Connection = this.OracleConn;
            this.cmdUploadAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("TYPE_DOCUMENT", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdUploadDelete
            // 
            this.cmdUploadDelete.CommandText = "DELETE FROM F_UPLOAD WHERE REF_STR =: I_DOC_ID AND F_UPLOAD.UPLOAD_ID IN (SELECT " +
    "UPLOAD_ID FROM M_UPLOAD_TYPE WHERE UPLOAD_TYPE =: I_UPLOAD_TYPE)";
            this.cmdUploadDelete.Connection = this.OracleConn;
            this.cmdUploadDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, "255")});
            // 
            // cmdUploadSelect
            // 
            this.cmdUploadSelect.CommandText = resources.GetString("cmdUploadSelect.CommandText");
            this.cmdUploadSelect.Connection = this.OracleConn;
            this.cmdUploadSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_REF_STR", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdUpload2Select
            // 
            this.cmdUpload2Select.CommandText = resources.GetString("cmdUpload2Select.CommandText");
            this.cmdUpload2Select.Connection = this.OracleConn;
            this.cmdUpload2Select.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_REF_STR", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdUpload2Add
            // 
            this.cmdUpload2Add.CommandText = resources.GetString("cmdUpload2Add.CommandText");
            this.cmdUpload2Add.Connection = this.OracleConn;
            this.cmdUpload2Add.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("TYPE_DOCUMENT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("DOC_NUMBER", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("START_DATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("STOP_DATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("YEAR", System.Data.OracleClient.OracleType.VarChar)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdUploadRequestSelect;
        private System.Data.OracleClient.OracleCommand cmdUploadAdd;
        private System.Data.OracleClient.OracleCommand cmdUploadDelete;
        private System.Data.OracleClient.OracleCommand cmdUploadSelect;
        private System.Data.OracleClient.OracleCommand cmdUpload2Select;
        private System.Data.OracleClient.OracleCommand cmdUpload2Add;



    }
}
