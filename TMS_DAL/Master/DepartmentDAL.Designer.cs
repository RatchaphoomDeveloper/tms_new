﻿namespace TMS_DAL.Master
{
    partial class DepartmentDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdDepartmentSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdDepartmentInsert = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdDepartmentSelect
            // 
            this.cmdDepartmentSelect.CommandText = "SELECT * FROM VW_M_DEPARTMENT_SELECT WHERE 1=1 ";
            // 
            // cmdDepartmentInsert
            // 
            this.cmdDepartmentInsert.Connection = this.OracleConn;
            this.cmdDepartmentInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DEPARTMENT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DEPARTMENT_NAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdDepartmentSelect;
        private System.Data.OracleClient.OracleCommand cmdDepartmentInsert;
    }
}
