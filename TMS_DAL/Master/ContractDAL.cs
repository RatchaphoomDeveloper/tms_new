﻿using dbManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class ContractDAL : OracleConnectionDAL
    {
        #region ContractDAL
        public ContractDAL()
        {
            InitializeComponent();
        }

        public ContractDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region TTERMINALSelect
        public DataTable TTERMINALSelect()
        {

            try
            {

                return dbManager.ExecuteDataTable(cmdTTERMINALSelect, "cmdTTERMINALSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region TransportTypeSelect
        public DataTable TransportTypeSelect()
        {

            try
            {
                return dbManager.ExecuteDataTable(cmdTransportTypeSelect, "cmdTransportTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region ContractTypeSelect
        public DataTable ContractTypeSelect()
        {

            try
            {
                return dbManager.ExecuteDataTable(cmdContractTypeSelect, "cmdContractTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region ProductTypeSelect
        public DataTable ProductTypeSelect()
        {

            try
            {
                return dbManager.ExecuteDataTable(cmdProductTypeSelect, "cmdProductTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region GUARANTEETYPESelect
        public DataTable GUARANTEETYPESelect()
        {

            try
            {
                return dbManager.ExecuteDataTable(cmdGUARANTEETYPESelect, "cmdGUARANTEETYPESelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region VEHSelectByPagesize
        public DataTable VEHSelectByPagesize(string fillter, int startIndex, int endIndex, string STRANSPORTID)
        {
            try
            {
                cmdVEHSelectByPagesize.CommandText = @"SELECT  * FROM (  SELECT ROW_NUMBER()OVER(ORDER BY STRUCKID) AS RN , VEH.* FROM ( SELECT DISTINCT VEH.STRUCKID ,VEH.SHEADREGISTERNO VEH_NO ,VEH.STRAILERID , TU.SHEADREGISTERNO TU_NO  ,VEH.SCARTYPEID  ,CASE WHEN VEH.SCARTYPEID='0' THEN 'สิบล้อ' ELSE 'SEMI-TRAILER' END VEH_TYPE,VEH.stransportid  
  ,TCONTRACT.SCONTRACTNO,TVENDOR.SABBREVIATION,TCONTRACT.SCONTRACTID,TVENDOR.SVENDORID
  FROM TTRUCK VEH   
  LEFT JOIN TTRUCK TU ON VEH.STRAILERID = TU.STRUCKID 
  LEFT JOIN TTRUCKTYPE VEH_TYPE ON VEH.SCARTYPEID =VEH_TYPE.SCARTYPEID  
  LEFT JOIN TCONTRACT_TRUCK ON TCONTRACT_TRUCK.STRUCKID = VEH.STRUCKID  
  LEFT JOIN TCONTRACT ON TCONTRACT.SCONTRACTID = TCONTRACT_TRUCK.SCONTRACTID AND TCONTRACT.CACTIVE <> 'Y' 
  LEFT JOIN TVENDOR ON TVENDOR.SVENDORID = TCONTRACT.SVENDORID
  WHERE (TCONTRACT_TRUCK.SCONTRACTID IS NULL OR TCONTRACT.CACTIVE = 'N')  AND  VEH.SCARTYPEID IN('0','3')  AND VEH.STRUCKID ||''||VEH.SHEADREGISTERNO ||''||TU.SHEADREGISTERNO ||''||TCONTRACT.SCONTRACTNO ||''|| TVENDOR.SABBREVIATION LIKE :FILLTER AND (VEH.STRANSPORTID = :STRANSPORTID OR TRIM(:STRANSPORTID) IS NULL) AND VEH.LOGDATA = 1) VEH ) TBL WHERE RN BETWEEN :STARTINDEX AND :ENDINDEX";
                cmdVEHSelectByPagesize.Parameters["FILLTER"].Value = fillter;
                cmdVEHSelectByPagesize.Parameters["STARTINDEX"].Value = startIndex;
                cmdVEHSelectByPagesize.Parameters["ENDINDEX"].Value = endIndex;
                cmdVEHSelectByPagesize.Parameters["STRANSPORTID"].Value = STRANSPORTID;
                return dbManager.ExecuteDataTable(cmdVEHSelectByPagesize, "cmdVEHSelectByPagesize");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractSelectByPagesize
        public DataTable MContractSelectByPagesize(string fillter, int startIndex, int endIndex)
        {
            try
            {
                cmdMContractSelectByPagesize.Parameters["fillter"].Value = fillter;
                cmdMContractSelectByPagesize.Parameters["startIndex"].Value = startIndex;
                cmdMContractSelectByPagesize.Parameters["endIndex"].Value = endIndex;
                return dbManager.ExecuteDataTable(cmdMContractSelectByPagesize, "cmdTContractSelectByPagesize");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TUSelect
        public DataTable TUSelect(string VEH_ID)
        {
            try
            {
                cmdTUSelect.Parameters["VEH_ID"].Value = VEH_ID;
                return dbManager.ExecuteDataTable(cmdTUSelect, "cmdTUSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelect
        public DataSet ContractSelect(string I_CONTRACT_ID)
        {
            cmdCONTRACTSelect.CommandType = CommandType.StoredProcedure;
            cmdCONTRACTSelect.Parameters["I_CONTRACT_ID"].Value = I_CONTRACT_ID;

            DataSet ds = new DataSet("CONTRACT");
            ds = dbManager.ExecuteDataSet(cmdCONTRACTSelect);
            ds.Tables[0].TableName = "GUARANTEE";
            ds.Tables[1].TableName = "INFORMATION";
            ds.Tables[2].TableName = "PLANT";
            ds.Tables[3].TableName = "GPRODUCT";
            ds.Tables[4].TableName = "TRUCK";
            ds.Tables[5].TableName = "UPLOAD";

            return ds;
        }
        #endregion

        #region ContractListSelect
        public DataTable ContractListSelect(string IP_CONTRACTNO, string IP_VENDORID, string IP_CACTIVE)
        {
            try
            {
                cmdConTractListSelect.Parameters.Clear();
                cmdConTractListSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IP_CONTRACTNO",
                    Value = IP_CONTRACTNO,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConTractListSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IP_VENDORID",
                    Value = IP_VENDORID,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConTractListSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IP_CACTIVE",
                    Value = IP_CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdConTractListSelect, "cmdConTractListSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ContractListSelect2(string IP_CONTRACTNO, string IP_VENDORID, string IP_CACTIVE, string SpotStatus)
        {
            try
            {
                cmdConTractListSelect.Parameters.Clear();
                cmdConTractListSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IP_CONTRACTNO",
                    Value = IP_CONTRACTNO,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConTractListSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IP_VENDORID",
                    Value = IP_VENDORID,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConTractListSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "IP_CACTIVE",
                    Value = IP_CACTIVE,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });

                cmdConTractListSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SPOT_STATUS",
                    Value = SpotStatus,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataTable(cmdConTractListSelect, "cmdConTractListSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ConTractSelectByVendor
        public DataTable ConTractSelectByVendor(string SVENDORID = "", string CONTRACTNO = "")
        {
            try
            {
                cmdConTractSelectByVendor.CommandText = @"SELECT SCONTRACTID
                                  ,SCONTRACTNO,SVENDORID
                               FROM TCONTRACT WHERE CACTIVE='Y' AND (SVENDORID = :I_SVENDORID OR TRIM(:I_SVENDORID) IS NULL) AND (SCONTRACTNO like '%" + CONTRACTNO + "%' OR TRIM('" + CONTRACTNO + "') IS NULL)";
                cmdConTractSelectByVendor.Parameters.Clear();
                cmdConTractSelectByVendor.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = SVENDORID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                //cmdConTractSelectByVendor.Parameters.Add(new OracleParameter()
                //{
                //    Direction = ParameterDirection.Input,
                //    ParameterName = "I_CONTRACTID",
                //    Value = CONTRACTID,
                //    OracleType = OracleType.VarChar,
                //    IsNullable = true,
                //});

                return dbManager.ExecuteDataTable(cmdConTractSelectByVendor, "cmdConTractSelectByVendor");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ConTractDetailSelect
        public DataTable ConTractDetailSelect(string Condition = "")
        {
            string query = cmdContractDetail.CommandText;
            try
            {
                cmdContractDetail.CommandText += Condition;
                return dbManager.ExecuteDataTable(cmdContractDetail, "cmdContractDetail");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        #region ContractSave
        public DataTable ContractSave(string I_CONTRACTID, string I_MODIFY_TYPE,
string I_CONTRACTTYPE,
string I_CONTRACTNO,
string I_CONTRACTNONEW,
string I_VENDOR,
string I_STARTDATE,
string I_ENDDATE,
string I_CONTRACTTRUCK,
string I_REMARK,
string I_CACTIVE,
string I_USERID,
string I_PROCUREMENT,
string I_MAINPLANT,
string I_PLNT,
string I_RENEWFROM,
            string I_GROUPSID,
            string I_NTRUCKCONTRACT,
                                             string I_NTRUCKRESERVE,
DataTable I_GUARANTEE,
DataTable I_CONTRACTPLANT,
DataTable I_GPRODUCT,
DataTable I_TRUCK, DataTable dtUpload, string flag)
        {
            try
            {
                DataSet dsGUARANTEE = new DataSet("DS");
                DataTable dtI = I_GUARANTEE.Copy();
                dtI.TableName = "DT";
                dsGUARANTEE.Tables.Add(dtI);

                DataSet dsCONTRACTPLANT = new DataSet("DS");
                dtI = I_CONTRACTPLANT.Copy();
                dtI.TableName = "DT";
                dsCONTRACTPLANT.Tables.Add(dtI);

                DataSet dsGPRODUCT = new DataSet("DS");
                dtI = I_GPRODUCT.Copy();
                dtI.TableName = "DT";
                dsGPRODUCT.Tables.Add(dtI);

                DataSet dsTRUCK = new DataSet("DS");
                dtI = I_TRUCK.Copy();
                dtI.TableName = "DT";
                dsTRUCK.Tables.Add(dtI);
                dbManager.Open();
                dbManager.BeginTransaction();
                if (!string.IsNullOrEmpty(I_CONTRACTID))
                {
                    cmdImportFileDelete.Parameters["I_DOC_ID"].Value = I_CONTRACTID;
                    dbManager.ExecuteNonQuery(cmdImportFileDelete);
                }


                DataTable dt = null;
                if (flag.ToLower() == "new")
                {
                    cmdCONTRACTAdd.CommandType = CommandType.StoredProcedure;
                    cmdCONTRACTAdd.Parameters["I_CONTRACTID"].Value = I_CONTRACTID == string.Empty ? 0 : int.Parse(I_CONTRACTID);
                    cmdCONTRACTAdd.Parameters["I_MODIFY_TYPE"].Value = I_MODIFY_TYPE;
                    cmdCONTRACTAdd.Parameters["I_CONTRACTTYPE"].Value = I_CONTRACTTYPE;
                    cmdCONTRACTAdd.Parameters["I_CONTRACTNO"].Value = I_CONTRACTNO;
                    cmdCONTRACTAdd.Parameters["I_CONTRACTNONEW"].Value = I_CONTRACTNONEW;
                    cmdCONTRACTAdd.Parameters["I_VENDOR"].Value = I_VENDOR;
                    cmdCONTRACTAdd.Parameters["I_STARTDATE"].Value = I_STARTDATE;
                    cmdCONTRACTAdd.Parameters["I_ENDDATE"].Value = I_ENDDATE;
                    cmdCONTRACTAdd.Parameters["I_CONTRACTTRUCK"].Value = I_CONTRACTTRUCK;
                    cmdCONTRACTAdd.Parameters["I_REMARK"].Value = I_REMARK;
                    cmdCONTRACTAdd.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                    cmdCONTRACTAdd.Parameters["I_USERID"].Value = I_USERID;
                    cmdCONTRACTAdd.Parameters["I_PROCUREMENT"].Value = I_PROCUREMENT;
                    cmdCONTRACTAdd.Parameters["I_MAINPLANT"].Value = I_MAINPLANT;
                    cmdCONTRACTAdd.Parameters["I_PLNT"].Value = I_PLNT;
                    cmdCONTRACTAdd.Parameters["I_RENEWFROM"].Value = I_RENEWFROM;
                    cmdCONTRACTAdd.Parameters["I_GUARANTEE"].Value = dsGUARANTEE.GetXml();
                    cmdCONTRACTAdd.Parameters["I_CONTRACTPLANT"].Value = dsCONTRACTPLANT.GetXml();
                    cmdCONTRACTAdd.Parameters["I_GPRODUCT"].Value = dsGPRODUCT.GetXml();
                    cmdCONTRACTAdd.Parameters["I_TRUCK"].Value = dsTRUCK.GetXml();
                    cmdCONTRACTAdd.Parameters["I_GROUPSID"].Value = string.IsNullOrEmpty(I_GROUPSID) ? 0 : int.Parse(I_GROUPSID);
                    cmdCONTRACTAdd.Parameters["I_NTRUCKCONTRACT"].Value = I_NTRUCKCONTRACT;
                    cmdCONTRACTAdd.Parameters["I_NTRUCKRESERVE"].Value = I_NTRUCKRESERVE;
                    dt = dbManager.ExecuteDataTable(cmdCONTRACTAdd, "cmdCONTRACTAdd");
                }
                else
                {
                    cmdCONTRACTEdit.CommandType = CommandType.StoredProcedure;
                    cmdCONTRACTEdit.Parameters["I_CONTRACTID"].Value = I_CONTRACTID;
                    cmdCONTRACTEdit.Parameters["I_MODIFY_TYPE"].Value = I_MODIFY_TYPE;
                    cmdCONTRACTEdit.Parameters["I_CONTRACTTYPE"].Value = I_CONTRACTTYPE;
                    cmdCONTRACTEdit.Parameters["I_CONTRACTNO"].Value = I_CONTRACTNO;
                    cmdCONTRACTEdit.Parameters["I_CONTRACTNONEW"].Value = I_CONTRACTNONEW;
                    cmdCONTRACTEdit.Parameters["I_VENDOR"].Value = I_VENDOR;
                    cmdCONTRACTEdit.Parameters["I_STARTDATE"].Value = I_STARTDATE;
                    cmdCONTRACTEdit.Parameters["I_ENDDATE"].Value = I_ENDDATE;
                    cmdCONTRACTEdit.Parameters["I_CONTRACTTRUCK"].Value = I_CONTRACTTRUCK;
                    cmdCONTRACTEdit.Parameters["I_REMARK"].Value = I_REMARK;
                    cmdCONTRACTEdit.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                    cmdCONTRACTEdit.Parameters["I_USERID"].Value = I_USERID;
                    cmdCONTRACTEdit.Parameters["I_PROCUREMENT"].Value = I_PROCUREMENT;
                    cmdCONTRACTEdit.Parameters["I_MAINPLANT"].Value = I_MAINPLANT;
                    cmdCONTRACTEdit.Parameters["I_PLNT"].Value = I_PLNT;
                    cmdCONTRACTEdit.Parameters["I_RENEWFROM"].Value = I_RENEWFROM;
                    cmdCONTRACTEdit.Parameters["I_GUARANTEE"].Value = dsGUARANTEE.GetXml();
                    cmdCONTRACTEdit.Parameters["I_CONTRACTPLANT"].Value = dsCONTRACTPLANT.GetXml();
                    cmdCONTRACTEdit.Parameters["I_GPRODUCT"].Value = dsGPRODUCT.GetXml();
                    cmdCONTRACTEdit.Parameters["I_TRUCK"].Value = dsTRUCK.GetXml();
                    cmdCONTRACTEdit.Parameters["I_GROUPSID"].Value = string.IsNullOrEmpty(I_GROUPSID) ? 0 : int.Parse(I_GROUPSID);
                    cmdCONTRACTEdit.Parameters["I_NTRUCKCONTRACT"].Value = I_NTRUCKCONTRACT;
                    cmdCONTRACTEdit.Parameters["I_NTRUCKRESERVE"].Value = I_NTRUCKRESERVE;
                    dt = dbManager.ExecuteDataTable(cmdCONTRACTEdit, "cmdCONTRACTEdit");
                }
                SaveAttachFile(ref dt, ref dbManager, dtUpload, I_USERID);
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }

        }
        #endregion

        #region SaveAttachFile
        private void SaveAttachFile(ref DataTable dt, ref DBManager dbManager, DataTable dtUpload, string I_USERID)
        {
            if (dt.Rows[0]["Message"] == DBNull.Value)
            {
                string CONTRACTID = dt.Rows[0]["CONTRACTID"].ToString();
                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdCONTRACTAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdCONTRACTAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdCONTRACTAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdCONTRACTAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdCONTRACTAttachInsert.Parameters["REF_INT"].Value = 0;
                    cmdCONTRACTAttachInsert.Parameters["REF_STR"].Value = CONTRACTID;
                    cmdCONTRACTAttachInsert.Parameters["ISACTIVE"].Value = "1";
                    cmdCONTRACTAttachInsert.Parameters["CREATE_BY"].Value = I_USERID;

                    dbManager.ExecuteNonQuery(cmdCONTRACTAttachInsert);
                }
            }
        }
        #endregion

        #region CheckTruckInContract
        public bool CheckTruckInContract(string struckID)
        {
            bool returnValue = false;
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();

                cmdCheckTruckInContract.CommandText = "FC_CHECK_TRUCK_INCONTRACT";
                cmdCheckTruckInContract.CommandType = CommandType.StoredProcedure;
                cmdCheckTruckInContract.Parameters["I_STRUCKID"].Value = struckID;
                dbManager.ExecuteNonQuery(cmdCheckTruckInContract);
                int result = Convert.ToInt32(cmdCheckTruckInContract.Parameters["VRETURN"].Value);
                if (result >= 1)
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbManager.Close();
            }
            return returnValue;
        }
        #endregion

        public DataTable TruckContractExpireDAL()
        {

            try
            {
                DataTable dtTruck = new DataTable();

                cmdTruckContractExpire.CommandText = "SELECT * FROM VW_TRUCK_CONTRACT_EXPIRE WHERE 1=1";

                dtTruck = dbManager.ExecuteDataTable(cmdTruckContractExpire, "cmdTruckContractExpire");

                return dtTruck;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverContractExpireDAL()
        {

            try
            {
                DataTable dtDriver = new DataTable();

                cmdDriverContractExpire.CommandText = "SELECT * FROM VW_DRIVER_CONTRACT_EXPIRE WHERE 1=1";

                dtDriver = dbManager.ExecuteDataTable(cmdDriverContractExpire, "cmdDriverContractExpire");

                return dtDriver;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateContractExpireDAL()
        {
            try
            {
                dbManager.Open();
                cmdUpdateContractExpire.CommandText = @"UPDATE TCONTRACT
                                                        SET CACTIVE = 'N'
                                                        WHERE TCONTRACT.DEND < TRUNC(SYSDATE)";

                dbManager.ExecuteNonQuery(cmdUpdateContractExpire);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        public DataTable SpotDeliverySelectDAL(string Condition)
        {

            try
            {
                DataTable dtDelivery = new DataTable();

                cmdSpotDeliverySelect.CommandText = "SELECT * FROM VW_SPOT_DELIVERY_SELECT WHERE 1=1 ";
                cmdSpotDeliverySelect.CommandText += Condition;

                dtDelivery = dbManager.ExecuteDataTable(cmdSpotDeliverySelect, "cmdSpotDeliverySelect");

                return dtDelivery;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region AfterContractSelect
        public DataTable AfterContractSelect(string I_STRUCKID, string I_CONTRACT_ID)
        {
            cmdAfterCONTRACTSelect.CommandType = CommandType.StoredProcedure;
            cmdAfterCONTRACTSelect.Parameters["I_STRUCKID"].Value = I_STRUCKID;
            cmdAfterCONTRACTSelect.Parameters["I_SCONTRACTID"].Value = I_CONTRACT_ID;

            DataTable dt = dbManager.ExecuteDataTable(cmdAfterCONTRACTSelect, "cmdAfterCONTRACTSelect");

            return dt;
        }
        #endregion

        #region + Instance +
        private static ContractDAL _instance;
        public static ContractDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ContractDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
