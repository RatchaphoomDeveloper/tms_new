﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class ContractTiedDAL : OracleConnectionDAL
    {
        #region ContractTiedDAL
        public ContractTiedDAL()
        {
            InitializeComponent();
        }

        public ContractTiedDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region WorkGroupSelect
        public DataTable WorkGroupSelect()
        {

            try
            {
                return dbManager.ExecuteDataTable(cmdWorkGroupSelect, "cmdWorkGroupSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect(string WorkGroupID)
        {
            try
            {
                cmdGroupSelect.Parameters.Clear();
                cmdGroupSelect.Parameters.Add(new System.Data.OracleClient.OracleParameter() {
                    ParameterName = "WorkGroupID",
                    Value = string.IsNullOrEmpty(WorkGroupID) ? 0 : int.Parse(WorkGroupID),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                });
                return dbManager.ExecuteDataTable(cmdGroupSelect, "cmdGroupSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region MContractSelectByPagesize
        public DataTable MContractSelectByPagesize(string fillter, int startIndex, int endIndex)
        {
            try
            {
                cmdMContractSelectByPagesize.Parameters["fillter"].Value = fillter;
                cmdMContractSelectByPagesize.Parameters["startIndex"].Value = startIndex;
                cmdMContractSelectByPagesize.Parameters["endIndex"].Value = endIndex;
                return dbManager.ExecuteDataTable(cmdMContractSelectByPagesize, "cmdTContractSelectByPagesize");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractSelect
        public DataTable MContractSelect(string WORKGROUPID, string GROUPSID, string SVENDORID, string ID)
        {
            try
            {
                cmdMContractSelect.Parameters["WORKGROUPID"].Value = string.IsNullOrEmpty(WORKGROUPID) ? 0 : int.Parse(WORKGROUPID);
                cmdMContractSelect.Parameters["GROUPSID"].Value = string.IsNullOrEmpty(GROUPSID) ? 0 : int.Parse(GROUPSID);
                cmdMContractSelect.Parameters["SVENDORID"].Value = string.IsNullOrEmpty(SVENDORID) ? 0 : int.Parse(SVENDORID);
                cmdMContractSelect.Parameters["ID"].Value = string.IsNullOrEmpty(ID) ? 0 : int.Parse(ID);
                return dbManager.ExecuteDataTable(cmdMContractSelect, "cmdTContract");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractSave
        public DataTable MContractSave(string I_ID, string I_NAME, string I_GROUPSID, string I_SVENDORID)
        {
            try
            {
                cmdMContractSave.CommandType = CommandType.StoredProcedure;
                cmdMContractSave.Parameters.Clear();
                cmdMContractSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    ParameterName = "I_ID",
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                });
                cmdMContractSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    ParameterName = "I_NAME",
                    Value = I_NAME,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                });
                cmdMContractSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    ParameterName = "I_GROUPSID",
                    Value = string.IsNullOrEmpty(I_GROUPSID) ? 0 : int.Parse(I_GROUPSID),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                });
                cmdMContractSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                });
                cmdMContractSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    ParameterName = "O_CUR",
                    OracleType = System.Data.OracleClient.OracleType.Cursor,
                    Direction = ParameterDirection.Output
                });
                return dbManager.ExecuteDataTable(cmdMContractSave, "cmdMContractSave");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MContractDelete
        public DataTable MContractDelete(string I_ID)
        {
            try
            {
                cmdMContractDelete.CommandType = CommandType.StoredProcedure;
                cmdMContractDelete.Parameters.Clear();
                cmdMContractDelete.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    ParameterName = "I_ID",
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                    OracleType = System.Data.OracleClient.OracleType.Number,
                });

                cmdMContractDelete.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    ParameterName = "O_CUR",
                    OracleType = System.Data.OracleClient.OracleType.Cursor,
                    Direction = ParameterDirection.Output
                });
                return dbManager.ExecuteDataTable(cmdMContractDelete, "cmdMContractDelete");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static ContractTiedDAL _instance;
        public static ContractTiedDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ContractTiedDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
