﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data.OracleClient;
using dbManager;

namespace TMS_DAL.Master
{
    public partial class VendorDAL : OracleConnectionDAL
    {
        #region VendorDAL
        public VendorDAL()
        {
            InitializeComponent();
        }

        public VendorDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region PositionSelect
        public DataTable PositionSelect()
        {

            try
            {

                return dbManager.ExecuteDataTable(cmdPositionSelect, "cmdPositionSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable PositionSelectAllDAL(string Condition)
        {

            try
            {
                cmdPositionSelectAll.CommandText = @"SELECT *" +
                                                   " FROM TEMPLOYEETYPES" +
                                                   " WHERE 1=1" + Condition;

                return dbManager.ExecuteDataTable(cmdPositionSelectAll, "cmdPositionSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region TitleSelect
        public DataTable TitleSelect()
        {

            try
            {

                return dbManager.ExecuteDataTable(cmdTitleSelect, "cmdTitleSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region StatusCancelSelect
        public DataTable StatusCancelSelect()
        {

            try
            {

                return dbManager.ExecuteDataTable(cmdStatusCancelSelect, "cmdStatusCancelSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region StatusSelect
        public DataTable StatusSelect()
        {

            try
            {

                return dbManager.ExecuteDataTable(cmdStatusSelect, "cmdStatusSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region VendorList
        public DataTable VendorList(string Condition)
        {
            try
            {
                cmdVendor.CommandType = CommandType.Text;
                cmdVendor.CommandText = @"SELECT EMPSAP.SEMPLOYEEID,EMPSAP.FNAME||' '||EMPSAP.LNAME AS sName ,EMPSAP.PERS_CODE ,ven.SABBREVIATION  ,EMP.STRANS_ID
                                         ,nvl(temployeetypes.person_type_desc,'อื่นๆ') AS SEMPTPYE
                                         ,CASE EMP.CACTIVE
                                          WHEN '1' THEN 'อนุญาต' 
                                          WHEN '0' THEN 'ระงับการใช้งาน'
                                          ELSE 'Black List'  END AS CACTIVE
                                            ,EMPSAP.DUPDATE AS DUPDATESAP ,EMPSAP.SUPDATE AS SUPDATESAP ,EMP.DUPDATE AS DUPDATEEMP , EMP.SUPDATE AS SUPDATEEMP,EMP.CHECKIN,EMP.NOTFILL,EMP.INUSE
                                            ,(SELECT TRUNC(sysdate) - TRUNC(TO_DATE(EMP.DATE_LASTJOB)) AS DateLast FROM Dual) AS LASTJOB
                                            ,CASE WHEN EMP.INUSE = '1' THEN 'จ้างงาน' ELSE 'เลิกจ้างงาน' END  INUSENAME
                                         ,EMP.PERSONEL_EXPIRE AS CitizenExpired
										 ,EMP.DDRIVEEXPIRE AS CarLicenseT4Expired
										 ,EMP.ACCIDENTENDDATE AS CarLicenseDDCExpired
                                         , M_STATUS.STATUS_NAME AS SPOT_STATUS_NAME
                                         , TCONTRACT.SCONTRACTNO
                                        FROM  TEMPLOYEE_SAP EMPSAP
                                        LEFT JOIN TEMPLOYEE EMP ON EMPSAP.SEMPLOYEEID = EMP.SEMPLOYEEID
                                        LEFT JOIN M_STATUS ON M_STATUS.STATUS_VALUE = CASE WHEN EMP.SPOT_STATUS IS NULL THEN 0 ELSE EMP.SPOT_STATUS END AND M_STATUS.STATUS_TYPE = 'SPOT_STATUS'
                                        LEFT JOIN TCONTRACT ON TCONTRACT.SCONTRACTID = EMP.CONTRACT_ID
                                        LEFT JOIN TVENDOR  ven  ON EMP.STRANS_ID = ven.SVENDORID 
                                        LEFT JOIN temployeetypes ON temployeetypes.person_type = EMP.SEMPTPYE 
                                        WHERE  1=1  " + Condition + @" AND NVL(EMP.STRANS_ID,'xxx') != 'PHTDPRT'  ORDER BY   EMP.CHECKIN ASC ,EMP.DUPDATE DESC
                                        ";
                cmdVendor.Parameters.Clear();
                //cmdVendorList.Parameters["SEMPLOYEEID"].Value = SEMPLOYEEID;
                DataTable dt = dbManager.ExecuteDataTable(cmdVendor, "cmdVendorList");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorWebList
        public DataTable VendorWebList(string Condition)
        {
            try
            {
                cmdVendor.CommandType = CommandType.Text;
                cmdVendor.CommandText = @"SELECT * FROM (SELECT EMPSAP.SEMPLOYEEID,EMPSAP.FNAME||' '||EMPSAP.LNAME AS sName ,EMPSAP.PERS_CODE ,ven.SABBREVIATION  ,EMP.STRANS_ID
                                         ,nvl(temployeetypes.person_type_desc,'อื่นๆ') AS SEMPTPYE
																				 ,EMP.CACTIVE
                                         ,CASE EMP.CACTIVE
                                          WHEN '1' THEN 'อนุญาต' 
                                          WHEN '0' THEN 'ระงับการใช้งาน'
                                          ELSE 'Black List'  END AS CACTIVE_DESC
                                            ,EMPSAP.DUPDATE AS DUPDATESAP ,EMPSAP.SUPDATE AS SUPDATESAP ,EMP.DUPDATE AS DUPDATEEMP , EMP.SUPDATE AS SUPDATEEMP,EMP.CHECKIN,EMP.NOTFILL,EMP.INUSE
                                            ,(SELECT TRUNC(sysdate) - TRUNC(TO_DATE(EMP.DATE_LASTJOB)) AS DateLast FROM Dual) AS LASTJOB
                                            ,CASE WHEN EMP.INUSE = '1' THEN 'จ้างงาน' ELSE 'เลิกจ้างงาน' END  INUSENAME
																				 ,UP.UPLOAD_ID , UPTYPE.UPLOAD_NAME 
     , CASE WHEN TRUNC(SYSDATE) > TRUNC(UP.STOP_DATE) THEN '1' ELSE '0' END AS IS_EXPIRE
     , CASE WHEN TRUNC(SYSDATE) > TRUNC(UP.STOP_DATE) THEN 'Red' ELSE 'Black' END AS ROW_COLOR
																				 ,TO_CHAR(UP.START_DATE,'dd/mm/yyyy') AS START_DATE
																				 ,TO_CHAR(UP.STOP_DATE,'dd/mm/yyyy') AS STOP_DATE
                                         ,EMP.PERSONEL_EXPIRE AS CitizenExpired
										 ,EMP.DDRIVEEXPIRE AS CarLicenseT4Expired
										 ,EMP.ACCIDENTENDDATE AS CarLicenseDDCExpired
                                        FROM  TEMPLOYEE_SAP EMPSAP
                                        LEFT JOIN TEMPLOYEE EMP ON EMPSAP.SEMPLOYEEID = EMP.SEMPLOYEEID
                                        LEFT JOIN F_UPLOAD UP ON UP.REF_STR = EMP.SEMPLOYEEID
                                        LEFT JOIN M_UPLOAD_TYPE UPTYPE ON UP.UPLOAD_ID = UPTYPE.UPLOAD_ID
                                        LEFT JOIN TVENDOR  ven  ON EMP.STRANS_ID = ven.SVENDORID 
                                        LEFT JOIN temployeetypes ON temployeetypes.person_type = EMP.SEMPTPYE 
                                        WHERE  1=1 AND NVL(EMP.STRANS_ID,'xxx') != 'PHTDPRT' AND UP.ISACTIVE = 1  
                                        )
                                        WHERE 1=1  " + Condition + @" 
                                        ORDER BY CHECKIN ASC ,DUPDATEEMP DESC";
                cmdVendor.Parameters.Clear();
                //cmdVendorList.Parameters["SEMPLOYEEID"].Value = SEMPLOYEEID;
                return dbManager.ExecuteDataTable(cmdVendor, "cmdVendorList");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorWebServiceList
        public DataTable VendorWebServiceList(string Condition)
        {
            try
            {
                cmdVendor.CommandType = CommandType.Text;
                cmdVendor.CommandText = @"SELECT DISTINCT * FROM (SELECT EMPSAP.SEMPLOYEEID,EMPSAP.FNAME||' '||EMPSAP.LNAME AS sName ,EMPSAP.PERS_CODE ,ven.SABBREVIATION  ,EMP.STRANS_ID
                                         ,nvl(temployeetypes.person_type_desc,'อื่นๆ') AS SEMPTPYE
																				 ,EMP.CACTIVE
                                         ,CASE EMP.CACTIVE
                                          WHEN '1' THEN 'อนุญาต' 
                                          WHEN '0' THEN 'ระงับการใช้งาน'
                                          ELSE 'Black List'  END AS CACTIVE_DESC
                                            ,TO_CHAR(EMPSAP.DUPDATE,'dd/mm/yyyy') AS DUPDATESAP ,EMPSAP.SUPDATE AS SUPDATESAP ,TO_CHAR(EMP.DUPDATE,'dd/mm/yyyy') AS DUPDATEEMP , EMP.SUPDATE AS SUPDATEEMP,EMP.CHECKIN,EMP.NOTFILL,EMP.INUSE
                                            ,(SELECT TRUNC(sysdate) - TRUNC(TO_DATE(EMP.DATE_LASTJOB)) AS DateLast FROM Dual) AS LASTJOB
                                            ,CASE WHEN EMP.INUSE = '1' THEN 'จ้างงาน' ELSE 'เลิกจ้างงาน' END  INUSENAME
																				 ,UP.UPLOAD_ID , UPTYPE.UPLOAD_NAME 
     , CASE WHEN TRUNC(SYSDATE) > TRUNC(UP.STOP_DATE) THEN '1' ELSE '0' END AS IS_EXPIRE
     , CASE WHEN TRUNC(SYSDATE) > TRUNC(UP.STOP_DATE) THEN 'Red' ELSE 'Black' END AS ROW_COLOR
																				 ,TO_CHAR(UP.START_DATE,'dd/mm/yyyy') AS START_DATE
																				 ,TO_CHAR(UP.STOP_DATE,'dd/mm/yyyy') AS STOP_DATE
                                         ,(SELECT TO_CHAR(MAX(STOP_DATE),'DD/MM/YYYY') FROM F_UPLOAD WHERE UPLOAD_ID = 31 AND REF_STR = EMPSAP.SEMPLOYEEID AND F_UPLOAD.ISACTIVE = 1) AS CitizenExpired
										 ,(SELECT TO_CHAR(MAX(STOP_DATE),'DD/MM/YYYY') FROM F_UPLOAD WHERE UPLOAD_ID = 33 AND REF_STR = EMPSAP.SEMPLOYEEID AND F_UPLOAD.ISACTIVE = 1) AS CarLicenseT4Expired
										 ,(SELECT TO_CHAR(MAX(STOP_DATE),'DD/MM/YYYY') FROM F_UPLOAD WHERE UPLOAD_ID = 34 AND REF_STR = EMPSAP.SEMPLOYEEID AND F_UPLOAD.ISACTIVE = 1) AS CarLicenseDDCExpired
                                        FROM  TEMPLOYEE_SAP EMPSAP
                                        LEFT JOIN TEMPLOYEE EMP ON EMPSAP.SEMPLOYEEID = EMP.SEMPLOYEEID
                                        LEFT JOIN F_UPLOAD UP ON UP.REF_STR = EMP.SEMPLOYEEID
                                        LEFT JOIN M_UPLOAD_TYPE UPTYPE ON UP.UPLOAD_ID = UPTYPE.UPLOAD_ID
                                        LEFT JOIN TVENDOR  ven  ON EMP.STRANS_ID = ven.SVENDORID 
                                        LEFT JOIN temployeetypes ON temployeetypes.person_type = EMP.SEMPTPYE 
                                        WHERE  1=1 AND NVL(EMP.STRANS_ID,'xxx') != 'PHTDPRT' AND UP.ISACTIVE = 1  
                                        )
                                        WHERE 1=1 " + Condition + @" 
                                        ORDER BY CHECKIN ASC ,DUPDATEEMP DESC";
                cmdVendor.Parameters.Clear();
                //cmdVendorList.Parameters["SEMPLOYEEID"].Value = SEMPLOYEEID;
                return dbManager.ExecuteDataTable(cmdVendor, "cmdVendorList");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorGetByPersCode
        public DataTable VendorGetByPersCode(string PersCode)
        {
            try
            {
                cmdVendor.CommandText = @"SELECT TEMPLOYEE.SEMPLOYEEID
      ,STRANS_ID
      ,STRANTYPE
      ,SEMPTPYE
      ,SDRIVERNO
      ,SREGIDTERNO
      ,STRUCKID
      ,INAME
      ,SNATIONNAL
      ,SORIGIN
      ,SRELIGION
      ,NHEIGHT
      ,NWEIGHT
      ,SSCAR
      ,DBIRTHDATE
      ,SADDRESS
      ,TUMBON
      ,AMPHUR
      ,SPROVINCEID
      ,SZIPCODE
      ,STEL
      ,SFAX
      ,SPERSONELNO
      ,PERSONEL_BEGIN
      ,PERSONEL_EXPIRE
      ,DDRIVEBEGIN
      ,DDRIVEEXPIRE
      ,NENTRYYEAR
      ,NEDUCATIONID
      ,SREMARK
      ,CACTIVE
      ,SSTATUSREMARK
      ,SPICNAME
      ,SBLACKLISTTYPE
      ,DBEGINBLACKLIST
      ,DENDBLACKLIST
      ,CBLACKLIST
      ,TEMPLOYEE.DCREATE
      ,TEMPLOYEE.SCREATE
      ,TEMPLOYEE.DUPDATE
      ,TEMPLOYEE.SUPDATE
      ,DATE_LASTJOB
      ,LAST_SHIPMENT
      ,SPATH
      ,SFILENAME
      ,SSYSFILENAME
      ,CAUSESAP
      ,CAUSEOVER
      ,STEL2
      ,SMAIL
      ,SMAIL2
      ,CHECKIN
      ,NOTFILL
      ,BANSTATUS
      ,CAUSESAPCANCEL
      ,CANCELSTATUS
      ,CAUSESAPCOMMIT
      ,INUSE
      ,WORKDATE
      ,NUMACCIDENT
      ,ACCIDENTSTARTDATE
      ,ACCIDENTENDDATE
      ,SETTLETO
      ,STERMINALID
      ,FNAME
      ,LNAME
      ,PERS_CODE
      ,CARRIER
      ,LICENSE_NO
      ,DRVSTATUS
      ,EMPSAPID
      ,TNAME FROM TEMPLOYEE 
JOIN TEMPLOYEE_SAP ON TEMPLOYEE.SEMPLOYEEID = TEMPLOYEE_SAP.SEMPLOYEEID
  WHERE TEMPLOYEE_SAP.PERS_CODE = :PersCode
";
                cmdVendor.Parameters.Clear();
                cmdVendor.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "PersCode",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    Value = PersCode,
                });
                //cmdVendorList.Parameters["SEMPLOYEEID"].Value = SEMPLOYEEID;
                return dbManager.ExecuteDataTable(cmdVendor, "cmdVendor");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorSelect
        public DataTable VendorSelect(string SEMPLOYEEID)
        {
            try
            {
                cmdVendorSelect.Parameters["SEMPLOYEEID"].Value = SEMPLOYEEID;
                return dbManager.ExecuteDataTable(cmdVendorSelect, "cmdVendorSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverSelect(string CardID)
        {
            try
            {
                cmdDriverSelect.Parameters["I_PERS_CODE"].Value = CardID;
                return dbManager.ExecuteDataTable(cmdDriverSelect, "cmdDriverSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable DriverSelectWebDAL(string CardID)
        {
            try
            {
                cmdDriverSelectWeb.Parameters["I_PERS_CODE"].Value = CardID;
                DataTable dtdriver = dbManager.ExecuteDataTable(cmdDriverSelectWeb, "cmdDriverSelectWeb");
                return dtdriver;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorP4Select
        public DataTable VendorP4Select(string SEMPLOYEEID, string REQID)
        {
            try
            {

                cmdVendorP4Select.CommandType = CommandType.StoredProcedure;
                cmdVendorP4Select.Parameters.Clear();
                cmdVendorP4Select.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEMPLOYEEID",
                    Value = SEMPLOYEEID,
                    IsNullable = true,

                });
                cmdVendorP4Select.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REQID",
                    Value = REQID,
                    IsNullable = true,

                });
                cmdVendorP4Select.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = System.Data.OracleClient.OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true,

                });
                return dbManager.ExecuteDataTable(cmdVendorP4Select, "cmdVendorP4Select");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorRequestFile
        public DataTable VendorRequestFile(string PERSON_TYPE)
        {
            try
            {

                cmdVendorRequestFile.CommandType = CommandType.Text;
                cmdVendorRequestFile.Parameters.Clear();
                cmdVendorRequestFile.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE",
                    Value = PERSON_TYPE,
                    IsNullable = true,

                });

                return dbManager.ExecuteDataTable(cmdVendorRequestFile, "cmdVendorRequestFile");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable VendorRequestFile_Doc(string PERSON_TYPE, string UPLOAD_TYPE_IND)
        {
            try
            {

                cmdVendorRequestFile_Doc.CommandType = CommandType.Text;
                cmdVendorRequestFile_Doc.Parameters.Clear();
                cmdVendorRequestFile_Doc.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "PERSON_TYPE",
                    Value = PERSON_TYPE,
                    IsNullable = true,
                });
                cmdVendorRequestFile_Doc.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "UPLOAD_TYPE_IND",
                    Value = UPLOAD_TYPE_IND,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdVendorRequestFile_Doc, "cmdVendorRequestFile_Doc");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Query SVENDORID, SVENDORNAME
        public DataTable SelectName(string Condition)
        {
            string Query = cmdVendorSelectNameCar.CommandText;
            if (Condition == "")
            {
                try
                {
                    cmdVendorSelectNameCar.Parameters.Clear();
                    return dbManager.ExecuteDataTable(cmdVendorSelectNameCar, "cmdVendorSelectNameCar");
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    cmdVendorSelectNameCar.CommandText = Query;
                }

            }
            else
            {

                try
                {
                    string Strsql = @"SELECT SVENDORID , SABBREVIATION FROM  TVENDOR WHERE SVENDORID =:SVENDORID";
                    cmdVendorSelectNameCar.CommandText = Strsql;
                    cmdVendorSelectNameCar.Parameters.Clear();
                    cmdVendorSelectNameCar.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = Condition;
                    return dbManager.ExecuteDataTable(cmdVendorSelectNameCar, "cmdVenderSelectName");
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    cmdVendorSelectNameCar.CommandText = Query;
                }
            }
        }

        public DataTable InitalNameVendor(string Condition)
        {

            cmdSelectNameVendor.Parameters.Clear();
            cmdSelectNameVendor.Parameters.Add(":SABBREVIATION", OracleType.VarChar).Value = Condition;
            return dbManager.ExecuteDataTable(cmdSelectNameVendor, "cmdSelectNameVendor");

        }
        #endregion

        #region TVendorSapSelect
        public DataTable TVendorSapSelect()
        {
            try
            {
                return dbManager.ExecuteDataTable(cmdTVendorSapSelect, "cmdTVendorSapSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TVendorSapSelect(string Condition)
        {
            try
            {
                string Strsql = @"SELECT SVENDORID , SABBREVIATION FROM  TVENDOR WHERE 1 = 1 AND CACTIVE = '1' 
                             {0}
                             ORDER BY SVENDORID";
                cmdLogDriverJobInsert.CommandText = string.Format(Strsql, Condition);
                return dbManager.ExecuteDataTable(cmdLogDriverJobInsert, "cmdTVendorSapSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region oldvender
        //Old code
        #region VendorSave
        public DataTable VendorSave(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                if (!string.IsNullOrEmpty(I_SEMPLOYEEID))
                {
                    cmdImportFileDelete.Parameters["I_DOC_ID"].Value = I_SEMPLOYEEID;
                    dbManager.ExecuteNonQuery(cmdImportFileDelete);
                }


                cmdVendorSave.CommandType = CommandType.StoredProcedure;

                cmdVendorSave.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorSave.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorSave.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorSave.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorSave.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorSave.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorSave.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorSave.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorSave.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorSave.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorSave.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorSave.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorSave.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorSave.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorSave.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorSave.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorSave.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorSave.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorSave.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorSave.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorSave.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorSave.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorSave.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorSave.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorSave.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorSave.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorSave.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorSave.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorSave.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorSave.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorSave.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorSave.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorSave.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorSave.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorSave.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorSave.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorSave.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorSave.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorSave.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorSave.Parameters["I_STERMINALID"].Value = I_STERMINALID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorSave, "cmdVendorSave");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorInsert
        public DataTable VendorInsert(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_EMPSAPID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorInsert.CommandType = CommandType.StoredProcedure;

                cmdVendorInsert.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorInsert.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorInsert.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorInsert.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorInsert.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorInsert.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorInsert.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorInsert.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorInsert.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorInsert.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorInsert.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorInsert.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorInsert.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorInsert.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorInsert.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorInsert.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorInsert.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorInsert.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorInsert.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorInsert.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorInsert.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorInsert.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorInsert.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorInsert.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorInsert.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorInsert.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorInsert.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorInsert.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorInsert.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorInsert.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorInsert.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorInsert.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorInsert.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorInsert.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorInsert.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorInsert.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorInsert.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorInsert.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorInsert.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorInsert.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorInsert.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorInsert, "cmdVendorInsert");

                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorP4Insert
        public DataTable VendorP4Insert(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            string I_EMPSAPID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Dispose();
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorP4Insert.CommandType = CommandType.StoredProcedure;

                cmdVendorP4Insert.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorP4Insert.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorP4Insert.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorP4Insert.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorP4Insert.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorP4Insert.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorP4Insert.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorP4Insert.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorP4Insert.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorP4Insert.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorP4Insert.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorP4Insert.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorP4Insert.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorP4Insert.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorP4Insert.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorP4Insert.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorP4Insert.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorP4Insert.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorP4Insert.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorP4Insert.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorP4Insert.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorP4Insert.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorP4Insert.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorP4Insert.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorP4Insert.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorP4Insert.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorP4Insert.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorP4Insert.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorP4Insert.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorP4Insert.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorP4Insert.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorP4Insert.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorP4Insert.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorP4Insert.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorP4Insert.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorP4Insert.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorP4Insert.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorP4Insert.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorP4Insert.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorP4Insert.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorP4Insert.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorP4Insert, "cmdVendorP4Insert");

                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorSaveChangeVendor
        public DataTable VendorSaveChangeVendor(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorSaveChangeVendor.CommandType = CommandType.StoredProcedure;

                cmdVendorSaveChangeVendor.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorSaveChangeVendor.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorSaveChangeVendor.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorSaveChangeVendor.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorSaveChangeVendor.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorSaveChangeVendor.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorSaveChangeVendor.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorSaveChangeVendor.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorSaveChangeVendor.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorSaveChangeVendor.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorSaveChangeVendor.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorSaveChangeVendor.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorSaveChangeVendor.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorSaveChangeVendor.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorSaveChangeVendor.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorSaveChangeVendor.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorSaveChangeVendor.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorSaveChangeVendor.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorSaveChangeVendor.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorSaveChangeVendor.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorSaveChangeVendor.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorSaveChangeVendor.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorSaveChangeVendor.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorSaveChangeVendor.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorSaveChangeVendor.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorSaveChangeVendor.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorSaveChangeVendor.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorSaveChangeVendor.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorSaveChangeVendor.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorSaveChangeVendor.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorSaveChangeVendor.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorSaveChangeVendor.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorSaveChangeVendor.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorSaveChangeVendor.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorSaveChangeVendor.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorSaveChangeVendor.Parameters["I_STERMINALID"].Value = I_STERMINALID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorSaveChangeVendor, "cmdVendorSaveChangeVendor");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorUpdate
        public DataTable VendorUpdate(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_EMPSAPID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();


                cmdVendorUpdate.CommandType = CommandType.StoredProcedure;

                cmdVendorUpdate.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorUpdate.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorUpdate.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorUpdate.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorUpdate.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorUpdate.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorUpdate.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorUpdate.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorUpdate.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorUpdate.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorUpdate.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorUpdate.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorUpdate.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorUpdate.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorUpdate.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorUpdate.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorUpdate.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorUpdate.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorUpdate.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorUpdate.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorUpdate.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorUpdate.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorUpdate.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorUpdate.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorUpdate.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorUpdate.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorUpdate.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorUpdate.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorUpdate.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorUpdate.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorUpdate.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorUpdate.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorUpdate.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorUpdate.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorUpdate.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorUpdate.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorUpdate.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorUpdate.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorUpdate.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorUpdate.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorUpdate.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorUpdate, "cmdVendorUpdate");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }

                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorP4Update
        public DataTable VendorP4Update(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID,
            string I_EMPSAPID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorP4Update.CommandType = CommandType.StoredProcedure;

                cmdVendorP4Update.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorP4Update.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorP4Update.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorP4Update.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorP4Update.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorP4Update.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorP4Update.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorP4Update.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorP4Update.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorP4Update.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorP4Update.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorP4Update.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorP4Update.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorP4Update.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorP4Update.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorP4Update.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorP4Update.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorP4Update.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorP4Update.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorP4Update.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorP4Update.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorP4Update.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorP4Update.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorP4Update.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorP4Update.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorP4Update.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorP4Update.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorP4Update.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorP4Update.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorP4Update.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorP4Update.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorP4Update.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorP4Update.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorP4Update.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorP4Update.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorP4Update.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorP4Update.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorP4Update.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorP4Update.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorP4Update.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorP4Update.Parameters["I_REQ_ID"].Value = I_REQ_ID;
                cmdVendorP4Update.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorP4Update, "cmdVendorP4Update");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorUpdateVendor
        public DataTable VendorUpdateVendor(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                if (!string.IsNullOrEmpty(I_SEMPLOYEEID))
                {
                    cmdImportFileDelete.Parameters["I_DOC_ID"].Value = I_SEMPLOYEEID;
                    dbManager.ExecuteNonQuery(cmdImportFileDelete);
                }


                cmdVendorUpdateVendor.CommandType = CommandType.StoredProcedure;

                cmdVendorUpdateVendor.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorUpdateVendor.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorUpdateVendor.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorUpdateVendor.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorUpdateVendor.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorUpdateVendor.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorUpdateVendor.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorUpdateVendor.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorUpdateVendor.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorUpdateVendor.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorUpdateVendor.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorUpdateVendor.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorUpdateVendor.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorUpdateVendor.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorUpdateVendor.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorUpdateVendor.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorUpdateVendor.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorUpdateVendor.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorUpdateVendor.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorUpdateVendor.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorUpdateVendor.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorUpdateVendor.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorUpdateVendor.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorUpdateVendor.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorUpdateVendor.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorUpdateVendor.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorUpdateVendor.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorUpdateVendor.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorUpdateVendor.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorUpdateVendor.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorUpdateVendor.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorUpdateVendor.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorUpdateVendor.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorUpdateVendor.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorUpdateVendor.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorUpdateVendor.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorUpdateVendor.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorUpdateVendor.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorUpdateVendor.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorUpdateVendor.Parameters["I_STERMINALID"].Value = I_STERMINALID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorUpdateVendor, "cmdVendorUpdateVendor");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorPKSaveDraft
        public DataTable VendorPKSaveDraft(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorPKSaveDraft.CommandType = CommandType.StoredProcedure;

                cmdVendorPKSaveDraft.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorPKSaveDraft.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorPKSaveDraft.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorPKSaveDraft.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorPKSaveDraft.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorPKSaveDraft.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorPKSaveDraft.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorPKSaveDraft.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorPKSaveDraft.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorPKSaveDraft.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorPKSaveDraft.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorPKSaveDraft.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorPKSaveDraft.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorPKSaveDraft.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorPKSaveDraft.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorPKSaveDraft.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorPKSaveDraft.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorPKSaveDraft.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorPKSaveDraft.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorPKSaveDraft.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorPKSaveDraft.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorPKSaveDraft.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorPKSaveDraft.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorPKSaveDraft.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorPKSaveDraft.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorPKSaveDraft.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorPKSaveDraft.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorPKSaveDraft.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorPKSaveDraft.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorPKSaveDraft.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorPKSaveDraft.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorPKSaveDraft.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorPKSaveDraft.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorPKSaveDraft.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorPKSaveDraft.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorPKSaveDraft.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorPKSaveDraft.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorPKSaveDraft.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorPKSaveDraft.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorPKSaveDraft.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorPKSaveDraft.Parameters["I_REQ_ID"].Value = I_REQ_ID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorPKSaveDraft, "cmdVendorPKSaveDraft");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }

                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorPKSaveApprove
        public DataTable VendorPKSaveApprove(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID, string I_EMPSAPID,
            DataTable dtUpload)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorPKSaveApprove.CommandType = CommandType.StoredProcedure;

                cmdVendorPKSaveApprove.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorPKSaveApprove.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorPKSaveApprove.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorPKSaveApprove.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorPKSaveApprove.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorPKSaveApprove.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorPKSaveApprove.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorPKSaveApprove.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorPKSaveApprove.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorPKSaveApprove.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorPKSaveApprove.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorPKSaveApprove.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorPKSaveApprove.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorPKSaveApprove.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorPKSaveApprove.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorPKSaveApprove.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorPKSaveApprove.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorPKSaveApprove.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorPKSaveApprove.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorPKSaveApprove.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorPKSaveApprove.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorPKSaveApprove.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorPKSaveApprove.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorPKSaveApprove.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorPKSaveApprove.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorPKSaveApprove.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorPKSaveApprove.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorPKSaveApprove.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorPKSaveApprove.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorPKSaveApprove.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorPKSaveApprove.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorPKSaveApprove.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorPKSaveApprove.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorPKSaveApprove.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorPKSaveApprove.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorPKSaveApprove.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorPKSaveApprove.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorPKSaveApprove.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorPKSaveApprove.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorPKSaveApprove.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorPKSaveApprove.Parameters["I_REQ_ID"].Value = I_REQ_ID;
                cmdVendorPKSaveApprove.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorPKSaveApprove, "cmdVendorPKSaveApprove");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }

                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #endregion

        #region newvender
        //new code
        #region VendorSave2
        public DataTable VendorSave2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                if (!string.IsNullOrEmpty(I_SEMPLOYEEID))
                {
                    cmdImportFileDelete.Parameters["I_DOC_ID"].Value = I_SEMPLOYEEID;
                    dbManager.ExecuteNonQuery(cmdImportFileDelete);
                }


                cmdVendorSave.CommandType = CommandType.StoredProcedure;

                cmdVendorSave.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorSave.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorSave.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorSave.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorSave.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorSave.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorSave.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorSave.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorSave.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorSave.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorSave.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorSave.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorSave.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorSave.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorSave.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorSave.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorSave.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorSave.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorSave.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorSave.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorSave.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorSave.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorSave.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorSave.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorSave.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorSave.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorSave.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorSave.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorSave.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorSave.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorSave.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorSave.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorSave.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorSave.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorSave.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorSave.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorSave.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorSave.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorSave.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorSave.Parameters["I_STERMINALID"].Value = I_STERMINALID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorSave, "cmdVendorSave");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorInsert2
        public DataTable VendorInsert2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorInsert.CommandType = CommandType.StoredProcedure;

                cmdVendorInsert.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorInsert.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorInsert.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorInsert.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorInsert.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorInsert.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorInsert.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorInsert.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorInsert.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorInsert.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorInsert.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorInsert.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorInsert.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorInsert.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorInsert.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorInsert.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorInsert.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorInsert.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorInsert.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorInsert.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorInsert.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorInsert.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorInsert.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorInsert.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorInsert.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorInsert.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorInsert.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorInsert.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorInsert.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorInsert.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorInsert.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorInsert.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorInsert.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorInsert.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorInsert.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorInsert.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorInsert.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorInsert.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorInsert.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorInsert.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorInsert.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorInsert, "cmdVendorInsert");

                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable VendorInsert3(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorInsert.CommandType = CommandType.StoredProcedure;

                cmdVendorInsert.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorInsert.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorInsert.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorInsert.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorInsert.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorInsert.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorInsert.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorInsert.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorInsert.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorInsert.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorInsert.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorInsert.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorInsert.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorInsert.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorInsert.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorInsert.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorInsert.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorInsert.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorInsert.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorInsert.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorInsert.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorInsert.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorInsert.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorInsert.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorInsert.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorInsert.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorInsert.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorInsert.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorInsert.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorInsert.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorInsert.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorInsert.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorInsert.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorInsert.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorInsert.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorInsert.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorInsert.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorInsert.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorInsert.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorInsert.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorInsert.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;
                cmdVendorInsert.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                if (ContractID == 0)
                    cmdVendorInsert.Parameters["I_CONTRACT_ID"].Value = DBNull.Value;
                else
                    cmdVendorInsert.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorInsert, "cmdVendorInsert");

                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorP4Insert2
        public DataTable VendorP4Insert2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Dispose();
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorP4Insert.CommandType = CommandType.StoredProcedure;

                cmdVendorP4Insert.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorP4Insert.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorP4Insert.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorP4Insert.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorP4Insert.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorP4Insert.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorP4Insert.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorP4Insert.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorP4Insert.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorP4Insert.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorP4Insert.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorP4Insert.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorP4Insert.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorP4Insert.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorP4Insert.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorP4Insert.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorP4Insert.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorP4Insert.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorP4Insert.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorP4Insert.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorP4Insert.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorP4Insert.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorP4Insert.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorP4Insert.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorP4Insert.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorP4Insert.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorP4Insert.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorP4Insert.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorP4Insert.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorP4Insert.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorP4Insert.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorP4Insert.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorP4Insert.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorP4Insert.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorP4Insert.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorP4Insert.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorP4Insert.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorP4Insert.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorP4Insert.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorP4Insert.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorP4Insert.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorP4Insert, "cmdVendorP4Insert");

                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable VendorP4Insert3(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                dbManager.Dispose();
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorP4Insert_Spot.CommandType = CommandType.StoredProcedure;

                cmdVendorP4Insert_Spot.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorP4Insert_Spot.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorP4Insert_Spot.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorP4Insert_Spot.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorP4Insert_Spot.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorP4Insert_Spot.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorP4Insert_Spot.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorP4Insert_Spot.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorP4Insert_Spot.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorP4Insert_Spot.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorP4Insert_Spot.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorP4Insert_Spot.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorP4Insert_Spot.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorP4Insert_Spot.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorP4Insert_Spot.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorP4Insert_Spot.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorP4Insert_Spot.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorP4Insert_Spot.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorP4Insert_Spot.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorP4Insert_Spot.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorP4Insert_Spot.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorP4Insert_Spot.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorP4Insert_Spot.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorP4Insert_Spot.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorP4Insert_Spot.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorP4Insert_Spot.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorP4Insert_Spot.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorP4Insert_Spot.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorP4Insert_Spot.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorP4Insert_Spot.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorP4Insert_Spot.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorP4Insert_Spot.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorP4Insert_Spot.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorP4Insert_Spot.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorP4Insert_Spot.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorP4Insert_Spot.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorP4Insert_Spot.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorP4Insert_Spot.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorP4Insert_Spot.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorP4Insert_Spot.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorP4Insert_Spot.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;
                cmdVendorP4Insert_Spot.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                if (ContractID == 0)
                    cmdVendorP4Insert_Spot.Parameters["I_CONTRACT_ID"].Value = DBNull.Value;
                else
                    cmdVendorP4Insert_Spot.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorP4Insert_Spot, "cmdVendorP4Insert_Spot");

                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorSaveChangeVendor2
        public DataTable VendorSaveChangeVendor2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorSaveChangeVendor.CommandType = CommandType.StoredProcedure;

                cmdVendorSaveChangeVendor.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorSaveChangeVendor.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorSaveChangeVendor.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorSaveChangeVendor.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorSaveChangeVendor.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorSaveChangeVendor.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorSaveChangeVendor.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorSaveChangeVendor.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorSaveChangeVendor.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorSaveChangeVendor.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorSaveChangeVendor.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorSaveChangeVendor.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorSaveChangeVendor.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorSaveChangeVendor.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorSaveChangeVendor.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorSaveChangeVendor.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorSaveChangeVendor.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorSaveChangeVendor.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorSaveChangeVendor.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorSaveChangeVendor.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorSaveChangeVendor.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorSaveChangeVendor.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorSaveChangeVendor.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorSaveChangeVendor.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorSaveChangeVendor.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorSaveChangeVendor.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorSaveChangeVendor.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorSaveChangeVendor.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorSaveChangeVendor.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorSaveChangeVendor.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorSaveChangeVendor.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorSaveChangeVendor.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorSaveChangeVendor.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorSaveChangeVendor.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorSaveChangeVendor.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorSaveChangeVendor.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorSaveChangeVendor.Parameters["I_STERMINALID"].Value = I_STERMINALID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorSaveChangeVendor, "cmdVendorSaveChangeVendor");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorUpdate2
        public DataTable VendorUpdate2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();


                cmdVendorUpdate.CommandType = CommandType.StoredProcedure;

                cmdVendorUpdate.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorUpdate.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorUpdate.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorUpdate.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorUpdate.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorUpdate.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorUpdate.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorUpdate.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorUpdate.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorUpdate.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorUpdate.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorUpdate.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorUpdate.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorUpdate.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorUpdate.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorUpdate.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorUpdate.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorUpdate.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorUpdate.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorUpdate.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorUpdate.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorUpdate.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorUpdate.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorUpdate.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorUpdate.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorUpdate.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorUpdate.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorUpdate.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorUpdate.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorUpdate.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorUpdate.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorUpdate.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorUpdate.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorUpdate.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorUpdate.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorUpdate.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorUpdate.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorUpdate.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorUpdate.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorUpdate.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorUpdate.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorUpdate, "cmdVendorUpdate");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }

                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();

                        if (string.Equals(dtUpload2.Rows[i]["START_DATE"].ToString(), string.Empty))
                            cmdVendorAttachInsert2.Parameters["START_DATE"].Value = DBNull.Value;
                        else
                            cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();

                        if (string.Equals(dtUpload2.Rows[i]["STOP_DATE"].ToString(), string.Empty))
                            cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = DBNull.Value;
                        else
                            cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();

                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorP4Update2
        public DataTable VendorP4Update2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID,
            string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorP4Update.CommandType = CommandType.StoredProcedure;

                cmdVendorP4Update.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorP4Update.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorP4Update.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorP4Update.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorP4Update.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorP4Update.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorP4Update.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorP4Update.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorP4Update.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorP4Update.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorP4Update.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorP4Update.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorP4Update.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorP4Update.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorP4Update.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorP4Update.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorP4Update.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorP4Update.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorP4Update.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorP4Update.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorP4Update.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorP4Update.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorP4Update.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorP4Update.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorP4Update.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorP4Update.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorP4Update.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorP4Update.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorP4Update.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorP4Update.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorP4Update.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorP4Update.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorP4Update.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorP4Update.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorP4Update.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorP4Update.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorP4Update.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorP4Update.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorP4Update.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorP4Update.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorP4Update.Parameters["I_REQ_ID"].Value = I_REQ_ID;
                cmdVendorP4Update.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorP4Update, "cmdVendorP4Update");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable VendorP4Update3(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID,
            string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorP4Update_Spot.CommandType = CommandType.StoredProcedure;

                cmdVendorP4Update_Spot.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorP4Update_Spot.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorP4Update_Spot.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorP4Update_Spot.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorP4Update_Spot.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorP4Update_Spot.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorP4Update_Spot.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorP4Update_Spot.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorP4Update_Spot.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorP4Update_Spot.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorP4Update_Spot.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorP4Update_Spot.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorP4Update_Spot.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorP4Update_Spot.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorP4Update_Spot.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorP4Update_Spot.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorP4Update_Spot.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorP4Update_Spot.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorP4Update_Spot.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorP4Update_Spot.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorP4Update_Spot.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorP4Update_Spot.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorP4Update_Spot.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorP4Update_Spot.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorP4Update_Spot.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorP4Update_Spot.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorP4Update_Spot.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorP4Update_Spot.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorP4Update_Spot.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorP4Update_Spot.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorP4Update_Spot.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorP4Update_Spot.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorP4Update_Spot.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorP4Update_Spot.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorP4Update_Spot.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorP4Update_Spot.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorP4Update_Spot.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorP4Update_Spot.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorP4Update_Spot.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorP4Update_Spot.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorP4Update_Spot.Parameters["I_REQ_ID"].Value = I_REQ_ID;
                cmdVendorP4Update_Spot.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;
                cmdVendorP4Update_Spot.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                if (ContractID == 0)
                    cmdVendorP4Update_Spot.Parameters["I_CONTRACT_ID"].Value = DBNull.Value;
                else
                    cmdVendorP4Update_Spot.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorP4Update_Spot, "cmdVendorP4Update_Spot");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorUpdateVendor2
        public DataTable VendorUpdateVendor2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                if (!string.IsNullOrEmpty(I_SEMPLOYEEID))
                {
                    cmdImportFileDelete.Parameters["I_DOC_ID"].Value = I_SEMPLOYEEID;
                    dbManager.ExecuteNonQuery(cmdImportFileDelete);
                }


                cmdVendorUpdateVendor.CommandType = CommandType.StoredProcedure;

                cmdVendorUpdateVendor.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorUpdateVendor.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorUpdateVendor.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorUpdateVendor.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorUpdateVendor.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorUpdateVendor.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorUpdateVendor.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorUpdateVendor.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorUpdateVendor.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorUpdateVendor.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorUpdateVendor.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorUpdateVendor.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorUpdateVendor.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorUpdateVendor.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorUpdateVendor.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorUpdateVendor.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorUpdateVendor.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorUpdateVendor.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorUpdateVendor.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorUpdateVendor.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorUpdateVendor.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorUpdateVendor.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorUpdateVendor.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorUpdateVendor.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorUpdateVendor.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorUpdateVendor.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorUpdateVendor.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorUpdateVendor.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorUpdateVendor.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorUpdateVendor.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorUpdateVendor.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorUpdateVendor.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorUpdateVendor.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorUpdateVendor.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorUpdateVendor.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorUpdateVendor.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorUpdateVendor.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorUpdateVendor.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorUpdateVendor.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorUpdateVendor.Parameters["I_STERMINALID"].Value = I_STERMINALID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorUpdateVendor, "cmdVendorUpdateVendor");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorPKSaveDraft2
        public DataTable VendorPKSaveDraft2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorPKSaveDraft.CommandType = CommandType.StoredProcedure;

                cmdVendorPKSaveDraft.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorPKSaveDraft.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorPKSaveDraft.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorPKSaveDraft.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorPKSaveDraft.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorPKSaveDraft.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorPKSaveDraft.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorPKSaveDraft.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorPKSaveDraft.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorPKSaveDraft.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorPKSaveDraft.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorPKSaveDraft.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorPKSaveDraft.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorPKSaveDraft.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorPKSaveDraft.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorPKSaveDraft.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorPKSaveDraft.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorPKSaveDraft.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorPKSaveDraft.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorPKSaveDraft.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorPKSaveDraft.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorPKSaveDraft.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorPKSaveDraft.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorPKSaveDraft.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorPKSaveDraft.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorPKSaveDraft.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorPKSaveDraft.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorPKSaveDraft.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorPKSaveDraft.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorPKSaveDraft.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorPKSaveDraft.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorPKSaveDraft.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorPKSaveDraft.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorPKSaveDraft.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorPKSaveDraft.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorPKSaveDraft.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorPKSaveDraft.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorPKSaveDraft.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorPKSaveDraft.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorPKSaveDraft.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorPKSaveDraft.Parameters["I_REQ_ID"].Value = I_REQ_ID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorPKSaveDraft, "cmdVendorPKSaveDraft");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }

                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable VendorPKSaveDraft3(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID,
            DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorPKSaveDraft_Spot.CommandType = CommandType.StoredProcedure;

                cmdVendorPKSaveDraft_Spot.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorPKSaveDraft_Spot.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorPKSaveDraft_Spot.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorPKSaveDraft_Spot.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorPKSaveDraft_Spot.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorPKSaveDraft_Spot.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorPKSaveDraft_Spot.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorPKSaveDraft_Spot.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorPKSaveDraft_Spot.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorPKSaveDraft_Spot.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorPKSaveDraft_Spot.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorPKSaveDraft_Spot.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorPKSaveDraft_Spot.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorPKSaveDraft_Spot.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorPKSaveDraft_Spot.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorPKSaveDraft_Spot.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorPKSaveDraft_Spot.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorPKSaveDraft_Spot.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorPKSaveDraft_Spot.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorPKSaveDraft_Spot.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorPKSaveDraft_Spot.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorPKSaveDraft_Spot.Parameters["I_REQ_ID"].Value = I_REQ_ID;
                cmdVendorPKSaveDraft_Spot.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                if (ContractID == 0)
                    cmdVendorPKSaveDraft_Spot.Parameters["I_CONTRACT_ID"].Value = DBNull.Value;
                else
                    cmdVendorPKSaveDraft_Spot.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorPKSaveDraft_Spot, "cmdVendorPKSaveDraft_Spot");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }

                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorPKSaveApprove2
        public DataTable VendorPKSaveApprove2(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID, string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorPKSaveApprove.CommandType = CommandType.StoredProcedure;

                cmdVendorPKSaveApprove.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorPKSaveApprove.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorPKSaveApprove.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorPKSaveApprove.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorPKSaveApprove.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorPKSaveApprove.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorPKSaveApprove.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorPKSaveApprove.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorPKSaveApprove.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorPKSaveApprove.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorPKSaveApprove.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorPKSaveApprove.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorPKSaveApprove.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorPKSaveApprove.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorPKSaveApprove.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorPKSaveApprove.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorPKSaveApprove.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorPKSaveApprove.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorPKSaveApprove.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorPKSaveApprove.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorPKSaveApprove.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorPKSaveApprove.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorPKSaveApprove.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorPKSaveApprove.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorPKSaveApprove.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorPKSaveApprove.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorPKSaveApprove.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorPKSaveApprove.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorPKSaveApprove.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorPKSaveApprove.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorPKSaveApprove.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorPKSaveApprove.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorPKSaveApprove.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorPKSaveApprove.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorPKSaveApprove.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorPKSaveApprove.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorPKSaveApprove.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorPKSaveApprove.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorPKSaveApprove.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorPKSaveApprove.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorPKSaveApprove.Parameters["I_REQ_ID"].Value = I_REQ_ID;
                cmdVendorPKSaveApprove.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorPKSaveApprove, "cmdVendorPKSaveApprove");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }

                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable VendorPKSaveApprove3(string I_SEMPLOYEEID,
                                             string I_FNAME,
                                             string I_LNAME,
                                             DateTime? I_DCREATE,
                                             string I_SUPDATE,
                                             string I_SCREATE,
                                             string I_PERS_CODE,
                                             string I_CARRIER,
                                             string I_LICENSE_NO,
                                             string I_DRVSTATUS,
                                             string I_STRANS_ID,
                                             int? I_SEMPTPYE,
                                             DateTime? I_DBIRTHDATE,
                                             string I_STEL,
                                             string I_STEL2,
                                             string I_SMAIL,
                                             string I_SMAIL2,
                                             DateTime? I_PERSONEL_BEGIN,
                                             DateTime? I_PERSONEL_EXPIRE,
                                             string I_CACTIVE,
                                             string I_CAUSEOVER,
                                             string I_SFILENAME,
                                             string I_SSYSFILENAME,
                                             string I_SPATH,
                                             string I_SDRIVERNO,
                                             DateTime? I_DDRIVEBEGIN,
                                             DateTime? I_DDRIVEEXPIRE,
                                             string I_SPERSONELNO,
                                             string I_BANSTATUS,
                                             string I_CAUSESAP,
                                             string I_CANCELSTATUS,
                                             string I_CAUSESAPCANCEL,
                                             string I_CAUSESAPCOMMIT,
                                             DateTime? I_WORKDATE,
                                            string I_NOTFILL,
            string I_TNAME,
                                            string I_NUMACCIDENT,
                                            DateTime? I_ACCIDENTSTARTDATE,
                                            DateTime? I_ACCIDENTENDDATE,
                                            DateTime? I_SETTLETO,
                                            string I_STERMINALID, string I_REQ_ID, string I_EMPSAPID,
            DataTable dtUpload, DataTable dtUpload2, string SpotStatus, int ContractID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();



                cmdVendorPKSaveApprove_Spot.CommandType = CommandType.StoredProcedure;

                cmdVendorPKSaveApprove_Spot.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdVendorPKSaveApprove_Spot.Parameters["I_FNAME"].Value = I_FNAME;
                cmdVendorPKSaveApprove_Spot.Parameters["I_LNAME"].Value = I_LNAME;
                cmdVendorPKSaveApprove_Spot.Parameters["I_DCREATE"].Value = I_DCREATE == null ? DBNull.Value : (object)I_DCREATE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SCREATE"].Value = I_SCREATE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SUPDATE"].Value = I_SUPDATE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_CARRIER"].Value = I_CARRIER;
                cmdVendorPKSaveApprove_Spot.Parameters["I_LICENSE_NO"].Value = I_LICENSE_NO;
                cmdVendorPKSaveApprove_Spot.Parameters["I_DRVSTATUS"].Value = I_DRVSTATUS;
                cmdVendorPKSaveApprove_Spot.Parameters["I_STRANS_ID"].Value = I_STRANS_ID;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SEMPTPYE"].Value = I_SEMPTPYE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_DBIRTHDATE"].Value = I_DBIRTHDATE == null ? DBNull.Value : (object)I_DBIRTHDATE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_STEL"].Value = I_STEL;
                cmdVendorPKSaveApprove_Spot.Parameters["I_STEL2"].Value = I_STEL2;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SMAIL"].Value = I_SMAIL;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SMAIL2"].Value = I_SMAIL2;
                cmdVendorPKSaveApprove_Spot.Parameters["I_PERSONEL_BEGIN"].Value = I_PERSONEL_BEGIN == null ? DBNull.Value : (object)I_PERSONEL_BEGIN;
                cmdVendorPKSaveApprove_Spot.Parameters["I_PERSONEL_EXPIRE"].Value = I_PERSONEL_EXPIRE == null ? DBNull.Value : (object)I_PERSONEL_EXPIRE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_CACTIVE"].Value = I_CACTIVE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_CAUSEOVER"].Value = I_CAUSEOVER;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SFILENAME"].Value = I_SFILENAME;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SSYSFILENAME"].Value = I_SSYSFILENAME;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SPATH"].Value = I_SPATH;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SDRIVERNO"].Value = I_SDRIVERNO;
                cmdVendorPKSaveApprove_Spot.Parameters["I_DDRIVEBEGIN"].Value = I_DDRIVEBEGIN == null ? DBNull.Value : (object)I_DDRIVEBEGIN;
                cmdVendorPKSaveApprove_Spot.Parameters["I_DDRIVEEXPIRE"].Value = I_DDRIVEEXPIRE == null ? DBNull.Value : (object)I_DDRIVEEXPIRE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_BANSTATUS"].Value = I_BANSTATUS;
                cmdVendorPKSaveApprove_Spot.Parameters["I_CAUSESAP"].Value = I_CAUSESAP;
                cmdVendorPKSaveApprove_Spot.Parameters["I_CANCELSTATUS"].Value = I_CANCELSTATUS;
                cmdVendorPKSaveApprove_Spot.Parameters["I_CAUSESAPCANCEL"].Value = I_CAUSESAPCANCEL;
                cmdVendorPKSaveApprove_Spot.Parameters["I_CAUSESAPCOMMIT"].Value = I_CAUSESAPCOMMIT;
                cmdVendorPKSaveApprove_Spot.Parameters["I_WORKDATE"].Value = I_WORKDATE == null ? DBNull.Value : (object)I_WORKDATE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_NOTFILL"].Value = I_NOTFILL;
                cmdVendorPKSaveApprove_Spot.Parameters["I_TNAME"].Value = I_TNAME;
                cmdVendorPKSaveApprove_Spot.Parameters["I_NUMACCIDENT"].Value = I_NUMACCIDENT;
                cmdVendorPKSaveApprove_Spot.Parameters["I_ACCIDENTSTARTDATE"].Value = I_ACCIDENTSTARTDATE == null ? DBNull.Value : (object)I_ACCIDENTSTARTDATE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_ACCIDENTENDDATE"].Value = I_ACCIDENTENDDATE == null ? DBNull.Value : (object)I_ACCIDENTENDDATE;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SETTLETO"].Value = I_SETTLETO == null ? DBNull.Value : (object)I_SETTLETO;
                cmdVendorPKSaveApprove_Spot.Parameters["I_STERMINALID"].Value = I_STERMINALID;
                cmdVendorPKSaveApprove_Spot.Parameters["I_REQ_ID"].Value = I_REQ_ID;
                cmdVendorPKSaveApprove_Spot.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;
                cmdVendorPKSaveApprove_Spot.Parameters["I_SPOT_STATUS"].Value = SpotStatus;

                if (ContractID == 0)
                    cmdVendorPKSaveApprove_Spot.Parameters["I_CONTRACT_ID"].Value = DBNull.Value;
                else
                    cmdVendorPKSaveApprove_Spot.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorPKSaveApprove_Spot, "cmdVendorPKSaveApprove_Spot");
                if (dt.Rows[0]["Message"] == DBNull.Value)
                {
                    string EMPID = dt.Rows[0]["EmpID"].ToString();
                    if (!string.IsNullOrEmpty(EMPID))
                    {
                        cmdImportFileDelete.Parameters["I_DOC_ID"].Value = EMPID;
                        dbManager.ExecuteNonQuery(cmdImportFileDelete);
                    }

                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert);
                    }
                    for (int i = 0; i < dtUpload2.Rows.Count; i++)
                    {
                        cmdVendorAttachInsert2.Parameters["FILENAME_SYSTEM"].Value = dtUpload2.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdVendorAttachInsert2.Parameters["FILENAME_USER"].Value = dtUpload2.Rows[i]["FILENAME_USER"].ToString();
                        cmdVendorAttachInsert2.Parameters["UPLOAD_ID"].Value = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                        cmdVendorAttachInsert2.Parameters["FULLPATH"].Value = dtUpload2.Rows[i]["FULLPATH"].ToString();
                        cmdVendorAttachInsert2.Parameters["START_DATE"].Value = dtUpload2.Rows[i]["START_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["STOP_DATE"].Value = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        cmdVendorAttachInsert2.Parameters["DOC_NUMBER"].Value = dtUpload2.Rows[i]["DOC_NUMBER"].ToString();

                        cmdVendorAttachInsert2.Parameters["REF_INT"].Value = 0;
                        cmdVendorAttachInsert2.Parameters["REF_STR"].Value = EMPID;
                        cmdVendorAttachInsert2.Parameters["ISACTIVE"].Value = "1";
                        cmdVendorAttachInsert2.Parameters["CREATE_BY"].Value = I_SCREATE;

                        dbManager.ExecuteNonQuery(cmdVendorAttachInsert2);
                    }
                }
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #endregion


        #region VendorCheckBeforeSave
        public DataTable VendorCheckBeforeSave(string I_PERS_CODE)
        {
            try
            {

                cmdVendorCheckBeforeSave.CommandType = CommandType.StoredProcedure;
                cmdVendorCheckBeforeSave.Parameters.Clear();
                cmdVendorCheckBeforeSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    ParameterName = "I_PERS_CODE",
                    Value = I_PERS_CODE
                });
                cmdVendorCheckBeforeSave.Parameters.Add(new System.Data.OracleClient.OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = System.Data.OracleClient.OracleType.Cursor,
                    ParameterName = "O_CUR",
                });
                return dbManager.ExecuteDataTable(cmdVendorCheckBeforeSave, "cmdVendorCheckBeforeSave");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorUpdateInuseBySemployeeid
        public bool VendorUpdateInuseBySemployeeid(string I_SEMPLOYEEID,
                                             string I_INUSE)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorUpdateInuseBySemployeeid.CommandType = CommandType.Text;
                cmdVendorUpdateInuseBySemployeeid.Parameters.Clear();
                cmdVendorUpdateInuseBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "SEMPLOYEEID",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = I_SEMPLOYEEID
                });
                cmdVendorUpdateInuseBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "INUSE",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = I_INUSE
                });

                bool isRes = false;
                //dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdVendorUpdateInuseBySemployeeid);
                if (row >= 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorUpdateInactiveBySemployeeid
        public bool VendorUpdateInactiveBySemployeeid(string I_SEMPLOYEEID, string I_CACTIVE)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorUpdateInActiveBySemployeeid.CommandType = CommandType.Text;
                cmdVendorUpdateInActiveBySemployeeid.Parameters.Clear();
                cmdVendorUpdateInActiveBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "SEMPLOYEEID",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = I_SEMPLOYEEID
                });
                cmdVendorUpdateInActiveBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "ICACTIVE",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = I_CACTIVE
                });
                cmdVendorUpdateInActiveBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "IUPDATEBY",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = "SP_TMS"
                });

                bool isRes = false;
                //dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdVendorUpdateInActiveBySemployeeid);
                if (row >= 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorUpdateSAPInactiveBySemployeeid
        public bool VendorUpdateSAPInactiveBySemployeeid(string I_SEMPLOYEEID, string I_DRVStatus)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorUpdateSAPInActiveBySemployeeid.CommandType = CommandType.Text;
                cmdVendorUpdateSAPInActiveBySemployeeid.Parameters.Clear();
                cmdVendorUpdateSAPInActiveBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "SEMPLOYEEID",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = I_SEMPLOYEEID
                });
                cmdVendorUpdateSAPInActiveBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "IDRVSTATUS",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = I_DRVStatus
                });
                cmdVendorUpdateSAPInActiveBySemployeeid.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "IUPDATEBY",
                    OracleType = OracleType.VarChar,
                    IsNullable = false,
                    Direction = ParameterDirection.Input,
                    Value = "SP_TMS"
                });

                bool isRes = false;
                //dbManager.Open();
                int row = dbManager.ExecuteNonQuery(cmdVendorUpdateSAPInActiveBySemployeeid);
                if (row >= 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GetSpecialCC_TeamStatusEmail
        public DataTable GetSpecialCC_TeamStatusEmailDAL()
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdGetSpecialCC_TeamStatus.CommandType = CommandType.Text;
                cmdGetSpecialCC_TeamStatus.Parameters.Clear();

                return dbManager.ExecuteDataTable(cmdGetSpecialCC_TeamStatus, "cmdGetSpecialCC_TeamStatus");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion


        #region EMPSAPIDUpdate
        public DataTable EMPSAPIDUpdate(string I_SEMPLOYEEID,
                                             string I_PERS_CODE,
                                            string I_EMPSAPID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdEMPSAPID_Update.CommandType = CommandType.StoredProcedure;

                cmdEMPSAPID_Update.Parameters["I_SEMPLOYEEID"].Value = I_SEMPLOYEEID;
                cmdEMPSAPID_Update.Parameters["I_PERS_CODE"].Value = I_PERS_CODE;
                cmdEMPSAPID_Update.Parameters["I_EMPSAPID"].Value = I_EMPSAPID;

                DataTable dt = dbManager.ExecuteDataTable(cmdEMPSAPID_Update, "cmdEMPSAPID_Update");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region EmployeeHistorySelect
        public DataTable EmployeeHistorySelect(string PERS_CODE)
        {
            try
            {
                cmdEmployeeHistory.CommandText = @"SELECT DISTINCT TCOMPLAIN_LOCK_DRIVER.DETAIL,TUSER.SFIRSTNAME || ' '  || TUSER.SLASTNAME FULLNAME,TCOMPLAIN_LOCK_DRIVER.UPDATE_DATETIME FROM TCOMPLAIN_LOCK_DRIVER
  LEFT JOIN TUSER ON TCOMPLAIN_LOCK_DRIVER.UPDATE_BY = TUSER.SUID
  WHERE PERS_CODE = :PERS_CODE
  ORDER BY UPDATE_DATETIME DESC";
                cmdEmployeeHistory.Parameters.Clear();
                cmdEmployeeHistory.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "PERS_CODE",
                    OracleType = System.Data.OracleClient.OracleType.VarChar,
                    Value = PERS_CODE,
                });
                return dbManager.ExecuteDataTable(cmdEmployeeHistory, "cmdEmployeeHistory");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region VendorPKEditOrApprove
        public DataTable VendorPKEditOrApprove(string I_SUPDATE, string I_SDESCRIPTION, string I_REQ_ID, string I_STATUS)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorPKEditOrApprove.CommandType = CommandType.StoredProcedure;

                cmdVendorPKEditOrApprove.Parameters.Clear();
                cmdVendorPKEditOrApprove.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "I_SUPDATE",
                    Value = I_SUPDATE
                });
                cmdVendorPKEditOrApprove.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "I_SDESCRIPTION",
                    Value = I_SDESCRIPTION
                });
                cmdVendorPKEditOrApprove.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "I_REQ_ID",
                    Value = I_REQ_ID
                });

                cmdVendorPKEditOrApprove.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "I_STATUS",
                    Value = I_STATUS
                });
                cmdVendorPKEditOrApprove.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                    ParameterName = "O_CUR"
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdVendorPKEditOrApprove, "cmdVendorPKEditOrApprove");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region VendorSelectDAL
        public DataTable VendorSelectDAL(string Condition)
        {
            string Query = cmdVendorSelectDAL.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdVendorSelectDAL.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdVendorSelectDAL, "cmdVendorSelectDAL");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdVendorSelectDAL.CommandText = Query;
            }
        }
        #endregion

        #region LogInsert
        public bool LogInsert(string DETAIL, string PERS_CODE, DateTime? DATE_END, string STATUS, string DRIVER_STATUS_CHANGE_TO, int CREATE_BY, string SVENDORID, int CUSSCORE_TOTAL_DISABLE)
        {
            try
            {
                dbManager.Dispose();
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdLogInsert.CommandText = @"INSERT INTO TCOMPLAIN_LOCK_DRIVER(
                                          DETAIL, PERS_CODE,DATE_START,DATE_END,STATUS
                                              ,DRIVER_STATUS_CHANGE_TO
                                              ,CREATE_BY
                                              ,CREATE_DATETIME
                                              ,UPDATE_BY
                                              ,UPDATE_DATETIME
                                              ,SVENDORID
                                              ,CUSSCORE_TOTAL_DISABLE
                                              )
                                          VALUES (:DETAIL, :PERS_CODE,sysdate,:DATE_END,:STATUS
                                              ,:DRIVER_STATUS_CHANGE_TO
                                              ,:CREATE_BY
                                              ,sysdate
                                              ,:CREATE_BY
                                              ,sysdate
                                              ,:SVENDORID
                                              ,:CUSSCORE_TOTAL_DISABLE
                                              )";

                cmdLogInsert.Parameters.Clear();
                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "DETAIL",
                    Value = DETAIL
                });
                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "PERS_CODE",
                    Value = PERS_CODE
                });
                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.DateTime,
                    IsNullable = true,
                    ParameterName = "DATE_END",
                    Value = DATE_END == null ? (object)DBNull.Value : DATE_END.Value
                });

                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "STATUS",
                    Value = STATUS
                });
                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "DRIVER_STATUS_CHANGE_TO",
                    Value = DRIVER_STATUS_CHANGE_TO
                });
                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                    ParameterName = "CREATE_BY",
                    Value = CREATE_BY
                });
                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "SVENDORID",
                    Value = SVENDORID
                });
                cmdLogInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                    ParameterName = "CUSSCORE_TOTAL_DISABLE",
                    Value = CUSSCORE_TOTAL_DISABLE
                });
                bool isRes = false;
                int row = dbManager.ExecuteNonQuery(cmdLogInsert);
                if (row > 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;

            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }


        public bool driverJobLogInsert(string STATUS, string TYPE, string DETAIL, string CREATE_BY)
        {
            try
            {
                dbManager.Dispose();
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdLogDriverJobInsert.CommandText = @"INSERT INTO TDRIVERJOB_LOG(
                                               STATUS
                                              ,TYPE
                                              ,DETAIL
                                              ,CREATE_BY
                                              ,CREATE_DATETIME  
                                              )
                                          VALUES ( 
                                               :STATUS
                                              ,:TYPE
                                              ,:DETAIL
                                              ,:CREATE_BY
                                              ,sysdate
                                              )";

                cmdLogDriverJobInsert.Parameters.Clear();
                cmdLogDriverJobInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "STATUS",
                    Value = STATUS
                });
                cmdLogDriverJobInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "TYPE",
                    Value = TYPE
                });
                cmdLogDriverJobInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "DETAIL",
                    Value = DETAIL
                });
                cmdLogDriverJobInsert.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "CREATE_BY",
                    Value = CREATE_BY
                });

                bool isRes = false;
                int row = dbManager.ExecuteNonQuery(cmdLogDriverJobInsert);
                if (row > 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
                return isRes;

            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }


        #endregion


        #region GetEmpByVendor
        public DataTable GetEmpByVendor(string VENDORID)
        {
            try
            {
                cmdVendor.CommandType = CommandType.StoredProcedure;
                cmdVendor.CommandText = @"USP_T_EMPBYVENDORID_SELETE";
                cmdVendor.Parameters.Clear();
                cmdVendor.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    ParameterName = "I_VENDORID",
                    Value = VENDORID
                });
                cmdVendor.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                //cmdVendorList.Parameters["SEMPLOYEEID"].Value = SEMPLOYEEID;
                return dbManager.ExecuteDataTable(cmdVendor, "cmdVendor");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static VendorDAL _instance;
        public static VendorDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new VendorDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }

}
