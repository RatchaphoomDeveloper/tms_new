﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class KPIDAL : OracleConnectionDAL
    {
        #region + Instance +
        private static KPIDAL _instance;
        public static KPIDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new KPIDAL();
                //}
                return _instance;
            }
        }
        #endregion
        #region Component
        public KPIDAL()
        {
            InitializeComponent();
        }

        public KPIDAL(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }
        #endregion
        #region SELECT DATA
        #region GetVendorContrart
        public DataTable SetVendorContrart(string condition)
        {
            string query = cmdGetVendorContract.CommandText.ToString();
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdGetVendorContract.CommandText = @"SELECT  C.SCONTRACTID,V.SABBREVIATION, C.SCONTRACTNO FROM TCONTRACT C ,TVENDOR V WHERE C.SVENDORID = V.SVENDORID AND C.CACTIVE='Y'  AND V.INUSE='1' " + condition + " ORDER BY V.SABBREVIATION";
                return dbManager.ExecuteDataTable(cmdGetVendorContract, "cmdGetVendorContract");
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region GetFormReport
        public DataTable SetFormReport()
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdGetFormreport, "cmdGetFormreport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region GetReportById
        public DataTable SetReportById(string condition)
        {
            try
            {
                cmdGetreportById.Parameters.Add(":KPI_EVA_HEAD_ID", OracleType.VarChar).Value = condition;
                return dbManager.ExecuteDataTable(cmdGetreportById, "cmdGetreportById");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                throw;
            }
        }
        #endregion
        #region GetKpireport
        public DataTable SetKpiReport(string Year)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();

                return dbManager.ExecuteDataTable(cmdKpireport, "cmdKpireport");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region Get Kpiconfig ตามปี
        public DataTable SetSelectYearConfig(string Year)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdSelectYearConfig.Parameters.Add(":YEAR", OracleType.VarChar).Value = Year;
                return dbManager.ExecuteDataTable(cmdSelectYearConfig, "cmdSelectYearConfig");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region Get รายละเอียดKPI Manual
        public DataTable SetLoadFromKPIByYearMonth(string Year, string Month, string Contractid)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdKPIByYearMonth.CommandText = @"SELECT D.NO,H.NAME,H.KPI_EVA_HEAD_ID,D.KPI_EVA_EDTAIL_ID,D.EVA_NAME,
                    (
                      CASE FREQUENCY
                        WHEN 'Y'
                        THEN 'รายปี'
                        WHEN 'M'
                        THEN 'รายเดือน'
                      END)FREQUENCY,
                      (
                      CASE CALCULATE
                        WHEN 'Max'
                        THEN 'ค่าสูงสุด'
                        WHEN 'Min'
                        THEN 'ค่าต่ำสุด'
                        WHEN 'Avg'
                        THEN 'ค่าเฉลี่ย'
                      END)CALCULATE,D.RANGE1,D.RANGE2,D.RANGE3,D.RANGE4,D.RANGE5,D.WEIGHT,
                      (CASE FREQUENCY
                        WHEN 'Y'
                        THEN (
                            SELECT 'ผลรวมคะแนน ' || SUM(INPUT_DATA) FROM  KPI_SCORE S
                            WHERE S.KPI_EVA_EDTAIL_ID =D.KPI_EVA_EDTAIL_ID
                            AND S.CONTRACT_NO =:CONTRACT_NO 
                            AND S.YEAR =:YEAR AND S.MONTH <=:MONTH
                            GROUP BY S.KPI_EVA_EDTAIL_ID  )                  
                        END)sumtotal,
                      K.INPUT_DATA,K.SCORE
                    FROM KPI_CONFIG C
                    LEFT JOIN KPI_EVA_HEAD H
                    ON C.KPI_EVA_HEAD_ID=H.KPI_EVA_HEAD_ID
                    LEFT JOIN KPI_EVA_EDTAIL D
                    ON H.KPI_EVA_HEAD_ID =D.KPI_EVA_HEAD_ID
                    LEFT JOIN KPI_SCORE K
                    ON C.KPI_EVA_HEAD_ID=K.KPI_EVA_HEAD_ID AND K.CONTRACT_NO =:CONTRACT_NO AND K.MONTH=:MONTH AND K.YEAR =:YEAR
                    AND K.KPI_EVA_EDTAIL_ID = D.KPI_EVA_EDTAIL_ID
                    WHERE C.YEAR        =:YEAR
                    AND C.MONTH         =:MONTH
                    ORDER BY KPI_EVA_EDTAIL_ID";
                cmdKPIByYearMonth.Parameters.Add(":YEAR", OracleType.VarChar).Value = Year;
                cmdKPIByYearMonth.Parameters.Add(":MONTH", OracleType.VarChar).Value = Month;
                cmdKPIByYearMonth.Parameters.Add(":CONTRACT_NO", OracleType.VarChar).Value = Contractid;
                return dbManager.ExecuteDataTable(cmdKPIByYearMonth, "cmdKPIByYearMonth");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #endregion
        #region SaveKPIFORM
        public DataTable SetSaveDataKPI(DataTable kpi_eva_head, DataTable kpi_eva_edtail)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdSaveReportKPI.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet("kpi_eva_head_ds");
                kpi_eva_head.TableName = "kpi_eva_head";
                ds.Tables.Add(kpi_eva_head);
                cmdSaveReportKPI.Parameters["KPI_EVA_HEAD_CLOB"].Value = ds.GetXml();
                DataSet kpi_eva_edtail_ds = new DataSet("kpi_eva_edtail_ds");
                kpi_eva_edtail.TableName = "kpi_eva_edtail";
                kpi_eva_edtail_ds.Tables.Add(kpi_eva_edtail);
                cmdSaveReportKPI.Parameters["KPI_EVA_EDTAIL_CLOB"].Value = kpi_eva_edtail_ds.GetXml();
                dt = dbManager.ExecuteDataTable(cmdSaveReportKPI, "cmdSaveReportKPI");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region EditKPIFORM
        public DataTable SetEditDataKPI(string kpi_eva_head_id, DataTable kpi_eva_head, DataTable kpi_eva_edtail)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdEditReportKPI.CommandType = CommandType.StoredProcedure;
                cmdEditReportKPI.Parameters["KPI_EVA_ID"].Value = kpi_eva_head_id;
                DataSet ds = new DataSet("kpi_eva_head_ds");
                kpi_eva_head.TableName = "kpi_eva_head";
                ds.Tables.Add(kpi_eva_head);
                cmdEditReportKPI.Parameters["KPI_EVA_HEAD_CLOB"].Value = ds.GetXml();
                DataSet kpi_eva_edtail_ds = new DataSet("kpi_eva_edtail_ds");
                kpi_eva_edtail.TableName = "kpi_eva_edtail";
                kpi_eva_edtail_ds.Tables.Add(kpi_eva_edtail);
                cmdEditReportKPI.Parameters["KPI_EVA_EDTAIL_CLOB"].Value = kpi_eva_edtail_ds.GetXml();
                dt = dbManager.ExecuteDataTable(cmdEditReportKPI, "cmdEditReportKPI");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region SaveKPIConfig
        public DataTable SetSaveConfig(string kpi_eva_year, DataTable SetKpiReport)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdSaveKpiConfig.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet("kpiconfig_ds");
                SetKpiReport.TableName = "kpiconfig";
                ds.Tables.Add(SetKpiReport);
                cmdSaveKpiConfig.Parameters["KPI_EVA_YEAR"].Value = kpi_eva_year;
                cmdSaveKpiConfig.Parameters["KPI_EVA_CONFIG_CLOB"].Value = ds.GetXml();
                dt = dbManager.ExecuteDataTable(cmdSaveKpiConfig, "cmdSaveKpiConfig");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region SaveManualKpiScore
        public DataTable SetKpiScoreInsert(DataTable score_delete, DataTable DataManual)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdManualInSert.CommandType = CommandType.StoredProcedure;
                DataSet ds_delete = new DataSet("datamanual_ds");
                score_delete.TableName = "score_delete";
                ds_delete.Tables.Add(score_delete);
                cmdManualInSert.Parameters["KPI_SCORE_DELETE_CLOB"].Value = ds_delete.GetXml();
                DataSet ds = new DataSet("datamanual_ds");
                DataManual.TableName = "datamanual";
                ds.Tables.Add(DataManual);
                cmdManualInSert.Parameters["KPI_SCORE_CLOB"].Value = ds.GetXml();
                dt = dbManager.ExecuteDataTable(cmdManualInSert, "cmdManualInSert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion





        public DataTable SetAllContract()
        {
            try
            {
                DataTable dt = new DataTable();
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                return dbManager.ExecuteDataTable(cmdAllContract, "cmdAllContract");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }
        }



        public DataTable KPISelectDAL()
        {
            try
            {
                cmdKPISelect.CommandText = @"SELECT * FROM M_KPI_FORMULA";

                return dbManager.ExecuteDataTable(cmdKPISelect, "cmdKPISelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPISelectForEditDAL(int FormulaID)
        {
            try
            {
                cmdKPISelectForEdit.CommandText = @"SELECT M_KPI_FORMULA.FORMULA_ID, M_KPI_FORMULA.FORMULA_NAME, M_KPI_FORMULA.FORMULA_DETAIL, M_KPI_FORMULA.FREQUENCY
                                                         , M_KPI_VARIABLE.VARIABLE_ID, M_KPI_VARIABLE.VARIABLE_NAME, M_KPI_VARIABLE.DEFAULT_VALUE
                                                    FROM M_KPI_FORMULA INNER JOIN M_KPI_VARIABLE ON M_KPI_FORMULA.FORMULA_ID = M_KPI_VARIABLE.FORMULA_ID
                                                    WHERE M_KPI_FORMULA.FORMULA_ID =:I_FORMULA_ID";

                cmdKPISelectForEdit.Parameters["I_FORMULA_ID"].Value = FormulaID;

                return dbManager.ExecuteDataTable(cmdKPISelectForEdit, "cmdKPISelectForEdit");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void KPISaveDAL(int FormulaID, string VariableA, string ValueA, string VariableB, string ValueB, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdKPISaveValueA.CommandText = @"UPDATE M_KPI_VARIABLE
                                                 SET VARIABLE_NAME =:I_VALIABLE_NAME, DEFAULT_VALUE =:I_VALUE, UPDATE_BY =:I_UPDATE_BY
                                                 WHERE FORMULA_ID =:I_FORMULA_ID
                                                 AND VARIABLE_FIX = 'A'";

                cmdKPISaveValueA.Parameters["I_FORMULA_ID"].Value = FormulaID;
                cmdKPISaveValueA.Parameters["I_VALIABLE_NAME"].Value = VariableA;
                cmdKPISaveValueA.Parameters["I_VALUE"].Value = ValueA;
                cmdKPISaveValueA.Parameters["I_UPDATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdKPISaveValueA);

                cmdKPISaveValueB.CommandText = @"UPDATE M_KPI_VARIABLE
                                                 SET VARIABLE_NAME =:I_VALIABLE_NAME, DEFAULT_VALUE =:I_VALUE, UPDATE_BY =:I_UPDATE_BY
                                                 WHERE FORMULA_ID =:I_FORMULA_ID
                                                 AND VARIABLE_FIX = 'B'";

                cmdKPISaveValueB.Parameters["I_FORMULA_ID"].Value = FormulaID;
                cmdKPISaveValueB.Parameters["I_VALIABLE_NAME"].Value = VariableB;
                cmdKPISaveValueB.Parameters["I_VALUE"].Value = ValueB;
                cmdKPISaveValueB.Parameters["I_UPDATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdKPISaveValueB);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void KPISaveTransactionDAL(string Year, int MonthID, string VendorID, int ContractID, DataTable dtData, int StatusID, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                DataSet ds = new DataSet("ds");
                dtData.TableName = "dt";
                ds.Tables.Add(dtData);

                cmdKPISaveTransaction.CommandText = "USP_T_KPI_INSERT";
                cmdKPISaveTransaction.CommandType = CommandType.StoredProcedure;

                cmdKPISaveTransaction.Parameters["I_YEAR"].Value = Year;
                cmdKPISaveTransaction.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdKPISaveTransaction.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdKPISaveTransaction.Parameters["I_CONTRACT_ID"].Value = ContractID;
                cmdKPISaveTransaction.Parameters["I_DATA"].Value = ds.GetXml();
                cmdKPISaveTransaction.Parameters["I_STATUS_ID"].Value = StatusID;
                cmdKPISaveTransaction.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdKPISaveTransaction);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void KPISaveTransactionBatchDAL(string Year, int MonthID, int TopicHeaderID, DataTable dtData, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                DataTable dtKPI = new DataTable();
                dtKPI.Columns.Add("TOPIC_HEADER_ID");
                dtKPI.Columns.Add("TOPIC_DETAIL_ID");
                dtKPI.Columns.Add("VALUE_A");
                dtKPI.Columns.Add("VALUE_B");
                dtKPI.Columns.Add("POINT");

                string TopicDetailID = string.Empty;
                string ValueA = string.Empty;
                string ValueB = string.Empty;
                for (int i = 1; i < dtData.Rows.Count; i++)
                {
                    dtKPI.Rows.Clear();

                    for (int j = 4; j < dtData.Columns.Count - 1; j = j + 2)
			        {
                        TopicDetailID = dtData.Columns[j].ColumnName.Replace("_A", string.Empty);
                        ValueA = dtData.Rows[i][j].ToString();
                        ValueB = dtData.Rows[i][j + 1].ToString();

                        if (!string.Equals(ValueA, string.Empty))
                            dtKPI.Rows.Add(TopicHeaderID, TopicDetailID, ValueA, ValueB, string.Empty);
			        }

                    DataSet ds = new DataSet("ds");
                    dtKPI.TableName = "dt";
                    ds.Tables.Add(dtKPI.Copy());

                    if (dtKPI.Rows.Count > 0)
                    {
                        cmdKPISaveTransaction.CommandText = "USP_T_KPI_INSERT";
                        cmdKPISaveTransaction.CommandType = CommandType.StoredProcedure;

                        cmdKPISaveTransaction.Parameters["I_YEAR"].Value = Year;
                        cmdKPISaveTransaction.Parameters["I_MONTH_ID"].Value = MonthID;
                        cmdKPISaveTransaction.Parameters["I_VENDOR_ID"].Value = dtData.Rows[i]["SVENDORID"].ToString();
                        cmdKPISaveTransaction.Parameters["I_CONTRACT_ID"].Value = dtData.Rows[i]["SCONTRACTID"].ToString();
                        cmdKPISaveTransaction.Parameters["I_DATA"].Value = ds.GetXml();
                        cmdKPISaveTransaction.Parameters["I_STATUS_ID"].Value = dtData.Rows[i]["STATUS_ID"].ToString();
                        cmdKPISaveTransaction.Parameters["I_CREATE_BY"].Value = CreateBy;

                        dbManager.ExecuteNonQuery(cmdKPISaveTransaction);
                    }

                    dbManager.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable KPIEvaluationSelectDAL(string Condition)
        {
            string Query = cmdKPIEvaluationSelect.CommandText;
            try
            {
                dbManager.Open();

                cmdKPIEvaluationSelect.CommandText = @"SELECT TMP.*, TUSER.SFIRSTNAME || ' ' || TUSER.SLASTNAME AS FULLNAME
                                                        FROM
                                                        (
                                                            SELECT M_KPI_TOPIC_HEADER.TOPIC_HEADER_ID, M_KPI_TOPIC_HEADER.HEADER_NAME, M_KPI_TOPIC_HEADER.HEADER_DETAIL
                                                                  , CASE WHEN NVL(UPDATE_BY, -1) = -1 THEN CREATE_BY ELSE UPDATE_BY END AS UPDATE_BY
                                                                  , CASE WHEN NVL(UPDATE_BY, -1) = -1 THEN TO_CHAR(CREATE_DATETIME, 'DD/MM/YYYY HH24:MM', 'NLS_DATE_LANGUAGE = ENGLISH') ELSE TO_CHAR(UPDATE_DATETIME, 'DD/MM/YYYY HH24:MM', 'NLS_DATE_LANGUAGE = ENGLISH') END AS UPDATE_DATETIME
                                                                  , CASE WHEN M_KPI_TOPIC_HEADER.IS_ACTIVE = 1 THEN 'Active' ELSE 'InActive' END AS IS_ACTIVE
                                                             FROM M_KPI_TOPIC_HEADER
                                                        ) TMP LEFT JOIN TUSER ON TMP.UPDATE_BY = TUSER.SUID
                                                        WHERE 1=1";

                cmdKPIEvaluationSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdKPIEvaluationSelect, "cmdKPIEvaluationSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdKPIEvaluationSelect.CommandText = Query;
            }
        }

        public DataTable LoadMethodDAL(string Condition)
        {
            string Query = cmdLoadMethod.CommandText;
            try
            {
                cmdLoadMethod.CommandText = @"SELECT * FROM M_KPI_METHOD WHERE 1=1 ";

                cmdLoadMethod.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdLoadMethod, "cmdLoadMethod");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdLoadMethod.CommandText = Query;
            }
        }

        public DataTable LoadFormulaDAL(string Condition)
        {
            string Query = cmdLoadFormula.CommandText;
            try
            {
                cmdLoadFormula.CommandText = @"SELECT * FROM M_KPI_FORMULA WHERE 1=1 ";

                cmdLoadFormula.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdLoadFormula, "cmdLoadFormula");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdLoadFormula.CommandText = Query;
            }
        }

        public void KPIFormSaveDAL(int TopicHeaderID, string FormName, string Detail, int IsActive, DataTable dtTopic, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdKPIFormSave.CommandType = CommandType.StoredProcedure;
                cmdKPIFormSave.CommandText = @"USP_M_KPI_FORM_SAVE";

                DataSet ds = new DataSet("ds");
                dtTopic.TableName = "dt";
                ds.Tables.Add(dtTopic.Copy());

                cmdKPIFormSave.Parameters["I_TOPIC_HEADER_ID"].Value = TopicHeaderID;
                cmdKPIFormSave.Parameters["I_FORM_NAME"].Value = FormName;
                cmdKPIFormSave.Parameters["I_DETAIL"].Value = Detail;
                cmdKPIFormSave.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdKPIFormSave.Parameters["I_XML"].Value = ds.GetXml();
                cmdKPIFormSave.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdKPIFormSave);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable KPIFormSelectDAL(int TopicHeaderID)
        {
            try
            {
                cmdKPIFormSelect.CommandText = @"SELECT HEADER.HEADER_NAME, HEADER.HEADER_DETAIL, HEADER.IS_ACTIVE
                                                     , DETAIL.FORMULA_ID, DETAIL.METHOD_ID, DETAIL.TOPIC_NAME, M_KPI_FORMULA.FORMULA_NAME
                                                     , TO_CHAR(DETAIL.LEVEL1_START) LEVEL1_START, TO_CHAR(DETAIL.LEVEL1_END) LEVEL1_END
                                                     , TO_CHAR(DETAIL.LEVEL2_START) LEVEL2_START, TO_CHAR(DETAIL.LEVEL2_END) LEVEL2_END
                                                     , TO_CHAR(DETAIL.LEVEL3_START) LEVEL3_START, TO_CHAR(DETAIL.LEVEL3_END) LEVEL3_END
                                                     , TO_CHAR(DETAIL.LEVEL4_START) LEVEL4_START, TO_CHAR(DETAIL.LEVEL4_END) LEVEL4_END
                                                     , TO_CHAR(DETAIL.LEVEL5_START) LEVEL5_START, TO_CHAR(DETAIL.LEVEL5_END) LEVEL5_END
                                                     , DETAIL.WEIGHT
                                                FROM M_KPI_TOPIC_HEADER HEADER INNER JOIN M_KPI_TOPIC_DETAIL DETAIL ON HEADER.TOPIC_HEADER_ID = DETAIL.TOPIC_HEADER_ID
                                                                               INNER JOIN M_KPI_FORMULA ON DETAIL.FORMULA_ID = M_KPI_FORMULA.FORMULA_ID
                                                WHERE HEADER.TOPIC_HEADER_ID =:I_TOPIC_HEADER_ID";

                cmdKPIFormSelect.Parameters["I_TOPIC_HEADER_ID"].Value = TopicHeaderID;

                DataTable dtKPIForm = dbManager.ExecuteDataTable(cmdKPIFormSelect, "cmdKPIFormSelect");
                return dtKPIForm;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIMappingFormSelectDAL(string Year, int MonthID, string VendorID, int ContractID, string Condition)
        {
            try
            {
                cmdKPIMappingFormSelect.CommandText = @"SELECT TMP.*, TMP2.VARIABLE_A AS VALUE_A, TMP2.VARIABLE_B AS VALUE_B, TMP2.POINT, '' AS POINT_BEFORE
                                                        FROM
                                                        (
                                                            SELECT HEADER.TOPIC_HEADER_ID, DETAIL.TOPIC_DETAIL_ID, DETAIL.FORMULA_ID, DETAIL.METHOD_ID, M_KPI_METHOD.METHOD_NAME, DETAIL.TOPIC_NAME, M_KPI_FORMULA.FORMULA_NAME, M_KPI_FORMULA.FREQUENCY
                                                                 , DETAIL.LEVEL1_START || '-' || DETAIL.LEVEL1_END AS LEVEL1
                                                                 , DETAIL.LEVEL2_START || '-' || DETAIL.LEVEL2_END AS LEVEL2
                                                                 , DETAIL.LEVEL3_START || '-' || DETAIL.LEVEL3_END AS LEVEL3
                                                                 , DETAIL.LEVEL4_START || '-' || DETAIL.LEVEL4_END AS LEVEL4
                                                                 , DETAIL.LEVEL5_START || '-' || DETAIL.LEVEL5_END AS LEVEL5
                                                                 , DETAIL.WEIGHT
                                                                 , (SELECT VARIABLE_NAME FROM M_KPI_VARIABLE WHERE FORMULA_ID = M_KPI_FORMULA.FORMULA_ID AND VARIABLE_FIX = 'A') AS VARIABLE_A
                                                                 , (SELECT VARIABLE_NAME FROM M_KPI_VARIABLE WHERE FORMULA_ID = M_KPI_FORMULA.FORMULA_ID AND VARIABLE_FIX = 'B') AS VARIABLE_B
                                                                 , HEADER.HEADER_DETAIL
                                                                 , DETAIL.LEVEL1_START, DETAIL.LEVEL1_END
                                                                 , DETAIL.LEVEL2_START, DETAIL.LEVEL2_END
                                                                 , DETAIL.LEVEL3_START, DETAIL.LEVEL3_END
                                                                 , DETAIL.LEVEL4_START, DETAIL.LEVEL4_END
                                                                 , DETAIL.LEVEL5_START, DETAIL.LEVEL5_END
                                                            FROM M_KPI_TOPIC_HEADER HEADER INNER JOIN M_KPI_TOPIC_DETAIL DETAIL ON HEADER.TOPIC_HEADER_ID = DETAIL.TOPIC_HEADER_ID
                                                                                           INNER JOIN M_KPI_FORMULA ON DETAIL.FORMULA_ID = M_KPI_FORMULA.FORMULA_ID
                                                                                           INNER JOIN M_KPI_METHOD ON DETAIL.METHOD_ID = M_KPI_METHOD.METHOD_ID
                                                            WHERE HEADER.TOPIC_HEADER_ID = (SELECT TOPIC_HEADER_ID FROM M_KPI_MAPPING_FORM WHERE YEAR=:I_YEAR AND MONTH_ID=:I_MONTH_ID)
                                                        ) TMP 
                                                          LEFT JOIN 
                                                          (
                                                              SELECT DETAIL.TOPIC_HEADER_ID, DETAIL.TOPIC_DETAIL_ID, TO_CHAR(DETAIL.VARIABLE_A) AS VARIABLE_A, TO_CHAR(DETAIL.VARIABLE_B) AS VARIABLE_B, TO_CHAR(POINT) AS POINT
                                                              FROM T_KPI_HEADER HEADER INNER JOIN T_KPI_DETAIL DETAIL ON HEADER.KPI_HEADER_ID = DETAIL.KPI_HEADER_ID
                                                              WHERE HEADER.YEAR =:I_YEAR
                                                              AND HEADER.MONTH_ID =:I_MONTH_ID
                                                              AND HEADER.VENDOR_ID =:I_VENDOR_ID
                                                              AND HEADER.CONTRACT_ID =:I_CONTRACT_ID
                                                          ) TMP2 ON TMP.TOPIC_HEADER_ID = TMP2.TOPIC_HEADER_ID AND TMP.TOPIC_DETAIL_ID = TMP2.TOPIC_DETAIL_ID
                                                        WHERE 1=1" + Condition +
                                                        " ORDER BY TMP.TOPIC_DETAIL_ID";

                cmdKPIMappingFormSelect.Parameters["I_YEAR"].Value = Year;
                cmdKPIMappingFormSelect.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdKPIMappingFormSelect.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdKPIMappingFormSelect.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dtKPIForm = dbManager.ExecuteDataTable(cmdKPIMappingFormSelect, "cmdKPIMappingFormSelect");
                return dtKPIForm;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIMappingFormSelect2DAL(string Year, int MonthID, string VendorID, int ContractID, int TopicHeaderID, bool IsPTT)
        {
            try
            {
                cmdKPIMappingFormSelect2.CommandText = @"SELECT TMP.*, TMP2.VARIABLE_A AS VALUE_A, TMP2.VARIABLE_B AS VALUE_B, TMP2.POINT
                                                        FROM
                                                        (
                                                            SELECT HEADER.TOPIC_HEADER_ID, DETAIL.TOPIC_DETAIL_ID, DETAIL.FORMULA_ID, DETAIL.METHOD_ID, M_KPI_METHOD.METHOD_NAME, DETAIL.TOPIC_NAME, M_KPI_FORMULA.FORMULA_NAME, M_KPI_FORMULA.FREQUENCY
                                                                 , DETAIL.LEVEL1_START || '-' || DETAIL.LEVEL1_END AS LEVEL1
                                                                 , DETAIL.LEVEL2_START || '-' || DETAIL.LEVEL2_END AS LEVEL2
                                                                 , DETAIL.LEVEL3_START || '-' || DETAIL.LEVEL3_END AS LEVEL3
                                                                 , DETAIL.LEVEL4_START || '-' || DETAIL.LEVEL4_END AS LEVEL4
                                                                 , DETAIL.LEVEL5_START || '-' || DETAIL.LEVEL5_END AS LEVEL5
                                                                 , DETAIL.WEIGHT
                                                                 , (SELECT VARIABLE_NAME FROM M_KPI_VARIABLE WHERE FORMULA_ID = M_KPI_FORMULA.FORMULA_ID AND VARIABLE_FIX = 'A') AS VARIABLE_A
                                                                 , (SELECT VARIABLE_NAME FROM M_KPI_VARIABLE WHERE FORMULA_ID = M_KPI_FORMULA.FORMULA_ID AND VARIABLE_FIX = 'B') AS VARIABLE_B
                                                                 , HEADER.HEADER_DETAIL
                                                                 , DETAIL.LEVEL1_START, DETAIL.LEVEL1_END
                                                                 , DETAIL.LEVEL2_START, DETAIL.LEVEL2_END
                                                                 , DETAIL.LEVEL3_START, DETAIL.LEVEL3_END
                                                                 , DETAIL.LEVEL4_START, DETAIL.LEVEL4_END
                                                                 , DETAIL.LEVEL5_START, DETAIL.LEVEL5_END
                                                            FROM M_KPI_TOPIC_HEADER HEADER INNER JOIN M_KPI_TOPIC_DETAIL DETAIL ON HEADER.TOPIC_HEADER_ID = DETAIL.TOPIC_HEADER_ID
                                                                                           INNER JOIN M_KPI_FORMULA ON DETAIL.FORMULA_ID = M_KPI_FORMULA.FORMULA_ID
                                                                                           INNER JOIN M_KPI_METHOD ON DETAIL.METHOD_ID = M_KPI_METHOD.METHOD_ID
                                                            WHERE HEADER.TOPIC_HEADER_ID = (SELECT TOPIC_HEADER_ID FROM M_KPI_MAPPING_FORM WHERE YEAR=:I_YEAR AND MONTH_ID=:I_MONTH_ID)
                                                        ) TMP 
                                                          INNER JOIN 
                                                          (
                                                              SELECT DETAIL.TOPIC_HEADER_ID, DETAIL.TOPIC_DETAIL_ID, DETAIL.VARIABLE_A, DETAIL.VARIABLE_B, POINT
                                                              FROM T_KPI_HEADER HEADER INNER JOIN T_KPI_DETAIL DETAIL ON HEADER.KPI_HEADER_ID = DETAIL.KPI_HEADER_ID
                                                              WHERE HEADER.YEAR =:I_YEAR
                                                              AND DETAIL.TOPIC_HEADER_ID =:I_TOPIC_HEADER_ID
                                                              AND HEADER.MONTH_ID =:I_MONTH_ID
                                                              AND HEADER.VENDOR_ID =:I_VENDOR_ID
                                                              AND HEADER.CONTRACT_ID =:I_CONTRACT_ID
                                                              AND HEADER.STATUS_ID = 3
                                                          ) TMP2 ON TMP.TOPIC_HEADER_ID = TMP2.TOPIC_HEADER_ID AND TMP.TOPIC_DETAIL_ID = TMP2.TOPIC_DETAIL_ID
                                                        ORDER BY TMP.TOPIC_DETAIL_ID";

                if (IsPTT)
                    cmdKPIMappingFormSelect2.CommandText = cmdKPIMappingFormSelect2.CommandText.Replace("AND HEADER.STATUS_ID = 3", "AND HEADER.STATUS_ID IN (2, 3)");

                cmdKPIMappingFormSelect2.Parameters["I_TOPIC_HEADER_ID"].Value = TopicHeaderID;
                cmdKPIMappingFormSelect2.Parameters["I_YEAR"].Value = Year;
                cmdKPIMappingFormSelect2.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdKPIMappingFormSelect2.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdKPIMappingFormSelect2.Parameters["I_CONTRACT_ID"].Value = ContractID;

                DataTable dtKPIForm = dbManager.ExecuteDataTable(cmdKPIMappingFormSelect2, "cmdKPIMappingFormSelect2");
                return dtKPIForm;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIFindSumDAL(string Year, string VendorID, int ContractID, int MonthID, int FormulaID, int TopicHeaderID, int TopicDetailID)
        {
            try
            {
                cmdKPIFindSum.CommandText = "USP_T_KPI_ACCIDENT_SUM";
                cmdKPIFindSum.CommandType = CommandType.StoredProcedure;

                cmdKPIFindSum.Parameters["I_YEAR"].Value = Year;
                cmdKPIFindSum.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdKPIFindSum.Parameters["I_CONTRACT_ID"].Value = ContractID;
                cmdKPIFindSum.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdKPIFindSum.Parameters["I_FORMULA_ID"].Value = FormulaID;
                cmdKPIFindSum.Parameters["I_TOPIC_HEADER_ID"].Value = TopicHeaderID;
                cmdKPIFindSum.Parameters["I_TOPIC_DETAIL_ID"].Value = TopicDetailID;

                DataTable dt = dbManager.ExecuteDataTable(cmdKPIFindSum, "cmdKPIFindSum");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPISelectListHeaderDAL(string Year)
        {
            try
            {
                cmdKPISelectListHeader.CommandText = @"SELECT DISTINCT TOPIC_HEADER_ID
                                                       FROM M_KPI_MAPPING_FORM
                                                       WHERE YEAR =:I_YEAR
                                                       AND TOPIC_HEADER_ID <> 0
                                                       ORDER BY TOPIC_HEADER_ID";

                cmdKPISelectListHeader.Parameters["I_YEAR"].Value = Year;

                DataTable dt = dbManager.ExecuteDataTable(cmdKPISelectListHeader, "cmdKPISelectListHeader");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPISelectListDetailDAL(int TopicHeaderID)
        {
            try
            {
                cmdKPISelectListDetail.CommandText = @"SELECT HEADER.TOPIC_HEADER_ID, DETAIL.TOPIC_DETAIL_ID, DETAIL.FORMULA_ID, DETAIL.METHOD_ID, M_KPI_METHOD.METHOD_NAME, DETAIL.TOPIC_NAME, M_KPI_FORMULA.FORMULA_NAME
                                                             , M_KPI_FORMULA.FREQUENCY, DETAIL.WEIGHT
                                                             , '' AS MONTH_1, '' AS MONTH_2, '' AS MONTH_3, '' AS MONTH_4, '' AS MONTH_5, '' AS MONTH_6
                                                             , '' AS MONTH_7, '' AS MONTH_8, '' AS MONTH_9, '' AS MONTH_10, '' AS MONTH_11, '' AS MONTH_12
                                                             , '' AS POINT
                                                             , HEADER.HEADER_NAME
                                                        FROM M_KPI_TOPIC_HEADER HEADER INNER JOIN M_KPI_TOPIC_DETAIL DETAIL ON HEADER.TOPIC_HEADER_ID = DETAIL.TOPIC_HEADER_ID
                                                                                       INNER JOIN M_KPI_METHOD ON DETAIL.METHOD_ID = M_KPI_METHOD.METHOD_ID
                                                                                       INNER JOIN M_KPI_FORMULA ON DETAIL.FORMULA_ID = M_KPI_FORMULA.FORMULA_ID
                                                        WHERE HEADER.TOPIC_HEADER_ID =:I_TOPIC_HEADER_ID
                                                        ORDER BY DETAIL.TOPIC_DETAIL_ID";

                cmdKPISelectListDetail.Parameters["I_TOPIC_HEADER_ID"].Value = TopicHeaderID;

                DataTable dt = dbManager.ExecuteDataTable(cmdKPISelectListDetail, "cmdKPISelectListDetail");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIExportSelectDAL()
        {
            try
            {
                cmdKPIExportSelect.CommandText = @"SELECT ' ' AS NO, TCONTRACT.SCONTRACTNO, TCONTRACT.SCONTRACTID, TVENDOR.SVENDORID
                                                   FROM TVENDOR LEFT JOIN TCONTRACT ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID
                                                   WHERE TVENDOR.CACTIVE = '1'
                                                   AND TCONTRACT.CACTIVE = 'Y'
                                                   GROUP BY TVENDOR.SABBREVIATION, TCONTRACT.SCONTRACTNO, TVENDOR.SVENDORID, TCONTRACT.SCONTRACTID
                                                   ORDER BY LENGTH(TCONTRACT.SCONTRACTNO), TCONTRACT.SCONTRACTNO";

                DataTable dt = dbManager.ExecuteDataTable(cmdKPIExportSelect, "cmdKPIExportSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPIExportFormSelectDAL(int TopicHeaderID)
        {
            try
            {
                cmdKPIExportFormSelect.CommandText = @"SELECT HEADER.TOPIC_HEADER_ID, DETAIL.TOPIC_NAME, M_KPI_VARIABLE.VARIABLE_NAME, M_KPI_VARIABLE.VARIABLE_FIX, M_KPI_VARIABLE.DEFAULT_VALUE
                                                              , DETAIL.FORMULA_ID, DETAIL.TOPIC_DETAIL_ID
                                                       FROM M_KPI_TOPIC_HEADER HEADER INNER JOIN M_KPI_TOPIC_DETAIL DETAIL ON HEADER.TOPIC_HEADER_ID = DETAIL.TOPIC_HEADER_ID
                                                                                      INNER JOIN M_KPI_FORMULA ON DETAIL.FORMULA_ID = M_KPI_FORMULA.FORMULA_ID
                                                                                      INNER JOIN M_KPI_VARIABLE ON DETAIL.FORMULA_ID = M_KPI_VARIABLE.FORMULA_ID
                                                       WHERE HEADER.TOPIC_HEADER_ID =:I_TOPIC_HEADER_ID
                                                       ORDER BY DETAIL.TOPIC_DETAIL_ID, M_KPI_VARIABLE.VARIABLE_ID";

                cmdKPIExportFormSelect.Parameters["I_TOPIC_HEADER_ID"].Value = TopicHeaderID;

                return dbManager.ExecuteDataTable(cmdKPIExportFormSelect, "cmdKPIExportFormSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPICheckDAL(int TopicHeaderID)
        {
            try
            {
                cmdKPICheck.CommandText = @"SELECT COUNT(*) AS COUNT
                                             FROM T_KPI_HEADER HEADER INNER JOIN T_KPI_DETAIL DETAIL ON HEADER.KPI_HEADER_ID = DETAIL.KPI_HEADER_ID
                                             WHERE DETAIL.TOPIC_HEADER_ID =:I_TOPIC_HEADER_ID
                                             AND HEADER.STATUS_ID IN (3, 4)";

                cmdKPICheck.Parameters["I_TOPIC_HEADER_ID"].Value = TopicHeaderID;

                return dbManager.ExecuteDataTable(cmdKPICheck, "cmdKPICheck");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTotalTruckDAL(string Year, int MonthID, int ContractID)
        {
            try
            {
//                cmdGetTotalTruck.CommandText = @"SELECT *
//                                                 FROM VW_M_CONTRACT_TRUCK_SUM
//                                                 WHERE 1=1 " + Condition;

                cmdGetTotalTruck.CommandText = @"SELECT TMP.*
                                                FROM
                                                (
                                                    SELECT TTRUCKCONFIRM.SCONTRACTID, TTRUCKCONFIRM.DDATE, SUM(NVL(TTRUCKCONFIRM.NUNAVAILABLE,0) + NVL(TTRUCKCONFIRM.NTRANSIT,0) + NVL(TTRUCKCONFIRM.NAVAILABLE,0)) AS TOTAL_TRUCK
                                                    FROM TTRUCKCONFIRM
                                                    WHERE TO_CHAR(TTRUCKCONFIRM.DDATE, 'YYYY') =:I_YEAR
                                                    AND TO_NUMBER(TO_CHAR(TTRUCKCONFIRM.DDATE, 'MM')) =:I_MONTH_ID
                                                    AND TTRUCKCONFIRM.SCONTRACTID =:I_CONTRACT_ID
                                                    AND TTRUCKCONFIRM.CCONFIRM IS NOT NULL
                                                    GROUP BY TTRUCKCONFIRM.SCONTRACTID, TTRUCKCONFIRM.DDATE
                                                ) TMP
                                                ORDER BY NVL(TMP.TOTAL_TRUCK, 0) DESC";

                cmdGetTotalTruck.Parameters["I_YEAR"].Value = Year;
                cmdGetTotalTruck.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdGetTotalTruck.Parameters["I_CONTRACT_ID"].Value = ContractID;

                return dbManager.ExecuteDataTable(cmdGetTotalTruck, "cmdGetTotalTruck");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTotalAccidentDAL(string Year, int MonthID, int ContractID, string VendorID, string Condition)
        {
            try
            {
                cmdGetTotalAccident.CommandText = @"SELECT TMP.YEAR, TMP.MONTH_ID, TMP.SCONTRACTID, TMP.SCONTRACTNO, TMP.SVENDORID, TMP.SABBREVIATION 
                                                        , NVL(SUM(TMP.TOTAL), 0) AS TOTAL
                                                    FROM
                                                    (
                                                        SELECT TO_CHAR(ACC_ACCIDENT.ACCIDENT_DATE,'YYYY') AS YEAR
                                                                , TO_NUMBER(TO_CHAR(ACC_ACCIDENT.ACCIDENT_DATE,'MM')) AS MONTH_ID
                                                                , TCONTRACT.SCONTRACTID
                                                                , TCONTRACT.SCONTRACTNO
                                                                , TVENDOR.SVENDORID
                                                                , TVENDOR.SABBREVIATION
                                                                , ACC_ACCIDENT.ACCIDENT_DATE
                                                                , 1 AS TOTAL
                                                        FROM TVENDOR LEFT JOIN TCONTRACT ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID
                                                                        LEFT JOIN ACC_DETAIL ON ACC_DETAIL.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                                        LEFT JOIN ACC_ACCIDENT ON ACC_DETAIL.ACCIDENT_ID = ACC_ACCIDENT.ACCIDENT_ID
                                                                        LEFT JOIN ACC_APPROVEEVENT ON ACC_APPROVEEVENT.ACCIDENT_ID = ACC_ACCIDENT.ACCIDENT_ID
                                                        WHERE TVENDOR.CACTIVE = '1'
                                                        AND TCONTRACT.CACTIVE = 'Y'
                                                        AND ACC_ACCIDENT.ACCIDENT_DATE IS NOT NULL
                                                        AND ACC_APPROVEEVENT.SERIOUS = 0
                                                    ) TMP
                                                    WHERE 1=1
                                                    AND TMP.YEAR =:I_YEAR
                                                    AND TMP.MONTH_ID =:I_MONTH_ID
                                                    AND TMP.SCONTRACTID =:I_CONTRACT_ID
                                                    AND TMP.SVENDORID =:I_VENDOR_ID
                                                    GROUP BY TMP.YEAR, TMP.MONTH_ID, TMP.SCONTRACTID, TMP.SCONTRACTNO, TMP.SVENDORID, TMP.SABBREVIATION " + Condition;

                cmdGetTotalAccident.Parameters["I_YEAR"].Value = Year;
                cmdGetTotalAccident.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdGetTotalAccident.Parameters["I_CONTRACT_ID"].Value = ContractID;
                cmdGetTotalAccident.Parameters["I_VENDOR_ID"].Value = VendorID;

                return dbManager.ExecuteDataTable(cmdGetTotalAccident, "cmdGetTotalAccident");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTotalIVMSDAL(string Year, int MonthID, int ContractID)
        {
            try
            {
                cmdGetTotalIVMS.CommandText = @"SELECT TCONTRACT.SCONTRACTID
                                                        , TMP.CONTRACT_NAME
                                                        , CASE WHEN SUM(TMP.CAR_FAST) = 0 THEN NULL ELSE SUM(TMP.CAR_FAST) END AS CAR_FAST
                                                        , CASE WHEN SUM(TMP.OVER_HOUR) = 0 THEN NULL ELSE SUM(TMP.OVER_HOUR) END AS OVER_HOUR
                                                        , CASE WHEN SUM(TMP.SUDDEN) = 0 THEN NULL ELSE SUM(TMP.SUDDEN) END AS SUDDEN
                                                        , TMP.REPORT_DATE, TMP.REPORT_DATE_DISPLAY
                                                FROM
                                                (
                                                    SELECT IVMS_KPI.REPORT_DATE
                                                            , TO_CHAR(IVMS_KPI.REPORT_DATE, 'DD/MM/YYYY') AS REPORT_DATE_DISPLAY
                                                            , IVMS_KPI.CONTRACT_NAME
                                                            , IVMS_KPI.LICENSE_PLATE
                                                            , NVL(IVMS_KPI.OVERSPEED_TR, 0) + NVL(IVMS_KPI.OVERSPEED_SM, 0) AS CAR_FAST
                                                            , NVL(IVMS_KPI.UNREGIST_CARD, 0) + NVL(IVMS_KPI.DRIVE_4HRS, 0) + NVL(IVMS_KPI.DRIVE_8HRS, 0) AS OVER_HOUR
                                                            , NVL(IVMS_KPI.SUDDEN_ACC, 0) + NVL(IVMS_KPI.SUDDEN_BRAKE, 0) AS SUDDEN
                                                            , TO_CHAR(IVMS_KPI.REPORT_DATE, 'YYYY') AS YEAR
                                                            , TO_NUMBER(TO_CHAR(IVMS_KPI.REPORT_DATE, 'MM')) AS MONTH_ID
                                                    FROM IVMS_KPI INNER JOIN IVMS_KPI_LASTVERSION ON IVMS_KPI.REPORT_DATE = IVMS_KPI_LASTVERSION.REPORT_DATE AND IVMS_KPI.KPI_VERSION = IVMS_KPI_LASTVERSION.KPI_VERSION
                                                    WHERE TO_CHAR(IVMS_KPI.REPORT_DATE, 'YYYY') =:I_YEAR
                                                    AND TO_NUMBER(TO_CHAR(IVMS_KPI.REPORT_DATE, 'MM')) =:I_MONTH_ID
                                                    AND IVMS_KPI.CONTRACT_NAME IS NOT NULL
                                                ) TMP INNER JOIN TCONTRACT ON TMP.CONTRACT_NAME = TCONTRACT.SCONTRACTNO
                                                WHERE TCONTRACT.SCONTRACTID =:I_CONTRACT_ID
                                                GROUP BY TCONTRACT.SCONTRACTID, TMP.CONTRACT_NAME, TMP.REPORT_DATE, TMP.REPORT_DATE_DISPLAY
                                                ORDER BY LENGTH(CONTRACT_NAME), CONTRACT_NAME";

                cmdGetTotalIVMS.Parameters["I_YEAR"].Value = Year;
                cmdGetTotalIVMS.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdGetTotalIVMS.Parameters["I_CONTRACT_ID"].Value = ContractID;

                return dbManager.ExecuteDataTable(cmdGetTotalIVMS, "cmdGetTotalIVMS");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetShipmentCostDataDAL(string Year, int MonthID, int ContractID)
        {
            try
            {
                cmdGetShipmentCostData.CommandText = @"SELECT YEAR_EN, MONTH_ID, CONTRACT_ID, LOSS_QTY, DISTANCE,DELIVERY_QTY
                                                            , CASE WHEN UPDATE_DATETIME IS NULL THEN CREATE_DATETIME ELSE UPDATE_DATETIME END AS UPDATE_DATETIME
                                                       FROM T_SHIPMENT_COST_JOB
                                                       WHERE YEAR_EN =:I_YEAR
                                                       AND MONTH_ID =:I_MONTH_ID
                                                       AND CONTRACT_ID =:I_CONTRACT_ID";

                cmdGetShipmentCostData.Parameters["I_YEAR"].Value = Year;
                cmdGetShipmentCostData.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdGetShipmentCostData.Parameters["I_CONTRACT_ID"].Value = ContractID;

                return dbManager.ExecuteDataTable(cmdGetShipmentCostData, "cmdGetShipmentCostData");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable KPILastUpdateSelectDAL(string Type, string Year, int MonthID)
        {
            try
            {
                if (string.Equals(Type, "IVMS"))
                {
                    cmdKPILastUpdateSelect.CommandText = @"SELECT IVMS.IVMS_DATE, KPI_VERSION
                                                            FROM
                                                            (
                                                                SELECT ROWNUM R, TO_CHAR(UPDATE_DATE, 'DD/MM/YYYY') AS IVMS_DATE
                                                                       , KPI_VERSION
                                                                FROM IVMS_KPI_LASTVERSION
                                                                WHERE TO_NUMBER(TO_CHAR(REPORT_DATE, 'MM')) =:I_MONTH
                                                                AND TO_NUMBER(TO_CHAR(REPORT_DATE, 'YYYY')) =:I_YEAR
                                                                ORDER BY REPORT_DATE DESC
                                                            ) IVMS
                                                            WHERE R = 1";
                }
                else if (string.Equals(Type, "SHIPMENT_COST"))
                {
                    cmdKPILastUpdateSelect.CommandText = @"SELECT SHIPMENT_COST.SHIPMENT_COST_DATE
                                                            FROM
                                                            (
                                                                SELECT ROWNUM R
                                                                     , CASE WHEN UPDATE_DATETIME IS NULL THEN TO_CHAR(CREATE_DATETIME, 'DD/MM/YYYY')
                                                                                                         ELSE TO_CHAR(UPDATE_DATETIME, 'DD/MM/YYYY') END AS SHIPMENT_COST_DATE
                                                                FROM T_SHIPMENT_COST_JOB
                                                                WHERE MONTH_ID =:I_MONTH
                                                                AND YEAR_EN =:I_YEAR
                                                                ORDER BY CREATE_DATETIME DESC
                                                            ) SHIPMENT_COST
                                                            WHERE R = 1";
                }
                else if (string.Equals(Type, "KPI"))
                {
                    cmdKPILastUpdateSelect.CommandText = @"SELECT KPI.KPI_DATE
                                                            FROM
                                                            (
                                                                SELECT ROWNUM R, TO_CHAR(UPDATE_DATETIME, 'DD/MM/YYYY') AS KPI_DATE
                                                                FROM T_KPI_HEADER
                                                                WHERE MONTH_ID =:I_MONTH
                                                                AND YEAR =:I_YEAR
                                                                ORDER BY UPDATE_DATETIME DESC
                                                            ) KPI
                                                            WHERE R = 1";
                }

                cmdKPILastUpdateSelect.Parameters["I_YEAR"].Value = Year;
                cmdKPILastUpdateSelect.Parameters["I_MONTH"].Value = MonthID;

                DataTable dt = dbManager.ExecuteDataTable(cmdKPILastUpdateSelect, "cmdKPILastUpdateSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
