﻿namespace TMS_DAL.Master
{
    partial class TopicDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdTopicSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTopic = new System.Data.OracleClient.OracleCommand();
            this.cmdTopicSelectAll = new System.Data.OracleClient.OracleCommand();
            this.cmdTopicAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdTopicCheck = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainTypeCheck = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainTypeInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdTopicUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainTypeUpdate = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTopicSelect
            // 
            this.cmdTopicSelect.CommandText = "SELECT ROW_NUMBER () OVER (ORDER BY TOPIC_ID) AS ID1, TOPIC_ID, TOPIC_NAME FROM M" +
    "_TOPIC WHERE ISACTIVE = \'1\'";
            this.cmdTopicSelect.Connection = this.OracleConn;
            // 
            // cmdTopic
            // 
            this.cmdTopic.Connection = this.OracleConn;
            // 
            // cmdTopicSelectAll
            // 
            this.cmdTopicSelectAll.Connection = this.OracleConn;
            // 
            // cmdTopicAdd
            // 
            this.cmdTopicAdd.Connection = this.OracleConn;
            this.cmdTopicAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_NAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTopicCheck
            // 
            this.cmdTopicCheck.Connection = this.OracleConn;
            this.cmdTopicCheck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_NAME", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdComplainTypeCheck
            // 
            this.cmdComplainTypeCheck.Connection = this.OracleConn;
            this.cmdComplainTypeCheck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_NAME", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdComplainTypeInsert
            // 
            this.cmdComplainTypeInsert.Connection = this.OracleConn;
            this.cmdComplainTypeInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_NAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_LOCK_DRIVER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ADD_MULTIPLE_CAR", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTopicUpdate
            // 
            this.cmdTopicUpdate.Connection = this.OracleConn;
            this.cmdTopicUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TOPIC_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_USER_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdComplainTypeUpdate
            // 
            this.cmdComplainTypeUpdate.Connection = this.OracleConn;
            this.cmdComplainTypeUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_NAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_LOCK_DRIVER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ADD_MULTIPLE_CAR", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_COMPLAIN_TYPE_ID", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdTopicSelect;
        private System.Data.OracleClient.OracleCommand cmdTopic;
        private System.Data.OracleClient.OracleCommand cmdTopicSelectAll;
        private System.Data.OracleClient.OracleCommand cmdTopicAdd;
        private System.Data.OracleClient.OracleCommand cmdTopicCheck;
        private System.Data.OracleClient.OracleCommand cmdComplainTypeCheck;
        private System.Data.OracleClient.OracleCommand cmdComplainTypeInsert;
        private System.Data.OracleClient.OracleCommand cmdTopicUpdate;
        private System.Data.OracleClient.OracleCommand cmdComplainTypeUpdate;

    }
}
