﻿using System;
using System.ComponentModel;
using System.Data;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Master
{
    public partial class DomainDAL : OracleConnectionDAL
    {
        public DomainDAL()
        {
            InitializeComponent();
        }

        public DomainDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable DomainSelectDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();

                dt = dbManager.ExecuteDataTable(cmdDomainSelect, "cmdDomainSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static DomainDAL _instance;
        public static DomainDAL Instance
        {
            get
            {
                _instance = new DomainDAL();
                return _instance;
            }
        }
        #endregion
    }
}