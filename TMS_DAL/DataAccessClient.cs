﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;

namespace TMS_DAL.Common
{
    public enum DB_RESULT
    {
        COMPLETED,
        INCOMPLETED,
        ORAINCOMPLETED
    }

    public class DataAccessClient
    {
        //private OracleConnection connection;
        //private OracleTransaction OraTrans;
        //private OracleCommand cmd;
        //private OracleDataAdapter adap;

        public DataAccessClient(string conn)
        {
            conn = conn.Replace("Unicode=True", "");
            //this.connection = new OracleConnection(conn);
        }


        public DB_RESULT ExecuteAdaptor(string statement, Hashtable parameters, out DataTable dt, out string error_message)
        {
            try
            {
                //cmd = this.connection.CreateCommand();
                //if (this.connection.State == ConnectionState.Closed)
                //{
                //    this.connection.Open();
                //}
                //OraTrans = this.connection.BeginTransaction(IsolationLevel.ReadCommitted);
                //cmd.Transaction = OraTrans;

                //cmd.CommandType = System.Data.CommandType.Text;
                //cmd.CommandText = statement;
                //cmd.BindByName = true;
                //if (parameters != null)
                //{
                //    foreach (string key in parameters.Keys)
                //    {
                //        cmd.Parameters.Add(key, parameters[key]);
                //    }
                //}

                //adap = new OracleDataAdapter(cmd);
                dt = new DataTable();
                //adap.Fill(dt);

                //OraTrans.Commit();
                //if (this.connection.State == ConnectionState.Open)
                //{
                //    this.connection.Close();
                //}
                error_message = null;
                return DB_RESULT.COMPLETED;
            }
            catch (Exception ex)
            {
                dt = null;
                //if (this.connection.State == ConnectionState.Open)
                //{
                //    this.connection.Close();
                //}
                error_message = ex.Message;
                return DB_RESULT.INCOMPLETED;
            }
        }
        
        public DB_RESULT ExecuteNonQuery(string statement, Hashtable parameters, out int rows_affected, out string error_message)
        {
            //cmd = this.connection.CreateCommand();
            //if (this.connection.State == ConnectionState.Closed)
            //{
            //    this.connection.Open();
            //}
            //OraTrans = this.connection.BeginTransaction(IsolationLevel.ReadCommitted);
            //cmd.Transaction = OraTrans;

            try
            {
                //cmd.CommandType = System.Data.CommandType.Text;
                //cmd.CommandText = statement;
                //cmd.BindByName = true;

                //if (parameters != null)
                //{
                //    foreach (string key in parameters.Keys)
                //    {
                //        cmd.Parameters.Add(key, parameters[key]);
                //    }
                //}
                rows_affected = 0;// cmd.ExecuteNonQuery();
                //OraTrans.Commit();
                //if (this.connection.State == ConnectionState.Open)
                //{
                //    this.connection.Close();
                //}
                error_message = null;
                return DB_RESULT.COMPLETED;
            }
            catch (OracleException ex)
            {
                //OraTrans.Rollback();
                //if (this.connection.State == ConnectionState.Open)
                //{
                //    this.connection.Close();
                //}
                rows_affected = 0;
                error_message = ex.Message;
                return DB_RESULT.ORAINCOMPLETED;
            }
            catch (Exception ex)
            {
                //OraTrans.Rollback();
                //if (this.connection.State == ConnectionState.Open)
                //{
                //    this.connection.Close();
                //}

                rows_affected = 0;
                error_message = ex.Message;
                return DB_RESULT.INCOMPLETED;
            }
        }

        #region + Instance +
        private static DataAccessClient _instance;
        public static DataAccessClient Instance
        {
            get
            {
                _instance = new DataAccessClient(Properties.Settings.Default.OracleConnectionString);
                return _instance;
            }
        }
        #endregion
    }
}
