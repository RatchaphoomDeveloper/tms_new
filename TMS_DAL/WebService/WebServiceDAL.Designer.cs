﻿namespace TMS_DAL.WebService
{
	partial class WebServiceDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdTu = new System.Data.OracleClient.OracleCommand();
            this.cmdComp = new System.Data.OracleClient.OracleCommand();
            this.cmdVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdWorkGroup = new System.Data.OracleClient.OracleCommand();
            this.cmdUpload = new System.Data.OracleClient.OracleCommand();
            this.cmdStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdContract = new System.Data.OracleClient.OracleCommand();

        }

        #endregion

		private System.Data.OracleClient.OracleCommand cmdSelect;
        private System.Data.OracleClient.OracleCommand cmdTruck;
        private System.Data.OracleClient.OracleCommand cmdTu;
        private System.Data.OracleClient.OracleCommand cmdComp;
        private System.Data.OracleClient.OracleCommand cmdVendor;
        private System.Data.OracleClient.OracleCommand cmdWorkGroup;
        private System.Data.OracleClient.OracleCommand cmdUpload;
        private System.Data.OracleClient.OracleCommand cmdStatus;
        private System.Data.OracleClient.OracleCommand cmdContract;
    }
}
