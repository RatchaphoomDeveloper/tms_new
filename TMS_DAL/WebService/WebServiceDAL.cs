﻿using dbManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.WebService
{
    public partial class WebServiceDAL : OracleConnectionDAL
    {
        #region TestWebServiceDAL
        public WebServiceDAL()
        {
            InitializeComponent();
        }

        public WebServiceDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region TestWebServiceSelect
        public DataSet TestWebServiceSelect()
        {
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = @"SELECT SEMPLOYEEID, FNAME, LNAME FROM TEMPLOYEE_SAP";
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "TEMPLOYEE_SAP";
            }
            return ds;
        }
        #endregion

        #region IsUserAuthen
        public bool IsUserAuthen(string username, string password)
        {
            try
            {
                // {0} " + CommonFunction.ReplaceInjection(txtUsername.Text.Trim()) + "
                // {1} " + EncodePass + "
                string authenQuery = @"SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE, USERGROUP_NAME  FROM TUSER INNER JOIN M_USERGROUP ON TUSER.CGROUP = M_USERGROUP.USERGROUP_ID WHERE LOWER(SUSERNAME) = LOWER('{0}') AND SPASSWORD = '{1}' AND CACTIVE = '1'";
                authenQuery = string.Format(authenQuery, username, password);

                cmdSelect.CommandType = CommandType.Text;
                cmdSelect.CommandText = authenQuery;
                cmdSelect.Parameters.Clear();

                //cmdMaintainSelect.Parameters.Clear();
                DataTable dt = dbManager.ExecuteDataTable(cmdSelect, "userAuthen");
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region + Instance +
        private static WebServiceDAL _instance;
        public static WebServiceDAL Instance
        {
            get
            {
                _instance = new WebServiceDAL();
                return _instance;
            }
        }
        #endregion

        #region GetUserLogin
        public DataSet GetUserLogin(int sid)
        {
            cmdSelect.CommandType = CommandType.StoredProcedure;
            cmdSelect.CommandText = @"USP_T_USER_SERVICE";
            cmdSelect.Parameters.Clear();
            cmdSelect.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Input,
                ParameterName = "I_USER_ID",
                OracleType = OracleType.VarChar,
                Value = sid.ToString(),
            });
            cmdSelect.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Output,
                ParameterName = "O_CUR_AUTHEN",
                IsNullable = true,
                OracleType = OracleType.Cursor
            });
            cmdSelect.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Output,
                ParameterName = "O_CUR_DATA",
                IsNullable = true,
                OracleType = OracleType.Cursor
            });
            cmdSelect.Parameters.Add(new OracleParameter()
            {
                Direction = ParameterDirection.Output,
                ParameterName = "O_CUR_SHIP_TO",
                IsNullable = true,
                OracleType = OracleType.Cursor
            });

            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count >= 3)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "Menu";
                ds.Tables[1].TableName = "User";
                ds.Tables[2].TableName = "ShipTo";
            }
            return ds;
        }
        #endregion

        #region GetVendor
        public DataSet GetVendor()
        {
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = @"SELECT * FROM VW_M_VENDOR";
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "VENDOR";
            }
            return ds;
        }
        #endregion

        #region GetContract
        public DataSet GetContract(string vendorID = "")
        {
            // vendorID
            if (string.IsNullOrEmpty(vendorID))
            {
                vendorID = " ";
            }

            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = string.Format(@"SELECT * FROM VW_M_CONTRACT WHERE (SVENDORID = '{0}' OR '{0}' = ' ')", vendorID);
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "CONTRACT";
            }
            return ds;
        }
        #endregion

        #region GetDriver
        public DataSet GetDriver(string vendorID = "")
        {
            // vendorID
            if (string.IsNullOrEmpty(vendorID))
            {
                vendorID = " ";
            }

            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = string.Format(@"SELECT * FROM VW_M_DRIVER WHERE (SVENDORID = '{0}' OR '{0}' = ' ')", vendorID);
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "DRIVER";
            }
            return ds;
        }
        #endregion

        #region GetDepartment
        public DataTable GetDepartment()
        {
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = @"SELECT * FROM VW_M_DEPARTMENT";
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataTable dt = dbManager.ExecuteDataTable(cmdSelect, "DEPARTMENT");
            return dt;
        }
        #endregion

        #region GetDivision
        public DataTable GetDivision(string departmentID)
        {
            cmdSelect.CommandType = CommandType.Text;
            if (string.IsNullOrEmpty(departmentID))
            {
                cmdSelect.CommandText = string.Format(@"SELECT * FROM VW_M_DIVISION");
            }
            else
            {
                cmdSelect.CommandText = string.Format(@"SELECT * FROM VW_M_DIVISION WHERE DEPARTMENT_ID = {0}", departmentID);
            }
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataTable dt = dbManager.ExecuteDataTable(cmdSelect, "DIVISION");
            return dt;
        }
        #endregion

        #region GetWorkGroup
        public DataSet GetWorkGroup()
        {
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = @"SELECT * FROM VW_M_WORKGROUP";
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "WORKGROUP";
            }
            return ds;
        }
        #endregion

        #region GetGroup
        public DataSet GetGroup(int workgroupID)
        {
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = string.Format(@"SELECT * FROM VW_M_GROUP WHERE WORKGROUP_ID = {0}", workgroupID.ToString());
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "GROUP";
            }
            return ds;
        }
        #endregion

        #region GetTerminal
        public DataSet GetTerminal()
        {
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = @"SELECT * FROM VW_M_TERMINAL";
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "TERMINAL";
            }
            return ds;
        }
        #endregion

        #region GetKM
        public DataSet GetKM(string condition)
        {
            cmdSelect.CommandType = CommandType.Text;

            cmdSelect.CommandText = string.Format(@"SELECT * FROM VW_M_KM_SELECT WHERE 1=1 {0}", condition);
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "KM";
            }
            return ds;
        }
        #endregion

        #region GetContentFilePath
        public DataTable GetContentFilePath(string condition)
        {
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = string.Format(@"SELECT * FROM VW_M_FUPLOAD_SELECT WHERE 1=1 {0} ", condition);
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataTable dt = dbManager.ExecuteDataTable(cmdSelect, "ContentFile");
            return dt;
        }
        #endregion

        #region GetTruckList
        public DataSet GetTruckList(string condition)
        {

            cmdSelect.CommandType = CommandType.Text;

            string query = "SELECT DISTINCT * FROM VW_M_TRUCK_LIST_WEB WHERE 1=1 " + condition + " ORDER BY DOC_YEAR DESC";
            cmdSelect.CommandText = query;
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "TruckList";
            }
            return ds;
        }

        private string GetTruckQuery()
        {
            return @"SELECT  TH.STRUCKID,V.SABBREVIATION,
                        CASE CT.CSTANDBY
                            WHEN 'N'
                            THEN 'รถในสัญญา'
                            ELSE 'รถสำรอง'
                        END SCARTYPE,
                        C.SCONTRACTNO,
                        TH.SHEADREGISTERNO,  
                        TH.STRAILERREGISTERNO,
                        TH.SCARTYPEID ,
                        CASE 
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '0' THEN
                          GET_C_CONFIG('00','VICHICLE_TYPE','1')
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '3' THEN
                          GET_C_CONFIG('34','VICHICLE_TYPE','1')
                          ELSE
                          TT.CONFIG_NAME
                          END  AS SCARCATEGORY,NVL(TH.NSLOT,ST.NSLOT) NSLOT,
                        NVL(TH.NTOTALCAPACITY,ST.NTOTALCAPACITY) NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,
                        TH.ISUSE,TH.CLASSGRP,
                          CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')
                                WHEN TH.CACTIVE ='D' THEN 
                                GET_C_CONFIG('02','TTRUCK_STATUS','1')                                    
                            END STATUS
                    FROM
                        TTRUCK TH
                    LEFT JOIN TTRUCK ST
                    ON TH.STRAILERREGISTERNO=ST.SHEADREGISTERNO
                    LEFT JOIN C_CONFIG TT
                    ON TH.VIEHICLE_TYPE=TT.CONFIG_VALUE
                    AND (TH.VIEHICLE_TYPE !='99'
                    AND TT.CONFIG_TYPE    ='VICHICLE_TYPE' )   
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID AND C.CACTIVE='Y'
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                WHERE TH.LOGDATA='1' AND TH.ISUSE ='1' AND TH.SCARTYPEID IN ('0','3') AND (TH.VIEHICLE_TYPE IS NULL OR TH.VIEHICLE_TYPE IN ('00','34')) AND C.CACTIVE    ='Y' {0} ORDER BY TH.STRUCKID DESC ";
        }
        #endregion

        #region GetTruckDetail
        public DataSet GetTruckDetail(string headLicensePlate, string tailLicensePlate)
        {
            string head = "", trailer = "";
            cmdTruck.CommandType = CommandType.Text;
            string query = "SELECT * FROM VW_M_TRUCK_DETAIL_HEAD_WEB WHERE 1=1 AND SHEADREGISTERNO = '" + headLicensePlate + "'";
            cmdTruck.CommandText = query;
            cmdTruck.Parameters.Clear();
            DataTable dtTruck = dbManager.ExecuteDataTable(cmdTruck, "cmdTruck");

            if (tailLicensePlate == "")
            {
                if (dtTruck.Rows.Count > 0)
                {
                    head = dtTruck.Rows[0]["SHEADREGISTERNO"].ToString().Trim();
                    trailer = dtTruck.Rows[0]["STRAILERREGISTERNO"].ToString().Trim();
                }
                trailer = trailer == "" ? head : trailer;
            }
            else
            {
                trailer = tailLicensePlate;
            }

            cmdTu.CommandType = CommandType.Text;
            query = "SELECT * FROM VW_M_TRUCK_DETAIL_TRAIL_WEB WHERE 1=1 AND STRAILERREGISTERNO = '" + trailer + "'";
            cmdTu.CommandText = query;
            cmdTu.Parameters.Clear();
            DataTable dtTu = dbManager.ExecuteDataTable(cmdTu, "cmdTu");

            cmdComp.CommandType = CommandType.Text;
            query = "SELECT * FROM VW_M_TRUCK_DETAIL_COMP_WEB WHERE 1=1 AND SHEADREGISTERNO = '" + trailer + "'";
            cmdComp.CommandText = query;
            cmdComp.Parameters.Clear();
            DataTable dtComp = dbManager.ExecuteDataTable(cmdComp, "cmdComp");


            //this.CheckEmpty(ref dtTruck);
            //this.CheckEmpty(ref dtTu);
            //this.CheckEmpty(ref dtComp);

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtTruck.Copy());
            ds.Tables.Add(dtTu.Copy());
            ds.Tables.Add(dtComp.Copy());
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "TruckDetail";
                ds.Tables[1].TableName = "TruckTu";
                ds.Tables[2].TableName = "TruckTuComp";
            }
            return ds;
        }
        #endregion

        private void CheckEmpty(ref DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (string.Equals(dt.Columns[j].DataType.FullName, "System.String") && string.Equals(dt.Rows[i][j].ToString(), string.Empty))
                    {
                        dt.Rows[i][j] = "-";
                    }
                }
            }
        }

        #region GetTruckLog
        public DataSet GetTruckLog(string licensePlate, string licenseTrailPlate)
        {

            //cmdSelect.CommandType = CommandType.Text;
            //string query = GetTruckLogQuery();
            //cmdSelect.CommandText = string.Format(query, condition);
            //cmdSelect.Parameters.Clear();

            cmdTruck.CommandType = CommandType.Text;
            string query = GetTruckLogQuery();
            cmdTruck.CommandText = string.Format(query, licensePlate);
            cmdTruck.Parameters.Clear();
            DataTable dtTruck = dbManager.ExecuteDataTable(cmdTruck, "cmdTruck");

            cmdTu.CommandType = CommandType.Text;
            query = GetTruckLogQuery();
            cmdTu.CommandText = string.Format(query, licenseTrailPlate);
            cmdTu.Parameters.Clear();
            DataTable dtTu = dbManager.ExecuteDataTable(cmdTu, "cmdTu");

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = new DataSet();
            ds.Tables.Add(dtTruck.Copy());
            ds.Tables.Add(dtTu.Copy());
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "TruckLogHead";
                ds.Tables[1].TableName = "TruckLogTrail";
            }
            return ds;
        }

        private string GetTruckLogQuery()
        {
            return @"SELECT T.SFIRSTNAME || T.SLASTNAME as FULLNAME,G.UPDATE_DATE,G.HEADDER ||' '|| G.NEWDATA ||' '|| G.OLDDATA AS DETAIL FROM TTRUCK_LOG G,TUSER T WHERE T.SUID=G.USER_ID AND G.REGISNO='{0}' ORDER BY G.UPDATE_DATE DESC ";
        }
        #endregion

        #region GetShipToList
        public DataSet GetShipToList(string Condition)
        {
            try
            {
                cmdSelect.CommandType = CommandType.Text;

                string query = @"SELECT DISTINCT M_SOLD_TO.SHIP_ID, M_SOLD_TO.SHIP_NAME
                             FROM M_SOLD_TO
                             WHERE 1=1
                             {0}
                             ORDER BY M_SOLD_TO.SHIP_ID";
                cmdSelect.CommandText = string.Format(query, Condition);
                cmdSelect.Parameters.Clear();

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "ShipToList";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Get_Material
        public DataSet Get_Material(string Condition)
        {
            try
            {
                cmdSelect.CommandType = CommandType.Text;

                string query = @"Select Distinct * from SHIPMENTCOST_OR.VW_GET_MATERIAL WHERE 1=1
                             {0}
                             ORDER BY MATERIAL_NUMBER";
                cmdSelect.CommandText = string.Format(query, Condition);
                cmdSelect.Parameters.Clear();

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "MATERIAL";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Get_Material_Group
        public DataSet Get_Material_Group(string Condition)
        {
            try
            {
                cmdSelect.CommandType = CommandType.Text;

                string query = @"select Distinct * from SHIPMENTCOST_OR.VW_GET_MATERIAL WHERE 1=1
                             {0}
                             ORDER BY MATERIAL_NUMBER";
                cmdSelect.CommandText = string.Format(query, Condition);
                cmdSelect.Parameters.Clear();

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "MATERIAL";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetPrvReg
        public DataSet GetPrvReg(string Condition)
        {
            try
            {
                cmdSelect.CommandType = CommandType.Text;

                string query = @"SELECT M_PROVINCE.*,M_REGION.REG_NAME FROM M_PROVINCE INNER JOIN M_REGION ON M_PROVINCE.REG_ID = M_REGION.REG_ID ORDER BY M_PROVINCE.ID ASC";
                cmdSelect.CommandText = string.Format(query, Condition);
                cmdSelect.Parameters.Clear();

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "PrvList";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Get_Checker
        public DataSet Get_Checker(string Condition)
        {
            try
            {
                cmdSelect.CommandType = CommandType.Text;

                string query = @"select (SFIRSTNAME || ' ' || SLASTNAME) as fullname from tuser where USERGROUP_ID in (" + Condition + ")";
                cmdSelect.CommandText = query;
                cmdSelect.Parameters.Clear();

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "Checker";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Get_Checker_Service
        public DataSet Get_Checker_Service(string Condition, string I_TEXTSEARCH)
        {
            try
            {
                cmdSelect.CommandType = CommandType.Text;

                string query = @"select SUID,(SFIRSTNAME || ' ' || SLASTNAME) as fullname from tuser where USERGROUP_ID in (" + Condition + ") AND (SFIRSTNAME||SLASTNAME) LIKE '%" + I_TEXTSEARCH + "%' ORDER BY DCREATE DESC";
                cmdSelect.CommandText = query;
                cmdSelect.Parameters.Clear();

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "Checker";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GET_SURPRISECHECK_CHECKLIST
        public DataSet GET_SURPRISECHECK_CHECKLIST(string Condition)
        {
            try
            {
                cmdSelect.CommandType = CommandType.StoredProcedure;
                cmdSelect.CommandText = @"USP_T_CHECKLIST_SELECT";
                cmdSelect.Parameters.Clear();
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "Checklist";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GET_SURPRISECHECK_CHECKLIST_VENDOR
        public DataSet GET_SURPRISECHECK_CHECKLIST_VENDOR(string VENDORID, string Condition, string I_IS_YEAR)
        {
            try
            {
                cmdSelect.CommandType = CommandType.StoredProcedure;
                cmdSelect.CommandText = @"USP_T_SUR_VENDOR_SELECT";
                cmdSelect.Parameters.Clear();
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    OracleType = OracleType.VarChar,
                    Value = VENDORID,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    OracleType = OracleType.VarChar,
                    Value = Condition,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_YEAR",
                    OracleType = OracleType.VarChar,
                    Value = I_IS_YEAR,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "Checklist";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GET_SURPRISECHECK_CHECKLIST_PTT
        public DataSet GET_SURPRISECHECK_CHECKLIST_PTT(string I_TEXTSEARCH, string I_DATESTART, string I_TERMINALID, string I_CHECKER)
        {
            try
            {
                cmdSelect.CommandType = CommandType.StoredProcedure;
                cmdSelect.CommandText = @"USP_T_SURPRISECHECK_HIS_SELECT";
                cmdSelect.Parameters.Clear();
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    OracleType = OracleType.VarChar,
                    Value = I_TEXTSEARCH,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATESTART",
                    OracleType = OracleType.VarChar,
                    Value = I_DATESTART,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TERMINALID",
                    OracleType = OracleType.VarChar,
                    Value = I_TERMINALID,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CHECKER",
                    OracleType = OracleType.VarChar,
                    Value = I_CHECKER,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "Checklist";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GET_SURPRISECHECK_CHECKLIST_PTT_CONFIRM
        public DataSet GET_SURPRISECHECK_CHECKLIST_PTT_CONFIRM(string Condition, string I_IS_YEAR)
        {
            try
            {
                cmdSelect.CommandType = CommandType.StoredProcedure;
                cmdSelect.CommandText = @"USP_T_SUR_PTT_SELECT";
                cmdSelect.Parameters.Clear();
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    OracleType = OracleType.VarChar,
                    Value = Condition,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_YEAR",
                    OracleType = OracleType.VarChar,
                    Value = I_IS_YEAR,
                });
                cmdSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
                if (ds != null && ds.Tables != null)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "ChecklistConfirm";
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region getShiptoBySaleDistrict
        public DataSet getShiptoBySaleDistrict(string condition)
        {

            cmdSelect.CommandType = CommandType.Text;

            string query = "SELECT * FROM VW_M_SOLDTO_BYSALE_SELECT WHERE 1=1 " + condition;
            cmdSelect.CommandText = query;
            cmdSelect.Parameters.Clear();

            //cmdMaintainSelect.Parameters.Clear();
            DataSet ds = dbManager.ExecuteDataSet(cmdSelect);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                ds.DataSetName = "DS";
                ds.Tables[0].TableName = "Shipto";
            }
            return ds;
        }
        #endregion

        #region GetMasterFilter
        public DataSet GetMasterFilter(string Filter)
        {
            DataSet ds = new DataSet();
            DataTable dtVendor = new DataTable();
            DataTable dtContract = new DataTable();
            DataTable dtWorkGroup = new DataTable();
            DataTable dtStatus = new DataTable();
            DataTable dtUpload = new DataTable();

            cmdVendor.CommandType = CommandType.Text;
            string query1 = "SELECT * FROM VW_M_FILTER_VENDOR_WEB WHERE 1 = 1 ";
            cmdVendor.CommandText = query1;
            cmdVendor.Parameters.Clear();
            dtVendor = dbManager.ExecuteDataTable(cmdVendor, "cmdVendor");

            if (Filter == "0")
            {
                cmdWorkGroup.CommandType = CommandType.Text;
                string query2 = "SELECT * FROM VW_M_TRUCK_WORKGROUP_SELECT WHERE 1 = 1 ";
                cmdWorkGroup.CommandText = query2;
                cmdWorkGroup.Parameters.Clear();
                dtWorkGroup = dbManager.ExecuteDataTable(cmdWorkGroup, "cmdWorkGroup");

                cmdContract.CommandType = CommandType.Text;
                string query3 = "SELECT * FROM VW_M_FILTER_CONTERACT_WEB WHERE 1 = 1 ";
                cmdContract.CommandText = query3;
                cmdContract.Parameters.Clear();
                dtContract = dbManager.ExecuteDataTable(cmdContract, "cmdContract");

                cmdStatus.CommandType = CommandType.Text;
                string query4 = "SELECT STATUS_VALUE,STATUS_NAME FROM M_STATUS WHERE 1=1 AND IS_ACTIVE = 1 AND STATUS_TYPE = 'TRUCK_STATUS' ";
                cmdStatus.CommandText = query4;
                cmdStatus.Parameters.Clear();
                dtStatus = dbManager.ExecuteDataTable(cmdStatus, "cmdStatus");

                cmdUpload.CommandType = CommandType.Text;
                string query5 = "SELECT UPLOAD_ID,UPLOAD_NAME FROM M_UPLOAD_TYPE WHERE UPLOAD_TYPE = 'TRUCK' AND ISACTIVE = 1  AND UPLOAD_TYPE_IND = 1 ";
                cmdUpload.CommandText = query5;
                cmdUpload.Parameters.Clear();
                dtUpload = dbManager.ExecuteDataTable(cmdUpload, "cmdUpload");
                dtUpload.Rows.Add("-1", "วันที่จดทะเบียนกับขนส่ง");
                dtUpload.Rows.Add("0", "เอกสารใบเทียบแป้น");
                dtUpload.Rows.Add("41", "เอกสารใบวัดน้ำ");

                ds.Tables.Add(dtVendor.Copy());
                ds.Tables.Add(dtWorkGroup.Copy());
                ds.Tables.Add(dtContract.Copy());
                ds.Tables.Add(dtStatus.Copy());
                ds.Tables.Add(dtUpload.Copy());
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "Vendor";
                    ds.Tables[1].TableName = "WorkGroup";
                    ds.Tables[2].TableName = "Contract";
                    ds.Tables[3].TableName = "Status";
                    ds.Tables[4].TableName = "Upload";
                }
            }
            else if (Filter == "1")
            {
                cmdStatus.CommandType = CommandType.Text;
                string query6 = "SELECT STATUS_VALUE,STATUS_NAME FROM M_STATUS WHERE 1=1 AND IS_ACTIVE = 1 AND STATUS_TYPE = 'USER_STATUS' ";
                cmdStatus.CommandText = query6;
                cmdStatus.Parameters.Clear();
                dtStatus = dbManager.ExecuteDataTable(cmdStatus, "cmdStatus");

                cmdUpload.CommandType = CommandType.Text;
                string query7 = "SELECT UPLOAD_ID,UPLOAD_NAME FROM M_UPLOAD_TYPE WHERE UPLOAD_TYPE = 'VENDOR' AND ISACTIVE = 1  AND UPLOAD_TYPE_IND = 1 ";
                cmdUpload.CommandText = query7;
                cmdUpload.Parameters.Clear();
                dtUpload = dbManager.ExecuteDataTable(cmdUpload, "cmdUpload");

                ds.Tables.Add(dtVendor.Copy());
                ds.Tables.Add(dtStatus.Copy());
                ds.Tables.Add(dtUpload.Copy());
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    ds.DataSetName = "DS";
                    ds.Tables[0].TableName = "Vendor";
                    ds.Tables[1].TableName = "Status";
                    ds.Tables[2].TableName = "Upload";
                }
            }

            return ds;
        }

        #endregion
    }
}
