﻿using dbManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL
{
    public partial class ConfirmTruckDAL : OracleConnectionDAL
    {
        #region ConfirmTruckDAL
        public ConfirmTruckDAL()
        {
            InitializeComponent();
        }

        public ConfirmTruckDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region + Instance +
        private static ConfirmTruckDAL _instance;
        public static ConfirmTruckDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ConfirmTruckDAL();
                //}
                return _instance;
            }
        }
        #endregion

        #region PTTConfirmTruckSelect
        public DataSet PTTConfirmTruckSelect(string I_DATE, string I_STERMINALID, string I_SCONTRACTID, string I_SVENDORID, string I_CSTANBY, string I_GROUPID, string I_CCONFIRM)
        {
            try
            {
                cmdConfirmTruck.CommandType = CommandType.StoredProcedure;
                cmdConfirmTruck.CommandText = "USP_T_TRUCKCON_PTT_SELECT";
                cmdConfirmTruck.Parameters.Clear();
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATE",
                    Value = I_DATE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STERMINALID",
                    Value = I_STERMINALID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = !string.IsNullOrEmpty(I_SCONTRACTID) ? long.Parse(I_SCONTRACTID) : -1,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CSTANBY",
                    Value = I_CSTANBY,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_GROUPID",
                    Value = !string.IsNullOrEmpty(I_GROUPID) ? long.Parse(I_GROUPID) : -1,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CCONFIRM",
                    Value = I_CCONFIRM,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_TERMINAL",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataSet(cmdConfirmTruck);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailSelect
        public DataSet PTTConfirmTruckDetailSelect(string I_DATE, string I_STERMINALID, string I_SCONTRACTID, string I_TEXTSEARCH, string I_CSTANBY, string I_CAR_STATUS_ID, string I_CCONFIRM, bool I_ISVENDOR)
        {
            try
            {
                cmdConfirmTruck.CommandType = CommandType.StoredProcedure;
                cmdConfirmTruck.CommandText = "USP_T_TRUCKCON_D_PTT_SELECT";
                cmdConfirmTruck.Parameters.Clear();
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATE",
                    Value = I_DATE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STERMINALID",
                    Value = I_STERMINALID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = !string.IsNullOrEmpty(I_SCONTRACTID) ? long.Parse(I_SCONTRACTID) : -1,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    Value = I_TEXTSEARCH,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CSTANBY",
                    Value = I_CSTANBY,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CAR_STATUS_ID",
                    Value = !string.IsNullOrEmpty(I_CAR_STATUS_ID) ? long.Parse(I_CAR_STATUS_ID) : -1,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CCONFIRM",
                    Value = I_CCONFIRM,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ISVENDOR",
                    Value = I_ISVENDOR ? 1 : 0,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                //cmdConfirmTruck.Parameters.Add(new OracleParameter()
                //{
                //    Direction = ParameterDirection.Output,
                //    ParameterName = "O_TERMINAL",
                //    OracleType = OracleType.Cursor,
                //    IsNullable = true,
                //});
                return dbManager.ExecuteDataSet(cmdConfirmTruck);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailCheckIVMSSelect
        public DataSet PTTConfirmTruckDetailCheckIVMSSelect(string I_SPLANTCODE, string I_LICENSE)
        {
            try
            {
                cmdConfirmTruck.CommandType = CommandType.StoredProcedure;
                cmdConfirmTruck.CommandText = "USP_T_TRUCKCON_D_IVMS_SELECT";
                cmdConfirmTruck.Parameters.Clear();
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SPLANTCODE",
                    Value = I_SPLANTCODE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_LICENSE",
                    Value = I_LICENSE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return dbManager.ExecuteDataSet(cmdConfirmTruck);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailCheckIVMSSelect
        public DataTable PTTConfirmTruckDetailCheckIVMSSelect(string ConnectionString, string splantcode, string License)
        {
            DBManager db = new DBManager(DataProvider.Oracle, ConnectionString);
            try
            {

                cmdConfirmTruck.CommandType = CommandType.StoredProcedure;
                cmdConfirmTruck.CommandText = "IVMS.VEHICLE_STATUS";
                cmdConfirmTruck.Parameters.Clear();
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "License",
                    Value = License,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "splantcode",
                    Value = splantcode,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "carriercode",
                    Value = "%",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "contractnum",
                    Value = "%",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "RETVAL",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                return db.ExecuteDataTable(cmdConfirmTruck, "VEHICLE_STATUS");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckDetailCheckIVMSSelect
        public DataTable PTTConfirmTruckDetailCheckIVMSSelect(string ConnectionString, DataTable dt)
        {
            DBManager db = new DBManager(DataProvider.Oracle, ConnectionString);
            try
            {

                cmdConfirmTruck.CommandType = CommandType.StoredProcedure;
                cmdConfirmTruck.CommandText = "IVMS.VEHICLE_STATUS";
                cmdConfirmTruck.Parameters.Clear();
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "License",
                    //Value = License,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "splantcode",
                    //Value = splantcode,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "carriercode",
                    Value = "%",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "contractnum",
                    Value = "%",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "RETVAL",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["IS_SELECT"] == "1")
                    {
                        cmdConfirmTruck.Parameters["License"].Value = dr["SHEADREGISTERNO"] + string.Empty;
                        cmdConfirmTruck.Parameters["splantcode"].Value = dr["STERMINALID"] + string.Empty;
                        DataTable dtIVMS = db.ExecuteDataTable(cmdConfirmTruck, "VEHICLE_STATUS");
                        if (dtIVMS != null && dtIVMS.Rows.Count > 0)
                        {
                            DataRow drIVMS = dtIVMS.Rows[0];
                            dr["IVMS_GPS_STAT"] = drIVMS["GPS_STAT"];
                            dr["IVMS_MDVR_STAT"] = drIVMS["MDVR_STAT"];
                            dr["IVMS_STATUS"] = drIVMS["STATUS"];
                            dr["IVMS_RADIUS_KM"] = drIVMS["Radius_Km"];
                            dr["IVMS_CHECKDATE"] = drIVMS["DateTime"];
                        }
                    }
                }
                return dt;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PTTConfirmTruckLockUpdate
        public bool PTTConfirmTruckLockUpdate(string I_NCONFIRMID, string I_USERID, string I_PTT_LOCK)
        {
            try
            {
                bool isRes = false;
                cmdConfirmTruck.CommandType = CommandType.StoredProcedure;
                cmdConfirmTruck.CommandText = "USP_T_TRUCKCON_PTT_LOCK_SELECT";
                cmdConfirmTruck.Parameters.Clear();
                
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_NCONFIRMID",
                    Value = !string.IsNullOrEmpty(I_NCONFIRMID) ? long.Parse(I_NCONFIRMID) : -1,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = I_USERID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_PTT_LOCK",
                    Value = !string.IsNullOrEmpty(I_PTT_LOCK) ? long.Parse(I_PTT_LOCK) : -1,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                dbManager.Open();
                dbManager.BeginTransaction();
                dbManager.ExecuteNonQuery(cmdConfirmTruck);
                dbManager.CommitTransaction();
                isRes = true;
                return isRes ;

            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region TruckByVendorSelect
        public DataTable TruckByVendorSelect(string I_VENDORCODE)
        {
            try
            {
                cmdConfirmTruck.CommandType = CommandType.StoredProcedure;
                cmdConfirmTruck.CommandText = "USP_T_TRUCKbyVENDOR_SELECT";
                cmdConfirmTruck.Parameters.Clear();
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDORCODE",
                    Value = I_VENDORCODE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                
                cmdConfirmTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    OracleType = OracleType.Cursor,
                    IsNullable = true,
                });

                return dbManager.ExecuteDataTable(cmdConfirmTruck, "USP_T_TRUCKbyVENDOR_SELECT");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
