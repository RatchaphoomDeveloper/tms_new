﻿namespace TMS_DAL.Transaction.TruckDoc
{
    partial class TruckDocDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdTruckCheckDocComplate = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTruckCheckDocComplate
            // 
            this.cmdTruckCheckDocComplate.CommandText = "SELECT * FROM TTRUCK T ,TREQ_DATACHANGE Q WHERE T.STRUCKID = Q.REF_ID AND STATUS=" +
    "\'1\' AND T.STRUCKID = :STRUCKID AND Q.TYPE = \'T\'";
            this.cmdTruckCheckDocComplate.Connection = this.OracleConn;
            this.cmdTruckCheckDocComplate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdTruckCheckDocComplate;
    }
}
