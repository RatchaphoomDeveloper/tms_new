﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;

namespace TMS_DAL.Transaction.TruckDoc
{
    public partial class TruckDocDAL : OracleConnectionDAL
    {
        #region Constructer
            public TruckDocDAL()
            {
                InitializeComponent();
            }

            public TruckDocDAL(IContainer container)
            {
                container.Add(this);

                InitializeComponent();
            }
        #endregion
            #region เช็คว่าเอกสารรถสมบูรณ์แล้วหรือยัง
            public DataTable CheckDocComplate(string STRUCKID)
            {
                try
                {
                    cmdTruckCheckDocComplate.Parameters.Clear();
                    cmdTruckCheckDocComplate.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                    return dbManager.ExecuteDataTable(cmdTruckCheckDocComplate, "cmdTruckCheckDocComplate");
                }
                catch (Exception ex)
                {
                    
                    throw new Exception(ex.Message);
                }
            }
            #endregion

            #region + Instance +
            private static TruckDocDAL _instance;
        public static TruckDocDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new TruckDocDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
