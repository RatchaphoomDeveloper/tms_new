﻿namespace TMS_DAL.Transaction.SendEmail
{
    partial class SendEmailDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdSendEmailReportSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSendEmailPTT = new System.Data.OracleClient.OracleCommand();
            this.cmdSendEmailCar = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdSendEmailReportSelect
            // 
            this.cmdSendEmailReportSelect.Connection = this.OracleConn;
            this.cmdSendEmailReportSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REPORT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdSendEmailPTT
            // 
            this.cmdSendEmailPTT.Connection = this.OracleConn;
            this.cmdSendEmailPTT.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdSendEmailCar
            // 
            this.cmdSendEmailCar.Connection = this.OracleConn;
            this.cmdSendEmailCar.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TEMPLATE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdSendEmailReportSelect;
        private System.Data.OracleClient.OracleCommand cmdSendEmailPTT;
        private System.Data.OracleClient.OracleCommand cmdSendEmailCar;
    }
}
