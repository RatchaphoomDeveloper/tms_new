﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;

namespace TMS_DAL.Transaction.SendEmail
{
    public partial class SendEmailDAL : OracleConnectionDAL
    {
        public SendEmailDAL()
        {
            InitializeComponent();
        }

        public SendEmailDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable SendEmailReportSelectDAL(int TemplateID, int ReportID)
        {
            try
            {
                dbManager.Open();
                if (TemplateID == 21)
                {
                    cmdSendEmailPTT.CommandText = @"SELECT EXTRACT(DAY FROM SYSDATE) - END_UPLOAD_DAY AS TOTAL_REMAIN
                                                             FROM M_MONTH_REPORT_REQUIRE
                                                             WHERE EXTRACT(DAY FROM SYSDATE) - END_UPLOAD_DAY IN (SELECT SCHEDULE_DAY FROM M_EMAIL_SCHEDULE WHERE TEMPLATE_ID = :I_TEMPLATE_ID)";

                    cmdSendEmailPTT.Parameters["I_TEMPLATE_ID"].Value = TemplateID;
                    return dbManager.ExecuteDataTable(cmdSendEmailPTT, "cmdSendEmailPTT");
                }
                else
                {
                    cmdSendEmailReportSelect.CommandText = @"SELECT DISTINCT SVENDORID, TVENDOR.SABBREVIATION, TMONTHLY_HEADER.REPORT_ID, M_EMAIL_SCHEDULE.SCHEDULE_ID, M_EMAIL_SCHEDULE.TEMPLATE_ID, M_EMAIL_SCHEDULE.SCHEDULE_NO
                                                             , M_EMAIL_SCHEDULE.SCHEDULE_DAY, M_EMAIL_TEMPLATE.TEMPLATE_NAME, M_EMAIL_TEMPLATE.SUBJECT, REPLACE(M_EMAIL_TEMPLATE.BODY, '{DAY}', M_EMAIL_SCHEDULE.SCHEDULE_DAY) AS BODY, TVENDOR.EMAIL
                                                              FROM M_MONTH_REPORT_REQUIRE LEFT JOIN TMONTHLY_HEADER ON M_MONTH_REPORT_REQUIRE.MONTH_ID = TMONTHLY_HEADER.MONTH_ID AND M_MONTH_REPORT_REQUIRE.REPORT_ID = TMONTHLY_HEADER.REPORT_ID
                                                                                          LEFT JOIN TVENDOR ON TMONTHLY_HEADER.VENDOR_ID = TVENDOR.SVENDORID
                                                                                          LEFT JOIN M_EMAIL_SCHEDULE ON M_MONTH_REPORT_REQUIRE.END_UPLOAD_DAY - EXTRACT(DAY FROM SYSDATE) = SCHEDULE_DAY AND M_EMAIL_SCHEDULE.TEMPLATE_ID =:I_TEMPLATE_ID
                                                                                          LEFT JOIN M_EMAIL_TEMPLATE ON M_EMAIL_SCHEDULE.TEMPLATE_ID = M_EMAIL_TEMPLATE.TEMPLATE_ID AND M_MONTH_REPORT_REQUIRE.EMAIL_TEMPLATE_ID = M_EMAIL_SCHEDULE.TEMPLATE_ID
                                                              WHERE M_MONTH_REPORT_REQUIRE.IS_ACTIVE = 1
                                                              AND TMONTHLY_HEADER.REPORT_ID =:I_REPORT_ID
                                                              AND TMONTHLY_HEADER.UPLOAD_YEAR = EXTRACT(YEAR FROM SYSDATE)
                                                              AND M_MONTH_REPORT_REQUIRE.MONTH_ID = EXTRACT(MONTH FROM SYSDATE)
                                                              AND M_MONTH_REPORT_REQUIRE.END_UPLOAD_DAY - EXTRACT(DAY FROM SYSDATE) IN (SELECT SCHEDULE_DAY FROM M_EMAIL_SCHEDULE WHERE TEMPLATE_ID =:I_TEMPLATE_ID)
                                                              ORDER BY TVENDOR.SABBREVIATION";

                    cmdSendEmailReportSelect.Parameters["I_TEMPLATE_ID"].Value = TemplateID;
                    cmdSendEmailReportSelect.Parameters["I_REPORT_ID"].Value = ReportID;
                    return dbManager.ExecuteDataTable(cmdSendEmailReportSelect, "cmdSendEmailReportSelect");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SendEmailCarSelectDAL(int TemplateID)
        {
            try
            {
                cmdSendEmailCar.CommandType = CommandType.StoredProcedure;
                cmdSendEmailCar.CommandText = "USP_T_TRUCK_NOT_SEND";
//                cmdSendEmailCar.CommandText = @"SELECT SABBREVIATION, EMAIL,
//                                                       LTRIM(MAX(SYS_CONNECT_BY_PATH(SCONTRACTNO,', '))
//                                                       KEEP (DENSE_RANK LAST ORDER BY curr),', ') AS SCONTRACTNO
//                                                      , M_EMAIL_TEMPLATE.SUBJECT, M_EMAIL_TEMPLATE.BODY
//                                                From (SELECT SABBREVIATION, EMAIL
//                                                  ,SCONTRACTNO,
//                                                  ROW_NUMBER() OVER (PARTITION BY SABBREVIATION ORDER BY SCONTRACTNO) AS curr,
//                                                               ROW_NUMBER() OVER (PARTITION BY SABBREVIATION  ORDER BY SCONTRACTNO) -1 AS prev
//                                                 FROM (SELECT DISTINCT TVENDOR.SABBREVIATION, TVENDOR.EMAIL
//                                                  ,SCONTRACTNO
//                                                  FROM TVENDOR 
//                                                LEFT JOIN TCONTRACT ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID
//                                                LEFT JOIN TCONTRACT_TRUCK ON TCONTRACT.SCONTRACTID = TCONTRACT_TRUCK.SCONTRACTID
//                                                LEFT JOIN TTRUCK ON TTRUCK.STRUCKID = TCONTRACT_TRUCK.STRUCKID
//                                                LEFT JOIN (SELECT DISTINCT TTRUCKCONFIRM.SCONTRACTID FROM TTRUCKCONFIRM 
//                                                  LEFT JOIN TTRUCKCONFIRMLIST ON TTRUCKCONFIRM.NCONFIRMID = TTRUCKCONFIRMLIST.NCONFIRMID
//                                                  WHERE  NVL(TTRUCKCONFIRM.CCONFIRM,'0') = '1' AND TO_DATE(TTRUCKCONFIRM.DDATE + 1) = TO_DATE(SYSDATE)) TTRUCKCONFIRMS  ON TCONTRACT.SCONTRACTID = TTRUCKCONFIRMS.SCONTRACTID
//                                                WHERE TCONTRACT.CACTIVE = 'Y' AND TTRUCKCONFIRMs.scontractid IS NULL))
//                                                  LEFT JOIN M_EMAIL_TEMPLATE ON M_EMAIL_TEMPLATE.TEMPLATE_ID =:I_TEMPLATE_ID
//                                                  GROUP BY SABBREVIATION, EMAIL, M_EMAIL_TEMPLATE.SUBJECT, M_EMAIL_TEMPLATE.BODY
//                                                  CONNECT BY prev = PRIOR curr  
//                                                START WITH curr = 1";

                cmdSendEmailCar.Parameters["I_TEMPLATE_ID"].Value = TemplateID;

                return dbManager.ExecuteDataTable(cmdSendEmailCar, "cmdSendEmailCar");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static SendEmailDAL _instance;
        public static SendEmailDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new SendEmailDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}