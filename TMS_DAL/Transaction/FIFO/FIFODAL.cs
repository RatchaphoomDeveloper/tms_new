﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.FIFO
{
    public partial class FIFODAL : OracleConnectionDAL
    {
        #region FIFODAL
        public FIFODAL()
        {
            InitializeComponent();
        }

        public FIFODAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region GetFIFO1
        public DataTable GetFIFO1(string SSTERMINALID, string SPERSONELNO, string SVENDORID)
        {
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "USP_T_GETFIFO1_SELECT_PTT";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SSTERMINALID",
                    IsNullable = true,
                    Value = SSTERMINALID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SPERSONELNO",
                    IsNullable = true,
                    Value = SPERSONELNO
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    Value = SVENDORID
                });
                
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                return dbManager.ExecuteDataTable(cmdPlanTransport, "cmdPlanTransport");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            //finally
            //{
            //    dbManager.Close();
            //}
        }
        #endregion

        #region + Instance +
        private static FIFODAL _instance;
        public static FIFODAL Instance
        {
            get
            {
                _instance = new FIFODAL();

                return _instance;
            }
        }
        #endregion
        
    }
}
