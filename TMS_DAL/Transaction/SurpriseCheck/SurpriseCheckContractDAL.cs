﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.SurpriseCheck
{
    public partial class SurpriseCheckContractDAL : OracleConnectionDAL
    {
        #region SurpriseCheckContractDAL
        public SurpriseCheckContractDAL()
        {
            InitializeComponent();
        }

        public SurpriseCheckContractDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        public DataTable SurpriseCheckContractSelect(string I_TRUCK, string I_VENDORID, string I_CONTRACTID, string I_STARTDATE, string I_ENDDATE, string I_IS_ACTIVE)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_T_CHECK_CONTRACT_SET";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TRUCK",
                    Value = I_TRUCK,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDORID",
                    Value = I_VENDORID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACTID",
                    Value = !string.IsNullOrEmpty(I_CONTRACTID) ? int.Parse(I_CONTRACTID) : -1,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STARTDATE",
                    Value = I_STARTDATE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ENDDATE",
                    Value = I_ENDDATE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_ACTIVE",
                    Value = !string.IsNullOrEmpty(I_IS_ACTIVE) ? int.Parse(I_IS_ACTIVE) : -1,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckContract, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet SurpriseCheckContractSelectByID(string ID)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_T_CHECK_CONTRACT_SET_ID";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = ID,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR_FILE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataSet ds = dbManager.ExecuteDataSet(cmdSurpriseCheckContract);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet SurpriseCheckContractListSelectByID(string ID)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_T_CHECK_CON_LIST_SET";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = ID,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR_FILE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataSet ds = dbManager.ExecuteDataSet(cmdSurpriseCheckContract);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet SurpriseCheckContractListServiceSelectByID(string ID)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_T_CHECK_CON_LIST_SERVICE";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = ID,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR_FILE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataSet ds = dbManager.ExecuteDataSet(cmdSurpriseCheckContract);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SurpriseCheckContractSave(DataSet dsData)
        {
            try
            {
                DataRow dr = dsData.Tables[0].Rows[0];
                int id = !string.IsNullOrEmpty(dr["ID"] + string.Empty) ? int.Parse(dr["ID"] + string.Empty) : 0;
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_T_CHECK_CONTRACT_SAVE";

                cmdSurpriseCheckContract.Parameters.Clear();

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = id,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = dsData.GetXml(),
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckContract, "TableName");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void SurpriseCheckContractChangeStatusSave(int ID, int Status, int User, string Remark)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_T_CHECK_CONTRACT_C_STATUS";

                cmdSurpriseCheckContract.Parameters.Clear();

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = ID,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    Value = Status,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USER",
                    Value = User,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REMARK",
                    Value = Remark,
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckContract);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public void SurpriseCheckContractListSave(DataSet dsData)
        {
            try
            {
                DataRow dr = dsData.Tables[0].Rows[0];
                int id = !string.IsNullOrEmpty(dr["T_CHECK_CONTRACT_ID"] + string.Empty) ? int.Parse(dr["T_CHECK_CONTRACT_ID"] + string.Empty) : 0;
                int STATUS_ID = !string.IsNullOrEmpty(dr["STATUS_ID"] + string.Empty) ? int.Parse(dr["STATUS_ID"] + string.Empty) : 0;
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_T_CHECK_CON_LIST_SAVE";

                cmdSurpriseCheckContract.Parameters.Clear();

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = id,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS_ID",
                    Value = STATUS_ID,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = dsData.GetXml(),
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckContract);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region M_CHECK_CONTRACT_LIST

        public DataTable MCheckContractListSelect()
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_M_CHECK_CONTRACT_LIST_SET";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckContract, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
        public DataTable TruckSelect(string I_SCONTRACTID, string I_SVENDORID)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_M_TRUCK_SEL";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = !string.IsNullOrEmpty(I_SCONTRACTID) ? int.Parse(I_SCONTRACTID) : -1,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckContract, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckSelectByChasis(string I_CHASIS, string I_STRANSPORTID)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_M_TRUCK_SEL_CHASIS";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CHASIS",
                    Value = I_CHASIS,
                    OracleType = OracleType.VarChar
                });

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRANSPORTID",
                    Value = I_STRANSPORTID,
                    OracleType = OracleType.VarChar
                });

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckContract, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ServiceTruckSelectByChasis(string I_CHASIS, string I_STRANSPORTID, string I_TRUCK_TYPE)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_M_TRUCK_SEL_CHASIS_SERVICE";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CHASIS",
                    Value = I_CHASIS,
                    OracleType = OracleType.VarChar
                });

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRANSPORTID",
                    Value = I_STRANSPORTID,
                    OracleType = OracleType.VarChar
                });

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TRUCK_TYPE",
                    Value = I_TRUCK_TYPE,
                    OracleType = OracleType.VarChar
                });

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckContract, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable StatusSelectByType(string I_STATUS_TYPE)
        {
            try
            {
                cmdSurpriseCheckContract.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckContract.CommandText = @"USP_M_STATUS_SEL_TYPE";

                cmdSurpriseCheckContract.Parameters.Clear();
                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS_TYPE",
                    Value = I_STATUS_TYPE,
                    OracleType = OracleType.VarChar
                });

                cmdSurpriseCheckContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckContract, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        #region + Instance +
        private static SurpriseCheckContractDAL _instance;
        public static SurpriseCheckContractDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new SurpriseCheckContractDAL();
                //}
                return _instance;
            }
        }
        #endregion

    }
}
