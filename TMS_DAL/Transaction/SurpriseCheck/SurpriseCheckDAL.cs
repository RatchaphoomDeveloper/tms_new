﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.SurpriseCheck
{
    public partial class SurpriseCheckDAL : OracleConnectionDAL
    {
        #region SurpriseCheckDAL
        public SurpriseCheckDAL()
        {
            InitializeComponent();
        }

        public SurpriseCheckDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region SurpriseCheckSelect
        public DataTable SurpriseCheckSelect(string Condition)
        {
            string Query = cmdSurpriseCheckSelect.CommandText;
            try
            {
                cmdSurpriseCheckSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckSelect, "cmdSurpriseCheckSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckSelect.CommandText = Query;
            }
        }
        #endregion

        #region SurpriseCheckCarSelect
        public DataTable SurpriseCheckCarSelect(string Condition)
        {
            string Query = cmdSurpriseCheckCarSelect.CommandText;
            try
            {
                cmdSurpriseCheckCarSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckCarSelect, "cmdSurpriseCheckCarSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckCarSelect.CommandText = Query;
            }
        }
        #endregion

        #region CheckTruckComplainInsert
        public DataTable CheckTruckComplainInsert(string DocID, DataTable dtHeader, string SDETAIL, string SCOMPLAINFNAME, string SCOMPLAINLNAME, string SCOMPLAINDIVISION, string DDATECOMPLAIN, string TTIMECOMPLAIN, string CTYPEPERSON
                                            , string SCOMPLAINBY, string SCREATE, string SUPDATE, string I_NSURPRISECHECKID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCheckTruckComplainInsert.CommandType = CommandType.StoredProcedure;

                DataSet ds = new DataSet("ds");
                dtHeader.TableName = "dt";
                ds.Tables.Add(dtHeader);

                cmdCheckTruckComplainInsert.Parameters["I_DOC_ID"].Value = DocID;
                cmdCheckTruckComplainInsert.Parameters["I_HEADER"].Value = ds.GetXml();
                //cmdComplainInsert.Parameters["SHEADCOMLIAIN"].Value = SHEADCOMLIAIN;
                //cmdComplainInsert.Parameters["SSUBJECT"].Value = SSUBJECT;
                cmdCheckTruckComplainInsert.Parameters["I_SDETAIL"].Value = SDETAIL;
                //cmdComplainInsert.Parameters["SDELIVERYNO"].Value = SDELIVERYNO;
                //cmdComplainInsert.Parameters["SHEADREGISTERNO"].Value = SHEADREGISTERNO;
                //cmdComplainInsert.Parameters["STRAILERREGISTERNO"].Value = STRAILERREGISTERNO;
                //cmdComplainInsert.Parameters["STRUCKID"].Value = STRUCKID;
                //cmdComplainInsert.Parameters["SVENDORID"].Value = SVENDORID;
                //cmdComplainInsert.Parameters["SPERSONALNO"].Value = SPERSONALNO;
                //cmdComplainInsert.Parameters["SEMPLOYEENAME"].Value = SEMPLOYEENAME;
                //cmdComplainInsert.Parameters["DDELIVERY"].Value = DDELIVERY;
                //cmdComplainInsert.Parameters["TDELIVERY"].Value = TDELIVERY;
                //cmdComplainInsert.Parameters["SCOMPLAINADDRESS"].Value = SCOMPLAINADDRESS;
                //cmdComplainInsert.Parameters["STERMINALID"].Value = STERMINALID;
                cmdCheckTruckComplainInsert.Parameters["I_SCOMPLAINFNAME"].Value = SCOMPLAINFNAME;
                cmdCheckTruckComplainInsert.Parameters["I_SCOMPLAINLNAME"].Value = SCOMPLAINLNAME;
                cmdCheckTruckComplainInsert.Parameters["I_SCOMPLAINDIVISION"].Value = SCOMPLAINDIVISION;
                cmdCheckTruckComplainInsert.Parameters["I_DDATECOMPLAIN"].Value = DDATECOMPLAIN;
                cmdCheckTruckComplainInsert.Parameters["I_TTIMECOMPLAIN"].Value = TTIMECOMPLAIN;
                cmdCheckTruckComplainInsert.Parameters["I_CTYPEPERSON"].Value = CTYPEPERSON;
                cmdCheckTruckComplainInsert.Parameters["I_SCOMPLAINBY"].Value = SCOMPLAINBY;
                cmdCheckTruckComplainInsert.Parameters["I_SCREATE"].Value = SCREATE;
                cmdCheckTruckComplainInsert.Parameters["I_SUPDATE"].Value = SUPDATE;
                //cmdComplainInsert.Parameters["SPRODUCTTYPEID"].Value = SPRODUCTTYPEID;
                //cmdComplainInsert.Parameters["SCONTRACTID"].Value = SCONTRACTID;
                //cmdComplainInsert.Parameters["I_DSERVICE"].Value = DSERVICE;
                cmdCheckTruckComplainInsert.Parameters["I_NSURPRISECHECKID"].Value = string.IsNullOrEmpty(I_NSURPRISECHECKID) ? 0 : int.Parse(I_NSURPRISECHECKID);

                DataTable dt = dbManager.ExecuteDataTable(cmdCheckTruckComplainInsert, "cmdComplainInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region CheckTruckComplainCanEdit
        public DataTable CheckTruckComplainCanEdit(string DocID)
        {
            try
            {
                cmdCheckTruckComplainCanEdit.Parameters["I_DOC_ID"].Value = DocID;
                return dbManager.ExecuteDataTable(cmdCheckTruckComplainCanEdit, "cmdCheckTruckComplainCanEdit");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckComplainSelect
        public DataTable CheckTruckComplainSelect(string Condition)
        {
            string Query = cmdCheckTruckComplainSelect.CommandText;
            try
            {
                cmdCheckTruckComplainSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdCheckTruckComplainSelect, "cmdCheckTruckComplainSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCheckTruckComplainSelect.CommandText = Query;
            }
        }
        #endregion

        #region CheckTruckComplainSelectHeader
        public DataTable CheckTruckComplainSelectHeader(string Condition)
        {
            string Query = cmdCheckTruckComplainSelectHeader.CommandText;
            try
            {
                cmdCheckTruckComplainSelectHeader.CommandText += Condition;
                cmdCheckTruckComplainSelectHeader.CommandText += " ORDER BY T_CHECKTRUCK_COMPLAIN.SSERVICEID";

                return dbManager.ExecuteDataTable(cmdCheckTruckComplainSelectHeader, "cmdCheckTruckComplainSelectHeader");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCheckTruckComplainSelectHeader.CommandText = Query;
            }
        }
        #endregion

        #region CheckTruckComplainCancelDoc
        public void CheckTruckComplainCancelDoc(string DocID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCheckTruckComplainCancelDoc.Parameters["I_DOC_ID"].Value = DocID;
                dbManager.ExecuteNonQuery(cmdCheckTruckComplainCancelDoc);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region CheckTruckUpdatePath
        public void CheckTruckUpdatePath(string I_PATH_PTT, string I_PATH_VENDOR, string I_KEY)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                cmdCheckTruckUpdatePath.CommandText = @"UPDATE TSURPRISECHECKYEARTRUCK
                                                       SET SPATH_PTT = : I_PATH_PTT,SPATH_VENDOR = : I_PATH_VENDOR
                                                       WHERE SHEADREGISTERNO =: I_KEY";
                cmdCheckTruckUpdatePath.Parameters["I_PATH_PTT"].Value = I_PATH_PTT;
                cmdCheckTruckUpdatePath.Parameters["I_PATH_VENDOR"].Value = I_PATH_VENDOR;
                cmdCheckTruckUpdatePath.Parameters["I_KEY"].Value = I_KEY;

                dbManager.ExecuteNonQuery(cmdCheckTruckUpdatePath);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region DriverUpdateSelect
        public DataTable DriverUpdateSelect(string DocID)
        {
            try
            {
                cmdDriverUpdateSelect.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdDriverUpdateSelect, "cmdDriverUpdateSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckTruckComplainChangeStatus
        public void CheckTruckComplainChangeStatus(string DocID, int DocStatusID, int UpdateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCheckTruckComplainChageStatus.Parameters["I_DOC_ID"].Value = DocID;
                cmdCheckTruckComplainChageStatus.Parameters["I_DOC_STATUS_ID"].Value = DocStatusID;
                cmdCheckTruckComplainChageStatus.Parameters["I_UPDATE_BY"].Value = UpdateBy;
                dbManager.ExecuteNonQuery(cmdCheckTruckComplainChageStatus);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region DriverUpdateLog
        public void DriverUpdateLog(string ID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdDriverUpdateLog.Parameters["I_ID"].Value = ID;

                dbManager.ExecuteNonQuery(cmdDriverUpdateLog);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region CheckTruckComplainLockDriver
        public void CheckTruckComplainLockDriver(string DocID, int Total, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCheckTruckComplainLockDriver.CommandType = CommandType.StoredProcedure;

                cmdCheckTruckComplainLockDriver.Parameters["I_DOC_ID"].Value = DocID;
                cmdCheckTruckComplainLockDriver.Parameters["I_TOTAL"].Value = Total;
                cmdCheckTruckComplainLockDriver.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdCheckTruckComplainLockDriver);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region DriverUpdateSelect2
        public DataTable DriverUpdateSelect2(string DocID)
        {
            try
            {
                cmdDriverUpdateSelect2.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdDriverUpdateSelect2, "cmdDriverUpdateSelect2");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region PersonalDetailSelect
        public DataTable PersonalDetailSelect(string EmployeeID)
        {
            try
            {
                cmdPersonalDetailSelect.Parameters["I_SEMPLOYEEID"].Value = EmployeeID;

                return dbManager.ExecuteDataTable(cmdPersonalDetailSelect, "cmdPersonalDetailSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region SurpriseCheckAddEdit
        public DataTable CheckListSelect()
        {
            try
            {
                cmdSurpriseCheckSelect.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckSelect.CommandText = @"USP_T_CHECKLIST_SELECT";

                cmdSurpriseCheckSelect.Parameters.Clear();

                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckSelect, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckTruckSelect(string I_STRUCKID, string I_IS_YEAR, string I_MODE)
        {
            try
            {
                cmdSurpriseCheckSelect.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckSelect.CommandText = @"USP_T_CHECK_TRUCK_SELECT";

                cmdSurpriseCheckSelect.Parameters.Clear();
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRUCKID",
                    Value = I_STRUCKID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_YEAR",
                    Value = string.IsNullOrEmpty(I_IS_YEAR) ? 0 : int.Parse(I_IS_YEAR),
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_MODE",
                    Value = I_MODE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckSelect, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckTruckEmailSelectDAL(string I_NSURPRISECHECKID)
        {
            try
            {
                cmdSurpriseChkMail.CommandType = CommandType.StoredProcedure;
                cmdSurpriseChkMail.CommandText = @"USP_T_CHECK_TRUCK_MAIL_SELECT";

                cmdSurpriseChkMail.Parameters.Clear();
                cmdSurpriseChkMail.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_NSURPRISECHECKID",
                    Value = I_NSURPRISECHECKID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseChkMail.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseChkMail, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckTruckFileSelect(string I_STRUCKID, string I_SPROCESS, string I_IS_YEAR)
        {
            try
            {
                cmdSurpriseCheckSelect.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckSelect.CommandText = @"USP_T_CHECK_TRUCK_FILE_SELECT";

                cmdSurpriseCheckSelect.Parameters.Clear();
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRUCKID",
                    Value = I_STRUCKID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SPROCESS",
                    Value = I_SPROCESS,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_YEAR",
                    Value = string.IsNullOrEmpty(I_IS_YEAR) ? 0 : int.Parse(I_IS_YEAR),
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckSelect, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void CheckTruckFileSave(string I_SCHECKID, string I_SPROCESS, string I_YEAR_ID, DataSet dsData)
        {
            try
            {
                cmdSurpriseCheckSelect.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckSelect.CommandText = @"USP_T_CHECK_TRUCK_FILE_SAVE";

                cmdSurpriseCheckSelect.Parameters.Clear();
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCHECKID",
                    Value = I_SCHECKID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SPROCESS",
                    Value = I_SPROCESS,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR_ID",
                    Value = !string.IsNullOrEmpty(I_YEAR_ID) ? int.Parse(I_YEAR_ID) : 0,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = dsData.GetXml(),
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckSelect);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public void CheckTruckItemUpdate(string I_YEAR_ID, DataSet dsData)
        {
            try
            {
                cmdSurpriseCheckSelect.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckSelect.CommandText = @"USP_T_CHECK_TRUCK_ITEM_UPDATE";

                cmdSurpriseCheckSelect.Parameters.Clear();

                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR_ID",
                    Value = !string.IsNullOrEmpty(I_YEAR_ID) ? int.Parse(I_YEAR_ID) : 0,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = dsData.GetXml(),
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckSelect);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable CheckTruckFileOtherSelect(string I_STRUCKID, string I_SCONTRACTID, string I_IS_YEAR)
        {
            try
            {
                cmdSurpriseCheckSelect.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckSelect.CommandText = @"USP_T_CHECK_TRUCK_OTHER_SEL";

                cmdSurpriseCheckSelect.Parameters.Clear();
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRUCKID",
                    Value = I_STRUCKID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = I_SCONTRACTID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_YEAR",
                    Value = string.IsNullOrEmpty(I_IS_YEAR) ? 0 : int.Parse(I_IS_YEAR),
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckSelect, "TableName");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void CheckTruckFileOtherSave(string I_SCHECKID, string I_STRUCKID, string I_SCONTRACTID, DataSet dsData)
        {
            try
            {
                cmdSurpriseCheckSelect.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckSelect.CommandText = @"USP_T_CHECK_TRUCK_OTHER_SAVE";

                cmdSurpriseCheckSelect.Parameters.Clear();
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCHECKID",
                    Value = I_SCHECKID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRUCKID",
                    Value = I_STRUCKID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = I_SCONTRACTID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckSelect.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = dsData.GetXml(),
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckSelect);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public void SurpriseCheckYearSaveDAL(string I_ID, string I_TOTALCAR, string I_SVENDORID, string I_SCONTRACTID, string I_APPOINTDATE, string I_APPOINTLOCATE, string I_USERCHECK, string I_TEAMCHECK, string I_SUID, string I_ISACTIVE)
        {
            try
            {
                cmdSurpriseCheckYear.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckYear.CommandText = @"USP_T_SURPRISECHECKYEAR_SAVE";

                cmdSurpriseCheckYear.Parameters.Clear();
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = I_ID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTALCAR",
                    Value = I_TOTALCAR,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = I_SCONTRACTID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPOINTDATE",
                    Value = I_APPOINTDATE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPOINTLOCATE",
                    Value = I_APPOINTLOCATE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERCHECK",
                    Value = I_USERCHECK,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEAMCHECK",
                    Value = I_TEAMCHECK,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SUID",
                    Value = I_SUID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ISACTIVE",
                    Value = I_ISACTIVE,
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckYear);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region + Instance +
        private static SurpriseCheckDAL _instance;
        public static SurpriseCheckDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new SurpriseCheckDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
