﻿namespace TMS_DAL.Transaction.SurpriseCheck
{
    partial class SurpriseCheckDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SurpriseCheckDAL));
            this.cmdSurpriseCheckSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckComplainInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckComplainCanEdit = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckComplainSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckComplainSelectHeader = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckComplainCancelDoc = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckComplainChageStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateLog = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckComplainLockDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateSelect2 = new System.Data.OracleClient.OracleCommand();
            this.cmdPersonalDetailSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSurprseCheck = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckYear = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseChkMail = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckCarSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckUpdatePath = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdSurpriseCheckSelect
            // 
            this.cmdSurpriseCheckSelect.CommandText = resources.GetString("cmdSurpriseCheckSelect.CommandText");
            this.cmdSurpriseCheckSelect.Connection = this.OracleConn;
            this.cmdSurpriseCheckSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdCheckTruckComplainInsert
            // 
            this.cmdCheckTruckComplainInsert.CommandText = "USP_T_CT_COMPLAIN_INSERT";
            this.cmdCheckTruckComplainInsert.Connection = this.OracleConn;
            this.cmdCheckTruckComplainInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_SDETAIL", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINFNAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINLNAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINDIVISION", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_DDATECOMPLAIN", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_TTIMECOMPLAIN", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CTYPEPERSON", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINBY", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_HEADER", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_NSURPRISECHECKID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdCheckTruckComplainCanEdit
            // 
            this.cmdCheckTruckComplainCanEdit.CommandText = resources.GetString("cmdCheckTruckComplainCanEdit.CommandText");
            this.cmdCheckTruckComplainCanEdit.Connection = this.OracleConn;
            this.cmdCheckTruckComplainCanEdit.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdCheckTruckComplainSelect
            // 
            this.cmdCheckTruckComplainSelect.CommandText = resources.GetString("cmdCheckTruckComplainSelect.CommandText");
            this.cmdCheckTruckComplainSelect.Connection = this.OracleConn;
            // 
            // cmdCheckTruckComplainSelectHeader
            // 
            this.cmdCheckTruckComplainSelectHeader.CommandText = resources.GetString("cmdCheckTruckComplainSelectHeader.CommandText");
            this.cmdCheckTruckComplainSelectHeader.Connection = this.OracleConn;
            // 
            // cmdCheckTruckComplainCancelDoc
            // 
            this.cmdCheckTruckComplainCancelDoc.CommandText = "UPDATE T_CHECKTRUCK_COMPLAIN SET DOC_STATUS_ID = (SELECT DOC_STATUS_ID FROM M_DOC" +
    "_STATUS WHERE SPECIAL_STATUS = \'2\') WHERE DOC_ID =:I_DOC_ID";
            this.cmdCheckTruckComplainCancelDoc.Connection = this.OracleConn;
            this.cmdCheckTruckComplainCancelDoc.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdDriverUpdateSelect
            // 
            this.cmdDriverUpdateSelect.CommandText = resources.GetString("cmdDriverUpdateSelect.CommandText");
            this.cmdDriverUpdateSelect.Connection = this.OracleConn;
            this.cmdDriverUpdateSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdCheckTruckComplainChageStatus
            // 
            this.cmdCheckTruckComplainChageStatus.CommandText = "UPDATE T_CHECKTRUCK_COMPLAIN SET DOC_STATUS_ID =: I_DOC_STATUS_ID, SUPDATE =: I_U" +
    "PDATE_BY WHERE DOC_ID =:I_DOC_ID";
            this.cmdCheckTruckComplainChageStatus.Connection = this.OracleConn;
            this.cmdCheckTruckComplainChageStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdDriverUpdateLog
            // 
            this.cmdDriverUpdateLog.CommandText = "UPDATE T_CT_COMPLAIN_LOCK_DRIVER SET STATUS = 1 WHERE ID =:I_ID";
            this.cmdDriverUpdateLog.Connection = this.OracleConn;
            this.cmdDriverUpdateLog.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_ID", System.Data.OracleClient.OracleType.Number, 20)});
            // 
            // cmdCheckTruckComplainLockDriver
            // 
            this.cmdCheckTruckComplainLockDriver.CommandText = "USP_T_CT_COMPLAIN_LOCK_DRIVER";
            this.cmdCheckTruckComplainLockDriver.Connection = this.OracleConn;
            this.cmdCheckTruckComplainLockDriver.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_TOTAL", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdDriverUpdateSelect2
            // 
            this.cmdDriverUpdateSelect2.CommandText = "SELECT ID, DOC_ID, SEMPLOYEEID, \'0\' AS CHANGE_TO FROM T_CT_COMPLAIN_LOCK_DRIVER W" +
    "HERE DOC_ID =: I_DOC_ID AND STATUS = 0 AND IS_UNLOCK = 1";
            this.cmdDriverUpdateSelect2.Connection = this.OracleConn;
            this.cmdDriverUpdateSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdPersonalDetailSelect
            // 
            this.cmdPersonalDetailSelect.CommandText = resources.GetString("cmdPersonalDetailSelect.CommandText");
            this.cmdPersonalDetailSelect.Connection = this.OracleConn;
            this.cmdPersonalDetailSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdSurprseCheck
            // 
            this.cmdSurprseCheck.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckYear
            // 
            this.cmdSurpriseCheckYear.Connection = this.OracleConn;
            // 
            // cmdSurpriseChkMail
            // 
            this.cmdSurpriseChkMail.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckCarSelect
            // 
            this.cmdSurpriseCheckCarSelect.CommandText = "select * from VW_T_SURPRISECHECK_CAR_SELECT WHERE 1=1";
            this.cmdSurpriseCheckCarSelect.Connection = this.OracleConn;
            // 
            // cmdCheckTruckUpdatePath
            // 
            this.cmdCheckTruckUpdatePath.Connection = this.OracleConn;
            this.cmdCheckTruckUpdatePath.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_PATH_PTT", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_KEY", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_PATH_VENDOR", System.Data.OracleClient.OracleType.VarChar, 255)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckSelect;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckComplainInsert;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckComplainCanEdit;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckComplainSelect;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckComplainSelectHeader;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckComplainCancelDoc;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateSelect;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckComplainChageStatus;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateLog;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckComplainLockDriver;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateSelect2;
        private System.Data.OracleClient.OracleCommand cmdPersonalDetailSelect;
        private System.Data.OracleClient.OracleCommand cmdSurprseCheck;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckYear;
        private System.Data.OracleClient.OracleCommand cmdSurpriseChkMail;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckCarSelect;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckUpdatePath;
    }
}
