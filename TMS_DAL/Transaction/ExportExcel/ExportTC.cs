﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using System.Data;

namespace TMS_DAL.Transaction.ExportExcel
{
    public partial class ExportTC : OracleConnectionDAL
    {
        public ExportTC()
        {
            InitializeComponent();
        }

        public ExportTC(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        public DataTable ExportTCDAL(string Query)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdExportTC.CommandText = Query;
                return dbManager.ExecuteDataTable(cmdExportTC, "cmdExportTC");
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #region + Instance +
        private static ExportTC _instance;
        public static ExportTC Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ExportTC();
                }
                return _instance;
            }
        }
        #endregion
            
    }
}
