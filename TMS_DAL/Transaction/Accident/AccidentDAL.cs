﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.Accident
{
    public partial class AccidentDAL : OracleConnectionDAL
    {
        #region AccidentDAL
        public AccidentDAL()
        {
            InitializeComponent();
        }

        public AccidentDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region + Instance +
        private static AccidentDAL _instance;
        public static AccidentDAL Instance
        {
            get
            {
                _instance = new AccidentDAL();
                return _instance;
            }
        }
        #endregion

        #region TruckSelect
        public DataTable TruckSelect(string I_TEXTSEARCH = "", string I_VENDOR_ID = "")
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TRUCK_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TEXTSEARCH,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_ID,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ShipmentSelect
        public DataTable ShipmentSelect(string I_TEXTSEARCH)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_SHIPMENT_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TEXTSEARCH,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region EmpSelect
        public DataTable EmpSelect(string I_TEXTSEARCH, string I_VENDORID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_EMP_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TEXTSEARCH,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDORID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDORID,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelect
        public DataTable ContractSelect(string I_VENDORID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_CONTRACT_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDORID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDORID,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TerminalSelect
        public DataTable TerminalSelect(string I_TEXTSEARCH)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TERMINAL_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TEXTSEARCH,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect(string I_SCONTRACTID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_GROUP_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SCONTRACTID,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Save
        #region SaveTab1
        public DataTable SaveTab1(string I_ACCIDENT_ID, string I_ACCIDENT_DATE, string I_REPORT_PTT, string I_REPORTER, string I_VENDOR_REPORT_TIME, string I_SEND_CONSIDER, string I_PTT_APPROVE, string I_USERID, string I_INFORMER_NAME, int I_CACTIVE, string I_DETAIL_ACCIDENTTYPE_ID, string I_DETAIL_SHIPMENT_NO
            , string I_DETAIL_STRUCKID, string I_DETAIL_SVENDORID, string I_DETAIL_GROUPID, string I_DETAIL_SCONTRACTID, string I_DETAIL_SEMPLOYEEID, string I_DETAIL_AGE, string I_DETAIL_SEMPLOYEEID2, string I_DETAIL_AGE2, string I_DETAIL_SEMPLOYEEID3, string I_DETAIL_AGE3, string I_DETAIL_SEMPLOYEEID4, string I_DETAIL_AGE4, int I_DETAIL_DRIVER_NO, string I_DETAIL_SOURCE, string I_DETAIL_LOCATIONS, string I_DETAIL_GPSL, string I_DETAIL_GPSR
            , string I_DETAIL_PRODUCT_ID,DataSet dsDO)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP1_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_DATE",
                    Value = I_ACCIDENT_DATE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REPORT_PTT",
                    Value = I_REPORT_PTT,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REPORTER",
                    Value = string.IsNullOrEmpty(I_REPORTER) ? 0 : int.Parse(I_REPORTER),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_REPORT_TIME",
                    Value = I_VENDOR_REPORT_TIME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,

                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEND_CONSIDER",
                    Value = I_SEND_CONSIDER,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_PTT_APPROVE",
                    Value = I_PTT_APPROVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_INFORMER_NAME",
                    Value = I_INFORMER_NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SHIPMENT_NO",
                    Value = string.IsNullOrEmpty(I_DETAIL_SHIPMENT_NO) ? 0 : Int64.Parse(I_DETAIL_SHIPMENT_NO),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_ACCIDENTTYPE_ID",
                    Value = string.IsNullOrEmpty(I_DETAIL_ACCIDENTTYPE_ID) ? 0 : int.Parse(I_DETAIL_ACCIDENTTYPE_ID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_STRUCKID",
                    Value = I_DETAIL_STRUCKID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SVENDORID",
                    Value = I_DETAIL_SVENDORID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_GROUPID",
                    Value = string.IsNullOrEmpty(I_DETAIL_GROUPID) ? 0 : int.Parse(I_DETAIL_GROUPID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SCONTRACTID",
                    Value = string.IsNullOrEmpty(I_DETAIL_SCONTRACTID) ? 0 : int.Parse(I_DETAIL_SCONTRACTID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SEMPLOYEEID",
                    Value = I_DETAIL_SEMPLOYEEID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_AGE",
                    Value = string.IsNullOrEmpty(I_DETAIL_AGE) ? 0 : int.Parse(I_DETAIL_AGE),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SEMPLOYEEID2",
                    Value = I_DETAIL_SEMPLOYEEID2,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_AGE2",
                    Value = string.IsNullOrEmpty(I_DETAIL_AGE2) ? 0 : int.Parse(I_DETAIL_AGE2),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SEMPLOYEEID3",
                    Value = I_DETAIL_SEMPLOYEEID3,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_AGE3",
                    Value = string.IsNullOrEmpty(I_DETAIL_AGE3) ? 0 : int.Parse(I_DETAIL_AGE3),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SEMPLOYEEID4",
                    Value = I_DETAIL_SEMPLOYEEID4,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_AGE4",
                    Value = string.IsNullOrEmpty(I_DETAIL_AGE4) ? 0 : int.Parse(I_DETAIL_AGE4),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_DRIVER_NO",
                    Value = I_DETAIL_DRIVER_NO,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_SOURCE",
                    Value = I_DETAIL_SOURCE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_LOCATIONS",
                    Value = I_DETAIL_LOCATIONS,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_GPSL",
                    Value = I_DETAIL_GPSL,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_GPSR",
                    Value = I_DETAIL_GPSR,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DETAIL_PRODUCT_ID",
                    Value = I_DETAIL_PRODUCT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATADO",
                    Value = dsDO.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion

                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region SaveTab2
        public DataTable SaveTab2(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, DataSet dsTIME, DataSet dsPARTY, DataSet dsINJURY, DataSet dsDAMAGE, DataSet dsLEAK_PRODUCT, DataSet dsTRANSPORT, DataSet dsEFFICE_CORP, DataSet dsFile, bool Serious, string I_SEMPLOYEEID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                #region USP_I_ACCIDENT_TAP2_TIME_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_TIME_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsTIME.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion

                #region USP_I_ACCIDENT_TAP2_PARTY_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_PARTY_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsPARTY.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion

                #region USP_I_ACCIDENT_TAP2_INJUR_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_INJUR_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsINJURY.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion

                #region USP_I_ACCIDENT_TAP2_DAMAG_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_DAMAG_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsDAMAGE.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion

                #region USP_I_ACCIDENT_TAP2_LEAK_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_LEAK_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsLEAK_PRODUCT.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion

                #region USP_I_ACCIDENT_TAP2_TRANS_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_TRANS_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsTRANSPORT.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion

                #region USP_I_ACCIDENT_TAP2_EFFIC_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_EFFIC_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsEFFICE_CORP.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion

                #region กรณีร้ายแรง
                if (Serious)
                {
                    #region USP_I_EMPLOYEE_STATUS_UPDATE
                    cmdAccident.CommandText = @"USP_I_EMPLOYEE_STATUS_UPDATE";
                    #region Parameter
                    cmdAccident.Parameters.Clear();
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_SEMPLOYEEID",
                        Value = I_SEMPLOYEEID,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });

                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_USERID",
                        Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_CACTIVE",
                        Value = 0,
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    #endregion
                    dbManager.ExecuteNonQuery(cmdAccident);
                    #endregion
                    
                }
                #endregion

                #region USP_I_ACCIDENT_TAP2_IMAGE_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP2_IMAGE_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsFile.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");
                dbManager.CommitTransaction();
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region SaveTab3Vendor
        public DataTable SaveTab3Vendor(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, string I_DAMAGE, DataSet dsFile)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP3V_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsFile.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DAMAGE",
                    Value = string.IsNullOrEmpty(I_DAMAGE) ? 0 : decimal.Parse(I_DAMAGE),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");
                dbManager.CommitTransaction();
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region ApproveTab3
        public DataTable ApproveTab3(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, int I_APPROVE_DATE, DataSet dsFile)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP3_APPROVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsFile.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPROVE_DATE",
                    Value = I_APPROVE_DATE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");
                dbManager.CommitTransaction();
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region SaveTab3
        public DataTable SaveTab3(string I_ACCIDENT_ID, string I_USERID, DataSet dsFile, bool Serious, string I_SEMPLOYEEID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP3_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsFile.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                #region USP_I_EMPLOYEE_STATUS_UPDATE
                cmdAccident.CommandText = @"USP_I_EMPLOYEE_STATUS_UPDATE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEMPLOYEEID",
                    Value = I_SEMPLOYEEID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = Serious ? 1 : 0,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.ExecuteNonQuery(cmdAccident);
                #endregion
                
                dbManager.CommitTransaction();
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region SaveTab4
        public DataTable SaveTab4(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, string UploadType, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataSet dsScoreList, int CostCheck, string CostCheckDate, DataSet dsScoreListCar, bool isValiDate, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, int Blacklist, string Sentencer, DataSet dsFile, string I_SEMPLOYEEID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                dbManager.Open();
                dbManager.BeginTransaction();
                #region กรณีร้ายแรง
                if (TotalDisableDriver == -2)
                {
                    #region USP_I_EMPLOYEE_STATUS_UPDATE
                    cmdAccident.CommandText = @"USP_I_EMPLOYEE_STATUS_UPDATE";
                    #region Parameter
                    cmdAccident.Parameters.Clear();
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_SEMPLOYEEID",
                        Value = I_SEMPLOYEEID,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });

                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_USERID",
                        Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_CACTIVE",
                        Value = 0,
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    #endregion

                    dbManager.ExecuteNonQuery(cmdAccident);
                    #endregion
                }
                #endregion

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAB4_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_UPLOAD_TYPE",
                    Value = UploadType,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsFile.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCORE_CAR",
                    Value = dsScoreListCar.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CUSSCORE_TYPE_ID",
                    Value = CusScoreTypeID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTAL_POINT",
                    Value = TotalPoint,
                    OracleType = OracleType.Double,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTAL_COST",
                    Value = TotalCost,
                    OracleType = OracleType.Double,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTAL_DISABLE_DRIVER",
                    Value = TotalDisableDriver,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COST_CHECK",
                    Value = CostCheck.ToString(),
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COST_CHECK_DATE",
                    Value = CostCheckDate,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COST_OTHER",
                    Value = CostOther,
                    OracleType = OracleType.Double,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ISSAVE_CUSSCORE",
                    Value = isSaveCusScore,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REMARK_TAB3",
                    Value = RemarkTab3,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_OIL_LOSE",
                    Value = OilLose,
                    OracleType = OracleType.Double,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_BLACKLIST",
                    Value = Blacklist,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SSENTENCERCODE",
                    Value = Sentencer,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = dsScoreList.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ISVALIDATE",
                    Value = isValiDate ? 0 : 1,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion

                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                #endregion



                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #endregion

        #region Appeal
        #region AccidentAppealSave
        public DataTable AccidentAppealSave(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataSet dsScoreList, DataSet dsScoreListCar, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, string AppealID, int Blacklist)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_APPEAL_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATACAR_A",
                    Value = dsScoreListCar.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CUSSCORE_TYPE_ID",
                    Value = CusScoreTypeID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTAL_POINT",
                    Value = TotalPoint,
                    OracleType = OracleType.Double,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTAL_COST",
                    Value = TotalCost,
                    OracleType = OracleType.Double,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTAL_DISABLE_DRIVER",
                    Value = TotalDisableDriver,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = dsScoreList.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COST_OTHER",
                    Value = CostOther,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ISSAVE_CUSSCORE",
                    Value = isSaveCusScore,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REMARK_TAB3",
                    Value = RemarkTab3,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_OIL_LOSE",
                    Value = OilLose,
                    OracleType = OracleType.Double,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DELIVERY_DATE",
                    Value = dsScoreListCar.Tables[0].Rows[0]["DELIVERY_DATE"] + string.Empty,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRUCKID",
                    Value = dsScoreListCar.Tables[0].Rows[0]["TRUCKID"] + string.Empty,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_BLACKLIST",
                    Value = Blacklist,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SAPPEALID",
                    Value = AppealID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                #endregion



                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #endregion

        #region UpdateStatus
        public DataTable UpdateStatus(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_STATUS_UPDATE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                #endregion

                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region Select
        #region AccidentSelect
        public DataTable AccidentSelect(string I_TEXTSEARCH, string I_ACCIDENT_DATE, string I_WORKGROUPID, string I_GROUPID, string I_SVENDORID, string I_SCONTRACTID, string I_TYPE, string I_CACTIVE, string I_CGROUP, string I_ACCIDENT_DATE_TO)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEXTSEARCH",
                    Value = I_TEXTSEARCH,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_DATE",
                    Value = I_ACCIDENT_DATE,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_WORKGROUPID",
                    Value = string.IsNullOrEmpty(I_WORKGROUPID) ? 0 : int.Parse(I_WORKGROUPID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_GROUPID",
                    Value = string.IsNullOrEmpty(I_GROUPID) ? 0 : int.Parse(I_GROUPID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = string.IsNullOrEmpty(I_SVENDORID) ? 0 : int.Parse(I_SVENDORID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = string.IsNullOrEmpty(I_SCONTRACTID) ? 0 : int.Parse(I_SCONTRACTID),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TYPE",
                    Value = string.IsNullOrEmpty(I_TYPE) ? -1 : int.Parse(I_TYPE),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? 0 : int.Parse(I_CACTIVE),
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CGROUP",
                    Value = I_CGROUP,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_DATE_TO",
                    Value = I_ACCIDENT_DATE_TO,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab1Select
        public DataTable AccidentTab1Select(string I_ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAB1_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab2Select
        public DataSet AccidentTab2Select(string I_ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAB2_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_TIME",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_DAMAGE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_EFFICE_CORP",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_PARTY",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_TRANSPORT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_IMAGE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_ACCIDENT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_INJURY",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_LEAK_PRODUCT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataSet ds = dbManager.ExecuteDataSet(cmdAccident);
                ds.Tables[0].TableName = "TIME";
                ds.Tables[1].TableName = "DAMAGE";
                ds.Tables[2].TableName = "EFFICE_CORP";
                ds.Tables[3].TableName = "PARTY";
                ds.Tables[4].TableName = "TRANSPORT";
                ds.Tables[5].TableName = "IMAGE";
                ds.Tables[6].TableName = "ACCIDENT";
                ds.Tables[7].TableName = "INJURY";
                ds.Tables[8].TableName = "LEAK_PRODUCT";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab3VendorSelect
        public DataSet AccidentTab3VendorSelect(string I_ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAB3V_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_ACCIDENT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_DAMAGE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_IMAGE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });


                DataSet ds = dbManager.ExecuteDataSet(cmdAccident);
                ds.Tables[0].TableName = "ACCIDENT";
                ds.Tables[1].TableName = "DAMAGE";
                ds.Tables[2].TableName = "IMAGE";

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab3Select
        public DataSet AccidentTab3Select(string I_ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAB3_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_ACCIDENT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_IMAGE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_APPROVEEVENT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataSet ds = dbManager.ExecuteDataSet(cmdAccident);
                ds.Tables[0].TableName = "ACCIDENT";
                ds.Tables[1].TableName = "IMAGE";
                ds.Tables[2].TableName = "APPROVEEVENT";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTab4Select
        public DataSet AccidentTab4Select(string I_ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAB4_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_ACCIDENT",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_FINE",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataSet ds = dbManager.ExecuteDataSet(cmdAccident);
                ds.Tables[0].TableName = "ACCIDENT";
                ds.Tables[1].TableName = "FINE";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentConcludeSelect
        public DataTable AccidentConcludeSelect(string I_YEAR, string I_SVENDORID, string I_SCONTRACTID, string I_GROUPID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_CONCLUDE_SELECT";
                #region Paameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = !string.IsNullOrEmpty(I_SCONTRACTID) ? int.Parse(I_SCONTRACTID) : 0,
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_GROUPID",
                    Value = !string.IsNullOrEmpty(I_GROUPID) ? int.Parse(I_GROUPID) : 0,
                    IsNullable = true,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion

                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region AccidentStatusSelect
        public DataTable AccidentStatusSelect()
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_STATUS_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentType
        #region AccidentTypeSelect
        public DataTable AccidentTypeSelect(string I_NAME, string I_CACTIVE)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TYPE_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_NAME",
                    IsNullable = true,
                    Value = I_NAME,
                    OracleType = OracleType.VarChar
                }); cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    Value = !string.IsNullOrEmpty(I_CACTIVE) ? int.Parse(I_CACTIVE) : 0,
                    OracleType = OracleType.Number
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTypeCheckValidate
        public bool AccidentTypeCheckValidate(int ID, string NAME)
        {
            try
            {
                cmdAccident.CommandText = "SELECT COUNT(ID) COUNTID FROM M_ACCIDENTTYPE WHERE (CACTIVE = 1) AND NAME = :NAME AND ID != :ID";
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ID",
                    Value = ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");
                if (dt.Rows.Count > 0 && int.Parse(dt.Rows[0]["COUNTID"] + string.Empty) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentTypeInsert
        public bool AccidentTypeInsert(string NAME, string CACTIVE, string USERID)
        {
            try
            {
                cmdAccident.CommandText = "INSERT INTO M_ACCIDENTTYPE(NAME, CACTIVE,CREATE_BY,CREATE_DATETIME,UPDATE_BY,UPDATE_DATETIME) VALUES (:NAME, :CACTIVE,:USERID,sysdate,:USERID,sysdate)  RETURNING ID INTO :ID";
                cmdAccident.Parameters.Clear();

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "USERID",
                    Value = USERID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "ID",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdAccident);
                if (row >= 0)
                {
                    int ID = int.Parse(cmdAccident.Parameters["ID"].Value + string.Empty);

                    if (row >= 0)
                    {
                        cmdAccident.CommandText = "INSERT INTO M_COMPLAIN_REQUIRE_FIELD( " +
   "COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,CREATE_DATETIME,UPDATE_BY,UPDATE_DATETIME,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION) " +
"SELECT :ID AS COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,SYSDATE,UPDATE_BY,SYSDATE,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION FROM M_COMPLAIN_REQUIRE_FIELD WHERE FIELD_TYPE = 'ACCIDENTTAB1' AND COMPLAIN_TYPE_ID = 0";
                        cmdAccident.Parameters.Clear();

                        cmdAccident.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "ID",
                            Value = ID,
                            OracleType = OracleType.Number,
                            IsNullable = true,
                        });
                        row = dbManager.ExecuteNonQuery(cmdAccident);
                        //                      if (row >= 0)
                        //                      {
                        //                          cmdAccident.CommandText = "INSERT INTO M_COMPLAIN_REQUEST_FILE(" +
                        //"COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,CREATE_DATETIME,CREATE_BY,UPDATE_BY,UPDATE_DATETIME,REQUEST_TYPE) " +
                        //"SELECT :PERSON_TYPE COMPLAIN_TYPE_ID, UPLOAD_ID,ISACTIVE,SYSDATE,CREATE_BY,UPDATE_BY,SYSDATE,REQUEST_TYPE FROM M_COMPLAIN_REQUEST_FILE" +
                        // " WHERE REQUEST_TYPE = 'ACCIDENTTAB1' AND COMPLAIN_TYPE_ID = 0";
                        //                          cmdAccident.Parameters.Clear();

                        //                          cmdAccident.Parameters.Add(new OracleParameter()
                        //                          {
                        //                              Direction = ParameterDirection.Input,
                        //                              ParameterName = "ID",
                        //                              Value = ID,
                        //                              OracleType = OracleType.Number,
                        //                              IsNullable = true,
                        //                          });
                        //                          dbManager.ExecuteNonQuery(cmdAccident);
                        isRes = true;
                        //                      }

                    }

                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion

        #region AccidentTypeUpdate
        public bool AccidentTypeUpdate(int ID, string NAME, string CACTIVE, string USERID)
        {
            try
            {
                cmdAccident.CommandText = @"UPDATE M_ACCIDENTTYPE 
SET NAME = :NAME
,CACTIVE = :CACTIVE 
,UPDATE_BY = :USERID
,UPDATE_DATETIME = SYSDATE
WHERE ID = :ID";
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "ID",
                    Value = ID,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "NAME",
                    Value = NAME,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "USERID",
                    Value = USERID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "CACTIVE",
                    Value = CACTIVE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });

                bool isRes = false;
                dbManager.Open();
                dbManager.BeginTransaction();
                int row = dbManager.ExecuteNonQuery(cmdAccident);
                if (row >= 0)
                {

                    cmdAccident.CommandText = "UPDATE M_COMPLAIN_REQUIRE_FIELD " +
                  "SET ISACTIVE = :CACTIVE " +
                  "WHERE COMPLAIN_TYPE_ID = :ID AND  FIELD_TYPE = 'ACCIDENTTAB1'";
                    cmdAccident.Parameters.Clear();
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "ID",
                        Value = ID,
                        OracleType = OracleType.Number,
                        IsNullable = true,
                    });
                    cmdAccident.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "CACTIVE",
                        Value = CACTIVE,
                        OracleType = OracleType.VarChar,
                        IsNullable = true,
                    });
                    row = dbManager.ExecuteNonQuery(cmdAccident);
                    if (row > 0)
                    {
                        cmdAccident.CommandText = "UPDATE M_COMPLAIN_REQUEST_FILE " +
                  "SET ISACTIVE = :CACTIVE " +
                  "WHERE COMPLAIN_TYPE_ID = :ID AND  REQUEST_TYPE = 'ACCIDENTTAB1'";
                        cmdAccident.Parameters.Clear();
                        cmdAccident.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "ID",
                            Value = ID,
                            OracleType = OracleType.Number,
                            IsNullable = true,
                        });
                        cmdAccident.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "CACTIVE",
                            Value = CACTIVE,
                            OracleType = OracleType.VarChar,
                            IsNullable = true,
                        });
                        row = dbManager.ExecuteNonQuery(cmdAccident);
                    }
                    else
                    {
                        cmdAccident.CommandText = "INSERT INTO M_COMPLAIN_REQUIRE_FIELD( " +
   "COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,CREATE_DATETIME,UPDATE_BY,UPDATE_DATETIME,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION) " +
"SELECT :ID AS COMPLAIN_TYPE_ID,CONTROL_ID, REQUIRE_TYPE,ISACTIVE,CREATE_BY,SYSDATE,UPDATE_BY,SYSDATE,CONTROL_ID_REQ,FIELD_TYPE,DESCRIPTION FROM M_COMPLAIN_REQUIRE_FIELD WHERE FIELD_TYPE = 'ACCIDENTTAB1' AND COMPLAIN_TYPE_ID = 0";
                        cmdAccident.Parameters.Clear();

                        cmdAccident.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "ID",
                            Value = ID,
                            OracleType = OracleType.Number,
                            IsNullable = true,
                        });
                        row = dbManager.ExecuteNonQuery(cmdAccident);
                    }
                    isRes = true;


                }
                dbManager.CommitTransaction();
                return isRes;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #endregion
        #endregion

        #region AccidentEventSelect
        public DataTable AccidentEventSelect()
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_EVENT_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentRequestFile
        public DataTable AccidentRequestFile(string UploadType)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACCIDENT_FILE_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_UPLOADTYPE",
                    Value = UploadType,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Tab4
        #region AccidentScoreDetailSelect
        public DataTable AccidentScoreDetailSelect(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCOREDETAIL_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreCarSelect
        public DataTable AccidentScoreCarSelect(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCORE_CAR_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreSelect
        public DataTable AccidentScoreSelect(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCORE_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentFile4
        public DataTable AccidentFile4Select(string ACCIDENT_ID, string UploadTypeID, string I_IS_VENDOR_DOWNLOAD)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_4_FILE_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_UPLOAD_TYPE",
                    Value = UploadTypeID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_VENDOR_DOWNLOAD",
                    Value = I_IS_VENDOR_DOWNLOAD,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentDuplicateTotal
        public DataTable AccidentDuplicateTotal(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_DUPLICATETOTAL";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentCostUpdate
        public DataTable AccidentCostUpdate(string I_ACCIDENT_ID, string I_COST_CHECK, string I_COST_CHECK_DATE, int I_CACTIVE, string I_USERID, string I_UPLOAD_TYPE, DataSet dsFile, DataSet dsFine, string I_AMOUNT_TOTAL)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;

                #region USP_I_ACC_COST_UPDATE
                cmdAccident.CommandText = @"USP_I_ACC_COST_UPDATE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COST_CHECK",
                    Value = I_COST_CHECK,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COST_CHECK_DATE",
                    Value = I_COST_CHECK_DATE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsFile.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_UPLOAD_TYPE",
                    Value = I_UPLOAD_TYPE,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                dbManager.ExecuteNonQuery(cmdAccident);

                #endregion

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACCIDENT_TAP4_FINE_SAVE";
                #region Parameter
                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_AMOUNT_TOTAL",
                    Value = string.IsNullOrEmpty(I_AMOUNT_TOTAL) ? 0 : decimal.Parse(I_AMOUNT_TOTAL),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    Value = dsFine.GetXml(),
                    OracleType = OracleType.Clob,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                #endregion

                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region AccidentChangeStatus
        public DataTable AccidentChangeStatus(string I_ACCIDENT_ID, int I_CACTIVE, string I_USERID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;

                #region USP_I_ACCIDENT_TAP3_SAVE
                cmdAccident.CommandText = @"USP_I_ACC_CHANGE_STATUS";
                #region Parameter
                cmdAccident.Parameters.Clear();

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = I_ACCIDENT_ID,
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = string.IsNullOrEmpty(I_USERID) ? 0 : int.Parse(I_USERID),
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    Value = I_CACTIVE,
                    OracleType = OracleType.Number,
                    IsNullable = true,
                });

                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");
                dbManager.CommitTransaction();
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region AccidentScoreDetailASelect
        public DataTable AccidentScoreDetailASelect(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCOREDETAIL_A_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreCarASelect
        public DataTable AccidentScoreCarASelect(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCORE_CAR_A_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreDetailPSelect
        public DataTable AccidentScoreDetailPSelect(string ACCIDENT_ID, string I_DELIVERY_DATE, string I_STRUCKID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCOREDETAIL_P_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DELIVERY_DATE",
                    Value = I_DELIVERY_DATE,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRUCKID",
                    Value = I_STRUCKID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreCarPSelect
        public DataTable AccidentScoreCarPSelect(string ACCIDENT_ID, string I_DELIVERY_DATE, string I_STRUCKID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCORE_CAR_P_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DELIVERY_DATE",
                    Value = I_DELIVERY_DATE,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRUCKID",
                    Value = I_STRUCKID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentScoreASelect
        public DataTable AccidentScoreASelect(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_SCORE_A_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AccidentAppealSelect
        public DataTable AccidentAppealSelect(string ACCIDENT_ID)
        {
            try
            {
                cmdAccident.CommandType = CommandType.StoredProcedure;
                cmdAccident.CommandText = @"USP_I_ACC_APPEAL_SELECT";

                cmdAccident.Parameters.Clear();
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ACCIDENT_ID",
                    Value = ACCIDENT_ID,
                    IsNullable = true,
                    OracleType = OracleType.VarChar
                });
                cmdAccident.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAccident, "cmdAccident");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
    }
}
