﻿namespace TMS_DAL.Transaction.SurpriseCheckYear
{
    partial class SurpriseCheckYearDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SurpriseCheckYearDAL));
            this.cmdSurpriseCheckYear = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckYearSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckYearTruckSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdDataTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckTeamSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckGroupSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckVendorSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSurpriseCheckYearTruckFileSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdSurpriseCheckYear
            // 
            this.cmdSurpriseCheckYear.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckYearSelect
            // 
            this.cmdSurpriseCheckYearSelect.CommandText = resources.GetString("cmdSurpriseCheckYearSelect.CommandText");
            this.cmdSurpriseCheckYearSelect.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckYearTruckSelect
            // 
            this.cmdSurpriseCheckYearTruckSelect.CommandText = resources.GetString("cmdSurpriseCheckYearTruckSelect.CommandText");
            this.cmdSurpriseCheckYearTruckSelect.Connection = this.OracleConn;
            // 
            // cmdDataTruck
            // 
            this.cmdDataTruck.CommandText = "SELECT * FROM VW_M_CONTRACTTRUCK_SELECT WHERE 1=1 ";
            this.cmdDataTruck.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckTeamSelect
            // 
            this.cmdSurpriseCheckTeamSelect.CommandText = "SELECT SUID,FULLNAME FROM VW_T_SURPRISECHECKTEAM_SELECT WHERE 1=1 ";
            this.cmdSurpriseCheckTeamSelect.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckGroupSelect
            // 
            this.cmdSurpriseCheckGroupSelect.CommandText = "select * from VW_M_GROUP where 1=1 ";
            this.cmdSurpriseCheckGroupSelect.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckVendorSelect
            // 
            this.cmdSurpriseCheckVendorSelect.CommandText = "select * from VW_T_SURPRISEYEAR_V_SELECT where 1=1 ";
            this.cmdSurpriseCheckVendorSelect.Connection = this.OracleConn;
            // 
            // cmdSurpriseCheckYearTruckFileSelect
            // 
            this.cmdSurpriseCheckYearTruckFileSelect.CommandText = "SELECT * FROM TSURPRISECHECKYEARTRUCKFILE WHERE 1=1 ";
            this.cmdSurpriseCheckYearTruckFileSelect.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckYear;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckYearSelect;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckYearTruckSelect;
        private System.Data.OracleClient.OracleCommand cmdDataTruck;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckTeamSelect;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckGroupSelect;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckVendorSelect;
        private System.Data.OracleClient.OracleCommand cmdSurpriseCheckYearTruckFileSelect;
    }
}
