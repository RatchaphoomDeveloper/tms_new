﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.SurpriseCheckYear
{
    public partial class SurpriseCheckYearDAL : OracleConnectionDAL
    {
        public SurpriseCheckYearDAL()
        {
            InitializeComponent();
        }

        public SurpriseCheckYearDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        public DataTable SurpriseCheckYearSelect(string Condition)
        {
            string query = cmdSurpriseCheckYearSelect.CommandText;
            try
            {
                cmdSurpriseCheckYearSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckYearSelect, "cmdSurpriseCheckYearSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckYearSelect.CommandText = query;
            }
        }
        public DataTable SurpriseCheckTeamSelect(string Condition)
        {
            string query = cmdSurpriseCheckTeamSelect.CommandText;
            try
            {
                cmdSurpriseCheckTeamSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckTeamSelect, "cmdSurpriseCheckTeamSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckTeamSelect.CommandText = query;
            }
        }
        public DataTable SurpriseCheckGroupSelectDAL(string Condition)
        {
            string query = cmdSurpriseCheckGroupSelect.CommandText;
            try
            {
                cmdSurpriseCheckGroupSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckGroupSelect, "cmdSurpriseCheckGroupSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckGroupSelect.CommandText = query;
            }
        }
        public DataTable SurpriseCheckVendorSelectDAL(string Condition)
        {
            string query = cmdSurpriseCheckVendorSelect.CommandText;
            try
            {
                cmdSurpriseCheckVendorSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckVendorSelect, "cmdSurpriseCheckVendorSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckVendorSelect.CommandText = query;
            }
        }
        public DataTable SurpriseCheckYearTruckSelectDAL(string Condition)
        {
            string query = cmdSurpriseCheckYearTruckSelect.CommandText;
            try
            {
                cmdSurpriseCheckYearTruckSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckYearTruckSelect, "cmdSurpriseCheckYearTruckSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckYearTruckSelect.CommandText = query;
            }
        }
        public DataTable SurpriseCheckYearTruckFileSelectDAL(string Condition)
        {
            string query = cmdSurpriseCheckYearTruckFileSelect.CommandText;
            try
            {
                cmdSurpriseCheckYearTruckFileSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSurpriseCheckYearTruckFileSelect, "cmdSurpriseCheckYearTruckFileSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSurpriseCheckYearTruckFileSelect.CommandText = query;
            }
        }
        public DataTable DataTruckSelectDAL(string Condition)
        {
            string query = cmdDataTruck.CommandText;
            try
            {
                cmdDataTruck.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdDataTruck, "cmdDataTruck");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdDataTruck.CommandText = query;
            }
        }

        public DataTable SurpriseChkReportDAL(string I_YEAR, string I_CONDITION)
        {
            try
            {
                cmdSurpriseCheckYear.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckYear.CommandText = @"USP_T_SURPRISECHKREPORT_SELECT";

                cmdSurpriseCheckYear.Parameters.Clear();
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONDITION",
                    Value = I_CONDITION,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                dbManager.Open();
                dbManager.BeginTransaction();

                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckYear, "cmdSurpriseCheckYear");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SurpriseChkReportYearDAL(string I_YEAR, string I_CONDITION)
        {
            try
            {
                cmdSurpriseCheckYear.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckYear.CommandText = @"USP_T_SURPRISEREPORTY_SELECT";

                cmdSurpriseCheckYear.Parameters.Clear();
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_YEAR",
                    Value = I_YEAR,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONDITION",
                    Value = I_CONDITION,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                dbManager.Open();
                dbManager.BeginTransaction();

                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckYear, "cmdSurpriseCheckYear");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SurpriseCheckYearSaveDAL(string I_ID, string I_TOTALCAR, string I_SVENDORID, string I_SCONTRACTID, string I_APPOINTDATE, string I_APPOINTLOCATE, string I_USERCHECK, string I_TEAMCHECK, string I_SUID, string I_ISACTIVE, string I_REMARK, DataSet I_DATA)
        {
            try
            {
                cmdSurpriseCheckYear.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckYear.CommandText = @"USP_T_SURPRISECHECKYEAR_SAVE";

                cmdSurpriseCheckYear.Parameters.Clear();
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = I_ID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TOTALCAR",
                    Value = I_TOTALCAR,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    Value = I_SVENDORID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = I_SCONTRACTID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPOINTDATE",
                    Value = I_APPOINTDATE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPOINTLOCATE",
                    Value = I_APPOINTLOCATE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERCHECK",
                    Value = I_USERCHECK,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TEAMCHECK",
                    Value = I_TEAMCHECK,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SUID",
                    Value = I_SUID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ISACTIVE",
                    Value = I_ISACTIVE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REMARK",
                    Value = I_REMARK,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATA",
                    Value = I_DATA.GetXml(),
                    OracleType = OracleType.Clob
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                DataTable dt = dbManager.ExecuteDataTable(cmdSurpriseCheckYear, "cmdSurpriseCheckYear");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void SurpriseCheckYearTruckSaveDAL(string I_DOC_ID, string I_SHEAD, string I_STRAIL, string I_IS_ACTIVE, string I_SUID, string I_TYPE)
        {
            try
            {
                cmdSurpriseCheckYear.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckYear.CommandText = @"USP_T_SURPRISETRUCK_SAVE";

                cmdSurpriseCheckYear.Parameters.Clear();
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DOC_ID",
                    Value = I_DOC_ID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SHEAD",
                    Value = I_SHEAD,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STRAIL",
                    Value = I_STRAIL,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_ACTIVE",
                    Value = I_IS_ACTIVE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SUID",
                    Value = I_SUID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TYPE",
                    Value = I_TYPE,
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckYear);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public void SurpriseCheckYearTruckFileSaveDAL(string I_DOC_ID, DataSet I_XML)
        {
            try
            {
                cmdSurpriseCheckYear.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckYear.CommandText = @"USP_TSURPRISECHKYEARFILE_SAVE";

                cmdSurpriseCheckYear.Parameters.Clear();
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = I_DOC_ID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_XML",
                    Value = I_XML.GetXml(),
                    OracleType = OracleType.Clob
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckYear);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void SurpriseCheckYearTruckUpdateStatusDAL(string I_ID, string I_USERID, string I_IS_ACTIVE, string I_PATH_PTT = "", string I_PATH_VENDOR = "")
        {
            try
            {
                cmdSurpriseCheckYear.CommandType = CommandType.StoredProcedure;
                cmdSurpriseCheckYear.CommandText = @"USP_T_SURPRISEYEARTRUCK_UPDATE";

                cmdSurpriseCheckYear.Parameters.Clear();
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    Value = !string.IsNullOrEmpty(I_ID) ? int.Parse(I_ID) : -1,
                    OracleType = OracleType.Number
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    Value = I_USERID,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_ACTIVE",
                    Value = I_IS_ACTIVE,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_PATH_PTT",
                    Value = I_PATH_PTT,
                    OracleType = OracleType.VarChar
                });
                cmdSurpriseCheckYear.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_PATH_VENDOR",
                    Value = I_PATH_VENDOR,
                    OracleType = OracleType.VarChar
                });

                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdSurpriseCheckYear);
                dbManager.CommitTransaction();
                //return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #region + Instance +
        private static SurpriseCheckYearDAL _instance;
        public static SurpriseCheckYearDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new SurpriseCheckYearDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
