﻿using dbManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.OrderPlan
{
    public partial class PlanTransportDAL : OracleConnectionDAL
    {
        #region PlanTransportDAL
        public PlanTransportDAL()
        {
            InitializeComponent();
        }

        public PlanTransportDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region GetTruckByVendorID
        public DataTable GetTruckByVendorID(string SVENDORID)
        {
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "USP_T_GETTRUCKBYVENDOR_SELECT";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    Value = SVENDORID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                return dbManager.ExecuteDataTable(cmdPlanTransport, "cmdPlanTransport");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            //finally
            //{
            //    dbManager.Close();
            //}
        }
        #endregion

        #region GetTruckByAssign
        public DataTable GetTruckByAssign(string SVENDORID, string DATESTART, string DATEEND)
        {
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "USP_T_GETTRUCKBYASSIGN_SELECT";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    Value = SVENDORID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_DATESTART",
                    IsNullable = true,
                    Value = DATESTART
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_DATEEND",
                    IsNullable = true,
                    Value = DATEEND
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                return dbManager.ExecuteDataTable(cmdPlanTransport, "cmdPlanTransport");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            //finally
            //{
            //    dbManager.Close();
            //}
        }
        #endregion

        #region GetOrderByAssign
        public DataTable GetOrderByAssign(string SVENDORID, string DATESTART, string DATEEND, string ASSIGNDATE, string STERMINALID, string SCONTRACTID, string STRUCKID, string SDELIVERYNO)
        {
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "USP_T_GETORDERBYASSIGN_SELECT";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    Value = SVENDORID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STARTDATE",
                    IsNullable = true,
                    Value = DATESTART
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ENDDATE",
                    IsNullable = true,
                    Value = DATEEND
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ASSIGNDATE",
                    IsNullable = true,
                    Value = ASSIGNDATE
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STERMINALID",
                    IsNullable = true,
                    Value = STERMINALID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_SCONTRACTID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(SCONTRACTID) ? 0 : int.Parse(SCONTRACTID)
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STRUCKID",
                    IsNullable = true,
                    Value = STRUCKID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SDELIVERYNO",
                    IsNullable = true,
                    Value = SDELIVERYNO
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                return dbManager.ExecuteDataTable(cmdPlanTransport, "cmdPlanTransport");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            //finally
            //{
            //    dbManager.Close();
            //}
        }
        #endregion

        #region GetOrderByAssign2
        public DataTable GetOrderByAssign2(string SVENDORID, string ASSIGNDATE, string STERMINALID, string SCONTRACTID, string STRUCKID, string SDELIVERYNO)
        {
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();

                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "USP_T_GETORDERBYASSIGN2_SELECT";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    Value = SVENDORID
                });

                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ASSIGNDATE",
                    IsNullable = true,
                    Value = ASSIGNDATE
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STERMINALID",
                    IsNullable = true,
                    Value = STERMINALID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_SCONTRACTID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(SCONTRACTID) ? 0 : int.Parse(SCONTRACTID)
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STRUCKID",
                    IsNullable = true,
                    Value = STRUCKID
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_SDELIVERYNO",
                    IsNullable = true,
                    Value = SDELIVERYNO
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                return dbManager.ExecuteDataTable(cmdPlanTransport, "cmdPlanTransport");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            //finally
            //{
            //    dbManager.Close();
            //}
        }
        #endregion

        #region GetLateFormIVMS
        public string GetLateFormIVMS(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string VEHICLE, string DESTTIME, string FULLNAME, string EMAIL, string ORDERID, string DATETIMETOTERMINAL)
        {
            DBManager db = new DBManager(DataProvider.Oracle, ConnectionString);
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                db.Open();
                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "IVMS.ORDER_ASSIGN_TO_VEHICLE";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "donumber",
                    Value = DONUMBER
                });

                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "splant",
                    Value = SPLANT
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "shipto",
                    Value = SHIPTO
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "vehicle",
                    Value = VEHICLE
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "fullname",
                    Value = FULLNAME
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "destTime",
                    Value = DESTTIME
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "email",
                    Value = EMAIL
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.VarChar,
                    ParameterName = "RETVAL",
                    Size = 200
                });
                db.ExecuteNonQuery(cmdPlanTransport);
                string RETVAL = cmdPlanTransport.Parameters["RETVAL"].Value + string.Empty;

                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "USP_T_ORDERPLAN_UPDATE_TOTER";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ORDERID",
                    Value = ORDERID
                });

                try
                {
                    string[] strDATETIMETOTERMINAL = DATETIMETOTERMINAL.Trim().Split(' ');
                    string[] strDATETIMETOTERMINALDate, strDATETIMETOTERMINALTime;
                    strDATETIMETOTERMINALDate = strDATETIMETOTERMINAL[0].Split('/');
                    strDATETIMETOTERMINALTime = strDATETIMETOTERMINAL[1].Split(':');
                    DateTime DATETIMETOTERM = new DateTime(int.Parse(strDATETIMETOTERMINALDate[2]), int.Parse(strDATETIMETOTERMINALDate[1]), int.Parse(strDATETIMETOTERMINALDate[0]), int.Parse(strDATETIMETOTERMINALTime[0]), int.Parse(strDATETIMETOTERMINALTime[1]), 0);
                }
                catch (Exception ex)
                {
                    DATETIMETOTERMINAL = string.Empty;
                }

                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_DATETIMETOTERMINAL",
                    Value = DATETIMETOTERMINAL
                });
                dbManager.Open();
                dbManager.ExecuteNonQuery(cmdPlanTransport);

                return RETVAL;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                db.Close();
                dbManager.Close();
            }
        }
        #endregion

        #region CheckLateFormIVMS
        public string CheckLateFormIVMS(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string VEHICLE, string DESTTIME, string FULLNAME, string EMAIL, string ORDERID)
        {
            DBManager db = new DBManager(DataProvider.Oracle, ConnectionString);
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                db.Open();
                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "IVMS.TMS_CHECK_TIMESPLANTBYTRUCK";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "donumber",
                    Value = DONUMBER
                });

                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "splant",
                    Value = SPLANT
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "shipto",
                    Value = SHIPTO
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "vehicle",
                    Value = VEHICLE
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "destTime",
                    Value = DESTTIME
                });

                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.VarChar,
                    ParameterName = "RETVAL",
                    Size = 200
                });
                db.ExecuteNonQuery(cmdPlanTransport);
                string RETVAL = cmdPlanTransport.Parameters["RETVAL"].Value + string.Empty;
                RETVAL = RETVAL.Replace("-", "/");

                //cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                //cmdPlanTransport.CommandText = "USP_T_ORDERPLAN_UPDATE_TOTER";
                //cmdPlanTransport.Parameters.Clear();
                //cmdPlanTransport.Parameters.Add(new OracleParameter()
                //{
                //    Direction = ParameterDirection.Input,
                //    OracleType = OracleType.VarChar,
                //    ParameterName = "I_ORDERID",
                //    Value = ORDERID
                //});

                //cmdPlanTransport.Parameters.Add(new OracleParameter()
                //{
                //    Direction = ParameterDirection.Input,
                //    OracleType = OracleType.VarChar,
                //    ParameterName = "I_DATETIMETOTERMINAL",
                //    Value = RETVAL
                //});
                //dbManager.Open();
                //dbManager.ExecuteNonQuery(cmdPlanTransport);
                return RETVAL;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region CheckLateFormIVMS
        public string CheckLateFormIVMSBefore(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string VEHICLE, string DESTTIME, string FULLNAME, string EMAIL, string ORDERID)
        {
            DBManager db = new DBManager(DataProvider.Oracle, ConnectionString);
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                db.Open();
                //cmdPlanTransport.Dispose();
                cmdPlanTransport.CommandType = CommandType.StoredProcedure;
                cmdPlanTransport.CommandText = "IVMS.CHECK_ORDER_TO_VEHICLE";
                cmdPlanTransport.Parameters.Clear();
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "donumber",
                    Value = DONUMBER
                });

                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "splant",
                    Value = SPLANT
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "shipto",
                    Value = SHIPTO
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "vehicle",
                    Value = VEHICLE
                });
                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "destTime",
                    Value = DESTTIME
                });

                cmdPlanTransport.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.VarChar,
                    ParameterName = "RETVAL",
                    Size = 200
                });
                db.ExecuteNonQuery(cmdPlanTransport);
                string RETVAL = cmdPlanTransport.Parameters["RETVAL"].Value + string.Empty;
                //RETVAL = RETVAL.Replace("-", "/");


                return RETVAL;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region + Instance +
        private static PlanTransportDAL _instance;
        public static PlanTransportDAL Instance
        {
            get
            {
                _instance = new PlanTransportDAL();

                return _instance;
            }
        }
        #endregion
    }
}
