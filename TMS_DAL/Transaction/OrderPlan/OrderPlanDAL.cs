﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;
using dbManager;

namespace TMS_DAL.Transaction.OrderPlan
{
    public partial class OrderPlanDAL : OracleConnectionDAL
    {
        #region OrderPlanDAL
        public OrderPlanDAL()
        {
            InitializeComponent();
        }

        public OrderPlanDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region WarehouseSelectDAL
        public DataTable WarehouseSelectDAL(string VendorID)
        {
            try
            {
                //                cmdWarehouseSelect.CommandText = @"SELECT DISTINCT TTRUCKCONFIRMLIST.STERMINALID,TTERMINALS.STERMINALNAME,TTERMINALS.SABBREVIATION  FROM  TTRUCKCONFIRM  
                //            LEFT JOIN TCONTRACT  ON TCONTRACT.SCONTRACTID=TTRUCKCONFIRM.SCONTRACTID
                //  LEFT JOIN TTRUCKCONFIRMLIST ON TTRUCKCONFIRMLIST.NCONFIRMID = TTRUCKCONFIRM.NCONFIRMID
                //  LEFT JOIN (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE '5%' OR T.STERMINALID LIKE '8%')) TTERMINALS ON TTERMINALS.STERMINALID = TTRUCKCONFIRMLIST.STERMINALID
                //  WHERE TTRUCKCONFIRMLIST.STERMINALID IS NOT NULL AND TCONTRACT.SVENDORID = :SVENDORID";
                cmdWarehouseSelect.CommandText = @"SELECT DISTINCT TTRUCKCONFIRMLIST.STERMINALID,TTERMINALS.STERMINALNAME,TTERMINALS.SABBREVIATION  FROM  TTRUCKCONFIRM  
                LEFT JOIN TCONTRACT  ON TCONTRACT.SCONTRACTID=TTRUCKCONFIRM.SCONTRACTID
                LEFT JOIN TTRUCKCONFIRMLIST ON TTRUCKCONFIRMLIST.NCONFIRMID = TTRUCKCONFIRM.NCONFIRMID
                LEFT JOIN (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE 'H%' OR T.STERMINALID LIKE 'K%')) TTERMINALS ON TTERMINALS.STERMINALID = TTRUCKCONFIRMLIST.STERMINALID
                WHERE TTRUCKCONFIRMLIST.STERMINALID IS NOT NULL AND TCONTRACT.SVENDORID = :SVENDORID";
                cmdWarehouseSelect.Parameters.Clear();
                cmdWarehouseSelect.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "SVENDORID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = VendorID,

                });

                return dbManager.ExecuteDataTable(cmdWarehouseSelect, "cmdWarehouseSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CompanySelectDAL
        public DataTable CompanySelectDAL()
        {
            try
            {
                return dbManager.ExecuteDataTable(cmdCompanySelect, "cmdPlantCodeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelectDAL
        public DataTable ContractSelectDAL(string VendorID)
        {
            try
            {
                cmdContractSelect.CommandText = @"SELECT DISTINCT SCONTRACTID, SCONTRACTNO FROM TBL_ORDERPLAN WHERE CACTIVE = 'Y' AND SVENDORID =:I_SVENDORID";

                cmdContractSelect.Parameters.Clear();
                cmdContractSelect.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = VendorID,

                });
                return dbManager.ExecuteDataTable(cmdContractSelect, "cmdContractSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetOrder1
        public DataTable GetOrder1(string DATESTART, string DATEEND, string NLINE, string STERMINALID, string TXTSEARCH2, string GROUPSID, string SVENDORID, string SCONTRACTID)
        {

            try
            {
                cmdOrderPlan.CommandType = CommandType.StoredProcedure;
                cmdOrderPlan.CommandText = "USP_T_GETORDER1_SELECT_PTT";
                cmdOrderPlan.Parameters.Clear();

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STARTDATE",
                    IsNullable = true,
                    Value = DATESTART
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ENDDATE",
                    IsNullable = true,
                    Value = DATEEND
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_NLINE",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(NLINE) ? 0 : int.Parse(NLINE)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STERMINALID",
                    IsNullable = true,
                    Value = STERMINALID
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_TXTSEARCH2",
                    IsNullable = true,
                    Value = TXTSEARCH2
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_GROUPSID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(GROUPSID) ? 0 : int.Parse(GROUPSID)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(SVENDORID) ? 0 : int.Parse(SVENDORID)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_SCONTRACTID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(SCONTRACTID) ? 0 : int.Parse(SCONTRACTID)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                return dbManager.ExecuteDataTable(cmdOrderPlan, "cmdOrderPlan");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GetOrder2
        public DataTable GetOrder2(string DATESTART, string DATEEND, string NLINE, string STERMINALID, string TXTSEARCH, string GROUPSID, string SVENDORID, string SCONTRACTID)
        {

            try
            {
                cmdOrderPlan.CommandType = CommandType.StoredProcedure;
                cmdOrderPlan.CommandText = "USP_T_GETORDER2_SELECT_PTT";
                cmdOrderPlan.Parameters.Clear();

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STARTDATE",
                    IsNullable = true,
                    Value = DATESTART
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ENDDATE",
                    IsNullable = true,
                    Value = DATEEND
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_NLINE",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(NLINE) ? 0 : int.Parse(NLINE)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_STERMINALID",
                    IsNullable = true,
                    Value = STERMINALID
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_TXTSEARCH",
                    IsNullable = true,
                    Value = TXTSEARCH
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_GROUPSID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(GROUPSID) ? 0 : int.Parse(GROUPSID)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(SVENDORID) ? 0 : int.Parse(SVENDORID)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_SCONTRACTID",
                    IsNullable = true,
                    Value = string.IsNullOrEmpty(SCONTRACTID) ? 0 : int.Parse(SCONTRACTID)
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.Cursor,
                    ParameterName = "O_CUR",
                    IsNullable = true
                });
                return dbManager.ExecuteDataTable(cmdOrderPlan, "cmdOrderPlan");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckLateFormIVMS
        public string CheckLateFormIVMS(string ConnectionString, string DONUMBER, string SPLANT, string SHIPTO, string DESTTIME, string ORDERID)
        {
            DBManager db = new DBManager(DataProvider.Oracle, ConnectionString);
            try
            {
                //dbManager.Dispose();
                //dbManager.Open();
                db.Open();
                //cmdPlanTransport.Dispose();
                cmdOrderPlan.CommandType = CommandType.StoredProcedure;
                cmdOrderPlan.CommandText = "IVMS.TMS_CHECK_TIMESPLANT";
                cmdOrderPlan.Parameters.Clear();
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "donumber",
                    Value = DONUMBER
                });

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "splant",
                    Value = SPLANT
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "shipto",
                    Value = SHIPTO
                });
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "destTime",
                    Value = DESTTIME
                });

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    OracleType = OracleType.VarChar,
                    ParameterName = "RETVAL",
                    Size = 200
                });
                db.ExecuteNonQuery(cmdOrderPlan);
                string RETVAL = cmdOrderPlan.Parameters["RETVAL"].Value + string.Empty;
                RETVAL = RETVAL.Replace("-", "/");

                cmdOrderPlan.CommandType = CommandType.StoredProcedure;
                cmdOrderPlan.CommandText = "USP_T_ORDERPLAN_UPDATE_EXPECT";
                cmdOrderPlan.Parameters.Clear();
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ORDERID",
                    Value = ORDERID
                });

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_DATETIMEEXPECT",
                    Value = RETVAL
                });

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_CHECKLATE",
                    Value = 1
                });
                dbManager.Open();
                dbManager.ExecuteNonQuery(cmdOrderPlan);

                return RETVAL;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                db.Close();
            }
        }
        #endregion

        #region ClearCheckLate
        public void ClearCheckLate(string ORDERID)
        {
            try
            {
                

                cmdOrderPlan.CommandType = CommandType.StoredProcedure;
                cmdOrderPlan.CommandText = "USP_T_ORDERPLAN_UPDATE_EXPECT";
                cmdOrderPlan.Parameters.Clear();
                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_ORDERID",
                    Value = ORDERID
                });

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    ParameterName = "I_DATETIMEEXPECT",
                    Value = DBNull.Value
                });

                cmdOrderPlan.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Number,
                    ParameterName = "I_CHECKLATE",
                    Value = DBNull.Value
                });
                dbManager.Open();
                dbManager.ExecuteNonQuery(cmdOrderPlan);


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region + Instance +
        private static OrderPlanDAL _instance;
        public static OrderPlanDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new OrderPlanDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}