﻿namespace TMS_DAL.Transaction.OrderPlan
{
    partial class OrderPlanDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdWarehouseSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCompanySelect = new System.Data.OracleClient.OracleCommand();
            this.cmdContractSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdOrderPlan = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdWarehouseSelect
            // 
            this.cmdWarehouseSelect.Connection = this.OracleConn;
            // 
            // cmdCompanySelect
            // 
            this.cmdCompanySelect.CommandText = "SELECT DISTINCT SVENDORID, SABBREVIATION FROM TBL_ORDERPLAN WHERE CACTIVE = \'Y\'";
            this.cmdCompanySelect.Connection = this.OracleConn;
            // 
            // cmdContractSelect
            // 
            this.cmdContractSelect.Connection = this.OracleConn;
            // 
            // cmdOrderPlan
            // 
            this.cmdOrderPlan.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdWarehouseSelect;
        private System.Data.OracleClient.OracleCommand cmdCompanySelect;
        private System.Data.OracleClient.OracleCommand cmdContractSelect;
        private System.Data.OracleClient.OracleCommand cmdOrderPlan;
    }
}
