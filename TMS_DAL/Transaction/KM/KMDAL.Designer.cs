﻿namespace TMS_DAL.Transaction.KM
{
    partial class KMDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KMDAL));
            this.cmdinsert = new System.Data.OracleClient.OracleCommand();
            this.cmdKMSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKMTagSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKMBookmarkInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdKmBookmarkStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdKMLikeInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdDeleteKMHeader = new System.Data.OracleClient.OracleCommand();
            this.cmdCountKM = new System.Data.OracleClient.OracleCommand();
            this.cmdKmViewInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdLikeViewKM = new System.Data.OracleClient.OracleCommand();
            this.cmdKMCommentInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdKMCommentSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdKMView = new System.Data.OracleClient.OracleCommand();
            this.cmdKMBookmark = new System.Data.OracleClient.OracleCommand();
            this.cmdKMVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdKMContract = new System.Data.OracleClient.OracleCommand();
            this.cmdKMWorkgroup = new System.Data.OracleClient.OracleCommand();
            this.cmdDefault = new System.Data.OracleClient.OracleCommand();
            this.cmdKMEmail = new System.Data.OracleClient.OracleCommand();
            this.cmdKMCheckShare = new System.Data.OracleClient.OracleCommand();
            this.cmdUserLike = new System.Data.OracleClient.OracleCommand();
            this.cmdKMLikeWebInsert = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdinsert
            // 
            this.cmdinsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TOPIC_NAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_TOPIC_ID", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DEPARTMENT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DIVISION_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_COP", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_KNOWLEDGE_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_OTHER_KEYWORDS", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_EXECUTIVE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DOC", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_KEYWORD", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_KM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_EMPLOYEE", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_USERGROUP", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_DIVIS", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_DEPART", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_USERNAME", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_GROUP", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_TRANSGROUP", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_NAME", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_NUMBER", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_OTHEREMAIL", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdKMSelect
            // 
            this.cmdKMSelect.CommandText = "SELECT * FROM VW_M_KM_SELECT WHERE 1=1 ";
            // 
            // cmdUploadSelect
            // 
            this.cmdUploadSelect.CommandText = "SELECT UPLOAD_ID,UPLOAD_NAME,FILENAME_SYSTEM,FILENAME_USER,FULLPATH FROM VW_M_KMU" +
    "PLOAD_SELECT WHERE 1=1 ";
            // 
            // cmdKMTagSelect
            // 
            this.cmdKMTagSelect.CommandText = resources.GetString("cmdKMTagSelect.CommandText");
            // 
            // cmdKMBookmarkInsert
            // 
            this.cmdKMBookmarkInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdKmBookmarkStatus
            // 
            this.cmdKmBookmarkStatus.CommandText = "select * from T_KM_BOOKMARK where STATUS_ID = 1 ";
            // 
            // cmdKMLikeInsert
            // 
            this.cmdKMLikeInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdDeleteKMHeader
            // 
            this.cmdDeleteKMHeader.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdCountKM
            // 
            this.cmdCountKM.CommandText = "SELECT * FROM VW_M_KM_SELECT_COUNT_CREATE WHERE 1=1 ";
            // 
            // cmdKmViewInsert
            // 
            this.cmdKmViewInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdLikeViewKM
            // 
            this.cmdLikeViewKM.CommandText = "SELECT * FROM VW_M_KM_VIEW_LIKE_SELECT WHERE 1=1 ";
            // 
            // cmdKMCommentInsert
            // 
            this.cmdKMCommentInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_DETAIL", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_KM_COMMENT_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdKMCommentSelect
            // 
            this.cmdKMCommentSelect.CommandText = "SELECT * FROM VW_M_KM_COMMENT_SELECT WHERE 1=1 ";
            // 
            // cmdKMView
            // 
            this.cmdKMView.CommandText = "select * from VW_M_KM_VIEW_SELECT where 1=1 ";
            // 
            // cmdKMBookmark
            // 
            this.cmdKMBookmark.CommandText = "select * from VW_M_KM_BOOKMARK_SELECT where 1=1 ";
            // 
            // cmdKMVendor
            // 
            this.cmdKMVendor.CommandText = "SELECT * FROM VW_M_VENDOR_SELECT WHERE 1=1 ";
            // 
            // cmdKMContract
            // 
            this.cmdKMContract.CommandText = "SELECT * FROM VW_M_CONTRACT_SELECT WHERE 1=1 ";
            // 
            // cmdKMWorkgroup
            // 
            this.cmdKMWorkgroup.CommandText = "SELECT * FROM VW_M_WORKGROUP_SELECT WHERE 1=1 ";
            // 
            // cmdKMEmail
            // 
            this.cmdKMEmail.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DETAIL", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdKMCheckShare
            // 
            this.cmdKMCheckShare.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdUserLike
            // 
            this.cmdUserLike.CommandText = "SELECT COUNT(*) AS IS_LIKE FROM T_KM_HEADER INNER JOIN T_KM_LIKE ON T_KM_HEADER.K" +
    "M_ID = T_KM_LIKE.KM_ID WHERE 1=1 ";
            // 
            // cmdKMLikeWebInsert
            // 
            this.cmdKMLikeWebInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_KM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdinsert;
        private System.Data.OracleClient.OracleCommand cmdKMSelect;
        private System.Data.OracleClient.OracleCommand cmdUploadSelect;
        private System.Data.OracleClient.OracleCommand cmdKMTagSelect;
        private System.Data.OracleClient.OracleCommand cmdKMBookmarkInsert;
        private System.Data.OracleClient.OracleCommand cmdKmBookmarkStatus;
        private System.Data.OracleClient.OracleCommand cmdKMLikeInsert;
        private System.Data.OracleClient.OracleCommand cmdDeleteKMHeader;
        private System.Data.OracleClient.OracleCommand cmdCountKM;
        private System.Data.OracleClient.OracleCommand cmdKmViewInsert;
        private System.Data.OracleClient.OracleCommand cmdLikeViewKM;
        private System.Data.OracleClient.OracleCommand cmdKMCommentInsert;
        private System.Data.OracleClient.OracleCommand cmdKMCommentSelect;
        private System.Data.OracleClient.OracleCommand cmdKMView;
        private System.Data.OracleClient.OracleCommand cmdKMBookmark;
        private System.Data.OracleClient.OracleCommand cmdKMVendor;
        private System.Data.OracleClient.OracleCommand cmdKMContract;
        private System.Data.OracleClient.OracleCommand cmdKMWorkgroup;
        private System.Data.OracleClient.OracleCommand cmdDefault;
        private System.Data.OracleClient.OracleCommand cmdKMEmail;
        private System.Data.OracleClient.OracleCommand cmdKMCheckShare;
        private System.Data.OracleClient.OracleCommand cmdUserLike;
        private System.Data.OracleClient.OracleCommand cmdKMLikeWebInsert;
    }
}
