﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;
using dbManager;

namespace TMS_DAL.Transaction.KM
{
    public partial class KMDAL : OracleConnectionDAL
    {
        public KMDAL()
        {
            InitializeComponent();
        }

        public KMDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable SelectKMDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMSelect, "cmdKMSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMSharetoDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdDefault.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdDefault, "cmdDefault");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMVendorDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMVendor.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMVendor, "cmdKMVendor");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMWorkgroupDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMWorkgroup.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMWorkgroup, "cmdKMWorkgroup");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMContractDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMContract.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMContract, "cmdKMContract");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectCountKMDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdCountKM.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCountKM, "cmdCountKM");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectLikeViewKMDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdLikeViewKM.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdLikeViewKM, "cmdLikeViewKM");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectUserLikeKMDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdUserLike.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdUserLike, "cmdUserLike");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
//        SELECT COUNT(*)
//FROM T_KM_HEADER 
//INNER JOIN T_KM_LIKE ON T_KM_HEADER.KM_ID = T_KM_LIKE.KM_ID
//WHERE T_KM_HEADER.TOPIC_ID = 'รข.0002' AND T_KM_LIKE.USER_ID = '1100'

        public DataTable SelectKMBookmarkDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKmBookmarkStatus.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKmBookmarkStatus, "cmdKmBookmarkStatus");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMTagDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMTagSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMTagSelect, "cmdKMTagSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable SelectKMCommentDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMCommentSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMCommentSelect, "cmdKMCommentSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMUploadDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdUploadSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdUploadSelect, "cmdUploadSelect");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMViewDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMView.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMView, "cmdKMView");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectKMBookDAL(string Condition)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();
                cmdKMBookmark.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdKMBookmark, "cmdKMBookmark");
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable DeleteKMBHeaderDAL(int KM, int Creater, int Status)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdDeleteKMHeader.CommandText = "USP_M_KM_HEADER_DELETE";
                cmdDeleteKMHeader.CommandType = CommandType.StoredProcedure;

                cmdDeleteKMHeader.Parameters["I_KM_ID"].Value = KM;
                cmdDeleteKMHeader.Parameters["I_STATUS"].Value = Status;
                cmdDeleteKMHeader.Parameters["I_CREATE_BY"].Value = Creater;

                DataTable dt = dbManager.ExecuteDataTable(cmdDeleteKMHeader, "cmdDeleteKMHeader");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }
        public DataTable InsertKMViewDAL(int KM)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdKmViewInsert.CommandText = "USP_M_KM_VIEW_INSERT";
                cmdKmViewInsert.CommandType = CommandType.StoredProcedure;

                cmdKmViewInsert.Parameters["I_KM_ID"].Value = KM;

                DataTable dt = dbManager.ExecuteDataTable(cmdKmViewInsert, "cmdKmViewInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }

        public DataTable InsertKMBLikeDAL(int KM, int Creater, int Status)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdKMLikeInsert.CommandText = "USP_M_KM_LIKE_INSERT";
                cmdKMLikeInsert.CommandType = CommandType.StoredProcedure;

                cmdKMLikeInsert.Parameters["I_KM_ID"].Value = KM;
                cmdKMLikeInsert.Parameters["I_STATUS"].Value = Status;
                cmdKMLikeInsert.Parameters["I_CREATE_BY"].Value = Creater;

                DataTable dt = dbManager.ExecuteDataTable(cmdKMLikeInsert, "cmdKMLikeInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }

        public DataTable InsertKMBLikeWebDAL(int KM, int Creater, int Status)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdKMLikeWebInsert.CommandText = "USP_M_KM_LIKE_WEB_INSERT";
                cmdKMLikeWebInsert.CommandType = CommandType.StoredProcedure;

                cmdKMLikeWebInsert.Parameters["I_KM_ID"].Value = KM;
                cmdKMLikeWebInsert.Parameters["I_STATUS"].Value = Status;
                cmdKMLikeWebInsert.Parameters["I_CREATE_BY"].Value = Creater;

                DataTable dt = dbManager.ExecuteDataTable(cmdKMLikeWebInsert, "cmdKMLikeWebInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);

            }
            finally
            {
                dbManager.Close();
            }

        }

        public DataTable InsertKMBookmarkDAL(int KM, int Creater, int Status)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdKMBookmarkInsert.CommandText = "USP_M_KM_BOOKMARK_INSERT";
                cmdKMBookmarkInsert.CommandType = CommandType.StoredProcedure;

                cmdKMBookmarkInsert.Parameters["I_KM_ID"].Value = KM;
                cmdKMBookmarkInsert.Parameters["I_STATUS"].Value = Status;
                cmdKMBookmarkInsert.Parameters["I_CREATE_BY"].Value = Creater;

                DataTable dt = dbManager.ExecuteDataTable(cmdKMBookmarkInsert, "cmdKMBookmarkInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);


            }
            finally
            {
                dbManager.Close();
            }

        }

        public DataTable InsertKMCommentDAL(int KM_Comment, int KM, int Creater, int Status, string Detail)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdKMCommentInsert.CommandText = "USP_M_KM_COMMENT_INSERT";
                cmdKMCommentInsert.CommandType = CommandType.StoredProcedure;

                cmdKMCommentInsert.Parameters["I_KM_COMMENT_ID"].Value = KM_Comment;
                cmdKMCommentInsert.Parameters["I_KM_ID"].Value = KM;
                cmdKMCommentInsert.Parameters["I_STATUS"].Value = Status;
                cmdKMCommentInsert.Parameters["I_CREATE_BY"].Value = Creater;
                cmdKMCommentInsert.Parameters["I_DETAIL"].Value = Detail;

                DataTable dt = dbManager.ExecuteDataTable(cmdKMCommentInsert, "cmdKMCommentInsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);


            }
            finally
            {
                dbManager.Close();
            }

        }

        public DataTable SelectKMEmailDAL(int Type, string Detail)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdKMEmail.CommandText = "USP_M_CHECK_KM_EMAIL_SELECT";
                cmdKMEmail.CommandType = CommandType.StoredProcedure;

                cmdKMEmail.Parameters["I_TYPE"].Value = Type;
                cmdKMEmail.Parameters["I_DETAIL"].Value = Detail;

                DataTable dt = dbManager.ExecuteDataTable(cmdKMEmail, "cmdKMEmail");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);


            }
            finally
            {
                dbManager.Close();
            }

        }

        public DataTable SelectKMCheckShareDAL(int Type, int UserID)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                if (Type == 1)
                    cmdKMCheckShare.CommandText = "USP_M_CHECK_KM_PTT_SELECT";
                else
                    cmdKMCheckShare.CommandText = "USP_M_CHECK_KM_VENDOR_SELECT";

                cmdKMCheckShare.CommandType = CommandType.StoredProcedure;

                cmdKMCheckShare.Parameters["I_USER_ID"].Value = UserID;

                DataTable dt = dbManager.ExecuteDataTable(cmdKMCheckShare, "cmdKMCheckShare");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);


            }
            finally
            {
                dbManager.Close();
            }

        }

        public DataTable InsertKMDAL(int KM, string TopicName, string TopicID, int Creater, int Department, int Division, string CoP, int Knowledge, DataTable Keyword, string OtherKey, string Executive, DataTable Doc, int Status, DataTable Employee, DataTable Usergroup, DataTable Divis, DataTable Depart, DataTable Cont_Username, DataTable Cont_Group, DataTable Cont_Transgroup, DataTable Cont_Name, DataTable Cont_Number, string OtherEmail)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                dbManager.BeginTransaction();
                cmdinsert.CommandText = "USP_M_KM_INSERT";
                cmdinsert.CommandType = CommandType.StoredProcedure;

                cmdinsert.Parameters["I_KM_ID"].Value = KM;
                cmdinsert.Parameters["I_TOPIC_NAME"].Value = TopicName;
                cmdinsert.Parameters["I_TOPIC_ID"].Value = TopicID;
                cmdinsert.Parameters["I_CREATE_BY"].Value = Creater;
                cmdinsert.Parameters["I_DEPARTMENT_ID"].Value = Department;
                cmdinsert.Parameters["I_DIVISION_ID"].Value = Division;
                cmdinsert.Parameters["I_COP"].Value = CoP;
                cmdinsert.Parameters["I_KNOWLEDGE_TYPE_ID"].Value = Knowledge;
                cmdinsert.Parameters["I_OTHER_KEYWORDS"].Value = OtherKey;
                cmdinsert.Parameters["I_EXECUTIVE"].Value = Executive;
                cmdinsert.Parameters["I_STATUS_ID"].Value = Status;
                cmdinsert.Parameters["I_OTHEREMAIL"].Value = OtherEmail;

                DataSet doc_ds = new DataSet("doc_ds");
                Doc.TableName = "doc";
                doc_ds.Tables.Add(Doc);
                cmdinsert.Parameters["I_DOC"].Value = doc_ds.GetXml();

                DataSet keyword_ds = new DataSet("keyword_ds");
                Keyword.TableName = "keyword";
                keyword_ds.Tables.Add(Keyword);
                cmdinsert.Parameters["I_KEYWORD"].Value = keyword_ds.GetXml();

                DataSet employee_ds = new DataSet("employee_ds");
                Employee.TableName = "employee";
                employee_ds.Tables.Add(Employee);
                cmdinsert.Parameters["I_EMPLOYEE"].Value = employee_ds.GetXml();
                DataSet usergroup_ds = new DataSet("usergroup_ds");
                Usergroup.TableName = "usergroup";
                usergroup_ds.Tables.Add(Usergroup);
                cmdinsert.Parameters["I_USERGROUP"].Value = usergroup_ds.GetXml();
                DataSet divis_ds = new DataSet("divis_ds");
                Divis.TableName = "divis";
                divis_ds.Tables.Add(Divis);
                cmdinsert.Parameters["I_DIVIS"].Value = divis_ds.GetXml();
                DataSet depart_ds = new DataSet("depart_ds");
                Depart.TableName = "depart";
                depart_ds.Tables.Add(Depart);
                cmdinsert.Parameters["I_DEPART"].Value = depart_ds.GetXml();

                DataSet cont_username_ds = new DataSet("cont_username_ds");
                Cont_Username.TableName = "cont_username";
                cont_username_ds.Tables.Add(Cont_Username);
                cmdinsert.Parameters["I_CONTRACT_USERNAME"].Value = cont_username_ds.GetXml();
                DataSet cont_group_ds = new DataSet("cont_group_ds");
                Cont_Group.TableName = "cont_group";
                cont_group_ds.Tables.Add(Cont_Group);
                cmdinsert.Parameters["I_CONTRACT_GROUP"].Value = cont_group_ds.GetXml();
                DataSet cont_transgroup_ds = new DataSet("cont_transgroup_ds");
                Cont_Transgroup.TableName = "cont_transgroup";
                cont_transgroup_ds.Tables.Add(Cont_Transgroup);
                cmdinsert.Parameters["I_CONTRACT_TRANSGROUP"].Value = cont_transgroup_ds.GetXml();
                DataSet cont_name_ds = new DataSet("cont_name_ds");
                Cont_Name.TableName = "cont_name";
                cont_name_ds.Tables.Add(Cont_Name);
                cmdinsert.Parameters["I_CONTRACT_NAME"].Value = cont_name_ds.GetXml();
                DataSet cont_number_ds = new DataSet("cont_number_ds");
                Cont_Number.TableName = "cont_number";
                cont_number_ds.Tables.Add(Cont_Number);
                cmdinsert.Parameters["I_CONTRACT_NUMBER"].Value = cont_number_ds.GetXml();

                DataTable dt = dbManager.ExecuteDataTable(cmdinsert, "cmdinsert");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);


            }
            finally
            {
                dbManager.Close();
            }

        }

        #region + Instance +
        private static KMDAL _instance;
        public static KMDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new KMDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}
