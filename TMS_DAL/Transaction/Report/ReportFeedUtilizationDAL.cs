﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.Report
{
    public partial class ReportFeedUtilizationDAL : OracleConnectionDAL
    {
        #region + Instance +
        private static ReportFeedUtilizationDAL _instance;
        public static ReportFeedUtilizationDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ReportFeedUtilizationDAL();
                //}
                return _instance;
            }
        }
        #endregion

        #region ReportFeedUtilizationDAL
        public ReportFeedUtilizationDAL()
        {
            InitializeComponent();
        }

        public ReportFeedUtilizationDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region ReportFeedUtilizationSelect
        public DataTable ReportFeedUtilizationSelect(int GROUPID, string DDELIVERYSTART, string DDELIVERYEND, string SESSIONID)
        {
            try
            {
                DateTime date;
                DateTime sdate = new DateTime();
                if (DateTime.TryParseExact(DDELIVERYSTART, "dd/MM/yyyy",new CultureInfo("en-US"), DateTimeStyles.None, out date))
                {
                    sdate = date;
                }
                DateTime edate = new DateTime();
                if (DateTime.TryParseExact(DDELIVERYEND, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out date))
                {
                    edate = date;
                }
                dbManager.Open();
                dbManager.BeginTransaction();
                cmdReportFeedUtilization.CommandType = CommandType.Text;
                cmdReportFeedUtilization.CommandText = "DELETE M_DATETEMP where SESSIONID = :SESSIONID";
                cmdReportFeedUtilization.Parameters.Clear();
                cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "SESSIONID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = SESSIONID,
                });
                dbManager.ExecuteNonQuery(cmdReportFeedUtilization);
                cmdReportFeedUtilization.CommandType = CommandType.Text;
                for (DateTime i = sdate; i <= edate; i = i.AddDays(1))
                {
                    cmdReportFeedUtilization.CommandText = @"INSERT INTO M_DATETEMP( DDELIVERY, GROUPID,SESSIONID ) SELECT :DDELIVERY,ID,:SESSIONID FROM M_GROUPS WHERE ID = :GROUPID OR (:GROUPID = 0 AND 0=0)";
                    cmdReportFeedUtilization.Parameters.Clear();
                    cmdReportFeedUtilization.Parameters.Add(new OracleParameter() {
                        Direction = ParameterDirection.Input,
                        ParameterName = "DDELIVERY",
                        IsNullable = true,
                        OracleType = OracleType.DateTime,
                        Value = i,
                    });
                    cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "GROUPID",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = GROUPID,
                    });
                    cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "SESSIONID",
                        IsNullable = true,
                        OracleType = OracleType.VarChar,
                        Value = SESSIONID,
                    });
                    dbManager.ExecuteNonQuery(cmdReportFeedUtilization);
                }
                dbManager.CommitTransaction();

                cmdReportFeedUtilization.CommandType = CommandType.StoredProcedure;
				cmdReportFeedUtilization.CommandText = @"USP_REPORT_FLEET_UTILIZATION";
				//cmdReportFeedUtilization.CommandText = @"USP_T_REPORTFEED_SELECT";

                cmdReportFeedUtilization.Parameters.Clear();
                cmdReportFeedUtilization.Parameters.Add(new OracleParameter() { 
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DDELIVERYSTART",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = DDELIVERYSTART,
                });
                cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DDELIVERYEND",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = DDELIVERYEND,
                });
                cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_GROUPID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = GROUPID,
                });
                cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SESSIONID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = SESSIONID,
                });
                cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdReportFeedUtilization, "cmdReportFeedUtilization");
                cmdReportFeedUtilization.CommandType = CommandType.Text;
                cmdReportFeedUtilization.CommandText = "DELETE M_DATETEMP where SESSIONID = :SESSIONID";
                cmdReportFeedUtilization.Parameters.Clear();
                cmdReportFeedUtilization.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "SESSIONID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = SESSIONID,
                });
                dbManager.ExecuteNonQuery(cmdReportFeedUtilization);
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect()
        {
            try
            {
                cmdReportFeedUtilization.CommandType = CommandType.Text;
                cmdReportFeedUtilization.CommandText = @"SELECT M_GROUPS.ID || ',' || M_WORKGROUP.NAME VALUE,M_GROUPS.NAME FROM M_GROUPS LEFT JOIN M_WORKGROUP ON M_WORKGROUP.ID = M_GROUPS.WORKGROUPID WHERE M_GROUPS.CACTIVE = 1 ORDER BY M_GROUPS.ID";

                cmdReportFeedUtilization.Parameters.Clear();
                
                return dbManager.ExecuteDataTable(cmdReportFeedUtilization, "cmdReportFeedUtilization");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
