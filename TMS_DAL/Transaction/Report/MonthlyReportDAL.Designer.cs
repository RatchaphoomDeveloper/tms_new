﻿namespace TMS_DAL.Transaction.Report
{
    partial class MonthlyReportDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonthlyReportDAL));
            this.cmdReportRequestFile = new System.Data.OracleClient.OracleCommand();
            this.cmdReportSaveUpload = new System.Data.OracleClient.OracleCommand();
            this.cmdReportSave = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdCanUploadSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdMonthSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdYearSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCusScoreSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdReportSaveTab4 = new System.Data.OracleClient.OracleCommand();
            this.cmdReportCheckSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdReportCheckSelectDetail = new System.Data.OracleClient.OracleCommand();
            this.cmdGetDateNow = new System.Data.OracleClient.OracleCommand();
            this.cmdCanUploadSelectAll = new System.Data.OracleClient.OracleCommand();
            this.cmdReportCheckSelectTab6 = new System.Data.OracleClient.OracleCommand();
            this.cmdReportCheckSelectDetailTab6 = new System.Data.OracleClient.OracleCommand();
            this.cmdAddCusScore = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectIDUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectContractByVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdGetEmailComplainAdmin = new System.Data.OracleClient.OracleCommand();
            this.cmdGetEmailVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdReportSelectConfig = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckTPNRequestFile = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectSpecialValue = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdReportRequestFile
            // 
            this.cmdReportRequestFile.CommandText = resources.GetString("cmdReportRequestFile.CommandText");
            this.cmdReportRequestFile.Connection = this.OracleConn;
            // 
            // cmdReportSaveUpload
            // 
            this.cmdReportSaveUpload.CommandText = resources.GetString("cmdReportSaveUpload.CommandText");
            this.cmdReportSaveUpload.Connection = this.OracleConn;
            this.cmdReportSaveUpload.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500)});
            // 
            // cmdReportSave
            // 
            this.cmdReportSave.CommandText = "USP_T_MONTHLY_INSERT";
            this.cmdReportSave.Connection = this.OracleConn;
            this.cmdReportSave.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTHID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_IS_PASS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_UPLOAD_COMPLETE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CUS_SCORE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REPORT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISSAVE_CONFIRM_DATE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdImportFileDelete
            // 
            this.cmdImportFileDelete.CommandText = "DELETE FROM F_UPLOAD WHERE REF_STR =: I_DOC_ID AND REF_INT =: I_SCONTRACTID";
            this.cmdImportFileDelete.Connection = this.OracleConn;
            this.cmdImportFileDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdCanUploadSelect
            // 
            this.cmdCanUploadSelect.CommandText = resources.GetString("cmdCanUploadSelect.CommandText");
            this.cmdCanUploadSelect.Connection = this.OracleConn;
            // 
            // cmdMonthSelect
            // 
            this.cmdMonthSelect.CommandText = resources.GetString("cmdMonthSelect.CommandText");
            this.cmdMonthSelect.Connection = this.OracleConn;
            this.cmdMonthSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4)});
            // 
            // cmdYearSelect
            // 
            this.cmdYearSelect.CommandText = "SELECT DISTINCT UPLOAD_YEAR FROM TMONTHLY_HEADER ORDER BY UPLOAD_YEAR DESC";
            this.cmdYearSelect.Connection = this.OracleConn;
            // 
            // cmdCusScoreSelect
            // 
            this.cmdCusScoreSelect.Connection = this.OracleConn;
            this.cmdCusScoreSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTH_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdReportSaveTab4
            // 
            this.cmdReportSaveTab4.CommandText = resources.GetString("cmdReportSaveTab4.CommandText");
            this.cmdReportSaveTab4.Connection = this.OracleConn;
            this.cmdReportSaveTab4.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTHID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_IS_PASS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DESCRIPTION", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CUS_SCORE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REPORT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_FINAL_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdReportCheckSelect
            // 
            this.cmdReportCheckSelect.CommandText = resources.GetString("cmdReportCheckSelect.CommandText");
            this.cmdReportCheckSelect.Connection = this.OracleConn;
            this.cmdReportCheckSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTHID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdReportCheckSelectDetail
            // 
            this.cmdReportCheckSelectDetail.CommandText = "SELECT IS_UPLOAD_COMPLETE, DOC_STATUS_ID FROM TMONTHLY_HEADER WHERE TMONTHLY_HEAD" +
    "ER.UPLOAD_YEAR =: I_YEAR AND TMONTHLY_HEADER.MONTH_ID =: I_MONTHID";
            this.cmdReportCheckSelectDetail.Connection = this.OracleConn;
            this.cmdReportCheckSelectDetail.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTHID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetDateNow
            // 
            this.cmdGetDateNow.CommandText = "SELECT     TO_CHAR (SYSDATE, \'DD/MM/YYYY\') AS TODAY FROM DUAL";
            this.cmdGetDateNow.Connection = this.OracleConn;
            // 
            // cmdCanUploadSelectAll
            // 
            this.cmdCanUploadSelectAll.CommandText = resources.GetString("cmdCanUploadSelectAll.CommandText");
            this.cmdCanUploadSelectAll.Connection = this.OracleConn;
            // 
            // cmdReportCheckSelectTab6
            // 
            this.cmdReportCheckSelectTab6.CommandText = resources.GetString("cmdReportCheckSelectTab6.CommandText");
            this.cmdReportCheckSelectTab6.Connection = this.OracleConn;
            this.cmdReportCheckSelectTab6.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4)});
            // 
            // cmdReportCheckSelectDetailTab6
            // 
            this.cmdReportCheckSelectDetailTab6.Connection = this.OracleConn;
            this.cmdReportCheckSelectDetailTab6.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTHID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REQUEST_TYPE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REF_STR", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_REF_INT", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdAddCusScore
            // 
            this.cmdAddCusScore.CommandText = resources.GetString("cmdAddCusScore.CommandText");
            this.cmdAddCusScore.Connection = this.OracleConn;
            this.cmdAddCusScore.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SREDUCETYPE", System.Data.OracleClient.OracleType.VarChar, 25),
            new System.Data.OracleClient.OracleParameter("I_SPROCESSID", System.Data.OracleClient.OracleType.VarChar, 25),
            new System.Data.OracleClient.OracleParameter("I_TTOPIC_TYPE_ID", System.Data.OracleClient.OracleType.VarChar, 25),
            new System.Data.OracleClient.OracleParameter("I_SREDUCEBY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SREFERENCEID", System.Data.OracleClient.OracleType.VarChar, 25),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DREDUCE", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdSelectIDUpdate
            // 
            this.cmdSelectIDUpdate.CommandText = resources.GetString("cmdSelectIDUpdate.CommandText");
            this.cmdSelectIDUpdate.Connection = this.OracleConn;
            this.cmdSelectIDUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTHID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_REPORT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdSelectContractByVendor
            // 
            this.cmdSelectContractByVendor.CommandText = "SELECT SCONTRACTID, SCONTRACTNO, SVENDORID FROM TCONTRACT WHERE CACTIVE = \'Y\' AND" +
    " SVENDORID =: I_SVENDORID";
            this.cmdSelectContractByVendor.Connection = this.OracleConn;
            this.cmdSelectContractByVendor.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 25)});
            // 
            // cmdGetEmailComplainAdmin
            // 
            this.cmdGetEmailComplainAdmin.CommandText = "SELECT SEMAIL AS EMAIL FROM TUSER WHERE SVENDORID =:I_SVENDORID AND SPOSITION <> " +
    "\'ผจ.ขปน.\'";
            this.cmdGetEmailComplainAdmin.Connection = this.OracleConn;
            this.cmdGetEmailComplainAdmin.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdGetEmailVendor
            // 
            this.cmdGetEmailVendor.CommandText = "SELECT EMAIL FROM TVENDOR WHERE SVENDORID =: I_SVENDORID";
            this.cmdGetEmailVendor.Connection = this.OracleConn;
            this.cmdGetEmailVendor.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdReportSelectConfig
            // 
            this.cmdReportSelectConfig.Connection = this.OracleConn;
            this.cmdReportSelectConfig.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_YEAR", System.Data.OracleClient.OracleType.VarChar, 4),
            new System.Data.OracleClient.OracleParameter("I_MONTHID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_IS_PASS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_UPLOAD_COMPLETE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CUS_SCORE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REPORT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISSAVE_CONFIRM_DATE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SCONTRACTID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckTPNRequestFile
            // 
            this.cmdTruckTPNRequestFile.Connection = this.OracleConn;
            // 
            // cmdSelectSpecialValue
            // 
            this.cmdSelectSpecialValue.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdReportRequestFile;
        private System.Data.OracleClient.OracleCommand cmdReportSaveUpload;
        private System.Data.OracleClient.OracleCommand cmdReportSave;
        private System.Data.OracleClient.OracleCommand cmdImportFileDelete;
        private System.Data.OracleClient.OracleCommand cmdCanUploadSelect;
        private System.Data.OracleClient.OracleCommand cmdMonthSelect;
        private System.Data.OracleClient.OracleCommand cmdYearSelect;
        private System.Data.OracleClient.OracleCommand cmdCusScoreSelect;
        private System.Data.OracleClient.OracleCommand cmdReportSaveTab4;
        private System.Data.OracleClient.OracleCommand cmdReportCheckSelect;
        private System.Data.OracleClient.OracleCommand cmdReportCheckSelectDetail;
        private System.Data.OracleClient.OracleCommand cmdGetDateNow;
        private System.Data.OracleClient.OracleCommand cmdCanUploadSelectAll;
        private System.Data.OracleClient.OracleCommand cmdReportCheckSelectTab6;
        private System.Data.OracleClient.OracleCommand cmdReportCheckSelectDetailTab6;
        private System.Data.OracleClient.OracleCommand cmdAddCusScore;
        private System.Data.OracleClient.OracleCommand cmdSelectIDUpdate;
        private System.Data.OracleClient.OracleCommand cmdSelectContractByVendor;
        private System.Data.OracleClient.OracleCommand cmdGetEmailComplainAdmin;
        private System.Data.OracleClient.OracleCommand cmdGetEmailVendor;
        private System.Data.OracleClient.OracleCommand cmdReportSelectConfig;
        private System.Data.OracleClient.OracleCommand cmdTruckTPNRequestFile;
        private System.Data.OracleClient.OracleCommand cmdSelectSpecialValue;
    }
}
