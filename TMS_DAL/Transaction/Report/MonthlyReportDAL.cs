﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;

namespace TMS_DAL.Transaction.Report
{
    public partial class MonthlyReportDAL : OracleConnectionDAL
    {
        public MonthlyReportDAL()
        {
            InitializeComponent();
        }

        public MonthlyReportDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable MonthlyReportRequestFileDAL(string Condition)
        {
            string Query = cmdReportRequestFile.CommandText;
            try
            {
                dbManager.Open();
                cmdReportRequestFile.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdReportRequestFile, "cmdReportRequestFile");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdReportRequestFile.CommandText = Query;
            }
        }

        public DataTable TruckTPNRequestFileDAL()
        {
            try
            {
                cmdReportRequestFile.CommandText = @"SELECT DISTINCT TMP.* FROM
                                                     (
                                                     SELECT M_UPLOAD_TYPE.UPLOAD_TYPE AS REQUEST_TYPE, M_UPLOAD_TYPE.UPLOAD_ID, M_UPLOAD_TYPE.UPLOAD_NAME, M_UPLOAD_TYPE.EXTENTION, M_UPLOAD_TYPE.MAX_FILE_SIZE 
                                                     FROM  M_UPLOAD_TYPE LEFT JOIN M_COMPLAIN_REQUEST_FILE ON M_UPLOAD_TYPE.UPLOAD_ID = M_COMPLAIN_REQUEST_FILE.UPLOAD_ID 
                                                     WHERE (M_UPLOAD_TYPE.ISACTIVE = 1)
                                                     AND M_UPLOAD_TYPE.UPLOAD_TYPE = 'TRUCK'
                                                     AND M_UPLOAD_TYPE.SPECIAL_FLAG = '1'
 
                                                     UNION ALL
 
                                                     SELECT M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE, M_UPLOAD_TYPE.UPLOAD_ID, M_UPLOAD_TYPE.UPLOAD_NAME, M_UPLOAD_TYPE.EXTENTION, M_UPLOAD_TYPE.MAX_FILE_SIZE
                                                     FROM  M_COMPLAIN_REQUEST_FILE, M_UPLOAD_TYPE
                                                     WHERE M_COMPLAIN_REQUEST_FILE.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID
                                                     AND M_UPLOAD_TYPE.UPLOAD_TYPE = 'TRUCK'
                                                     AND (M_COMPLAIN_REQUEST_FILE.ISACTIVE = 1)
                                                     AND (M_UPLOAD_TYPE.ISACTIVE = 1)
                                                     AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 1
                                                     ) TMP";

                DataTable dtRequestFileTPN = dbManager.ExecuteDataTable(cmdReportRequestFile, "cmdReportRequestFile");

                return dtRequestFileTPN;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ReportSaveDAL(bool IsSaveFileUpload, DataTable dtUpload, string Year, int MonthID, string VendorID, int CreateBy, int StatusID, int ReportID, int IsPass, int IsUploadComplete, int CusScore, int isSaveConfirmDate, string SCONTRACTID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdReportSave.CommandType = CommandType.StoredProcedure;
                cmdReportSave.Parameters["I_YEAR"].Value = Year;
                cmdReportSave.Parameters["I_MONTHID"].Value = MonthID;
                cmdReportSave.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdReportSave.Parameters["I_IS_PASS"].Value = IsPass;
                cmdReportSave.Parameters["I_DOC_STATUS_ID"].Value = StatusID;
                cmdReportSave.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdReportSave.Parameters["I_IS_UPLOAD_COMPLETE"].Value = IsUploadComplete;
                cmdReportSave.Parameters["I_CUS_SCORE"].Value = CusScore;
                cmdReportSave.Parameters["I_REPORT_ID"].Value = ReportID;
                cmdReportSave.Parameters["I_ISSAVE_CONFIRM_DATE"].Value = isSaveConfirmDate;
                cmdReportSave.Parameters["I_SCONTRACTID"].Value = SCONTRACTID;

                dbManager.ExecuteNonQuery(cmdReportSave);

                if (IsSaveFileUpload)
                {
                    cmdImportFileDelete.Parameters["I_DOC_ID"].Value = Year + "-" + MonthID.ToString() + "-" + ReportID.ToString() + "-" + VendorID;
                    cmdImportFileDelete.Parameters["I_SCONTRACTID"].Value = SCONTRACTID;
                    dbManager.ExecuteNonQuery(cmdImportFileDelete);

                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        cmdReportSaveUpload.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                        cmdReportSaveUpload.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                        cmdReportSaveUpload.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                        cmdReportSaveUpload.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                        cmdReportSaveUpload.Parameters["REF_INT"].Value = dtUpload.Rows[i]["REF_INT"].ToString();
                        cmdReportSaveUpload.Parameters["REF_STR"].Value = Year + "-" + MonthID.ToString() + "-" + ReportID.ToString() + "-" + VendorID;
                        cmdReportSaveUpload.Parameters["ISACTIVE"].Value = "1";
                        cmdReportSaveUpload.Parameters["CREATE_BY"].Value = CreateBy;

                        dbManager.ExecuteNonQuery(cmdReportSaveUpload);
                    }
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable CanUploadSelectDAL()
        {
            try
            {
                dbManager.Open();
                cmdCanUploadSelect.CommandText = "SELECT EXTRACT(YEAR FROM SYSDATE) AS YEAR, EXTRACT(MONTH FROM SYSDATE) AS MONTH, MONTH_REPORT_ID, MONTH_ID, REPORT_ID, DESCRIPTION, START_UPLOAD_DAY, END_UPLOAD_DAY, DESCRIPTION_DETAIL FROM M_MONTH_REPORT_REQUIRE WHERE MONTH_ID = (SELECT EXTRACT(MONTH FROM SYSDATE) FROM DUAL) AND EXTRACT(DAY FROM SYSDATE) BETWEEN START_UPLOAD_DAY AND END_UPLOAD_DAY AND M_MONTH_REPORT_REQUIRE.IS_ACTIVE = 1";

                return dbManager.ExecuteDataTable(cmdCanUploadSelect, "cmdCanUploadSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable CanUploadSelectAllDAL()
        {
            try
            {
                dbManager.Open();
                cmdCanUploadSelectAll.CommandText = "SELECT EXTRACT(YEAR FROM SYSDATE) AS YEAR, EXTRACT(MONTH FROM SYSDATE) AS MONTH, MONTH_REPORT_ID, MONTH_ID, REPORT_ID, DESCRIPTION, START_UPLOAD_DAY, END_UPLOAD_DAY, DESCRIPTION_DETAIL FROM M_MONTH_REPORT_REQUIRE WHERE M_MONTH_REPORT_REQUIRE.IS_ACTIVE = 1";

                return dbManager.ExecuteDataTable(cmdCanUploadSelectAll, "cmdCanUploadSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable MonthSelectDAL(string Year)
        {
            try
            {
                dbManager.Open();
                cmdMonthSelect.Parameters["I_YEAR"].Value = Year;

                return dbManager.ExecuteDataTable(cmdMonthSelect, "cmdMonthSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable YearSelectDAL()
        {
            try
            {
                dbManager.Open();
                return dbManager.ExecuteDataTable(cmdYearSelect, "cmdYearSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable CusScoreSelectDAL(string Year, int MonthID, string VendorID, string SCONTRACTID)
        {
            try
            {
                DataTable dt = new DataTable();
                dbManager.Open();
                cmdCusScoreSelect.CommandText = @"SELECT TMP.*
                                                  , CASE WHEN (
                                                                  SELECT COUNT(*) FROM M_COMPLAIN_REQUEST_FILE WHERE REQUEST_TYPE = TMP.REPORT_NAME AND UPLOAD_ID NOT IN (SELECT UPLOAD_ID FROM F_UPLOAD WHERE REF_STR =:I_YEAR || '-' || :I_MONTH_ID || '-' || REPORT_ID || '-' || :I_VENDOR_ID AND REF_INT =:I_SCONTRACTID)
                                                              ) > 0 THEN 'N' ELSE 'Y' END AS IS_UPLOAD_COMPLETE
                                                FROM
                                                  (
                                                    SELECT M_MONTH_REPORT_REQUIRE.DESCRIPTION AS REPORT_MONTH, TMONTHLY_HEADER.REPORT_ID, IS_PASS, TMONTHLY_HEADER.DOC_STATUS_ID, TMONTHLY_HEADER.DESCRIPTION, DOC_STATUS_NAME, TMONTHLY_HEADER.CONFIRM_DATETIME
                                                    , CASE WHEN REPORT_ID = 1 THEN 'REPORT_MONTHLY_1'
                                                           WHEN REPORT_ID = 2 THEN 'REPORT_MONTHLY_3'
                                                           WHEN REPORT_ID = 3 THEN 'REPORT_MONTHLY_6' END AS REPORT_NAME
                                                    FROM TMONTHLY_HEADER INNER JOIN M_DOC_STATUS_MONTHLY ON TMONTHLY_HEADER.DOC_STATUS_ID = M_DOC_STATUS_MONTHLY.DOC_STATUS_ID
                                                                         INNER JOIN M_MONTH_REPORT_REQUIRE ON TMONTHLY_HEADER.REPORT_ID = M_MONTH_REPORT_REQUIRE.REPORT_ID AND TMONTHLY_HEADER.MONTH_ID = M_MONTH_REPORT_REQUIRE.MONTH_ID
                                                    WHERE UPLOAD_YEAR =:I_YEAR 
                                                    AND MONTH_ID =:I_MONTH_ID 
                                                    AND VENDOR_ID =:I_VENDOR_ID 
                                                    AND TMONTHLY_HEADER.SCONTRACTID =:I_SCONTRACTID
                                                  ) TMP";

                cmdCusScoreSelect.Parameters["I_YEAR"].Value = Year;
                cmdCusScoreSelect.Parameters["I_MONTH_ID"].Value = MonthID;
                cmdCusScoreSelect.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdCusScoreSelect.Parameters["I_SCONTRACTID"].Value = SCONTRACTID;

                dt = dbManager.ExecuteDataTable(cmdCusScoreSelect, "cmdCusScoreSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ReportSaveTab4DAL(string Year, int MonthID, string VendorID, int StatusID, DataTable dtScore, int UserID, bool IsValidate, string SCONTRACTID)
        {
            DataTable dt = new DataTable();
            int TransactionID = 0;
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                string ReduceDate = string.Empty;               //dd/MM/yyyy
                if (MonthID == 1)
                    ReduceDate = DateTime.DaysInMonth(int.Parse(Year) - 1, 12).ToString() + '/' + "12" + '/' + (int.Parse(Year) - 1).ToString();
                else
                    ReduceDate = DateTime.DaysInMonth(int.Parse(Year), MonthID - 1).ToString() + '/' + (MonthID - 1).ToString("00") + '/' + Year.ToString();

                for (int i = 0; i < dtScore.Rows.Count; i++)
                {
                    cmdReportSaveTab4.Parameters["I_YEAR"].Value = Year;
                    cmdReportSaveTab4.Parameters["I_MONTHID"].Value = MonthID;
                    cmdReportSaveTab4.Parameters["I_VENDOR_ID"].Value = VendorID;
                    cmdReportSaveTab4.Parameters["I_IS_PASS"].Value = dtScore.Rows[i]["IS_PASS"].ToString();
                    cmdReportSaveTab4.Parameters["I_DESCRIPTION"].Value = dtScore.Rows[i]["DESCRIPTION"].ToString();
                    cmdReportSaveTab4.Parameters["I_DOC_STATUS_ID"].Value = StatusID;
                    cmdReportSaveTab4.Parameters["I_CUS_SCORE"].Value = dtScore.Rows[i]["CUS_SCORE"].ToString();
                    cmdReportSaveTab4.Parameters["I_REPORT_ID"].Value = dtScore.Rows[i]["REPORT_ID"].ToString();
                    cmdReportSaveTab4.Parameters["I_FINAL_BY"].Value = UserID;
                    cmdReportSaveTab4.Parameters["I_SCONTRACTID"].Value = SCONTRACTID;
                    dbManager.ExecuteNonQuery(cmdReportSaveTab4);

                    cmdSelectIDUpdate.Parameters["I_YEAR"].Value = Year;
                    cmdSelectIDUpdate.Parameters["I_MONTHID"].Value = MonthID;
                    cmdSelectIDUpdate.Parameters["I_VENDOR_ID"].Value = VendorID;
                    cmdSelectIDUpdate.Parameters["I_REPORT_ID"].Value = dtScore.Rows[i]["REPORT_ID"].ToString();
                    cmdSelectIDUpdate.Parameters["I_SCONTRACTID"].Value = SCONTRACTID;

                    dt = dbManager.ExecuteDataTable(cmdSelectIDUpdate, "cmdSelectIDUpdate");
                    TransactionID = int.Parse(dt.Rows[0]["TRANSACTION_ID"].ToString());

                    if ((IsValidate) && (decimal.Parse(dtScore.Rows[i]["CUS_SCORE"].ToString()) > 0) && (string.Equals(dtScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString(), "Y")))
                    {
                        cmdAddCusScore.Parameters["I_SREDUCETYPE"].Value = "02";
                        cmdAddCusScore.Parameters["I_SPROCESSID"].Value = "100";
                        cmdAddCusScore.Parameters["I_TTOPIC_TYPE_ID"].Value = dtScore.Rows[i]["REPORT_ID"].ToString() + dtScore.Rows[i]["REPORT_ID"].ToString();
                        cmdAddCusScore.Parameters["I_SREDUCEBY"].Value = UserID;
                        cmdAddCusScore.Parameters["I_CACTIVE"].Value = "1";
                        cmdAddCusScore.Parameters["I_SREFERENCEID"].Value = TransactionID;
                        cmdAddCusScore.Parameters["I_SCONTRACTID"].Value = SCONTRACTID;
                        cmdAddCusScore.Parameters["I_DREDUCE"].Value = ReduceDate;

                        dbManager.ExecuteNonQuery(cmdAddCusScore);

                        //cmdSelectContractByVendor.Parameters["I_SVENDORID"].Value = VendorID;
                        //DataTable dtContract = new DataTable();
                        //dtContract = dbManager.ExecuteDataTable(cmdSelectContractByVendor, "cmdSelectContractByVendor");

                        //for (int j = 0; j < dtContract.Rows.Count; j++)
                        //{
                        //    cmdAddCusScore.Parameters["I_SREDUCETYPE"].Value = "02";
                        //    cmdAddCusScore.Parameters["I_SPROCESSID"].Value = "100";
                        //    cmdAddCusScore.Parameters["I_TTOPIC_TYPE_ID"].Value = dtScore.Rows[i]["REPORT_ID"].ToString() + dtScore.Rows[i]["REPORT_ID"].ToString();
                        //    cmdAddCusScore.Parameters["I_SREDUCEBY"].Value = UserID;
                        //    cmdAddCusScore.Parameters["I_CACTIVE"].Value = "1";
                        //    cmdAddCusScore.Parameters["I_SREFERENCEID"].Value = TransactionID;
                        //    cmdAddCusScore.Parameters["I_SCONTRACTID"].Value = dtContract.Rows[j]["SCONTRACTID"].ToString();

                        //    dbManager.ExecuteNonQuery(cmdAddCusScore);
                        //}
                    }
                }
                
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ReportCheckSelectDAL(string Year, int MonthID, string Condition)
        {
            string Query = cmdReportCheckSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdReportCheckSelect.CommandText += Condition;

                cmdReportCheckSelect.Parameters["I_YEAR"].Value = Year;
                cmdReportCheckSelect.Parameters["I_MONTHID"].Value = MonthID;

                return dbManager.ExecuteDataTable(cmdReportCheckSelect, "cmdReportCheckSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdReportCheckSelect.CommandText = Query;
            }
        }

        public DataTable ReportCheckSelectTab6DAL(string Year, string Condition)
        {
            try
            {
                dbManager.Open();
                cmdReportCheckSelectTab6.CommandText = @"SELECT DISTINCT M_MONTH.MONTH_ID, NAME_TH AS SEND_MONTH, M_MONTH.DATA_MONTH, TVENDOR.SVENDORID, SABBREVIATION
                                                          , '' AS REPORT_1, '' AS REPORT_1_DATE, '' AS REPORT_2, '' AS REPORT_2_DATE, '' AS REPORT_3, '' AS REPORT_3_DATE
                                                          , TMONTHLY_HEADER.SCONTRACTID, TCONTRACT.SCONTRACTNO
                                                        FROM TMONTHLY_HEADER INNER JOIN M_MONTH ON TMONTHLY_HEADER.MONTH_ID = M_MONTH.MONTH_ID
                                                                             INNER JOIN M_MONTH_REPORT_REQUIRE ON M_MONTH.MONTH_ID = M_MONTH_REPORT_REQUIRE.MONTH_ID AND M_MONTH_REPORT_REQUIRE.REPORT_ID = TMONTHLY_HEADER.REPORT_ID
                                                                             INNER JOIN TVENDOR ON TMONTHLY_HEADER.VENDOR_ID = TVENDOR.SVENDORID
                                                                             INNER JOIN TCONTRACT ON TCONTRACT.SCONTRACTID = TMONTHLY_HEADER.SCONTRACTID
                                                        WHERE TMONTHLY_HEADER.UPLOAD_YEAR =:I_YEAR";

                cmdReportCheckSelectTab6.CommandText += Condition;
                cmdReportCheckSelectTab6.CommandText += " ORDER BY M_MONTH.MONTH_ID, TVENDOR.SVENDORID, TCONTRACT.SCONTRACTNO";

                cmdReportCheckSelectTab6.Parameters["I_YEAR"].Value = Year;

                return dbManager.ExecuteDataTable(cmdReportCheckSelectTab6, "cmdReportCheckSelectTab6");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ReportCheckSelectDetailDAL(string Year, int MonthID, string Condition)
        {
            string Query = cmdReportCheckSelectDetail.CommandText;
            try
            {
                dbManager.Open();
                cmdReportCheckSelectDetail.CommandText += Condition;

                cmdReportCheckSelectDetail.Parameters["I_YEAR"].Value = Year;
                cmdReportCheckSelectDetail.Parameters["I_MONTHID"].Value = MonthID;

                return dbManager.ExecuteDataTable(cmdReportCheckSelectDetail, "cmdReportCheckSelectDetail");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdReportCheckSelectDetail.CommandText = Query;
            }
        }

        public DataTable ReportCheckSelectDetailTab6DAL(string Year, int MonthID, string Condition, string RequestType, string RefString, int RefInt)
        {
            string Query = cmdReportCheckSelectDetailTab6.CommandText;
            try
            {
                dbManager.Open();
                cmdReportCheckSelectDetailTab6.CommandText = @"SELECT IS_UPLOAD_COMPLETE, DOC_STATUS_ID, TO_CHAR(CONFIRM_DATETIME, 'DD/MM/YYYY') AS DATE_CONFIRM, IS_PASS 
                                                              , CASE WHEN (
                                                                            SELECT COUNT(*) FROM M_COMPLAIN_REQUEST_FILE WHERE REQUEST_TYPE =:I_REQUEST_TYPE AND UPLOAD_ID NOT IN (SELECT UPLOAD_ID FROM F_UPLOAD WHERE REF_STR =:I_REF_STR AND REF_INT =:I_REF_INT)
                                                                        ) > 0 THEN 'N' ELSE 'Y' END AS IS_DOC_FINAL
                                                              FROM TMONTHLY_HEADER WHERE TMONTHLY_HEADER.UPLOAD_YEAR =:I_YEAR AND TMONTHLY_HEADER.MONTH_ID =:I_MONTHID";

                cmdReportCheckSelectDetailTab6.CommandText += Condition;

                cmdReportCheckSelectDetailTab6.Parameters["I_YEAR"].Value = Year;
                cmdReportCheckSelectDetailTab6.Parameters["I_MONTHID"].Value = MonthID;
                cmdReportCheckSelectDetailTab6.Parameters["I_REQUEST_TYPE"].Value = RequestType;
                cmdReportCheckSelectDetailTab6.Parameters["I_REF_STR"].Value = RefString;
                cmdReportCheckSelectDetailTab6.Parameters["I_REF_INT"].Value = RefInt;

                return dbManager.ExecuteDataTable(cmdReportCheckSelectDetailTab6, "cmdReportCheckSelectDetailTab6");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdReportCheckSelectDetailTab6.CommandText = Query;
            }
        }

        public DataTable GetDateNowDAL()
        {
            try
            {
                dbManager.Open();
                return dbManager.ExecuteDataTable(cmdGetDateNow, "cmdGetDateNow");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataSet GetEmailReportMonthlyNotPassDAL(string DeliveryDepartmentCode, string VendorID)
        {
            try
            {
                dbManager.Open();
                DataSet dsEmail = new DataSet();
                DataTable dtEmailComplainAdmin = new DataTable();           //รข
                DataTable dtEmailVendor = new DataTable();                  //ผู้ขนส่ง

                //รข
                cmdGetEmailComplainAdmin.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCode;
                dtEmailComplainAdmin = dbManager.ExecuteDataTable(cmdGetEmailComplainAdmin, "cmdGetEmailComplainAdmin");

                //Vendor
                cmdGetEmailVendor.Parameters["I_SVENDORID"].Value = VendorID;
                dtEmailVendor = dbManager.ExecuteDataTable(cmdGetEmailVendor, "cmdGetEmailVendor");

                dsEmail.Tables.Add(dtEmailComplainAdmin.Copy());
                dsEmail.Tables.Add(dtEmailVendor.Copy());

                return dsEmail;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ReportSelectConfigDAL()
        {
            try
            {
                dbManager.Open();
                cmdReportSelectConfig.CommandText = @"SELECT START_UPLOAD_DAY, END_UPLOAD_DAY
                                                      FROM M_MONTH_REPORT_REQUIRE
                                                      WHERE ROWNUM = 1
                                                      AND IS_ACTIVE = 1";

                return dbManager.ExecuteDataTable(cmdReportSelectConfig, "cmdReportSelectConfig");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable TruckSelectSpecialValueDAL()
        {
            try
            {
                cmdSelectSpecialValue.CommandText = @"SELECT TO_CHAR(TO_DATE(M_UPLOAD_TYPE.SPECIAL_FLAG_VALUE || '/'|| TO_CHAR(sysdate, 'YYYY'),'dd/mm/yyyy'), 'dd MONTH yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') AS SPECIALDATE
                                                    FROM M_UPLOAD_TYPE WHERE UPLOAD_TYPE = 'TRUCK' AND SPECIAL_FLAG = 1";

                DataTable dtSpecialValue = dbManager.ExecuteDataTable(cmdSelectSpecialValue, "cmdSelectSpecialValue");

                return dtSpecialValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static MonthlyReportDAL _instance;
        public static MonthlyReportDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new MonthlyReportDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}