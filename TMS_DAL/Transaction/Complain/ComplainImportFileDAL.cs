﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.IO;
using System.Globalization;
using System.Collections.Generic;

namespace TMS_DAL.Transaction.Complain
{
    public partial class ComplainImportFileDAL : OracleConnectionDAL
    {
        public ComplainImportFileDAL()
        {
            InitializeComponent();
        }

        public ComplainImportFileDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void ImportFileInsertDAL(int SSERVICEID, DataTable dtFile, string UserID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                for (int i = 0; i < dtFile.Rows.Count; i++)
                {
                    cmdImportFileInsert.Parameters["NID"].Value = i + 1;
                    cmdImportFileInsert.Parameters["SSERVICEID"].Value = SSERVICEID;
                    cmdImportFileInsert.Parameters["SFILENAME"].Value = dtFile.Rows[i]["FILENAME_USER"].ToString();
                    cmdImportFileInsert.Parameters["SGENFILENAME"].Value = dtFile.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdImportFileInsert.Parameters["SFILEPATH"].Value = dtFile.Rows[i]["FULLPATH"].ToString();
                    cmdImportFileInsert.Parameters["UPLOAD_ID"].Value = int.Parse(dtFile.Rows[i]["UPLOAD_ID"].ToString());
                    cmdImportFileInsert.Parameters["SCREATE"].Value = UserID;

                    dbManager.ExecuteNonQuery(cmdImportFileInsert);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ImportFileSelectDAL(string DocID, string UploadType)
        {
            try
            {
                dbManager.Open();                
                cmdImportFileSelect.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileSelect.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                return dbManager.ExecuteDataTable(cmdImportFileSelect, "cmdImportFileSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ImportFileSelectDAL2(string DocID, string UploadType, string UploadTypeIND)
        {
            try
            {
                dbManager.Open();
                cmdImportFileSelect2.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileSelect2.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdImportFileSelect2.Parameters["I_UPLOAD_TYPE_IND"].Value = UploadTypeIND;

                return dbManager.ExecuteDataTable(cmdImportFileSelect2, "cmdImportFileSelect2");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ImportFileSelectDAL3(string DocID, string UploadType, string UploadTypeIND)
        {
            try
            {
                dbManager.Open();
                cmdImportFileSelect3.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileSelect3.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdImportFileSelect3.Parameters["I_UPLOAD_TYPE_IND"].Value = UploadTypeIND;

                return dbManager.ExecuteDataTable(cmdImportFileSelect3, "cmdImportFileSelect3");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ImportFileSelectDAL(string DocID, string UploadType, string Condition)
        {
            string Query = cmdImportFileSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdImportFileSelect.CommandText += Condition;

                cmdImportFileSelect.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileSelect.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                return dbManager.ExecuteDataTable(cmdImportFileSelect, "cmdImportFileSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdImportFileSelect.CommandText = Query;
            }
        }

        public void ComplainAttachInsertDAL(DataTable dtUpload, string DocID, int CreateBy, string SSOLUTION, string SPROTECT, string SRESPONSIBLE, int DocStatusID, string CCEmail, string UploadType)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdImportFileDelete.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileDelete.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                dbManager.ExecuteNonQuery(cmdImportFileDelete);

                cmdComplainUpdate.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainUpdate.Parameters["SSOLUTION"].Value = SSOLUTION;
                cmdComplainUpdate.Parameters["SPROTECT"].Value = SPROTECT;
                cmdComplainUpdate.Parameters["SRESPONSIBLE"].Value = SRESPONSIBLE;
                cmdComplainUpdate.Parameters["DOC_STATUS_ID"].Value = DocStatusID;
                cmdComplainUpdate.Parameters["I_CC_EMAIL"].Value = CCEmail;
                dbManager.ExecuteNonQuery(cmdComplainUpdate);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdComplainAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdComplainAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdComplainAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdComplainAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdComplainAttachInsert.Parameters["REF_INT"].Value = 0;
                    cmdComplainAttachInsert.Parameters["REF_STR"].Value = DocID;
                    cmdComplainAttachInsert.Parameters["ISACTIVE"].Value = "1";
                    cmdComplainAttachInsert.Parameters["CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdComplainAttachInsert);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainTab3SaveDAL(DataTable dtUpload, string DocID, int CreateBy, string UploadType, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataTable dtScoreList, int DocStatusID, int CostCheck, DateTime? CostCheckDate, DataTable dtScoreListCar, bool isValiDate, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, int Blacklist, string Sentencer, Dictionary<int, string> flagVendorDownload = null)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdImportFileDelete.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileDelete.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                dbManager.ExecuteNonQuery(cmdImportFileDelete);

                cmdScoreListCarDelete.Parameters["I_DOC_ID"].Value = DocID;
                dbManager.ExecuteNonQuery(cmdScoreListCarDelete);

                for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
                {
                    cmdScoreListCarInsert.Parameters["DOC_ID"].Value = DocID;
                    cmdScoreListCarInsert.Parameters["STOPICID"].Value = dtScoreListCar.Rows[i]["STOPICID"].ToString();

                    if (string.Equals(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), string.Empty))
                        cmdScoreListCarInsert.Parameters["I_DELIVERY_DATE"].Value = DBNull.Value;
                    else
                        cmdScoreListCarInsert.Parameters["I_DELIVERY_DATE"].Value = DateTime.ParseExact(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), "dd/MM/yyyy", null);

                    cmdScoreListCarInsert.Parameters["STRUCKID"].Value = dtScoreListCar.Rows[i]["TRUCKID"].ToString();
                    cmdScoreListCarInsert.Parameters["CARHEAD"].Value = dtScoreListCar.Rows[i]["CARHEAD"].ToString();
                    cmdScoreListCarInsert.Parameters["CARDETAIL"].Value = dtScoreListCar.Rows[i]["CARDETAIL"].ToString();
                    cmdScoreListCarInsert.Parameters["I_TOTAL_CAR"].Value = dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString();
                    cmdScoreListCarInsert.Parameters["CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdScoreListCarInsert);
                }

                DataSet ds = new DataSet("ds");
                dtScoreList.TableName = "dt";
                ds.Tables.Add(dtScoreList);

                cmdComplainUpdateTab3.CommandType = CommandType.StoredProcedure;
                cmdComplainUpdateTab3.CommandText = "USP_T_COMPLAIN_INSERT_SCORE";
                cmdComplainUpdateTab3.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainUpdateTab3.Parameters["I_CUSSCORE_TYPE_ID"].Value = CusScoreTypeID;
                cmdComplainUpdateTab3.Parameters["I_TOTAL_POINT"].Value = TotalPoint;
                cmdComplainUpdateTab3.Parameters["I_TOTAL_COST"].Value = TotalCost;
                cmdComplainUpdateTab3.Parameters["I_TOTAL_DISABLE_DRIVER"].Value = TotalDisableDriver;
                cmdComplainUpdateTab3.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdComplainUpdateTab3.Parameters["I_DOC_STATUS_ID"].Value = DocStatusID;
                cmdComplainUpdateTab3.Parameters["I_XML"].Value = ds.GetXml();
                cmdComplainUpdateTab3.Parameters["I_COST_CHECK"].Value = CostCheck;
                if (CostCheckDate == null)
                    cmdComplainUpdateTab3.Parameters["I_COST_CHECK_DATE"].Value = DBNull.Value;
                else
                    cmdComplainUpdateTab3.Parameters["I_COST_CHECK_DATE"].Value = CostCheckDate;
                cmdComplainUpdateTab3.Parameters["I_COST_OTHER"].Value = CostOther;
                cmdComplainUpdateTab3.Parameters["I_OIL_LOSE"].Value = OilLose;
                cmdComplainUpdateTab3.Parameters["I_ISSAVE_CUSSCORE"].Value = isSaveCusScore;
                cmdComplainUpdateTab3.Parameters["I_REMARK_TAB3"].Value = RemarkTab3;
                cmdComplainUpdateTab3.Parameters["I_BLACKLIST"].Value = Blacklist;
                cmdComplainUpdateTab3.Parameters["I_SSENTENCERCODE"].Value = Sentencer;

                dbManager.ExecuteNonQuery(cmdComplainUpdateTab3);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdComplainAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdComplainAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdComplainAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdComplainAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdComplainAttachInsert.Parameters["REF_INT"].Value = 0;
                    cmdComplainAttachInsert.Parameters["REF_STR"].Value = DocID;
                    cmdComplainAttachInsert.Parameters["ISACTIVE"].Value = "1";
                    cmdComplainAttachInsert.Parameters["CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdComplainAttachInsert);
                }

                if (isValiDate)
                {
                    cmdComplainComfirm.Parameters["I_DOC_ID"].Value = DocID;
                    dbManager.ExecuteNonQuery(cmdComplainComfirm);
                }

                if (flagVendorDownload != null)
                {
                    if (flagVendorDownload.Count > 0)
                    {
                        string _sql = String.Format(@"Update F_UPLOAD Set IS_VENDOR_VIEW =:FLAG 
                                                    Where ID=:FILE_ID");
                        cmdComplainUpdateTab3.CommandText = _sql;
                        cmdComplainUpdateTab3.CommandType = CommandType.Text;
                        cmdComplainUpdateTab3.Parameters.Clear();
                        cmdComplainUpdateTab3.Parameters.Add(":FLAG", System.Data.OracleClient.OracleType.Char);
                        cmdComplainUpdateTab3.Parameters.Add(":FILE_ID", System.Data.OracleClient.OracleType.Number);
                        foreach (KeyValuePair<int, string> entry in flagVendorDownload)
                        {
                            cmdComplainUpdateTab3.Parameters[":FLAG"].Value = entry.Value;
                            cmdComplainUpdateTab3.Parameters[":FILE_ID"].Value = entry.Key;
                            dbManager.ExecuteNonQuery(cmdComplainUpdateTab3);
                        }

                    }
                }
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainTab3AppealSaveDAL(string DocID, int CreateBy, int CusScoreTypeID, decimal TotalPoint, decimal TotalCost, int TotalDisableDriver, DataTable dtScoreList, DataTable dtScoreListCar, decimal CostOther, int isSaveCusScore, string RemarkTab3, decimal OilLose, string AppealID, int Blacklist)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdScoreListCarDelete_A.Parameters["I_DOC_ID"].Value = DocID;
                dbManager.ExecuteNonQuery(cmdScoreListCarDelete_A);

                for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
                {
                    cmdScoreListCarInsert_A.Parameters["DOC_ID"].Value = DocID;
                    cmdScoreListCarInsert_A.Parameters["STOPICID"].Value = dtScoreListCar.Rows[i]["STOPICID"].ToString();

                    if (string.Equals(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), string.Empty))
                        cmdScoreListCarInsert_A.Parameters["I_DELIVERY_DATE"].Value = DBNull.Value;
                    else
                        cmdScoreListCarInsert_A.Parameters["I_DELIVERY_DATE"].Value = dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString();
                    //cmdScoreListCarInsert_A.Parameters["I_DELIVERY_DATE"].Value = DateTime.ParseExact(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), "dd/MM/yyyy", new CultureInfo("en-US")).ToString("dd/MM/yyyy");

                    cmdScoreListCarInsert_A.Parameters["STRUCKID"].Value = dtScoreListCar.Rows[i]["TRUCKID"].ToString();
                    cmdScoreListCarInsert_A.Parameters["CARHEAD"].Value = dtScoreListCar.Rows[i]["CARHEAD"].ToString();
                    cmdScoreListCarInsert_A.Parameters["CARDETAIL"].Value = dtScoreListCar.Rows[i]["CARDETAIL"].ToString();
                    cmdScoreListCarInsert_A.Parameters["I_TOTAL_CAR"].Value = dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString();
                    cmdScoreListCarInsert_A.Parameters["CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdScoreListCarInsert_A);
                }

                DataSet ds = new DataSet("ds");
                dtScoreList.TableName = "dt";
                ds.Tables.Add(dtScoreList);

                cmdComplainUpdateTab3_A.CommandType = CommandType.StoredProcedure;
                cmdComplainUpdateTab3_A.CommandText = "USP_T_COMPLAIN_INSERT_SCORE_A";
                cmdComplainUpdateTab3_A.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainUpdateTab3_A.Parameters["I_CUSSCORE_TYPE_ID"].Value = CusScoreTypeID;
                cmdComplainUpdateTab3_A.Parameters["I_TOTAL_POINT"].Value = TotalPoint;
                cmdComplainUpdateTab3_A.Parameters["I_TOTAL_COST"].Value = TotalCost;
                cmdComplainUpdateTab3_A.Parameters["I_TOTAL_DISABLE_DRIVER"].Value = TotalDisableDriver;
                cmdComplainUpdateTab3_A.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdComplainUpdateTab3_A.Parameters["I_XML"].Value = ds.GetXml();
                cmdComplainUpdateTab3_A.Parameters["I_COST_OTHER"].Value = CostOther;
                cmdComplainUpdateTab3_A.Parameters["I_OIL_LOSE"].Value = OilLose;
                cmdComplainUpdateTab3_A.Parameters["I_ISSAVE_CUSSCORE"].Value = isSaveCusScore;
                cmdComplainUpdateTab3_A.Parameters["I_REMARK_TAB3"].Value = RemarkTab3;
                cmdComplainUpdateTab3_A.Parameters["I_DELIVERY_DATE"].Value = dtScoreListCar.Rows[0]["DELIVERY_DATE"].ToString();
                cmdComplainUpdateTab3_A.Parameters["I_STRUCKID"].Value = dtScoreListCar.Rows[0]["TRUCKID"].ToString();
                cmdComplainUpdateTab3_A.Parameters["I_SAPPEALID"].Value = AppealID;
                cmdComplainUpdateTab3_A.Parameters["I_BLACKLIST"].Value = Blacklist;

                dbManager.ExecuteNonQuery(cmdComplainUpdateTab3_A);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3Score.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3Score, "cmdComplainSelectTab3Score");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreAppealDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3Score_A.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3Score_A, "cmdComplainSelectTab3Score_A");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreDetailDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3ScoreDetail.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3ScoreDetail, "cmdComplainSelectTab3ScoreDetail");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreDetailPartialDAL(string DocID, string StruckID, string DeliveryDate)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3ScoreDetailPartial.CommandText = @"SELECT TCOMPLAIN_SCORE_DETAIL.STOPICID, STOPICNAME,SUSPEND_DRIVER,DECODE (TTOPIC.BLACKLIST, 1, 'BL', '') AS Blacklist, COST AS Cost
                                                                        , CASE WHEN (SELECT NVL(STRUCKID, '0') FROM TCOMPLAIN_SCORE_CAR WHERE TCOMPLAIN_SCORE_CAR.DOC_ID =:I_DOC_ID AND ROWNUM = 1) <> '0'
                                                                                                  THEN COST || ' x ' || '1' || ' = ' || COST * 1
                                                                                                  ELSE COST / TOTAL_CAR || ' x ' || TOTAL_CAR || ' = ' || COST / TOTAL_CAR * TOTAL_CAR END AS COSTDISPLAY
                                                                        , DISABLE_DRIVER AS DISABLEDRIVER
                                                                        , CASE WHEN (SELECT NVL(STRUCKID, '0') FROM TCOMPLAIN_SCORE_CAR WHERE TCOMPLAIN_SCORE_CAR.DOC_ID =:I_DOC_ID AND ROWNUM = 1) <> '0'
                                                                                                  THEN DISABLE_DRIVER || ' x ' || '1' || ' = ' || DISABLE_DRIVER * 1
                                                                                                  ELSE DISABLE_DRIVER / TOTAL_CAR || ' x ' || TOTAL_CAR || ' = ' || DISABLE_DRIVER / TOTAL_CAR * TOTAL_CAR END AS DISABLEDRIVERDISPLAY
                                                                        , SCORE1 AS Score1, SCORE2 AS Score2, SCORE3 AS Score3, SCORE4 AS Score4, SCORE5 AS Score5
                                                                        , CASE WHEN (SELECT NVL(STRUCKID, '0') FROM TCOMPLAIN_SCORE_CAR WHERE TCOMPLAIN_SCORE_CAR.DOC_ID =:I_DOC_ID AND ROWNUM = 1) <> '0'
                                                                                                  THEN POINT / TOTAL_CAR
                                                                                                  ELSE POINT END AS Score6
                                                                        , CASE WHEN (SELECT NVL(STRUCKID, '0') FROM TCOMPLAIN_SCORE_CAR WHERE TCOMPLAIN_SCORE_CAR.DOC_ID =:I_DOC_ID AND ROWNUM = 1) <> '0'
                                                                                                  THEN POINT / TOTAL_CAR || ' x ' || '1' || ' = ' || POINT / TOTAL_CAR * 1
                                                                                                  ELSE POINT / TOTAL_CAR || ' x ' || TOTAL_CAR || ' = ' || POINT END AS SCORE6DISPLAY
                                                                        , TOTAL_CAR AS TotalCar
                                                                FROM TCOMPLAIN_SCORE_DETAIL INNER JOIN TTOPIC ON TCOMPLAIN_SCORE_DETAIL.STOPICID = TTOPIC.STOPICID
                                                                WHERE TCOMPLAIN_SCORE_DETAIL.DOC_ID =:I_DOC_ID
                                                                AND TCOMPLAIN_SCORE_DETAIL.ISACTIVE = '1'
                                                                AND TTOPIC.CACTIVE = '1'
                                                                AND TCOMPLAIN_SCORE_DETAIL.STOPICID IN (SELECT TCOMPLAIN_SCORE_CAR.STOPICID FROM TCOMPLAIN_SCORE_CAR WHERE DOC_ID =:I_DOC_ID AND NVL(STRUCKID, '0') =:I_STRUCKID AND TO_CHAR(DELIVERY_DATE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') =:I_DELIVERY_DATE)";

                cmdComplainSelectTab3ScoreDetailPartial.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainSelectTab3ScoreDetailPartial.Parameters["I_STRUCKID"].Value = string.Equals(StruckID, string.Empty) ? "0" : StruckID;
                cmdComplainSelectTab3ScoreDetailPartial.Parameters["I_DELIVERY_DATE"].Value = DeliveryDate;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3ScoreDetailPartial, "cmdComplainSelectTab3ScoreDetailPartial");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreCarDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3ScoreCar.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3ScoreCar, "cmdComplainSelectTab3ScoreCar");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreCarPartialDAL(string DocID, string StruckID, string DeliveryDate)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3ScoreCarPartial.CommandText = @"SELECT STOPICID, STRUCKID AS TRUCKID, CARHEAD, CARDETAIL, TO_CHAR(DELIVERY_DATE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DELIVERY_DATE, TO_CHAR(TOTAL_CAR) AS TOTAL_CAR
                                                                     FROM TCOMPLAIN_SCORE_CAR
                                                                     WHERE TCOMPLAIN_SCORE_CAR.DOC_ID =:I_DOC_ID
                                                                     AND STRUCKID =:I_STRUCKID
                                                                     AND TO_CHAR(DELIVERY_DATE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') =:I_DELIVERY_DATE";

                cmdComplainSelectTab3ScoreCarPartial.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainSelectTab3ScoreCarPartial.Parameters["I_STRUCKID"].Value = StruckID;
                cmdComplainSelectTab3ScoreCarPartial.Parameters["I_DELIVERY_DATE"].Value = DeliveryDate;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3ScoreCarPartial, "cmdComplainSelectTab3ScoreCarPartial");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreDetailAppealDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3ScoreDetail_A.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3ScoreDetail_A, "cmdComplainSelectTab3ScoreDetail_A");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectTab3ScoreCarAppealDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainSelectTab3ScoreCar_A.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainSelectTab3ScoreCar_A, "cmdComplainSelectTab3ScoreCar_A");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainUpdateCostDAL(string DocID, int CostCheck, DateTime? CostCheckDate, int DocStatusID, DataTable dtUpload, int CreateBy, string UploadType)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUpdateCost.Parameters["I_DOC_ID"].Value = DocID;
                cmdUpdateCost.Parameters["I_COST_CHECK"].Value = CostCheck;
                if (CostCheckDate == null)
                    cmdUpdateCost.Parameters["I_COST_CHECK_DATE"].Value = DBNull.Value;
                else
                    cmdUpdateCost.Parameters["I_COST_CHECK_DATE"].Value = CostCheckDate;

                dbManager.ExecuteNonQuery(cmdUpdateCost);

                cmdUpdateCostStatus.Parameters["I_DOC_ID"].Value = DocID;
                cmdUpdateCostStatus.Parameters["I_DOC_STATUS_ID"].Value = DocStatusID;
                dbManager.ExecuteNonQuery(cmdUpdateCostStatus);

                cmdImportFileDelete.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileDelete.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                dbManager.ExecuteNonQuery(cmdImportFileDelete);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdComplainAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdComplainAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdComplainAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdComplainAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdComplainAttachInsert.Parameters["REF_INT"].Value = 0;
                    cmdComplainAttachInsert.Parameters["REF_STR"].Value = DocID;
                    cmdComplainAttachInsert.Parameters["ISACTIVE"].Value = "1";
                    cmdComplainAttachInsert.Parameters["CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdComplainAttachInsert);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static ComplainImportFileDAL _instance;
        public static ComplainImportFileDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ComplainImportFileDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}