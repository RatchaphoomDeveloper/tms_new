﻿namespace TMS_DAL.Transaction.Complain
{
    partial class ComplainImportFileDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComplainImportFileDAL));
            this.cmdImportFileDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileSelect2 = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainAttachInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainUpdateTab3 = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3Score = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3ScoreDetail = new System.Data.OracleClient.OracleCommand();
            this.cmdScoreListCarDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdScoreListCarInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3ScoreCar = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainComfirm = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateCost = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateCostStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainUpdateTab3_A = new System.Data.OracleClient.OracleCommand();
            this.cmdScoreListCarInsert_A = new System.Data.OracleClient.OracleCommand();
            this.cmdScoreListCarDelete_A = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3ScoreDetail_A = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3ScoreCar_A = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3Score_A = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3ScoreDetailPartial = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectTab3ScoreCarPartial = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileSelect3 = new System.Data.OracleClient.OracleCommand();
            // 
            // OracleConn
            // 
            this.OracleConn.ConnectionString = "Data Source=172.17.15.39:1526/tstaging;Persist Security Info=True;User ID=TMSIVUA" +
    "T;Password=Tmsivuat123;Unicode=True";
            // 
            // cmdImportFileDelete
            // 
            this.cmdImportFileDelete.CommandText = "DELETE FROM F_UPLOAD WHERE REF_STR =: I_DOC_ID AND F_UPLOAD.UPLOAD_ID IN (SELECT " +
    "UPLOAD_ID FROM M_UPLOAD_TYPE WHERE UPLOAD_TYPE =: I_UPLOAD_TYPE)";
            this.cmdImportFileDelete.Connection = this.OracleConn;
            this.cmdImportFileDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, "255")});
            // 
            // cmdImportFileInsert
            // 
            this.cmdImportFileInsert.CommandText = " INSERT INTO TCOMPLAINTIMPORTFILE(NID,SSERVICEID,UPLOAD_ID,SFILENAME,SGENFILENAME" +
    ",SFILEPATH,SCREATE,DCREATE)  VALUES (:NID,:SSERVICEID,:UPLOAD_ID,:SFILENAME,:SGE" +
    "NF ILENAME,:SFILEPATH,:SCREATE,SYSDATE)";
            this.cmdImportFileInsert.Connection = this.OracleConn;
            this.cmdImportFileInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("NID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("SSERVICEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("SFILENAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("SGENFILENAME", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("SFILEPATH", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("SCREATE", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdImportFileSelect
            // 
            this.cmdImportFileSelect.CommandText = resources.GetString("cmdImportFileSelect.CommandText");
            this.cmdImportFileSelect.Connection = this.OracleConn;
            this.cmdImportFileSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdImportFileSelect2
            // 
            this.cmdImportFileSelect2.CommandText = resources.GetString("cmdImportFileSelect2.CommandText");
            this.cmdImportFileSelect2.Connection = this.OracleConn;
            this.cmdImportFileSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Char, 1)});
            // 
            // cmdComplainAttachInsert
            // 
            this.cmdComplainAttachInsert.CommandText = resources.GetString("cmdComplainAttachInsert.CommandText");
            this.cmdComplainAttachInsert.Connection = this.OracleConn;
            this.cmdComplainAttachInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500)});
            // 
            // cmdComplainUpdate
            // 
            this.cmdComplainUpdate.CommandText = "UPDATE TCOMPLAIN SET SSOLUTION =: SSOLUTION, SPROTECT =:SPROTECT, SRESPONSIBLE =:" +
    "SRESPONSIBLE, DOC_STATUS_ID =:DOC_STATUS_ID, CC_EMAIL =: I_CC_EMAIL WHERE DOC_ID" +
    " =: I_DOC_ID";
            this.cmdComplainUpdate.Connection = this.OracleConn;
            this.cmdComplainUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("SSOLUTION", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("SPROTECT", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("SRESPONSIBLE", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CC_EMAIL", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdComplainUpdateTab3
            // 
            this.cmdComplainUpdateTab3.CommandText = "USP_T_COMPLAIN_INSERT_SCORE ";
            this.cmdComplainUpdateTab3.Connection = this.OracleConn;
            this.cmdComplainUpdateTab3.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_CUSSCORE_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_POINT", System.Data.OracleClient.OracleType.Double),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_COST", System.Data.OracleClient.OracleType.Double),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_DISABLE_DRIVER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_XML", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_COST_CHECK", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_COST_CHECK_DATE", System.Data.OracleClient.OracleType.DateTime, 255),
            new System.Data.OracleClient.OracleParameter("I_COST_OTHER", System.Data.OracleClient.OracleType.Float),
            new System.Data.OracleClient.OracleParameter("I_ISSAVE_CUSSCORE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REMARK_TAB3", System.Data.OracleClient.OracleType.VarChar, 2000),
            new System.Data.OracleClient.OracleParameter("I_OIL_LOSE", System.Data.OracleClient.OracleType.Float),
            new System.Data.OracleClient.OracleParameter("I_BLACKLIST", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SSENTENCERCODE", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdComplainSelectTab3Score
            // 
            this.cmdComplainSelectTab3Score.CommandText = resources.GetString("cmdComplainSelectTab3Score.CommandText");
            this.cmdComplainSelectTab3Score.Connection = this.OracleConn;
            this.cmdComplainSelectTab3Score.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdComplainSelectTab3ScoreDetail
            // 
            this.cmdComplainSelectTab3ScoreDetail.CommandText = resources.GetString("cmdComplainSelectTab3ScoreDetail.CommandText");
            this.cmdComplainSelectTab3ScoreDetail.Connection = this.OracleConn;
            this.cmdComplainSelectTab3ScoreDetail.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdScoreListCarDelete
            // 
            this.cmdScoreListCarDelete.CommandText = "DELETE FROM TCOMPLAIN_SCORE_CAR WHERE DOC_ID=: I_DOC_ID";
            this.cmdScoreListCarDelete.Connection = this.OracleConn;
            this.cmdScoreListCarDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdScoreListCarInsert
            // 
            this.cmdScoreListCarInsert.CommandText = resources.GetString("cmdScoreListCarInsert.CommandText");
            this.cmdScoreListCarInsert.Connection = this.OracleConn;
            this.cmdScoreListCarInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("DOC_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("STOPICID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("STRUCKID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("CARHEAD", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("CARDETAIL", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DELIVERY_DATE", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_CAR", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdComplainSelectTab3ScoreCar
            // 
            this.cmdComplainSelectTab3ScoreCar.CommandText = resources.GetString("cmdComplainSelectTab3ScoreCar.CommandText");
            this.cmdComplainSelectTab3ScoreCar.Connection = this.OracleConn;
            this.cmdComplainSelectTab3ScoreCar.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdComplainComfirm
            // 
            this.cmdComplainComfirm.CommandText = "UPDATE TCOMPLAIN SET COMPLAIN_DATE = SYSDATE, SECOUND_DATE = SYSDATE + 7 WHERE DO" +
    "C_ID =: I_DOC_ID";
            this.cmdComplainComfirm.Connection = this.OracleConn;
            this.cmdComplainComfirm.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdUpdateCost
            // 
            this.cmdUpdateCost.CommandText = "UPDATE TCOMPLAIN_SCORE SET COST_CHECK =: I_COST_CHECK, COST_CHECK_DATE =: I_COST_" +
    "CHECK_DATE WHERE DOC_ID =: I_DOC_ID AND ISACTIVE = 1";
            this.cmdUpdateCost.Connection = this.OracleConn;
            this.cmdUpdateCost.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_COST_CHECK", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_COST_CHECK_DATE", System.Data.OracleClient.OracleType.DateTime, 255)});
            // 
            // cmdUpdateCostStatus
            // 
            this.cmdUpdateCostStatus.CommandText = "UPDATE TCOMPLAIN SET DOC_STATUS_ID = :I_DOC_STATUS_ID WHERE DOC_ID = :I_DOC_ID";
            this.cmdUpdateCostStatus.Connection = this.OracleConn;
            this.cmdUpdateCostStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdComplainUpdateTab3_A
            // 
            this.cmdComplainUpdateTab3_A.CommandText = "USP_T_COMPLAIN_INSERT_SCORE ";
            this.cmdComplainUpdateTab3_A.Connection = this.OracleConn;
            this.cmdComplainUpdateTab3_A.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_CUSSCORE_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_POINT", System.Data.OracleClient.OracleType.Double),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_COST", System.Data.OracleClient.OracleType.Double),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_DISABLE_DRIVER", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_XML", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_COST_OTHER", System.Data.OracleClient.OracleType.Float),
            new System.Data.OracleClient.OracleParameter("I_ISSAVE_CUSSCORE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REMARK_TAB3", System.Data.OracleClient.OracleType.VarChar, 2000),
            new System.Data.OracleClient.OracleParameter("I_OIL_LOSE", System.Data.OracleClient.OracleType.Float),
            new System.Data.OracleClient.OracleParameter("I_DELIVERY_DATE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_STRUCKID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SAPPEALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_BLACKLIST", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdScoreListCarInsert_A
            // 
            this.cmdScoreListCarInsert_A.CommandText = resources.GetString("cmdScoreListCarInsert_A.CommandText");
            this.cmdScoreListCarInsert_A.Connection = this.OracleConn;
            this.cmdScoreListCarInsert_A.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("DOC_ID", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("STOPICID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("STRUCKID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("CARHEAD", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("CARDETAIL", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DELIVERY_DATE", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_TOTAL_CAR", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdScoreListCarDelete_A
            // 
            this.cmdScoreListCarDelete_A.CommandText = "DELETE FROM TCOMPLAIN_SCORE_CAR_A WHERE DOC_ID=: I_DOC_ID";
            this.cmdScoreListCarDelete_A.Connection = this.OracleConn;
            this.cmdScoreListCarDelete_A.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdComplainSelectTab3ScoreDetail_A
            // 
            this.cmdComplainSelectTab3ScoreDetail_A.CommandText = resources.GetString("cmdComplainSelectTab3ScoreDetail_A.CommandText");
            this.cmdComplainSelectTab3ScoreDetail_A.Connection = this.OracleConn;
            this.cmdComplainSelectTab3ScoreDetail_A.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdComplainSelectTab3ScoreCar_A
            // 
            this.cmdComplainSelectTab3ScoreCar_A.CommandText = resources.GetString("cmdComplainSelectTab3ScoreCar_A.CommandText");
            this.cmdComplainSelectTab3ScoreCar_A.Connection = this.OracleConn;
            this.cmdComplainSelectTab3ScoreCar_A.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdComplainSelectTab3Score_A
            // 
            this.cmdComplainSelectTab3Score_A.CommandText = resources.GetString("cmdComplainSelectTab3Score_A.CommandText");
            this.cmdComplainSelectTab3Score_A.Connection = this.OracleConn;
            this.cmdComplainSelectTab3Score_A.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30)});
            // 
            // cmdComplainSelectTab3ScoreDetailPartial
            // 
            this.cmdComplainSelectTab3ScoreDetailPartial.Connection = this.OracleConn;
            this.cmdComplainSelectTab3ScoreDetailPartial.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_STRUCKID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DELIVERY_DATE", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdComplainSelectTab3ScoreCarPartial
            // 
            this.cmdComplainSelectTab3ScoreCarPartial.Connection = this.OracleConn;
            this.cmdComplainSelectTab3ScoreCarPartial.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_STRUCKID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DELIVERY_DATE", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdImportFileSelect3
            // 
            this.cmdImportFileSelect3.CommandText = resources.GetString("cmdImportFileSelect3.CommandText");
            this.cmdImportFileSelect3.Connection = this.OracleConn;
            this.cmdImportFileSelect3.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE_IND", System.Data.OracleClient.OracleType.Char, 1)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdImportFileDelete;
        private System.Data.OracleClient.OracleCommand cmdImportFileInsert;
        private System.Data.OracleClient.OracleCommand cmdImportFileSelect;
        private System.Data.OracleClient.OracleCommand cmdImportFileSelect2;
        private System.Data.OracleClient.OracleCommand cmdComplainAttachInsert;
        private System.Data.OracleClient.OracleCommand cmdComplainUpdate;
        private System.Data.OracleClient.OracleCommand cmdComplainUpdateTab3;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3Score;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3ScoreDetail;
        private System.Data.OracleClient.OracleCommand cmdScoreListCarDelete;
        private System.Data.OracleClient.OracleCommand cmdScoreListCarInsert;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3ScoreCar;
        private System.Data.OracleClient.OracleCommand cmdComplainComfirm;
        private System.Data.OracleClient.OracleCommand cmdUpdateCost;
        private System.Data.OracleClient.OracleCommand cmdUpdateCostStatus;
        private System.Data.OracleClient.OracleCommand cmdComplainUpdateTab3_A;
        private System.Data.OracleClient.OracleCommand cmdScoreListCarInsert_A;
        private System.Data.OracleClient.OracleCommand cmdScoreListCarDelete_A;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3ScoreDetail_A;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3ScoreCar_A;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3Score_A;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3ScoreDetailPartial;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectTab3ScoreCarPartial;
        private System.Data.OracleClient.OracleCommand cmdImportFileSelect3;
    }
}
