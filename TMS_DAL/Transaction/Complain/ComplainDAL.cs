﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;

namespace TMS_DAL.Transaction.Complain
{
    public partial class ComplainDAL : OracleConnectionDAL
    {
        public ComplainDAL()
        {
            InitializeComponent();
        }

        public ComplainDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable ComplainSelectDAL(string Condition)
        {
            string Query = cmdComplainSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdComplainSelect, "cmdComplainSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainSelect.CommandText = Query;
            }
        }

        public DataTable ComplainSelectLockDriverDAL(string Condition)
        {
            string Query = cmdComplainLockDriverSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainLockDriverSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdComplainLockDriverSelect, "cmdComplainLockDriverSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainLockDriverSelect.CommandText = Query;
            }
        }
        public DataTable UploadDocLateSelectDAL(string Condition)
        {
            string Query = cmdComplainUploadLate.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainUploadLate.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdComplainUploadLate, "cmdComplainUploadLate");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainUploadLate.CommandText = Query;
            }
        }
        public DataTable SupplainSelectDAL(string Condition)
        {
            string Query = cmdSupplainSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdSupplainSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSupplainSelect, "cmdSupplainSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdSupplainSelect.CommandText = Query;
            }
        }

        public DataTable TruckWorkgroupDAL(string Condition)
        {
            string Query = cmdTruckWorkgroup.CommandText;
            try
            {
                dbManager.Open();
                cmdTruckWorkgroup.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdTruckWorkgroup, "cmdTruckWorkgroup");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdTruckWorkgroup.CommandText = Query;
            }
        }
        public DataTable ComplainSelectAddDriverDAL(string Condition)
        {
            string Query = cmdComplainSelectAddDriver.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainSelectAddDriver.CommandText += Condition;
                cmdComplainSelectAddDriver.CommandText += " ORDER BY ID";

                return dbManager.ExecuteDataTable(cmdComplainSelectAddDriver, "cmdComplainSelectAddDriver");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainSelectAddDriver.CommandText = Query;
            }
        }
        public DataTable ComplainCanEditSelectDAL(string DocID, string SCREATE, string OTP, int OTPType)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = new DataTable();
                dt.Columns.Add("doc_id");
                dt.Columns.Add("SSERVICEID");
                cmdComplainCanEdit.Parameters["I_DOC_ID"].Value = DocID;
                DataTable dtCanEdit = dbManager.ExecuteDataTable(cmdComplainCanEdit, "cmdComplainCanEdit");

                //ล่าช้า
                if (string.Equals(dtCanEdit.Rows[0]["CAN_EDIT"].ToString(), "0") && string.Equals(OTP, string.Empty))
                {
                    dbManager.RollbackTransaction();
                    dt.Rows.Add("URGENT", "");
                }
                else
                {
                    dt.Rows.Add("", "");
                    for (int i = 0; i < dtCanEdit.Rows.Count; i++)
                    {
                        cmdUpdateUrgent.Parameters["I_IS_URGENT"].Value = (dtCanEdit.Rows[i]["CAN_EDIT"].ToString() == "0") ? 1 : 0;
                        cmdUpdateUrgent.Parameters["I_SSERVICEID"].Value = dtCanEdit.Rows[i]["SSERVICEID"].ToString();
                        dbManager.ExecuteNonQuery(cmdUpdateUrgent);
                    }

                    if (!string.Equals(OTP, string.Empty))
                    {
                        cmdComplainOTPSave.CommandText = @"UPDATE T_OTP
                                                       SET ISACTIVE = 0, USE_DATE = SYSDATE, UPDATE_BY = :I_USER_ID, UPDATE_DATETIME = SYSDATE, REF_DOC_NO =:I_REF_DOC_NO
                                                       WHERE OTP_CODE =: I_OTP_CODE AND OTP_TYPE =:I_OTP_TYPE";
                        cmdComplainOTPSave.Parameters["I_OTP_CODE"].Value = OTP;
                        cmdComplainOTPSave.Parameters["I_OTP_TYPE"].Value = OTPType;
                        cmdComplainOTPSave.Parameters["I_USER_ID"].Value = SCREATE;
                        cmdComplainOTPSave.Parameters["I_REF_DOC_NO"].Value = dt.Rows[0]["Doc_ID"].ToString();
                        dbManager.ExecuteNonQuery(cmdComplainOTPSave);
                    }

                    dbManager.CommitTransaction();
                }
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainSelectDriverDAL(string Condition)
        {
            string Query = cmdComplainSelectDriver.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainSelectDriver.CommandText += Condition;
                cmdComplainSelectDriver.CommandText += " ORDER BY ID";

                return dbManager.ExecuteDataTable(cmdComplainSelectDriver, "cmdComplainSelectDriver");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainSelectDriver.CommandText = Query;
            }
        }

        public DataTable ComplainSelectHistoryDAL(string Condition)
        {
            string Query = cmdComplainSelectHistory.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainSelectHistory.CommandText += Condition;
                cmdComplainSelectHistory.CommandText += " ORDER BY DOC_ID ASC";

                return dbManager.ExecuteDataTable(cmdComplainSelectHistory, "cmdComplainSelectHistory");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainSelectHistory.CommandText = Query;
            }
        }

        public DataTable ComplainSelectHeaderDAL(string Condition)
        {
            string Query = cmdComplainSelectHeader.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainSelectHeader.CommandText += Condition;
                cmdComplainSelectHeader.CommandText += " ORDER BY TCOMPLAIN.SSERVICEID";

                return dbManager.ExecuteDataTable(cmdComplainSelectHeader, "cmdComplainSelectHeader");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainSelectHeader.CommandText = Query;
            }
        }

        public DataTable ComplainExportDAL(string Condition)
        {
            string Query = cmdComplainExport.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainExport.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdComplainExport, "dtComplainExport");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainExport.CommandText = Query;
            }
        }

        public DataTable ComplainImportFileDAL(string SSERVICEID)
        {
            try
            {
                dbManager.Open();
                cmdComplainImportFile.Parameters["SSERVICEID"].Value = SSERVICEID;
                return dbManager.ExecuteDataTable(cmdComplainImportFile, "dtComplainImportFile");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainRequestFileDAL(string Condition)
        {
            string Query = cmdComplainRequestFile.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainRequestFile.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdComplainRequestFile, "cmdComplainRequestFile");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainRequestFile.CommandText = Query;
            }
        }

        public DataTable ComplainInsertDAL(string DocID, DataTable dtHeader, string SDETAIL, string SCOMPLAINFNAME, string SCOMPLAINLNAME, string SCOMPLAINDIVISION, string DDATECOMPLAIN, string TTIMECOMPLAIN, string CTYPEPERSON
                                            , string SCOMPLAINBY, string SCREATE, string SUPDATE, string OTP, int OTPType)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainInsert.CommandType = CommandType.StoredProcedure;

                DataSet ds = new DataSet("ds");
                dtHeader.TableName = "dt";
                ds.Tables.Add(dtHeader);

                cmdComplainInsert.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainInsert.Parameters["I_HEADER"].Value = ds.GetXml();
                cmdComplainInsert.Parameters["I_SDETAIL"].Value = SDETAIL;
                cmdComplainInsert.Parameters["I_SCOMPLAINFNAME"].Value = SCOMPLAINFNAME;
                cmdComplainInsert.Parameters["I_SCOMPLAINLNAME"].Value = SCOMPLAINLNAME;
                cmdComplainInsert.Parameters["I_SCOMPLAINDIVISION"].Value = SCOMPLAINDIVISION;
                cmdComplainInsert.Parameters["I_DDATECOMPLAIN"].Value = DDATECOMPLAIN;
                cmdComplainInsert.Parameters["I_TTIMECOMPLAIN"].Value = TTIMECOMPLAIN;
                cmdComplainInsert.Parameters["I_CTYPEPERSON"].Value = CTYPEPERSON;
                cmdComplainInsert.Parameters["I_SCOMPLAINBY"].Value = SCOMPLAINBY;
                cmdComplainInsert.Parameters["I_SCREATE"].Value = SCREATE;
                cmdComplainInsert.Parameters["I_SUPDATE"].Value = SUPDATE;

                DataTable dt = dbManager.ExecuteDataTable(cmdComplainInsert, "cmdComplainInsert");

                cmdComplainCanEdit.Parameters["I_DOC_ID"].Value = dt.Rows[0]["Doc_ID"].ToString();
                DataTable dtCanEdit = dbManager.ExecuteDataTable(cmdComplainCanEdit, "cmdComplainCanEdit");

                //ล่าช้า
                if (string.Equals(dtCanEdit.Rows[0]["CAN_EDIT"].ToString(), "0") && string.Equals(OTP, string.Empty))
                {
                    dbManager.RollbackTransaction();
                    dt.Rows[0][0] = "URGENT";
                }
                else
                {
                    for (int i = 0; i < dtCanEdit.Rows.Count; i++)
                    {
                        cmdUpdateUrgent.Parameters["I_IS_URGENT"].Value = (dtCanEdit.Rows[i]["CAN_EDIT"].ToString() == "0") ? 1 : 0;
                        cmdUpdateUrgent.Parameters["I_SSERVICEID"].Value = dtCanEdit.Rows[i]["SSERVICEID"].ToString();
                        dbManager.ExecuteNonQuery(cmdUpdateUrgent);
                    }

                    if (!string.Equals(OTP, string.Empty))
                    {
                        cmdComplainOTPSave.CommandText = @"UPDATE T_OTP
                                                       SET ISACTIVE = 0, USE_DATE = SYSDATE, UPDATE_BY = :I_USER_ID, UPDATE_DATETIME = SYSDATE, REF_DOC_NO =:I_REF_DOC_NO
                                                       WHERE OTP_CODE =: I_OTP_CODE AND OTP_TYPE =:I_OTP_TYPE";
                        cmdComplainOTPSave.Parameters["I_OTP_CODE"].Value = OTP;
                        cmdComplainOTPSave.Parameters["I_OTP_TYPE"].Value = OTPType;
                        cmdComplainOTPSave.Parameters["I_USER_ID"].Value = SCREATE;
                        cmdComplainOTPSave.Parameters["I_REF_DOC_NO"].Value = dt.Rows[0]["Doc_ID"].ToString();
                        dbManager.ExecuteNonQuery(cmdComplainOTPSave);
                    }

                    dbManager.CommitTransaction();
                }

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable OutboundSelectDAL(string Condition)
        {
            string Query = cmdOutboundSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdOutboundSelect.CommandText = @"SELECT TPLANSCHEDULELIST.SDELIVERYNO 
                                                  FROM TCONTRACT_TRUCK LEFT JOIN TTRUCK ON TCONTRACT_TRUCK.STRUCKID = TTRUCK.STRUCKID 
                                                                       LEFT JOIN TPLANSCHEDULE ON TTRUCK.SHEADREGISTERNO = TPLANSCHEDULE.SHEADREGISTERNO 
                                                                       LEFT JOIN TPLANSCHEDULELIST ON TPLANSCHEDULE.NPLANID = TPLANSCHEDULELIST.NPLANID 
                                                                       LEFT JOIN TVENDOR ON TPLANSCHEDULE.SVENDORID = TVENDOR.SVENDORID 
                                                                       LEFT JOIN TCONTRACT ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                  WHERE 1=1
                                                  AND TVENDOR.INUSE = '1'
                                                  AND TCONTRACT.CACTIVE = 'Y'";
                cmdOutboundSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdOutboundSelect, "cmdOutboundSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdOutboundSelect.CommandText = Query;
            }
        }

        public DataTable OutboundSelectDAL2(string Condition)
        {
            string Query = cmdOutboundSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdOutboundSelect2.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdOutboundSelect2, "cmdOutboundSelect2");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdOutboundSelect2.CommandText = Query;
            }
        }

        public DataTable VendorSelectDAL(string condition = "")
        {
            try
            {
                //SELECT DISTINCT TPLANSCHEDULE.SVENDORID, TVENDOR.SABBREVIATION FROM TCONTRACT_TRUCK LEFT JOIN TTRUCK ON TCONTRACT_TRUCK.STRUCKID = TTRUCK.STRUCKID LEFT JOIN TPLANSCHEDULE ON TTRUCK.SHEADREGISTERNO = TPLANSCHEDULE.SHEADREGISTERNO LEFT JOIN TPLANSCHEDULELIST ON TPLANSCHEDULE.NPLANID = TPLANSCHEDULELIST.NPLANID LEFT JOIN TVENDOR ON TPLANSCHEDULE.SVENDORID = TVENDOR.SVENDORID LEFT JOIN TCONTRACT ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID WHERE 1=1 AND TTRUCK.CACTIVE = 'Y' AND TVENDOR.CACTIVE = '1' AND TCONTRACT.CACTIVE = 'Y'
                dbManager.Open();
                cmdVendorSelect.CommandText = String.Format(@"SELECT ven.DUPDATE, ven.SVENDORID,ven.SABBREVIATION,vensap.SNO ||' '|| vensap.SDISTRICT||' '|| vensap.SREGION ||' '|| vensap.SPROVINCE ||' '|| vensap.SPROVINCECODE as Address
                                            , ven.STEL ,CASE ven.CACTIVE
                                                WHEN '0' THEN 'ไม่อนุญาตให้ใช้งาน' 
                                                ELSE ' ' 
                                            END AS CACTIVE
                                            ,ven.CHECKIN ,ven.NOTFILL,ven.INUSE
                                            FROM TVendor ven
                                            LEFT  JOIN TVENDOR_SAP vensap
                                            ON ven.SVENDORID = vensap.SVENDORID
                                            WHERE  1=1   AND ven.INUSE = '1' AND NVL(ven.CACTIVE,'Y') != '0' {0}
                                            ORDER BY ven.INUSE DESC NULLS LAST,ven.DUPDATE DESC NULLS LAST", condition);
                //WHERE  1=1   AND ven.INUSE = '1' AND NVL(ven.CACTIVE,'Y') != '0'
                return dbManager.ExecuteDataTable(cmdVendorSelect, "cmdVendorSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable EmployeeSelectDAL()
        {
            try
            {
                dbManager.Open();
                return dbManager.ExecuteDataTable(cmdEmployeeSelect, "cmdEmployeeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainListSelectDAL(string Condition)
        {
            string Query = cmdComplainListSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainListSelect.CommandText = @"SELECT * FROM (SELECT DISTINCT TO_NUMBER(REPLACE(REPLACE(REPLACE(c.DOC_ID, '-01-', ''), '-02-', ''), '-', '')) AS DOC_ORDERBY 
                                                    , M_GROUPS.NAME GROUPNAME, FN_GET_COMPLAIN_LIST('DDELIVERY', c.DOC_ID) AS DDELIVERY 
                                                    , FN_GET_COMPLAIN_LIST('CARHEAD', c.DOC_ID) AS SHEADREGISTERNO, FN_GET_COMPLAIN_LIST('CARDETAIL', c.DOC_ID) AS STRAILERREGISTERNO 
                                                    , V.SVENDORNAME, DOC_STATUS_NAME AS STATUS, TCONTRACT.SCONTRACTNO, c.DOC_ID, C.SCREATE
                                                    , TUSER.SFIRSTNAME || '(' || CASE TUSER.CGROUP 
                                                            WHEN '0' THEN TVENDOR.SABBREVIATION 
                                                            WHEN '1' THEN TUNIT.UNITABBR 
                                                            WHEN '2' THEN TTERMINAL.SABBREVIATION
                                                            WHEN '3' THEN TUNIT.UNITABBR 
                                                            WHEN '4' THEN TUNIT.UNITABBR 
                                                            WHEN '5' THEN TUNIT.UNITABBR 
                                                            END || ')' AS CREATENAME 
                                                    , TO_CHAR(c.DDATECOMPLAIN, 'DD/MM/YYYY') AS DDATECOMPLAIN, FN_COUNT_IS_URGENT (c.DOC_ID) AS IS_URGENT
                                                    FROM ((TCOMPLAIN c LEFT JOIN TVENDOR_SAP v ON c.SVENDORID = V.SVENDORID) 
                                                        LEFT JOIN (SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'C_1_1' ) THEADCOMPLAIN ON NVL(c.SHEADCOMLIAIN,-1) = THEADCOMPLAIN.NID 
                                                        LEFT JOIN TTOPIC t ON c.STOPICID = T.STOPICID AND C.SVERSION = T.SVERSION ) 
                                                        LEFT JOIN TAPPEAL ap ON C.SSERVICEID = AP.SREFERENCEID AND AP.SAPPEALTYPE = '080' 
                                                        LEFT JOIN TCONTRACT ON c.SCONTRACTID = TCONTRACT.SCONTRACTID LEFT JOIN TCOMPLAINTYPE comp ON comp.SCOMPLAINTYPEID = c.SHEADCOMLIAIN 
                                                        LEFT JOIN TUSER ON c.SCREATE = TUSER.SUID 
                                                        LEFT JOIN TVENDOR ON TUSER.SVENDORID=TVENDOR.SVENDORID 
                                                        LEFT JOIN TTERMINAL ON TUSER.SVENDORID=TTERMINAL.STERMINALID 
                                                        LEFT JOIN TUNIT ON TUSER.SVENDORID=TUNIT.UNITCODE 
                                                        LEFT JOIN M_DOC_STATUS ON c.DOC_STATUS_ID = M_DOC_STATUS.DOC_STATUS_ID 
                                                        LEFT JOIN M_COMPLAIN_POINT ON c.CCUT = M_COMPLAIN_POINT.id 
                                                        LEFT JOIN M_GROUPS ON TCONTRACT.GROUPSID = M_GROUPS.ID 
                                                        LEFT JOIN TEMPLOYEE_SAP ON C.SPERSONALNO = TEMPLOYEE_SAP.SEMPLOYEEID 
                                                    WHERE 1=1 ";

                cmdComplainListSelect.CommandText += Condition;
                cmdComplainListSelect.CommandText += ") TMP ORDER BY DOC_ORDERBY DESC";
                //cmdComplainListSelect.CommandText += " ORDER BY c.DOC_ID DESC";

                return dbManager.ExecuteDataTable(cmdComplainListSelect, "cmdComplainListSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainListSelect.CommandText = Query;
            }
        }

        public DataTable ComplainRequireFieldDAL(int COMPLAIN_TYPE_ID, string FIELD_TYPE)
        {
            try
            {
                dbManager.Open();
                cmdComplainRequireField.Parameters["COMPLAIN_TYPE_ID"].Value = COMPLAIN_TYPE_ID;
                cmdComplainRequireField.Parameters["FIELD_TYPE"].Value = FIELD_TYPE;
                return dbManager.ExecuteDataTable(cmdComplainRequireField, "cmdComplainRequireField");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainCanEditDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainCanEdit.CommandText = @"SELECT SSERVICEID, DDELIVERY, '15/' || TO_CHAR(ADD_MONTHS(DDELIVERY, 1), 'MM', 'NLS_DATE_LANGUAGE = ENGLISH') || '/' || 
                                                  CASE WHEN (TO_CHAR(ADD_MONTHS(DDELIVERY, 1), 'MM', 'NLS_DATE_LANGUAGE = ENGLISH') = '01') 
                                                       THEN  TO_CHAR(ADD_MONTHS(DDELIVERY, 1), 'YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') 
                                                  ELSE TO_CHAR(DDELIVERY, 'YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') END AS DATE_EDIT, 

                                                  CASE WHEN TO_DATE(TO_CHAR(ADD_MONTHS(DDELIVERY, 1), 'MM', 'NLS_DATE_LANGUAGE = ENGLISH') || '/' || '15/' || 
                                                  CASE WHEN (TO_CHAR(ADD_MONTHS(DDELIVERY, 1), 'MM', 'NLS_DATE_LANGUAGE = ENGLISH') = '01') 
                                                  THEN  TO_CHAR(ADD_MONTHS(DDELIVERY, 1), 'YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') 
                                                  ELSE TO_CHAR(DDELIVERY, 'YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') END, 'MM/DD/YYYY') >= TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY'), 'MM/DD/YYYY')
                                                  THEN 1 ELSE 0 END AS CAN_EDIT FROM TCOMPLAIN 
  
                                                  WHERE DOC_ID =:I_DOC_ID 
                                                  ORDER BY DDELIVERY";

                cmdComplainCanEdit.Parameters["I_DOC_ID"].Value = DocID;
                return dbManager.ExecuteDataTable(cmdComplainCanEdit, "cmdComplainCanEdit");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ContractSelectDAL(string Condition)
        {
            string Query = cmdContractSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdContractSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdContractSelect, "cmdContractSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdContractSelect.CommandText = Query;
            }
        }
        public void ComplainChangeStatusDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainChangeStatus.Parameters["I_DOC_ID"].Value = DocID;

                dbManager.ExecuteNonQuery(cmdComplainChangeStatus);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public void ComplainChangStatusDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainChangStatus.Parameters["I_DOC_ID"].Value = DocID;

                dbManager.ExecuteNonQuery(cmdComplainChangStatus);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public void UnlockDriverSaveDAL(string I_EMPLOYEEID)
        {
            try
            {
                cmdUnlockDriverSave.CommandType = CommandType.StoredProcedure;
                cmdUnlockDriverSave.CommandText = @"USP_T_UNLOCKDRIVER_SAVE";

                cmdUnlockDriverSave.Parameters.Clear();
                cmdUnlockDriverSave.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_EMPLOYEEID",
                    Value = I_EMPLOYEEID,
                    OracleType = OracleType.VarChar
                });
                dbManager.Open();
                dbManager.BeginTransaction();

                dbManager.ExecuteNonQuery(cmdUnlockDriverSave);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable ContractSelectDAL2(string Condition)
        {
            string Query = cmdContractSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdContractSelect.CommandText = @"SELECT DISTINCT TCONTRACT.SCONTRACTID, TCONTRACT.SCONTRACTNO, TCONTRACT.SVENDORID
                                                  FROM TCONTRACT
                                                  WHERE 1 = 1
                                                  AND TCONTRACT.CACTIVE = 'Y'";

                cmdContractSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdContractSelect, "cmdContractSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdContractSelect.CommandText = Query;
            }
        }

        public DataTable TruckSelectDAL(string Condition)
        {
            string Query = cmdTruckSelect.CommandText;
            try
            {
                dbManager.Open();

                cmdTruckSelect.CommandText = @"SELECT DISTINCT TTRUCK.SHEADREGISTERNO, TTRUCK.STRAILERREGISTERNO, TTRUCK.STRUCKID 
                                                FROM TCONTRACT_TRUCK LEFT JOIN TTRUCK ON TCONTRACT_TRUCK.STRUCKID = TTRUCK.STRUCKID 
                                                                     LEFT JOIN TCONTRACT ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID 
                                                                     LEFT JOIN TVENDOR ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID 
                                                WHERE 1=1 AND TVENDOR.CACTIVE = '1' AND TCONTRACT.CACTIVE = 'Y'";

                cmdTruckSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdTruckSelect, "cmdTruckSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdTruckSelect.CommandText = Query;
            }
        }

        public DataTable TruckOutContractSelectDAL(string Condition)
        {
            try
            {
                cmdTruckOutContractSelect.CommandText = @"SELECT DISTINCT TMP.SHEADREGISTERNO, TMP.STRAILERREGISTERNO, TMP.STRUCKID
                                                          FROM
                                                          (
                                                              SELECT TTRUCK.SHEADREGISTERNO, TTRUCK.STRAILERREGISTERNO, TTRUCK.STRUCKID, TTRUCKCONFIRM.DDATE, TCONTRACT.SCONTRACTID, TCONTRACT.SCONTRACTNO, TVENDOR.SVENDORID, TVENDOR.SABBREVIATION
                                                              FROM TTRUCK INNER JOIN TTRUCKCONFIRMLIST ON TTRUCK.STRUCKID = TTRUCKCONFIRMLIST.STRUCKID
                                                                          INNER JOIN TTRUCKCONFIRM ON TTRUCKCONFIRMLIST.NCONFIRMID = TTRUCKCONFIRM.NCONFIRMID
                                                                          INNER JOIN TCONTRACT ON TTRUCKCONFIRM.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                                          INNER JOIN TVENDOR ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID
                                                              WHERE TVENDOR.CACTIVE = '1'
                                                              AND TCONTRACT.CACTIVE = 'Y'" + Condition.Replace("TCONTRACT_TRUCK", "TCONTRACT") +
                                                            @"ORDER BY TTRUCKCONFIRM.DDATE DESC
                                                          ) TMP
                                                          UNION ALL
                                                          SELECT DISTINCT TTRUCK.SHEADREGISTERNO, TTRUCK.STRAILERREGISTERNO, TTRUCK.STRUCKID 
                                                          FROM TCONTRACT_TRUCK LEFT JOIN TTRUCK ON TCONTRACT_TRUCK.STRUCKID = TTRUCK.STRUCKID 
                                                                               LEFT JOIN TCONTRACT ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID 
                                                                               LEFT JOIN TVENDOR ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID 
                                                          WHERE 1=1 AND TVENDOR.CACTIVE = '1' AND TCONTRACT.CACTIVE = 'Y'" + Condition;

                return dbManager.ExecuteDataTable(cmdTruckOutContractSelect, "cmdTruckOutContractSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable PersonalSelectDAL(string Condition)
        {
            string Query = cmdPersonalSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdPersonalSelect.CommandText += Condition;
                cmdPersonalSelect.CommandText += " ORDER BY TEMPLOYEE_SAP.FNAME, TEMPLOYEE_SAP.LNAME";

                return dbManager.ExecuteDataTable(cmdPersonalSelect, "cmdPersonalSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdPersonalSelect.CommandText = Query;
            }
        }

        public DataTable LoginSelectDAL(string Condition)
        {
            string Query = cmdLoginSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdLoginSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdLoginSelect, "cmdLoginSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdLoginSelect.CommandText = Query;
            }
        }

        public void ComplainAttachInsertDAL(DataTable dtUpload, string DocID, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdComplainAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdComplainAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdComplainAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdComplainAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdComplainAttachInsert.Parameters["REF_INT"].Value = null;
                    cmdComplainAttachInsert.Parameters["REF_STR"].Value = DocID;
                    cmdComplainAttachInsert.Parameters["ISACTIVE"].Value = "1";
                    cmdComplainAttachInsert.Parameters["CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdComplainAttachInsert);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainAddDriverDAL(string DocID, string DriverNo)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainAddDriver.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainAddDriver.Parameters["I_DRIVERNO"].Value = DriverNo;

                dbManager.ExecuteNonQuery(cmdComplainAddDriver);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainInsertLockDriverNewDAL(string DocID, string DriverNo, string CreateBy, string VendorId)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainLockDriverNew.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainLockDriverNew.Parameters["I_DRIVERNO"].Value = DriverNo;
                cmdComplainLockDriverNew.Parameters["I_SCREATE"].Value = CreateBy;
                cmdComplainLockDriverNew.Parameters["I_VENDORID"].Value = VendorId;

                dbManager.ExecuteNonQuery(cmdComplainLockDriverNew);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public void ComplainDelDriverDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainDelDriver.Parameters["I_DOC_ID"].Value = DocID;

                dbManager.ExecuteNonQuery(cmdComplainDelDriver);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainUpdateOldDriverDAL(string DocID, string DriverNo, int ACTIVE, string Days, string Remark, int Blacklist)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainUpdateOldDriver.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainUpdateOldDriver.Parameters["I_DRIVERNO"].Value = DriverNo;
                cmdComplainUpdateOldDriver.Parameters["I_IS_ACTIVE"].Value = ACTIVE;
                cmdComplainUpdateOldDriver.Parameters["I_DAYS"].Value = Days;
                cmdComplainUpdateOldDriver.Parameters["I_REMARK"].Value = Remark;
                cmdComplainUpdateOldDriver.Parameters["I_BLACKLIST"].Value = Blacklist;

                dbManager.ExecuteNonQuery(cmdComplainUpdateOldDriver);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainUpdateDriverDAL(string DocID, string DriverNo, int ACTIVE, string Days, string Remark, int Blacklist)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainUpdateDriver.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainUpdateDriver.Parameters["I_DRIVERNO"].Value = DriverNo;
                cmdComplainUpdateDriver.Parameters["I_IS_ACTIVE"].Value = ACTIVE;
                cmdComplainUpdateDriver.Parameters["I_DAYS"].Value = Days;
                cmdComplainUpdateDriver.Parameters["I_REMARK"].Value = Remark;
                cmdComplainUpdateDriver.Parameters["I_BLACKLIST"].Value = Blacklist;

                dbManager.ExecuteNonQuery(cmdComplainUpdateDriver);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainCancelDocDAL(string DocID, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainCancelDoc.Parameters["I_DOC_ID"].Value = DocID;
                dbManager.ExecuteNonQuery(cmdComplainCancelDoc);

                DataTable dtDriver = new DataTable();
                cmdSelectDriverDisable.CommandText = @"SELECT SEMPLOYEEID
                                                       FROM TCOMPLAIN_LOCK_DRIVER
                                                       WHERE DOC_ID = :I_DOC_ID
                                                       AND IS_INVALID_DRIVER = 0";
                cmdSelectDriverDisable.Parameters["I_DOC_ID"].Value = DocID;
                dtDriver = dbManager.ExecuteDataTable(cmdSelectDriverDisable, "cmdSelectDriverDisable");

                if (dtDriver.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDriver.Rows.Count; i++)
                    {
                        cmdGenerateText.CommandText = @"SELECT FN_GET_MSG_DISABLE_DRIVER('MSG_DISABLE_DRIVER', :I_DOC_ID, :I_SEMPLOYEEID, 'ใช้งาน', ' ', TO_CHAR(SYSDATE, 'DD/MM/YYYY HH:MI:SS', 'NLS_DATE_LANGUAGE = ENGLISH'), 'ยกเลิกเอกสารข้อร้องเรียน') AS TEXT FROM DUAL";
                        cmdGenerateText.Parameters["I_DOC_ID"].Value = DocID;
                        cmdGenerateText.Parameters["I_SEMPLOYEEID"].Value = dtDriver.Rows[i]["SEMPLOYEEID"].ToString();
                        DataTable dtText = dbManager.ExecuteDataTable(cmdGenerateText, "cmdGenerateText");

                        cmdCancelDocUpdateDisable.CommandText = @"UPDATE TCOMPLAIN_LOCK_DRIVER
                                                          SET IS_INVALID_DRIVER = 1, STATUS = 0, DATE_END = SYSDATE, UPDATE_BY = :I_SCREATE, UPDATE_DATETIME = SYSDATE
                                                              , DETAIL = :I_TEXT
                                                          WHERE DOC_ID = :I_DOC_ID
                                                          AND SEMPLOYEEID = :I_SEMPLOYEEID
                                                          AND IS_INVALID_DRIVER = 0";
                        cmdCancelDocUpdateDisable.Parameters["I_DOC_ID"].Value = DocID;
                        cmdCancelDocUpdateDisable.Parameters["I_SCREATE"].Value = CreateBy;
                        cmdCancelDocUpdateDisable.Parameters["I_SEMPLOYEEID"].Value = dtDriver.Rows[i]["SEMPLOYEEID"].ToString();
                        cmdCancelDocUpdateDisable.Parameters["I_TEXT"].Value = dtText.Rows[0]["TEXT"].ToString();
                        dbManager.ExecuteNonQuery(cmdCancelDocUpdateDisable);
                    }
                }

                dbManager.CommitTransaction();
                return dtDriver;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ScoreSelectDAL()
        {
            try
            {
                dbManager.Open();
                return dbManager.ExecuteDataTable(cmdScoreSelect, "cmdScoreSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable CusScoreSelectDAL()
        {
            try
            {
                dbManager.Open();
                return dbManager.ExecuteDataTable(cmdCusScoreTypeSelect, "cmdCusScoreTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainChangeStatusDAL(string DocID, int DocStatusID, int UpdateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainChageStatus.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainChageStatus.Parameters["I_DOC_STATUS_ID"].Value = DocStatusID;
                cmdComplainChageStatus.Parameters["I_UPDATE_BY"].Value = UpdateBy;
                dbManager.ExecuteNonQuery(cmdComplainChageStatus);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();

                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable PersonalDetailSelectDAL(string EmployeeID)
        {
            try
            {
                cmdPersonalDetailSelect.Parameters["I_SEMPLOYEEID"].Value = EmployeeID;

                return dbManager.ExecuteDataTable(cmdPersonalDetailSelect, "cmdPersonalDetailSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void LockDriverInsertDAL(string DocID, string EmployeeID, string DateStart, string DateEnd, string Status, string DriverStatus, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdLockDriverInsert.Parameters["I_DOC_ID"].Value = DocID;
                cmdLockDriverInsert.Parameters["I_SEMPLOYEEID"].Value = EmployeeID;
                cmdLockDriverInsert.Parameters["I_DATE_START"].Value = DateStart;
                cmdLockDriverInsert.Parameters["I_DATE_END"].Value = DateEnd;
                cmdLockDriverInsert.Parameters["I_STATUS"].Value = Status;
                cmdLockDriverInsert.Parameters["I_DRIVER_STATUS"].Value = DriverStatus;
                cmdLockDriverInsert.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdLockDriverInsert);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable DriverUpdateSelectDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdDriverUpdateSelect.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdDriverUpdateSelect, "cmdDriverUpdateSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void DriverUpdateLogDAL(string DocID, string ID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdDriverUpdateLog.Parameters["I_DOC_ID"].Value = DocID;
                cmdDriverUpdateLog.Parameters["I_ID"].Value = ID;

                dbManager.ExecuteNonQuery(cmdDriverUpdateLog);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void DriverUpdateTMSDAL(string EmployeeID, string StatusTMS, string StatusSAP, string StatusBan, string Remark, string Blacklist)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                if (Blacklist == "1")
                {
                    cmdDriverUpdateTMS.CommandText = @"UPDATE TEMPLOYEE
                                                   SET CACTIVE =:I_STATUS, DUPDATE = SYSDATE,CANCELSTATUS =:I_STATUSBAN,CAUSESAPCANCEL =:I_REMARK
                                                   WHERE SEMPLOYEEID =:I_SEMPLOYEEID";
                }
                else
                {
                    cmdDriverUpdateTMS.CommandText = @"UPDATE TEMPLOYEE
                                                   SET CACTIVE =:I_STATUS, DUPDATE = SYSDATE,BANSTATUS =:I_STATUSBAN,CAUSESAP =:I_REMARK
                                                   WHERE SEMPLOYEEID =:I_SEMPLOYEEID";
                }
                cmdDriverUpdateTMS.Parameters["I_SEMPLOYEEID"].Value = EmployeeID;
                cmdDriverUpdateTMS.Parameters["I_STATUS"].Value = StatusTMS;
                cmdDriverUpdateTMS.Parameters["I_STATUSBAN"].Value = StatusBan;
                cmdDriverUpdateTMS.Parameters["I_REMARK"].Value = Remark;
                dbManager.ExecuteNonQuery(cmdDriverUpdateTMS);

                cmdDriverUpdateSAP.CommandText = @"UPDATE TEMPLOYEE_SAP
                                                   SET DRVSTATUS =:I_STATUS, DUPDATE = SYSDATE
                                                   WHERE SEMPLOYEEID =:I_SEMPLOYEEID";

                cmdDriverUpdateSAP.Parameters["I_SEMPLOYEEID"].Value = EmployeeID;
                cmdDriverUpdateSAP.Parameters["I_STATUS"].Value = StatusSAP;
                dbManager.ExecuteNonQuery(cmdDriverUpdateSAP);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainLockDriverDAL(string DocID, int Total, int CreateBy, string Detail, string Blacklist, string Driver)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainLockDriver.CommandType = CommandType.StoredProcedure;

                cmdComplainLockDriver.Parameters["I_DOC_ID"].Value = DocID;
                cmdComplainLockDriver.Parameters["I_TOTAL"].Value = Total;
                cmdComplainLockDriver.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdComplainLockDriver.Parameters["I_DETAIL"].Value = Detail;
                cmdComplainLockDriver.Parameters["I_BLACKLIST"].Value = Blacklist;
                cmdComplainLockDriver.Parameters["I_DRIVER"].Value = Driver;

                dbManager.ExecuteNonQuery(cmdComplainLockDriver);
                dbManager.CommitTransaction();

            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable TBL_REQUEST_TMP(string sServiceType)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdTblReqTemp.CommandType = CommandType.StoredProcedure;

                cmdTblReqTemp.Parameters["REQTYPE_ID1"].Value = sServiceType;


                //dbManager.ExecuteNonQuery(cmdTblReqTemp);                

                DataTable dt = dbManager.ExecuteDataTable(cmdTblReqTemp, "cmdTblReqTemp");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable DriverUpdateSelect2DAL(string DocID)
        {
            try
            {
                dbManager.Open();

                //cmdDriverUpdateSelect2.CommandText = "SELECT ID, DOC_ID, SEMPLOYEEID, '0' AS CHANGE_TO FROM TCOMPLAIN_LOCK_DRIVER WHERE DOC_ID =:I_DOC_ID AND STATUS = 0 AND IS_UNLOCK = 0";
                cmdDriverUpdateSelect2.CommandText = @"SELECT SPERSONALNO AS SEMPLOYEEID
                                                       FROM TCOMPLAIN
                                                       WHERE TCOMPLAIN.DOC_ID =:I_DOC_ID";

                cmdDriverUpdateSelect2.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdDriverUpdateSelect2, "cmdDriverUpdateSelect2");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ComplainExportFinalDAL(string Condition)
        {
            string Query = cmdComplainExportFinal.CommandText;
            try
            {
                dbManager.Open();
                cmdComplainExportFinal.CommandText = @"SELECT DISTINCT TCOMPLAIN.DOC_ID
                                                      , M_COMPLAIN_TYPE.COMPLAIN_TYPE_NAME AS SUBJECT, '' AS SDOCUMENT
                                                      , '' AS DATEIMPORT
                                                      , '' AS STOPICNAME
                                                      , CASE WHEN TCOMPLAIN_SCORE.TOTAL_POINT = -1 THEN NULL ELSE TCOMPLAIN_SCORE.TOTAL_POINT END AS CCUT
                                                      , CASE WHEN TCOMPLAIN_SCORE.TOTAL_COST = -1 THEN NULL ELSE TCOMPLAIN_SCORE.TOTAL_COST END AS FINECOST
                                                      , TO_CHAR(TCOMPLAIN.DDATECOMPLAIN, 'DD/MM/YYYY') AS DDATECOMPLAIN
                                                      , FN_GET_COMPLAIN_LIST('DDELIVERY_LIST', TCOMPLAIN.DOC_ID) AS DDELIVERY
                                                      , FN_GET_COMPLAIN_LIST('CARHEAD_LIST', TCOMPLAIN.DOC_ID) AS SHEADREGISTERNO
                                                      , FN_GET_COMPLAIN_LIST('CARDETAIL_LIST', TCOMPLAIN.DOC_ID) AS STRAILERREGISTERNO
                                                      , TVENDOR.SABBREVIATION, TCONTRACT.SCONTRACTNO, TCOMPLAIN.SCOMPLAINADDRESS, TCOMPLAIN.SDELIVERYNO
                                                      , TTERMINAL.SABBREVIATION AS WAREHOUSE, TCOMPLAIN.SCOMPLAINFNAME, M_TOPIC.TOPIC_NAME, M_COMPLAIN_TYPE.COMPLAIN_TYPE_NAME
                                                      , TCOMPLAIN.SDETAIL, TCOMPLAIN.SSOLUTION, TUSER.SFIRSTNAME as CREATENAME, TUSER.SPOSITION as CREATE_POSITION
                                                      , TO_CHAR(TCOMPLAIN.DCREATE, 'DD/MM/YYYY') AS DCREATE
                                                      , TO_CHAR(TCOMPLAIN.DUPDATE, 'DD/MM/YYYY') AS DUPDATE
                                                      , CASE WHEN TUSER_2.SFIRSTNAME IS NOT NULL THEN TUSER_2.SFIRSTNAME ELSE NULL END AS UPDATENAME
                                                      , CASE WHEN TUSER_2.SFIRSTNAME IS NOT NULL THEN TUSER_2.SPOSITION ELSE NULL END AS UPDATE_POSITION
                                                      , TO_CHAR(TCOMPLAIN.COMPLAIN_DATE, 'DD/MM/YYYY') AS COMPLAIN_DATE
                                                      , TP.FULLNAME , '' AS IS_CORRUPT,
                                                      (SELECT M_DOC_STATUS.DOC_STATUS_NAME FROM M_DOC_STATUS WHERE doc_status_id = TCOMPLAIN.doc_status_id
                                                      ) AS DOC_STATUS_NAME, (SELECT M_GROUPS.NAME GROUPNAME
                                                      FROM M_GROUPS WHERE TCONTRACT.GROUPSID = M_GROUPS.ID
                                                      ) AS GROUPNAME, TCOMPLAIN.TOTAL_CAR,
                                                      TP.SPERSONALNO , TCOMPLAIN.WAREHOUSE_TO, TCOMPLAIN.SCOMPLAINDIVISION,
                                                      CASE TUSER.CGROUP
                                                        WHEN '0'
                                                        THEN TVENDOR.SABBREVIATION
                                                        WHEN '1'
                                                        THEN
                                                          (SELECT TUNIT.UNITNAME FROM TUNIT WHERE TUSER.SVENDORID = TUNIT.UNITCODE
                                                          )
                                                        WHEN '2'
                                                        THEN TTERMINAL.SABBREVIATION
                                                      END SUNITNAME ,
                                                      TCOMPLAIN_SCORE.COST_OTHER , TCOMPLAIN_SCORE.OIL_LOSE,
                                                      TOTAL_DISABLE_DRIVER,  DECODE(NVL(BLACKLIST,0),'1','ใช่','ไม่') AS BLACKLIST,
                                                      ''                                                         AS TOTALPAID ,
                                                      TOTAL_COST, DECODE(NVL(COST_CHECK, 0),1,'ชำระครบถ้วน', 'ยังไม่ได้ชำระ/ชำระยังไม่ครบถ้วน') AS COST_CHECK,
                                                      TO_CHAR(COST_CHECK_DATE, 'DD/MM/YYYY')                                        AS COST_CHECK_DATE
                                                    FROM TCOMPLAIN
                                                    LEFT JOIN M_TOPIC
                                                    ON TCOMPLAIN.SSUBJECT = M_TOPIC.TOPIC_ID
                                                    LEFT JOIN M_COMPLAIN_TYPE
                                                    ON TCOMPLAIN.SHEADCOMLIAIN = M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID
                                                    AND M_TOPIC.TOPIC_ID       = M_COMPLAIN_TYPE.TOPIC_ID
                                                    LEFT JOIN TCOMPLAIN_SCORE
                                                    ON TCOMPLAIN.DOC_ID = TCOMPLAIN_SCORE.DOC_ID
                                                    LEFT JOIN TVENDOR
                                                    ON TCOMPLAIN.SVENDORID = TVENDOR.SVENDORID
                                                    LEFT JOIN TCONTRACT
                                                    ON TCOMPLAIN.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                    LEFT JOIN TTERMINAL
                                                    ON TCOMPLAIN.STERMINALID = TTERMINAL.STERMINALID
                                                    LEFT JOIN TUSER
                                                    ON TCOMPLAIN.SCREATE = TUSER.SUID
                                                    LEFT JOIN TUSER TUSER_2
                                                    ON TCOMPLAIN.SUPDATE             = TUSER_2.SUID
                                                    LEFT JOIN
                                                      (SELECT SPERSONELNO as SPERSONALNO,
                                                        FULLNAME,
                                                        STRANS_ID,
                                                        SEMPLOYEEID
                                                      FROM
                                                        (SELECT E.SPERSONELNO,
                                                          E.INAME
                                                          || ES.FNAME
                                                          || ' '
                                                          || ES.LNAME AS FULLNAME,
                                                          E.STEL ,
                                                          E.STRANS_ID,
                                                          E.SEMPLOYEEID 
                                                        FROM TEMPLOYEE e
                                                        INNER JOIN TEMPLOYEE_SAP es
                                                        ON E.SEMPLOYEEID       = ES.SEMPLOYEEID
                                                        )
                                                      ) TP ON TP.SEMPLOYEEID         = TCOMPLAIN.SPERSONALNO 
                                                    WHERE (TCOMPLAIN_SCORE.ISACTIVE IS NULL
                                                    OR TCOMPLAIN_SCORE.ISACTIVE      = 1)";
                cmdComplainExportFinal.CommandText += Condition;
                cmdComplainExportFinal.CommandText += " ORDER BY TCOMPLAIN.DOC_ID";

                return dbManager.ExecuteDataTable(cmdComplainExportFinal, "cmdComplainExportFinal");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdComplainExportFinal.CommandText = Query;
            }
        }

        public DataTable ComplainImportSelectDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainImportSelect.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainImportSelect, "cmdComplainImportSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ExportScoreSelectDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdExportScoreSelect.CommandText = @"SELECT TCOMPLAIN_SCORE_DETAIL.STOPICID, TTOPIC.STOPICNAME, TTOPIC.TTOPIC_TYPE_ID, TTOPIC.IS_CORRUPT
                                                    FROM TCOMPLAIN_SCORE_DETAIL INNER JOIN TTOPIC ON TCOMPLAIN_SCORE_DETAIL.STOPICID = TTOPIC.STOPICID
                                                    WHERE TTOPIC.CACTIVE = 1
                                                    AND TCOMPLAIN_SCORE_DETAIL.ISACTIVE = 1
                                                    AND TCOMPLAIN_SCORE_DETAIL.DOC_ID =: I_DOC_ID";

                cmdExportScoreSelect.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdExportScoreSelect, "cmdExportScoreSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UpdateDriverStatusDAL(DataTable dtDriver)
        {
            try
            {
                if (dbManager != null)
                    dbManager.Dispose();

                dbManager.Open();
                dbManager.BeginTransaction();

                DataTable dtDriverDetail = new DataTable();

                for (int i = 0; i < dtDriver.Rows.Count; i++)
                {
                    dtDriverDetail = PersonalDetailSelectDAL(dtDriver.Rows[i]["SEMPLOYEEID"].ToString());

                    cmdUpdateDriverStatus.Parameters["I_SEMPLOYEEID"].Value = dtDriverDetail.Rows[0]["SEMPLOYEEID"].ToString();
                    cmdUpdateDriverStatus.Parameters["I_CACTIVE"].Value = 0;

                    dbManager.ExecuteNonQuery(cmdUpdateDriverStatus);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ChangeStatusDAL(string DocID, int DocStatusID, int UpdateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdChangeStatus.Parameters["I_DOC_ID"].Value = DocID;
                cmdChangeStatus.Parameters["I_DOC_STATUS_ID"].Value = DocStatusID;
                cmdChangeStatus.Parameters["I_SUPDATE"].Value = UpdateBy;

                dbManager.ExecuteNonQuery(cmdChangeStatus);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataSet GetEmailComplainDAL(string DeliveryDepartmentCodeAdmin, string DeliveryDepartmentCodeAdmin2, string DeliveryDepartmentCode, int UserID, string VendorID, bool IsAdmin, bool Check)
        {//IsAdmin ใเฉพาะทีม รข.มาตรฐาน,สัญญา หรือไม่
            string Tmp1 = cmdGetEmailComplainAdmin.CommandText;
            string Tmp2 = cmdGetEmailComplain.CommandText;
            string Tmp3 = cmdGetEmailComplainAdmin2.CommandText;

            try
            {
                dbManager.Open();

                DataSet dsEmail = new DataSet();
                DataTable dtEmailDepartmentAdmin = new DataTable();             //ผจ. รข
                DataTable dtEmailDepartmentAdmin2 = new DataTable();             //ผจ. ปง
                DataTable dtEmailDepartment = new DataTable();                  //รข
                DataTable dtUser = new DataTable();                             //User Create Doc
                DataTable dtVendor = new DataTable();                           //Vendor

                if (Check)
                {
                    //ผจ. รข
                    if (!string.Equals(DeliveryDepartmentCodeAdmin, string.Empty))
                    {
                        cmdGetEmailComplainAdmin.CommandText += " AND SPOSITION LIKE '%ผจ.ขปน.%'";

                        cmdGetEmailComplainAdmin.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCodeAdmin;
                        dtEmailDepartmentAdmin = dbManager.ExecuteDataTable(cmdGetEmailComplainAdmin, "cmdGetEmailComplainAdmin");
                        dsEmail.Tables.Add(dtEmailDepartmentAdmin.Copy());
                    }

                    //ผจ. ปง
                    if (!string.Equals(DeliveryDepartmentCodeAdmin2, string.Empty))
                    {
                        cmdGetEmailComplainAdmin2.CommandText += " AND SPOSITION LIKE '%ผจ.ปง.%'";

                        cmdGetEmailComplainAdmin2.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCodeAdmin2;
                        dtEmailDepartmentAdmin2 = dbManager.ExecuteDataTable(cmdGetEmailComplainAdmin2, "cmdGetEmailComplainAdmin2");
                        dsEmail.Tables.Add(dtEmailDepartmentAdmin2.Copy());
                    }

                    //รข
                    if (!string.Equals(DeliveryDepartmentCode, string.Empty))
                    {
                        if (IsAdmin)
                            cmdGetEmailComplain.CommandText += " AND IS_CONTRACT IN (1, 2)";

                        cmdGetEmailComplain.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCode;
                        dtEmailDepartment = dbManager.ExecuteDataTable(cmdGetEmailComplain, "cmdGetEmailComplain");
                        dsEmail.Tables.Add(dtEmailDepartment.Copy());
                    }

                    //User
                    if (UserID != 0)
                    {
                        cmdGetEmailUser.Parameters["I_SUID"].Value = UserID;
                        dtUser = dbManager.ExecuteDataTable(cmdGetEmailUser, "cmdGetEmailUser");
                        dsEmail.Tables.Add(dtUser.Copy());
                    }

                    //Vendor
                    if (!string.Equals(VendorID, string.Empty))
                    {
                        cmdGetEmailVendor.Parameters["I_SVENDORID"].Value = VendorID;
                        dtVendor = dbManager.ExecuteDataTable(cmdGetEmailVendor, "cmdGetEmailVendor");
                        dsEmail.Tables.Add(dtVendor.Copy());
                    }
                }
                else
                {
                    if (!string.Equals(DeliveryDepartmentCodeAdmin, string.Empty))
                    {
                        cmdGetEmailComplainAdmin.CommandText += " AND SPOSITION LIKE '%ผจ.ขปน.%'";

                        cmdGetEmailComplainAdmin.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCodeAdmin;
                        dtEmailDepartmentAdmin = dbManager.ExecuteDataTable(cmdGetEmailComplainAdmin, "cmdGetEmailComplainAdmin");
                        dsEmail.Tables.Add(dtEmailDepartmentAdmin.Copy());
                    }

                    //ผจ. ปง
                    if (!string.Equals(DeliveryDepartmentCodeAdmin2, string.Empty))
                    {
                        cmdGetEmailComplainAdmin2.CommandText += " AND SPOSITION LIKE '%ผจ.ปง.%' AND SEMAIL = '%choosak.a@pttplc.com%' ";

                        cmdGetEmailComplainAdmin2.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCodeAdmin2;
                        dtEmailDepartmentAdmin2 = dbManager.ExecuteDataTable(cmdGetEmailComplainAdmin2, "cmdGetEmailComplainAdmin2");
                        dsEmail.Tables.Add(dtEmailDepartmentAdmin2.Copy());
                    }

                    //รข
                    if (!string.Equals(DeliveryDepartmentCode, string.Empty))
                    {
                        if (IsAdmin)
                            cmdGetEmailComplain.CommandText += " AND IS_CONTRACT IN (1, 2)";

                        cmdGetEmailComplain.CommandText += " AND SEMAIL IN ('suphakit.k@pttplc.com','nut.t@pttplc.com','sake.k@pttplc.com','thanyavit.k@pttplc.com','nataporn.c@pttplc.com','patrapol.n@pttplc.com','thrathorn.v@pttplc.com')";
                        cmdGetEmailComplain.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCode;
                        dtEmailDepartment = dbManager.ExecuteDataTable(cmdGetEmailComplain, "cmdGetEmailComplain");
                        dsEmail.Tables.Add(dtEmailDepartment.Copy());
                    }

                    //User
                    if (UserID != 0)
                    {
                        cmdGetEmailUser.Parameters["I_SUID"].Value = UserID;
                        dtUser = dbManager.ExecuteDataTable(cmdGetEmailUser, "cmdGetEmailUser");
                        dsEmail.Tables.Add(dtUser.Copy());
                    }

                    //Vendor
                    if (!string.Equals(VendorID, string.Empty))
                    {
                        cmdGetEmailVendor.Parameters["I_SVENDORID"].Value = VendorID;
                        dtVendor = dbManager.ExecuteDataTable(cmdGetEmailVendor, "cmdGetEmailVendor");
                        dsEmail.Tables.Add(dtVendor.Copy());
                    }
                }

                return dsEmail;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdGetEmailComplainAdmin.CommandText = Tmp1;
                cmdGetEmailComplain.CommandText = Tmp2;
                cmdGetEmailComplainAdmin2.CommandText = Tmp3;
            }
        }

        public DataSet GetEmailComplainSaveTab3DAL(string VendorID, string DeliveryDepartmentCode, int UserID)
        {
            try
            {
                dbManager.Open();
                DataSet dsEmail = new DataSet();
                DataTable dtEmailVendor = new DataTable();
                DataTable dtEmailComplainAdmin = new DataTable();           //รข
                DataTable dtUser = new DataTable();                         //User Create Doc

                //Vendor
                cmdGetEmailVendor.Parameters["I_SVENDORID"].Value = VendorID;
                dtEmailVendor = dbManager.ExecuteDataTable(cmdGetEmailVendor, "cmdGetEmailVendor");

                //รข
                cmdGetEmailComplain.Parameters["I_SVENDORID"].Value = DeliveryDepartmentCode;
                dtEmailComplainAdmin = dbManager.ExecuteDataTable(cmdGetEmailComplain, "cmdGetEmailComplainAdmin");

                //User
                cmdGetEmailUser.Parameters["I_SUID"].Value = UserID;
                dtUser = dbManager.ExecuteDataTable(cmdGetEmailUser, "cmdGetEmailUser");

                dsEmail.Tables.Add(dtEmailVendor.Copy());
                dsEmail.Tables.Add(dtEmailComplainAdmin.Copy());
                dsEmail.Tables.Add(dtUser.Copy());

                return dsEmail;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectDuplicateTotalCarDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdSelectDuplicateTotalCar.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdSelectDuplicateTotalCar, "cmdSelectDuplicateTotalCar");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectDuplicateTotalCarPartialDAL(string DocID, string Condition)
        {
            string Query = cmdSelectDuplicateTotalCarPartial.CommandText;

            try
            {
                dbManager.Open();
                cmdSelectDuplicateTotalCarPartial.CommandText += Condition;
                cmdSelectDuplicateTotalCarPartial.CommandText += " GROUP BY TO_CHAR(DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') ORDER BY TO_CHAR(DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH')";

                cmdSelectDuplicateTotalCarPartial.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdSelectDuplicateTotalCarPartial, "cmdSelectDuplicateTotalCarPartial");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdSelectDuplicateTotalCarPartial.CommandText = Query;
            }
        }

        public DataTable ComplainEmailRequestDocDAL(string DocID)
        {
            try
            {
                dbManager.Open();
                cmdComplainEmailRequestDoc.CommandText = @"SELECT DOC_ID AS DOCID
                                                          , NVL(FN_GET_COMPLAIN_LIST('CARHEAD_LIST', :I_DOC_ID), '-') AS CAR
                                                          , NVL(FN_GET_COMPLAIN_LIST('DDELIVERY_LIST', :I_DOC_ID), '-') AS DELIVERY_DATE
                                                          , TVENDOR.SABBREVIATION AS VENDOR
                                                          , TCONTRACT.SCONTRACTNO AS CONTRACT
                                                          , NVL(FN_GET_COMPLAIN_LIST('WAREHOUSE', :I_DOC_ID), '-') AS WAREHOUSE
                                                          , NVL(FN_GET_COMPLAIN_LIST('COMPLAINTYPE_LIST', :I_DOC_ID), '-') AS COMPLAIN
                                                          , SCOMPLAINDIVISION AS COMPLAIN_DEPARTMENT
                                                          , CASE TUSER.CGROUP WHEN '0' THEN TVENDOR.SABBREVIATION WHEN '1' THEN TUNIT.UNITNAME WHEN '2' THEN TTERMINAL.SABBREVIATION END CREATE_DEPARTMENT
                                                          , TUSER.SEMAIL AS EMAIL_CREATE
                                                          , NVL(TEMPLOYEE_SAP.FNAME || ' ' || NVL(TEMPLOYEE_SAP.LNAME, ''), '-') AS DRIVER
                                                          , TO_CHAR(TCOMPLAIN.DCREATE, 'DD/MM/YYYY') AS LOCK_DATE
                                                          , TCOMPLAIN.SVENDORID AS VENDORID
                                                          , TCOMPLAIN.CC_EMAIL AS CCEMAIL
                                                    FROM TCOMPLAIN LEFT JOIN TVENDOR ON TCOMPLAIN.SVENDORID = TVENDOR.SVENDORID
                                                                   LEFT JOIN TCONTRACT ON TCOMPLAIN.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                                   LEFT JOIN TUSER ON TCOMPLAIN.SCREATE = TUSER.SUID
                                                                   LEFT JOIN TTERMINAL ON TUSER.SVENDORID = TTERMINAL.STERMINALID
                                                                   LEFT JOIN TUNIT ON TUSER.SVENDORID = TUNIT.UNITCODE
                                                                   LEFT JOIN TEMPLOYEE_SAP ON TCOMPLAIN.SPERSONALNO = TEMPLOYEE_SAP.SEMPLOYEEID
                                                    WHERE DOC_ID =:I_DOC_ID";

                cmdComplainEmailRequestDoc.Parameters["I_DOC_ID"].Value = DocID;

                return dbManager.ExecuteDataTable(cmdComplainEmailRequestDoc, "cmdComplainEmailRequestDoc");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void OTPSaveDAL(int OTPType, string OTPCode, int UserID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdOTPSave.Parameters["I_OTP_TYPE"].Value = OTPType;
                cmdOTPSave.Parameters["I_OTP_CODE"].Value = OTPCode;
                cmdOTPSave.Parameters["I_CREATE_BY"].Value = UserID;
                cmdOTPSave.Parameters["I_ISACTIVE"].Value = 1;

                cmdOTPSave.CommandText = @"INSERT INTO T_OTP (OTP_CODE, OTP_TYPE, ISACTIVE, CREATE_BY)
                                           VALUES (:I_OTP_CODE, :I_OTP_TYPE, :I_ISACTIVE, :I_CREATE_BY)";

                dbManager.ExecuteNonQuery(cmdOTPSave);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable OTPCheckDAL(string OTP, int OTPType)
        {
            try
            {
                dbManager.Open();
                cmdOTPCheck.CommandText = "SELECT * FROM T_OTP WHERE OTP_CODE =: I_OTP_CODE AND ISACTIVE = 1 AND OTP_TYPE =:I_OTP_TYPE";

                cmdOTPCheck.Parameters["I_OTP_CODE"].Value = OTP;
                cmdOTPCheck.Parameters["I_OTP_TYPE"].Value = OTPType;
                return dbManager.ExecuteDataTable(cmdOTPCheck, "cmdOTPCheck");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable AppealSelectDAL(string DocID)
        {
            try
            {
                //                cmdAppealSelect.CommandText = @"SELECT  SUM(NVL(TAPPEAL.NPOINT, 0) - NVL(TREDUCEPOINT.NPOINT, 0)) AS TOTAL_POINT,
                //                                                        SUM(NVL(TAPPEAL.COST, 0) - NVL(TREDUCEPOINT.COST, 0)) AS TOTAL_COST,
                //                                                        SUM(NVL(TAPPEAL.DISABLE_DRIVER, 0) - NVL(TREDUCEPOINT.DISABLE_DRIVER, 0)) AS TOTAL_DISABLE
                //                                                FROM TREDUCEPOINT INNER JOIN TAPPEAL ON TREDUCEPOINT.NREDUCEID = TAPPEAL.NREDUCEID
                //                                                WHERE 1=1-- TREDUCEPOINT.CACTIVE = 1
                //                                                AND TAPPEAL.CSTATUS IN ('3', '6')
                //                                                AND TREDUCEPOINT.SREFERENCEID =:I_DOC_ID";
                dbManager.Open();
                cmdAppealSelect.CommandText = @"SELECT  SUM(NVL(TAPPEAL.NPOINT, 0) - NVL(TREDUCEPOINT.NPOINT, 0)) AS TOTAL_POINT,
                                                        SUM(NVL(TAPPEAL.COST, 0) - NVL(TREDUCEPOINT.COST, 0)) AS TOTAL_COST,
                                                        SUM(NVL(TAPPEAL.DISABLE_DRIVER, 0) - NVL(TREDUCEPOINT.DISABLE_DRIVER, 0)) AS TOTAL_DISABLE,
                                                        BLACKLIST, COST_OTHER, OIL_LOSE
                                                FROM TREDUCEPOINT INNER JOIN TAPPEAL ON TREDUCEPOINT.NREDUCEID = TAPPEAL.NREDUCEID
                                                                  INNER JOIN TCOMPLAIN_SCORE_A ON TREDUCEPOINT.SREFERENCEID = TCOMPLAIN_SCORE_A.DOC_ID AND TCOMPLAIN_SCORE_A.ISACTIVE = 1
                                                WHERE 1=1-- TREDUCEPOINT.CACTIVE = 1
                                                AND TAPPEAL.CSTATUS IN ('3', '6')
                                                AND TREDUCEPOINT.SREFERENCEID =:I_DOC_ID
                                                GROUP BY TCOMPLAIN_SCORE_A.BLACKLIST, COST_OTHER, OIL_LOSE, TCOMPLAIN_SCORE_A.UPDATE_DATETIME
                                                ORDER BY TCOMPLAIN_SCORE_A.UPDATE_DATETIME DESC";

                cmdAppealSelect.Parameters["I_DOC_ID"].Value = DocID;
                return dbManager.ExecuteDataTable(cmdAppealSelect, "cmdAppealSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectScorePointDAL(string Point)
        {
            try
            {
                dbManager.Open();
                cmdSelectScorePoint.CommandText = @"SELECT ROW_NUMBER () OVER (ORDER BY m.NID) AS ID1,m.NID,m.NSTART,m.NEND,m.NSCORE,m.DUPDATE,U.SFIRSTNAME || ' ' || U.SLASTNAME AS FULLNAME
                                                  FROM LSTMULTIPLEIMPACT m LEFT JOIN TUSER u ON m.SUPDATE = U.SUID
                                                  WHERE NSTART <= :I_POINT AND NEND >= :I_POINT";

                cmdSelectScorePoint.Parameters["I_POINT"].Value = Point;

                return dbManager.ExecuteDataTable(cmdSelectScorePoint, "cmdSelectScorePoint");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UpdateDriverStatusDAL(string SEMPLOYEEID, string CACTIVE, string DRVSTATUS)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdStatusEmployee.CommandText = @"UPDATE TEMPLOYEE
                                                  SET CACTIVE =:I_CACITVE
                                                  WHERE SEMPLOYEEID =:I_SEMPLOYEEID";
                cmdStatusEmployee.Parameters["I_SEMPLOYEEID"].Value = SEMPLOYEEID;
                cmdStatusEmployee.Parameters["I_CACITVE"].Value = CACTIVE;
                dbManager.ExecuteNonQuery(cmdStatusEmployee);

                cmdStatusEmployeeSAP.CommandText = @"UPDATE TEMPLOYEE_SAP
                                                     SET DRVSTATUS =:I_DRVSTATUS
                                                     WHERE SEMPLOYEEID =:I_SEMPLOYEEID";
                cmdStatusEmployeeSAP.Parameters["I_SEMPLOYEEID"].Value = SEMPLOYEEID;
                cmdStatusEmployeeSAP.Parameters["I_DRVSTATUS"].Value = DRVSTATUS;
                dbManager.ExecuteNonQuery(cmdStatusEmployeeSAP);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        public DataTable SearchDateEndLockDriverDAL(string Condition)
        {
            string Query = cmdSearchByDO.CommandText;
            try
            {
                dbManager.Open();
                cmdDateEndLockDriverSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdDateEndLockDriverSelect, "cmdDateEndLockDriverSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdDateEndLockDriverSelect.CommandText = Query;
            }
        }

        public DataTable SearchByDODAL(string Condition)
        {
            string Query = cmdSearchByDO.CommandText;
            try
            {
                dbManager.Open();
                cmdSearchByDO.CommandText += Condition;

                cmdSearchByDO.CommandText = @"SELECT TPLANSCHEDULELIST.SDELIVERYNO
                                              , TVENDOR.SVENDORID
                                              , TVENDOR.SABBREVIATION AS VENDOR
                                              , TCONTRACT.SCONTRACTID
                                              , TCONTRACT.SCONTRACTNO
                                              , TCONTRACT_TRUCK.STRUCKID
                                              , TPLANSCHEDULE.SHEADREGISTERNO
                                              , TPLANSCHEDULE.STRAILERREGISTERNO
                                              , TEMPLOYEE_SAP.SEMPLOYEEID AS EMPLOYEEID
                                              , TTERMINAL.STERMINALID AS WAREHOUSE_ID_FROM
                                              , TTERMINAL.SABBREVIATION AS WAREHOUSE_FROM
                                              , TCUSTOMER.CUST_ABBR AS WAREHOUSE_TO
                                              , TO_CHAR(TPLANSCHEDULE.DDELIVERY, 'dd/mm/yyyy', 'NLS_DATE_LANGUAGE = ENGLISH') AS DDELIVERY
                                        FROM TCONTRACT_TRUCK LEFT JOIN TTRUCK ON TCONTRACT_TRUCK.STRUCKID = TTRUCK.STRUCKID 
                                                             LEFT JOIN TPLANSCHEDULE ON TTRUCK.SHEADREGISTERNO = TPLANSCHEDULE.SHEADREGISTERNO
                                                             LEFT JOIN TPLANSCHEDULELIST ON TPLANSCHEDULE.NPLANID = TPLANSCHEDULELIST.NPLANID
                                                             LEFT JOIN TVENDOR ON TPLANSCHEDULE.SVENDORID = TVENDOR.SVENDORID
                                                             LEFT JOIN TCONTRACT ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                             LEFT JOIN TTERMINAL ON TPLANSCHEDULE.STERMINALID = TTERMINAL.STERMINALID
                                                             LEFT JOIN TEMPLOYEE_SAP ON TPLANSCHEDULE.SEMPLOYEEID = TEMPLOYEE_SAP.EMPSAPID
                                                             LEFT JOIN TCUSTOMER ON TPLANSCHEDULELIST.SSHIPTO = TCUSTOMER.SHIP_TO
                                        WHERE 1=1 AND TVENDOR.INUSE = '1'
                                        AND TCONTRACT.CACTIVE = 'Y'
                                        AND TPLANSCHEDULELIST.SDELIVERYNO IS NOT NULL";

                cmdSearchByDO.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSearchByDO, "cmdSearchByDO");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdSearchByDO.CommandText = Query;
            }
        }

        public DataTable SentencerSelctDAL(int I_SENTENCER_TYPE_ID)
        {
            try
            {
                DataTable dt = new DataTable();
                dbManager.Open();

                cmdSentencerSelect.CommandText = @"SELECT DISTINCT TSENTENCER.SSENTENCERCODE, TSENTENCER.SSENTENCERNAME
                                                   FROM TSENTENCER
                                                   WHERE TSENTENCER.CEXPIRETERM = 0
                                                   AND TSENTENCER.SENTENCER_TYPE_ID =:I_SENTENCER_TYPE_ID
                                                   ORDER BY TSENTENCER.SSENTENCERNAME";

                cmdSentencerSelect.Parameters["I_SENTENCER_TYPE_ID"].Value = I_SENTENCER_TYPE_ID;
                dt = dbManager.ExecuteDataTable(cmdSentencerSelect, "cmdSentencerSelect");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SentencerSelctDetailDAL(string I_SSENTENCERCODE)
        {
            try
            {
                DataTable dt = new DataTable();
                dbManager.Open();

                cmdSentencerSelectDetail.CommandText = @"SELECT NLIST, CODE, FNAME, SPOSITION, SJOB, CACTIVE
                                                         FROM TSENTENCER
                                                         WHERE 1=1
                                                         AND SSENTENCERCODE =:I_SSENTENCERCODE";

                cmdSentencerSelectDetail.Parameters["I_SSENTENCERCODE"].Value = I_SSENTENCERCODE;
                dt = dbManager.ExecuteDataTable(cmdSentencerSelectDetail, "cmdSentencerSelectDetail");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable GetDriverConfictDAL()
        {
            try
            {
                dbManager.Open();
                cmdGetDriverConfict.CommandText = @"SELECT * FROM VW_AUTO_DRIVER_UPDATE";

                return dbManager.ExecuteDataTable(cmdGetDriverConfict, "cmdGetDriverConfict");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static ComplainDAL _instance;
        public static ComplainDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new ComplainDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}