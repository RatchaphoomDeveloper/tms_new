﻿namespace TMS_DAL.Transaction.Complain
{
    partial class ComplainDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComplainDAL));
            this.cmdComplainExport = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainImportFile = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainRequestFile = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdOutboundSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainListSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainRequireField = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectHeader = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainCanEdit = new System.Data.OracleClient.OracleCommand();
            this.cmdContractSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdOutboundSelect2 = new System.Data.OracleClient.OracleCommand();
            this.cmdEmployeeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorSelect = new System.Data.OracleClient.OracleCommand();
            this.oracleCommand1 = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdPersonalSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdLoginSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainAttachInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainCancelDoc = new System.Data.OracleClient.OracleCommand();
            this.cmdScoreSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCusScoreTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainChageStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdPersonalDetailSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdLockDriverInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateLog = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainLockDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateSelect2 = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainExportFinal = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainImportSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdExportScoreSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateUrgent = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateDriverStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdChangeStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdGetEmailComplain = new System.Data.OracleClient.OracleCommand();
            this.cmdGetEmailVendor = new System.Data.OracleClient.OracleCommand();
            this.cmdGetEmailUser = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectDuplicateTotalCar = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainEmailRequestDoc = new System.Data.OracleClient.OracleCommand();
            this.cmdGetEmailComplainAdmin = new System.Data.OracleClient.OracleCommand();
            this.cmdCancelDocUpdateDisable = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectDriverDisable = new System.Data.OracleClient.OracleCommand();
            this.cmdGetEmailComplainAdmin2 = new System.Data.OracleClient.OracleCommand();
            this.cmdOTPSave = new System.Data.OracleClient.OracleCommand();
            this.cmdOTPCheck = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainOTPSave = new System.Data.OracleClient.OracleCommand();
            this.cmdAppealSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectDuplicateTotalCarPartial = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectScorePoint = new System.Data.OracleClient.OracleCommand();
            this.cmdStatusEmployee = new System.Data.OracleClient.OracleCommand();
            this.cmdStatusEmployeeSAP = new System.Data.OracleClient.OracleCommand();
            this.cmdSearchByDO = new System.Data.OracleClient.OracleCommand();
            this.cmdSentencerSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdSentencerSelectDetail = new System.Data.OracleClient.OracleCommand();
            this.cmdGetDriverConfict = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateTMS = new System.Data.OracleClient.OracleCommand();
            this.cmdDriverUpdateSAP = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckOutContractSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckWorkgroup = new System.Data.OracleClient.OracleCommand();
            this.cmdTblReqTemp = new System.Data.OracleClient.OracleCommand();
            this.cmdSupplainSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainAddDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectAddDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainUpdateDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainUpdateOldDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainDelDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainLockDriverNew = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainChangeStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainChangStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainSelectHistory = new System.Data.OracleClient.OracleCommand();
            this.cmdGenerateText = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainLockDriverSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUnlockDriverSave = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainUploadLate = new System.Data.OracleClient.OracleCommand();
            this.cmdDateEndLockDriverSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdComplainExport
            // 
            this.cmdComplainExport.CommandText = resources.GetString("cmdComplainExport.CommandText");
            this.cmdComplainExport.Connection = this.OracleConn;
            this.cmdComplainExport.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SSERVICEID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdComplainImportFile
            // 
            this.cmdComplainImportFile.CommandText = "SELECT ROW_NUMBER() OVER (PARTITION BY SSERVICEID ORDER BY SSERVICEID) AS ROW_ID," +
    " SDOCUMENT FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = :SSERVICEID";
            this.cmdComplainImportFile.Connection = this.OracleConn;
            this.cmdComplainImportFile.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SSERVICEID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdComplainRequestFile
            // 
            this.cmdComplainRequestFile.CommandText = resources.GetString("cmdComplainRequestFile.CommandText");
            this.cmdComplainRequestFile.Connection = this.OracleConn;
            // 
            // cmdComplainInsert
            // 
            this.cmdComplainInsert.CommandText = "USP_T_COMPLAIN_INSERT";
            this.cmdComplainInsert.Connection = this.OracleConn;
            this.cmdComplainInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_SDETAIL", System.Data.OracleClient.OracleType.VarChar, 2000),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINFNAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINLNAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINDIVISION", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_DDATECOMPLAIN", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_TTIMECOMPLAIN", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_CTYPEPERSON", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCOMPLAINBY", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_HEADER", System.Data.OracleClient.OracleType.Clob),
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdOutboundSelect
            // 
            this.cmdOutboundSelect.CommandText = resources.GetString("cmdOutboundSelect.CommandText");
            this.cmdOutboundSelect.Connection = this.OracleConn;
            // 
            // cmdComplainListSelect
            // 
            this.cmdComplainListSelect.CommandText = resources.GetString("cmdComplainListSelect.CommandText");
            this.cmdComplainListSelect.Connection = this.OracleConn;
            this.cmdComplainListSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TOPIC_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdComplainSelect
            // 
            this.cmdComplainSelect.CommandText = resources.GetString("cmdComplainSelect.CommandText");
            this.cmdComplainSelect.Connection = this.OracleConn;
            // 
            // cmdComplainRequireField
            // 
            this.cmdComplainRequireField.CommandText = resources.GetString("cmdComplainRequireField.CommandText");
            this.cmdComplainRequireField.Connection = this.OracleConn;
            this.cmdComplainRequireField.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("COMPLAIN_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FIELD_TYPE", System.Data.OracleClient.OracleType.VarChar, 25)});
            // 
            // cmdComplainSelectHeader
            // 
            this.cmdComplainSelectHeader.CommandText = resources.GetString("cmdComplainSelectHeader.CommandText");
            this.cmdComplainSelectHeader.Connection = this.OracleConn;
            // 
            // cmdComplainCanEdit
            // 
            this.cmdComplainCanEdit.CommandText = resources.GetString("cmdComplainCanEdit.CommandText");
            this.cmdComplainCanEdit.Connection = this.OracleConn;
            this.cmdComplainCanEdit.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdContractSelect
            // 
            this.cmdContractSelect.CommandText = resources.GetString("cmdContractSelect.CommandText");
            this.cmdContractSelect.Connection = this.OracleConn;
            // 
            // cmdOutboundSelect2
            // 
            this.cmdOutboundSelect2.CommandText = resources.GetString("cmdOutboundSelect2.CommandText");
            this.cmdOutboundSelect2.Connection = this.OracleConn;
            this.cmdOutboundSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TOPIC_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdEmployeeSelect
            // 
            this.cmdEmployeeSelect.CommandText = resources.GetString("cmdEmployeeSelect.CommandText");
            this.cmdEmployeeSelect.Connection = this.OracleConn;
            // 
            // cmdVendorSelect
            // 
            this.cmdVendorSelect.CommandText = resources.GetString("cmdVendorSelect.CommandText");
            this.cmdVendorSelect.Connection = this.OracleConn;
            this.cmdVendorSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TOPIC_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // oracleCommand1
            // 
            this.oracleCommand1.CommandText = resources.GetString("oracleCommand1.CommandText");
            this.oracleCommand1.Connection = this.OracleConn;
            // 
            // cmdTruckSelect
            // 
            this.cmdTruckSelect.CommandText = resources.GetString("cmdTruckSelect.CommandText");
            this.cmdTruckSelect.Connection = this.OracleConn;
            this.cmdTruckSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TOPIC_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdPersonalSelect
            // 
            this.cmdPersonalSelect.CommandText = resources.GetString("cmdPersonalSelect.CommandText");
            this.cmdPersonalSelect.Connection = this.OracleConn;
            // 
            // cmdLoginSelect
            // 
            this.cmdLoginSelect.CommandText = resources.GetString("cmdLoginSelect.CommandText");
            this.cmdLoginSelect.Connection = this.OracleConn;
            // 
            // cmdComplainAttachInsert
            // 
            this.cmdComplainAttachInsert.CommandText = resources.GetString("cmdComplainAttachInsert.CommandText");
            this.cmdComplainAttachInsert.Connection = this.OracleConn;
            this.cmdComplainAttachInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500)});
            // 
            // cmdComplainCancelDoc
            // 
            this.cmdComplainCancelDoc.CommandText = "UPDATE TCOMPLAIN SET DOC_STATUS_ID = (SELECT DOC_STATUS_ID FROM M_DOC_STATUS WHER" +
    "E SPECIAL_STATUS = \'2\') WHERE DOC_ID =: I_DOC_ID";
            this.cmdComplainCancelDoc.Connection = this.OracleConn;
            this.cmdComplainCancelDoc.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdScoreSelect
            // 
            this.cmdScoreSelect.CommandText = resources.GetString("cmdScoreSelect.CommandText");
            this.cmdScoreSelect.Connection = this.OracleConn;
            // 
            // cmdCusScoreTypeSelect
            // 
            this.cmdCusScoreTypeSelect.CommandText = "SELECT CUSSCORE_TYPE_ID, CUSSCORE_TYPE_NAME, VALUE_TYPE FROM M_CUSSCORE_TYPE WHER" +
    "E ISACTIVE = 1";
            this.cmdCusScoreTypeSelect.Connection = this.OracleConn;
            this.cmdCusScoreTypeSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SSERVICEID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdComplainChageStatus
            // 
            this.cmdComplainChageStatus.CommandText = "UPDATE TCOMPLAIN SET DOC_STATUS_ID =: I_DOC_STATUS_ID, SUPDATE =: I_UPDATE_BY WHE" +
    "RE DOC_ID =: I_DOC_ID";
            this.cmdComplainChageStatus.Connection = this.OracleConn;
            this.cmdComplainChageStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdPersonalDetailSelect
            // 
            this.cmdPersonalDetailSelect.CommandText = resources.GetString("cmdPersonalDetailSelect.CommandText");
            this.cmdPersonalDetailSelect.Connection = this.OracleConn;
            this.cmdPersonalDetailSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdLockDriverInsert
            // 
            this.cmdLockDriverInsert.CommandText = resources.GetString("cmdLockDriverInsert.CommandText");
            this.cmdLockDriverInsert.Connection = this.OracleConn;
            this.cmdLockDriverInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 25),
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_DATE_START", System.Data.OracleClient.OracleType.NVarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DATE_END", System.Data.OracleClient.OracleType.NVarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_DRIVER_STATUS", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdDriverUpdateSelect
            // 
            this.cmdDriverUpdateSelect.CommandText = resources.GetString("cmdDriverUpdateSelect.CommandText");
            this.cmdDriverUpdateSelect.Connection = this.OracleConn;
            this.cmdDriverUpdateSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdDriverUpdateLog
            // 
            this.cmdDriverUpdateLog.CommandText = "UPDATE TCOMPLAIN_LOCK_DRIVER SET STATUS = 1 WHERE DOC_ID =: I_DOC_ID AND ID =: I_" +
    "ID";
            this.cmdDriverUpdateLog.Connection = this.OracleConn;
            this.cmdDriverUpdateLog.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_ID", System.Data.OracleClient.OracleType.Number, 20),
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdComplainLockDriver
            // 
            this.cmdComplainLockDriver.CommandText = "USP_T_COMPLAIN_LOCK_DRIVER";
            this.cmdComplainLockDriver.Connection = this.OracleConn;
            this.cmdComplainLockDriver.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_TOTAL", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DETAIL", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_BLACKLIST", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_DRIVER", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdDriverUpdateSelect2
            // 
            this.cmdDriverUpdateSelect2.CommandText = "SELECT ID, DOC_ID, SEMPLOYEEID, \'0\' AS CHANGE_TO FROM TCOMPLAIN_LOCK_DRIVER WHERE" +
    " DOC_ID =:I_DOC_ID AND STATUS = 0 AND IS_UNLOCK = 0";
            this.cmdDriverUpdateSelect2.Connection = this.OracleConn;
            this.cmdDriverUpdateSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdComplainExportFinal
            // 
            this.cmdComplainExportFinal.Connection = this.OracleConn;
            this.cmdComplainExportFinal.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SSERVICEID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdComplainImportSelect
            // 
            this.cmdComplainImportSelect.CommandText = "SELECT DISTINCT UPLOAD_NAME FROM F_UPLOAD INNER JOIN M_UPLOAD_TYPE ON F_UPLOAD.UP" +
    "LOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID WHERE REF_STR =: I_DOC_ID";
            this.cmdComplainImportSelect.Connection = this.OracleConn;
            this.cmdComplainImportSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdExportScoreSelect
            // 
            this.cmdExportScoreSelect.Connection = this.OracleConn;
            this.cmdExportScoreSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdUpdateUrgent
            // 
            this.cmdUpdateUrgent.CommandText = "UPDATE TCOMPLAIN SET IS_URGENT =: I_IS_URGENT WHERE SSERVICEID =: I_SSERVICEID";
            this.cmdUpdateUrgent.Connection = this.OracleConn;
            this.cmdUpdateUrgent.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_IS_URGENT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SSERVICEID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdUpdateDriverStatus
            // 
            this.cmdUpdateDriverStatus.CommandText = "UPDATE TEMPLOYEE SET CACTIVE=: I_CACTIVE, SETTLETO = NULL WHERE SEMPLOYEEID =: I_" +
    "SEMPLOYEEID";
            this.cmdUpdateDriverStatus.Connection = this.OracleConn;
            this.cmdUpdateDriverStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_CACTIVE", System.Data.OracleClient.OracleType.VarChar, 28)});
            // 
            // cmdChangeStatus
            // 
            this.cmdChangeStatus.CommandText = "UPDATE TCOMPLAIN SET DOC_STATUS_ID =: I_DOC_STATUS_ID, SUPDATE =: I_SUPDATE, DUPD" +
    "ATE = SYSDATE WHERE DOC_ID =: I_DOC_ID";
            this.cmdChangeStatus.Connection = this.OracleConn;
            this.cmdChangeStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SUPDATE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetEmailComplain
            // 
            this.cmdGetEmailComplain.CommandText = "SELECT SEMAIL AS EMAIL FROM TUSER WHERE SVENDORID =:I_SVENDORID AND SPOSITION <> " +
    "\'ผจ.ขปน.\' AND CACTIVE = \'1\'";
            this.cmdGetEmailComplain.Connection = this.OracleConn;
            this.cmdGetEmailComplain.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdGetEmailVendor
            // 
            this.cmdGetEmailVendor.CommandText = "SELECT EMAIL FROM TVENDOR WHERE SVENDORID =: I_SVENDORID AND CACTIVE = \'1\'";
            this.cmdGetEmailVendor.Connection = this.OracleConn;
            this.cmdGetEmailVendor.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdGetEmailUser
            // 
            this.cmdGetEmailUser.CommandText = "SELECT SEMAIL AS EMAIL FROM TUSER WHERE SUID =: I_SUID AND CACTIVE = \'1\'";
            this.cmdGetEmailUser.Connection = this.OracleConn;
            this.cmdGetEmailUser.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SUID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdSelectDuplicateTotalCar
            // 
            this.cmdSelectDuplicateTotalCar.CommandText = resources.GetString("cmdSelectDuplicateTotalCar.CommandText");
            this.cmdSelectDuplicateTotalCar.Connection = this.OracleConn;
            this.cmdSelectDuplicateTotalCar.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdComplainEmailRequestDoc
            // 
            this.cmdComplainEmailRequestDoc.Connection = this.OracleConn;
            this.cmdComplainEmailRequestDoc.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdGetEmailComplainAdmin
            // 
            this.cmdGetEmailComplainAdmin.CommandText = "SELECT SEMAIL AS EMAIL FROM TUSER WHERE SVENDORID =: I_SVENDORID AND CACTIVE = \'1" +
    "\'";
            this.cmdGetEmailComplainAdmin.Connection = this.OracleConn;
            this.cmdGetEmailComplainAdmin.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdCancelDocUpdateDisable
            // 
            this.cmdCancelDocUpdateDisable.Connection = this.OracleConn;
            this.cmdCancelDocUpdateDisable.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_TEXT", System.Data.OracleClient.OracleType.VarChar, 500)});
            // 
            // cmdSelectDriverDisable
            // 
            this.cmdSelectDriverDisable.Connection = this.OracleConn;
            this.cmdSelectDriverDisable.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetEmailComplainAdmin2
            // 
            this.cmdGetEmailComplainAdmin2.CommandText = "SELECT SEMAIL AS EMAIL FROM TUSER WHERE SVENDORID =: I_SVENDORID AND CACTIVE = \'1" +
    "\'";
            this.cmdGetEmailComplainAdmin2.Connection = this.OracleConn;
            this.cmdGetEmailComplainAdmin2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdOTPSave
            // 
            this.cmdOTPSave.Connection = this.OracleConn;
            this.cmdOTPSave.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_OTP_TYPE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_OTP_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdOTPCheck
            // 
            this.cmdOTPCheck.Connection = this.OracleConn;
            this.cmdOTPCheck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_OTP_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_OTP_TYPE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdComplainOTPSave
            // 
            this.cmdComplainOTPSave.Connection = this.OracleConn;
            this.cmdComplainOTPSave.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_OTP_TYPE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_OTP_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_USER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REF_DOC_NO", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdAppealSelect
            // 
            this.cmdAppealSelect.Connection = this.OracleConn;
            this.cmdAppealSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdSelectDuplicateTotalCarPartial
            // 
            this.cmdSelectDuplicateTotalCarPartial.CommandText = "SELECT DISTINCT TO_CHAR(DDELIVERY, \'DD/MM/YYYY\', \'NLS_DATE_LANGUAGE = ENGLISH\') A" +
    "S DELIVERY_DATE, SUM(TOTAL_CAR) AS TOTAL_CAR FROM TCOMPLAIN WHERE DOC_ID =:I_DOC" +
    "_ID";
            this.cmdSelectDuplicateTotalCarPartial.Connection = this.OracleConn;
            this.cmdSelectDuplicateTotalCarPartial.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdSelectScorePoint
            // 
            this.cmdSelectScorePoint.Connection = this.OracleConn;
            this.cmdSelectScorePoint.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_POINT", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdStatusEmployee
            // 
            this.cmdStatusEmployee.Connection = this.OracleConn;
            this.cmdStatusEmployee.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CACITVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdStatusEmployeeSAP
            // 
            this.cmdStatusEmployeeSAP.Connection = this.OracleConn;
            this.cmdStatusEmployeeSAP.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DRVSTATUS", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdSearchByDO
            // 
            this.cmdSearchByDO.Connection = this.OracleConn;
            // 
            // cmdSentencerSelect
            // 
            this.cmdSentencerSelect.Connection = this.OracleConn;
            this.cmdSentencerSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SENTENCER_TYPE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdSentencerSelectDetail
            // 
            this.cmdSentencerSelectDetail.Connection = this.OracleConn;
            this.cmdSentencerSelectDetail.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SSENTENCERCODE", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdGetDriverConfict
            // 
            this.cmdGetDriverConfict.Connection = this.OracleConn;
            // 
            // cmdDriverUpdateTMS
            // 
            this.cmdDriverUpdateTMS.Connection = this.OracleConn;
            this.cmdDriverUpdateTMS.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_STATUSBAN", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdDriverUpdateSAP
            // 
            this.cmdDriverUpdateSAP.Connection = this.OracleConn;
            this.cmdDriverUpdateSAP.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_STATUS", System.Data.OracleClient.OracleType.VarChar, 10)});
            // 
            // cmdTruckOutContractSelect
            // 
            this.cmdTruckOutContractSelect.Connection = this.OracleConn;
            this.cmdTruckOutContractSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("TOPIC_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckWorkgroup
            // 
            this.cmdTruckWorkgroup.CommandText = "SELECT * FROM VW_M_TRUCK_WORKGROUP_SELECT WHERE 1 = 1 ";
            this.cmdTruckWorkgroup.Connection = this.OracleConn;
            // 
            // cmdTblReqTemp
            // 
            this.cmdTblReqTemp.CommandText = "USP_M_MV_GENERATE1";
            this.cmdTblReqTemp.Connection = this.OracleConn;
            this.cmdTblReqTemp.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("REQTYPE_ID1", System.Data.OracleClient.OracleType.VarChar, 2),
            new System.Data.OracleClient.OracleParameter("O_CUR_REQUEST_ID", System.Data.OracleClient.OracleType.Cursor, 50, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdSupplainSelect
            // 
            this.cmdSupplainSelect.CommandText = resources.GetString("cmdSupplainSelect.CommandText");
            this.cmdSupplainSelect.Connection = this.OracleConn;
            // 
            // cmdComplainAddDriver
            // 
            this.cmdComplainAddDriver.CommandText = "INSERT INTO TCOMPLAIN_ADD_DRIVER (DOC_ID, SDRIVERNO) VALUES (: I_DOC_ID, : I_DRIV" +
    "ERNO)";
            this.cmdComplainAddDriver.Connection = this.OracleConn;
            this.cmdComplainAddDriver.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_DRIVERNO", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdComplainSelectAddDriver
            // 
            this.cmdComplainSelectAddDriver.CommandText = "SELECT SDRIVERNO FROM TCOMPLAIN_ADD_DRIVER WHERE 1=1";
            this.cmdComplainSelectAddDriver.Connection = this.OracleConn;
            // 
            // cmdComplainSelectDriver
            // 
            this.cmdComplainSelectDriver.CommandText = resources.GetString("cmdComplainSelectDriver.CommandText");
            this.cmdComplainSelectDriver.Connection = this.OracleConn;
            // 
            // cmdComplainUpdateDriver
            // 
            this.cmdComplainUpdateDriver.CommandText = "UPDATE TCOMPLAIN_ADD_DRIVER SET IS_ACTIVE = : I_IS_ACTIVE,DAYS = : I_DAYS,REMARK " +
    "= : I_REMARK,BLACKLIST = : I_BLACKLIST WHERE DOC_ID = : I_DOC_ID AND SDRIVERNO =" +
    " : I_DRIVERNO";
            this.cmdComplainUpdateDriver.Connection = this.OracleConn;
            this.cmdComplainUpdateDriver.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_DRIVERNO", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DAYS", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_BLACKLIST", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdComplainUpdateOldDriver
            // 
            this.cmdComplainUpdateOldDriver.CommandText = "UPDATE TCOMPLAIN SET LOCK_DRIVER = : I_IS_ACTIVE,DAYS = : I_DAYS,REMARK = : I_REM" +
    "ARK,BLACKLIST = : I_BLACKLIST WHERE DOC_ID = : I_DOC_ID AND SPERSONALNO = : I_DR" +
    "IVERNO";
            this.cmdComplainUpdateOldDriver.Connection = this.OracleConn;
            this.cmdComplainUpdateOldDriver.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_DRIVERNO", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_DAYS", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_BLACKLIST", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdComplainDelDriver
            // 
            this.cmdComplainDelDriver.CommandText = "DELETE FROM TCOMPLAIN_ADD_DRIVER WHERE DOC_ID = : I_DOC_ID";
            this.cmdComplainDelDriver.Connection = this.OracleConn;
            this.cmdComplainDelDriver.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdComplainLockDriverNew
            // 
            this.cmdComplainLockDriverNew.CommandText = resources.GetString("cmdComplainLockDriverNew.CommandText");
            this.cmdComplainLockDriverNew.Connection = this.OracleConn;
            this.cmdComplainLockDriverNew.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_DRIVERNO", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_SCREATE", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_VENDORID", System.Data.OracleClient.OracleType.VarChar, 20)});
            // 
            // cmdComplainChangeStatus
            // 
            this.cmdComplainChangeStatus.CommandText = "UPDATE TCOMPLAIN SET DOC_STATUS_ID = 3,DUPDATE = SYSDATE WHERE DOC_ID =: I_DOC_ID" +
    " ";
            this.cmdComplainChangeStatus.Connection = this.OracleConn;
            this.cmdComplainChangeStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdComplainChangStatus
            // 
            this.cmdComplainChangStatus.CommandText = "UPDATE TCOMPLAIN SET DOC_STATUS_ID = 12 WHERE DOC_ID =: I_DOC_ID ";
            this.cmdComplainChangStatus.Connection = this.OracleConn;
            this.cmdComplainChangStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdComplainSelectHistory
            // 
            this.cmdComplainSelectHistory.CommandText = resources.GetString("cmdComplainSelectHistory.CommandText");
            this.cmdComplainSelectHistory.Connection = this.OracleConn;
            // 
            // cmdGenerateText
            // 
            this.cmdGenerateText.CommandText = "select";
            this.cmdGenerateText.Connection = this.OracleConn;
            this.cmdGenerateText.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SEMPLOYEEID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdComplainLockDriverSelect
            // 
            this.cmdComplainLockDriverSelect.CommandText = "SELECT * FROM VW_T_UNLOCKDRIVER_SELECT WHERE 1=1";
            this.cmdComplainLockDriverSelect.Connection = this.OracleConn;
            // 
            // cmdUnlockDriverSave
            // 
            this.cmdUnlockDriverSave.Connection = this.OracleConn;
            // 
            // cmdComplainUploadLate
            // 
            this.cmdComplainUploadLate.CommandText = "SELECT * FROM VW_T_COMPLAIN_UPLOAD_LATE WHERE 1=1";
            this.cmdComplainUploadLate.Connection = this.OracleConn;
            // 
            // cmdDateEndLockDriverSelect
            // 
            this.cmdDateEndLockDriverSelect.CommandText = "select * from VW_T_CHECKDATEEND_LOCKDRIVER where 1=1 ";
            this.cmdDateEndLockDriverSelect.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdComplainExport;
        private System.Data.OracleClient.OracleCommand cmdComplainImportFile;
        private System.Data.OracleClient.OracleCommand cmdComplainRequestFile;
        private System.Data.OracleClient.OracleCommand cmdComplainInsert;
        private System.Data.OracleClient.OracleCommand cmdOutboundSelect;
        private System.Data.OracleClient.OracleCommand cmdComplainListSelect;
        private System.Data.OracleClient.OracleCommand cmdComplainSelect;
        private System.Data.OracleClient.OracleCommand cmdComplainRequireField;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectHeader;
        private System.Data.OracleClient.OracleCommand cmdComplainCanEdit;
        private System.Data.OracleClient.OracleCommand cmdContractSelect;
        private System.Data.OracleClient.OracleCommand cmdOutboundSelect2;
        private System.Data.OracleClient.OracleCommand cmdEmployeeSelect;
        private System.Data.OracleClient.OracleCommand cmdVendorSelect;
        private System.Data.OracleClient.OracleCommand oracleCommand1;
        private System.Data.OracleClient.OracleCommand cmdTruckSelect;
        private System.Data.OracleClient.OracleCommand cmdPersonalSelect;
        private System.Data.OracleClient.OracleCommand cmdLoginSelect;
        private System.Data.OracleClient.OracleCommand cmdComplainAttachInsert;
        private System.Data.OracleClient.OracleCommand cmdComplainCancelDoc;
        private System.Data.OracleClient.OracleCommand cmdScoreSelect;
        private System.Data.OracleClient.OracleCommand cmdCusScoreTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdComplainChageStatus;
        private System.Data.OracleClient.OracleCommand cmdPersonalDetailSelect;
        private System.Data.OracleClient.OracleCommand cmdLockDriverInsert;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateSelect;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateLog;
        private System.Data.OracleClient.OracleCommand cmdComplainLockDriver;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateSelect2;
        private System.Data.OracleClient.OracleCommand cmdComplainExportFinal;
        private System.Data.OracleClient.OracleCommand cmdComplainImportSelect;
        private System.Data.OracleClient.OracleCommand cmdExportScoreSelect;
        private System.Data.OracleClient.OracleCommand cmdUpdateUrgent;
        private System.Data.OracleClient.OracleCommand cmdUpdateDriverStatus;
        private System.Data.OracleClient.OracleCommand cmdChangeStatus;
        private System.Data.OracleClient.OracleCommand cmdGetEmailComplain;
        private System.Data.OracleClient.OracleCommand cmdGetEmailVendor;
        private System.Data.OracleClient.OracleCommand cmdGetEmailUser;
        private System.Data.OracleClient.OracleCommand cmdSelectDuplicateTotalCar;
        private System.Data.OracleClient.OracleCommand cmdComplainEmailRequestDoc;
        private System.Data.OracleClient.OracleCommand cmdGetEmailComplainAdmin;
        private System.Data.OracleClient.OracleCommand cmdCancelDocUpdateDisable;
        private System.Data.OracleClient.OracleCommand cmdSelectDriverDisable;
        private System.Data.OracleClient.OracleCommand cmdGetEmailComplainAdmin2;
        private System.Data.OracleClient.OracleCommand cmdOTPSave;
        private System.Data.OracleClient.OracleCommand cmdOTPCheck;
        private System.Data.OracleClient.OracleCommand cmdComplainOTPSave;
        private System.Data.OracleClient.OracleCommand cmdAppealSelect;
        private System.Data.OracleClient.OracleCommand cmdSelectDuplicateTotalCarPartial;
        private System.Data.OracleClient.OracleCommand cmdSelectScorePoint;
        private System.Data.OracleClient.OracleCommand cmdStatusEmployee;
        private System.Data.OracleClient.OracleCommand cmdStatusEmployeeSAP;
        private System.Data.OracleClient.OracleCommand cmdSearchByDO;
        private System.Data.OracleClient.OracleCommand cmdSentencerSelect;
        private System.Data.OracleClient.OracleCommand cmdSentencerSelectDetail;
        private System.Data.OracleClient.OracleCommand cmdGetDriverConfict;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateTMS;
        private System.Data.OracleClient.OracleCommand cmdDriverUpdateSAP;
        private System.Data.OracleClient.OracleCommand cmdTruckOutContractSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckWorkgroup;
        private System.Data.OracleClient.OracleCommand cmdTblReqTemp;
        private System.Data.OracleClient.OracleCommand cmdSupplainSelect;
        private System.Data.OracleClient.OracleCommand cmdComplainAddDriver;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectAddDriver;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectDriver;
        private System.Data.OracleClient.OracleCommand cmdComplainUpdateDriver;
        private System.Data.OracleClient.OracleCommand cmdComplainUpdateOldDriver;
        private System.Data.OracleClient.OracleCommand cmdComplainDelDriver;
        private System.Data.OracleClient.OracleCommand cmdComplainLockDriverNew;
        private System.Data.OracleClient.OracleCommand cmdComplainChangeStatus;
        private System.Data.OracleClient.OracleCommand cmdComplainChangStatus;
        private System.Data.OracleClient.OracleCommand cmdComplainSelectHistory;
        private System.Data.OracleClient.OracleCommand cmdGenerateText;
        private System.Data.OracleClient.OracleCommand cmdComplainLockDriverSelect;
        private System.Data.OracleClient.OracleCommand cmdUnlockDriverSave;
        private System.Data.OracleClient.OracleCommand cmdComplainUploadLate;
        private System.Data.OracleClient.OracleCommand cmdDateEndLockDriverSelect;
    }
}
