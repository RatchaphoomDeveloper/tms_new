﻿namespace TMS_DAL.Transaction.Appeal
{
    partial class AppealDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppealDAL));
            this.cmdAppealComplainSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdAppealMonthlyReportSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateStatusComplain = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainCheckStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdAppealSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdAppealSelectAll = new System.Data.OracleClient.OracleCommand();
            this.cmdComplainAttachInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdImportFileSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdAppeal = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdAppealComplainSelect
            // 
            this.cmdAppealComplainSelect.Connection = this.OracleConn;
            this.cmdAppealComplainSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SVENDORID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdAppealMonthlyReportSelect
            // 
            this.cmdAppealMonthlyReportSelect.Connection = this.OracleConn;
            this.cmdAppealMonthlyReportSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SVENDORID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdUpdateStatusComplain
            // 
            this.cmdUpdateStatusComplain.Connection = this.OracleConn;
            this.cmdUpdateStatusComplain.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DOC_STATUS_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdComplainCheckStatus
            // 
            this.cmdComplainCheckStatus.Connection = this.OracleConn;
            this.cmdComplainCheckStatus.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SVENDORID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdAppealSelect
            // 
            this.cmdAppealSelect.CommandText = "SELECT * FROM TAPPEAL WHERE SAPPEALID =:I_SAPPEALID";
            this.cmdAppealSelect.Connection = this.OracleConn;
            this.cmdAppealSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SAPPEALID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdAppealSelectAll
            // 
            this.cmdAppealSelectAll.Connection = this.OracleConn;
            this.cmdAppealSelectAll.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SAPPEALID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdComplainAttachInsert
            // 
            this.cmdComplainAttachInsert.CommandText = resources.GetString("cmdComplainAttachInsert.CommandText");
            this.cmdComplainAttachInsert.Connection = this.OracleConn;
            this.cmdComplainAttachInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("FILENAME_SYSTEM", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("FILENAME_USER", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("UPLOAD_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_INT", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("REF_STR", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("ISACTIVE", System.Data.OracleClient.OracleType.Char, 1),
            new System.Data.OracleClient.OracleParameter("CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("FULLPATH", System.Data.OracleClient.OracleType.VarChar, 500)});
            // 
            // cmdImportFileDelete
            // 
            this.cmdImportFileDelete.CommandText = "DELETE FROM F_UPLOAD WHERE REF_STR =: I_DOC_ID AND F_UPLOAD.UPLOAD_ID IN (SELECT " +
    "UPLOAD_ID FROM M_UPLOAD_TYPE WHERE UPLOAD_TYPE =: I_UPLOAD_TYPE)";
            this.cmdImportFileDelete.Connection = this.OracleConn;
            this.cmdImportFileDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 30),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, "255")});
            // 
            // cmdImportFileSelect
            // 
            this.cmdImportFileSelect.CommandText = resources.GetString("cmdImportFileSelect.CommandText");
            this.cmdImportFileSelect.Connection = this.OracleConn;
            this.cmdImportFileSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_DOC_ID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdAppeal
            // 
            this.cmdAppeal.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdAppealComplainSelect;
        private System.Data.OracleClient.OracleCommand cmdAppealMonthlyReportSelect;
        private System.Data.OracleClient.OracleCommand cmdUpdateStatusComplain;
        private System.Data.OracleClient.OracleCommand cmdComplainCheckStatus;
        private System.Data.OracleClient.OracleCommand cmdAppealSelect;
        private System.Data.OracleClient.OracleCommand cmdAppealSelectAll;
        private System.Data.OracleClient.OracleCommand cmdComplainAttachInsert;
        private System.Data.OracleClient.OracleCommand cmdImportFileDelete;
        private System.Data.OracleClient.OracleCommand cmdImportFileSelect;
        private System.Data.OracleClient.OracleCommand cmdAppeal;
    }
}
