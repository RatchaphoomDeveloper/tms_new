﻿using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System;
using System.Data.OracleClient;
using TMS_DAL.Master;

namespace TMS_DAL.Transaction.Appeal
{
    public partial class AppealDAL : OracleConnectionDAL
    {

        public enum eType
        {
            All = 0, //
            SurpriseCheck = 1, // ตรวจสภาพรถ
            Accident = 2, //อุบัติเหตุ
            Complain = 3, //เรื่องร้องเรียน
            ConfirmContracts = 4, //ยืนยันรถตามสัญญา
            PlanTransport = 5, //ยืนยันรถตามแผน
            ReportMonthly = 6 //รายงานประจำเดือน

        }
        public AppealDAL()
        {
            InitializeComponent();
        }

        public AppealDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable AppealComplainSelectDAL(string SVENDORID, string Condition)
        {
            string Query = cmdAppealComplainSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdAppealComplainSelect.CommandText = @"SELECT DISTINCT
  - TCOMPLAIN_SCORE.TOTAL_POINT AS SUMPOINT,
  TCOMPLAIN_SCORE.TOTAL_COST + TCOMPLAIN_SCORE.COST_OTHER AS COST,
  TCOMPLAIN_SCORE.TOTAL_DISABLE_DRIVER AS DISABLE_DRIVER,
  CASE WHEN NVL(TCOMPLAIN_SCORE.BLACKLIST, 0) = 0 THEN '' ELSE 'Y' END AS BLACKLIST,
                                                       ID1, SCHECKID, TO_CHAR(DCHECK, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK, SCONTRACTNO, OUTBOUNDNO, STYPE, SPROCESS, STOPICNAME, STOPICID, SVERSION, SHEADREGISTERNO, STRAILERREGISTERNO, SAPPEALID, ISSUETEXT, SDRIVERNO, AREAACCIDENT, SCONTRACTID, DCREATE, SCUSTOMERID, NREDUCEID, STATUS, CHKSTATUS, SCHECKLISTID, SVERSIONLIST, STYPECHECKLISTID, FINALPOINT, ATTACH_NO, TO_CHAR(DINCIDENT, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DINCIDENT, SVENDORID, NPOINT, SAPPEALNO, SPROCESSID, SPROCESSNAME
                                                     , CASE WHEN TMP.SSENTENCER = 1 THEN 0 WHEN TMP.SSENTENCER = 2 THEN - NPOINT ELSE - NVL(TMP.TOTAL_POINT, '') END AS TOTAL_POINT
                                                     , CASE WHEN TMP.SSENTENCER = 1 THEN 0 WHEN TMP.SSENTENCER = 2 THEN SUM(COST) ELSE NVL(TMP.TOTAL_COST, '') END AS TOTAL_COST
                                                     , CASE WHEN TMP.SSENTENCER = 1 THEN 0 WHEN TMP.SSENTENCER = 2 THEN SUM(DISABLE_DRIVER) ELSE NVL(TMP.TOTAL_DISABLE_DRIVER, '') END AS TOTAL_DISABLE_DRIVER
                                                     , CASE WHEN NVL(BLACKLIST, 0) = 0 THEN '' ELSE 'Y' END AS BLACKLIST
                                                     , CASE WHEN NVL(BLACKLIST_A, 0) = 0 THEN '' ELSE 'Y' END AS BLACKLIST_A
,VENDOR_NAME_TH,SAPPEALTEXT,SWAITDOCUMENT,CSTATUS,DINCIDENT7
                                                    FROM
                                                    (
                                                    SELECT DISTINCT TCOMPLAIN.DOC_ID || '/' || TO_CHAR(TCOMPLAIN_SCORE_CAR.DELIVERY_DATE, 'YYYY-MM-DD', 'NLS_DATE_LANGUAGE = ENGLISH') || '/' || NVL(TCOMPLAIN_SCORE_CAR.STRUCKID, '') AS ID1, TCOMPLAIN.DOC_ID AS SCHECKID, TCOMPLAIN_SCORE_CAR.DELIVERY_DATE AS DCHECK, TCONTRACT.SCONTRACTNO, TCOMPLAIN.SDELIVERYNO AS OUTBOUNDNO,'เรื่องร้องเรียน' AS STYPE
                                                   , '080' AS SPROCESS, TCOMPLAIN.DOC_ID AS STOPICNAME, 0 AS STOPICID, 0 AS SVERSION, NVL(TCOMPLAIN_SCORE_CAR.CARHEAD,TCOMPLAIN.SHEADREGISTERNO) AS SHEADREGISTERNO, NVL(TCOMPLAIN_SCORE_CAR.CARDETAIL,TCOMPLAIN.STRAILERREGISTERNO) AS STRAILERREGISTERNO
                                                   , CASE WHEN TCOMPLAIN_SCORE_CAR.TOTAL_CAR IS NULL 
                                                            OR TCOMPLAIN_SCORE_CAR.TOTAL_CAR = 0 THEN TCOMPLAIN_SCORE_DETAIL.POINT / TCOMPLAIN_SCORE_DETAIL.TOTAL_CAR
                                                                                                      ELSE TCOMPLAIN_SCORE_DETAIL.POINT / TCOMPLAIN_SCORE_DETAIL.TOTAL_CAR * TCOMPLAIN_SCORE_CAR.TOTAL_CAR END AS SUMPOINT
                                                   , CASE WHEN TCOMPLAIN_SCORE_CAR.TOTAL_CAR IS NULL 
                                                            OR TCOMPLAIN_SCORE_CAR.TOTAL_CAR = 0 THEN TCOMPLAIN_SCORE_DETAIL.COST / TCOMPLAIN_SCORE_DETAIL.TOTAL_CAR
                                                                                                      ELSE TCOMPLAIN_SCORE_DETAIL.COST / TCOMPLAIN_SCORE_DETAIL.TOTAL_CAR * TCOMPLAIN_SCORE_CAR.TOTAL_CAR END AS COST
                                                   , CASE WHEN TCOMPLAIN_SCORE_CAR.TOTAL_CAR IS NULL 
                                                            OR TCOMPLAIN_SCORE_CAR.TOTAL_CAR = 0 THEN TCOMPLAIN_SCORE_DETAIL.DISABLE_DRIVER / TCOMPLAIN_SCORE_DETAIL.TOTAL_CAR
                                                                                                      ELSE TCOMPLAIN_SCORE_DETAIL.DISABLE_DRIVER / TCOMPLAIN_SCORE_DETAIL.TOTAL_CAR * TCOMPLAIN_SCORE_CAR.TOTAL_CAR END AS DISABLE_DRIVER
                                                   
                                                   , TCOMPLAIN_SCORE_A.BLACKLIST AS BLACKLIST_A
                                                   , TAPPEAL.SAPPEALID, TCOMPLAIN.SDETAIL AS ISSUETEXT, TCOMPLAIN.SDRIVERNO
                                                   , TCOMPLAIN.SCOMPLAINADDRESS AS AREAACCIDENT, TCOMPLAIN.SCONTRACTID, TO_DATE(TCOMPLAIN.DCREATE, 'DD/MM/YYYY') AS DCREATE, '' AS SCUSTOMERID, TREDUCEPOINT.NREDUCEID
                                                   , CASE WHEN TAPPEAL.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(TCOMPLAIN.COMPLAIN_DATE, 'DD/MM/YYYY'))) > 0 THEN 'เหลือเวลา ' || TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(TCOMPLAIN.COMPLAIN_DATE, 'DD/MM/YYYY'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '3' THEN CASE WHEN TAPPEAL.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE 
                                                                                          CASE WHEN TAPPEAL.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน (เปลี่ยน)' END  END  END  END END END END END END END AS STATUS
                                                   , CASE WHEN TAPPEAL.SAPPEALID IS NULL THEN  CASE WHEN TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(TCOMPLAIN.COMPLAIN_DATE, 'DD/MM/YYYY'))) > 0 THEN 'W' ELSE 'E' END  ELSE 
                                                     CASE WHEN TAPPEAL.CSTATUS IN ('0', '1', '4', '5') THEN 'F' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '2' THEN 'I' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '6' THEN 'N' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '3' THEN  'F'  END  END END END END AS CHKSTATUS
                                                   , '0' AS SCHECKLISTID, '0' AS SVERSIONLIST, '0' AS STYPECHECKLISTID
                                                   , CASE WHEN TAPPEAL.CSTATUS = '3' THEN CASE WHEN TAPPEAL.SSENTENCER = '1' THEN '0' ELSE '-' || TAPPEAL.NPOINT END ELSE '' END AS FINALPOINT
                                                   , NVL(TAPPEAL.ATTACH_NO, 1) AS ATTACH_NO, TCOMPLAIN_SCORE_CAR.DELIVERY_DATE DINCIDENT
, TO_CHAR(TCOMPLAIN.COMPLAIN_DATE + 7,'DD/MM/YYYY') DINCIDENT7
, VS.SVENDORID, NVL(TAPPEAL.NPOINT,TCOMPLAIN_SCORE.TOTAL_POINT) AS NPOINT, TAPPEAL.SAPPEALNO, '080' AS SPROCESSID
                                                   , 'เรื่องร้องเรียน' AS SPROCESSNAME
                                                   , TCOMPLAIN_SCORE_A.TOTAL_POINT, TCOMPLAIN_SCORE_A.TOTAL_COST, TCOMPLAIN_SCORE_A.TOTAL_DISABLE_DRIVER, TAPPEAL.SSENTENCER
,vs.SABBREVIATION AS VENDOR_NAME_TH,TAPPEAL.SAPPEALTEXT,TAPPEAL.SWAITDOCUMENT,TAPPEAL.CSTATUS
                                            FROM TCOMPLAIN LEFT JOIN TCOMPLAIN_SCORE_CAR ON TCOMPLAIN.DOC_ID = TCOMPLAIN_SCORE_CAR.DOC_ID AND ROWNUM = 1
                                                           LEFT JOIN TAPPEAL ON TCOMPLAIN.DOC_ID = TAPPEAL.SREFERENCEID AND TCOMPLAIN_SCORE_CAR.DELIVERY_DATE = TAPPEAL.DINCIDENT AND NVL(TAPPEAL.SHEADREGISTERNO, 0) = NVL(TCOMPLAIN_SCORE_CAR.CARHEAD, 0)
                                                           LEFT JOIN TREDUCEPOINT ON TCOMPLAIN.DOC_ID = TREDUCEPOINT.SREFERENCEID AND NVL(TREDUCEPOINT.SHEADID, 0) = NVL(TCOMPLAIN_SCORE_CAR.STRUCKID, 0) AND TREDUCEPOINT.DREDUCE = TCOMPLAIN_SCORE_CAR.DELIVERY_DATE AND TREDUCEPOINT.CACTIVE = 1
                                                           LEFT JOIN TCONTRACT ON TREDUCEPOINT.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                           INNER JOIN TCOMPLAIN_SCORE_DETAIL ON TCOMPLAIN_SCORE_CAR.DOC_ID = TCOMPLAIN_SCORE_DETAIL.DOC_ID AND TCOMPLAIN_SCORE_CAR.STOPICID = TCOMPLAIN_SCORE_DETAIL.STOPICID AND TCOMPLAIN_SCORE_CAR.DELIVERY_DATE = TCOMPLAIN.DDELIVERY
                                                           INNER JOIN TCOMPLAIN_SCORE ON TCOMPLAIN_SCORE_DETAIL.DOC_ID = TCOMPLAIN_SCORE.DOC_ID AND TCOMPLAIN_SCORE.ISACTIVE = 1
                                                           LEFT JOIN TCOMPLAIN_SCORE_A ON TCOMPLAIN.DOC_ID = TCOMPLAIN_SCORE_A.DOC_ID AND TCOMPLAIN.DDELIVERY = TCOMPLAIN_SCORE_A.DELIVERY_DATE AND TCOMPLAIN_SCORE_A.SAPPEALID = TAPPEAL.SAPPEALID
LEFT JOIN TVENDOR vs ON TCOMPLAIN.SVENDORID = VS.SVENDORID
                                            WHERE TCOMPLAIN.DOC_STATUS_ID IN (4, 6, 10, 11)
                                            AND TCOMPLAIN.CCUT = '1'
                                            AND TCOMPLAIN_SCORE_DETAIL.ISACTIVE = 1
                                            AND (TREDUCEPOINT.CACTIVE IN (1, 0) OR TREDUCEPOINT.CACTIVE IS NULL)
                                            AND TCOMPLAIN.SVENDORID = :SVENDORID";

                cmdAppealComplainSelect.CommandText += Condition;
                cmdAppealComplainSelect.CommandText += @") TMP
INNER JOIN TCOMPLAIN_SCORE ON TMP.SCHECKID = TCOMPLAIN_SCORE.DOC_ID WHERE TCOMPLAIN_SCORE.ISACTIVE = 1
                                                            GROUP BY 
TCOMPLAIN_SCORE.TOTAL_POINT, TCOMPLAIN_SCORE.TOTAL_COST, TCOMPLAIN_SCORE.COST_OTHER, TCOMPLAIN_SCORE.TOTAL_DISABLE_DRIVER, TCOMPLAIN_SCORE.BLACKLIST,
ID1, SCHECKID, DCHECK, SCONTRACTNO, OUTBOUNDNO, STYPE, SPROCESS, STOPICNAME, STOPICID, SVERSION, SHEADREGISTERNO, STRAILERREGISTERNO, SAPPEALID, ISSUETEXT, SDRIVERNO, AREAACCIDENT, SCONTRACTID, DCREATE, SCUSTOMERID, NREDUCEID, STATUS, CHKSTATUS, SCHECKLISTID, SVERSIONLIST, STYPECHECKLISTID, FINALPOINT, ATTACH_NO, DINCIDENT, SVENDORID, NPOINT, SAPPEALNO, SPROCESSID, SPROCESSNAME, TMP.TOTAL_POINT, TMP.TOTAL_COST, TMP.TOTAL_DISABLE_DRIVER, TMP.SSENTENCER, TMP.BLACKLIST_A
,VENDOR_NAME_TH,SAPPEALTEXT,SWAITDOCUMENT,CSTATUS,DINCIDENT7";

                cmdAppealComplainSelect.Parameters["SVENDORID"].Value = SVENDORID;

                return dbManager.ExecuteDataTable(cmdAppealComplainSelect, "cmdAppealComplainSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdAppealComplainSelect.CommandText = Query;
            }
        }
        #region AppealSentencerTypeSelect
        public DataTable AppealSentencerTypeSelect()
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region USP_T_APPEAL_SELECT
                cmdAppeal.CommandText = @"USP_SENTENCER_TYPE_SELECT";

                cmdAppeal.Parameters.Clear();

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealSentencerTypeSelect
        public DataTable AppealSentencerTypeSelect(string I_SSENTENCERCODE)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region USP_T_APPEAL_SELECT
                cmdAppeal.CommandText = @"USP_SENTENCER_TYPE_NAME_SELECT";

                cmdAppeal.Parameters.Clear();

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SSENTENCERCODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SSENTENCERCODE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealChangeSave
        public DataTable AppealChangeSave(string I_SAPPEALID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region SAVE
                cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_4_SAVE";

                cmdAppeal.Parameters.Clear();
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SAPPEALID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_SAPPEALID) ? 0 : int.Parse(I_SAPPEALID),
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion

                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                if (dt != null && dt.Rows.Count > 0)
                {
                    I_SAPPEALID = dt.Rows[0]["MESSAGE"] + string.Empty;

                    UploadDAL.Instance.UploadAdd(I_SAPPEALID, "SUPPLIANTPTT_CHANGE", dtUpload, I_USERID, dbManager);
                    DataRow dr = I_DATATABLE.Tables[0].Rows[0];
                    //if (dr["SSENTENCER"] + string.Empty == "1")
                    //{
                    #region USP_T_APPEAL_PTT_6_SAVE
                    cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_6_SAVE";
                    cmdAppeal.Parameters.Clear();
                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_SAPPEALID",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = string.IsNullOrEmpty(I_SAPPEALID) ? 0 : int.Parse(I_SAPPEALID),
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_USERID",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = I_USERID,
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_DATATABLE",
                        IsNullable = true,
                        OracleType = OracleType.Clob,
                        Value = I_DATATABLE.GetXml(),
                    });
                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                    #endregion

                    #region USP_T_APPEAL_PTT_POINT_SAVE
                    cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_POINT_SAVE";

                    cmdAppeal.Parameters.Clear();
                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_NREDUCEID",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = string.IsNullOrEmpty(dr["NREDUCEID"] + string.Empty) ? 0 : int.Parse(dr["NREDUCEID"] + string.Empty),
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_STATUS",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = string.IsNullOrEmpty(dr["SSENTENCER"] + string.Empty) ? 0 : int.Parse(dr["SSENTENCER"] + string.Empty),
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                    #endregion

                    dt.Rows[0]["MESSAGE"] = DBNull.Value;
                }
                dbManager.CommitTransaction();

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region AppealConsiderSave
        public DataTable AppealConsiderSave(string I_SAPPEALID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region SAVE
                cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_4_SAVE";

                cmdAppeal.Parameters.Clear();
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SAPPEALID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_SAPPEALID) ? 0 : int.Parse(I_SAPPEALID),
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion

                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                if (dt != null && dt.Rows.Count > 0)
                {
                    I_SAPPEALID = dt.Rows[0]["MESSAGE"] + string.Empty;

                    UploadDAL.Instance.UploadAdd(I_SAPPEALID, "SUPPLIANTPTT", dtUpload, I_USERID, dbManager);
                    DataRow dr = I_DATATABLE.Tables[0].Rows[0];
                    if (dr["SSENTENCER"] + string.Empty == "1")
                    {
                        #region USP_T_APPEAL_PTT_4_POINT_SAVE
                        cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_4_POINT_SAVE";

                        cmdAppeal.Parameters.Clear();
                        cmdAppeal.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_SAPPEALID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(I_SAPPEALID) ? 0 : int.Parse(I_SAPPEALID),
                        });

                        cmdAppeal.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DINCIDENT",
                            IsNullable = true,
                            OracleType = OracleType.VarChar,
                            Value = dr["DINCIDENT"] + string.Empty,
                        });

                        cmdAppeal.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                        #endregion

                        #region USP_T_APPEAL_PTT_POINT_SAVE
                        cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_POINT_SAVE";

                        cmdAppeal.Parameters.Clear();
                        cmdAppeal.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_NREDUCEID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(dr["NREDUCEID"] + string.Empty) ? 0 : int.Parse(dr["NREDUCEID"] + string.Empty),
                        });

                        cmdAppeal.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_STATUS",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(dr["SSENTENCER"] + string.Empty) ? 0 : int.Parse(dr["SSENTENCER"] + string.Empty),
                        });

                        cmdAppeal.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                        #endregion
                    }
                    dt.Rows[0]["MESSAGE"] = DBNull.Value;
                }
                dbManager.CommitTransaction();

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region AppealSelectByID
        public DataSet AppealSelectByID(string I_APPEALID)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region USP_T_APPEAL_PTT_ID_SELECT
                cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_ID_SELECT";

                cmdAppeal.Parameters.Clear();

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPEALID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_APPEALID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "APPEAL");
                DataSet ds = new DataSet("DS");
                ds.Tables.Add(dt.Copy());

                #endregion
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];

                    #region USP_T_APPEAL_PTT_COMP_SELECT
                    cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_COMP_SELECT";

                    cmdAppeal.Parameters.Clear();

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_SHEADREGISTERNO",
                        IsNullable = true,
                        OracleType = OracleType.VarChar,
                        Value = dr["SHEADREGISTERNO"] + string.Empty,
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_SCONTRACTID",
                        IsNullable = true,
                        OracleType = OracleType.VarChar,
                        Value = dr["SCONTRACTID"] + string.Empty,
                    });
                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    dt = dbManager.ExecuteDataTable(cmdAppeal, "APPEAL_C");
                    ds.Tables.Add(dt.Copy());
                    #endregion

                    #region USP_T_APPEAL_PTT_COMP2_SELECT
                    cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_COMP2_SELECT";

                    cmdAppeal.Parameters.Clear();

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_SREFERENCEID",
                        IsNullable = true,
                        OracleType = OracleType.VarChar,
                        Value = dr["SREFERENCEID"] + string.Empty,
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    dt = dbManager.ExecuteDataTable(cmdAppeal, "APPEAL_G");
                    ds.Tables.Add(dt.Copy());
                    #endregion

                    #region USP_T_APPEAL_PTT_SENT_SELECT
                    cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_SENT_SELECT";

                    cmdAppeal.Parameters.Clear();

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_APPEALID",
                        IsNullable = true,
                        OracleType = OracleType.VarChar,
                        Value = I_APPEALID,
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    dt = dbManager.ExecuteDataTable(cmdAppeal, "APPEAL_S");
                    ds.Tables.Add(dt.Copy());
                    #endregion

                    #region USP_T_APPEAL_PTT_RESULT_SELECT
                    cmdAppeal.CommandText = @"USP_T_APPEAL_PTT_RESULT_SELECT";

                    cmdAppeal.Parameters.Clear();

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_APPEALID",
                        IsNullable = true,
                        OracleType = OracleType.VarChar,
                        Value = I_APPEALID,
                    });

                    cmdAppeal.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });
                    dt = dbManager.ExecuteDataTable(cmdAppeal, "APPEAL_R");
                    ds.Tables.Add(dt.Copy());
                    #endregion
                }

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region AppealVendorSave
        public DataTable AppealVendorSave(string I_SAPPEALID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region SAVE
                cmdAppeal.CommandText = @"USP_T_APPEAL_V_SAVE";

                cmdAppeal.Parameters.Clear();
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SAPPEALID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_SAPPEALID) ? 0 : int.Parse(I_SAPPEALID),
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion

                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                if (dt != null && dt.Rows.Count > 0)
                {
                    I_SAPPEALID = dt.Rows[0]["MESSAGE"] + string.Empty;

                    UploadDAL.Instance.UploadAdd(I_SAPPEALID, "SUPPLIANT", dtUpload, I_USERID, dbManager);

                    dt.Rows[0]["MESSAGE"] = DBNull.Value;
                }
                dbManager.CommitTransaction();

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #region AppealSelect
        public DataTable AppealSelect(string I_SEARCH,
                                     string I_STARTDATE,
                                     string I_ENDDATE,
                                     string I_DOCID,
                                     string I_STATUS,
                                     string I_APPEALID,
                                     string I_TYPE)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region USP_T_APPEAL_SELECT
                cmdAppeal.CommandText = @"USP_T_APPEAL_SELECT";

                cmdAppeal.Parameters.Clear();
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STARTDATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_STARTDATE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ENDDATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ENDDATE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DOCID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DOCID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_STATUS,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPEALID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_APPEALID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TYPE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AppealVendorSelect
        public DataTable AppealVendorSelect(string I_SEARCH,
                                     string I_STARTDATE,
                                     string I_ENDDATE,
                                     string I_DOCID,
                                     string I_STATUS,
                                     string I_APPEALID,
                                     string I_TYPE,
                                     string I_SVENDORID,
                                     string I_V_ID)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region USP_T_APPEAL_SELECT
                if (I_TYPE == (int)eType.SurpriseCheck + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VSC_SELECT";
                }
                else if (I_TYPE == (int)eType.Accident + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VA_SELECT";
                }
                else if (I_TYPE == (int)eType.Complain + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VC_SELECT";
                }
                else if (I_TYPE == (int)eType.ConfirmContracts + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VCC_SELECT";
                }
                else if (I_TYPE == (int)eType.PlanTransport + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VPT_SELECT";
                }
                else if (I_TYPE == (int)eType.ReportMonthly + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VRM_SELECT";
                }
                

                cmdAppeal.Parameters.Clear();
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STARTDATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_STARTDATE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ENDDATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ENDDATE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DOCID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DOCID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_STATUS,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPEALID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_APPEALID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SVENDORID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SVENDORID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TYPE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_V_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = !string.IsNullOrEmpty(I_V_ID) ? I_V_ID : (object)DBNull.Value,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AppealORSelect
        public DataTable AppealORSelect(string I_SEARCH,
                                     string I_STARTDATE,
                                     string I_ENDDATE,
                                     string I_DOCID,
                                     string I_STATUS,
                                     string I_APPEALID,
                                     string I_TYPE,
                                     string I_V_ID)
        {
            try
            {
                cmdAppeal.CommandType = CommandType.StoredProcedure;

                #region USP_T_APPEAL_SELECT
                if (I_TYPE == (int)eType.SurpriseCheck + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VSC_SELECT_OR";
                }
                else if (I_TYPE == (int)eType.Accident + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VA_SELECT_OR";
                }
                else if (I_TYPE == (int)eType.Complain + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VC_SELECT_OR";
                }
                else if (I_TYPE == (int)eType.ConfirmContracts + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VCC_SELECT_OR";
                }
                else if (I_TYPE == (int)eType.PlanTransport + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VPT_SELECT_OR";
                }
                else if (I_TYPE == (int)eType.ReportMonthly + string.Empty)
                {
                    cmdAppeal.CommandText = @"USP_T_APPEAL_VRM_SELECT_OR";
                }


                cmdAppeal.Parameters.Clear();
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STARTDATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_STARTDATE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ENDDATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ENDDATE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DOCID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DOCID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_STATUS,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_APPEALID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_APPEALID,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TYPE,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_V_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = !string.IsNullOrEmpty(I_V_ID) ? I_V_ID : (object)DBNull.Value,
                });
                cmdAppeal.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdAppeal, "cmdAppeal");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public DataTable AppealMonthlyReportSelectDAL(string SVENDORID, string Condition)
        {
            string Query = cmdAppealComplainSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdAppealMonthlyReportSelect.CommandText = @"SELECT TREDUCEPOINT.NREDUCEID AS ID1, TRANSACTION_ID AS SCHECKID, TO_CHAR(FINAL_DATETIME, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK, TCONTRACT.SCONTRACTNO, '' AS OUTBOUNDNO
                                                              , (SELECT SPROCESSNAME FROM TPROCESS WHERE SPROCESSID = 100) AS STYPE, 100 AS SPROCESS
                                                              , TREDUCEPOINT.SREDUCENAME AS STOPICNAME, STOPICID AS STOPICID, SVERSIONLIST AS SVERSION, '' AS SHEADREGISTERNO, '' AS STRAILERREGISTERNO
                                                              , TREDUCEPOINT.NPOINT AS SUMPOINT, TAPPEAL.SAPPEALID, '' AS ISSUETEXT, '' AS SDRIVERNO , '' AS AREAACCIDENT, TREDUCEPOINT.SCONTRACTID
                                                              , TMONTHLY_HEADER.CREATE_DATETIME AS DCREATE, '' AS SCUSTOMERID, TREDUCEPOINT.NREDUCEID
                                                              , CASE WHEN TAPPEAL.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(TMONTHLY_HEADER.FINAL_DATETIME, 'DD/MM/YYYY'))) > 0 THEN 'เหลือเวลา ' || TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(TMONTHLY_HEADER.FINAL_DATETIME, 'DD/MM/YYYY'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '3' THEN CASE WHEN TAPPEAL.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE 
                                                                   CASE WHEN TAPPEAL.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END END END END END END AS STATUS
                                                              , CASE WHEN TAPPEAL.SAPPEALID IS NULL THEN  CASE WHEN TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(TMONTHLY_HEADER.FINAL_DATETIME, 'DD/MM/YYYY'))) > 0 THEN 'W' ELSE 'E' END  ELSE 
                                                                   CASE WHEN TAPPEAL.CSTATUS IN ('0', '1', '4', '5') THEN 'F' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '2' THEN 'I' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '6' THEN 'N' ELSE
                                                                   CASE WHEN TAPPEAL.CSTATUS = '3' THEN  'F'  END  END END END END AS CHKSTATUS
                                                              , '0' AS SCHECKLISTID, '0' AS SVERSIONLIST, '0' AS STYPECHECKLISTID
                                                              , CASE WHEN TAPPEAL.CSTATUS = '3' THEN CASE WHEN TAPPEAL.SSENTENCER = '1' THEN '0' ELSE '-' || TAPPEAL.NPOINT END ELSE '' END AS FINALPOINT
                                                              , NVL(TAPPEAL.ATTACH_NO, 1) AS ATTACH_NO, TAPPEAL.DINCIDENT, TAPPEAL.SVENDORID, TAPPEAL.NPOINT, TAPPEAL.SAPPEALNO, '100' AS SPROCESSID
                                                              , (SELECT SPROCESSNAME FROM TPROCESS WHERE SPROCESSID = 100) AS SPROCESSNAME
                                                              , '' AS COST, '' AS DISABLE_DRIVER, '' AS TOTAL_POINT, '' AS TOTAL_COST, '' AS TOTAL_DISABLE_DRIVER, '' AS BLACKLIST, '' AS BLACKLIST_A
                                                        FROM TMONTHLY_HEADER INNER JOIN TREDUCEPOINT ON TO_CHAR(TMONTHLY_HEADER.TRANSACTION_ID) = TREDUCEPOINT.SREFERENCEID
                                                                             LEFT JOIN TAPPEAL ON TO_CHAR(TREDUCEPOINT.NREDUCEID) = TAPPEAL.NREDUCEID
                                                                             LEFT JOIN TCONTRACT ON TREDUCEPOINT.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                        WHERE TMONTHLY_HEADER.DOC_STATUS_ID = (SELECT DOC_STATUS_ID FROM M_DOC_STATUS_MONTHLY WHERE IS_CLOSE = 1)
                                                        AND TREDUCEPOINT.SPROCESSID = 100
                                                        AND TREDUCEPOINT.IS_APPEAL IS NULL
                                                        AND (TREDUCEPOINT.CACTIVE = 1 OR TREDUCEPOINT.CACTIVE IS NULL)
                                                        AND TMONTHLY_HEADER.VENDOR_ID =:SVENDORID";

                cmdAppealMonthlyReportSelect.CommandText += Condition;

                cmdAppealMonthlyReportSelect.Parameters["SVENDORID"].Value = SVENDORID;

                return dbManager.ExecuteDataTable(cmdAppealMonthlyReportSelect, "cmdAppealMonthlyReportSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdAppealMonthlyReportSelect.CommandText = Query;
            }
        }

        public DataTable AppealAccidentSelectDAL(string SVENDORID, string Condition)
        {
            string Query = cmdAppealComplainSelect.CommandText;
            try
            {
                dbManager.Open();
                cmdAppealComplainSelect.CommandText = @"SELECT 
  - ACC_SCORE.TOTAL_POINT AS SUMPOINT,115 STOPICID,
  ACC_SCORE.TOTAL_COST + ACC_SCORE.COST_OTHER AS COST,
  'H' AS DISABLE_DRIVER,
  CASE WHEN NVL(ACC_SCORE.BLACKLIST, 0) = 0 THEN '' ELSE 'Y' END AS BLACKLIST,
                                                       ID1, SCHECKID, TO_CHAR(DCHECK, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK, SCONTRACTNO, OUTBOUNDNO, STYPE, SPROCESS, STOPICNAME, SVERSION, SHEADREGISTERNO, STRAILERREGISTERNO, SAPPEALID, ISSUETEXT, AREAACCIDENT
  , SCONTRACTID, DCREATE, SCUSTOMERID, NREDUCEID, STATUS, CHKSTATUS, SCHECKLISTID, SVERSIONLIST, STYPECHECKLISTID, FINALPOINT, ATTACH_NO, DINCIDENT, SVENDORID, NPOINT, SAPPEALNO, SPROCESSID, SPROCESSNAME
                                                     , CASE WHEN TMP.SSENTENCER = 1 THEN 0 WHEN TMP.SSENTENCER = 2 THEN - NPOINT ELSE - NVL(TMP.TOTAL_POINT, '') END AS TOTAL_POINT
                                                     , CASE WHEN TMP.SSENTENCER = 1 THEN 0 WHEN TMP.SSENTENCER = 2 THEN SUM(COST) ELSE NVL(TMP.TOTAL_COST, '') END AS TOTAL_COST
                                                     ,  TMP.TOTAL_DISABLE_DRIVER
                                                     , CASE WHEN NVL(BLACKLIST, 0) = 0 THEN '' ELSE 'Y' END AS BLACKLIST
                                                     , CASE WHEN NVL(BLACKLIST_A, 0) = 0 THEN '' ELSE 'Y' END AS BLACKLIST_A
                                                    FROM
                                                    (
                                                    SELECT DISTINCT ACC_ACCIDENT.ACCIDENT_ID || '/' || ACC_SCORE_CAR.DELIVERY_DATE || '/' || NVL(ACC_SCORE_CAR.STRUCKID, '') AS ID1, ACC_ACCIDENT.ACCIDENT_ID AS SCHECKID, ACC_SCORE_CAR.DELIVERY_DATE AS DCHECK, TCONTRACT.SCONTRACTNO, '' AS OUTBOUNDNO,'เรื่องอุบัติเหตุ' AS STYPE
                                                   , '090' AS SPROCESS, ACC_ACCIDENT.ACCIDENT_ID AS STOPICNAME, acc_score_detail.STOPICID AS STOPICID, 0 AS SVERSION, ACC_SCORE_CAR.CARHEAD AS SHEADREGISTERNO, ACC_SCORE_CAR.CARDETAIL AS STRAILERREGISTERNO
                                                   , CASE WHEN ACC_SCORE_CAR.TOTAL_CAR IS NULL 
                                                            OR ACC_SCORE_CAR.TOTAL_CAR = 0 THEN ACC_SCORE_DETAIL.POINT / ACC_SCORE_DETAIL.TOTAL_CAR
                                                                                                      ELSE ACC_SCORE_DETAIL.POINT / ACC_SCORE_DETAIL.TOTAL_CAR * ACC_SCORE_CAR.TOTAL_CAR END AS SUMPOINT
                                                   , CASE WHEN ACC_SCORE_CAR.TOTAL_CAR IS NULL 
                                                            OR ACC_SCORE_CAR.TOTAL_CAR = 0 THEN ACC_SCORE_DETAIL.COST / ACC_SCORE_DETAIL.TOTAL_CAR
                                                                                                      ELSE ACC_SCORE_DETAIL.COST / ACC_SCORE_DETAIL.TOTAL_CAR * ACC_SCORE_CAR.TOTAL_CAR END AS COST
                                                   , CASE WHEN ACC_SCORE_CAR.TOTAL_CAR IS NULL 
                                                            OR ACC_SCORE_CAR.TOTAL_CAR = 0 THEN ACC_SCORE_DETAIL.DISABLE_DRIVER / ACC_SCORE_DETAIL.TOTAL_CAR
                                                                                                      ELSE ACC_SCORE_DETAIL.DISABLE_DRIVER / ACC_SCORE_DETAIL.TOTAL_CAR * ACC_SCORE_CAR.TOTAL_CAR END AS DISABLE_DRIVER
                                                   
                                                   , ACC_SCORE_A.BLACKLIST AS BLACKLIST_A
                                                   , TAPPEAL.SAPPEALID, '' AS ISSUETEXT, acc_detail.shipment_no
                                                   , '' AS AREAACCIDENT
                                                      , tcontract.SCONTRACTID, TO_DATE(acc_accident.create_date, 'DD/MM/YYYY') AS DCREATE, '' AS SCUSTOMERID, TREDUCEPOINT.NREDUCEID
                                                   , CASE WHEN TAPPEAL.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(acc_accident.approve_date, 'DD/MM/YYYY'))) > 0 THEN 'เหลือเวลา ' || TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(acc_accident.approve_date, 'DD/MM/YYYY'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '3' THEN CASE WHEN TAPPEAL.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE 
                                                                                          CASE WHEN TAPPEAL.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน (เปลี่ยน)' END  END  END  END END END END END END END AS STATUS
                                                   , CASE WHEN TAPPEAL.SAPPEALID IS NULL THEN  CASE WHEN TRUNC(7 - (TO_DATE(SYSDATE, 'DD/MM/YYYY') - TO_DATE(acc_accident.approve_date, 'DD/MM/YYYY'))) > 0 THEN 'W' ELSE 'E' END  ELSE 
                                                     CASE WHEN TAPPEAL.CSTATUS IN ('0', '1', '4', '5') THEN 'F' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '2' THEN 'I' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '6' THEN 'N' ELSE
                                                     CASE WHEN TAPPEAL.CSTATUS = '3' THEN  'F'  END  END END END END AS CHKSTATUS
                                                   , '0' AS SCHECKLISTID, '0' AS SVERSIONLIST, '0' AS STYPECHECKLISTID
                                                   , CASE WHEN TAPPEAL.CSTATUS = '3' THEN CASE WHEN TAPPEAL.SSENTENCER = '1' THEN '0' ELSE '-' || TAPPEAL.NPOINT END ELSE '' END AS FINALPOINT
                                                   , NVL(TAPPEAL.ATTACH_NO, 1) AS ATTACH_NO, TAPPEAL.DINCIDENT, TAPPEAL.SVENDORID, TAPPEAL.NPOINT, TAPPEAL.SAPPEALNO, '090' AS SPROCESSID
                                                   , 'อุบัติเหตุ' AS SPROCESSNAME
                                                   , ACC_SCORE_A.TOTAL_POINT, ACC_SCORE_A.TOTAL_COST
, CASE WHEN ACC_SCORE_A.TOTAL_DISABLE_DRIVER IS NULL THEN ''
 WHEN ACC_SCORE_A.TOTAL_DISABLE_DRIVER = '-2' THEN 'H'
ELSE to_char(ACC_SCORE_A.TOTAL_DISABLE_DRIVER) END TOTAL_DISABLE_DRIVER
, TAPPEAL.SSENTENCER
                                            FROM ACC_ACCIDENT 
                                                      LEFT JOIN ACC_DETAIL ON ACC_ACCIDENT.ACCIDENT_ID = ACC_DETAIL.ACCIDENT_ID
                                                      LEFT JOIN ACC_SCORE_CAR  ON ACC_ACCIDENT.ACCIDENT_ID = ACC_SCORE_CAR.ACCIDENT_ID  AND to_date(ACC_SCORE_CAR.DELIVERY_DATE) = to_date(ACC_ACCIDENT.ACCIDENT_DATE)
                                                           LEFT JOIN TAPPEAL ON ACC_ACCIDENT.ACCIDENT_ID = TAPPEAL.SREFERENCEID AND to_date(ACC_SCORE_CAR.DELIVERY_DATE) = TAPPEAL.DINCIDENT AND NVL(TAPPEAL.SHEADREGISTERNO, 0) = NVL(ACC_SCORE_CAR.CARHEAD, 0)
                                                           LEFT JOIN TREDUCEPOINT ON ACC_ACCIDENT.ACCIDENT_ID = TREDUCEPOINT.SREFERENCEID AND NVL(TREDUCEPOINT.SHEADID, 0) = NVL(ACC_SCORE_CAR.STRUCKID, 0) AND TREDUCEPOINT.DREDUCE = ACC_SCORE_CAR.DELIVERY_DATE
                                                           LEFT JOIN TCONTRACT ON TREDUCEPOINT.SCONTRACTID = TCONTRACT.SCONTRACTID
                                                           INNER JOIN ACC_SCORE_DETAIL ON ACC_SCORE_CAR.accident_id = ACC_SCORE_DETAIL.accident_id AND ACC_SCORE_CAR.STOPICID = ACC_SCORE_DETAIL.STOPICID
                                                           INNER JOIN ACC_SCORE ON ACC_SCORE_DETAIL.ACCIDENT_ID = ACC_SCORE.accident_id AND ACC_SCORE.ISACTIVE = 1
                                                           LEFT JOIN ACC_SCORE_A ON ACC_ACCIDENT.ACCIDENT_ID = ACC_SCORE_A.ACCIDENT_ID AND  to_date(ACC_ACCIDENT.ACCIDENT_DATE) = to_date(ACC_SCORE_A.DELIVERY_DATE) AND ACC_SCORE_A.SAPPEALID = TAPPEAL.SAPPEALID
                                            WHERE ACC_ACCIDENT.cactive >= 8
                                            AND ACC_ACCIDENT.CCUT = '1'
                                            AND ACC_SCORE_DETAIL.ISACTIVE = 1
                                            AND (TREDUCEPOINT.CACTIVE IN (1, 0) OR TREDUCEPOINT.CACTIVE IS NULL)
                                            AND acc_detail.SVENDORID = :SVENDORID";

                cmdAppealComplainSelect.CommandText += Condition;
                cmdAppealComplainSelect.CommandText += @") TMP
INNER JOIN ACC_SCORE ON TMP.SCHECKID = ACC_SCORE.accident_id WHERE ACC_SCORE.ISACTIVE = 1
                                                            GROUP BY 
ACC_SCORE.TOTAL_POINT, ACC_SCORE.TOTAL_COST, ACC_SCORE.COST_OTHER, ACC_SCORE.TOTAL_DISABLE_DRIVER, ACC_SCORE.BLACKLIST,
ID1, SCHECKID, DCHECK, SCONTRACTNO, OUTBOUNDNO, STYPE, SPROCESS, STOPICNAME, SVERSION, SHEADREGISTERNO, STRAILERREGISTERNO, SAPPEALID, ISSUETEXT, AREAACCIDENT
  , SCONTRACTID, DCREATE, SCUSTOMERID
  , NREDUCEID, STATUS, CHKSTATUS, SCHECKLISTID, SVERSIONLIST, STYPECHECKLISTID, FINALPOINT, ATTACH_NO, DINCIDENT, SVENDORID, NPOINT, SAPPEALNO, SPROCESSID, SPROCESSNAME, TMP.TOTAL_POINT, TMP.TOTAL_COST, TMP.TOTAL_DISABLE_DRIVER, TMP.SSENTENCER, TMP.BLACKLIST_A";

                cmdAppealComplainSelect.Parameters["SVENDORID"].Value = SVENDORID;

                return dbManager.ExecuteDataTable(cmdAppealComplainSelect, "cmdAppealComplainSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdAppealComplainSelect.CommandText = Query;
            }
        }

        public void UpdateStatusComplainDAL(string DocID, int DocStatusID, string DeliveryDate)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUpdateStatusComplain.CommandText = @"UPDATE TCOMPLAIN
                                                        SET DOC_STATUS_ID = :I_DOC_STATUS_ID
                                                        WHERE DOC_ID = :I_DOC_ID";

                cmdUpdateStatusComplain.Parameters["I_DOC_ID"].Value = DocID;
                cmdUpdateStatusComplain.Parameters["I_DOC_STATUS_ID"].Value = DocStatusID;

                dbManager.ExecuteNonQuery(cmdUpdateStatusComplain);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ComplainCheckStatusDAL()
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdComplainCheckStatus.CommandType = CommandType.StoredProcedure;
                cmdComplainCheckStatus.CommandText = "USP_T_COMPLAIN_STATUS";

                dbManager.ExecuteNonQuery(cmdComplainCheckStatus);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable AppealSelectDAL(string AppealID)
        {
            try
            {
                dbManager.Open();
                cmdAppealSelect.Parameters["I_SAPPEALID"].Value = AppealID;

                return dbManager.ExecuteDataTable(cmdAppealSelect, "cmdAppealSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable AppealSelectAllDAL(string AppealID)
        {
            try
            {
                dbManager.Open();
                cmdAppealSelectAll.CommandText = @"SELECT TAPPEAL.SAPPEALID, TREDUCEPOINT.SREFERENCEID, TO_CHAR(TREDUCEPOINT.DREDUCE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DREDUCE
                                                                  , TREDUCEPOINT.SHEADID
                                                             FROM TAPPEAL LEFT JOIN TREDUCEPOINT ON TAPPEAL.NREDUCEID = TREDUCEPOINT.NREDUCEID
                                                             WHERE SAPPEALID =:I_SAPPEALID";

                cmdAppealSelectAll.Parameters["I_SAPPEALID"].Value = AppealID;

                return dbManager.ExecuteDataTable(cmdAppealSelectAll, "cmdAppealSelectAll");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void AppealSaveImportFileDAL(DataTable dtUpload, string DocID, string UploadType, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdImportFileDelete.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileDelete.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                dbManager.ExecuteNonQuery(cmdImportFileDelete);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdComplainAttachInsert.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdComplainAttachInsert.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdComplainAttachInsert.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdComplainAttachInsert.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdComplainAttachInsert.Parameters["REF_INT"].Value = 0;
                    cmdComplainAttachInsert.Parameters["REF_STR"].Value = DocID;
                    cmdComplainAttachInsert.Parameters["ISACTIVE"].Value = "1";
                    cmdComplainAttachInsert.Parameters["CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdComplainAttachInsert);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable ImportFileSelectDAL(string DocID, string UploadType)
        {
            try
            {
                dbManager.Open();
                cmdImportFileSelect.Parameters["I_DOC_ID"].Value = DocID;
                cmdImportFileSelect.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                return dbManager.ExecuteDataTable(cmdImportFileSelect, "cmdImportFileSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static AppealDAL _instance;
        public static AppealDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new AppealDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}