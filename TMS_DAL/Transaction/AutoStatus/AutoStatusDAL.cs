﻿using System;
using System.ComponentModel;
using System.Data;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.Transaction.AutoStatus
{
    public partial class AutoStatusDAL : OracleConnectionDAL
    {
        public AutoStatusDAL()
        {
            InitializeComponent();
        }

        public AutoStatusDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void DriverLogInsertDAL(DataTable dtData)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdDriverLogInsert.CommandText = @"INSERT INTO L_AUTO_DRIVER_CHANGE (USER_NAME, LICENSE_TYPE, CHG_DATETIME, DRIVER_CODE, DRVSTATUS_SAP, DRVSTATUS_TMS, PERS_CODE, LICENSE_NO
                                                                                   , VALID_FROM, VALID_TO, STEL, STEL2, STRANS_ID, FIRST_NAME, LAST_NAME, STATUS, MESSAGE)
                                                   VALUES (:I_USER_NAME, :I_LICENSE_TYPE, :I_CHG_DATETIME, :I_DRIVER_CODE, :I_DRVSTATUS_SAP, :I_DRVSTATUS_TMS, :I_PERS_CODE, :I_LICENSE_NO
                                                                                   , :I_VALID_FROM, :I_VALID_TO, :I_STEL, :I_STEL2, :I_STRANS_ID, :I_FIRST_NAME, :I_LAST_NAME, :I_STATUS, :I_MESSAGE)";

                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    cmdDriverLogInsert.Parameters["I_USER_NAME"].Value = dtData.Rows[i]["USER_NAME"].ToString();
                    cmdDriverLogInsert.Parameters["I_LICENSE_TYPE"].Value = dtData.Rows[i]["LICENSE_TYPE"].ToString();
                    cmdDriverLogInsert.Parameters["I_CHG_DATETIME"].Value = dtData.Rows[i]["CHG_DATETIME"].ToString();
                    cmdDriverLogInsert.Parameters["I_DRIVER_CODE"].Value = dtData.Rows[i]["DRIVER_CODE"].ToString();
                    cmdDriverLogInsert.Parameters["I_DRVSTATUS_SAP"].Value = dtData.Rows[i]["DRVSTATUS_SAP"].ToString();
                    cmdDriverLogInsert.Parameters["I_DRVSTATUS_TMS"].Value = dtData.Rows[i]["DRVSTATUS_TMS"].ToString();
                    cmdDriverLogInsert.Parameters["I_PERS_CODE"].Value = dtData.Rows[i]["PERS_CODE"].ToString();
                    cmdDriverLogInsert.Parameters["I_LICENSE_NO"].Value = dtData.Rows[i]["LICENSE_NO"].ToString();
                    cmdDriverLogInsert.Parameters["I_VALID_FROM"].Value = dtData.Rows[i]["VALID_FROM"].ToString();
                    cmdDriverLogInsert.Parameters["I_VALID_TO"].Value = dtData.Rows[i]["VALID_TO"].ToString();
                    cmdDriverLogInsert.Parameters["I_STEL"].Value = dtData.Rows[i]["STEL"].ToString();
                    cmdDriverLogInsert.Parameters["I_STEL2"].Value = dtData.Rows[i]["STEL2"].ToString();
                    cmdDriverLogInsert.Parameters["I_STRANS_ID"].Value = dtData.Rows[i]["STRANS_ID"].ToString();
                    cmdDriverLogInsert.Parameters["I_FIRST_NAME"].Value = dtData.Rows[i]["FIRST_NAME"].ToString();
                    cmdDriverLogInsert.Parameters["I_LAST_NAME"].Value = dtData.Rows[i]["LAST_NAME"].ToString();
                    cmdDriverLogInsert.Parameters["I_STATUS"].Value = dtData.Rows[i]["STATUS"].ToString();
                    cmdDriverLogInsert.Parameters["I_MESSAGE"].Value = dtData.Rows[i]["MESSAGE"].ToString();

                    dbManager.ExecuteNonQuery(cmdDriverLogInsert);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable DriverLogSelectDAL(string Condition, string DateStart, string DateEnd)
        {
            string Query = cmdDriverLogSelect.CommandText;
            try
            {
                dbManager.Open();

                cmdDriverLogSelect.CommandText = @"SELECT ID, USER_NAME
                                                         , TO_CHAR(TO_DATE(CHG_DATETIME, 'MM/DD/YYYY HH:MI:SS AM', 'NLS_DATE_LANGUAGE = ENGLISH'), 'DD/MM/YYYY HH:MI:SS AM', 'NLS_DATE_LANGUAGE = ENGLISH') AS CHG_DATETIME
                                                         , DRIVER_CODE, SAP_STATUS.STATUS_NAME AS DRVSTATUS_SAP, TMS_STATUS.STATUS_NAME AS DRVSTATUS_TMS, PERS_CODE, STATUS, MESSAGE
                                                         , TO_CHAR(CREATE_DATETIME, 'DD/MM/YYYY HH:MI:SS AM', 'NLS_DATE_LANGUAGE = ENGLISH') AS CREATE_DATETIME
                                                    FROM L_AUTO_DRIVER_CHANGE LEFT JOIN M_MASTER_STATUS SAP_STATUS ON L_AUTO_DRIVER_CHANGE.DRVSTATUS_SAP = SAP_STATUS.STATUS_VALUE AND SAP_STATUS.STATUS_TYPE = 'DRIVER'
                                                                              LEFT JOIN M_MASTER_STATUS TMS_STATUS ON L_AUTO_DRIVER_CHANGE.DRVSTATUS_TMS = TMS_STATUS.STATUS_VALUE AND TMS_STATUS.STATUS_TYPE = 'DRIVER'
                                                    WHERE 1=1" + Condition +
                                                 @" AND TO_CHAR(TO_DATE(CHG_DATETIME, 'MM/DD/YYYY HH:MI:SS AM', 'NLS_DATE_LANGUAGE = ENGLISH'), 'YYYYMMDD', 'NLS_DATE_LANGUAGE = ENGLISH')
                                                        >=:I_DATE_START
                                                    AND TO_CHAR(TO_DATE(CHG_DATETIME, 'MM/DD/YYYY HH:MI:SS AM', 'NLS_DATE_LANGUAGE = ENGLISH'), 'YYYYMMDD', 'NLS_DATE_LANGUAGE = ENGLISH')
                                                        <=:I_DATE_END
                                                    ORDER BY CREATE_DATETIME DESC";

                cmdDriverLogSelect.Parameters["I_DATE_START"].Value = DateStart;
                cmdDriverLogSelect.Parameters["I_DATE_END"].Value = DateEnd;

                cmdDriverLogSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdDriverLogSelect, "cmdDriverLogSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
                cmdDriverLogSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static AutoStatusDAL _instance;
        public static AutoStatusDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                _instance = new AutoStatusDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}