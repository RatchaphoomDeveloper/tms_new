﻿using System;
using System.ComponentModel;
using TMS_DAL.ConnectionDAL;
using System.Data;
using System.Data.OracleClient;

namespace TMS_DAL.Transaction.ContractConfirm
{
    public partial class ContractConfirmDAL : OracleConnectionDAL
    {
        public ContractConfirmDAL()
        {
            InitializeComponent();
        }

        public ContractConfirmDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable SelectTruckIDDAL(string Condition)
        {
            string Query = cmdSelectTruckID.CommandText;
            try
            {
                dbManager.Open();
                cmdSelectTruckID.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdSelectTruckID, "cmdSelectTruckID");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdSelectTruckID.CommandText = Query;
                dbManager.Close();
            }
        }

        public DataTable SelectTruckConfirmDAL(string User, string DateStart, string DateEnd, string ContractNo, string CConfirm)
        {
            try
            {
                dbManager.Open();
                cmdSelectConfirmTruck.CommandText = @"SELECT  rownum SKEYID, CONT.NCONFIRMID, CONT.SCONTRACTID, CONT.DDATE, CONT.DDATE+1 DEV_DATE,truk.NONHAND NONHANDSYSTEM,truk.NSTANDBY NSTANDBYSYSTEM,CONT.NTRUCK,CONT.NUNAVAILABLE,CONT.NTRANSIT,CONT.NAVAILABLE,CONT.NAVAILABLE + CONT.NTRANSIT NSUM,/*NVL(truk.NONHAND,0)*/NVL(CONT.NTRUCK,0) NONHAND,NVL(CONT.NOUTHAND,0) NOUTHAND,CONT.CCONFIRM,CONT.COIL,CONT.CGAS
                                                        ,CASE WHEN NVL(conflst.NCONFIRM,0)=0 THEN  NVL(truk.NONHAND,0)-NVL(truk.NHOLD,0) ELSE NVL(conflst.NCONFIRM,0)  END NCONFIRM
                                                        ,CASE WHEN CONT.CCONFIRM='1' THEN 'ส่งข้อมูลให้ ปตท.' ELSE 'รอส่งข้อมูล' END SCONFIRM
                                                        ,CONT.CACTIVE,CONT.SREMARK,CONT.TCARCONFIRM,CONT.TPLANCONFIRM,truk.NREJECT ,CONT.DBEGIN,CONT.DEND, CONT.DEXPIRE,CONT.SCONTRACTNO
                                                         , (NVL(TRUK.NINACTIVE,0)+ NVL(chktrck.NISSUE,0)) NISSUE,(NVL(TRUK.NINACTIVE,0)+NVL(NHOLD,0)) NHOLD,NVL(NBAN,0) NBAN,
cont.SABBREVIATION,cont.PTT_LOCK
                                                        FROM( 
                                                                 SELECT conf.NCONFIRMID,conf.SCONTRACTID,conf.DDATE,ntruckcontract NTRUCK,conf.NONHAND,ntruckreserve NOUTHAND,NVL(conf.CCONFIRM,'0') CCONFIRM,conf.DCONFIRM,conf.CACTIVE,conf.SREMARK ,TERM.TCARCONFIRM,term.TPLANCONFIRM,CONT.DBEGIN,CONT.DEND, CONF.DEXPIRE,CONT.SCONTRACTNO,CONT.COIL,CONT.CGAS,conf.NUNAVAILABLE,conf.NTRANSIT,conf.NAVAILABLE  ,TTERMINALNAME.SABBREVIATION,conf.PTT_LOCK
                                                                FROM  TTRUCKCONFIRM conf 
                                                                LEFT JOIN TCONTRACT cont ON conf.SCONTRACTID=cont.SCONTRACTID 
                                                               LEFT JOIN (SELECT * FROM TUSER WHERE 1=1 and SUID=:usr_SUID ) usr ON cont.SVENDORID=usr.SVENDORID 
                                                                LEFT JOIN TTERMINAL term ON cont.STERMINALID=TERM.STERMINALID 
LEFT JOIN  (SELECT scontractid,
       LTRIM(MAX(SYS_CONNECT_BY_PATH(SABBREVIATION,' ,'))
       KEEP (DENSE_RANK LAST ORDER BY curr),' ,') AS SABBREVIATION
From (SELECT TCONTRACT_PLANT.scontractid
  ,TTERMINAL.sabbreviation,
  ROW_NUMBER() OVER (PARTITION BY TCONTRACT_PLANT.scontractid ORDER BY SABBREVIATION) AS curr,
               ROW_NUMBER() OVER (PARTITION BY TCONTRACT_PLANT.scontractid ORDER BY SABBREVIATION) -1 AS prev
  FROM TCONTRACT_PLANT
  LEFT JOIN TTERMINAL ON TTERMINAL.STERMINALID = TCONTRACT_PLANT.STERMINALID
  WHERE tcontract_plant.cactive = '1')
  GROUP BY scontractid
  CONNECT BY prev = PRIOR curr AND scontractid = PRIOR scontractid
START WITH curr = 1) TTERMINALNAME ON TTERMINALNAME.scontractid = cont.scontractid
                                                                WHERE 1=1 AND  NVL(cont.ntruck,0) != 0  AND CONT.CSPACIALCONTRAC='N' AND NVL(CONT.CACTIVE,'Y') !='N' 
                                                                AND usr.SUID=:usr_SUID 
                                                                AND DEXPIRE BETWEEN TO_DATE(:DSTART, 'dd/mm/yyyy hh24:mi:ss') AND TO_DATE(:DEND, 'dd/mm/yyyy hh24:mi:ss')
                                                            ) cont 
                                                            LEFT JOIN ( 
                                                                SELECT truk.SCONTRACTID ,SUM(CASE WHEN NVL(truk.CSTANDBY,'N')='Y' THEN 0 ELSE 1 END) NONHAND ,SUM(CASE WHEN NVL(truk.CSTANDBY,'N')='Y' THEN 1 ELSE 0 END) NSTANDBY ,SUM(CASE WHEN NVL(truk.CREJECT,'N')='Y' THEN 1 ELSE 0 END) NREJECT ,SUM( CASE WHEN NVL(truk.CSTANDBY,'N')='N' THEN (CASE WHEN NVL(TRCK.CACTIVE,'N')='N' THEN 1 ELSE 0 END) ELSE  0 END ) NINACTIVE
                                                                  ,SUM(
                                                                NVL( CASE WHEN NVL(truk.CSTANDBY,'N')='N' THEN
                                                                    CASE WHEN  NVL(TRCK.CHOLD,'0') ='1' THEN 1 ELSE (CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 0 ELSE 1 END) END
                                                               ELSE 0 END ,0)
                                                                ) NHOLD  
                                                                 ,SUM(
                                                                NVL(CASE WHEN NVL(truk.CSTANDBY,'N')='Y' THEN
                                                                    CASE WHEN NVL(TRCK.CHOLD,'0')='0' THEN 
                                                                        CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 0 ELSE 1 END
                                                                    ELSE 1 END
                                                                 ELSE 0 END,0)
                                                                ) NHOLD_OUT
                                                                ,SUM(CASE WHEN NVL(TRCK.CHOLD,'0') ='2' THEN 1 ELSE 0 END) NBAN
                                                                FROM TCONTRACT_TRUCK truk 
                                                                LEFT JOIN TTRUCK trck ON  TRUK.STRUCKID=TRCK.STRUCKID
                                                                LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(trck.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
                                                                WHERE 1=1 --AND NVL(trck.CACTIVE,'N') ='Y' 
                                                                GROUP BY  truk.SCONTRACTID  
                                                            ) truk ON cont.SCONTRACTID=truk.SCONTRACTID
                                                            LEFT JOIN (  
                                                                SELECT SCONTRACTID,SUM(nDFINAL_MA+nCMA) NISSUE FROM 
                                                                ( 
                                                                    SELECT TCheckTruck.SCONTRACTID  ,(CASE WHEN MIN(TCheckTruck.DFINAL_MA) < SYSDATE THEN 1 ELSE 0 END)  nDFINAL_MA ,(CASE WHEN TCheckTruck.CMA='2' THEN 1 ELSE 0 END) nCMA  
                                                                    FROM TCheckTruck 
                                                                    LEFT JOIN TCONTRACT_TRUCK  ON TCheckTruck.SCONTRACTID=TCONTRACT_TRUCK.SCONTRACTID AND TCheckTruck.STRUCKID=TCONTRACT_TRUCK.STRUCKID
                                                                    LEFT JOIN TCheckTruckITEM  ON TCheckTruck.SCHECKID=TCheckTruckITEM.SCHECKID
                                                                    WHERE 1=1 AND TCheckTruck.cClean='0' AND NVL(TCONTRACT_TRUCK.CSTANDBY,'N')='N' AND NVL(TCheckTruckITEM.COTHER,'0')!='1'
                                                                    GROUP BY TCheckTruck.SCONTRACTID,TCheckTruck.CMA
                                                                ) ctrck GROUP BY SCONTRACTID 
                                                            ) chktrck  ON cont.SCONTRACTID=chktrck.SCONTRACTID 
                                                            LEFT JOIN (
                                                                SELECT NCONFIRMID ,COUNT(NCONFIRMID) NCONFIRM FROM TTRUCKCONFIRMLIST WHERE 1=1 AND CCONFIRM='1'   GROUP BY NCONFIRMID
                                                            ) conflst ON cont.NCONFIRMID=conflst.NCONFIRMID
                                                            WHERE 1=1 
                                                            AND CONT.scontractno LIKE '%'|| NVL(:CONT_SCONTRACTNO,CONT.scontractno) ||'%'  AND NVL(CONT.CCONFIRM,'0') LIKE '%' || NVL(:CCONFIRM,CONT.CCONFIRM) || '%' 
                                                            AND DEXPIRE BETWEEN TO_DATE(:DSTART, 'dd/mm/yyyy hh24:mi:ss') AND TO_DATE(:DEND, 'dd/mm/yyyy hh24:mi:ss') ORDER BY  CONT.SCONTRACTNO";

                cmdSelectConfirmTruck.Parameters["usr_SUID"].Value = User;
                cmdSelectConfirmTruck.Parameters["DSTART"].Value = DateStart;
                cmdSelectConfirmTruck.Parameters["DEND"].Value = DateEnd;
                cmdSelectConfirmTruck.Parameters["CONT_SCONTRACTNO"].Value = ContractNo;
                cmdSelectConfirmTruck.Parameters["CCONFIRM"].Value = CConfirm;

                return dbManager.ExecuteDataTable(cmdSelectConfirmTruck, "cmdSelectConfirmTruck");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable SelectTruckConfirmPTTDAL(string VendorID, string DEXPIRE, string ContractNo, string CConfirm, string SHEADREGISTERNO, string STERMINALID, string CSTANBY)
        {
            try
            {
                dbManager.Open();
                #region CommandText
                cmdSelectConfirmTruckPTT.CommandType = CommandType.StoredProcedure;
                cmdSelectConfirmTruckPTT.CommandText = @"USP_T_TRUCKCONFIRM_SELECT_PTT";
                #endregion
                

                cmdSelectConfirmTruckPTT.Parameters["I_SVENDORID"].Value = VendorID;
                cmdSelectConfirmTruckPTT.Parameters["I_DEXPIRE"].Value = DEXPIRE;
                cmdSelectConfirmTruckPTT.Parameters["I_CONT_SCONTRACTNO"].Value = ContractNo;
                cmdSelectConfirmTruckPTT.Parameters["I_CCONFIRM"].Value = CConfirm;
                cmdSelectConfirmTruckPTT.Parameters["I_SHEADREGISTERNO"].Value = SHEADREGISTERNO;
                cmdSelectConfirmTruckPTT.Parameters["I_CSTANBY"].Value = CSTANBY;
                cmdSelectConfirmTruckPTT.Parameters["I_STERMINALID"].Value = string.IsNullOrEmpty(STERMINALID) ? "0" : STERMINALID;
                return dbManager.ExecuteDataTable(cmdSelectConfirmTruckPTT, "cmdSelectConfirmTruckPTT");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void UpdateConfirmTruckDAL(string NConfirmID, string CConfirm)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUpdateConfirmTruck.Parameters["I_NCONFIRMID"].Value = NConfirmID;
                cmdUpdateConfirmTruck.Parameters["I_CCONFIRM"].Value = CConfirm;

                dbManager.ExecuteNonQuery(cmdUpdateConfirmTruck);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public bool UpdateTruckConfirmCount(string NCONFIRMID, int NUNAVAILABLE, int NTRANSIT, int NAVAILABLE)
        {
            bool isRes = false;
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();
                cmdUpdateTruckConfirmCount.CommandType = CommandType.Text;
                cmdUpdateTruckConfirmCount.CommandText = @"
Update  TTRUCKCONFIRM 
SET NAVAILABLE = :NAVAILABLE, NUNAVAILABLE = :NUNAVAILABLE, NTRANSIT = :NTRANSIT
WHERE NCONFIRMID = :NCONFIRMID

";
                cmdUpdateTruckConfirmCount.Parameters.Clear();
                cmdUpdateTruckConfirmCount.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "NCONFIRMID",
                    OracleType = OracleType.VarChar,
                    IsNullable = true,
                    Value = NCONFIRMID,
                });
                cmdUpdateTruckConfirmCount.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "NUNAVAILABLE",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                    Value = NUNAVAILABLE,
                });
                cmdUpdateTruckConfirmCount.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "NAVAILABLE",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                    Value = NAVAILABLE,
                });
                cmdUpdateTruckConfirmCount.Parameters.Add(new OracleParameter()
                {
                    ParameterName = "NTRANSIT",
                    OracleType = OracleType.Number,
                    IsNullable = true,
                    Value = NTRANSIT,
                });


                int row = dbManager.ExecuteNonQuery(cmdUpdateTruckConfirmCount);
                if (row > 0)
                {
                    isRes = true;
                }
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
                
            }
            finally
            {
                dbManager.Close();
            }
            return isRes;
        }

        #region GetTcontractTruck
        public DataTable GetTcontractTruck(string I_CONTRACT_ID)
        {
            try
            {
                dbManager.Open();
                cmdContractConfirm.CommandType = CommandType.Text;
                cmdContractConfirm.CommandText = @"SELECT NULL CARTYPE
  ,NULL DELIVERY_PART
   , VEH.SHEADREGISTERNO
 , TU.SHEADREGISTERNO STRAILERREGISTERNO
  ,NULL CAR_STATUS
,NULL WAREHOUSE
  ,NULL DELIVERY_NUM
  ,NULL GPS_STATUS
  ,NULL CCTV_STATUS
  ,cont_trck.struckid
  ,NULL CSTANBY
  ,cstandby TTRUCK_TYPE_NAME
  ,NULL CAR_STATUS_ID
  ,NULL STERMINALID
  ,NULL DELIVERY_NUM
  ,NULL REMARK
  ,0 CPASSED
 ,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.CHOLD,'0')='2' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN (CASE WHEN NVL(TRCK.CHOLD,'0')='2' THEN '3' ELSE '2' END) ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END CMA

  ,0 IS_CONFIRM
  ,NULL SCARTYPENAME
  ,NULL DDATE
  ,cont_trck.scontractid
  ,tu.struckid STRAILERID
  ,NULL DFINAL_MA
  ,NULL NDAY_MA
  ,NULL CPASSED
  ,NULL TCONFLST_CCONFIRM
  ,NULL PERSONAL_CODE
,NULL IVMS_GPS_STAT
,NULL IVMS_MDVR_STAT,NULL IVMS_STATUS,NULL IVMS_RADIUS_KM,NULL IVMS_CHECKDATE
FROM TCONTRACT_TRUCK CONT_TRCK
LEFT JOIN TTRUCK VEH ON CONT_TRCK.STRUCKID = VEH.STRUCKID
LEFT JOIN TTRUCK TU ON CONT_TRCK.STRAILERID = TU.STRUCKID
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --CACTIVE='Y'
) trck ON  VEH.STRUCKID=trck.STRUCKID
  LEFT JOIN 
(
   SELECT chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO , MAX(chktrk.CMA) CMA,MIN(DBEGIN_MA) DBEGIN_MA,MIN(DFINAL_MA) DFINAL_MA,MIN(chktrk.NDAY_MA) NDAY_MA,COUNT(STRUCKID) nClean
   FROM TCheckTruck  chktrk  LEFT JOIN TCheckTruckITEM ichktrk ON chktrk.SCHECKID=ichktrk.SCHECKID
   WHERE 1=1 AND cClean='0' AND NVL(ichktrk.COTHER,'0')!='1'
   GROUP BY chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO --,chktrk.CMA
) chktrck ON  CONT_TRCK.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=CONT_TRCK.STRUCKID
WHERE (CONT_TRCK.SCONTRACTID = :I_CONTRACT_ID AND CONT_TRCK.REF_SCONTRACTID IS NULL) OR CONT_TRCK.REF_SCONTRACTID = :I_CONTRACT_ID
  ORDER BY VEH.SHEADREGISTERNO";

                cmdContractConfirm.Parameters.Clear();
                cmdContractConfirm.Parameters.Add(new OracleParameter() { 
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    Value = I_CONTRACT_ID
                });

                return dbManager.ExecuteDataTable(cmdContractConfirm, "cmdContractConfirm");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GetTruckBySContractid
        public DataTable GetTruckBySContractid(string SCONTRACTID, string STERMINALID, string SHEADREGISTERNO, string DDATE, string CSTANBY)
        {
            try
            {
                dbManager.Open();
                #region CommandText
                cmdContractConfirm.CommandType = CommandType.StoredProcedure;
                cmdContractConfirm.CommandText = @"USP_T_TRUCKBYSCID_SELECT_PTT";
                #endregion
                
                cmdContractConfirm.Parameters.Clear();
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SCONTRACTID",
                    Value = SCONTRACTID
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STERMINALID",
                    Value = string.IsNullOrEmpty(STERMINALID) ? "0" : STERMINALID
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SHEADREGISTERNO",
                    Value = SHEADREGISTERNO
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DDATE",
                    Value = DDATE
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CSTANBY",
                    Value = CSTANBY
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.Cursor,
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR"
                });
                return dbManager.ExecuteDataTable(cmdContractConfirm, "cmdContractConfirm");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GetTruckBySContractidImport
        public DataTable GetTruckBySContractidImport(string SCONTRACTID, string STERMINALID, string SHEADREGISTERNO, string DDATE)
        {
            try
            {
                dbManager.Open();
                #region CommandText
                cmdContractConfirm.CommandType = CommandType.Text;
                cmdContractConfirm.CommandText = @"SELECT CASE WHEN NVL(CONF.CSTANBY,'N') = 'Y' THEN 'รถปกติ' ELSE 'รถหมุนเวียน' END AS CARTYPE
  , CONF.DELIVERY_PART
  ,TRCK.SHEADREGISTERNO
   ,CASE WHEN NVL(CTRK.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
  ,CASE NVL(CTRK.CSTANDBY,'N')
     WHEN 'N' THEN 'ในสัญญา'
     ELSE 'สำรอง'
   END CSTANDBY
   , CONF.CAR_STATUS
, CONF.SABBREVIATION AS WAREHOUSE
, CONF.DELIVERY_NUM
, CONF.GPS_STATUS
, CONF.CCTV_STATUS
, CONF.remark
,CASE (CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.CHOLD,'0')='2' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN (CASE WHEN NVL(TRCK.CHOLD,'0')='2' THEN '3' ELSE '2' END) ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END) 
   WHEN '3'  THEN 'ตกค้าง'
  WHEN '2'  THEN 'ห้ามวิ่ง'
  WHEN '1'  THEN 'แก้ไขภายใน' || NVL(nday_ma,0) ||  ' วัน'
  WHEN '0'  THEN 'ปกติ'
   ELSE 'ปกติ'
 END 
   CMA
,CASE NVL(CONF.TCONFLST_CCONFIRM,'0')
   WHEN '0' THEN 'ปฏิเสธ'
   ELSE 'ยืนยัน'
 END IS_CONFIRM
FROM 
( 
  SELECT * 
  FROM TCONTRACT
  WHERE 1=1 AND CACTIVE='Y' AND SCONTRACTID=:SCONTRACTID
) cont
LEFT JOIN TCONTRACT_TRUCK ctrk ON cont.SCONTRACTID= ctrk.SCONTRACTID
LEFT JOIN 
(
   SELECT chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO , MAX(chktrk.CMA) CMA,MIN(DBEGIN_MA) DBEGIN_MA,MIN(DFINAL_MA) DFINAL_MA,MIN(chktrk.NDAY_MA) NDAY_MA,COUNT(STRUCKID) nClean
   FROM TCheckTruck  chktrk  LEFT JOIN TCheckTruckITEM ichktrk ON chktrk.SCHECKID=ichktrk.SCHECKID
   WHERE 1=1 AND cClean='0' AND NVL(ichktrk.COTHER,'0')!='1'
   GROUP BY chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO --,chktrk.CMA
) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=ctrk.STRUCKID
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE CACTIVE='Y'
) trck ON  CTRK.STRUCKID=trck.STRUCKID
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --ANDCACTIVE='Y'
) trcktail ON  CTRK.STRAILERID=trcktail.STRUCKID
LEFT JOIN 
(
  SELECT tconf.NCONFIRMID ,tconf.SCONTRACTID  ,tconf.ddate,tconf.cconfirm tconf_cconfirm
  ,tconflst.nlistno ,tconflst.cconfirm tconflst_cconfirm ,tconflst.struckid ,tconflst.sheadid ,tconflst.sheadregisterno ,tconflst.strailerid ,tconflst.strailerregisterno ,tconflst.cstanby ,tconflst.cpassed
  , tconflst.DELIVERY_PART, M_CAR_CONFIRM_STATUS.CAR_STATUS_NAME AS CAR_STATUS,TTERMINAL.STERMINALID, TTERMINAL.SABBREVIATION, tconflst.DELIVERY_NUM, tconflst.GPS_STATUS, tconflst.CCTV_STATUS
  , tconflst.LAT_LONG, tconflst.KM2PLANT, tconflst.HR2PLANT, tconflst.ADDRESS_CURRENT, tconflst.REMARK, tconflst.PERSONAL_CODE
 FROM ttruckconfirm tconf 
  LEFT JOIN ttruckconfirmlist tconflst ON tconf.nconfirmid=tconflst.nconfirmid
  LEFT JOIN M_CAR_CONFIRM_STATUS ON tconflst.CAR_STATUS_ID = M_CAR_CONFIRM_STATUS.CAR_STATUS_ID
  LEFT JOIN TTERMINAL ON tconflst.STERMINALID = TTERMINAL.STERMINALID
  where tconf.scontractid=:SCONTRACTID AND tconf.DDATE=TO_DATE(:DDATE,'DD/MM/YYYY')
) conf ON CONT.SCONTRACTID=conf.SCONTRACTID and ctrk.struckid=conf.struckid
WHERE 1=1 AND TRCK.CACTIVE='Y' --AND  NVL(CTRK.CSTANDBY,'N')='N'
AND cont.SCONTRACTID=:SCONTRACTID  AND (CONF.STERMINALID = :STERMINALID OR (trim(:STERMINALID) = 0 AND 0=0))
  AND (conf.SHEADREGISTERNO LIKE :SHEADREGISTERNO OR conf.STRAILERREGISTERNO LIKE :SHEADREGISTERNO)
GROUP BY CTRK.STRUCKID,TRCK.SHEADREGISTERNO,CTRK.STRAILERID,TRCK.STRAILERREGISTERNO,NDAY_MA,CONF.cpassed,CONF.TCONFLST_CCONFIRM,TRCK.SCARTYPENAME,NVL(CTRK.CSTANDBY,'N'),CONT.SCONTRACTID
,CASE (CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.CHOLD,'0')='2' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN (CASE WHEN NVL(TRCK.CHOLD,'0')='2' THEN '3' ELSE '2' END) ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END) 
   WHEN '3'  THEN 'ตกค้าง'
  WHEN '2'  THEN 'ห้ามวิ่ง'
  WHEN '1'  THEN 'แก้ไขภายใน' || NVL(nday_ma,0) ||  ' วัน'
  WHEN '0'  THEN 'ปกติ'
   ELSE 'ปกติ'
 END ,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO
, CONF.DELIVERY_PART, CONF.CAR_STATUS, CONF.SABBREVIATION, CONF.DELIVERY_NUM, CONF.GPS_STATUS, CONF.CCTV_STATUS
, CONF.LAT_LONG, CONF.KM2PLANT, CONF.HR2PLANT, CONF.ADDRESS_CURRENT, CONF.REMARK, CONF.PERSONAL_CODE ,CONF.STERMINALID,NVL(CONF.CSTANBY,'N') ORDER BY TRCK.SHEADREGISTERNO";
                #endregion

                cmdContractConfirm.Parameters.Clear();
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "SCONTRACTID",
                    Value = SCONTRACTID
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "STERMINALID",
                    Value = string.IsNullOrEmpty(STERMINALID) ? "0" : STERMINALID
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "SHEADREGISTERNO",
                    Value = SHEADREGISTERNO
                });
                cmdContractConfirm.Parameters.Add(new OracleParameter()
                {
                    OracleType = OracleType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "DDATE",
                    Value = DDATE
                });

                return dbManager.ExecuteDataTable(cmdContractConfirm, "cmdContractConfirm");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GetMcarConfirmStatus
        public DataTable GetMcarConfirmStatus()
        {
            try
            {
                dbManager.Open();
                #region CommandText
                cmdContractConfirm.CommandType = CommandType.Text;
                cmdContractConfirm.CommandText = @"SELECT * FROM M_CAR_CONFIRM_STATUS Where CAR_ACTIVE = 1 ORDER BY LISTNO";
                #endregion

                cmdContractConfirm.Parameters.Clear();
                return dbManager.ExecuteDataTable(cmdContractConfirm, "cmdContractConfirm");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region SaveTtruckConfirmList
        public bool SaveTtruckConfirmList(string UserID, int NCONFIRMID, DataTable TTRUCKCONFIRMLIST)
        {

            bool IsRes = false;
            DataSet dsTTRUCKCONFIRMLIST = new DataSet("DS");
            DataTable dtI = TTRUCKCONFIRMLIST.Copy();
            dtI.TableName = "DT";
            dsTTRUCKCONFIRMLIST.Tables.Add(dtI);

            cmdContractConfirm.CommandType = CommandType.StoredProcedure;
            cmdContractConfirm.CommandText = "USP_T_TTRUCKCONFIRMLIST";
            cmdContractConfirm.Parameters.Clear();
            cmdContractConfirm.Parameters.Add(new OracleParameter() {
                ParameterName = "I_USERID",
                Value = UserID,
                OracleType = OracleType.VarChar
            });
            cmdContractConfirm.Parameters.Add(new OracleParameter()
            {
                ParameterName = "I_NCONFIRMID",
                Value = NCONFIRMID,
                OracleType = OracleType.Number
            });
            cmdContractConfirm.Parameters.Add(new OracleParameter()
            {
                ParameterName = "I_TTRUCKCONFIRMLIST",
                Value = dsTTRUCKCONFIRMLIST.GetXml(),
                OracleType = OracleType.Clob
            });

            
            try
            {
                dbManager.Dispose();
                dbManager.Open();
                dbManager.BeginTransaction();
                dbManager.ExecuteNonQuery(cmdContractConfirm);
                dbManager.CommitTransaction();
                IsRes = true;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
            return IsRes;
        }
        #endregion

        #region + Instance +
        private static ContractConfirmDAL _instance;
        public static ContractConfirmDAL Instance
        {
            get
            {
                //if (_instance == null)
                //{
                    _instance = new ContractConfirmDAL();
                //}
                return _instance;
            }
        }
        #endregion
    }
}