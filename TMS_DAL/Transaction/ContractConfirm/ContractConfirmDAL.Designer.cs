﻿namespace TMS_DAL.Transaction.ContractConfirm
{
    partial class ContractConfirmDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdSelectTruckID = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectConfirmTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateConfirmTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdSelectConfirmTruckPTT = new System.Data.OracleClient.OracleCommand();
            this.cmdUpdateTruckConfirmCount = new System.Data.OracleClient.OracleCommand();
            this.cmdContractConfirm = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdSelectTruckID
            // 
            this.cmdSelectTruckID.CommandText = "SELECT STRUCKID FROM TTRUCK WHERE CACTIVE = \'Y\'";
            this.cmdSelectTruckID.Connection = this.OracleConn;
            this.cmdSelectTruckID.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("SVENDORID", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdSelectConfirmTruck
            // 
            this.cmdSelectConfirmTruck.Connection = this.OracleConn;
            this.cmdSelectConfirmTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("usr_SUID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("DSTART", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("DEND", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("CONT_SCONTRACTNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("CCONFIRM", System.Data.OracleClient.OracleType.VarChar, 50)});
            // 
            // cmdUpdateConfirmTruck
            // 
            this.cmdUpdateConfirmTruck.CommandText = "UPDATE TTRUCKCONFIRM SET CCONFIRM = :I_CCONFIRM, DUPDATE = SYSDATE WHERE NCONFIRM" +
    "ID = :I_NCONFIRMID";
            this.cmdUpdateConfirmTruck.Connection = this.OracleConn;
            this.cmdUpdateConfirmTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_NCONFIRMID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CCONFIRM", System.Data.OracleClient.OracleType.VarChar, 10)});
            // 
            // cmdSelectConfirmTruckPTT
            // 
            this.cmdSelectConfirmTruckPTT.Connection = this.OracleConn;
            this.cmdSelectConfirmTruckPTT.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_SVENDORID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_DEXPIRE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CONT_SCONTRACTNO", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CCONFIRM", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_SHEADREGISTERNO", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_STERMINALID", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_CSTANBY", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdUpdateTruckConfirmCount
            // 
            this.cmdUpdateTruckConfirmCount.Connection = this.OracleConn;
            // 
            // cmdContractConfirm
            // 
            this.cmdContractConfirm.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdSelectTruckID;
        private System.Data.OracleClient.OracleCommand cmdSelectConfirmTruck;
        private System.Data.OracleClient.OracleCommand cmdUpdateConfirmTruck;
        private System.Data.OracleClient.OracleCommand cmdSelectConfirmTruckPTT;
        private System.Data.OracleClient.OracleCommand cmdUpdateTruckConfirmCount;
        private System.Data.OracleClient.OracleCommand cmdContractConfirm;
    }
}
