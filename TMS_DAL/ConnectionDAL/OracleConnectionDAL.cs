﻿using System.ComponentModel;
using dbManager;

namespace TMS_DAL.ConnectionDAL
{
    public partial class OracleConnectionDAL : Component
    {
        public OracleConnectionDAL()
        {
            InitializeComponent();
        }

        public OracleConnectionDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DBManager dbManager = new DBManager(DataProvider.Oracle, Properties.Settings.Default.OracleConnectionString);
    }
}