﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TMS_DAL.ConnectionDAL;
using TMS_Entity;

namespace TMS_DAL.Common
{
    public partial class cmdCommonDAL : OracleConnectionDAL
    {
        public cmdCommonDAL()
        {
            InitializeComponent();
        }

        public cmdCommonDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        public bool ExecuteNonQuery(string statement, List<ParameterEntity> parameters, out string error_message)
        {
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdDtSelect.CommandType = CommandType.Text;
                cmdDtSelect.CommandText = statement;
                if (parameters != null)
                {
                    foreach (var item in parameters)
                    {
                        cmdDtSelect.Parameters.Add(item.sKey, item.sValue);
                    }
                }
                dbManager.ExecuteNonQuery(cmdDtSelect);
                if (dbManager.Connection.State == ConnectionState.Open)
                    dbManager.Close();
                error_message = null;
                return true;
            }
            catch (Exception ex)
            {
                error_message = ex.Message;
                return false;
            }
        }

        public int? GetCount(string statement, List<ParameterEntity> parameters, out string error_message)
        {
            int? result = 0;
            try
            {
                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdDtSelect.CommandType = CommandType.Text;
                cmdDtSelect.CommandText = statement;
                if (parameters != null)
                {
                    foreach (var item in parameters)
                    {
                        cmdDtSelect.Parameters.Add(item.sKey, item.sValue);
                    }
                }

                DataTable dt = dbManager.ExecuteDataTable(cmdDtSelect, "cmdDtSelect");
                if (dt.Rows.Count > 0)
                {
                    result = int.Parse(dt.Rows[0][0].ToString());
                }
                if (dbManager.Connection.State == ConnectionState.Open)
                    dbManager.Close();
                error_message = null;
            }
            catch (Exception ex)
            {
                
                 if (dbManager.Connection.State == ConnectionState.Open)
                    dbManager.Close();
                error_message = ex.Message;
            }

            return result;
        }
        public DataTable ExecuteAdaptor(string statement, List<ParameterEntity> parameters, out string error_message)
        {
            DataTable dt = null;
            try
            {

                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdDtSelect.CommandType = CommandType.Text;
                cmdDtSelect.CommandText = statement;
                if (parameters != null)
                {
                    foreach (var item in parameters)
                    {
                        cmdDtSelect.Parameters.Add(item.sKey, item.sValue);
                    }
                }

                dt = dbManager.ExecuteDataTable(cmdDtSelect, "cmdDtSelect");
                if (dbManager.Connection.State == ConnectionState.Open)
                    dbManager.Close();
                error_message = null;
            }
            catch (Exception ex)
            {
                if (dbManager.Connection.State == ConnectionState.Open)
                    dbManager.Close();
                error_message = ex.Message;
            }

            return dt;
        }


        public DataTable ExecuteAdaptor(string statement, Hashtable parameters, out string error_message)
        {
            DataTable dt = null;
            try
            {

                if (dbManager.Connection.State != ConnectionState.Open)
                    dbManager.Open();
                cmdDtSelect.CommandType = CommandType.Text;
                cmdDtSelect.CommandText = statement;
                if (parameters != null)
                {
                    foreach (string key in parameters.Keys)
                    {
                        cmdDtSelect.Parameters.Add(key, parameters[key]);
                        
                    }
                }

                dt = dbManager.ExecuteDataTable(cmdDtSelect, "cmdDtSelect");                
                if (dbManager.Connection.State == ConnectionState.Open)
                    dbManager.Close();
                error_message = null;
            }
            catch (Exception ex)
            {
                if (dbManager.Connection.State == ConnectionState.Open)
                    dbManager.Close();
                error_message = ex.Message;
            }

            return dt;
        }

        #region + Instance +
        private static cmdCommonDAL _instance;
        public static cmdCommonDAL Instance
        {
            get
            {
                _instance = new cmdCommonDAL();
                return _instance;
            }
        }
        #endregion
    }
}
