﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.AuthenUserGroupDAL
{
    public partial class UserGroupDAL : OracleConnectionDAL
    {
        public UserGroupDAL()
        {
            InitializeComponent();
        }

        public UserGroupDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UserGroupSelectDAL(string Condition)
        {
            string Query = cmdUserGroupSelect.CommandText;
            try
            {
                cmdUserGroupSelect.CommandText = @"SELECT MU.USERGROUP_ID, MU.USERGROUP_NAME, MU.IS_ADMIN, MU.IS_ACTIVE , SS.STATUS_NAME AS IS_ADMIN_TEXT
                                                   FROM M_USERGROUP MU
                                                   LEFT JOIN M_STATUS SS ON SS.STATUS_VALUE = MU.IS_ADMIN
                                                   WHERE 1=1 AND SS.STATUS_TYPE = 'SPECIAL_STATUS'   " + Condition + " ORDER BY IS_ADMIN  ";

                //cmdUserGroupSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUserGroupSelect, "cmdUserGroupSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserGroupSelect.CommandText = Query;
            }
        }


        public DataTable getUserGroupName(string Condition)
        {
            string Query = cmdgetUserGroupName.CommandText;
            try
            {
                cmdgetUserGroupName.CommandText = @"SELECT USERGROUP_NAME
                                                    FROM M_USERGROUP 
                                                    WHERE 1=1 " + Condition ;
              
                return dbManager.ExecuteDataTable(cmdgetUserGroupName, "cmdgetUserGroupName");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdgetUserGroupName.CommandText = Query;
            }
        }

        public void UserGroupAddDAL(int UserGroupID, string UserGroupName, string IsAdmin, string IsActive, int CreateBy, string TableSelect, string FieldSelect, string docPrefix)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUserGroupAdd.CommandType = CommandType.StoredProcedure;
                cmdUserGroupAdd.CommandText = "USP_M_USERGROUP";

                cmdUserGroupAdd.Parameters["I_USERGROUP_ID"].Value = UserGroupID;
                cmdUserGroupAdd.Parameters["I_USERGROUP_NAME"].Value = UserGroupName;
                cmdUserGroupAdd.Parameters["I_IS_ADMIN"].Value = IsAdmin;
                cmdUserGroupAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdUserGroupAdd.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdUserGroupAdd.Parameters["I_TABLE_SELECT"].Value = TableSelect;
                cmdUserGroupAdd.Parameters["I_FIELD_SELECT"].Value = FieldSelect;
                cmdUserGroupAdd.Parameters["I_DOC_PREFIX"].Value = docPrefix;
                //cmdUserGroupAdd.Parameters["I_SYSTEM_ID"].Value = SystemID;

                dbManager.ExecuteNonQuery(cmdUserGroupAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UserGroupDAL _instance;
        public static UserGroupDAL Instance
        {
            get
            {
                _instance = new UserGroupDAL();
                return _instance;
            }
        }
        #endregion

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdUserGroupSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdgetUserGroupName = new System.Data.OracleClient.OracleCommand();
            this.cmdUserGroupAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdUserGroupSelect
            // 
            this.cmdUserGroupSelect.Connection = this.OracleConn;
            // 
            // cmdgetUserGroupName
            // 
            this.cmdgetUserGroupName.Connection = this.OracleConn;
            // 
            // cmdUserGroupAdd
            // 
            this.cmdUserGroupAdd.Connection = this.OracleConn;
            this.cmdUserGroupAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_NAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_IS_ADMIN", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TABLE_SELECT", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_FIELD_SELECT", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("I_DOC_PREFIX", System.Data.OracleClient.OracleType.VarChar)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdUserGroupSelect;
        private System.Data.OracleClient.OracleCommand cmdgetUserGroupName;
        private System.Data.OracleClient.OracleCommand cmdUserGroupAdd;
    }
}