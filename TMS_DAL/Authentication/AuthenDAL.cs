﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using TMS_DAL.Common;
using TMS_DAL.ConnectionDAL;
using System.Data.OracleClient;

namespace TMS_DAL.Authentication
{
    public partial class AuthenDAL : OracleConnectionDAL
    {
        public AuthenDAL()
        {
            InitializeComponent();
        }

        public AuthenDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable AuthenSelectDAL(string UserGroupID)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();

                cmdAuthenSelect.CommandText = @"SELECT DISTINCT TMP4.*, CASE WHEN NVL(MENU_LEVEL2.MENU_ID, -1) = -1 THEN MENU_LEVEL1.QUERY_STRING ELSE MENU_LEVEL2.QUERY_STRING END AS QUERY_STRING
                                                FROM
                                                (
                                                SELECT TMP3.*, NVL(VISIBLE, 0) AS VISIBLE, NVL(READ, 0) AS READ,  NVL(WRITE, 0) AS WRITE , TMENU.SMENUORDER
                                                                                                FROM
                                                                                                (
                                                                                                    SELECT TMP2.MENU_ID, TMP2.MENU_NAME, TMP2.MENU_NAME_PTT, TMP2.MENU_ID_LEVEL1
                                                                                                            , TMP2.MENU_NAME_LEVEL1
                                                                                                            , TMP2.MENU_NAME_LEVEL1_PTT
                                                                                                            , M_MENU.MENU_ID AS MENU_ID_LEVEL2, TMP2.ROW_ORDER_LEVEL1
                                                                                                            , M_MENU.MENU_NAME AS MENU_NAME_LEVEL2
                                                                                                            , M_MENU.MENU_NAME_PTT AS MENU_NAME_LEVEL2_PTT
                                                                                                            , M_MENU.ROW_ORDER AS ROW_ORDER_LEVEL2
                                                                                                            , CASE WHEN NVL(M_MENU.MENU_URL, ' ') = ' ' THEN TMP2.MENU_URL ELSE M_MENU.MENU_URL END AS MENU_URL_TMS
                                                                                                            , CASE WHEN NVL(M_MENU.DESCRIPTION, ' ') = ' ' THEN TMP2.DESCRIPTION ELSE M_MENU.DESCRIPTION END AS DESCRIPTION
                                                            
                                                                                                    FROM
                                                                                                        (
                                                                                                            SELECT DESCRIPTION, M_MENU.MENU_URL, TMP1.MENU_ID, TMP1.MENU_NAME, TMP1.MENU_NAME_PTT, M_MENU.MENU_ID AS MENU_ID_LEVEL1, M_MENU.MENU_NAME AS MENU_NAME_LEVEL1, M_MENU.MENU_NAME_PTT AS MENU_NAME_LEVEL1_PTT, M_MENU.ROW_ORDER AS ROW_ORDER_LEVEL1
                                                                                                            FROM
                                                                                                            (
                                                                                                                SELECT MENU_ID, MENU_NAME, MENU_NAME_PTT, ROW_ORDER, MENU_URL
                                                                                                                FROM M_MENU
                                                                                                                WHERE MENU_LEVEL = 0
                                                                                                            ) TMP1 INNER JOIN M_MENU ON TMP1.MENU_ID = M_MENU.PARENT_MENU_ID 
                                                                                                            ) TMP2
                                                                                                    LEFT JOIN M_MENU ON TMP2.MENU_ID_LEVEL1 = M_MENU.PARENT_MENU_ID
                                                                                                ) TMP3                                                                                                
                                                                                                LEFT JOIN M_AUTHORIZATION ON M_AUTHORIZATION.MENU_ID = (CASE WHEN NVL(TMP3.MENU_ID_LEVEL2, -1) = -1 THEN TMP3.MENU_ID_LEVEL1 ELSE TMP3.MENU_ID_LEVEL2 END) 
                                                                                                AND M_AUTHORIZATION.USERGROUP_ID = ''|| :I_USERGROUP_ID ||'' 
                                                                                                JOIN TMENU ON UPPER(TMENU.SMENULINK) = UPPER(TMP3.MENU_URL_TMS) AND SMENUHEAD IS NULL  AND TMENU.CACTIVE = 1
                                                                                                ORDER BY MENU_NAME, MENU_ID_LEVEL1, ROW_ORDER_LEVEL1, MENU_ID_LEVEL2, ROW_ORDER_LEVEL2
                                                ) TMP4
                                                LEFT JOIN M_MENU MENU_LEVEL1 ON MENU_LEVEL1.MENU_ID = TMP4.MENU_ID_LEVEL1
                                                LEFT JOIN M_MENU MENU_LEVEL2 ON MENU_LEVEL2.MENU_ID = TMP4.MENU_ID_LEVEL2
                                                ORDER BY TMP4.SMENUORDER  ";

                cmdAuthenSelect.Parameters.Clear();
                cmdAuthenSelect.Parameters.Add(new OracleParameter() { ParameterName = "I_USERGROUP_ID", Value = UserGroupID });

                // cmdAuthenSelect.Parameters.Add(new OracleParameter() { ParameterName = "PRM", Value = groupAccess });
                // AND ( TMENU.PRMSDISABLE  LIKE '%'|| :PRM ||'%') 
               // cmdAuthenSelect.Parameters["I_USERGROUP_ID"].Value = UserGroupID;
                //cmdAuthenSelect.Parameters["PRM"].Value = '%' + UserGroupID + '%';
                

                dt = dbManager.ExecuteDataTable(cmdAuthenSelect, "cmdAuthenSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable AuthenSelectDALOnlyGridShow(string UserGroupID)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();

                cmdAuthenSelect.CommandText = @" SELECT  TMP4.MENU_ID, TMP4.MENU_NAME, TMP4.MENU_NAME_LEVEL1 , TMP4.MENU_NAME_LEVEL2,TMP4.DESCRIPTION,TMP4.VISIBLE,TMP4.READ,TMP4.WRITE 
                                                         , TMP4.MENU_ID_LEVEL1 , TMP4.MENU_ID_LEVEL2
                                                FROM
                                                (
                                                SELECT TMP3.*, NVL(VISIBLE, 0) AS VISIBLE, NVL(READ, 0) AS READ,  NVL(WRITE, 0) AS WRITE
                                                                                                FROM
                                                                                                (
                                                                                                    SELECT TMP2.MENU_ID, TMP2.MENU_NAME, TMP2.MENU_NAME_PTT, TMP2.MENU_ID_LEVEL1
                                                                                                            , TMP2.MENU_NAME_LEVEL1
                                                                                                            , TMP2.MENU_NAME_LEVEL1_PTT
                                                                                                            , M_MENU.MENU_ID AS MENU_ID_LEVEL2, TMP2.ROW_ORDER_LEVEL1
                                                                                                            , M_MENU.MENU_NAME AS MENU_NAME_LEVEL2
                                                                                                            , M_MENU.MENU_NAME_PTT AS MENU_NAME_LEVEL2_PTT
                                                                                                            , M_MENU.ROW_ORDER AS ROW_ORDER_LEVEL2
                                                                                                            , CASE WHEN NVL(M_MENU.MENU_URL, ' ') = ' ' THEN TMP2.MENU_URL ELSE M_MENU.MENU_URL END AS MENU_URL_TMS
                                                                                                            , CASE WHEN NVL(M_MENU.DESCRIPTION, ' ') = ' ' THEN TMP2.DESCRIPTION ELSE M_MENU.DESCRIPTION END AS DESCRIPTION
                                                            
                                                                                                    FROM
                                                                                                        (
                                                                                                            SELECT DESCRIPTION, M_MENU.MENU_URL, TMP1.MENU_ID, TMP1.MENU_NAME, TMP1.MENU_NAME_PTT, M_MENU.MENU_ID AS MENU_ID_LEVEL1, M_MENU.MENU_NAME AS MENU_NAME_LEVEL1, M_MENU.MENU_NAME_PTT AS MENU_NAME_LEVEL1_PTT, M_MENU.ROW_ORDER AS ROW_ORDER_LEVEL1
                                                                                                            FROM
                                                                                                            (
                                                                                                                SELECT MENU_ID, MENU_NAME, MENU_NAME_PTT, ROW_ORDER, MENU_URL
                                                                                                                FROM M_MENU
                                                                                                                WHERE MENU_LEVEL = 0
                                                                                                            ) TMP1 INNER JOIN M_MENU ON TMP1.MENU_ID = M_MENU.PARENT_MENU_ID 
                                                                                                            ) TMP2
                                                                                                    LEFT JOIN M_MENU ON TMP2.MENU_ID_LEVEL1 = M_MENU.PARENT_MENU_ID
                                                                                                ) TMP3
                                                                                                LEFT JOIN M_AUTHORIZATION ON M_AUTHORIZATION.MENU_ID = (CASE WHEN NVL(TMP3.MENU_ID_LEVEL2, -1) = -1 THEN TMP3.MENU_ID_LEVEL1 ELSE TMP3.MENU_ID_LEVEL2 END) AND M_AUTHORIZATION.USERGROUP_ID =:I_USERGROUP_ID
                                                                                                ORDER BY MENU_NAME, MENU_ID_LEVEL1, ROW_ORDER_LEVEL1, MENU_ID_LEVEL2, ROW_ORDER_LEVEL2
                                                ) TMP4
                                                LEFT JOIN M_MENU MENU_LEVEL1 ON MENU_LEVEL1.MENU_ID = TMP4.MENU_ID_LEVEL1
                                                LEFT JOIN M_MENU MENU_LEVEL2 ON MENU_LEVEL2.MENU_ID = TMP4.MENU_ID_LEVEL2";

                cmdAuthenSelect.Parameters["I_USERGROUP_ID"].Value = UserGroupID;

                dt = dbManager.ExecuteDataTable(cmdAuthenSelect, "cmdAuthenSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void AuthenInsertDAL(string UserGroupID, DataTable dtAuthen, int UserID)
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();

                dbManager.BeginTransaction();

                cmdAuthenDelete.CommandText = "DELETE FROM M_AUTHORIZATION WHERE USERGROUP_ID =:I_USERGROUP_ID";

                cmdAuthenDelete.Parameters["I_USERGROUP_ID"].Value = UserGroupID;
                dbManager.ExecuteNonQuery(cmdAuthenDelete);

                for (int i = 0; i < dtAuthen.Rows.Count; i++)
                {
                    cmdAuthenInsert.CommandText = @"INSERT INTO M_AUTHORIZATION (USERGROUP_ID, MENU_ID, CREATE_BY, VISIBLE, READ, WRITE)
                                                    VALUES (:I_USERGROUP_ID, :I_MENU_ID, :I_CREATE_BY, :I_VISIBLE, :I_READ, :I_WRITE)";
                    //GenID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM LSTHOLIDAY ORDER BY NID DESC) WHERE ROWNUM <= 1");
                    cmdAuthenInsert.Parameters["I_USERGROUP_ID"].Value = UserGroupID;
                    cmdAuthenInsert.Parameters["I_MENU_ID"].Value = dtAuthen.Rows[i]["MENU_ID"].ToString();
                    cmdAuthenInsert.Parameters["I_CREATE_BY"].Value = UserID;
                    cmdAuthenInsert.Parameters["I_VISIBLE"].Value = dtAuthen.Rows[i]["VISIBLE"].ToString();
                    cmdAuthenInsert.Parameters["I_READ"].Value = dtAuthen.Rows[i]["READ"].ToString();
                    cmdAuthenInsert.Parameters["I_WRITE"].Value = dtAuthen.Rows[i]["WRITE"].ToString();

                    dbManager.ExecuteNonQuery(cmdAuthenInsert);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataTable AuthenSelectTemplateDAL()
        {
            try
            {
                dbManager.Open();
                DataTable dt = new DataTable();

                cmdAuthenSelectTemplate.CommandText = @"SELECT TMP2.MENU_ID, TMP2.MENU_NAME, TMP2.MENU_ID_LEVEL1, TMP2.MENU_NAME_LEVEL1, M_MENU.MENU_ID AS MENU_ID_LEVEL2, TMP2.ROW_ORDER_LEVEL1
                                                             , M_MENU.MENU_NAME AS MENU_NAME_LEVEL2, M_MENU.ROW_ORDER AS ROW_ORDER_LEVEL2
                                                             , 0 AS VISIBLE, 0 AS READ, 0 AS WRITE
                                                             , CASE WHEN NVL(M_MENU.MENU_URL, ' ') = ' ' THEN TMP2.MENU_URL ELSE M_MENU.MENU_URL END AS MENU_URL
                                                             , CASE WHEN NVL(M_MENU.DESCRIPTION, ' ') = ' ' THEN TMP2.DESCRIPTION ELSE M_MENU.DESCRIPTION END AS DESCRIPTION
                                                        FROM
                                                        (
                                                            SELECT DESCRIPTION, M_MENU.MENU_URL, TMP1.MENU_ID, TMP1.MENU_NAME, M_MENU.MENU_ID AS MENU_ID_LEVEL1, M_MENU.MENU_NAME AS MENU_NAME_LEVEL1, M_MENU.ROW_ORDER AS ROW_ORDER_LEVEL1
                                                            FROM
                                                            (
                                                                SELECT MENU_ID, MENU_NAME, ROW_ORDER, MENU_URL
                                                                FROM M_MENU
                                                                WHERE MENU_LEVEL = 0
                                                            ) TMP1 INNER JOIN M_MENU ON TMP1.MENU_ID = M_MENU.PARENT_MENU_ID 
                                                        ) TMP2
                                                          LEFT JOIN M_MENU ON TMP2.MENU_ID_LEVEL1 = M_MENU.PARENT_MENU_ID
                                                        ORDER BY TMP2.MENU_ID , TMP2.MENU_ID_LEVEL1, TMP2.ROW_ORDER_LEVEL1, ROW_ORDER_LEVEL2";

                dt = dbManager.ExecuteDataTable(cmdAuthenSelectTemplate, "cmdAuthenSelectEmpty");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static AuthenDAL _instance;
        public static AuthenDAL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthenDAL();
                }
                return _instance;
            }
        }
        #endregion

            }

    partial class AuthenDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdAuthenInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdAuthenDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdAuthenSelectTemplate = new System.Data.OracleClient.OracleCommand();
            this.cmdAuthenSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdAuthenInsert
            // 
            this.cmdAuthenInsert.Connection = this.OracleConn;
            this.cmdAuthenInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MENU_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VISIBLE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_READ", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_WRITE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdAuthenDelete
            // 
            this.cmdAuthenDelete.Connection = this.OracleConn;
            this.cmdAuthenDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdAuthenSelectTemplate
            // 
            this.cmdAuthenSelectTemplate.Connection = this.OracleConn;
            // 
            // cmdAuthenSelect
            // 
            this.cmdAuthenSelect.Connection = this.OracleConn;
            this.cmdAuthenSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdAuthenInsert;
        private System.Data.OracleClient.OracleCommand cmdAuthenDelete;
        private System.Data.OracleClient.OracleCommand cmdAuthenSelectTemplate;
        private System.Data.OracleClient.OracleCommand cmdAuthenSelect;

    }
}