﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using TMS_DAL.StatusDAL;

namespace TMS_BLL.AuthenStatusBLL
{
    public class StatusBLL
    {
        public DataTable StatusSelectBLL(string Condition)
        {
            try
            {
                return StatusDAL.Instance.StatusSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static StatusBLL _instance;
        public static StatusBLL Instance
        {
            get
            {
                _instance = new StatusBLL();
                return _instance;
            }
        }
        #endregion
    }
}