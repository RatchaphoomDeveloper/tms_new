﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using TMS_DAL.ConnectionDAL;

namespace TMS_DAL.StatusDAL
{
    public partial class StatusDAL : OracleConnectionDAL
    {
        public StatusDAL()
        {
            InitializeComponent();
        }

        public StatusDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.cmdStatusSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdStatusSelect
            // 
            this.cmdStatusSelect.CommandText = "SELECT * FROM M_STATUS WHERE 1=1";
            this.cmdStatusSelect.Connection = this.OracleConn;

        }

        public DataTable StatusSelectDAL(string Condition)
        {
            string Query = cmdStatusSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdStatusSelect.CommandText += Condition;

                cmdStatusSelect.CommandText += " ORDER BY ROW_ORDER, STATUS_ID";

                dt = dbManager.ExecuteDataTable(cmdStatusSelect, "cmdStatusSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdStatusSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static StatusDAL _instance;
        public static StatusDAL Instance
        {
            get
            {
                _instance = new StatusDAL();
                return _instance;
            }
        }
        #endregion

        private System.Data.OracleClient.OracleCommand cmdStatusSelect;

    }
}