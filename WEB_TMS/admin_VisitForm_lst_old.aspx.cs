﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class admin_ChkList_lst : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            Session["oNTYPEVISITFORMID"] = null;

            LogUser("37", "R", "เปิดดูข้อมูลหน้า แบบฟอร์มประเมินสถานประกอบการ", "");

        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("NTYPEVISITFORMID", "STYPEVISITFORMNAME", "CACTIVE", "DUPDATE", "GROUPCOUNT", "VISITFORMCOUNT")
                    .Cast<object[]>()
                    .Select(s => new { NTYPEVISITFORMID = s[0].ToString(), STYPEVISITFORMNAME = s[1].ToString(), CACTIVE = s[2].ToString(), DUPDATE = s[3].ToString(), GROUPCOUNT = s[4].ToString(), VISITFORMCOUNT = s[5].ToString() });
                string delid = "";
                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();
                    foreach (var l in ld)
                    {
                        delid += l.NTYPEVISITFORMID + ",";

                        string strsql3 = "DELETE FROM TVISITFORM WHERE NTYPEVISITFORMID = " + l.NTYPEVISITFORMID;
                        using (OracleCommand com3 = new OracleCommand(strsql3, con))
                        {
                            com3.ExecuteNonQuery();
                        }

                        string strsql = "DELETE FROM TGROUPOFVISITFORM WHERE NTYPEVISITFORMID = " + l.NTYPEVISITFORMID;
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            com.ExecuteNonQuery();
                        }

                        string strsql1 = "DELETE FROM TTYPEOFVISITFORM WHERE NTYPEVISITFORMID = " + l.NTYPEVISITFORMID;
                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
                        {
                            com1.ExecuteNonQuery();
                        }

                        string strsql2 = "DELETE FROM TTYPEOFVISITFORMLIST WHERE NTYPEVISITFORMID = " + l.NTYPEVISITFORMID;
                        using (OracleCommand com2 = new OracleCommand(strsql2, con))
                        {
                            com2.ExecuteNonQuery();
                        }
                    }
                }

                LogUser("37", "D", "ลบข้อมูลหน้า แบบฟอร์มประเมินสถานประกอบการ รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                gvw.DataBind();
                gvw.Selection.UnselectAll();

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "NTYPEVISITFORMID");
                string stmID = Convert.ToString(data);

                Session["oNTYPEVISITFORMID"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_VisitForm_add.aspx";

                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_VisitForm_add.aspx");
    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    protected void btnConfig_Click(object sender, EventArgs e)
    {
        Response.Redirect("Admin_VisitForm_Config.aspx");
    }
}
