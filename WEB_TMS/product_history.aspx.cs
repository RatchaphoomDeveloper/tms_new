﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;

public partial class product_history : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    static List<sProduct> listProduct = new List<sProduct>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                ViewState["setdata"] = strQuery[0];

            }
          
            #region เช็ค Permission
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("48", "R", "เปิดดูข้อมูลหน้า ประวัติการเปลี่ยนแปลงข้อมูลผลิตภัณฑ์", ViewState["setdata"] + "");

            #endregion
            setdata();
            Listgrid();
        }
    }

    void setdata()
    {

        string sPROD_ID = ViewState["setdata"] + "";
        string strsql = @"SELECT pd.PROD_ID, pd.PROD_ABBR, pd.DENSITY, pd.DATE_CREATED, USER_CREATED, pd.DATE_UPDATED, 
                         SUSER.SFIRSTNAME||' '||SUSER.SLASTNAME  AS  USER_UPDATED, pd.PROD_CATEGORY, PORT_FLAG,pd.PROD_ABBR_V,pd.LANDTRANSPORT, pd.WATERTRANSPORT, 
                          pd.PIPELINETRANSPORT, pd.TRAINTRANSPORT,pd.SPRODUCTTYPEID
                          FROM TPRODUCT pd
                          LEFT JOIN  TUSER SUSER
                          ON pd.USER_UPDATED = SUSER.SUID 
                          WHERE pd.PROD_ID = '" + CommonFunction.ReplaceInjection(sPROD_ID) + "'";

   
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);
        if (dt.Rows.Count > 0)
        {
            #region AdddatatoList Employee
            if (listProduct.Count > 0)
            {
                listProduct.RemoveAt(0);
            }

            DateTime? sDATE_CREATED = null;
            DateTime? sDATE_UPDATED = null;

            if (!string.IsNullOrEmpty(dt.Rows[0]["DATE_CREATED"] + ""))
            {
                sDATE_CREATED = DateTime.Parse(dt.Rows[0]["DATE_CREATED"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DATE_UPDATED"] + ""))
            {
                sDATE_UPDATED = DateTime.Parse(dt.Rows[0]["DATE_UPDATED"] + "");
            }
            listProduct.Add(new sProduct
            {
                PROD_ID = dt.Rows[0]["PROD_ID"] + "",
                PROD_ABBR = dt.Rows[0]["PROD_ABBR"] + "",
                DENSITY = dt.Rows[0]["DENSITY"] + "",
                DATE_CREATED = sDATE_CREATED,
                USER_CREATED = dt.Rows[0]["USER_CREATED"] + "",
                DATE_UPDATED = sDATE_UPDATED,
                USER_UPDATED = dt.Rows[0]["USER_UPDATED"] + "",
                PROD_CATEGORY = dt.Rows[0]["PROD_CATEGORY"] + "",
                PORT_FLAG = dt.Rows[0]["PORT_FLAG"] + "",
                PROD_ABBR_V = dt.Rows[0]["PROD_ABBR_V"] + "",
                LANDTRANSPORT = dt.Rows[0]["LANDTRANSPORT"] + "",
                WATERTRANSPORT = dt.Rows[0]["WATERTRANSPORT"] + "",
                PIPELINETRANSPORT = dt.Rows[0]["PIPELINETRANSPORT"] + "",
                TRAINTRANSPORT = dt.Rows[0]["TRAINTRANSPORT"] + "",
                SPRODUCTTYPEID = dt.Rows[0]["SPRODUCTTYPEID"] + ""
            });
            #endregion
        }

    }

    void Listgrid()
    {
        string PROD_ID = ViewState["setdata"] + "";
        string Historysql = @"SELECT shistory.PROD_ID, shistory.PROD_ABBR, shistory.DENSITY, shistory.DATE_CREATED, shistory.USER_CREATED, shistory.DATE_UPDATED, 
                          shistory.PROD_CATEGORY, shistory.PORT_FLAG, shistory.PROD_ABBR_V, shistory.LANDTRANSPORT, shistory.WATERTRANSPORT, 
                          shistory.PIPELINETRANSPORT, shistory.TRAINTRANSPORT,shistory.NVERSION ,suser.SFIRSTNAME||' '||suser.SLASTNAME AS USER_UPDATED,shistory.SPRODUCTTYPEID
                          FROM TPRODUCT_HISTORY shistory
LEFT JOIN TUSER suser
ON SHISTORY.USER_UPDATED = SUSER.SUID WHERE shistory.PROD_ID ='" + CommonFunction.ReplaceInjection(PROD_ID) + "' ORDER BY shistory.NVERSION ASC ";


        List<string[]> sds = new List<string[]>();

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, Historysql);

        string DateUpdate = "";
        string SUPDATE = "";
        string datachange = "";
        string dataold = "";
        string datanew = "";

        var PresentData = listProduct.FirstOrDefault();

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
               
                if (i + 1 != dt.Rows.Count)
                {
                    DateUpdate = dt.Rows[i+1]["DATE_UPDATED"] + "";
                    SUPDATE = dt.Rows[i+1]["USER_UPDATED"] + "";
                    if (dt.Rows[i]["PROD_ABBR"] + "" != dt.Rows[i + 1]["PROD_ABBR"] + "")
                    {
                        datachange += "ชื่อย่อจองรถ" + "<br>";
                        dataold += dt.Rows[i]["PROD_ABBR"] + "" + "<br>";
                        datanew += dt.Rows[i+1]["PROD_ABBR"] + "" + "<br>";
                    }

                    if (dt.Rows[i]["DENSITY"] + "" != dt.Rows[i + 1]["DENSITY"] + "")
                    {
                        datachange += "ความหนาแน่น" + "<br>";
                        dataold += dt.Rows[i]["DENSITY"] + "" + "<br>";
                        datanew += dt.Rows[i + 1]["DENSITY"] + "" + "<br>";
                    }

                    //if (dt.Rows[i]["DATE_CREATED"] + "" != dt.Rows[i + 1]["DATE_CREATED"] + "")
                    //{
                    //    datachange += "วันที่สร้าง" + "<br>";
                    //    dataold += dt.Rows[i]["DATE_CREATED"] + "" + "<br>";
                    //    datanew += dt.Rows[i + 1]["DATE_CREATED"] + "" + "<br>";
                    //}

                    //if (dt.Rows[i]["USER_CREATED"] + "" != dt.Rows[i + 1]["USER_CREATED"] + "")
                    //{
                    //    datachange += "ผู้สร้าง" + "<br>";
                    //    dataold += dt.Rows[i]["USER_CREATED"] + "" + "<br>";
                    //    datanew += dt.Rows[i + 1]["USER_CREATED"] + "" + "<br>";
                    //}

                    if (dt.Rows[i]["PROD_CATEGORY"] + "" != dt.Rows[i + 1]["PROD_CATEGORY"] + "")
                    {
                        datachange += "ประเภทผลิตภัณฑ์" + "<br>";
                        dataold += dt.Rows[i]["PROD_CATEGORY"] + "" + "<br>";
                        datanew += dt.Rows[i + 1]["PROD_CATEGORY"] + "" + "<br>";
                    }

                    if (dt.Rows[i]["PORT_FLAG"] + "" != dt.Rows[i + 1]["PORT_FLAG"] + "")
                    {
                        datachange += "สถานะการใช้งาน" + "<br>";
                        dataold += dt.Rows[i]["PORT_FLAG"] + "" + "<br>";
                        datanew += dt.Rows[i + 1]["PORT_FLAG"] + "" + "<br>";
                    }

                    if (dt.Rows[i]["PROD_ABBR_V"] + "" != dt.Rows[i + 1]["PROD_ABBR_V"] + "")
                    {
                        datachange += "ชื่อย่อ(ขนส่งทางเรือ)" + "<br>";
                        dataold += dt.Rows[i]["PROD_ABBR_V"] + "" + "<br>";
                        datanew += dt.Rows[i + 1]["PROD_ABBR_V"] + "" + "<br>";
                    }

                    if (dt.Rows[i]["LANDTRANSPORT"] + "" != dt.Rows[i + 1]["LANDTRANSPORT"] + "")
                    {
                        datachange += "ขนส่งทางรถ" + "<br>";

                        switch (dt.Rows[i]["LANDTRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางรถ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางรถ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (dt.Rows[i+1]["LANDTRANSPORT"] + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางรถ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางรถ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }

                        //if (dt.Rows[i]["LANDTRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางรถ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางรถ" + "<br>";
                        //}

                        //if (dt.Rows[i + 1]["LANDTRANSPORT"] + "" == "1")
                        //{
                        //    datanew += "ขนส่งทางรถ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางรถ" + "<br>";
                        //}
                        
                    }

                    if (dt.Rows[i]["WATERTRANSPORT"] + "" != dt.Rows[i + 1]["WATERTRANSPORT"] + "")
                    {
                        datachange += "ขนส่งทางเรือ" + "<br>";

                        switch (dt.Rows[i]["WATERTRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางเรือ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางเรือ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (dt.Rows[i + 1]["WATERTRANSPORT"] + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางเรือ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางเรือ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }
                        //if (dt.Rows[i]["WATERTRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางเรือ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางเรือ" + "<br>";
                        //}

                        //if (dt.Rows[i + 1]["WATERTRANSPORT"] + "" == "1")
                        //{
                        //    datanew += "ขนส่งทางเรือ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางเรือ" + "<br>";
                        //}
                    }

                    if (dt.Rows[i]["PIPELINETRANSPORT"] + "" != dt.Rows[i + 1]["PIPELINETRANSPORT"] + "")
                    {
                        datachange += "ขนส่งทางท่อ" + "<br>";


                        switch (dt.Rows[i]["PIPELINETRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางท่อ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางท่อ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (dt.Rows[i + 1]["PIPELINETRANSPORT"] + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางท่อ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางท่อ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }
                        //if (dt.Rows[i]["PIPELINETRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางท่อ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางท่อ" + "<br>";
                        //}

                        //if (dt.Rows[i + 1]["PIPELINETRANSPORT"] + "" == "1")
                        //{
                        //    datanew += "ขนส่งทางท่อ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางท่อ" + "<br>";
                        //}
                    }

                    if (dt.Rows[i]["TRAINTRANSPORT"] + "" != dt.Rows[i + 1]["TRAINTRANSPORT"] + "")
                    {
                        datachange += "ขนส่งทางรถไฟ" + "<br>";


                        switch (dt.Rows[i]["TRAINTRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางรถไฟ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางรถไฟ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (dt.Rows[i + 1]["TRAINTRANSPORT"] + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางรถไฟ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางรถไฟ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }
                        //if (dt.Rows[i]["TRAINTRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางรถไฟ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางรถไฟ" + "<br>";
                        //}

                        //if (dt.Rows[i + 1]["TRAINTRANSPORT"] + "" == "1")
                        //{
                        //    datanew += "ขนส่งทางรถไฟ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางรถไฟ" + "<br>";
                        //}
                    }

                }
                else
                {
                    DateUpdate = PresentData.DATE_UPDATED+"";
                    SUPDATE = PresentData.USER_UPDATED;
                    if (dt.Rows[i]["PROD_ABBR"] + "" != PresentData.PROD_ABBR)
                    {
                        datachange += "ชื่อย่อจองรถ" + "<br>";
                        dataold += dt.Rows[i]["PROD_ABBR"] + "" + "<br>";
                        datanew += PresentData.PROD_ABBR + "<br>";
                    }

                    if (dt.Rows[i]["DENSITY"] + "" != PresentData.DENSITY)
                    {
                        datachange += "ความหนาแน่น" + "<br>";
                        dataold += dt.Rows[i]["DENSITY"] + "" + "<br>";
                        datanew += PresentData.DENSITY + "<br>";
                    }

                    //if (dt.Rows[i]["DATE_CREATED"] + "" != dt.Rows[i + 1]["DATE_CREATED"] + "")
                    //{
                    //    datachange += "วันที่สร้าง" + "<br>";
                    //    dataold += dt.Rows[i]["DATE_CREATED"] + "" + "<br>";
                    //    datanew += dt.Rows[i + 1]["DATE_CREATED"] + "" + "<br>";
                    //}

                    //if (dt.Rows[i]["USER_CREATED"] + "" != dt.Rows[i + 1]["USER_CREATED"] + "")
                    //{
                    //    datachange += "ผู้สร้าง" + "<br>";
                    //    dataold += dt.Rows[i]["USER_CREATED"] + "" + "<br>";
                    //    datanew += dt.Rows[i + 1]["USER_CREATED"] + "" + "<br>";
                    //}

                    if (dt.Rows[i]["PROD_CATEGORY"] + "" != PresentData.PROD_CATEGORY)
                    {
                        datachange += "ประเภทผลิตภัณฑ์" + "<br>";
                        dataold += dt.Rows[i]["PROD_CATEGORY"] + "" + "<br>";
                        datanew += PresentData.PROD_CATEGORY+ "<br>";
                    }

                    if (dt.Rows[i]["PORT_FLAG"] + "" != PresentData.PORT_FLAG)
                    {
                        datachange += "สถานะการใช้งาน" + "<br>";
                        dataold += dt.Rows[i]["PORT_FLAG"] + "" + "<br>";
                        datanew += PresentData.PORT_FLAG + "<br>";
                    }

                    if (dt.Rows[i]["PROD_ABBR_V"] + "" != PresentData.PROD_ABBR_V)
                    {
                        datachange += "ชื่อย่อ(ขนส่งทางเรือ)" + "<br>";
                        dataold += dt.Rows[i]["PROD_ABBR_V"] + "" + "<br>";
                        datanew += PresentData.PROD_ABBR_V + "<br>";
                    }

                    if (dt.Rows[i]["LANDTRANSPORT"] + "" != PresentData.LANDTRANSPORT)
                    {
                        datachange += "ขนส่งทางรถ" + "<br>";

                        switch (dt.Rows[i]["LANDTRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางรถ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางรถ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (PresentData.LANDTRANSPORT + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางรถ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางรถ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }

                        //if (dt.Rows[i]["LANDTRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางรถ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางรถ" + "<br>";
                        //}

                        //if (PresentData.LANDTRANSPORT == "1")
                        //{
                        //    datanew += "ขนส่งทางรถ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางรถ" + "<br>";
                        //}
                    }

                    if (dt.Rows[i]["WATERTRANSPORT"] + "" != PresentData.WATERTRANSPORT)
                    {
                        datachange += "ขนส่งทางเรือ" + "<br>";

                        switch (dt.Rows[i]["WATERTRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางเรือ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางเรือ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (PresentData.WATERTRANSPORT + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางเรือ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางเรือ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }

                        //if (dt.Rows[i]["WATERTRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางเรือ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางเรือ" + "<br>";
                        //}

                        //if (PresentData.WATERTRANSPORT == "1")
                        //{
                        //    datanew += "ขนส่งทางเรือ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางเรือ" + "<br>";
                        //}
                    }

                    if (dt.Rows[i]["PIPELINETRANSPORT"] + "" != PresentData.PIPELINETRANSPORT)
                    {
                        datachange += "ขนส่งทางท่อ" + "<br>";


                        switch (dt.Rows[i]["PIPELINETRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางท่อ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางท่อ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (PresentData.PIPELINETRANSPORT + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางท่อ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางท่อ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }
                        //if (dt.Rows[i]["PIPELINETRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางท่อ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางท่อ" + "<br>";
                        //}

                        //if (PresentData.PIPELINETRANSPORT == "1")
                        //{
                        //    datanew += "ขนส่งทางท่อ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางท่อ" + "<br>";
                        //}
                    }

                    if (dt.Rows[i]["TRAINTRANSPORT"] + "" != PresentData.TRAINTRANSPORT)
                    {
                        datachange += "ขนส่งทางรถไฟ" + "<br>";

                        switch (dt.Rows[i]["TRAINTRANSPORT"] + "")
                        {
                            case "0": dataold += "ไม่ขนส่งทางรถไฟ" + "<br>";
                                break;
                            case "1": dataold += "ขนส่งทางรถไฟ" + "<br>";
                                break;
                            default: dataold += " " + "<br>";
                                break;
                        }

                        switch (PresentData.TRAINTRANSPORT + "")
                        {
                            case "0": datanew += "ไม่ขนส่งทางรถไฟ" + "<br>";
                                break;
                            case "1": datanew += "ขนส่งทางรถไฟ" + "<br>";
                                break;
                            default: datanew += " " + "<br>";
                                break;
                        }
                        //if (dt.Rows[i]["TRAINTRANSPORT"] + "" == "1")
                        //{
                        //    dataold += "ขนส่งทางรถไฟ" + "<br>";
                        //}
                        //else
                        //{
                        //    dataold += "ไม่ขนส่งทางรถไฟ" + "<br>";
                        //}

                        //if (PresentData.TRAINTRANSPORT == "1")
                        //{
                        //    datanew += "ขนส่งทางรถไฟ" + "<br>";
                        //}
                        //else
                        //{
                        //    datanew += "ไม่ขนส่งทางรถไฟ" + "<br>";
                        //}
                    }
                }

                sds.Add(new string[] { DateUpdate, SUPDATE, datachange, dataold, datanew });
                DateUpdate = "";
                SUPDATE = "";
                datachange = "";
                dataold = "";
                datanew = "";


            }
        }

        gvw.DataSource = sds.Select(s => new { DateUpdate = s[0], SUPDATE = s[1], datachange = s[2], dataold = s[3], datanew = s[4] }).OrderByDescending(o=>o.DateUpdate);
        gvw.DataBind();
        gvw.Visible = true;
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //Setdata();

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {


    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    [Serializable]
    class sProduct
    {
        public string PROD_ID { get; set; }
        public string PROD_ABBR { get; set; }
        public string DENSITY { get; set; }
        public DateTime? DATE_CREATED { get; set; }
        public string USER_CREATED { get; set; }
        public DateTime? DATE_UPDATED { get; set; }
        public string USER_UPDATED { get; set; }
        public string PROD_CATEGORY { get; set; }
        public string PORT_FLAG { get; set; }
        public string PROD_ABBR_V { get; set; }
        public string LANDTRANSPORT { get; set; }
        public string WATERTRANSPORT { get; set; }
        public string PIPELINETRANSPORT { get; set; }
        public string TRAINTRANSPORT { get; set; }
        public string SPRODUCTTYPEID { get; set; }
    }
}