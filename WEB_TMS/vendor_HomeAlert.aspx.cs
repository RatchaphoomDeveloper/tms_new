﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class vendor_HomeAlert : System.Web.UI.Page
{
    string _conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    string _sConfigID = "001";//รหัสค่าวันที่คอนฟิก
    protected void Page_Load(object sender, EventArgs e)
    {
        string gg = Session["UserID"].ToString();
        string gg2 = Session["SVDID"].ToString();

        if (string.IsNullOrEmpty(Session["UserID"] + "") || string.IsNullOrEmpty(Session["SVDID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        else
        {

            string sUserID = Session["UserID"] + "";
            if (!IsPostBack)
            {
                // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;                
                lblVendorID.Text = Session["SVDID"].ToString();
                SetComBoCause(cbStatus);
                ListData();
                ListDataOther();
                draw_calendar((DateTime.Now.Month + "").PadLeft(2, '0'), (DateTime.Now.Year + 543) + "", DateTime.Now.Year + "");
            }
        }
    }

    #region CallBack

    protected void xcpn_Load(object sender, EventArgs e)
    {

        ListData();
        ListDataOther();

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');
        int index = 0;
        if (paras.Length > 1)
        {
            if (!string.IsNullOrEmpty(paras[1]))
            {
                index = int.Parse(paras[1]);
            }
        }
        switch (paras[0])
        {
            case "change": ListData(); ListDataOther();
                break;
            case "View":

                dynamic i = gvw.GetRowValues(index, "REQUEST_ID", "STATUSID", "VEH_ID", "TU_ID", "SCARTYPEID");

                string _reqID = i[0] + "";
                string _statusID = i[1] + "";
                string _VEH_ID = i[2] + "";
                string _TU_ID = i[3] + "";
                string _SCARTYPEID = i[4] + "";

                if (!string.IsNullOrEmpty(_reqID) && !string.IsNullOrEmpty(_statusID))
                {
                    switch (_statusID)
                    {
                        //รอพิจารณา
                        case "01":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //ปรับแก้ไขคำขอ
                        case "02":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //รอยืนยันคิวนัดหมาย
                        case "03":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //รอเข้ารับบริการ
                        case "04":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //รอบันทึกผล
                        case "05":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //รอนำส่งเอกสารปิดงาน
                        case "06":

                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //รถไม่ได้มาตรฐาน
                        case "07":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //รอปิดงาน
                        case "08":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //รอแนบใบเสร็จ
                        case "09":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //ปิดงานแล้ว
                        case "10":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;
                        //มว. แจ้ง รข. ปรับแก้ไข
                        case "12":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                            break;

                    }
                }
                else
                {
                    string Check_SCHASIS = SystemFunction.Check_SCHASIS(_VEH_ID);
                    //เช็คว่ารถมีเลข แซสสซีย์ ไหม
                    if (!string.IsNullOrEmpty(Check_SCHASIS))
                    {

                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Check_SCHASIS + "' );");
                        return;
                    }
                    else
                    {


                        if (Page.IsCallback)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + "1" + "' );");
                            DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("vendor_request_add.aspx?vehID=" + Server.UrlEncode(STCrypt.Encrypt(_VEH_ID))
                                + "&&tuID=" + Server.UrlEncode(STCrypt.Encrypt(_TU_ID))
                                + "&&reqtype=" + Server.UrlEncode(STCrypt.Encrypt("01"))
                                + "&&reqID="
                                + "&&carTypeID=" + Server.UrlEncode(STCrypt.Encrypt(_SCARTYPEID))
                                + "&&reqOther=" + Server.UrlEncode(STCrypt.Encrypt("0"))
                                // + "&&mode=" + Server.UrlEncode(STCrypt.Encrypt("add"))
                                );//0=ในวาระ 1 = other
                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + "2" + "' );");
                            Response.Redirect("vendor_request_add.aspx?vehID=" + Server.UrlEncode(STCrypt.Encrypt(_VEH_ID))
                                + "&&tuID=" + Server.UrlEncode(STCrypt.Encrypt(_TU_ID))
                                + "&&reqtype=" + Server.UrlEncode(STCrypt.Encrypt(_SCARTYPEID))
                                + "&&reqID="
                                + "&&carTypeID=" + Server.UrlEncode(STCrypt.Encrypt(_SCARTYPEID))
                                + "&&reqOther=" + Server.UrlEncode(STCrypt.Encrypt("O"))
                                // + "&&mode=" + Server.UrlEncode(STCrypt.Encrypt("add"))
                                );
                        }
                    }
                }

                break;
            case "View2":

                dynamic k = gvw2.GetRowValues(index, "REQUEST_ID", "STATUSREQ_ID", "VEH_ID", "TU_ID", "REQTYPE_ID");

                string _reqID2 = k[0] + "";
                string _statusID2 = k[1] + "";
                string _VEH_ID2 = k[2] + "";
                string _TU_ID2 = k[3] + "";
                string _SCARTYPEID2 = k[4] + "";

                if (!string.IsNullOrEmpty(_reqID2) && !string.IsNullOrEmpty(_statusID2))
                {
                    switch (_statusID2)
                    {
                        //รอพิจารณา
                        case "01":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //ปรับแก้ไขคำขอ
                        case "02":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //รอยืนยันคิวนัดหมาย
                        case "03":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //รอเข้ารับบริการ
                        case "04":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //รอบันทึกผล
                        case "05":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //รอนำส่งเอกสารปิดงาน
                        case "06":

                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //รถไม่ได้มาตรฐาน
                        case "07":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //รอปิดงาน
                        case "08":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //รอแนบใบเสร็จ
                        case "09":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //ปิดงานแล้ว
                        case "10":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;
                        //มว. แจ้ง รข. ปรับแก้ไข
                        case "12":
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID2)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID2));
                            break;

                    }
                }
                else
                {
                    string Check_SCHASIS = SystemFunction.Check_SCHASIS(_VEH_ID2);
                    //เช็คว่ารถมีเลข แซสสซีย์ ไหม
                    if (!string.IsNullOrEmpty(Check_SCHASIS))
                    {

                        CommonFunction.SetPopupOnLoad(gvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Check_SCHASIS + "' );");
                        return;
                    }
                    else
                    {


                        if (Page.IsCallback)
                            DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("service_add.aspx?vehID=" + Server.UrlEncode(STCrypt.Encrypt(_VEH_ID2))
                                + "&&tuID=" + Server.UrlEncode(STCrypt.Encrypt(_TU_ID2))
                                + "&&reqtype=" + Server.UrlEncode(STCrypt.Encrypt("01"))
                                + "&&reqID="
                                + "&&carTypeID=" + Server.UrlEncode(STCrypt.Encrypt(_SCARTYPEID2))
                                + "&&reqOther=" + Server.UrlEncode(STCrypt.Encrypt("0"))
                                // + "&&mode=" + Server.UrlEncode(STCrypt.Encrypt("add"))
                                );//0=ในวาระ 1 = other
                        else
                            Response.Redirect("service_add.aspx?vehID=" + Server.UrlEncode(STCrypt.Encrypt(_VEH_ID2))
                                + "&&tuID=" + Server.UrlEncode(STCrypt.Encrypt(_TU_ID2))
                                + "&&reqtype=" + Server.UrlEncode(STCrypt.Encrypt(_SCARTYPEID2))
                                + "&&reqID="
                                + "&&carTypeID=" + Server.UrlEncode(STCrypt.Encrypt(_SCARTYPEID2))
                                + "&&reqOther=" + Server.UrlEncode(STCrypt.Encrypt("O"))
                                // + "&&mode=" + Server.UrlEncode(STCrypt.Encrypt("add"))
                                );
                    }
                }

                break;

        }
    }

    protected void xcpnCalendar_Load(object sender, EventArgs e)
    {
        ListDetailCalendar();
    }

    protected void xcpnCalendar_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');
        DateTime dateTime = DateTime.Parse("01-" + txtMonth.Text + "-" + txtYear.Text);
        switch (paras[0])
        {
            case "Prev":
                DateTime dateTimePrev = dateTime.AddMonths(-1);
                draw_calendar(dateTimePrev.Month + "", (dateTimePrev.Date.Year + 543) + "", dateTimePrev.Year + "");
                break;
            case "Next":
                DateTime dateTimeNext = dateTime.AddMonths(1);
                draw_calendar((dateTimeNext.Month + "").PadLeft(2, '0') + "", (dateTimeNext.Date.Year + 543) + "", dateTimeNext.Year + "");
                break;
            case "ListData":
                draw_calendar((dateTime.Month + "").PadLeft(2, '0') + "", (dateTime.Date.Year + 543) + "", dateTime.Year + "");
                ListDetailCalendar();
                break;
        }
    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {


        switch (e.CallbackName)
        {

            case "CUSTOMCALLBACK":
                string[] param = e.Args[0].Split(';');
                int index = 0;
                if (!string.IsNullOrEmpty(param[1]))
                {
                    index = int.Parse(param[1]);
                }


                //switch (param[0])
                //{





                //}
                break;

        }

    }

    protected void gvw2_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {


        switch (e.CallbackName)
        {

            case "CUSTOMCALLBACK":
                string[] param = e.Args[0].Split(';');
                int index = 0;
                if (!string.IsNullOrEmpty(param[1]))
                {
                    index = int.Parse(param[1]);
                }


                switch (param[0])
                {


                    case "View":

                        dynamic i = gvw2.GetRowValues(index, "REQUEST_ID", "STATUSREQ_ID", "VEH_ID", "TU_ID", "REQTYPE_ID");

                        string _reqID = i[0] + "";
                        string _statusID = i[1] + "";
                        string _VEH_ID = i[2] + "";
                        string _TU_ID = i[3] + "";
                        string _typeID = i[4] + "";

                        if (!string.IsNullOrEmpty(_reqID) && !string.IsNullOrEmpty(_statusID))
                        {
                            switch (_statusID)
                            {
                                //รอพิจารณา
                                case "01":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //ปรับแก้ไขคำขอ
                                case "02":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //รอยืนยันคิวนัดหมาย
                                case "03":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //รอเข้ารับบริการ
                                case "04":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //รอบันทึกผล
                                case "05":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //รอนำส่งเอกสารปิดงาน
                                case "06":

                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //รถไม่ได้มาตรฐาน
                                case "07":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //รอปิดงาน
                                case "08":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //รอแนบใบเสร็จ
                                case "09":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //ปิดงานแล้ว
                                case "10":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                                //มว. แจ้ง รข. ปรับแก้ไข
                                case "12":
                                    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(_statusID));
                                    break;
                            }
                        }


                        break;


                }
                break;

        }

    }

    protected void gvwDataCar_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {


        switch (e.CallbackName)
        {

            case "CUSTOMCALLBACK":
                string[] param = e.Args[0].Split(';');


                switch (param[0])
                {


                    case "ListData": ListDetailCalendar();
                        break;
                }
                break;

        }

    }


    #endregion

    private void SetComBoCause(ASPxComboBox comboBox)
    {
        comboBox.Items.Clear();
        comboBox.DataSource = sdsStatusReq;
        comboBox.ValueField = "ID";
        comboBox.TextField = "NAME";
        comboBox.DataBind();
        if (comboBox.Items.Count > 1)
        {

            comboBox.Value = "0";
        }


    }

    public void ListData()
    {
        string _vendorID = lblVendorID.Text;
        string _condition = @"";
//        string _query = @"
//SELECT SCONTRACTID,VEH_ID,TU_ID,VEH_CHASIS,TU_CHASIS,SHEADREGISTERNO,STRAILERREGISTERNO,RAILERREGISTERNUMBER,DWATEREXPIRE,STATUSID,STATUSNAME
//,REQUEST_DATE,APPOINTMENT_DATE,ACCEPT_DATE,REQUEST_ID,REQUEST_OTHER_FLAG,NDATE,RK_FLAG,SCARTYPEID,NYEAR,CREATE_DATE
//,CASE WHEN STATUSID IN ('09','02','07','06') OR NVL(STATUSID,'xxx') = 'xxx'  THEN 'RED' ELSE 'DEFALUT' END as COLOR
//FROM
//(
//    SELECT 
//    T1.SCONTRACTID
//    ,T1.STRUCKID as VEH_ID
//    ,T2.STRUCKID as TU_ID 
//    ,T1.SCHASIS as VEH_CHASIS
//    ,T2.SCHASIS as TU_CHASIS
//    ,T1.SHEADREGISTERNO
//    ,T1.STRAILERREGISTERNO 
//    ,T1.RAILERREGISTERNUMBER
//    , TO_DATE(T2.DWATEREXPIRE) as DWATEREXPIRE
//    ,R.STATUS_FLAG as STATUSID 
//    ,S.STATUSREQ_NAME as STATUSNAME
//    ,R.REQUEST_DATE
//    , TO_DATE(R.APPOINTMENT_DATE) as APPOINTMENT_DATE
//    ,R.SERVICE_DATE as ACCEPT_DATE
//    ,R.REQUEST_ID  ,R.REQUEST_OTHER_FLAG 
//    , (TO_DATE(T2.DWATEREXPIRE) - TO_DATE (SYSDATE) ) as NDATE
//    , R.RK_FLAG
//    ,T1.SCARTYPEID
//    ,extract(year from sysdate) - extract(year from NVL(NVL(T2.DREGISTER,T2.DSIGNIN),sysdate))  as NYEAR,R.CREATE_DATE
//    FROM 
//    (     
//        SELECT C.SCONTRACTID,C.SVENDORID, T.STRUCKID, T.SHEADREGISTERNO, T.STRAILERREGISTERNO, T.SCARTYPEID, T.SCHASIS,
//        T.SHEADREGISTERNO||'/'||T.STRAILERREGISTERNO   AS  RAILERREGISTERNUMBER,
//        NULL AS  DWATEREXPIRE,
//        T.CARCATE_ID, TCAT.CARCATE_NAME
//        
//        FROM TCONTRACT C
//        INNER JOIN TCONTRACT_TRUCK CT ON C.SCONTRACTID = CT.SCONTRACTID AND C.SVENDORID = '{1}' 
//        LEFT JOIN TTRUCK T ON  CT.STRUCKID = T.STRUCKID AND T.SCARTYPEID = 3 AND T.CACTIVE = 'Y' AND C.SVENDORID = '{1}' 
//        LEFT JOIN TBL_CARCATE TCAT ON T.CARCATE_ID = TCAT.CARCATE_ID
//        WHERE  (TO_DATE(C.DEND) >= TO_DATE (SYSDATE) AND TCAT.ISACTIVE_FLAG  = 'Y' )  
//    )T1             
//    LEFT JOIN TTRUCK T2 ON  T1.STRAILERREGISTERNO = T2.SHEADREGISTERNO AND T2.STRANSPORTID = '{1}'
//    LEFT JOIN TBL_REQUEST R ON T1.STRUCKID = R.VEH_ID AND T2.STRUCKID = R.TU_ID AND R.REQTYPE_ID ='01'
//    LEFT JOIN TBL_STATUSREQ S ON R.STATUS_FLAG = S.STATUSREQ_ID
//    WHERE extract(year from sysdate) - extract(year from NVL(NVL(T2.DREGISTER,T2.DSIGNIN),sysdate)) <= 8
//    UNION
//         
//    SELECT 
//    C.SCONTRACTID
//    ,T3.STRUCKID as VEH_ID
//    ,NULL as TU_ID 
//    ,T3.SCHASIS as VEH_CHASIS
//    ,NULL as TU_CHASIS
//    ,T3.SHEADREGISTERNO
//    ,NULL as STRAILERREGISTERNO 
//    ,T3.SHEADREGISTERNO as RAILERREGISTERNUMBER
//    , TO_DATE(T3.DWATEREXPIRE) as DWATEREXPIRE
//    ,R.STATUS_FLAG as STATUSID 
//    ,S.STATUSREQ_NAME as STATUSNAME
//    ,R.REQUEST_DATE
//    ,TO_DATE(R.APPOINTMENT_DATE) as APPOINTMENT_DATE
//    ,R.ACCEPT_DATE
//    ,R.REQUEST_ID  ,R.REQUEST_OTHER_FLAG 
//    , (TO_DATE(T3.DWATEREXPIRE) - TO_DATE (SYSDATE) ) as NDATE
//    , R.RK_FLAG  
//    ,T3.SCARTYPEID
//    ,extract(year from sysdate) - extract(year from NVL(NVL(T3.DREGISTER,T3.DSIGNIN),sysdate))  as NYEAR,R.CREATE_DATE
//    FROM TCONTRACT C
//    INNER JOIN TCONTRACT_TRUCK CT ON C.SCONTRACTID = CT.SCONTRACTID
//    INNER JOIN TTRUCK T3 ON  CT.STRUCKID = T3.STRUCKID AND T3.SCARTYPEID =0 AND T3.CACTIVE = 'Y' AND C.SVENDORID = '{1}' 
//    LEFT JOIN TBL_CARCATE TCAT ON T3.CARCATE_ID = TCAT.CARCATE_ID
//    LEFT JOIN TBL_REQUEST R ON NVL(T3.STRUCKID,'TRxxxxxx') =NVL( R.VEH_ID,'TRxxxxxx') /*AND NVL(T3.STRUCKID,'TRxxxxxx') = NVL(R.TU_ID,'TRxxxxxx')*/ AND R.REQTYPE_ID ='01'
//    LEFT JOIN TBL_STATUSREQ S ON R.STATUS_FLAG = S.STATUSREQ_ID
//    WHERE  TO_DATE(C.DEND) >= TO_DATE (SYSDATE) AND TCAT.ISACTIVE_FLAG  = 'Y'
//    AND extract(year from sysdate) - extract(year from NVL(NVL(T3.DREGISTER,T3.DSIGNIN),sysdate)) <= 8  /*AND NVL(S.STATUSREQ_ID,'N') = 'N'*/
//) TALL
//JOIN TBL_CONFIGURATIONS CF ON CF.CONFIG_ID = '{0}'
//WHERE  TO_DATE(TALL.DWATEREXPIRE) > TO_DATE('01/01/1900','dd/MM/yyyy')  AND  ((TO_DATE(TALL.DWATEREXPIRE) - TO_DATE (SYSDATE) ) <= CF.CONFIG_VALUE OR  RK_FLAG='Y') AND  (NVL(TALL.REQUEST_OTHER_FLAG,'xx')='xx' OR  TALL.REQUEST_OTHER_FLAG <> 'O')  {2} 
//ORDER BY REQUEST_DATE  DESC NULLS LAST ,CREATE_DATE NULLS LAST ,   NDATE NULLS LAST";

        string _query = @"SELECT C.SCONTRACTID,C.SVENDORID, T.STRUCKID, T.SHEADREGISTERNO, T.STRAILERREGISTERNO, T.SCARTYPEID, T.SCHASIS
, CASE WHEN NVL(T.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN T.SHEADREGISTERNO||'/'||T.STRAILERREGISTERNO ELSE T.SHEADREGISTERNO END   AS  RAILERREGISTERNUMBER
, CASE WHEN T.SCARTYPEID = '3' THEN R.DWATEREXPIRE ELSE T.DWATEREXPIRE END   AS  DWATEREXPIRE
, CASE WHEN T.SCARTYPEID = '3' 
THEN extract(year from sysdate) - extract(year from NVL(NVL(R.DREGISTER,R.DSIGNIN),sysdate))
 ELSE extract(year from sysdate) - extract(year from NVL(NVL(T.DREGISTER,T.DSIGNIN),sysdate)) END   AS  NDATE
,T.CARCATE_ID, TCAT.CARCATE_NAME,REQ.STATUS_FLAG as STATUSID,S.STATUSREQ_NAME as STATUSNAME
,CASE WHEN REQ.STATUS_FLAG IN ('09','02','07','06') OR NVL(REQ.STATUS_FLAG,'xxx') = 'xxx'  THEN 'RED' ELSE 'DEFALUT' END as COLOR
,REQ.REQUEST_DATE, TO_DATE(REQ.APPOINTMENT_DATE) as APPOINTMENT_DATE,REQ.SERVICE_DATE as ACCEPT_DATE,REQ.REQUEST_ID
,T.STRUCKID as VEH_ID
,R.STRUCKID as TU_ID 
,CASE WHEN REQ.WATEROFF_DATE  < SYSDATE AND REQ.STATUS_FLAG ='06' THEN 'N' ELSE 'Y' END as  LOCK_REQ
FROM TCONTRACT C
INNER JOIN TCONTRACT_TRUCK CT ON C.SCONTRACTID = CT.SCONTRACTID --AND C.SVENDORID = '{1}' 
LEFT JOIN TTRUCK T ON  CT.STRUCKID = T.STRUCKID AND T.CACTIVE = 'Y' AND T.SCARTYPEID IN ('0','3')
LEFT JOIN TTRUCK R ON  T.STRAILERID = R.STRUCKID  AND R.CACTIVE = 'Y' --AND C.SVENDORID = '{1}' 
LEFT JOIN TBL_CARCATE TCAT ON T.CARCATE_ID = TCAT.CARCATE_ID
LEFT JOIN TBL_REQUEST REQ ON T.STRUCKID = REQ.STRUCKID
LEFT JOIN TBL_STATUSREQ S ON REQ.STATUS_FLAG = S.STATUSREQ_ID
JOIN TBL_CONFIGURATIONS CF ON CF.CONFIG_ID = '{0}'
WHERE NVL(C.CACTIVE,'xxx') = 'Y' AND (TO_DATE(C.DEND) >= TO_DATE (SYSDATE) AND TCAT.ISACTIVE_FLAG  = 'Y' )   AND   (REQ.REQUEST_OTHER_FLAG != 'O' OR NVL(REQ.REQUEST_OTHER_FLAG,'xxx') = 'xxx' )  AND (REQ.RK_FLAG = 'N' OR NVL(REQ.RK_FLAG,'xxx') = 'xxx' )
AND CASE WHEN T.SCARTYPEID = '3' 
THEN extract(year from sysdate) - extract(year from NVL(NVL(R.DREGISTER,R.DSIGNIN),sysdate))
ELSE extract(year from sysdate) - extract(year from NVL(NVL(T.DREGISTER,T.DSIGNIN),sysdate)) END <= 8 AND C.SVENDORID = '{1}' {2}
AND (CASE WHEN T.SCARTYPEID = '3' THEN TRUNC(R.DWATEREXPIRE) ELSE TRUNC(T.DWATEREXPIRE) END) - TRUNC(SYSDATE) <= 75
ORDER BY REQ.REQUEST_DATE  DESC NULLS LAST ,REQ.CREATE_DATE NULLS LAST ,CASE WHEN T.SCARTYPEID = '3' 
THEN extract(year from sysdate) - extract(year from NVL(NVL(R.DREGISTER,R.DSIGNIN),sysdate))
 ELSE extract(year from sysdate) - extract(year from NVL(NVL(T.DREGISTER,T.DSIGNIN),sysdate)) END NULLS LAST";

        if ((cbStatus.SelectedItem != null ? cbStatus.SelectedItem.Value : null) != null)
        {
            if ((cbStatus.SelectedItem.Value.ToString()) != "00" && (cbStatus.SelectedItem.Value.ToString()) != "0")
            {
                _condition = @" AND REQ.STATUS_FLAG ='" + CommonFunction.ReplaceInjection(cbStatus.SelectedItem.Value.ToString()) + @"'";
            }
            else if ((cbStatus.SelectedItem.Value.ToString() + "") == "00")
            {
               // _condition = @" AND  NVL(STATUSID,'xx') ='xx' AND NVL(STATUSNAME,'xx') ='xx' ";
            }
        }
        DataTable dtData = new DataTable();
        dtData = CommonFunction.Get_Data(_conn, string.Format(_query, _sConfigID, _vendorID, _condition));
        gvw.DataSource = dtData;
        gvw.DataBind();
    }

    public void ListDataOther()
    {
        string _condition = @"";
        string _vendorID = lblVendorID.Text;
        if ((cbStatus.SelectedItem != null ? cbStatus.SelectedItem.Value : null) != null)
        {
            if ((cbStatus.SelectedItem.Value.ToString()) != "00" && (cbStatus.SelectedItem.Value.ToString()) != "0")
            {
                _condition = @" AND R.STATUS_FLAG  ='" + CommonFunction.ReplaceInjection(cbStatus.SelectedItem.Value.ToString()) + @"'";
            }

        }
        string _qry = @"
SELECT R.REQUEST_ID
,R.REQTYPE_ID
,TO_DATE(R.REQUEST_DATE) as REQUESTDATE
,TO_DATE(R.APPOINTMENT_DATE ) as APPDATE 
,TO_DATE(R.SERVICE_DATE ) as ACCEPTDATE
,S.STATUSREQ_ID 
,S.STATUSREQ_NAME 
,RT.REQTYPE_ID
,RT.REQTYPE_NAME
,CASE WHEN NVL(R.TU_NO,'xx') <>'xx' THEN R.VEH_NO||'/'||R.TU_NO ELSE  R.VEH_NO END as REGISNO
,R.VEH_ID
,TU_ID
,R.CREATE_DATE
,CASE WHEN S.STATUSREQ_ID  IN ('09','02','07','06') OR NVL(S.STATUSREQ_ID ,'xxx') = 'xxx'  THEN 'RED' ELSE 'DEFALUT' END as COLOR
,CASE WHEN R.WATEROFF_DATE  < SYSDATE AND R.STATUS_FLAG ='06' THEN 'N' ELSE 'Y' END as  LOCK_REQ
FROM TBL_REQUEST R
LEFT JOIN TBL_STATUSREQ S ON R.STATUS_FLAG = S.STATUSREQ_ID
LEFT JOIN TBL_REQTYPE RT ON R.REQTYPE_ID = RT.REQTYPE_ID
WHERE R.REQUEST_OTHER_FLAG = 'O'  AND R.RK_FLAG = 'N'
AND R.VENDOR_ID='" + CommonFunction.ReplaceInjection(_vendorID) + @"' " + _condition
+ @" ORDER BY 
R.CREATE_DATE ASC NULLS LAST,CASE WHEN S.STATUSREQ_ID IN ('10','11')  THEN null ELSE R.REQUEST_DATE  END ASC NULLS LAST ";

        DataTable dtDataOther = new DataTable();
        dtDataOther = CommonFunction.Get_Data(_conn, _qry);
        gvw2.DataSource = dtDataOther;
        gvw2.DataBind();
    }

    protected void btnRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("service_add.aspx");
    }

    void draw_calendar(string month, string year, string enyear)
    {

        txtMonth.Text = month;
        txtYear.Text = year;

        string calendar = "";
        DateTime dateTime = DateTime.Parse("01-" + month + "-" + year);

        int days = DateTime.DaysInMonth(int.Parse(enyear), int.Parse(month));
        string sDate = (!string.IsNullOrEmpty(txtDate.Text) ? txtDate.Text.PadLeft(2, '0') : "01");
        DateTime dateTimeShow;
        if (days > int.Parse(sDate))
        {
            dateTimeShow = DateTime.Parse(sDate + "-" + month + "-" + year);
            lblCalendardate.Text = dateTimeShow.ToString("dd MMMM yyyy");
        }
        else
        {
            dateTimeShow = DateTime.Parse("01" + "-" + month + "-" + year);
            DateTime endOfMonth = new DateTime(dateTimeShow.Year, dateTimeShow.Month, DateTime.DaysInMonth(int.Parse(enyear), int.Parse(month)));
            txtDate.Text = endOfMonth.Day + "";
            lblCalendardate.Text = endOfMonth.ToString("dd MMMM yyyy");
        }
        /* draw table */
        calendar += " <table width='100%' border='0' cellpadding='2' cellspacing='1'>";

        /* table headings */
        string[] headings = { "อา", "จ", "อ", "พ", "พฤ", "ศ", "ส" };
        //string[] headingscolor = { "#FF0000", "#FFFF00", "#FF00FF", "#00FF00", "#FFCC00", "#66FFFF", "#9933FF" };
        string[] headingscolor = { "#dce6f1", "#dce6f1", "#dce6f1", "#dce6f1", "#dce6f1", "#dce6f1", "#dce6f1" };
        calendar += "<tr>";
        for (int i = 0; i < headings.Length; i++)
        {
            calendar += "<td width='14%' align='center' bgcolor='" + headingscolor[i] + "'>" + headings[i] + "</td>";
        }
        calendar += "</tr>";


        #region Datatable Calendar
        DataRow dr;
        DataTable dt = new DataTable();
        dt.Columns.Add("Sun");
        dt.Columns.Add("Mon");
        dt.Columns.Add("Tue");
        dt.Columns.Add("Wed");
        dt.Columns.Add("Thu");
        dt.Columns.Add("Fri");
        dt.Columns.Add("Sat");
        dr = dt.NewRow();
        for (int i = 0; i < DateTime.DaysInMonth(dateTime.Year, dateTime.Month); i += 1)
        {
            //txtMonth.Text = Convert.ToDateTime(dateTime.AddDays(0)).ToString("dddd");
            if (dateTime.AddDays(i).DayOfWeek + "" == "Sunday")
            {
                dr["Sun"] = i + 1;
            }
            if (dateTime.AddDays(i).DayOfWeek + "" == "Monday")
            {
                dr["Mon"] = i + 1;
            }
            if (dateTime.AddDays(i).DayOfWeek + "" == "Tuesday")
            {
                dr["Tue"] = i + 1;
            }
            if (dateTime.AddDays(i).DayOfWeek + "" == "Wednesday")
            {
                dr["Wed"] = i + 1;

            }
            if (dateTime.AddDays(i).DayOfWeek + "" == "Thursday")
            {
                dr["Thu"] = i + 1;
            }
            if (dateTime.AddDays(i).DayOfWeek + "" == "Friday")
            {
                dr["Fri"] = i + 1;

            }
            if (dateTime.AddDays(i).DayOfWeek + "" == "Saturday")
            {
                dr["Sat"] = i + 1;
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                continue;
            }
            if (i == DateTime.DaysInMonth(dateTime.Year, dateTime.Month) - 1)
            {
                dt.Rows.Add(dr);
                dr = dt.NewRow();
            }

        }
        #endregion

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            //int nDay = i + 1;
            calendar += "<tr>";
            calendar += "<td height='27' width='14%' align='right' class='radius' style='background-color:#FFFFFF;border-radius:2px;'><a onclick='ListData(" + CommonFunction.ReplaceInjection(dt.Rows[i]["Sun"] + "") + ")' style='width:100%;cursor:pointer'>  <div style='position:relative;top:-6px'><span class='Txt01'>" + (dt.Rows[i]["Sun"] + "") + "</span><div style='position:relative;top:12px;float:left'>" + Event_Calendar(dt.Rows[i]["Sun"] + "", month, enyear) + "</div></div></a></td>";
            calendar += "<td height='27' width='14%' align='right' class='radius' style='background-color:#FFFFFF;border-radius:2px;'><a onclick='ListData(" + CommonFunction.ReplaceInjection(dt.Rows[i]["Mon"] + "") + ")' style='width:100%;cursor:pointer'>  <div style='position:relative;top:-6px'><span class='Txt01'>" + (dt.Rows[i]["Mon"] + "") + "</span><div style='position:relative;top:12px;float:left'>" + Event_Calendar(dt.Rows[i]["Mon"] + "", month, enyear) + "</div></div></a></td>";
            calendar += "<td height='27' width='14%' align='right' class='radius' style='background-color:#FFFFFF;border-radius:2px;'><a onclick='ListData(" + CommonFunction.ReplaceInjection(dt.Rows[i]["Tue"] + "") + ")' style='width:100%;cursor:pointer'>  <div style='position:relative;top:-6px'><span class='Txt01'>" + (dt.Rows[i]["Tue"] + "") + "</span><div style='position:relative;top:12px;float:left'>" + Event_Calendar(dt.Rows[i]["Tue"] + "", month, enyear) + "</div></div></a></td>";
            calendar += "<td height='27' width='14%' align='right' class='radius' style='background-color:#FFFFFF;border-radius:2px;'><a onclick='ListData(" + CommonFunction.ReplaceInjection(dt.Rows[i]["Wed"] + "") + ")' style='width:100%;cursor:pointer'>  <div style='position:relative;top:-6px'><span class='Txt01'>" + (dt.Rows[i]["Wed"] + "") + "</span><div style='position:relative;top:12px;float:left'>" + Event_Calendar(dt.Rows[i]["Wed"] + "", month, enyear) + "</div></div></a></td>";
            calendar += "<td height='27' width='14%' align='right' class='radius' style='background-color:#FFFFFF;border-radius:2px;'><a onclick='ListData(" + CommonFunction.ReplaceInjection(dt.Rows[i]["Thu"] + "") + ")' style='width:100%;cursor:pointer'>  <div style='position:relative;top:-6px'><span class='Txt01'>" + (dt.Rows[i]["Thu"] + "") + "</span><div style='position:relative;top:12px;float:left'>" + Event_Calendar(dt.Rows[i]["Thu"] + "", month, enyear) + "</div></div></a></td>";
            calendar += "<td height='27' width='14%' align='right' class='radius' style='background-color:#FFFFFF;border-radius:2px;'><a onclick='ListData(" + CommonFunction.ReplaceInjection(dt.Rows[i]["Fri"] + "") + ")' style='width:100%;cursor:pointer'>  <div style='position:relative;top:-6px'><span class='Txt01'>" + (dt.Rows[i]["Fri"] + "") + "</span><div style='position:relative;top:12px;float:left'>" + Event_Calendar(dt.Rows[i]["Fri"] + "", month, enyear) + "</div></div></a></td>";
            calendar += "<td height='27' width='14%' align='right' class='radius' style='background-color:#FFFFFF;border-radius:2px;'><a onclick='ListData(" + CommonFunction.ReplaceInjection(dt.Rows[i]["Sat"] + "") + ")' style='width:100%;cursor:pointer'>  <div style='position:relative;top:-6px'><span class='Txt01'>" + (dt.Rows[i]["Sat"] + "") + "</span><div style='position:relative;top:12px;float:left'>" + Event_Calendar(dt.Rows[i]["Sat"] + "", month, enyear) + "</div></div></a></td>";
            calendar += "</tr>";
        }

        calendar += " </table>";
        ltrCalendar.Text = calendar;

    }

    private string Event_Calendar(string date, string month, string year)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(date))
        {
            string QUERY = @"SELECT TRK.SHEADREGISTERNO, TRK.STRAILERREGISTERNO,TRK.DWATEREXPIRE,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID,
CASE WHEN TRK.DWATEREXPIRE < SYSDATE THEN '3'
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx' OR TRK.DWATEREXPIRE < SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx'  THEN '2'
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') = 'xxx' THEN '1'
ELSE '0' END as STATUS
FROM  TTRUCK TRK INNER JOIN  (SELECT distinct tct.SCONTRACTID,tct.STRUCKID, ct.SCONTRACTNO,ct.SVENDORID, ct.GROUPSID,tv.SABBREVIATION
                        FROM TCONTRACT_TRUCK tct
                        LEFT JOIN TCONTRACT ct  ON tct.SCONTRACTID         = ct.SCONTRACTID
                        LEFT JOIN TVENDOR tv on tv.SVENDORID = ct.SVENDORID
                        WHERE NVL(ct.CACTIVE,'Y')  = 'Y'
                        ) ct2 ON NVL(TRK.STRUCKID,1) = NVL(CT2.STRUCKID,1)
LEFT JOIN TBL_REQUEST REQ
ON TRK.STRUCKID = REQ.STRUCKID
WHERE TO_CHAR(TRK.DWATEREXPIRE,'dd/MM/yyyy') = '" + CommonFunction.ReplaceInjection(date.PadLeft(2, '0')) + "/" + CommonFunction.ReplaceInjection(month) + "/" + CommonFunction.ReplaceInjection(year) + @"'
AND TRK.STRANSPORTID = '" + CommonFunction.ReplaceInjection(lblVendorID.Text) + @"'
ORDER BY 
CASE WHEN TRK.DWATEREXPIRE < SYSDATE THEN 3
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx' OR TRK.DWATEREXPIRE < SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx'  THEN 2
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') = 'xxx' THEN 1
ELSE 0 END
DESC";

            DataTable dt = CommonFunction.Get_Data(_conn, QUERY);
            if (dt.Rows.Count > 0)
            {
                switch (dt.Rows[0]["STATUS"] + "")
                {
                    case "3": Result = "<img src='images/em.gif' width='16' height='16' />";
                        break;
                    case "2": Result = "<img src='images/green.png' width='16' height='16' />";
                        break;
                    case "1": Result = "<img src='images/red.png' width='16' height='16' />";
                        break;
                }
            }
        }
        else
        {
            Result = "";
        }
        return Result;
    }

    void ListDetailCalendar()
    {
        if (!string.IsNullOrEmpty(txtDate.Text))
        {

            DateTime dateTimeShow = DateTime.Parse((!string.IsNullOrEmpty(txtDate.Text) ? txtDate.Text.PadLeft(2, '0') : "01") + "-" + txtMonth.Text + "-" + txtYear.Text);
            lblCalendardate.Text = dateTimeShow.ToString("dd MMMM yyyy");
            string Query = @"SELECT TRK.SHEADREGISTERNO, TRK.STRAILERREGISTERNO,TRK.DWATEREXPIRE,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID,
CASE WHEN TRK.DWATEREXPIRE < SYSDATE THEN '3'
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx' OR TRK.DWATEREXPIRE < SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx'  THEN '2'
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') = 'xxx' THEN '1'
ELSE '0' END as STATUS,
CASE WHEN TRK.DWATEREXPIRE < SYSDATE THEN 'images/em.gif'
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx' OR TRK.DWATEREXPIRE < SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx'  THEN 'images/green.png'
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') = 'xxx' THEN 'images/red.png'
ELSE '' END as IMG,
CASE WHEN NVL(TRK.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TRK.STRAILERREGISTERNO||'/'||TRK.SHEADREGISTERNO||' (expire '||TO_CHAR(ADD_MONTHS(TRK.DWATEREXPIRE,6516),'dd/MM/yyyy')||')'
WHEN NVL(TRK.STRAILERREGISTERNO,'xxx') = 'xxx' THEN TRK.SHEADREGISTERNO||' (expire '||TO_CHAR(ADD_MONTHS(TRK.DWATEREXPIRE,6516),'dd/MM/yyyy')||')'
ELSE '' END  as DETAIL
FROM  TTRUCK TRK INNER JOIN  (SELECT distinct tct.SCONTRACTID,tct.STRUCKID, ct.SCONTRACTNO,ct.SVENDORID, ct.GROUPSID,tv.SABBREVIATION
                        FROM TCONTRACT_TRUCK tct
                        LEFT JOIN TCONTRACT ct  ON tct.SCONTRACTID         = ct.SCONTRACTID
                        LEFT JOIN TVENDOR tv on tv.SVENDORID = ct.SVENDORID
                        WHERE NVL(ct.CACTIVE,'Y')  = 'Y'
                        ) ct2 ON NVL(TRK.STRUCKID,1) = NVL(CT2.STRUCKID,1)
LEFT JOIN TBL_REQUEST REQ
ON TRK.STRUCKID = REQ.STRUCKID
WHERE TO_CHAR(TRK.DWATEREXPIRE,'dd/MM/yyyy') = '" + CommonFunction.ReplaceInjection(txtDate.Text.PadLeft(2, '0')) + "/" + CommonFunction.ReplaceInjection(txtMonth.Text) + "/" + CommonFunction.ReplaceInjection((int.Parse(txtYear.Text) - 543) + "") + @"'
AND TRK.STRANSPORTID = '" + CommonFunction.ReplaceInjection(lblVendorID.Text) + @"'
ORDER BY 
CASE WHEN TRK.DWATEREXPIRE < SYSDATE THEN 3
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx' OR TRK.DWATEREXPIRE < SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') <>'xxx'  THEN 2
WHEN TRK.DWATEREXPIRE > SYSDATE AND NVL(REQ.REQUEST_ID,'xxx') = 'xxx' THEN 1
ELSE 0 END
DESC";
            DataTable dt = CommonFunction.Get_Data(_conn, Query);
            if (dt.Rows.Count > 0)
            {
                gvwDataCar.DataSource = dt;

            }
            gvwDataCar.DataBind();
        }
    }
}