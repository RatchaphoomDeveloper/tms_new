﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_Entity;
using Utility;
using TMS_BLL.Transaction.Complain;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class questionnaire_form : PageBase
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string PageFileUpload = "AUDIT_FILE";
    string PagePermission = "questionnaire.aspx";
    string GroupPermission = "01";
    #region " Prop "
    protected USER_PERMISSION Page_Status
    {
        get { return (USER_PERMISSION)ViewState[this.ToString() + "Page_Status"]; }
        set { ViewState[this.ToString() + "Page_Status"] = value; }
    }
    protected PAGE_ACTION PageAction
    {
        get { return (PAGE_ACTION)ViewState[this.ToString() + "PageAction"]; }
        set { ViewState[this.ToString() + "PageAction"] = value; }
    }

    #region " GRV SUMSCORE "
    protected double SumWeight
    {
        get { return (double)ViewState[this.ToString() + "SumWeight"]; }
        set { ViewState[this.ToString() + "SumWeight"] = value; }
    }
    protected double SumAverage
    {
        get { return (double)ViewState[this.ToString() + "SumAverage"]; }
        set { ViewState[this.ToString() + "SumAverage"] = value; }
    }
    protected double SumScore
    {
        get { return (double)ViewState[this.ToString() + "SumScore"]; }
        set { ViewState[this.ToString() + "SumScore"] = value; }
    }
    protected double SumResult
    {
        get { return (double)ViewState[this.ToString() + "SumResult"]; }
        set { ViewState[this.ToString() + "SumResult"] = value; }
    }
    protected double SumAverageScore
    {
        get { return (double)ViewState[this.ToString() + "SumAverageScore"]; }
        set { ViewState[this.ToString() + "SumAverageScore"] = value; }
    }
    protected double SumItems
    {
        get { return (double)ViewState[this.ToString() + "SumItems"]; }
        set { ViewState[this.ToString() + "SumItems"] = value; }
    }
    protected double SumPercentage
    {
        get { return (double)ViewState[this.ToString() + "SumPercentage"]; }
        set { ViewState[this.ToString() + "SumPercentage"] = value; }
    }
    #endregion " GRV SUMSCORE "

    protected string EDIT_NTYPEVISITFORMID
    {
        get { return ViewState[this.ToString() + "NTYPEVISITFORMID"].ToString(); }
        set { ViewState[this.ToString() + "NTYPEVISITFORMID"] = value; }
    }
    protected string QDocID
    {
        get { return ViewState[this.ToString() + "QDocID"].ToString(); }
        set { ViewState[this.ToString() + "QDocID"] = value; }
    }
    protected string QTeamID
    {
        get { return ViewState[this.ToString() + "QTeamID"].ToString(); }
        set { ViewState[this.ToString() + "QTeamID"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected DataTable dtScoreVendorInForm
    {
        get { return (DataTable)ViewState[this.ToString() + "dtScoreVendorInForm"]; }
        set { ViewState[this.ToString() + "dtScoreVendorInForm"] = value; }
    }
    protected DataTable YearDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "YearDDL"]; }
        set { ViewState[this.ToString() + "YearDDL"] = value; }
    }
    protected DataTable VendorDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "VendorDDL"]; }
        set { ViewState[this.ToString() + "VendorDDL"] = value; }
    }

    public Questionnaire QuestionnaireData
    {
        get { return (Questionnaire)ViewState[this.ToString() + "QuestionnaireData"]; }
        set { ViewState[this.ToString() + "QuestionnaireData"] = value; }
    }

    /// <summary>
    /// Form ID สำหรับแก้ไข
    /// </summary>
    public string sNTYPEVISITFORMID1
    {
        get { return ViewState[this.ToString() + "sNTYPEVISITFORMID1"].ToString(); }
        set { ViewState[this.ToString() + "sNTYPEVISITFORMID1"] = value; }
    }
    public string sNTYPEVISITFORM_Y1
    {
        get { return ViewState[this.ToString() + "sNTYPEVISITFORM_Y1"].ToString(); }
        set { ViewState[this.ToString() + "sNTYPEVISITFORM_Y1"] = value; }
    }

    public List<RadioValueList> DTRdo1
    {
        get { return (List<RadioValueList>)ViewState[this.ToString() + "DTRdo1"]; }
        set { ViewState[this.ToString() + "DTRdo1"] = value; }
    }

    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        try
        {
            if (!IsPostBack)
            {
                user_profile = SessionUtility.GetUserProfileSession();
                CheckPermission();
                QuestionnaireData = new Questionnaire();
                QDocID = "0";
                QTeamID = string.Empty;
                EDIT_NTYPEVISITFORMID = "0";
                InitForm();

                SumWeight = 0.00;
                SumAverage = 0.00;
                SumScore = 0.00;
                SumResult = 0.00;
                SumPercentage = 0.00;
                SumItems = 0.00;
                SumAverageScore = 0.0;

                if (Session["EditNQUESTIONNAIREID"] != null)
                {
                    PageAction = PAGE_ACTION.EDIT;
                    QDocID = Session["EditNQUESTIONNAIREID"].ToString();
                    Session["EditNQUESTIONNAIREID"] = null;
                    BindEdit();
                }
                else if (Session["ViewNQUESTIONNAIREID"] != null)
                {
                    PageAction = PAGE_ACTION.VIEW;
                    QDocID = Session["ViewNQUESTIONNAIREID"].ToString();
                    Session["ViewNQUESTIONNAIREID"] = null;
                    BindEdit();
                    SetViewMode();
                }
                else
                {
                    PageAction = PAGE_ACTION.ADD;
                    ((HtmlAnchor)this.FindControlRecursive(Page, "ProcessTab")).Visible = false;
                    TabGroup.Visible = false;

                    DateTime today = DateTime.Today;
                    string s_today = today.ToString("dd/MM/yyyy");
                    txtDCREATE.Text = s_today;
                }
                InitialUpload();
                this.AssignAuthen();
            }

            RegisterPostDownload();
        }
        catch (Exception ex)
        {
            Session["EditNQUESTIONNAIREID"] = null;
            Session["ViewNQUESTIONNAIREID"] = null;
            alertFail(ex.Message);
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnAddForm.Enabled = false;
                btnAddTeam.Enabled = false;
                btnNotSave.Enabled = false;
                btnSaveBeforeChange.Enabled = false;
                btnSaveChange.Enabled = false;
                btnSaveTab2.Enabled = false;
                cmdUpload.Enabled = false;

                //View Mode
                ddlFormName.Enabled = false;
                ddlVendor.Enabled = false;
                ddlYear.Enabled = false;
                txtAddress.Enabled = false;
                txtRemark.Enabled = false;
                txtTQUESTIONNAIRE_TEAM.Enabled = false;
                txtDCHECK.Enabled = false;
                cboUploadType.Enabled = false;
                fileUpload.Enabled = false;
                rdoTeamType.Enabled = false;
                chkIS_VENDOR_VIEW.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public void SetViewMode()
    {
        btnAddForm.Visible = false;
        btnAddTeam.Visible = false;
        ddlFormName.Enabled = false;
        ddlVendor.Enabled = false;
        ddlYear.Enabled = false;

        txtAddress.Enabled = false;
        txtRemark.Enabled = false;
        txtTQUESTIONNAIRE_TEAM.Enabled = false;
        txtDCHECK.Enabled = false;

        cmdUpload.Visible = false;
        cboUploadType.Enabled = false;
        fileUpload.Enabled = false;
        rdoTeamType.Enabled = false;
        btnSaveTab2.Visible = false;
        chkIS_VENDOR_VIEW.Enabled = false;
    }
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null)
                return controlToReturn;
        }
        return null;
    }
    public void BindEdit()
    {
        try
        {
            string _err = string.Empty;
            QuestionnaireData = new QuestionnaireBLL().GetQuestionnaire(ref _err, int.Parse(QDocID));
            if (QuestionnaireData != null)
            {
                EDIT_NTYPEVISITFORMID = QuestionnaireData.NTYPEVISITFORMID.ToString();
                txtRemark.Text = QuestionnaireData.SREMARK;
                txtAddress.Text = QuestionnaireData.SADDRESS;
                txtDCHECK.Text = QuestionnaireData.SDCHECK;
                txtDCREATE.Text = QuestionnaireData.SDCREATE;
                ListItem selV = ddlVendor.Items.FindByValue(QuestionnaireData.SVENDORID);
                if (selV != null) selV.Selected = true;
                else if (!string.IsNullOrEmpty(QuestionnaireData.SVENDORID))
                {
                    var vendor = new QuestionnaireBLL().GetVendorByID(ref _err, QuestionnaireData.SVENDORID);
                    ddlVendor.Items.Insert(0, new ListItem() { Value = QuestionnaireData.SVENDORID, Text = vendor.SVENDORNAME });
                }

                ListItem selY = ddlYear.Items.FindByValue(QuestionnaireData.YEAR);
                ListItem selY2 = ddlYearTab2.Items.FindByValue(QuestionnaireData.YEAR);
                if (selY != null)
                {
                    ddlYear.ClearSelection();
                    selY.Selected = true;
                }
                if (selY2 != null)
                {
                    ddlYearTab2.ClearSelection();
                    selY2.Selected = true;
                }


                if (QuestionnaireData.IS_VENDOR_VIEW == "1")
                    chkIS_VENDOR_VIEW.Checked = true;
                else chkIS_VENDOR_VIEW.Checked = false;

                LoadFormName();
                LoadGroup();
                LoadItem();
                LoadTableScore();

                ddlYear.Enabled = false;
                ddlVendor.Enabled = false;

                grvTQUESTIONNAIRE_TEAM.DataSource = QuestionnaireData.LST_TEAM;
                grvTQUESTIONNAIRE_TEAM.DataBind();

                GetAllScore();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void GetScoreNotInCurrentGroup()
    {
        if ((ddlYear.SelectedValue == null) || (ddlFormName.SelectedValue == null) || (ddlGroupName.SelectedValue == null)) return;

        string _err = string.Empty;
        TQUESTIONNAIRE search = new TQUESTIONNAIRE() { NYEAR = ddlYear.SelectedValue, NTYPEVISITFORMID = int.Parse(ddlFormName.SelectedValue), SVENDORID = QuestionnaireData.SVENDORID, NGROUP = ddlGroupName.SelectedValue };
        var ScoreNotInCurrentGroup = new QuestionnaireBLL().GetTotalScore(ref _err, search);
        hdScoreNotInCurrentGroup.Value = String.Format("{0:n2}", ScoreNotInCurrentGroup.ToString());
    }
    public void GetAllScore()
    {
        try
        {
            if (string.IsNullOrEmpty(ddlYear.SelectedValue) || string.IsNullOrEmpty(ddlFormName.SelectedValue)) return;

            string _err = string.Empty;
            TQUESTIONNAIRE search = new TQUESTIONNAIRE() { NYEAR = ddlYear.SelectedValue, NTYPEVISITFORMID = int.Parse(ddlFormName.SelectedValue), SVENDORID = QuestionnaireData.SVENDORID };
            var TotalScore = new QuestionnaireBLL().GetTotalScore(ref _err, search);
            txtPoint.Text = String.Format("{0:n2}", TotalScore.ToString());
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    public void InitForm()
    {
        int cYear = DateTime.Today.Year;
        int endYear = cYear + 4;

        string _err = string.Empty;
        YearDDL = new QuestionnaireBLL().GetListYearForInputForm(ref _err);
        if (YearDDL.Rows.Count > 0)
        {
            ddlYear.DataValueField = "YEAR";
            ddlYear.DataTextField = "YEAR";
            ddlYear.DataSource = YearDDL;
            ddlYear.DataBind();

            ddlYearTab2.DataValueField = "YEAR";
            ddlYearTab2.DataTextField = "YEAR";
            ddlYearTab2.DataSource = YearDDL;
            ddlYearTab2.DataBind();

            ListItem sel = ddlYear.Items.FindByValue(cYear.ToString());
            if (sel != null) sel.Selected = true;
            else ddlYear.SelectedIndex = 0;

            ListItem sel2 = ddlYearTab2.Items.FindByValue(cYear.ToString());
            if (sel2 != null) sel2.Selected = true;
            else ddlYearTab2.SelectedIndex = 0;
        }
        else
        {
            alertSuccess("ไม่พบข้อมูลแบบประเมินที่สามารถใช้งานได้", "questionnaire.aspx");
            return;
        }
        // เลือกปีปัจจุบัน


        LoadVendor();
        LoadFormName();
        LoadGroup();
        LoadItem();
        LoadTeamType();
        LoadTeam();
    }

    public void LoadTeam()
    {
        try
        {
            if (int.Parse(QDocID) == 0)
            {
                QuestionnaireData.LST_TEAM = new List<TQUESTIONNAIRE_TEAM>();
            }
            grvTQUESTIONNAIRE_TEAM.DataSource = QuestionnaireData.LST_TEAM;
            grvTQUESTIONNAIRE_TEAM.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void LoadTeamType()
    {
        try
        {
            string _err = string.Empty;

            rdoTeamType.DataSource = new QuestionnaireBLL().GetQUESTIONNAIRE_TEAM_TYPE(ref _err);
            rdoTeamType.DataValueField = "CONFIG_VALUE";
            rdoTeamType.DataTextField = "CONFIG_NAME";
            rdoTeamType.DataBind();
            if (rdoTeamType.Items.Count > 0) rdoTeamType.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void LoadItem()
    {
        try
        {
            if (ddlGroupName.SelectedItem != null)
            {
                string _err = string.Empty;
                GridView1.DataSource = new QuestionnaireBLL().GetDetailFormQ(ref _err, int.Parse(ddlGroupName.SelectedItem.Value));
                GridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    public void GetRadioList(int NTYPEVISITFORMID)
    {
        string _err = string.Empty;
        DTRdo1 = new QuestionnaireBLL().GetRadio(ref _err, int.Parse(NTYPEVISITFORMID.ToString()), true);
        txtMaxRdo.Text = DTRdo1.Max(x => x.NTYPEVISITLISTSCORE).ToString();
    }

    public void LoadGroup()
    {
        try
        {
            if (ddlFormName.SelectedItem != null)
            {
                string _err = string.Empty;
                ddlGroupName.DataValueField = "NGROUPID";
                ddlGroupName.DataTextField = "SGROUPNAME";
                ddlGroupName.DataSource = new QuestionnaireBLL().GetTGROUPOFVISITFORM(ref _err, int.Parse(ddlFormName.SelectedItem.Value), true);
                ddlGroupName.DataBind();
                ddlGroupName.Items.Insert(0, new ListItem() { Text = "- เลือกข้อมูล -", Value = "0" });
                GetRadioList(int.Parse(ddlFormName.SelectedItem.Value));
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    public void LoadTableScore()
    {
        SumAverageScore = 0.00;
        string _err = string.Empty;
        string year = string.Empty;
        TQUESTIONNAIRE search = new TQUESTIONNAIRE() { NYEAR = "0", SVENDORID = "" };

        if (ddlYear.SelectedItem != null) search.NYEAR = ddlYear.SelectedItem.Value;
        if (!string.IsNullOrEmpty(QuestionnaireData.SVENDORID))
        {
            search.SVENDORID = QuestionnaireData.SVENDORID;
        }
        dtScoreVendorInForm = new QuestionnaireBLL().GetScoreVendorInForm(ref _err, search);
        grvScore.DataSource = dtScoreVendorInForm;
        grvScore.DataBind();
    }
    public void LoadFormName()
    {
        try
        {
            string _err = string.Empty;
            string year = string.Empty;
            if (ddlYear.SelectedItem != null) year = ddlYear.SelectedItem.Value;

            DropDownListHelper.BindDropDownList(ref ddlFormName, new QuestionnaireBLL().GetFormNameForInput(ref _err, year), "NTYPEVISITFORMID", "STYPEVISITFORMNAME", false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    public void LoadVendor()
    {
        string year = ddlYear.SelectedItem.Value;
        try
        {
            string _err = string.Empty;
            VendorDDL = new QuestionnaireBLL().GetVendorNotEntryQuestion(ref _err, year);
            DataRow dr = VendorDDL.NewRow();
            dr["SABBREVIATION"] = "";
            VendorDDL.Rows.InsertAt(dr, 0);
            ddlVendor.DataSource = VendorDDL;
            ddlVendor.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        if (user_profile.CGROUP == "0")
        {
            GotoDefault();
            return;
        }

        //string _err = string.Empty;
        //Page_Status = new QuestionnaireBLL().CheckPermission(ref _err, Session["UserID"].ToString(), PagePermission);
        //if (Page_Status == USER_PERMISSION.DISABLE)
        //{
        //    ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
        //}
        //else if (Page_Status == USER_PERMISSION.VIEW)
        //{
        //    SetViewMode();
        //}


    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }

    #region " Upload File New "

    public List<F_UPLOAD> DTFile
    {
        get
        {
            if ((List<F_UPLOAD>)ViewState["dtUpload"] != null)
                return (List<F_UPLOAD>)ViewState["dtUpload"];
            else
                return null;
        }
        set { ViewState["dtUpload"] = value; }
    }
    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DTFile.RemoveAt(e.RowIndex);
            dgvUploadFile.DataSource = DTFile;
            dgvUploadFile.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView gv = (GridView)sender;
        string FullPath = gv.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string path = Path.GetDirectoryName(FullPath);
        string FileName = Path.GetFileName(FullPath);

        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(path + "\\" + FileName);
        Response.End();
    }

    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");

                //if (PageAction == PAGE_ACTION.VIEW || Page_Status == USER_PERMISSION.VIEW)
                //{
                //    imgDelete.Visible = false;
                //}
                //else
                //{
                //    imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                //}
                if (!CanWrite)
                {
                    imgDelete.Visible = false;
                }
                else
                {
                    imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                }
                ImageButton imgView;
                imgView = (ImageButton)e.Row.Cells[0].FindControl("imgView");

                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(imgView);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void RegisterPostDownload()
    {
        foreach (GridViewRow row in dgvUploadFile.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow) continue;

            ImageButton imgView = row.Cells[0].FindControl("imgView") as ImageButton;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(imgView);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Quesrionnaire" + "\\" + QDocID;
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialUpload()
    {
        try
        {
            dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("AUDIT_FILE");
            DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);
            DTFile = new List<F_UPLOAD>();
            if (int.Parse(QDocID) > 0)
            {
                DTFile = QuestionnaireData.LST_FILE_UPLOAD;
                dgvUploadFile.DataSource = DTFile;
                dgvUploadFile.DataBind();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }

    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                DTFile.Add(new F_UPLOAD()
                {
                    FILENAME_SYSTEM = System.IO.Path.GetFileName(Path + "\\" + FileNameSystem),
                    FULLPATH = Path + "\\" + FileNameSystem,
                    ID = 0,
                    FILENAME_USER = FileNameUser,
                    UPLOAD_ID = int.Parse(cboUploadType.SelectedValue),
                    UPLOAD_NAME = cboUploadType.SelectedItem.Text
                });

                dgvUploadFile.DataSource = DTFile;
                dgvUploadFile.DataBind();
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSaveUpload()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            foreach (var item in DTFile)
            {
                data.Add(decimal.Parse(item.UPLOAD_ID.ToString()));
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion " Upload File New "
    protected void grvTQUESTIONNAIRE_TEAM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var grv = sender as GridView;
        if (e.CommandName == "RowSelect")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            string TEAM_TYPE = grv.DataKeys[rowIndex.Value]["TEAM_TYPE"].ToString();
            string SNAME = grv.DataKeys[rowIndex.Value]["SNAME"].ToString();
            txtTQUESTIONNAIRE_TEAM.Text = SNAME;
            ListItem sel = rdoTeamType.Items.FindByValue(TEAM_TYPE);
            if (sel != null)
            {
                rdoTeamType.ClearSelection();
                sel.Selected = true;
            }

            QTeamID = rowIndex.ToString();
        }
    }
    protected void ddlFormName_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadGroup();
        LoadItem();
    }

    public bool ValidateSaveTab1()
    {
        if (string.IsNullOrEmpty(ddlYear.SelectedValue))
        {
            alertFail("กรุณาระบุบปีประเมิน");
            return false;
        }
        if (string.IsNullOrEmpty(ddlVendor.SelectedValue))
        {
            alertFail("กรุณาระบุบริษัทผู้ประกอบการขนส่ง");
            return false;
        }

        if (string.IsNullOrEmpty(txtDCHECK.Text))
        {
            alertFail("กรุณาระบุวันที่ตรวจประเมิน");
            return false;
        }
        return true;
    }
    protected void btnAddForm_Click(object sender, EventArgs e)
    {
        if (!ValidateSaveTab1()) return;
        try
        {
            int? refID = 0;
            double? TotalPoint = 0;
            if (int.Parse(QDocID) > 0)
            {
                #region " Edit "
                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    using (OracleTransaction tran = con.BeginTransaction())
                    {
                        try
                        {
                            string _err = string.Empty;
                            string _strDel = "DELETE FROM TQUESTIONNAIRE_TEAM WHERE NQUESTIONNAIREID = " + QDocID;
                            using (OracleCommand comDel = new OracleCommand(_strDel, con)) { comDel.Transaction = tran; comDel.ExecuteNonQuery(); }

                            string strsql1 = @"INSERT INTO TQUESTIONNAIRE_TEAM ( NQUESTIONNAIREID, SNAME,  DCREATE, SCREATE,TEAM_INDEX,TEAM_TYPE,QTEAM_ID) VALUES ( :NQUESTIONNAIREID  ,:SNAME , SYSDATE , :SCREATE ,:NINDEX,:TEAM_TYPE,:QTEAM_ID )";

                            QuestionnaireData.LST_TEAM.OrderBy(x => x.TEAM_INDEX);
                            int index = 0;
                            foreach (var itm in QuestionnaireData.LST_TEAM)
                            {

                                int? TeamRef = new QuestionnaireBLL().GetTeamID(ref _err);
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = int.Parse(QDocID);
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = itm.SNAME;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.Parameters.Add(":NINDEX", OracleType.Number).Value = index;
                                    com1.Parameters.Add(":TEAM_TYPE", OracleType.Number).Value = itm.TEAM_TYPE;
                                    com1.Parameters.Add(":QTEAM_ID", OracleType.Number).Value = TeamRef;
                                    com1.ExecuteNonQuery();
                                }
                                index++;
                            }

                            #region " File "

                            var delFile = DTFile.Where(x => x.ID != 0).Select(r => r.ID);
                            if (delFile != null)
                            {
                                string _strCondition = string.Join("','", DTFile.Select(r => r.ID));
                                string _whereCondition = string.Empty;
                                if (!string.IsNullOrEmpty(_strCondition))
                                {
                                    _whereCondition = string.Format(" ID NOT IN ('{0}') and ", _strCondition);
                                }
                                using (OracleCommand comDel1 = new OracleCommand(string.Format(@"UPDATE F_UPLOAD SET ISACTIVE = 0 WHERE {0} ref_int = {1} and ref_str = '{2}'", _whereCondition, QDocID, PageFileUpload), con))
                                {
                                    comDel1.Transaction = tran;
                                    comDel1.Parameters.Clear();
                                    comDel1.ExecuteNonQuery();
                                }
                            }

                            foreach (var item in DTFile)
                            {
                                if (item.ID == 0)
                                {
                                    using (OracleCommand com1 = new OracleCommand(@"INSERT INTO F_UPLOAD (FILENAME_SYSTEM, FILENAME_USER, UPLOAD_ID, REF_INT, REF_STR, ISACTIVE, CREATE_BY, FULLPATH) VALUES (:FILENAME_SYSTEM, :FILENAME_USER, :UPLOAD_ID, :REF_INT, :REF_STR, :ISACTIVE, :CREATE_BY, :FULLPATH)", con))
                                    {
                                        com1.Transaction = tran;
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":FILENAME_SYSTEM", OracleType.VarChar).Value = item.FILENAME_SYSTEM;
                                        com1.Parameters.Add(":FILENAME_USER", OracleType.VarChar).Value = item.FILENAME_USER;
                                        com1.Parameters.Add(":UPLOAD_ID", OracleType.VarChar).Value = item.UPLOAD_ID;
                                        com1.Parameters.Add(":REF_INT", OracleType.Number).Value = QDocID;
                                        com1.Parameters.Add(":REF_STR", OracleType.VarChar).Value = PageFileUpload;
                                        com1.Parameters.Add(":ISACTIVE", OracleType.Number).Value = "1";
                                        com1.Parameters.Add(":CREATE_BY", OracleType.VarChar).Value = Session["UserID"];
                                        com1.Parameters.Add(":FULLPATH", OracleType.VarChar).Value = item.FULLPATH;
                                        com1.ExecuteNonQuery();
                                    }
                                }
                            }

                            #endregion " File "


                            string strsql = @"UPDATE TQUESTIONNAIRE SET SVENDORID =:SVENDORID, DCHECK=to_date(:DCHECK,'DD/MM/YYYY'),SADDRESS =:SADDRESS,SREMARK = :SREMARK,DUPDATE= SYSDATE,SUPDATE  =:SUPDATE,NVALUE   =:NVALUE WHERE  NQUESTIONNAIREID =:NQUESTIONNAIREID ";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Transaction = tran;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = ddlVendor.SelectedItem.Value;
                                com.Parameters.Add(":DCHECK", OracleType.VarChar).Value = txtDCHECK.Text;
                                com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":NVALUE", OracleType.Number).Value = TotalPoint;
                                com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;
                                com.ExecuteNonQuery();
                            }
                            tran.Commit();
                            alertSuccess(" บันทึกข้อมูลเรียบร้อย ", "questionnaire_form.aspx");
                            Session["EditNQUESTIONNAIREID"] = QDocID;
                        }
                        catch (Exception ex)
                        {
                            alertFail(ex.Message);
                        }
                    }

                }
                #endregion " Edit "
            }
            else
            {
                #region " Add "
                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    using (OracleTransaction tran = con.BeginTransaction())
                    {

                        try
                        {
                            string _err = string.Empty;
                            refID = new QuestionnaireBLL().GetDocID(ref _err);
                            string strsql1 = @"INSERT INTO TQUESTIONNAIRE_TEAM ( NQUESTIONNAIREID, SNAME,  DCREATE, SCREATE,TEAM_INDEX,TEAM_TYPE,QTEAM_ID) VALUES ( :NQUESTIONNAIREID  ,:SNAME , SYSDATE , :SCREATE ,:NINDEX,:TEAM_TYPE,:QTEAM_ID )";

                            int index = 0;
                            QuestionnaireData.LST_TEAM.OrderBy(x => x.TEAM_INDEX);
                            foreach (var itm in QuestionnaireData.LST_TEAM)
                            {

                                int? TeamRef = new QuestionnaireBLL().GetTeamID(ref _err);
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = refID;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = itm.SNAME;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.Parameters.Add(":NINDEX", OracleType.Number).Value = index;
                                    com1.Parameters.Add(":TEAM_TYPE", OracleType.Number).Value = itm.TEAM_TYPE;
                                    com1.Parameters.Add(":QTEAM_ID", OracleType.Number).Value = TeamRef;
                                    com1.ExecuteNonQuery();
                                }
                                index++;
                            }

                            #region " File "

                            var delFile = DTFile.Where(x => x.ID != 0).Select(r => r.ID);

                            foreach (var item in DTFile)
                            {
                                if (item.ID == 0)
                                {
                                    using (OracleCommand com1 = new OracleCommand(@"INSERT INTO F_UPLOAD (FILENAME_SYSTEM, FILENAME_USER, UPLOAD_ID, REF_INT, REF_STR, ISACTIVE, CREATE_BY, FULLPATH) VALUES (:FILENAME_SYSTEM, :FILENAME_USER, :UPLOAD_ID, :REF_INT, :REF_STR, :ISACTIVE, :CREATE_BY, :FULLPATH)", con))
                                    {
                                        com1.Transaction = tran;
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":FILENAME_SYSTEM", OracleType.VarChar).Value = item.FILENAME_SYSTEM;
                                        com1.Parameters.Add(":FILENAME_USER", OracleType.VarChar).Value = item.FILENAME_USER;
                                        com1.Parameters.Add(":UPLOAD_ID", OracleType.VarChar).Value = item.UPLOAD_ID;
                                        com1.Parameters.Add(":REF_INT", OracleType.Number).Value = refID;
                                        com1.Parameters.Add(":REF_STR", OracleType.VarChar).Value = PageFileUpload;
                                        com1.Parameters.Add(":ISACTIVE", OracleType.Number).Value = "1";
                                        com1.Parameters.Add(":CREATE_BY", OracleType.VarChar).Value = Session["UserID"];
                                        com1.Parameters.Add(":FULLPATH", OracleType.VarChar).Value = item.FULLPATH;
                                        com1.ExecuteNonQuery();
                                    }
                                }
                            }


                            #endregion " File "

                            string strsql = @"INSERT INTO TQUESTIONNAIRE ( SVENDORID, DCHECK, SADDRESS,SREMARK,NVALUE,DUPDATE,SUPDATE,NQUESTIONNAIREID,NYEAR,DCREATE,NNO,SCREATE ) VALUES( :SVENDORID , to_date(:DCHECK,'DD/MM/YYYY'), :SADDRESS, :SREMARK,:NVALUE , SYSDATE, :SUPDATE,  :NQUESTIONNAIREID ,:YEAR,SYSDATE,1,:SCREATE)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Transaction = tran;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = ddlVendor.SelectedItem.Value;
                                com.Parameters.Add(":DCHECK", OracleType.VarChar).Value = txtDCHECK.Text;
                                com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                com.Parameters.Add(":NVALUE", OracleType.VarChar).Value = TotalPoint.ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = user_profile.USER_ID;
                                com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = refID;
                                com.Parameters.Add(":YEAR", OracleType.VarChar).Value = ddlYear.SelectedItem.Value;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = user_profile.USER_ID;
                                //com.Parameters.Add(":NTYPEVISITFORMID", OracleType.VarChar).Value = QuestionnaireData.NTYPEVISITFORMID;
                                com.ExecuteNonQuery();
                            }

                            tran.Commit();
                            alertSuccess(" บันทึกข้อมูลเรียบร้อย ", "questionnaire_form.aspx");
                            Session["EditNQUESTIONNAIREID"] = refID.ToString();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                }
                #endregion " Add "
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("questionnaire.aspx");
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int? _keyNGROUPID = int.Parse(DataBinder.Eval(e.Row.DataItem, "NGROUPID").ToString());
            int? _keyNTYPEVISITFORMID = int.Parse(DataBinder.Eval(e.Row.DataItem, "NTYPEVISITFORMID").ToString());
            int? _keyNVISITFORMID = int.Parse(DataBinder.Eval(e.Row.DataItem, "NVISITFORMID").ToString());
            string expression = string.Format(" NGROUPID={0} and NTYPEVISITFORMID={1} and NVISITFORMID={2}", _keyNGROUPID, _keyNTYPEVISITFORMID, _keyNVISITFORMID);
            DataRow[] foundRows = null;
            if (QuestionnaireData.DT_QUESTIONNAIRELIST != null)
            {
                foundRows = QuestionnaireData.DT_QUESTIONNAIRELIST.Select(expression);
            }

            RadioButtonList RdoListPoint = e.Row.Cells[1].FindControl("RdoListPoint") as RadioButtonList;
            if (RdoListPoint != null)
            {
                if (!CanWrite)
                    RdoListPoint.Enabled = false;

                //if (PageAction == PAGE_ACTION.VIEW) RdoListPoint.Enabled = false;
                //if (Page_Status == USER_PERMISSION.VIEW) RdoListPoint.Enabled = false;

                foreach (RadioValueList itm in DTRdo1)
                {
                    string val = itm.NTYPEVISITLISTSCORE.ToString() + "," + itm.CONFIG_VALUE;
                    RdoListPoint.Items.Add(new ListItem("&nbsp;" + itm.STYPEVISITLISTNAME, val));
                }

                if (int.Parse(QDocID) > 0)
                {
                    if (foundRows.Length > 0)
                    {
                        string radioVal = "," + foundRows[0]["NVALUEITEM"].ToString();

                        foreach (ListItem itm in RdoListPoint.Items)
                        {
                            string val_itm = itm.Value;
                            if (val_itm.Contains(radioVal))
                            {
                                itm.Selected = true;
                                break;
                            }
                        }
                        string remark = foundRows[0]["SREMARK"].ToString();
                    }
                }
                else
                {
                    RdoListPoint.ClearSelection();
                }
            }

            Label lblSVISITFORMNAME = e.Row.Cells[1].FindControl("lblSVISITFORMNAME") as Label;
            if (lblSVISITFORMNAME != null)
            {
                string _str = DataBinder.Eval(e.Row.DataItem, "SVISITFORMNAME").ToString().Replace("\n", "<br />");
                lblSVISITFORMNAME.Text = _str;
            }

            TextBox txtRemark = e.Row.Cells[1].FindControl("txtRemark") as TextBox;
            if (txtRemark != null)
            {
                if (!CanWrite)
                    txtRemark.Enabled = false;

                //if (PageAction == PAGE_ACTION.VIEW) txtRemark.Enabled = false;
                //if (Page_Status == USER_PERMISSION.VIEW) txtRemark.Enabled = false;
            }
            if ((txtRemark != null) && (foundRows != null) && (int.Parse(QDocID) > 0))
            {

                if (foundRows.Length > 0)
                {
                    txtRemark.Text = foundRows[0]["SREMARK"].ToString();
                    if (!CanWrite)
                        txtRemark.Enabled = false;

                    //if (Page_Status == USER_PERMISSION.VIEW) txtRemark.Enabled = false;
                }

            }
        }
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadVendor();
    }
    protected void btnAddTeam_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtTQUESTIONNAIRE_TEAM.Text.Trim()))
        {
            alertFail("กรุณาระบุชื่อคณะกรรมการ");
            return;
        }
        if (!string.IsNullOrEmpty(rdoTeamType.SelectedValue))
        {
            if (string.IsNullOrEmpty(QTeamID)) // Add new
            {
                if (QuestionnaireData.LST_TEAM == null) QuestionnaireData.LST_TEAM = new List<TQUESTIONNAIRE_TEAM>();
                QuestionnaireData.LST_TEAM.Add(new TQUESTIONNAIRE_TEAM() { NQUESTIONNAIREID = decimal.Parse(QDocID), TEAM_TYPE = rdoTeamType.SelectedItem.Value, SNAME = txtTQUESTIONNAIRE_TEAM.Text.Trim(), QTEAM_ID = 0, TEAM_INDEX = QuestionnaireData.LST_TEAM.Count + 1, TYPE_NAME = rdoTeamType.SelectedItem.Text });
            }
            else
            {
                QuestionnaireData.LST_TEAM[int.Parse(QTeamID)].SNAME = txtTQUESTIONNAIRE_TEAM.Text.Trim();
                QuestionnaireData.LST_TEAM[int.Parse(QTeamID)].TEAM_TYPE = rdoTeamType.SelectedItem.Value;
                QuestionnaireData.LST_TEAM[int.Parse(QTeamID)].TYPE_NAME = rdoTeamType.SelectedItem.Text.Trim();

            }
            grvTQUESTIONNAIRE_TEAM.DataSource = QuestionnaireData.LST_TEAM;
            grvTQUESTIONNAIRE_TEAM.DataBind();
        }
        else
        {
            alertFail("กรุณาระบุประเภทคณะกรรมการ");
            return;
        }

        txtTQUESTIONNAIRE_TEAM.Text = string.Empty;

    }
    protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdIsChange.Value == "1")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "AlertSave()", true);
        }
        else GroupNameChange();
    }
    public bool SaveTab2()
    {
        bool result = true;
        try
        {
            #region  " Edit "
            double? TotalPoint = 0;
            GridView grv = GridView1;

            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                using (OracleTransaction tran = con.BeginTransaction())
                {
                    try
                    {
                        string _err = string.Empty;

                        if (grv.Rows.Count != 0) EDIT_NTYPEVISITFORMID = grv.DataKeys[0]["NTYPEVISITFORMID"].ToString();
                        //UPDATE FORM
                        string strsql = @"UPDATE TQUESTIONNAIRE SET NTYPEVISITFORMID=:NTYPEVISITFORMID ,DUPDATE= SYSDATE,SUPDATE  =:SUPDATE ,IS_VENDOR_VIEW=:IS_VENDOR_VIEW WHERE  NQUESTIONNAIREID =:NQUESTIONNAIREID ";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            com.Transaction = tran;
                            com.Parameters.Clear();
                            com.Parameters.Add(":NTYPEVISITFORMID", OracleType.VarChar).Value = EDIT_NTYPEVISITFORMID == "0" ? "" : EDIT_NTYPEVISITFORMID;
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = user_profile.USER_ID;
                            com.Parameters.Add(":IS_VENDOR_VIEW", OracleType.VarChar).Value = chkIS_VENDOR_VIEW.Checked ? "1" : "0";
                            com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;

                            com.ExecuteNonQuery();
                        }

                        if (grv.Rows.Count != 0)
                        {
                            int? NGROUPID = int.Parse(grv.DataKeys[0]["NGROUPID"].ToString());
                            int? NTYPEVISITFORMID = int.Parse(grv.DataKeys[0]["NTYPEVISITFORMID"].ToString());
                            using (OracleCommand comDel1 = new OracleCommand("DELETE FROM TQUESTIONNAIRELIST WHERE NQUESTIONNAIREID =:NQUESTIONNAIREID and NGROUPID=:NGROUPID and NTYPEVISITFORMID=:NTYPEVISITFORMID", con))
                            {
                                comDel1.Transaction = tran;
                                comDel1.Parameters.Clear();
                                comDel1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;
                                comDel1.Parameters.Add(":NGROUPID", OracleType.Number).Value = NGROUPID.ToString();
                                comDel1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = NTYPEVISITFORMID.ToString();
                                comDel1.ExecuteNonQuery();
                            }

                            string strsql4 = @"INSERT INTO TQUESTIONNAIRELIST (NQUESTIONNAIREID, NVISITFORMID, NGROUPID,  NTYPEVISITFORMID, NVALUE, SREMARK, SCREATE, DCREATE,NVALUEITEM) VALUES (:NQUESTIONNAIREID ,:NVISITFORMID ,:NGROUPID ,:NTYPEVISITFORMID ,:NVALUE ,:SREMARK ,:SCREATE ,SYSDATE ,:NVALUEITEM)";

                            foreach (GridViewRow row in grv.Rows)
                            {
                                int? NVISITFORMID = int.Parse(grv.DataKeys[row.RowIndex]["NVISITFORMID"].ToString());
                                if (row.RowType != DataControlRowType.DataRow) continue;
                                double? NVALUE = 0.0;
                                double? NVALUEITEM = 0.0;
                                string NVALUEITEM_COL = string.Empty;
                                string Remark = string.Empty;
                                RadioButtonList RdoListPoint = row.Cells[1].FindControl("RdoListPoint") as RadioButtonList;
                                if (RdoListPoint != null)
                                {
                                    NVALUEITEM = double.Parse(RdoListPoint.SelectedValue.Split(',')[0]);
                                    NVALUEITEM_COL = RdoListPoint.SelectedValue.Split(',')[1];
                                }
                                HiddenField hdd = row.Cells[1].FindControl("hdWEIGHT") as HiddenField;
                                if (hdd != null) { NVALUE = double.Parse(hdd.Value); }

                                TextBox txtRemark = row.Cells[2].FindControl("txtRemark") as TextBox;
                                if (txtRemark != null) { Remark = txtRemark.Text; }
                                using (OracleCommand com3 = new OracleCommand(strsql4, con))
                                {
                                    com3.Transaction = tran;
                                    TotalPoint += (NVALUE * NVALUEITEM); // Recalculate ; 
                                    com3.Parameters.Clear();
                                    com3.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;
                                    com3.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = NVISITFORMID.ToString();
                                    com3.Parameters.Add(":NGROUPID", OracleType.Number).Value = NGROUPID.ToString();
                                    com3.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = NTYPEVISITFORMID.ToString();
                                    com3.Parameters.Add(":NVALUE", OracleType.Number).Value = NVALUE;
                                    com3.Parameters.Add(":NVALUEITEM", OracleType.Number).Value = NVALUEITEM_COL;
                                    com3.Parameters.Add(":SREMARK", OracleType.VarChar).Value = Remark;
                                    com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = user_profile.USER_ID;
                                    com3.ExecuteNonQuery();
                                }
                            }
                        }

                        tran.Commit();
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        alertFail(ex.Message);
                    }

                }

            }
            #endregion " Edit "
        }
        catch (Exception ex)
        {
            result = false;
            alertFail(ex.Message);
        }

        return result;
    }
    protected void btnSaveTab2_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(ddlGroupName.SelectedValue))
            {
                alertFail("กรุณาเลือก ชื่อหมวด/กลุ่ม ที่ต้องการบันทึก");
                return;
            }

            bool result = SaveTab2();
            if (result)
            {
                alertSuccess("บันทึกข้อมูลเรียบร้อย");
                hdIsChange.Value = "0";
                string _err = string.Empty;
                QuestionnaireData = new QuestionnaireBLL().GetQuestionnaire(ref _err, int.Parse(QDocID));
                LoadTableScore();
                GetAllScore();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    protected void btnSaveChange_Click(object sender, EventArgs e)
    {

    }

    public void GroupNameChange()
    {
        hdIsChange.Value = "0";
        LoadItem();
        GetAllScore();
        GetScoreNotInCurrentGroup();
    }
    protected void btnNotSave_Click(object sender, EventArgs e)
    {
        GroupNameChange();
    }
    protected void btnSaveBeforeChange_Click(object sender, EventArgs e)
    {
        var result = SaveTab2();
        if (result) GroupNameChange();
    }
    protected void grvScore_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {

            if (dtScoreVendorInForm != null)
            {
                SumItems = (double)dtScoreVendorInForm.AsEnumerable().Sum(row => row.Field<decimal>("ITMGRUP"));
                SumWeight = (double)dtScoreVendorInForm.AsEnumerable().Sum(row => row.Field<decimal>("NWEIGHT"));
                SumScore = (double)dtScoreVendorInForm.AsEnumerable().Sum(row => row.Field<decimal>("TSUM_SCORE"));
            }
            else
            {
                SumItems = 0.00;
                SumWeight = 0.00;
                SumScore = 0.00;
            }

            Label lblITMGRUP = (Label)e.Row.Cells[2].FindControl("lblFITMGRUP");
            if (lblITMGRUP != null) lblITMGRUP.Text = SumItems.ToString();

            Label lblNWEIGHT = (Label)e.Row.Cells[3].FindControl("lblFNWEIGHT");
            if (lblNWEIGHT != null) lblNWEIGHT.Text = SumWeight.ToString("0.00");

            Label lblSUM_SCORE = (Label)e.Row.Cells[5].FindControl("lblFSUM_SCORE");
            if (lblSUM_SCORE != null) lblSUM_SCORE.Text = SumScore.ToString("0.00");

            Label lblFAverageScore = (Label)e.Row.Cells[5].FindControl("lblFAverageScore");
            if (lblFAverageScore != null) lblFAverageScore.Text = SumAverageScore.ToString("0.00");

            Label lblFPercentage = (Label)e.Row.Cells[5].FindControl("lblFPercentage");
            if (lblFPercentage != null)
            {
                if (SumWeight != 0) SumPercentage = (SumScore / SumWeight) * 100;

                lblFPercentage.Text = SumPercentage.ToString("0.00");
            }

        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblAverageScore = (Label)e.Row.Cells[2].FindControl("lblAverageScore");
            Label lblPercentage = (Label)e.Row.Cells[2].FindControl("lblPercentage");

            if (dtScoreVendorInForm != null)
            {
                double AverageScore = 0.00;
                double ITMGRUP = double.Parse(DataBinder.Eval(e.Row.DataItem, "ITMGRUP").ToString());
                double TSUM_SCORE = double.Parse(DataBinder.Eval(e.Row.DataItem, "TSUM_SCORE").ToString());
                double SUM_SCORE = double.Parse(DataBinder.Eval(e.Row.DataItem, "SUM_SCORE").ToString());
                double NWEIGHT = double.Parse(DataBinder.Eval(e.Row.DataItem, "NWEIGHT").ToString());

                if (lblAverageScore != null)
                {

                    if (ITMGRUP != 0)
                    {
                        AverageScore = (SUM_SCORE / ITMGRUP);
                        SumAverageScore += AverageScore;
                    }
                    lblAverageScore.Text = AverageScore.ToString("0.00");
                }


                if (lblPercentage != null)
                {
                    double Percentage = 0.00;
                    if (NWEIGHT != 0)
                    {
                        Percentage = (TSUM_SCORE / NWEIGHT) * 100;
                    }
                    lblPercentage.Text = Percentage.ToString("0.00");
                }
            }
            else
            {
                SumAverageScore = 0.00;
                SumPercentage = 0.00;
                lblPercentage.Text = "0.00";
                lblPercentage.Text = "0.00";
            }
        }
    }

}