﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;
using System.Drawing;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;

public partial class ReportCarWaterExpire : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static int nNextYeatCheckWater = 3;
    private static DataTable dtMainData = new DataTable();
    private static string sTruckID_CheckWater = "";
    private static string sMonth = "";
    private static string TextMonth = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            //ListData();

            SetCboYear();

        }
    }

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "ใบรับรอง")
        {
            ASPxTextBox txtChecking = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtChecking") as ASPxTextBox;
            txtChecking.ClientInstanceName = txtChecking.ID + "_" + e.VisibleIndex;
            ASPxTextBox txtRequestID = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtRequestID") as ASPxTextBox;
            txtRequestID.ClientInstanceName = txtRequestID.ID + "_" + e.VisibleIndex;
        }


    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListData(sMonth);
        }
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": bindChart11();//ListData();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string[] paras = e.Args[0].Split(';');
                string ss = txtSeriesName.Text.Trim();
                switch (paras[0])
                {

                    case "ListData":
                        switch (ss)
                        {
                            case "ม.ค.": ListData("01");
                                sMonth = "01";
                                TextMonth = "ม.ค.";
                                break;
                            case "ก.พ.": ListData("02");
                                sMonth = "02";
                                TextMonth = "ก.พ.";
                                break;
                            case "มี.ค.": ListData("03");
                                sMonth = "03";
                                TextMonth = "มี.ค.";
                                break;
                            case "เม.ย.": ListData("04");
                                sMonth = "04";
                                TextMonth = "เม.ย.";
                                break;
                            case "พ.ค.": ListData("05");
                                sMonth = "05";
                                TextMonth = "พ.ค.";
                                break;
                            case "มิ.ย.": ListData("06");
                                sMonth = "06";
                                TextMonth = "มิ.ย.";
                                break;
                            case "ก.ค.": ListData("07");
                                sMonth = "07";
                                TextMonth = "ก.ค.";
                                break;
                            case "ส.ค.": ListData("08");
                                sMonth = "08";
                                TextMonth = "ส.ค.";
                                break;
                            case "ก.ย.": ListData("09");
                                sMonth = "09";
                                TextMonth = "ก.ย.";
                                break;
                            case "ต.ค.": ListData("10");
                                sMonth = "10";
                                TextMonth = "ต.ค.";
                                break;
                            case "พ.ย.": ListData("11");
                                sMonth = "11";
                                TextMonth = "พ.ย.";
                                break;
                            case "ธ.ค.": ListData("12");
                                sMonth = "12";
                                TextMonth = "ธ.ค.";
                                break;
                        }
                        break;
                }
                break;
        }
    }

    void ListData(string Value)
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(Value))
        {
            Condition += " AND TO_CHAR(REQ.WATER_EXPIRE_DATE,'MM') = '" + CommonFunction.ReplaceInjection(Value) + "'";
        }
        if (!string.IsNullOrEmpty(cboYear.Text))
        {
            Condition += " AND TO_CHAR(REQ.WATER_EXPIRE_DATE,'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }

        string QUERY = @"SELECT ROWNUM||'.' as NO
,TRUNC(TO_DATE(REQ.APPROVE_DATE,'dd/MM/yyyy')) as APPROVE_DATE
,TRUNC(TO_DATE(REQ.WATER_EXPIRE_DATE,'dd/MM/yyyy')) as WATER_EXPIRE_DATE
,TRUNC(TO_DATE(REQ.SERVICE_DATE,'dd/MM/yyyy')) as SERVICE_DATE
,VEN.SABBREVIATION
,CAT.CARCATE_NAME
,RQT.REQTYPE_NAME
,CAS.CAUSE_NAME
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN TBL_CARCATE CAT ON CAT.CARCATE_ID = REQ.CARCATE_ID
WHERE 1=1 " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;

        }
        gvw.DataBind();
    }

    private void ListReport(string Type)
    {
        #region Grap
        string Condition = "";

        if (!string.IsNullOrEmpty(cboYear.Text + ""))
        {
            lblsTail.Text = "รายงานจำนวนรถหมดอายุวัดน้ำ (จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            Condition = "WHERE TO_CHAR(WATER_EXPIRE_DATE,'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }
        else
        {
            lblsTail.Text = "-";
        }



        string QUERY = @"SELECT TRC.RPTID,TRC.RPTNAME,M1.M1,M2.M2,M3.M3,M4.M4,M5.M5,M6.M6,M7.M7,M8.M8,M9.M9,M10.M10,M11.M11,M12.M12 FROM TBL_RPT_CARWATEREXPIRE TRC
LEFT JOIN 
(
    SELECT 1 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '01' THEN 1 ELSE 0 END) AS M1
    FROM TBL_REQUEST
    " + Condition + @"
)M1 ON M1.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 2 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '02' THEN 1 ELSE 0 END) AS M2
    FROM TBL_REQUEST
    " + Condition + @"
)M2 ON M2.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 3 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '03' THEN 1 ELSE 0 END) AS M3
    FROM TBL_REQUEST
    " + Condition + @"
)M3 ON M3.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 4 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '04' THEN 1 ELSE 0 END) AS M4
    FROM TBL_REQUEST
    " + Condition + @"
)M4 ON M4.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 5 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '05' THEN 1 ELSE 0 END) AS M5
    FROM TBL_REQUEST
   " + Condition + @"
)M5 ON M5.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 6 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '06' THEN 1 ELSE 0 END) AS M6
    FROM TBL_REQUEST
    " + Condition + @"
)M6 ON M6.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 7 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '07' THEN 1 ELSE 0 END) AS M7
    FROM TBL_REQUEST
    " + Condition + @"
)M7 ON M7.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 8 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '08' THEN 1 ELSE 0 END) AS M8
    FROM TBL_REQUEST
    " + Condition + @"
)M8 ON M8.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 9 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '09' THEN 1 ELSE 0 END) AS M9
    FROM TBL_REQUEST
   " + Condition + @"
)M9 ON M9.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 10 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '10' THEN 1 ELSE 0 END) AS M10
    FROM TBL_REQUEST
   " + Condition + @"
)M10 ON M10.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 11 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '11' THEN 1 ELSE 0 END) AS M11
    FROM TBL_REQUEST
   " + Condition + @"
)M11 ON M11.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 12 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '12' THEN 1 ELSE 0 END) AS M12
    FROM TBL_REQUEST
   " + Condition + @"
)M12 ON M12.RPTID =  TRC.RPTID";

        DataTable dtGraph = CommonFunction.Get_Data(conn, QUERY);
        #endregion


        #region Data
        string sCondition = "";
        if (!string.IsNullOrEmpty(sMonth))
        {
            sCondition += " AND TO_CHAR(REQ.WATER_EXPIRE_DATE,'MM') = '" + CommonFunction.ReplaceInjection(sMonth) + "'";
        }
        if (!string.IsNullOrEmpty(cboYear.Text))
        {
            sCondition += " AND TO_CHAR(REQ.WATER_EXPIRE_DATE,'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }

        string sQUERY = @"SELECT ROWNUM||'.' as NO
,TRUNC(TO_DATE(REQ.APPROVE_DATE,'dd/MM/yyyy')) as APPROVE_DATE
,TRUNC(TO_DATE(REQ.WATER_EXPIRE_DATE,'dd/MM/yyyy')) as WATER_EXPIRE_DATE
,TRUNC(TO_DATE(REQ.SERVICE_DATE,'dd/MM/yyyy')) as SERVICE_DATE
,VEN.SABBREVIATION
,CAT.CARCATE_NAME
,RQT.REQTYPE_NAME
,CAS.CAUSE_NAME
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN TBL_CARCATE CAT ON CAT.CARCATE_ID = REQ.CARCATE_ID
WHERE 1=1 " + sCondition + "";

        DataTable dt = CommonFunction.Get_Data(conn, sQUERY);
        #endregion
        rpt_CarWaterExpire report = new rpt_CarWaterExpire();

        #region function report

        XRChart xrChart2 = report.FindControl("xrChart1", true) as XRChart;
        xrChart2.Series.Clear();
        //add Title
        ChartTitle chartTitle1 = new ChartTitle();
        chartTitle1.Text = "รายงานจำนวนรถหมอายุวัดน้ำ";
        chartTitle1.Font = new Font("Tahoma", 15);
        xrChart2.Titles.Add(chartTitle1);
        if (!string.IsNullOrEmpty(cboYear.Text))
        {
            ChartTitle chartTitle2 = new ChartTitle();
            chartTitle2.Text = "ปี " + cboYear.Text;
            chartTitle2.Font = new Font("Tahoma", 13);
            xrChart2.Titles.Add(chartTitle2);
        }

        if (dtGraph.Rows.Count > 0)
        {
            Series series = new Series("", ViewType.Bar);


            for (int i = 0; i < dtGraph.Rows.Count; i++)
            {
                string COLNAME = "M" + (i + 1);
                double Value = !string.IsNullOrEmpty(dtGraph.Rows[i][COLNAME] + "") ? double.Parse(dtGraph.Rows[i][COLNAME] + "") : 0;

                series.Points.Add(new SeriesPoint(dtGraph.Rows[i]["RPTNAME"] + "", new double[] { Value }));

            }
            xrChart2.Legend.Visible = false;
            xrChart2.Series.Add(series);
        }


        if (!string.IsNullOrEmpty(TextMonth) && !string.IsNullOrEmpty(cboYear.Text))
        {
            ((XRLabel)report.FindControl("xrLabel2", true)).Text = "รายงานจำนวนรถหมดอายุวัดน้ำ (จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + ")";
            ((XRLabel)report.FindControl("xrLabel3", true)).Text = "รายการรถหมดอายุวัดน้ำเดือน " + TextMonth + " " + cboYear.Text + " จำนวน " + dt.Rows.Count + " รายการ";
        }
        else
        {
            ((XRLabel)report.FindControl("xrLabel2", true)).Text = "";
        }

        ((XRLabel)report.FindControl("xrLabel4", true)).Text = "วันที่จัดทำรายงาน: " + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

        report.Name = "CarWaterExpire";
        report.DataSource = dt;
        string fileName = "รายงานจำนวนรถหมดอายุวัดน้ำ_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();


        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ListReport("P");
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ListReport("E");
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion

    private void bindChart11()
    {
        string Condition = "";

        if (!string.IsNullOrEmpty(cboYear.Text + ""))
        {
            lblsTail.Text = "รายงานจำนวนรถหมดอายุวัดน้ำ (จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            Condition = "WHERE TO_CHAR(WATER_EXPIRE_DATE,'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }
        else
        {
            lblsTail.Text = "-";
        }



        string QUERY = @"SELECT TRC.RPTID,TRC.RPTNAME,M1.M1,M2.M2,M3.M3,M4.M4,M5.M5,M6.M6,M7.M7,M8.M8,M9.M9,M10.M10,M11.M11,M12.M12 FROM TBL_RPT_CARWATEREXPIRE TRC
LEFT JOIN 
(
    SELECT 1 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '01' THEN 1 ELSE 0 END) AS M1
    FROM TBL_REQUEST
    " + Condition + @"
)M1 ON M1.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 2 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '02' THEN 1 ELSE 0 END) AS M2
    FROM TBL_REQUEST
    " + Condition + @"
)M2 ON M2.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 3 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '03' THEN 1 ELSE 0 END) AS M3
    FROM TBL_REQUEST
    " + Condition + @"
)M3 ON M3.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 4 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '04' THEN 1 ELSE 0 END) AS M4
    FROM TBL_REQUEST
    " + Condition + @"
)M4 ON M4.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 5 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '05' THEN 1 ELSE 0 END) AS M5
    FROM TBL_REQUEST
   " + Condition + @"
)M5 ON M5.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 6 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '06' THEN 1 ELSE 0 END) AS M6
    FROM TBL_REQUEST
    " + Condition + @"
)M6 ON M6.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 7 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '07' THEN 1 ELSE 0 END) AS M7
    FROM TBL_REQUEST
    " + Condition + @"
)M7 ON M7.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 8 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '08' THEN 1 ELSE 0 END) AS M8
    FROM TBL_REQUEST
    " + Condition + @"
)M8 ON M8.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 9 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '09' THEN 1 ELSE 0 END) AS M9
    FROM TBL_REQUEST
   " + Condition + @"
)M9 ON M9.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 10 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '10' THEN 1 ELSE 0 END) AS M10
    FROM TBL_REQUEST
   " + Condition + @"
)M10 ON M10.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 11 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '11' THEN 1 ELSE 0 END) AS M11
    FROM TBL_REQUEST
   " + Condition + @"
)M11 ON M11.RPTID =  TRC.RPTID
LEFT JOIN 
(
    SELECT 12 as RPTID ,SUM(CASE WHEN TO_CHAR(WATER_EXPIRE_DATE,'MM') = '12' THEN 1 ELSE 0 END) AS M12
    FROM TBL_REQUEST
   " + Condition + @"
)M12 ON M12.RPTID =  TRC.RPTID";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        // DateTimeFormatInfo mfi = new DateTimeFormatInfo();
        if (dt.Rows.Count > 0)
        {
            Series series = new Series("0 - 5", ViewType.Bar);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string COLNAME = "M" + (i + 1);
                double Value = !string.IsNullOrEmpty(dt.Rows[i][COLNAME] + "") ? double.Parse(dt.Rows[i][COLNAME] + "") : 0;



                series.Points.Add(new SeriesPoint(dt.Rows[i]["RPTNAME"] + "", new double[] { Value }));

            }
            WebChartControl1.Series.Add(series);
            //WebChartControl1.Series.Add(new Series("0 - 5", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("6 - 10", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("11 - 15", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("16 - 30", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("31 - 35", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("46 - 60", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("61 - 75", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("มากกว่า 78", ViewType.Bar));

            WebChartControl1.Legend.Visible = false;
            WebChartControl1.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative;
            WebChartControl1.SeriesTemplate.ValueScaleType = ScaleType.Numerical;
            WebChartControl1.SeriesTemplate.PointOptions.PointView = PointView.Values;
            WebChartControl1.SeriesTemplate.LegendPointOptions.PointView = PointView.Argument;
            // ((DevExpress.XtraCharts.XYDiagram)WebChartControl1.Diagram).AxisX.Label.Angle = 90;
            WebChartControl1.DataSource = dt;

            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q1" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q2" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q3" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q4" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q5" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q6" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q7" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q8" });



            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";


            WebChartControl1.DataBind();



        }


    }
}