﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Kpi_process.aspx.cs" Inherits="Kpi_process" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
    <style type="text/css">
    th
    {
        text-align:center;
    }
    .error
    {
        color: red;
    }
    </style>
    <script type="text/javascript">
        function Showmodal()
        {
            $('#modal_popup').modal('show');
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <br />
    <h5>บันทึกผลประเมิน KPI</h5>
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ผลประเมิน KPI
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            ปี :
                        </div>
                        <div class="col-md-1">
                             <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            ผู้ขนส่ง :
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddl_vendor" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddl_vendor_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            เลขที่สัญญา :
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddl_contract" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            &nbsp;
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-2 text-right">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            <asp:Button runat="server" ID="cmdSearch" Width="150px" CssClass="btn btn-primary" Text="ค้นหา" OnClick="cmdSearch_Click" />
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <%-- หน้าจอแสดงผู้ขนส่ง --%>
        <asp:GridView ID="gvd_kpi" runat="server" Width="80%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="Both" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" DataKeyNames="SCONTRACTID"
                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowCommand="gvd_kpi_RowCommand">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
            <Columns>
                        <asp:BoundField DataField="SABBREVIATION" HeaderText="ผู้ขนส่ง">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField> 
                        <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" >
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>                        
                        <asp:ButtonField CommandName="Detail" ControlStyle-CssClass="btn btn-info" ButtonType="Button" Text="ประเมิน KPI" HeaderText="ประเมิน KPI" ItemStyle-HorizontalAlign="Center" />
            </Columns>
        </asp:GridView>
    </div>
</div>


    <div class="modal fade" style="z-index:1060" id='modal_popup' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" >
        <div class="modal-dialog">
            <div class="modal-content" style="text-align:left;">
                <div class="modal-header">
                    <h4 class="modal-title">ประเมินผลผู้ขนส่ง</h4>
                </div>
                <div class="modal-body" style="word-wrap:break-word;">
                   <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-2 text-right">
                                    ปี :
                                </div>
                                <div class="col-md-3" style="font-size: 20px;font-weight: bold;">
                                     <%= ddlYear.Text %>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-2 text-right">
                                    เดือน :
                                </div>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:HiddenField runat="server" ID="KeyRow" />
                    <asp:Button runat="server" ID="cmdManual" Width="150px" CssClass="btn btn-primary" Text="Manual" OnClick="cmdManual_Click" />
                    <asp:Button runat="server" ID="cmdImport" Width="150px" CssClass="btn btn-primary" Text="Import" OnClick="cmdImport_Click" />                   
                    <button id="btnClose" runat="server" type="button" class="btn btn-md bth-hover btn-danger" data-dismiss="modal" >ยกเลิก</button>     
                   
                </div>
            </div>
        </div>
    </div>
</asp:Content>
