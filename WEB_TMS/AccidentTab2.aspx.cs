﻿using EmailHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;

public partial class AccidentTab2 : PageBase
{
    DataTable dt;
    DataSet ds;
    DataRow dr;
    string MessSAPSuccess = "ข้อมูลพนักงานใน SAP สำเร็จ<br/ >";
    string MessSAPFail = "<span style=\"color:Red;\">{0}ข้อมูลพนักงานใน SAP ไม่สำเร็จ !!!{1}</span><br/ >";
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            CreateAccParty();
            CreateAccLeakProduct();
            hidCGROUP.Value = Session["CGROUP"] + string.Empty;
            SetDrowDownList();
            InitialRequireField();
            InitialRequireFile();
            InitialUpload();
            if (Request.QueryString["str"] != null)
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SetData(strQuery);
                liTab1.Visible = true;
                GeneralTab1.HRef = "AccidentTab1.aspx?str=" + str;
                if (int.Parse(hidCactive.Value) >= 4)
                {
                    liTab3.Visible = true;
                    GeneralTab3.HRef = "AccidentTab3.aspx?str=" + str;
                    if (hidCGROUP.Value == "0")
                    {
                        GeneralTab3.HRef = "AccidentTab3_Vendor.aspx?str=" + str;
                    }
                    if (int.Parse(hidCactive.Value) >= 8)
                    {
                        liTab4.Visible = true;
                        GeneralTab4.HRef = "AccidentTab4.aspx?str=" + str;
                    }
                }
            }
            this.AssignAuthen();
        }
        SetEnabledControlByID(hidCactive.Value);
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnAddParties.Enabled = false;
                btnAddProduct.Enabled = false;
                btnUpload.Enabled = false;
                btnApprove.Disabled = true;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region SetData
    private void SetData(string ACCIDENT_ID)
    {
        ds = AccidentBLL.Instance.AccidentTab2Select(ACCIDENT_ID);
        if (ds.Tables.Count > 0)
        {
            #region จาก Tab1
            dt = ds.Tables["ACCIDENT"];
            if (dt != null && dt.Rows.Count > 0)
            {
                gvDO.DataSource = dt;
                gvDO.DataBind();
                DataRow dr = dt.Rows[0];
                txtAccidentID.Text = dr["ACCIDENT_ID"] + string.Empty;
                if (!dr.IsNull("ACCIDENT_DATE"))
                {
                    txtAccidentDate.Text = DateTime.Parse(dr["ACCIDENT_DATE"] + string.Empty).ToString(DateTimeFormat);
                }
                if (!dr.IsNull("SYS_TIME"))
                    txtAccidentDateSystem.Text = DateTime.Parse(dr["SYS_TIME"] + string.Empty).ToString(DateTimeFormat);
                rblREPORTER.SelectedValue = dr["REPORTER"] + string.Empty;
                if (!dr.IsNull("REPORT_PTT"))
                    txtAccidentDatePtt.Text = DateTime.Parse(dr["REPORT_PTT"] + string.Empty).ToString(DateTimeFormat);
                txtAccidentName.Text = dr["INFORMER_NAME"] + string.Empty;
                ddlAccidentType.SelectedValue = dr["ACCIDENTTYPE_ID"] + string.Empty;
                txtShipmentNo.Text = dr["SHIPMENT_NO"] + string.Empty;
                txtTruck.Text = dr["SHEADREGISTERNO"] + string.Empty;
                hidSTRUCKID.Value = dr["STRUCKID"] + string.Empty;
                ddlVendor.SelectedValue = dr["SVENDORID"] + string.Empty;
                ddlVendor_SelectedIndexChanged(null, null);
                ddlContract.SelectedValue = dr["SCONTRACTID"] + string.Empty;
                ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                ddlWorkGroup_SelectedIndexChanged(null, null);
                ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                txtEMPNAME.Text = dr["EMPNAME"] + string.Empty;
                txtPERS_CODE.Text = dr["PERS_CODE"] + string.Empty;
                hidSEMPLOYEEID.Value = dr["SEMPLOYEEID"] + string.Empty;
                txtAGE.Text = dr["AGE"] + string.Empty;
                txtEMPNAME2.Text = dr["EMPNAME2"] + string.Empty;
                txtPERS_CODE2.Text = dr["PERS_CODE2"] + string.Empty;
                hidSEMPLOYEEID2.Value = dr["SEMPLOYEEID2"] + string.Empty;
                txtAGE2.Text = dr["AGE2"] + string.Empty;
                txtEMPNAME3.Text = dr["EMPNAME3"] + string.Empty;
                txtPERS_CODE3.Text = dr["PERS_CODE3"] + string.Empty;
                hidSEMPLOYEEID3.Value = dr["SEMPLOYEEID3"] + string.Empty;
                txtAGE3.Text = dr["AGE3"] + string.Empty;
                txtEMPNAME4.Text = dr["EMPNAME4"] + string.Empty;
                txtPERS_CODE4.Text = dr["PERS_CODE4"] + string.Empty;
                hidSEMPLOYEEID4.Value = dr["SEMPLOYEEID4"] + string.Empty;
                txtAGE4.Text = dr["AGE4"] + string.Empty;
                txtSABBREVIATION.Text = dr["SOURCE"] + string.Empty;
                //hidSTERMINALID.Value = dr["STERMINALID"] + string.Empty;
                txtLocation.Text = dr["LOCATIONS"] + string.Empty;
                txtGPSL.Text = dr["GPSL"] + string.Empty;
                txtGPSR.Text = dr["GPSR"] + string.Empty;
                hidID.Value = txtAccidentID.Text;
                hidCactive.Value = dr["CACTIVE"] + string.Empty;
                hidDRIVER_NO.Value = dr["DRIVER_NO"] + string.Empty;
                if (dr["CACTIVE"] + string.Empty != "1")
                {
                    plTab1.Enabled = false;
                }
                else
                {
                    plTab1.Enabled = true;
                }

            }
            #endregion

            #region TIME
            dt = ds.Tables["TIME"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
                txtTimeCreate.Text = DateTime.Parse(dr["CREATE_DATE"] + string.Empty).ToString("dd/MM/yyyy hh:mm");
                rblEvent.SelectedValue = dr["EVENT"] + string.Empty;
                rblEvent_SelectedIndexChanged(null, null);
                txtRemark.Text = dr["REMARK"] + string.Empty;
                rblParty.SelectedValue = dr["PARTY"] + string.Empty;
                rblParty_SelectedIndexChanged(null, null);
                rblEstimate.SelectedValue = dr["ESTIMATE"] + string.Empty;

                txtDetail.Text = dr["DETAIL"] + string.Empty;
                //txtSpeed.Text = dr["SPEED"] + string.Empty;
                txtSpeed_TMS.Text = dr["SPEED_TMS"] + string.Empty;
                txtAlcohol.Text = dr["ALCOHOL"] + string.Empty;
            }
            #endregion

            #region DAMAGE
            dt = ds.Tables["DAMAGE"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
                rblLeak.SelectedValue = dr["LEAK"] + string.Empty;
                rblLeak_SelectedIndexChanged(null, null);
                rblFire_Truck.SelectedValue = dr["FIRE_TRUCK"] + string.Empty;
                rblFire_Commu.SelectedValue = dr["FIRE_COMMU"] + string.Empty;
                rblContaminated.SelectedValue = dr["CONTAMINATED"] + string.Empty;

                txtContaminated_Detail.Text = dr["CONTAMINATED_DETAIL"] + string.Empty;
                rblContaminated_SelectedIndexChanged(null, null);
                txtDamage_Vendor.Text = dr["DAMAGE_VENDOR"] + string.Empty;
                txtDamage_Party.Text = dr["DAMAGE_PARTY"] + string.Empty;
                txtDamage_Other.Text = dr["DAMAGE_OTHER"] + string.Empty;
                rblDamage_Truck.SelectedValue = dr["DAMAGE_TRUCK"] + string.Empty;
                rblDamage_Truck_SelectedIndexChanged(null, null);
                cbDamage_Traffic1.Checked = (dr["DAMAGE_TRAFFIC"] + string.Empty) == "1";
                cbDamage_Traffic2.Checked = (dr["DAMAGE_TRAFFIC"] + string.Empty) == "2";
                cbDamage_Traffic3.Checked = (dr["DAMAGE_TRAFFIC"] + string.Empty) == "3";
                txtDamage_ETC.Text = dr["DAMAGE_ETC"] + string.Empty;
            }
            #endregion

            #region EFFICE_CORP
            dt = ds.Tables["EFFICE_CORP"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
                rblMedia.SelectedValue = dr["MEDIA"] + string.Empty;
                txtAction_Media.Text = dr["ACTION_MEDIA"] + string.Empty;
                rblPeople.SelectedValue = dr["PEOPLE"] + string.Empty;
                txtAction_People.Text = dr["ACTION_PEOPLE"] + string.Empty;
            }
            #endregion

            #region PARTY
            dt = ds.Tables["PARTY"];
            if (dt != null && dt.Rows.Count > 0)
            {
                gvParties.DataSource = dt;
                gvParties.DataBind();
            }
            #endregion

            #region TRANSPORT
            dt = ds.Tables["TRANSPORT"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
                rblC_SENT_HEAD.SelectedValue = dr["CHANGE_HEAD"] + string.Empty;
                rblC_SENT_HEAD_SelectedIndexChanged(null, null);
                txtTransfer.Text = dr["TRANSFER"] + string.Empty;
                txtLicense.Text = dr["LICENSE"] + string.Empty;
                rbC_Sent_Destination.SelectedValue = dr["T_SENT_DESTINATION"] + string.Empty;
                txtC_Sent_Destination.Text = dr["CHANGE_HEAD_DETAIL"] + string.Empty;
                //rbChange_Head0.Checked = dr["CHANGE_HEAD"] + string.Empty == "0" ? true : false;
                //rbChange_Head1.Checked = !rbChange_Head0.Checked;
                //rbC_Sent_Destination0.Checked = dr["C_SENT_DESTINATION"] + string.Empty == "0" ? true : false;
                //rbC_Sent_Destination1.Checked = !rbC_Sent_Destination0.Checked;
                //txtC_Sent_Head.Text = dr["C_SENT_HEAD"] + string.Empty;
                //TRANSFER
                //rbT_Sent_Destination0.Checked = dr["T_SENT_DESTINATION"] + string.Empty == "0" ? true : false;
                //rbT_Sent_Destination1.Checked = !rbT_Sent_Destination0.Checked;
                //txtT_Sent_Head.Text = dr["T_SENT_HEAD"] + string.Empty;

            }
            #endregion

            #region IMAGE
            dt = ds.Tables["IMAGE"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dtUpload = dt;
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            #endregion

            #region INJURY
            dt = ds.Tables["INJURY"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
                txtVENDOR_DEAD.Text = dr["VENDOR_DEAD"] + string.Empty;
                txtVENDOR_INJURY.Text = dr["VENDOR_INJURY"] + string.Empty;
                txtVENDOR_STOP.Text = dr["VENDOR_STOP"] + string.Empty;
                txtVENDOR_DETAIL.Text = dr["VENDOR_DETAIL"] + string.Empty;
                txtPARTY_DEAD.Text = dr["PARTY_DEAD"] + string.Empty;
                txtPARTY_INJURY.Text = dr["PARTY_INJURY"] + string.Empty;
                txtPARTY_DETAIL.Text = dr["PARTY_DETAIL"] + string.Empty;
                txtOTHER_DEAD.Text = dr["OTHER_DEAD"] + string.Empty;
                txtOTHER_INJURY.Text = dr["OTHER_INJURY"] + string.Empty;
                txtOTHER_DETAIL.Text = dr["OTHER_DETAIL"] + string.Empty;

            }
            #endregion

            #region LEAK_PRODUCT
            dt = ds.Tables["LEAK_PRODUCT"];
            if (dt != null && dt.Rows.Count > 0)
            {
                gvProduct.DataSource = dt;
                gvProduct.DataBind();
            }
            #endregion

            if (rblEstimate.SelectedIndex < 0)
            {
                lblType.Text = "";
            }
            else if (Serious())
            {
                lblType.Text = "เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                lblType.ForeColor = Color.Red;
            }
            else
            {
                lblType.Text = "ไม่เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                lblType.ForeColor = Color.Green;
            }
        }

    }
    #endregion

    #region DrowDownList
    private void SetDrowDownList()
    {
        ViewState["DataAccidentType"] = AccidentBLL.Instance.AccidentTypeSelect(string.Empty, "1");
        ddlAccidentType.DataTextField = "NAME";
        ddlAccidentType.DataValueField = "ID";
        ddlAccidentType.DataSource = ViewState["DataAccidentType"];
        ddlAccidentType.DataBind();
        ddlAccidentType.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.DataTextField = "NAME";
        ddlWorkGroup.DataValueField = "ID";
        ddlWorkGroup.DataSource = ContractTiedBLL.Instance.WorkGroupSelect();
        ddlWorkGroup.DataBind();
        ddlWorkGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.SelectedIndex = 0;
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlGroup.SelectedIndex = 0;
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;

        #region Event
        ViewState["DataAccEvent"] = AccidentBLL.Instance.AccidentEventSelect();
        rblEvent.DataTextField = "NAME";
        rblEvent.DataValueField = "ID";
        rblEvent.DataSource = ViewState["DataAccEvent"];
        rblEvent.DataBind();
        #endregion
    }
    #endregion

    #region ddl
    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlContract_SelectedIndexChanged
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
            {
                dt = AccidentBLL.Instance.GroupSelect(ddlContract.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                    ddlWorkGroup_SelectedIndexChanged(null, null);
                    ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlWorkGroup_SelectedIndexChanged

    #endregion

    #region ddlWorkGroup_SelectedIndexChanged
    protected void ddlWorkGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroupsDataBind(ddlWorkGroup.SelectedValue + string.Empty);
    }
    #endregion

    #region ddlGroupsDataBind
    private void ddlGroupsDataBind(string WordGroupID)
    {
        ddlGroup.DataTextField = "NAME";
        ddlGroup.DataValueField = "ID";
        ddlGroup.DataSource = ContractTiedBLL.Instance.GroupSelect(WordGroupID);
        ddlGroup.DataBind();
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });
    }
    #endregion

    protected void rblEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblEvent.SelectedIndex == rblEvent.Items.Count - 1)
        {
            txtRemark.ReadOnly = false;
            txtRemark.Text = string.Empty;
        }
        else
        {
            txtRemark.ReadOnly = true;
        }
    }
    protected void rblParty_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblParty.SelectedValue == "0")
        {
            plParties.Visible = false;
            gvParties.DataSource = null;
            gvParties.DataBind();
            CreateAccParty();
        }
        else
        {
            plParties.Visible = true;
        }
    }
    protected void rblLeak_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblLeak.SelectedValue == "1")
        {
            plProduct.Visible = false;
            gvProduct.DataSource = null;
            gvProduct.DataBind();
            CreateAccLeakProduct();
        }
        else
        {
            plProduct.Visible = true;
        }
    }
    protected void rblContaminated_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblContaminated.SelectedValue == "0")
        {
            txtContaminated_Detail.ReadOnly = false;
            txtContaminated_Detail.Text = string.Empty;
        }
        else
        {
            txtContaminated_Detail.ReadOnly = true;
        }
    }
    protected void rblDamage_Truck_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblDamage_Truck.SelectedValue == "0")
        {
            plDamage_Truck.Visible = false;
            rblC_SENT_HEAD.ClearSelection();
            txtTransfer.Text = string.Empty;
            txtLicense.Text = string.Empty;
            rbC_Sent_Destination.ClearSelection();
            txtC_Sent_Destination.Text = string.Empty;
        }
        else
        {
            plDamage_Truck.Visible = true;
        }
    }
    protected void rblC_SENT_HEAD_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblTransfer.Text = "ทะเบียนหัว";
        if (rblC_SENT_HEAD.SelectedValue == "0")
        {
            txtLicense.Visible = false;
            lblLicense.Visible = false;
        }
        else if (rblC_SENT_HEAD.SelectedValue == "1")
        {
            txtLicense.ReadOnly = false;
            lblLicense.Visible = true;
            txtLicense.Visible = true;
        }
        else
        {
            lblTransfer.Text = "ระบุ";
            txtLicense.Visible = false;
            lblLicense.Visible = false;
        }
    }
    #endregion

    #region btn
    #region mpSave_ClickOK
    protected void mpSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string accID = SaveData(int.Parse(string.IsNullOrEmpty(hidCactive.Value) ? "0" : hidCactive.Value), false);
            if (!string.IsNullOrEmpty(accID))
            {
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess("บันทึกสำเร็จ", "AccidentTab2.aspx?str=" + encryptedValue);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region mpSaveSend_ClickOK
    protected void mpSaveSend_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string mess = Validate();
            mess += ValidateSaveUpLoadFile();
            if (!string.IsNullOrEmpty(mess))
            {
                alertFail(RemoveSpecialCharacters(mess));
            }
            else
            {
                int emiltemp = ConfigValue.EmailAccident2;
                if (Serious())
                {
                    emiltemp = ConfigValue.EmailAccident3;
                }
                string accID = SaveData(4, emiltemp == ConfigValue.EmailAccident3);
                mess += "บันทึกสำเร็จและส่งข้อมูลสำเร็จ";
                if (!string.IsNullOrEmpty(accID))
                {

                    if (SendEmail(emiltemp, accID))
                    {
                        mess += "<br/>ส่ง E-mail สำเร็จ";
                    }
                    else
                    {
                        mess += "<br/><span style=\"color:Red;\">ส่ง E-mail ไม่สำเร็จ</span>";
                    }
                    //send email
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    alertSuccess(mess, "AccidentTab2.aspx?str=" + encryptedValue);
                }
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #region mpBack_ClickOK
    protected void mpBack_ClickOK(object sender, EventArgs e)
    {

    }
    #endregion

    #endregion

    #region SaveData
    private string SaveData(int CACTIVE, bool Serious)
    {
        DataSet dsTIME = new DataSet("DS"), dsPARTY = new DataSet("DS"), dsINJURY = new DataSet("DS"), dsDAMAGE = new DataSet("DS"), dsLEAK_PRODUCT = new DataSet("DS"), dsTRANSPORT = new DataSet("DS"), dsEFFICE_CORP = new DataSet("DS"), dsFile = new DataSet("DS");
        dt = new DataTable("DT");
        dt.Columns.Add("EVENT", typeof(string));
        dt.Columns.Add("REMARK", typeof(string));
        dt.Columns.Add("PARTY", typeof(string));
        dt.Columns.Add("ESTIMATE", typeof(string));
        dt.Columns.Add("DETAIL", typeof(string));
        dt.Columns.Add("SPEED_TMS", typeof(string));
        dt.Columns.Add("ALCOHOL", typeof(string));
        dt.Rows.Add(rblEvent.SelectedValue, txtRemark.Text.Trim(), rblParty.SelectedValue, rblEstimate.SelectedValue, txtDetail.Text.Trim(), txtSpeed_TMS.Text.Trim(), txtAlcohol.Text.Trim());
        dsTIME.Tables.Add(dt.Copy());

        CreateDataAccParty();
        dsPARTY.Tables.Add(this.dt.Copy());
        CreateDataAccLeakProduct();
        dsLEAK_PRODUCT.Tables.Add(this.dt.Copy());

        dt = new DataTable("DT");
        dt.Columns.Add("VENDOR_DEAD", typeof(string));
        dt.Columns.Add("VENDOR_INJURY", typeof(string));
        dt.Columns.Add("VENDOR_STOP", typeof(string));
        dt.Columns.Add("VENDOR_DETAIL", typeof(string));
        dt.Columns.Add("PARTY_DEAD", typeof(string));
        dt.Columns.Add("PARTY_INJURY", typeof(string));
        dt.Columns.Add("PARTY_DETAIL", typeof(string));
        dt.Columns.Add("OTHER_DEAD", typeof(string));
        dt.Columns.Add("OTHER_INJURY", typeof(string));
        dt.Columns.Add("OTHER_DETAIL", typeof(string));
        dt.Rows.Add(txtVENDOR_DEAD.Text.Trim(), txtVENDOR_INJURY.Text.Trim(), txtVENDOR_STOP.Text.Trim(), txtVENDOR_DETAIL.Text.Trim(), txtPARTY_DEAD.Text.Trim(), txtPARTY_INJURY.Text.Trim(), txtPARTY_DETAIL.Text.Trim(), txtOTHER_DEAD.Text.Trim(), txtOTHER_INJURY.Text.Trim(), txtOTHER_DETAIL.Text.Trim());
        dsINJURY.Tables.Add(dt.Copy());

        dt = new DataTable("DT");
        dt.Columns.Add("LEAK", typeof(string));
        dt.Columns.Add("FIRE_TRUCK", typeof(string));
        dt.Columns.Add("FIRE_COMMU", typeof(string));
        dt.Columns.Add("CONTAMINATED", typeof(string));
        dt.Columns.Add("CONTAMINATED_DETAIL", typeof(string));
        dt.Columns.Add("DAMAGE_VENDOR", typeof(string));
        dt.Columns.Add("DAMAGE_PARTY", typeof(string));
        dt.Columns.Add("DAMAGE_OTHER", typeof(string));
        dt.Columns.Add("DAMAGE_TRUCK", typeof(string));
        dt.Columns.Add("DAMAGE_TRAFFIC", typeof(string));
        dt.Columns.Add("DAMAGE_ETC", typeof(string));
        string DAMAGE_TRAFFIC = string.Empty;
        if (cbDamage_Traffic1.Checked)
        {
            DAMAGE_TRAFFIC = "1";
        }
        if (cbDamage_Traffic2.Checked)
        {
            DAMAGE_TRAFFIC = "2";
        }
        if (cbDamage_Traffic3.Checked)
        {
            DAMAGE_TRAFFIC = "3";
        }
        dt.Rows.Add(rblLeak.SelectedValue, rblFire_Truck.SelectedValue, rblFire_Commu.SelectedValue, rblContaminated.SelectedValue, txtContaminated_Detail.Text.Trim(), txtDamage_Vendor.Text.Trim(), txtDamage_Party.Text.Trim(), txtDamage_Other.Text.Trim(), rblDamage_Truck.SelectedValue, DAMAGE_TRAFFIC, txtDamage_ETC.Text.Trim());
        dsDAMAGE.Tables.Add(dt.Copy());

        dt = new DataTable("DT");
        dt.Columns.Add("CHANGE_HEAD", typeof(string));
        dt.Columns.Add("TRANSFER", typeof(string));
        dt.Columns.Add("LICENSE", typeof(string));
        dt.Columns.Add("T_SENT_DESTINATION", typeof(string));
        dt.Columns.Add("CHANGE_HEAD_DETAIL", typeof(string));

        dt.Rows.Add(rblC_SENT_HEAD.SelectedValue, txtTransfer.Text.Trim(), txtLicense.Text.Trim(), rbC_Sent_Destination.SelectedValue, txtC_Sent_Destination.Text.Trim());
        dsTRANSPORT.Tables.Add(dt.Copy());

        dt = new DataTable("DT");
        dt.Columns.Add("MEDIA", typeof(string));
        dt.Columns.Add("ACTION_MEDIA", typeof(string));
        dt.Columns.Add("PEOPLE", typeof(string));
        dt.Columns.Add("ACTION_PEOPLE", typeof(string));

        dt.Rows.Add(rblMedia.SelectedValue, txtAction_Media.Text.Trim(), rblPeople.SelectedValue, txtAction_People.Text.Trim());
        dsEFFICE_CORP.Tables.Add(dt.Copy());
        string SEMPLOYEEID = hidSEMPLOYEEID.Value;
        switch (hidDRIVER_NO.Value)
        {
            case "2":
                SEMPLOYEEID = hidSEMPLOYEEID2.Value;
                break;
            case "3":
                SEMPLOYEEID = hidSEMPLOYEEID3.Value;
                break;
            case "4":
                SEMPLOYEEID = hidSEMPLOYEEID4.Value;
                break;
            default:
                break;
        }
        dtUpload.TableName = "DT";
        dsFile.Tables.Add(dtUpload.Copy());
        dt = AccidentBLL.Instance.SaveTab2(hidID.Value, CACTIVE, Session["UserID"] + string.Empty, dsTIME, dsPARTY, dsINJURY, dsDAMAGE, dsLEAK_PRODUCT, dsTRANSPORT, dsEFFICE_CORP, dsFile, Serious, SEMPLOYEEID);

        #region SAP
        if (Serious && dt.Rows.Count > 0)
        {

            try
            {
                string Detail = string.Empty;
                Detail += "รถขนส่งเกิดอุบัติเหตุ หมายเลขเอกสาร " + txtAccidentID.Text.Trim();
                Detail += "<br/>สถานะการทำงาน : ระงับการทำงาน";
                Detail += "<br/>หมายเหตุ : อยู่ระหว่างการตรวจสอบรถขนส่งเกิดอุบัติเหตุ(Tab2)";
                VendorBLL.Instance.LogInsert(Detail, txtPERS_CODE.Text.Trim(), null, "0", "0", int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, 0);
                //if (string.Equals(txtDisableFinal.Text.Trim(), "Hold"))
                //{

                //}
                //int TotalLock = (string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? 0 :(string.Equals(txtDisableFinal.Text.Trim(), "Hold") ? -2 : int.Parse(txtDisableFinal.Text.Trim()));
                //ComplainBLL.Instance.ComplainLockDriverBLL(hidID.Value, TotalLock, int.Parse(Session["UserID"].ToString()));
                //DataTable dtDriver = ComplainBLL.Instance.DriverUpdateSelect2BLL(hidID.Value);
                DataTable dtDriverDetail;

                if (!string.Equals(SEMPLOYEEID, string.Empty))
                {
                    dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(SEMPLOYEEID);

                    // ComplainBLL.Instance.UpdateDriverStatusBLL(hidPERS_CODE.Value, "2", "2");

                    SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                    {
                        DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                        PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                        LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                        LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                        LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                        Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                        Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                        FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                        LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                        CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                        DRV_STATUS = ConfigValue.DriverDisable
                    };

                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    if (string.Equals(resultCheck, "N"))
                        throw new Exception("ไม่สามารถ ระงับ พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                }

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
        #endregion
        return dt.Rows.Count > 0 ? dt.Rows[0][0] + string.Empty : "";
    }
    #endregion

    #region btnAddParties_Click
    protected void btnAddParties_Click(object sender, EventArgs e)
    {
        CreateAccParty();
    }
    #endregion

    #region btnAddProduct_Click
    protected void btnAddProduct_Click(object sender, EventArgs e)
    {
        CreateAccLeakProduct();
    }
    #endregion

    #region SetEnabledControlByID
    private void SetEnabledControlByID(string ID)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "SetEnabledControlByID('" + ID + "');", true);
    }
    #endregion

    #region ACC_PARTY
    #region CreateAccParty
    private void CreateAccParty()
    {
        CreateDataAccParty();
        dt.Rows.Add("", "");
        gvParties.DataSource = dt;
        gvParties.DataBind();
    }
    #endregion

    #region CreateDataAccParty
    private void CreateDataAccParty()
    {
        dt = new DataTable("DT");
        dt.Columns.Add("DETAIL", typeof(string));
        dt.Columns.Add("STRAILERREGISTERNO", typeof(string));

        if (gvParties.Rows.Count > 0)
        {
            for (int i = 0; i < gvParties.Rows.Count; i++)
            {
                GridViewRow row = gvParties.Rows[i];
                TextBox txtDetailParties = (TextBox)row.FindControl("txtDetailParties");
                TextBox txtSTRAILERREGISTERNO = (TextBox)row.FindControl("txtSTRAILERREGISTERNO");
                dt.Rows.Add(txtDetailParties.Text.Trim(), txtSTRAILERREGISTERNO.Text.Trim());
            }
        }
    }
    #endregion

    #region gvParties_RowUpdating
    protected void gvParties_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        CreateDataAccParty();
        dt.Rows[e.RowIndex].Delete();
        dt.AcceptChanges();
        gvParties.DataSource = dt;
        gvParties.DataBind();
    }
    #endregion
    #endregion

    #region ACC_LEAK_PRODUCT
    #region CreateAccParty
    private void CreateAccLeakProduct()
    {
        CreateDataAccLeakProduct();
        dt.Rows.Add("", "");
        gvProduct.DataSource = dt;
        gvProduct.DataBind();
    }
    #endregion

    #region CreateDataAccLeakProduct
    private void CreateDataAccLeakProduct()
    {
        dt = new DataTable("DT");
        dt.Columns.Add("TYPE", typeof(string));
        dt.Columns.Add("AMOUNT", typeof(string));

        if (gvProduct.Rows.Count > 0)
        {
            for (int i = 0; i < gvProduct.Rows.Count; i++)
            {
                GridViewRow row = gvProduct.Rows[i];
                TextBox txtType = (TextBox)row.FindControl("txtType");
                TextBox txtAmount = (TextBox)row.FindControl("txtAmount");
                dt.Rows.Add(txtType.Text.Trim(), txtAmount.Text.Trim());
            }
        }
    }
    #endregion

    #region gvProduct_RowUpdating
    protected void gvProduct_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        CreateDataAccLeakProduct();
        dt.Rows[e.RowIndex].Delete();
        dt.AcceptChanges();
        gvProduct.DataSource = dt;
        gvProduct.DataBind();
    }
    #endregion
    #endregion

    #region Require
    private void ClearRequire()
    {

    }

    private void InitialRequireField()
    {
        try
        {
            this.ClearRequire();

            DataTable dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(0, "ACCIDENTTAB2");

            object tmp;
            Label tmpReq;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                tmp = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                if (tmp != null)
                {
                    tmpReq = (Label)this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID_REQ"].ToString());

                    if (tmpReq != null)
                    {
                        if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                        {//Require Field
                            tmpReq.Visible = true;
                            if (hidCactive.Value == "2")
                            {
                                Type = tmp.GetType().ToString();
                                if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                                {
                                    DropDownList ddl = (DropDownList)tmp;
                                    ddl.Enabled = false;
                                }
                                else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                                {
                                    TextBox txt = (TextBox)tmp;
                                    txt.Enabled = false;
                                    switch (txt.ID)
                                    {
                                        //case "txtShipmentNo":
                                        //    btnShipmentNoSearch.Enabled = false;
                                        //    btnShipmentNoSearchClear.Enabled = false;
                                        //    break;
                                        //case "txtTruck":
                                        //    btnTruck.Enabled = false;
                                        //    btnTruckClear.Enabled = false;
                                        //    break;
                                        //case "txtPERS_CODE":
                                        //    btnPERS_CODE.Enabled = false;
                                        //    btnPERS_CODEClear.Enabled = false;
                                        //    break;
                                        //case "txtSABBREVIATION":
                                        //    btnTERMINAL.Enabled = false;
                                        //    btnTERMINALClear.Enabled = false;
                                        //    break;
                                        //case "txtGPSL":
                                        //    txtGPSR.Enabled = false;
                                        //    break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "2"))
                        {//Optional Field
                            tmpReq.Visible = false;
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "3"))
                        {//Disable Field
                            Type = tmp.GetType().ToString();
                            if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                            {
                                DropDownList ddl = (DropDownList)tmp;
                                ddl.SelectedIndex = 0;
                                ddl.Enabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                            {
                                TextBox txt = (TextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Enabled = false;
                            }
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    #endregion

    #region Upload
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, null, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "ACCIDENTTAB2" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[0]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void DownloadFile(string Path, string FileName)
    {
        try
        {
            //if (File.Exists(Path + "\\" + FileName))
            //{
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(Path + "\\" + FileName);
            Response.End();
            //}
            //else
            //{
            //    alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
            //}

        }
        catch (Exception ex)
        {
            alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
        }

    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("ACCIDENTTAB2");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);
        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");
        dtUpload.Columns.Add("REF_INT");
        dtUpload.Columns.Add("REF_STR");
        dtUpload.Columns.Add("STATUS");
        dtUpload.Columns.Add("REMARK");

        GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
    }

    private void InitialRequireFile()
    {
        dtRequestFile = AccidentBLL.Instance.AccidentRequestFile("ACCIDENTTAB2");
        GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);
    }

    #endregion

    #region Validate
    private string Validate()
    {
        string mess = string.Empty;
        try
        {

            DataTable dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(0, "ACCIDENTTAB2");

            StringBuilder sb = new StringBuilder();
            object c;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                {
                    c = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                    Type = c.GetType().ToString();

                    if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                    {
                        TextBox txt = (TextBox)c;
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                    {
                        DropDownList cbo = (DropDownList)c;
                        if (cbo.SelectedIndex < 1)
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.RadioButton"))
                    {
                        RadioButton rb = (RadioButton)c;
                        if (rb.ID == "cbDamage_Traffic1")
                        {
                            if (!rb.Checked && !cbDamage_Traffic2.Checked && !cbDamage_Traffic3.Checked)
                                sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                        }
                        else
                        {
                            if (!rb.Checked)
                                sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                        }

                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.RadioButtonList"))
                    {
                        RadioButtonList rbl = (RadioButtonList)c;
                        if (rbl.SelectedIndex < 0)
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
                    {
                        HtmlInputText txt = (HtmlInputText)c;
                        if (string.Equals(txt.Value.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                }
            }
            if (!string.Equals(sb.ToString(), string.Empty))
                mess += sb.ToString();
            return mess;
        }
        catch (Exception ex)
        {
            return RemoveSpecialCharacters(ex.Message);
            //throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    #endregion

    #region ValidateSaveUpLoadFile
    private string ValidateSaveUpLoadFile()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                return sb.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region SendEmail
    private bool SendEmail(int TemplateID, string accID)
    {
        try
        {
            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                string EmailList = string.Empty;
                if (TemplateID == ConfigValue.EmailAccident2)
                {
                    #region EmailAccident2
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, false, true);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}
                    //                    EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com,TMS_yutasak.c@pttor.com
                    //,TMS_kittipong.l@pttor.com,TMS_suphakit.k@pttor.com
                    //,TMS_apipat.k@pttor.com
                    //,TMS_nut.t@pttor.com";

                    //EmailList += "nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,SUDTADA.S@PTTOR.COM,PRASARN.N@PTTOR.COM";

                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", txtTruck.Text.Trim());
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem != null ? ddlVendor.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem != null ? ddlContract.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{DRIVER}", txtEMPNAME.Text.Trim());
                    Body = Body.Replace("{ACCSTATUS}", ddlAccidentType.SelectedItem != null ? ddlAccidentType.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{SOURCE}", txtSABBREVIATION.Text.Trim());
                    Body = Body.Replace("{ACCPOINT}", txtLocation.Text.Trim());
                    Body = Body.Replace("{GPS}", txtGPSL.Text.Trim() + "," + txtGPSR.Text.Trim());
                    //Body = Body.Replace("{CREATE_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);
                    if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
                    {
                        string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
                        if (AccidentDate.Any())
                        {
                            Body = Body.Replace("{DATE}", AccidentDate[0]);
                            Body = Body.Replace("{TIME}", AccidentDate[1]);
                        }

                    }
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab3_Vendor.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident2", ColumnEmailName);
                    #endregion

                }
                else if (TemplateID == ConfigValue.EmailAccident3)
                {
                    #region EmailAccident3
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, false);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}
                    //                    EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com,TMS_yutasak.c@pttor.com
                    //,TMS_kittipong.l@pttor.com,TMS_suphakit.k@pttor.com
                    //,TMS_apipat.k@pttor.com
                    //,TMS_nut.t@pttor.com";

                    //EmailList += "nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com";

                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", txtTruck.Text.Trim());
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem != null ? ddlVendor.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem != null ? ddlContract.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{DRIVER}", txtEMPNAME.Text.Trim());
                    Body = Body.Replace("{ACCSTATUS}", ddlAccidentType.SelectedItem != null ? ddlAccidentType.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{SOURCE}", txtSABBREVIATION.Text.Trim());
                    Body = Body.Replace("{ACCPOINT}", txtLocation.Text.Trim());
                    Body = Body.Replace("{GPS}", txtGPSL.Text.Trim() + "," + txtGPSR.Text.Trim());
                    Body = Body.Replace("{LOCK_DATE}", DateTime.Now.ToString("dd/MM/yyyy"));
                    if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
                    {
                        string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
                        if (AccidentDate.Any())
                        {
                            Body = Body.Replace("{DATE}", AccidentDate[0]);
                            Body = Body.Replace("{TIME}", AccidentDate[1]);
                        }

                    }
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab3_Vendor.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident3", ColumnEmailName);
                    #endregion

                }
                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region Serious
    private bool Serious()
    {
        return (((!(string.IsNullOrEmpty(txtVENDOR_DEAD.Text.Trim()) || txtVENDOR_DEAD.Text.Trim() == "0"))//พขร เสียชีวิต
                || (!(string.IsNullOrEmpty(txtVENDOR_STOP.Text.Trim()) || txtVENDOR_STOP.Text.Trim() == "0"))//พขร หยุดงาน
                || (!(string.IsNullOrEmpty(txtPARTY_DEAD.Text.Trim()) || txtPARTY_DEAD.Text.Trim() == "0"))//คู่กรณี เสียชีวิต
                || (!(string.IsNullOrEmpty(txtOTHER_DEAD.Text.Trim()) || txtOTHER_DEAD.Text.Trim() == "0"))//บุคคลที่ 3 เสียชีวิต
                || rblLeak.SelectedValue == "0"//การหกรั่วไหลของผลิตภัณฑ์ที่ขนส่ง
                || rblFire_Truck.SelectedValue == "0"//ไฟไหม้/ระเบิด รถบรรทุกผลิตภัณฑ์
                || rblFire_Commu.SelectedValue == "0"//ไฟไหม้/ระเบิด พื้นที่ชุมชนรอบข้าง 
                || rblDamage_Truck.SelectedValue == "1"//รถขนส่งสามารถเดินรถ เพื่อทำการขนส่งต่อไป ไม่ได้
                || cbDamage_Traffic1.Checked//ผลกระทบต่อการจราจร กีดขวาง ต้องปิดการจราจร ไม่สามารถเดินรถผ่านได้
                || rblDamage_Truck.SelectedValue == "1"//รถขนส่งสามารถเดินรถ เพื่อทำการขนส่งต่อไป ไม่ได้
                || rblMedia.SelectedValue == "0"//สื่อมวลชนในสถานที่เกิดเหตุ/ข้อมูลเหตุการณ์ผู้เผยแพร่สู่สาธารณะชน มี
                ));
    }
    #endregion

}