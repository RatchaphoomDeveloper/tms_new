using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class openFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string str = Request.QueryString["str"];

            if (str != "")
            {
                
                Response.Redirect(str);
            }
           
             #region old code
            /*
            string str = Request.QueryString["str"];
            string[] strQuery;
            if (str != "")
            {
                string sPath = "";
                if (ConfigurationManager.AppSettings["Encrypt"].ToString() != "0")
                {
                    strQuery = STCrypt.DecryptURL(str);
                    sPath = strQuery[0];
                    sPath = Server.UrlEncode(sPath);
                }
                else
                {
                    sPath = str;
                }
                Response.Redirect(sPath);
            }
             */
             #endregion
        }
    }
}
