﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;
using System.IO;

public partial class vendor_checkscore : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //dteStart.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            //dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            dteStart.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            LogUser("5", "R", "เปิดดูข้อมูลหน้า ตรวจสอบการตัดคะแนน", "");

        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {
        BindData();
    }


    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "Search":
                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                break;

            case "edit":

                int Index = int.Parse(e.Parameter.Split(';')[1]);

                gvw.StartEdit(Index);

                dynamic ContractID = gvw.GetRowValues(Index, "SCONTRACTID");
                Session["oSSCONTRACTID"] = ((ContractID + "") == "") ? 0 : ContractID + "";

                //sds1.SelectCommand = @"";
                sds1.SelectParameters.Clear();
                //sds1.SelectParameters.Add(":oSearch", txtSearch.Text.Trim());
                sds1.SelectParameters.Add(":oSSCONTRACTID", Session["oSSCONTRACTID"] + "");
                sds1.SelectParameters.Add(":dStart", dteStart.Text);
                sds1.SelectParameters.Add(":dEnd", dteEnd.Text);
                //sds.SelectParameters.Add(":dStart", dteStart.Date.ToString("dd/MM/yyyy 00:00:00", new CultureInfo("en-US")));
                //sds.SelectParameters.Add(":dEnd", dteEnd.Date.ToString("dd/MM/yyyy 23:59:59", new CultureInfo("en-US")));
                //sds.SelectParameters.Add("dStart", DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("dd/MM/yyyy 00:00:00"));
                //sds.SelectParameters.Add("dEnd", DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("dd/MM/yyyy 23:59:59"));

                sds1.DataBind();
                gvw.DataBind();

                break;

        }

    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void BindData()
    {
        string strsql = "";
        string strstatus = "";
        string Status = cboStatus.Value + "";

        if (Status == "1")
        {
            strstatus = "AND CT.DEND > SYSDATE";
        }
        else if (Status == "2")
        {
            strstatus = "AND CT.DEND < SYSDATE";
        }

        strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY MAX(o.DREDUCE), o.SCONTRACTID) AS ID1, o.SCONTRACTID,CT.SCONTRACTNO,CT.DBEGIN,CT.DEND,CT.NTRUCK ,COUNT(o.SCONTRACTID) AS NCOUNT,SUM(o.NPOINT) AS SUMNPOINT,SUM(o.COST) AS COST,SUM(o.DISABLE_DRIVER) AS DISABLE_DRIVER , TO_CHAR(MAX(o.DREDUCE), 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK FROM (
SELECT DISTINCT CASE WHEN R.SPROCESSID = '020' AND r.SCONTRACTID is null  THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID ,nvl(r.NPOINT,0) AS NPOINT,nvl(r.COST,0) AS COST,nvl(r.DISABLE_DRIVER,0) AS DISABLE_DRIVER , r.DREDUCE,R.SHEADREGISTERNO,R.STRAILERREGISTERNO
FROM (TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO /*AND nvl(R.STRAILERREGISTERNO,1) = nvl(T.STRAILERREGISTERNO,1)*/)
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1) WHERE nvl(r.CACTIVE,'0') = '1' ) o LEFT JOIN TCONTRACT ct ON o.SCONTRACTID = CT.SCONTRACTID  WHERE CT.SVENDORID = :SVENDORID AND  o.DREDUCE BETWEEN TO_DATE(:dStart,'DD/MM/YYYY  HH24:MI:SS') AND TO_DATE(:dEnd,'DD/MM/YYYY  HH24:MI:SS') AND (nvl(CT.SCONTRACTNO,'') LIKE '%' || :oSearch || '%' OR nvl(o.SHEADREGISTERNO,'') LIKE '%' || :oSearch || '%' OR nvl(o.STRAILERREGISTERNO,'') LIKE '%' || :oSearch || '%') " + strstatus + " GROUP BY o.SCONTRACTID,CT.SCONTRACTNO,CT.DBEGIN,CT.DEND,CT.NTRUCK";

//        strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY MAX(o.DREDUCE), o.SCONTRACTID) AS ID1, o.SCONTRACTID,CT.SCONTRACTNO,CT.DBEGIN,CT.DEND
//                          ,CT.NTRUCK ,COUNT(o.SCONTRACTID) AS NCOUNT,SUM(o.NPOINT) AS SUMNPOINT,SUM(o.COST) AS COST
//                          ,SUM(o.DISABLE_DRIVER) AS DISABLE_DRIVER
//                          , CASE WHEN BLACKLIST = 1 THEN 'Y' ELSE '' END AS BLACKLIST
//                          , TO_CHAR(MAX(o.DREDUCE), 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK
//                    FROM (
//                            SELECT CASE WHEN R.SPROCESSID = '020' AND r.SCONTRACTID is null  THEN CASE WHEN T.SCONTRACTID is null THEN 
//                                   CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
//                                   ,nvl(r.NPOINT,0) AS NPOINT,nvl(r.COST,0) AS COST,nvl(r.DISABLE_DRIVER,0) AS DISABLE_DRIVER , r.BLACKLIST, r.DREDUCE,R.SHEADREGISTERNO
//                                   ,R.STRAILERREGISTERNO
//                            FROM (TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO)
//                    LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1)
//                    WHERE nvl(r.CACTIVE,'0') = '1' ) o LEFT JOIN TCONTRACT ct ON o.SCONTRACTID = CT.SCONTRACTID  WHERE CT.SVENDORID = :SVENDORID AND o.DREDUCE BETWEEN TO_DATE(:dStart,'DD/MM/YYYY  HH24:MI:SS') AND TO_DATE(:dEnd,'DD/MM/YYYY  HH24:MI:SS') AND (nvl(CT.SCONTRACTNO,'') LIKE '%' || :oSearch || '%' OR nvl(o.SHEADREGISTERNO,'') LIKE '%' || :oSearch || '%' OR nvl(o.STRAILERREGISTERNO,'') LIKE '%' || :oSearch || '%')
//                    GROUP BY o.SCONTRACTID,CT.SCONTRACTNO,CT.DBEGIN,CT.DEND,CT.NTRUCK, BLACKLIST";

        sds.SelectCommand = strsql;
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add(":oSearch", txtSearch.Text.Trim());
        sds.SelectParameters.Add(":SVENDORID", Session["SVDID"] + "");
        sds.SelectParameters.Add(":dStart", dteStart.Text + " 00:00:00");
        sds.SelectParameters.Add(":dEnd", dteEnd.Text + " 23:59:59");
        //sds.SelectParameters.Add(":dStart", dteStart.Date.ToString("dd/MM/yyyy 00:00:00", new CultureInfo("en-US")));
        //sds.SelectParameters.Add(":dEnd", dteEnd.Date.ToString("dd/MM/yyyy 23:59:59", new CultureInfo("en-US")));
        //sds.SelectParameters.Add("dStart", DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("dd/MM/yyyy 00:00:00"));
        //sds.SelectParameters.Add("dEnd", DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("dd/MM/yyyy 23:59:59"));

        sds.DataBind();
        gvw.DataBind();

    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
