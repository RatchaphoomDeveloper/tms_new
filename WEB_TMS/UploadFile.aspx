﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="UploadFile.aspx.cs" Inherits="UploadFile" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="ไฟล์อัพโหลด"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell style="text-align:right">
                                                <asp:Label ID="lblSearchType" runat="server" Text="กลุ่มเอกสาร :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:260px">
                                                <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell style="text-align:left">
                                                <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" Width="100px" CssClass="btn btn-md btn-hover btn-info" OnClick="cmdSearch_Click" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>

                                    <br />

                                    <asp:GridView ID="dgvTopic" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" PageSize="10" OnPageIndexChanging="dgvTopic_PageIndexChanging" AllowPaging="true"
                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnRowUpdating="dgvTopic_RowUpdating">
                                        <Columns>
                                            <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UPLOAD_TYPE" HeaderText="หัวข้อ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (MB)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุล">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ISACTIVE" HeaderText="สถานะ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="การกระทำ">
                                                <ItemTemplate>
                                                    <asp:Button ID="cmdEdit" runat="server" Text="แก้ไข" CommandName="update" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                        <PagerStyle CssClass="pagination-ys" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdAdd" runat="server" Text="เพิ่ม" Width="100px" CssClass="btn btn-md btn-hover btn-info" OnClick="cmdAdd_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>