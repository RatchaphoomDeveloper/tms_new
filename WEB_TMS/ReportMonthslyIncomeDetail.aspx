﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportMonthslyIncomeDetail.aspx.cs" Inherits="ReportMonthslyIncomeDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="70%" align="right">&nbsp;</td>
                                                <td width="9%"  align="right">
                                                    <dx:ASPxComboBox runat="server" ID="cboMonth">
                                                        <Items>
                                                            <dx:ListEditItem Text="มกราคม" Value="01" />
                                                            <dx:ListEditItem Text="กุมภาพันธ์" Value="02" />
                                                            <dx:ListEditItem Text="มีนาคม" Value="03" />
                                                            <dx:ListEditItem Text="เมษายน" Value="04" />
                                                            <dx:ListEditItem Text="พฤษภาคม" Value="05" />
                                                            <dx:ListEditItem Text="มิถุนายน" Value="06" />
                                                            <dx:ListEditItem Text="กรกฎาคม" Value="07" />
                                                            <dx:ListEditItem Text="สิงหาคม" Value="08" />
                                                            <dx:ListEditItem Text="กันยายน" Value="09" />
                                                            <dx:ListEditItem Text="ตุลาคม" Value="10" />
                                                            <dx:ListEditItem Text="พฤศจิกายน" Value="11" />
                                                            <dx:ListEditItem Text="ธันวาคม" Value="12" />
                                                        </Items>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="ระบุเดือน"
                                                            RequiredField-IsRequired="true" ValidationGroup="add" Display="Dynamic">
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="2%" align="center">-</td>
                                                <td width="9%">
                                                    <dx:ASPxComboBox runat="server" ID="cboYear">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="ระบุปี"
                                                            RequiredField-IsRequired="true" ValidationGroup="add" Display="Dynamic">
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="10%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false" ValidationGroup="add">
                                                        <ClientSideEvents Click="function(){  if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('search');}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="87%" height="35">
                                        <%--<dx:ASPxLabel runat="server" ID="lblsHead" Text="ตารางปฏิบัติงานประจำวันที่" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>--%>
                                        <%--<dx:ASPxLabel runat="server" ID="lblsTail" Text="" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>--%>
                                    </td>
                                    <td width="13%">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                            <tr>
                                                <td width="37%"></td>
                                                <td width="37%"></td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnPDF" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="PDF" OnClick="btnPDF_Click" ValidationGroup="add">
                                                                          <ClientSideEvents Click="function(){ if(!ASPxClientEdit.ValidateGroup('add')) return false;}" />
                                                        <Image Url="images/ic_pdf2.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnExcel" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="Excel" OnClick="btnExcel_Click" ValidationGroup="add">
                                                        <ClientSideEvents Click="function(){ if(!ASPxClientEdit.ValidateGroup('add')) return false;}" />
                                                        <Image Url="images/ic_ms_excel.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="100%" AutoGenerateColumns="false">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ที่" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนหัว" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="VEH_NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนท้าย" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="TU_NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รหัสวัดน้ำ" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="SCAR_NUM">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทคำขอ" Width="18%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="REQTYPE_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="สาเหตุ" Width="23%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="CAUSE_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันนัดหมาย" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="APPOINTMENT_DATE">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="สถานะ" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="STATUSREQ_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
