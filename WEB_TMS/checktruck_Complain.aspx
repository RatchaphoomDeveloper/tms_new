﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
  CodeFile="checktruck_Complain.aspx.cs" Inherits="checktruck_Complain"
    StylesheetTheme="Aqua" %>
<%@ Register Src="~/UserControl/cButtonDelete.ascx" TagPrefix="uc2" TagName="cButtonDelete" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <%--<asp:Panel ID="pnlMain" runat="server" EnableViewState="true"></asp:Panel>--%>
    <%--<asp:Button ID="cmdTest" runat="server" Text="Get" onclick="cmdTest_Click" />--%>
    <br />
    <br />
    <ul class="nav nav-tabs" runat="server" id="tabtest">
        <li class="active" id="Tab1" runat="server"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab">ข้อมูลทั่วไป</a></li>
        <li class="" id="Tab2" runat="server"><a href="#TabProcess" data-toggle="tab" aria-expanded="false"
            runat="server" id="ProcessTab">การดำเนินการแก้ไข</a></li>
        <li class="" id="Tab3" runat="server"><a href="#TabFinal" data-toggle="tab" aria-expanded="false"
            runat="server" id="FinalTab">บันทึกตัดคะแนน</a></li>
    </ul>
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลทั่วไป
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="Table1" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>
                        หมายเลขเอกสาร
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtDocID" runat="server" style="width: 250px; text-align: center"
                                                    readonly="readonly" value="Generate by System" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:Label ID="lblEditDate" runat="server" Text="(เอกสารนี้สามารถแก้ไขได้ ภายในวันที่&nbsp;{0})"
                                                    Visible="false" ForeColor="Blue" Font-Underline="true"></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                เรื่องที่ร้องเรียน
                                                <asp:Label ID="lblReqcmbTopic" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cmbTopic" runat="server" class="form-control" DataTextField="TOPIC_NAME"
                                                    DataValueField="TOPIC_ID" AutoPostBack="true" Width="250px" OnSelectedIndexChanged="cmbTopic_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                คลังต้นทาง
                                                <asp:Label ID="lblReqcbxOrganiz" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cbxOrganiz" runat="server" class="form-control" DataTextField="STERMINALNAME"
                                                    DataValueField="STERMINALID" Width="250px">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ประเภทเรื่องร้องเรียน - หลัก
                                                <asp:Label ID="lblReqcboComplainType" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboComplainType" runat="server" class="form-control" DataTextField="COMPLAIN_TYPE_NAME"
                                                    DataValueField="COMPLAIN_TYPE_ID"  Width="250px" >
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:CheckBox ID="chkLock" runat="server" Text="หยุดพักการขนส่งชั่วคราว (พขร.)" ForeColor="Red" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                บริษัทผู้ประกอบการขนส่ง
                                                <asp:Label ID="lblReqcboVendor" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboVendor" runat="server" class="form-control" DataTextField="SVENDORNAME" DataValueField="SVENDORID" Width="250px" disabled="disabled" >
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                        ประเภทผลิตภัณฑ์
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <dx:ASPxRadioButtonList ID="rblTypeProduct" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="น้ำมัน" Value="O" />
                                                        <dx:ListEditItem Text="ก๊าซ" Value="G" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                เลขที่สัญญา
                                                <asp:Label ID="lblReqcboContract" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboContract" runat="server" class="form-control" DataTextField="SCONTRACTNO"
                                                     disabled="disabled" DataValueField="SCONTRACTID" Width="250px" >
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                เลข Outbound
                                                <asp:Label ID="lblReqcboDelivery" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboDelivery" runat="server" class="form-control" DataTextField="SDELIVERYNO"
                                                    DataValueField="SDELIVERYNO" Width="250px">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ทะเบียนรถ (หัว)
                                                <asp:Label ID="lblReqcboHeadRegist" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboHeadRegist" runat="server" class="form-control" DataTextField="SHEADREGISTERNO"
                                                     DataValueField="STRUCKID" Width="250px"  disabled="disabled" >
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                        ทะเบียนรถ (ท้าย)
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtTrailerRegist" runat="server" disabled="disabled"
                                                    style="width: 250px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ชื่อ พขร. ที่ถูกร้องเรียน
                                                <asp:Label ID="lblReqcmbPersonalNo" runat="server"  ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cmbPersonalNo" runat="server" class="form-control" DataTextField="FULLNAME"
                                                    DataValueField="SPERSONELNO" Width="250px">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                สถานที่เกิดเหตุ
                                                <asp:Label ID="lblReqtxtComplainAddress" runat="server"  ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtComplainAddress" runat="server" style="width: 250px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                วันที่ขนส่ง
                                                <asp:Label ID="lblReqtxtDateTrans" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="100px">
                                                <asp:TextBox ID="txtDateTrans" runat="server" CssClass="datetimepicker"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                จำนวนรถ (คัน)
                                                <asp:Label ID="lblReqtxtTotalCar" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtTotalCar" runat="server" style="width: 250px; text-align:center" disabled="disabled" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="5">
                                                <asp:GridView ID="dgvHeader" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnRowDeleting="dgvHeader_RowDeleting"
                                                    OnRowCommand="dgvHeader_RowCommand" OnRowDataBound="dgvHeader_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="TopicID" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TopicName" HeaderText="เรื่องที่ร้องเรียน">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ComplainID" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ComplainName" HeaderText="เรื่องร้องเรียน-หลัก">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ContractID" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ContractName" HeaderText="เลขที่สัญญา">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CarHead" HeaderText="ทะเบียนรถ(หัว)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="DeliveryDate" HeaderText="วันที่ ขนส่ง">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <div class="col-sm-1">
                                                                <asp:ImageButton ID="imgViewComplain" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                    Width="16px" Height="16px" Style="cursor: pointer" CommandName="Select" />&nbsp;</div>
                                                                <div class="col-sm-1">
                                                                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/btnEdit.png" Width="16px"
                                                                    Height="16px" Style="cursor: pointer" CommandName="EditHeader" />&nbsp;</div>
                                                                <uc2:cButtonDelete runat="server" ID="cButtonDelete" OnClickDelete="cButtonDelete_ClickDelete" IDDelete='<%# "gvContract_" + Container.DataItemIndex %>'  />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdAddHeader" runat="server" Text="เพิ่มรายการ" class="btn btn-md btn-hover btn-info" UseSubmitBehavior="false"
                                Width="100px" OnClick="cmdAddHeader_Click" />
                            &nbsp;
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" class="btn btn-md btn-hover btn-danger" UseSubmitBehavior="false"
                                Style="width: 80px" OnClick="cmdCancel_Click" Visible="false" />
                        </div>
                        <br />
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i>รายละเอียดและสาเหตุ
                            </div>
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <div class="panel-body">
                                        <asp:Table ID="Table2" Width="100%" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    ชื่อผู้ร้องเรียน
                                                    <asp:Label ID="lblReqtxtFName" runat="server" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox CssClass="form-control" ID="txtFName" runat="server" Width="250px" />
                                                </asp:TableCell><asp:TableCell>
                                    &nbsp;
                                                </asp:TableCell><asp:TableCell>
                                    หน่วยงาน
                                    <font color="#ff0000">&nbsp;*</font>
                                                </asp:TableCell><asp:TableCell>
                                                    <asp:TextBox CssClass="form-control" ID="txtFNameDivision" runat="server" Width="250px" />
                                                </asp:TableCell></asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    วันที่รับร้องเรียน
                                                    <asp:Label ID="lblReqtxtDateComplain" runat="server" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                </asp:TableCell><asp:TableCell Width="100px">
                                                    <asp:TextBox runat="server" CssClass="datetimepicker" ID="txtDateComplain"></asp:TextBox>
                                                </asp:TableCell><asp:TableCell>
                                        &nbsp;
                                                </asp:TableCell><asp:TableCell>

                                                </asp:TableCell><asp:TableCell>
                                                </asp:TableCell></asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                            
                                                </asp:TableCell><asp:TableCell Width="100px">
                                            
                                                </asp:TableCell><asp:TableCell>
                                        &nbsp;
                                                </asp:TableCell><asp:TableCell>

                                                </asp:TableCell><asp:TableCell>
                                                </asp:TableCell></asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>

                                ชื่อผู้บันทึก
                                                </asp:TableCell><asp:TableCell>
                                                    <input class="form-control" id="txtComplainBy" runat="server" style="width: 250px;"
                                                        disabled="disabled" />
                                                </asp:TableCell><asp:TableCell>
                                &nbsp;
                                                </asp:TableCell><asp:TableCell>
                                หน่วยงาน
                                                </asp:TableCell><asp:TableCell>
                                                    <input class="form-control" id="txtComplainDivisionBy" runat="server" style="width: 250px;"
                                                        disabled="disabled" />
                                                </asp:TableCell></asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                            วันที่สร้างเอกสาร
                                                </asp:TableCell><asp:TableCell Width="100px">
                                                    <asp:TextBox runat="server" CssClass="datetimepicker" ID="txtCreateDate" Enabled="false"></asp:TextBox>
                                                </asp:TableCell><asp:TableCell>
                                        &nbsp;
                                                </asp:TableCell><asp:TableCell>

                                                </asp:TableCell><asp:TableCell>
                                                </asp:TableCell></asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                    สาเหตุ / รายละเอียด <font color="#ff0000">&nbsp;*</font>
                                                </asp:TableCell><asp:TableCell ColumnSpan="4">
                                                    <textarea id="txtDetail" runat="server" cols="1" rows="10" class="form-control" style="width: 100%;
                                                        height: 120px;"></textarea>
                                                </asp:TableCell></asp:TableRow>
                                        </asp:Table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" style="text-align: right">
                                <asp:Button ID="cmdSaveTab1" runat="server" Enabled="false" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info"
                                    Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave"
                                    UseSubmitBehavior="false" />
                                &nbsp;
                                <asp:Button ID="cmdCancelDoc" runat="server" Enabled="false" Text="ยกเลิกเอกสาร"
                                    CssClass="btn btn-md btn-hover btn-danger" Style="width: 120px;" data-toggle="modal"
                                    UseSubmitBehavior="false" data-target="#ModalConfirmBeforeCancel" />
                                &nbsp;
                                <asp:Button ID="cmdCloseTab1" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false"
                                    OnClientClick="javaScript:window.close(); return false;" Style="width: 100px" />
                            </div>
                        </div>
                        <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    
    <div class="tab-pane fade" id="TabProcess">
        <br />
        <br />
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>การดำเนินการ และ วิธีการป้องกัน
            </div>
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <div class="panel-body">
                        <asp:Table ID="Table4" runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Width="35%">
                                การดำเนินการ<font color="#ff0000">*</font>
                                </asp:TableCell><asp:TableCell>
                                    <asp:TextBox CssClass="form-control" ID="txtOperate" runat="server" Width="100%"
                                        Height="75px" TextMode="MultiLine" />
                                </asp:TableCell></asp:TableRow><asp:TableRow>
                                <asp:TableCell Width="35%">
                                วิธีการป้องกันการเกิดปัญหาซ้ำ
                                </asp:TableCell><asp:TableCell>
                                    <asp:TextBox CssClass="form-control" ID="txtProtect" runat="server" Width="100%"
                                        Height="75px" TextMode="MultiLine" />
                                </asp:TableCell></asp:TableRow><asp:TableRow>
                                <asp:TableCell Width="35%">
                                พนักงานผู้รับผิดชอบ
                                </asp:TableCell><asp:TableCell>
                                    <asp:TextBox CssClass="form-control" ID="txtsEmployee" runat="server" Width="100%" />
                                </asp:TableCell></asp:TableRow><asp:TableRow>
                                <asp:TableCell Width="35%">
                                CC Email
                                </asp:TableCell><asp:TableCell>
                                    <asp:TextBox CssClass="form-control" ID="txtCCEmail" runat="server" Width="100%" />
                                </asp:TableCell></asp:TableRow><asp:TableRow>
                                <asp:TableCell Width="35%">
                                        
                                </asp:TableCell><asp:TableCell>
                                &nbsp;ในกรณีที่มีหลายอีเมล์ ให้คั่นด้วย "," เช่น Email1@ptt.com,Email2@ptt.com,Email3@ptt.com
                                </asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                &nbsp; </div></div><div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ) </div><div class="panel-body">
                <div class="dataTable_wrapper">
                    <div class="panel-body">
                        <asp:GridView ID="dgvRequestFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                            AlternatingRowStyle BackColor="White" ForeColor="#284775">
                            <Columns>
                                <asp:BoundField DataField="COMPLAIN_TYPE_NAME" HeaderText="หัวข้อ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="COMPLAIN_TYPE_ID" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="panel-footer" style="text-align: right">
                &nbsp; </div></div><div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>แนบเอกสาร หลักฐาน </div><div class="panel-body">
                <div class="dataTable_wrapper">
                    <div class="panel-body">
                        <asp:Table ID="Table3" runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Width="35%">
                                        ประเภทไฟล์เอกสาร
                                </asp:TableCell><asp:TableCell>
                                    <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                        Width="350px" DataValueField="UPLOAD_ID">
                                    </asp:DropDownList>
                                </asp:TableCell></asp:TableRow><asp:TableRow>
                                <asp:TableCell Width="35%">
                                    <asp:FileUpload ID="fileUpload" runat="server" Width="100%" />
                                </asp:TableCell><asp:TableCell>
                                    <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false"
                                        OnClick="cmdUpload_Click" Style="width: 100px" />
                                </asp:TableCell></asp:TableRow></asp:Table><br />

                        <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                            DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                            OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                            OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                            Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="panel-footer" style="text-align: right">
                <asp:Button ID="cmdSaveTab2" runat="server" Enabled="false" Text="บันทึก (ยืนยัน)" UseSubmitBehavior="false"
                    CssClass="btn btn-md btn-hover btn-info" OnClick="cmdSaveTab2_Click" Style="width: 100px" />
                &nbsp; <asp:Button ID="cmdSaveTab2Draft" runat="server" Enabled="false" Text="บันทึก (ร่าง)" UseSubmitBehavior="false"
                    CssClass="btn btn-md btn-hover btn-warning" OnClick="cmdSaveTab2Draft_Click"
                    Style="width: 100px" />
                &nbsp; <asp:Button ID="cmdCloseTab2" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false"
                    OnClientClick="javaScript:window.close(); return false;" Style="width: 100px" />
            </div>
        </div>
        <br />
        <dx:ASPxDateEdit ID="dteFinish" runat="server" SkinID="xdte" Visible="false">
        </dx:ASPxDateEdit>
    </div>
    <div class="tab-pane fade" id="TabFinal">
        <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>
                        <asp:Label ID="lblCusScoreHeader" runat="server" Text="การตัดคะแนน (เลขที่สัญญา {0})"></asp:Label></div><div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:Table ID="Table111" Width="100%" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Width="15%">&nbsp;</asp:TableCell><asp:TableCell Width="25%" ColumnSpan="2"> <asp:CheckBox ID="chkSaveTab3Draft" runat="server" Text="อยู่ระหว่างการพิจารณาของ รข."
                                                ForeColor="Red" />
                                        </asp:TableCell>
                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell Width="15%"> หัวข้อตัดคะแนนประเมิน<font color="#ff0000">*</font> </asp:TableCell><asp:TableCell
                                            Width="25%" ColumnSpan="4"> <asp:DropDownList ID="cboScore" runat="server" class="form-control" DataTextField="STOPICNAME"
                                                OnSelectedIndexChanged="cboScore_SelectedIndexChanged" DataValueField="STOPICID"
                                                AutoPostBack="true" Width="100%">
                                            </asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell ColumnSpan="4"> <asp:Label ID="lblShowOption" runat="server" Text="[พขร. ทุจริต]" Visible="false"
                                                ForeColor="Red"></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="lblScore1" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore1" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center" /></asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell
                                                        Width="15%"> <asp:Label ID="lblScore2" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label></asp:TableCell><asp:TableCell
                                                            Width="25%"> <asp:TextBox CssClass="form-control" ID="txtScore2" runat="server" Width="200px"
                                                                Enabled="false" Style="text-align: center" /></asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="lblScore3" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore3" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center" /></asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="lblScore4" runat="server" Text="แผนงานขนส่ง"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore4" runat="server" Width="200px"
                                                                Enabled="false" Style="text-align: center" /></asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow ID="rowListCarTab3" Visible="false">
                                        <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px;
                                            vertical-align: top;"> <asp:GridView ID="dgvScore" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="">
                                                        <HeaderTemplate>
                                                            &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true"
                                                                Checked="true" OnCheckedChanged="chkChooseScoreAll_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemStyle Width="36px" />
                                                        <ItemTemplate>
                                                            &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TRUCKID" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CARHEAD" HeaderText="ทะเบียนรถ (หัว)">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CARDETAIL" HeaderText="ทะเบียนรถ (หาง)">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            </asp:GridView>
                                        </asp:TableCell></asp:TableRow>
                                    <asp:TableRow ID="rowTotalCarTab3" Visible="false">
                                        <asp:TableCell> <asp:Label ID="Label12" runat="server" Text="จำนวนรถ (คัน)"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtTotalCarTab3" runat="server" CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow runat="server" ID="rowOil" Visible="false">
                                        <asp:TableCell> <asp:Label ID="Label10" runat="server" Text="ค่าน้ำมัน / ลิตร"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtOilPrice" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilPrice_OnKeyPress');"
                                                    CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="Label11" runat="server" Text="จำนวนน้ำมัน (ลิตร)"></asp:Label></asp:TableCell><asp:TableCell
                                                            Width="20%"> <asp:TextBox ID="txtOilQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilQty_OnKeyPress');"
                                                                CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow><asp:TableRow runat="server" ID="rowZeal" Visible="false">
                                        <asp:TableCell> <asp:Label ID="lblZeal1" runat="server" Text="ค่าปรับต่อ 1 ซิล"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtZeal" runat="server" Enabled="false" CssClass="form-control"
                                                    Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="Label9" runat="server" Text="จำนวน ซิล"></asp:Label></asp:TableCell><asp:TableCell
                                                            Width="20%"> <asp:TextBox ID="txtZealQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtZealQty_OnKeyPress');"
                                                                CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label1" runat="server" Text="ค่าปรับ"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtCost" runat="server" Width="200px" ReadOnly="true"
                                                    Enabled="false" Style="text-align: center" />
                                            </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="Label2" runat="server" Text="ระงับ พชร. (วัน)"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtDisableDriver" runat="server" Width="200px"
                                                        Enabled="false" Style="text-align: center" />
                                                </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="lblScore5" runat="server" Text="ผลคูณความรุนแรง"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore5" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center" /></asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="lblScore6" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore6" runat="server" Width="200px"
                                                                Enabled="false" Style="text-align: center" /></asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell ColumnSpan="5"> <asp:GridView ID="dgvScoreList" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                OnRowDeleting="dgvScore_RowDeleting" OnRowCommand="dgvScoreList_RowCommand" AlternatingRowStyle-BackColor="White"
                                                RowStyle-ForeColor="#284775">
                                                <Columns>
                                                    <asp:BoundField DataField="STOPICID" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="STOPICNAME" HeaderText="หัวข้อตัดคะแนน">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="430px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Score6Display" HeaderText="หักคะแนนต่อครั้ง">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CostDisplay" HeaderText="ค่าปรับ(บาท)">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DisableDriverDisplay" HeaderText="ระงับ พขร.(วัน)">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgViewScore" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                Width="23px" Height="23px" Style="cursor: pointer" CommandName="Select" />&nbsp; <asp:ImageButton ID="imgDeleteScore" runat="server" ImageUrl="~/Images/bin1.png"
                                                                Width="23px" Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            </asp:GridView>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right">
                        <asp:Button ID="cmdAddScore" CssClass="btn btn-md btn-hover btn-info" Style="width: 100px" UseSubmitBehavior="false"
                            runat="server" Text="เพิ่มรายการ" OnClick="cmdAddScore_Click" />
                        <br />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="panel panel-info" id="div1" runat="server">
            <div class="panel-heading">
                <i class="fa fa-table"></i>แนบเอกสารพิจารณาโทษ - อื่นๆ </div><div class="panel-body">
                <div class="dataTable_wrapper">
                    <div class="panel-body">
                        <asp:Table ID="Table5" runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Width="35%"> ประเภทไฟล์เอกสาร </asp:TableCell><asp:TableCell>
                                    <asp:DropDownList ID="cboUploadTypeTab3" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                        Width="350px" DataValueField="UPLOAD_ID">
                                    </asp:DropDownList>
                                </asp:TableCell></asp:TableRow><asp:TableRow>
                                <asp:TableCell Width="35%">
                                    <asp:FileUpload ID="fileUploadTab3" runat="server" Width="100%" />
                                </asp:TableCell><asp:TableCell>
                                    <asp:Button ID="cmdUploadTab3" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false"
                                        OnClick="cmdUploadTab3_Click" Style="width: 100px" />
                                </asp:TableCell></asp:TableRow></asp:Table><br />
                        <asp:GridView ID="dgvUploadFileTab3" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" DataKeyNames="UPLOAD_ID,FULLPATH"
                            OnRowDeleting="dgvUploadFileTab3_RowDeleting" OnRowUpdating="dgvUploadFileTab3_RowUpdating"
                            OnRowDataBound="dgvUploadFileTab3_RowDataBound">
                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                            Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                </div>
                <br />
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="panel panel-info" id="divScore" runat="server">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>บันทึกการตัดคะแนน </div><div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:Table ID="Table222" runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell Width="20%"> <asp:Label ID="lblCusScoreType" runat="server" Text="เงื่อนไขการตัดคะแนน"></asp:Label></asp:TableCell><asp:TableCell
                                                ColumnSpan="2" Width="40%"> <asp:RadioButtonList ID="radCusScoreType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                    OnSelectedIndexChanged="radCusScoreType_SelectedIndexChanged">
                                                </asp:RadioButtonList>
                                            </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label13" runat="server" Text="หักคะแนน"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtPointFinal" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell><asp:Label ID="lblShowSumPoint" Visible="false"
                                                        runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label14" runat="server" Text="ค่าปรับ"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtCostFinal" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell><asp:Label ID="lblShowSumCost" Visible="false"
                                                        runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label15" runat="server" Text="ระงับ พขร. (วัน)"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtDisableFinal" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell><asp:Label ID="lblShowSumDisable" Visible="false"
                                                        runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell></asp:TableRow></asp:Table><asp:Table runat="server" ID="tblScoreFinal" Width="100%" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label3" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore1_SelectedIndexChanged"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore1Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore1Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label4" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore2_SelectedIndexChanged"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore2Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore2Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label5" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore3_SelectedIndexChanged"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore3Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore3Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label6" runat="server" Text="แผนงานขนส่ง"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore4_SelectedIndexChanged"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore4Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore4Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label7" runat="server" Text="ผลคูณความรุนแรง"></asp:Label></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore5Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell> <asp:Label ID="Label8" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore6Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" /></asp:TableCell><asp:TableCell Style="padding-left: 5px"> </asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                        <asp:Button ID="cmdSaveTab3" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" OnClick="cmdSaveTab3_Click" UseSubmitBehavior="false"
                            Style="width: 100px" />
                        &nbsp; <asp:Button ID="cmdSaveTab3Draft" runat="server" Text="บันทึก (ร่าง)" CssClass="btn btn-md btn-hover btn-warning" UseSubmitBehavior="false"
                            OnClick="cmdSaveTab3Draft_Click" Style="width: 100px" />
                        &nbsp; <asp:Button ID="cmdChangeStatus" runat="server" Text="ขอเอกสารเพิ่มเติม" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false"
                            Style="width: 140px" data-toggle="modal" data-target="#ModalConfirmBeforeChange" />
                        &nbsp; <asp:Button ID="cmdCloseTab3" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false"
                            OnClientClick="javaScript:window.close(); return false;" Style="width: 100px" />
                        <br />
                    </div>
                </div>
                <br />
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <br />
    </div>

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab1_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpChangeStatus" IDModel="ModalConfirmBeforeChange"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdChangeStatus_Click"
        TextTitle="ยืนยันการเปลี่ยนสถานะ" TextDetail="คุณต้องการขอเอกสารเพิ่มเติมใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmCancel" IDModel="ModalConfirmBeforeCancel"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdCancelDoc_Click"
        TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกเอกสารใช่หรือไม่ ?" />
    <%--</div>--%>
    <%--<script>

        $(document).ready(function () {
            
            cmdSaveTabremove();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function cmdSaveTabremove() {
            $("#<%=cmdSaveTab1.ClientID%>,#<%=cmdCancelDoc.ClientID%>").removeAttr('onclick');

        }
    </script>--%>
</asp:Content>
