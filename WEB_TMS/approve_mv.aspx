﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="approve_mv.aspx.cs" Inherits="approve_mv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript">

     
    </script>
    <script type="text/javascript">
        function DoClick(sReport) {

            if (sReport != '') {
                if (sReport == '0') {
                    btnReport1.DoClick();
                }
                else if (sReport == '1') {
                    btnReport2.DoClick();
                }
                else if (sReport == '2') {
                    btnReport3.DoClick();
                }
                else if (sReport == '3') {
                    btnReport4.DoClick();
                }
                else if (sReport == '4') {
                    btnReport5.DoClick();
                }
            }
        }

        function DoNewTab(sReport) {
            if (sReport != '') {

                window.open(sReport);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ if(s.cpExport != undefined)DoClick(s.cpExport);s.cpExport=''; if(s.cpNewTab != undefined)DoNewTab(s.cpNewTab);    eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" cellpadding="3" cellspacing="1">
                    <tr>
                        <td width="36%" valign="top">
                            <table width="100%" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td>
                                        <img src="images/arr.gif" width="7" height="7" alt="" />
                                        ประเภทคำขอ
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxRadioButtonList runat="server" ID="rblReqType" RepeatDirection="Vertical"
                                            SkinID="rblStatus" DataSourceID="sdsReqType" TextField="REQTYPE_NAME" ValueField="REQTYPE_ID"
                                            RepeatColumns="1">
                                        </dx:ASPxRadioButtonList>
                                        <asp:SqlDataSource ID="sdsReqType" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                            SelectCommand="SELECT REQTYPE_ID, REQTYPE_NAME FROM TBL_REQTYPE WHERE (ISACTIVE_FLAG = :ISACTIVE_FLAG) ORDER BY REQTYPE_ID ASC">
                                            <SelectParameters>
                                                <asp:Parameter DefaultValue="Y" Name="ISACTIVE_FLAG" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="75px">
                                                    <img src="images/arr.gif" width="7" height="7" alt="" />
                                                    ช่วงเวลา
                                                </td>
                                                <td>
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblDateType" SkinID="rblStatus">
                                                        <Items>
                                                            <dx:ListEditItem Text="วันที่ยื่นคำขอ" Value="0" />
                                                            <dx:ListEditItem Text="วันที่หมดอายุวัดน้ำ" Value="1" />
                                                            <dx:ListEditItem Text="วันที่ขอรับคิว" Value="2" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%--<dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte" CssClass="dxeLineBreakFix">
                                        </dx:ASPxDateEdit>--%>
                                         <%--<dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte" CssClass="dxeLineBreakFix">
                                        </dx:ASPxDateEdit>--%>
                                         <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="200px">
                                                   <asp:TextBox ID="edtStart" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                </td>
                                                <td>
                                                    -
                                                </td>
                                                <td width="200px">
                                                   <asp:TextBox ID="edtEnd" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="36%">
                            <table width="100%" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td>
                                        <img src="images/arr.gif" width="7" height="7" alt="" />
                                        คำค้นหา
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox runat="server" ID="txtSearch" Width="70%" NullText="บริษัทขนส่ง,ทะเบียนรถ">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="images/arr.gif" width="7" height="7" alt="" />
                                        ตัวกรองสถานะ
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxCheckBoxList runat="server" ID="cblStatus" DataSourceID="sdsStatus" TextField="STATUSREQ_NAME"
                                            ValueField="STATUSREQ_ID" RepeatColumns="2">
                                        </dx:ASPxCheckBoxList>
                                        <asp:SqlDataSource ID="sdsStatus" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                            SelectCommand="SELECT STATUSREQ_ID, STATUSREQ_NAME FROM TBL_STATUSREQ WHERE (ISACTIVE_FLAG = :ISACTIVE_FLAG) AND NORDER > 2 ORDER BY NORDER">
                                            <SelectParameters>
                                                <asp:Parameter DefaultValue="Y" Name="ISACTIVE_FLAG" Type="String" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td rowspan="6">
                                    </td>
                                </tr>--%>
                            </table>
                        </td>
                        <td width="28%" valign="top" align="left" bgcolor="#E3F7F8">
                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <dx:ASPxButton runat="server" ID="ASPxButton2" Text="ตารางคิวงานที่ตอบรับ" AutoPostBack="false">
                                            <ClientSideEvents Click="function(){ xcpn.PerformCallback('QWORK');}" />
                                        </dx:ASPxButton>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxButton runat="server" ID="ASPxButton1" Text="เพิ่มคำขอรับบริการ" AutoPostBack="false">
                                            <ClientSideEvents Click="function(){ xcpn.PerformCallback('newbill');}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" class="StatusBG">
                                        <dx:ASPxButton runat="server" ID="btnPrintPlot" Text="พิมพ์แผนงานประจำวัน" AutoPostBack="true"
                                            OnClick="btnPrintPlot_Click">
                                            <%--<ClientSideEvents Click="function (s, e) {  xcpn.PerformCallback('request');}" />--%>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="StatusBG">
                                        <%--<dx:ASPxDateEdit runat="server" ID="edtStart1" CssClass="dxeLineBreakFix" SkinID="xdte">
                                        </dx:ASPxDateEdit>--%>
                                        <asp:TextBox ID="edtStart1" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                    </td>
                                    <td align="center" class="StatusBG">
                                        <%--<dx:ASPxDateEdit runat="server" ID="edtEnd1" CssClass="dxeLineBreakFix" SkinID="xdte">
                                        </dx:ASPxDateEdit>--%>
                                        <asp:TextBox ID="edtEnd1" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxButton runat="server" ID="btnReportbankForm" Text="พิมพ์BankForm" AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) {  dxConfirmPrint('','เลือกรายงาน', function (s, e) {xcpn.PerformCallback('print');   dxpopupPrint.Hide(); }, function(s, e) {  dxpopupPrint.Hide();} );    }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" CssClass="dxeLineBreakFix" OnClick="btnSearch_Click"
                                AutoPostBack="false">
                                <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('search') }" />--%>
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="ASPxButton3" SkinID="_cancelsearch" CssClass="dxeLineBreakFix"
                                AutoPostBack="false">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Unsearch') }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <dx:ASPxGridView runat="server" ID="gvw" AutoGenerateColumns="true" KeyFieldName="REQUEST_ID"
                                Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Width="120px">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ทะเบียนท้าย" FieldName="TU_NO" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Width="120px">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="ยื่นคำขอ" FieldName="REQUEST_DATE" Width="120px">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หมดอายุวัดน้ำ" FieldName="DWATEREXPIRE" Width="120px">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="วันที่ขอรับคิว" FieldName="ACCEPT_DATE" Width="120px">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="วันที่นัดหมาย" FieldName="SERVICE_DATE" Width="120px">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                        Width="300px">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ประเภทคำขอ" FieldName="REQTYPE_NAME" HeaderStyle-HorizontalAlign="Center"
                                        Width="200px">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="สถานะคำขอ" FieldName="STATUSREQ_NAME" HeaderStyle-HorizontalAlign="Center"
                                        Width="200px">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="จัดคิว" Width="80px">
                                        <DataItemTemplate>
                                            <dx:ASPxButton runat="server" ID="btnOrder" Text="จัดคิว" AutoPostBack="false" ClientEnabled='<%# Eval("STATUS_FLAG").ToString() == "03"  || Eval("STATUS_FLAG").ToString() == "04" || Eval("STATUS_FLAG").ToString() == "06"  || Eval("STATUS_FLAG").ToString() == "07" || Eval("STATUS_FLAG").ToString() == "10" ||  Eval("RK_FLAG").ToString() == "M" ? true : false %>'>
                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="บันทึก" Width="80px">
                                        <DataItemTemplate>
                                            <dx:ASPxButton runat="server" ID="btnSave" Text="บันทึก" AutoPostBack="false" ClientEnabled='<%# Eval("STATUS_FLAG").ToString() == "04" || Eval("STATUS_FLAG").ToString() == "06"  || Eval("STATUS_FLAG").ToString() == "07" || Eval("STATUS_FLAG").ToString() == "10"  ? true : false %>'>
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit3;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ใบงาน" Width="80px">
                                        <DataItemTemplate>
                                            <dx:ASPxButton runat="server" ID="btnReport" Text="พิมพ์" AutoPostBack="true" ClientEnabled='<%# Eval("STATUS_FLAG").ToString() == "04" || Eval("STATUS_FLAG").ToString() == "06"  || Eval("STATUS_FLAG").ToString() == "07" || Eval("STATUS_FLAG").ToString() == "10"  ? true : false %>' OnClick="btnReport1_Click">
                                                <ClientSideEvents Click="function (s, e) { txtIndexgvw.SetText(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)) }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="STATUS_FLAG" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="RK_FLAG" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="REQUEST_ID" Visible="false">
                                    </dx:GridViewDataColumn>

                                </Columns>
                                <Settings ShowHorizontalScrollBar="True" UseFixedTableLayout="true" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxTextBox runat="server" ID="txtIndexgvw" ClientInstanceName="txtIndexgvw" ClientVisible="false"></dx:ASPxTextBox>
                            <dx:ASPxButton runat="server" ID="btnReport1" ClientInstanceName="btnReport1" OnClick="btnReport1_Click"
                                ClientVisible="false">
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnReport2" ClientInstanceName="btnReport2" OnClick="btnReport2_Click"
                                ClientVisible="false">
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnReport3" ClientInstanceName="btnReport3" AutoPostBack="false"
                                ClientVisible="false">
                                <ClientSideEvents Click="function (s, e) {  alert('3');}" />
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnReport4" ClientInstanceName="btnReport4" AutoPostBack="false"
                                ClientVisible="false">
                                <ClientSideEvents Click="function (s, e) { alert('4');}" />
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnReport5" ClientInstanceName="btnReport5" AutoPostBack="false"
                                ClientVisible="false">
                                <ClientSideEvents Click="function (s, e) {  alert('5');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td>ขอสำเนาเอกสารการตรวจสอบปริมาตรถังน้ำมัน
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <dx:ASPxGridView runat="server" ID="gvwOther" Width="100%" KeyFieldName="REQUEST_ID"
                                AutoGenerateColumns="false">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Width="9%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ทะเบียนท้าย" FieldName="TU_NO" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Width="9%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="ยื่นคำขอ" FieldName="REQUEST_DATE" Width="9%">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="หมดอายุวัดน้ำ" FieldName="DWATEREXPIRE" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                        Width="28%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ประเภทคำขอ" FieldName="REQTYPE_NAME" HeaderStyle-HorizontalAlign="Center"
                                        Width="20%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="สถานะคำขอ" FieldName="STATUSREQ_NAME" HeaderStyle-HorizontalAlign="Center"
                                        Width="20%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="จัดคิว" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton runat="server" ID="btnSaveother" AutoPostBack="false" Text="บันทึก">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('request;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
