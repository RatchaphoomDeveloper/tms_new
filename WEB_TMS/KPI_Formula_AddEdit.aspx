﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Formula_AddEdit.aspx.cs" Inherits="KPI_Formula_AddEdit" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="แก้ไข สูตรการคำนวณ"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblFormulaName" runat="server" Text="หัวข้อสูตร :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtFormulaName" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblFormulaDetail" runat="server" Text="สูตรการคำนวณ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtFormulaDetail" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="text-align:right;">
                                                <asp:Label ID="lblVariableA" runat="server" Text="ข้อความ A :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtVariableA" runat="server" CssClass="form-control" Width="350px"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblValueA" runat="server" Text="ค่าเริ่มต้น A :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtValueA" runat="server" CssClass="form-control" Width="350px"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="text-align:right;">
                                                <asp:Label ID="lblVariableB" runat="server" Text="ข้อความ B :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtVariableB" runat="server" CssClass="form-control" Width="350px"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblValueB" runat="server" Text="ค่าเริ่มต้น B :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtValueB" runat="server" CssClass="form-control" Width="350px"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="text-align:right;">
                                                <asp:Label ID="lblFrequency" runat="server" Text="ความถี่ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtFrequency" runat="server" CssClass="form-control" Width="350px" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdCancel_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>