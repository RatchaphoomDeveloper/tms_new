﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_request.aspx.cs" Inherits="vendor_request" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){  if(s.cpNewTab != undefined)DoNewTab(s.cpNewTab);    eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="43%" align="left">
                                        ประเภทข้อมูล :
                                    </td>
                                    <td width="45%" align="left">
                                        สถานะ :
                                    </td>
                                    <td width="10%">
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxRadioButtonList runat="server" ID="rblDatastatus" RepeatColumns="4" Width="98%"
                                           >
                                            <Items>
                                                <dx:ListEditItem Text="ข้อมูลรถ" Value="H" />
                                                <dx:ListEditItem Text="Truck Component(TC)" Value="T" />
                                                <dx:ListEditItem Text="ข้อมูลสัญญา" Value="C" />
                                                <dx:ListEditItem Text="ข้อมูล พขร." Value="E" />
                                                <dx:ListEditItem Text="ข้อมูลผู้ขนส่ง" Value="V" />
                                            </Items>
                                        </dx:ASPxRadioButtonList>
                                    </td>
                                    <td>
                                        <dx:ASPxRadioButtonList runat="server" ID="rblStatus" RepeatColumns="4" Width="98%"
                                            >
                                            <Items>
                                                <dx:ListEditItem Text="ผขส.ขอเปลี่ยนข้อมูล" Value="0" />
                                                <dx:ListEditItem Text="รข.ขอข้อมูลเพิ่มเติม" Value="3" />
                                                <dx:ListEditItem Text="รข. อนุมัติ" Value="1" />
                                                <dx:ListEditItem Text="รข. ปฏิเสธ" Value="2" />
                                            </Items>
                                        </dx:ASPxRadioButtonList>
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                <tr></tr>
                                <tr>
                                   
                                  <td colspan="2">ค้นหาข้อมูลเพิ่มเติม : <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" placeholder="ค้นหา ทะเบียนรถ พขร. สัญญา" style="width:99%"></asp:TextBox></td>
                                  <td><dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false" Height="30px" style="margin-top:15px">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('search') }" />
                                        </dx:ASPxButton></td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView runat="server" ID="gvw" Width="100%" KeyFieldName="REQ_ID" AutoGenerateColumns="false">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ประเภทข้อมูล" FieldName="STYPE" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Width="12%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="รายการที่ถูกแก้ไข" FieldName="DESCRIPTION" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="วันที่สร้างคำขอ" FieldName="DCREATE" Width="15%">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUSNAME" Width="15%">
                                        <%--   <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                        </PropertiesTextEdit>--%>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="" Width="12%">
                                        <HeaderTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                        <dx:ASPxButton runat="server" ID="btnadd" AutoPostBack="false" Text="เพิ่มคำร้องขอ" Enabled='<%# (CanWrite) ? true : false %>'>
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('request'); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <DataItemTemplate>
                                            <%--  <dx:ASPxButton runat="server" ID="btnSaveother" AutoPostBack="false" Text="แก้ไข" ClientEnabled='<%# Eval("STATUS").ToString() == "0" ? true : false %>'>
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('approve;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>--%>
                                            <dx:ASPxButton runat="server" ID="btnSaveother" AutoPostBack="false" Text="ยื่นคำร้อง">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('approve;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="TYPE" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="STATUS" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager PageSize="25">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
