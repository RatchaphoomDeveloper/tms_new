﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Globalization;
using System.Web.Configuration;
using TMS_BLL.Master;
public partial class admin_home : System.Web.UI.Page
{
    string _conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //foreach (object objSession in HttpContext.Current.Session)
            //{
            //    Response.Write(objSession + " : " + HttpContext.Current.Session["" + objSession] + "<br>");
            //}
            string sdate2day = DateTime.Today.ToString("dd MMM yyyy");
            spn_ConfirmContract.InnerHtml += sdate2day; //"สรุปรถยืนยันตามสัญญา" +
            spn_ConfirmPlan.InnerHtml += sdate2day;//"สรุปรถยืนยันตามแผน"+sdate2day
            spn_TruckIssue.InnerHtml += sdate2day;//"สรุปรถเกิดปัญหา"sdate2day
            //spn_Plan2Day.InnerHtml += sdate2day;//"แผนงาน"+sdate2day
            try
            {
                ListConfTruck();
                ListConfPlan();
                ListAccident();
                ListPersonalInfo();
                ListAppeal();
                ListLoad();
                ListApprove();
                this.ListDriverLog();
            }
            catch { }
            Session["ssAlert"] = null;
        }
    }
    void ListConfTruck()
    {
        string[] ArrayConfTruckList = { "รถตามสัญญา", "ยืนยันแล้ว", "ไม่ยืนยัน" };
        string msg = ""
            , sOutHtml = @"<table border=""0"" width=""100%"">
                                <tr>
                                    <td width=""67%"">{0}</td>
                                    <td align=""center"" width=""33%"">{1}</td>
                                </tr>
                                <tr visible=""{7}"" align=""center"">
                                    <td colspan=""2""><font color=""red"">{8}</font></td> 
                                </tr> 
                                <tr visible=""{6}"">
                                    <td>{2}</td>
                                    <td align=""center"">{3}</td>
                                </tr>
                                <tr>
                                    <td>{4}</td>
                                    <td align=""center"">{5}</td>
                                </tr>
                            </table>"
            , sConfTruck = @"SELECT --TCONT.SVENDORID , 
SUM(NVL(TCONT.NTRUCK,0)) NTRUCK 
,SUM(NVL(TCONF.CONFIRMED,0)) CONFIRMED
,SUM(NVL(TCONF.CONFIRMES,0)) CONFIRMES
FROM(
    SELECT DISTINCT CONT.SVENDORID,CONT.SCONTRACTID,COUNT(CONT_TRCK.STRUCKID)  NTRUCK
    FROM TCONTRACT CONT 
    LEFT JOIN TCONTRACT_TRUCK CONT_TRCK  ON CONT.SCONTRACTID=CONT_TRCK.SCONTRACTID
    LEFT JOIN TTRUCK TRCK ON CONT_TRCK.STRUCKID=TRCK.STRUCKID
    WHERE NVL(NTRUCK,0)>0 AND NVL(CACTIVE,'Y')='Y' AND CGROUPCONTRACT =('C') AND NVL(CSTANDBY,'N')='N'
    AND TO_DATE(SYSDATE,'dd/mm/yyyy') BETWEEN TO_DATE(CONT.DBEGIN,'dd/mm/yyyy') AND  TO_DATE(CONT.DEND,'dd/mm/yyyy')
    AND NVL(TRCK.CACTIVE,'N')='Y'
    --AND CONT.SVENDORID='{0}' --0010001144     0010001778 
    GROUP BY CONT.SVENDORID,CONT.SCONTRACTID 
)TCONT
LEFT JOIN (
     SELECT SCONTRACTID 
     ,SUM(CASE WHEN NVL(i_trckconf.CCONFIRM,'0')='0' THEN 1 ELSE 0 END) CONFIRMES
    ,SUM(CASE WHEN NVL(i_trckconf.CCONFIRM,'0')='1' THEN 1 ELSE 0 END) CONFIRMED
     FROM TTRUCKCONFIRM t_trckconf
     LEFT JOIN TTRUCKCONFIRMLIST i_trckconf ON T_TRCKCONF.NCONFIRMID =I_TRCKCONF.NCONFIRMID
     WHERE 1=1 AND DDATE = TO_DATE('{1}','dd/MM/yyyy') AND NVL(t_trckconf.CCONFIRM,'0')='1'
     GROUP BY SCONTRACTID 
 ) TCONF ON TCONT.SCONTRACTID=TCONF.SCONTRACTID
 
 --GROUP BY TCONT.SVENDORID";
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["SVDID"], DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtConfTruck.Rows.Count > 1)
            {
                msg = "ไม่พบรายการ..";
                ltrvendor_conftruck.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!");
            }
            else if (dtConfTruck.Rows.Count == 1)
            {
                msg = "";
                try
                {
                    ltrvendor_conftruck.Text = string.Format(sOutHtml, ArrayConfTruckList[0], " " + dtConfTruck.Rows[0]["NTRUCK"] + " คัน"
                         , ArrayConfTruckList[1], " " + double.Parse(dtConfTruck.Rows[0]["CONFIRMED"] + "").ToString("#,0") + " คัน"
                         , ArrayConfTruckList[2], " " + double.Parse(dtConfTruck.Rows[0]["CONFIRMES"] + "").ToString("#,0") + " คัน"
                         , true, !true, "");
                }
                catch { }
            }
            else
            {
                msg = "ไม่พบรายการ";
                ltrvendor_conftruck.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ");
            }
        }
    }
    void ListConfPlan()
    {
        //string[] ArrayConfTruckList = { "รถตามแผน", "ยืนยันแล้ว", "ไม่ยืนยัน" };
        string[] ArrayConfTruckList = { "แผนงาน", "จัดงาน", "ไม่ได้จัดงาน" };
        string msg = ""
            , sOutHtml = @"<table border=""0"" width=""100%"">
                                <tr>
                                    <td width=""67%"">{0}</td>
                                    <td align=""center"" width=""33%"">{1}</td>
                                </tr>
                                <tr visible=""{7}"" align=""center"">
                                    <td colspan=""2""><font color=""red"">{8}</font></td> 
                                </tr> 
                                <tr style=""display:{6}"">
                                    <td>{2}</td>
                                    <td align=""center"">{3}</td>
                                </tr>
                                <tr>
                                    <td>{4}</td>
                                    <td align=""center"">{5}</td>
                                </tr>
                            </table>"
            ,
        #region OLDVERSION
//            sConfTruck = @"SELECT /*SVENDORID ,*/COUNT(NPLANID) NRECORD ,SUM(CONFIRMED) CONFIRMED,SUM(CONFIRMES) CONFIRMES
//FROM(
//    SELECT t_vend.SVENDORID,(t_plnschd.NPLANID) NPLANID 
//     ,MAX(CASE WHEN NVL(t_plnschd.CCONFIRM,'0')='0' THEN 1 ELSE 0 END) CONFIRMED
//    ,MAX(CASE WHEN NVL(t_plnschd.CCONFIRM,'0')='1' THEN 1 ELSE 0 END) CONFIRMES 
//     FROM  TVENDOR t_vend 
//     LEFT JOIN TPLANSCHEDULE t_plnschd ON t_vend.SVENDORID =t_plnschd.SVENDORID
//     LEFT JOIN TPlanScheduleList i_plnschd ON T_PLNSCHD.NPLANID =i_plnschd.NPLANID
//    WHERE 1=1 
//    --AND t_vend.SVENDORID='{0}' --0010001144     0010001778
//    AND t_plnschd.CACTIVE = '1'
//    AND i_plnschd.CACTIVE = '1'
//    AND TO_DATE(DDELIVERY,'DD/MM/YYYY') = TO_DATE('{1}','dd/MM/yyyy')
//    GROUP BY t_vend.SVENDORID,t_plnschd.NPLANID
//)
//-- GROUP BY SVENDORID ";
        #endregion
 sConfTruck = @"SELECT 
COUNT(CASE 
WHEN ODP.ORDERTYPE = '1' THEN TO_DATE(ODP.DATE_CREATE,'DD/MM/YYYY') 
WHEN ODP.ORDERTYPE = '2' THEN TO_DATE(ODP.DDELIVERY,'DD/MM/YYYY')
ELSE SYSDATE END) as NRECORD
 ,SUM(CASE WHEN NVL(PSL.CACTIVE,'XXX') = '0' OR  NVL(PSL.CACTIVE,'XXX') = 'XXX'  THEN 1 ELSE 0 END) CONFIRMES 
 ,SUM(CASE WHEN NVL(PSL.CACTIVE,'XXX') = '1' THEN 1 ELSE 0 END) CONFIRMED
FROM TBL_ORDERPLAN ODP
LEFT JOIN TPlanScheduleList PSL ON PSL.SDELIVERYNO = ODP.SDELIVERYNO
--LEFT JOIN TPLANSCHEDULE t_plnschd ON t_vend.SVENDORID =t_plnschd.SVENDORID
WHERE 1=1 
AND ODP.CACTIVE = 'Y' AND NVL(PSL.CACTIVE,'XXX') <> '0' 
AND CASE 
WHEN ODP.ORDERTYPE = '1' THEN TO_DATE(ODP.DATE_CREATE,'DD/MM/YYYY') 
WHEN ODP.ORDERTYPE = '2' THEN TO_DATE(ODP.DDELIVERY,'DD/MM/YYYY')
ELSE SYSDATE END = TO_DATE('{1}','dd/MM/yyyy')";
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["SVDID"], DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtConfTruck.Rows.Count > 1)
            {
                msg = "ไม่พบรายการ..";
                ltr_confplan.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "false", "true", "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!");
            }
            else if (dtConfTruck.Rows.Count == 1)
            {
                msg = "";//double.Parse(dtConfTruck.Rows[0]["NCOMMIT"] + "").ToString("#,0")
                ltr_confplan.Text = string.Format(sOutHtml, ArrayConfTruckList[0], " " + dtConfTruck.Rows[0]["NRECORD"] + " รายการ"
                    , ArrayConfTruckList[1], " " + double.Parse((dtConfTruck.Rows[0]["CONFIRMED"] + "" == "" ? "0" : dtConfTruck.Rows[0]["CONFIRMED"]) + "").ToString("#,0") + " รายการ"
                     , ArrayConfTruckList[2], " " + double.Parse((dtConfTruck.Rows[0]["CONFIRMES"] + "" == "" ? "0" : dtConfTruck.Rows[0]["CONFIRMES"]) + "").ToString("#,0") + " รายการ"
                     , "true", "false", "");
            }
            else
            {
                msg = "ไม่พบรายการ";
                ltr_confplan.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "none", "true", "ไม่พบรายการ");
            }
        }
    }
    void ListAccident()
    {
        if (Session["ssAlert"] == null) PreAlert();
        string[] ArrayConfTruckList = { "ปกติ", "ตกค้าง", "รถเกิดอุบัติเหตุ", "ไม่ผ่านการตรวจสภาพ", "ห้ามวิ่ง", "ให้แก้ไข (วิ่งได้)" };
        #region format
        string msg = ""
            , sOutHtml = @"<table width=""100%"" border=""0"">
				          <tbody> 
                          <tr>
				                <td width=""50%"">&nbsp;</td>
				                <td width=""25%"" align=""right"">รถในสัญญา</td>
				                <td width=""25%"" align=""right"">รถสำรอง</td>
				            </tr> 
                            <tr>
				                <td width=""50%"" title=""รถที่ระบุอยู่ภายใต้สัญญาทั้งหมด"">รถทั้งหมด</td>
				                <td width=""25%"" align=""right"">{0}</td>
				                <td width=""25%"" align=""right"">{1}</td>
				            </tr> 
                             <tr>
				                <td width=""50%"" title=""รถที่ถูกกัก ณ คลังปลายทางเท่านั้น"">รถตกค้าง</td>
				                <td width=""25%"" align=""right"">{2}</td>
				                <td width=""25%"" align=""right"">{3}</td>
				            </tr> 
                             <tr>
				                <td width=""50%"">รถไม่ผ่านการตรวจสภาพ</td>
				                <td width=""25%"" align=""right""></td>
				                <td width=""25%"" align=""right""></td>
				            </tr> 
                             <tr>
				                <td width=""50%"" title=""รถที่ถูกให้แก้ไขจากการตรวจสภาพเท่านั้น"">&nbsp;&nbsp;-รอการแก้ไข(วิ่งได้ )</td>
				                <td width=""25%"" align=""right"">{4}</td>
				                <td width=""25%"" align=""right"">{5}</td>
				            </tr>
                             <tr>
				                <td width=""50%"" title=""รถที่ถูกห้ามวิ่งจากการตรวจสภาพเท่านั้น"">&nbsp;&nbsp;-ห้ามวิ่ง</td>
				                <td width=""25%"" align=""right"">{6}</td>
				                <td width=""25%"" align=""right"">{7}</td>
				            </tr>  
				          </tbody>
                        </table>"
            , sConfTruck = @"SELECT  COUNT(STRUCKID) NALL
,COUNT(STRUCKID)-( NVL(SUM(CASE WHEN NVL(CBAN,'0')='1' THEN 1 ELSE 0 END),0) 
+NVL(SUM(CASE WHEN NVL(CREJECT,'0')='1' THEN 1 ELSE 0 END),0)    
+NVL(SUM(CASE WHEN NVL(CACCIDENT,'0')='1' THEN 1 ELSE 0 END),0)
+NVL(SUM(CASE WHEN NVL(CHOLD,'0')='1' THEN 1 ELSE 0 END) ,0)
+NVL(SUM(CASE WHEN NVL(NMA,'0')='1' THEN 1 ELSE 0 END),0)

) Stand
,NVL(SUM(CASE WHEN NVL(CBAN,'0')='1' THEN 1 ELSE 0 END),0) NBAN
,NVL(SUM(CASE WHEN NVL(CREJECT,'0')='1' THEN 1 ELSE 0 END),0) NREJECT
,NVL(SUM(CASE WHEN NVL(CACCIDENT,'0')='1' THEN 1 ELSE 0 END),0) NACCIDENT
,NVL(SUM(CASE WHEN NVL(CHOLD,'0')='1' THEN 1 ELSE 0 END),0) NHOLD
,NVL(SUM(CASE WHEN NVL(NMA,'0')='1' THEN 1 ELSE 0 END),0) NMA
FROM (
    SELECT TRCK.STRUCKID , TRCK.SHEADREGISTERNO
    ,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(VEHS.VEH_STATUS,'0')='1' THEN '1' ELSE '0' END CHOLD
    ,NVL(TRCK.CACCIDENT,'0') CACCIDENT
    ,NVL(TRCK.CSTATUS,'0') CSTATUS
    ,NVL(TBAN.CBAN,'0') CBAN
    ,NVL(TLOAD.LOAD,'0') CREJECT
    ,NVL(TCHK.NMA,'0') NMA
    FROM TTRUCK TRCK
    LEFT JOIN (
        SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(VEH_NO,'(',''),')',''),'.',''),'-',''),',','') VEH_NO,'1' VEH_STATUS
        FROM TVEHICLES
        WHERE VEH_STATUS IS NOT NULL
    ) VEHS ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TRCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','')=VEHS.VEH_NO
    LEFT JOIN (
        SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','')  SHEADREGISTERNO,'1' CBAN
        FROM TCARBAN
        WHERE 1=1
        --AND TO_DATE('16/10/2012','DD/MM/YYYY') BETWEEN TO_DATE(DBAN,'DD/MM/YYYY') AND TO_DATE(DUNBAN,'DD/MM/YYYY')
    )TBAN ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TRCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','')= TBAN.SHEADREGISTERNO
    LEFT JOIN(
        SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T_PLNSCHD.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') SHEADREGISTERNO 
        ,NVL(SUM(CASE WHEN NVL(i_plnschd.CCOMMIT,'1')='0' THEN 1 ELSE 0  END),0) LOAD 
         FROM  TPLANSCHEDULE t_plnschd 
         LEFT JOIN TPlanScheduleList i_plnschd ON T_PLNSCHD.NPLANID =i_plnschd.NPLANID
        WHERE 1=1  AND t_plnschd.CACTIVE = '1' AND i_plnschd.CACTIVE = '1'
        --AND TO_DATE(DDELIVERY,'DD/MM/YYYY') = TO_DATE('16/10/2012','DD/MM/YYYY')
        GROUP BY T_PLNSCHD.SHEADREGISTERNO 
    ) TLOAD ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TRCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','')=TLOAD.SHEADREGISTERNO
    LEFT JOIN(
        SELECT T_CHKTRCK.STRUCKID ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T_CHKTRCK.SHEADERREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') SHEADERREGISTERNO
        ,1 NMA  
         FROM  TCHECKTRUCK T_CHKTRCK
         LEFT JOIN TCHECKTRUCKITEM I_CHKTRCK ON T_CHKTRCK.SCHECKID =I_CHKTRCK.SCHECKID
        WHERE 1=1  AND I_CHKTRCK.CMA='1'
        --AND TO_DATE(DCHECK,'DD/MM/YYYY') = TO_DATE('16/10/2012','DD/MM/YYYY')
        GROUP BY  T_CHKTRCK.STRUCKID ,T_CHKTRCK.SHEADERREGISTERNO
    ) TCHK ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TRCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','')=TCHK.SHEADERREGISTERNO
    WHERE NVL(TRCK.CACTIVE,'Y')='Y'
) Tall
WHERE 1=1 ";
        #endregion
        string struckban = "";
        #region SQL QUERY
        string[] SQL = { 
@"SELECT COUNT(CONT.SCONTRACTID) NRECORD,
SUM(CASE WHEN NVL(CONT_TRCK.CSTANDBY,'N') ='N' THEN 1 ELSE 0 END ) NINPROC
,SUM(CASE WHEN NVL(CONT_TRCK.CSTANDBY,'N') ='Y' THEN 1 ELSE 0 END ) NSTANDBY
FROM TCONTRACT CONT
LEFT JOIN TCONTRACT_TRUCK CONT_TRCK ON CONT.SCONTRACTID=CONT_TRCK.SCONTRACTID
WHERE 1=1  AND NVL(CONT.CACTIVE,'Y') = 'Y'  {0}" ,
@"SELECT  TCARBAN.SVENDORID
,TTRUCK.STRUCKID SHEADID ,TCARBAN.SHEADREGISTERNO  ,TCARBAN.STRAILERREGISTERNO--,NVL(TCONTRACT_TRUCK.CSTANDBY,'N') CSTANDBY_HEAD
,(CASE WHEN NVL(TCONTRACT_TRUCK.CSTANDBY,'N') ='N' THEN 1 ELSE 0 END) NPROC
,(CASE WHEN NVL(TCONTRACT_TRUCK.CSTANDBY,'N') ='Y' THEN 1 ELSE 0 END) NSTANDBY
--,TAILER.STRUCKID STAILID,TCARBAN.STRAILERREGISTERNO ,NVL(CONT_TAILER.CSTANDBY,'N') CSTANDBY_TAIL
FROM TCARBAN
LEFT JOIN TTRUCK ON TCARBAN.SHEADREGISTERNO=TTRUCK.SHEADREGISTERNO
LEFT JOIN TCONTRACT_TRUCK ON TCONTRACT_TRUCK.STRUCKID=TTRUCK.STRUCKID
LEFT JOIN TCONTRACT ON TCONTRACT_TRUCK.SCONTRACTID=TCONTRACT.SCONTRACTID
--LEFT JOIN TTRUCK TAILER ON TCARBAN.STRAILERREGISTERNO=TAILER.SHEADREGISTERNO
--LEFT JOIN TCONTRACT_TRUCK CONT_TAILER ON CONT_TAILER.STRUCKID=TTRUCK.STRUCKID
WHERE 1=1 AND NVL(CUNBAN,'0')='0' AND NVL(TCONTRACT.CACTIVE,'Y') = 'Y' " ,
#region OLDVERSION
//    @"SELECT CONT_TRCK.SCONTRACTID, CONT_TRCK.STRUCKID, TRCK.SHEADREGISTERNO, CONT_TRCK.STRAILERID,TRCK.STRAILERREGISTERNO
//,NVL(CONT_TRCK.CSTANDBY,'N') CSTANDBY,CAST(NVL(TRCK.CHOLD,'0') as varchar2(1)) CHOLD
//,NVL(CHKTRCKS.CMA,'x') CMA
//FROM TCONTRACT CONT
//LEFT JOIN TCONTRACT_TRUCK CONT_TRCK ON CONT.SCONTRACTID=CONT_TRCK.SCONTRACTID
//LEFT JOIN TTRUCK TRCK ON CONT_TRCK.STRUCKID= TRCK.STRUCKID
//LEFT JOIN (
//    SELECT distinct CHKTRCK.STRUCKID, CHKTRCK.SHEADERREGISTERNO, CHKTRCK.STRAILERREGISTERNO, MAX(CHKTRCKLST.CMA ) CMA
//    ,COUNT(CHKTRCK.STRUCKID)
//    FROM TCHECKTRUCK CHKTRCK
//    LEFT JOIN TCHECKTRUCKITEM CHKTRCKLST ON CHKTRCK.SCHECKID=CHKTRCKLST.SCHECKID
//    WHERE NVL(CHKTRCKLST.COTHER,'0')!='1' AND NVL(CHKTRCKLST.CMA,'0')!='0'  AND  NVL(CHKTRCKLST.CEDITED,'0')='0' AND NVL(CHKTRCKLST.CCHECKED,'0') !='1' 
//    GROUP BY CHKTRCK.STRUCKID, CHKTRCK.SHEADERREGISTERNO, CHKTRCK.STRAILERREGISTERNO
//) CHKTRCKS ON CONT_TRCK.STRUCKID=CHKTRCKS.STRUCKID
//WHERE 1=1 AND NVL(CHKTRCKS.CMA,'x')!='x'  AND NVL(CONT.CACTIVE,'Y') = 'Y' {0} 
//--AND NVL(TRCK.CACTIVE,'Y')='Y' AND NVL(CONT_TRCK.CREJECT,'N')='N'
//
//" ,	 
	#endregion
@"SELECT TTR.STRUCKID, TTR.SHEADREGISTERNO, TTR.STRAILERREGISTERNO, TTR.SCHASIS,tc.SCONTRACTNO,v.SVENDORNAME,TCT.SCONTRACTID,cc.CMA,TTR.CHOLD,TCT.CSTANDBY
FROM  (((TTRUCK TTR INNER JOIN TContract_Truck TCT ON TTR.STRUCKID = TCT.STRUCKID) 
INNER JOIN   
(
SELECT c.STRUCKID,MAX(ct.CMA) as CMA FROM (TCHECKTRUCK c INNER JOIN    TCHECKTRUCKITEM ct ON c.SCHECKID =  CT.SCHECKID)
INNER JOIN  TCHECKTRUCKFILE cl ON CT.SCHECKID = CL.SCHECKID   WHERE nvl(CCHECKED,0) != 1 GROUP BY c.STRUCKID
) cc ON TTR.STRUCKID = cc.STRUCKID )
INNER JOIN TContract TC ON TCT.SCONTRACTID = TC.SCONTRACTID) LEFT JOIN TVENDOR_SAP v ON tc.SVENDORID = v.SVENDORID
WHERE  NVL(TC.CACTIVE,'Y') = 'Y' {0}

" ,
@""
                       };
        #endregion
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["SVDID"], DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            //รถทั้งหมด
            DataTable dt_STANDBY = CommonFunction.Get_Data(OraConnection, string.Format(SQL[0] + "", " AND NVL(CONT_TRCK.CREJECT,'N')!='Y'"));
            //รถตกค้าง
            DataTable dtBAN = CommonFunction.Get_Data(OraConnection, string.Format(SQL[1] + "", ""));
            if (dtBAN.Rows.Count > 0)
            {
                struckban = "<table>";
                foreach (DataRow drBAN in dtBAN.Rows) struckban += @"<tr ><td style=""color:#ffffff;"">" + (dtBAN.Rows.IndexOf(drBAN) + 1) + ". " + drBAN["SHEADREGISTERNO"] + ((drBAN["STRAILERREGISTERNO"] + "" != "") ? " - " + drBAN["STRAILERREGISTERNO"] + "</td></tr>" : "");
                struckban += "</table>";
            }
            //รถไม่ผ่านการตรวจสภาพ
            DataTable dtCHECK = CommonFunction.Get_Data(OraConnection, string.Format(SQL[2] + "", " "));

            msg = "";
            ltr_accident.Text = string.Format(sOutHtml
                , " " + ((dt_STANDBY.Rows.Count > 0) ? double.Parse(dt_STANDBY.Rows[0]["NINPROC"] + "").ToString("#,0") : "0") + " คัน"
                , " " + ((dt_STANDBY.Rows.Count > 0) ? double.Parse(dt_STANDBY.Rows[0]["NSTANDBY"] + "").ToString("#,0") : "0") + " คัน"
                 , " " + ((dtBAN.Rows.Count > 0) ? double.Parse(dtBAN.Select("NPROC= 1").Length + "").ToString("#,0") : "0") + " คัน"
                 , " " + ((dtBAN.Rows.Count > 0) ? double.Parse(dtBAN.Select("NSTANDBY= 1").Length + "").ToString("#,0") : "0") + " คัน"
                 , " " + ((dtCHECK.Rows.Count > 0) ? double.Parse(dtCHECK.Select("CMA='1'  AND CHOLD = '0'  AND CSTANDBY='N'").Length + "").ToString("#,0") : "0") + " คัน"
                 , " " + ((dtCHECK.Rows.Count > 0) ? double.Parse(dtCHECK.Select("CMA='1'  AND CHOLD = '0'  AND CSTANDBY='Y'").Length + "").ToString("#,0") : "0") + " คัน"
                 , " " + ((dtCHECK.Rows.Count > 0) ? double.Parse(dtCHECK.Select("CMA <> '0' AND CHOLD = '1' AND CSTANDBY='N'").Length + "").ToString("#,0") : "0") + " คัน"
                 , @" <span >" + ((dtCHECK.Rows.Count > 0) ? double.Parse(dtCHECK.Select("CMA <> '0' AND CHOLD = '1' AND CSTANDBY='Y'").Length + "").ToString("#,0") : "0") + " คัน</span>"
                 , true, !true, "");

            dt_STANDBY.Dispose(); dtBAN.Dispose(); dtCHECK.Dispose();

        }
    }
    void ListPersonalInfo()
    {/*
        string msg = ""
            , sConfTruck = @"SELECT TUSER.SUID , TUSER.SVENDORID ,TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SFULLNAME
,CASE TUSER.CGROUP 
WHEN '0' THEN TVENDOR.SABBREVIATION 
WHEN '1' THEN TUNIT.UNITNAME 
WHEN '2' THEN TTERMINAL.SABBREVIATION
END  SUNITNAME
FROM TUSER 
LEFT JOIN TVENDOR ON TUSER.SVENDORID=TVENDOR.SVENDORID
LEFT JOIN TTERMINAL ON TUSER.SVENDORID=TTERMINAL.STERMINALID
LEFT JOIN TUNIT ON TUSER.SVENDORID=TUNIT.UNITCODE
WHERE SUID='{0}'";
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["UserID"]));
            if (dtConfTruck.Rows.Count > 0)
            {
                lbFulllName.Text = "คุณ " + dtConfTruck.Rows[0]["SFULLNAME"];
                lblVendorName.Text = "" + dtConfTruck.Rows[0]["SUNITNAME"];
            }

        }*/
    }
    void ListAppeal()
    {
        string[] ArrayConfTruckList = { "ยื่นอุทรธณ์เดือน ", "ยื่นอุทธรณ์สะสม", "รับพิจารณาแล้ว", "ค้างพิจารณา" };

        DateTime d1 = DateTime.Now.AddDays(-1 * (DateTime.Now.Day - 1));
        DateTime d2 = d1.AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - 1);
        string msg = ""
            , sOutHtml = @"<table width=""100%"" border=""0"">
				          <tbody>  
                          <tr>
				            <td title=""ยื่นอุทธรณ์สะสม(ทั้งปี)"">{9}</td>
				            <td align=""right"">{10}</td> 
				            </tr>
                          <tr style=""display:{6};"">
				            <td width=""60%"">{0}</td>
				            <td width=""23%"" align=""right"">{1}</td> 
				            </tr>
				          <tr visible=""{7}"" align=""center"">
                            <td colspan=""3""><font color=""red"">{8}</font></td> 
                          </tr>
				          <tr>
				            <td>{2}</td>
				            <td align=""right"">{3}</td> 
				            </tr>
				          
				          <tr>
				            <td>{4}</td>
				            <td align=""right"">{5}</td> 
				            </tr> 
				          </tbody></table>"
            , sConfTruck = @"SELECT COUNT(SAPPEALID) NCOUNT
,NVL(SUM(CASE WHEN CSTATUS='3' THEN 1 ELSE 0 END),0) NCOMMIT 
,NVL(SUM(CASE WHEN CSTATUS='3' THEN 0 ELSE 1 END),0) NREJECT
FROM TAPPEAL
WHERE 1=1 AND TO_DATE(DAPPEAL,'dd/mm/yyyy') BETWEEN TO_DATE('{0}','dd/mm/yyyy') AND TO_DATE('{1}','dd/mm/yyyy')
 ";
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, d1.ToString("dd/MM/yyyy", new CultureInfo("en-US")), d2.ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtConfTruck.Rows.Count > 1)
            {
                msg = "ไม่พบรายการ..";
                ltrappeal.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!", "&nbsp;", "");
            }
            else if (dtConfTruck.Rows.Count == 1)
            {
                msg = "";
                ltrappeal.Text = string.Format(sOutHtml, ArrayConfTruckList[0] + DateTime.Today.ToString("MMMM"), " " + dtConfTruck.Rows[0]["NCOUNT"] + " รายการ"
                     , ArrayConfTruckList[2], " " + double.Parse(dtConfTruck.Rows[0]["NCOMMIT"] + "").ToString("#,0") + " รายการ"
                     , ArrayConfTruckList[3], " " + double.Parse(dtConfTruck.Rows[0]["NREJECT"] + "").ToString("#,0") + " รายการ"
                     , "", !true, ""
                     , ArrayConfTruckList[1], " " + double.Parse(CommonFunction.Get_Value(OraConnection, "SELECT COUNT(*) NRECORD FROM TAPPEAL WHERE 1=1 AND TO_DATE(DAPPEAL,'dd/mm/yyyy') BETWEEN TO_DATE('1/1/" + DateTime.Today.Year + "','dd/mm/yyyy') AND TO_DATE('31/12/" + DateTime.Today.Year + "','dd/mm/yyyy')") + "").ToString("#,0") + " รายการ");
                // Response.Write("SELECT COUNT(*) FROM TAPPEAL WHERE 1=1 AND TO_DATE(DAPPEAL,'dd/mm/yyyy') BETWEEN TO_DATE('1/1/" + DateTime.Today.Year + "','dd/mm/yyyy') AND TO_DATE('31/12/" + DateTime.Today.Year + "','dd/mm/yyyy')");
            }
            else
            {
                msg = "ไม่พบรายการ";
                ltrappeal.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "none", true, "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!", "&nbsp;", "");
            }
        }
    }
    void ListLoad()
    {

        string[] ArrayConfTruckList = { "เดือน " + DateTime.Now.ToString("MMMM"), "สะสม (ทั้งปี)" };

        string msg = ""
            , sOutHtml = @"<table width=""100%"" border=""0"">
				          <tbody>  
				          <tr visible=""{7}"" align=""center"">
                            <td colspan=""3""><font color=""red"">{8}</font></td> 
                          </tr>
                          <tr style=""display:{6};"">
				            <td width=""60%"">{0}</td>
				            <td width=""23%"" align=""right"">{1}</td> 
				            </tr>
				          <tr>
				            <td>{2}</td>
				            <td align=""right"">{3}</td> 
				            </tr>
				          <tr>
				            <td>{4}</td>
				            <td align=""right"">{5}</td> 
				            </tr> 
                          <tr>
				            <td>&nbsp;</td>
				            <td align=""right""></td> 
				            </tr>
				          </tbody></table>"
            , sConfTruck = @"SELECT COUNT(SACCIDENTID) NRECORD FROM TACCIDENT WHERE nvl(CSENDTOPK,'0') = '1' AND  TO_DATE(DACCIDENT,'DD/MM/YYYY')  BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY') ";
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            //DateTime d1 = DateTime.Now.AddDays(-1 * (DateTime.Now.Day - 1));
            //DateTime d2 = DateTime.Now.AddDays(-1 * (DateTime.Now.Day - 1)).AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - 1);
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck
                , DateTime.Now.AddDays(-1 * (DateTime.Now.Day - 1)).ToString("dd/MM/yyyy", new CultureInfo("en-US"))
                , DateTime.Now.AddDays(-1 * (DateTime.Now.Day - 1)).AddDays(DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - 1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtConfTruck.Rows.Count > 1)
            {
                msg = "ไม่พบรายการ..";
                ltrloadorder.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!");
            }
            else if (dtConfTruck.Rows.Count == 1)
            {
                msg = "";//double.Parse(dtConfTruck.Rows[0]["Stand"] + "").ToString("#,0")
                ltrloadorder.Text = string.Format(sOutHtml
                     , ArrayConfTruckList[0], " " + double.Parse(dtConfTruck.Rows[0]["NRECORD"] + "").ToString("#,0") + " ครั้ง"
                     , ArrayConfTruckList[1], " " + double.Parse(CommonFunction.Get_Value(OraConnection, string.Format(sConfTruck, "1/1/" + DateTime.Today.Year, "31/12/" + DateTime.Today.Year)) + "").ToString("#,0") + " ครั้ง"
                     , "&nbsp;", " "
                     , "", !true, "");
            }
            else
            {
                msg = "ไม่พบรายการ";
                ltrloadorder.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "none", true, "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!");
            }
        }
    }
    void ListApprove()
    {

        lblMonth.Text = "เดือน"+ DateTime.Now.ToString("MMMM");
        string Month = CommonFunction.ReplaceInjection((DateTime.Now.Month + "").PadLeft(2, '0'));
        string YEAR = CommonFunction.ReplaceInjection((DateTime.Now.Year + ""));
        string Period = Month + YEAR;
        //string RK_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('09','01','02') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "' ";
        //string RK_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('03') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "'";
        //string MV_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('03','04','07','06') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "'";
        //string MV_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "'";

        string RK_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('01','12') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND RK_FLAG <> 'M'";
        string RK_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE (STATUS_FLAG in ('03','04','07','06','10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "')  AND RK_FLAG <> 'M'";
        string MV_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE (STATUS_FLAG in ('01','03','04','05','08','09','07','06') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND REQTYPE_ID  = 01)";
        string MV_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND REQTYPE_ID  = 01";
        //string MV_EDITPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('02','12') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND REQTYPE_ID  = 01";
        string MV_CANCELPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('11') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND REQTYPE_ID  = 01";
        string MV_ONPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE (STATUS_FLAG in ('01','03','04','05','08','09','07','06') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "') AND REQTYPE_ID  = 02";
        string MV_ENDPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND REQTYPE_ID  = 02";
        //string MV_EDITPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('02','12') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND REQTYPE_ID  = 02";
        string MV_CANCELPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('11') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND REQTYPE_ID  = 02";
        DataTable dtPer = CommonFunction.Get_Data(_conn, "SELECT SMENUID,CPERMISSION,SUID FROM TPERMISSION WHERE SUID = '" + CommonFunction.ReplaceInjection(Session["UserID"] + "") + "'");
        if (dtPer.Rows.Count > 0)
        {
            if (dtPer.Select("SMENUID = '56' AND CPERMISSION <> 0").Count() > 0)
            {
                DataTable dt = CommonFunction.Get_Data(_conn, RK_ONPROCESS);
                lblSumw1.Text = (dt.Rows.Count > 0 ? dt.Rows.Count + "" : "0") + " รายการ";

                DataTable dt2 = CommonFunction.Get_Data(_conn, RK_ENDPROCESS);
                lblSumw3.Text = (dt2.Rows.Count > 0 ? dt2.Rows.Count + "" : "0") + " รายการ";
                trRk.Visible = true;
            }
            else
            {
                trRk.Visible = false;
            }

            if (dtPer.Select("SMENUID = '72' AND CPERMISSION <> 0").Count() > 0)
            {
                DataTable dt3 = CommonFunction.Get_Data(_conn, MV_ONPROCESS);
                lblSumw21.Text = (dt3.Rows.Count > 0 ? dt3.Rows.Count + "" : "0") + " รายการ";

                //DataTable dt4 = CommonFunction.Get_Data(_conn, MV_EDITPROCESS);
                //lblSumw22.Text = (dt4.Rows.Count > 0 ? dt4.Rows.Count + "" : "0") + " รายการ";

                DataTable dt5 = CommonFunction.Get_Data(_conn, MV_ENDPROCESS);
                lblSumw23.Text = (dt5.Rows.Count > 0 ? dt5.Rows.Count + "" : "0") + " รายการ";

                DataTable dt6 = CommonFunction.Get_Data(_conn, MV_CANCELPROCESS);
                lblSumw24.Text = (dt6.Rows.Count > 0 ? dt6.Rows.Count + "" : "0") + " รายการ";
                trMv.Visible = true;

                DataTable dt7 = CommonFunction.Get_Data(_conn, MV_ONPROCESS2);
                lblSumw25.Text = (dt7.Rows.Count > 0 ? dt7.Rows.Count + "" : "0") + " รายการ";

                //DataTable dt8 = CommonFunction.Get_Data(_conn, MV_EDITPROCESS);
                //lblSumw22.Text = (dt4.Rows.Count > 0 ? dt4.Rows.Count + "" : "0") + " รายการ";

                DataTable dt9 = CommonFunction.Get_Data(_conn, MV_ENDPROCESS2);
                lblSumw27.Text = (dt9.Rows.Count > 0 ? dt9.Rows.Count + "" : "0") + " รายการ";

                DataTable dt10 = CommonFunction.Get_Data(_conn, MV_CANCELPROCESS2);
                lblSumw28.Text = (dt10.Rows.Count > 0 ? dt10.Rows.Count + "" : "0") + " รายการ";
                trMv2.Visible = true;
            }
            else
            {
                trMv.Visible = false;
                trMv2.Visible = false;
            }
        }

    }
    protected DataTable PreAlert()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("rows", typeof(string));
        dt.Columns.Add("type", typeof(string));
        dt.Columns.Add("detail", typeof(string));
        dt.Columns.Add("amount", typeof(string));
        return dt;
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        switch (e.Parameter)
        {
            case "popup":
                //เมื่อกดปุ่มตกลง ในpopup ลืม password
                string msg = "";
                if (txtForgetUsername.Text.Trim() != "" && txtoldpassword.Text.Trim() != "" && txtnewpassword.Text.Trim() != "")
                {
                    using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                    {
                        var EncodePass = STCrypt.encryptMD5(txtoldpassword.Text.Trim());
                        var NewEncodePass = STCrypt.encryptMD5(txtnewpassword.Text.Trim());
                        var NewEncode = STCrypt.encryptMD5(txtnewpassword.Text.Trim());
                        DataTable dt = CommonFunction.Get_Data(OraConnection, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME ,SPASSWORD FROM TUSER WHERE SUID = '" + Session["UserId"] + "' AND CACTIVE = '1'");
                        if (dt.Rows.Count > 0)
                        {
                            msg = "";
                            if (EncodePass != dt.Rows[0]["SPASSWORD"] + "")
                            {
                                msg += "<br>- รหัสผ่านเดิมไม่ถูกต้อง!";
                            }
                            if (NewEncodePass != NewEncode)
                            {
                                msg += "<br>- รหัสผ่านใหม่ไม่ตรงกัน!";
                            }
                            if (msg == "")
                            {
                                if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();

                                string sql = "UPDATE TUSER SET SPASSWORD = '" + NewEncode + "',SOLDPASSWORD = '" + dt.Rows[0]["SPASSWORD"] + "" + "' WHERE SUID='" + dt.Rows[0]["SUID"] + "" + "' AND SUSERNAME = '" + txtForgetUsername.Text.Trim() + "'";
                                using (OracleCommand com = new OracleCommand(sql, OraConnection))
                                {
                                    try
                                    {
                                        com.ExecuteNonQuery();
                                        msg = "ระบบได้ทำการเปลี่ยนรหัสผ่านให้ท่านแล้ว<br>ท่านสามารถเข้าใช้งานด้วยรหัสผ่านใหม่ได้ทันทีในการครั้งต่อไป";
                                    }
                                    catch
                                    {
                                        msg = "ระบบไม่สามารถดำเนินการเปลี่ยนรหัสผ่านให้ท่านได้<br>กรุณาลองใหม่อีกครั้ง";
                                    }
                                    finally
                                    {
                                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('ผลการดำเนินการ','" + msg + "',function(){pcForget.Hide();});");
                                    }
                                }

                            }
                            else
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบข้อมูลดังนี้ " + msg + "',function(){pcForget.Hide();});");
                            }
                        }
                        else
                        {
                            msg = "ไม่พบผู้ใช้งานดังกล่าว";
                            CommonFunction.SetPopupOnLoad(xcpn, "pcForget.Hide(); dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + msg + "',function(){pcForget.Hide();});");
                        }

                    }
                }
                break;
        }
    }
    protected void xcpn_conftruck_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        switch (e.Parameter)
        {
            case "admin_conftruck":
                ListConfTruck();
                break;
            case "admin_confplan":
                ListConfPlan();
                break;
            case "admin_accident":
                ListAccident();
                break;
            case "admin_appeal":
                ListAppeal();
                break;
            case "admin_load":
                ListLoad();
                break;
            case "admin_approve":
                ListApprove();
                break;

        }
    }

    protected void xcpn_DriverLog_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        switch (e.Parameter)
        {
            case "Refresh":
                this.ListDriverLog();
                break;

        }
    }

    private void ListDriverLog()
    {
        lblDate1.Text = string.Empty;
        lblTotal1.Text = string.Empty;
        lblDate2.Text = string.Empty;
        lblTotal2.Text = string.Empty;
        lblDate3.Text = string.Empty;
        lblTotal3.Text = string.Empty;
        DataTable dt = DashboardBLL.Instance.DocStatusSelectBLL();
        if (dt.Rows.Count > 0)
        {
            lblDate1.Text = dt.Rows[0]["CHG_DATETIME"].ToString();
            lblTotal1.Text = dt.Rows[0]["COUNT"].ToString();

            if (dt.Rows.Count > 1)
            {
                lblDate2.Text = dt.Rows[1]["CHG_DATETIME"].ToString();
                lblTotal2.Text = dt.Rows[1]["COUNT"].ToString();
            }

            if (dt.Rows.Count > 2)
            {
                lblDate3.Text = dt.Rows[2]["CHG_DATETIME"].ToString();
                lblTotal3.Text = dt.Rows[2]["COUNT"].ToString();
            }
        }
    }
}//End Class vendor_home