﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RfcTMS.aspx.cs" Inherits="RfcTMS" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RCF:Service</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Literal ID="ltrLog" runat="server"></asp:Literal>
        <asp:DataGrid ID="dgdRFC" runat="server">
        </asp:DataGrid>
        <hr /> 
        <asp:DataGrid ID="dgdResult" runat="server">
        </asp:DataGrid>
    </div>
    </form>
</body>
</html>
