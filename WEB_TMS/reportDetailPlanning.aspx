﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="reportDetailPlanning.aspx.cs" Inherits="reportDetailPlanning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
<script type="text/javascript" src="Javascript/DevExpress/DevExpress.js" > </script>
    <table>
        <tr>
            <td style="background-color: #D3D3D3">
                ช่วงเวลาที่ต้องการค้นหา : </td>
            <td colspan="2">
                <div style="float: left">
                    <dx:ASPxDateEdit ID="adeStart" runat="server" ClientInstanceName="dteStart" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ระหว่างวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </div>
                <div style="float: left">
                    <dx:ASPxDateEdit ID="adeEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ถึงวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                หน่วยงาน</td>
            <td>
                <dx:ASPxComboBox ID="cbxTerminalStart" runat="server" CallbackPageSize="10" ClientInstanceName="cbxTerminalStart"
                    EnableCallbackMode="True" SkinID="xcbbATC" TextFormatString="{0}" ValueField="SCODE"
                    Width="150px" OnItemRequestedByValue="cbxTerminalStart_ItemRequestedByValue"
                    OnItemsRequestedByFilterCondition="cbxTerminalStart_ItemsRequestedByFilterCondition" nulltext="-- ทั้งหมด --">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="หน่วยงาน" FieldName="SNAME" Width="100px" />
                        <dx:ListBoxColumn Caption="รหัสหน่วยงาน" FieldName="SCODE" Width="80px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td>
                &nbsp; </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                ต้นทาง </td>
            <td>
                <dx:ASPxComboBox ID="cbxTerminal" runat="server" CallbackPageSize="10" ClientInstanceName="cbxTerminal"
                    EnableCallbackMode="True" OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL" nulltext="-- ทั้งหมด --"
                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="STERMINALID" Width="150px">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" Width="100px" />
                        <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" Width="80px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td>
                &nbsp; </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                Export File </td>
            <td>
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" ValidationGroup="search" OnClick="btnSubmit_Click"
                        Text="Pdf" Width="100">
                    </dx:ASPxButton>
                </div>
                <div style="float: left">
                    &nbsp;</div>
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" ValidationGroup="search" OnClick="ASPxButton2_Click"
                        Text="Excel" Width="100">
                    </dx:ASPxButton>
                </div>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
