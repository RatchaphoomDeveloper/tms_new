﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ExportData.aspx.cs" Inherits="ExportData" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <div id="DivNoneMode" runat="server" visible="true">
    </div>
    <a href="SupportReport.aspx">Support Report</a>
    <div id="CONFTRCK" runat="server" visible="true">
        <table width="100%">
            <tr>
                <td align="center" colspan="2">
                    <h2>
                        ข้อมูลการยืนยันรถตามสัญญา
                    </h2>
                </td>
            </tr>
            <tr>
                <td style="width: 10%; background-color: AppWorkspace">
                    ผู้ขนส่ง :
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbxVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cbxVendor"
                        EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                        OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                        SkinID="xcbbATC" TextFormatString="{1}" ValueField="SVENDORID" Width="500px">
                        <ClientSideEvents SelectedIndexChanged="function(s,e){ cboContractNo.PerformCallback(); }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="360px" />
                        </Columns>
                    </dx:ASPxComboBox>
                    <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                        SelectCommand="SELECt * FROM TVENDOR WHERE NVL(CACTIVE,'1')='1'"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="background-color: AppWorkspace">
                    สัญญา :
                </td>
                <td>
                    <dx:ASPxComboBox ID="cboContractNo" runat="server" CallbackPageSize="30" ClientInstanceName="cboContractNo"
                        EnableCallbackMode="True" OnItemRequestedByValue="cboContractNo_OnItemRequestedByValueSQL"
                        OnItemsRequestedByFilterCondition="cboContractNo_OnItemsRequestedByFilterConditionSQL"
                        SkinID="xcbbATC" TextFormatString="{1}" ValueField="SCONTRACTID" Width="500px">
                        <ClientSideEvents BeginCallback="function(s,e){LoadingPanel.Show();}" EndCallback="function(s,e){LoadingPanel.Hide();}" />
                        <Columns>
                            <dx:ListBoxColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" Width="140px" />
                            <dx:ListBoxColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="260px" />
                            <dx:ListBoxColumn Caption="คลัง" FieldName="TERMINAL" Width="60px" />
                        </Columns>
                    </dx:ASPxComboBox>
                    <asp:SqlDataSource ID="sdsContract" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="background-color: AppWorkspace">
                    ช่วงวันที่
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dteStart" runat="server" ClientInstanceName="dteStart" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ระหว่างวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                    -
                    <dx:ASPxDateEdit ID="dteEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ถึงวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <dx:ASPxButton ID="btnSearch" runat="server" ClientInstanceName="btnSearch" Text=" ค้นหารายการ "
                        OnClick="btnSearch_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="float: left">
                        <dx:ASPxButton ID="btnXlsExport" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                            OnClick="btnXlsExport_Click">
                            <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                            </Image>
                        </dx:ASPxButton>
                    </div>
                </td>
                <td>
                </td>
            </tr>
            <tr id="tr_data" runat="server">
                <td colspan="2">
                    <dx:ASPxGridView ID="Confirm_Truck" runat="server" SkinID="_gvw" 
                        DataSourceID="sdsConfirmTruck" 
                        onafterperformcallback="Confirm_Truck_AfterPerformCallback">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="ที่" FieldName="LINE_NO" ShowInCustomizationForm="True"
                                Width="3%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="รหัสผู้ขนส่ง" FieldName="SVENDORID" ShowInCustomizationForm="True"
                                Width="20%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                Width="10%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" ShowInCustomizationForm="True"
                                Width="3%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                Width="19%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="วันที่ยืนยีนสัญญา" FieldName="DDATE" ShowInCustomizationForm="True"
                                Width="18%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="สถานะการยืนยัน" FieldName="IS_CONFIRM_TRUCK"
                                ShowInCustomizationForm="True" Width="22%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="sdsConfirmTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="sdsConfirmTruck"
                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"  
                        SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY CONF.DDATE) LINE_NO, VEND.SVENDORID ,VEND.SABBREVIATION SVENDORNAME ,CONT.SCONTRACTID ,CONT.SCONTRACTNO,CONF.DDATE+1 DDATE,NVL(CONF.CCONFIRM,'') IS_CONFIRM_TRUCK
FROM TVENDOR VEND
LEFT JOIN TCONTRACT CONT ON VEND.SVENDORID=CONT.SVENDORID
LEFT JOIN TTRUCKCONFIRM CONF ON CONT.SCONTRACTID=CONF.SCONTRACTID
WHERE 1=1 AND VEND.SVENDORID= NVL( :S_VENDORID,VEND.SVENDORID) AND CONT.SCONTRACTID=NVL( :S_CONTRACTID,CONT.SCONTRACTID)
AND CONF.DDATE &gt;= TO_DATE( :D_START,'DD/MM/YYYY')-1 AND CONF.DDATE &lt;= TO_DATE( :D_END ,'DD/MM/YYYY')-1 ORDER BY CONF.DDATE--,VEND.SABBREVIATION,CONT.SCONTRACTNO">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cbxVendor" Name=":S_VENDORID"  PropertyName="Value" />
                            <asp:ControlParameter ControlID="cboContractNo" Name=":S_CONTRACTID"  PropertyName="Value" />
                            <asp:ControlParameter ControlID="dteStart" Name=":D_START" PropertyName="Value" />
                            <asp:ControlParameter ControlID="dteEnd" Name=":D_END" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="Confirm_Truck" PageHeader-Font-Names="Tahoma"
            PageHeader-Font-Bold="true">
            <Styles>
                <Header Font-Names="Tahoma" Font-Size="10">
                </Header>
                <Cell Font-Names="Tahoma" Font-Size="8">
                </Cell>
            </Styles>
            <PageHeader>
                <Font Bold="True" Names="Tahoma"></Font>
            </PageHeader>
        </dx:ASPxGridViewExporter>
    </div>
    <div id="PSCD" runat="server" visible="false">
        <table width="100%">
            <tr>
                <td align="center" colspan="2">
                    <h2>
                        ข้อมูลการจัดแผนงาน(คลัง,ปง.)
                    </h2>
                </td>
            </tr>
            <tr>
                <td style="width: 10%; background-color: AppWorkspace">
                    ช่วงวันที่
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dteStart_PSCHD" runat="server" ClientInstanceName="dteStart_PSCHD" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ระหว่างวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                    -
                    <dx:ASPxDateEdit ID="dteEnd_PSCHD" runat="server" ClientInstanceName="dteEnd_PSCHD" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ถึงวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <dx:ASPxButton ID="btnSearch_PSCHD" runat="server" ClientInstanceName="btnSearch_PSCHD" Text=" ค้นหารายการ "
                        OnClick="btnSearch_PSCHD_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="float: left">
                        <dx:ASPxButton ID="btnXlsExport_PSCHD" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                            OnClick="btnXlsExport_PSCHD_Click">
                            <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                            </Image>
                        </dx:ASPxButton>
                    </div>
                </td>
                <td>
                </td>
            </tr>
            <tr id="tr1" runat="server">
                <td colspan="2">
                    <dx:ASPxGridView ID="gvw_PSCHD" runat="server" SkinID="_gvw" 
                        DataSourceID="sds_PSCHD" 
                        onafterperformcallback="gvw_PSCHD_AfterPerformCallback">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="ที่" FieldName="LINE_NO" ShowInCustomizationForm="True"
                                Width="3%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="SHIPMENT_NO" FieldName="SHIPMENT_NO" ShowInCustomizationForm="True"
                                Width="20%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="DELIVERY_NO" FieldName="DELIVERY_NO" ShowInCustomizationForm="True"
                                Width="10%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="DELIVERY_DATE" FieldName="DELIVERY_DATE" ShowInCustomizationForm="True"
                                Width="3%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="SUPPLY_PLANT" FieldName="SUPPLY_PLANT" ShowInCustomizationForm="True"
                                Width="19%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="PLANTNAME" FieldName="PLANTNAME" ShowInCustomizationForm="True"
                                Width="18%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="IS_CREATE_PLAN" FieldName="IS_CREATE_PLAN"
                                ShowInCustomizationForm="True" Width="22%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="sds_PSCHD" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="sds_PSCHD"
                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"  
                        SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY DLVR.DELIVERY_DATE,PLNT.SABBREVIATION) LINE_NO ,SHMT.SHIPMENT_NO ,DLVR.DELIVERY_NO ,DLVR.DELIVERY_DATE ,DLVR.SUPPLY_PLANT ,PLNT.SABBREVIATION PLANTNAME
,CASE WHEN PLNLST.SDELIVERYNO is null THEN '' ELSE '1' END IS_CREATE_PLAN
FROM TSHIPMENT  SHMT
LEFT JOIN TDELIVERY_SAP DLVR ON lpad(SHMT.DELIVERY_NO,10,'0')=lpad(DLVR.DELIVERY_NO,10,'0')
LEFT JOIN TTERMINAL PLNT ON DLVR.SUPPLY_PLANT=PLNT.STERMINALID
LEFT JOIN TPLANSCHEDULEList PLNLST ON lpad(PLNLST.SDELIVERYNO,10,'0')=lpad(DLVR.DELIVERY_NO,10,'0')
LEFT JOIN (SELECT DISTINCT SHIPMENT_NO,DELIVERY_NO,SHIPPING_POINT FROM SAPECP1000087.SHIPMENT_DELIVERY_GROUP@MASTER ) SHPT ON SHPT.SHIPMENT_NO=SHMT.SHIPMENT_NO 
AND lpad(SHMT.DELIVERY_NO,10,'0')=lpad(SHPT.DELIVERY_NO,10,'0')
WHERE 1=1 AND PLNLST.CACTIVE = '1' AND PLNT.CACTIVE = '1' AND SUBSTR(NVL(SHPT.SHIPPING_POINT,'X000'),0,1)  IN ( 'D','E')
AND DLVR.DELIVERY_DATE &gt;= TO_DATE( :D_START ,'DD/MM/YYYY') AND DLVR.DELIVERY_DATE &lt;= TO_DATE( :D_END ,'DD/MM/YYYY')
GROUP BY SHMT.SHIPMENT_NO ,DLVR.DELIVERY_NO ,DLVR.DELIVERY_DATE,DLVR.SUPPLY_PLANT,PLNT.SABBREVIATION,PLNLST.SDELIVERYNO
ORDER BY DLVR.DELIVERY_DATE,PLNT.SABBREVIATION
">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="dteStart_PSCHD" Name=":D_START" PropertyName="Value" />
                            <asp:ControlParameter ControlID="dteEnd_PSCHD" Name=":D_END" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
        <dx:ASPxGridViewExporter ID="grid_PSCHDExporter" runat="server" GridViewID="gvw_PSCHD" PageHeader-Font-Names="Tahoma"
            PageHeader-Font-Bold="true">
            <Styles>
                <Header Font-Names="Tahoma" Font-Size="10">
                </Header>
                <Cell Font-Names="Tahoma" Font-Size="8">
                </Cell>
            </Styles>
            <PageHeader>
                <Font Bold="True" Names="Tahoma"></Font>
            </PageHeader>
        </dx:ASPxGridViewExporter>
    </div>
    <div id="CONFPLNT" runat="server" visible="false">
    
        <table width="100%">
            <tr>
                <td align="center" colspan="2">
                    <h2>
                        ข้อมูลการยืนยันแผน(ผู้ขนส่ง)
                    </h2>
                </td>
            </tr>
            <tr>
                <td style="width: 10%; background-color: AppWorkspace">
                    ช่วงวันที่
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dteStart_CONFPLNT" runat="server" ClientInstanceName="dteStart_CONFPLNT" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ระหว่างวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                    -
                    <dx:ASPxDateEdit ID="dteEnd_CONFPLNT" runat="server" ClientInstanceName="dteEnd_CONFPLNT" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ถึงวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <dx:ASPxButton ID="btnSearch_CONFPLNT" runat="server" ClientInstanceName="btnSearch_CONFPLNT" Text=" ค้นหารายการ "
                        OnClick="btnSearch_CONFPLNT_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="float: left">
                        <dx:ASPxButton ID="btnXlsExport_CONFPLNT" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                            OnClick="btnXlsExport_CONFPLNT_Click">
                            <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                            </Image>
                        </dx:ASPxButton>
                    </div>
                </td>
                <td>
                </td>
            </tr>
            <tr id="tr2" runat="server">
                <td colspan="2">
                    <dx:ASPxGridView ID="gvw_CONFPLNT" runat="server" SkinID="_gvw" 
                        DataSourceID="sds_CONFPLNT" >
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="ที่" FieldName="LINE_NO" ShowInCustomizationForm="True"
                                Width="3%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="SVENDORID" FieldName="SVENDORID" ShowInCustomizationForm="True"
                                Width="20%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="SVENDORNAME" FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                Width="10%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="DPLAN" FieldName="DPLAN" ShowInCustomizationForm="True"
                                Width="3%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="SCONTRACTNO" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                Width="19%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="STERMINALID" FieldName="STERMINALID" ShowInCustomizationForm="True"
                                Width="18%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="PLANTNAME" FieldName="PLANTNAME"
                                ShowInCustomizationForm="True" Width="22%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="IS_CONFIRM_PLAN" FieldName="IS_CONFIRM_PLAN"
                                ShowInCustomizationForm="True" Width="22%">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="sds_CONFPLNT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="sds_CONFPLNT"
                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"  
                        SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY PSCD.DDELIVERY ,VEND.SABBREVIATION) LINE_NO ,VEND.SVENDORID ,VEND.SABBREVIATION SVENDORNAME ,PSCD.DDELIVERY DPLAN ,CONT.SCONTRACTNO

 ,PSCD.STERMINALID ,PLNT.SABBREVIATION PLANTNAME
,NVL(PSCD.CCONFIRM,'') IS_CONFIRM_PLAN 
FROM TVENDOR VEND
LEFT JOIN TPLANSCHEDULE PSCD ON VEND.SVENDORID=PSCD.SVENDORID
LEFT JOIN TTRUCK TRCK ON PSCD.SHEADREGISTERNO=TRCK.SHEADREGISTERNO
LEFT JOIN TCONTRACT_TRUCK CONTTRCK ON TRCK.STRUCKID=CONTTRCK.STRUCKID
LEFT JOIN TCONTRACT CONT ON CONTTRCK.SCONTRACTID=CONT.SCONTRACTID
LEFT JOIN TTERMINAL PLNT ON PSCD.STERMINALID=PLNT.STERMINALID
WHERE PSCD.CACTIVE = '1' AND PSCD.DDELIVERY &gt;= TO_DATE( :D_START ,'DD/MM/YYYY') AND PSCD.DDELIVERY &lt;= TO_DATE( :D_END ,'DD/MM/YYYY')
ORDER BY PSCD.DDELIVERY ,VEND.SABBREVIATION
">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="dteStart_CONFPLNT" Name=":D_START" PropertyName="Value" />
                            <asp:ControlParameter ControlID="dteEnd_CONFPLNT" Name=":D_END" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
        <dx:ASPxGridViewExporter ID="grid_CONFPLNTExporter" runat="server" GridViewID="gvw_CONFPLNT" PageHeader-Font-Names="Tahoma"
            PageHeader-Font-Bold="true">
            <Styles>
                <Header Font-Names="Tahoma" Font-Size="10">
                </Header>
                <Cell Font-Names="Tahoma" Font-Size="8">
                </Cell>
            </Styles>
            <PageHeader>
                <Font Bold="True" Names="Tahoma"></Font>
            </PageHeader>
        </dx:ASPxGridViewExporter>
    </div>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
    </dx:ASPxLoadingPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
