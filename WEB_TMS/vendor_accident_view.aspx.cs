﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Text;
using System.Configuration;
using DevExpress.Web.ASPxGridView;

public partial class vendor_accident_view : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/Accident/{0}/{2}/{1}/";

    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            var dt1 = new List<DT1>();
            Session["DT1"] = dt1;

            bool boolchk = false;

            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            Session["addSACCIDENTID"] = null;
            if (Session["oSACCIDENTID"] != null)
            {
                Session["addSACCIDENTID"] = Session["oSACCIDENTID"];
                BindData();
                boolchk = true;
            }

            PageControl.TabPages[1].ClientEnabled = boolchk;

            dteDateAccident.Value = DateTime.Now;
            dteDateAccident.MaxDate = DateTime.Today.AddDays(-1);

            dteBEGINWORK.Value = DateTime.Now;
            dteBEGINWORK.MaxDate = DateTime.Today.AddDays(-1);

            dteLASTWORK.Value = DateTime.Now;
            dteLASTWORK.MaxDate = DateTime.Today.AddDays(-1);

        }
    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRUCKID,SCONTRACTID,SCONTRACTNO,SVENDORID,SVENDORNAME
        FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,
        c.SCONTRACTNO,c.SVENDORID,vs.SVENDORNAME FROM ((TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) 
        INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID) LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID
        WHERE t.SHEADREGISTERNO LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

    }

    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboTrailerRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.STRAILERREGISTERNO) AS RN , 
        T.STRAILERREGISTERNO FROM (TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID 
        WHERE T.STRAILERREGISTERNO LIKE :fillter AND c.SVENDORID LIKE :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", txtVendorID.Text));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }

    protected void cboTrailerRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void sgvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
      
    }

    private void BindData()
    {
        DataTable dt = new DataTable();
        string strsql = @"SELECT SACCIDENTID,DACCIDENT,TACCIDENTTIME,DDATENOTIFY,SCITIZENID,SEMPLOYEEID,SHEADREGISTERNO,STRAILERREGISTERNO,STRUCKID
,SACCIDENTLOCALE,CTRANSPORTPROCESS,CEVENT,SEVENTOTHER,CNOLITIGANT,SLITIGANT,CFAULT,CVALUE,NVALUE,NDAMAGEPRODUCT,CVALUETERMINAL,NVALUETERMINAL,CPRODUCT,SPRODUCT
,NAMOUNT,CENVIRONMENT,CWATER,CCOMMUNITY,CFARM,CFIRE,CVEHICLEFIRE,CCOMMUNITYFIRE,CDAMAGEPRODUCT,CCORPORATE,CMEDIACOUNTRY,CMEDIACOMMUNITY,SCONTRACTID,SVENDORID,CINSPECTIONHISTORY
,CMAINTENANCEHISTORY,DLASTWORK,TLASTWORK,DBEGINWORK,TBEGINWORK,SSTART,SEND,NSTARTTODESTINATION,NSTARTTOACCIDENT,NDRIVEHOUR,CPARK,TPARKTIME,NPARKTOACCIDENT
,CAFTERONEHOUR,NBEFOREJOB,NALCOHOLACCIDENT,CDRUGACCIDENT,CSIGHTCHECK,SSIGHTCHECK,CDISEASECHECK,SDISEASECHECK,CILLNESS,CMEDICINE,SMEDICINE,CDDC,SNOTDDC
,NEXPERIENCEYEAR,NEXPERIENCEMONTH,CUSEPHONE,SUSEPHONE,CSPEEDOVERLIMIT,NSPEEDOVERLIMIT,CURGENT,SURGENT,CWARNINGSIGN,SWARNINGSIGN,SACCIDENTHISTORY1,SACCIDENTHISTORY2
,SACCIDENTHISTORY3,CPLACE,CLIGHTING,SLIGHTING,CRAIN,SRAIN,CROUTINE,CPERSONAL,NORGANIZATIONEFFECT,NDRIVEREFFECT,NVALUEEFFECT,NENVIRONMENTEFFECT,NFORECASTDAMAGE,NPENALTY,
SBEGINHUMAN,SCOMMENT,SDETAILWRONGCONTRACT,SMANAGEANDEDIT,SPROTECTION
,CUSE,NSCORESUBTRACT,CDELIVERY,CRETURNSCORE,SRETURNSCOREREMARK,CSENDBY FROM TACCIDENT WHERE SACCIDENTID = '" + Session["oSACCIDENTID"] + "'";

        dt = CommonFunction.Get_Data(sql, strsql);
        if (dt.Rows.Count > 0)
        {
            txtDate.Text = dt.Rows[0]["DDATENOTIFY"] + "";
            txtACCIDENTHISTORY1.Text = dt.Rows[0]["SACCIDENTHISTORY1"] + "";
            txtACCIDENTHISTORY2.Text = dt.Rows[0]["SACCIDENTHISTORY2"] + "";
            txtACCIDENTHISTORY3.Text = dt.Rows[0]["SACCIDENTHISTORY3"] + "";
            txtACCIDENTLOCALE.Text = dt.Rows[0]["SACCIDENTLOCALE"] + "";
            txtALCOHOLACCIDENT.Text = dt.Rows[0]["NALCOHOLACCIDENT"] + "";
            txtAMOUNT.Text = dt.Rows[0]["NAMOUNT"] + "";
            txtBEFOREJOB.Text = dt.Rows[0]["NBEFOREJOB"] + "";
            txtContractID.Text = dt.Rows[0]["SCONTRACTID"] + "";
            txtDAMAGEPRODUCT.Text = dt.Rows[0]["NDAMAGEPRODUCT"] + "";
            txtDISEASECHECK.Text = dt.Rows[0]["CDISEASECHECK"] + "";
            txtDRIVEHOUR.Text = dt.Rows[0]["NDRIVEHOUR"] + "";
            txtEmployeeID.Text = dt.Rows[0]["SEMPLOYEEID"] + "";
            txtEND.Text = dt.Rows[0]["SEND"] + "";
            txtEVENTOTHER.Text = dt.Rows[0]["SEVENTOTHER"] + "";
            txtEXPERIENCEMONTH.Text = dt.Rows[0]["NEXPERIENCEMONTH"] + "";
            txtEXPERIENCEYEAR.Text = dt.Rows[0]["NEXPERIENCEYEAR"] + "";
            txtLIGHTING.Text = dt.Rows[0]["SLIGHTING"] + "";
            txtLITIGANT.Text = dt.Rows[0]["SLITIGANT"] + "";
            txtNOTDDC.Text = dt.Rows[0]["SNOTDDC"] + "";
            txtPARKTIME.Text = dt.Rows[0]["TPARKTIME"] + "";
            txtPARKTOACCIDENT.Text = dt.Rows[0]["NPARKTOACCIDENT"] + "";
            txtPRODUCT.Text = dt.Rows[0]["SPRODUCT"] + "";
            txtRAIN.Text = dt.Rows[0]["SRAIN"] + "";
            txtSIGHTCHECK.Text = dt.Rows[0]["SSIGHTCHECK"] + "";
            txtSPEEDOVERLIMIT.Text = dt.Rows[0]["NSPEEDOVERLIMIT"] + "";
            txtSTART.Text = dt.Rows[0]["SSTART"] + "";
            txtSTARTTOACCIDENT.Text = dt.Rows[0]["NSTARTTOACCIDENT"] + "";
            txtSTARTTODESTINATION.Text = dt.Rows[0]["NSTARTTODESTINATION"] + "";
            txtTruckID.Text = dt.Rows[0]["STRUCKID"] + "";
            txtURGENT.Text = dt.Rows[0]["SURGENT"] + "";
            txtUSEPHONE.Text = dt.Rows[0]["SUSEPHONE"] + "";
            txtVALUE.Text = dt.Rows[0]["NVALUE"] + "";
            txtVALUETERMINAL.Text = dt.Rows[0]["NVALUETERMINAL"] + "";
            txtVendorID.Text = dt.Rows[0]["SVENDORID"] + "";
            txtWARNINGSIGN.Text = dt.Rows[0]["SWARNINGSIGN"] + "";

            chkCOMMUNITY.Checked = ("" + dt.Rows[0]["CMEDIACOMMUNITY"] == "1") ? true : false;
            chkCOMMUNITYFIRE.Checked = ("" + dt.Rows[0]["CCOMMUNITYFIRE"] == "1") ? true : false;
            chkCORPORATE.Checked = ("" + dt.Rows[0]["CCORPORATE"] == "1") ? true : false;
            chkDAMAGEPRODUCT.Checked = ("" + dt.Rows[0]["CDAMAGEPRODUCT"] == "1") ? true : false;
            chkENVIRONMENT.Checked = ("" + dt.Rows[0]["CENVIRONMENT"] == "1") ? true : false;
            chkFARM.Checked = ("" + dt.Rows[0]["CFARM"] == "1") ? true : false;
            chkFIRE.Checked = ("" + dt.Rows[0]["CFIRE"] == "1") ? true : false;
            chkMEDIACOMMUNITY.Checked = ("" + dt.Rows[0]["CMEDIACOMMUNITY"] == "1") ? true : false;
            chkMEDIACOUNTRY.Checked = ("" + dt.Rows[0]["CMEDIACOUNTRY"] == "1") ? true : false;
            chkPersonal.Checked = ("" + dt.Rows[0]["CPERSONAL"] == "1") ? true : false;
            chkPRODUCT.Checked = ("" + dt.Rows[0]["CPRODUCT"] == "1") ? true : false;
            chkVALUE.Checked = ("" + dt.Rows[0]["CVALUE"] == "1") ? true : false;
            chkVALUETERMINAL.Checked = ("" + dt.Rows[0]["CVALUETERMINAL"] == "1") ? true : false;
            chkVEHICLEFIRE.Checked = ("" + dt.Rows[0]["CVEHICLEFIRE"] == "1") ? true : false;
            chkWATER.Checked = ("" + dt.Rows[0]["CWATER"] == "1") ? true : false;

            rblAFTERONEHOUR.Value = dt.Rows[0]["CAFTERONEHOUR"] + "";
            rblDDC.Value = dt.Rows[0]["CDDC"] + "";
            rblDISEASECHECK.Value = dt.Rows[0]["CDISEASECHECK"] + "";
            rblDRUGACCIDENT.Value = dt.Rows[0]["CDRUGACCIDENT"] + "";
            rblEVENT.Value = dt.Rows[0]["CEVENT"] + "";
            rblFAULT.Value = dt.Rows[0]["CFAULT"] + "";
            rblILLNESS.Value = dt.Rows[0]["CILLNESS"] + "";
            rblINSPECTIONHISTORY.Value = dt.Rows[0]["CINSPECTIONHISTORY"] + "";
            rblLIGHTING.Value = dt.Rows[0]["CLIGHTING"] + "";
            rblLITIGANT.Value = dt.Rows[0]["CNOLITIGANT"] + "";
            rblMAINTENANCEHISTORY.Value = dt.Rows[0]["CMAINTENANCEHISTORY"] + "";
            rblMEDICINE.Value = dt.Rows[0]["CMEDICINE"] + "";
            rblPARK.Value = dt.Rows[0]["CPARK"] + "";
            rblPLACE.Value = dt.Rows[0]["CPLACE"] + "";
            rblRAIN.Value = dt.Rows[0]["CRAIN"] + "";
            rblROUTINE.Value = dt.Rows[0]["CROUTINE"] + "";
            rblSIGHTCHECK.Value = dt.Rows[0]["CSIGHTCHECK"] + "";
            rblSPEEDOVERLIMIT.Value = dt.Rows[0]["CSPEEDOVERLIMIT"] + "";
            rblTRANSPORTPROCESS.Value = dt.Rows[0]["CTRANSPORTPROCESS"] + "";
            rblURGENT.Value = dt.Rows[0]["CURGENT"] + "";
            rblUSEPHONE.Value = dt.Rows[0]["CUSEPHONE"] + "";
            rblWARNINGSIGN.Value = dt.Rows[0]["CWARNINGSIGN"] + "";

            DateTime date;
            dteBEGINWORK.Value = DateTime.TryParse(dt.Rows[0]["DBEGINWORK"] + "", out date) ? date : DateTime.Now;
            dteDateAccident.Value = DateTime.TryParse(dt.Rows[0]["DACCIDENT"] + "", out date) ? date : DateTime.Now;
            dteLASTWORK.Value = DateTime.TryParse(dt.Rows[0]["DLASTWORK"] + "", out date) ? date : DateTime.Now;

            tedBEGINWORK.Text = dt.Rows[0]["TBEGINWORK"] + "";
            tedLASTWORK.Text = dt.Rows[0]["TLASTWORK"] + "";
            tedMEDICINE.Text = dt.Rows[0]["SMEDICINE"] + "";

            cmbPersonalNo.Value = dt.Rows[0]["SCITIZENID"] + "";
            cboHeadRegist.Value = dt.Rows[0]["SHEADREGISTERNO"] + "";
            cboTrailerRegist.Value = dt.Rows[0]["STRAILERREGISTERNO"] + "";



            cbxTruePK.Value = dt.Rows[0]["CFAULT"] + "";
            cmbOrganizationEffect.Value = dt.Rows[0]["NORGANIZATIONEFFECT"] + "";
            cmbDriverEffect.Value = dt.Rows[0]["NDRIVEREFFECT"] + "";
            cmbValueEffect.Value = dt.Rows[0]["NVALUEEFFECT"] + "";
            cmbEnvironmentEffect.Value = dt.Rows[0]["NENVIRONMENTEFFECT"] + "";
            txtForecastDamage.Text = dt.Rows[0]["NFORECASTDAMAGE"] + "";
            txtPenalty.Text = dt.Rows[0]["NPENALTY"] + "";
            txtBeginHuman.Text = dt.Rows[0]["SBEGINHUMAN"] + "";
            txtComment.Text = dt.Rows[0]["SCOMMENT"] + "";
            txtDetailWrongContract.Text = dt.Rows[0]["SDETAILWRONGCONTRACT"] + "";
            txtManageAndEdit.Text = dt.Rows[0]["SMANAGEANDEDIT"] + "";
            txtProtection.Text = dt.Rows[0]["SPROTECTION"] + "";
            rblUse.Value = dt.Rows[0]["CUSE"] + "";
            txtScoreSubtract.Text = dt.Rows[0]["NSCORESUBTRACT"] + "";
            rblDelivery.Value = dt.Rows[0]["CDELIVERY"] + "";

            chkReturnScore.Checked = ("" + dt.Rows[0]["CRETURNSCORE"] == "1") ? true : false;
            txtReturnScoreRemark.Text = dt.Rows[0]["SRETURNSCOREREMARK"] + "";

            rblStatus.Value = Session["sendStatus"] + "";

            if ((dt.Rows[0]["CSENDBY"] + "").Contains("1"))
            {
                rblDelivery.Value = "1";
                rblDelivery.ClientEnabled = false;
            }


            strsql = "SELECT * FROM TPERSONALACCIDENT WHERE SACCIDENTID = '" + Session["oSACCIDENTID"] + "'";
            DataTable dt1 = CommonFunction.Get_Data(sql, strsql);

            int num = 0;

            var ddd = new List<DT1>();
            if (dt1.Rows.Count > 0)
            {
                foreach (DataRow dr in dt1.Rows)
                {
                    ddd.Add(new DT1
                    {
                        dtsID = int.TryParse(dr["ID"] + "", out num) ? num : 0,
                        dtsGroup = dr["SGROUP"] + "",
                        dtsType = dr["STYPE"] + "",
                        dtsName = dr["SNAME"] + ""
                    });
                }

                sgvw.DataSource = ddd;
                sgvw.DataBind();
            }
            Session["DT1"] = ddd;


            DataTable dtFile = CommonFunction.Get_Data(sql, "SELECT NID, SFILENAME,SFILEPATH FROM TACCIDENTIMPORTFILE WHERE SACCIDENTID ='" + Session["oSACCIDENTID"] + "'");
            foreach (DataRow dr in dtFile.Rows)
            {
                switch ("" + dr["NID"])
                {
                    case "1":
                        txtFileName0.Text = dr["SFILENAME"] + "";
                        txtFilePath0.Text = dr["SFILEPATH"] + "";
                        break;
                    case "2":
                        txtFileName1.Text = dr["SFILENAME"] + "";
                        txtFilePath1.Text = dr["SFILEPATH"] + "";
                        break;
                    case "3":
                        txtFileName2.Text = dr["SFILENAME"] + "";
                        txtFilePath2.Text = dr["SFILEPATH"] + "";
                        break;
                    case "4":
                        txtFileName3.Text = dr["SFILENAME"] + "";
                        txtFilePath3.Text = dr["SFILEPATH"] + "";
                        break;
                    case "5":
                        txtFileName4.Text = dr["SFILENAME"] + "";
                        txtFilePath4.Text = dr["SFILEPATH"] + "";
                        break;
                    case "6":
                        txtFileName5.Text = dr["SFILENAME"] + "";
                        txtFilePath5.Text = dr["SFILEPATH"] + "";
                        break;
                    case "7":
                        txtFileName6.Text = dr["SFILENAME"] + "";
                        txtFilePath6.Text = dr["SFILEPATH"] + "";
                        break;
                    case "8":
                        txtFileName7.Text = dr["SFILENAME"] + "";
                        txtFilePath7.Text = dr["SFILEPATH"] + "";
                        break;
                    case "9":
                        txtFileName8.Text = dr["SFILENAME"] + "";
                        txtFilePath8.Text = dr["SFILEPATH"] + "";
                        break;
                }


            }

            VisibleControlUpload();
        }
    }

    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID, SPERSONELNO,FULLNAME FROM (SELECT E.SEMPLOYEEID, E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND  E.SPERSONELNO LIKE :fillter AND e.STRANS_ID LIKE :vendorid) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        //sdsPersonal.SelectParameters.Add("vendorid", TypeCode.String, Session["SVDID"] + "");
        sdsPersonal.SelectParameters.Add("vendorid", TypeCode.String, String.Format("%{0}%", txtVendorID.Text));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

  

    private void VisibleControlUpload()
    {
        bool visible = string.IsNullOrEmpty(txtFilePath0.Text);
        txtFileName0.ClientVisible = !visible;
        btnView0.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath1.Text);
        txtFileName1.ClientVisible = !visible;
        btnView1.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath2.Text);
        txtFileName2.ClientVisible = !visible;
        btnView2.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath3.Text);
        txtFileName3.ClientVisible = !visible;
        btnView3.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath4.Text);
        txtFileName4.ClientVisible = !visible;
        btnView4.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath5.Text);
        txtFileName5.ClientVisible = !visible;
        btnView5.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath6.Text);
        txtFileName6.ClientVisible = !visible;
        btnView6.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath7.Text);
        txtFileName7.ClientVisible = !visible;
        btnView7.ClientVisible = !visible;

        visible = string.IsNullOrEmpty(txtFilePath8.Text);
        txtFileName8.ClientVisible = !visible;
        btnView8.ClientVisible = !visible;
    }


    [Serializable]
    struct DT1
    {
        public int dtsID { get; set; }
        public string dtsGroup { get; set; }
        public string dtsType { get; set; }
        public string dtsName { get; set; }
    }

}