﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="accident_check.aspx.cs" Inherits="accident_check" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function VisibleControl() {

            var bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
            uploader0.SetClientVisible(bool);
            txtFileName0.SetClientVisible(!bool);
            btnView0.SetEnabled(!bool);
            btnDelFile0.SetEnabled(!bool);

            bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
            uploader1.SetClientVisible(bool);
            txtFileName1.SetClientVisible(!bool);
            btnView1.SetEnabled(!bool);
            btnDelFile1.SetEnabled(!bool);

            bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
            uploader2.SetClientVisible(bool);
            txtFileName2.SetClientVisible(!bool);
            btnView2.SetEnabled(!bool);
            btnDelFile2.SetEnabled(!bool);

            bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
            uploader3.SetClientVisible(bool);
            txtFileName3.SetClientVisible(!bool);
            btnView3.SetEnabled(!bool);
            btnDelFile3.SetEnabled(!bool);

            bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
            uploader4.SetClientVisible(bool);
            txtFileName4.SetClientVisible(!bool);
            btnView4.SetEnabled(!bool);
            btnDelFile4.SetEnabled(!bool);

            bool = txtFilePath5.GetValue() == "" || txtFilePath5.GetValue() == null;
            uploader5.SetClientVisible(bool);
            txtFileName5.SetClientVisible(!bool);
            btnView5.SetEnabled(!bool);
            btnDelFile5.SetEnabled(!bool);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <dx:ASPxPageControl ID="ASPxPageControl1" ClientInstanceName="PageControl" runat="server"
                    Width="100%" ActiveTabIndex="0">
                    <ClientSideEvents TabClick="function (s,e){if(!ASPxClientEdit.ValidateGroup('add')) e.cancel = true }" />
                    <TabPages>
                        <dx:TabPage Name="t1" Text="ข้อมูลทั่วไป">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td class="style28">
                                                <span style="text-align: left">ทะเบียนรถ(หัว) <font color="#ff0000">*</font></span>
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cboHeadRegist" runat="server" CallbackPageSize="30" ClientEnabled="false"
                                                    ClientInstanceName="cboHeadRegist" ItemStyle-Wrap="True" EnableCallbackMode="True"
                                                    OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="180px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));txtVendorName.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORNAME'));txtVendorID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORID'));txtContractID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTID'));
                                                    txtContractNO.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTNO'));txtTypeContract.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTTYPENAME'));};cboTrailerRegist.PerformCallback();cboDelivery.PerformCallback();cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'))}" />
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORNAME" Width="140px" />
                                                        <dx:ListBoxColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="รหัสรถ" FieldName="STRUCKID" Width="80px" />
                                                        <dx:ListBoxColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" Width="60px" />
                                                        <dx:ListBoxColumn Caption="ประเภทสัญญา" FieldName="SCONTRACTTYPENAME" Width="80px" />
                                                        <dx:ListBoxColumn Caption="รหัสผู้ประกอบการ" FieldName="SVENDORID" Width="100px" />
                                                    </Columns>
                                                    <ItemStyle Wrap="True"></ItemStyle>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                <span style="text-align: left">ทะเบียนรถ (ท้าย)</span>
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxComboBox ID="cboTrailerRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboTrailerRegist"
                                                    ClientEnabled="false" EnableCallbackMode="True" OnItemRequestedByValue="cboTrailerRegist_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboTrailerRegist_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="STRAILERREGISTERNO" Width="180px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <dx:ASPxTextBox ID="txtTruckID" ClientInstanceName="txtTruckID" runat="server" Width="180px"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 15%">
                                                Outbound No.
                                            </td>
                                            <td style="width: 35%">
                                                <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" MaxLength="10"
                                                    ClientEnabled="false" ClientInstanceName="cboDelivery" EnableCallbackMode="True"
                                                    OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SDELIVERYNO" Width="180px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtOilValue.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('NVALUE')); txtShipment.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHIPMENT_NO'));}else{txtShipment.SetValue('');txtOilValue.SetValue('')};}" />
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="Outbound No." FieldName="SDELIVERYNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ปริมาณน้ำมัน" FieldName="NVALUE" Width="100px" />
                                                        <dx:ListBoxColumn Caption="Shipment No." FieldName="SHIPMENT_NO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="รหัสผู้ประกอบการ" FieldName="SVENDORID" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ทะเบียนรถ(หัว)" FieldName="SHEADREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ทะเบียนรถ(ท้าย)" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 15%;">
                                                Shipment No.
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 35%;">
                                                <dx:ASPxTextBox ID="txtShipment" runat="server" ClientEnabled="False" ClientInstanceName="txtShipment"
                                                    Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                ชื่อผู้ขนส่ง
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxTextBox ID="txtVendorName" runat="server" ClientEnabled="False" ClientInstanceName="txtVendorName"
                                                    Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                รหัสบริษัท
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="txtVendorID" ClientInstanceName="txtVendorID" runat="server"
                                                    Width="180px" ClientEnabled="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                เลขที่สัญญา
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxTextBox ID="txtContractNO" runat="server" ClientEnabled="False" ClientInstanceName="txtContractNO"
                                                    Width="180px">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtContractID" runat="server" ClientEnabled="False" ClientVisible="false"
                                                    ClientInstanceName="txtContractID" Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ประเภทสัญญา
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="txtTypeContract" ClientInstanceName="txtTypeContract" runat="server"
                                                    Width="180px" ClientEnabled="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                ประเภทบรรทุก
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxRadioButtonList ID="rblTypeProduct" runat="server" ClientEnabled="false"
                                                    RepeatDirection="Horizontal" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="น้ำมัน" Value="O" Selected="true" />
                                                        <dx:ListEditItem Text="ก๊าซ" Value="G" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ปริมาณน้ำมัน
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="txtOilValue" ClientInstanceName="txtOilValue" runat="server"
                                                    Width="180px" ClientEnabled="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ใบกำกับการขนส่ง
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="txtTransportNO" runat="server" Width="180px" ClientEnabled="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                รายละเอียดการขนส่ง
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtTransportDetail" runat="server" Width="180px" ClientEnabled="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                ผู้บันทึกข้อมูล
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxTextBox ID="txtUserCreate" runat="server" Width="180px" ClientEnabled="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton4" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t2" Text="เหตุการณ์และสาเหตุ">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 25%">
                                                วันที่-เวลาเกิดเหตุ <font color="#ff0000">*</font>
                                            </td>
                                            <td style="width: 30%">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteDateAccident" runat="server" SkinID="xdte" ClientEnabled="false">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td>
                                                            เวลา
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTimeEdit ID="tdeAcccidentTime" runat="server" ClientEnabled="false" Width="60px">
                                                            </dx:ASPxTimeEdit>
                                                        </td>
                                                        <td>
                                                            น.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 25%;">
                                                วันที่-เวลาแจ้งเหตุ <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteDateNotify" runat="server" SkinID="xdte" ClientEnabled="false">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุเวลาแจ้งเหตุ" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td>
                                                            เวลา
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTimeEdit ID="dteTimeNotify" runat="server" ClientEnabled="false" Width="60px">
                                                            </dx:ASPxTimeEdit>
                                                        </td>
                                                        <td>
                                                            น.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                บัตรประชาชนพขร. ขณะเกิดเหตุ <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="30" ClientEnabled="false"
                                                    ClientInstanceName="cmbPersonalNo" EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SPERSONELNO" Width="150px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {txtEmployee.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('FULLNAME'));txtEmployeeID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SEMPLOYEEID'));}" />
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="หมายเลขบัตรประชาชน" FieldName="SPERSONELNO" />
                                                        <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                                        <dx:ListBoxColumn Caption="รหัสพนักงาน" FieldName="SEMPLOYEEID" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุรหัสบัตรประชาชน" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ชื่อ พขร. <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="txtEmployee" runat="server" ClientEnabled="false" Width="250px"
                                                    ClientInstanceName="txtEmployee">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุชื่อพนักงาน" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtEmployeeID" runat="server" ClientInstanceName="txtEmployeeID"
                                                    Width="10px" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                คู่กรณี <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style27" colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtLitigant" runat="server" ClientEnabled="false" Width="170px">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="ChkAdd">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุคู่กรณี" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxCheckBox ID="chkNOLitigant" runat="server" ClientEnabled="false" ClientInstanceName="chkNOLitigant"
                                                                CheckState="Unchecked" Text="ไม่มีคู่กรณี">
                                                            </dx:ASPxCheckBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                เรื่อง <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cmbTopic" runat="server" Width="170px" SelectedIndex="0" ClientEnabled="false"
                                                    DataSourceID="sdsTopicP2" ValueField="NID" TextField="SDETAIL" ValueType="System.String">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTopicP2" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'A_2_1'">
                                                </asp:SqlDataSource>
                                                <%-- <dx:ASPxTextBox ID="txtTopic" runat="server" Width="250px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุเรื่อง" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>--%>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ประเภทความสูญเสีย <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style18">
                                                <%--  <dx:ASPxTextBox ID="txtTypeAccident" runat="server" Width="250px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุประเภทความสูญเสีย" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>--%>
                                                <dx:ASPxComboBox ID="cmbTypeAccident" runat="server" Width="170px" ClientEnabled="false"
                                                    SelectedIndex="0" DataSourceID="sdsTypeAccident" ValueField="NID" TextField="SDETAIL"
                                                    ValueType="System.String">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTypeAccident" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'A_2_3'">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                สถานที่เกิดเหตุ <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxMemo ID="txtAddress" runat="server" ClientEnabled="false" Height="100px"
                                                    Width="250px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุสถานที่เกิดเหตุ" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ประเภทเหตุการณ์<font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style18">
                                                <%--<dx:ASPxMemo ID="txtTypeEvent" runat="server" Height="100px" Width="250px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุประเภทเหตุการณ์" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxMemo>--%>
                                                <dx:ASPxComboBox ID="cmbTypeEvent" ClientEnabled="false" runat="server" Width="170px"
                                                    SelectedIndex="0" DataSourceID="sdsTypeEvent" ValueField="NID" TextField="SDETAIL"
                                                    ValueType="System.String">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTypeEvent" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'A_2_4'">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                ประเภทของงานที่ปฏิบัติ<br />
                                                ขณะเกิดเหตุ
                                            </td>
                                            <td align="left" class="style27">
                                                <%-- <dx:ASPxMemo ID="txtTypeOperate" runat="server" Height="100px" Width="250px">
                                                </dx:ASPxMemo>--%>
                                                <dx:ASPxComboBox ID="cmbTypeOperate" ClientEnabled="false" runat="server" Width="170px"
                                                    SelectedIndex="0" DataSourceID="sdsTypeOperate" ValueField="NID" TextField="SDETAIL"
                                                    ValueType="System.String">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTypeOperate" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'A_2_2'">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                <span style="text-align: left">รายละเอียดเหตุการณ์</span>&nbsp;<font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxMemo ID="txtDetail" runat="server" ClientEnabled="false" Height="100px" Width="250px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุรายละเอียดเหตุการณ์" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                การดำเนินการเบื้องต้น
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxMemo ID="txtPrimaryOperate" ClientEnabled="false" runat="server" Height="100px"
                                                    Width="250px">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ผู้ที่เกี่ยวข้อง / ผู้ประสบเหตุ&nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxMemo ID="txtAccomplice" ClientEnabled="false" runat="server" Height="100px"
                                                    Width="250px">
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                สาเหตุขณะนั้น
                                            </td>
                                            <td align="left" class="style27" colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cbxCause" runat="server" ClientEnabled="false" Width="170px"
                                                                SelectedIndex="0" DataSourceID="sdsCause" ValueField="NACCIDENTCAUSEID" TextField="SACCIDENTCAUSENAME">
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsCause" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NACCIDENTCAUSEID, SACCIDENTCAUSENAME FROM LSTACCIDENTCAUSE ORDER BY NACCIDENTCAUSEID DESC">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                        <td>
                                                            ระบุ
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtCauseOther" ClientEnabled="false" runat="server" Width="230px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                การกระทำที่ต่ำกว่ามาตรฐานเรื่อง
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxTextBox ID="txtLowStandard" ClientEnabled="false" runat="server" Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28" colspan="3">
                                                <b>เอกสารที่ต้องแนบการพิจารณา</b> Allowed file types:
                                                <%= Resources.CommonResource.FileUploadType.Replace("."," ").Substring(0,30) %>ฯลฯ<br>
                                                Max file size:
                                                <%= Resources.CommonResource.TooltipMaxFileSize1MB %>
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="30%">
                                                            1) บันทึกแจ้งความ หรือใบเครมประกัน
                                                        </td>
                                                        <td width="35%">
                                                            <dx:ASPxUploadControl ID="uplExcel0" runat="server" ClientInstanceName="uploader0"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader0.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png,.zip,.rar หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath0.SetValue((e.callbackData+'').split('|')[0]);txtFileName0.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName0" runat="server" Width="300px" ClientInstanceName="txtFileName0"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath0" runat="server" Width="220px" ClientInstanceName="txtFilePath0"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="35%">
                                                            <dx:ASPxButton ID="btnView0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath0.GetValue());}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath0.GetValue() +';1');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            2) แผนที่เกิดเหตุ และรูปถ่าย (สถานที่เกิดเหตุ<br />/รูปรถที่เกิดเหตุ)
                                                        </td>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel1" runat="server" ClientInstanceName="uploader1"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader1.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath1.SetValue((e.callbackData+'').split('|')[0]);txtFileName1.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName1" runat="server" Width="300px" ClientInstanceName="txtFileName1"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath1" runat="server" Width="220px" ClientInstanceName="txtFilePath1"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath1.GetValue());}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath1.GetValue() +';2');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            3) ผลการตรวจสารเสพติด
                                                        </td>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel2" runat="server" ClientInstanceName="uploader2"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader2.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath2.SetValue((e.callbackData+'').split('|')[0]);txtFileName2.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName2" runat="server" Width="300px" ClientInstanceName="txtFileName2"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath2" runat="server" Width="220px" ClientInstanceName="txtFilePath2"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath2.GetValue());}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath2.GetValue() +';3');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            4) สรุปข้อมูล GPS
                                                        </td>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel3" runat="server" ClientInstanceName="uploader3"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader3.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath3.SetValue((e.callbackData+'').split('|')[0]);txtFileName3.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName3" runat="server" Width="300px" ClientInstanceName="txtFileName3"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath3" runat="server" Width="220px" ClientInstanceName="txtFilePath3"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath2.GetValue());}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath3.GetValue() +';4');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            5) ใบกำกับการขนส่ง
                                                        </td>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel4" runat="server" ClientInstanceName="uploader4"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader4.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath4.SetValue((e.callbackData+'').split('|')[0]);txtFileName4.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName4" runat="server" Width="300px" ClientInstanceName="txtFileName4"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath4" runat="server" Width="220px" ClientInstanceName="txtFilePath4"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath4.GetValue());}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath4.GetValue() +';5');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            6) เอกสารอื่น ๆ (ถ้ามี)
                                                        </td>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel5" runat="server" ClientInstanceName="uploader5"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader5.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath5.SetValue((e.callbackData+'').split('|')[0]);txtFileName5.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName5" runat="server" Width="300px" ClientInstanceName="txtFileName5"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath5" runat="server" Width="220px" ClientInstanceName="txtFilePath5"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView5" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView5" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath5.GetValue());}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile5" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile5" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath5.GetValue() +';6');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton6" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t3" Text="ความเสียหาย">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                ความเสียหายที่เกิดขึ้น <font color="#ff0000">*</font>
                                            </td>
                                            <td colspan="2">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ภาพลักษณ์องค์กร
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                ตัวคูณความรุนแรง<font color="#ff0000">*</font>
                                                <dx:ASPxComboBox ID="cmbOrganizationEffect" runat="server" ClientInstanceName="cmbOrganizationEffect"
                                                    ItemStyle-Wrap="True" Width="100px" TextField="NIMPACTLEVEL" TextFormatString="{0}"
                                                    ValueField="NIMPACTLEVEL" CssClass="dxeLineBreakFix" DataSourceID="sdsCORGANIZATIONEFFECT">
                                                    <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                        <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                    </Columns>
                                                    <ItemStyle Wrap="True"></ItemStyle>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsCORGANIZATIONEFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '10') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style27" colspan="2">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            เสียชีวิต พนักงานขับรถ
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtnDriver" runat="server" Width="60px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            คน, ผู้ประสบเหตุ
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtnVictims" runat="server" Width="80px" RightToLeft="True">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            คน
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" class="style18">
                                                ตัวคูณความรุนแรง<font color="#ff0000">*</font>
                                                <dx:ASPxComboBox ID="cmbDriverEffect" ClientInstanceName="cmbDriverEffect" runat="server"
                                                    ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" TextField="NIMPACTLEVEL" TextFormatString="{0}"
                                                    ValueField="NIMPACTLEVEL" Width="100px" DataSourceID="sdsDRIVEREFFECT">
                                                    <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                        <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                    </Columns>
                                                    <ItemStyle Wrap="True"></ItemStyle>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsDRIVEREFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '07') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td colspan="2" class="style19">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ทรัพย์สิน เป็นมูลค่า
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtnValue" runat="server" Width="100px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            บาท
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;" class="style20">
                                                ตัวคูณความรุนแรง<font color="#ff0000">*</font>
                                                <dx:ASPxComboBox ID="cmbValueEffect" ClientInstanceName="cmbValueEffect" runat="server"
                                                    ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" Width="100px" TextFormatString="{0}"
                                                    TextField="NIMPACTLEVEL" ValueField="NIMPACTLEVEL" DataSourceID="sdsVALUEEFFECT">
                                                    <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                        <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                    </Columns>
                                                    <ItemStyle Wrap="True"></ItemStyle>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsVALUEEFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '08') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td colspan="2">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            สิ่งแวดล้อม
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                ตัวคูณความรุนแรง<font color="#ff0000">*</font>
                                                <dx:ASPxComboBox ID="cmbEnvironmentEffect" ClientInstanceName="cmbEnvironmentEffect"
                                                    ItemStyle-Wrap="True" runat="server" CssClass="dxeLineBreakFix" TextFormatString="{0}"
                                                    TextField="NIMPACTLEVEL" ValueField="NIMPACTLEVEL" Width="100px" DataSourceID="sdsENVIRONEMTNEFFECT">
                                                    <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                        <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                    </Columns>
                                                    <ItemStyle Wrap="True"></ItemStyle>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsENVIRONEMTNEFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '09') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ฝ่ายถูก / ผิด <font color="#ff0000">*</font>
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cbxTruePK" runat="server" SelectedIndex="0" Width="80px">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="ฝ่ายถูก" Value="0" />
                                                        <dx:ListEditItem Text="ฝ่ายผิด" Value="1" />
                                                        <dx:ListEditItem Text="ประมาทร่วม" Value="2" />
                                                    </Items>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            หักคะแนน
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtScoreSubtract" runat="server" ClientEnabled="False" Width="50px">
                                                                <Border BorderColor="#CCCCCC" />
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            คะแนน
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ประเภทผลิตภัณฑ์ที่ไหลลงสู่สาธารณะ &nbsp
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cmbsProduct" runat="server" Width="80px" DataSourceID="sdsProduct"
                                                                TextField="PROD_ABBR" ValueField="PROD_ID">
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsProduct" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT PROD_ID, PROD_ABBR FROM TPRODUCT"></asp:SqlDataSource>
                                                        </td>
                                                        <td>
                                                            จำนวน
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtnAmount" runat="server" Width="100px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cmbUnit" runat="server" Width="60px" DataSourceID="sdsUnit"
                                                                ValueField="NPRODUCTUNITID" TextField="SPRODUCTUNITNAME">
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsUnit" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NPRODUCTUNITID, SPRODUCTUNITNAME FROM TPRODUCTUNIT"></asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" class="style21">
                                                ประมาณการความเสียหาย&nbsp;
                                            </td>
                                            <td class="style22">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtForecastDamage" runat="server" Width="130px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            บาท
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;" class="style23">
                                                วงเงินปรับ
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;" class="style24">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtPenalty" runat="server" Width="130px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            บาท
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 20%">
                                                ผู้แจ้งเรื่อง
                                            </td>
                                            <td style="width: 30%">
                                                <dx:ASPxTextBox ID="txtBeginHuman" runat="server" Width="200px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 15%;">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 35%;">
                                                <dx:ASPxTextBox ID="txtBeginTerminal" runat="server" Width="200px" Visible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtWrongContractNO" runat="server" Width="100px" Visible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                <dx:ASPxMemo ID="txtContractDetail" runat="server" Height="71px" Width="220px" Visible="false">
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                ความคิดเห็นผู้สอบสวน
                                            </td>
                                            <td>
                                                <dx:ASPxMemo ID="txtComment" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                รายละเอียดการปฏิบัติผิดสัญญา
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                <dx:ASPxMemo ID="txtDetailWrongContract" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                การจัดการและการแก้ไข
                                            </td>
                                            <td>
                                                <dx:ASPxMemo ID="txtManageAndEdit" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                การดำเนินการป้องกัน
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                <dx:ASPxMemo ID="txtProtection" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <dx:ASPxButton ID="ASPxButton7" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save2');}" />
                                                </dx:ASPxButton>
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton8" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t4" Text="บันทึกตัดคะแนน">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                การนำไปใช้ประเมินผล <font color="#ff0000">*</font>
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxRadioButtonList ID="rblUse" runat="server" RepeatDirection="Horizontal" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="ใช้" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="ไม่ใช้" Value="0" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาเลือก" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                            </td>
                                            <td style="width: 40%">
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                สถานะจัดส่งรายงานการเกิดอุบัติเหตุ
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxRadioButtonList ID="rblDelivery" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="จัดส่งแล้ว" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="ไม่จัดส่ง" Value="0" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                สถานะปัจจุบัน <font color="#ff0000">*</font>
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxRadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="ดำเนินการ" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="อุทธรณ์" Value="2" />
                                                        <dx:ListEditItem Text="ปิดเรื่อง" Value="3" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาเลือก" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                <dx:ASPxCheckBox ID="chkReturnScore" runat="server" CheckState="Unchecked" Text="คลิกเลือกเพื่อคืนคะแนน">
                                                </dx:ASPxCheckBox>
                                            </td>
                                            <td style="width: 40%">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                เหตุผลการคืนคะแนน
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxMemo ID="txtReturnScoreRemark" runat="server" Height="71px" Width="300px">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('Save3');}" />
                                                </dx:ASPxButton>
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton2" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" align="left" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
