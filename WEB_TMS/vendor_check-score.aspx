﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_check-score.aspx.cs" Inherits="vendor_checkscore" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo; inEndRequestHandler();}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="15%">
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="เลขที่สัญญา,ทะเบียนรถ">
                            </dx:ASPxTextBox>
                        </td>
                        <td style="width:200px">
                            <%--<dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>--%>
                            <asp:TextBox ID="dteStart" runat="server" CssClass="datepicker" Style="text-align: center; width:200px"></asp:TextBox>
                        </td>
                        <td>
                            -
                        </td>
                        <td style="width:200px">
                            <%--<dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>--%>
                            <asp:TextBox ID="dteEnd" runat="server" CssClass="datepicker" Style="text-align: center; width:200px"></asp:TextBox>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus" Width="110px"
                                SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="- สถานะสัญญา -" />
                                    <dx:ListEditItem Text="อยู่ในระยะสัญญา" Value="1" />
                                    <dx:ListEditItem Text="หมดสัญญา" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8">
                           *H = Hold
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SCONTRACTID" ShowInCustomizationForm="True"
                                        VisibleIndex="1" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                        VisibleIndex="2" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ระยะเวลาสัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                        VisibleIndex="3" Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lbldStart" runat="server" Text='<%# Eval("DBEGIN" ,"{0:dd/MM/yyyy}") %>'>
                                            </dx:ASPxLabel>
                                            -
                                            <dx:ASPxLabel ID="lbldEnd" runat="server" Text='<%# Eval("DEND" ,"{0:dd/MM/yyyy}") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="จำนวนรถ" FieldName="NTRUCK" ShowInCustomizationForm="True"
                                        VisibleIndex="5" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="บทปรับ / บทลงโทษ" ShowInCustomizationForm="True"
                                        VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="จำนวนครั้ง" FieldName="NCOUNT" ShowInCustomizationForm="True"
                                                VisibleIndex="8" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="คะแนน" FieldName="SUMNPOINT" ShowInCustomizationForm="True"
                                                VisibleIndex="9" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ค่าปรับ" FieldName="COST" ShowInCustomizationForm="True"
                                                VisibleIndex="9" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ระงับ พขร. (วัน)" FieldName="DISABLE_DRIVER" ShowInCustomizationForm="True"
                                                VisibleIndex="9" Width="10%" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataDateColumn Caption="ครั้งล่าสุด" FieldName="DCHECK" VisibleIndex="10"
                                                Width="15%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataDateColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataColumn Width="5%" CellStyle-Cursor="hand" VisibleIndex="11">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" AutoPostBack="false"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                <Image Width="16px" Height="16px" Url="Images/search.png">
                                                </Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Templates>
                                    <EditForm>
                                        <table width="100%">
                                            <tr align="center">
                                                <td>
                                                    <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="sgvw"
                                                        DataSourceID="sds1" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px"
                                                        Width="100%" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SettingsBehavior-AllowSort="false">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                                                Width="3%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (หัว)" FieldName="SHEADREGISTERNO"
                                                                ShowInCustomizationForm="True" VisibleIndex="1" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (ท้าย)" FieldName="STRAILERREGISTERNO"
                                                                ShowInCustomizationForm="True" VisibleIndex="2" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="พนักงานขับรถ" FieldName="EMPLOYEEID" ShowInCustomizationForm="True"
                                                                VisibleIndex="3" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="รายการโดนตัดคะแนน" FieldName="STOPICNAME" ShowInCustomizationForm="True"
                                                                VisibleIndex="3" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="คะแนนที่ตัด" FieldName="SUMNPOINT" ShowInCustomizationForm="True"
                                                                VisibleIndex="4" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ค่าปรับ" FieldName="COST" ShowInCustomizationForm="True"
                                                                VisibleIndex="4" Width="8%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ระงับ พขร. (วัน)" FieldName="DISABLE_DRIVER" ShowInCustomizationForm="True"
                                                                VisibleIndex="4" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="BLACKLIST" FieldName="BLACKLIST" ShowInCustomizationForm="True"
                                                                VisibleIndex="4" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataDateColumn Caption="วันที่ตัดคะแนน" FieldName="DREDUCE" VisibleIndex="5"
                                                                Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataTextColumn Caption="สถานะอุทธรณ์" FieldName="STATUS" ShowInCustomizationForm="True"
                                                                VisibleIndex="4" Width="10%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <Settings ShowFooter="True" />
                                                        <TotalSummary>
                                                            <dx:ASPxSummaryItem  FieldName="SUMNPOINT" ShowInColumn="" SummaryType="Sum" DisplayFormat="รวมตัดคะแนน {0}" />
                                                        </TotalSummary>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                       SkinID="_close">
                                                        <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </EditForm>
                                </Templates>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" >
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sds1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY o.NREDUCEID) AS ID1,o.SHEADREGISTERNO,o.STRAILERREGISTERNO,o.EMPLOYEEID,o.STOPICNAME ,o.SUMNPOINT ,TO_CHAR(o.DREDUCE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DREDUCE,o.STATUS,o.SCONTRACTID,o.COST,o.DISABLE_DRIVER,o.BLACKLIST  FROM 
(
SELECT R.NREDUCEID,R.SHEADREGISTERNO,R.STRAILERREGISTERNO,ES.FNAME || ' ' || ES.LNAME AS EMPLOYEEID ,CASE WHEN (R.SPROCESSID='090' OR R.SPROCESSID='080' OR R.SPROCESSID='040') THEN R.SREDUCENAME ELSE Tp.STOPICNAME END STOPICNAME ,- nvl(r.NPOINT,0) AS SUMNPOINT ,r.DREDUCE, 
nvl(r.COST,0) AS COST,
CASE WHEN R.SPROCESSID = '090' AND R.DISABLE_DRIVER = -2 THEN 'H'
     else nvl(r.DISABLE_DRIVER,0) || '' end AS DISABLE_DRIVER,
CASE WHEN R.BLACKLIST = 1 THEN 'Y' ELSE '' END AS BLACKLIST,
CASE WHEN ap.SAPPEALID IS NULL THEN 'ไม่ได้ยื่นอุทธรณ์'  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน (เปลี่ยน)' END  END  END  END END END END END END END As STATUS , 

CASE WHEN R.SPROCESSID = '020' AND r.SCONTRACTID is null THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
FROM ((((TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO /*AND nvl(R.STRAILERREGISTERNO,1) = nvl(T.STRAILERREGISTERNO,1)*/)
LEFT JOIN 
(
SELECT tct.* FROM TCONTRACT_TRUCK tct
LEFT JOIN TCONTRACT ct
ON tct.SCONTRACTID = ct.SCONTRACTID
WHERE NVL(ct.CACTIVE,'Y') = 'Y'
) ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1))LEFT JOIN (SELECT STOPICID,STOPICNAME,SVERSION FROM TTOPIC) tp ON R.STOPICID = Tp.STOPICID AND nvl(R.SVERSIONLIST,'1') = TP.SVERSION ) LEFT JOIN  TEMPLOYEE_SAP es ON r.SDRIVERNO = ES.SEMPLOYEEID)
LEFT JOIN TAPPEAL ap on R.NREDUCEID = ap.NREDUCEID WHERE nvl(R.CACTIVE,'0') = '1'
) o    WHERE TO_DATE(TO_CHAR(o.DREDUCE,'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH'),'DD/MM/YYYY') BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') AND o.SCONTRACTID = :oSSCONTRACTID">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="dteStart" Name="dStart" PropertyName="Text" />
                                    <asp:ControlParameter ControlID="dteEnd" Name="dEnd" PropertyName="Text" />
                                    <asp:SessionParameter Name="oSSCONTRACTID" SessionField="oSSCONTRACTID" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
