﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="transportation-other.aspx.cs" Inherits="transportationother" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" ></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="60%">
                            &nbsp;
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="ชื่อผู้ประกอบการ,เลขทะเบียนรถ">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                           
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }" ></ClientSideEvents>
                                
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="8">
                            จำนวนรายการอุบัติเหตุ&nbsp;<dx:ASPxLabel ID="lblCarCount" runat="server" Text="0"
                                Font-Bold="true" ForeColor="Red">
                            </dx:ASPxLabel>
                            &nbsp;รายการ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="1"
                                        Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="NPROBLEMID" ShowInCustomizationForm="True"
                                        Visible="False" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Outbound No." FieldName="SDELIVERYNO" ShowInCustomizationForm="True"
                                        VisibleIndex="3" Width="12%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ประกอบการ" FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="4" Width="18%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="พนักงานขับรถ" FieldName="FULLNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="5" Width="12%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" ShowInCustomizationForm="True" VisibleIndex="6">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="7" Width="12%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="8" Width="12%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่เกิดเหตุ" FieldName="DPROBLEMDATE" 
                                        VisibleIndex="9" Width="12%" HeaderStyle-HorizontalAlign="Center">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataColumn Caption="สถานะ" FieldName="STATUS" VisibleIndex="10" Width="12%" HeaderStyle-HorizontalAlign="Center">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="11" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="False" CausesValidation="False"
                                                Text="แก้ไข">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY p.NPROBLEMID) AS ID1, p.NPROBLEMID,p.SDELIVERYNO,VS.SVENDORNAME,ES.FNAME || ' ' || ES.LNAME AS FULLNAME,p.SHEADREGISTERNO,p.STRAILERREGISTERNO, p.DPROBLEMDATE, case when p.CSTATUS = '1' then 'ดำเนินการ' else (case when p.CSTATUS = '2' then 'อุทธรณ์' else (case when p.CSTATUS = '3' then 'ปิดเรื่อง' end) end) end AS STATUS FROM (TPROBLEM p LEFT JOIN TVENDOR_SAP vs ON p.SVENDORID = VS.SVENDORID) LEFT JOIN TEMPLOYEE_SAP es ON p.SEMPLOYEEID = ES.SEMPLOYEEID WHERE (p.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR p.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR VS.SVENDORNAME LIKE '%' || :oSearch || '%' ) AND To_Date(p.DPROBLEMDATE) BETWEEN :dStart AND :dEnd ">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtSearch" Name="oSearch" PropertyName="Text" />
                                    <asp:ControlParameter ControlID="dteStart" Name="dStart" PropertyName="Value" />
                                    <asp:ControlParameter ControlID="dteEnd" Name="dEnd" PropertyName="Value" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
