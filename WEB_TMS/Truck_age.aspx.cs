﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using GemBox.Spreadsheet;
using System.IO;
using System.Web.Security;
using System.Globalization;

public partial class Truck_age : PageBase
{
    #region + View State +
    private DataTable dtTruck
    {
        get
        {
            if ((DataTable)ViewState["dtTruck"] != null)
                return (DataTable)ViewState["dtTruck"];
            else
                return null;
        }
        set
        {
            ViewState["dtTruck"] = value;
        }
    }

    private DataTable dtContract
    {
        get
        {
            if ((DataTable)ViewState["dtContract"] != null)
                return (DataTable)ViewState["dtContract"];
            else
                return null;
        }
        set
        {
            ViewState["dtContract"] = value;
        }
    }
    private DataTable dtWorkgroup
    {
        get
        {
            if ((DataTable)ViewState["dtWorkgroup"] != null)
                return (DataTable)ViewState["dtWorkgroup"];
            else
                return null;
        }
        set
        {
            ViewState["dtWorkgroup"] = value;
        }
    }

    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }

    private string SVDID
    {
        get
        {
            if ((string)ViewState["SVDID"] != null)
                return (string)ViewState["SVDID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SVDID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        try
        {
            if (!IsPostBack)
            {
                //txtDateStart.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                //txtDateEnd.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                this.InitialForm();

                CGROUP = Session["CGROUP"].ToString();
                SVDID = Session["SVDID"].ToString();

                if (string.Equals(CGROUP, "0"))
                {
                    ddlVendor.SelectedValue = SVDID;
                    ddlVendor.Enabled = false;

                    this.GetContract(" AND SVENDORID = '" + SVDID + "'");
                }

                if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
                {
                    var decryptedBytes = MachineKey.Decode(Request.QueryString["License"], MachineKeyProtection.All);
                    var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                    string License = decryptedValue;
                    txtLicense.Text = License;
                    cmdSearch_Click(null, null);
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void InitialForm()
    {
        try
        {
            //this.GetContract(string.Empty);
            this.GetVendor();
            this.GetWorkGroup(string.Empty);
            //this.GetTruckType();
            this.GetTruckPermission();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetContract(string Condition)
    {
        try
        {
            dtContract = ComplainBLL.Instance.ContractSelectBLL(Condition);
            DropDownListHelper.BindDropDownList(ref ddlContract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetWorkGroup(string Condition)
    {
        try
        {
            dtWorkgroup = ComplainBLL.Instance.TruckWorkgroupBLL(Condition);
            DropDownListHelper.BindDropDownList(ref ddlWorkGroup, dtWorkgroup, "ID", "NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetVendor()
    {
        try
        {
            DataTable dtVendor = ComplainBLL.Instance.VendorSelectBLL();
            DropDownListHelper.BindDropDownList(ref ddlVendor, dtVendor, "SVENDORID", "SABBREVIATION", true);

            //if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            //{
            //    ddlVendor.SelectedValue = Session["CGROUP"].ToString();
            //    ddlVendor.Enabled = false;
            //}

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //private void GetTruckType()
    //{
    //    try
    //    {
    //        DataTable dtTruckType = CarBLL.Instance.LoadDataCartype();
    //        DropDownListHelper.BindDropDownList(ref ddlTruckType, dtTruckType, "CONFIG_VALUE", "CONFIG_NAME", true);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    private void GetTruckPermission()
    {
        try
        {
            DataTable dtPermission = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'TRUCK_STATUS'");
            DropDownListHelper.BindDropDownList(ref ddlPermission, dtPermission, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvTruck_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            dgvTruck.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvTruck, dtTruck);

            for (int i = 0; i < dgvTruck.Rows.Count; i++)
            {
                string ColorText = dtTruck.Rows[dgvTruck.PageIndex * dgvTruck.PageSize + i]["ROW_COLOR"].ToString();
                if (string.Equals(ColorText, "Red"))
                    dgvTruck.Rows[i].Cells[GridViewHelper.GetColumnIndexByName(dgvTruck, "STOP_DATE")].ForeColor = Color.Red;
                else
                    dgvTruck.Rows[i].Cells[GridViewHelper.GetColumnIndexByName(dgvTruck, "STOP_DATE")].ForeColor = Color.Black;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckDuplicate()
    {
        try
        {
            for (int i = dtTruck.Rows.Count - 1; i > 0; i--)
            {
                if (string.Equals(dtTruck.Rows[i - 1]["STRUCKID"].ToString(), dtTruck.Rows[i]["STRUCKID"].ToString()))
                {
                    dtTruck.Rows[i]["SHEADREGISTERNO"] = string.Empty;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void MergeRows(GridView gridView)
    {
        try
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                //for (int i = 0; i < row.Cells.Count; i++)
                for (int i = 0; i < 5; i++)
                {
                    if (row.Cells[GridViewHelper.GetColumnIndexByName(dgvTruck, "SHEADREGISTERNO")].Text == previousRow.Cells[GridViewHelper.GetColumnIndexByName(dgvTruck, "SHEADREGISTERNO")].Text && row.Cells[i].Text == previousRow.Cells[i].Text)
                    {
                        row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 :
                                               previousRow.Cells[i].RowSpan + 1;
                        previousRow.Cells[i].Visible = false;
                    }
                }

                //if (string.Equals(row.Cells[GridViewHelper.GetColumnIndexByName(dgvTruck, "IS_EXPIRE")], "1"))
                if (string.Equals(dtTruck.Rows[rowIndex], "1"))
                {//Doc Expire
                    row.Cells[GridViewHelper.GetColumnIndexByName(dgvTruck, "IS_EXPIRE")].ForeColor = Color.Red;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    } 
    protected void dgvTruck_PreRender(object sender, EventArgs e)
    {
        try
        {
            this.MergeRows(dgvTruck);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTruck_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string ColorText = dtTruck.Rows[e.Row.RowIndex]["ROW_COLOR"].ToString();
                if (string.Equals(ColorText, "Red"))
                    e.Row.Cells[GridViewHelper.GetColumnIndexByName(dgvTruck, "STOP_DATE")].ForeColor = Color.Red;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex == 0)
                this.GetContract(string.Empty);
            else
                this.GetContract(" AND SVENDORID = '" + ddlVendor.SelectedValue + "'");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
                ddlVendor.SelectedValue = dtContract.Rows[ddlContract.SelectedIndex]["SVENDORID"].ToString();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            dtTruck = CarBLL.Instance.TruckAgeSelect(this.GetCondition());
            GridViewHelper.BindGridView(ref dgvTruck, dtTruck);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetCondition()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            if (ddlVendor.SelectedIndex > 0)
                sb.Append(" AND VENDOR_ID = '" + ddlVendor.SelectedValue + "'");

            if (ddlContract.SelectedIndex > 0)
                sb.Append(" AND CONTRACT_ID = '" + ddlContract.SelectedValue + "'");

            if (!string.Equals(txtLicense.Text.Trim(), string.Empty))
                sb.Append(" AND (SHEADREGISTERNO LIKE'%" + txtLicense.Text.Trim() + "%' OR STRAILERREGISTERNO LIKE'%" + txtLicense.Text.Trim() + "%')");

            if (ddlTruckAge.SelectedIndex > 0)
            {
                if (ddlTruckAge.SelectedIndex == ddlTruckAge.Items.Count - 1)
                    sb.Append(" AND TRUCK_AGE_YEAR > 8");
                else
                    sb.Append(" AND TRUCK_AGE_YEAR = " + ddlTruckAge.SelectedValue);
            }

            if (TxtTruckExpire.Text.Trim() != "")
            {
                sb.Append("AND (NVL(TRUNC ((SYSDATE) - TO_DATE (STOP_DATE, 'DD/MM/YYYY')),0) BETWEEN -1 * " + TxtTruckExpire.Text.Trim() + " AND 0 )");
            }

            if (!string.IsNullOrEmpty(ddlPermission.SelectedValue))
            {
                string checkAllow = "";
                string checkDisallow = "";
                string checkDisabled = "";
                //string checkOver = "";
                string[] permissions = ddlPermission.SelectedValue.Split('|');
                foreach (string item in permissions)
                {
                    if (item.Trim().ToUpper() == "Y")
                    {
                        checkAllow = "Y";
                    }
                    else if (item.Trim().ToUpper() == "N")
                    {
                        checkDisallow = "N";
                    }
                    else if (item.Trim().ToUpper() == "D")
                    {
                        checkDisabled = "D";
                    }
                }
                sb.Append(" AND STATUS_VALUE IN('" + CommonFunction.ReplaceInjection(checkAllow) + "','" + CommonFunction.ReplaceInjection(checkDisallow) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled) + "')");
            }


            if (!string.IsNullOrEmpty(ddlDocumentType.SelectedValue))
                sb.Append(" AND UPLOAD_ID = " + ddlDocumentType.SelectedValue + "");

            if (!string.IsNullOrEmpty(ddlIsExpire.SelectedValue))
                sb.Append(" AND IS_EXPIRE = " + ddlIsExpire.SelectedValue + "");

            if (!string.IsNullOrEmpty(txtDateStart.Text.Trim()) && !string.IsNullOrEmpty(txtDateEnd.Text.Trim()))
                sb.Append(" AND TO_DATE(STOP_DATE,'dd/mm/yyyy') BETWEEN TO_DATE('" + txtDateStart.Text.Trim() + "','dd/mm/yyyy') AND TO_DATE('" + txtDateEnd.Text.Trim() + "','dd/mm/yyyy')");

            if (!string.IsNullOrEmpty(ddlWorkGroup.SelectedValue))
                sb.Append(" AND WORKGROUPID = " + ddlWorkGroup.SelectedValue + "");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdExport_Click(object sender, EventArgs e)
    {
        try
        {
            if (dtTruck == null || dtTruck.Rows.Count == 0)
            {
                throw new Exception("ไม่มีข้อมูลที่ต้องการ Export");
            }

            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            workbook.Worksheets.Add("Export");
            ExcelWorksheet worksheet = workbook.Worksheets["Export"];
            DataTable dtFinal = dtTruck.Copy();

            dtFinal.Columns.Remove("STRUCKID");
            dtFinal.Columns.Remove("STATUS_VALUE");
            dtFinal.Columns.Remove("VENDOR_ID");
            dtFinal.Columns.Remove("CONTRACT_ID");
            dtFinal.Columns.Remove("TRUCK_TYPE_ID");
            dtFinal.Columns.Remove("UPLOAD_ID");
            dtFinal.Columns.Remove("ROW_COLOR");
            dtFinal.Columns.Remove("IS_EXPIRE");
            dtFinal.Columns.Remove("TRUCK_AGE_YEAR");
            dtFinal.Columns.Remove("WORKGROUPID");
            dtFinal.Columns.Remove("GROUPNAME");
            dtFinal.Columns.Remove("WORKGROUPNAME");
            dtFinal.Columns.Remove("IS_EXPIRE_TEXT");

            this.SetFormatCell(worksheet.Cells[0, 0], "รายงานอายุรถข้อมูลรถบรรทุก", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 0, 0, 10).Merged = true;
            this.SetFormatCell(worksheet.Cells[1, 0], "ทะเบียนหัว", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 1], "ทะเบียนหาง", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 2], "สถานะ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 3], "ผู้ขนส่ง", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 4], "เลขที่สัญญา", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 5], "ประเภทรถ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 6], "ประเภทเอกสาร", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 7], "หมายเลขเอกสาร", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 8], "ปี", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 9], "วันที่เริ่มต้นเอกสาร", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 10], "วันที่สิ้นสุดเอกสาร", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 11], "ตรวจสอบอายุ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);


            for (int i = 2; i <= dtFinal.Rows.Count + 1; i++)
            {
                for (int j = 0; j < dtFinal.Columns.Count; j++)
                {//Export Detail
                    this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 2][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                }
            }

            int columnCount = worksheet.CalculateMaxUsedColumns();

            for (int i = 0; i < columnCount; i++)
                worksheet.Columns[i].AutoFit(1.3);
            string Path = this.CheckPath();
            string FileName = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
            cell.Style.Font.Size = 12 * 20;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void ddlWorkGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlWorkGroup.SelectedIndex > 0)
            this.GetContract(" AND TCONTRACT.GROUPSID = " + ddlWorkGroup.SelectedValue);
        else
            DropDownListHelper.BindDropDownList(ref ddlContract, null, "SCONTRACTID", "SCONTRACTNO", true);

    }
}