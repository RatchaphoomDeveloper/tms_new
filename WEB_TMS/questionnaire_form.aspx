﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="questionnaire_form.aspx.cs" Inherits="questionnaire_form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .rdoListNoBorder {
            border: none;
        }

            .rdoListNoBorder tr td {
                border: none !important;
            }

        .marginTp {
            margin-top: 5px;
        }

        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">

    <ul class="nav nav-tabs" runat="server" id="tabtest" clientidmode="Static">
        <li class="active" id="Tab1" runat="server" clientidmode="Static"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab" clientidmode="Static">ข้อมูลทั่วไป</a></li>
        <li class="" id="Tab2" runat="server" clientidmode="Static"><a href="#TabGroup" data-toggle="tab" aria-expanded="false" runat="server" id="ProcessTab" clientidmode="Static">ข้อมูลแบบสอบถาม</a></li>
    </ul>
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane active" id="TabGeneral" runat="server" clientidmode="Static">
            <br />
            <asp:UpdatePanel ID="uplMain" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlYear" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnAddTeam" EventName="Click" />
                    <asp:PostBackTrigger ControlID="cmdUpload" />
                </Triggers>
                <ContentTemplate>
                    <div class="panel panel-info" style="margin-top: 20px">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลแบบฟอร์มการทำงาน
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=ddlYear.ClientID %>" class="col-md-2 control-label">ปี</label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control marginTp" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <label for="<%=txtDCREATE.ClientID %>" class="col-md-3 control-label">วันที่บันทึก</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtDCREATE" autocomplete="off" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=ddlVendor.ClientID %>" class="col-md-2 control-label">บริษัทผู้ประกอบการขนส่ง</label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlVendor" runat="server" DataTextField="SABBREVIATION" CssClass="form-control marginTp" DataValueField="SVENDORID">
                                            </asp:DropDownList>
                                        </div>

                                        <label for="<%=txtDCHECK.ClientID %>" class="col-md-3 control-label">วันที่ตรวจประเมิน</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtDCHECK" runat="server" CssClass="datepicker"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=txtAddress.ClientID %>" class="col-md-2 control-label">สถานที่ตรวจสอบ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtAddress" MaxLength="500" CssClass="form-control" runat="server" autocomplete="off"></asp:TextBox>
                                        </div>
                                        <label for="<%=txtRemark.ClientID %>" class="col-md-3 control-label">อื่นๆ เพิ่มเติม</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtRemark" autocomplete="off" CssClass="form-control" runat="server" MaxLength="500"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info" style="margin-top: 20px">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลผู้เข้าร่วมการตรวจประเมิน
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=txtTQUESTIONNAIRE_TEAM.ClientID %>" class="col-md-2 control-label">รายชื่อ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtTQUESTIONNAIRE_TEAM" MaxLength="200" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                        <div class="col-md-1">
                                            <asp:Button ID="btnAddTeam" runat="server" Text="เพิ่มรายการ" CssClass="btn btn-hover btn-info" OnClick="btnAddTeam_Click" />
                                        </div>

                                        <label for="<%=rdoTeamType.ClientID %>" class="col-md-2 control-label">หน้าที่</label>
                                        <div class="col-md-3 radio">
                                            <asp:RadioButtonList ID="rdoTeamType" RepeatLayout="Flow" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="2" runat="server"></asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <asp:GridView ID="grvTQUESTIONNAIRE_TEAM" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" AlternatingRowStyle-HorizontalAlign="Center"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnRowCommand="grvTQUESTIONNAIRE_TEAM_RowCommand"
                                    DataKeyNames="QTEAM_ID,SNAME,TEAM_TYPE" HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" RowStyle-HorizontalAlign="Center"
                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="ลำดับ">
                                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblSNAME" runat="server" Text='<%# Eval("SNAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="หน้าที่" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTYPE_NAME" runat="server" Text='<%# Eval("TYPE_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="แก้ไขข้อมูล" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageEdit" runat="server" ImageUrl="~/Images/document.gif"
                                                    Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CommandName="RowSelect" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info" style="margin-top: 20px">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>เอกสารแนบ
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">

                                <div class="panel-body">
                                    <label for="<%=cboUploadType.ClientID %>" class="col-md-2 control-label">ประเภทเอกสาร</label>
                                    <div class="col-md-3">
                                        <asp:Table ID="Table3" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell ColumnSpan="2">
                                                    <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                                        Width="350px" DataValueField="UPLOAD_ID">
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:FileUpload ID="fileUpload" runat="server" />
                                                </asp:TableCell><asp:TableCell>
                                                    <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" OnClick="cmdUpload_Click" Style="width: 100px" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>

                                    </div>
                                    <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                        EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                        EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                        OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                        OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                            <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                            <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                            <asp:BoundField DataField="FULLPATH" Visible="false" />
                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="16px" CausesValidation="True" Height="16px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="16px" Height="16px" Style="cursor: pointer" CommandName="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div style="text-align: right;">
                        <asp:Button ID="btnAddForm" runat="server" Text="บันทึกข้อมูลทั่วไป" CssClass="btn btn-hover btn-info" OnClick="btnAddForm_Click" />
                        <asp:Button ID="btnClose" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 100px" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane" id="TabGroup" runat="server" clientidmode="Static">
            <br />
            <asp:UpdatePanel ID="uplGroup" runat="server" UpdateMode="Always">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlFormName" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveChange" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnChangeGroup" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveBeforeChange" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveTab2" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <div class="panel panel-info" style="margin-top: 20px;">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลหมวด
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="col-md-6">
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=ddlYearTab2.ClientID %>" class="col-md-4 control-label">ปี</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlYearTab2" Enabled="false" runat="server" CssClass="form-control marginTp">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=ddlFormName.ClientID %>" class="col-md-4 control-label">ชื่อแบบฟอร์ม</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlFormName" runat="server" CssClass="form-control marginTp" OnSelectedIndexChanged="ddlFormName_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=ddlGroupName.ClientID %>" class="col-md-4 control-label">ชื่อหมวด/กลุ่ม</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlGroupName" runat="server" CssClass="form-control marginTp" OnSelectedIndexChanged="ddlGroupName_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=txtPoint.ClientID %>" class="col-md-4 control-label">คะแนน</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtPoint" runat="server" Text="0.0" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=chkIS_VENDOR_VIEW.ClientID %>" class="col-md-4 control-label">แสดงให้ผู้คนส่งเห็น</label>
                                        <div class="col-md-4 checkbox-inline"> 
                                                <asp:CheckBox runat="server" ID="chkIS_VENDOR_VIEW" /> 
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info" style="margin-top: 20px;">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>Summary
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:GridView ID="grvScore" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" ShowFooter="true" DataKeyNames="SUM_SCORE,NWEIGHT,ITMGRUP"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowDataBound="grvScore_RowDataBound"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="ลำดับ" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="Left" HeaderStyle-HorizontalAlign="Center" HeaderText="Topics">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSGROUPNAME" runat="server" Text='<%# Eval("SGROUPNAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <strong>Total</strong>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="Items">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <FooterStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblITMGRUP" runat="server" Text='<%# Eval("ITMGRUP") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <strong>
                                                        <asp:Label ID="lblFITMGRUP" runat="server" Text='0'></asp:Label></strong>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="Weight">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <FooterStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNWEIGHT" runat="server" Text='<%# Eval("NWEIGHT","{0:n2}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <strong>
                                                        <asp:Label ID="lblFNWEIGHT" runat="server" Text='0'></asp:Label></strong>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="Average Score">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <FooterStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAverageScore" runat="server" Text='0'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <strong>
                                                        <asp:Label ID="lblFAverageScore" runat="server" Text='0'></asp:Label>
                                                    </strong>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="Result">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <FooterStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSUM_SCORE" runat="server" Text='<%# Eval("TSUM_SCORE","{0:n2}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <strong>
                                                        <asp:Label ID="lblFSUM_SCORE" runat="server" Text='0'></asp:Label></strong>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="Percentage">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <FooterStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPercentage" runat="server" Text='0'></asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <strong>
                                                        <asp:Label ID="lblFPercentage" runat="server" Text='0'></asp:Label></strong>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info" style="margin-top: 20px;">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลแบบสอบถาม
                            <div style="float: right;">
                                <asp:Label ID="Label21" runat="server" Text="คะแนน "></asp:Label>
                                <asp:Label ID="lblPoint" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:GridView ID="GridView1" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                        CellPadding="4" GridLines="None" CssClass="table table-hover GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnDataBound="GridView1_DataBound" HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                        <Columns>
                                            <asp:TemplateField HeaderText="รหัสคำถาม" HeaderStyle-Width="50px" ItemStyle-Width="50px">
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFORM_CODE" runat="server" Text='<%# Eval("FORM_CODE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                <ItemTemplate>
                                                    <asp:RadioButtonList ID="RdoListPoint" runat="server" CssClass="rdoListNoBorder" onchange="SelChange();"></asp:RadioButtonList>
                                                    <div style="display: none;">
                                                        <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: right;">
                        <asp:Button ID="btnSaveTab2" runat="server" Text="บันทึกข้อมูลแบบสอบถาม" CssClass="btn btn-hover btn-info" OnClick="btnSaveTab2_Click" OnClientClick="return CheckAllChk();" />
                        <asp:Button ID="Button2" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 100px" />
                        <div style="display: none;">
                            <asp:Button ID="btnSaveBeforeChange" runat="server" OnClick="btnSaveBeforeChange_Click" />
                            <asp:Button ID="btnChangeGroup" runat="server" OnClick="btnNotSave_Click" />
                        </div>

                    </div>
                    <asp:HiddenField ID="hdIsChange" runat="server" Value="0" />
                    <asp:HiddenField ID="hdGroupCount" runat="server" Value="0" />
                    <asp:HiddenField ID="hdScoreNotInCurrentGroup" runat="server" Value="0" />
                    <asp:HiddenField ID="hdScoreSum" runat="server" Value="0" />
                    <div class="modal fade" style="z-index: 1060" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" id="alertChange">
                        <div class="modal-dialog">
                            <div class="modal-content" style="text-align: left;">
                                <div class="modal-header">
                                    <h4 class="modal-title">แจ้งเตือน</h4>
                                </div>
                                <div class="modal-body" style="word-wrap: break-word;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td valign="top" style="width: 50px;">
                                                <asp:Image ImageUrl="~/Images/Question_Mark.gif" ID="imgPopup" runat="server" /></td>
                                            <td valign="top" align="left" style="font-size: 16px; vertical-align: central;" class="TextDetail">
                                                <div style="height: 40px; text-align: center; display: table-cell; vertical-align: middle;">ผลประเมินแบบสอบถามมีการเปลี่ยนแปลง ท่านต้องการบันทึกข้อมูลหรือไม่?</div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <asp:Button runat="server" ID="btnSaveChange" Text="บันทึกข้อมูล" CssClass="btn btn-md bth-hover btn-info" OnClientClick="clickSaveBeforeChange()" />
                                    <asp:Button runat="server" ID="btnNotSave" Text="ไม่บันทึก" CssClass="btn btn-md bth-hover btn-warning" OnClientClick="clickChangeGroup()" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" style="z-index: 1060" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" id="alertWithClose">
                        <div class="modal-dialog">
                            <div class="modal-content" style="text-align: left;">
                                <div class="modal-header">
                                    <h4 class="modal-title">แจ้งเตือน</h4>
                                </div>
                                <div class="modal-body" style="word-wrap: break-word;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td valign="top" style="width: 50px;">
                                                <asp:Image ImageUrl="~/Images/Icon_Lamp.png" ID="Image1" runat="server" /></td>
                                            <td valign="top" align="left" style="font-size: 16px; vertical-align: central;" class="TextDetail">
                                                <div style="height: 40px; text-align: center; display: table-cell; vertical-align: middle;">บันทึกข้อมูลเรียบร้อย</div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <asp:Button runat="server" ID="Button1" Text="ปิดหน้าต่าง" CssClass="btn btn-md bth-hover btn-info" OnClientClick="CloseModalSave()" />
                                    <asp:Button runat="server" ID="Button3" Text="กลับหน้าแรก" CssClass="btn btn-md bth-hover btn-warning" OnClientClick="BlackToMain()" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="display: none;">
                        <asp:TextBox ID="txtMaxRdo" runat="server" Text="0"></asp:TextBox>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
    <script type="text/javascript">
        var ScoreNotInCurrentGroup = 0;
        var MaxRdo = parseFloat($("#<%=txtMaxRdo.ClientID%>").val());
        function SelChange() {
            CalGroup();
            $('#<%=hdIsChange.ClientID%>').attr('value', '1');
            ScoreNotInCurrentGroup = parseFloat($("#cph_Main_lblPoint").text()) + ScoreNotInCurrentGroup;
            $("#<%=txtPoint.ClientID%>").val(ScoreNotInCurrentGroup.toFixed(2));
        }

        function CalGroup() {
            var i = 1;
            ScoreNotInCurrentGroup = parseFloat($("#<%=hdScoreNotInCurrentGroup.ClientID%>").val());
            var _PointGrvCurr = 0.0;
            $("#cph_Main_GridView1 input[type=radio]:checked").each(function () {
                var _id = "cph_Main_GridView" + i.toString() + "_RdoListPoint";
                var rdoId = $(this).attr("id");
                var pieces = rdoId.replace(_id, "").split(/[\s_]+/);
                var _hwId = pieces[pieces.length - 1];
                var _hW = "cph_Main_GridView" + i.toString() + "_hdWEIGHT_" + _hwId.toString();
                var RdoVal = $(this).val();
                var _curr = parseFloat($("#" + _hW).val()) * parseFloat(RdoVal.split(",")[0]);
                _PointGrvCurr += parseFloat(_curr);
            });
            $("#cph_Main_lblPoint").text((_PointGrvCurr / MaxRdo).toFixed(2));
        }

        function CallAll() {

        }

        function CheckAllChk() {
            var isValid = true;
            var rdoList = $("table[id*=RdoListPoint]");
            rdoList.each(function (key) {
                if (isValid) {
                    isValid = ($(this).find("input[type=radio]:checked").length > 0);
                    if (!isValid) {
                        alertFail("มีแบบสอบถามที่ยังกรอกคะแนนไม่ครบ");
                        return;
                    }
                }
            });
            return isValid;
        }
        function AlertSave() {
            $('#alertChange').find('input').attr('data-dismiss', 'modal');
            var $dialog = $('#alertChange').find(".modal-dialog");
            var offset = (($(window).height() - $dialog.height()) / 2 - $dialog.height() / 2);
            if ($dialog.height() < 0) {
                offset = $(window).height() / 2 - 200;
            }

            offset = $(window).height() / 2 - 200;
            $('#alertChange').find('.modal-dialog').css('margin-top', offset);

            $('#alertChange').modal('show');
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            CalGroup();
        });

        function clickSaveBeforeChange() {
            $("#<%=btnSaveBeforeChange.ClientID%>").click();
        }
        function clickChangeGroup() {
            $("#<%=btnChangeGroup.ClientID%>").click();
        }

        function CloseModalSave() {
            $('#alertWithClose').modal('hide');
        }

        function BlackToMain() {
            location.replace("questionnaire.aspx");
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

