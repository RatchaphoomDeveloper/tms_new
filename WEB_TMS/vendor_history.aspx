﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_history.aspx.cs" Inherits="vendor_history" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" runat="server">
                    <tr>
                        <td colspan="2">จัดการข้อมูลผู้ขนส่ง</td>
                    </tr>
                    <tr>
                        <td>ค้าหาบริษัทผู้ขนส่ง </td>
                        <td>
                            <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="รหัสบริษัทผู้ขนส่ง,ชื่อบริษัทผู้ขนส่ง"
                                CssClass="dxeLineBreakFix" Width="220px">
                            </dx:ASPxTextBox>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="btnCancelSearch" runat="server" SkinID="_cancelsearch" CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('CancelSearch'); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false" SkinID="_gvw">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="วันที่อัพเดทข้อมูลล่าสุด" FieldName="DUPDATE">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="รหัสผู้ขนส่ง" FieldName="SVENDORID">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" FieldName="SABBREVIATION">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ที่อยู่" FieldName="ADDRESS">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="หมายเลขโทรศัพท์" FieldName="STEL">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="การไม่อนุญาตใช้งาน" FieldName="CACTIVE">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_calldata" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    
                                </Columns>
                                   <SettingsPager PageSize="50">
                                            </SettingsPager>
                                            
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
