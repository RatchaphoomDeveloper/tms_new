﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity;
using OfficeOpenXml;
using System.Globalization;
using Utility;
using System.IO;

public partial class AnnualScoreDetails : PageBase
{

    string GroupPermission = "01";

    #region " Prop "
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected DataTable VendorDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "VendorDDL"]; }
        set { ViewState[this.ToString() + "VendorDDL"] = value; }
    }

    protected DataTable dtContract
    {
        get { return (DataTable)ViewState[this.ToString() + "dtContract"]; }
        set { ViewState[this.ToString() + "dtContract"] = value; }
    }

    protected DataTable dtGroup
    {
        get { return (DataTable)ViewState[this.ToString() + "dtGroup"]; }
        set { ViewState[this.ToString() + "dtGroup"] = value; }
    }

    protected FinalGade SearchCriteria
    {
        get { return (FinalGade)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }

    protected DataTable dtSPROCESS
    {
        get { return (DataTable)ViewState[this.ToString() + "dtSPROCESS"]; }
        set { ViewState[this.ToString() + "dtSPROCESS"] = value; }
    }
    protected string SPROCESSID
    {
        get { return ViewState[this.ToString() + "SPROCESSID"].ToString(); }
        set { ViewState[this.ToString() + "SPROCESSID"] = value; }
    }
    protected string VSCONTRACTNO
    {
        get { return ViewState[this.ToString() + "VSCONTRACTNO"] == null ? "" : ViewState[this.ToString() + "VSCONTRACTNO"].ToString(); }
        set { ViewState[this.ToString() + "VSCONTRACTNO"] = value; }
    }

    protected string VSABBREVIATION
    {
        get { return ViewState[this.ToString() + "VSABBREVIATION"] == null ? "" : ViewState[this.ToString() + "VSABBREVIATION"].ToString(); }
        set { ViewState[this.ToString() + "VSABBREVIATION"] = value; }
    }

    protected string VGROUPNAME
    {
        get { return ViewState[this.ToString() + "VGROUPNAME"] == null ? "" : ViewState[this.ToString() + "VGROUPNAME"].ToString(); }
        set { ViewState[this.ToString() + "VGROUPNAME"] = value; }
    }

    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            if (Session["AASM_SCONTRACTNO"] == null ||
                Session["AASM_SABBREVIATION"] == null ||
                Session["AASM_GROUPNAME"] == null) btnClose_Click(null, null);
            else
            {
                VSABBREVIATION = Session["AASM_SABBREVIATION"].ToString();
                VSCONTRACTNO = Session["AASM_SCONTRACTNO"].ToString();
                VGROUPNAME = Session["AASM_GROUPNAME"].ToString();

                Session["AASM_SCONTRACTNO"] = null;
                Session["AASM_SABBREVIATION"] = null;
                Session["AASM_GROUPNAME"] = null;
                SearchCriteria = (FinalGade)Session["AASM_SearchCriteria"];
                lblContNo.Text = VSCONTRACTNO;
                lblGroup.Text = VGROUPNAME;
                lblVendor.Text = VSABBREVIATION;
            }

            InitForm();

            LoadMain();
            //this.AssignAuthen();
        }
    }
    private void InitForm()
    {
        try
        {
            int StartYear = 2015;
            int cYear = DateTime.Today.Year;
            int endYear = cYear + 4;
            int index = 0;
            for (int i = StartYear; i <= endYear; i++)
            {
                ddlYearSearch.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
            }

            ///เลือกปีปัจจุบัน
            ListItem sel = ddlYearSearch.Items.FindByValue(cYear.ToString());
            ddlYearSearch.ClearSelection();
            if (sel != null) sel.Selected = true;
            else ddlYearSearch.SelectedIndex = 0;

            ddlVendorSearch.SelectedIndex = 0;
            LoadVendor();

            DataTable dt = QuarterReportBLL.Instance.GetSPROCESSID();
            if (dt != null)
            {
                List<string> lstStirng = new List<string>();
                foreach (DataRow itm in dt.Rows) lstStirng.Add(itm["SPROCESSID"].ToString());

                SPROCESSID = string.Join(",", lstStirng.ToArray());
            }
            else
                SPROCESSID = "''";

            string _err = string.Empty;
            dtGroup = new FinalGradeBLL().GetGroup(ref _err, true);
            DataRow drG = dtGroup.NewRow();
            drG["NAME"] = "";
            dtGroup.Rows.InsertAt(drG, 0);
            ddlGroup.DataSource = dtGroup;
            ddlGroup.DataBind();

            dtContract = ComplainBLL.Instance.ContractSelectBLL("");
            DataRow dr = dtContract.NewRow();
            dr["SCONTRACTNO"] = "";
            dtContract.Rows.InsertAt(dr, 0);

            ddlContract.DataValueField = "SCONTRACTID";
            ddlContract.DataTextField = "SCONTRACTNO";
            ddlContract.DataSource = dtContract;
            ddlContract.DataBind();

            if (SearchCriteria != null)
            {
                sel = ddlYearSearch.Items.FindByValue(SearchCriteria.YEAR);
                if (sel != null)
                {
                    ddlYearSearch.ClearSelection();
                    sel.Selected = true;
                }

                lblYear.Text = SearchCriteria.YEAR;
            }
            else SearchCriteria = new FinalGade() { YEAR = string.Empty, VENDORID = string.Empty };
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    public void LoadVendor()
    {
        try
        {
            VendorDDL = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = VendorDDL.NewRow();
            dr["SABBREVIATION"] = "";
            VendorDDL.Rows.InsertAt(dr, 0);
            ddlVendorSearch.DataSource = VendorDDL;
            ddlVendorSearch.DataBind();

            if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
            {
                ddlVendorSearch.ClearSelection();
                ListItem lst = ddlVendorSearch.Items.FindByValue(user_profile.SVDID);
                if (lst != null) lst.Selected = true;
                ddlVendorSearch.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    //private void AssignAuthen()
    //{
    //    try
    //    {
    //        if (!CanRead)
    //        {
    //            btnExport.Enabled = false;
    //        }
    //        if (!CanWrite)
    //        {
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}
    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }

    private void LoadMain()
    {
        try
        {
            string _err = string.Empty;
            SearhData = new FinalGradeBLL().GetReduceScore(ref _err, SearchCriteria);
            grvMain.DataSource = SearhData;
            grvMain.DataBind();

            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            {
                grvMain.Columns[12].Visible = false;
                grvMain.Columns[13].Visible = false;
                grvMain.Columns[14].Visible = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
        //    GotoDefault();
        //    return;
        //}
    }

    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {

        }
    }

    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string DISABLE_DRIVER = DataBinder.Eval(e.Row.DataItem, "DISABLE_DRIVER").ToString();
                if (DISABLE_DRIVER == "H")
                {
                    Label lblHOLD = e.Row.Cells[1].FindControl("lblHOLD") as Label;
                    if (lblHOLD != null) lblHOLD.Text = "Y";
                }
                else
                {
                    Label lblDISABLE_DRIVER = e.Row.Cells[1].FindControl("lblDISABLE_DRIVER") as Label;
                    if (lblDISABLE_DRIVER != null) lblDISABLE_DRIVER.Text = DISABLE_DRIVER;
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        LoadMain();
    }
    protected void btnViewHistory_Click(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadMain();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        InitForm();
        LoadMain();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        string FileName = string.Empty;
        if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            FileName = "AnnualScoreDetailsFormat_vendor.xlsx";
        else
            FileName = "AnnualScoreDetailsFormat.xlsx";

        using (ExcelPackage pck = new ExcelPackage(new MemoryStream(), new MemoryStream(File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath("~/FileFormat/Admin/" + FileName)))))
        {
            ExcelWorksheet ws = pck.Workbook.Worksheets[1];
            int StartRow = 2;
            int index = 1;
            foreach (DataRow dr in SearhData.Rows)
            {
                ws.Cells["A" + StartRow.ToString()].Value = "" + index.ToString();
                ws.Cells["B" + StartRow.ToString()].Value = dr["SPROCESSNAME"].ToString();
                ws.Cells["C" + StartRow.ToString()].Value = dr["STOPICNAME"].ToString();
                ws.Cells["D" + StartRow.ToString()].Value = dr["DREDUCE"].ToString();
                ws.Cells["E" + StartRow.ToString()].Value = dr["SHEADREGISTERNO"].ToString();
                ws.Cells["F" + StartRow.ToString()].Value = dr["STRAILERREGISTERNO"].ToString();
                ws.Cells["G" + StartRow.ToString()].Value = dr["SEMPLOYEENAME"].ToString();

                string DISABLE_DRIVER = dr["DISABLE_DRIVER"].ToString();
                string lblHOLD = string.Empty;
                string lblDISABLE_DRIVER = string.Empty;
                if (DISABLE_DRIVER == "H") lblHOLD = "Y";
                else lblDISABLE_DRIVER = DISABLE_DRIVER;

                ws.Cells["H" + StartRow.ToString()].Value = lblDISABLE_DRIVER;
                ws.Cells["I" + StartRow.ToString()].Value = lblHOLD;
                ws.Cells["J" + StartRow.ToString()].Value = dr["BLACKLIST"].ToString();
                ws.Cells["K" + StartRow.ToString()].Value = decimal.Parse(dr["SUMNPOINT"].ToString()).ToString("#,0.00");
                ws.Cells["L" + StartRow.ToString()].Value = decimal.Parse(dr["COST"].ToString()).ToString("#,0.00");
                if (!string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                {
                    ws.Cells["M" + StartRow.ToString()].Value = dr["SREDUCEBY"].ToString();
                    ws.Cells["N" + StartRow.ToString()].Value = dr["APPROVE_DATE"].ToString();
                    ws.Cells["O" + StartRow.ToString()].Value = dr["STATUS"].ToString();
                }
                StartRow++;
                index++;
            }

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=AnnualScoreDetails.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("AnnualScoreMain.aspx");
    }
}