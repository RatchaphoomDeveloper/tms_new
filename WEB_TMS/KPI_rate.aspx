﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="KPI_rate.aspx.cs" Inherits="KPI_rate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        function CalculateRow(Index, SUMTYPE, PAHSE1, PAHSE2, PAHSE3, PAHSE4, PAHSE5, KPI_SUMWEIGHT) {

            /*เคลียตัวซัมก่อน*/
            window["lblSum_" + Index + ""].SetText('');
            window["lblSumWeight_" + Index + ""].SetText('');

            var SVAL = null;

            /*หาค่าเฉลี่ย*/
            if (SUMTYPE == "0") {
                SVAL = SumAVG(Index);
            }
            /*หาค่าน้อย*/
            else if (SUMTYPE == "1") {
                SVAL = SumLow(Index);
            }
            /*หาค่ามาก*/
            else if (SUMTYPE == "2") {
                SVAL = SumHeight(Index);
            }



            if (CheckRank(SVAL, PAHSE1)) {
                window["lblSum_" + Index + ""].SetText('1');
                SumAllWeight(Index, '1', KPI_SUMWEIGHT);
            }
            else if (CheckRank(SVAL, PAHSE2)) {
                window["lblSum_" + Index + ""].SetText('2');
                SumAllWeight(Index, '2', KPI_SUMWEIGHT);
            }
            else if (CheckRank(SVAL, PAHSE3)) {
                window["lblSum_" + Index + ""].SetText('3');
                SumAllWeight(Index, '3', KPI_SUMWEIGHT);
            }
            else if (CheckRank(SVAL, PAHSE4)) {
                window["lblSum_" + Index + ""].SetText('4');
                SumAllWeight(Index, '4', KPI_SUMWEIGHT);
            }
            else if (CheckRank(SVAL, PAHSE5)) {
                window["lblSum_" + Index + ""].SetText('5');
                SumAllWeight(Index, '5', KPI_SUMWEIGHT);
            }
            else {
                var ValCheck = null;
                /*วนเช็คว่าเป็นค่าว่างทุกช่องไหม*/
                for (var i = 1; i < 13; i++) {
                    if (window["txtM" + i + "_" + Index + ""] != undefined) {
                        if (window["txtM" + i + "_" + Index + ""].GetValue() != null) {
                            ValCheck = i;
                        }
                    }
                }
                if (ValCheck == null) {
                    window["lblSum_" + Index + ""].SetText('');
                }
                else {
                    window["lblSum_" + Index + ""].SetText('ไม่มีค่าที่ตรงกับ Rank');
                }
            }

            FootTotal();
        }
        /*หาค่าเฉลี่ย*/
        function SumAVG(Index) {
            var divide = 0;
            var SUM = 0;

            for (var i = 1; i < 13; i++) {


                if (window["txtM" + i + "_" + Index + ""] != undefined) {

                    if (window["txtM" + i + "_" + Index + ""].GetValue() != null) {
                        divide = parseFloat(divide) + 1;
                        SUM = parseFloat(window["txtM" + i + "_" + Index + ""].GetValue()) + parseFloat(SUM);
                        /* SUM = window["txtM" + i + "_" + Index + ""].GetValue() + SUM;*/
                    }
                }
                else {

                }

            }

            return (SUM / divide).toFixed(0);
        }

        /*หาค่าต่ำ*/
        function SumLow(Index) {

            var SUM = null;

            for (var i = 1; i < 13; i++) {



                if (window["txtM" + i + "_" + Index + ""] != undefined) {

                    if (window["txtM" + i + "_" + Index + ""].GetValue() != null) {
                        /*เช็คว่าถ้าค่า SUM เป็น null ให้ใส่ค่าครั้งแรกไปเพื่อใช้เทียบ*/
                        if (SUM == null) {
                            SUM = window["txtM" + i + "_" + Index + ""].GetValue();
                        }

                        if (parseFloat(SUM) > parseFloat(window["txtM" + i + "_" + Index + ""].GetValue())) {
                            SUM = window["txtM" + i + "_" + Index + ""].GetValue();
                        }
                        else {

                        }
                    }
                }

            }

            return parseFloat(SUM).toFixed(0);
        }

        /*หาค่าต่ำ*/
        function SumHeight(Index) {

            var SUM = null;

            for (var i = 1; i < 13; i++) {



                if (window["txtM" + i + "_" + Index + ""] != undefined) {

                    if (window["txtM" + i + "_" + Index + ""].GetValue() != null) {
                        /*เช็คว่าถ้าค่า SUM เป็น null ให้ใส่ค่าครั้งแรกไปเพื่อใช้เทียบ*/
                        if (SUM == null) {
                            SUM = window["txtM" + i + "_" + Index + ""].GetValue();
                        }

                        if (parseFloat(SUM) < parseFloat(window["txtM" + i + "_" + Index + ""].GetValue())) {
                            SUM = window["txtM" + i + "_" + Index + ""].GetValue();
                        }
                        else {

                        }
                    }

                }

            }

            return parseFloat(SUM).toFixed(0);
        }

        /*เช็ค Rank ถ้าใช่ return true ไม่ใช่ return false*/
        function CheckRank(SVAL, PAHSE) {

            var VAL1 = null;
            var VAL2 = null;
            /*เช็คว่าค่าที่ส่งมาเป็นแบบช่วง หรือแบบเดี่ยว*/
            var Rank = PAHSE + "";
            if (Rank.indexOf("-") > -1) {

                var res = Rank.split("-");

                VAL1 = res[0];
                VAL2 = res[1];


                if (parseFloat(VAL1) <= parseFloat(SVAL) && parseFloat(VAL2) >= parseFloat(SVAL)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                if (parseFloat(SVAL) == parseFloat(PAHSE)) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        /*เช็ค Rank ถ้าใช่ return true ไม่ใช่ return false แต่ฟังชั่นนี้จะเช็คตัวสุดท้ายคือ Phase5 ถ้าค่าที่ส่งมามีมากกว่าตัวที่อยู่ใน Phase5 จะให้เป็น 5 ทันที*/
        function CheckRankEnd(SVAL, PAHSE) {

            var VAL1 = null;
            var VAL2 = null;
            /*เช็คว่าค่าที่ส่งมาเป็นแบบช่วง หรือแบบเดี่ยว*/
            var Rank = PAHSE + "";
            if (Rank.indexOf("-") > -1) {

                var res = Rank.split("-");

                VAL1 = res[0];
                VAL2 = res[1];


                if (parseFloat(VAL1) <= parseFloat(SVAL) && parseFloat(VAL2) >= parseFloat(SVAL)) {
                    return true;
                }
                else {
                    if (parseFloat(VAL2) < parseFloat(SVAL)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
            else {
                if (parseFloat(SVAL) == parseFloat(PAHSE)) {
                    return true;
                }
                else if (parseFloat(SVAL) > parseFloat(PAHSE)) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        /*คำนวนคะแนน Weight*/
        function SumAllWeight(Index, SCORE, KPI_SUMWEIGHT) {

            var Result = (parseFloat(SCORE) / 5) * KPI_SUMWEIGHT;

            window["lblSumWeight_" + Index + ""].SetText(Result.toFixed(0));
        }

        function isNumber(evt) {

            if (evt != null) {
                var iKeyCode = evt.keyCode;

                if (iKeyCode == '46' || (iKeyCode > 47 && iKeyCode < 58)) {

                }
                else {
                    evt.preventDefault();

                }
            }
        }

        function JFormat(Obj) {

            if (Obj.GetValue() != null) {
                var Value = Obj.GetValue();

                if (Value.indexOf(".") == -1) {
                    Value = parseFloat(Value).toFixed(0);
                }
                else if (Value.indexOf(".00") > -1) {
                    Value = parseFloat(Value).toFixed(0);
                }
                else {
                    Value = parseFloat(Value).toFixed(2);
                }
                Obj.SetText(Value);
            }
        }

        function FootTotal() {

            var Sum = 0;
            for (var i = 0; i < parseInt(gvwT1.GetVisibleRowsOnPage()); i++) {

                if (window["lblSumWeight_" + i + ""] != undefined) {
                    if (window["lblSumWeight_" + i + ""].GetValue() != null) {
                        Sum = parseInt(window["lblSumWeight_" + i + ""].GetValue()) + parseInt(Sum);
                    }
                }
            }
            lblTotal.SetText(Sum);
        }

      

    </script>
    <style type="text/css">
        .hide
        {
            display: none;
        }
        
        .Noborder
        {
            border: 0px !important;
        }
        
        /*.FR
        {
            text-align:right !important;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" cellpadding="3" cellspacing="1">
                    <tr>
                        <td>
                            <dx:ASPxGridView runat="server" ID="gvwT1" KeyFieldName="KPI_ID" ClientInstanceName="gvwT1">
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="KPI_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CACTIVE" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="KPI_FREQUENCY" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="KPI_SUM" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ชื่อหัวข้อประเมิน" FieldName="KPI_NAME">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtKPI_NAME" Width="200px" Text='<%# Eval("KPI_NAME") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ความถี่" FieldName="KPI_FREQUENCY_NAME">
                                    <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtKPI_NAME" Width="100px" Text='<%# Eval("KPI_FREQUENCY_NAME") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="วิธีคำนวณผลรวม" FieldName="KPI_SUM_NAME">
                                     <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtKPI_SUM_NAME" Width="100px" Text='<%# Eval("KPI_SUM_NAME") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="1" FieldName="PHASE1" Width="50px"  CellStyle-HorizontalAlign="Center">
                                     <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtPHASE1" Width="50px" Text='<%# Eval("PHASE1") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="2" FieldName="PHASE2" Width="50px"  CellStyle-HorizontalAlign="Center">
                                    <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtPHASE2" Width="50px" Text='<%# Eval("PHASE2") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="3" FieldName="PHASE3" Width="50px"  CellStyle-HorizontalAlign="Center">
                                    <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtPHASE3" Width="50px" Text='<%# Eval("PHASE3") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="4" FieldName="PHASE4" Width="50px"  CellStyle-HorizontalAlign="Center">
                                    <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtPHASE4" Width="50px" Text='<%# Eval("PHASE4") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="5" FieldName="PHASE5" Width="50px"  CellStyle-HorizontalAlign="Center">
                                    <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtPHASE5" Width="50px" Text='<%# Eval("PHASE5") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Weight<br>(ผลรวมไม่เกิน 100)" FieldName="KPI_SUMWEIGHT">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="txtKPI_SUMWEIGHT" Width="80px" Text='<%# Eval("KPI_SUMWEIGHT") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewBandColumn Caption="คะแนนประเมินผล KPI">
                                        <Columns>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M1" Width="50px" FieldName="M1"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM1" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M2" Width="50px" FieldName="M2"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM2" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M3" Width="50px" FieldName="M3"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM3" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M4" Width="50px" FieldName="M4"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM4" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M5" Width="50px" FieldName="M5"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM5" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M6" Width="50px" FieldName="M6"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM6" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M7" Width="50px" FieldName="M7"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM7" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M8" Width="50px" FieldName="M8"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM8" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M9" Width="50px" FieldName="M9"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM9" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M10" Width="50px" FieldName="M10"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM10" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M11" Width="50px" FieldName="M11"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM11" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn HeaderStyle-CssClass="hide" Caption="M12" Width="50px" FieldName="M12"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtM12" Width="50px" HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" LostFocus="function(s,e){ JFormat(s);}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataColumn Caption="สรุปผลคะแนน" FieldName="SUMKPI">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox runat="server" ID="lblSum" ReadOnly="true" CssClass="Noborder" HorizontalAlign="Right">
                                            </dx:ASPxTextBox>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="สรุปผลคะแนน<br>หลัง  Weight" FieldName="SUMKPI_WEIGHT">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox runat="server" ID="lblSumWeight" ReadOnly="true" CssClass="Noborder"
                                                HorizontalAlign="Right">
                                            </dx:ASPxTextBox>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowFooter="True" />
                                <SettingsPager PageSize="100">
                                </SettingsPager>
                                <Templates>
                                    <FooterRow>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right">
                                                    <dx:ASPxLabel runat="server" ID="lblTotal" ClientInstanceName="lblTotal">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </FooterRow>
                                </Templates>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ASPxButton runat="server" ID="btnSave" Text="บันทึกค่า" CssClass="dxeLineBreakFix"
                                AutoPostBack="false">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnCancel" Text="ยกเลิก" CssClass="dxeLineBreakFix"
                                AutoPostBack="false">
                                <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('Cancel'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
