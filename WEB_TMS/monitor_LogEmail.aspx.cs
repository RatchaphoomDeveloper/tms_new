﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class monitor_LogEmail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        gvwLogEmail.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwLogEmail_AfterPerformCallback);
    }
    protected void gvwLogEmail_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                gvwLogEmail.CancelEdit();
                gvwLogEmail.DataBind();
                break;
            case "STARTEDIT":
                dynamic Data = gvwLogEmail.GetRowValues(gvwLogEmail.EditingRowVisibleIndex, "SEMAILFROM", "SEMAILTO", "SSUBJECT", "SMESSAGE");
                Literal ltrDetail  = (Literal)gvwLogEmail.FindEditFormTemplateControl("ltrDetail");
                ltrDetail.Text = "<b>From : </b>" + Data[0] + "<br /><b>To : </b>" + Data[1] + "<br /><b>Subject : </b>" + Data[2] + "<hr />" + Data[3] + "<hr />";
                break;
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvwLogEmail.CancelEdit();
        gvwLogEmail.DataBind();
    }
    protected void btnXlsExport_Click(object sender, EventArgs e)
    {

    }

}