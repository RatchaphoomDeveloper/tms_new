﻿using DevExpress.Web.ASPxGridView;
using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Authentication;
using TMS_BLL.UserGroupSelectBLL;
using TMS_BLL.Master;

public partial class Authenticate : PageBase
{
    DataTable dt;

    #region + View State +
    private DataTable dtWaterCheck
    {
        get
        {
            if ((DataTable)ViewState["dtWaterCheck"] != null)
                return (DataTable)ViewState["dtWaterCheck"];
            else
                return null;
        }
        set
        {
            ViewState["dtWaterCheck"] = value;
        }
    }

    private DataTable dtOperationTransit
    {
        get
        {
            if ((DataTable)ViewState["dtOperationTransit"] != null)
                return (DataTable)ViewState["dtOperationTransit"];
            else
                return null;
        }
        set
        {
            ViewState["dtOperationTransit"] = value;
        }
    }

    private DataTable dtTransitProblem
    {
        get
        {
            if ((DataTable)ViewState["dtTransitProblem"] != null)
                return (DataTable)ViewState["dtTransitProblem"];
            else
                return null;
        }
        set
        {
            ViewState["dtTransitProblem"] = value;
        }
    }

    private DataTable dtDatabase
    {
        get
        {
            if ((DataTable)ViewState["dtDatabase"] != null)
                return (DataTable)ViewState["dtDatabase"];
            else
                return null;
        }
        set
        {
            ViewState["dtDatabase"] = value;
        }
    }

    private DataTable dtCheckPerformance
    {
        get
        {
            if ((DataTable)ViewState["dtCheckPerformance"] != null)
                return (DataTable)ViewState["dtCheckPerformance"];
            else
                return null;
        }
        set
        {
            ViewState["dtCheckPerformance"] = value;
        }
    }

    private DataTable dtOther
    {
        get
        {
            if ((DataTable)ViewState["dtOther"] != null)
                return (DataTable)ViewState["dtOther"];
            else
                return null;
        }
        set
        {
            ViewState["dtOther"] = value;
        }
    }
    private DataTable dtMobile
    {
        get 
        {
            if ((DataTable)ViewState["dtMobile"] != null)
                return (DataTable)ViewState["dtMobile"];
            else
                return null;
        }
        set
        {
            ViewState["dtMobile"] = value;
        }
    }
    private DataTable dtReport
    {
        get
        {
            if ((DataTable)ViewState["dtReport"] != null)
                return (DataTable)ViewState["dtReport"];
            else
                return null;
        }
        set
        {
            ViewState["dtReport"] = value;
        }
    }

    private DataTable dtCGroup
    {
        get
        {
            if ((DataTable)ViewState["dtCGroup"] != null)
                return (DataTable)ViewState["dtCGroup"];
            else
                return null;
        }
        set
        {
            ViewState["dtCGroup"] = value;
        }
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.InitialForm();
            this.AssignAuthen();
        }
    }
    #endregion

    private void InitialForm()
    {
        try
        {
            //DataTable dtUserLogin = (DataTable)Session["UserLogin"];
            UserID = int.Parse(Session["UserID"].ToString()); //int.Parse(dtUserLogin.Rows[0]["USER_ID"].ToString());

            DataTable dtUserGroup = TMS_BLL.UserGroupSelectBLL.UserGroupBLL.Instance.UserGroupSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlUserGroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true);
          

            this.VisibleButtom(false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdOpenAll.Enabled = false;
                cmdCloseAll.Enabled = false;
                cmdOpenAll.Style.Add("cursor", "not-allowed");
                cmdCloseAll.Style.Add("cursor", "not-allowed");
            }
            if (!CanWrite)
            {
                cmdSave.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void VisibleButtom(bool visible)
    {
        try
        {
            cmdCloseAll.Visible = visible;
            cmdOpenAll.Visible = visible;
            cmdSave.Visible = visible;
            cmdCancel.Visible = visible;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlUserGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlUserGroup.SelectedIndex > 0)
            {
                this.VisibleButtom(true);
                this.LoadAuthen();
            }
            else
            {
                this.VisibleButtom(false);

                GridViewHelper.BindGridView(ref dgvWaterCheck, null);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadAuthen()
    {
        try
        {
            string getcGroup = Session["CGROUP"].ToString();
            string getcUserGroupID = ddlUserGroup.SelectedValue;
            DataTable dtAuthen = AuthenBLL.Instance.AuthenSelectBLL(getcUserGroupID);

            if (dtAuthen.Rows.Count == 0) {    
                dtAuthen = AuthenBLL.Instance.AuthenSelectTemplateBLL();
            }

            dtWaterCheck = dtAuthen.Select("MENU_NAME = 'ตรวจวัดน้ำ'").CopyToDataTable();
            GridViewHelper.BindGridView(ref dgvWaterCheck, dtWaterCheck);

            dtOperationTransit = dtAuthen.Select("MENU_NAME = 'การปฏิบัติงานขนส่ง'").CopyToDataTable();
            GridViewHelper.BindGridView(ref dgvOperationTransit, dtOperationTransit);

            dtTransitProblem = dtAuthen.Select("MENU_NAME = 'ปัญหาการขนส่ง'").CopyToDataTable();
            GridViewHelper.BindGridView(ref dgvTransitProblem, dtTransitProblem);

            dtDatabase = dtAuthen.Select("MENU_NAME = 'ฐานข้อมูลระบบ'").CopyToDataTable();
            GridViewHelper.BindGridView(ref dgvDatabase, dtDatabase);

            dtCheckPerformance = dtAuthen.Select("MENU_NAME = 'ระบบประเมินผล'").CopyToDataTable();
            GridViewHelper.BindGridView(ref dgvCheckPerformance, dtCheckPerformance);

            dtOther = dtAuthen.Select("MENU_NAME = 'อื่นๆ'").CopyToDataTable();
            GridViewHelper.BindGridView(ref dgvOther, dtOther);

            dtCGroup = UserBLL.Instance.getCGroupFromUserGroupID(" AND USERGROUP_ID = " + ddlUserGroup.SelectedValue.ToString());
            string CGroup = "";
            if (dtCGroup.Rows.Count > 0)
            {
                CGroup = dtCGroup.Rows[0]["IS_ADMIN"].ToString();
            }
            if (CGroup == "0") //เจ้าหน้าที่ ปตท. & คลัง
            {
                liOther.Visible = false;
            }
            else
            {
                liOther.Visible = true;
            }

            dtMobile = dtAuthen.Select("MENU_NAME = 'Mobile'").CopyToDataTable();
            GridViewHelper.BindGridView(ref dgvMobile, dtMobile);

            //else
            //{
            //    dgvOther.DataSource = null;
            //    dgvOther.DataBind();
            //}
            //dtReport = dtAuthen.Select("MENU_NAME = 'รายงานระบบ'").CopyToDataTable();
            //GridViewHelper.BindGridView(ref dgvReport, dtReport);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCloseAll_Click(object sender, EventArgs e)
    {
        this.ActionAll(dgvWaterCheck, false);
        this.ActionAll(dgvOperationTransit, false);
        this.ActionAll(dgvTransitProblem, false);
        this.ActionAll(dgvDatabase, false);
        this.ActionAll(dgvCheckPerformance, false);
        this.ActionAll(dgvOther, false);
    }

    protected void cmdOpenAll_Click(object sender, EventArgs e)
    {
        this.ActionAll(dgvWaterCheck, true);
        this.ActionAll(dgvOperationTransit, true);
        this.ActionAll(dgvTransitProblem, true);
        this.ActionAll(dgvDatabase, true);
        this.ActionAll(dgvCheckPerformance, true);
        this.ActionAll(dgvOther, true);
    }

    private void ActionAll(GridView dgv, bool IsCheck)
    {
        try
        {
            CheckBox chk;
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                chk = (CheckBox)dgv.Rows[i].FindControl("chkVisible");
                if (chk != null)
                    chk.Checked = IsCheck;

                chk = (CheckBox)dgv.Rows[i].FindControl("chkRead");
                if (chk != null)
                    chk.Checked = IsCheck;

                chk = (CheckBox)dgv.Rows[i].FindControl("chkWrite");
                if (chk != null)
                    chk.Checked = IsCheck;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtSave = new DataTable();
            dtSave.Columns.Add("MENU_ID");
            dtSave.Columns.Add("VISIBLE");
            dtSave.Columns.Add("READ");
            dtSave.Columns.Add("WRITE");

            this.GetAuthenSelect(dgvWaterCheck, ref dtSave);
            this.GetAuthenSelect(dgvOperationTransit, ref dtSave);
            this.GetAuthenSelect(dgvTransitProblem, ref dtSave);
            this.GetAuthenSelect(dgvDatabase, ref dtSave);
            this.GetAuthenSelect(dgvCheckPerformance, ref dtSave);
            this.GetAuthenSelect(dgvOther, ref dtSave);
            this.GetAuthenSelect(dgvMobile, ref dtSave);


            AuthenBLL.Instance.AuthenInsertBLL(ddlUserGroup.SelectedValue, dtSave, UserID);
            alertSuccess(AuthenResources.SaveSuccess);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void GetAuthenSelect(GridView dgv, ref DataTable dtSave)
    {
        try
        {
            string MENU_ID_LEVEL1 = string.Empty;
            string MENU_ID_LEVEL2 = string.Empty;

            CheckBox chkVisible, chkRead, chkWrite;

            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                MENU_ID_LEVEL1 = dgv.DataKeys[i].Values["MENU_ID_LEVEL1"].ToString();
                MENU_ID_LEVEL2 = dgv.DataKeys[i].Values["MENU_ID_LEVEL2"].ToString();

                chkVisible = (CheckBox)dgv.Rows[i].FindControl("chkVisible");
                chkRead = (CheckBox)dgv.Rows[i].FindControl("chkRead");
                chkWrite = (CheckBox)dgv.Rows[i].FindControl("chkWrite");

                dtSave.Rows.Add(string.Equals(MENU_ID_LEVEL2, string.Empty) ? MENU_ID_LEVEL1 : MENU_ID_LEVEL2
                              , chkVisible.Checked ? 1 : 0
                              , chkRead.Checked ? 1 : 0
                              , chkWrite.Checked ? 1 : 0);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Authenticate.aspx");
    }

    protected void dgvWaterCheck_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvWaterCheck.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvWaterCheck, dtWaterCheck);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvOperationTransit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
             dgvOperationTransit.PageIndex = e.NewPageIndex;
             GridViewHelper.BindGridView(ref dgvOperationTransit, dtOperationTransit);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvTransitProblem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
             dgvTransitProblem.PageIndex = e.NewPageIndex;
             GridViewHelper.BindGridView(ref dgvTransitProblem, dtTransitProblem);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvDatabase_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvDatabase.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDatabase, dtDatabase);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvCheckPerformance_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvCheckPerformance.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvCheckPerformance, dtCheckPerformance);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvOther_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvDatabase.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvOther, dtOther);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvMobile_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvMobile.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvMobile, dtMobile);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
      
}