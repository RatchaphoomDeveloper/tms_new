﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;
using System.IO;

public partial class transportationother : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();

            Session["oNPROBLEMID"] = null;

            LogUser("23", "R", "เปิดดูข้อมูลหน้า ปัญหาอื่นๆ", "");
        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

        gvw.DataBind();
        lblCarCount.Text = ((DataView)sds.Select(DataSourceSelectArguments.Empty)).ToTable().Rows.Count + "";
    }

    //กด แสดงเลข Record
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "Search":
                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();

                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("ID1", "NPROBLEMID", "SDELIVERYNO", "SVENDORNAME", "FULLNAME", "SHEADREGISTERNO", "STRAILERREGISTERNO", "DPROBLEMDATE", "STATUS")
                    .Cast<object[]>()
                    .Select(s => new { ID1 = s[0].ToString(), NPROBLEMID = s[1].ToString(), SDELIVERYNO = s[2].ToString(), SVENDORNAME = s[3].ToString(), FULLNAME = s[4].ToString(), SHEADREGISTERNO = s[5].ToString(), STRAILERREGISTERNO = s[6].ToString(), DPROBLEMDATE = s[7].ToString(), STATUS = s[8].ToString() });

                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();
                    foreach (var l in ld)
                    {
                        string strsql = "DELETE FROM TPROBLEM WHERE NPROBLEMID = " + l.NPROBLEMID;
                        using(OracleCommand com = new OracleCommand(strsql,con))
                        {
                            com.ExecuteNonQuery();
                        }

                        string strsql2 = "DELETE FROM TPROBLEMDETAIL WHERE NPROBLEMID = " + l.NPROBLEMID;
                        using (OracleCommand com1 = new OracleCommand(strsql2, con))
                        {
                            com1.ExecuteNonQuery();
                        }

                        string strsql1 = "SELECT * FROM TPROBLEMIMPORTFILE WHERE NPROBLEMID = " + l.NPROBLEMID;
                        DataTable dt = new DataTable();
                        dt = CommonFunction.Get_Data(con, strsql1);

                        if (dt.Rows.Count > 0)
                        {

                            string strdel = "DELETE FROM TPROBLEMIMPORTFILE WHERE NPROBLEMID = " + l.NPROBLEMID;
                            using (OracleCommand com = new OracleCommand(strdel, con))
                            {
                                com.ExecuteNonQuery();
                            }

                            foreach (DataRow dr in dt.Rows)
                            {

                                string FilePath = dr["SFILEPATH"] + "";

                                if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                                {
                                    File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                                }
                            }
                        }
                    }
                }


                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();
                gvw.DataBind();

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "NPROBLEMID");
                string stmID = Convert.ToString(data);

                Session["oNPROBLEMID"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "transportation-other_add.aspx";

                break;

        }

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("transportation-other_add.aspx");
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
