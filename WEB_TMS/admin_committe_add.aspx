﻿<%@ Page Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="admin_committe_add.aspx.cs"
    Inherits="admin_committe_add" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" CausesValidation="False"
        HideContentOnCallback="False">
        <ClientSideEvents EndCallback="function(s,e){alert('pnl');eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0">
                    <tbody>
                        <tr>
                            <td width="14%">
                                &nbsp;</td>
                            <td width="4%">
                                &nbsp;</td>
                            <td width="22%">
                                &nbsp;</td>
                            <td width="9%">
                                &nbsp;</td>
                            <td width="21%">
                                &nbsp;</td>
                            <td width="20%" align="center">
                                &nbsp;</td>
                            <td width="10%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <span class="style23">ชื่อคณะกรรมการ</span></td>
                            <td colspan="4">
                                <dx:ASPxTextBox ID="txtsnentencername" ClientInstanceName="txtsnentencername" runat="server"
                                    NullText="ชื่อกลุ่มคณะกรรมการ" Width="350px" CssClass="dxeLineBreakFix">
                                    <ValidationSettings CausesValidation="true" ErrorDisplayMode="ImageWithTooltip" Display="Dynamic"
                                        ValidationGroup="submit">
                                        <RequiredField ErrorText="กรุณาระบุ!" IsRequired="true" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                            <td align="center">
                                <asp:Literal ID="ltr_SSENTENCERCODE" runat="server" Visible="false"></asp:Literal>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <span class="style23">วาระดำรงตำแหน่ง</span></td>
                            <td colspan="4">
                                <table>
                                    <tr>
                                        <td>
                                            <dx:ASPxDateEdit ID="detdbeginterm" runat="server" ClientInstanceName="detdbeginterm"
                                                CssClass="dxeLineBreakFix" SkinID="xdte">
                                                <ValidationSettings Display="Dynamic" ValidationGroup="submit" ErrorDisplayMode="ImageWithTooltip"
                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                        <td>
                                            -</td>
                                        <td>
                                            <dx:ASPxDateEdit ID="detdendterm" runat="server" ClientInstanceName="detdendterm"
                                                CssClass="dxeLineBreakFix" SkinID="xdte">
                                                <ValidationSettings Display="Dynamic" ValidationGroup="submit" ErrorDisplayMode="ImageWithTooltip"
                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <span class="style23">ประเภท</span></td>
                            <td colspan="6">
                                <dx:ASPxRadioButtonList ID="radType" runat='server' ClientInstanceName="rblcActiveTerm"
                                    RepeatDirection="Horizontal">
                                    <Items>
                                        <dx:ListEditItem Selected="true" Text="คณะกรรมการพิจารณาโทษ" Value="1" />
                                        <dx:ListEditItem Text="คณะกรรมการพิจารณาอุทธรณ์" Value="2" />
                                    </Items>
                                    <Border BorderStyle="None" BorderWidth="0px" />
                                </dx:ASPxRadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="style23">สถานะการหมดวาระ</span></td>
                            <td colspan="6">
                                <dx:ASPxRadioButtonList ID="rblcActiveTerm" runat='server' ClientInstanceName="rblcActiveTerm"
                                    RepeatDirection="Horizontal">
                                    <Items>
                                        <dx:ListEditItem Selected="true" Text="Active" Value="0" />
                                        <dx:ListEditItem Text="InActive" Value="1" />
                                    </Items>
                                    <Border BorderStyle="None" BorderWidth="0px" />
                                </dx:ASPxRadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td height="25" colspan="3">
                                <%--<span class="style23">รายชื่อคณะกรรมการพิจารณาอุทธรณ์ </span></td>--%>
                            <td align="center">
                                &nbsp;</td>
                            <td align="center">
                                &nbsp;</td>
                            <td align="center">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <dx:ASPxGridView ID="gvwSentencer" SkinID="_gvw" ClientInstanceName="gvwSentencer"
                                    KeyFieldName="SSENTENCERID" AutoGenerateColumns="False" Width="100%" runat="server"
                                    OnAfterPerformCallback="gvwSentencer_AfterPerformCallback" OnCustomButtonCallback="gvwSentencer_CustomButtonCallback">
                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
                                    <Columns>
                                        <dx:GridViewCommandColumn Width="3%" VisibleIndex="0">
                                            <CustomButtons>
                                                <dx:GridViewCommandColumnCustomButton ID="cmdBtnDel" Text="ลบ">
                                                </dx:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                            <%--<DeleteButton Visible="true" Text="ลบ">
                                            </DeleteButton>--%>
                                            <FooterTemplate>
                                                <dx:ASPxButton ID="btnadd" runat="server" SkinID="_add">
                                                    <ClientSideEvents Click="function(s,e){ gvwSentencer.PerformCallback('btnadd$'); }" />
                                                </dx:ASPxButton>
                                            </FooterTemplate>
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn FieldName="NLIST" Caption="ลำดับแสดง" VisibleIndex="1"
                                            Width="3%">
                                            <DataItemTemplate>
                                                <dx:ASPxTextBox ID="txtnlist" runat="server" Text='<%# Eval("NLIST")+"" %>' Width="95%"
                                                    MaxLength="2">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="submit">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ!" />
                                                        <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CODE" Caption="รหัสพนักงาน" VisibleIndex="2"
                                            Width="10%">
                                            <DataItemTemplate>
                                                <dx:ASPxTextBox ID="txtcode" runat="server" Text='<%# Eval("CODE")+"" %>' Width="95%">
                                                 <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="submit">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ!" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SIFLNAME" Caption="ชื่อ - สกุล" VisibleIndex="3"
                                            Width="22%">
                                            <DataItemTemplate>
                                                <dx:ASPxTextBox ID="txtiflname" runat="server" Text='<%# Eval("SIFLNAME")+"" %>'
                                                    Width="95%">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="submit">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ!" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SPOSITION" Caption="ตำแหน่ง" VisibleIndex="4"
                                            Width="15%">
                                            <DataItemTemplate>
                                                <dx:ASPxTextBox ID="txtsposition" runat="server" Text='<%# Eval("SPOSITION")+"" %>'
                                                    Width="95%">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="submit">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ!" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SJOB" Caption="ตำแหน่งในคณะทำงาน" VisibleIndex="5"
                                            Width="16%">
                                            <DataItemTemplate>
                                                <dx:ASPxTextBox ID="txtsjob" runat="server" Text='<%# Eval("SJOB")+"" %>' Width="95%">
                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="submit">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ!" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CEXPIRE" Caption="Active" VisibleIndex="6"
                                            Width="3%">
                                            <DataItemTemplate>
                                                <dx:ASPxCheckBox ID="ckbCEXPIRE" runat="server" title='<%# Eval("CEXPIRE")+""=="0"?"หมดวาระ":"อยู่ในวาระ" %>'
                                                    Checked='<%# Eval("CEXPIRE")+""=="0"?false:true %>'>
                                                </dx:ASPxCheckBox>
                                            </DataItemTemplate>
                                            <FooterTemplate>
                                                <dx:ASPxButton ID="btnsubmit" runat="server" SkinID="_submit" ValidationGroup="submit"
                                                    CssClass="dxeLineBreakFix" CausesValidation="true">
                                                    <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('submit')) return false; gvwSentencer.PerformCallback('btnsubmit$'); }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_cancel" CausesValidation="false"
                                                    CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){ gvwSentencer.PerformCallback('btnCancel$'); }" />
                                                </dx:ASPxButton>
                                            </FooterTemplate>
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn> 
                                        <dx:GridViewDataTextColumn Caption="sKeyID" FieldName="NLIST" Visible="false" />
                                        <dx:GridViewDataTextColumn Caption="SSENTENCERCODE" FieldName="SSENTENCERCODE" Visible="false" />
                                        <dx:GridViewDataTextColumn Caption="SSENTENCERID" FieldName="SSENTENCERID" Visible="false" />
                                        <dx:GridViewDataTextColumn Caption="SSENTENCERNAME" FieldName="SSENTENCERNAME" Visible="false" />
                                        <dx:GridViewDataTextColumn Caption="DSENTENCES" FieldName="DSENTENCES" Visible="false" />
                                        <dx:GridViewDataTextColumn Caption="DBEGINTERM" FieldName="DBEGINTERM" Visible="false" />
                                        <dx:GridViewDataTextColumn Caption="DENDTERM" FieldName="DENDTERM" Visible="false" />
                                    </Columns>
                                    <SettingsEditing EditFormColumnCount="6" Mode="Inline" />
                                    <SettingsPager Mode="ShowAllRecords" />
                                    <Settings ShowFooter="true" />
                                    <Styles>
                                        <Header HorizontalAlign="Center">
                                        </Header>
                                    </Styles>
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="sdsSentencer" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    OnInserting="sdsSentencer_Modifying" OnUpdating="sdsSentencer_Modifying" CancelSelectOnNullParameter="False"
                                    EnableCaching="True" CacheKeyDependency="ckdSentencer" SelectCommand="SELECT SSENTENCERCODE,SSENTENCERID,SSENTENCERNAME,DSENTENCES,DBEGINTERM,DENDTERM,CODE,INAME,FNAME,LNAME,NVL(FNAME,'') sIFLName,SPOSITION,SJOB,CACTIVE CEXPIRE,NLIST,CEXPIRETERM, SENTENCER_TYPE_ID FROM TSENTENCER 
                                    WHERE 1=1 AND SSENTENCERCODE LIKE '%'|| NVL(:SSENTENCERCODE,SSENTENCERCODE) ||'%' ">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ltr_SSENTENCERCODE" PropertyName="Text" Name=":SSENTENCERCODE"
                                            Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="center">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>--%>
