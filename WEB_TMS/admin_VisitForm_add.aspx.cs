﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_Entity;

public partial class admin_VisitForm_add_ : PageBase
{
    string _conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string PagePermission = "admin_VisitForm_lst.aspx";
    #region " Prop "
    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }
    protected TTYPEOFVISITFORM DataTYPEOFVISITFORM
    {
        get { return (TTYPEOFVISITFORM)ViewState[this.ToString() + "TTYPEOFVISITFORM"]; }
        set { ViewState[this.ToString() + "TTYPEOFVISITFORM"] = value; }
    }

    protected PAGE_ACTION PageAction
    {
        get { return (PAGE_ACTION)ViewState[this.ToString() + "PageAction"]; }
        set { ViewState[this.ToString() + "PageAction"] = value; }
    }
    protected string ID_CONFIG_VALUE
    {
        get { return ViewState[this.ToString() + "ID_CONFIG_VALUE"].ToString(); }
        set { ViewState[this.ToString() + "ID_CONFIG_VALUE"] = value; }
    }

    protected string QSerachGroupID
    {
        get { return ViewState[this.ToString() + "QSerachGroupID"].ToString(); }
        set { ViewState[this.ToString() + "QSerachGroupID"] = value; }
    }

    protected string QGroupID
    {
        get { return ViewState[this.ToString() + "QGroupID"].ToString(); }
        set { ViewState[this.ToString() + "QGroupID"] = value; }
    }
    protected string QDocID
    {
        get { return ViewState[this.ToString() + "QDocID"].ToString(); }
        set { ViewState[this.ToString() + "QDocID"] = value; }
    }
    protected string QItemID
    {
        get { return ViewState[this.ToString() + "QItemID"].ToString(); }
        set { ViewState[this.ToString() + "QItemID"] = value; }
    }
    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        try
        {
            if (!IsPostBack)
            {
                user_profile = SessionUtility.GetUserProfileSession();
                CheckPermission();

                QGroupID = string.Empty;
                QItemID = string.Empty;
                QSerachGroupID = string.Empty;
                ID_CONFIG_VALUE = string.Empty;
                if (Session["EditNTYPEVISITFORMID"] != null)
                {
                    PageAction = PAGE_ACTION.EDIT;
                    QDocID = Session["EditNTYPEVISITFORMID"].ToString();
                    Session["EditNTYPEVISITFORMID"] = null;
                    BindEdit();
                }
                else if (Session["ViewNTYPEVISITFORMID"] != null)
                {
                    PageAction = PAGE_ACTION.VIEW;
                    QDocID = Session["ViewNTYPEVISITFORMID"].ToString();
                    Session["ViewNTYPEVISITFORMID"] = null;
                    BindEdit();
                    SetViewMode();
                }
                else
                {
                    PageAction = PAGE_ACTION.ADD;
                    QDocID = string.Empty;
                    ((HtmlAnchor)this.FindControlRecursive(Page, "ProcessTab")).Visible = false;
                    ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = false;
                    TabItem.Visible = false;
                    TabGroup.Visible = false;
                }
                LoadMain();
                InitForm();
                this.AssignAuthen();
            }
        }
        catch (Exception ex)
        {
            Session["ViewNTYPEVISITFORMID"] = null;
            Session["EditNTYPEVISITFORMID"] = null;
            alertFail(ex.Message);
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnAddForm.Enabled = false;
                btnGroupAdd.Enabled = false;
                btnItemAdd.Enabled = false;
               // grvMain.Columns[5].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            GotoDefault();
            return;
        }


    }
    public void SetViewMode()
    {
        txtFormName.Enabled = false;
        txtGroupID.Enabled = false;
        txtGroupName.Enabled = false;
        txtItemName.Enabled = false;
        txtItemPoint.Enabled = false;
        txtRdoPoint.Enabled = false;

        btnAdd.Enabled = false;
        btnAddForm.Enabled = false;
        btnClear.Enabled = false;
        btnGroupAdd.Enabled = false;
        btnGroupClear.Enabled = false;
        btnItemAdd.Enabled = false;
        btnItemClear.Enabled = false;

        btnAdd.Visible = false;
        btnAddForm.Visible = false;
        btnClear.Visible = false;
        btnGroupAdd.Visible = false;
        btnGroupClear.Visible = false;
        btnItemAdd.Visible = false;
        btnItemClear.Visible = false;

        ddlRdo.Enabled = false;
        ddlGroupName.Enabled = false;

        rdoGroupStatus.Enabled = false;
        rdoItemStatus.Enabled = false;
        rdoOptionStatus.Enabled = false;
        rdoStatus.Enabled = false;
        chkShowInList.Enabled = false;

    }
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null)
                return controlToReturn;
        }
        return null;
    }
    public void BindEdit()
    {
        string _err = string.Empty;
        DataTYPEOFVISITFORM = new QuestionnaireBLL().getTTYPEOFVISITFORM(ref _err, int.Parse(QDocID));
        if (DataTYPEOFVISITFORM != null)
        {
            txtFormName.Text = DataTYPEOFVISITFORM.STYPEVISITFORMNAME;
            chkShowInList.Checked = DataTYPEOFVISITFORM.SHOWINLIST == "1" ? true : false;
            ListItem sel = rdoStatus.Items.FindByValue(DataTYPEOFVISITFORM.CACTIVE);
            if (sel != null)
            {
                rdoStatus.ClearSelection();
                sel.Selected = true;
            }
        }

        if (DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST == null)
        {
            DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST = new List<TTYPEOFVISITFORMLIST>();
        }
    }
    public void InitForm()
    {
        string _err = string.Empty;
        ddlRdo.DataTextField = "CONFIG_NAME";
        ddlRdo.DataValueField = "CONFIG_VALUE";
        ddlRdo.DataSource = new QuestionnaireBLL().GetDropDownRadioFromConfig(ref _err);
        ddlRdo.DataBind();
    }
    public bool ValidateBeforeAdd()
    {
        if (string.IsNullOrEmpty(txtRdoPoint.Text.Trim()))
        {
            alertFail("กรุณาระบุคะแนน");
            return false;
        }

        double RdoPoint = 0;
        bool IsDecimal = double.TryParse(txtRdoPoint.Text, out RdoPoint);
        if ((!IsDecimal) || ((RdoPoint != 0) && (RdoPoint <= 0.0009)) || (RdoPoint > 999.999))
        {
            alertFail("ข้อมูลคะแนนไม่ถูกต้อง (0.001-999.999)");
            return false;
        }

        return true;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (!ValidateBeforeAdd()) return;
        string _err = string.Empty;
        if (string.IsNullOrEmpty(ID_CONFIG_VALUE)) //เพิ่มอันใหม่
        {
            if (!string.IsNullOrEmpty(QDocID))
            {
                if (new QuestionnaireBLL().CheckDuplicateRadio(ref _err, int.Parse(QDocID), ddlRdo.SelectedValue.ToString()))
                {
                    alertFail("ไม่สามารถเพิ่มข้อมูลคะแนนซ้ำกันได้");
                    return;
                }
            }

            //เชคข้อมูลในตาราง
            if (DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST != null)
            {
                TTYPEOFVISITFORMLIST foundRows = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST.Where(x => x.CONFIG_VALUE == ddlRdo.SelectedValue.ToString()).FirstOrDefault();
                if (foundRows != null)
                {
                    alertFail("ไม่สามารถเพิ่มข้อมูลคะแนนซ้ำกันได้");
                    return;
                }
            }

            TTYPEOFVISITFORMLIST ItmAdd = new TTYPEOFVISITFORMLIST();
            ItmAdd.NTYPEVISITLISTID = int.Parse(ddlRdo.SelectedItem.Value);
            ItmAdd.NTYPEVISITFORMID = 0; //Mark Add new
            ItmAdd.CONFIG_VALUE = ddlRdo.SelectedValue;
            ItmAdd.STYPEVISITLISTNAME = ddlRdo.SelectedItem.Text;
            ItmAdd.NTYPEVISITLISTSCORE = decimal.Parse(txtRdoPoint.Text.Trim());
            ItmAdd.CACTIVE = rdoOptionStatus.SelectedValue;
            DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST.Add(ItmAdd);
            grvMain.DataSource = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST;
            grvMain.DataBind();
        }
        else // Edit อันเก่า
        {
            TTYPEOFVISITFORMLIST itm = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST.Where(x => x.CONFIG_VALUE == ID_CONFIG_VALUE).FirstOrDefault();
            if (itm != null)
            {
                itm.NTYPEVISITLISTSCORE = decimal.Parse(txtRdoPoint.Text);
                itm.CACTIVE = rdoOptionStatus.SelectedValue;
                grvMain.DataSource = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST;
                grvMain.DataBind();

                ID_CONFIG_VALUE = string.Empty;
                ddlRdo.Enabled = true;
            }
        }
        txtRdoPoint.Text = string.Empty;
        rdoOptionStatus.ClearSelection();
        rdoOptionStatus.SelectedValue = "1";
        ddlRdo.ClearSelection();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtRdoPoint.Text = "0.0";
        txtFormName.Text = string.Empty;
        ddlRdo.Enabled = true;
        ddlRdo.ClearSelection();
        ddlRdo.SelectedIndex = 0;
    }
    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RowSelect")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            int _keyCONFIG_VALUE = int.Parse(DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST[rowIndex.Value].CONFIG_VALUE.ToString());
            string C_ACTIVE = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST[rowIndex.Value].CACTIVE.ToString();
            if (_keyCONFIG_VALUE != null)
            {
                txtRdoPoint.Text = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST[rowIndex.Value].NTYPEVISITLISTSCORE.ToString();
                ID_CONFIG_VALUE = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST[rowIndex.Value].CONFIG_VALUE.ToString();
                ListItem sel = ddlRdo.Items.FindByValue(ID_CONFIG_VALUE);
                if (sel != null)
                {
                    ddlRdo.ClearSelection();
                    sel.Selected = true;
                    ddlRdo.Enabled = false;
                }
                rdoOptionStatus.ClearSelection();
                ListItem rdo = rdoOptionStatus.Items.FindByValue(C_ACTIVE);
                if (rdo != null)
                    rdo.Selected = true;
                else
                    rdoOptionStatus.SelectedIndex = 0;
            }
        }
    }
    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk = e.Row.Cells[1].FindControl("chkActive") as CheckBox;
            if (chk != null)
            {
                string CACTIVE = DataBinder.Eval(e.Row.DataItem, "CACTIVE").ToString();
                chk.Checked = CACTIVE == "1" ? true : false;
            }
        }
    }
    public void LoadMain()
    {
        string _err = string.Empty;
        if (string.IsNullOrEmpty(QDocID))
        {
            DataTYPEOFVISITFORM = new TTYPEOFVISITFORM();
            DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST = new List<TTYPEOFVISITFORMLIST>();
            DataTYPEOFVISITFORM.DT_TGROUPOFVISITFORM = null;
            DataTYPEOFVISITFORM.DT_GROUPDLL = null;
            DataTYPEOFVISITFORM.DT_ITEMS = null;
        }

        grvMain.DataSource = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST;
        grvMain.DataBind();
        GrvSub.DataSource = DataTYPEOFVISITFORM.DT_TGROUPOFVISITFORM;
        GrvSub.DataBind();

        ddlGroupName.DataValueField = "NGROUPID";
        ddlGroupName.DataTextField = "SGROUPNAME";

        ddlGroupName.DataSource = DataTYPEOFVISITFORM.DT_GROUPDLL;
        ddlGroupName.DataBind();
        ddlGroupName.ClearSelection();

        ddlGroupNameSearch.DataValueField = "NGROUPID";
        ddlGroupNameSearch.DataTextField = "SGROUPNAME";
        ddlGroupNameSearch.DataSource = DataTYPEOFVISITFORM.DT_GROUPDLL;
        ddlGroupNameSearch.DataBind();
        ddlGroupNameSearch.ClearSelection();
        ddlGroupNameSearch.Items.Insert(0, new ListItem() { Text = "- เลือกทั้งหมด -", Value = "0" });

        if (DataTYPEOFVISITFORM.DT_GROUPDLL != null)
        {
            if (DataTYPEOFVISITFORM.DT_GROUPDLL.Rows.Count > 0)
            {
                ddlGroupNameSearch.ClearSelection();
                ddlGroupNameSearch.SelectedIndex = 1;
                ddlGroupName_SelectedIndexChanged(null, null);
            }
        }
    }
    protected void GrvSub_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RowSelect")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            string _keyNGROUPID = GrvSub.DataKeys[rowIndex.Value]["NGROUPID"].ToString();
            string _keySGROUPNAME = GrvSub.DataKeys[rowIndex.Value]["SGROUPNAME"].ToString();
            string _keyCACTIVE = GrvSub.DataKeys[rowIndex.Value]["CACTIVE"].ToString();
            string _keyGROUP_INDEX = GrvSub.DataKeys[rowIndex.Value]["GROUP_INDEX"].ToString();

            txtGroupName.Text = _keySGROUPNAME;
            rdoGroupStatus.ClearSelection();
            ListItem rdo = rdoGroupStatus.Items.FindByValue(_keyCACTIVE);
            if (rdo != null) rdo.Selected = true;
            else rdoGroupStatus.SelectedIndex = 0;
            txtGroupID.Text = _keyNGROUPID;
            txtGROUP_INDEX.Text = _keyGROUP_INDEX;
            QGroupID = _keyNGROUPID;
            //uplGroup.Update();
        }
    }
    protected void GrvSub_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk = e.Row.Cells[1].FindControl("chkActive") as CheckBox;
            if (chk != null)
            {
                string CACTIVE = DataBinder.Eval(e.Row.DataItem, "CACTIVE").ToString();
                chk.Checked = CACTIVE == "1" ? true : false;
            }
        }
    }
    protected void btnAddForm_Click(object sender, EventArgs e)
    {
        string _err = string.Empty;
        DataTYPEOFVISITFORM.CACTIVE = rdoStatus.SelectedValue;
        DataTYPEOFVISITFORM.STYPEVISITFORMNAME = txtFormName.Text.Trim();
        DataTYPEOFVISITFORM.SHOWINLIST = chkShowInList.Checked ? "1" : "0";
        if (!string.IsNullOrEmpty(QDocID)) // Check In Base
        {
            if (new QuestionnaireBLL().CheckDuplicateFormName(ref _err, int.Parse(QDocID), txtFormName.Text.Trim()))
            {
                alertFail("ไม่สามารถเพิ่มข้อมูลฟอร์มซ้ำกันได้");
                return;
            }
        }
        else
        {
            if (new QuestionnaireBLL().CheckDuplicateFormName(ref _err, 0, txtFormName.Text.Trim()))
            {
                alertFail("ไม่สามารถเพิ่มข้อมูลฟอร์มซ้ำกันได้");
                return;
            }
        }

        if (string.IsNullOrEmpty(txtFormName.Text.Trim()))
        {
            alertFail("กรุณาระบุชื่อแบบฟอร์ม");
            return;
        }

        if (!string.IsNullOrEmpty(QDocID))
        {
            // Edit
            using (OracleConnection con = new OracleConnection(_conn))
            {

                if (con.State == ConnectionState.Closed) con.Open();
                _err = string.Empty;
                using (OracleTransaction tran = con.BeginTransaction())
                {
                    try
                    {
                        string sql = string.Empty;
                        /// INSERT NEW LIST
                        var _new = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST.Where(x => x.NTYPEVISITFORMID == 0).ToList();
                        if (_new.Count > 0)
                        {
                            foreach (var itm in _new)
                            {
                                sql = @"INSERT INTO TTYPEOFVISITFORMLIST ( NTYPEVISITLISTID,NTYPEVISITFORMID, STYPEVISITLISTNAME, NTYPEVISITLISTSCORE, CONFIG_VALUE,CACTIVE) 
                            VALUES ( :NTYPEVISITLISTID  ,:NTYPEVISITFORMID, :STYPEVISITLISTNAME, :NTYPEVISITLISTSCORE, :CONFIG_VALUE, :CACTIVE )";



                                using (OracleCommand com1 = new OracleCommand(sql, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NTYPEVISITLISTID", OracleType.Number).Value = itm.CONFIG_VALUE;
                                    com1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = QDocID;
                                    com1.Parameters.Add(":STYPEVISITLISTNAME", OracleType.VarChar).Value = itm.STYPEVISITLISTNAME;
                                    com1.Parameters.Add(":NTYPEVISITLISTSCORE", OracleType.VarChar).Value = itm.NTYPEVISITLISTSCORE;
                                    com1.Parameters.Add(":CONFIG_VALUE", OracleType.Number).Value = itm.CONFIG_VALUE;
                                    com1.Parameters.Add(":CACTIVE", OracleType.Number).Value = itm.CACTIVE;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }

                        sql = @"UPDATE TTYPEOFVISITFORMLIST SET STYPEVISITLISTNAME=:STYPEVISITLISTNAME, NTYPEVISITLISTSCORE=:NTYPEVISITLISTSCORE, CONFIG_VALUE=:CONFIG_VALUE,CACTIVE=:CACTIVE WHERE NTYPEVISITLISTID=:NTYPEVISITLISTID and NTYPEVISITFORMID=:NTYPEVISITFORMID";
                        var _edit = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST.Where(x => x.NTYPEVISITFORMID != 0).ToList();
                        foreach (var itm in _edit)
                        {
                            using (OracleCommand com1 = new OracleCommand(sql, con))
                            {
                                com1.Transaction = tran;
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":STYPEVISITLISTNAME", OracleType.VarChar).Value = itm.STYPEVISITLISTNAME;
                                com1.Parameters.Add(":NTYPEVISITLISTSCORE", OracleType.VarChar).Value = itm.NTYPEVISITLISTSCORE;
                                com1.Parameters.Add(":CONFIG_VALUE", OracleType.VarChar).Value = itm.CONFIG_VALUE;
                                com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = itm.CACTIVE;
                                com1.Parameters.Add(":NTYPEVISITLISTID", OracleType.Number).Value = itm.NTYPEVISITLISTID;
                                com1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = QDocID;
                                com1.ExecuteNonQuery();
                            }
                        }

                        sql = @"UPDATE TTYPEOFVISITFORM  SET  STYPEVISITFORMNAME=:STYPEVISITFORMNAME ,SUPDATE=:SUPDATE,CACTIVE=:CACTIVE,SHOWINLIST=:SHOWINLIST,DUPDATE=sysdate WHERE NTYPEVISITFORMID =:NTYPEVISITFORMID";
                        using (OracleCommand com1 = new OracleCommand(sql, con))
                        {
                            com1.Transaction = tran;
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":STYPEVISITFORMNAME", OracleType.VarChar).Value = DataTYPEOFVISITFORM.STYPEVISITFORMNAME.ToString();
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = DataTYPEOFVISITFORM.CACTIVE;
                            com1.Parameters.Add(":SHOWINLIST", OracleType.VarChar).Value = DataTYPEOFVISITFORM.SHOWINLIST.ToString();
                            com1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = QDocID;
                            com1.ExecuteNonQuery();
                        }
                        tran.Commit();
                        alertSuccess(" บันทึกข้อมูลเรียบร้อย ", "admin_VisitForm_lst.aspx");

                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        alertFail(ex.Message);
                    }
                }
            }
        }
        else
        {
            int? refID = 0;
            // Add
            using (OracleConnection con = new OracleConnection(_conn))
            {
                if (con.State == ConnectionState.Closed) con.Open();
                _err = string.Empty;
                string sql = string.Empty;
                using (OracleTransaction tran = con.BeginTransaction())
                {
                    try
                    {

                        refID = new QuestionnaireBLL().GetTYPEOFVISITFORMID(ref _err);
                        foreach (var itm in DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST)
                        {
                            sql = @"INSERT INTO TTYPEOFVISITFORMLIST ( NTYPEVISITLISTID,NTYPEVISITFORMID, STYPEVISITLISTNAME, NTYPEVISITLISTSCORE, CONFIG_VALUE,CACTIVE) 
                            VALUES ( :NTYPEVISITLISTID ,:NTYPEVISITFORMID, :STYPEVISITLISTNAME, :NTYPEVISITLISTSCORE, :CONFIG_VALUE, :CACTIVE )";
                            using (OracleCommand com1 = new OracleCommand(sql, con))
                            {
                                com1.Transaction = tran;
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":NTYPEVISITLISTID", OracleType.Number).Value = itm.CONFIG_VALUE;
                                com1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = refID;
                                com1.Parameters.Add(":STYPEVISITLISTNAME", OracleType.VarChar).Value = itm.STYPEVISITLISTNAME;
                                com1.Parameters.Add(":NTYPEVISITLISTSCORE", OracleType.VarChar).Value = itm.NTYPEVISITLISTSCORE;
                                com1.Parameters.Add(":CONFIG_VALUE", OracleType.Number).Value = itm.CONFIG_VALUE;
                                com1.Parameters.Add(":CACTIVE", OracleType.Number).Value = itm.CACTIVE;
                                com1.ExecuteNonQuery();
                            }
                        }

                        sql = @"INSERT INTO TTYPEOFVISITFORM (NTYPEVISITFORMID,STYPEVISITFORMNAME,DCREATE,SCREATE,CACTIVE,SHOWINLIST,DUPDATE)
                          VALUES (:NTYPEVISITFORMID,:STYPEVISITFORMNAME,SYSDATE,:SCREATE,:CACTIVE,:SHOWINLIST,SYSDATE)";
                        using (OracleCommand com1 = new OracleCommand(sql, con))
                        {
                            com1.Transaction = tran;
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = refID;
                            com1.Parameters.Add(":STYPEVISITFORMNAME", OracleType.VarChar).Value = DataTYPEOFVISITFORM.STYPEVISITFORMNAME.ToString();
                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = DataTYPEOFVISITFORM.CACTIVE;
                            com1.Parameters.Add(":SHOWINLIST", OracleType.VarChar).Value = DataTYPEOFVISITFORM.SHOWINLIST.ToString();
                            com1.ExecuteNonQuery();
                        }

                        tran.Commit();
                        alertSuccess(" บันทึกข้อมูลเรียบร้อย ", "admin_VisitForm_lst.aspx");
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        alertFail(ex.Message);
                    }
                }
            }
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_VisitForm_lst.aspx");
    }
    protected void btnItemClear_Click(object sender, EventArgs e)
    {
        txtItemName.Text = string.Empty;
        txtItemPoint.Text = string.Empty;
        QGroupID = string.Empty;
        QItemID = string.Empty;
        ddlGroupName.Enabled = true;
        ddlGroupName.ClearSelection();
        rdoItemStatus.ClearSelection();
        rdoItemStatus.SelectedIndex = 0;
        txtFORM_CODE.Text = string.Empty;
    }
    protected void btnItemAdd_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtItemName.Text))
        {
            alertFail("กรุณาระบุหัวข้อคำถาม");
            return;
        }

        if (string.IsNullOrEmpty(ddlGroupName.SelectedValue) || ddlGroupName.SelectedValue == "ALL")
        {
            alertFail("กรุณาระบุชื่อหมวด/กลุ่ม");
            return;
        }

        double ItemPoint = 0;
        bool IsDecimal = double.TryParse(txtItemPoint.Text, out ItemPoint);
        if ((!IsDecimal) || ((ItemPoint != 0) && (ItemPoint <= 0.009)) || (ItemPoint > 999.99))
        {
            alertFail("ข้อมูล Weight ไม่ถูกต้อง (0.01-999.99)");
            txtItemPoint.Focus();
            return;
        }

        double ItemCode = 0;
        txtFORM_CODE.Text = txtFORM_CODE.Text.Trim();
        IsDecimal = double.TryParse(txtFORM_CODE.Text, out ItemCode);
        if ((!IsDecimal) || ((ItemCode != 0) && (ItemCode <= 0.09)) || (ItemCode > 99.99))
        {
            alertFail("ข้อมูลรหัสคำถามไม่ถูกต้อง (0.01-999.99)");
            txtFORM_CODE.Focus();
            return;
        }

        try
        {
            VisitForm itm = new VisitForm();
            itm.CACTIVE = rdoItemStatus.SelectedValue;
            itm.NGROUPID = decimal.Parse(ddlGroupName.SelectedValue.ToString());
            if (!string.IsNullOrEmpty(QItemID)) itm.NVISITFORMID = int.Parse(QItemID); // Edit
            itm.NTYPEVISITFORMID = decimal.Parse(QDocID);
            itm.NWEIGHT = txtItemPoint.Text.Trim();
            itm.SCREATE = Session["UserID"].ToString();
            itm.SUPDATE = Session["UserID"].ToString();
            itm.SVISITFORMNAME = txtItemName.Text.Trim();
            itm.FORM_CODE = txtFORM_CODE.Text.Trim();
            string _err = string.Empty;
            bool result = new QuestionnaireBLL().ActionItemForm(ref _err, itm);
            if (result)
            {
                alertSuccess(" บันทึกข้อมูลเรียบร้อย ");
                BindEdit();
                btnItemClear_Click(null, null);
                ddlGroupName_SelectedIndexChanged(null, null);
            }
            else
            {
                alertFail(_err);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnGroupAdd_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtGroupName.Text.Trim()))
        {
            alertFail("กรุณาระบุ ชื่อหมวด/กลุ่ม");
            return;
        }

        GroupOfVisitform itm = new GroupOfVisitform() { CACTIVE = rdoGroupStatus.SelectedValue, NTYPEVISITFORMID = QDocID, SCREATE = Session["UserID"].ToString(), SGROUPNAME = txtGroupName.Text.Trim(), SUPDATE = Session["UserID"].ToString(), NGROUPID = "0" };

        if (!string.IsNullOrEmpty(QGroupID))
        {
            itm.NGROUPID = QGroupID.ToString();
        }
        string _err = string.Empty;
        try
        {
            bool result = new QuestionnaireBLL().ActionGroupForm(ref _err, itm);
            if (!result)
            {
                alertFail(_err);
            }
            else
            {
                alertSuccess(" บันทึกข้อมูลเรียบร้อย ");
                BindEdit();
                LoadMain();
                btnGroupClear_Click(null, null);
                if (!string.IsNullOrEmpty(QSerachGroupID))
                {
                    ListItem _ol = ddlGroupNameSearch.Items.FindByValue(QSerachGroupID);
                    if (_ol != null)
                    {
                        ddlGroupNameSearch.ClearSelection();
                        _ol.Selected = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnGroupClear_Click(object sender, EventArgs e)
    {
        QGroupID = string.Empty;
        rdoGroupStatus.ClearSelection();
        rdoGroupStatus.SelectedIndex = 0;
        txtGroupName.Text = string.Empty;
        txtGroupID.Text = "-";
        txtGROUP_INDEX.Text = "-";
    }
    protected void grvItem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridView grv = sender as GridView;
        if (e.CommandName == "RowSelect")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            string _keyNGROUPID = grv.DataKeys[rowIndex.Value]["NGROUPID"].ToString();
            string _keySVISITFORMNAME = grv.DataKeys[rowIndex.Value]["SVISITFORMNAME"].ToString();
            string _keyCACTIVE = grv.DataKeys[rowIndex.Value]["CACTIVE"].ToString();
            string _keyIndex = grv.DataKeys[rowIndex.Value]["ITEM_INDEX"].ToString();
            string _keyPoint = grv.DataKeys[rowIndex.Value]["NWEIGHT"].ToString();
            string _keyNVISITFORMID = grv.DataKeys[rowIndex.Value]["NVISITFORMID"].ToString();
            string _keySGROUPNAME = grv.DataKeys[rowIndex.Value]["SGROUPNAME"].ToString();
            string _keyFORM_CODE = grv.DataKeys[rowIndex.Value]["FORM_CODE"].ToString();

            QItemID = _keyNVISITFORMID;
            txtItemName.Text = _keySVISITFORMNAME;
            txtItemPoint.Text = _keyPoint;
            txtFORM_CODE.Text = _keyFORM_CODE;
            rdoItemStatus.ClearSelection();
            ListItem rdo = rdoItemStatus.Items.FindByValue(_keyCACTIVE);
            if (rdo != null)
                rdo.Selected = true;
            else
                rdoItemStatus.SelectedIndex = 0;

            ddlGroupName.ClearSelection();
            ddlGroupName.Enabled = false;
            ListItem ddlGroup = ddlGroupName.Items.FindByValue(_keyNGROUPID);
            if (ddlGroup != null)
                ddlGroup.Selected = true;
            else
            {
                ddlGroupName.Items.Add(new ListItem() { Value = _keyNGROUPID, Selected = true, Text = _keySGROUPNAME });
            }
        }
    }
    protected void grvItem_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk = e.Row.Cells[1].FindControl("chkActive") as CheckBox;
            if (chk != null)
            {
                string CACTIVE = DataBinder.Eval(e.Row.DataItem, "CACTIVE").ToString();
                chk.Checked = CACTIVE == "1" ? true : false;
            }
        }
    }

    #region " Disable Column "
    protected void grvMain_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (PageAction == PAGE_ACTION.VIEW) e.Row.Cells[4].Visible = false;
        }
        catch (Exception)
        {

        }

    }
    protected void grvItem_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (PageAction == PAGE_ACTION.VIEW) e.Row.Cells[6].Visible = false;
        }
        catch (Exception)
        {

        }
    }
    protected void GrvSub_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (PageAction == PAGE_ACTION.VIEW) e.Row.Cells[4].Visible = false;
        }
        catch (Exception)
        {

        }
    }
    #endregion " Disable Column "

    protected void ddlGroupName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList _ddl = ddlGroupNameSearch;
        string _err = string.Empty;
        QSerachGroupID = _ddl.SelectedValue;
        if (string.IsNullOrEmpty(_ddl.SelectedValue))
        {
            DataTYPEOFVISITFORM.DT_ITEMS = null;
        }
        else
        {
            DataTYPEOFVISITFORM.DT_ITEMS = new QuestionnaireBLL().GetITEM_VISITFORM(ref _err, int.Parse(QDocID), int.Parse(_ddl.SelectedValue.ToString()));
        }
        grvItem.DataSource = DataTYPEOFVISITFORM.DT_ITEMS;
        grvItem.DataBind();
    }
    protected void grvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvItem.PageIndex = e.NewPageIndex;
        grvItem.DataSource = DataTYPEOFVISITFORM.DT_ITEMS;
        grvItem.DataBind();
    }
    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        grvMain.DataSource = DataTYPEOFVISITFORM.LstTYPEOFVISITFORMLIST;
        grvMain.DataBind();
    }
}