﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ContractTruckManagement.aspx.cs" Inherits="ContractTruckManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
    <script type="text/javascript">
    // <![CDATA[
        var lastCountry = null;
        function OnCountryChanged(cmbCountry) {
            if (gvw.GetEditor("City").InCallback())
                lastCountry = cmbCountry.GetValue().toString();
            else
                gvw.GetEditor("City").PerformCallback(cmbCountry.GetValue().toString());
        }
        function OnEndCallback(s, e) {
            if (lastCountry) {
                gvw.GetEditor("City").PerformCallback(lastCountry);
                lastCountry = null;
            }
        }
    // ]]> 
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
<table class="OptionsTable BottomMargin" width="100%">
        <tr>
            <td align="right" style="width: 90%">
                <dx:ASPxTextBox ID="txtKeyword" runat="server" ClientInstanceName="txtKeyword" Width="40%" NullText="รหัสสัญญา/เลขที่สัญญา/ชื่อผู้ขนส่ง">
                </dx:ASPxTextBox>
            </td>
            <td>
                <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search">
                  <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('Search'); }" />
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <dx:ASPxGridView ID="gvw" ClientInstanceName="gvw" runat="server" DataSourceID="sdsContractTruck"
        Width="100%" AutoGenerateColumns="False" KeyFieldName="KEY_ID" OnCellEditorInitialize="grid_CellEditorInitialize"
        OnAfterPerformCallback="gvw_AfterPerformCallback" 
        onrowupdating="gvw_RowUpdating" >
        <SettingsPager PageSize="50" />
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="true" />
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0" Width="3%">
                <DeleteButton Visible="True" />
                <EditButton Visible="True" />
            </dx:GridViewCommandColumn>
            <dx:GridViewDataColumn Caption="SCONTRACTNO" FieldName="SCONTRACTNO" ReadOnly="True" />
            <dx:GridViewDataComboBoxColumn Caption="ทะเบียนหัว" FieldName="SVEHNO">
                <Settings FilterMode="DisplayText" />
                <PropertiesComboBox DataSourceID="sdsVehicle" TextField="VEH_NO" ValueField="STRUCKID"
                    EnableSynchronization="False" IncrementalFilteringMode="Contains">
                    <%--<ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }"></ClientSideEvents>--%>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="ทะเบียนหาง" FieldName="STUNO">
                <Settings FilterMode="DisplayText" />
                <PropertiesComboBox DataSourceID="sdsTrailer" TextField="TU_NO" ValueField="STRUCKID"
                    EnableSynchronization="False" IncrementalFilteringMode="Contains">
                    <%--<ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>--%>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataColumn Caption="สำรอง" FieldName="CSTANDBY" Width="15px" />
            <dx:GridViewDataColumn Caption="KEY_ID" FieldName="KEY_ID" Visible="false" />
            <dx:GridViewDataColumn Caption="SCONTRACTID" FieldName="SCONTRACTID" Visible="false" />
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sdsContractTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
        
        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False" 
        SelectCommand="SELECT  'KEY'||CONT_TRCK.SCONTRACTID||''||CONT_TRCK.STRUCKID KEY_ID,CONT_TRCK.SCONTRACTID,CONT.SCONTRACTNO ,CONT.SVENDORID
,STRUCKID ,VEH_TRCK.SHEADREGISTERNO SVEHNO
,STRAILERID STUID ,TU_TRCK.SHEADREGISTERNO STUNO 
,CONT_TRCK.CSTANDBY,CONT_TRCK.CREJECT,CONT_TRCK.DSTART,CONT_TRCK.DEND
,CONT_TRCK.DCREATE,CONT_TRCK.SCREATE,CONT_TRCK.DUPDATE,CONT_TRCK.SUPDATE
FROM TCONTRACT_TRUCK CONT_TRCK
LEFT JOIN TCONTRACT CONT ON CONT.SCONTRACTID=CONT_TRCK.SCONTRACTID
LEFT JOIN TTRUCK VEH_TRCK ON CONT_TRCK.STRUCKID=VEH_TRCK.STRUCKID
LEFT JOIN TTRUCK TU_TRCK ON CONT_TRCK.STRAILERID=TU_TRCK.STRUCKID
LEFT JOIN TVENDOR VEND ON CONT.SVENDORID = VEND.SVENDORID
WHERE CONT_TRCK.SCONTRACTID is not null AND STRUCKID is not null
AND CONT.SCONTRACTNO||''||VEND.SABBREVIATION LIKE '%'|| NVL( :INP_Keyword , CONT.SCONTRACTNO||''||VEND.SABBREVIATION ) ||'%'"
 UpdateCommand="IUTCONTRACT_TRUCK"  UpdateCommandType="StoredProcedure"
 
        DeleteCommand="DELETE TCONTRACT_TRUCK WHERE SCONTRACTID = :INP_SCONTRACTID AND STRUCKID = :INP_STRUCKID " 
        onupdated="sdsContractTruck_Updated" onupdating="sdsContractTruck_Updating" >
<SelectParameters>
    <asp:ControlParameter Name="INP_Keyword" ControlID="txtKeyword" PropertyName="Text" ConvertEmptyStringToNull="true" />
</SelectParameters>
<DeleteParameters>
<asp:Parameter Name="INP_SCONTRACTID" ConvertEmptyStringToNull="true" Type="String" />
<asp:Parameter Name="INP_STRUCKID" ConvertEmptyStringToNull="true" Type="String" />
</DeleteParameters>
<UpdateParameters>
    <asp:Parameter Name="INP_SCONTRACTID" Type="String" />
    <asp:Parameter Name="INP_STRUCKID" Type="String" />
    <asp:Parameter Name="INP_STRAILERID" Type="String" />
    <asp:Parameter Name="INP_CSTANDBY" Type="String" />
    <asp:Parameter Name="INP_CREJECT" Type="String" />
    <asp:Parameter Name="INP_DSTART" Type="String" />
    <asp:Parameter Name="INP_DEND" Type="String" />
    <asp:Parameter Name="INP_DCREATE" Type="String" />
    <asp:Parameter Name="INP_SCREATE" Type="String" />
    <asp:Parameter Name="INP_DUPDATE" Type="String" />
    <asp:Parameter Name="INP_SUPDATE" Type="String" />
    <asp:Parameter Name="KEY_ID" Type="String" />
</UpdateParameters>
</asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsVehicle" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
        SelectCommand="SELECT TTRUCK.STRUCKID    ,TTRUCK.SHEADREGISTERNO VEH_NO 
,TTRUCK.STRANSPORTID ,TTRUCK.CACTIVE ,NVL(TTRUCK.CHOLD,'0') CHOLD
,TRUNC(TTRUCK.DWATEREXPIRE ) DWATEREXPIRE 
,TTRUCKTYPE.SCARTYPENAME 
,CASE WHEN NVL(TTRUCK.CHOLD,'0')='0' THEN
CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 'N/A' ELSE 'SAP' END 
ELSE 'TMS' END MS_HOLD
FROM TTRUCK  
LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
WHERE TTRUCK.CACTIVE='Y' AND TTRUCK.SCARTYPEID IN ('0','3' )"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsTrailer" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
        SelectCommand="SELECT TTRUCK.STRUCKID    ,TTRUCK.SHEADREGISTERNO TU_NO 
,TTRUCK.STRANSPORTID ,TTRUCK.CACTIVE ,NVL(TTRUCK.CHOLD,'0') CHOLD
,TRUNC(TTRUCK.DWATEREXPIRE ) DWATEREXPIRE 
,TTRUCKTYPE.SCARTYPENAME 
,CASE WHEN NVL(TTRUCK.CHOLD,'0')='0' THEN
CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 'N/A' ELSE 'SAP' END 
ELSE 'TMS' END MS_HOLD
FROM TTRUCK  
LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
WHERE TTRUCK.CACTIVE='Y' AND TTRUCK.SCARTYPEID IN ('4' )"></asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
