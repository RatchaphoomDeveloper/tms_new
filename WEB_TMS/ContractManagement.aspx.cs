﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class ContractManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {
            dteEnd.Text = DateTime.Now.ToString("dd/MM/yyyy");//.AddDays(1)

            Cache.Remove(sdsContract.CacheKeyDependency);
            Cache[sdsContract.CacheKeyDependency] = new object();
            sdsContract.Select(new System.Web.UI.DataSourceSelectArguments());
            sdsContract.DataBind();
        }
    }
    protected void xgvw_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {

    }
}