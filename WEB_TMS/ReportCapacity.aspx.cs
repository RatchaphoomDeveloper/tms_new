﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;
using System.Drawing;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;
using System.Windows.Forms;


public partial class ReportCapacity : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static DataTable dtMainData = new DataTable();
    private static DataTable dtGraphData = new DataTable();
    private static DataTable dtGraphStackBarData = new DataTable();
    private static DataTable dtNameYear = new DataTable();
    private static string series = "";
    private static string argument = "";
    private static int start = 0;
    private static int end = 0;

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            //ListData();
            rblCodition.SelectedIndex = 0;
            SetCboYear();
            //bindChartTankTruck();
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": if (rblCodition.SelectedIndex == 0) { cboMonth.Text = ""; } DataToGraph(); bindChartBar(); bindChartLine(); bindChartTankTruck(); bindChartStackBar(); //ListData("");
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;
            case "ListData": bindChartBar(); bindChartLine(); bindChartTankTruck(); bindChartStackBar();
                break;
        }
    }

    void DataToGraph()
    {
        string JoinMAX = "";
        string ColumnMAX = "";
        string JoinACT = "";
        string ColumnACT = "";
        string JoinACTDown = "";
        string ColumnACTDown = "";
        string sDate = "";

        if (rblCodition.SelectedIndex == 0)
        {
            start = 1;
            end = 13;
        }
        else
        {
            if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
            {
                start = int.Parse(cboMonth.Value + "");
                end = int.Parse(cboMonth.Value + "") + 1;
            }
            else
            {
                start = 1;
                end = 13;
            }
        }
      
        for (int i = start; i < end; i++)
        {
            string sDateStart = "";
            string sDateEnd = "";
         
            if (rblCodition.SelectedIndex == 0)
            {
                if (!string.IsNullOrEmpty(cboYear.Text))
                {
                    //Condition += " AND TO_CHAR(TIC.EXAMDATE,'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
                    sDate = "01/01/" + cboYear.Value;
                    sDateStart = "01/01/" + cboYear.Value;
                    sDateEnd = "01/12/" + cboYear.Value;
                }
                else
                {
                    sDate = "01/01/" + DateTime.Now.Year;
                    sDateStart = "01/01/" + DateTime.Now.Year;
                    sDateEnd = "01/12/" + DateTime.Now.Year;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
                {
                    sDate = "01/" + (i + "").PadLeft(2, '0') + "/" + cboYear.Value;
                }
                else
                {
                    if (!string.IsNullOrEmpty(cboYear.Text))
                    {
                        sDate = "01/" + (i + "").PadLeft(2, '0') + "/" + cboYear.Value;
                    }
                    else
                    {
                        sDate = "01/" + (i + "").PadLeft(2, '0') + "/" + DateTime.Now.Year;
                    }
                }
            }

            


            #region MAXCAPACITY

            ColumnMAX += ",Law" + i + ".M" + i + "";

            JoinMAX += @"
LEFT JOIN
(
    SELECT '" + i + "' as sKey, (COUNT(DN.sDaty)*MAX(CAP.MAXCAPACITY))+ NVL(SUM(CAP.OUTSOURCE),0) as  M" + i + @" FROM  
    (
        select last_day(add_months(TO_DATE('" + sDate + @"','dd/MM/yyyy'), -1))+1+rownum-1 as sDaty ,to_char(to_date((last_day(add_months(TO_DATE('" + sDate + @"','dd/MM/yyyy'), -1))+1+rownum-1),'dd/mm/yyyy'), 'DY') as sDayName
        from dual 
        connect by 
        rownum <= last_day(TO_DATE('" + sDateEnd + @"','dd/MM/yyyy'))-(last_day(add_months(TO_DATE('" + sDateStart + @"','dd/MM/yyyy'), -1))+1)+1
    )DN
    LEFT JOIN 
    (
        SELECT DHOLIDAY FROM LSTHOLIDAY_MV
    )HLD 
    ON TO_CHAR(HLD.DHOLIDAY,'dd/MM') =  TO_CHAR(DN.sDaty,'dd/MM') 
    LEFT JOIN TBL_RPT_CAPACITY CAP
    ON  CASE WHEN  NVL(TO_CHAR(CAP.SDATE),'xxx') <> 'xxx' THEN TO_CHAR(TO_DATE(CAP.SDATE,'dd/MM/yyyy'),'dd/MM/yyyy') ELSE 'xxx' END  =  TO_CHAR(DN.sDaty,'dd/MM/yyyy') 
    WHERE  sDayName NOT LIKE '%ส.%' AND sDayName <> 'อา.' AND NVL(TO_CHAR(HLD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx'
    GROUP BY '" + i + @"'
)Law" + i + " ON Law" + i + @".sKey = cmp.RPTID

";
            #endregion

            #region ACTWORK ปีที่เลือก
            int nYear = int.Parse(DateTime.Now.Year + "");
            string Year = !string.IsNullOrEmpty(cboYear.Value+"") ? (int.Parse(cboYear.Value + "") - 1) + "" : (nYear - 1) + "";


            ColumnACT += ",ACT" + i + ".A" + i + "";

            JoinACT += @"
LEFT JOIN 
(
    SELECT '" + i + "' as sKey2,SUM(CASE WHEN TO_CHAR(REQ.SERVICE_DATE,'MM') = '" + (i + "").PadLeft(2, '0') + "' AND NVL(ACT.REQUEST_ID,'xxx') <> 'xxx' THEN 1 ELSE 0 END) as A" + i + @"
    FROM TBL_REQUEST REQ
    LEFT JOIN 
    (
           SELECT REQUEST_ID  FROM TBL_TIME_INNER_CHECKINGS GROUP BY REQUEST_ID
    )ACT ON ACT.REQUEST_ID = REQ.REQUEST_ID
    WHERE TO_CHAR(REQ.SERVICE_DATE,'yyyy') = '" + CommonFunction.ReplaceInjection(Year) + @"'
    GROUP BY '" + i + @"'
)ACT" + i + @"
ON ACT" + i + @".sKey2 = cmp.RPTID";
            #endregion

            #region ACTWORK ปีที่แล้ว;
            //int nYear = int.Parse(DateTime.Now.Year + "");
            string BackYear = !string.IsNullOrEmpty(cboYear.Value+"") ? (int.Parse(cboYear.Value + "") - 1) + "" : (nYear - 1) + "";

            ColumnACTDown += ",ACTDown" + i + ".L" + i + "";

            JoinACTDown += @"
LEFT JOIN 
(
    SELECT '" + i + "' as sKey3,SUM(CASE WHEN TO_CHAR(REQ.SERVICE_DATE,'MM') = '" + (i + "").PadLeft(2, '0') + "' AND NVL(ACT.REQUEST_ID,'xxx') <> 'xxx' THEN 1 ELSE 0 END) as L" + i + @"
    FROM TBL_REQUEST REQ
    LEFT JOIN 
    (
           SELECT REQUEST_ID  FROM TBL_TIME_INNER_CHECKINGS GROUP BY REQUEST_ID
    )ACT ON ACT.REQUEST_ID = REQ.REQUEST_ID
    WHERE TO_CHAR(REQ.SERVICE_DATE,'yyyy') = '" + CommonFunction.ReplaceInjection(BackYear) + @"'
    GROUP BY '" + i + @"'
)ACTDown" + i + @"
ON ACTDown" + i + @".sKey3 = cmp.RPTID";

        }
            #endregion

        string QUERY = "SELECT cmp.RPTID,cmp.RPTNAME" + ColumnMAX + " " + ColumnACT + @" " + ColumnACTDown + @"
                        FROM TBL_RPT_CARWATEREXPIRE cmp
                        " + JoinMAX + "  " + JoinACT + " " + JoinACTDown + " ORDER BY cmp.RPTID ASC";


        dtGraphData.Clear();
        dtGraphData = CommonFunction.Get_Data(conn, QUERY);
    }

    private void ListReport(string Type)
    {
        rpt_Capacity report = new rpt_Capacity();

        #region function report

        XRChart xrChart1 = report.FindControl("xrChart1", true) as XRChart;
        XRChart xrChart2 = report.FindControl("xrChart2", true) as XRChart;
        XRChart xrChart3 = report.FindControl("xrChart3", true) as XRChart;
        XRChart xrChart4 = report.FindControl("xrChart4", true) as XRChart;
        xrChart1.Series.Clear();
        xrChart2.Series.Clear();
        xrChart3.Series.Clear();
        xrChart4.Series.Clear();

        //add Title
        ChartTitle chartTitle1 = new ChartTitle();
        chartTitle1.Text = "รายงานปริมาณการดำเนินงาน";
        chartTitle1.Font = new Font("Tahoma", 15);

        ChartTitle chartTitleTank = new ChartTitle();
        chartTitleTank.Text = "        Tank Truck Calibration Capacity Tracking";
        chartTitleTank.Font = new Font("Tahoma", 15);


        xrChart1.Titles.Add(chartTitle1);
        xrChart2.Titles.Add(chartTitleTank);
        xrChart3.Titles.Add(chartTitle1);
        xrChart4.Titles.Add(chartTitle1);


        if (rblCodition.SelectedIndex == 0)
        {
            ChartTitle chartTitle2 = new ChartTitle();
            chartTitle2.Text = "ปี " + cboYear.Text;
            chartTitle2.Font = new Font("Tahoma", 13);
            xrChart1.Titles.Add(chartTitle2);
            xrChart2.Titles.Add(chartTitle2);
            xrChart3.Titles.Add(chartTitle2);
            xrChart4.Titles.Add(chartTitle2);
        }
        else
        {
            if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
            {
                ChartTitle chartTitle2 = new ChartTitle();
                chartTitle2.Text = "เดือน " + cboMonth.Text + "ปี " + cboYear.Text;
                chartTitle2.Font = new Font("Tahoma", 13);
                xrChart1.Titles.Add(chartTitle2);
                xrChart2.Titles.Add(chartTitle2);
                xrChart3.Titles.Add(chartTitle2);
                xrChart4.Titles.Add(chartTitle2);
            }
        }

        if (dtGraphData.Rows.Count > 0)
        {
            int nYear = int.Parse(DateTime.Now.Year + "") + 543;
            string BackYear = cboYear.Value != null ? (int.Parse(cboYear.Text + "") - 1) + "" : (nYear - 1) + "";

            Series series = new Series("MAX CAPACITY", ViewType.Bar);
            Series series2 = new Series("ACTUAL WORK", ViewType.Bar);
            Series series3 = new Series("ACTUAL " + BackYear + "", ViewType.Bar);
            Series seriesXr4 = new Series("MAX CAPACITY", ViewType.Bar);
            Series series2Xr4 = new Series("ACTUAL WORK", ViewType.Bar);
            Series seriesLine = new Series("MAX CAPACITY", ViewType.Line);
            Series series2Line = new Series("ACTUAL WORK", ViewType.Line);
            Series series3Line = new Series("ACTUAL " + BackYear + "", ViewType.Line);

            for (int i = start; i < end; i++)
            {
                string COLMAX = "M" + i;
                string COLACT = "A" + i;
                string COLACTDown = "L" + i;

                int newi = i - 1;

                double ValueMAX = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLMAX] + "") ? double.Parse(dtGraphData.Rows[newi][COLMAX] + "") : 0;
                series.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueMAX }));
                seriesLine.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueMAX }));
                seriesXr4.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueMAX }));

                double ValueACT = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLACT] + "") ? double.Parse(dtGraphData.Rows[newi][COLACT] + "") : 0;
                series2.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACT }));
                series2Line.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACT }));
                series2Xr4.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACT }));

                double ValueACTDown = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLACTDown] + "") ? double.Parse(dtGraphData.Rows[newi][COLACTDown] + "") : 0;
                series3.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACTDown }));
                series3Line.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACTDown }));
            }

            xrChart1.Series.Add(series);
            xrChart1.Series.Add(series2);
            xrChart1.Series.Add(series3);

            xrChart2.Series.Add(seriesXr4);
            xrChart2.Series.Add(series2Xr4);

            xrChart4.Series.Add(seriesLine);
            xrChart4.Series.Add(series2Line);
            xrChart4.Series.Add(series3Line);

        }

        if (dtGraphStackBarData.Rows.Count > 0)
        {
            Series series = new Series("MAX CAPACITY", ViewType.StackedBar);

            Series series2 = new Series("Planning", ViewType.StackedBar);

            Series series3 = new Series("Complete Jobs", ViewType.StackedBar);

            Series series4 = new Series("Failed Jobs", ViewType.StackedBar);

            Series series5 = new Series("Postpane", ViewType.StackedBar);


            double Value = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["MAXCAPACITY"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["MAXCAPACITY"] + "") : 0;
            series.Points.Add(new SeriesPoint("MAX CAPACITY", new double[] { Value }));
            xrChart3.Series.Add(series);

            double Value2 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["PLANNING"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["PLANNING"] + "") : 0;
            series2.Points.Add(new SeriesPoint("Planning", new double[] { Value2 }));
            xrChart3.Series.Add(series2);

            double Value4 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["RESULT_TRY"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["RESULT_TRY"] + "") : 0;
            series3.Points.Add(new SeriesPoint("TOTAL ACTUAL", new double[] { Value4 }));
            xrChart3.Series.Add(series3);

            double Value5 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["NORESULT_TRY"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["NORESULT_TRY"] + "") : 0;
            series4.Points.Add(new SeriesPoint("TOTAL ACTUAL", new double[] { Value5 }));
            xrChart3.Series.Add(series4);

            double Value6 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["POSTPANE"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["POSTPANE"] + "") : 0;
            series5.Points.Add(new SeriesPoint("POSTPANE", new double[] { Value6 }));
            xrChart3.Series.Add(series5);



        }

        ((XRLabel)report.FindControl("xrLabel2", true)).Text = "รายงาน Capacity รวม  (จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + ")";

        ((XRLabel)report.FindControl("xrLabel3", true)).Text = "วันที่จัดทำรายงาน: " + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

        report.Name = "ReportTeabpansCompletely";
        if (dtMainData.Rows.Count > 0)
        {
            report.DataSource = dtMainData;
        }
        string fileName = "รายงานผลการนำรถส่งเทียบแป้น_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();


        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ListReport("P");
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ListReport("E");
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion

    //    #region SetAutoComplete

    //    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    //    {
    //        ASPxComboBox comboBox = (ASPxComboBox)source;
    //        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
    //          FROM ( 
    //                SELECT TVENDOR.SVENDORID,TVENDOR.SABBREVIATION FROM TVENDOR
    //                INNER JOIN 
    //                (
    //                    SELECT 
    //                       SVENDORID, TO_DATE(DEND) - TO_DATE(sysdate) as DateCountdown
    //                    FROM TCONTRACT
    //                    WHERE   TO_DATE(DEND) - TO_DATE(sysdate)  >= 0
    //                )O
    //                ON TVENDOR.SVENDORID = O.SVENDORID
    //                GROUP BY TVENDOR.SVENDORID,TVENDOR.SABBREVIATION)
    //         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

    //        sdsVendor.SelectParameters.Clear();
    //        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
    //        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
    //        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
    //        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

    //        comboBox.DataSource = sdsVendor;
    //        comboBox.DataBind();

    //    }

    //    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    //    {
    //    }
    //    #endregion

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    private void bindChartBar()
    {

        if (dtGraphData.Rows.Count > 0)
        {
            int nYear = int.Parse(DateTime.Now.Year + "") + 543;
            string BackYear = cboYear.Value != null ? (int.Parse(cboYear.Text + "") - 1) + "" : (nYear - 1) + "";

            Series series = new Series("MAX CAPACITY", ViewType.Bar);
            series.ResetLegendPointOptions();
            Series series2 = new Series("ACTUAL WORK", ViewType.Bar);
            Series series3 = new Series("ACTUAL " + BackYear + "", ViewType.Bar);

            for (int i = start; i < end; i++)
            {
                string COLMAX = "M" + i;
                string COLACT = "A" + i;
                string COLACTDown = "L" + i;

                int newi = i - 1;

                double ValueMAX = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLMAX] + "") ? double.Parse(dtGraphData.Rows[newi][COLMAX] + "") : 0;
                series.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueMAX }));

                double ValueACT = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLACT] + "") ? double.Parse(dtGraphData.Rows[newi][COLACT] + "") : 0;
                series2.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACT }));

                double ValueACTDown = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLACTDown] + "") ? double.Parse(dtGraphData.Rows[newi][COLACTDown] + "") : 0;
                series3.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACTDown }));

            }

            WebChartControl1.Series.Add(series);
            WebChartControl1.Series.Add(series2);
            WebChartControl1.Series.Add(series3);

            ChartTitle chartTitle1 = new ChartTitle();
            chartTitle1.Text = "รายงานปริมาณการดำเนินการ";
            chartTitle1.Font = new Font("Tahoma", 15);
            WebChartControl1.Titles.Add(chartTitle1);
            if (rblCodition.SelectedIndex == 0)
            {

                ChartTitle chartTitle2 = new ChartTitle();
                chartTitle2.Text = " ปี " + (!string.IsNullOrEmpty(cboYear.Text) ? cboYear.Text : (DateTime.Now.Year + 543) + "") + "";
                chartTitle2.Font = new Font("Tahoma", 13);
                WebChartControl1.Titles.Add(chartTitle2);
            }
            else
            {
                if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
                {
                    ChartTitle chartTitle2 = new ChartTitle();
                    chartTitle2.Text = "เดือน" + cboMonth.Text + " ปี " + cboYear.Text + "";
                    chartTitle2.Font = new Font("Tahoma", 13);
                    WebChartControl1.Titles.Add(chartTitle2);
                }
            }
            WebChartControl1.Legend.Visible = true;
            WebChartControl1.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative;
            WebChartControl1.SeriesTemplate.ValueScaleType = ScaleType.Numerical;
            WebChartControl1.SeriesTemplate.PointOptions.PointView = PointView.Values;
            WebChartControl1.SeriesTemplate.LegendPointOptions.PointView = PointView.Argument;
            ((DevExpress.XtraCharts.XYDiagram)WebChartControl1.Diagram).AxisX.Label.Angle = 0;
            WebChartControl1.DataSource = dtGraphData;
            WebChartControl1.DataBind();
        }

    }

    private void bindChartLine()
    {
        if (dtGraphData.Rows.Count > 0)
        {
            int nYear = int.Parse(DateTime.Now.Year + "") + 543;
            string BackYear = cboYear.Value != null ? (int.Parse(cboYear.Text + "") - 1) + "" : (nYear - 1) + "";
            Series series = new Series("MAX CAPACITY", ViewType.Line);
            Series series2 = new Series("ACTUAL WORK", ViewType.Line);
            Series series3 = new Series("ACTUAL " + BackYear + "", ViewType.Line);
            for (int i = start; i < end; i++)
            {
                string COLMAX = "M" + i;
                string COLACT = "A" + i;
                string COLACTDown = "L" + i;

                int newi = i - 1;

                double ValueMAX = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLMAX] + "") ? double.Parse(dtGraphData.Rows[newi][COLMAX] + "") : 0;
                series.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueMAX }));

                double ValueACT = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLACT] + "") ? double.Parse(dtGraphData.Rows[newi][COLACT] + "") : 0;
                series2.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACT }));

                double ValueACTDown = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLACTDown] + "") ? double.Parse(dtGraphData.Rows[newi][COLACTDown] + "") : 0;
                series3.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACTDown }));

            }

            WebChartControl2.Series.Add(series);
            WebChartControl2.Series.Add(series2);
            WebChartControl2.Series.Add(series3);

            ChartTitle chartTitle1 = new ChartTitle();
            chartTitle1.Text = "รายงานปริมาณการดำเนินการ";
            chartTitle1.Font = new Font("Tahoma", 15);
            WebChartControl2.Titles.Add(chartTitle1);
            if (rblCodition.SelectedIndex == 0)
            {

                ChartTitle chartTitle2 = new ChartTitle();
                chartTitle2.Text = " ปี " + (!string.IsNullOrEmpty(cboYear.Text) ? cboYear.Text : (DateTime.Now.Year + 543) + "") + "";
                chartTitle2.Font = new Font("Tahoma", 13);
                WebChartControl2.Titles.Add(chartTitle2);
            }
            else
            {
                if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
                {
                    ChartTitle chartTitle2 = new ChartTitle();
                    chartTitle2.Text = "เดือน" + cboMonth.Text + " ปี " + cboYear.Text + "";
                    chartTitle2.Font = new Font("Tahoma", 13);
                    WebChartControl2.Titles.Add(chartTitle2);
                }
            }
            WebChartControl2.Legend.Visible = true;
            WebChartControl2.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative;
            WebChartControl2.SeriesTemplate.ValueScaleType = ScaleType.Numerical;
            WebChartControl2.SeriesTemplate.PointOptions.PointView = PointView.Values;
            WebChartControl2.SeriesTemplate.LegendPointOptions.PointView = PointView.Argument;
            ((DevExpress.XtraCharts.XYDiagram)WebChartControl2.Diagram).AxisX.Label.Angle = 0;
            WebChartControl2.DataSource = dtGraphData;
            WebChartControl2.DataBind();
        }
    }

    private void bindChartStackBar()
    {
        string Condition = "";

        //string ss = "";
        //switch (cboMonth.Text)
        //{
        //    case "ม.ค.": ss = "01";
        //        break;
        //    case "ก.พ.": ss = "02";
        //        break;
        //    case "มี.ค.": ss = "03";
        //        break;
        //    case "เม.ย.": ss = "04";
        //        break;
        //    case "พ.ค.": ss = "05";
        //        break;
        //    case "มิ.ย.": ss = "06";
        //        break;
        //    case "ก.ค.": ss = "07";
        //        break;
        //    case "ส.ค.": ss = "08";
        //        break;
        //    case "ก.ย.": ss = "09";
        //        break;
        //    case "ต.ค.": ss = "10";
        //        break;
        //    case "พ.ย.": ss = "11";
        //        break;
        //    case "ธ.ค.": ss = "12";
        //        break;
        //}


        ////txtargument.Text = ss;
        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text + ""))
        //{

        //}
        //else
        //{
        //    lblsTail.Text = "-";
        //}

        string sDate = "";
        string sDateStart = "";
        string sDateEnd = "";
        if (rblCodition.SelectedIndex == 0)
        {

            if (!string.IsNullOrEmpty(cboYear.Text))
            {
                sDate = "01/01/" + cboYear.Value;
                sDateStart = "01/01/" + cboYear.Value;
                sDateEnd = "01/12/" + cboYear.Value;
                Condition += " AND TO_CHAR(TIC.EXAMDATE,'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
            }
            else
            {
                sDate = "01/01/" + DateTime.Now.Year;
                sDateStart = "01/01/" + DateTime.Now.Year;
                sDateEnd = "01/12/" + DateTime.Now.Year;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
            {
                sDate = "01/" + cboMonth.Value + "/" + cboYear.Value;
                sDateStart = "01/" + cboMonth.Value + "/" + cboYear.Value;
                sDateEnd = "01/" + cboMonth.Value + "/" + cboYear.Value;
                Condition += " AND TO_CHAR(TIC.EXAMDATE,'MM/yyyy') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "/" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
            }
            else
            {
                sDate = "01/01/" + DateTime.Now.Year;
                sDateStart = "01/01/" + DateTime.Now.Year;
                sDateEnd = "01/12/" + DateTime.Now.Year;
            }
        }

        string QUERY = @"
SELECT '11' as sKey, (COUNT(DN.sDaty)*MAX(CAP.MAXCAPACITY))+ NVL(SUM(CAP.OUTSOURCE),0) as  MAXCAPACITY
,LAW.CCHECKING_WATER as PLANNING 
,LAW.RESULT_TRY as RESULT_TRY
,LAW.NORESULT_TRY as NORESULT_TRY
,LAW.POSTPANE as POSTPANE
FROM  
(
        select last_day(add_months(TO_DATE('" + sDate + @"','dd/MM/yyyy'), -1))+1+rownum-1 as sDaty ,to_char(to_date((last_day(add_months(TO_DATE('" + sDate + @"','dd/MM/yyyy'), -1))+1+rownum-1),'dd/mm/yyyy'), 'DY') as sDayName
        from dual 
        connect by 
        rownum <= last_day(TO_DATE('" + sDateEnd + @"','dd/MM/yyyy'))-(last_day(add_months(TO_DATE('" + sDateStart + @"','dd/MM/yyyy'), -1))+1)+1
)DN
LEFT JOIN 
(
    SELECT DHOLIDAY FROM LSTHOLIDAY_MV
)HLD 
ON TO_CHAR(HLD.DHOLIDAY,'dd/MM') =  TO_CHAR(DN.sDaty,'dd/MM') 
LEFT JOIN TBL_RPT_CAPACITY CAP
ON  CASE WHEN  NVL(TO_CHAR(CAP.SDATE),'xxx') <> 'xxx' THEN TO_CHAR(TO_DATE(CAP.SDATE,'dd/MM/yyyy'),'dd/MM/yyyy') ELSE 'xxx' END  =  TO_CHAR(DN.sDaty,'dd/MM/yyyy') 
LEFT JOIN
(
    SELECT '11' as sKey,SUM(CASE WHEN NVL(REQ.CCHECKING_WATER,'xxx') <> 'xxx' THEN 1 ELSE 0 END) as CCHECKING_WATER
    ,SUM(CASE WHEN NVL(REQ.RESULT_TRY,'xxx') <> 'xxx' AND REQ.RESULT_TRY='Y'  THEN 1 
    ELSE NULL END) as RESULT_TRY
    ,SUM(CASE WHEN NVL(REQ.RESULT_TRY,'xxx') <> 'xxx' AND REQ.RESULT_TRY='N'  THEN 1 
    ELSE NULL END) as NORESULT_TRY
    ,SUM(CASE WHEN NVL(REQ.EVENTMOVE,'xxx') <> 'xxx' THEN 1 
    ELSE NULL END) as POSTPANE
    FROM TBL_REQUEST REQ
    LEFT JOIN 
    (
        SELECT REQUEST_ID,MAX(EXAMDATE) as  EXAMDATE  FROM  TBL_TIME_INNER_CHECKINGS WHERE ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID
    )
    TIC ON TIC.REQUEST_ID = REQ.REQUEST_ID
    WHERE 1=1 " + Condition + @"
)LAW ON LAW.sKey = '11'
WHERE  sDayName NOT LIKE '%ส.%' AND sDayName <> 'อา.' AND NVL(TO_CHAR(HLD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx'
GROUP BY '11',LAW.CCHECKING_WATER,LAW.RESULT_TRY ,LAW.NORESULT_TRY ,LAW.POSTPANE 
";
        // dtGraphData.Clear();

        dtGraphStackBarData.Clear();
        dtGraphStackBarData = CommonFunction.Get_Data(conn, QUERY);
        WebChartControl3.ClearSelection();
        if (dtGraphStackBarData.Rows.Count > 0)
        {
            Series series = new Series("MAX CAPACITY", ViewType.StackedBar);

            Series series2 = new Series("Planning", ViewType.StackedBar);

            Series series3 = new Series("Complete Jobs", ViewType.StackedBar);

            Series series4 = new Series("Failed Jobs", ViewType.StackedBar);

            Series series5 = new Series("Postpane", ViewType.StackedBar);


            double Value = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["MAXCAPACITY"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["MAXCAPACITY"] + "") : 0;
            series.Points.Add(new SeriesPoint("MAX CAPACITY", new double[] { Value }));
            WebChartControl3.Series.Add(series);

            double Value2 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["PLANNING"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["PLANNING"] + "") : 0;
            series2.Points.Add(new SeriesPoint("Planning", new double[] { Value2 }));
            WebChartControl3.Series.Add(series2);

            double Value4 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["RESULT_TRY"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["RESULT_TRY"] + "") : 0;
            series3.Points.Add(new SeriesPoint("TOTAL ACTUAL", new double[] { Value4 }));
            WebChartControl3.Series.Add(series3);

            double Value5 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["NORESULT_TRY"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["NORESULT_TRY"] + "") : 0;
            series4.Points.Add(new SeriesPoint("TOTAL ACTUAL", new double[] { Value5 }));
            WebChartControl3.Series.Add(series4);

            double Value6 = !string.IsNullOrEmpty(dtGraphStackBarData.Rows[0]["POSTPANE"] + "") ? double.Parse(dtGraphStackBarData.Rows[0]["POSTPANE"] + "") : 0;
            series5.Points.Add(new SeriesPoint("POSTPANE", new double[] { Value6 }));
            WebChartControl3.Series.Add(series5);


            ChartTitle chartTitle1 = new ChartTitle();
            chartTitle1.Text = "รายงานปริมาณการดำเนินการ";
            chartTitle1.Font = new Font("Tahoma", 15);
            WebChartControl3.Titles.Add(chartTitle1);

            if (rblCodition.SelectedIndex == 0)
            {

                ChartTitle chartTitle2 = new ChartTitle();
                chartTitle2.Text = " ปี " + (!string.IsNullOrEmpty(cboYear.Text) ? cboYear.Text : (DateTime.Now.Year + 543) + "") + "";
                chartTitle2.Font = new Font("Tahoma", 13);
                WebChartControl3.Titles.Add(chartTitle2);
            }
            else
            {
                if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
                {
                    ChartTitle chartTitle2 = new ChartTitle();
                    chartTitle2.Text = "เดือน" + cboMonth.Text + " ปี " + cboYear.Text + "";
                    chartTitle2.Font = new Font("Tahoma", 13);
                    WebChartControl3.Titles.Add(chartTitle2);
                }
            }

            WebChartControl3.Legend.Visible = true;
            WebChartControl3.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative;
            WebChartControl3.SeriesTemplate.ValueScaleType = ScaleType.Numerical;
            WebChartControl3.SeriesTemplate.PointOptions.PointView = PointView.Values;
            WebChartControl3.SeriesTemplate.LegendPointOptions.PointView = PointView.Argument;

            ((DevExpress.XtraCharts.XYDiagram)WebChartControl3.Diagram).AxisX.Label.Angle = 0;

            WebChartControl3.DataSource = dtGraphStackBarData;
            WebChartControl3.DataBind();



        }


    }

    private void bindChartTankTruck()
    {
        if (dtGraphData.Rows.Count > 0)
        {
            Series series = new Series("MAX CAPACITY", ViewType.Bar);
            Series series2 = new Series("ACTUAL WORK", ViewType.Bar);

            for (int i = start; i < end; i++)
            {
                string COLMAX = "M" + i;
                string COLACT = "A" + i;

                int newi = i - 1;
                double ValueMAX = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLMAX] + "") ? double.Parse(dtGraphData.Rows[newi][COLMAX] + "") : 0;
                series.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueMAX }));



                double ValueACT = !string.IsNullOrEmpty(dtGraphData.Rows[newi][COLACT] + "") ? double.Parse(dtGraphData.Rows[newi][COLACT] + "") : 0;
                series2.Points.Add(new SeriesPoint(dtGraphData.Rows[newi]["RPTNAME"] + "", new double[] { ValueACT }));

            }

            WebChartControl4.Series.Add(series);
            WebChartControl4.Series.Add(series2);
            //lblTitle1.Text = "%เทียบแป้น/วัดน้ำเสร็จ";
            //lblTitle2.Text = "%เทียบแป้นผ่าน/ส่งเทียบแป้น";
            //lblTitle3.Text = "%เทียบแป้นไม่ผ่าน/ส่งเทียบแป้น";

            ChartTitle chartTitle1 = new ChartTitle();
            chartTitle1.Text = "รายงานผลการนำรถส่งเทียบแป้น";
            chartTitle1.Font = new Font("Tahoma", 15);
            WebChartControl4.Titles.Add(chartTitle1);

            if (rblCodition.SelectedIndex == 0)
            {

                ChartTitle chartTitle2 = new ChartTitle();
                chartTitle2.Text = " ปี " + (!string.IsNullOrEmpty(cboYear.Text) ? cboYear.Text : (DateTime.Now.Year + 543) + "") + "";
                chartTitle2.Font = new Font("Tahoma", 13);
                WebChartControl4.Titles.Add(chartTitle2);
            }
            else
            {
                if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
                {
                    ChartTitle chartTitle2 = new ChartTitle();
                    chartTitle2.Text = "เดือน" + cboMonth.Text + " ปี " + cboYear.Text + "";
                    chartTitle2.Font = new Font("Tahoma", 13);
                    WebChartControl4.Titles.Add(chartTitle2);
                }
            }

            WebChartControl4.Legend.Visible = true;
            WebChartControl4.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative;
            WebChartControl4.SeriesTemplate.ValueScaleType = ScaleType.Numerical;
            WebChartControl4.SeriesTemplate.PointOptions.PointView = PointView.Values;
            WebChartControl4.SeriesTemplate.LegendPointOptions.PointView = PointView.Argument;
            ((DevExpress.XtraCharts.XYDiagram)WebChartControl4.Diagram).AxisX.Label.Angle = 0;
            WebChartControl4.DataSource = dtGraphData;
            WebChartControl4.DataBind();
        }


    }
}