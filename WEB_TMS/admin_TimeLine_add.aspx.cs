﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;

public partial class admin_TimeLine_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/TimeLine/{0}/{2}/{1}/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {          
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            if (Session["sternalID"] != null)
            {
                cbxOrganiz.Enabled = false;
                listData();

            }
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //btnSearch.enable = false;
            }
            if (!CanWrite)
            {
                btnDelFile.Enabled = false;
                btnSubmit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {

        Session["sternalID"] = null;
        cbxOrganiz.SelectedIndex = 0;
        txtCarConfirm.Text = "";
        txtPlanConfirm.Text = "";
        txtCloseOrder.Text = "";
        rblStatus.Value = 1;
        ckbStatus.Checked = false;
        lblUser.Text = "";
        txtFileName.Text = "";
        txtFileName1.Text = "";
        txtFilePath.Text = "";
        txtFilePath1.Text = "";


    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        if (CanWrite)
        {
            switch (paras[0])
            {


                case "Save":

                    if (Session["sternalID"] != null)
                    {

                        sds.Update();

                        LogUser("32", "E", "แก้ไขข้อมูลหน้า Time Line", Session["sternalID"] + "");

                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_TimeLine_lst.aspx';});");

                    ClearControl();

                    break;


                case "deleteFile":

                    string FilePath = paras[1];

                    if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                    {
                        File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                    }

                    string cNo = paras[2];
                    if (cNo == "1")
                    {
                        txtFileName.Text = "";
                        txtFilePath.Text = "";
                    }
                    else
                    {
                        txtFileName1.Text = "";
                        txtFilePath1.Text = "";
                    }

                    VisibleControlUpload();

                    break;
            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }

    }


    void listData()
    {
        cbxOrganiz.Enabled = false;

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT STERMINALID, TCARCONFIRM, TPLANCONFIRM, TCLOSEORDER, CSTATUS, CACTIVE, SEVIDENCE,SFILENAME,SFILEPATH,SEVIDENCE1,SFILENAME1,SFILEPATH1,MIN2STAT,MAX2STAT  from TTERMINAL WHERE STERMINALID = '" + Session["sternalID"] + "'");
        if (dt.Rows.Count > 0)
        {
            TP01RouteOnItemsRequestedByFilterConditionSQL(cbxOrganiz, new ListEditItemsRequestedByFilterConditionEventArgs(0, 9, dt.Rows[0][0] + ""));

            cbxOrganiz.SelectedItem.Value = dt.Rows[0]["STERMINALID"] + "";
            txtCarConfirm.Text = dt.Rows[0]["TCARCONFIRM"] + "";
            txtPlanConfirm.Text = dt.Rows[0]["TPLANCONFIRM"] + "";
            txtCloseOrder.Text = dt.Rows[0]["TCLOSEORDER"] + "";
            if (dt.Rows[0]["CSTATUS"] + "" == "1")
            {
                ckbStatus.Checked = true;
            }
            rblStatus.Value = dt.Rows[0]["CACTIVE"] + "";
            lblUser.Text = Session["UserName"] + "";
            txtEvidence.Text = dt.Rows[0]["SEVIDENCE"] + "";
            txtFileName.Text = dt.Rows[0]["SFILENAME"] + "";
            txtFilePath.Text = dt.Rows[0]["SFILEPATH"] + "";
            txtEvidence1.Text = dt.Rows[0]["SEVIDENCE1"] + "";
            txtFileName1.Text = dt.Rows[0]["SFILENAME1"] + "";
            txtFilePath1.Text = dt.Rows[0]["SFILEPATH1"] + "";
            txtMinCHG2STAT.Text = dt.Rows[0]["MIN2STAT"] + "";
            txtMaxCHG2STAT.Text = dt.Rows[0]["MAX2STAT"] + "";
            VisibleControlUpload();
        }

    }
    
    protected void TP01RouteOnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsOrganiz.SelectCommand = "SELECT STERMINALID, STERMINALNAME FROM (SELECT T.STERMINALID, TS.STERMINALNAME, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T  INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE T.STERMINALID = '" + Session["sternalID"] + "" + "')  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";

        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();

        if (comboBox.Items.Count > 0)
        {
            comboBox.SelectedIndex = 0;
        }
    }

    protected void TP01RouteOnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;
            int count = _Filename.Count() - 1;

            string genName = "timeline" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                if (upl.ID == "uplExcel")
                {
                    e.CallbackData = data + "|" + e.UploadedFile.FileName;
                }
                else if (upl.ID == "uplExcel1")
                {
                    e.CallbackData = data + "|" + e.UploadedFile.FileName;
                }

                
            }
        }
        else
        {

                txtFileName.Text = "";
                txtFilePath.Text = "";
                txtFileName1.Text = "";
                txtFilePath1.Text = "";

            return;

        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void VisibleControlUpload()
    {
        if (!string.IsNullOrEmpty(txtFilePath.Text))
        {
            uplExcel.ClientVisible = false;
            txtFileName.ClientVisible = true;
            btnView.ClientEnabled = true;
            btnDelFile.ClientEnabled = true;

        }
        else
        {
            uplExcel.ClientVisible = true;
            txtFileName.ClientVisible = false;
            btnView.ClientEnabled = false;
            btnDelFile.ClientEnabled = false;
        }

        if (!string.IsNullOrEmpty(txtFilePath1.Text))
        {
            uplExcel1.ClientVisible = false;
            txtFileName1.ClientVisible = true;
            btnView1.ClientEnabled = true;
            btnDelFile1.ClientEnabled = true;
        }
        else
        {
            uplExcel1.ClientVisible = true;
            txtFileName1.ClientVisible = false;
            btnView1.ClientEnabled = false;
            btnDelFile1.ClientEnabled = false;
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}