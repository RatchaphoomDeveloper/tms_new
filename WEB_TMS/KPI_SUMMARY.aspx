﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_SUMMARY.aspx.cs" Inherits="KPI_SUMMARY" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <div class="form-horizontal">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>สรุปผลประเมินดัชนีชี้วัดประสิทธิภาพการทำงาน KPI (รายปี)
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ปี</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" >
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label"></label>
                            <div class="col-md-3">
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เลขที่สัญญา</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">

                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                        <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                        <asp:Button Text="ดูข้อมูล KPI รายเดือน" runat="server" ID="btnViewMonth" class="btn btn-md bth-hover btn-info" OnClick="btnViewMonth_Click" Width="150px" Visible="false" />

                    </div>
                </div>

                
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>ผลการค้นหา จำนวน <asp:Label Text="0" runat="server" ID="lblItem" /> รายการ (*คะแนนที่แสดงคำนวณจากข้อมูลจนถึงเดือนล่าสุดที่แจ้งผล)
            </div>
            <div class="panel-body">
                <div class="row form-group">
                <asp:GridView runat="server" ID="gvKPI" PageSize="50"
                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                    HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                    AllowPaging="True" OnRowUpdating="gvKPI_RowUpdating" DataKeyNames="YEAR,SCONTRACTID,SVENDORID" OnPageIndexChanging="gvKPI_PageIndexChanging">
                    <Columns>
                        
                        <asp:BoundField HeaderText="บริษัทผู้ขนส่ง" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"  />
                        <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText="สรุปผลประเมิน KPI รายปี (40%)" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                    <asp:Label ID="lblKPI" runat="server" Text='<%# Eval("KPI_YEAR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ดูรายละเอียด" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                    <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="View" CommandName="update"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="50" />
                                            <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
                    </div>
                
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

