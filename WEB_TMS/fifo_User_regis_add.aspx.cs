﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;

public partial class fifo_User_regis_add : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //bool chkurl = false;
            //Session["CheckPermission"] = null;
            //string AddEdit = "";

            //if (Session["cPermission"] != null)
            //{
            //    string[] url = (Session["cPermission"] + "").Split('|');
            //    string[] chkpermision;
            //    bool sbreak = false;

            //    foreach (string inurl in url)
            //    {
            //        chkpermision = inurl.Split(';');
            //        if (chkpermision[0] == "18")
            //        {
            //            switch (chkpermision[1])
            //            {
            //                case "0":
            //                    chkurl = false;

            //                    break;
            //                case "1":
            //                    chkurl = true;

            //                    break;

            //                case "2":
            //                    chkurl = true;
            //                    AddEdit = "1";
            //                    break;
            //            }
            //            sbreak = true;
            //        }

            //        if (sbreak == true) break;
            //    }
            //}

            //if (chkurl == false)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //}

            //Session["CheckPermission"] = AddEdit;
            SetCboDate();
            string str = Request.QueryString["str"];
            string uid = Request.QueryString["uid"];
            txtTerminal.Text = str + "";
            txtUserID.Text = uid + "";
        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {
        cboHeadRegist.SelectedIndex = -1;
        cmbPersonalNo.SelectedIndex = -1;
        txtFullname.Text = "";
        txtTel.Text = "";
        cboTrailerRegist.SelectedIndex = -1;
        SetCboDate();
        txtVenderID.Text = "";
    }

    void SetCboDate()
    {
        cboDate.Items.Clear();
        cboDate.Items.Insert(0, new ListEditItem(DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH")), DateTime.Now.Date));
        cboDate.Items.Insert(1, new ListEditItem(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("th-TH")), DateTime.Now.AddDays(1).Date));
        cboDate.SelectedIndex = 0;
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {


        lblTerminal.Text = CommonFunction.Get_Value(sql, "SELECT STERMINALNAME FROM TTERMINAL_SAP WHERE STERMINALID = '" + txtTerminal.Text + "'");


        if (cboDate.SelectedIndex == 1)
        {
            sds.SelectCommand = "SELECT NID, NNO, SHEADREGISTERNO, STRAILERREGISTERNO, SPERSONALNO, SEMPLOYEENAME, STEL, DDATE , SVENDORID FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' AND to_char(DDATE,'dd/MM/yyyy') = to_char(sysdate + 1,'dd/MM/yyyy') AND STERMINAL = :STERMINAL  ORDER BY NNO DESC";
        }
        else
        {
            sds.SelectCommand = "SELECT NID, NNO, SHEADREGISTERNO, STRAILERREGISTERNO, SPERSONALNO, SEMPLOYEENAME, STEL, DDATE , SVENDORID FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' AND to_char(DDATE,'dd/MM/yyyy') = to_char(sysdate,'dd/MM/yyyy') AND STERMINAL = :STERMINAL  ORDER BY NNO DESC";
        }
        gvw.DataBind();
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {


            case "Save":
                //if ("" + Session["CheckPermission"] == "1")
                //{
                string checkFIFO = "";
                string checkEmployee = "";
                if (cboDate.SelectedIndex == 1)
                {
                    checkFIFO = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SHEADREGISTERNO LIKE '%' || '" + cboHeadRegist.Value + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = to_char(SYSDATE +1,'dd/MM/yyyy') AND (CPLAN IS NULL OR CPLAN = '0')";
                    checkEmployee = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SEMPLOYEEID LIKE '%' || '" + hideEmployeeID.Text + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = to_char(SYSDATE +1,'dd/MM/yyyy') AND (CPLAN IS NULL OR CPLAN = '0')";
                }
                else
                {
                    checkFIFO = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SHEADREGISTERNO LIKE '%' || '" + cboHeadRegist.Value + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = to_char(SYSDATE,'dd/MM/yyyy')  AND (CPLAN IS NULL OR CPLAN = '0')";
                    checkEmployee = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SEMPLOYEEID LIKE '%' || '" + hideEmployeeID.Text + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = to_char(SYSDATE,'dd/MM/yyyy') AND (CPLAN IS NULL OR CPLAN = '0')";
                }

                if (CommonFunction.Count_Value(sql, checkFIFO) > 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','รถทะเบียน " + cboHeadRegist.Value + " ได้ทำการลงคิวแล้วและยังไม่ได้ถูกนำไปจัดแผน !');");
                    return;
                }

                if (CommonFunction.Count_Value(sql, checkEmployee) > 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','รหัสพนักงาน " + hideEmployeeID.Text + " ได้ทำการลงคิวแล้ว !');");
                    return;
                }

                string genID;

                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (!string.IsNullOrEmpty(txtTerminal.Text))
                    {
                        con.Open();

                        string tmpTERMINAL = (txtTerminal.Text.StartsWith("80000")) ? "H102" : txtTerminal.Text;
                        genID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TFIFO ORDER BY NID DESC) WHERE ROWNUM <= 1");
                        string genNO = "0";

                        if (cboDate.SelectedIndex == 1)
                        {
                            genNO = CommonFunction.Gen_ID(con, "SELECT NNO FROM (SELECT NNO FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' AND to_char(DDATE,'dd/MM/yyyy') = to_char(SYSDATE +1,'dd/MM/yyyy') AND STERMINAL = '" + tmpTERMINAL + "'  ORDER BY NNO DESC) WHERE ROWNUM <= 1");
                        }
                        else
                        {
                            genNO = CommonFunction.Gen_ID(con, "SELECT NNO FROM (SELECT NNO FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' AND to_char(DDATE,'dd/MM/yyyy') = to_char(SYSDATE,'dd/MM/yyyy') AND STERMINAL = '" + tmpTERMINAL + "'  ORDER BY NNO DESC) WHERE ROWNUM <= 1");
                        }

                        string strsql = "INSERT INTO TFIFO(NID, NNO, STERMINAL, SHEADREGISTERNO, STRAILERREGISTERNO, SPERSONALNO, SEMPLOYEENAME, STEL, DDATE, SCREATE, DCREATE, SVENDORID,SEMPLOYEEID) VALUES (:NID, :NNO, :STERMINAL, :SHEADREGISTERNO, :STRAILERREGISTERNO, :SPERSONALNO, :SEMPLOYEENAME, :STEL, :DDATE, :SCREATE, SYSDATE, :SVENDORID,:SEMPLOYEEID)";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":NID", OracleType.Number).Value = Convert.ToInt32(genID);
                            com.Parameters.Add(":NNO", OracleType.Number).Value = Convert.ToInt32(genNO);
                            com.Parameters.Add(":STERMINAL", OracleType.VarChar).Value = tmpTERMINAL;
                            com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                            com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                            com.Parameters.Add(":SPERSONALNO", OracleType.VarChar).Value = (cmbPersonalNo.Value + "").Trim();
                            com.Parameters.Add(":SEMPLOYEENAME", OracleType.VarChar).Value = txtFullname.Text.Trim();
                            com.Parameters.Add(":STEL", OracleType.VarChar).Value = txtTel.Text.Trim();

                            if (cboDate.SelectedIndex == 1)
                            {
                                com.Parameters.Add(":DDATE", OracleType.DateTime).Value = DateTime.Now.AddDays(1);
                            }
                            else
                            {
                                com.Parameters.Add(":DDATE", OracleType.DateTime).Value = DateTime.Now;
                            }

                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = txtUserID.Text;
                            com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = txtVenderID.Text;
                            com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = hideEmployeeID.Text;
                            com.ExecuteNonQuery();

                        }

                        //LogUser("18", "I", "ลงคิวเข้ารับงาน โดย พขร.", genID + "");
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");
                    }
                }
                ClearControl();
                gvw.DataBind();
                //}
                //else
                //{
                //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                //}
                break;

            case "ListPage":

                xcpn.JSProperties["cpRedirectTo"] = "fifo_User_regis_lst.aspx?str=" + txtTerminal.Text + "&uid=" + txtUserID.Text;
                break;

        }

    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,SVENDORID AS STRANSPORTID,SVENDORNAME
FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,
c.SCONTRACTNO,c.SVENDORID,vs.SVENDORNAME FROM ((TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) 
INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID) LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID
WHERE  t.SHEADREGISTERNO LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

    }
    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboTrailerRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.STRAILERREGISTERNO) AS RN , 
T.STRAILERREGISTERNO FROM (TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID 
WHERE STRAILERREGISTERNO IS NOT NULL AND T.STRAILERREGISTERNO LIKE :fillter AND c.SVENDORID = :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, txtVenderID.Text);
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }

    protected void cboTrailerRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }


    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SPERSONELNO,FULLNAME,STEL,SDRIVERNO,SEMPLOYEEID FROM (SELECT NVL(E.SPERSONELNO,' ') AS SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL, E.SDRIVERNO,E.SEMPLOYEEID ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND E.SPERSONELNO || E.INAME || ES.FNAME || ' ' || ES.LNAME LIKE :fillter AND E.STRANS_ID LIKE :vendorID) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsPersonal.SelectParameters.Add("vendorID", TypeCode.String, String.Format("%{0}%", txtVenderID.Text));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    //private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    //{
    //    UserTrace trace = new UserTrace(this, sql);
    //    trace.SUID = Session["UserID"] + "";
    //    trace.SMENUID = MENUID;
    //    trace.SCREATE = Session["UserID"] + "";
    //    trace.STYPE = TYPE;
    //    trace.SDESCRIPTION = DESCEIPTION;
    //    trace.SREFERENTID = REFERENTID;
    //    trace.Insert();
    //}
}