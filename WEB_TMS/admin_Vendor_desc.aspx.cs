﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using DevExpress.Web.ASPxGridView;
using System.Text;

public partial class admin_Vendor_desc : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        #endregion

        if (!IsPostBack)
        {
            dteStart.Text = DateTime.Now.AddMonths(-1).Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
           // Response.Write("" + Session["oSVENDORID1"]);
            if (Session["oSVENDORID1"] != null)
            {
                ListData();
            }
        }
    }


    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

        gvw.CancelEdit();
        gvw1.CancelEdit();
        gvw2.CancelEdit();


    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {

            case "view":
                BindData();
                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SCONTRACTID");
                int conid = Convert.ToInt32(data);

                string rowsIndex = paras[1] + "";
                gvw.StartEdit(int.Parse(rowsIndex));
                Literal ltl = (Literal)gvw.FindEditFormTemplateControl("ltlGuarantee");
                Literal ltl1 = (Literal)gvw.FindEditFormTemplateControl("ltlTruck");
                DataTable dt = new DataTable();
                dt = CommonFunction.Get_Data(sql, "SELECT SGUARANTEETYPE,SBOOKNO,NAMOUNT,SREMARK FROM TCONTRACT_GUARANTEES WHERE NCONTRACTID = " + conid);
                if (dt.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    string st = @"<tr><td width='5%' align='center'><span class='style24'>{0}</span></td>
                                <td align='center'><span class='style24'>{1}</span></td>
                                <td align='center'><span class='style24'>{2}</span></td>
                                <td align='center'><span class='style24'>{3}</span></td>
                                <td align='center'><span class='style24'>{4}</span></td>
                              </tr>";
                    int i = 1;
                    foreach (DataRow dr in dt.Rows)
                    {

                        sb.Append(string.Format(st, i, dr["SGUARANTEETYPE"] + "", dr["SBOOKNO"] + "", dr["NAMOUNT"] + "", dr["SREMARK"] + ""));
                        i++;
                    }

                    ltl.Text = sb.ToString();
                    sb = null;
                }
                else
                {
                    if (ltl1 != null)
                    {
                        ltl.Text = "<tr><td align='center' colspan='5' style='color:red'>ไม่มีข้อมูล</td></tr>";
                    }
                }

                DataTable dt1 = new DataTable();
                dt1 = CommonFunction.Get_Data(sql, @"SELECT T.SHEADREGISTERNO,T.STRAILERREGISTERNO,TT.SCARTYPENAME, CASE CT.CSTANDBY WHEN 'N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END SCARTYPE, T.SCHASIS,
                                                    CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NTOTALCAPACITY ELSE T.NTOTALCAPACITY END NTOTALCAPACITY
                                                    ,CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NSLOT ELSE T.NSLOT END NSLOT
                                                    FROM (TCONTRACT_TRUCK ct 
                                                    LEFT JOIN TTRUCK t ON ct.STRUCKID = t.STRUCKID
                                                    LEFT JOIN TTRUCK TRCK_TAIL ON ct.STRAILERID = TRCK_TAIL.STRUCKID
                                                    ) 
                                                    LEFT JOIN TTRUCKTYPE tt ON T.SCARTYPEID = TT.SCARTYPEID WHERE ct.SCONTRACTID = '" + conid + @"' 
                                                    ORDER BY SCARTYPE");

                if (dt1.Rows.Count > 0)
                {
                    StringBuilder sb1 = new StringBuilder();
                    string st1 = @"<tr>
                                <td align='center'><span class='style24'>{0}</span></td>
                                <td width='15%'><span class='style24'>{1}</span></td>
                                <td width='15%'><span class='style24'>{2}</span></td>
                                <td><span class='style24'>{3}</span></td>
                                <td><span class='style24'>{4}</span></td>
                                <td width='15%'><span class='style24'>{5}</span></td>
                                <td align='center'><span class='style24'>{6}</span></td>
                                <td align='center'><span class='style24'>{7}</span></td>
                                </tr>";
                    int i = 1;
                    foreach (DataRow dr in dt1.Rows)
                    {

                        sb1.Append(string.Format(st1, i, dr["SHEADREGISTERNO"] + "", dr["STRAILERREGISTERNO"] + "", dr["SCARTYPENAME"] + "", dr["SCARTYPE"] + "", dr["SCHASIS"] + "", dr["NTOTALCAPACITY"] + "", dr["NSLOT"] + ""));
                        i++;
                    }

                    ltl1.Text = sb1.ToString();
                    sb1 = null;
                }
                else
                {
                    if (ltl1 != null)
                    {
                        ltl1.Text = "<tr><td align='center' colspan='8' style='color:red'>ไม่มีข้อมูล</td></tr>";
                    }
                }

                //ltl1.Text = "<tr><td colspan='5'>ssssssss</td></tr>";
                //gvw.DataBind();

                break;

            case "view1":
                //BindData();
                gvw1.DataBind();
                dynamic data1 = "";
                dynamic carTypeId = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SCARTYPEID");
                dynamic data2 = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRANSPORTTYPE");

                if (carTypeId == 0)
                {
                    data1 = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRUCKID");
                }
                else 
                {
                    data1 = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRAILERID"); 
                }

                string conid1 = data1 + "";
                int TypeTransport = Convert.ToInt32(data2);

                string rowsIndex1 = paras[1] + "";
                gvw1.StartEdit(int.Parse(rowsIndex1));

                Literal ltl3 = (Literal)gvw1.FindEditFormTemplateControl("ltlTruckTotal");
                Literal ltl4 = (Literal)gvw1.FindEditFormTemplateControl("ltlTruckTotal1");

                StringBuilder sb2 = new StringBuilder();
                string st2 = @"
                            <tr>
                              <td width='15%' align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสผู้ขนส่ง</span></td>
                              <td width='19%' align='left'><span class='style24'>{0}</span></td>
                              <td width='15%' align='left' bgcolor='#FFEBD7'><span class='style24'>ชื่อผู้ขนส่ง</span></td>
                              <td width='32%' align='left'><span class='style24'>{1}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ประเภทผู้ขนส่ง</span></td>
                              <td align='left'><span class='style24'>{2}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ชื่อเจ้าของรถ</span></td>
                              <td align='left'><span class='style24'>{3}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ทะเบียนรถ (หัว) </span></td>
                              <td align='left'><span class='style24'>{4}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ทะเบียนรถ(ท้าย)</span></td>
                              <td align='left'><span class='style24'>{5}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสรถ(หัว)</span></td>
                              <td align='left'><span class='style24'>{6}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสรถ(ท้าย)</span></td> 
                              <td align='left'><span class='style24'>{7}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ประเภทรถ</span></td>
                              <td align='left'><span class='style24'>{8}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'></span></td>
                              <td align='left'><span class='style24'></span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>หมายเลขแชชซีย์</span></td>
                              <td align='left'><span class='style24'>{9}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>หมายเลขเครื่องยนต์</span></td>
                              <td align='left'><span class='style24'>{10}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>จำนวนล้อ</span></td>
                              <td align='left'><span class='style24'>{11} ล้อ</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>วันที่จดทะเบียน</span></td>
                              <td align='left'><span class='style24'>{12}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>กลุ่มผลิตภัณฑ์ที่บรรทุก</span></td>
                              <td align='left'><span class='style24'>{13}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>วันหมดอายุวัดน้ำ</span></td>
                              <td align='left'><span class='style24'>{18}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>น้ำหนักบรรทุก</span></td>
                              <td align='left'><span class='style24'>{15} กก.</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>น้ำหนักรถ</span></td>
                              <td align='left'><span class='style24'>{14} กก.</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>จำนวนช่อง</span></td>
                              <td align='left'><span class='style24'>{16} ช่อง</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ความจุรวม</span></td>
                              <td align='left'><span class='style24'>{17} ลิตร </span></td>
                            </tr> ";

                DataTable dt3 = new DataTable();
                if (TypeTransport == 1)
                {
                    dt3 = CommonFunction.Get_Data(sql, @"SELECT T.SCARTYPEID,T.DWATEREXPIRE,T.STRANSPORTID,C.SABBREVIATION CUST_ABBR,TT.STRANS_TYPE_DESC,T.SOWNER_NAME,T.SHEADREGISTERNO,T.STRAILERREGISTERNO,T.STRUCKID SHEADID,T.STRAILERID,TP.SCARTYPENAME
                                                        ,T.SCHASIS,T.SENGINE,T.NWHEELS,T.DREGISTER,T.SPROD_GRP
                                                        ,T.NWEIGHT
                                                        ,T.NLOAD_WEIGHT
                                                        ,CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NSLOT ELSE T.NSLOT END NSLOT
                                                        ,CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NTOTALCAPACITY ELSE T.NTOTALCAPACITY END NTOTALCAPACITY
                                                        FROM ((TTRUCK T 
                                                                LEFT JOIN TTRUCKTYPE TP ON T.SCARTYPEID = TP.SCARTYPEID
                                                                LEFT JOIN TTRUCK TRCK_TAIL ON T.STRAILERID=TRCK_TAIL.STRUCKID
                                                        ) 
                                                        LEFT JOIN TTRANS_TYPE TT ON T.STRANSPORTTYPE = TT.STRANS_TYPE) 
                                                        LEFT JOIN TVENDOR C ON T.STRANSPORTID = C.SVENDORID 
                                                        WHERE T.STRUCKID ='" + CommonFunction.ReplaceInjection(conid1) + "'");
                    if (dt3.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt3.Rows)
                        {
                            string SHEADREGISTENO = "";
                            string STRAILREGISTENO = "";

                            switch (dr["SCARTYPEID"] + "")
                            {
                                case "0": SHEADREGISTENO = dr["SHEADREGISTERNO"] + ""; STRAILREGISTENO = dr["STRAILERREGISTERNO"] + "";
                                    break;
                                case "3": SHEADREGISTENO = dr["SHEADREGISTERNO"] + ""; STRAILREGISTENO = dr["STRAILERREGISTERNO"] + "";
                                    break;
                                case "4": SHEADREGISTENO = dr["STRAILERREGISTERNO"] + ""; STRAILREGISTENO = dr["SHEADREGISTERNO"] + "";
                                    break;
                            }
                            sb2.Append(string.Format(st2, dr["STRANSPORTID"] + "", dr["CUST_ABBR"] + "", dr["STRANS_TYPE_DESC"] + "", dr["SOWNER_NAME"] + "", SHEADREGISTENO, STRAILREGISTENO, dr["SHEADID"] + "", dr["STRAILERID"] + "", dr["SCARTYPENAME"] + "", dr["SCHASIS"] + "", dr["SENGINE"] + "", dr["NWHEELS"] + "", dr["DREGISTER"] + "", dr["SPROD_GRP"] + "", dr["NWEIGHT"] + "", dr["NLOAD_WEIGHT"] + "", dr["NSLOT"] + "", dr["NTOTALCAPACITY"] + "", dr["DWATEREXPIRE"] + ""));
                        }
                        ltl3.Text = sb2.ToString();
                        sb2 = null;
                    }
                    else
                    {
                        if (ltl3 != null)
                        {
                            sb2.Append(string.Format(st2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
                            ltl3.Text = sb2.ToString();
                        }
                    }
                }
                else
                {
                    dt3 = CommonFunction.Get_Data(sql, "SELECT T.SCARTYPEID,T.DWATEREXPIRE,T.STRANSPORTID,v.SABBREVIATION,TT.STRANS_TYPE_DESC,T.SOWNER_NAME,T.SHEADREGISTERNO,T.STRAILERREGISTERNO,T.SHEADID,T.STRAILERID,TP.SCARTYPENAME,T.SCHASIS,T.SENGINE,T.NWHEELS,T.DREGISTER,T.SPROD_GRP,T.NWEIGHT,T.NLOAD_WEIGHT,T.NSLOT,T.NTOTALCAPACITY FROM ((TTRUCK T INNER JOIN TTRUCKTYPE TP ON T.SCARTYPEID = TP.SCARTYPEID)  LEFT JOIN TTRANS_TYPE TT ON T.STRANSPORTTYPE = TT.STRANS_TYPE) LEFT JOIN TVENDOR v ON T.STRANSPORTID = V.SVENDORID WHERE T.STRUCKID ='" + CommonFunction.ReplaceInjection(conid1) + "'");
                    if (dt3.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt3.Rows)
                        {
                            string SHEADREGISTENO = "";
                            string STRAILREGISTENO = "";

                            switch (dr["SCARTYPEID"] + "")
                            {
                                case "0": SHEADREGISTENO = dr["SHEADREGISTERNO"] + ""; STRAILREGISTENO = dr["STRAILERREGISTERNO"] + "";
                                    break;
                                case "3": SHEADREGISTENO = dr["SHEADREGISTERNO"] + ""; STRAILREGISTENO = dr["STRAILERREGISTERNO"] + "";
                                    break;
                                case "4": SHEADREGISTENO = dr["STRAILERREGISTERNO"] + ""; STRAILREGISTENO = dr["SHEADREGISTERNO"] + "";
                                    break;
                            }


                            sb2.Append(string.Format(st2, dr["STRANSPORTID"] + "", dr["SABBREVIATION"] + "", dr["STRANS_TYPE_DESC"] + "", dr["SOWNER_NAME"] + "", SHEADREGISTENO, STRAILREGISTENO, dr["SHEADID"] + "", dr["STRAILERID"] + "", dr["SCARTYPENAME"] + "", dr["SCHASIS"] + "", dr["SENGINE"] + "", dr["NWHEELS"] + "", dr["DREGISTER"] + "", dr["SPROD_GRP"] + "", dr["NWEIGHT"] + "", dr["NLOAD_WEIGHT"] + "", dr["NSLOT"] + "", dr["NTOTALCAPACITY"] + "", dr["DWATEREXPIRE"] + ""));
                        }
                        ltl3.Text = sb2.ToString();
                        sb2 = null;
                    }
                    else
                    {
                        if (ltl3 != null)
                        {
                            sb2.Append(string.Format(st2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
                            ltl3.Text = sb2.ToString();
                        }
                    }
                }

                DataTable dt4 = new DataTable();
                dt4 = CommonFunction.Get_Data(sql, "SELECT NCOMPARTNO,NPANLEVEL,NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID = '" + conid1 + "' ORDER BY NCOMPARTNO,NPANLEVEL");

                if (dt4.Rows.Count > 0)
                {
                    StringBuilder sb1 = new StringBuilder();
                    string st1 = @"<tr>
                                    <td align='center' class='style24'>{0}</td>
                                    <td align='center' class='style24'>{1}</td>
                                    <td align='center' class='style24'>{2}</td>
                                  </tr>";
                    int i = 1;
                    foreach (DataRow dr in dt4.Rows)
                    {
                        sb1.Append(string.Format(st1, dr["NCOMPARTNO"] + "", dr["NPANLEVEL"] + "", dr["NCAPACITY"] + ""));
                        i++;
                    }

                    ltl4.Text = sb1.ToString();
                    sb1 = null;
                }
                else
                {
                    if (ltl4 != null)
                    {
                        ltl4.Text = "<tr><td align='center' colspan='3' style='color:red'>ไม่มีข้อมูล</td></tr>";
                    }
                }
                break;

            case "view2":
                string rowsIndex2 = paras[1] + "";
                gvw2.StartEdit(int.Parse(rowsIndex2));
                break;

            case "Search":
                BindData();
                break;

            case "Search1":
                gvw1.DataBind();
                break;

            case "Search2":
                gvw2.DataBind();
                break;
        }
    }

    void ListData()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT s.SVENDORNAME,v.sAbbreviation , (s.sNo || ' ' || s.sDistrict || ' ' || s.sRegion || ' ' || s.sProvince || ' ' || s.sProvinceCode) AS ADDRESS,v.sTel , v.sFax,v.dStartPTT,wm_concat(' ' || c.SCOORDINATORNAME) As SCONAMEAPPEND FROM (TVENDOR v INNER JOIN TVENDOR_SAP s ON v.SVENDORID = s.SVENDORID) LEFT JOIN TCO_VENDOR c ON v.SVENDORID = c.SVENDORID WHERE  s.SVENDORID = '" + Session["oSVENDORID1"] + "" + "' GROUP BY s.SVENDORNAME,v.sAbbreviation , s.sNo,s.sDistrict,s.sRegion,s.sProvince,s.sProvinceCode,v.sTel , v.sFax,v.dStartPTT");
        if (dt.Rows.Count > 0)
        {
            lblName.Text = dt.Rows[0]["SVENDORNAME"] + "";
            lblSubname.Text = dt.Rows[0]["SABBREVIATION"] + "";
            lblAddress.Text = dt.Rows[0]["ADDRESS"] + "";
            lblTelephone.Text = dt.Rows[0]["STEL"] + "";
            lblFax.Text = dt.Rows[0]["SFAX"] + "";
            lblCo.Text = dt.Rows[0]["SCONAMEAPPEND"] + "";
            lblDate.Text = dt.Rows[0]["DSTARTPTT"] + "";
        }
    }
    void SetWebSitemap()
    {

    }
    void BindData()
    {
        if (ASPxPageControl1.ActiveTabIndex == 1)
        {
            string str = "";

            //Cache.Remove(sds.CacheKeyDependency);
            //Cache[sds.CacheKeyDependency] = new object();
            //sds.Select(new System.Web.UI.DataSourceSelectArguments());
            //sds.DataBind();
            sds.SelectCommand = "";
            sds.SelectParameters.Clear();
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                str = "AND (c.SCONTRACTNO LIKE '%' || :oSearch || '%' OR ct.SCONTRACTTYPENAME LIKE '%' || :oSearch || '%')  ";

                sds.SelectParameters.Add("oSearch", txtSearch.Text);

            }

            switch (cboGroup.SelectedIndex)
            {
                case 0:
                    sds.SelectCommand = @"SELECT C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME ,C.DBEGIN, C.DEND, C.NTRUCK, SUM(CASE WHEN  NVL(TCT.CSTANDBY,'N')='Y' THEN 1 ELSE 0 END)  NSTANDBYTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME ,REPLACE(wm_concat(' ' || CTR.SCOORDINATORNAME),',',NULL) As SCONAMEAPPEND 
                                        FROM (TCONTRACT C LEFT JOIN TCONTRACTTYPE CT ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID
                                               LEFT JOIN TVENDOR_SAP V ON C.SVENDORID = V.SVENDORID
                                               LEFT JOIN TTERMINAL T ON C.STERMINALID = T.STERMINALID)
                                        LEFT JOIN TCO_CONTRACT CTR ON C.SCONTRACTID = CTR.SCONTRACTID
                                        LEFT JOIN TCONTRACT_TRUCK TCT ON C.SCONTRACTID = TCT.SCONTRACTID
                                        WHERE 1 = 1 " + str + @"
                                        AND c.SVENDORID = :oSVENDORID AND c.cActive = 'Y' 
                                        AND  (
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  <= TO_DATE(c.DBEGIN,'dd/MM/yyyy')  AND  TO_DATE(c.DBEGIN,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  >= TO_DATE(c.DEND,'dd/MM/yyyy')  AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(c.DBEGIN,'DD/MM/YYYY')  <=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(:oEnd,'dd/MM/yyyy')  <= TO_DATE(c.DEND,'dd/MM/yyyy')  OR
                                            TO_DATE(c.DBEGIN,'dd/MM/yyyy')  >=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy')  
                                        )

                                        GROUP BY C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME, C.DBEGIN, C.DEND, C.NTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME";
                    break;
                case 1:
                    sds.SelectCommand = @"SELECT C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME ,C.DBEGIN, C.DEND, C.NTRUCK,SUM(CASE WHEN  NVL(TCT.CSTANDBY,'N')='Y' THEN 1 ELSE 0 END)  NSTANDBYTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME ,REPLACE(wm_concat(' ' || CTR.SCOORDINATORNAME),',',NULL) As SCONAMEAPPEND 
                                        FROM (TCONTRACT C LEFT JOIN TCONTRACTTYPE CT ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID
                                               LEFT JOIN TVENDOR_SAP V ON C.SVENDORID = V.SVENDORID
                                               LEFT JOIN TTERMINAL T ON C.STERMINALID = T.STERMINALID)
                                        LEFT JOIN TCO_CONTRACT CTR ON C.SCONTRACTID = CTR.SCONTRACTID
                                        LEFT JOIN TCONTRACT_TRUCK TCT ON C.SCONTRACTID = TCT.SCONTRACTID
                                        WHERE 1 = 1 " + str + @" AND c.SVENDORID = :oSVENDORID AND (c.cActive = 'N' Or c.cActive IS NULL) 
--AND c.DBEGIN >=  TO_DATE(:oBegin,'dd/MM/yyyy') AND c.DEND <= TO_DATE(:oEnd,'dd/MM/yyyy')  
                                        AND  (
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  <= TO_DATE(c.DBEGIN,'dd/MM/yyyy')  AND  TO_DATE(c.DBEGIN,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  >= TO_DATE(c.DEND,'dd/MM/yyyy')  AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(c.DBEGIN,'DD/MM/YYYY')  <=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(:oEnd,'dd/MM/yyyy')  <= TO_DATE(c.DEND,'dd/MM/yyyy')  OR
                                            TO_DATE(c.DBEGIN,'dd/MM/yyyy')  >=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy')  
                                        )
                                        GROUP BY C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME, C.DBEGIN, C.DEND, C.NTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME";
                    break;
            }
            sds.SelectParameters.Add("oSVENDORID", "" + Session["oSVENDORID1"]);
            sds.SelectParameters.Add("oBegin", dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sds.SelectParameters.Add("oEnd", dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));

            sds.DataBind();
            gvw.DataBind();
        }
    }
}