﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCalendar.ascx.cs" Inherits="ucCalendar" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>

<link href="fullcalendar/lib/cupertino/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link href='fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="fullcalendar/styleButton.css" rel="stylesheet" type="text/css" />



<script src='fullcalendar/lib/moment.min.js'></script>
<script src="fullcalendar/lang/th.js" type="text/javascript"></script>
<script src='fullcalendar/fullcalendar.min.js'></script>
<script src='fullcalendar/lib/jquery-ui.custom.min.js'></script>

<script type="text/javascript">

      $(document).ready(function(s){
          $('tr[id$=trVisible]').attr('class', 'displayNone');
      });

    var month = "ม.ค.";
    function OnClick(s, e) {
        s.Focus();
        month = s.GetText();
    }
    function OnPrevClick(s, e) {
        lblYear.SetText(parseInt(lblYear.GetText()) - 1);
        month = "ม.ค.";
    }
    function OnNextClick(s, e) {
        lblYear.SetText(parseInt(lblYear.GetText()) + 1);
        month = "ม.ค.";
    }
    function OnOkClick(s, e) {
        dde.SetText(month + "-" + lblYear.GetText());
        dde.HideDropDown();
        $("input[id$=btnCallbackCalendar]").click();
    }


    function SetDataCalendar(sValue) {

        lblCalendarDate.SetValue(sValue + "")
        txtCalendarDate.SetValue(sValue + "");
        // $("input[id$=btnAddDataCalendar]").click();

        $('tr[id$=trVisible]').attr('class', 'displayNone');
        
    }

    

</script>
<style type="text/css">
    .buttonMonth
    {
        border-width: 1px;
        width: 50px;
        text-align: center;
    }
    .tab
    {
        width: 100%;
        border-color: #600;
        border-width: 0 0 1px 1px;
        border-style: solid;
    }
    .cell
    {
        border-color: #600;
        border-width: 1px 1px 0 0;
        border-style: solid;
        margin: 0;
        padding: 4px;
        background-color: #eee6a3;
    }
    
      .td_x
    {
        padding-bottom: 10px;
    }
    #loading 
    {
    	text-align:right;
		display: none;
		top: 10px;
		right: 10px;
	}
	
	    .displayNone
       {
       	 display:none;
       }
       
       
       .displayTR
       {
       	 display: table-row;
       }
       
</style>
<asp:Literal ID="ltrjQueryScript" runat="server"></asp:Literal>
<table  width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="#CCCCCC">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="5" cellpadding="0" bgcolor="#FFFFFF">
                <tr>
                    <td>
                      <dx:ASPxTextBox ID="txtValueCalendar" runat="server" ClientVisible="false" ClientInstanceName="txtValueCalendar">
                                    </dx:ASPxTextBox>
                       <asp:Button ID="btnAddDataCalendar" runat="server" Style="display: none" OnClick="btnAddDataCalendar_Click" />
                       <%-- <table>
                            <tr>
                                <td>
                                    กรุณาเลือกวันที่ปฏิทิน :
                                </td>
                                <td>
                                    <dx:ASPxDropDownEdit ID="ddeCalendar" runat="server" OnInit="ddeCalendar_Init" Width="100"
                                        ClientInstanceName="dde">
                                    </dx:ASPxDropDownEdit>
                                    <asp:Button ID="btnCallbackCalendar" runat="server" OnClick="btnCallbackCalendar_Click"
                                        Style="display: none" />
                                   
                                    <dx:ASPxTextBox ID="txtValueCalendar" runat="server" ClientVisible="false" ClientInstanceName="txtValueCalendar">
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                        </table>--%>
                        <div style="width: 100%;" >
                            <div style="width:100%; text-align:center;">
                            <ul id="months-list"  style=" margin-bottom:-22px"></ul>
                            </div>
                            
                            <div id='calendar'>
                            </div>
                            <%--<asp:ImageButton ID="Call" ImageUrl=""  />--%>
                            <asp:Literal ID="ltrmonthcalendar" runat="server"></asp:Literal>
                            <asp:Literal ID="ltrday" runat="server"></asp:Literal>
                        </div>
                    </td>
                </tr>
            </table>
            <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
                </dx:ASPxLoadingPanel>
        </td>
    </tr>
     <tr>
        <td><font color="red">* การจองคิววัดน้ำจะสมบูรณ์เมื่อได้รับการยืนยันจาก มว.</font>
        </td>
    </tr>
    <tr>
        <td><font color="red">* การเลือกวันนัดหมายหากเลยวันหมดอายุวัดน้ำ จะไม่สามารถรับงานขนส่งกับ
            ปตท. ได้</font></td>
    </tr>
</table>
