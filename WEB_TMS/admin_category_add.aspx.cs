﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;

public partial class admin_category_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
          
            rblSubCategory.ClientVisible = false;
            sdsSubCategory.SelectParameters.Clear();
            sdsSubCategory.SelectParameters.Add("SHEADTYPEID", "01");
            sdsSubCategory.DataBind();
            rblSubCategory.DataBind();

            Session["SubCount"] = 0;
            if (Session["oSCATEGORYID"] != null)
            {
                rblCategory.DataBind();
                listData();

            }

            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnSubmit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {

        Session["oSCATEGORYID"] = null;
        Session["SubCount"] = null;
        rblCategory.DataSource = null;
        rblSubCategory.DataSource = null;
        cbxCategoryname.SelectedIndex = -1;
        txtNImpact.Text = "";
        txtSImpact.Text = "";
        txtDefine.Text = "";
        rblStatus.Value = 0;


    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        if (CanWrite)
        {
            switch (paras[0])
            {


                case "Save":

                    if (Session["oSCATEGORYID"] == null)
                    {
                        Session["oSCATEGORYID"] = CommonFunction.Gen_ID(sql, "SELECT SCATEGORYID FROM (SELECT SCATEGORYID FROM TCATEGORY ORDER BY CAST(SCATEGORYID AS INT) DESC) WHERE ROWNUM <= 1");

                        if (Convert.ToInt16(Session["SubCount"]) != 0)
                        {
                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "INSERT INTO TCATEGORY(SREMARK,SCATEGORYID, SCATEGORYTYPEID, SCATEGORYNAME, SIMPACTLEVEL, NIMPACTLEVEL, SDEFINE, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE) VALUES (:SREMARK,:SCATEGORYID, :SCATEGORYTYPEID, :SCATEGORYNAME, :SIMPACTLEVEL, :NIMPACTLEVEL, :SDEFINE, :CACTIVE, SYSDATE, :SCREATE, SYSDATE, :SUPDATE)";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDescription.Text;
                                    com.Parameters.Add(":SCATEGORYID", OracleType.VarChar).Value = Session["oSCATEGORYID"].ToString();
                                    com.Parameters.Add(":SCATEGORYTYPEID", OracleType.VarChar).Value = paras[1];
                                    com.Parameters.Add(":SCATEGORYNAME", OracleType.VarChar).Value = cbxCategoryname.Value;
                                    com.Parameters.Add(":SIMPACTLEVEL", OracleType.VarChar).Value = txtSImpact.Text;
                                    com.Parameters.Add(":NIMPACTLEVEL", OracleType.Int32).Value = Convert.ToInt32(txtNImpact.Text);
                                    com.Parameters.Add(":SDEFINE", OracleType.VarChar).Value = txtDefine.Text;
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus.Value;
                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.ExecuteNonQuery();
                                }

                            }
                        }
                        else
                        {
                            sds.Insert();
                        }

                        LogUser("33", "I", "บันทึกข้อมูลหน้า Category", Session["oSCATEGORYID"] + "");
                    }
                    else
                    {
                        if (Convert.ToInt16(Session["SubCount"]) != 0)
                        {
                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TCATEGORY SET SREMARK = :SREMARK, SCATEGORYTYPEID = :SCATEGORYTYPEID, SCATEGORYNAME = :SCATEGORYNAME, SIMPACTLEVEL = :SIMPACTLEVEL, NIMPACTLEVEL = :NIMPACTLEVEL, SDEFINE = :SDEFINE, CACTIVE = :CACTIVE, DUPDATE = SYSDATE, SUPDATE = :SUPDATE WHERE SCATEGORYID = :SCATEGORYID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDescription.Text;
                                    com.Parameters.Add(":SCATEGORYTYPEID", OracleType.VarChar).Value = paras[1];
                                    com.Parameters.Add(":SCATEGORYNAME", OracleType.VarChar).Value = cbxCategoryname.Value;
                                    com.Parameters.Add(":SIMPACTLEVEL", OracleType.VarChar).Value = txtSImpact.Text;
                                    com.Parameters.Add(":NIMPACTLEVEL", OracleType.Int32).Value = Convert.ToInt32(txtNImpact.Text);
                                    com.Parameters.Add(":SDEFINE", OracleType.VarChar).Value = txtDefine.Text;
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus.Value;
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SCATEGORYID", OracleType.VarChar).Value = Session["oSCATEGORYID"].ToString();
                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                        else
                        {
                            sds.Update();
                        }

                        LogUser("33", "E", "แก้ไขข้อมูลหน้า Category",Session["oSCATEGORYID"] + "");
                    }


                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_category_lst.aspx';});");

                    ClearControl();

                    break;

                case "ShowSubCategory":
                    rblSubCategory.ClientVisible = true;
                    sdsSubCategory.SelectParameters.Clear();
                    sdsSubCategory.SelectParameters.Add("SHEADTYPEID", rblCategory.Value.ToString());
                    sdsSubCategory.DataBind();
                    rblSubCategory.DataBind();
                    Session["SubCount"] = rblSubCategory.Items.Count;

                    break;

                case "ShowSubCategory1":
                    Session["SubCount"] = rblSubCategory.Items.Count;
                    break;
            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }

    }



    void listData()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT CT.SHEADTYPEID,C.SCATEGORYTYPEID,C.SCATEGORYNAME,C.SIMPACTLEVEL,C.NIMPACTLEVEL,C.SDEFINE,C.CACTIVE,C.SREMARK FROM TCATEGORY C left join TCATEGORYTYPE CT ON C.SCATEGORYTYPEID=CT.SCATEGORYTYPEID WHERE C.SCATEGORYID = '" + Session["oSCATEGORYID"].ToString() + "'");
        if (dt.Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(dt.Rows[0][0] + ""))
            {
                
                sdsSubCategory.SelectParameters.Clear();
                sdsSubCategory.SelectParameters.Add("SHEADTYPEID", dt.Rows[0][0] + "");
                sdsSubCategory.DataBind();
                rblSubCategory.DataBind();
                rblSubCategory.ClientVisible = true;

                rblCategory.Value = dt.Rows[0][0] + "";
                rblSubCategory.Value = dt.Rows[0][1] + "";
                cbxCategoryname.Value = dt.Rows[0][2] + "";
                txtSImpact.Text = dt.Rows[0][3] + "";
                txtNImpact.Text = dt.Rows[0][4] + "";
                txtDefine.Text = dt.Rows[0][5] + "";
                rblStatus.Value = dt.Rows[0][6] + "";
                txtDescription.Text = dt.Rows[0][7] + "";

                Session["SubCount"] = 1;

            }
            else
            {

                rblCategory.Value = dt.Rows[0][1] + "";
                cbxCategoryname.Value = dt.Rows[0][2] + "";
                txtSImpact.Text = dt.Rows[0][3] + "";
                txtNImpact.Text = dt.Rows[0][4] + "";
                txtDefine.Text = dt.Rows[0][5] + "";
                rblStatus.Value = dt.Rows[0][6] + "";
                txtDescription.Text = dt.Rows[0][7] + "";
            }
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}