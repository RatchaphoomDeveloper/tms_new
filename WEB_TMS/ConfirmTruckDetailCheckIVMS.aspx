﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ConfirmTruckDetailCheckIVMS.aspx.cs" Inherits="ConfirmTruckDetailCheckIVMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="form-horizontal">
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a class="accordion-toggle" data-toggle="collapse" href="#collapse1" id="acollapse1">ค้นหา</a>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row form-group">
                                <label class="col-md-3 control-label">บริษัทผู้ขนส่ง :<asp:Label Text=" *" runat="server" ForeColor="Red" /></label></label>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>

                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">
                                    ทเบียนรถ(หัว)
                                    <asp:Label Text=" *" runat="server" ForeColor="Red" /></label>
                                <div class="col-md-3">
                                    <dx:ASPxComboBox ID="ddlSHEADREGISTERNO" runat="server" Width="100%" ClientInstanceName="cboVendor" CssClass="form-control" TextFormatString="{0}" ValueField="SHEADREGISTERNO" IncrementalFilteringMode="Contains" >
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="ทะเบียน(หัว)" FieldName="SHEADREGISTERNO"  />
                                                            <dx:ListBoxColumn Caption="ทะเบียน(หาง)" FieldName="STRAILERREGISTERNO" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                </div>
                                <label class="col-md-3 control-label">คลังต้นทางประสงค์เข้ารับงาน<asp:Label Text=" *" runat="server" ForeColor="Red" /></label></label>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTTERMINAL">
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <asp:Button Text="ตรวจสอบ IVMS" runat="server" ID="btnCheckIVMS" class="btn btn-md bth-hover btn-info" OnClick="btnCheckIVMS_Click" style="background-color:yellow;border-color:antiquewhite;color:black;" />
                        <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                        
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2"><i class="fa fa-table"></i>ผลการค้นหา จำนวน
                <asp:Label Text="0" runat="server" ID="lblItem" />
                            รายการ </a>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <asp:GridView runat="server" ID="gvData"
                                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                    HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]" OnPageIndexChanging="gvData_PageIndexChanging" OnRowDataBound="gvData_RowDataBound">
                                    <Columns>
                                       
                                        <asp:BoundField HeaderText="ทะเบียนหัว" DataField="SHEADREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        
                                        <asp:BoundField HeaderText="คลังต้นทาง" HtmlEncode="false" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                       
                                        <asp:BoundField HeaderText="GPS" DataField="IVMS_GPS_STAT_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="true" />
                                        <asp:BoundField HeaderText="กล้อง" DataField="IVMS_MDVR_STAT_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="true" />
                                        <asp:BoundField HeaderText="สถานะ Tracking" DataField="STATUS" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="true" />
                                        <asp:BoundField HeaderText="การกระจัด(กม.)" HtmlEncode="false" DataField="Radius_Km" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="true" />
                                        <asp:BoundField HeaderText="วันที่อัพเดต" DataField="DateTime" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="true" />
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
