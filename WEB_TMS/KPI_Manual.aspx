﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Manual.aspx.cs" Inherits="KPI_Manual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="form-horizontal">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>สรุปผลประเมินดัชนีชี้วัดประสิทธิภาพการทำงาน KPI (รายเดือน)
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ปี</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" >
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เดือน</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlMonth" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เลขที่สัญญา</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">สถานะ</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlStatus" >
                                </asp:DropDownList>
                            </div>
       
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">

                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                        <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                        <asp:Button Text="Batch Input" runat="server" ID="btnImport" class="btn btn-md bth-hover btn-info" OnClick="btnImport_Click" />
                    </div>
                </div>

                
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>ประจำปี : <asp:Label Text="" runat="server" ID="lblItem" /> <asp:Label ID="lblVendorRemark" runat="server" Text=""></asp:Label>
            </div>
            <div class="panel-body">
                <asp:GridView runat="server" ID="gvKPI"
                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                    HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]" PageSize="50"
                    AllowPaging="True" OnRowUpdating="gvKPI_RowUpdating" DataKeyNames="KPI_HEADER_ID,YEAR,MONTH_ID,SCONTRACTID,SVENDORID" OnPageIndexChanging="gvKPI_PageIndexChanging">
                    <Columns>
                        <%--<asp:TemplateField HeaderText = "ลำดับที่" ItemStyle-Width="80">
                            <ItemTemplate>
                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="KPI_HEADER_ID" DataField="kpi_header_id" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                        
                        <asp:BoundField HeaderText="บริษัทผู้ขนส่ง" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"  />
                        <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="สถานะ" DataField="STATUS_NAME" HeaderStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="ใส่ข้อมูล KPI" CommandName="update"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="ROW_COLOR" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblRowColor" runat="server" Text='<%# Eval("ROW_COLOR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ลำดับที่">
                            <ItemTemplate>
                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="KPI_HEADER_ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblKPIHeaderID" runat="server" Text='<%# Eval("KPI_HEADER_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="บริษัทผู้ขนส่ง">
                            <ItemTemplate>
                                <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("SABBREVIATION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="เลขที่สัญญา">
                            <ItemTemplate>
                                <asp:Label ID="lblContractNo" runat="server" Text='<%# Eval("SCONTRACTNO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะ">
                            <ItemTemplate>
                                <asp:Label ID="lblStatusName" runat="server" Text='<%# Eval("STATUS_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ข้อมูล KPI" ItemStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="ใส่ข้อมูล KPI" CommandName="update"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="50" />
                                            <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

