﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_appeal_lst.aspx.cs" Inherits="vendor_appeal_lst" StylesheetTheme="Aqua" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPanel" Assembly="DevExpress.Web.v11.2" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function VisibleControl() {

            var bool;

            if (txtDocumentOther.GetValue() != "I" && txtDocumentOther.GetValue() != "F") {
                switch (txtType.GetValue()) {
                    case "011":
                        lblName0.SetValue("1) รูปภาพอุปกรณ์ที่มีปัญหา");
                        bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
                        uploader0.SetClientVisible(bool);
                        txtFileName0.SetClientVisible(!bool);
                        btnView0.SetEnabled(!bool);
                        btnDelFile0.SetEnabled(!bool);

                        lblName1.SetValue("2) เอกสารตรวจสอบรถขนส่งประจำวัน");
                        bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
                        uploader1.SetClientVisible(bool);
                        txtFileName1.SetClientVisible(!bool);
                        btnView1.SetEnabled(!bool);
                        btnDelFile1.SetEnabled(!bool);

                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(false);
                        btnView2.SetClientVisible(false);
                        btnDelFile2.SetClientVisible(false);

                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(false);
                        btnView3.SetClientVisible(false);
                        btnDelFile3.SetClientVisible(false);

                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(false);
                        btnView4.SetClientVisible(false);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "090":
                        lblName0.SetValue("1) ดำเนินการตามรายงานอุบัติเหตุ");
                        bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
                        uploader0.SetClientVisible(bool);
                        txtFileName0.SetClientVisible(!bool);
                        btnView0.SetEnabled(!bool);
                        btnDelFile0.SetEnabled(!bool);

                        lblName1.SetValue("2) ข้อมูลระบบ IVMS");
                        bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
                        uploader1.SetClientVisible(bool);
                        txtFileName1.SetClientVisible(!bool);
                        btnView1.SetEnabled(!bool);
                        btnDelFile1.SetEnabled(!bool);

                        lblName2.SetValue("3) ภาพถ่าย");
                        bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
                        uploader2.SetClientVisible(bool);
                        txtFileName2.SetClientVisible(!bool);
                        btnView2.SetEnabled(!bool);
                        btnDelFile2.SetEnabled(!bool);

                        lblName3.SetValue("4) เอกสารประกอบอื่น ๆ");
                        bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
                        uploader3.SetClientVisible(bool);
                        txtFileName3.SetClientVisible(!bool);
                        btnView3.SetEnabled(!bool);
                        btnDelFile3.SetEnabled(!bool);

                        lblName4.SetValue("5) เอกสารประกอบอื่น ๆ");
                        bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
                        uploader4.SetClientVisible(bool);
                        txtFileName4.SetClientVisible(!bool);
                        btnView4.SetEnabled(!bool);
                        btnDelFile4.SetEnabled(!bool);
                        break;
                    case "010":
                        lblName0.SetValue("1) แผนงานขนส่ง, ใบแนะนำการเติม");
                        bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
                        uploader0.SetClientVisible(bool);
                        txtFileName0.SetClientVisible(!bool);
                        btnView0.SetEnabled(!bool);
                        btnDelFile0.SetEnabled(!bool);

                        lblName1.SetValue("2) ใบกำกับการขนส่ง");
                        bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
                        uploader1.SetClientVisible(bool);
                        txtFileName1.SetClientVisible(!bool);
                        btnView1.SetEnabled(!bool);
                        btnDelFile1.SetEnabled(!bool);

                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(false);
                        btnView2.SetClientVisible(false);
                        btnDelFile2.SetClientVisible(false);

                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(false);
                        btnView3.SetClientVisible(false);
                        btnDelFile3.SetClientVisible(false);

                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(false);
                        btnView4.SetClientVisible(false);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "020":
                        lblName0.SetValue("1) ข้อมูล GPS");
                        bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
                        uploader0.SetClientVisible(bool);
                        txtFileName0.SetClientVisible(!bool);
                        btnView0.SetEnabled(!bool);
                        btnDelFile0.SetEnabled(!bool);

                        uploader1.SetClientVisible(false);
                        txtFileName1.SetClientVisible(false);
                        btnView1.SetClientVisible(false);
                        btnDelFile1.SetClientVisible(false);

                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(false);
                        btnView2.SetClientVisible(false);
                        btnDelFile2.SetClientVisible(false);

                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(false);
                        btnView3.SetClientVisible(false);
                        btnDelFile3.SetClientVisible(false);

                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(false);
                        btnView4.SetClientVisible(false);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "080":
                        lblName0.SetValue("1) เอกสารชี้แจง");
                        bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
                        uploader0.SetClientVisible(bool);
                        txtFileName0.SetClientVisible(!bool);
                        btnView0.SetEnabled(!bool);
                        btnDelFile0.SetEnabled(!bool);

                        lblName1.SetValue("2) ข้อมูลระบบ IVMS");
                        bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
                        uploader1.SetClientVisible(bool);
                        txtFileName1.SetClientVisible(!bool);
                        btnView1.SetEnabled(!bool);
                        btnDelFile1.SetEnabled(!bool);

                        lblName2.SetValue("3) ภาพถ่าย");
                        bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
                        uploader2.SetClientVisible(bool);
                        txtFileName2.SetClientVisible(!bool);
                        btnView2.SetEnabled(!bool);
                        btnDelFile2.SetEnabled(!bool);

                        lblName3.SetValue("4) เอกสารประกอบอื่น ๆ");
                        bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
                        uploader3.SetClientVisible(bool);
                        txtFileName3.SetClientVisible(!bool);
                        btnView3.SetEnabled(!bool);
                        btnDelFile3.SetEnabled(!bool);

                        lblName4.SetValue("5) เอกสารประกอบอื่น ๆ");
                        bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
                        uploader4.SetClientVisible(bool);
                        txtFileName4.SetClientVisible(!bool);
                        btnView4.SetEnabled(!bool);
                        btnDelFile4.SetEnabled(!bool);
                        break;
                    case "100":
                        lblName0.SetValue("1) เอกสารการอุทธรณ์");
                        bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
                        uploader0.SetClientVisible(bool);
                        txtFileName0.SetClientVisible(!bool);
                        btnView0.SetEnabled(!bool);
                        btnDelFile0.SetEnabled(!bool);

                        lblName1.SetValue("2) เอกสารการอุทธรณ์");
                        bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
                        uploader1.SetClientVisible(bool);
                        txtFileName1.SetClientVisible(!bool);
                        btnView1.SetEnabled(!bool);
                        btnDelFile1.SetEnabled(!bool);

                        lblName2.SetValue("3) เอกสารการอุทธรณ์");
                        bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
                        uploader2.SetClientVisible(bool);
                        txtFileName2.SetClientVisible(!bool);
                        btnView2.SetEnabled(!bool);
                        btnDelFile2.SetEnabled(!bool);

                        lblName3.SetValue("4) เอกสารการอุทธรณ์");
                        bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
                        uploader3.SetClientVisible(bool);
                        txtFileName3.SetClientVisible(!bool);
                        btnView3.SetEnabled(!bool);
                        btnDelFile3.SetEnabled(!bool);

                        lblName4.SetValue("5) เอกสารการอุทธรณ์");
                        bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
                        uploader4.SetClientVisible(bool);
                        txtFileName4.SetClientVisible(!bool);
                        btnView4.SetEnabled(!bool);
                        btnDelFile4.SetEnabled(!bool);
                        break;
                }

            }
            else {

                switch (txtType.GetValue()) {
                    case "011":
                        lblName0.SetValue("1) รูปภาพอุปกรณ์ที่มีปัญหา");
                        uploader0.SetClientVisible(false);
                        txtFileName0.SetClientVisible(true);
                        txtFileName0.SetEnabled(false);
                        btnView0.SetEnabled(true);
                        btnDelFile0.SetClientVisible(false);

                        lblName1.SetValue("2) เอกสารตรวจสอบรถขนส่งประจำวัน");
                        uploader1.SetClientVisible(false);
                        txtFileName1.SetClientVisible(true);
                        txtFileName1.SetEnabled(false);
                        btnView1.SetEnabled(true);
                        btnDelFile1.SetClientVisible(false);

                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(false);
                        btnView2.SetClientVisible(false);
                        btnDelFile2.SetClientVisible(false);

                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(false);
                        btnView3.SetClientVisible(false);
                        btnDelFile3.SetClientVisible(false);

                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(false);
                        btnView4.SetClientVisible(false);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "090":
                        lblName0.SetValue("1) ดำเนินการตามรายงานอุบัติเหตุ");
                        uploader0.SetClientVisible(false);
                        txtFileName0.SetClientVisible(true);
                        txtFileName0.SetEnabled(false);
                        btnView0.SetEnabled(true);
                        btnDelFile0.SetClientVisible(false);

                        uploader1.SetClientVisible(false);
                        txtFileName1.SetClientVisible(false);
                        btnView1.SetClientVisible(false);
                        btnDelFile1.SetClientVisible(false);

                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(false);
                        btnView2.SetClientVisible(false);
                        btnDelFile2.SetClientVisible(false);

                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(false);
                        btnView3.SetClientVisible(false);
                        btnDelFile3.SetClientVisible(false);

                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(false);
                        btnView4.SetClientVisible(false);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "010":
                        lblName0.SetValue("1) แผนงานขนส่ง, ใบแนะนำการเติม");
                        uploader0.SetClientVisible(false);
                        txtFileName0.SetClientVisible(true);
                        txtFileName0.SetEnabled(false);
                        btnView0.SetEnabled(true);
                        btnDelFile0.SetClientVisible(false);

                        lblName1.SetValue("2) ใบกำกับการขนส่ง");
                        uploader1.SetClientVisible(false);
                        txtFileName1.SetClientVisible(true);
                        txtFileName1.SetEnabled(false);
                        btnView1.SetEnabled(true);
                        btnDelFile1.SetClientVisible(false);

                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(false);
                        btnView2.SetClientVisible(false);
                        btnDelFile2.SetClientVisible(false);

                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(false);
                        btnView3.SetClientVisible(false);
                        btnDelFile3.SetClientVisible(false);

                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(false);
                        btnView4.SetClientVisible(false);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "020":
                        lblName0.SetValue("1) ข้อมูล GPS");
                        uploader0.SetClientVisible(false);
                        txtFileName0.SetClientVisible(true);
                        txtFileName0.SetEnabled(false);
                        btnView0.SetEnabled(true);
                        btnDelFile0.SetClientVisible(false);

                        uploader1.SetClientVisible(false);
                        txtFileName1.SetClientVisible(false);
                        btnView1.SetClientVisible(false);
                        btnDelFile1.SetClientVisible(false);

                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(false);
                        btnView2.SetClientVisible(false);
                        btnDelFile2.SetClientVisible(false);

                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(false);
                        btnView3.SetClientVisible(false);
                        btnDelFile3.SetClientVisible(false);

                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(false);
                        btnView4.SetClientVisible(false);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "080":
                        lblName0.SetValue("1) เอกสารขี้แจง");
                        uploader0.SetClientVisible(false);
                        txtFileName0.SetClientVisible(true);
                        txtFileName0.SetEnabled(false);
                        btnView0.SetEnabled(true);
                        btnDelFile0.SetClientVisible(false);

                        lblName1.SetValue("2) ข้อมูลระบบ IVMS");
                        uploader1.SetClientVisible(false);
                        txtFileName1.SetClientVisible(true);
                        txtFileName1.SetEnabled(false);
                        btnView1.SetEnabled(true);
                        btnDelFile1.SetClientVisible(false);

                        lblName2.SetValue("3) ภาพถ่าย");
                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(true);
                        txtFileName2.SetEnabled(false);
                        btnView2.SetEnabled(true);
                        btnDelFile2.SetClientVisible(false);

                        lblName3.SetValue("4) เอกสารประกอบอื่น ๆ");
                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(true);
                        txtFileName3.SetEnabled(false);
                        btnView3.SetEnabled(true);
                        btnDelFile3.SetClientVisible(false);

                        lblName4.SetValue("5) เอกสารประกอบอื่น ๆ");
                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(true);
                        txtFileName4.SetEnabled(false);
                        btnView4.SetEnabled(true);
                        btnDelFile4.SetClientVisible(false);
                        break;
                    case "100":
                        lblName0.SetValue("1) เอกสารการอุทธรณ์");
                        uploader0.SetClientVisible(false);
                        txtFileName0.SetClientVisible(true);
                        txtFileName0.SetEnabled(false);
                        btnView0.SetEnabled(true);
                        btnDelFile0.SetClientVisible(false);

                        lblName1.SetValue("2) เอกสารการอุทธรณ์");
                        uploader1.SetClientVisible(false);
                        txtFileName1.SetClientVisible(true);
                        txtFileName1.SetEnabled(false);
                        btnView1.SetEnabled(true);
                        btnDelFile1.SetClientVisible(false);

                        lblName2.SetValue("3) เอกสารการอุทธรณ์");
                        uploader2.SetClientVisible(false);
                        txtFileName2.SetClientVisible(true);
                        txtFileName2.SetEnabled(false);
                        btnView2.SetEnabled(true);
                        btnDelFile2.SetClientVisible(false);

                        lblName3.SetValue("4) เอกสารการอุทธรณ์");
                        uploader3.SetClientVisible(false);
                        txtFileName3.SetClientVisible(true);
                        txtFileName3.SetEnabled(false);
                        btnView3.SetEnabled(true);
                        btnDelFile3.SetClientVisible(false);

                        lblName4.SetValue("5) เอกสารการอุทธรณ์");
                        uploader4.SetClientVisible(false);
                        txtFileName4.SetClientVisible(true);
                        txtFileName4.SetEnabled(false);
                        btnView4.SetEnabled(true);
                        btnDelFile4.SetClientVisible(false);
                        break;

                }


            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <script type="text/javascript">
        function closeModalAttach() {
            $('#ShowAttach').modal('hide');
        }
    </script>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; inEndRequestHandler();}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxTextBox ID="hideAppealID" runat="server" Width="10px" ClientInstanceName="hideAppealID"
                                ClientVisible="false"></dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtType" ClientInstanceName="txtType" runat="server" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtDocumentOther" ClientInstanceName="txtDocumentOther" runat="server"
                                ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="เลขที่สัญญา,ทะเบียนรถ">
                            </dx:ASPxTextBox>
                        </td>
                        <td style="width:120px; text-align:right">
                            <asp:Label ID="lblDate" runat="server" Text="วันที่พิจารณา :"></asp:Label>
                        </td>
                        <td>
                            <%--<dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>--%>
                            <asp:TextBox ID="dteStart" runat="server" CssClass="datepicker" Style="text-align: center;"></asp:TextBox>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <%--<dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>--%>
                            <asp:TextBox ID="dteEnd" runat="server" CssClass="datepicker" Style="text-align: center;"></asp:TextBox>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus" Width="110px"
                                SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="- สถานะ -" />
                                    <dx:ListEditItem Text="กำลังพิจารณา" Value="1" />
                                    <dx:ListEditItem Text="ตัดสิน" Value="2" />
                                    <dx:ListEditItem Text="รอยื่นอุทธรณ์" Value="3" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboType" runat="server" ClientInstanceName="cboStatus" Width="140px"
                                SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="ตรวจสภาพรถ" Value="1" />
                                    <dx:ListEditItem Text="อุบัติเหตุ" Value="2" />
                                    <dx:ListEditItem Text="เรื่องร้องเรียน" Value="3" />
                                    <dx:ListEditItem Text="ยืนยันรถตามสัญญา" Value="4" />
                                    <dx:ListEditItem Text="ยืนยันรถตามแผน" Value="5" />
                                    <dx:ListEditItem Text="รายงานประจำเดือน" Value="6" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" 
                                OnClick="btnSearch_Click">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8">
                            *H = Hold
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                 KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                        Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SCHECKID" ShowInCustomizationForm="True"
                                        Visible="False" VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่เกิดเหตุ" FieldName="DCHECK" VisibleIndex="1"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                        VisibleIndex="2" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Outbound No." FieldName="OUTBOUNDNO" ShowInCustomizationForm="True"
                                        VisibleIndex="2" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ประเภท" FieldName="STYPE" ShowInCustomizationForm="True"
                                        VisibleIndex="3" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หัวข้อปัญหา" FieldName="STOPICNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="5" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="8">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="9">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="คะแนนที่ตัด" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="SUMPOINT" ShowInCustomizationForm="True"
                                                VisibleIndex="8">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="หลัง" FieldName="TOTAL_POINT" ShowInCustomizationForm="True"
                                                VisibleIndex="9">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ค่าปรับ" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="COST" ShowInCustomizationForm="True"
                                                VisibleIndex="8">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="หลัง" FieldName="TOTAL_COST" ShowInCustomizationForm="True"
                                                VisibleIndex="9">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ระงับ พขร." ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="DISABLE_DRIVER" ShowInCustomizationForm="True"
                                                VisibleIndex="8">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="หลัง" FieldName="TOTAL_DISABLE_DRIVER" ShowInCustomizationForm="True"
                                                VisibleIndex="9">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="Blacklist" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="BLACKLIST" ShowInCustomizationForm="True"
                                                VisibleIndex="8">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="หลัง" FieldName="BLACKLIST_A" ShowInCustomizationForm="True"
                                                VisibleIndex="9">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <%--<dx:GridViewDataTextColumn Caption="คะแนน<br>ที่ตัด" FieldName="SUMPOINT" ShowInCustomizationForm="True"
                                        VisibleIndex="10" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ค่าปรับ" FieldName="COST" ShowInCustomizationForm="True"
                                        VisibleIndex="10" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ระงับ พขร.<br>(วัน)" FieldName="DISABLE_DRIVER" ShowInCustomizationForm="True"
                                        VisibleIndex="10" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนน<br>หลังยื่นอุทธรณ์" FieldName="FINALPOINT"
                                        ShowInCustomizationForm="True" VisibleIndex="10" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>--%>
                                    <dx:GridViewDataTextColumn Caption="สถานะ<br>การอุทธรณ์" FieldName="STATUS" ShowInCustomizationForm="True"
                                        VisibleIndex="10" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption=" " Width="8%" CellStyle-Cursor="hand" VisibleIndex="13">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="false" Text="ยื่นอุทธรณ์"
                                                CausesValidation="False" Width="100px">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="SPROCESS" FieldName="SPROCESS" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="STOPICID" FieldName="STOPICID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ISSUETEXT" FieldName="ISSUETEXT" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="SDRIVERNO" FieldName="SDRIVERNO" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="AREAACCIDENT" FieldName="AREAACCIDENT" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="SCONTRACTID" FieldName="SCONTRACTID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="NREDUCEID" FieldName="NREDUCEID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="CHKSTATUS" FieldName="CHKSTATUS" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="SAPPEALID" FieldName="SAPPEALID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="DCREATE" FieldName="DCREATE" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="SCUSTOMERID" FieldName="SCUSTOMERID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="SVERSION" FieldName="SVERSION" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SCHECKLISTID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SVERSIONLIST" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="STYPECHECKLISTID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ATTACH_NO" Visible="false">
                                    </dx:GridViewDataTextColumn> 
                                    <dx:GridViewDataTextColumn FieldName="DINCIDENT" Visible="false" />
                                    <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false" />
                                    <dx:GridViewDataTextColumn FieldName="NPOINT" Visible="false" />
                                    <dx:GridViewDataTextColumn FieldName="SPROCESSID" Visible="false" /> 
                                </Columns>
                                <Templates>
                                    <EditForm>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <dx:ASPxPageControl ID="PageControl" runat="server" ActiveTabIndex="0" Width="100%">
                                                        <TabPages>
                                                            <dx:TabPage Name="t1" Text="ยื่นอุทธรณ์">
                                                                <ContentCollection>
                                                                    <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                                                        <table cellpadding="2" cellspacing="1" style="border-collapse: collapse; width: 100%">
                                                                            <%--<tr>
                                                                                <td>

                                                                                </td>
                                                                                <td>
                                                                                    <asp:UpdatePanel ID="udpFile" runat="server" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <dx:ASPxButton ID="cmdAttach" runat="server" Text="เอกสารอุทธรณ์จากฝั่ง ปตท." Width="170px"
                                                                                                OnClick="cmdAttach_Click" Visible="false" />
                                                                                            <asp:Label ID="lblAttachTotal" runat="server" Text="มีไฟล์แนบทั้งหมด {0} เอกสาร" Visible="false"></asp:Label>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>--%>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    รายละเอียดการขออุทธรณ์
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <dx:ASPxMemo ID="txtAppealDetail" runat="server" Height="180px" Width="500px">
                                                                                    </dx:ASPxMemo>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="style28" colspan="3">
                                                                                    <b>เอกสารประกอบการขออุทธรณ์</b>Allowed file types:
                                                                                    <%= Resources.CommonResource.FileUploadType.Replace("."," ").Substring(0,30) %>ฯลฯ<br>
                                                                                    Max file size:
                                                                                    <%= Resources.CommonResource.TooltipMaxFileSize1MB %>
                                                                                    ได้)
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="25%">
                                                                                    <dx:ASPxLabel ID="lblName0" ClientInstanceName="lblName0" runat="server">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                                <td width="35%">
                                                                                    <dx:ASPxUploadControl ID="uplExcel0" runat="server" ClientInstanceName="uploader0"
                                                                                        NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                                            AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                        </ValidationSettings>
                                                                                        <ClientSideEvents TextChanged="function(s,e){uploader0.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png,.zip,.rar หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath0.SetValue((e.callbackData+'').split('|')[0]);txtFileName0.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                                        </ClientSideEvents>
                                                                                        <BrowseButton Text="แนบไฟล์">
                                                                                        </BrowseButton>
                                                                                    </dx:ASPxUploadControl>
                                                                                    <dx:ASPxTextBox ID="txtFileName0" runat="server" Width="300px" ClientInstanceName="txtFileName0"
                                                                                        ClientEnabled="false" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                    <dx:ASPxTextBox ID="txtFilePath0" runat="server" Width="220px" ClientInstanceName="txtFilePath0"
                                                                                        ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td width="40%">
                                                                                    <dx:ASPxButton ID="btnView0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnView0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath0.GetValue());}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                    <dx:ASPxButton ID="btnDelFile0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnDelFile0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath0.GetValue() +';1');}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <dx:ASPxLabel ID="lblName1" ClientInstanceName="lblName1" runat="server">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxUploadControl ID="uplExcel1" runat="server" ClientInstanceName="uploader1"
                                                                                        NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                                            AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                        </ValidationSettings>
                                                                                        <ClientSideEvents TextChanged="function(s,e){uploader1.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath1.SetValue((e.callbackData+'').split('|')[0]);txtFileName1.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                                        </ClientSideEvents>
                                                                                        <BrowseButton Text="แนบไฟล์">
                                                                                        </BrowseButton>
                                                                                    </dx:ASPxUploadControl>
                                                                                    <dx:ASPxTextBox ID="txtFileName1" runat="server" Width="300px" ClientInstanceName="txtFileName1"
                                                                                        ClientEnabled="false" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                    <dx:ASPxTextBox ID="txtFilePath1" runat="server" Width="220px" ClientInstanceName="txtFilePath1"
                                                                                        ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxButton ID="btnView1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnView1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath1.GetValue());}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                    <dx:ASPxButton ID="btnDelFile1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnDelFile1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath1.GetValue() +';2');}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <dx:ASPxLabel ID="lblName2" ClientInstanceName="lblName2" runat="server">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxUploadControl ID="uplExcel2" runat="server" ClientInstanceName="uploader2"
                                                                                        NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                                            AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                        </ValidationSettings>
                                                                                        <ClientSideEvents TextChanged="function(s,e){uploader2.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath2.SetValue((e.callbackData+'').split('|')[0]);txtFileName2.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                                        </ClientSideEvents>
                                                                                        <BrowseButton Text="แนบไฟล์">
                                                                                        </BrowseButton>
                                                                                    </dx:ASPxUploadControl>
                                                                                    <dx:ASPxTextBox ID="txtFileName2" runat="server" Width="300px" ClientInstanceName="txtFileName2"
                                                                                        ClientEnabled="false" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                    <dx:ASPxTextBox ID="txtFilePath2" runat="server" Width="220px" ClientInstanceName="txtFilePath2"
                                                                                        ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxButton ID="btnView2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnView2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath2.GetValue());}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                    <dx:ASPxButton ID="btnDelFile2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnDelFile2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath2.GetValue() +';3');}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <dx:ASPxLabel ID="lblName3" ClientInstanceName="lblName3" runat="server">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxUploadControl ID="uplExcel3" runat="server" ClientInstanceName="uploader3"
                                                                                        NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                                            AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                        </ValidationSettings>
                                                                                        <ClientSideEvents TextChanged="function(s,e){uploader3.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath3.SetValue((e.callbackData+'').split('|')[0]);txtFileName3.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                                        </ClientSideEvents>
                                                                                        <BrowseButton Text="แนบไฟล์">
                                                                                        </BrowseButton>
                                                                                    </dx:ASPxUploadControl>
                                                                                    <dx:ASPxTextBox ID="txtFileName3" runat="server" Width="300px" ClientInstanceName="txtFileName3"
                                                                                        ClientEnabled="false" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                    <dx:ASPxTextBox ID="txtFilePath3" runat="server" Width="220px" ClientInstanceName="txtFilePath3"
                                                                                        ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxButton ID="btnView3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnView3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath3.GetValue());}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                    <dx:ASPxButton ID="btnDelFile3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnDelFile3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath3.GetValue() +';4');}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <dx:ASPxLabel ID="lblName4" ClientInstanceName="lblName4" runat="server">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxUploadControl ID="uplExcel4" runat="server" ClientInstanceName="uploader4"
                                                                                        NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                                            AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                        </ValidationSettings>
                                                                                        <ClientSideEvents TextChanged="function(s,e){uploader4.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath4.SetValue((e.callbackData+'').split('|')[0]);txtFileName4.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                                        </ClientSideEvents>
                                                                                        <BrowseButton Text="แนบไฟล์">
                                                                                        </BrowseButton>
                                                                                    </dx:ASPxUploadControl>
                                                                                    <dx:ASPxTextBox ID="txtFileName4" runat="server" Width="300px" ClientInstanceName="txtFileName4"
                                                                                        ClientEnabled="false" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                    <dx:ASPxTextBox ID="txtFilePath4" runat="server" Width="220px" ClientInstanceName="txtFilePath4"
                                                                                        ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxButton ID="btnView4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnView4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath4.GetValue());}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                    <dx:ASPxButton ID="btnDelFile4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                        ClientInstanceName="btnDelFile4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath4.GetValue() +';5');}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                                        </Image>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <asp:Label ID="lblAppeal" runat="server" Text="เอกสารพิจารณาอุทธรณ์" ForeColor="Blue" Font-Underline="true" Visible="false"></asp:Label>
                                                                                    <div id="divAttach" runat="server">
                                                                                        <dx:ASPxGridView ID="dgvUpload" runat="server" AutoGenerateColumns="False" Settings-ShowColumnHeaders="true"
                                                                                        Style="margin-top: 0px" ClientInstanceName="sgvw" Width="100%" KeyFieldName="dtID"
                                                                                        OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SkinID="_gvw"  Visible="false">
                                                                                        <Columns>
                                                                                            <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                                                                                VisibleIndex="0">
                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                <CellStyle HorizontalAlign="Center">
                                                                                                </CellStyle>
                                                                                            </dx:GridViewDataTextColumn>
                                                                                            <dx:GridViewDataColumn Caption="UPLOAD_ID" FieldName="UPLOAD_ID" Visible="false"></dx:GridViewDataColumn>
                                                                                            <dx:GridViewDataColumn Caption="ประเภทไฟล์เอกสาร" HeaderStyle-HorizontalAlign="Center" FieldName="UPLOAD_NAME"></dx:GridViewDataColumn>
                                                                                            <dx:GridViewDataColumn Caption="ชื่อไฟล์ (ในระบบ)" HeaderStyle-HorizontalAlign="Center" FieldName="FILENAME_SYSTEM"></dx:GridViewDataColumn>
                                                                                            <dx:GridViewDataColumn Caption="ชื่อไฟล์ (ตามผู้ใช้งาน)" HeaderStyle-HorizontalAlign="Center" FieldName="FILENAME_USER"></dx:GridViewDataColumn>
                                                                                            <dx:GridViewDataColumn Caption="FULLPATH" FieldName="FULLPATH" Visible="false"></dx:GridViewDataColumn>
                                                                                            <dx:GridViewDataColumn Width="5%" CellStyle-Cursor="hand">
                                                                                                <DataItemTemplate>
                                                                                                    <dx:ASPxButton ID="imbDownload" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                                                                                        <ClientSideEvents Click="function(s,e){window.open('DownloadFile.aspx?row=' + s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                                        <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('download;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }"></ClientSideEvents>--%>
                                                                                                        
                                                                                                        <Image Width="24px" Height="24px" Url="Images/view1.png">
                                                                                                        </Image>
                                                                                                    </dx:ASPxButton>
                                                                                                </DataItemTemplate>
                                                                                                <CellStyle HorizontalAlign="Center">
                                                                                                </CellStyle>
                                                                                            </dx:GridViewDataColumn>
                                                                                            </Columns>
                                                                                        </dx:ASPxGridView>                                                                                 </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <dx:ASPxButton ID="btnConfirm" runat="server" AutoPostBack="false" Text="ยื่นอุทธรณ์"
                                                                                        CssClass="dxeLineBreakFix" Width="80px">
                                                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }">
                                                                                        </ClientSideEvents>
                                                                                    </dx:ASPxButton>
                                                                                    &nbsp
                                                                                    <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix"
                                                                                        Width="80px">
                                                                                        <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dx:ContentControl>
                                                                </ContentCollection>
                                                            </dx:TabPage>
                                                            <dx:TabPage Text="ผลการพิจารณาการยื่นอุทธรณ์" Visible="false">
                                                                <ContentCollection>
                                                                    <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                                                        <table width="100%" cellspacing="3" cellpadding="2" border="0">
                                                                            <tr>
                                                                                <td width="30%" valign="top">
                                                                                    ผลการพิจารณา
                                                                                </td>
                                                                                <td width="70%">
                                                                                    <dx:ASPxRadioButtonList ID="rblStatus" runat="server" ClientEnabled="false" Border-BorderStyle="None">
                                                                                        <Items>
                                                                                            <dx:ListEditItem Text="ตัดสินให้มีความผิด" Value="2" />
                                                                                            <dx:ListEditItem Text="ไม่มีความผิด" Value="1" />
                                                                                        </Items>
                                                                                        <Border BorderStyle="None" />
                                                                                    </dx:ASPxRadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    ข้อมูลประกอบการตัดสิน (ถ้ามี)
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxMemo ID="txtData" runat="server" Width="300px" Height="100px" ClientEnabled="false">
                                                                                    </dx:ASPxMemo>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_close" CssClass="dxeLineBreakFix"
                                                                                        Width="80px">
                                                                                        <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dx:ContentControl>
                                                                </ContentCollection>
                                                            </dx:TabPage>
                                                            <dx:TabPage Name="t2" Text="ปตท.ขอเอกสารเพิ่มเติม" ClientEnabled="false">
                                                                <ContentCollection>
                                                                    <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                                        <table cellpadding="2" cellspacing="1" style="border-collapse: collapse; width: 100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <dx:ASPxLabel ID="lblWait" runat="server" ClientInstanceName="lblWait" Text="ปตท.ขอเอกสารเพิ่มเติม">
                                                                                    </dx:ASPxLabel>
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <dx:ASPxMemo ID="txtWaitDocument" runat="server" ClientInstanceName="txtWaitDocument"
                                                                                        Width="300px" Height="100px" ClientEnabled="false" >
                                                                                    </dx:ASPxMemo>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    เอกสารเพิ่มเติม
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <span style="text-align: left">ชื่อหลักฐาน</span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <dx:ASPxTextBox ID="txtEvidence" runat="server" Width="170px">
                                                                                                </dx:ASPxTextBox>
                                                                                            </td>
                                                                                            <td>
                                                                                                <span style="text-align: left">ไฟล์แนบ </span>
                                                                                            </td>
                                                                                            <td>
                                                                                                <dx:ASPxUploadControl ID="MultipleUploader" runat="server" ClientInstanceName="MultipleUploader"
                                                                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="MultipleUploadControl_FileUploadComplete">
                                                                                                    <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData!=''){xcpn.PerformCallback('Upload');}else{dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpeg,.jpg หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');} }">
                                                                                                    </ClientSideEvents>
                                                                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                                    </ValidationSettings>
                                                                                                    <BrowseButton Text="แนบไฟล์">
                                                                                                    </BrowseButton>
                                                                                                </dx:ASPxUploadControl>
                                                                                            </td>
                                                                                            <td>
                                                                                                <dx:ASPxButton ID="btnAdd" ClientInstanceName="btnAdd" runat="server" SkinID="_add">
                                                                                                    <ClientSideEvents Click="function(s,e){MultipleUploader.Upload();}"></ClientSideEvents>
                                                                                                </dx:ASPxButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" Settings-ShowColumnHeaders="false"
                                                                                        Style="margin-top: 0px" ClientInstanceName="sgvw" Width="100%" KeyFieldName="dtID"
                                                                                        OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" OnHtmlDataCellPrepared="sgvw_HtmlDataCellPrepared" SkinID="_gvw">
                                                                                        <Columns>
                                                                                            <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                                                                                VisibleIndex="0">
                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                <CellStyle HorizontalAlign="Center">
                                                                                                </CellStyle>
                                                                                            </dx:GridViewDataTextColumn>
                                                                                            <dx:GridViewDataColumn Caption="รหัส" FieldName="dtID" Visible="false">
                                                                                            </dx:GridViewDataColumn>
                                                                                            <dx:GridViewDataColumn Caption="FilePath" FieldName="dtFilePath" Visible="false">
                                                                                            </dx:GridViewDataColumn>
                                                                                            <dx:GridViewDataTextColumn VisibleIndex="4" Width="10%" FieldName="dd" CellStyle-ForeColor="#0066FF">
                                                                                                <DataItemTemplate>
                                                                                                    <dx:ASPxLabel ID="lblScore" runat="server" Text="ชื่อหลักฐาน">
                                                                                                    </dx:ASPxLabel>
                                                                                                </DataItemTemplate>
                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                <CellStyle ForeColor="#0066FF" HorizontalAlign="Center">
                                                                                                </CellStyle>
                                                                                            </dx:GridViewDataTextColumn>
                                                                                            <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" VisibleIndex="5" Width="85%">
                                                                                                <DataItemTemplate>
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <dx:ASPxLabel ID="lblEvidence" runat="server" Text='<%# Eval("dtEvidenceName") %>'
                                                                                                                    Width="250">
                                                                                                                </dx:ASPxLabel>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <dx:ASPxButton ID="imbView0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                                                                                    Text="View" Width="15px">
                                                                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('ViewList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                                                </dx:ASPxButton>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                                                                                    Text="Delete" Width="15px">
                                                                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('DeleteList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                                                </dx:ASPxButton>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </DataItemTemplate>
                                                                                            </dx:GridViewDataTextColumn>
                                                                                        </Columns>
                                                                                        <Settings GridLines="None" ShowColumnHeaders="False"></Settings>
                                                                                        <Border BorderStyle="None" />
                                                                                    </dx:ASPxGridView>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <dx:ASPxButton ID="btnConfirm1" runat="server" AutoPostBack="false" Text="บันทึก"
                                                                                        CssClass="dxeLineBreakFix" Width="80px">
                                                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('SaveFileMore;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }">
                                                                                        </ClientSideEvents>
                                                                                    </dx:ASPxButton>
                                                                                    &nbsp
                                                                                    <dx:ASPxButton ID="btnCancel1" runat="server" SkinID="_close" CssClass="dxeLineBreakFix"
                                                                                        Width="80px">
                                                                                        <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dx:ContentControl>
                                                                </ContentCollection>
                                                            </dx:TabPage>
                                                        </TabPages>
                                                    </dx:ASPxPageControl>
                                                </td>
                                            </tr>
                                        </table>
                                    </EditForm>
                                </Templates>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser"></asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    <%--<div class="modal fade" id="ShowAttach" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"
        aria-hidden="true">
        <div class="modal-dialog" style="width:975px; height:700px">
            <asp:UpdatePanel ID="udpAttach" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label1" runat="server" Text="ดาวน์โหลดไฟล์เอกสาร"></asp:Label></h4>
                        </div>
                        <div class="modal-body" style="width:950px; height:700px; overflow:scroll">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <i class="fa fa-table"></i>
                                            <asp:Label ID="Label2" runat="server" Text="แนบไฟล์เอกสาร"></asp:Label></div>
                                        <div class="panel-body">
                                            <div class="dataTable_wrapper">
                                                <div class="panel-body">
                                                                                            <asp:Table ID="Table3" runat="server" Width="100%">
                                                                                                <asp:TableRow>
                                                                                                    <asp:TableCell ColumnSpan="5">
                                                                                                        <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                                                            CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                                                                                            EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                                                                                            EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                                                                                            OnRowUpdating="dgvUploadFile_RowUpdating" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                                                                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField HeaderText="No.">
                                                                                                                    <ItemTemplate>
                                                                                                                        <%# Container.DataItemIndex + 1 %>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                                                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                                                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                                                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                                                                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                                                                                <asp:TemplateField HeaderText="Action">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                                        </asp:GridView>
                                                                                                    </asp:TableCell>
                                                                                                </asp:TableRow>
                                                                                                <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                                                                <asp:TableRow><asp:TableCell style="text-align:center" ColumnSpan="5"><asp:Button ID="cmdClose" runat="server" Text="ปิด" Width="80px" CssClass="btn btn-success" OnClick="cmdClose_Click" />
                                                                                                    

                                                                                                              </asp:TableCell></asp:TableRow> 
                                                                                                </asp:Table>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                    </div>
                        </div>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>--%>
</asp:Content>
