﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="download_invoice_request_mv.aspx.cs" Inherits="download_invoice_request_mv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
    <dx:ASPxCallbackPanel ID = "xcpn" runat="server" ClientInstanceName="xcpn" HideContentOnCallback="false">
         <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
          <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="bottom" width = "50%">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="150px">
                                        เอกสารชำระเงินค่าบริการ 
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID = "btnDowload" runat="server" Text="Download" ClientInstanceName="btnDowload" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click = "function(e,s){rvw.SaveToDisk('PDF');}" />
                                        </dx:ASPxButton>

                                        <dx:ASPxButton ID = "btnBack" runat="server" SkinID="_back" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function(s,e) { window.location = 'approve_mv.aspx'; }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            
                            
                        </td>
                        <td align="left" width="50%">
                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <dx:ReportViewer ID = "rvw" runat="server" ClientInstanceName="rvw"></dx:ReportViewer>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            หมายเหตุ <br>
                            * เป็นค่าบริการประมาณการเท่านั้น <br>
                            * ไม่ใช่ราคารับบริการโดย Outsource
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
          </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

