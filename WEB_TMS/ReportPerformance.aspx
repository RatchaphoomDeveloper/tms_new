﻿<%@ Page Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ReportPerformance.aspx.cs"
    Inherits="ReportPerformance" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" OnCallback="xcpn_Callback">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent" runat="server">
                <table width="100%" border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            &nbsp; </td>
                        <td width="111px">
                            <dx:ASPxComboBox ID="cmbsMonth" runat="Server" ClientInstanceName="cmbsMonth" Width="110px" SelectedIndex="0">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="SEARCH">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุเดือน" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                        <td width="96px">
                            <dx:ASPxComboBox ID="cmbsYear" runat="Server" ClientInstanceName="cmbsYear" Width="95px" SelectedIndex="0"
                                ValueField="cYear" TextField="sYear">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="SEARCH">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุปี" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                        <td width="82px">
                            <dx:ASPxButton ID="btnSearch" runat="Server" ClientInstanceName="btnSearch" SkinID="_search" CausesValidation="true"
                                ValidationGroup="SEARCH">
                                <ClientSideEvents Click="function(s,e){  if(!ASPxClientEdit.ValidateGroup('SEARCH') || xcpn.InCallback()) return false; else xcpn.PerformCallback('SEARCH;'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ReportToolbar ID="ReportToolbar1" runat="server" ReportViewerID="ReportViewer1" ShowDefaultButtons="False"
                                Width="80%">
                                <Items>
                                    <dx:ReportToolbarButton ItemKind="Search" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton ItemKind="PrintReport" />
                                    <dx:ReportToolbarButton ItemKind="PrintPage" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                    <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                    <dx:ReportToolbarLabel ItemKind="PageLabel" />
                                    <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                    </dx:ReportToolbarComboBox>
                                    <dx:ReportToolbarLabel ItemKind="OfLabel" />
                                    <dx:ReportToolbarTextBox ItemKind="PageCount" />
                                    <dx:ReportToolbarButton ItemKind="NextPage" />
                                    <dx:ReportToolbarButton ItemKind="LastPage" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                                    <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                                    <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                        <Elements>
                                            <dx:ListElement Value="pdf" />
                                            <dx:ListElement Value="xls" />
                                            <dx:ListElement Value="xlsx" />
                                            <dx:ListElement Value="rtf" />
                                            <dx:ListElement Value="mht" />
                                            <dx:ListElement Value="html" />
                                            <dx:ListElement Value="txt" />
                                            <dx:ListElement Value="csv" />
                                            <dx:ListElement Value="png" />
                                        </Elements>
                                    </dx:ReportToolbarComboBox>
                                </Items>
                                <Styles>
                                    <LabelStyle>
                                        <Margins MarginLeft="3px" MarginRight="3px" />
                                    </LabelStyle>
                                </Styles>
                            </dx:ReportToolbar>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ReportViewer ID="ReportViewer1" runat="server">
                            </dx:ReportViewer>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
