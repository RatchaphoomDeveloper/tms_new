﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;

public partial class ExpireEmployee : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {

            string sqlquery = @"
SELECT CASE WHEN e.STRANS_ID = 'PHTDPRT' THEN 'PHTDPRT' ELSE v.SABBREVIATION END AS SVENDORNAME,ES.FNAME || ' ' || ES.LNAME  AS SEMPLOYEENAME
,
 CASE WHEN E.DBIRTHDATE IS NOT NULL THEN trunc(months_between(sysdate,E.DBIRTHDATE)/12) || ' ปี ' || trunc(mod(months_between(sysdate,E.DBIRTHDATE),12)) || ' เดือน ' ELSE '' END  AS SYEARMONTH
FROM TEMPLOYEE e 
LEFT JOIN TEMPLOYEE_SAP es ON e.SEMPLOYEEID = ES.SEMPLOYEEID
LEFT JOIN TVENDOR v ON E.STRANS_ID = V.SVENDORID
WHERE NVL(E.CACTIVE,'1') != '0'
AND NVL(V.CACTIVE,'Y') != 'N' 
AND E.DBIRTHDATE IS NOT NULL 
";

            int temp;
            if (dteStart.Value != null && dteEnd.Value != null && cbxYear.Value.ToString() != "-1")
            {
                int yEnd = (int.TryParse(cbxYearEnd.Value.ToString(), out temp) ? temp : 0) * (-1);
                switch (cbxYear.Value.ToString())
                {
                    case "<":
                        sqlquery += " AND e.DBIRTHDATE > TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                        break;
                    case ">":
                        sqlquery += " AND e.DBIRTHDATE < TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                        break;

                    case "=":
                    default:
                        sqlquery += " AND e.DBIRTHDATE BETWEEN TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND  TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";

                        break;
                }
            }

            if (cbxYear.Value.ToString() != "-1")
            {
                int YearEnd = int.TryParse(cbxYearEnd.Value.ToString(), out temp) ? temp : 0;
                switch (cbxYear.Value.ToString())
                {
                    case "=":
                        sqlquery += " AND trunc(months_between(sysdate,e.DBIRTHDATE)/12) = " + YearEnd;

                        break;
                    case "<":
                        sqlquery += " AND trunc(months_between(sysdate,e.DBIRTHDATE)/12) < " + YearEnd;

                        break;
                    case ">":
                        sqlquery += " AND trunc(months_between(sysdate,e.DBIRTHDATE)/12) > " + YearEnd;

                        break;
                    default:
                        int YearStart = int.TryParse(cbxYearEnd.Value.ToString(), out temp) ? temp : 0;
                        sqlquery += " AND trunc(months_between(sysdate,e.DBIRTHDATE)/12) Between " + YearStart + " AND " + YearEnd;
                        break;

                }

            }

            //Response.Write(sqlquery);
            //DataTable dt = new DataTable();
            //dt = CommonFunction.Get_Data(sql,sqlquery);


            dsReport dsRep = new dsReport();



            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == System.Data.ConnectionState.Closed) con.Open();


                using (OracleDataAdapter adapter = new OracleDataAdapter(sqlquery, con))
                {
                    adapter.Fill(dsRep.dsExpireEmployee);
                }


            }



            xrtExpireEmployee report = new xrtExpireEmployee();
            report.DataSource = dsRep.dsExpireEmployee;
            if (dteStart.Value != null && dteEnd.Value != null)
            {
                report.Parameters["pStart"].Value = dteStart.Date.ToString("dd/MM/yyyy");
                report.Parameters["pEnd"].Value = dteEnd.Date.ToString("dd/MM/yyyy");
            }
            report.Name = "รายงานค้นหา พขร. ตามอายุ";
            this.ReportViewer1.Report = report;
        }
        else
        {
            ListYear();
            ListYear2();
        }
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

    protected void ListYear()
    {
        cbxYear.Items.Add(new ListEditItem(" - ทั้งหมด - ", (-1).ToString()));
        cbxYear.Items.Add(new ListEditItem(" น้อยกว่า ", "<"));
        cbxYear.Items.Add(new ListEditItem(" มากกว่า ", ">"));
        cbxYear.Items.Add(new ListEditItem(" เท่ากับ ", "="));

        //for (int i = 15; i <= 100; i++)
        //{
        //    cbxYear.Items.Add(new ListEditItem(i.ToString(), i.ToString()));
        //}

    }

    protected void ListYear2()
    {
        for (int i = 5; i <= 30;i++)
        {
            cbxYearEnd.Items.Add(new ListEditItem(i.ToString(), i.ToString()));
        }
    }
}