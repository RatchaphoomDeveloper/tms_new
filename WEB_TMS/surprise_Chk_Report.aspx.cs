﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.IO;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using System.Configuration;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using EmailHelper;
using TMS_BLL.Transaction.Appeal;
using TMS_BLL.Transaction.Accident;
using System.Web.Security;
using GemBox.Spreadsheet;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.DataVisualization.Charting;
using TMS_BLL.Transaction.SurpriseCheckYear;
//using TMS_BLL.Transaction.KM;

public partial class surprise_Chk_Report : PageBase
{
    #region + View State +

    private DataTable dtReport
    {
        get
        {
            if ((DataTable)ViewState["dtReport"] != null)
                return (DataTable)ViewState["dtReport"];
            else
                return null;
        }
        set
        {
            ViewState["dtReport"] = value;
        }
    }
    private DataTable dtReportYear
    {
        get
        {
            if ((DataTable)ViewState["dtReportYear"] != null)
                return (DataTable)ViewState["dtReportYear"];
            else
                return null;
        }
        set
        {
            ViewState["dtReportYear"] = value;
        }
    }
    private string Detail
    {
        get
        {
            if ((string)ViewState["Detail"] != null)
                return (string)ViewState["Detail"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["Detail"] = value;
        }
    }
    private int CountRow
    {
        get
        {
            if ((int)ViewState["CountRow"] != null)
                return (int)ViewState["CountRow"];
            else
                return 0;
        }
        set
        {
            ViewState["CountRow"] = value;
        }
    }

    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            this.SetDrowDownList();
            this.SetDrowDownListYear();
            ddlYearAll.SelectedValue = DateTime.Now.Year.ToString();
            ddlYear.SelectedValue = DateTime.Now.Year.ToString();
            AssignAuthen();
            this.LoadWorkgroup();
            this.LoadVendor();
            this.LoadWorkgroupYear();
            this.LoadVendorYear();
            this.LoadData();
            this.LoadDataYear();
            //this.Bindchart();
            //this.Bindchart2();
        }


    #endregion
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void SetDrowDownList()
    {
        for (int i = 2015; i < DateTime.Now.Year + 1; i++)
        {
            ddlYearAll.Items.Add(new ListItem()
            {
                Text = i + string.Empty,
                Value = i + string.Empty
            });
        }
        ddlYearAll.SelectedValue = DateTime.Now.Year + string.Empty;
    }
    private void SetDrowDownListYear()
    {
        for (int i = 2015; i < DateTime.Now.Year + 1; i++)
        {
            ddlYear.Items.Add(new ListItem()
            {
                Text = i + string.Empty,
                Value = i + string.Empty
            });
        }
        ddlYear.SelectedValue = DateTime.Now.Year + string.Empty;
    }

    private void LoadData()
    {
        try
        {
            dtReport = SurpriseCheckYearBLL.Instance.SurpriseChkReportBLL(ddlYearAll.SelectedValue, this.GetCondition());
            GridViewHelper.BindGridView(ref dgvSurpriseCheckReport, dtReport);
            this.Bindchart();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDataYear()
    {
        try
        {
            dtReportYear = SurpriseCheckYearBLL.Instance.SurpriseChkReportYearBLL(ddlYear.SelectedValue, this.GetConditionYear());
            dtReportYear.Columns.Add("PERCENTCHECK");
            if (dtReportYear.Rows.Count > 0)
            {
                for (int i = 0; i < dtReportYear.Rows.Count; i++)
                {
                    if (i % 2 == 1)
                    {
                        dtReportYear.Rows[i]["PERCENTCHECK"] = dtReportYear.Rows[i - 1]["COUNTCHECK_ALL"].ToString() == "0" ? 0 : Math.Round((decimal.Parse(dtReportYear.Rows[i - 1]["COUNTCHECK_ALL"].ToString()) / decimal.Parse(dtReportYear.Rows[i]["COUNTCHECK_ALL"].ToString())) * 100, 0, MidpointRounding.AwayFromZero);
                        dtReportYear.Rows[i - 1]["PERCENTCHECK"] = dtReportYear.Rows[i]["PERCENTCHECK"];
                        dtReportYear.Rows[i]["PERCENTALL"] = dtReportYear.Rows[i - 1]["PERCENTALL"];
                    }
                }
            }
            GridViewHelper.BindGridView(ref dgvSurpriseChkReportYear, dtReportYear);
            this.Bindchart2();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string GetCondition()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlgroup.SelectedIndex > 0)
                sb.Append(" AND GROUPID = '" + ddlgroup.SelectedValue + "'");
            if (ddlVendor.SelectedIndex > 0)
                sb.Append(" AND SVENDORID = '" + ddlVendor.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string GetConditionYear()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlgroupYear.SelectedIndex > 0)
                sb.Append(" AND GROUPID = '" + ddlgroupYear.SelectedValue + "'");
            if (ddlVendorYear.SelectedIndex > 0)
                sb.Append(" AND SVENDORID = '" + ddlVendorYear.SelectedValue + "'");
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void Bindchart()
    {
        DataSet ds = new DataSet();

        int COUNTCHECK_ALL = dtReport.AsEnumerable().Sum(it => int.Parse(it["COUNTCHECK_ALL"] + string.Empty));
        int COUNT_TRUCKALL = dtReport.AsEnumerable().Sum(it => int.Parse(it["COUNT_TRUCKALL"] + string.Empty));
        DataTable ChartData = new DataTable();
        ChartData.Columns.Add("x");
        ChartData.Columns.Add("y");
        DataRow dr = ChartData.NewRow();
        dr["x"] = "ตรวจแล้ว";
        dr["y"] = (COUNTCHECK_ALL == 0 ? 0 : COUNTCHECK_ALL / 2);
        ChartData.Rows.Add(dr);
        dr = ChartData.NewRow();
        dr["x"] = "ยังไมได้ตรวจ";
        dr["y"] = (COUNT_TRUCKALL == 0 ? 0 : COUNT_TRUCKALL / 2);
        ChartData.Rows.Add(dr);
        //storing total rows count to loop on each Record  
        string[] XPointMember = new string[ChartData.Rows.Count];
        int[] YPointMember = new int[ChartData.Rows.Count];

        for (int count = 0; count < ChartData.Rows.Count; count++)
        {
            //storing Values for X axis  
            XPointMember[count] = ChartData.Rows[count]["x"].ToString();
            //storing values for Y Axis  
            YPointMember[count] = Convert.ToInt32(ChartData.Rows[count]["y"]);

        }
        //binding chart control  
        Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);

        //Setting width of line  
        Chart1.Series[0].BorderWidth = 10;
        //setting Chart type   
        Chart1.Series[0].ChartType = SeriesChartType.Pie;


        foreach (Series charts in Chart1.Series)
        {
            foreach (DataPoint point in charts.Points)
            {
                switch (point.AxisLabel)
                {
                    case "ตรวจแล้ว": point.Color = Color.Blue; break;
                    case "ยังไมได้ตรวจ": point.Color = Color.Coral; break;
                }
                point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

            }
        }
    }
    private void Bindchart2()
    {
        DataSet ds = new DataSet();

        int COUNTCHECK_ALL = dtReportYear.AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => int.Parse(it["COUNTCHECK_ALL"] + string.Empty));
        int COUNT_TRUCKALL = dtReportYear.AsEnumerable().Sum(it => int.Parse(it["COUNT_TRUCKALL"] + string.Empty));
        DataTable ChartData = new DataTable();
        ChartData.Columns.Add("x");
        ChartData.Columns.Add("y");
        DataRow dr = ChartData.NewRow();
        dr["x"] = "ตรวจแล้ว";
        dr["y"] = COUNTCHECK_ALL;
        ChartData.Rows.Add(dr);
        dr = ChartData.NewRow();
        dr["x"] = "ยังไมได้ตรวจ";
        dr["y"] = (COUNT_TRUCKALL == 0 ? 0 : COUNT_TRUCKALL / 2);
        ChartData.Rows.Add(dr);
        //storing total rows count to loop on each Record  
        string[] XPointMember = new string[ChartData.Rows.Count];
        int[] YPointMember = new int[ChartData.Rows.Count];

        for (int count = 0; count < ChartData.Rows.Count; count++)
        {
            //storing Values for X axis  
            XPointMember[count] = ChartData.Rows[count]["x"].ToString();
            //storing values for Y Axis  
            YPointMember[count] = Convert.ToInt32(ChartData.Rows[count]["y"]);

        }
        //binding chart control  
        Chart3.Series[0].Points.DataBindXY(XPointMember, YPointMember);

        //Setting width of line  
        Chart3.Series[0].BorderWidth = 10;
        //setting Chart type   
        Chart3.Series[0].ChartType = SeriesChartType.Pie;


        foreach (Series charts in Chart3.Series)
        {
            foreach (DataPoint point in charts.Points)
            {
                switch (point.AxisLabel)
                {
                    case "ตรวจแล้ว": point.Color = Color.Blue; break;
                    case "ยังไมได้ตรวจ": point.Color = Color.Coral; break;
                }
                point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

            }
        }
    }
    protected void dgvSurpriseCheckReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvSurpriseCheckReport.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvSurpriseCheckReport, dtReport);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseCheckReport_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void dgvSurpriseChkReportYear_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvSurpriseChkReportYear.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvSurpriseChkReportYear, dtReportYear);
            this.Bindchart2();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseChkReportYear_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.LoadData();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlYearAll.SelectedValue = DateTime.Now.Year.ToString();
        ddlgroup.ClearSelection();
        ddlVendor.ClearSelection();
    }

    private void LoadVendor()
    {
        try
        {
            DataTable dt_vendor = SurpriseCheckYearBLL.Instance.SurpriseCheckVendorSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadWorkgroup()
    {
        try
        {
            DataTable dtWorkgroup = SurpriseCheckYearBLL.Instance.SurpriseCheckGroupSelectBLL(string.Empty);//KMBLL.Instance.SelectKMWorkgroupBLL(" AND CACTIVE = 1 ");
            DropDownListHelper.BindDropDownList(ref ddlgroup, dtWorkgroup, "GROUP_ID", "GROUP_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadVendorYear()
    {
        try
        {
            DataTable dt_vendor = SurpriseCheckYearBLL.Instance.SurpriseCheckVendorSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendorYear, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadWorkgroupYear()
    {
        try
        {
            DataTable dtWorkgroup = SurpriseCheckYearBLL.Instance.SurpriseCheckGroupSelectBLL(string.Empty); //KMBLL.Instance.SelectKMWorkgroupBLL(" AND CACTIVE = 1 ");
            DropDownListHelper.BindDropDownList(ref ddlgroupYear, dtWorkgroup, "GROUP_ID", "GROUP_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            this.Export();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void Export()
    {
        try
        {
            if (dtReport == null || dtReport.Rows.Count == 0)
            {
                throw new Exception("ไม่มีข้อมูลที่ต้องการ Export");
            }

            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/Chart2.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["Sheet1"];
            DataTable dtFinal = dtReport.Copy();

            int COUNTCHECK_ALL = dtFinal.AsEnumerable().Sum(it => int.Parse(it["COUNTCHECK_ALL"] + string.Empty));
            int COUNT_TRUCKALL = dtFinal.AsEnumerable().Sum(it => int.Parse(it["COUNT_TRUCKALL"] + string.Empty));
            COUNTCHECK_ALL = (COUNTCHECK_ALL == 0 ? 0 : COUNTCHECK_ALL / 2);
            COUNT_TRUCKALL = (COUNT_TRUCKALL == 0 ? 0 : COUNT_TRUCKALL / 2);
            dtFinal.Columns.Remove("ID");
            dtFinal.Columns.Remove("GROUPID");
            dtFinal.Columns.Remove("SVENDORID");

            this.SetFormatCell(worksheet.Cells[0, 0], "รายงานภาพรวมการตรวจรถ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 15);
            worksheet.Cells.GetSubrangeAbsolute(0, 0, 0, 18).Merged = true;

            this.SetFormatCell(worksheet.Cells[18, 0], "กลุ่มงานขนส่ง", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 1], "ผลิตภัณฑ์", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 2], "บริษัทขนส่ง", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 3], "เขตการขนส่ง	", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 4], "มกราคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 5], "กุมภาพันธ์", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 6], "มีนาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 7], "เมษายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 8], "พฤษภาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 9], "มิถุนายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 10], "กรกฏาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 11], "สิงหาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 12], "กันยายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 13], "ตุลาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 14], "พฤศจิกายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 15], "ธันวาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 16], "จำนวนรถที่ผ่านการตรวจทั้งหมด", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 17], "จำนวนรถทั้งหมด", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 18], "ความครบถ้วน(%)", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);

            this.SetFormatCell(worksheet.Cells[2, 0], "ตรวจแล้ว", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[2, 1], "ยังไม่ตรวจ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCellFormula(worksheet.Cells[3, 0], "=SUM(" + (ExcelColumnFromNumber(16) + (20)) + ":" + (ExcelColumnFromNumber(16) + (dtFinal.Rows.Count + 19)) + ")", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false);
            this.SetFormatCellFormula(worksheet.Cells[3, 1], "=SUM(" + (ExcelColumnFromNumber(17) + (20)) + ":" + (ExcelColumnFromNumber(17) + (dtFinal.Rows.Count + 19)) + ")", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false);

            for (int i = 19; i <= dtFinal.Rows.Count + 18; i++)
            {
                for (int j = 0; j < dtFinal.Columns.Count; j++)
                {//Export Detail
                    if (j == 16)
                        this.SetFormatCellFormula(worksheet.Cells[i, j], "=SUM(" + (ExcelColumnFromNumber(4) + (i + 1)) + ":" + (ExcelColumnFromNumber(15) + (i + 1)) + ")", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j == 17)
                        this.SetNumberFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 19][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j == 18)
                        this.SetFormatCellFormula(worksheet.Cells[i, j], "=(" + (ExcelColumnFromNumber(16) + (i + 1)) + "/" + (ExcelColumnFromNumber(17) + (i + 1)) + ")*100", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j > 3 && j < 16)
                        this.SetNumberFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 19][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else
                        this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 19][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false, 12);
                }
            }

            int columnCount = worksheet.CalculateMaxUsedColumns();

            for (int i = 0; i < columnCount; i++)
                worksheet.Columns[i].AutoFit(1.3);
            string Path = this.CheckPath();
            string FileName = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText, int FontSize)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
            cell.Style.Font.Size = FontSize * 20;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnSearchYear_Click(object sender, EventArgs e)
    {
        try
        {
            this.LoadDataYear();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnClearYear_Click(object sender, EventArgs e)
    {
        ddlYear.SelectedValue = DateTime.Now.Year.ToString();
        ddlgroupYear.ClearSelection();
        ddlVendorYear.ClearSelection();
    }
    protected void btnExportYear_Click(object sender, EventArgs e)
    {
        try
        {
            this.ExportYear();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void ExportYear()
    {
        try
        {
            if (dtReportYear == null || dtReportYear.Rows.Count == 0)
            {
                throw new Exception("ไม่มีข้อมูลที่ต้องการ Export");
            }
            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/Chart.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["Sheet1"];
            DataTable dtFinal = dtReportYear.Copy();

            int COUNTCHECK_ALL = dtFinal.AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => int.Parse(it["COUNTCHECK_ALL"] + string.Empty));
            int COUNT_TRUCKALL = dtFinal.AsEnumerable().Sum(it => int.Parse(it["COUNT_TRUCKALL"] + string.Empty));
            COUNT_TRUCKALL = (COUNT_TRUCKALL == 0 ? 0 : COUNT_TRUCKALL / 2);

            dtFinal.Columns.Remove("ID");
            dtFinal.Columns.Remove("STATUS_ID");
            dtFinal.Columns.Remove("GROUPID");

            this.SetFormatCell(worksheet.Cells[0, 0], "รายงานภาพรวมการตรวจรถประจำปี " + ddlYear.SelectedValue, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 15);
            worksheet.Cells.GetSubrangeAbsolute(0, 0, 0, 18).Merged = true;

            this.SetFormatCell(worksheet.Cells[18, 0], "กลุ่มงานขนส่ง", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 1], "ผลิตภัณฑ์", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 2], "เขตการขนส่ง", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 3], "แผน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 4], "มกราคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 5], "กุมภาพันธ์", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 6], "มีนาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 7], "เมษายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 8], "พฤษภาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 9], "มิถุนายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 10], "กรกฏาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 11], "สิงหาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 12], "กันยายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 13], "ตุลาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 14], "พฤศจิกายน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 15], "ธันวาคม", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 16], "จำนวนรถ(คัน)", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 17], "ความคืบหน้า(%)", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 18], "จำนวนรถทั้งหมด", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[18, 19], "ความครบถ้วน(%)", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);

            this.SetFormatCell(worksheet.Cells[2, 0], "ตรวจแล้ว", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[2, 1], "ยังไม่ตรวจ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetNumberFormatCell(worksheet.Cells[3, 0], COUNTCHECK_ALL.ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false);
            this.SetFormatCellFormula(worksheet.Cells[3, 1], "=SUM(" + (ExcelColumnFromNumber(18) + (20)) + ":" + (ExcelColumnFromNumber(18) + (dtFinal.Rows.Count + 19)) + ")", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false);

            for (int i = 19; i <= dtFinal.Rows.Count + 18; i++)
            {
                for (int j = 0; j < dtFinal.Columns.Count; j++)
                {//Export Detail
                    if (j == 16)
                        this.SetFormatCellFormula(worksheet.Cells[i, j], "=SUM(" + (ExcelColumnFromNumber(4) + (i + 1)) + ":" + (ExcelColumnFromNumber(15) + (i + 1)) + ")", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j == 18)
                        this.SetNumberFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 19][j - 1].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j == 17)
                        this.SetFormatCellFormula(worksheet.Cells[i, j], "=(" + (ExcelColumnFromNumber(16) + (i)) + "/" + (ExcelColumnFromNumber(16) + (i + 1)) + ")*100", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j == 19)
                        this.SetFormatCellFormula(worksheet.Cells[i, j], "=(" + (ExcelColumnFromNumber(16) + (i + 1)) + "/" + (ExcelColumnFromNumber(18) + (i)) + ")*100", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j > 3 && j < 16)
                        this.SetNumberFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 19][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                    else if (j != 17 && j != 19)
                        this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 19][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false, 12);

                    if (j == 0 || j == 1 || j == 2 || j == 17 || j == 18 || j == 19)
                    {
                        worksheet.Cells.GetSubrangeAbsolute(i, j, i + 1, j).Merged = true;
                    }
                }
            }

            int columnCount = worksheet.CalculateMaxUsedColumns();

            for (int i = 0; i < columnCount; i++)
                worksheet.Columns[i].AutoFit(1.3);

            string Path = this.CheckPath();
            string FileName = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public static string ExcelColumnFromNumber(int column)
    {
        string columnString = "";
        decimal columnNumber = column;
        while (columnNumber > 0)
        {
            decimal currentLetterNumber = (columnNumber) % 26;
            char currentLetter = (char)(currentLetterNumber + 65);
            columnString = currentLetter + columnString;
            columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
        }
        return columnString;
    }
    private void SetFormatCellFormula(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Formula = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            //cell.Style.WrapText = WrapText;
            cell.Style.Font.Size = 12 * 20;
            cell.Style.Font.Name = "Calibri";
            cell.Style.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \"\"??_-;_-@_-";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvSurpriseChkReportYear_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex % 2 == 0)
            {
                e.Row.Cells[1].RowSpan = 2;
                e.Row.Cells[2].RowSpan = 2;
                e.Row.Cells[3].RowSpan = 2;
                e.Row.Cells[18].RowSpan = 2;
                e.Row.Cells[19].RowSpan = 2;
                e.Row.Cells[20].RowSpan = 2;
            }
            else
            {
                e.Row.Cells.Remove(e.Row.Cells[1]);
                e.Row.Cells.Remove(e.Row.Cells[1]);
                e.Row.Cells.Remove(e.Row.Cells[1]);
                e.Row.Cells.Remove(e.Row.Cells[15]);
                e.Row.Cells.Remove(e.Row.Cells[15]);
                e.Row.Cells.Remove(e.Row.Cells[15]);
            }
        }
    }
    private void SetNumberFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = int.Parse(value);
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            //cell.Style.WrapText = WrapText;
            cell.Style.Font.Size = 12 * 20;
            cell.Style.Font.Name = "Calibri";
            cell.Style.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \"\"??_-;_-@_-";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void ddlgroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string condition = string.Empty;
            if (ddlgroup.SelectedIndex > 0)
                condition = " AND GROUPSID = '" + ddlgroup.SelectedValue + "'";
            DataTable dt_vendor = SurpriseCheckYearBLL.Instance.SurpriseCheckVendorSelectBLL(condition);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlgroupYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string condition = string.Empty;
            if (ddlgroupYear.SelectedIndex > 0)
                condition = " AND GROUPSID = '" + ddlgroupYear.SelectedValue + "'";
            DataTable dt_vendor = SurpriseCheckYearBLL.Instance.SurpriseCheckVendorSelectBLL(condition);
            DropDownListHelper.BindDropDownList(ref ddlVendorYear, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseCheckReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }
}
