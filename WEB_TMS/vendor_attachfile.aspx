﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_attachfile.aspx.cs" Inherits="vendor_attachfile" %>

<%@ Register TagPrefix="uc" TagName="ucCalendar" Src="~/ucCalendar.ascx" %>
<%@ Register Src="DocumentCharges.ascx" TagName="UserControl" TagPrefix="myUsr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript" language="javascript">
        function OpenPage(sUrl) {

            window.open(sUrl);
        }

        function openFile(str) {

            window.open("" + str + "");
        }

        function OnStartUpload(s, e) {


            var filename = s.GetText();

            if (filename != "") {
                var arrfilename = filename.split('.');
                var indx = s.name.substring(s.name.lastIndexOf('_') + 1, s.name.length);

                if (arrfilename[arrfilename.length - 1].toLowerCase() == 'pdf' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpeg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'bmp' || arrfilename[arrfilename.length - 1].toLowerCase() == 'gif' || arrfilename[arrfilename.length - 1].toLowerCase() == 'png' || arrfilename[arrfilename.length - 1].toLowerCase() == 'doc' || arrfilename[arrfilename.length - 1].toLowerCase() == 'docx' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xls' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xlsx') {
                    s.Upload(indx);
                }
                else {
                    s.ClearText();
                    dxWarning('แจ้งเตือน', 'เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น');
                }
            }
        }

        function UploadFile1(s, e) {

            var filename = s.GetText();

            if (filename != "") {
                var arrfilename = filename.split('.');

                var indx = s.name.substring(s.name.lastIndexOf('_') + 1, s.name.length);


                if (arrfilename[arrfilename.length - 1].toLowerCase() == 'pdf' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpeg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'bmp' || arrfilename[arrfilename.length - 1].toLowerCase() == 'gif' || arrfilename[arrfilename.length - 1].toLowerCase() == 'png' || arrfilename[arrfilename.length - 1].toLowerCase() == 'doc' || arrfilename[arrfilename.length - 1].toLowerCase() == 'docx' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xls' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xlsx') {
                    gvwBill.PerformCallback('UPLOAD;' + indx);
                }
                else {
                    s.ClearText();
                    dxWarning('แจ้งเตือน', 'เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น');
                }
            }
        }

        function PadLeft(Value) {
            var cdate = Value;
            var CV = cdate.toString();
            var _item = CV.split('/');
            var sv = "";
            var sss = _item[1].toString();
            if (sss.length > 1) {
                sv = _item[1] + '-' + _item[0] + '-' + (parseInt(_item[2]) - 543);

            }
            else {
                sv = '0' + sss + '-' + _item[0] + '-' + (parseInt(_item[2]) - 543);

            }

            return sv;
        }

        function DateDecreasSysdate() {

            var getd = PadLeft(txtCalendarDate.GetText()).toString();
            var gets = PadLeft(txtDatenow.GetText()).toString();

            var sDate = new Date(getd);
            var gDate = new Date(gets);

            if (gDate > sDate) {
                return false;
            }
            else {
                return true;
            }
        }
        
    </script>
    <style type="text/css">
        .cHideControl
        {
            display: none;
        }
        
        .cShowControl
        {
            display: block;
        }
        
        .cInline
        {
            display: inline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" OnCallback="xcpn_Callback"
        OnLoad="xcpn_Load" HideContentOnCallback="false" CausesValidation="False">
        <ClientSideEvents EndCallback="function(s,e){
          
          //set call back other
         xcpnGvwotherDoc.PerformCallback('BIND;');

         eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                <tr>
                                    <td colspan="4" align="right">
                                        <myUsr:UserControl ID="myUserControl" runat="server"></myUsr:UserControl>
                                        <dx:ASPxTextBox runat="server" ID="txtCheckDate" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                              <%--  <tr>
                                    <td colspan="4" align="right">
                                        <table width="100%" border="0" cellpadding="5" cellspacing="2">
                                            <tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%">
                                        <dx:ASPxButton ID="btnEditData" runat="server" ClientInstanceName="btnEditData" Text="แก้ไขใบเรียกเก็บ/คำนวณค่าธรรมเนียมใหม่"
                                            AutoPostBack="false" ClientVisible="false">
                                            <ClientSideEvents Click="function (s,e) { xcpn.PerformCallback('EDIT;'); }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxTextBox ID="txtUrlDownloadBill" runat="server" ClientInstanceName="txtUrlDownloadBill"
                                            ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td align="right" width="30%">
                                      <%--  <table width="100%" border="0" cellpadding="5" cellspacing="2">
                                        </table>--%>
                                    </td>
                                    <td class="cWidth1 StatusBG" align="false" width="20%">สถานะใบคำขอ</td>
                                    <td align="left" width="30%">
                                        <dx:ASPxLabel ID="lblStatusRQ" runat="server" CssClass="active">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel ID="lblStatusID" runat="server" CssClass="cHideControl">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" bgcolor="#B9EAEF">วันที่นัดหมาย </td>
                                    <td width="30%">
                                        <table>
                                            <tr>
                                                <td>
                                                    <dx:ASPxLabel runat="server" ID="lblCalendarDate" ClientInstanceName="lblCalendarDate">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <dx:ASPxButton ID="btnShowCalendar" runat="server" SkinID="_appointdate" AutoPostBack="false">
                                                    </dx:ASPxButton>
                                                    <dx:ASPxTextBox ID="txtCalendarDate" runat="server" ClientVisible="false" ClientInstanceName="txtCalendarDate">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="20%" bgcolor="#B9EAEF">วันที่หมดอายุวัดน้ำ </td>
                                    <td width="30%">
                                        <dx:ASPxLabel runat="server" ID="lblwaterexp">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="displayNone" id="trVisible">
                                    <td colspan="4">
                                        <uc:ucCalendar ID="ucCalendar1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#B9EAEF">ประเภทคำขอ</td>
                                    <td>
                                        <dx:ASPxLabel ID="lblRQType" runat="server" ClientInstanceName="lblRQType">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td bgcolor="#B9EAEF">สาเหตุ</td>
                                    <td>
                                        <dx:ASPxLabel ID="lblCause" runat="server">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#B9EAEF">ทะเบียนรถ</td>
                                    <td>
                                        <dx:ASPxLabel ID="lblRegistration" runat="server">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td bgcolor="#B9EAEF">ประเภทรถ</td>
                                    <td>
                                        <dx:ASPxLabel ID="lblCarType" runat="server">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#B9EAEF">บริษัทขนส่ง</td>
                                    <td>
                                        <dx:ASPxLabel ID="lblCompany" runat="server">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td bgcolor="#B9EAEF">ใบเรียกเก็บ</td>
                                    <td><a onclick="OpenPage(txtUrlDownloadBill.GetValue());"><img src="images/ic_pdf2.gif"
                                    width="16" height="16" border="0" style="cursor: pointer;" /></a>
                                        <dx:ASPxHyperLink ID="hylDownloadBill" runat="server" ClientInstanceName="hylDownloadBill"
                                            Text="ดาวน์โหลดใบเรียกเก็บ" Style="cursor: pointer;">
                                            <ClientSideEvents Click="function(s,e){OpenPage(txtUrlDownloadBill.GetValue());}" />
                                        </dx:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                            <tr>
                                                <td>
                                                    <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                                        Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="แป้น" HeaderStyle-HorizontalAlign="Center" Width="20%"
                                                                Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SNAME") %>' runat="server" ID="xlbldCarConFirm" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 1" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT1").ToString()==""?"-":Eval("SLOT1").ToString()%>'
                                                                        runat="server" ID="xlbSLOT1" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 2" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT2").ToString()==""?"-":Eval("SLOT2").ToString()%>'
                                                                        runat="server" ID="xlbSLOT2" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 3" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT3").ToString()==""?"-":Eval("SLOT3").ToString()%>'
                                                                        runat="server" ID="xlbSLOT3" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 4" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT4").ToString()==""?"-":Eval("SLOT4").ToString()%>'
                                                                        runat="server" ID="xlbSLOT4" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 5" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT5").ToString()==""?"-":Eval("SLOT5").ToString()%>'
                                                                        runat="server" ID="xlbSLOT5" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 6" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT6").ToString()==""?"-":Eval("SLOT6").ToString()%>'
                                                                        runat="server" ID="xlbSLOT6" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 7" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT7").ToString()==""?"-":Eval("SLOT7").ToString()%>'
                                                                        runat="server" ID="xlbSLOT7" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 8" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT8").ToString()==""?"-":Eval("SLOT8").ToString()%>'
                                                                        runat="server" ID="xlbSLOT8" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 9" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT9").ToString()==""?"-":Eval("SLOT9").ToString()%>'
                                                                        runat="server" ID="xlbSLOT9" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ช่อง 10" HeaderStyle-HorizontalAlign="Center"
                                                                Width="8%" Settings-AllowDragDrop="False">
                                                                <Settings AllowDragDrop="False"></Settings>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel Text='<%# Eval("SLOT10").ToString()==""?"-":Eval("SLOT10").ToString()%>'
                                                                        runat="server" ID="xlbSLOT10" />
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <SettingsPager AlwaysShowPager="false" PageSize="3" Visible="false">
                                                        </SettingsPager>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right"><span class="corp">จำนวนช่อง :
                                                    <dx:ASPxLabel ID="lblCompartTotal" runat="server" Font-Bold="True">
                                                    </dx:ASPxLabel>
                                                    &nbsp;ช่อง&nbsp; ความจุรวม :
                                                    <dx:ASPxLabel ID="lblCapacityTotal" runat="server" Font-Bold="True">
                                                    </dx:ASPxLabel>
                                                    ลิตร</span> </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"><img src="images/cv.png" width="16" height="16" alt="" /> แนบไฟล์เอกสารสำคัญ
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="padding: 0 0 0 0;">
                                        <%--<dx:ASPxGridView ID="gvwBill" runat="server" AutoGenerateColumns="false" ClientInstanceName="gvwBill"
                                            SkinID="_gvw" Width="100%" OnAfterPerformCallback="gvwBill_AfterPerformCallback" >
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="รายการเอกสาร" Width="20%" CellStyle-BackColor="#B9EAEF">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text='<%#Eval("DOC_DESCRIPTION")%>'
                                                            CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text='*' ForeColor="Red" ClientVisible='<%# (Eval("CDYNAMIC")).ToString() == "Y" ? true : false %>'
                                                            CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="30%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxUploadControl ID="uclBill" runat="server" ClientVisible='<%# (Eval("CONSIDER")).ToString() == "Y" ? false : true %>'
                                                            Width="70%" ClientInstanceName="uclBill" OnFileUploadComplete="uclBill_FileUploadComplete">
                                                            <ClientSideEvents TextChanged="function(s,e){OnStartUpload(s,e)}" FileUploadComplete="
                                                             function(s,e){
                                                                 if(e.callbackData =='')
                                                                 {
                                                                    if(gvwBill.InCallback()) return false; else UploadFile1(s,e);
                                                                 }
                                                                 else
                                                                 {
                                                                    dxWarning('แจ้งเตือน', (e.callbackData+'').split('|')[0]);
                                                                 } 
                                                             }" />
                                                        </dx:ASPxUploadControl>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="50%" Caption="sFileName">
                                                    <DataItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                   
                                                                    <dx:ASPxLabel ID="lblsNameShow" runat="server" Text='<%#Bind("FILE_NAME") %>' Style="display: inline;"
                                                                        CssClass="dxeLineBreakFix">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                                <td><img title="ดูเอกสาร" src="images/view1.png" width="25px" height="25px" border="0"
                                                                class='<%# (Eval("SVISIBLE")).ToString() == "N" ? "cHideControl" : "cShowControl" %>'
                                                                onclick=<%# "javascript:openFile('" + Eval("OPENFILE") + "')" %> style="cursor: pointer;">
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnDelOtherDoc" ClientInstanceName="btnDelOtherDoc" runat="server"
                                                                        ToolTip="ลบรายการ" ClientVisible='<%# (Eval("CONSIDER")).ToString() == "Y" || (Eval("SVISIBLE")).ToString() == "N" ? false : true %>'
                                                                        CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                        <ClientSideEvents Click="function (s, e) { txtIndxDelPublicFile.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {  dxPopupConfirm.Hide();  gvwBill.PerformCallback('DEL;'+ txtIndxDelPublicFile.GetValue() + ';' + s.name); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="DOCTYPE_ID" Visible="false">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager Mode="ShowAllRecords" />
                                            <Settings ShowColumnHeaders="False"></Settings>
                                            <Border BorderStyle="None" ></Border>
                                            <Styles>
                                                <Cell>
                                                    <Border BorderWidth="0px"></Border>
                                                </Cell>
                                            </Styles>
                                        </dx:ASPxGridView>--%>
                                        <dx:ASPxGridView ID="gvwBill" runat="server" AutoGenerateColumns="false" ClientInstanceName="gvwBill"
                                            SkinID="_gvw" Width="100%" OnAfterPerformCallback="gvwBill_AfterPerformCallback"
                                            Border-BorderWidth="0px" Border-BorderStyle="None" Border-BorderColor="White">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="รายการเอกสาร" Width="20%" CellStyle-BackColor="#B9EAEF">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text='<%#Eval("DOC_DESCRIPTION")%>'
                                                            CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text='*' ForeColor="Red" ClientVisible='<%# (Eval("CDYNAMIC")).ToString() == "Y" ? true : false %>'
                                                            CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="30%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxUploadControl ID="uclBill" runat="server" Width="70%" ClientInstanceName="uclBill"
                                                            OnFileUploadComplete="uclBill_FileUploadComplete">
                                                            <ClientSideEvents TextChanged="function(e,s){OnStartUpload(e,s)}" FileUploadComplete="
                                                             function(s,e){
                                                                 if(e.callbackData =='')
                                                                 {
                                                                    if(gvwBill.InCallback()) return false; else UploadFile1(s,e);
                                                                 }
                                                                 else
                                                                 {
                                                                    dxWarning('แจ้งเตือน', (e.callbackData+'').split('|')[0]);
                                                                 } 
                                                             }" />
                                                        </dx:ASPxUploadControl>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="50%" Caption="sFileName">
                                                    <DataItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <%--<img src="images/ic_pdf2.gif" width="16" height="16" border="0" class='<%# (Eval("sOpenFile")).ToString() == "N" ? "cHideControl" : "cShowControl cInline" %>' />--%>
                                                                    <dx:ASPxLabel ID="lblsNameShow" runat="server" Text='<%#Bind("FILE_NAME") %>' Style="display: inline;"
                                                                        CssClass="dxeLineBreakFix">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                                <td><img title="ดูเอกสาร" src="images/view1.png" width="25px" height="25px" border="0"
                                                                class='<%# (Eval("SVISIBLE")).ToString() == "N" ? "cHideControl" : "cShowControl" %>'
                                                                onclick=<%# "javascript:openFile('" + Eval("OPENFILE") + "')" %> style="cursor: pointer;">
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnDelOtherDoc" ClientInstanceName="btnDelOtherDoc" runat="server"
                                                                        ToolTip="ลบรายการ" ClientVisible='<%# Eval("SVISIBLE").ToString() == "N" || Eval("CONSIDER").ToString() == "Y" ? false : true %>'
                                                                        CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                        <ClientSideEvents Click="function (s, e) { txtIndxDelPublicFile.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {  dxPopupConfirm.Hide();  gvwBill.PerformCallback('DEL;'+ txtIndxDelPublicFile.GetValue() + ';' + s.name); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="DOCTYPE_ID" Visible="false">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager Mode="ShowAllRecords" />
                                            <SettingsPager Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <Settings ShowColumnHeaders="False"></Settings>
                                            <Border BorderColor="White"></Border>
                                            <Styles Cell-BorderLeft-BorderColor="White" Cell-BorderBottom-BorderColor="White"
                                                Cell-BorderTop-BorderColor="White" Cell-BorderBottom-BorderWidth="1px">
                                                <Cell>
                                                    <Border BorderWidth="0px"></Border>

<BorderLeft BorderColor="White"></BorderLeft>

<BorderTop BorderColor="White"></BorderTop>

<BorderBottom BorderColor="White" BorderWidth="1px"></BorderBottom>
                                                </Cell>
                                            </Styles>
                                        </dx:ASPxGridView>
                                        <dx:ASPxTextBox ID="txtIndxDelPublicFile" runat="server" ClientInstanceName="txtIndxDelPublicFile"
                                            ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="padding: 0 0 0 0; border: 0px;">
                                        <table id="tbOtherFile" runat="server" style="border-width: 0px; width: 100%; padding: 0 0 0 0;
                                            border: 0px;" cellpadding="4" cellspacing="0">
                                            <tr>
                                                <td colspan="1" style="padding: 0 0 0 5;" bgcolor="#B9EAEF" width="20%">เอกสารอื่นๆ
                                                </td>
                                                <td width="25%" valign="top" style="padding-top: 5px">
                                                    <dx:ASPxUploadControl ID="ulcOtherDoc" ClientInstanceName="ulcOtherDoc" runat="server"
                                                        CssClass="dxeLineBreakFix" Width="82%" NullText="Click here to browse files..."
                                                        OnFileUploadComplete="ulcOtherDoc_FileUploadComplete">
                                                        <ValidationSettings MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                        </ValidationSettings>
                                                        <ClientSideEvents TextChanged="function(s,e){OnStartUpload(s,e)}" FileUploadComplete="
                                                             function(s,e){
                                                                 if(e.callbackData =='')
                                                                 {
                                                                    ulcOtherDoc.Upload(); xcpnGvwotherDoc.PerformCallback('UPLOAD;');
                                                                 }
                                                                 else
                                                                 {
                                                                    dxWarning('แจ้งเตือน', (e.callbackData+'').split('|')[0]);
                                                                 } 
                                                             }" />
                                                    </dx:ASPxUploadControl>
                                                </td>
                                                <td width="5%">
                                                    <dx:ASPxButton ID="btnAddOtherDoc" runat="server" ClientInstanceName="btnAddOtherDoc"
                                                        Text="เพิ่ม" Width="20%" CssClass="dxeLineBreakFix" AutoPostBack="false" ClientVisible="false">
                                                        <ClientSideEvents Click=" function(s,e){  var msg='';   if(ulcOtherDoc.GetText()==''){ msg+='<br>แนบไฟล์เอกสารอื่นๆ'; } if(msg != ''){dxWarning('แจ้งเตือน','กรุณา' + msg); return false;} else{ var filename = ulcOtherDoc.GetText(); var arrfilename =  filename.split('.'); if(arrfilename[arrfilename.length-1] == 'pdf' || arrfilename[arrfilename.length-1] == 'PDF') { ulcOtherDoc.Upload(); xcpnGvwotherDoc.PerformCallback('BIND;') } else { dxWarning('แจ้งเตือน','เฉพาะไฟล์ .pdf เท่านั้น');}   } }    " />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="60%" style="padding: 0 0 0 0;">
                                                    <dx:ASPxCallbackPanel ID="xcpnGvwotherDoc" runat="server" ClientInstanceName="xcpnGvwotherDoc"
                                                        OnCallback="xcpnGvwotherDoc_Callback">
                                                        <PanelCollection>
                                                            <dx:PanelContent>
                                                                <dx:ASPxGridView ID="gvwOtherDoc" ClientInstanceName="gvwOtherDoc" runat="server"
                                                                    SkinID="_gvw" Width="100%">
                                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn Width="75%" Caption="sFileName">
                                                                            <DataItemTemplate>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td><img src="images/ic_pdf2.gif" width="16" height="16" border="0" style="display: none;" />
                                                                                            <dx:ASPxLabel ID="lblsNameShow" runat="server" Text='<%#Bind("FILE_NAME") %>'>
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                        <td><img title="ดูเอกสาร" src="images/view1.png" width="25px" height="25px" border="0"
                                                                                        class='<%# (Eval("SVISIBLE")).ToString() == "N" ? "cHideControl" : "cShowControl" %>'
                                                                                        onclick=<%# "javascript:openFile('" + Eval("OPENFILE") + "')" %> style="cursor: pointer;">
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxButton ID="btnDelOtherDoc" ClientInstanceName="btnDelOtherDoc" runat="server"
                                                                                                ToolTip="ลบรายการ" ClientVisible='<%# (Eval("CONSIDER")).ToString() == "Y" ? false : true %>'
                                                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                                <ClientSideEvents Click="function (s, e) { txtIndxDelOterhFile.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {  dxPopupConfirm.Hide(); if(xcpnGvwotherDoc.InCallback()) return; else xcpnGvwotherDoc.PerformCallback('DEL;'+ txtIndxDelOterhFile.GetValue() + ';' + s.name); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                                            </dx:ASPxButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="DOC_ID" Visible="false">
                                                                        </dx:GridViewDataColumn>
                                                                    </Columns>
                                                                    <SettingsPager Mode="ShowAllRecords" />
                                                                    <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                                                    <Styles>
                                                                        <Table>
                                                                            <Border BorderStyle="None" />
                                                                        </Table>
                                                                        <Cell>
                                                                            <Border BorderWidth="0px" />
                                                                        </Cell>
                                                                    </Styles>
                                                                    <Border BorderWidth="0px" />
                                                                </dx:ASPxGridView>
                                                                <dx:ASPxTextBox ID="txtIndxDelOterhFile" runat="server" ClientInstanceName="txtIndxDelOterhFile"
                                                                    ClientVisible="false">
                                                                </dx:ASPxTextBox>
                                                            </dx:PanelContent>
                                                        </PanelCollection>
                                                    </dx:ASPxCallbackPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#B9EAEF">ชื่อผู้ยื่นคำขอ<font color="red">*</font></td>
                                    <td colspan="3">
                                        <dx:ASPxTextBox runat="server" ID="txtName" Width="40%">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุชื่อผู้ยื่นคำขอ"
                                                RequiredField-IsRequired="true">
                                                <RequiredField IsRequired="True" ErrorText="ระบุชื่อผู้ยื่นคำขอ"></RequiredField>
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#B9EAEF">เบอร์โทรศัพท์<font color="red">*</font></td>
                                    <td colspan="3">
                                        <dx:ASPxTextBox runat="server" ID="txtPhone" Width="40%">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุเบอร์โทรศัพท์"
                                                RequiredField-IsRequired="true">
                                                <RequiredField IsRequired="True" ErrorText="ระบุเบอร์โทรศัพท์"></RequiredField>
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <dx:ASPxButton ID="btnSubmit" runat="server" ClientInstanceName="btnSubmit" Text="ส่งคำขอ"
                                            CssClass="dxeLineBreakFix" AutoPostBack="false" ValidationGroup="add">
                                            <ClientSideEvents Click="function(s,e){
                                if(!ASPxClientEdit.ValidateGroup('add')) return false;
                                
                                if(txtCalendarDate.GetText() == '')
                                {
                                    dxWarning('แจ้งเตือน','กรุณาระบุวันที่นัดหมาย');
                                    return false;
                                }
                                else
                                {
                                  if(txtStatusNow.GetValue() != '06')
                                  {
                                      if(DateDecreasSysdate())
                                      {
                                            xcpn.PerformCallback('SAVE;' + txtCalendarDate.GetText());
                                      }
                                      else
                                      {
                                          dxConfirm('คุณต้องการยืนยันการส่งคำขอใช่หรือไม่','ท่านได้ใส่วันนัดหมายย้อนหลัง ท่านต้องการบันทึกรายการ ใช่หรือไม่', function (s, e) { xcpn.PerformCallback('SAVE;' + txtCalendarDate.GetText()); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} );
                                      }
                                  }
                                  else
                                  {
                                    xcpn.PerformCallback('SAVE;' + txtCalendarDate.GetText());
                                  }
                                
                                }}" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnCancel" runat="server" ClientInstanceName="btnCancel" Text="ย้อนกลับ"
                                            CssClass="dxeLineBreakFix" AutoPostBack="false">
                                            <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('BACK;');}" />
                                        </dx:ASPxButton>
                                        <dx:ASPxTextBox runat="server" ID="txtDatenow" ClientInstanceName="txtDatenow" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox ID="txtStatusNow" runat="server" ClientInstanceName="txtStatusNow"
                                            ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"><img src="images/comment.png" width="16" height="16" alt="" /> หมายเหตุ
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <dx:ASPxGridView ID="gvwhistory" runat="server" ClientInstanceName="gvwhistory" AutoGenerateColumns="false"
                                            Width="100%" SkinID="_gvw" Style="margin-top: 0px">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="วันที่ - เวลา" Width="15%" FieldName="REMARK_DATE">
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ผู้บันทึก" Width="20%" FieldName="SNAME">
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ขั้นตอน" Width="25%" FieldName="SDESCRIPTION">
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="หมายเหตุ" Width="40%" FieldName="REMARKS">
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
