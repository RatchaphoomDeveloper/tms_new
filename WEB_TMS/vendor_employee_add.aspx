﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_employee_add.aspx.cs" Inherits="vendor_employee_add" EnableViewState="true" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>


<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script src="Javascript/readable-range.js" type="text/jscript"></script>
    <style type="text/css">
        #cph_Main_xcpn_rpnInformation_uploadEMP_TextBox0 {
            padding: 0px;
            border-bottom-color: White !important;
            border-left-color: White !important;
            border-right-color: White !important;
            border-top-color: White !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            border-right-width: 0px !important;
            border-top-width: 0px !important;
            border-bottom-style: none !important;
            border-left-style: none !important;
            border-right-style: none !important;
            border-top-style: none !important;
        }

        #cph_Main_xcpn_rpnInformation_uploadEMP_ClearBox0 {
            padding: 0px;
            border-bottom-color: White !important;
            border-left-color: White !important;
            border-right-color: White !important;
            border-top-color: White !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            border-right-width: 0px !important;
            border-top-width: 0px !important;
            border-bottom-style: none !important;
            border-left-style: none !important;
            border-right-style: none !important;
            border-top-style: none !important;
        }

        #cph_Main_rpnInformation_uploadEMP_UploadInputs tr td {
            border: 0px;
        }
    </style>
    <script type="text/javascript">

        function rblChang(Value) {
            if (Value == "0") {
                $('div[id*="Typedefault"]').hide();
                $('div[id*="trWait"]').show();
                $('div[id*="trstatusWait"]').show();
                $('div[id*="trCancel"]').hide();
                $('div[id*="trsCancelstatus"]').hide();
            }
            else if (Value == "1") {
                $('div[id*="Typedefault"]').show();
                $('div[id*="trWait"]').hide();
                $('div[id*="trstatusWait"]').hide();
                $('div[id*="trCancel"]').hide();
                $('div[id*="trsCancelstatus"]').hide();
            }
            else if (Value == "2") {
                $('div[id*="Typedefault"]').hide();
                $('div[id*="trWait"]').hide();
                $('div[id*="trstatusWait"]').hide();
                $('div[id*="trCancel"]').show();
                $('div[id*="trsCancelstatus"]').show();
            }
        }

        function VisibleControlEMP() {
            if (!($('#txtEMPFilePath').val() == "" || $('#txtEMPFilePath').val() == null)) {
                chkEMPUpload1.SetValue('1');

            } else {
                chkEMPUpload1.SetValue('');
            }
        }

        function VisibleControl() {
            var bool = txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null;
            uploader1.SetClientVisible(bool);
            txtFileName.SetClientVisible(!bool);
            btnView.SetEnabled(!bool);
            btnDelFile.SetEnabled(!bool);
            if (!(txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null)) {
                chkUpload1.SetValue('1');
            } else {
                chkUpload1.SetValue('');
            }

        }

        function VisibleControl2() {


            var bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
            uploader2.SetClientVisible(bool);
            txtFileName2.SetClientVisible(!bool);
            btnView2.SetEnabled(!bool);
            btnDelFile2.SetEnabled(!bool);
            if (!(txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null)) {
                chkUpload2.SetValue('1');
            } else {
                chkUpload2.SetValue('');
            }

        }

        function VisibleControl3() {


            var bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
            uploader3.SetClientVisible(bool);
            txtFileName3.SetClientVisible(!bool);
            btnView3.SetEnabled(!bool);
            btnDelFile3.SetEnabled(!bool);
            if (!(txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null)) {
                chkUpload3.SetValue('1');
            } else {
                chkUpload3.SetValue('');
            }

        }

        function VisibleControl4() {

            var bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
            uploader4.SetClientVisible(bool);
            txtFileName4.SetClientVisible(!bool);
            btnView4.SetEnabled(!bool);
            btnDelFile4.SetEnabled(!bool);
            if (!(txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null)) {
                chkUpload4.SetValue('1');
            } else {
                chkUpload4.SetValue('');
            }
        }

        function OnBirthdayValidation(s, e) {


            var Enddate = dedtendPercode.GetDate();
            var Startdate = dedtstartPercode.GetDate();

            if (Enddate == null) {
                e.isValid = false;
                e.errorText = "กรุณาระบุเเวลาอนุญาตบัตรประชาชน";
            }
            else {
                if (Enddate < Startdate) {
                    e.isValid = false;
                    e.errorText = "วันที่สิ้นสุดต้องไม่น้อยกว่าวันที่เริ่มต้น";
                }
            }
        }

        //        function OnValueChanged(s, e) {
        //            Page_ClientValidate(""); // undocumented
        //        }

        //        function validateDates(s, e) {
        //            var date1 = e1.GetDate();
        //            var date2 = e2.GetDate();
        //            e.IsValid = date1 == null || date2 == null || date1 < date2;
        //        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">


    <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="100%"
        HeaderText="รายละเอียดพนักงาน ">
        <PanelCollection>
            <dx:PanelContent runat="server" ID="arp">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table id="Table1" width="100%" runat="server">
                    <tr id="trHistoryEmp" runat="server" style="display: none;">
                        <td></td>
                        <td align="right">
                            <asp:Button Text="ประวัติพนักงาน" ID="btnHistoryEmp" OnClick="btnHistoryEmp_Click" runat="server" CssClass="btn btn-md bth-hover btn-info" />

                        </td>
                    </tr>
                    <tr>
                        <td width="30%" valign="top">
                            <asp:Panel ID="plImage" runat="server">
                                <table id="Table2" runat="server" width="100%">
                                    <tr>
                                        <td align="center">
                                            <dx:ASPxImage runat="server" ID="imgEmp" Width="175px" Height="200px" ClientInstanceName="imgEmp">
                                            </dx:ASPxImage>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td width="32px"></td>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uploadEMP" runat="server" ClientInstanceName="uploadEMP"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uploadEMP_FileUploadComplete"
                                                            Width="150px" CssClass="dxucTextBox_Aqua2">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData ==&#39;&#39;)
                 {dxWarning(&#39;แจ้งเตือน&#39;,&#39;ระบบสามารถ import file ได้เฉพาะ นามสกุล .jpg,.jpeg หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที&#39;);}
                 else{txtEMPFilePath.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[0]);txtEMPFileName.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[1]);txtEMPTruePath.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[2]);txtEMPSysfilename.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[3]);imgEmp.SetImageUrl((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[2]+(e.callbackData+&#39;&#39;).split(&#39;|&#39;)[3]);VisibleControlEMP();} }"
                                                                TextChanged="function(s,e){uploadEMP.Upload();}"></ClientSideEvents>
                                                            <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                            <ClientSideEvents TextChanged="function(s,e){uploadEMP.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .jpg,.jpeg หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{$('#txtEMPFilePath').val((e.callbackData+'').split('|')[0]);$('#txtEMPFileName').val((e.callbackData+'').split('|')[1]);$('#txtEMPTruePath').val((e.callbackData+'').split('|')[2]);$('#txtEMPSysfilename').val((e.callbackData+'').split('|')[3]);imgEmp.SetImageUrl((e.callbackData+'').split('|')[2]+(e.callbackData+'').split('|')[3]);VisibleControlEMP();} }" />
                                                            <BrowseButton Text="เลือกรูปประจำตัว">
                                                            </BrowseButton>
                                                        </dx:ASPxUploadControl>
                                                        <div style="display: none;">
                                                            <asp:TextBox runat="server" ID="txtEMPFilePath" ClientIDMode="Static" />
                                                            <asp:TextBox runat="server" ID="txtEMPFileName" ClientIDMode="Static" />
                                                            <asp:TextBox runat="server" ID="txtEMPTruePath" ClientIDMode="Static" />
                                                            <asp:TextBox runat="server" ID="txtEMPSysfilename" ClientIDMode="Static" />
                                                            <dx:ASPxTextBox ID="chkEMPUpload1" runat="server" ClientInstanceName="chkEMPUpload1"
                                                                ForeColor="White" Width="1px">
                                                                <Border BorderStyle="None" />
                                                                <Border BorderStyle="None"></Border>
                                                            </dx:ASPxTextBox>
                                                        </div>


                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label1" Text="&nbsp;*" ForeColor="Red" runat="server" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>

                        </td>
                        <td width="70%">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="plInformation" runat="server">
                                        <div class="form-horizontal">
                                            <div id="divEmpID" runat="server" style="display: none;" class="row form-group">
                                                <label class="col-md-4 control-label">รหัสพนักงาน :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox runat="server" ID="txtEmpID" CssClass="form-control" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">ตำแหน่ง :<asp:Label ID="lblReqddlPosition" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                                <div class="col-md-6  ">
                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlPosition" AutoPostBack="true" OnSelectedIndexChanged="ddlPosition_SelectedIndexChanged">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">คำนำหน้าชื่อ :<asp:Label ID="lblReqddlTitle" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6 ">
                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlTitle">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">ชื่อ :<asp:Label ID="lblReqtxtName" Text="&nbsp;*" ForeColor="Red" Visible="false" runat="server" /></label>
                                                <div class="col-md-6 ">
                                                    <asp:TextBox runat="server" ID="txtName" CssClass="form-control" placeholder="กรุณาระบุชื่อ" MaxLength="50" />

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">นามสกุล :<asp:Label ID="lblReqtxtSurName" Text="&nbsp;*" ForeColor="Red" Visible="false" runat="server" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtSurName" CssClass="form-control" placeholder="กรุณาระบุนามสกุล" MaxLength="50" />

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">วันเกิด :<asp:Label ID="lblReqdedtBirthDay" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="dedtBirthDay" CssClass="datepicker " />
                                                    <div class="clearfix"></div>
                                                    <div id="DateBirth" style="text-align: center;" ></div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">หมายเลขโทรศัพท์หลัก :<asp:Label ID="lblReqtxtTel" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtTel" CssClass="form-control numberPhone" MaxLength="50" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">หมายเลขโทรศัพท์สำรอง :<asp:Label ID="lblReqtxtTel2" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtTel2" CssClass="form-control numberPhone" MaxLength="50" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">อีเมล์หลัก :<asp:Label ID="lblReqtxtMail" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtMail" CssClass="form-control "  MaxLength="100"/>

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">อีเมล์สำรอง :<asp:Label ID="lblReqtxtMail2" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtMail2" CssClass="form-control " MaxLength="100" />

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">บริษัทผู้ขนส่ง :<asp:Label ID="lblReqcboVendor" Text="&nbsp;*" ForeColor="Red" Visible="false" runat="server" /></label>
                                                <div class="col-md-6">
                                                    <dx:ASPxComboBox ID="cboVendor" runat="server" Width="100%" ClientInstanceName="cboVendor" CssClass="form-control" TextFormatString="{1}" ValueField="SVENDORID" IncrementalFilteringMode="Contains">

                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="200px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>

                                                    <asp:HiddenField runat="server" ID="hidVendor" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">หมายเลขบัตรประชาชน :<asp:Label ID="lblReqtxtPercode" Text="&nbsp;*" ForeColor="Red" Visible="false" runat="server" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" onkeypress="IDCard(this)" ID="txtPercode" CssClass="IDCard number form-control" MaxLength="17" />


                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">ช่วงระยะเวลาอนุญาตบัตรประชาชน :<asp:Label ID="lblReqdedtstartPercode" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="dedtstartPercode" CssClass="datepicker" />

                                                </div>

                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">ถึง :<asp:Label ID="lblReqdedtendPercode" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="dedtendPercode" CssClass="datepicker" />
                                                    <div class="clearfix"></div>
                                                    <div id="Card_exprie" style="text-align:center"></div>
                                                </div>

                                                
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">วันที่บริษัทจ้างงาน :<asp:Label ID="lblReqdedStartWorkDate" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="dedStartWorkDate" CssClass="datepicker" />

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">หมายเลขใบขับขี่ประเภท 4 :<asp:Label ID="lblReqtxtdrivelicence" Text="&nbsp;*" Visible="false" ForeColor="Red" runat="server" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtdrivelicence" CssClass="form-control " MaxLength="50" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">ช่วงระยะเวลาอนุญาตใบขับขี่ ประเภท 4 :<asp:Label ID="lblReqdedtstartlicence" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="dedtstartlicence" CssClass="datepicker" />

                                                </div>

                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">ถึง :<asp:Label ID="lblReqdedtEndlicence" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="dedtEndlicence" CssClass="datepicker" />
                                                    <div class="clearfix"></div>
                                                    <div id="licence_exprie" style="text-align:center"></div>
                                                </div>
                                            </div>
                                            <div class="row form-group">

                                                <label class="col-md-4 control-label">คลังต้นทาง :<asp:Label ID="lblReqcboMainPlant" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                <div class="col-md-6">
                                                    <dx:ASPxComboBox ID="cboMainPlant" ClientInstanceName="cboMainPlant" runat="server" Width="100%"
                                                        CssClass="form-control" TextFormatString="{1}" ValueField="STERMINALID" IncrementalFilteringMode="Contains">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" />
                                                            <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="STERMINALNAME" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>

                                                </div>

                                            </div>
                                            <div id="div11" runat="server" class="">
                                                <div class="row form-group">
                                                    <label class="col-md-4 control-label">หมายเลขใบขับขี่เชิงป้องกันอุบัติเหตุ (DDC) :<asp:Label ID="lblReqtxtNumAccident" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox runat="server" ID="txtNumAccident" CssClass="form-control" MaxLength="50" />

                                                    </div>

                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-md-4 control-label">ช่วงระยะเวลาอนุญาติ :<asp:Label ID="lblReqAccidentStartDate" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox runat="server" ID="txtAccidentStartDate" CssClass="datepicker" />

                                                    </div>

                                                </div>
                                                <div class="row form-group">
                                                    <label class="col-md-4 control-label">ถึง :<asp:Label ID="lblReqtxtAccidentEndDate" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox runat="server" ID="txtAccidentEndDate" CssClass="datepicker" />
                                                        <div class="clearfix">&nbsp;</div>
                                                        <div id="AccidentEndDate_exprie" style="text-align:center"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="plDocument" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel2" ClientInstanceName="rpn" runat="server" Width="100%"
                                HeaderText="เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ)">
                                <PanelCollection>
                                    <dx:PanelContent runat="server" ID="PanelContent2">

                                        <asp:GridView ID="dgvRequestFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                            <Columns>
                                                <asp:BoundField DataField="PERSON_TYPE_DESC" HeaderText="หัวข้อ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PERSON_TYPE" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        </asp:GridView>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" ClientInstanceName="rpn" runat="server" Width="100%"
                        HeaderText="เอกสารสำคัญของพนักงาน">
                        <PanelCollection>
                            <dx:PanelContent runat="server" ID="PanelContent1">
                                <div class="row form-group">
                                    <div class="col-md-4 text-right padding-top9">
                                    </div>
                                    <div class="col-md-4  ">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 text-right padding-top9">
                                    </div>
                                    <div class="col-md-4  ">
                                        <asp:DropDownList runat="server" ID="ddlUploadType" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 text-right padding-top9">
                                    </div>
                                    <div class="col-md-4  ">
                                        <asp:FileUpload ID="fileUpload" runat="server" />

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 text-right padding-top9">
                                    </div>
                                    <div class="col-md-4  ">
                                        <asp:Button Text="Upload" ID="btnUpload" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUpload_Click" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12  ">
                                        <asp:GridView ID="dgvUploadFile" runat="server" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                                            HorizontalAlign="Center" AutoGenerateColumns="false"
                                            CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH" OnRowDeleting="dgvUploadFile_RowDeleting"
                                            OnRowUpdating="dgvUploadFile_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                        </asp:GridView>
                                    </div>
                                </div>

                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    </asp:Panel>
<asp:Panel ID="plStatus" runat="server">
                    <div class="form-horizontal">
                        <div id="tbStatus" runat="server" width="100%">
                            <div class="row form-group">
                                <label class="col-md-4 control-label">สถานะการทำงาน :</label>
                                <div class="col-md-4">
                                    <dx:ASPxRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal" Width="100%">
                                        <Items>
                                            <dx:ListEditItem Text="อนุญาต" Value="1" />
                                            <dx:ListEditItem Text="ระงับการใช้งาน" Value="0" />
                                            <dx:ListEditItem Text="Black List" Value="2" />
                                        </Items>

                                        <ClientSideEvents ValueChanged="function(s,e){ rblChang(s.GetValue()) }" />
                                    </dx:ASPxRadioButtonList>
                                </div>
                            </div>
                            <div runat="server" id="Typedefault" class="row form-group">
                                <label class="col-md-4 control-label">สาเหตุการอนุญาติ :</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtConfirm" Rows="5" CssClass="form-control" />
                                   
                                </div>
                            </div>
                            <div runat="server" id="trWait">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ประเภทการระงับ :</label>
                                    <div class="col-md-5">
                                        <asp:DropDownList CssClass="form-control" runat="server" ID="ddlStatus">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">วันที่ระงับถึง :</label>
                                    <div class="col-md-5">
                                        <asp:TextBox runat="server" ID="txtSettleTo" CssClass="datepicker" />
                                    </div>
                                </div>
                            </div>

                            <div runat="server" id="trstatusWait" class="row form-group">
                                <label class="col-md-4 control-label">สาเหตุที่ระงับ :</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtComment" Rows="5" CssClass="form-control" />
                                </div>
                            </div>
                            <div runat="server" id="trCancel" class="row form-group">
                                <label class="col-md-4 control-label">ประเภทการ Black List :</label>
                                <div class="col-md-5">
                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlStatus2" Width="100%"></asp:DropDownList>
                                </div>
                            </div>
                            <div runat="server" id="trsCancelstatus" class="row form-group">
                                <label class="col-md-4 control-label">สาเหตุที่ Black List :</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtstatus2" Rows="5" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-4 control-label">ใช้งานล่าสุด :</label>
                                <div class="col-md-4">
                                    <dx:ASPxLabel CssClass="control-label" runat="server" ID="lbldateuse">
                                    </dx:ASPxLabel>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-4 control-label">วันที่ใช้งานล่าสุดเมื่อ :</label>
                                <div class="col-md-4">
                                    <dx:ASPxLabel CssClass="control-label" runat="server" ID="lblDayuse">
                                    </dx:ASPxLabel>
                                </div>
                            </div>
                            <div class="row form-group hide">
                                <label class="col-md-4 control-label">สาเหตุการไม่ได้งานเกินกำหนด :</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtComment2" Rows="5" CssClass="form-control" />
                                   
                                </div>
                            </div>


                        </div>
                        <div id="trUpdateUser" runat="server">
                            <div class="row form-group">
                                <label class="col-md-4 control-label">วันที่อัพเดทข้อมูลล่าสุด :</label>
                                <div class="col-md-4">
                                    <dx:ASPxLabel CssClass="control-label" runat="server" ID="lblDUPDATE">
                                    </dx:ASPxLabel>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-4 control-label">ผู้อัพเดทข้อมูลล่าสุด :</label>
                                <div class="col-md-4">
                                    <dx:ASPxLabel CssClass="control-label" runat="server" ID="lblSUPDATE">
                                    </dx:ASPxLabel>
                                </div>
                            </div>
                        </div>
                        <div id="divDescription" runat="server">
                            <div class="row form-group">
                                <label class="col-md-4 control-label">สถานะคำขอ :</label>
                                <div class="col-md-5">
                                    <dx:ASPxLabel CssClass="control-label" runat="server" ID="lblStatus" Text="-">
                                        
                                    </dx:ASPxLabel>
                                    <asp:HiddenField runat="server" ID="hidStatusID" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-4 control-label">หมายเหตุ :<asp:Label ID="Label2" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtDescription" Rows="5" CssClass="form-control" />
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="row form-group">
                            <div class="col-md-1" id="divEmty" runat="server">
                            </div>
                            <div class="col-md-2" runat="server" id="tdSaveDraft">
                                <input id="btnSaveDraft" runat="server" type="button" value="บันทึกชั่วคราว" data-toggle="modal" data-target="#ModalConfirmBeforeSaveDraft" class="btn btn-md bth-hover btn-info" style="width: 100%" />
                            </div>
                            <div class="col-md-2" runat="server" id="tdEdit">
                                <input id="btnEdit" runat="server" type="button" value="ขอข้อมูลเพิ่มเติม" data-toggle="modal" data-target="#ModalConfirmBeforeEdit" class="btn btn-md bth-hover btn-info" style="width: 100%" />
                            </div>
                            <div class="col-md-2" runat="server" id="tdApprove">
                                <input id="btnApprove" runat="server" type="button" value="บันทึก และ อนุมัติ" data-toggle="modal" data-target="#ModalConfirmBeforeApprove" class="btn btn-md bth-hover btn-info" style="width: 100%" />
                            </div>
                            <div class="col-md-2" runat="server" id="tdNoApprove">
                                <input id="btnNoApprove" runat="server" type="button" value="ปฏิเสธ" data-toggle="modal" data-target="#ModalConfirmBeforeNoApprove" class="btn btn-md bth-hover btn-info" style="width: 100%" />
                            </div>
                            <div class="col-md-2" runat="server" id="tdSave">
                                <input id="btnSave" runat="server" type="button" value="บันทึกและส่งข้อมูล" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" style="width: 100%" />
                            </div>
                            <div class="col-md-2">
                                <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-info" style="width: 100%" />
                            </div>

                        </div>


                    </div>

                    <div style="display: none;">
                        <asp:HiddenField ID="UpEMP" runat="server" />

                    </div>
                </asp:Panel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
    <div style="display: none;">
        <input type="button" id="btnConfirmBeforeSave" name="name" value=" " data-toggle="modal" data-target="#ModalConfirmBeforeSaveCheckVendor" />
    </div>
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveCheckVendor" IDModel="ModalConfirmBeforeSaveCheckVendor" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="btnSave_Click" TextTitle="ยืนยันการเปลี่ยนบริษัทผู้ขนส่ง" TextDetail="คุณต้องการเปลี่ยนบริษัทผู้ขนส่งใช่หรือไม่ ?" CommandArgument="ConfirmBeforeSaveCheckVendor" />
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="btnSave_Click" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveDraft" IDModel="ModalConfirmBeforeSaveDraft" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSaveDraft_ClickOK" TextTitle="ยืนยันการบันทึกชั่วคราว" TextDetail="คุณต้องการบันทึกชั่วคราวใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmEdit" IDModel="ModalConfirmBeforeEdit" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmEdit_ClickOK" TextTitle="ยืนยันการขอข้อมูลเพิ่มเติม" TextDetail="คุณต้องการขอข้อมูลเพิ่มเติมใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmApprove" IDModel="ModalConfirmBeforeApprove" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmApprove_ClickOK" TextTitle="ยืนยันการบันทึก และ อนุมัติ" TextDetail="คุณต้องการบันทึก และ อนุมัติใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmNoApprove" IDModel="ModalConfirmBeforeNoApprove" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmNoApprove_ClickOK" TextTitle="ยืนยันการปฏิเสธ" TextDetail="คุณต้องการปฏิเสธใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmBack" IDModel="ModalConfirmBack" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmBack_ClickOK" TextTitle="ยืนยันการย้อนกลับ" TextDetail="คุณต้องการย้อนกลับใช่หรือไม่ ?" />
    <asp:HiddenField ID="hidID" runat="server" />
    <script>
        $(document).ready(function () {
            updatedate();
            onLoadData();
            //คำนวณวันเกิด
            $("#<%=dedtBirthDay.ClientID %>").change(function () {
                var months = moment.preciseDiff(moment($("#<%=dedtBirthDay.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#DateBirth").text("อายุ " + months);
            });
            //END คำนวณวันเกิด
            //คำนวณบัตรประชาชน
            $("#<%=dedtendPercode.ClientID %>").change(function () {
                var Card_exprie = moment.preciseDiff(moment($("#<%=dedtendPercode.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#Card_exprie").text("หมดอายุใน " + Card_exprie);
            });
            //END คำนวณวันเกิด
            //หมายเลขใบขับขี่ประเภท 4
            $("#<%=dedtEndlicence.ClientID %>").change(function () {
                var licence_exprie = moment.preciseDiff(moment($("#<%=dedtEndlicence.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#licence_exprie").text("หมดอายุใน " + licence_exprie);
            });
            //END คำนวณวันเกิด
            //หมายเลขขับขี่เชิงป้องกันอุบัติเหตุ 
            $("#<%=txtAccidentEndDate.ClientID %>").change(function () {
                var AccidentEndDate_exprie = moment.preciseDiff(moment($("#<%=txtAccidentEndDate.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#AccidentEndDate_exprie").text("หมดอายุใน " + AccidentEndDate_exprie);
            });
            //END หมายเลขขับขี่เชิงป้องกันอุบัติเหตุ

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();        
        prm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            updatedate();
            SetdedtBirthDay();
        }
        function updatedate() {
            $("#<%=dedtstartPercode.ClientID %>").on('change', function (e) {
                $("#<%=dedtendPercode.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate($(this).val());
            });
            $("#<%=dedtendPercode.ClientID %>").on('change', function (e) {
                $("#<%=dedtstartPercode.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate($(this).val());
            });

            $("#<%=dedtstartlicence.ClientID %>").on('change', function (e) {
                $("#<%=dedtEndlicence.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate($(this).val());
            });
            $("#<%=dedtEndlicence.ClientID %>").on('change', function (e) {
                $("#<%=dedtstartlicence.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate($(this).val());
            });
        }
        function confirmChangevendor() {
            $('#btnConfirmBeforeSave').click();
        }
        function backurl() {
            history.back();
            return false;
        }
        function SetdedtBirthDay() {
            $("#<%=dedtBirthDay.ClientID %>").parent('.input-group.date').datepicker('update');
        }
        function onLoadData() {
            if ($("#<%=dedtBirthDay.ClientID %>").val() != "") {
                var months = moment.preciseDiff(moment($("#<%=dedtBirthDay.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#DateBirth").text("อายุ " +months);
            }
            //////
            if ($("#<%=dedtendPercode.ClientID %>").val() != "") {
                var Card_exprie = moment.preciseDiff(moment($("#<%=dedtendPercode.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#Card_exprie").text("หมดอายุใน " + Card_exprie);
            }
            //////////
            if ($("#<%=dedtEndlicence.ClientID %>").val() != "") {
                var licence_exprie = moment.preciseDiff(moment($("#<%=dedtEndlicence.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#licence_exprie").text("หมดอายุใน " + licence_exprie);
            }
            ////////
            if ($("#<%=txtAccidentEndDate.ClientID %>").val() != "") {
                var AccidentEndDate_exprie = moment.preciseDiff(moment($("#<%=txtAccidentEndDate.ClientID %>").val(), "DD/MM/YYYY"), moment());
                $("#AccidentEndDate_exprie").text("หมดอายุใน " + AccidentEndDate_exprie);
            }
            ///////
        }
    </script>

</asp:Content>
