﻿using EmailHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;

public partial class AccidentTab3 : PageBase
{
    DataTable dt;
    DataSet ds;
    DataRow dr;
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CGROUP"] + string.Empty == "0")
            {
                Response.Redirect("AccidentTab3_Vendor.aspx?str=" + Request.QueryString["str"]);
                return;
            }

            hidCGROUP.Value = Session["CGROUP"] + string.Empty;
            //SetDrowDownList();

            if (Request.QueryString["str"] != null && !string.IsNullOrEmpty(Request.QueryString["str"]))
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SetData(strQuery);
                liTab1.Visible = true;
                GeneralTab1.HRef = "AccidentTab1.aspx?str=" + str;
                liTab2.Visible = true;
                GeneralTab2.HRef = "AccidentTab2.aspx?str=" + str;
                if (int.Parse(hidCactive.Value) >= 7)
                {
                    liTab4.Visible = true;
                    GeneralTab4.HRef = "AccidentTab4.aspx?str=" + str;
                }
            }
        }
        SetEnabledControlByID(hidCactive.Value);
    }
    #endregion

    #region SetData
    private void SetData(string ACCIDENT_ID)
    {
        ds = AccidentBLL.Instance.AccidentTab3Select(ACCIDENT_ID);
        if (ds.Tables.Count > 0)
        {
            #region จาก Tab1
            dt = ds.Tables["ACCIDENT"];
            if (dt != null && dt.Rows.Count > 0)
            {

                DataRow dr = dt.Rows[0];
                txtAccidentID.Text = dr["ACCIDENT_ID"] + string.Empty;
                if (!dr.IsNull("ACCIDENT_DATE"))
                {
                    txtAccidentDate.Text = DateTime.Parse(dr["ACCIDENT_DATE"] + string.Empty).ToString(DateTimeFormat);
                }
                if (!dr.IsNull("SYS_TIME"))
                    txtAccidentDateSystem.Text = DateTime.Parse(dr["SYS_TIME"] + string.Empty).ToString(DateTimeFormat);
                rblREPORTER.SelectedValue = dr["REPORTER"] + string.Empty;
                if (!dr.IsNull("REPORT_PTT"))
                    txtAccidentDatePtt.Text = DateTime.Parse(dr["REPORT_PTT"] + string.Empty).ToString(DateTimeFormat);
                txtAccidentName.Text = dr["INFORMER_NAME"] + string.Empty;
                if (!dr.IsNull("TIMECREATE"))
                    txtDatePrimary.Text = DateTime.Parse(dr["TIMECREATE"] + string.Empty).ToString(DateTimeFormat);
                if (!dr.IsNull("DAMAGECREATE"))
                    txtDateAnalysis.Text = DateTime.Parse(dr["DAMAGECREATE"] + string.Empty).ToString(DateTimeFormat);
                hidID.Value = txtAccidentID.Text;
                hidCactive.Value = dr["CACTIVE"] + string.Empty;
                hidSEMPLOYEEID.Value = dr["SEMPLOYEEID"] + string.Empty;
                hidPERS_CODE.Value = dr["PERS_CODE"] + string.Empty;
                switch (dr["DRIVER_NO"] + string.Empty)
                {
                    case "2":
                        hidSEMPLOYEEID.Value = dr["SEMPLOYEEID2"] + string.Empty;
                        hidPERS_CODE.Value = dr["PERS_CODE2"] + string.Empty;
                        break;
                    case "3":
                        hidSEMPLOYEEID.Value = dr["SEMPLOYEEID3"] + string.Empty;
                        hidPERS_CODE.Value = dr["PERS_CODE3"] + string.Empty;
                        break;
                    case "4":
                        hidSEMPLOYEEID.Value = dr["SEMPLOYEEID4"] + string.Empty;
                        hidPERS_CODE.Value = dr["PERS_CODE4"] + string.Empty;
                        break;
                    default:
                        break;
                }
                
                
                txtDAMAGE.Text = dr["DAMAGE"] + string.Empty;
                if (!dr.IsNull("APPROVE_DATE"))
                    txtApproveDate.Text = DateTime.Parse(dr["APPROVE_DATE"] + string.Empty).ToString(DateTimeFormat);
            }
            #endregion

            #region IMAGE
            dt = ds.Tables["IMAGE"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dtUpload = dt;
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            #endregion

            #region APPROVEEVENT
            dt = ds.Tables["APPROVEEVENT"];
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                rblEVALUATION.SelectedValue = dr["EVALUATION"] + string.Empty;
                txtEVALUATION_DETAIL.Text = dr["EVALUATION_DETAIL"] + string.Empty;
                rblSERIOUS.SelectedValue = dr["SERIOUS"] + string.Empty;
                txtEVALUATION_DETAIL.Text = dr["SERIOUS_DETAIL"] + string.Empty;
                rblLIFE.SelectedValue = dr["LIFE"] + string.Empty;
                rblPROPERTY.SelectedValue = dr["PROPERTY"] + string.Empty;
                rblENVIRONMENT.SelectedValue = dr["ENVIRONMENT"] + string.Empty;
                rblCORPORATE.SelectedValue = dr["CORPORATE"] + string.Empty;
                txtOil.Text = dr["OIL"] + string.Empty;
                ListItem listItem = null;
                for (int i = 0; i < cblPERSONALFACTORS.Items.Count; i++)
                {
                    listItem = cblPERSONALFACTORS.Items[i];
                    if ((dr["PERSONALFACTORS"] + string.Empty).Contains(listItem.Value))
                    {
                        listItem.Selected = true;
                    }
                }
                for (int i = 0; i < cblROUTEHAZARDOUS.Items.Count; i++)
                {
                    listItem = cblROUTEHAZARDOUS.Items[i];
                    if ((dr["ROUTEHAZARDOUS"] + string.Empty).Contains(listItem.Value))
                    {
                        listItem.Selected = true;
                    }
                }
                for (int i = 0; i < cblTRUCK.Items.Count; i++)
                {
                    listItem = cblTRUCK.Items[i];
                    if ((dr["TRUCK"] + string.Empty).Contains(listItem.Value))
                    {
                        listItem.Selected = true;
                    }
                }
                for (int i = 0; i < cblENVIRONMENTS.Items.Count; i++)
                {
                    listItem = cblENVIRONMENTS.Items[i];
                    if ((dr["ENVIRONMENTS"] + string.Empty).Contains(listItem.Value))
                    {
                        listItem.Selected = true;
                    }
                }
                for (int i = 0; i < cblCOMPANYPOLICY.Items.Count; i++)
                {
                    listItem = cblCOMPANYPOLICY.Items[i];
                    if ((dr["COMPANYPOLICY"] + string.Empty).Contains(listItem.Value))
                    {
                        listItem.Selected = true;
                    }
                }
                txtPERSONALFACTORS.Text = dr["PERSONALFACTORS_DETAIL"] + string.Empty;
                txtROUTEHAZARDOUS.Text = dr["ROUTEHAZARDOUS_DETAIL"] + string.Empty;
                txtTRUCK.Text = dr["TRUCK_DETAIL"] + string.Empty;
                txtENVIRONMENTS.Text = dr["ENVIRONMENT_DETAIL"] + string.Empty;
                txtCOMPANYPOLICY.Text = dr["COMPANYPOLICY_DETAIL"] + string.Empty;
                if (dr["SERIOUS"] + string.Empty == "0")
                {
                    lblType.Text = "เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                    lblType.ForeColor = Color.Red;
                }
                else if (dr["SERIOUS"] + string.Empty == "1")
                {
                    lblType.Text = "ไม่เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                    lblType.ForeColor = Color.Green;
                }
            }
            #endregion
        }

    }
    #endregion

    #region SetEnabledControlByID
    private void SetEnabledControlByID(string ID)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "SetEnabledControlByID('" + ID + "');", true);
    }
    #endregion

    #region dgvUploadFile
    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void DownloadFile(string Path, string FileName)
    {
        try
        {
            //if (File.Exists(Path + "\\" + FileName))
            //{
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(Path + "\\" + FileName);
            Response.End();
            //}
            //else
            //{
            //    alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
            //}

        }
        catch (Exception ex)
        {
            alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
        }

    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");

                DataRowView dr = (DataRowView)e.Row.DataItem;
                TextBox txt = (TextBox)e.Row.FindControl("txtRemark");
                if (txt != null)
                {
                    txt.Text = dr["REMARK"] + string.Empty;
                }
                RadioButtonList rbl = (RadioButtonList)e.Row.FindControl("rblStatus");
                if (rbl != null)
                {
                    rbl.SelectedValue = dr["STATUS"] + string.Empty;
                    if (!string.IsNullOrEmpty(rbl.SelectedValue))
                    {
                        rbl.Enabled = false;
                        txt.Enabled = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    #endregion

    #region btn
    #region mpEdit_ClickOK
    protected void mpEdit_ClickOK(object sender, EventArgs e)
    {
        try
        {
            bool IsApproves = true;
            string Remark = string.Empty;
            string accID = ApproveData(7, 0, ref IsApproves, ref Remark);
            string mess = "ส่งแก้ไขเอกสารสำเร็จ";
            if (!string.IsNullOrEmpty(accID))
            {
                if (SendEmail(ConfigValue.EmailAccident5, accID, IsApproves, Remark))
                {
                    mess += "<br/>ส่ง E-mail สำเร็จ";
                }
                else
                {
                    mess += "<br/><span style=\"color:Red;\">ส่ง E-mail ไม่สำเร็จ</span>";
                }
                //send email
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess(mess, "AccidentTab3.aspx?str=" + encryptedValue);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #region mpSave_ClickOK
    protected void mpSave_ClickOK(object sender, EventArgs e)
    {
        try
        {

            if (rblSERIOUS.SelectedIndex < 0 || rblEVALUATION.SelectedIndex < 0)
            {
                alertFail("กรุณาเลือก การประเมินความผิดของผู้ขนส่งและการประเมินความผิดของผู้ขนส่ง !!!");
                return;
            }
            string accID = SaveData();
            string mess = "บันทึกสำเร็จ";

            if (!string.IsNullOrEmpty(accID))
            {
                //if (SendEmail(ConfigValue.EmailAccident6, accID))
                //{
                //    mess += "<br/>ส่ง E-mail สำเร็จ";
                //}
                //else
                //{
                //    mess += "<br/><span style=\"color:Red;\">ส่ง E-mail ไม่สำเร็จ</span>";
                //}
                //send email
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess(mess, "AccidentTab3.aspx?str=" + encryptedValue);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region mpSaveSend_ClickOK
    protected void mpSaveSend_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string mess = Validate();
            if (!string.IsNullOrEmpty(mess))
            {
                alertFail(RemoveSpecialCharacters(mess));
            }
            else
            {
                bool IsApproves = true;
                string Remark = string.Empty;
                string accID = ApproveData(8, 1, ref IsApproves, ref Remark);
                mess += "อนุมัติสำเร็จ";
                if (!string.IsNullOrEmpty(accID))
                {
                    if (SendEmail(ConfigValue.EmailAccident5, accID, IsApproves, Remark))
                    {
                        mess += "<br/>ส่ง E-mail สำเร็จ";
                    }
                    else
                    {
                        mess += "<br/><span style=\"color:Red;\">ส่ง E-mail ไม่สำเร็จ</span>";
                    }
                    //send email
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    alertSuccess(mess, "AccidentTab3.aspx?str=" + encryptedValue);
                }
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #endregion

    #region ApproveData
    private string ApproveData(int CACTIVE, int IsApprove, ref bool IsApproves, ref string Remark)
    {
        IsApproves = true;
        DataSet dsFile = new DataSet("DS");
        dtUpload.Clear();
        for (int i = 0; i < dgvUploadFile.Rows.Count; i++)
        {
            GridViewRow gvr = dgvUploadFile.Rows[i];
            RadioButtonList rblStatus = (RadioButtonList)gvr.FindControl("rblStatus");
            HiddenField hidSTATUS = (HiddenField)gvr.FindControl("hidSTATUS");
            if (rblStatus.SelectedValue == "1" && string.IsNullOrEmpty(hidSTATUS.Value))
            {
                IsApproves = false;
            }
        }
        for (int i = 0; i < dgvUploadFile.Rows.Count; i++)
        {
            GridViewRow gvr = dgvUploadFile.Rows[i];
            dr = dtUpload.NewRow();
            RadioButtonList rblStatus = (RadioButtonList)gvr.FindControl("rblStatus");
            HiddenField hidSTATUS = (HiddenField)gvr.FindControl("hidSTATUS");
            if (rblStatus != null)
            {
                dr["STATUS"] = rblStatus.SelectedValue;
            }
            HiddenField hidUPLOAD_ID = (HiddenField)gvr.FindControl("hidUPLOAD_ID");
            if (hidUPLOAD_ID != null)
            {
                dr["UPLOAD_ID"] = hidUPLOAD_ID.Value;
            }
            if (!IsApproves || (IsApproves && rblStatus.SelectedValue == "0"))
            {
                Remark += "<br/>";
            }

            HiddenField hidUPLOAD_NAME = (HiddenField)gvr.FindControl("hidUPLOAD_NAME");
            if (hidUPLOAD_NAME != null)
            {
                dr["UPLOAD_NAME"] = hidUPLOAD_NAME.Value;
                if (!IsApproves || (IsApproves && rblStatus.SelectedValue == "0"))
                    Remark += " <span style='font-weight: bold;'>" + hidUPLOAD_NAME.Value + "</span>";
            }
            HiddenField hidFILENAME_SYSTEM = (HiddenField)gvr.FindControl("hidFILENAME_SYSTEM");
            if (hidFILENAME_SYSTEM != null)
            {
                dr["FILENAME_SYSTEM"] = hidFILENAME_SYSTEM.Value;
                if (!IsApproves || (IsApproves && rblStatus.SelectedValue == "0"))
                    Remark += "<span style='font-weight: bold;'> : </span> " + hidFILENAME_SYSTEM.Value;
            }
            HiddenField hidFILENAME_USER = (HiddenField)gvr.FindControl("hidFILENAME_USER");
            if (hidFILENAME_USER != null)
            {
                dr["FILENAME_USER"] = hidFILENAME_USER.Value;
            }
            HiddenField hidFULLPATH = (HiddenField)gvr.FindControl("hidFULLPATH");
            if (hidFULLPATH != null)
            {
                dr["FULLPATH"] = hidFULLPATH.Value;
            }
            if (rblStatus != null)
            {
                if (!IsApproves || (IsApproves && rblStatus.SelectedValue == "0"))
                    Remark += "<span style='font-weight: bold;'> : </span> " + (rblStatus.SelectedItem != null
                        ? rblStatus.SelectedItem.Text.Trim() == "ผ่าน" ? "<span style='color:green;'>" + rblStatus.SelectedItem.Text.Trim() + "</span>" : "<span style='color:red;'>ไม่ผ่าน</span>"
                        : "<span style='color:red;'>ไม่ผ่าน</span>") + " ";
            }
            TextBox txtRemark = (TextBox)gvr.FindControl("txtRemark");
            if (txtRemark != null)
            {
                dr["REMARK"] = txtRemark.Text.Trim();
                if (!IsApproves || (IsApproves && rblStatus.SelectedValue == "0"))
                    Remark += " หมายเหตุ " + (!string.IsNullOrEmpty(txtRemark.Text.Trim()) ? txtRemark.Text.Trim() : "-");
            }
            dtUpload.Rows.Add(dr);

        }
        dtUpload.TableName = "DT";
        dsFile.Tables.Add(dtUpload.Copy());
        dt = AccidentBLL.Instance.ApproveTab3(hidID.Value, CACTIVE, Session["UserID"] + string.Empty, IsApprove, dsFile);
        return dt.Rows.Count > 0 ? dt.Rows[0][0] + string.Empty : "";
    }
    #endregion

    #region SaveData
    private string SaveData()
    {
        DataSet dsAPPROVEEVENT = new DataSet("DS");
        dt = new DataTable("DT");
        dt.Columns.Add("EVALUATION", typeof(string));
        dt.Columns.Add("EVALUATION_DETAIL", typeof(string));
        dt.Columns.Add("SERIOUS", typeof(string));
        dt.Columns.Add("SERIOUS_DETAIL", typeof(string));
        dt.Columns.Add("LIFE", typeof(string));
        dt.Columns.Add("PROPERTY", typeof(string));
        dt.Columns.Add("ENVIRONMENT", typeof(string));
        dt.Columns.Add("CORPORATE", typeof(string));
        dt.Columns.Add("OIL", typeof(string));
        dt.Columns.Add("PERSONALFACTORS", typeof(string));
        dt.Columns.Add("ROUTEHAZARDOUS", typeof(string));
        dt.Columns.Add("TRUCK", typeof(string));
        dt.Columns.Add("ENVIRONMENTS", typeof(string));
        dt.Columns.Add("COMPANYPOLICY", typeof(string));
        dt.Columns.Add("PERSONALFACTORS_DETAIL", typeof(string));
        dt.Columns.Add("ROUTEHAZARDOUS_DETAIL", typeof(string));
        dt.Columns.Add("TRUCK_DETAIL", typeof(string));
        dt.Columns.Add("ENVIRONMENT_DETAIL", typeof(string));
        dt.Columns.Add("COMPANYPOLICY_DETAIL", typeof(string));

        ListItem listItem = null;
        string PERSONALFACTORS = string.Empty, ROUTEHAZARDOUS = string.Empty, TRUCK = string.Empty, ENVIRONMENTS = string.Empty, COMPANYPOLICY = string.Empty;
        for (int i = 0; i < cblPERSONALFACTORS.Items.Count; i++)
        {
            listItem = cblPERSONALFACTORS.Items[i];
            if (listItem.Selected)
            {
                PERSONALFACTORS += listItem.Value;
            }
        }
        for (int i = 0; i < cblROUTEHAZARDOUS.Items.Count; i++)
        {
            listItem = cblROUTEHAZARDOUS.Items[i];
            if (listItem.Selected)
            {
                ROUTEHAZARDOUS += listItem.Value;
            }
        }
        for (int i = 0; i < cblTRUCK.Items.Count; i++)
        {
            listItem = cblTRUCK.Items[i];
            if (listItem.Selected)
            {
                TRUCK += listItem.Value;
            }
        }
        for (int i = 0; i < cblENVIRONMENTS.Items.Count; i++)
        {
            listItem = cblENVIRONMENTS.Items[i];
            if (listItem.Selected)
            {
                ENVIRONMENTS += listItem.Value;
            }
        }
        for (int i = 0; i < cblCOMPANYPOLICY.Items.Count; i++)
        {
            listItem = cblCOMPANYPOLICY.Items[i];
            if (listItem.Selected)
            {
                COMPANYPOLICY += listItem.Value;
            }
        }
        dt.Rows.Add(rblEVALUATION.SelectedValue, txtEVALUATION_DETAIL.Text.Trim(), rblSERIOUS.SelectedValue, txtSERIOUS_DETAIL.Text.Trim(), rblLIFE.SelectedValue, rblPROPERTY.SelectedValue, rblENVIRONMENT.SelectedValue, rblCORPORATE.SelectedValue, txtOil.Text.Trim(), PERSONALFACTORS, ROUTEHAZARDOUS, TRUCK, ENVIRONMENTS, COMPANYPOLICY, txtPERSONALFACTORS.Text.Trim(), txtROUTEHAZARDOUS.Text.Trim(), txtTRUCK.Text.Trim(), txtENVIRONMENTS.Text.Trim(), txtCOMPANYPOLICY.Text.Trim());
        dsAPPROVEEVENT.Tables.Add(dt.Copy());
        bool Serious = false;
        if (rblSERIOUS.SelectedValue == "1" && rblEVALUATION.SelectedValue == "0")
        {
            Serious = true;
        }
        #region SAP

        try
        {
            string Detail = string.Empty;
            Detail += "รถขนส่งเกิดอุบัติเหตุ หมายเลขเอกสาร " + txtAccidentID.Text.Trim();
            if (Serious)
            {
                Detail += "<br/>สถานะการทำงาน : อนุญาต";
            }
            else
            {
                Detail += "<br/>สถานะการทำงาน : ระงับการทำงาน";

            }

            Detail += "<br/>หมายเหตุ : อยู่ระหว่างการตรวจสอบรถขนส่งเกิดอุบัติเหตุ(Tab3)";
            VendorBLL.Instance.LogInsert(Detail, hidPERS_CODE.Value, null, "0", "0", int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, 0);
            DataTable dtDriverDetail;

            if (!string.Equals(hidSEMPLOYEEID.Value, string.Empty))
            {
                dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(hidSEMPLOYEEID.Value);

                // ComplainBLL.Instance.UpdateDriverStatusBLL(hidPERS_CODE.Value, "2", "2");

                SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                {
                    DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                    PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                    LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                    LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                    LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                    Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                    Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                    FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                    LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                    CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                    DRV_STATUS = Serious ? ConfigValue.DriverEnable : ConfigValue.DriverDisable
                };

                string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                string resultCheck = result.Substring(0, 1);

                if (string.Equals(resultCheck, "N"))
                    throw new Exception("ไม่สามารถ ระงับ พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
        #endregion

        dt = AccidentBLL.Instance.SaveTab3(hidID.Value, Session["UserID"] + string.Empty, dsAPPROVEEVENT, Serious, hidSEMPLOYEEID.Value);

        return dt.Rows.Count > 0 ? dt.Rows[0][0] + string.Empty : "";
    }
    #endregion

    #region SendEmail
    private bool SendEmail(int TemplateID, string accID, bool IsApproves, string Remark)
    {
        try
        {
            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            dt = AccidentBLL.Instance.AccidentTab1Select(accID);

            if (dtTemplate.Rows.Count > 0 && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                string EmailList = string.Empty, IsApprove = IsApproves ? "<span style='color:green;'>" + "อนุมัติ</span>" : "<span style='color:red;'>แก้ไขเอกสาร</span>";
                if (TemplateID == ConfigValue.EmailAccident5)
                {
                    #region EmailAccident5

                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, true, true);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}
                    //if (IsApproves)
                    //{
                    //    //                        EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com
                    //    //,TMS_yutasak.c@pttor.com,TMS_kittipong.l@pttor.com
                    //    //,TMS_suphakit.k@pttor.com,TMS_apipat.k@pttor.com
                    //    //,TMS_nut.t@pttor.com
                    //    //,TMS_terapat.p@pttor.com
                    //    //,TMS_sarun.c@pttor.com";

                    //    //EmailList += @"suphakit.k@pttor.com , nut.t@pttor.com,sake.k@pttor.com,terapat.p@pttor.com , wasupol.p@pttor.com, wasupol.p@pttor.com";
                    //}
                    //else
                    //{
                    //    //                        EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com
                    //    //,TMS_yutasak.c@pttor.com,TMS_kittipong.l@pttor.com
                    //    //,TMS_suphakit.k@pttor.com,TMS_apipat.k@pttor.com
                    //    //,TMS_nut.t@pttor.com";

                    //    //EmailList += @"terapat.p@pttor.com , wasupol.p@pttor.com,";
                    //}

                    //EmailList += "raviwan.t@pttor.com,YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,chutapha.c@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com";

                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                    Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                    Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                    Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                    Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                    Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                    Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                    Body = Body.Replace("{REPORT_CHECK}", IsApprove);
                    Body = Body.Replace("{REMARK}", Remark.Substring(5));
                    Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                    Body = Body.Replace("{USER_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);
                    if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
                    {
                        string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
                        if (AccidentDate.Any())
                        {
                            Body = Body.Replace("{DATE}", AccidentDate[0]);
                            Body = Body.Replace("{TIME}", AccidentDate[1]);
                        }

                    }
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab3_Vendor.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident5", ColumnEmailName);
                    #endregion

                }

                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region Validate
    private string Validate()
    {
        string mess = string.Empty;
        try
        {

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dgvUploadFile.Rows.Count; i++)
            {
                GridViewRow gvr = dgvUploadFile.Rows[i];
                RadioButtonList rblStatus = (RadioButtonList)gvr.FindControl("rblStatus");
                HiddenField hidUPLOAD_ID = (HiddenField)gvr.FindControl("hidUPLOAD_ID");
                HiddenField hidSTATUS = (HiddenField)gvr.FindControl("hidSTATUS");
                if (rblStatus != null)
                {
                    if (rblStatus.SelectedValue != "0" && string.IsNullOrEmpty(hidSTATUS.Value))
                    {
                        sb.Append("เอกสารทุกประเภทต้องผ่านทั้งหมดถึงจะทำการอนุมัติได้ !!!");
                    }
                }
            }
            if (!string.Equals(sb.ToString(), string.Empty))
                mess += sb.ToString();
            return mess;
        }
        catch (Exception ex)
        {
            return RemoveSpecialCharacters(ex.Message);
            //throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    #endregion
}