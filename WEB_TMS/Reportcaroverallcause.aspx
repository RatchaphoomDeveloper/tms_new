﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="Reportcaroverallcause.aspx.cs" Inherits="Reportcaroverallcause" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="70%" align="right">
                                                    <dx:ASPxComboBox ID="cboVendor" runat="server" Width="80%" ClientInstanceName="cboVendor"
                                                        TextFormatString="{0} - {1}" CallbackPageSize="30" EnableCallbackMode="True"
                                                        OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" ValueField="SVENDORID" AutoPostBack="false">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                    </asp:SqlDataSource>
                                                </td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="2%">
                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text=" - " CssClass="dxeLineBreakFix">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="10%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="87%" height="35">
                                        <dx:ASPxLabel runat="server" ID="lblsHead" Text="รายงานรถไม่ผ่าน รวมสาเหตุ(ภาพรวม) แยกบริษัท"
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblsTail" Text=" ระหว่างวันที่ 14/12/2556 - 15/3/2557 "
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td width="13%">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                            <tr>
                                                <td width="13%"><img src="images/ic_pdf2.gif" width="16" height="16" alt="" /></td>
                                                <td width="37%">Print</td>
                                                <td width="13%"><img src="images/ic_ms_excel.gif" width="16" height="16" alt="" />
                                                </td>
                                                <td width="37%">Export</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="100%" AutoGenerateColumns="false">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ที่" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <%#Container.ItemIndex + 1 %>.
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รายละเอียด" Width="30%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblDescoption">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ม.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lbljan">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ก.พ." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblFeb">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="มี.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblMarch">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="เม.ษ." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblApril">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="พ.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblMay">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="มิ.ย." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblJune">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ก.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblJuly">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ส.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblAug">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ก.ย." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblSep">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ต.ค" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblOct">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="พ.ย." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblSep">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ธ.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblDe">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รวม" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblSum">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
