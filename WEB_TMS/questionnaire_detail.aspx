﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="questionnaire_detail.aspx.cs" Inherits="questionnaire_detail" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            width: 50%;
            height: 31px;
        }
        .padding
        {
            padding:0px 0px 0px 0px;	
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}" />
        <PanelCollection>
            <dx:PanelContent>
               <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" style="width: 20%">
                            <span style="color: Red">กรุณากรอกข้อมูลให้ครบถ้วน</span>
                        </td>
                        <td style="width: 35%">
                            &nbsp;
                        </td>
                        <td bgcolor="#FFFFFF" style="width: 15%">
                        </td>
                        <td style="width: 30%">
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            ครั้งที่ตรวจประเมิน</td>
                        <td align="left" class="style27">
                            <dx:ASPxLabel ID="lblNNO" runat="server"></dx:ASPxLabel></td>
                        <td>
                            สถานประกอบการ
                        </td>
                        <td>
                           <dx:ASPxLabel ID="lblVendor" runat="server"></dx:ASPxLabel></td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td bgcolor="#FFFFFF" class="style24">
                            วันที่ตรวจประเมิน
                        </td>
                        <td align="left">
                           <dx:ASPxLabel ID="lblDate" runat="server"></dx:ASPxLabel></td>
                        <td>
                            สถานที่ตรวจประเมิน
                        </td>
                        <td>
                          <dx:ASPxLabel ID="lblAddress" runat="server"></dx:ASPxLabel></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            รายชื่อคณะผู้ตรวจ
                        </td>
                        <td>
                           <asp:Literal ID="ltrChackName" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            ผลการประเมินแบบสอบถาม
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" DataSourceID="sdsMaster" SettingsBehavior-AllowSort="false"
                                KeyFieldName="NGROUPID" Width="100%" >
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="NGROUPID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="หัวข้อแบบประเมิน" FieldName="SGROUPNAME" VisibleIndex="0"
                                        Width="50%" />
                                    <dx:GridViewDataColumn Caption="ระดับคะแนน" VisibleIndex="1" Width="20%" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="เหตุผลเพิ่มเติม" VisibleIndex="2" Width="30%" />
                                </Columns>

<SettingsBehavior AllowSort="False"></SettingsBehavior>

                                <SettingsDetail ShowDetailButtons="False" ShowDetailRow="True" />
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="detailGrid" runat="server" DataSourceID="sdsDetail" KeyFieldName="NTYPEVISITFORMID"
                                            Width="100%" OnBeforePerformDataSelect="detailGrid_DataSelect" Settings-ShowColumnHeaders="false"
                                            OnHtmlDataCellPrepared="detailGrid_HtmlDataCellPrepared" SettingsBehavior-AllowSort="false" >
                                            <Settings GridLines="None" ></Settings>
                                            <Columns>
                                                <dx:GridViewDataColumn FieldName="SVISITFORMNAME" VisibleIndex="2" Width="51%" />
                                                <dx:GridViewDataColumn VisibleIndex="3" Width="21%" CellStyle-HorizontalAlign="Center"
                                                     Caption="#">
                                                    <DataItemTemplate>
                                                        <dx:ASPxRadioButtonList ID="rblWEIGHT" runat="server" ClientInstanceName="rblWEIGHT" ReadOnly="true"
                                                            TextField="STYPEVISITLISTNAME" ValueField="NTYPEVISITLISTSCORE" DataSourceID="sdsRedio"
                                                            SkinID="rblStatus">
                                                        </dx:ASPxRadioButtonList>
                                                        <dx:ASPxTextBox ID="txtWEIGHT" runat="server" ClientInstanceName="txtWEIGHT" Text='<%#Eval("NWEIGHT")%>'
                                                            ClientVisible="false" ClientEnabled="false">
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtOldSum" runat="server" ClientInstanceName="txtOldSum" Text="0"
                                                            ClientVisible="false" ClientEnabled="false">
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn VisibleIndex="4" Width="28%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox ID="txtRemark" runat="server" Width="100%" Border-BorderColor="White" ClientEnabled="false" ForeColor="Black">
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NWEIGHT" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NVISITFORMID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NGROUPID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NTYPEVISITFORMID" Visible="false">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsBehavior AllowSort="False" />
                                            <SettingsPager Visible="False">
                                            </SettingsPager>
                                            <Settings ShowFooter='false'  />
                                            <Border BorderStyle="None" />
                                        </dx:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <Styles Cell-BackColor="#ccffff">
                                 <Cell BackColor="#CCFFFF"></Cell>
                                </Styles>
                                <Settings GridLines="None" ></Settings>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sdsMaster" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT GV.NGROUPID ,GV.NNO || '. ' || GV.SGROUPNAME AS SGROUPNAME  FROM TTYPEOFVISITFORM tv INNER JOIN TGROUPOFVISITFORM gv ON TV.NTYPEVISITFORMID = GV.NTYPEVISITFORMID where TV.NTYPEVISITFORMID = :NTYPEVISITFORMID ORDER BY GV.NNO">
                                <SelectParameters>
                                 <asp:SessionParameter SessionField="sNTYPEVISITFORMID" Name="NTYPEVISITFORMID" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsDetail" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT NVISITFORMID,'&nbsp;&nbsp;&nbsp;' || rownum || ') ' || SVISITFORMNAME AS SVISITFORMNAME, nvl(NWEIGHT,0) AS NWEIGHT, NVISITFORMID ,NGROUPID,NTYPEVISITFORMID
 FROM TVISITFORM WHERE (NGROUPID = :oNGROUPID) AND (CACTIVE = '1')">
                                <SelectParameters>
                                    <asp:SessionParameter Name="oNGROUPID" SessionField="ONGROUPID" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsRedio" runat="server" EnableCaching="true" CacheKeyDependency="chkRedio"
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT TVL.NTYPEVISITLISTSCORE , TVL.STYPEVISITLISTNAME  FROM TTYPEOFVISITFORM tv INNER JOIN TTYPEOFVISITFORMLIST tvl ON TV.NTYPEVISITFORMID = TVL.NTYPEVISITFORMID WHERE TV.NTYPEVISITFORMID = :NTYPEVISITFORMID ORDER BY TVL.NTYPEVISITLISTSCORE">
                                 <SelectParameters>
                                 <asp:SessionParameter SessionField="sNTYPEVISITFORMID" Name="NTYPEVISITFORMID" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            รายละเอียดเพิ่มเติม
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                          <asp:Literal ID="ltrDetail" runat="server" ></asp:Literal></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            <table>
                                <tr>
                                    <td>
                                        คะแนนรวม
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtTotal" ClientInstanceName="txtTotal" ClientEnabled="false"
                                            Border-BorderColor="White" runat="server" Width="80px" Text="0" CssClass="dxeLineBreakFix" ForeColor="Black"
                                            RightToLeft="True">
                                            <Border BorderColor="White"></Border>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        คะแนน
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13">
                        </td>
                        <td colspan="2" class="style13">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'questionnaire.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
