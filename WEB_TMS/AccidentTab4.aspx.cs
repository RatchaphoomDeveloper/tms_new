﻿using EmailHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;

public partial class AccidentTab4 : PageBase
{
    private bool IsSpecialDoc
    {
        get
        {
            if ((bool)ViewState["IsSpecialDoc"] != null)
                return (bool)ViewState["IsSpecialDoc"];
            else
                return false;
        }
        set
        {
            ViewState["IsSpecialDoc"] = value;
        }
    }

    DataTable dt;
    DataSet ds;
    DataRow dr;
    decimal sumAmount = 0, Amount = 0;
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtScore
    {
        get
        {
            if ((DataTable)ViewState["dtScore"] != null)
                return (DataTable)ViewState["dtScore"];
            else
                return null;
        }
        set
        {
            ViewState["dtScore"] = value;
        }
    }
    private DataTable dtScoreList
    {
        get
        {
            if ((DataTable)ViewState["dtScoreList"] != null)
                return (DataTable)ViewState["dtScoreList"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreList"] = value;
        }
    }
    private DataTable dtScoreListCar
    {
        get
        {
            if ((DataTable)ViewState["dtScoreListCar"] != null)
                return (DataTable)ViewState["dtScoreListCar"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreListCar"] = value;
        }
    }
    private DataTable dtCarDistinct
    {
        get
        {
            if ((DataTable)ViewState["dtCarDistinct"] != null)
                return (DataTable)ViewState["dtCarDistinct"];
            else
                return null;
        }
        set
        {
            ViewState["dtCarDistinct"] = value;
        }
    }
    private DataTable dtUploadTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTab3"] != null)
                return (DataTable)ViewState["dtUploadTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTab3"] = value;
        }
    }
    private DataTable dtUploadTypeCost
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeCost"] != null)
                return (DataTable)ViewState["dtUploadTypeCost"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeCost"] = value;
        }
    }
    private DataTable dtUploadTypeTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTab3"] != null)
                return (DataTable)ViewState["dtUploadTypeTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTab3"] = value;
        }
    }
    private DataTable dtUploadCost
    {
        get
        {
            if ((DataTable)ViewState["dtUploadCost"] != null)
                return (DataTable)ViewState["dtUploadCost"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadCost"] = value;
        }
    }
    private DataTable dtDup
    {
        get
        {
            if ((DataTable)ViewState["dtDup"] != null)
                return (DataTable)ViewState["dtDup"];
            else
                return null;
        }
        set
        {
            ViewState["dtDup"] = value;
        }
    }
    private DataTable dtCusScoreType
    {
        get
        {
            if ((DataTable)ViewState["dtCusScoreType"] != null)
                return (DataTable)ViewState["dtCusScoreType"];
            else
                return null;
        }
        set
        {
            ViewState["dtCusScoreType"] = value;
        }
    }
    private int IndexEdit
    {
        get
        {
            if ((int)ViewState["IndexEdit"] != null)
                return (int)ViewState["IndexEdit"];
            else
                return -1;
        }
        set
        {
            ViewState["IndexEdit"] = value;
        }
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hidCGROUP.Value = Session["CGROUP"] + string.Empty;
            //SetDrowDownList();

            if (Request.QueryString["str"] != null && !string.IsNullOrEmpty(Request.QueryString["str"]))
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SetData(strQuery);
                liTab1.Visible = true;
                GeneralTab1.HRef = "AccidentTab1.aspx?str=" + str;
                liTab2.Visible = true;
                GeneralTab2.HRef = "AccidentTab2.aspx?str=" + str;
                liTab3.Visible = true;
                if (Session["CGROUP"] + string.Empty == ConfigValue.UserGroup1.ToString())
                {
                    GeneralTab3.HRef = "AccidentTab3_Vendor.aspx?str=" + str;
                    this.ScreenForVendor();

                }
                else
                {
                    GeneralTab3.HRef = "AccidentTab3.aspx?str=" + str;
                    if (!string.IsNullOrEmpty(hidCactive.Value) && int.Parse(hidCactive.Value) >= 10 && int.Parse(hidCactive.Value) <= 15)
                        cmdShowAppeal.Visible = true;
                }
                if (hidCactive.Value == "8")
                {
                    cmdSaveIsCost.Visible = false;
                }
                else if (!string.IsNullOrEmpty(hidCactive.Value) && int.Parse(hidCactive.Value) >= 9 && int.Parse(hidCactive.Value) <= 15)
                {
                    cmdSaveTab3.Visible = false;
                    cmdSaveTab3Draft.Visible = false;
                    cmdChangeStatus.Enabled = false;

                    cmdShowAppeal.Visible = true;
                    //chkBlacklistFinal.Visible = true;
                    lblAppealPoint.Visible = true;
                    txtAppealPoint.Visible = true;
                    lblAppealCost.Visible = true;
                    txtAppealCost.Visible = true;
                    lblAppealCostOther.Visible = true;
                    txtAppealCostOther.Visible = true;
                    lblAppealDisable.Visible = true;
                    txtAppealDisable.Visible = true;
                    lblAppealOilLose.Visible = true;
                    txtAppealOilLose.Visible = true;
                }
            }

            IsSpecialDoc = false;
            this.CheckSpecialDocument();
        }
        this.CheckPostBack();
        SetEnabledControlByID(hidCactive.Value);
    }

    private void CheckSpecialDocument()
    {
        try
        {
            string sqlCon = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
            DataTable dt = CommonFunction.Get_Data(sqlCon, "SELECT DOC_ID FROM T_DOC_SPECIAL");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (string.Equals(txtAccidentID.Text.Trim(), dt.Rows[i]["DOC_ID"].ToString()))
                {
                    cmdSaveTab3.Visible = true;
                    cmdSaveTab3.Enabled = true;
                    IsSpecialDoc = true;
                    break;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    #endregion

    #region DrowdownList
    private void SetDrowDownList()
    {
        DataTable dtScore = ComplainBLL.Instance.ScoreSelectBLL();
        DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
    }
    #endregion

    #region SetData
    private void SetData(string ACCIDENT_ID)
    {
        ds = AccidentBLL.Instance.AccidentTab4Select(ACCIDENT_ID);
        if (ds.Tables.Count > 0)
        {
            #region จาก Tab1
            dt = ds.Tables["ACCIDENT"];
            if (dt != null && dt.Rows.Count > 0)
            {

                dr = dt.Rows[0];
                DateTime dttACCIDENT = new DateTime(), dtt = new DateTime();
                TimeSpan ts;
                txtAccidentID.Text = dr["ACCIDENT_ID"] + string.Empty;
                if (!dr.IsNull("ACCIDENT_DATE"))
                {
                    dttACCIDENT = DateTime.Parse(dr["ACCIDENT_DATE"] + string.Empty);
                    txtAccidentDate.Text = dttACCIDENT.ToString(DateTimeFormat);
                }
                if (!dr.IsNull("SYS_TIME"))
                {
                    dtt = DateTime.Parse(dr["SYS_TIME"] + string.Empty);
                    txtAccidentDateSystem.Text = dtt.ToString(DateTimeFormat);
                    ts = (dtt - dttACCIDENT);
                    lblAccidentDateSystem.Text = ts.Days + "-" + ts.Hours.ToString().PadLeft(2, '0') + "-" + ts.Minutes.ToString().PadLeft(2, '0');
                }

                rblREPORTER.SelectedValue = dr["REPORTER"] + string.Empty;
                if (!dr.IsNull("REPORT_PTT"))
                {
                    dtt = DateTime.Parse(dr["REPORT_PTT"] + string.Empty);
                    txtAccidentDatePtt.Text = dtt.ToString(DateTimeFormat);
                    ts = (dtt - dttACCIDENT);
                    lblAccidentDatePtt.Text = ts.Days + "-" + ts.Hours.ToString().PadLeft(2, '0') + "-" + ts.Minutes.ToString().PadLeft(2, '0');
                }

                txtAccidentName.Text = dr["INFORMER_NAME"] + string.Empty;
                if (!dr.IsNull("TIMECREATE"))
                {
                    dtt = DateTime.Parse(dr["TIMECREATE"] + string.Empty);
                    txtDatePrimary.Text = dtt.ToString(DateTimeFormat);
                    ts = (dtt - dttACCIDENT);
                    lblDatePrimary.Text = ts.Days + "-" + ts.Hours.ToString().PadLeft(2, '0') + "-" + ts.Minutes.ToString().PadLeft(2, '0');
                }
                if (!dr.IsNull("DAMAGECREATE"))
                {
                    dtt = DateTime.Parse(dr["DAMAGECREATE"] + string.Empty);
                    txtDateAnalysis.Text = dtt.ToString(DateTimeFormat);
                    ts = (dtt - dttACCIDENT);
                    lblDateAnalysis.Text = ts.Days + "-" + ts.Hours.ToString().PadLeft(2, '0') + "-" + ts.Minutes.ToString().PadLeft(2, '0');
                }
                hidID.Value = txtAccidentID.Text;
                hidCactive.Value = dr["CACTIVE"] + string.Empty;
                if (!dr.IsNull("APPROVE_DATE"))
                {
                    dtt = DateTime.Parse(dr["APPROVE_DATE"] + string.Empty);
                    txtApproveDate.Text = dtt.ToString(DateTimeFormat);
                    ts = (dtt - dttACCIDENT);
                    lblApproveDate.Text = ts.Days + "-" + ts.Hours.ToString().PadLeft(2, '0') + "-" + ts.Minutes.ToString().PadLeft(2, '0');
                }
                if (dr["SERIOUS"] + string.Empty == "0")
                {
                    lblType.Text = "เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                    lblType.ForeColor = Color.Red;
                }
                else if (dr["SERIOUS"] + string.Empty == "1")
                {
                    lblType.Text = "ไม่เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                    lblType.ForeColor = Color.Green;
                }
                hidPERS_CODE.Value = dr["PERS_CODE"] + string.Empty;
                hidSVENDORID.Value = dr["SVENDORID"] + string.Empty;
                hidSEMPLOYEEID.Value = dr["SEMPLOYEEID"] + string.Empty;
                hidSEMPLOYEEID2.Value = dr["SEMPLOYEEID2"] + string.Empty;
                hidPERS_CODE2.Value = dr["PERS_CODE2"] + string.Empty;
                hidSEMPLOYEEID3.Value = dr["SEMPLOYEEID3"] + string.Empty;
                hidPERS_CODE3.Value = dr["PERS_CODE3"] + string.Empty;
                hidSEMPLOYEEID4.Value = dr["SEMPLOYEEID4"] + string.Empty;
                hidPERS_CODE4.Value = dr["PERS_CODE4"] + string.Empty;
                if (dr.IsNull("AMOUNT_TOTAL") || dr["AMOUNT_TOTAL"] + string.Empty == "0" || dr["AMOUNT_TOTAL"] + string.Empty == "")
                {
                    txtAmount.ReadOnly = false;

                }
                else
                {
                    txtAmount.ReadOnly = true;
                    txtAmount.Text = dr["AMOUNT_TOTAL"] + string.Empty;
                }

                if (!dr.IsNull("APPROVE_DATE_TAB4"))
                {
                    dtt = DateTime.Parse(dr["APPROVE_DATE_TAB4"] + string.Empty);
                    txtComplainDate.Text = dtt.ToString(DateFormat);
                    txtSecondDate.Text = dtt.AddDays(7).ToString(DateFormat);
                    hidApproveDate.Value = dtt.ToString(DateFormat);
                }

                txtDAMAGE.Text = dr["DAMAGE"] + string.Empty;
                txtOil.Text = dr["OIL"] + string.Empty;
                txtLIFE.Text = dr["LIFE"] + string.Empty == "0" ? "บาดเจ็บเล็กน้อย"
                    : dr["LIFE"] + string.Empty == "1" ? "รักษาทางการแพทย์ แต่ไม่ต้องหยุดงาน"
                    : dr["LIFE"] + string.Empty == "2" ? "หยุดงาน หรือ ต้องเปลี่ยนงาน"
                    : dr["LIFE"] + string.Empty == "3" ? "ทุพลภาพหรือเสียชีวิต"
                    : dr["LIFE"] + string.Empty == "4" ? "ไม่มีผลกระทบ"
                    : "";

                lblLIFE.Text = dr["LIFE"] + string.Empty == "0" ? string.Format(lblLIFE.Text, "2")
                    : dr["LIFE"] + string.Empty == "1" ? string.Format(lblLIFE.Text, "3")
                    : dr["LIFE"] + string.Empty == "2" ? string.Format(lblLIFE.Text, "4")
                    : dr["LIFE"] + string.Empty == "3" ? string.Format(lblLIFE.Text, "5")
                    : dr["LIFE"] + string.Empty == "4" ? string.Format(lblLIFE.Text, "1")
                    : string.Format(lblLIFE.Text, "0");

                txtPROPERTY.Text = dr["PROPERTY"] + string.Empty == "0" ? "เสียหายน้อยมาก (ไม่เกิน 50,000 บาท)"
                    : dr["PROPERTY"] + string.Empty == "1" ? "เสียหายปานกลาง (ไม่เกิน 250,000 บาท)"
                    : dr["PROPERTY"] + string.Empty == "2" ? "เสียหายมาก (ไม่เกิน 1,000,000 บาท)"
                    : dr["PROPERTY"] + string.Empty == "3" ? "เสียหายมาก (มากกว่า 1,000,000 บาท)"
                    : dr["PROPERTY"] + string.Empty == "4" ? "ไม่มีผลกระทบ"
                    : "";
                lblPROPERTY.Text = dr["PROPERTY"] + string.Empty == "0" ? string.Format(lblPROPERTY.Text, "2")
                    : dr["PROPERTY"] + string.Empty == "1" ? string.Format(lblPROPERTY.Text, "3")
                    : dr["PROPERTY"] + string.Empty == "2" ? string.Format(lblPROPERTY.Text, "4")
                    : dr["PROPERTY"] + string.Empty == "3" ? string.Format(lblPROPERTY.Text, "5")
                    : dr["PROPERTY"] + string.Empty == "4" ? string.Format(lblPROPERTY.Text, "1")
                    : string.Format(lblPROPERTY.Text, "0");

                txtENVIRONMENT.Text = dr["ENVIRONMENT"] + string.Empty == "0" ? "มีผลกระทบเล็กน้อย"
                    : dr["ENVIRONMENT"] + string.Empty == "1" ? "มีผลกระทบปานกลาง"
                    : dr["ENVIRONMENT"] + string.Empty == "2" ? "มีผลกระทบรุนแรง"
                    : dr["ENVIRONMENT"] + string.Empty == "3" ? "มีผลกระทบรุนแรงมาก"
                    : dr["ENVIRONMENT"] + string.Empty == "4" ? "ไม่มีผลกระทบ"
                    : "";
                lblENVIRONMENT.Text = dr["ENVIRONMENT"] + string.Empty == "0" ? string.Format(lblENVIRONMENT.Text, "2")
                    : dr["ENVIRONMENT"] + string.Empty == "1" ? string.Format(lblENVIRONMENT.Text, "3")
                    : dr["ENVIRONMENT"] + string.Empty == "2" ? string.Format(lblENVIRONMENT.Text, "4")
                    : dr["ENVIRONMENT"] + string.Empty == "3" ? string.Format(lblENVIRONMENT.Text, "5")
                    : dr["ENVIRONMENT"] + string.Empty == "4" ? string.Format(lblENVIRONMENT.Text, "1")
                    : string.Format(lblENVIRONMENT.Text, "0");

                txtCORPORATE.Text = dr["CORPORATE"] + string.Empty == "0" ? "อาจจะทำให้เกิดผลกระทบ"
                    : dr["CORPORATE"] + string.Empty == "1" ? "มีผลกระทบทางอ้อม"
                    : dr["CORPORATE"] + string.Empty == "2" ? "มีผลกระทบโดยตรง บริหารจัดการได้)"
                    : dr["CORPORATE"] + string.Empty == "3" ? "มีผลกระทบโดยตรง ไม่สามารถบริหารจัดการได้"
                    : dr["CORPORATE"] + string.Empty == "4" ? "ไม่มีผลกระทบ"
                    : "";
                lblCORPORATE.Text = dr["CORPORATE"] + string.Empty == "0" ? string.Format(lblCORPORATE.Text, "2")
                    : dr["CORPORATE"] + string.Empty == "1" ? string.Format(lblCORPORATE.Text, "3")
                    : dr["CORPORATE"] + string.Empty == "2" ? string.Format(lblCORPORATE.Text, "4")
                    : dr["CORPORATE"] + string.Empty == "3" ? string.Format(lblCORPORATE.Text, "5")
                    : dr["CORPORATE"] + string.Empty == "4" ? string.Format(lblCORPORATE.Text, "1")
                    : string.Format(lblCORPORATE.Text, "0");
            }
            #endregion

            #region FINE
            dt = ds.Tables["FINE"];
            sumAmount = dt.AsEnumerable().Sum(it => decimal.Parse(it["AMOUNT"] + string.Empty));
            dr = dt.NewRow();
            dr["NO"] = DBNull.Value;
            dr["FILENAMES"] = "จำนวนเงินที่ชำระแล้ว";
            dr["AMOUNT"] = sumAmount;
            dt.Rows.Add(dr);
            gvFine.DataSource = dt;
            gvFine.DataBind();
            #endregion

            EnableTab3();
        }

    }
    #endregion

    #region SetEnabledControlByID
    private void SetEnabledControlByID(string ID)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "SetEnabledControlByID('" + ID + "');", true);
    }
    #endregion

    #region SendEmail
    private bool SendEmail(int TemplateID, string accID, bool IsApproves, string Remark)
    {
        try
        {
            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            dt = AccidentBLL.Instance.AccidentTab1Select(accID);

            if (dtTemplate.Rows.Count > 0 && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                string EmailList = string.Empty, IsApprove = IsApproves ? "อนุมัติ" : "แก้ไขเอกสาร";
                if (TemplateID == ConfigValue.EmailAccident6)
                {
                    #region EmailAccident6
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(ConfigValue.DeliveryDepartmentCodeAdmin, ConfigValue.DeliveryDepartmentCodeAdmin2, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, true, true
                        );
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}

                    //EmailList += "raviwan.t@pttor.com,YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,kwanploy.p@pttor.com,nattira.a@pttor.com ,teerapat.le@pttor.com,nontivat.i@pttor.com,patkanit.s@pttor.com,witawat.t@pttor.com,kritsada.h@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,kittipong.l@pttor.com,norawit.k@pttor.com,settasak.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,chutapha.c@pttor.com,SUDTADA.S@PTTOR.COM,PRASARN.N@PTTOR.COM";

                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                    Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                    Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                    Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                    Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                    Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                    Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                    Body = Body.Replace("{REPORT_CHECK}", IsApprove);
                    Body = Body.Replace("{REMARK}", "");
                    Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                    Body = Body.Replace("{USER_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);
                    if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
                    {
                        string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
                        if (AccidentDate.Any())
                        {
                            Body = Body.Replace("{DATE}", AccidentDate[0]);
                            Body = Body.Replace("{TIME}", AccidentDate[1]);
                        }

                    }
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab4.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident6", ColumnEmailName);
                    #endregion

                }
                else if (TemplateID == ConfigValue.EmailAccident8)
                {
                    #region EmailAccident8
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, true, true);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}
                    //                    EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com
                    //                    ,TMS_yutasak.c@pttor.com,TMS_kittipong.l@pttor.com
                    //                    ,TMS_suphakit.k@pttor.com,TMS_apipat.k@pttor.com
                    //                    ,TMS_nut.t@pttor.com
                    //                    ,TMS_terapat.p@pttor.com
                    //                    ,TMS_sarun.c@pttor.com";

                    //EmailList += "bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com";

                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                    Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                    Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                    Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                    Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                    Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                    Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                    Body = Body.Replace("{REPORT_CHECK}", IsApprove);
                    Body = Body.Replace("{REMARK}", Remark);
                    Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                    Body = Body.Replace("{USER_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);
                    if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
                    {
                        string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
                        if (AccidentDate.Any())
                        {
                            Body = Body.Replace("{DATE}", AccidentDate[0]);
                            Body = Body.Replace("{TIME}", AccidentDate[1]);
                        }

                    }
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab4.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident8", ColumnEmailName);
                    #endregion

                }
                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region Validate
    private string Validate()
    {
        string mess = string.Empty;
        try
        {

            StringBuilder sb = new StringBuilder();

            if (!string.Equals(sb.ToString(), string.Empty))
                mess += sb.ToString();
            return mess;
        }
        catch (Exception ex)
        {
            return RemoveSpecialCharacters(ex.Message);
            //throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    #endregion

    #region จากข้อร้องเรียน

    private void ScreenForVendor()
    {
        try
        {

            //Tab3
            Row1.Visible = false;
            Row2.Visible = false;
            Row3.Visible = false;
            Row4.Visible = false;
            Row5.Visible = false;
            Row6.Visible = false;
            Row7.Visible = false;
            Row8.Visible = false;
            Row9.Visible = false;
            lblCusScoreType.Visible = false;
            radCusScoreType.Visible = false;
            cmdAddScore.Visible = false;
            cmdSaveTab3.Visible = false;
            cmdSaveTab3Draft.Visible = false;
            cmdChangeStatus.Visible = false;
            divSecond.Visible = true;
            lblRemarkTab3.Visible = false;
            txtRemarkTab3.Visible = false;
            //lblUploadTypeTab3.Visible = false;
            //lblCostOther.Visible = false;
            //txtCostOther.Visible = false;
            //lblOilLose.Visible = false;
            //txtOilLose.Visible = false;
            lblTotalCostAll.Visible = false;
            radCostCheck.Visible = false;
            lblCostCheckDate.Visible = false;
            txtCostCheckDate.Visible = false;
            cmdSaveIsCost.Visible = false;
            cmdShowAppeal.Text = "ยื่นอุทธรณ์";
            cmdShowAppeal.Visible = true;
            dgvScore.Visible = false;
            dgvScoreTotalCar.Visible = false;

            dgvScoreList.Columns[5].Visible = false;


            divCost.Visible = false;

            ImageButton imgButtonUploadTab3;
            for (int i = 0; i < dgvUploadFileTab3.Rows.Count; i++)
            {
                imgButtonUploadTab3 = (ImageButton)dgvUploadFileTab3.Rows[i].FindControl("imgDelete");
                if (imgButtonUploadTab3 != null)
                    imgButtonUploadTab3.Visible = false;
            }

            //lblUploadTypeTab3.Visible = false;
            cboUploadTypeTab3.Visible = false;
            fileUploadTab3.Visible = false;
            cmdUploadTab3.Visible = false;

            //((HtmlAnchor)this.FindControlRecursive(Page, "ProcessTab")).Visible = false;

            //dtUploadTab3 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_SCORE", " AND IS_VENDOR_DOWNLOAD = '1'");
            //GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckPostBack()
    {
        try
        {
            var ctrlName = Request.Params[Page.postEventSourceID];
            var args = Request.Params[Page.postEventArgumentID];

            if (ctrlName == txtScore1.UniqueID && args == "txtScore1_OnKeyPress")
                txtScore1_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore2.UniqueID && args == "txtScore2_OnKeyPress")
                txtScore2_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore3.UniqueID && args == "txtScore3_OnKeyPress")
                txtScore3_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore4.UniqueID && args == "txtScore4_OnKeyPress")
                txtScore4_OnKeyPress(ctrlName, args);

            if (ctrlName == txtZealQty.UniqueID && args == "txtZealQty_OnKeyPress")
                txtZealQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilQty.UniqueID && args == "txtOilQty_OnKeyPress")
                txtOilQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilPrice.UniqueID && args == "txtOilPrice_OnKeyPress")
                txtOilPrice_OnKeyPress(ctrlName, args);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void txtScore1_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore2_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore3_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore4_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CalulateTotal()
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtScore1.Text.Trim(), out tmp) || (decimal.Parse(txtScore1.Text.Trim()) < 0))
                txtScore1.Text = "0";

            if (!decimal.TryParse(txtScore2.Text.Trim(), out tmp) || (decimal.Parse(txtScore2.Text.Trim()) < 0))
                txtScore2.Text = "0";

            if (!decimal.TryParse(txtScore3.Text.Trim(), out tmp) || (decimal.Parse(txtScore3.Text.Trim()) < 0))
                txtScore3.Text = "0";

            if (!decimal.TryParse(txtScore4.Text.Trim(), out tmp) || (decimal.Parse(txtScore4.Text.Trim()) < 0))
                txtScore4.Text = "0";

            int sum = 1;
            sum *= (txtScore1.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore1.Text.Trim());
            sum *= (txtScore2.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore2.Text.Trim());
            sum *= (txtScore3.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore3.Text.Trim());
            sum *= (txtScore4.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore4.Text.Trim());
            txtScore5.Text = sum.ToString();

            DataTable dt = ComplainBLL.Instance.SelectScorePointBLL(txtScore5.Text.Trim());
            if (dt.Rows.Count > 0)
                txtScore6.Text = dt.Rows[0]["NSCORE"].ToString();
            else
                txtScore6.Text = "0";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void txtZealQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            int tmp;
            if (!int.TryParse(txtZealQty.Text.Trim(), out tmp) || decimal.Parse(txtZealQty.Text.Trim()) < 0)
            {
                txtZealQty.Text = "0";
                txtCost.Text = "0";
                return;
            }

            txtZealQty.Text = int.Parse(txtZealQty.Text.Trim()).ToString();
            txtCost.Text = (int.Parse(txtZealQty.Text.Trim()) * int.Parse(txtZeal.Text.Trim())).ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtOilQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtOilQty.Text.Trim(), out tmp) || decimal.Parse(txtOilQty.Text.Trim()) < 0)
                txtOilQty.Text = "0";

            if (decimal.Parse(txtOilQty.Text.Trim()) > 0)
            {
                if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString(), "0") && decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
                else if (decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()) || decimal.Parse(txtOilQty.Text.Trim()) > decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString()))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
            }

            txtOilQty.Text = decimal.Parse(txtOilQty.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }

    private void txtOilPrice_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            txtOilPrice.ReadOnly = true;
            decimal tmp;
            if (!decimal.TryParse(txtOilPrice.Text.Trim(), out tmp) || decimal.Parse(txtOilPrice.Text.Trim()) < 0)
                txtOilPrice.Text = "0";

            txtOilPrice.Text = decimal.Parse(txtOilPrice.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
        finally
        {
            txtOilPrice.ReadOnly = false;
        }
    }

    private void CalculateOil()
    {
        try
        {
            if (!string.Equals(txtOilQty.Text.Trim(), string.Empty) && !string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                txtCost.Text = (int.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString()) * decimal.Parse(txtOilPrice.Text.Trim()) * decimal.Parse(txtOilQty.Text.Trim())).ToString();
            else
                txtCost.Text = "0";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void EnableSaveTab3(bool IsSave)
    {
        cmdSaveTab3.Enabled = IsSave;
        cmdSaveTab3Draft.Enabled = IsSave;
        cmdChangeStatus.Enabled = IsSave;
        cmdSaveIsCost.Enabled = IsSave;
    }

    private void EnableTab3()
    {
        try
        {
            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            //ถ้าเป็น รข หรือเจ้าหน้าที่ ปตท ให้เปิด Tab3
            if ((string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode) || string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup2.ToString()) || string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString())))
            {
                if (string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode))
                    this.EnableSaveTab3(true);
                else
                    this.EnableSaveTab3(false);

                DataTable dtSentencer = ComplainBLL.Instance.SentencerSelectBLL(1);
                DropDownListHelper.BindDropDownList(ref ddlSentencer, dtSentencer, "SSENTENCERCODE", "SSENTENCERNAME", true);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables["ACCIDENT"];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        txtRemarkTab3.Text = dr["REMARK_TAB4"] + string.Empty;
                        lblCusScoreHeader.Text = string.Format(lblCusScoreHeader.Text, dr["SCONTRACTNO"] + string.Empty);

                        if (!string.Equals(dr["SSENTENCERCODE"] + string.Empty, string.Empty))
                        {
                            if (string.Equals(dr["SSENTENCERCODE"] + string.Empty, "0"))
                            {
                                radSentencerType.SelectedValue = "0";
                                ddlSentencer.SelectedIndex = 0;
                                ddlSentencer.Enabled = false;
                            }
                            else
                            {
                                radSentencerType.SelectedValue = "1";
                                ddlSentencer.Enabled = true;
                                ddlSentencer.SelectedValue = dr["SSENTENCERCODE"] + string.Empty;
                            }
                        }
                    }

                }

                dtScore = ComplainBLL.Instance.ScoreSelectBLL();
                DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
                this.SelectDuplicateCar();

                dtScoreList = new DataTable();
                dtScoreList.Columns.Add("STOPICID");
                dtScoreList.Columns.Add("STOPICNAME");
                dtScoreList.Columns.Add("Cost");
                dtScoreList.Columns.Add("CostDisplay");
                dtScoreList.Columns.Add("DisableDriver");
                dtScoreList.Columns.Add("DisableDriverDisplay");
                dtScoreList.Columns.Add("Score1");
                dtScoreList.Columns.Add("Score2");
                dtScoreList.Columns.Add("Score3");
                dtScoreList.Columns.Add("Score4");
                dtScoreList.Columns.Add("Score5");
                dtScoreList.Columns.Add("Score6");
                dtScoreList.Columns.Add("Score6Display");
                dtScoreList.Columns.Add("TotalCar");

                dtScoreListCar = new DataTable();
                dtScoreListCar.Columns.Add("STOPICID");
                dtScoreListCar.Columns.Add("TRUCKID");
                dtScoreListCar.Columns.Add("CARHEAD");
                dtScoreListCar.Columns.Add("CARDETAIL");
                dtScoreListCar.Columns.Add("DELIVERY_DATE");
                dtScoreListCar.Columns.Add("TOTAL_CAR");

                dtUploadTab3 = new DataTable();
                dtUploadTab3.Columns.Add("UPLOAD_ID");
                dtUploadTab3.Columns.Add("UPLOAD_NAME");
                dtUploadTab3.Columns.Add("FILENAME_SYSTEM");
                dtUploadTab3.Columns.Add("FILENAME_USER");
                dtUploadTab3.Columns.Add("FULLPATH");

                dtUploadTypeCost = new DataTable();
                dtUploadTypeCost.Columns.Add("UPLOAD_ID");
                dtUploadTypeCost.Columns.Add("UPLOAD_NAME");
                dtUploadTypeCost.Columns.Add("FILENAME_SYSTEM");
                dtUploadTypeCost.Columns.Add("FILENAME_USER");
                dtUploadTypeCost.Columns.Add("FULLPATH");

                DropDownListHelper.BindDropDownList(ref ddlScore1, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore2, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore3, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore4, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);

                dtUploadTypeTab3 = UploadTypeBLL.Instance.UploadTypeSelectBLL("ACCIDENT_SCORE");
                DropDownListHelper.BindDropDownList(ref cboUploadTypeTab3, dtUploadTypeTab3, "UPLOAD_ID", "UPLOAD_NAME", true);

                dtUploadTypeCost = UploadTypeBLL.Instance.UploadTypeSelectBLL("ACCIDENT_COST");
                DropDownListHelper.BindDropDownList(ref cboUploadCost, dtUploadTypeCost, "UPLOAD_ID", "UPLOAD_NAME", true);

                dtUploadCost = AccidentBLL.Instance.AccidentFile4Select(hidID.Value, "ACCIDENT_COST", string.Empty);
                GridViewHelper.BindGridView(ref dgvUploadCost, dtUploadCost);
                DropDownListHelper.BindDropDownList(ref ddlFileName, dtUploadCost, "FILENAME_USER", "FILENAME_USER", false);
                ddlFileName.Items.Insert(0, new ListItem() { Text = "--เลือก--", Value = "" });
                int sum = 0;
                //for (int i = 0; i < dtHeader.Rows.Count; i++)
                //{
                //    if (!string.Equals(dtHeader.Rows[i]["TotalCar"].ToString(), string.Empty) && int.Parse(dtHeader.Rows[i]["TotalCar"].ToString()) > 0)
                //        sum += int.Parse(dtHeader.Rows[i]["TotalCar"].ToString());
                //}

                if (sum > 0)
                {
                    //SumCar = sum;
                    //rowTotalCarTab3.Visible = true;
                    //txtTotalCarTab3.Text = SumCar.ToString();
                    rowListTotalCarTab3.Visible = true;

                    dtDup = AccidentBLL.Instance.AccidentDuplicateTotal(hidID.Value);
                    GridViewHelper.BindGridView(ref dgvScoreTotalCar, dtDup);
                }
                else
                {
                    rowListCarTab3.Visible = true;
                }

                dtCusScoreType = ComplainBLL.Instance.CusScoreSelectBLL();
                DropDownListHelper.BindRadioButton(ref radCusScoreType, dtCusScoreType, "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME");

                if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                    dtUploadTab3 = AccidentBLL.Instance.AccidentFile4Select(hidID.Value, "ACCIDENT_SCORE", "1");
                else
                    dtUploadTab3 = AccidentBLL.Instance.AccidentFile4Select(hidID.Value, "ACCIDENT_SCORE", string.Empty);

                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);

                dtScoreList = AccidentBLL.Instance.AccidentScoreDetailSelect(hidID.Value);
                dtScoreListCar = AccidentBLL.Instance.AccidentScoreCarSelect(hidID.Value);

                dtScoreList.Columns["COST"].ColumnName = "Cost";
                dtScoreList.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
                dtScoreList.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
                dtScoreList.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
                dtScoreList.Columns["SCORE1"].ColumnName = "Score1";
                dtScoreList.Columns["SCORE2"].ColumnName = "Score2";
                dtScoreList.Columns["SCORE3"].ColumnName = "Score3";
                dtScoreList.Columns["SCORE4"].ColumnName = "Score4";
                dtScoreList.Columns["SCORE5"].ColumnName = "Score5";
                dtScoreList.Columns["SCORE6"].ColumnName = "Score6";
                dtScoreList.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
                dtScoreList.Columns["TOTALCAR"].ColumnName = "TotalCar";

                GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);

                dt = new DataTable();
                dt = AccidentBLL.Instance.AccidentScoreSelect(hidID.Value);
                if (dt.Rows.Count > 0)
                {
                    if (!string.Equals(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString(), "-1"))
                    {
                        radCusScoreType.SelectedValue = dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString();

                        txtPointFinal.Enabled = false;
                        txtCostFinal.Enabled = false;
                        txtDisableFinal.Enabled = false;

                        lblShowSumPoint.Text = "(Sum : {0})";
                        lblShowSumCost.Text = "(Sum : {0})";
                        lblShowSumDisable.Text = "(Sum : {0})";

                        lblShowSumPoint.Visible = false;
                        lblShowSumCost.Visible = false;
                        lblShowSumDisable.Visible = false;

                        int row = this.GetSelectRow(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString());

                        string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                        switch (CusType)
                        {
                            //case "MAX": this.FindMax(); break;
                            //case "SUM": this.FindSum("Normal"); break;
                            case "FREETEXT":
                                this.FindSum(string.Empty);

                                txtPointFinal.Enabled = true;
                                txtCostFinal.Enabled = true;
                                txtDisableFinal.Enabled = true;

                                lblShowSumPoint.Visible = true;
                                lblShowSumCost.Visible = true;
                                lblShowSumDisable.Visible = true; break;
                            //this.FreeText(); break;
                            default:
                                break;
                        }

                        radCostCheck.SelectedValue = dt.Rows[0]["COST_CHECK"].ToString();
                        txtCostCheckDate.Text = (dt.Rows[0]["COST_CHECK_DATE"] != null) ? dt.Rows[0]["COST_CHECK_DATE"].ToString() : string.Empty;
                        txtCostOther.Text = dt.Rows[0]["COST_OTHER"].ToString();
                        txtOilLose.Text = dt.Rows[0]["OIL_LOSE"].ToString();


                        this.CheckBlacklist();
                        if (string.Equals(dt.Rows[0]["BLACKLIST"].ToString(), "1"))
                        {
                            chkBlacklist.Checked = true;
                            chkBlacklist.Enabled = true;
                        }
                    }

                    if (!string.Equals(dt.Rows[0]["TOTAL_POINT"].ToString(), "-1"))
                        txtPointFinal.Text = dt.Rows[0]["TOTAL_POINT"].ToString();

                    if (!string.Equals(dt.Rows[0]["TOTAL_COST"].ToString(), "-1"))
                        txtCostFinal.Text = dt.Rows[0]["TOTAL_COST"].ToString();
                    int CostOther, CostFinal;
                    int.TryParse(txtCostOther.Text.Trim(), out CostOther);
                    int.TryParse(txtCostFinal.Text.Trim(), out CostFinal);
                    txtAmount.Text = (CostOther + CostFinal) + string.Empty;
                    txtAmount.ReadOnly = false;
                    if (string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-2"))
                    {
                        txtDisableFinal.Text = "Hold";
                        txtDisableFinal.ReadOnly = true;
                    }
                    else if (!string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-1"))
                        txtDisableFinal.Text = dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString();

                    //if (string.Equals(DocStatusID, ConfigValue.DocStatus5) || string.Equals(DocStatusID, ConfigValue.DocStatus6) || string.Equals(DocStatusID, ConfigValue.DocStatus7) || string.Equals(DocStatusID, ConfigValue.DocStatus8) || string.Equals(DocStatusID, ConfigValue.DocStatus9))
                    //    this.NotSaveTab3();
                }

                if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup2) && !string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode))
                {//เจ้าหน้าที่ ปตท ไม่ใช่ รข.
                    this.NotSaveTab3();
                    cmdSaveIsCost.Visible = false;
                }
                //if (ConfigValue.ComplainAdminUser.Contains(dtLogin.Rows[0]["SUSERNAME"].ToString()))
                //{//Super Admin
                //    ((HtmlAnchor)this.FindControlRecursive(Page, "ChangeStatusTab")).Visible = true;
                //    cmdAdminChangeStatus.Enabled = true;

                //    txtDocIDTab4.Value = txtDocID.Value;
                //    this.LoadDocStatus();
                //}

                this.CheckBlacklist();
                CheckCanEdit();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void NotSaveTab3()
    {
        cmdSaveTab3.Enabled = false;
        cmdSaveTab3Draft.Enabled = false;
        cmdChangeStatus.Enabled = false;
        //cmdAdminChangeStatus.Enabled = false;
    }

    protected void cboScore_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.EnableScore(false);

            if (cboScore.SelectedIndex == 0)
            {
                this.ClearScreenTab3();
            }
            else
            {
                //115
                DataRow dr = dtScore.Rows[cboScore.SelectedIndex];
                if (dr != null)
                {
                    txtCost.Text = (dr["TOTAL_COST"].ToString() == string.Empty) ? "0" : dr["TOTAL_COST"].ToString();
                    if (cboScore.SelectedValue == "115")
                    {
                        txtDisableDriver.Text = "Hold";
                        txtDisableDriver.Enabled = false;
                    }
                    else
                    {
                        txtDisableDriver.Text = (dr["TOTAL_BREAK"].ToString() == string.Empty) ? "0" : dr["TOTAL_BREAK"].ToString();
                    }

                    txtScore1.Text = dr["SCOL02"].ToString();
                    txtScore2.Text = dr["SCOL03"].ToString();
                    txtScore3.Text = dr["SCOL04"].ToString();
                    txtScore4.Text = dr["SCOL06"].ToString();
                    txtScore5.Text = dr["NMULTIPLEIMPACT"].ToString();
                    txtScore6.Text = dr["NPOINT"].ToString();
                    lblShowOption.Visible = (dr["IS_CORRUPT"].ToString() == "0") ? false : true;

                    if (dr["TTOPIC_TYPE_NAME"].ToString().Contains("น้ำมัน"))
                    {
                        txtOilPrice.Text = "0";
                        txtOilQty.Text = "0";
                        rowOil.Visible = true;
                    }
                    else
                    {
                        txtOilPrice.Text = string.Empty;
                        txtOilQty.Text = string.Empty;
                        rowOil.Visible = false;
                    }

                    if (dr["TTOPIC_TYPE_NAME"].ToString().Contains("ซีล"))
                    {
                        txtZeal.Text = dr["VALUE2"].ToString();
                        txtZealQty.Text = "0";
                        rowZeal.Visible = true;
                    }
                    else
                    {
                        txtZeal.Text = string.Empty;
                        txtZealQty.Text = string.Empty;
                        rowZeal.Visible = false;
                    }

                    if ((!lblShowOption.Visible) && (string.Equals(dr["CAN_EDIT"].ToString(), "1")))
                    {//สามารถแก้ไขคะแนนได้
                        this.EnableScore(true);
                    }

                    if (string.Equals(dr["BLACKLIST"].ToString(), "1"))
                    {
                        chkBlacklist.Checked = true;
                        chkBlacklist.Enabled = true;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void EnableScore(bool Enable)
    {
        txtScore1.Enabled = Enable;
        txtScore2.Enabled = Enable;
        txtScore3.Enabled = Enable;
        txtScore4.Enabled = Enable;
    }

    protected void dgvScore_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dtTmp = dtScoreListCar.Copy();
            dtTmp.Rows.Clear();

            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                //if (!string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[e.RowIndex]["STOPICID"].ToString()))
                //    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
                if (i != e.RowIndex)
                    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
            }
            dtScoreListCar = dtTmp;

            dtScoreList.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);
            this.CheckBlacklist();
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckBlacklist()
    {
        try
        {
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (this.CheckTopicBlacklist(dtScoreList.Rows[i]["STOPICID"].ToString()))
                {
                    chkBlacklist.Enabled = true;
                    return;
                }
            }

            chkBlacklist.Checked = false;
            chkBlacklist.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CheckCanEdit()
    {
        if (!string.Equals(txtPointFinal.Text.Trim(), string.Empty))
        {
            DataTable dtAppeal = AccidentBLL.Instance.AccidentAppealSelect(hidID.Value);
            if (dtAppeal.Rows.Count == 0)
            {
                chkBlacklistFinal.Checked = chkBlacklist.Checked;
                txtAppealPoint.Text = txtPointFinal.Text;
                txtAppealCost.Text = txtCostFinal.Text;
                txtAppealCostOther.Text = txtCostOther.Text;
                txtAppealDisable.Text = txtDisableFinal.Text;
                txtAppealOilLose.Text = txtOilLose.Text;
            }
            else
            {
                //txtAppealPoint.Text = (decimal.Parse(txtPointFinal.Text.Trim()) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_POINT"].ToString())).ToString();
                //txtAppealCost.Text = (decimal.Parse(txtCostFinal.Text.Trim()) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_COST"].ToString())).ToString();
                //txtAppealDisable.Text = (decimal.Parse(txtDisableFinal.Text.Trim()) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_DISABLE"].ToString())).ToString();

                chkBlacklistFinal.Checked = (string.Equals(dtAppeal.Rows[0]["OIL_LOSE"].ToString(), "0") ? false : true);
                txtAppealPoint.Text = (decimal.Parse(txtPointFinal.Text) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_POINT"].ToString())).ToString();
                txtAppealCost.Text = (decimal.Parse(txtCostFinal.Text) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_COST"].ToString())).ToString();
                txtAppealCostOther.Text = dtAppeal.Rows[0]["COST_OTHER"].ToString();
                txtAppealDisable.Text = dtAppeal.Rows[0]["TOTAL_DISABLE"] + string.Empty == "-2" ? "Hold" : dtAppeal.Rows[0]["TOTAL_DISABLE"] + string.Empty;
                txtAppealOilLose.Text = dtAppeal.Rows[0]["OIL_LOSE"].ToString();
            }

        }
    }

    private void SelectDuplicateCar()
    {
        try
        {
            dtCarDistinct = new DataTable();
            dtCarDistinct.Columns.Add("DELIVERY_DATE");
            dtCarDistinct.Columns.Add("TRUCKID");
            dtCarDistinct.Columns.Add("CARHEAD");
            dtCarDistinct.Columns.Add("CARDETAIL");
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    dr = ds.Tables[0].Rows[i];
                    if (string.Equals(dr["STRUCKID"].ToString(), string.Empty))
                        continue;

                    if (dtCarDistinct.Rows.Count == 0)
                    {
                        dtCarDistinct.Rows.Add(DateTime.Parse(dr["ACCIDENT_DATE"].ToString()).ToString("dd/MM/yyyy HH:mm"), dr["STRUCKID"].ToString(), dr["SHEADREGISTERNO"].ToString(), dr["STRAILERREGISTERNO"].ToString());
                    }
                    else
                    {
                        int j;
                        for (j = 0; j < dtCarDistinct.Rows.Count; j++)
                        {
                            if (string.Equals(dtCarDistinct.Rows[j]["STRUCKID"].ToString(), dr["STRUCKID"].ToString()) && string.Equals(dtCarDistinct.Rows[j]["DELIVERY_DATE"].ToString(), dr["ACCIDENT_DATE"].ToString()))
                                break;
                        }
                        if ((j == dtCarDistinct.Rows.Count) && ((!string.Equals(dtCarDistinct.Rows[j - 1]["STRUCKID"].ToString(), dr["STRUCKID"].ToString())) || (!string.Equals(dtCarDistinct.Rows[j - 1]["DELIVERY_DATE"].ToString(), dr["ACCIDENT_DATE"].ToString()))))
                            dtCarDistinct.Rows.Add(dr["ACCIDENT_DATE"].ToString(), dr["STRUCKID"].ToString(), dr["SHEADREGISTERNO"].ToString(), dr["STRAILERREGISTERNO"].ToString());
                    }
                }
            }


            GridViewHelper.BindGridView(ref dgvScore, dtCarDistinct);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void RefreshCusScore()
    {
        try
        {
            if (!string.Equals(radCusScoreType.SelectedValue, string.Empty))
            {
                int row = this.GetSelectRow(radCusScoreType.SelectedValue);

                txtPointFinal.Enabled = false;
                txtCostFinal.Enabled = false;
                txtDisableFinal.Enabled = false;

                lblShowSumPoint.Text = "(ผลรวม : {0})";
                lblShowSumCost.Text = "(ผลรวม : {0})";
                lblShowSumDisable.Text = "(ผลรวม : {0})";

                lblShowSumPoint.Visible = false;
                lblShowSumCost.Visible = false;
                lblShowSumDisable.Visible = false;

                string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                switch (CusType)
                {
                    case "MAX": this.FindMax(); break;
                    case "SUM": this.FindSum("Normal"); break;
                    case "FREETEXT":
                        lblShowSumPoint.Visible = true;
                        lblShowSumCost.Visible = true;
                        lblShowSumDisable.Visible = true;
                        this.FreeText(); break;
                    default:
                        break;
                }
            }
            else
            {
                //txtPointFinal.Text = "0";
                //txtCostFinal.Text = "0";
                //txtDisableFinal.Text = "0";
            }

            //this.CalculateScore("ddlScore1", "txtScore1Final", "Score1");
            //this.CalculateScore("ddlScore2", "txtScore2Final", "Score2");
            //this.CalculateScore("ddlScore3", "txtScore3Final", "Score3");
            //this.CalculateScore("ddlScore4", "txtScore4Final", "Score4");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private int GetSelectRow(string Value)
    {
        try
        {
            for (int i = 0; i < dtCusScoreType.Rows.Count; i++)
            {
                if (string.Equals(dtCusScoreType.Rows[i]["CUSSCORE_TYPE_ID"].ToString(), Value))
                    return i;
            }

            return -1;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FindMax()
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString()) > PointMax)
                    PointMax = decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            }
            txtPointFinal.Text = PointMax.ToString();

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString()) > CostMax)
                    CostMax = decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            }
            txtCostFinal.Text = CostMax.ToString();

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (dtScoreList.Rows[i]["DisableDriver"] + string.Empty == "-2")
                {
                    DisableDriverMax = -2;
                }
                else if (decimal.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString()) > DisableDriverMax)
                    DisableDriverMax = int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());

            }
            if (DisableDriverMax == -2)
            {
                txtDisableFinal.Text = "Hold";
                txtDisableFinal.ReadOnly = true;
            }
            else
            {
                txtDisableFinal.Text = DisableDriverMax.ToString();
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FindSum(string ShowType)
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                PointMax += decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtPointFinal.Text = PointMax.ToString();
            else
                lblShowSumPoint.Text = string.Format(lblShowSumPoint.Text, PointMax.ToString());

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                CostMax += decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtCostFinal.Text = CostMax.ToString();
            else
                lblShowSumCost.Text = string.Format(lblShowSumCost.Text, CostMax.ToString());

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (dtScoreList.Rows[i]["DisableDriver"] == "-2")
                {
                    DisableDriverMax = -2;
                    break;
                }
                DisableDriverMax += int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());

            }
            if (DisableDriverMax == -2)
            {
                txtDisableFinal.Text = "Hold";
                lblShowSumDisable.Text = "Hold";
            }
            else
            {
                if (string.Equals(ShowType, "Normal"))
                    txtDisableFinal.Text = DisableDriverMax.ToString();
                else
                    lblShowSumDisable.Text = string.Format(lblShowSumDisable.Text, DisableDriverMax.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FreeText()
    {
        try
        {
            txtPointFinal.Text = string.Empty;
            txtCostFinal.Text = string.Empty;
            txtDisableFinal.Text = string.Empty;

            this.FindSum("ForFreeText");

            txtPointFinal.Enabled = true;
            txtCostFinal.Enabled = true;
            txtDisableFinal.Enabled = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private bool CheckTopicBlacklist(string STOPICID)
    {
        try
        {
            for (int i = 0; i < dtScore.Rows.Count; i++)
            {
                if (string.Equals(dtScore.Rows[i]["STOPICID"].ToString(), STOPICID) && string.Equals(dtScore.Rows[i]["BLACKLIST"].ToString(), "1"))
                    return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ClearScreenTab3()
    {
        try
        {
            cboScore.SelectedIndex = 0;
            txtCost.Text = string.Empty;
            txtDisableDriver.Text = string.Empty;
            txtScore1.Text = string.Empty;
            txtScore2.Text = string.Empty;
            txtScore3.Text = string.Empty;
            txtScore4.Text = string.Empty;
            txtScore5.Text = string.Empty;
            txtScore6.Text = string.Empty;
            lblShowOption.Visible = false;

            txtOilPrice.Text = string.Empty;
            txtOilQty.Text = string.Empty;
            txtZeal.Text = string.Empty;
            txtZealQty.Text = string.Empty;

            rowOil.Visible = false;
            rowZeal.Visible = false;

            this.EnableScore(false);

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = true;
            }
            ((CheckBox)this.FindControlRecursive(dgvScore, "chkChooseScoreAll")).Checked = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null)
                return controlToReturn;
        }
        return null;
    }

    protected void chkChooseScoreAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void chkChooseScoreListCarAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdAddScore_Click(object sender, EventArgs e)
    {
        try
        {
            int CountCar = this.ValidateAddScore();

            string CostDisplay = string.Empty;
            string Score6Display = string.Empty;
            string DisableDriverDisplay = string.Empty;

            CostDisplay = txtCost.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString();
            Score6Display = txtScore6.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString();
            if (txtDisableDriver.Text.Trim() == "Hold")
            {
                DisableDriverDisplay = "Hold";
            }
            else
            {
                DisableDriverDisplay = txtDisableDriver.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString();
            }


            dtScoreList.Rows.Add(cboScore.SelectedValue, cboScore.SelectedItem, (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString(), CostDisplay, txtDisableDriver.Text.Trim() == "Hold" ? "-2" : (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString(), DisableDriverDisplay, txtScore1.Text.Trim(), txtScore2.Text.Trim(), txtScore3.Text.Trim(), txtScore4.Text.Trim(), txtScore5.Text.Trim(), (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString(), Score6Display, CountCar);

            CheckBox Chk;
            if (rowListTotalCarTab3.Visible)
            {//ป้อนจำนวนคัน
                TextBox Txt;

                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                    {
                        Txt = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, string.Empty, string.Empty, string.Empty, dtDup.Rows[i]["DELIVERY_DATE"].ToString(), Txt.Text.Trim());
                    }
                }
            }
            else
            {//เลือกทะเบียนรถ
                for (int i = 0; i < dgvScore.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtCarDistinct.Rows[i]["CARHEAD"].ToString(), dtCarDistinct.Rows[i]["CARDETAIL"].ToString(), dtCarDistinct.Rows[i]["DELIVERY_DATE"].ToString(), 0);
                }
            }

            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);
            this.ClearScreenTab3();
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdChangeStatus_Click(object sender, EventArgs e)
    {
        try
        {
            lblModalTitle3.Text = "กรุณาป้อน รายการเอกสาร ที่ขอเพิ่มเติม จากผู้ขนส่ง";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowWithAutoPostBack", "$('#ShowWithAutoPostBack').modal();", true);
            upModal3.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdCloseTab2_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_Complain_lst.aspx");
    }

    protected void cmdSaveIsCost_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSaveIsCost();

            DateTime? CostCheckDate = null;
            if (!string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
                CostCheckDate = DateTime.ParseExact(txtCostCheckDate.Text.Trim(), "dd/MM/yyyy", null);

            int DocStatusID = (string.Equals(radCostCheck.SelectedValue, "1")) ? ConfigValue.DocStatus10 : ConfigValue.DocStatus11;

            DataSet ds = new DataSet("DS");
            dtUploadCost.TableName = "DT";
            ds.Tables.Add(dtUploadCost.Copy());

            DataSet dsFine = new DataSet("DS");
            CreateDataAccFine();
            dt.TableName = "DT";
            dsFine.Tables.Add(dt.Copy());
            DateTime? dtt = ConvertToDateNull(hidApproveDate.Value);
            int cactive = int.Parse(hidCactive.Value);
            if (dtt != null)
            {
                if (dtt.Value.AddDays(7) < DateTime.Today || cactive > 10)
                {
                    if (radCostCheck.SelectedValue == "1")
                    {
                        cactive = 15;
                    }
                    else if (radCostCheck.SelectedValue == "2")
                    {
                        cactive = 14;
                    }
                    else if (radCostCheck.SelectedValue == "0")
                    {
                        cactive = 13;
                    }
                }
            }


            AccidentBLL.Instance.AccidentCostUpdate(hidID.Value, radCostCheck.SelectedValue, txtCostCheckDate.Text.Trim(), cactive, Session["UserID"] + string.Empty, "ACCIDENT_COST", ds, dsFine, txtAmount.Text.Trim());
            //ComplainImportFileBLL.Instance.ComplainUpdateCostBLL(DocID, int.Parse(radCostCheck.SelectedValue), CostCheckDate, DocStatusID, dtUploadCost, int.Parse(Session["UserID"].ToString()), "COMPLAIN_COST");
            //SendEmail(ConfigValue.EmailAccident8, hidID.Value, true, string.Empty);

            byte[] plaintextBytes = Encoding.UTF8.GetBytes(hidID.Value);
            string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + hidID.Value + ")", "AccidentTab4.aspx?str=" + encryptedValue);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSaveTab3Draft_Click(object senderf, EventArgs e)
    {
        try
        {
            this.SaveTab3(false, int.Parse(hidCactive.Value));
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdShowAppeal_Click(object senderf, EventArgs e)
    {
        try
        {
            Session["AppealAccID"] = hidID.Value;
            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                Response.Redirect("admin_suppliant_lst.aspx");
            else
                Response.Redirect("admin_suppliant_lst.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSaveTab3_Click(object sender, EventArgs e)
    {
        try
        {
            this.SaveTab3(true, 9);
            this.LockAndUnlockDriver();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void SaveTab3(bool isValiDate, int CACTIVE)
    {
        try
        {
            int isSaveCusScore = 0;
            if (isValiDate)
            {
                this.ValidateSaveTab3();
                isSaveCusScore = 1;
            }

            decimal tmp2;
            if (!string.Equals(txtCostOther.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtCostOther.Text.Trim(), out tmp2))
                    throw new Exception("กรุณาป้อน ค่าปรับ เป็นตัวเลข");
            }
            else
            {
                txtCostOther.Text = "0";
            }

            if (!string.Equals(txtOilLose.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtOilLose.Text.Trim(), out tmp2))
                    throw new Exception("กรุณาป้อน ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร) เป็นตัวเลข");
            }
            else
            {
                txtOilLose.Text = "0";
            }

            //DateTime? CostCheckDate = null;
            //if (!string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
            //    CostCheckDate = DateTime.ParseExact(txtCostCheckDate.Text.Trim(), "dd/MM/yyyy", null);

            int Blacklist = (chkBlacklist.Checked) ? 1 : 0;

            string Sentencer = string.Empty;
            if (radSentencerType.SelectedIndex >= 0)
                Sentencer = (ddlSentencer.SelectedIndex == 0) ? radSentencerType.SelectedValue : ddlSentencer.SelectedValue;

            DataSet dsFile = new DataSet("DS");
            dtUploadTab3.TableName = "DT";
            dsFile.Tables.Add(dtUploadTab3.Copy());
            DataSet dsScoreList = new DataSet("DS");
            dtScoreList.TableName = "DT";
            dsScoreList.Tables.Add(dtScoreList.Copy());
            DataSet dsScoreListCar = new DataSet("DS");
            dtScoreListCar.TableName = "DT";
            dsScoreListCar.Tables.Add(dtScoreListCar.Copy());

            AccidentBLL.Instance.SaveTab4(hidID.Value, CACTIVE
                , Session["UserID"] + string.Empty
                , "ACCIDENT_SCORE"
                , (radCusScoreType.SelectedIndex > -1) ? int.Parse(radCusScoreType.SelectedValue) : -1
                , (!string.Equals(txtPointFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtPointFinal.Text.Trim()) : -1
                , (!string.Equals(txtCostFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostFinal.Text.Trim()) : -1
                , (!string.Equals(txtDisableFinal.Text.Trim(), string.Empty))
                ? (string.Equals(txtDisableFinal.Text.Trim(), "Hold") ? -2 : int.Parse(txtDisableFinal.Text.Trim())) : -1
                , dsScoreList
                , int.Parse(radCostCheck.SelectedValue)
                , txtCostCheckDate.Text.Trim()
                , dsScoreListCar
                , isValiDate
                , decimal.Parse(txtCostOther.Text.Trim())
                , isSaveCusScore, txtRemarkTab3.Text.Trim()
                , decimal.Parse(txtOilLose.Text.Trim())
                , Blacklist, Sentencer, dsFile, hidSVENDORID.Value);
            string mess = "บันทึกสำเร็จ";
            if (isValiDate)
            {
                if (!IsSpecialDoc)
                {
                    this.SendEmail(ConfigValue.EmailAccident6, hidID.Value, true, string.Empty);
                    mess += "<br/>ส่ง Email สำเร็จ";
                }
            }


            if (!string.IsNullOrEmpty(hidID.Value))
            {
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(hidID.Value);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess(mess, "AccidentTab4.aspx?str=" + encryptedValue);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LockAndUnlockDriver()
    {
        try
        {
            string Detail = string.Empty;
            Detail += "รถขนส่งเกิดอุบัติเหตุ หมายเลขเอกสาร " + txtAccidentID.Text.Trim();
            Detail += "<br/>สถานะการทำงาน : ระงับการทำงาน";
            Detail += "<br/>หมายเหตุ : อยู่ระหว่างการตรวจสอบรถขนส่งเกิดอุบัติเหตุ(Tab4)";
            VendorBLL.Instance.LogInsert(Detail, hidPERS_CODE.Value, null, "0", "0", int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, 0);
            VendorBLL.Instance.LogInsert(Detail, hidPERS_CODE2.Value, null, "0", "0", int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, 0);
            VendorBLL.Instance.LogInsert(Detail, hidPERS_CODE3.Value, null, "0", "0", int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, 0);
            VendorBLL.Instance.LogInsert(Detail, hidPERS_CODE4.Value, null, "0", "0", int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, 0);
            //if (string.Equals(txtDisableFinal.Text.Trim(), "Hold"))
            //{

            //}
            //int TotalLock = (string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? 0 :(string.Equals(txtDisableFinal.Text.Trim(), "Hold") ? -2 : int.Parse(txtDisableFinal.Text.Trim()));
            //ComplainBLL.Instance.ComplainLockDriverBLL(hidID.Value, TotalLock, int.Parse(Session["UserID"].ToString()));
            //DataTable dtDriver = ComplainBLL.Instance.DriverUpdateSelect2BLL(hidID.Value);
            DataTable dtDriverDetail;

            if (chkBlacklist.Checked)
            {
                if (!string.Equals(hidSEMPLOYEEID.Value, string.Empty))
                {
                    dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(hidSEMPLOYEEID.Value);

                    ComplainBLL.Instance.UpdateDriverStatusBLL(hidSEMPLOYEEID.Value, "2", "2");

                    SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                    {
                        DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                        PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                        LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                        LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                        LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                        Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                        Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                        FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                        LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                        CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                        DRV_STATUS = ConfigValue.DriverOut
                    };

                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    if (string.Equals(resultCheck, "N"))
                        throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                }
                if (!string.Equals(hidSEMPLOYEEID2.Value, string.Empty))
                {
                    dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(hidSEMPLOYEEID2.Value);

                    ComplainBLL.Instance.UpdateDriverStatusBLL(hidSEMPLOYEEID2.Value, "2", "2");

                    SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                    {
                        DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                        PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                        LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                        LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                        LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                        Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                        Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                        FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                        LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                        CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                        DRV_STATUS = ConfigValue.DriverOut
                    };

                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    if (string.Equals(resultCheck, "N"))
                        throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                }
                if (!string.Equals(hidSEMPLOYEEID3.Value, string.Empty))
                {
                    dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(hidSEMPLOYEEID3.Value);

                    ComplainBLL.Instance.UpdateDriverStatusBLL(hidSEMPLOYEEID3.Value, "2", "2");

                    SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                    {
                        DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                        PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                        LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                        LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                        LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                        Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                        Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                        FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                        LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                        CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                        DRV_STATUS = ConfigValue.DriverOut
                    };

                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    if (string.Equals(resultCheck, "N"))
                        throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                }
                if (!string.Equals(hidSEMPLOYEEID4.Value, string.Empty))
                {
                    dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(hidSEMPLOYEEID4.Value);

                    ComplainBLL.Instance.UpdateDriverStatusBLL(hidSEMPLOYEEID4.Value, "2", "2");

                    SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                    {
                        DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                        PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                        LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                        LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                        LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                        Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                        Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                        FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                        LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                        CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                        DRV_STATUS = ConfigValue.DriverOut
                    };

                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    if (string.Equals(resultCheck, "N"))
                        throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                }

            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSaveTab3()
    {
        try
        {
            if (dtScoreList.Rows.Count == 0)
                throw new Exception("กรุณาเลือก หัวข้อการตัดคะแนน");

            if (radCusScoreType.SelectedIndex < 0)
                throw new Exception("กรุณาเลือก เงื่อนไขการตัดคะแนน");

            if (string.Equals(txtPointFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน คะแนน");

            if (string.Equals(txtCostFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ค่าปรับ");

            if (string.Equals(txtDisableFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ระงับ พขร. (วัน)");

            int tmp1;
            decimal tmp2;
            if (!string.Equals(txtDisableFinal.Text.Trim(), "Hold"))
            {
                if (!int.TryParse(txtDisableFinal.Text.Trim(), out tmp1))
                    throw new Exception("กรุณาป้อน ระงับ พขร. (วัน) เป็นตัวเลข");
            }


            if (!decimal.TryParse(txtPointFinal.Text.Trim(), out tmp2))
                throw new Exception("กรุณาป้อน คะแนน (วัน) เป็นตัวเลข");

            if (!decimal.TryParse(txtCostFinal.Text.Trim(), out tmp2))
                throw new Exception("กรุณาป้อน ค่าปรับ เป็นตัวเลข");

            //ในกรณีมีวันที่เกิดเหตุหลายวัน ต้องเลือกเป็น Sum เท่านั้น
            if (dgvScore.Visible)
                this.CheckDeliveryDate(dtCarDistinct);
            else if (dgvScoreTotalCar.Visible)
                this.CheckDeliveryDate(dtDup);

            if ((ddlSentencer.Enabled) && (ddlSentencer.SelectedIndex < 1))
                throw new Exception("กรุณาเลือก คณะกรรมการพิจารณาโทษ");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdUploadCost_Click(object sender, EventArgs e)
    {
        this.StartUploadCost();
    }

    private void StartUploadCost()
    {
        try
        {
            if (cboUploadCost.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUploadCost.HasFile)
            {
                string FileNameUser = fileUploadCost.PostedFile.FileName;
                DataRow[] drs = dtUploadCost.Select("FILENAME_USER = '" + FileNameUser + "'");
                if (drs.Any())
                {
                    alertFail("File " + FileNameUser + "มีอยู่แล้วในระบบกรุณาตั้งชื่อ File ใหม่หรือเลือก File ใหม่");
                    return;
                }
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFileCost(System.IO.Path.GetExtension(FileNameUser), fileUploadCost.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadCost.SaveAs(Path + "\\" + FileNameSystem);

                dtUploadCost.Rows.Add(cboUploadCost.SelectedValue, cboUploadCost.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadCost, dtUploadCost);
                DropDownListHelper.BindDropDownList(ref ddlFileName, dtUploadCost, "FILENAME_USER", "FILENAME_USER", false);
                ddlFileName.Items.Insert(0, new ListItem() { Text = "--เลือก--", Value = "" });
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Complain" + "\\" + hidID.Value;
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFileCost(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeCost.Rows[cboUploadCost.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeCost.Rows[cboUploadCost.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeCost.Rows[cboUploadCost.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadCost_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadCost.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadCost, dtUploadCost);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadCost_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadCost.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    protected void dgvUploadCost_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckDeliveryDate(DataTable dt)
    {
        try
        {
            string DeliveryDate = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0)
                    DeliveryDate = dt.Rows[i]["DELIVERY_DATE"].ToString();
                if ((!string.Equals(DeliveryDate, dt.Rows[i]["DELIVERY_DATE"].ToString())) && (!string.Equals(radCusScoreType.SelectedValue, "2")))
                    throw new Exception("ในกรณีที่มีหลายวันที่เกิดเหตุ ต้องเลือกตัดคะแนน เป็นผลรวมเท่านั้น");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int ValidateAddScore()
    {
        try
        {
            if (cboScore.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก หัวข้อตัดคะแนน");

            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (string.Equals(dtScoreList.Rows[i]["STOPICID"].ToString(), cboScore.SelectedValue))
                    throw new Exception("เลือกหัวข้อ ตัดคะแนน ซ้ำกัน");
            }

            if (string.Equals(txtScore1.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ภาพลักษณ์องค์กร");

            if (string.Equals(txtScore2.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ความพึงพอใจลูกค้า");

            if (string.Equals(txtScore3.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน กฎ/ระเบียบฯ");

            if (string.Equals(txtScore4.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน แผนงานขนส่ง");

            if (rowOil.Visible)
            {
                if (string.Equals(txtOilQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนน้ำมัน");

                if (string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน ค่าน้ำมัน");
            }

            if (rowZeal.Visible)
            {
                if (string.Equals(txtZealQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนซิล");
            }

            if (rowListTotalCarTab3.Visible)
            {
                //if (string.Equals(txtTotalCarTab3.Text.Trim(), string.Empty))
                //    throw new Exception("กรุณาป้อน จำนวนรถ (คัน)");

                //int tmp;
                //if (!int.TryParse(txtTotalCarTab3.Text.Trim(), out tmp))
                //    throw new Exception("กรุณาป้อน จำนวนรถ (คัน) เป็นตัวเลข");

                //if (SumCar < int.Parse(txtTotalCarTab3.Text.Trim()))
                //    throw new Exception("จำนวนรถที่ป้อน มีค่ามากกว่า จำนวนรถที่ถูกร้องเรียน");

                CheckBox Chk;
                TextBox Txt1;
                int MaxNumber = 0;
                int tmp;
                int TotalCar = 0;
                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");

                    if (Chk.Checked)
                    {
                        MaxNumber = int.Parse(dtDup.Rows[i]["TOTAL_CAR"].ToString());

                        Txt1 = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        if (string.Equals(Txt1.Text.Trim(), string.Empty))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน)");
                        if (!int.TryParse(Txt1.Text.Trim(), out tmp))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน) เป็นตัวเลข");
                        if (MaxNumber < int.Parse(Txt1.Text.Trim()))
                            throw new Exception("จำนวนรถที่ป้อน มีค่ามากกว่า จำนวนรถที่ถูกร้องเรียน");

                        TotalCar += int.Parse(Txt1.Text.Trim());
                    }
                }
                return TotalCar;
            }
            else
                return this.CountCar();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSaveIsCost()
    {
        try
        {
            //decimal CostFinal = string.Equals(txtCostFinal.Text.Trim(), string.Empty) ? 0 : decimal.Parse(txtCostFinal.Text.Trim());
            //decimal CostOther = string.Equals(txtCostOther.Text.Trim(), string.Empty) ? 0 : decimal.Parse(txtCostOther.Text.Trim());

            if (string.Equals(radCostCheck.SelectedValue, "0") && !string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
                throw new Exception("กรณีชำระค่าปรับยังไม่ครบถ้วน ห้ามระบุ วันที่ชำระครบถ้วน");

            if (string.Equals(radCostCheck.SelectedValue, "1") && string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
                throw new Exception("กรณีชำระค่าปรับครบถ้วน ให้ระบุ วันที่ชำระครบถ้วน");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int CountCar()
    {
        try
        {
            int CountCar = 0;

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                if (Chk.Checked)
                {
                    CountCar += 1;
                }
            }

            if (CountCar == 0)
                throw new Exception("กรุณาเลือก ทะเบียนรถ ที่ต้องการตัดคะแนน");

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdUploadTab3_Click(object sender, EventArgs e)
    {
        this.StartUploadTab3();
    }

    private void StartUploadTab3()
    {
        try
        {
            if (cboUploadTypeTab3.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUploadTab3.HasFile)
            {
                string FileNameUser = fileUploadTab3.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFileTab3(System.IO.Path.GetExtension(FileNameUser), fileUploadTab3.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadTab3.SaveAs(Path + "\\" + FileNameSystem);

                dtUploadTab3.Rows.Add(cboUploadTypeTab3.SelectedValue, cboUploadTypeTab3.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFileTab3(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFileTab3_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadTab3.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFileTab3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFileTab3.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    protected void dgvUploadFileTab3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void ddlScore1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore1", "txtScore1Final", "Score1");
    }

    protected void ddlScore2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore2", "txtScore2Final", "Score2");
    }

    protected void ddlScore3_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore3", "txtScore3Final", "Score3");
    }

    protected void ddlScore4_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore4", "txtScore4Final", "Score4");
    }

    protected void dgvScoreList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            IndexEdit = Index;

            if (string.Equals(e.CommandName, "Select"))
            {
                DataTable dtTmp = dtScoreList.Copy();
                //string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ภาพลักษณ์องค์กร", "ความพึงพอใจลูกค้า", "กฎ/ระเบียบฯ", "แผนงานขนส่ง", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };

                string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };
                dtTmp.Columns["STOPICNAME"].ColumnName = "หัวข้อตัดคะแนน";
                dtTmp.Columns["Cost"].ColumnName = "ค่าปรับ";
                dtTmp.Columns["DisableDriver"].ColumnName = "ระงับ พขร. (วัน)";
                //dtTmp.Columns["Score1"].ColumnName = "ภาพลักษณ์องค์กร";
                //dtTmp.Columns["Score2"].ColumnName = "ความพึงพอใจลูกค้า";
                //dtTmp.Columns["Score3"].ColumnName = "กฎ/ระเบียบฯ";
                //dtTmp.Columns["Score4"].ColumnName = "แผนงานขนส่ง";
                dtTmp.Columns["Score5"].ColumnName = "ผลคูณความรุนแรง";
                dtTmp.Columns["Score6"].ColumnName = "หักคะแนนต่อครั้ง";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                for (int i = 0; i < dtTmp.Columns.Count; i++)
                {
                    if (display.Contains(dtTmp.Columns[i].ColumnName))
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dtTmp.Columns[i].ColumnName + " : " + dtTmp.Rows[Index][i].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    if (i == dtTmp.Columns.Count - 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        //sb.Append("ทะเบียนรถ : " + this.GetCarList(dtTmp.Rows[Index]["STOPICID"].ToString()));
                        if (rowListTotalCarTab3.Visible)
                        {
                            //sb.Append("จำนวนรถ : " + dtTmp.Rows[Index]["TotalCar"].ToString() + " คัน");
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        else
                        {
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
            //else if (string.Equals(e.CommandName, "EditHeader"))
            //{
            //    ServiceIDEdit = dtHeader.Rows[Index]["SSERVICEID"].ToString();
            //    cmdAddHeader.Text = "บันทึก";
            //    cmdCancel.Visible = true;

            //    cmbTopic.SelectedValue = dtHeader.Rows[Index]["TOPICID"].ToString();
            //    cmbTopic_SelectedIndexChanged(null, null);

            //    cboComplainType.SelectedValue = dtHeader.Rows[Index]["COMPLAINID"].ToString();
            //    cbxOrganiz.SelectedValue = dtHeader.Rows[Index]["ORGANIZID"].ToString();
            //    chkLock.Checked = (string.Equals(dtHeader.Rows[Index]["LOCKDRIVERID"].ToString(), "0") ? false : true);
            //    rblTypeProduct.Value = dtHeader.Rows[Index]["TYPEPRODUCTID"].ToString();
            //    txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

            //    cboVendor.SelectedValue = dtHeader.Rows[Index]["VENDORID"].ToString();
            //    cboVendor_SelectedIndexChanged(null, null);

            //    cboContract.SelectedValue = dtHeader.Rows[Index]["CONTRACTID"].ToString();
            //    cboContract_SelectedIndexChanged(null, null);

            //    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
            //    cboHeadRegist_SelectedIndexChanged(null, null);

            //    if (!string.Equals(dtHeader.Rows[Index]["DELIVERYID"].ToString(), string.Empty))
            //        cboDelivery.SelectedValue = dtHeader.Rows[Index]["DELIVERYID"].ToString();

            //    cmbPersonalNo.SelectedValue = dtHeader.Rows[Index]["PERSONALID"].ToString();
            //    txtDateTrans.Text = (dtHeader.Rows[Index]["DELIVERYDATE"] != null) ? dtHeader.Rows[Index]["DELIVERYDATE"].ToString() : string.Empty;
            //    //teTimeTrans.Text = dtHeader.Rows[Index]["DELIVERYTIME"].ToString();
            //    txtComplainAddress.Value = dtHeader.Rows[Index]["COMPLAINADDRESS"].ToString();
            //    txtTotalCar.Value = dtHeader.Rows[Index]["TOTALCAR"].ToString();
            //}
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private int GetCarListCount(string STOPICID)
    {
        try
        {
            int CountCar = 0;
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (rowListTotalCarTab3.Visible)
                        CountCar += int.Parse(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                    else
                        CountCar += 1;
                }
            }

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetCarListString(string STOPICID)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (sb.Length > 0)
                        sb.Append(", ");

                    if (rowListTotalCarTab3.Visible)
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                        sb.Append(" คัน");
                    }
                    else
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["CARHEAD"].ToString());
                        if (!string.Equals(dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), string.Empty))
                        {
                            sb.Append("/");
                            sb.Append(dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
                        }
                    }
                }
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdChangeStatusConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            AccidentBLL.Instance.AccidentChangeStatus(hidID.Value, 3, Session["UserID"] + string.Empty);
            //this.SendEmail(ConfigValue.EmailComplainRequest);
            //alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void lblSentencerDetail_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlSentencer.SelectedIndex == 0)
                throw new Exception("กรุณา เลือก คณะกรรมการพิจารณาโทษ");

            DataTable dtDetail = ComplainBLL.Instance.SentencerSelectDetailBLL(ddlSentencer.SelectedValue);
            if (dtDetail.Rows.Count > 0)
            {
                dtDetail.Columns["NLIST"].ColumnName = "ลำดับ";
                dtDetail.Columns["CODE"].ColumnName = "รหัสพนักงาน";
                dtDetail.Columns["FNAME"].ColumnName = "ชื่อ - นามสกุล";
                dtDetail.Columns["SPOSITION"].ColumnName = "ตำแหน่ง";
                dtDetail.Columns["SJOB"].ColumnName = "ตำแหน่งในคณะทำงาน";
                dtDetail.Columns["CACTIVE"].ColumnName = "สถานะ";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                sb.Append("<tr style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                for (int i = 0; i < dtDetail.Columns.Count; i++)
                {
                    sb.Append("<td style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                    sb.Append("&nbsp;&nbsp;" + dtDetail.Columns[i].ColumnName + "&nbsp;&nbsp;");
                    sb.Append("</td>");
                }
                sb.Append("</tr>");

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    sb.Append("<tr style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                    for (int j = 0; j < dtDetail.Columns.Count; j++)
                    {

                        if (string.Equals(dtDetail.Columns[j].ColumnName, "สถานะ"))
                        {
                            sb.Append("<td style=\"border-color:cadetblue; border-style:solid; border-width:1px; text-align:center\">");
                            if (string.Equals(dtDetail.Rows[i][j].ToString(), "1"))                             //Active
                                sb.Append("&nbsp;&nbsp;" + "&#10003;" + "&nbsp;&nbsp;");
                            else
                                sb.Append("&nbsp;&nbsp;" + "&#x2718;" + "&nbsp;&nbsp;");    //Not Active
                        }
                        else
                        {
                            sb.Append("<td style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                            sb.Append("&nbsp;&nbsp;" + dtDetail.Rows[i][j].ToString() + "&nbsp;&nbsp;");
                        }
                        sb.Append("</td>");
                    }

                    sb.Append("</tr>");
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void radCusScoreType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void radSentencerType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (radSentencerType.SelectedIndex == 0)
            {
                ddlSentencer.SelectedIndex = 0;
                ddlSentencer.Enabled = false;
            }
            else
            {
                ddlSentencer.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region gvFine_RowUpdating
    protected void gvFine_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        CreateDataAccFine();
        dt.Rows[e.RowIndex].Delete();
        dt.AcceptChanges();
        sumAmount = 0;
        for (int i = 0; i < dt.Rows.Count - 1; i++)
        {
            dt.Rows[i]["NO"] = i + 1;
            if (decimal.TryParse(dt.Rows[i]["AMOUNT"] + string.Empty, out Amount))
            {
                sumAmount += Amount;
            }
        }
        dt.Rows.Add("", "จำนวนเงินที่ชำระแล้ว", sumAmount);
        gvFine.DataSource = dt;
        gvFine.DataBind();
        CostCheck();
    }
    #endregion

    #region CreateDataAccFine
    private void CreateDataAccFine()
    {
        dt = new DataTable("DT");
        dt.Columns.Add("NO", typeof(string));
        dt.Columns.Add("FILENAMES", typeof(string));
        dt.Columns.Add("AMOUNT", typeof(string));
        sumAmount = 0;
        if (gvFine.Rows.Count > 0)
        {
            for (int i = 0; i < gvFine.Rows.Count - 1; i++)
            {
                GridViewRow row = gvFine.Rows[i];
                HiddenField hidNO = (HiddenField)row.FindControl("hidNO");
                HiddenField hidFILENAMES = (HiddenField)row.FindControl("hidFILENAMES");
                HiddenField hidAMOUNT = (HiddenField)row.FindControl("hidAMOUNT");
                dt.Rows.Add(i + 1, hidFILENAMES.Value, hidAMOUNT.Value);
                if (decimal.TryParse(hidAMOUNT.Value, out Amount))
                {
                    sumAmount += Amount;
                }

            }
        }
    }
    #endregion

    #region btnFineAdd_Click
    protected void btnFineAdd_Click(object sender, EventArgs e)
    {
        string mess = string.Empty;
        if (string.IsNullOrEmpty(txtNo.Text.Trim()))
        {
            mess += "กรุณาป้อนครั้งที่";
        }
        if (ddlFileName.SelectedIndex <= 0)
        {
            mess += "กรุณาป้อนชื่อไฟล์(ตามผู้ใช้งาน)";
        }
        if (string.IsNullOrEmpty(txtAmountAdd.Text.Trim()))
        {
            mess += "<br/>กรุณาป้อนจำนวนเงิน";
        }
        if (string.IsNullOrEmpty(mess))
        {

            CreateDataAccFine();
            DataRow[] drs = dt.Select("FILENAMES = '" + ddlFileName.SelectedValue + "'");
            if (drs.Any())
            {
                alertFail("File " + ddlFileName.SelectedValue + "ได้ทำการชำระไปแล้วกรุณาเลือกไฟล์ใหม่");
                return;
            }
            dt.Rows.Add(txtNo.Text.Trim(), ddlFileName.SelectedValue, txtAmountAdd.Text.Trim());
            if (decimal.TryParse(txtAmountAdd.Text.Trim(), out Amount))
            {
                sumAmount += Amount;
            }
            dt.Rows.Add("", "จำนวนเงินที่ชำระแล้ว", sumAmount);
            gvFine.DataSource = dt;
            gvFine.DataBind();
            CostCheck();

        }
        else
        {
            alertFail(mess);
        }
    }
    #endregion

    #region CostCheck
    private void CostCheck()
    {
        if (decimal.TryParse(txtAmount.Text.Trim(), out Amount))
        {
            if (sumAmount == Amount)
            {
                radCostCheck.SelectedValue = "1";
                btnFineAdd.Enabled = false;
            }
            else if (Amount == 0)
            {
                radCostCheck.SelectedValue = "0";
                btnFineAdd.Enabled = true;
            }
            else if (sumAmount < Amount)
            {
                radCostCheck.SelectedValue = "2";
                btnFineAdd.Enabled = true;
            }

            sumAmount += Amount;
        }
    }
    #endregion
}