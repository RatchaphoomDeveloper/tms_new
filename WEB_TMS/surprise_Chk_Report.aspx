﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="surprise_Chk_Report.aspx.cs" Inherits="surprise_Chk_Report" StylesheetTheme="Aqua" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="server">
    <div class="panel panel-default" style="width: 98%; padding: 10px; margin: 10px">
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#SurpriceCheckReport" aria-controls="SurpriceCheckReport" role="tab" data-toggle="tab">ภาพรวมการตรวจรถ</a></li>
                <li><a href="#SurpriceCheckReportYear" aria-controls="SurpriceCheckReportYear" role="tab" data-toggle="tab">ตารางการตรวจประจำปี</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                <div role="tabpanel" class="tab-pane active" id="SurpriceCheckReport">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ข้อมูลทั่วไป&#8711;</a>
                                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ปี" runat="server" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlYearAll" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="เขตการขนส่ง" runat="server" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlgroup" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlgroup_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="บริษัทขนส่ง" runat="server" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right" runat="server" id="div2">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" OnClick="btnSearch_Click" CssClass="btn btn-info" />
                                            <asp:Button ID="btnClear" runat="server" Text="เคลียร์" OnClick="btnClear_Click" CssClass="btn btn-danger" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>

                                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <center>
                                                    <asp:Chart ID="Chart1" runat="server"
                                                        BorderlineWidth="0" Height="360px" Palette="None"
                                                        Width="380px" BorderlineColor="64, 0, 64">
                                                        <Titles>
                                                            <asp:Title ShadowOffset="0" Name="Items" />
                                                        </Titles>
                                                        <Legends>
                                                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                                                LegendStyle="Row" />
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Default" />
                                                        </Series>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right" runat="server" id="divButton">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click" CssClass="btn btn-success" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnExport" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                                    <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract2">
                                    <div class="panel-body">
                                        <asp:GridView runat="server" DataKeyNames="ID" ID="dgvSurpriseCheckReport" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvSurpriseCheckReport_PageIndexChanging"
                                            OnRowUpdating="dgvSurpriseCheckReport_RowUpdating" OnRowDataBound="dgvSurpriseCheckReport_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="NAME" HeaderText="กลุ่มงานขนส่ง">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="GROUPNAME" HeaderText="เขตการขนส่ง">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TYPE_NAME" HeaderText="ผลิตภัณฑ์">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SABBREVIATION" HeaderText="บริษัทขนส่ง">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_JAN" HeaderText="มกราคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_FEB" HeaderText="กุมภาพันธ์">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_MAR" HeaderText="มีนาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_APR" HeaderText="เมษายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_MAY" HeaderText="พฤษภาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_JUN" HeaderText="มิถุนายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_JUL" HeaderText="กรกฏาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_AUG" HeaderText="สิงหาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_SEP" HeaderText="กันยายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_OCT" HeaderText="ตุลาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_NOV" HeaderText="พฤศจิกายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_DEC" HeaderText="ธันวาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNTCHECK_ALL" HeaderText="จำนวนรถที่ผ่านการตรวจทั้งหมด">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_TRUCKALL" HeaderText="จำนวนรถทั้งหมด">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PERCENTALL" HeaderText="ความครบถ้วน(%)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="panel-footer" style="text-align: right">
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div role="tabpanel" class="tab-pane" id="SurpriceCheckReportYear">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ข้อมูลทั่วไป&#8711;</a>
                                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ปี" runat="server" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="เขตการขนส่ง" runat="server" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlgroupYear" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlgroupYear_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="บริษัทขนส่ง" runat="server" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="ddlVendorYear" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right" runat="server" id="div3">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnSearchYear" runat="server" Text="ค้นหา" OnClick="btnSearchYear_Click" CssClass="btn btn-info" />
                                            <asp:Button ID="btnClearYear" runat="server" Text="เคลียร์" OnClick="btnClearYear_Click" CssClass="btn btn-danger" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <input type="hidden" id="hiddencollapseFindContract3" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract3">
                                    <div class="panel-body">
                                        <div class="row form-group">

                                            <div class="col-md-12">
                                                <center>
                                                    <asp:Chart ID="Chart3" runat="server"
                                                        BorderlineWidth="0" Height="360px" Palette="None"
                                                        Width="380px" BorderlineColor="64, 0, 64">
                                                        <Titles>
                                                            <asp:Title ShadowOffset="0" Name="Items" />
                                                        </Titles>
                                                        <Legends>
                                                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                                                LegendStyle="Row" />
                                                        </Legends>
                                                        <Series>
                                                            <asp:Series Name="Default" />
                                                        </Series>
                                                        <ChartAreas>
                                                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                                                        </ChartAreas>
                                                    </asp:Chart>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right" runat="server" id="div1">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnExportYear" runat="server" Text="Export" OnClick="btnExportYear_Click" CssClass="btn btn-success" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnExportYear" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract1" id="acollapseFindContract1">แสดงข้อมูล&#8711;</a>
                                    <input type="hidden" id="hiddencollapseFindContract1" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract1" style="width: 1400px">
                                    <div class="panel-body" style="overflow: auto">
                                        <asp:GridView runat="server" DataKeyNames="ID" ID="dgvSurpriseChkReportYear" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" PageSize="10" AllowPaging="true"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvSurpriseChkReportYear_PageIndexChanging"
                                            OnRowUpdating="dgvSurpriseChkReportYear_RowUpdating" OnRowDataBound="dgvSurpriseChkReportYear_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="NAME" HeaderText="กลุ่มงานขนส่ง">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TYPE_NAME" HeaderText="ผลิตภัณฑ์">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="GROUPNAME" HeaderText="เขตการขนส่ง">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STATUS_NAME" HeaderText="แผน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_JAN" HeaderText="มกราคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_FEB" HeaderText="กุมภาพันธ์">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_MAR" HeaderText="มีนาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_APR" HeaderText="เมษายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_MAY" HeaderText="พฤษภาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_JUN" HeaderText="มิถุนายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_JUL" HeaderText="กรกฏาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_AUG" HeaderText="สิงหาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_SEP" HeaderText="กันยายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_OCT" HeaderText="ตุลาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_NOV" HeaderText="พฤศจิกายน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_DEC" HeaderText="ธันวาคม">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNTCHECK_ALL" HeaderText="จำนวนรถ(คัน)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PERCENTCHECK" HeaderText="ความคืบหน้า(%)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="COUNT_TRUCKALL" HeaderText="จำนวนรถทั้งหมด">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PERCENTALL" HeaderText="ความครบถ้วน(%)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="panel-footer" style="text-align: right">
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnExportYear" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
