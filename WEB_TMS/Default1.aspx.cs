﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Text;
using System.Web.Security;
using EmailHelper;

public partial class _default1 : PageBase
{
    string sqlCon = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string _DefaultPassword = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //txtPassword.Text = "p@ssw0rd";
        cmbSWWCODE.ItemsRequestedByFilterCondition += new ListEditItemsRequestedByFilterConditionEventHandler(cmbSWWCODE_ItemsRequestedByFilterCondition);
        cmbSWWCODE.ItemRequestedByValue += new ListEditItemRequestedByValueEventHandler(cmbSWWCODE_ItemRequestedByValue);

        if (!IsPostBack)
        {
            //TestWebService();
            if ("" + User.Identity.Name != "")
            {
                string[] userindname = User.Identity.Name.Split('\\');
                if (userindname.Length >= 1)
                {//ถ้าค่า ที่ดึง จากAD Split แล้ว lengthมากกว่าเท่ากับ 1
                    //Response.Write("AD[1] :" + userindname[1]);
                    if (userindname[1] != "")
                    {
                        DataTable dt = new DataTable();

                        // Ping
                        #region + Old Code +
                        //string squery = "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE  FROM TUSER WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(userindname[1]) + "') ";
                        #endregion

                        #region + New Code +
                        string squery = "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE " +
                            " , USERGROUP_NAME , M_USERGROUP.USERGROUP_ID , IS_ADMIN  " + 
                            " FROM TUSER " +
                            " INNER JOIN M_USERGROUP ON TUSER.USERGROUP_ID = M_USERGROUP.USERGROUP_ID " +
                            " WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(userindname[1]) + "') ";
                        #endregion

                        dt = CommonFunction.Get_Data(sqlCon, squery);

                        if (dt.Rows.Count > 0)
                        {
                            Session["UserID"] = dt.Rows[0][0] + "";
                            Session["SVDID"] = dt.Rows[0][1] + "";
                            Session["CGROUP"] = dt.Rows[0][2] + "";
                            Session["UserName"] = dt.Rows[0][4] + "";
                            Session["LoginID"] = dt.Rows[0][3] + "";
                            Session["UserGroup"] = dt.Rows[0]["USERGROUP_NAME"];
                            Session["CHKCHANGEPASSWORD"] = dt.Rows[0]["CHKDATE"] + "";
                            Session["UserGroupID"] = dt.Rows[0]["USERGROUP_ID"];
                            Session["UserLogin"] = dt;
                            string surl = ("" + Session["CGROUP"] == "0") ? "vendor_home.aspx" : (("" + Session["CGROUP"] == "1") ? "admin_home.aspx" : "Pagehome.aspx");
                            //ส่งคาไป เมื่อendcallback เพื่อ redirect ไปหน้าอื่น
                            Response.Redirect(surl);
                        }
                    }
                }
            }

         
            try
            {
                if (Request.QueryString != null && !string.Equals(Request.QueryString.ToString(), string.Empty))
                {
                    
                    txtUsername.Text = Request.QueryString["Username"].ToString();
                    txtPassword.Text = Request.QueryString["Password"].ToString();

                    DevExpress.Web.ASPxClasses.CallbackEventArgsBase tmp = new DevExpress.Web.ASPxClasses.CallbackEventArgsBase("login");
                    xcpn_Callback(null, tmp);
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }

    }
    
    void cmbSWWCODE_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        comboBox.Items.Clear();
        String Selectgrp = CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(rblgroup.Value.ToString()));
        switch (Selectgrp)
        {

            case "0":
                sdsOrganiz.SelectCommand = "SELECT SVENDORID AS VENDOR_CODE, SABBREVIATION AS VENDOR_ABBR FROM (SELECT ven.SVENDORID, ven.SABBREVIATION, ROW_NUMBER()OVER(ORDER BY ven.SVENDORID ) AS RN FROM TVendor ven LEFT  JOIN TVENDOR_SAP vensap ON ven.SVENDORID = vensap.SVENDORID WHERE  1=1   AND ven.INUSE = '1' AND NVL(ven.CACTIVE,'Y') != '0' AND ven.SABBREVIATION LIKE '%" + e.Filter + "%' OR ven.SVENDORID = '" + e.Filter + "') ";//WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'
                //sdsOrganiz.SelectCommand = "SELECT SVENDORID AS VENDOR_CODE, SVENDORNAME AS VENDOR_ABBR FROM (SELECT v.SVENDORID, VS.SVENDORNAME, ROW_NUMBER()OVER(ORDER BY VS.SVENDORNAME ) AS RN FROM  TVENDOR v INNER JOIN TVENDOR_SAP vs ON V.SVENDORID = VS.SVENDORID WHERE VS.SVENDORNAME LIKE '%" + e.Filter + "%' OR V.SVENDORID = '" + e.Filter + "')  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";

                break;

            case "1":
                //sdsOrganiz.SelectCommand = @"SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR,1 AS ROWDATA FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT WHERE UNITCODE LIKE '5%' OR UNITCODE LIKE '8%') UNION SELECT '','- เลือกข้อมูล -',0 FROM Dual ORDER BY ROWDATA,VENDOR_CODE";
                sdsOrganiz.SelectCommand = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT WHERE UNITNAME LIKE '%" + e.Filter + "%' OR UNITCODE = '" + e.Filter + "')  ";//WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'

                break;

            case "2":
                sdsOrganiz.SelectCommand = @"SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR,1 AS ROWDATA FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE '" + PlantHelper.Plant5 + "' OR T.STERMINALID LIKE '" + PlantHelper.Plant8 + "') AND TS.STERMINALNAME LIKE '%" + e.Filter + "%' AND T.CACTIVE = '1' AND T.CSTATUS = '1' AND T.sabbreviation IS NOT NULL) ";//WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'
                //sdsOrganiz.SelectCommand = "SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR FROM (SELECT T.STERMINALID , TS.STERMINALNAME , ROW_NUMBER()OVER(ORDER BY TS.STERMINALNAME ) AS RN FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID WHERE TS.STERMINALNAME LIKE '%" + e.Filter + "%' OR T.STERMINALID = '" + e.Filter + "')  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";

                break;
        }
        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();

        if (comboBox.Items.Count > 0)
        {
            comboBox.SelectedIndex = 0;
        }

    }
    //สร้าง ข้อมูล ใน autocomplete
    void cmbSWWCODE_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    //event ของ callback panel
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        int count;
        lblErrorMsg.ClientVisible = false;
        switch (e.Parameter)
        {
            case "login":
                //เมื่อกดปุ่ม login
                if (!String.IsNullOrEmpty(txtUsername.Text + "") || !String.IsNullOrEmpty(txtPassword.Text + ""))
                {
                    var EncodePass = STCrypt.encryptMD5(txtPassword.Text);
                    DataTable dt = new DataTable();

                    //Ping
                    #region + Old Code +
                    //dt = CommonFunction.Get_Data(sqlCon, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE  FROM TUSER WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(txtUsername.Text.Trim()) + "') AND SPASSWORD = '" + EncodePass + "' AND CACTIVE = '1'");                    
                    #endregion

                    #region + New Code +
                    dt = CommonFunction.Get_Data(sqlCon, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE " +
                    " , USERGROUP_NAME, M_USERGROUP.USERGROUP_ID , IS_ADMIN " +
                    " FROM TUSER INNER " + 
                    " JOIN M_USERGROUP ON TUSER.USERGROUP_ID = M_USERGROUP.USERGROUP_ID " + 
                    " WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(txtUsername.Text.Trim()) + "') AND SPASSWORD = '" + EncodePass + "' AND CACTIVE = '1'");
                    #endregion

                    if (dt.Rows.Count > 0)
                    {
                        Session["UserID"] = dt.Rows[0][0] + "";
                        Session["SVDID"] = dt.Rows[0][1] + "";
                        Session["CGROUP"] = dt.Rows[0][2] + "";
                        Session["UserName"] = dt.Rows[0][4] + "";
                        Session["LoginID"] = dt.Rows[0][3] + "";
                        Session["UserGroup"] = dt.Rows[0]["USERGROUP_NAME"] + "";
                        Session["CHKCHANGEPASSWORD"] = dt.Rows[0]["CHKDATE"] + "";
                        Session["UserGroupID"] = dt.Rows[0]["USERGROUP_ID"];
                        Session["UserLogin"] = dt;
                        string surl = ("" + Session["CGROUP"] == "0") ? "vendor_home.aspx" : (("" + Session["CGROUP"] == "1") ? "admin_home.aspx" : "Pagehome.aspx");
                        //ส่งคาไป เมื่อendcallback เพื่อ redirect ไปหน้าอื่น

                        if (Request.QueryString != null && !string.Equals(Request.QueryString.ToString(), string.Empty))
                            Response.Redirect(surl, false);
                        else
                            xcpn.JSProperties["cpRedirectTo"] = surl;
                        return;
                    }
                    else
                    {
                        string msg = "";
                        dt = CommonFunction.Get_Data(sqlCon, "SELECT SUID,SVENDORID,REPLACE(CGROUP,' ','') CGROUP,SUSERNAME,CACTIVE,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE  FROM TUSER WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(txtUsername.Text.Trim()) + "') AND SPASSWORD = '" + EncodePass + "'");
                        if (dt.Rows.Count > 0)
                        {

                            switch (dt.Rows[0]["CACTIVE"] + "")
                            {
                                case "1":
                                    if (txtUsernameHist.Text == txtUsername.Text)
                                    {
                                        count = int.TryParse(txtCount.Text, out count) ? count : 0;
                                        txtCount.Text = (++count) + "";
                                    }
                                    else
                                    {
                                        txtUsernameHist.Text = txtUsername.Text;
                                        txtCount.Text = "1";
                                    }
                                    msg = "Username/Password ของท่านไม่ถูกต้องครั้งที่ " + txtCount.Text + " กรุณาตรวจสอบอีกครั้ง หากผิดพลาดเกิน 3 ครั้งระบบจะ Lock การใช้งานของ Username ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
                                    break;

                                case "0":
                                    msg = "Username ของท่านไม่มีสิทธิ์เข้าระบบ ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
                                    break;
                                case "2":
                                    msg = "Username ของท่านถูก Lock ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
                                    break;
                            }
                        }
                        else
                        {
                            if (txtUsernameHist.Text == txtUsername.Text)
                            {
                                count = int.TryParse(txtCount.Text, out count) ? count : 0;
                                txtCount.Text = (++count) + "";
                            }
                            else
                            {
                                txtUsernameHist.Text = txtUsername.Text;
                                txtCount.Text = "1";
                            }
                            msg = "Username/Password ของท่านไม่ถูกต้องครั้งที่ " + txtCount.Text + " กรุณาตรวจสอบอีกครั้ง หากผิดพลาดเกิน 3 ครั้งระบบจะ Lock การใช้งานของ Username ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
                        }
                        //lblErrorMsg.ClientVisible = true;
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','" + msg + "');");
                    }
                    if (int.TryParse(txtCount.Text, out count) ? count >= 3 : false)
                    {
                        string msg = "Username ของท่านถูกล๊อคแล้ว กรุณาติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
                        //lblErrorMsg.ClientVisible = true;
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','" + msg + "');");
                        //function lock userid
                        LockUsername(txtUsername.Text);
                    }
                }
                break;
            case "cancel":
                //เมื่อกดปุ่ม cancel
                txtUsername.Text = "";
                txtPassword.Text = "";
                txtUsernameHist.Text = "";
                txtCount.Text = "0";
                break;
            case "popup":
                //เมื่อกดปุ่มตกลง ในpopup ลืม password
                SentEmail(CommonFunction.ReplaceInjection(txtForgetUsername.Text), CommonFunction.ReplaceInjection(cmbSWWCODE.Value + ""), CommonFunction.ReplaceInjection(txtEmail.Text));
                break;
        }
    }
    private void SentEmail(string _Username, string _SWWCODE, string _Email)
    {
        //function sendmail username and new password 
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        dt1 = CommonFunction.Get_Data(sqlCon, "SELECT SEMAIL FROM TUSER  WHERE SVENDORID = '" + _SWWCODE + "'");
        if (dt1.Rows.Count > 0)
        {
            dt2 = CommonFunction.Get_Data(sqlCon, "SELECT SEMAIL FROM TUSER  WHERE SVENDORID = '" + _SWWCODE + "' AND SUSERNAME = '" + _Username + "'");
            if (dt2.Rows.Count > 0)
            {
                dt3 = CommonFunction.Get_Data(sqlCon, "SELECT SEMAIL,SFIRSTNAME||' '||SLASTNAME AS FULL_NAME FROM TUSER  WHERE SVENDORID = '" + _SWWCODE + "' AND SUSERNAME = '" + _Username + "' AND SEMAIL = '" + _Email + "'");
                if (dt3.Rows.Count > 0)
                {

                    using (OracleConnection con = new OracleConnection(sqlCon))
                    {
                        con.Open();
                        DataTable dtPW = CommonFunction.Get_Data(sqlCon, "select FN_GENERATE_PASSWORD as PASSWORD from dual");
                        _DefaultPassword = dtPW.Rows[0]["PASSWORD"].ToString().Trim();
                        string sql = "UPDATE TUSER SET SPASSWORD = '" + STCrypt.encryptMD5(_DefaultPassword) + "' WHERE SUSERNAME = '" + _Username + "'";
                        using (OracleCommand com = new OracleCommand(sql, con))
                        {
                            com.ExecuteNonQuery();
                        }
                    }

                    string str = @"<table>
        <tr><td colspan='2'>เรียน {0}</td></tr>
        <tr><td colspan='2'>Username และ Password ของท่านคือ</td></tr>
        <tr><td>Username :</td><td>{1}</td></tr>
        <tr><td>Password :</td><td>{2}</td></tr>
        </table>";
                    string _from = ConfigurationManager.AppSettings["SystemMail"].ToString();
                    string _to = dt3.Rows[0][0] + "";

                    if (ConfigurationManager.AppSettings["usemail"].ToString() == "0")
                    {
                        _from = ConfigurationManager.AppSettings["demoMailSend"].ToString();
                        _to = ConfigurationManager.AppSettings["demoMailRecv"].ToString();
                    }

                    //CommonFunction.SendMail(_from, _to, "แจ้ง Username และ Password", string.Format(str, dt.Rows[0][0] + "", _Username, _DefaultPassword), "");
                    MailService.SendMail(_to, "แจ้ง Username และ Password ระบบ TMS", string.Format(str, dt3.Rows[0][1] + "", _Username, _DefaultPassword));

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ระบบได้ส่ง Username และ Password ไปยังที่อยู่ Email ของท่านแล้ว กรุณาตรวจสอบตามที่อยู่ Email ของท่าน',function(){dxPopupInfo.Hide();});");
                }
                else
                {
                    //dt3
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Error + "','Email ของท่านไม่ตรงกับขอมูลที่มีในระบบ',function(){dxPopupInfo.Hide();});");
                }
            }
            else
            {
                //dt2
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Error + "','Username ของท่านไม่ตรงกับขอมูลที่มีในระบบ',function(){dxPopupInfo.Hide();});");
            }
        }
        else
        {
            //dt1
            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Error + "','หน่วยงาน ของท่านไม่ตรงกับขอมูลที่มีในระบบ',function(){dxPopupInfo.Hide();});");
        }
    }
    private void LockUsername(string _Username)
    {
        using (OracleConnection con = new OracleConnection(sqlCon))
        {
            con.Open();
            string sql = "UPDATE TUSER SET CACTIVE = '2' WHERE SUSERNAME = '" + _Username + "'";
            using (OracleCommand com = new OracleCommand(sql, con))
            {
                com.ExecuteNonQuery();
            }
        }

        UserTrace trace = new UserTrace(this, sqlCon);
        trace.SUID = _Username + "";
        trace.SMENUID = "100";
        trace.SCREATE = _Username + "";
        trace.STYPE = "F";
        trace.SDESCRIPTION = "ป้อน Password  ผิดเกิน 3 ครั้ง";
        trace.SREFERENTID = "";
        trace.Insert();
    }

}