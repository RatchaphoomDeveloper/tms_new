﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_vendor_lst.aspx.cs" Inherits="admin_vendor_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>

    <style type="text/css">
        .style13
        {
            height: 23px;
        }
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="75%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" 
                                NullText="กรุณาป้อนข้อมูลที่ต้องการค้นหา" Style="margin-left: 0px" 
                                Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="right">
                            ค้นหาจาก&nbsp;&nbsp; </td>
                        <td width="8%" class="style13">
                            <dx:ASPxComboBox ID="cbxGroup" runat="server" Width="90px" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="ผู้ประกอบการ" Value="0" />
                                    <dx:ListEditItem Text="จังหวัด" Value="1" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="SVENDORID"
                                SkinID="_gvw" DataSourceID="sds"  SettingsPager-PageSize="10">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption = "ที่" HeaderStyle-HorizontalAlign="Center" Width="1%" VisibleIndex="1">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                       <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัสผู้ประกอบการ" VisibleIndex="1" 
                                        FieldName="SVENDORID" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ชื่อบริษัทผู้ประกอบการ" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                        Width="25%" FieldName="SVENDORNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="จังหวัด" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center" Width="15%" FieldName="SPROVINCE" >
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ชื่อผู้ประสานงาน" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SCONAMEAPPEND" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หมายเลขโทรศัพท์" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SCOTELAPPEND" Width="15%">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="7">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                             <Image Width="16px" Height="16px" Url="Images/search.png" ></Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        
                                        <CellStyle  HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT v.SVENDORID, s.SVENDORNAME, s.SPROVINCE, wm_concat(' ' || c.SCOORDINATORNAME) As SCONAMEAPPEND,wm_concat(' ' || c.STEL)  As SCOTELAPPEND
FROM (TVENDOR v INNER JOIN TVENDOR_SAP s ON v.SVENDORID = s.SVENDORID) LEFT JOIN TCO_VENDOR c ON v.SVENDORID = c.SVENDORID 
WHERE nvl(V.CACTIVE,'Y') != 'N' AND NVL(v.CCATEGORY,'C') = 'C' 
AND s.SVENDORNAME||NVL(CONT.SCONTRACTNO,'') LIKE '%' || :oSearch || '%'
GROUP BY v.SVENDORID,s.SVENDORNAME, s.SPROVINCE"
                                     
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                               
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtSearch" Name="oSearch" 
                                        PropertyName="Text" />
                                </SelectParameters>
                               
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td align="right" width="60%">
                            &nbsp;</td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
