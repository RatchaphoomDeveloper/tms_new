﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxUploadControl;
using System.Web.Configuration;
using System.IO;
using System.Data.OleDb;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxTabControl;
using System.Configuration;
using DevExpress.Web.ASPxGridView;
using System.Collections.ObjectModel;
using System.Security.Permissions;
using System.Diagnostics;
using TMS_BLL.Transaction.OrderPlan;
using TMS_BLL.Transaction.ContractConfirm;
using TMS_BLL.Master;
using GemBox.Spreadsheet;
using System.Drawing;

public partial class OrderPlan : PageBase
{
    #region Member
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string connIVMS = WebConfigurationManager.ConnectionStrings["IVMSConnectionString"].ConnectionString;
    private const string sPathSave = "/ORDER_PLAN/{0}/{1}/";
    //private static string UserID = "";
    //private static string sPathFile = "";
    //private static DataTable dt = new DataTable();
    //private static List<TData> lstData = new List<TData>();
    //private static string MENUID = "";

    private List<TData> lstData { get { return SystemFunction.ConvertObject<TData>(Session["lstData"]); } set { Session["lstData"] = value; } }
    private DataTable dt { get { return SystemFunction.ConvertObject(Session["dt"]); } set { Session["dt"] = value; } }
    private string sPathFile { get { return Session["sPathFile"] + ""; } set { Session["sPathFile"] = value; } }
    private string MENUID { get { return Session["MENUID"] + ""; } set { Session["MENUID"] = value; } }
    #endregion


    private void ClearSessionStatic()
    {
        Session["lstData"] = "";
        Session["dt"] = "";
        Session["sPathFile"] = "";
        Session["MENUID"] = "";
    }

    private void NewLstStatic()
    {
        sPathFile = "";
        dt = new DataTable();
        lstData = new List<TData>();
        MENUID = "";

    }

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        string str = Request.QueryString["str"];
        if (!string.IsNullOrEmpty(str))
        {

            if (str == "1")
            {
                ASPxPageControl1.TabPages[0].Visible = false;

            }
            else
            {
                ASPxPageControl1.TabPages[1].Visible = false;
                ASPxPageControl1.TabPages[2].Visible = false;
                ASPxPageControl1.TabPages[3].Visible = false;
            }
        }
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        gvwPop.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwPop_CustomColumnDisplayText);
        gvw.AfterPerformCallback += new DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventHandler(gvw_AfterPerformCallback);
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        gvw.HtmlRowPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventHandler(gvw_HtmlRowPrepared);
        //gvw.CommandButtonInitialize += new DevExpress.Web.ASPxGridView.CommandButtonInitialize
        //gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        gvwT3.AfterPerformCallback += new DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventHandler(gvwT3_AfterPerformCallback);
        gvwT3.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvwT3_HtmlDataCellPrepared);

        //gvwT3.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwT3_HtmlDataCellPrepared);
        gvwT1.HtmlRowPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventHandler(gvwT1_HtmlRowPrepared);
        gvwT1.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvwT1_HtmlDataCellPrepared);
        gvwT4.AfterPerformCallback += new DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventHandler(gvwT4_AfterPerformCallback);
        //gvwT4.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwT4_HtmlDataCellPrepared);
        gvwT1.CustomColumnSort += new ASPxGridViewCustomColumnSortEventHandler(gvwT1_CustomColumnSort);
        
        this.Culture = "en-US";
        this.UICulture = "en-US";
        CheckBoxHeader();
        if (!IsPostBack)
        {

            ClearSessionStatic();
            NewLstStatic();
            //#region เช็คสิทธิ์
            //getOldAuthenticateCode();  //ถ้าเอาขึ้นโปร ต้องเปิดใช้งาน   
            //#endregion
            MENUID = "58";
            LogUser(MENUID, "R", "เปิดดูข้อมูลหน้า จัดรายการขนส่งให้ลูกค้า", "");

            CreateFolder("/UploadFile/ProgreassBar/");
            uclOrder.AdvancedModeSettings.TemporaryFolder = "/UploadFile/ProgreassBar";


            string ExcelPath = "UploadFile/FileDownload/ORDER_PLAN/plan.xlsx";
            string PdfPath = "UploadFile/FileDownload/ORDER_PLAN/คู่มือจัดการแผน.pdf";

            btnExcelT2.ClientSideEvents.Click = "function(){ window.open('" + ExcelPath + "');}";
            btnPDFT2.ClientSideEvents.Click = "function(){  window.open('" + PdfPath + "');}";
            btnExcelT3.ClientSideEvents.Click = "function(){ window.open('" + ExcelPath + "');}";
            btnPdfT3.ClientSideEvents.Click = "function(){  window.open('" + PdfPath + "');}";


            ASPxPageControl1.Visible = true;

            //UserID = Session["UserID"] + "";



            cboTeminal.DataSource = sdsTeminal;
            cboTeminal.DataBind();

            cboTeminal.Value = Teminal_ID();


            lblCountCStanby.Text = " 0 ";
            lblDetailTeminal.Text = " 0 ";
            rblChoice.SelectedIndex = 0;
            edtCFT.Value = DateTime.Now;
            //edtStart.Value = DateTime.Now;
            //edtEnd.Value = DateTime.Now.AddDays(1);
            //edtStartT3.Value = DateTime.Now;
            //edtEndT3.Value = DateTime.Now.AddDays(1);
            //edtStartT4.Value = DateTime.Now;
            //edtEndT4.Value = DateTime.Now.AddDays(1);

            edtStart.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            edtEnd.Text = DateTime.Now.Date.AddDays(6).ToString("dd/MM/yyyy");
            edtStartT3.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            edtEndT3.Text = DateTime.Now.Date.AddDays(6).ToString("dd/MM/yyyy");
            edtStartT4.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            edtEndT4.Text = DateTime.Now.Date.AddDays(6).ToString("dd/MM/yyyy");

            //cboWindowTime.DataBind();
            //cboWindowTime.Items.Insert(0, new ListEditItem("- เลือก -", ""));
            //cboWindowTimeT3.DataBind();
            //cboWindowTimeT3.Items.Insert(0, new ListEditItem("- เลือก -", ""));

            cboWindowTimeT4.DataBind();
            cboWindowTimeT4.Items.Insert(0, new ListEditItem("- เลือก -", ""));

            //ListData();
            //ListT1();
            //gvwT1.ExpandAll();

            this.InitialForm();

            #region Check Permission
            this.AssignAuthen(); //ถ้าเอาขึ้นโปร ต้องปิดเปิดใช้งานทั้ง function   
            #endregion

        }
        else
        {
            ddlWarehouse.DataSource = ViewState["DataTTERMINAL"];
            ddlWarehouse.DataBind();
            ddlSTERMINAL.DataSource = ViewState["DataTTERMINAL"];
            ddlSTERMINAL.DataBind();
            ddlSTERMINALT3.DataSource = ViewState["DataTTERMINAL"];
            ddlSTERMINALT3.DataBind();
            //DropDownListHelper.BindDropDownList(ref ddlGroup, (DataTable)ViewState["DataGroup"], "ID", "NAME", true);
            if (ViewState["DataCONTRACT"] != null)
            {
                DropDownListHelper.BindDropDownList(ref cboContractSearch, (DataTable)ViewState["DataCONTRACT"], "SCONTRACTID", "SCONTRACTNO", true);
            }

        }
    }
            #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //จัดสรรงาน
                btnSearch.Enabled = false;
                btnExport1.Enabled = false;
                //ยืนยันแผนงาน
                ASPxButton4.Enabled = false; //ค้นหา
                btnExport2.Enabled = false;
                //แผนงานที่ถูกยกเลิก
                ASPxButton3.Enabled = false; //ค้นหา
            }
            if (!CanWrite)
            {   //จัดสรรงาน
                btnConfirm.Enabled = false;
                ASPxButton1.Enabled = false;
                btnSubmit.Enabled = false;
            }
            if (CanWrite)
            {
                txtPermission.Text = "1";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void getOldAuthenticateCode()
    {
        bool chkurl = false;
        string AddEdit = "";
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == "58")
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            chkurl = true;

                            break;

                        case "2":
                            chkurl = true;
                            AddEdit = "1";
                            txtPermission.Text = "1";
                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        if (chkurl == false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        Session["CheckPermission"] = AddEdit;
    }

    #region InitialForm
    private void InitialForm()
    {
        try
        {
            txtDateConfirm.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);

            //DataTable dtWarehouse = OrderPlanBLL.Instance.WarehouseSelectBLL();
            //DropDownListHelper.BindDropDownList(ref ddlWarehouse, dtWarehouse, "STERMINALID", "SABBREVIATION", true);

            DataTable dtCompany = VendorBLL.Instance.TVendorSapSelect();
            DropDownListHelper.BindDropDownList(ref ddlCompany, dtCompany, "SVENDORID", "SABBREVIATION", true);

            ViewState["DataTTERMINAL"] = ContractBLL.Instance.TTERMINALSelect();
            ddlWarehouse.DataSource = ViewState["DataTTERMINAL"];
            ddlWarehouse.DataBind();
            ddlSTERMINAL.DataSource = ViewState["DataTTERMINAL"];
            ddlSTERMINAL.DataBind();
            ddlSTERMINALT3.DataSource = ViewState["DataTTERMINAL"];
            ddlSTERMINALT3.DataBind();
            ViewState["DataGroup"] = WorkGroupBLL.Instance.GroupSelect(0, "%%", "1");
            DropDownListHelper.BindDropDownList(ref ddlGroup, (DataTable)ViewState["DataGroup"], "ID", "NAME", true);
            DropDownListHelper.BindDropDownList(ref ddlGroupT3, (DataTable)ViewState["DataGroup"], "ID", "NAME", true);
            //AAAAA ไม่ต้อง Load ข้อมูลตั้งแต่เปิด
            //if (Session["CGROUP"] + string.Empty == "1")
            //{
            //    //ddlWarehouse.Value = Session["SVDID"] + string.Empty;
            //    //ddlSTERMINAL.Value = Session["SVDID"] + string.Empty;
            //    //ddlSTERMINALT3.Value = Session["SVDID"] + string.Empty;
            //    ListData();
            //    ListData2();

            //}
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region Tab1
    #region ddlCompany_SelectedIndexChanged
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCompany.SelectedIndex > 0)
            {

                DataTable dtContract = OrderPlanBLL.Instance.ContractSelectBLL(ddlCompany.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
                DropDownListHelper.BindDropDownList(ref ddlWarehouse, null, string.Empty, string.Empty, true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    void gvwT1_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
    {
        if (e.Column.FieldName == "ShipName")
        {
            e.Handled = true;
            string s1 = e.Value1.ToString(), s2 = e.Value2.ToString();
            if (s1.Length > s2.Length)
                e.Result = 1;
            else
                if (s1.Length == s2.Length)
                    e.Result = System.Collections.Comparer.Default.Compare(s1, s2);
                else
                    e.Result = -1;
        }
    }

    void gvwT1_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "เลขที่สัญญา")
        {
            string Text = e.CellValue + "";
            if (Text.Contains("สัญญา"))
            {
                e.Cell.HorizontalAlign = HorizontalAlign.Center;
            }
        }
    }

    void gvwT1_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (!string.IsNullOrEmpty(e.GetValue("SABBREVIATION") + ""))
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#B2DFEE");
        }
        if (string.IsNullOrEmpty(e.GetValue("SABBREVIATION") + "") && !string.IsNullOrEmpty(e.GetValue("SCONTRACTNO") + ""))
        {
            if (e.Row.Cells.Count == 5)
            {
                e.Row.Cells[1].Style.Add("background-color", "#E0FFFF");
                e.Row.Cells[2].Style.Add("background-color", "#E0FFFF");
                e.Row.Cells[3].Style.Add("background-color", "#E0FFFF");
                e.Row.Cells[4].Style.Add("background-color", "#E0FFFF");
            }
        }

    }

    private void SearchTab1()
    {
        try
        {
            //this.ListT1();
            //gvwT1.ExpandAll();

            //DateTime ConditionDate = DateTime.ParseExact(txtDateConfirm.Text.Trim(), "dd/MM/yyyy", null);
            //ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");

            string VendorID = "%";
            if (ddlCompany.SelectedIndex > 0)
                VendorID = ddlCompany.SelectedValue;

            string ContractNo = "%";
            if (ddlContract.SelectedIndex > 0)
                ContractNo = ddlContract.SelectedItem.Text;

            DataTable dt = ContractConfirmBLL.Instance.SelectTruckConfirmPTTBLL(VendorID, txtDateConfirm.Text.Trim(), ContractNo, "1", "%" + txtSheadregisterno.Text.Trim() + "%", ddlWarehouse.Value + string.Empty, (cbCSTANBY.Checked ? "N" : "Y"));
            xgvw.DataSource = dt;
            xgvw.DataBind();

            int count = dt.AsEnumerable().Sum(it => int.Parse(it["COUNTNSUM"] + string.Empty));
            lblDetailTeminal.Text = count + string.Empty;
            count = dt.AsEnumerable().Sum(it => int.Parse(it["COUNTCSTANBY"] + string.Empty));
            lblCountCStanby.Text = count + string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSearchTab1_Click(object sender, EventArgs e)
    {
        this.SearchTab1();
    }

    protected void xgvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        try
        {
            ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");
            if (xgvwCar == null) CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('ข้อความจากระบบ','ระบบไม่สามารถแสดงรายการ รถ <br>ได้เนื่องจากการทำงานที่ผิดพลาดหรือนานเกินไป กรุณาตรวจสอบ อีกครั้ง!')");
            string CallbackName = (e.CallbackName.Equals("STARTEDIT") || e.CallbackName.Equals("CANCELEDIT") || e.CallbackName.Equals("SORT")) ? e.CallbackName : e.Args[0].ToUpper();//switch CallbackName 

            switch (e.CallbackName)
            {
                case "SORT":
                case "CANCELEDIT":
                    xgvw.CancelEdit();
                    this.SearchTab1();
                    break;
                case "STARTEDIT":
                    BindTruckWithDataInGridEditing(xgvwCar, "STARTEDIT");
                    break;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void xgvwCar_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            if (e.DataColumn.Caption.Equals("ที่"))
            {
                e.Cell.Text = string.Format("{0}.", e.VisibleIndex + 1);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void xgvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            if (e.DataColumn.Caption == "ยืนยันแล้ว")
            {

                ASPxTextBox txtnTruckConfirm = xgvw.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)xgvw.Columns["ยืนยันแล้ว"], "txtnTruckConfirm") as ASPxTextBox;
                int NCONFIRM = 0;
                if (Session["ss_data"] != null)
                {
                    NCONFIRM = ((DataTable)Session["ss_data"]).Select("CSEND='1'  AND DDATE='" + ((DateTime)e.GetValue("DDATE")).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND SCONTRACTID='" + e.GetValue("SCONTRACTID") + "' ").Length;
                    txtnTruckConfirm.Value = (NCONFIRM <= 0) ? "" + e.GetValue("NCONFIRM") : "" + NCONFIRM;
                    txtnTruckConfirm.Attributes.Add("title", NCONFIRM + "/" + e.GetValue("NONHAND"));
                }
                else
                {
                    txtnTruckConfirm.Value = "" + e.GetValue("NCONFIRM");
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        //ASPxButton btnEdit = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["การจัดการ"], "btnEdit") as ASPxButton;
        ASPxButton btnEdit = gvw.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)gvw.Columns["การจัดการ"], "btnEdit") as ASPxButton;
        ASPxButton btnDel = gvw.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)gvw.Columns["การจัดการ"], "btnDel") as ASPxButton;

        if (e.DataColumn.FieldName == "CHECKLATE")
        {
            

            CheckBox cbSelect = gvw.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)e.DataColumn, "cbSelect") as CheckBox;
            if (cbSelect != null)
            {
                //e.CellValue
                //dynamic data = gvw.GetRowValues(e.VisibleIndex, "CHECKLATE");
                // if (data + string.Empty == "1")
                cbSelect.Checked = e.CellValue + string.Empty == "1" ? true : false;

            }
        }

        if (!CanWrite)
        {
            btnEdit.Enabled = false;
            btnDel.Enabled = false;
        }
    }

    void gvw_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (!string.IsNullOrEmpty(e.GetValue("DATETIMEEXPECT") + ""))
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#ccc");
        }
        string color = "#000";
        ASPxLabel lblDATETIMEEXPECT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "lblDATETIMEEXPECT") as ASPxLabel;
        if (!string.IsNullOrEmpty(e.GetValue("DATETIMEEXPECT") + string.Empty))
        {
            DateTime? DATETIMEEXPECT = null, DATETIMETOTERMINAL = null;
            string[] strDATETIMEEXPECT = (e.GetValue("DATETIMEEXPECT") + string.Empty).Trim().Split(' ');
            string[] strDATETIMEEXPECTDate, strDATETIMEEXPECTTime;
            if (strDATETIMEEXPECT.Any())
            {
                strDATETIMEEXPECTDate = strDATETIMEEXPECT[0].Split('/');
                strDATETIMEEXPECTTime = strDATETIMEEXPECT[1].Split(':');
                DATETIMEEXPECT = new DateTime(int.Parse(strDATETIMEEXPECTDate[2]), int.Parse(strDATETIMEEXPECTDate[1]), int.Parse(strDATETIMEEXPECTDate[0]), int.Parse(strDATETIMEEXPECTTime[0]), int.Parse(strDATETIMEEXPECTTime[1]), 0);
            }
            if (DATETIMEEXPECT != null)
            {
                if (DATETIMEEXPECT < DateTime.Now)
                {
                    color = "#FF0A0A";
                }
                //else
                //{
                //    color = "#FF0A0A";
                //}
                lblDATETIMEEXPECT.ForeColor = System.Drawing.ColorTranslator.FromHtml(color);
            }
        }


    }
    #endregion

    #region Tab2
    void gvw_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");
        
        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');
                int Inx = 0;
                if (param.Length > 1)
                {
                    Inx = int.Parse(param[1] + "");
                }
                //ASPxTextBox txtSDELIVERYNO = gvw.FindEditFormTemplateControl("txtSDELIVERYNO") as ASPxTextBox;
                //ASPxComboBox cboDelivery = gvw.FindEditFormTemplateControl("cboDelivery") as ASPxComboBox;
                //ASPxComboBox cboTRANTYPE = gvw.FindEditFormTemplateControl("cboTRANTYPE") as ASPxComboBox;
                ASPxComboBox cboVendor = gvw.FindEditFormTemplateControl("cboVendor") as ASPxComboBox;
                ASPxComboBox cboContract = gvw.FindEditFormTemplateControl("cboContract") as ASPxComboBox;
                TextBox txtDate = gvw.FindEditFormTemplateControl("txtDate") as TextBox;
                DropDownList ddlNLINE = gvw.FindEditFormTemplateControl("ddlNLINE") as DropDownList;
                TextBox txtLateDay = gvw.FindEditFormTemplateControl("txtLateDay") as TextBox;
                TextBox txtLateTime = gvw.FindEditFormTemplateControl("txtLateTime") as TextBox;
                dynamic data = gvw.GetRowValues(Inx, "ORDERID");

                switch (param[0])
                {
                    case "savegvw":
                        if (CanWrite) //ถ้าเอาขึ้นโปร ต้อง ปิด ใช้งาน   
                        //if ("" + Session["CheckPermission"] == "1") //ถ้าเอาขึ้นโปร ต้อง เปิด ใช้งานทั้ง    
                        {
                            try
                            {
                                string QUERY_HIS = "SELECT ORDERID,NVERSION FROM  TBL_ORDERPLAN_HISTORY WHERE ORDERID = '" + data + "' ORDER BY NVERSION DESC";
                                DataTable dt = CommonFunction.Get_Data(conn, QUERY_HIS);
                                string NVERSION = dt.Rows.Count > 0 ? (int.Parse(dt.Rows[0]["NVERSION"] + "") + 1) + "" : "1";
                                INS_TO_HISTORY(data, NVERSION);

                                //                            string QUERY = @"UPDATE TBL_ORDERPLAN
                                //                                         SET 
                                //                                         SDELIVERYNO = '" + cboDelivery.Value + @"',
                                //                                         SVENDORID = '" + cboVendor.Value + @"',
                                //                                         SABBREVIATION = '" + txtVendorname.Text + @"',
                                //                                         SCONTRACTID = " + cboContract.Value + @",
                                //                                         SCONTRACTNO = '" + cboContract.Text + @"',
                                //                                         ORDERTYPE = '" + cboTRANTYPE.Value + @"',
                                //                                         DATE_UPDATE = SYSDATE,
                                //                                         SUPDATE = '" + UID + @"'
                                //                                         WHERE ORDERID = '" + data + "'";
                                string QUERY = @"UPDATE TBL_ORDERPLAN
                                         SET 
                                         SVENDORID = '" + cboVendor.Value + @"',
                                         SABBREVIATION = '" + txtVendorname.Text + @"',
                                         SCONTRACTID = " + cboContract.Value + @",
                                         SCONTRACTNO = '" + cboContract.Text + @"',
                                         DATE_UPDATE = SYSDATE,
                                         SUPDATE = '" + UID + @"',
                                        ddelivery = to_date('" + txtDate.Text + @"','dd/mm/yyyy'),
                                         nwindowtimeid = " + ddlNLINE.SelectedValue + @",
                                         LATEDAY = " + (string.IsNullOrEmpty(txtLateDay.Text.Trim()) ? (object)"NULL" : txtLateDay.Text.Trim()) + @",
                                         LATETIME = '" + txtLateTime.Text.Trim() + @"'
                                         WHERE ORDERID = '" + data + "'";

                                AddTODB(QUERY);
                                gvw.CancelEdit();
                                ListData();
                                if (SendMail(data))
                                {
                                    CommonFunction.SetPopupOnLoad(gvwT3, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
                                }
                                else
                                {
                                    CommonFunction.SetPopupOnLoad(gvwT3, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "'บันทึกข้อมูลเรียบร้อยแล้ว<br>แต่่ไม่สามารถส่ง E-Mail ไปยัง ผู้ขนส่งได้')");
                                }
                            }
                            catch
                            {
                                CommonFunction.SetPopupOnLoad(gvw, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Resources.CommonResource.Msg_Error + "')");
                            }
                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                        }
                        break;

                    case "cancel": gvw.CancelEdit();
                        break;
                    case "DDLONCHANGE":
                        string value = param[2];
                        for (int i = 0; i < gvw.VisibleRowCount; i++)
                        {
                            CheckBox cbSelect = gvw.FindRowCellTemplateControl(i, null, "cbSelect") as CheckBox;
                            cbSelect.Checked = (value != "true" ? false : true);
                        }
                        break;


                }
                break;
            
            case "PAGERONCLICK":
                switch (ASPxPageControl1.ActiveTabIndex)
                {
                    case 0:
                        break;
                    case 1:
                        ListData();
                        break;
                    case 2:
                        ListData2();
                        break;
                    case 3:
                        ListData3();
                        break;
                    default:
                        break;
                }
                break;
        }
        CheckBoxHeader();
    }

    #endregion

    #region Tab3
    void gvwT3_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");

        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');
                int Inx = 0;
                if (param.Length > 1)
                {
                    Inx = int.Parse(param[1] + "");
                }

                ASPxComboBox cboVendor = gvwT3.FindEditFormTemplateControl("cboVendor") as ASPxComboBox;
                ASPxComboBox cboContract = gvwT3.FindEditFormTemplateControl("cboContractT3") as ASPxComboBox;
                TextBox txtDate = gvwT3.FindEditFormTemplateControl("txtDate") as TextBox;
                DropDownList ddlNLINE = gvwT3.FindEditFormTemplateControl("ddlNLINE") as DropDownList;
                TextBox txtLateDay = gvwT3.FindEditFormTemplateControl("txtLateDay") as TextBox;
                TextBox txtLateTime = gvwT3.FindEditFormTemplateControl("txtLateTime") as TextBox;
                switch (param[0])
                {
                    case "savegvwT3":

                        try
                        {
                            dynamic data = gvwT3.GetRowValues(Inx, "ORDERID", "SDELIVERYNO", "NWINDOWTIMEID", "STERMINALID");
                            string ORDERID = data[0] + "";
                            string SDELIVERYNO = data[1] + "";
                            string NWINDOWTIMEID = data[2] + "";
                            string STERMINALID = data[3] + "";
                            string QUERY_HIS = "SELECT ORDERID,NVERSION FROM  TBL_ORDERPLAN_HISTORY WHERE ORDERID = '" + ORDERID + "' ORDER BY NVERSION DESC";
                            DataTable dt = CommonFunction.Get_Data(conn, QUERY_HIS);
                            string NVERSION = dt.Rows.Count > 0 ? (int.Parse(dt.Rows[0]["NVERSION"] + "") + 1) + "" : "1";
                            INS_TO_HISTORY(ORDERID, NVERSION);
                            string SCONTRACTID = cboContract.Value + "";
                            string SCONTRACTNO = cboContract.Text;
                            string SVENDORID = cboVendor.Value + "";

                            Re_Plan(SDELIVERYNO, NWINDOWTIMEID, STERMINALID);

                            // string RePlan = Re_Plan(SDELIVERYNO, ORDERID, SCONTRACTID, SCONTRACTNO, SVENDORID);

                            string QUERY = @"UPDATE TBL_ORDERPLAN
                                         SET 
                                         SVENDORID = '" + SVENDORID + @"',
                                         SABBREVIATION = '" + txtVendornameT3.Text + @"',
                                         SCONTRACTID = " + SCONTRACTID + @",
                                         SCONTRACTNO = '" + SCONTRACTNO + @"',
                                         DATE_UPDATE = SYSDATE,
                                         SUPDATE = '" + UID + @"',
                                        ddelivery = to_date('" + txtDate.Text + @"','dd/mm/yyyy'),
                                         nwindowtimeid = " + ddlNLINE.SelectedValue + @",
                                         LATEDAY = " + (string.IsNullOrEmpty(txtLateDay.Text.Trim()) ? (object)"NULL" : txtLateDay.Text.Trim()) + @",
                                         LATETIME = '" + txtLateTime.Text.Trim() + @"'
                                         WHERE ORDERID = '" + ORDERID + "'";

                            AddTODB(QUERY);
                            gvwT3.CancelEdit();
                            ListData2();
                            if (SendMail(ORDERID))
                            {
                                CommonFunction.SetPopupOnLoad(gvwT3, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
                            }
                            else
                            {
                                CommonFunction.SetPopupOnLoad(gvwT3, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','บันทึกข้อมูลเรียบร้อยแล้ว<br>แต่่ไม่สามารถส่ง E-Mail ไปยัง ผู้ขนส่งได้')");
                            }
                        }
                        catch
                        {
                            CommonFunction.SetPopupOnLoad(gvwT3, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Resources.CommonResource.Msg_Error + "')");
                        }

                        break;

                    case "cancel": gvwT3.CancelEdit();
                        break;

                }
                break;
            case "PAGERONCLICK":
                switch (ASPxPageControl1.ActiveTabIndex)
                {
                    case 0:
                        break;
                    case 1:
                        ListData();
                        break;
                    case 2:
                        ListData2();
                        break;
                    case 3:
                        ListData3();
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    void gvwT3_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        //ASPxButton btnEdit = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["การจัดการ"], "btnEdit") as ASPxButton;
        ASPxButton btnEdit2 = gvwT3.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)gvwT3.Columns["การจัดการ"], "btnEdit2") as ASPxButton;
        ASPxButton btnDel2 = gvwT3.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)gvwT3.Columns["การจัดการ"], "btnDel2") as ASPxButton;

        if (!CanWrite) //ถ้าเอาขึ้นโปร ต้อง ปิด ใช้งาน   
        {
            btnEdit2.Enabled = false;
            btnDel2.Enabled = false;
        }
    }
    #endregion

    #region Tab4
    void gvwT4_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        ListData3();
        //throw new NotImplementedException();
    }

    #endregion

    #region xcpn_Load
    protected void xcpn_Load(object sender, EventArgs e)
    {

        switch (ASPxPageControl1.ActiveTabIndex)
        {
            case 0:
                this.SearchTab1();
                //ListT1();

                break;
            case 1:

                //ListData();

                break;
            case 2:

                ListData2();

                break;
            case 3:

                ListData3();

                break;

        }
    }
    #endregion

    #region xcpn_Callback
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }


        string[] paras = e.Parameter.Split(';');

        int inx = 0;
        if (paras.Length > 1)
        {
            string sss = txtIndx.Text;
            inx = int.Parse(paras[1] + "");
        }



        switch (paras[0])
        {
            case "UPLOAD":
                ExcelToDataTable(sPathFile);
                break;

            case "SaveAndList":
                AddDataToDataBase();
                ListData();
                break;

            case "Search":

                switch (ASPxPageControl1.ActiveTabIndex)
                {
                    case 0:
                        //ListT1();
                        //gvwT1.ExpandAll();
                        this.SearchTab1();
                        xgvw.CancelEdit();
                        break;
                    case 1: ListData();
                        CheckBoxHeader();
                        break;
                    case 2: ListData2();
                        break;
                    case 3:
                        ListData3();
                        break;
                }


                break;
            case "Claer":
                switch (ASPxPageControl1.ActiveTabIndex)
                {
                    case 0:
                        ddlCompany.SelectedIndex = 0;
                        ddlWarehouse.Value = null;
                        ddlContract.Items.Clear();
                        txtSheadregisterno.Text = string.Empty;
                        cbCSTANBY.Checked = false;
                        txtDateConfirm.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        break;
                    case 1: //ListData();
                        ddlGroup.SelectedIndex = 0;
                        cboWindowTime.SelectedIndex = 0;
                        cboVendorSearch.Value = null;
                        cboContractSearch.Value = null;
                        txtSearch2.Text = string.Empty;
                        ddlSTERMINAL.Value = null;
                        break;
                    case 2: //ListData2();
                        ddlGroupT3.SelectedIndex = 0;
                        ddlNLINET3.SelectedIndex = 0;
                        cboVendorSearchT3.Value = null;
                        cboContractSearchT3.Value = null;
                        txtSearch3.Text = string.Empty;
                        ddlSTERMINALT3.Value = null;
                        break;
                    case 3:
                        //ListData3();
                        break;
                }

                //this.SearchTab1();
                break;
            case "DDLONCHANGE":
                string value = paras[2];
                for (int i = 0; i < gvw.VisibleRowCount; i++)
                {
                    CheckBox cbSelect = gvw.FindRowCellTemplateControl(i, null, "cbSelect") as CheckBox;
                    if (cbSelect != null)
                    {
                        cbSelect.Checked = (value != "true" ? false : true);
                    }
                    
                }
                gvw.UpdateEdit();
                break;
            case "IVMS":
                for (int i = 0; i < gvw.VisibleRowCount; i++)
                {
                    CheckBox cbSelect = gvw.FindRowCellTemplateControl(i, null, "cbSelect") as CheckBox;
                    dynamic dataCheckIVMS = gvw.GetRowValues(i, "SDELIVERYNO", "SPLNT_CODE", "SHIP_TO", "LATEDATE", "LATETIME", "ORDERID");
                    if (cbSelect != null)
                    {
                        if (cbSelect.Checked)
                        {
                            if (!string.IsNullOrEmpty(dataCheckIVMS[3] + string.Empty))
                            {
                                DateTime Latetime = Convert.ToDateTime(dataCheckIVMS[3] + string.Empty);
                                if (!string.IsNullOrEmpty(dataCheckIVMS[4] + string.Empty))
                                {
                                    string[] str = (dataCheckIVMS[4] + string.Empty).Split('.');
                                    if (str.Any())
                                    {
                                        Latetime = Latetime.AddHours(int.Parse(str[0]));
                                        Latetime = Latetime.AddMinutes(int.Parse(str[1]));
                                    }
                                }
                                string mess = OrderPlanBLL.Instance.CheckLateFormIVMS(connIVMS, dataCheckIVMS[0] + string.Empty, dataCheckIVMS[1] + string.Empty, (dataCheckIVMS[2] + string.Empty).Replace("000009", ""), Latetime.ToString("dd/MM/yyyy HH:mm"), dataCheckIVMS[5] + string.Empty);
                                ASPxLabel lblDATETIMEEXPECT = gvw.FindRowCellTemplateControl(i, null, "lblDATETIMEEXPECT") as ASPxLabel;
                                if (lblDATETIMEEXPECT != null)
                                {
                                    lblDATETIMEEXPECT.Text = mess;
                                }

                            }
                        }
                        else
                        {
                            OrderPlanBLL.Instance.ClearCheckLate(dataCheckIVMS[5] + string.Empty);
                        }

                    }
                    
                }
                ListData();
                break;
            case "edit":
                gvw.StartEdit(inx);

                //ASPxTextBox txtSDELIVERYNO = gvw.FindEditFormTemplateControl("txtSDELIVERYNO") as ASPxTextBox;

                //ASPxComboBox cboDelivery = gvw.FindEditFormTemplateControl("cboDelivery") as ASPxComboBox;
                //ASPxComboBox cboTRANTYPE = gvw.FindEditFormTemplateControl("cboTRANTYPE") as ASPxComboBox;
                ASPxComboBox cboVendor = gvw.FindEditFormTemplateControl("cboVendor") as ASPxComboBox;
                ASPxComboBox cboContract = gvw.FindEditFormTemplateControl("cboContract") as ASPxComboBox;
                TextBox txtDate = gvw.FindEditFormTemplateControl("txtDate") as TextBox;
                DropDownList ddlNLINE = gvw.FindEditFormTemplateControl("ddlNLINE") as DropDownList;
                TextBox txtLateDay = gvw.FindEditFormTemplateControl("txtLateDay") as TextBox;
                TextBox txtLateTime = gvw.FindEditFormTemplateControl("txtLateTime") as TextBox;

                dynamic data = gvw.GetRowValues(inx, "SDELIVERYNO", "SVENDORID", "SCONTRACTID", "ORDERTYPE", "SABBREVIATION", "DDELIVERY", "NLINE", "TSTART", "TEND", "LATEDAY", "LATETIME");
                //txtSDELIVERYNO.Text = data[0] + "";
                //cboDelivery.Value = data[0] + "";
                cboVendor.Value = data[1] + "";
                //เซ็ตคา่ให้สัญญาเพื่อจะได้มีขอมูลมาแสดง
                if (!string.IsNullOrEmpty(cboVendor.Value + ""))
                {

                    DataTable dt = CommonFunction.Get_Data(conn, "SELECT SCONTRACTID,SCONTRACTNO,SVENDORID FROM TCONTRACT WHERE SVENDORID = '" + cboVendor.Value + "' AND CACTIVE = 'Y'");
                    if (dt.Rows.Count > 0)
                    {
                        cboContract.DataSource = dt;
                        cboContract.DataBind();
                    }
                }


                cboContract.Value = data[2] + "";
                //cboTRANTYPE.Value = data[3] + "";
                txtVendorname.Text = data[4] + "";
                if (!string.IsNullOrEmpty(data[5] + ""))
                {
                    DateTime ddt = DateTime.Parse(data[5] + "");
                    txtDate.Text = ddt.ToString("dd/MM/yyyy");
                }
                txtLateDay.Text = data[9] + string.Empty;
                txtLateTime.Text = data[10] + string.Empty;
                ddlNLINE.SelectedValue = data[6] + "";
                //txtTSTART.Text = data[7] + "";
                //txtTEND.Text = data[8] + "";
                LogUser(MENUID, "E", "แก้ไขแบ่งงานให้ผู้ขนส่ง", data[0] + "");
                break;

            case "editT3":
                gvwT3.StartEdit(inx);

                //ASPxTextBox txtSDELIVERYNO = gvw.FindEditFormTemplateControl("txtSDELIVERYNO") as ASPxTextBox;

                //ASPxComboBox cboDeliveryT3 = gvwT3.FindEditFormTemplateControl("cboDelivery") as ASPxComboBox;
                //ASPxComboBox cboTRANTYPET3 = gvwT3.FindEditFormTemplateControl("cboTRANTYPE") as ASPxComboBox;
                ASPxComboBox cboVendorT3 = gvwT3.FindEditFormTemplateControl("cboVendor") as ASPxComboBox;
                ASPxComboBox cboContractT3 = gvwT3.FindEditFormTemplateControl("cboContractT3") as ASPxComboBox;
                TextBox txtDate3 = gvwT3.FindEditFormTemplateControl("txtDate") as TextBox;
                DropDownList ddlNLINE3 = gvwT3.FindEditFormTemplateControl("ddlNLINE") as DropDownList;
                TextBox txtLateDay3 = gvwT3.FindEditFormTemplateControl("txtLateDay") as TextBox;
                TextBox txtLateTime3 = gvwT3.FindEditFormTemplateControl("txtLateTime") as TextBox;
                dynamic dataT3 = gvwT3.GetRowValues(inx, "SDELIVERYNO", "SVENDORID", "SCONTRACTID", "ORDERTYPE", "SABBREVIATION", "DDELIVERY", "NLINE", "TSTART", "TEND", "LATEDAY", "LATETIME");
                //txtSDELIVERYNO.Text = data[0] + "";

                //cboDeliveryT3.Value = dataT3[0] + "";
                cboVendorT3.Value = dataT3[1] + "";
                //เซ็ตคา่ให้สัญญาเพื่อจะได้มีขอมูลมาแสดง
                if (!string.IsNullOrEmpty(cboVendorT3.Value + ""))
                {

                    DataTable dt = CommonFunction.Get_Data(conn, "SELECT SCONTRACTID,SCONTRACTNO,SVENDORID FROM TCONTRACT WHERE SVENDORID = '" + cboVendorT3.Value + "'");
                    if (dt.Rows.Count > 0)
                    {
                        cboContractT3.DataSource = dt;
                        cboContractT3.DataBind();
                    }
                }


                cboContractT3.Value = dataT3[2] + "";
                //cboTRANTYPET3.Value = dataT3[3] + "";
                txtVendornameT3.Text = dataT3[4] + "";
                if (!string.IsNullOrEmpty(dataT3[5] + ""))
                {
                    DateTime ddt = DateTime.Parse(dataT3[5] + "");
                    txtDate3.Text = ddt.ToString("dd/MM/yyyy");
                }
                txtLateDay3.Text = dataT3[9] + string.Empty;
                txtLateTime3.Text = dataT3[10] + string.Empty;
                ddlNLINE3.SelectedValue = dataT3[6] + "";
                LogUser(MENUID, "E", "แก้ไขงานที่ผู้ขนส่งจัดลงแผน", dataT3[0] + "");
                break;


            case "delete":
                dynamic data2 = gvw.GetRowValues(inx, "ORDERID", "SDELIVERYNO");

                string ORDERID = data2[0] + "";
                string SDELIVERYNO = data2[1] + "";

                string Return_MultiORDER = "";
                Return_MultiORDER = UpdatePlanSch(SDELIVERYNO, ORDERID);

                LogUser(MENUID, "D", "ดึงแผนกลับแบ่งงานให้ผู้ขนส่ง", data2[1] + "");
                ListData();
                if (SendMailDel(Return_MultiORDER))
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "'บันทึกข้อมูลเรียบร้อยแล้ว<br>แต่่ไม่สามารถส่ง E-Mail ไปยัง ผู้ขนส่งได้')");
                }
                break;

            case "deleteT3":
                dynamic data2T3 = gvwT3.GetRowValues(inx, "ORDERID", "SDELIVERYNO", "LATEDATE", "LATETIME", "STERMINALID", "SHIP_TO", "FULLNAME", "SEMAIL");
                string ORDERIDT3 = data2T3[0] + "";
                string SDELIVERYNOT3 = data2T3[1] + "";
                string Return_MultiORDERT3 = "";
                Return_MultiORDERT3 = UpdatePlanSch(SDELIVERYNOT3, ORDERIDT3);
                LogUser(MENUID, "D", "ดึงแผนกลับงานที่ผู้ขนส่งจัดลงแผน", data2T3[1] + "");
                #region IVMS

                if (!string.IsNullOrEmpty(data2T3[2] + string.Empty))
                {

                    DateTime Latetime = Convert.ToDateTime(data2T3[2] + string.Empty);
                    if (!string.IsNullOrEmpty(data2T3[3] + string.Empty))
                    {
                        string[] str = (data2T3[3] + string.Empty).Split('.');
                        if (str.Any())
                        {
                            Latetime = Latetime.AddHours(int.Parse(str[0]));
                            Latetime = Latetime.AddMinutes(int.Parse(str[1]));
                        }
                    }
                    //ส่ง ทะเบียนรถเป็นค่าว่างไปกรณียกเลิกหรือลบแผน
                    string mess = PlanTransportBLL.Instance.GetLateFormIVMS(connIVMS, data2T3[1] + string.Empty, data2T3[4] + string.Empty, (data2T3[5] + string.Empty).Replace("000009", ""), "", Latetime.ToString("dd/MM/yyyy HH:mm"), data2T3[6] + string.Empty, data2T3[7] + string.Empty, data2T3[0] + string.Empty, string.Empty);

                }
                #endregion
                ListData2();
                if (SendMailDel(Return_MultiORDERT3))
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','บันทึกข้อมูลเรียบร้อยแล้ว<br>แต่่ไม่สามารถส่ง E-Mail ไปยัง ผู้ขนส่งได้')");
                }
                break;

            case "BACK": xcpn.JSProperties["cpRedirectTo"] = "vendor_HomeAlert.aspx";
                break;
            case "saveAdd":
                DataTable dtAdd = new DataTable();
                dtAdd.Columns.Add("Company", typeof(string));
                dtAdd.Columns.Add("Contract Number", typeof(string));
                dtAdd.Columns.Add("Job Type", typeof(string));
                dtAdd.Columns.Add("Delivery Date", typeof(string));
                dtAdd.Columns.Add("Time Windows", typeof(string));
                dtAdd.Columns.Add("Delivery", typeof(string));
                dtAdd.Columns.Add("LATEDAY", typeof(string));
                dtAdd.Columns.Add("LATETIME", typeof(string));
                DataRow dr = dtAdd.NewRow();
                dr["Company"] = (this.cboVendor.Text).Trim();
                dr["Contract Number"] = this.cboContract.Text.Trim();
                dr["Job Type"] = 1;
                dr["Delivery Date"] = this.txtDate.Text;
                dr["Time Windows"] = this.ddlNLINE.SelectedValue;
                dr["Delivery"] = txtDeliveryNoAdd.Text.Trim();
                dr["LATEDAY"] = this.txtLateDay.Text.Trim();
                dr["LATETIME"] = this.txtLateTime.Text.Trim();
                dtAdd.Rows.Add(dr);
                Session["dt"] = dtAdd;
                AddDataToDataBase();

                this.cboVendor.Value = null;
                this.cboContract.Value = null;
                this.txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                this.ddlNLINE.SelectedIndex = 0;
                txtDeliveryNoAdd.Text = string.Empty;
                this.txtLateDay.Text = "0";
                this.txtLateTime.Text = "00.00";
                break;
        }
        CheckBoxHeader();
    }
    #endregion

    #region UpdatePlanSch
    private string UpdatePlanSch(string SDELIVERYNO, string ORDER_ID)
    {
        string ORDERPLAN = "";
        string DEL_ORDER = "";
        //หาว่า DO นี้มี NPLANID อะไร แต่ถ้าไม่มีแสดงว่ายังไม่ได้จัดให้ลบได้เลย
        string QUERY_MULTIDROP = "SELECT NPLANID FROM TPLANSCHEDULELIST  WHERE SDELIVERYNO = '" + SDELIVERYNO + "' AND CACTIVE = '1'";
        DataTable DT_NPLANID = CommonFunction.Get_Data(conn, QUERY_MULTIDROP);

        string USER_ID = Session["UserID"] + "";

        if (DT_NPLANID.Rows.Count > 0)
        {
            //หา PLANID เพื่อไปหาทะเบียนรถ
            string CHK = "SELECT NPLANID,STRUCKID,STERMINALID FROM TPLANSCHEDULE WHERE NPLANID = '" + DT_NPLANID.Rows[0]["NPLANID"] + "'  AND CACTIVE = '1'";
            DataTable dt_PLAN = CommonFunction.Get_Data(conn, CHK);
            string STRUCKID = "";
            string STERMINAL = "";
            string NPLANID = "0";
            if (dt_PLAN.Rows.Count > 0)
            {
                STRUCKID = dt_PLAN.Rows[0]["STRUCKID"] + "";
                STERMINAL = dt_PLAN.Rows[0]["STERMINALID"] + "";
                NPLANID = dt_PLAN.Rows[0]["NPLANID"] + "";
            }
            else
            {
                //e.Result = "ไม่พบแผนจากเลข DO นี้";
                //return;
            }

            //หาทะเบียนหัว ทะเบียนหาง
            string Veh = "SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO FROM TTRUCK WHERE STRUCKID = '" + STRUCKID + "'";
            DataTable dt_Veh = CommonFunction.Get_Data(conn, Veh);
            string VehNo = "";
            string TuNo = "";
            if (dt_Veh.Rows.Count > 0)
            {
                VehNo = dt_Veh.Rows[0]["SHEADREGISTERNO"] + "";
                TuNo = dt_Veh.Rows[0]["STRAILERREGISTERNO"] + "";
            }

            //หาว่าแผนนั้นมี DO อะไรบ้าง
            string SELECT_SCH_LST = "SELECT NPLANID,SDELIVERYNO FROM TPLANSCHEDULELIST  WHERE NPLANID = " + NPLANID + " ORDER BY NDROP ASC";
            DataTable dt_SCH = CommonFunction.Get_Data(conn, SELECT_SCH_LST);
            if (dt_SCH.Rows.Count > 1)
            {
                for (int i = 0; i < dt_SCH.Rows.Count; i++)
                {
                    string SELECT_ORDER = "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN  WHERE SDELIVERYNO = '" + dt_SCH.Rows[i]["SDELIVERYNO"] + "' AND CACTIVE = 'Y'";
                    DataTable dt_ORPLAN = CommonFunction.Get_Data(conn, SELECT_ORDER);
                    if (dt_ORPLAN.Rows.Count > 0)
                    {
                        ORDERPLAN += ";'" + dt_ORPLAN.Rows[0]["ORDERID"] + "'";
                        DEL_ORDER += "," + dt_ORPLAN.Rows[0]["SDELIVERYNO"];
                        string QUERYT3 = "UPDATE TBL_ORDERPLAN SET CACTIVE = 'N',DATE_UPDATE = SYSDATE,SUPDATE = '" + USER_ID + "'  WHERE ORDERID = '" + dt_ORPLAN.Rows[0]["ORDERID"] + "' AND CACTIVE = 'Y'";
                        AddTODB(QUERYT3);
                    }

                }
            }
            else//มีแผนแต่ค่าไม่มากกว่า 1 แสดงว่า ไม่เป็น MultiDrop ลบ ORder ได้เลย
            {
                string QUERYT3 = "UPDATE TBL_ORDERPLAN SET CACTIVE = 'N',DATE_UPDATE = SYSDATE,SUPDATE = '" + USER_ID + "'   WHERE ORDERID = '" + ORDER_ID + "'";
                AddTODB(QUERYT3);
                ORDERPLAN = ";'" + ORDER_ID + "'";
                DEL_ORDER = "," + SDELIVERYNO;
            }

            //ลบข้อมูล Iterminal
            DataTable dt_DetailDEL = CommonFunction.Get_Data(conn, "SELECT * FROM TBL_ORDERPLAN WHERE ORDERID = '" + ORDER_ID + "' AND SDELIVERYNO = '" + SDELIVERYNO + "'");
            if (dt_DetailDEL.Rows.Count > 0)
            {
                string NTIMEWINDOW = dt_DetailDEL.Rows[0]["NWINDOWTIMEID"] + "";

                DEL_ORDER = !string.IsNullOrEmpty(DEL_ORDER) ? DEL_ORDER.Remove(0, 1) : "";
                ITEMINAL_DELETE(DEL_ORDER, NTIMEWINDOW, VehNo, TuNo, STERMINAL, NPLANID, DateTime.Now.ToString());
            }
            if (dt_SCH.Rows.Count > 0)
            {
                //ลบข้อมูลแผน
                string UPDATE_SCH = "UPDATE TPLANSCHEDULE SET  CACTIVE = '0',DUPDATE = sysdate WHERE NPLANID = '" + dt_SCH.Rows[0]["NPLANID"] + "'";
                AddTODB(UPDATE_SCH);
                //ลบข้อมูลแผนย่อย
                string UPDATE_SCH_LST = "UPDATE TPLANSCHEDULELIST SET  CACTIVE = '0'  WHERE NPLANID = '" + dt_SCH.Rows[0]["NPLANID"] + "'";
                AddTODB(UPDATE_SCH_LST);
            }


        }
        else//กรณีไม่เคยจัดแผนลบได้เลย
        {
            string QUERYT3 = "UPDATE TBL_ORDERPLAN SET CACTIVE = 'N',DATE_UPDATE = SYSDATE,SUPDATE = '" + USER_ID + "'  WHERE ORDERID = '" + ORDER_ID + "'";
            AddTODB(QUERYT3);
            ORDERPLAN = ";'" + ORDER_ID + "'";
        }

        //string UPDATE_SCH_LST = "UPDATE TPLANSCHEDULELIST SET  CACTIVE = '0' WHERE SDELIVERYNO = '" + SDELIVERYNO + "'";
        //AddTODB(UPDATE_SCH_LST);

        //string SELECT_SCH_LST = "SELECT NPLANID,SDELIVERYNO FROM TPLANSCHEDULELIST  WHERE SDELIVERYNO = '" + SDELIVERYNO + "'";
        //DataTable dt_SCH = CommonFunction.Get_Data(conn, SELECT_SCH_LST);
        //if (dt_SCH.Rows.Count > 0)
        //{
        //    string UPDATE_SCH = "UPDATE TPLANSCHEDULE SET  CACTIVE = '0' WHERE NPLANID = '" + dt_SCH.Rows[0]["NPLANID"] + "'";
        //    AddTODB(UPDATE_SCH);
        //}


        return ORDERPLAN;
    }

    #endregion


    private void Re_Plan(string DO_ID, string STIMEWINDOW, string STERMINAL)
    {
        //เอา DO ไปหาแผน
        string QUERY_MULTIDROP = "SELECT SDELIVERYNO,NPLANID FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(DO_ID) + "' AND CACTIVE = '1'";
        DataTable dt_ChkMultiDrop = CommonFunction.Get_Data(conn, QUERY_MULTIDROP);
        if (dt_ChkMultiDrop.Rows.Count > 0)
        {
            //เอา DO ไปหาแผน ที่เป็นมันติดรอปทั้งหมด
            string NPLANID = dt_ChkMultiDrop.Rows[0]["NPLANID"] + "";

            string QUERY_MULTIDROP2 = "SELECT SDELIVERYNO,NPLANID FROM TPLANSCHEDULELIST WHERE NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID) + "' AND CACTIVE = '1' ORDER BY NDROP ASC";
            DataTable dt_ChkMultiDrop2 = CommonFunction.Get_Data(conn, QUERY_MULTIDROP2);
            //ถ้าเป็นมัลติดรอปให้รีแผน
            if (dt_ChkMultiDrop2.Rows.Count > 1)
            {
                string QUERY_STRUCKID = "SELECT STRUCKID,NPLANID,STERMINALID FROM TPLANSCHEDULE WHERE NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID) + "' AND CACTIVE = '1'";
                DataTable dt_ChkMultiDropTruck = CommonFunction.Get_Data(conn, QUERY_STRUCKID);

                string VehNo = "";
                string TuNo = "";
                string TerminalID = "";
                if (dt_ChkMultiDropTruck.Rows.Count > 0)//หารถจาก TPLANSCHEDULE ใหม่เพื่อไม่ให้เกิดเรื่องบัคของข้อมูล
                {
                    TerminalID = dt_ChkMultiDropTruck.Rows[0]["STERMINALID"] + "";

                    string Veh = "SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO FROM TTRUCK WHERE STRUCKID = '" + dt_ChkMultiDropTruck.Rows[0]["STRUCKID"] + "'";
                    DataTable dt_Veh = CommonFunction.Get_Data(conn, Veh);
                    if (dt_Veh.Rows.Count > 0)
                    {
                        VehNo = dt_Veh.Rows[0]["SHEADREGISTERNO"] + "";
                        TuNo = dt_Veh.Rows[0]["STRAILERREGISTERNO"] + "";
                    }
                }

                //กรณีถ้า NPLAN มีจะวนลบก่อน
                string SPLAN_DELFIRST = "";
                for (int i = 0; i < dt_ChkMultiDrop2.Rows.Count; i++)
                {
                    SPLAN_DELFIRST += "," + dt_ChkMultiDrop2.Rows[i]["SDELIVERYNO"] + "";
                }
                //ลบแผนเก่าออกจาก Iterminal
                SPLAN_DELFIRST = !string.IsNullOrEmpty(SPLAN_DELFIRST) ? SPLAN_DELFIRST.Remove(0, 1) : "";
                string CLEAR = ITEMINAL_DELETE(SPLAN_DELFIRST, STIMEWINDOW, VehNo, TuNo, STERMINAL, NPLANID, DateTime.Now.ToString());

                //วันเพื่ออัพเดทดรอปใหม่
                AddTODB("UPDATE TPLANSCHEDULELIST SET  CACTIVE = 0 WHERE NPLANID = " + NPLANID + " AND SDELIVERYNO = '" + CommonFunction.ReplaceInjection(DO_ID) + "'");
                DataRow[] myResultSet = dt_ChkMultiDrop2.Select("SDELIVERYNO <> '" + CommonFunction.ReplaceInjection(DO_ID) + "'");
                SPLAN_DELFIRST = "";
                for (int i = 0; i < myResultSet.Count(); i++)
                {
                    AddTODB("UPDATE TPLANSCHEDULELIST SET  NDROP = " + (i + 1) + " WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(myResultSet[i]["SDELIVERYNO"] + "") + "' AND CACTIVE = '1' ");
                    SPLAN_DELFIRST += "," + myResultSet[i]["SDELIVERYNO"] + "";
                }

                //และเซฟแผนใหม่
                SPLAN_DELFIRST = !string.IsNullOrEmpty(SPLAN_DELFIRST) ? SPLAN_DELFIRST.Remove(0, 1) : "";
                string ADD = ITEMINAL_REPLAN(SPLAN_DELFIRST, STIMEWINDOW, VehNo, TuNo, STERMINAL, NPLANID, DateTime.Now.ToString());

                //ถ้าแผนที่สร้างจาก DO และ TPLANSCHEDULELIST ไม่มี Drop เลยจะตีเป็นไม่ใช้แผนนั้น
                if (myResultSet.Count() == 0)
                {
                    AddTODB("UPDATE TPLANSCHEDULE SET  CACTIVE = 0 WHERE NPLANID = '" + CommonFunction.ReplaceInjection(dt_ChkMultiDrop.Rows[0]["NPLANID"] + "") + "'");
                }

            }
            else// ไม่เป็นมัลติดรอปลบแผนได้เลย
            {

                string QUERY_STRUCKID = "SELECT STRUCKID,NPLANID,STERMINALID FROM TPLANSCHEDULE WHERE NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID) + "' AND CACTIVE = '1'";
                DataTable dt_ChkMultiDropTruck = CommonFunction.Get_Data(conn, QUERY_STRUCKID);

                string VehNo = "";
                string TuNo = "";
                string TerminalID = "";
                if (dt_ChkMultiDropTruck.Rows.Count > 0)//หารถจาก TPLANSCHEDULE ใหม่เพื่อไม่ให้เกิดเรื่องบัคของข้อมูล
                {
                    TerminalID = dt_ChkMultiDropTruck.Rows[0]["STERMINALID"] + "";

                    string Veh = "SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO FROM TTRUCK WHERE STRUCKID = '" + dt_ChkMultiDropTruck.Rows[0]["STRUCKID"] + "'";
                    DataTable dt_Veh = CommonFunction.Get_Data(conn, Veh);
                    if (dt_Veh.Rows.Count > 0)
                    {
                        VehNo = dt_Veh.Rows[0]["SHEADREGISTERNO"] + "";
                        TuNo = dt_Veh.Rows[0]["STRAILERREGISTERNO"] + "";
                    }
                }

                //กรณีถ้า NPLAN มีจะวนลบก่อน
                string SPLAN_DELFIRST = "";
                for (int i = 0; i < dt_ChkMultiDrop2.Rows.Count; i++)
                {
                    SPLAN_DELFIRST += "," + dt_ChkMultiDrop2.Rows[i]["SDELIVERYNO"] + "";
                }
                //ลบแผนเก่าออกจาก Iterminal
                SPLAN_DELFIRST = !string.IsNullOrEmpty(SPLAN_DELFIRST) ? SPLAN_DELFIRST.Remove(0, 1) : "";
                string CLEAR = ITEMINAL_DELETE(SPLAN_DELFIRST, STIMEWINDOW, VehNo, TuNo, STERMINAL, NPLANID, DateTime.Now.ToString());

                AddTODB("UPDATE TPLANSCHEDULE SET  CACTIVE = 0 WHERE NPLANID = " + NPLANID + "");
                AddTODB("UPDATE TPLANSCHEDULELIST SET  CACTIVE = 0 WHERE NPLANID = " + NPLANID + "");


            }

        }
        else
        {

        }


    }

    //private void ReplanIterminal_Edit(DataTable dt, string SDELIVERYNO,string NPLANID)
    //{
    //    DataRow[] myResultSet = dt.Select("SDELIVERYNO <> '" + CommonFunction.ReplaceInjection(SDELIVERYNO) + "'");
    //    if (myResultSet.Length > 0)
    //    {

    //    }
    //    else//
    //    {
    //        //ลบข้อมูลแผน
    //        string UPDATE_SCH = "UPDATE TPLANSCHEDULE SET  CACTIVE = '0' WHERE NPLANID = '" + NPLANID + "'";
    //        AddTODB(UPDATE_SCH);
    //        //ลบข้อมูลแผนย่อย
    //        string UPDATE_SCH_LST = "UPDATE TPLANSCHEDULELIST SET  CACTIVE = '0' WHERE NPLANID = '" + NPLANID + "'";
    //        AddTODB(UPDATE_SCH_LST);
    //    }


    //}

    protected void xcpnOutBound_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string paras = CommonFunction.ReplaceInjection(e.Parameter);

        using (OracleConnection con = new OracleConnection(conn))
        {
            if ("" + paras == "") return;
            int count = CommonFunction.Count_Value(con, "SELECT * FROM TDELIVERY_SAP WHERE LPAD(DELIVERY_NO,10,'0') =LPAD('" + CommonFunction.ReplaceInjection(paras) + "',10,'0')");

            string para = "";
            if (paras.Substring(0, 3) == "008")
            {
                para = !string.IsNullOrEmpty(paras) ? paras.Remove(0, 2) : "";
            }
            else
            {
                para = paras;
            }

            //if (count == 0)
            //{
            if (con.State == ConnectionState.Closed) con.Open();

            using (OracleCommand com = new OracleCommand("SCHD_DELIVERY_GET", con))
            {
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("S_OUTBOUND", OracleType.VarChar).Value = para;

                com.ExecuteNonQuery();
            }

            // }

        }

    }

    protected void uclOrder_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string UserID = Session["UserID"] + "";
        string Path = string.Format(sPathSave, DateTime.Now.ToShortDateString().Replace('/', '-'), UserID);
        CreateFolder(Path);
        string[] FILETYPE = e.UploadedFile.FileName.Split('.');


        LogUser(MENUID, "I", "อัพโหลด (Excel) จัดรายการขนส่งให้ลูกค้า ", "");
        //ฟังชั่นเช็คประเภทรูปและขนาดในการอับโหลด
        string AlertText = SystemFunction.DOC_CheckSize(FILETYPE[FILETYPE.Length - 1], e.UploadedFile.FileBytes.Length);
        if (CheckTypeFileUpLoad(e.UploadedFile))
        {
            if (string.IsNullOrEmpty(AlertText))
            {
                // สร้างชื่อไฟล์
                string FILENAME = SystemFunction.SplitFileName(e.UploadedFile.FileName);
                //string[] FILETYPE = e.UploadedFile.FileName.Split('.');
                string SYSFILENAME = "ODP_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + "." + FILETYPE[FILETYPE.Length - 1];

                //เก็บเอกสารลง Temp

                e.UploadedFile.SaveAs(Server.MapPath("./UploadFile") + Path + SYSFILENAME);
                if (File.Exists(Server.MapPath("./UploadFile") + Path + SYSFILENAME))
                {
                    string Pathchk = Server.MapPath("./UploadFile") + Path.Replace("/", "\\") + SYSFILENAME;
                    Session["sDataUpload"] = Pathchk;
                }
                else
                {

                }
                e.CallbackData = "";
            }
            else
            {
                e.CallbackData = AlertText;
            }
        }
        else
        {
            e.CallbackData = "เฉพาะไฟล์ .xls, .xlsx เท่านั้น";
        }

    }

    //สร้างโฟลเดอร์เพื่อไว้เก็บไฟล์
    private void CreateFolder(string Path)
    {
        try
        {

            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./UploadFile") + Path.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./UploadFile") + Path.Replace("/", "\\"));
            }
            #endregion
        }
        catch (Exception e)
        {
            // Console.WriteLine("The process failed: {0}", e.ToString());
        }
    }

    protected bool CheckTypeFileUpLoad(UploadedFile ful)
    {
        string[] nameFile = ful.FileName.Split('.');
        if (nameFile[nameFile.Length - 1].ToLower() == "xls" || nameFile[nameFile.Length - 1].ToLower() == "xlsx")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {

    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {

    }

    private void ExcelToDataTable(string FilePath)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }

        try
        {

            //string connectionString = string.Format("provider=Microsoft.Jet.OLEDB.4.0; data source={0};Extended Properties=Excel 8.0;", FilePath);
            string connectionString = "Provider='Microsoft.ACE.OLEDB.12.0';Data Source='" + FilePath + "';Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text'";
            string query = string.Format("SELECT * FROM [{0}$]", "Sheet1");

            DataSet ds = new DataSet();
            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                con.Open();
                OleDbDataAdapter adapter = new OleDbDataAdapter(query, con);
                adapter.Fill(ds);


                dt = new DataTable();

                if (ds.Tables[0].Rows.Count >= 0)
                {
                    dt = ds.Tables[0];
                }

                con.Close();
            }
        }
        catch
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','เอกสารที่แนบรูปแบบไม่ตรงกับฐานข้อมูล')");
        }
    }

    private void AddDataToDataBase()
    {
        string UserID = Session["UserID"] + "";
        if (dt.Rows.Count > 0)
        {
            string ERROR = "";

            //DataTable dtVendor = CommonFunction.Get_Data(conn, "SELECT SVENDORID,SABBREVIATION FROM TVENDOR");
            DataTable dtCONTRACT = CommonFunction.Get_Data(conn, @"SELECT TCR.SCONTRACTID,TCR.SCONTRACTNO, NVL(TCR.STERMINALID, REPLACE( SUBSTR(TCR.SCONT_PLNT,0,5),',','' )) as  STERMINALID,TEM.SABBREVIATION ,TCR.SVENDORID,VEN.SABBREVIATION as VENDORNAME
FROM TCONTRACT TCR
LEFT JOIN TTERMINAL TEM
ON NVL(TCR.STERMINALID, REPLACE( SUBSTR(TCR.SCONT_PLNT,0,5),',','' ))  = TEM.STERMINALID
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID = TCR.SVENDORID");
            string StrSave = "";
            DateTime Dtemp = new DateTime();
            if (rblChoice.Value == "A")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string deliveryPad = !string.IsNullOrEmpty(dt.Rows[i][5] + "") ? (dt.Rows[i][5] + "").Trim().PadLeft(10, '0') : "";

                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");
                    bool ISGET_DO = false;
                    //เป็นตัวใช้เช็คว่าจะหา DO มาจากไหน ถ้ากรณีที่ไม่มีใน TDELIVERY จะไปดูจาก TDELIVERY_SAP ว่ามีไหม ถ้ามีจะเอามาจาก TDELIVERY_SAP แต่ถ้าไม่มีจะเอามาจาก TRANS_DO
                    if (dtDELIVERY.Rows.Count > 0)
                    {

                    }
                    else
                    {

                        if (deliveryPad.Contains(";"))
                        {

                        }
                        else
                        {
                            ISGET_DO = true;
                            DO_GET(deliveryPad);
                            dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");
                        }
                    }

                    Dtemp = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? ConvertToDateNull(dt.Rows[i][3] + "").Value : new DateTime();
                    //string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
                    //string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy") : "";
                    string sDate = Dtemp.ToString("dd/MM/yyyy");
                    string eDate = Dtemp.ToString("dd/MM/yyyy");
                    string SVENDORID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[4] + "" : "";
                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[0] + "" : "";
                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[2] + "" : "";
                    string STERMINALNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[3] + "" : "";
                    // string sSupplyPlant = "";
                    //if (dtDELIVERY.Rows.Count > 0)
                    //{
                    //    sSupplyPlant = GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "");
                    //}

                    STERMINALID = /*sSupplyPlant; (sSupplyPlant != "") ? sSupplyPlant :*/ (dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SPLNT_CODE"] + "") ? dtDELIVERY.Rows[0]["SPLNT_CODE"] + "" : GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "")) : "");
                    STERMINALNAME = CommonFunction.Get_Value(conn, "SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID='" + STERMINALID + "'");
                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["OTHER"] + "") ? dtDELIVERY.Rows[0]["OTHER"] + "" : "null") : "null";



                    string SVENDORNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[5] + "" : "";
                    string SCONTRACTNO = dt.Rows[i][1] + "";
                    string ORDERTYPE = dt.Rows[i][2] + "";
                    string NWINDOWTIMEID = dt.Rows[i][4] + "";
                    string SDELIVERYNO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "") ? (dtDELIVERY.Rows[0]["DELIVERY_NO"] + "").Trim().PadLeft(10, '0') : "null") : "";
                    string SHIP_TO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SHIP_TO"] + "") ? dtDELIVERY.Rows[0]["SHIP_TO"] + "" : SystemFunction.Get_SHIPTO_FROM_TBORDERPLAN(SDELIVERYNO)) : SystemFunction.Get_SHIPTO_FROM_TBORDERPLAN(SDELIVERYNO);

                    string LateDay = dt.Rows[i]["LATEDAY"] + string.Empty;
                    string LateTime = dt.Rows[i]["LATETIME"] + string.Empty;

                    int Check = CommonFunction.Count_Value(conn, "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN WHERE SDELIVERYNO = '" + SDELIVERYNO + @"'  AND NVL(CACTIVE,'Y') = 'Y'");
                    //ถ้ามีในระบบให้อัพเดท
                    if (Check > 0)
                    {
                        StrSave = @"
                                    UPDATE TBL_ORDERPLAN
                                    SET    SVENDORID     = '" + SVENDORID + @"',
                                           SABBREVIATION = '" + SVENDORNAME + @"',
                                           SCONTRACTID   = '" + SCONTRACTID + @"',
                                           SCONTRACTNO   = '" + SCONTRACTNO + @"',
                                           ORDERTYPE     = '" + ORDERTYPE + @"',
                                           DDELIVERY    = TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
                                           NWINDOWTIMEID = " + NWINDOWTIMEID + @",
                                           --DATE_CREATE   = SYSDATE,
                                           --SCREATE       = '" + UserID + @"',
                                           DATE_UPDATE   = SYSDATE,
                                           SUPDATE       = '" + UserID + @"',
                                           STERMINALID    = '" + STERMINALID + @"',
                                           STERMINALNAME  = '" + STERMINALNAME + @"',
                                           NVALUE       = " + CAPACITY + @",
                                           SHIP_TO = '" + SHIP_TO + @"',
                                           LATEDAY = " + (string.IsNullOrEmpty(LateDay) ? (object)"NULL" : txtLateDay.Text.Trim()) + @",
                                           LATETIME = '" + LateTime + @"'
                                    WHERE  SDELIVERYNO   = '" + SDELIVERYNO + @"' AND CACTIVE = 'Y'
                        ";

                        ERROR += CheckError(SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, STERMINALID, STERMINALNAME, SHIP_TO, CAPACITY, SDELIVERYNO, ORDERTYPE, NWINDOWTIMEID, eDate, deliveryPad, ISGET_DO, Dtemp);

                    }
                    else //ถ้าไม่มีในระบบให้ Save
                    {
                        StrSave = @"
INSERT INTO TBL_ORDERPLAN (ORDERID,SVENDORID,SABBREVIATION,SCONTRACTID,SCONTRACTNO,ORDERTYPE,DDELIVERY,NWINDOWTIMEID, SDELIVERYNO,DATE_CREATE, SCREATE,STERMINALID,STERMINALNAME,NVALUE,CACTIVE,SHIP_TO,LATEDAY,LATETIME) 
VALUES ( '" + genID() + @"',
 '" + SVENDORID + @"',
 '" + SVENDORNAME + @"',
 '" + SCONTRACTID + @"',
 '" + SCONTRACTNO + @"',
 '" + ORDERTYPE + @"',
 TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
 " + NWINDOWTIMEID + @",
 '" + SDELIVERYNO + @"',
 SYSDATE,
 '" + UserID + @"',
 '" + STERMINALID + @"',
 '" + STERMINALNAME + @"',
 " + CAPACITY + @",
   'Y' ,
'" + SHIP_TO + @"',
" + (string.IsNullOrEmpty(LateDay) ? (object)"NULL" : txtLateDay.Text.Trim()) + @",
'" + LateTime + @"')";
                        ERROR += CheckError(SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, STERMINALID, STERMINALNAME, SHIP_TO, CAPACITY, SDELIVERYNO, ORDERTYPE, NWINDOWTIMEID, eDate, deliveryPad, ISGET_DO, Dtemp);

                    }
                    //เช็คว่ามี string ที่เป็น SDELIVERYNO ไหม ถ้ามีไม่ต้องเซฟ
                    if (ERROR.Contains(SDELIVERYNO))
                    {
                        //DetailAlertList.Append(ERROR);
                    }
                    else
                    {
                        AddTODB(StrSave);
                        Update_PLAN_Capacity(SDELIVERYNO, CAPACITY);
                    }

                }

            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string deliveryPad = !string.IsNullOrEmpty(dt.Rows[i][5] + "") ? (dt.Rows[i][5] + "").Trim().PadLeft(10, '0') : "";
                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");

                    //เป็นตัวใช้เช็คว่าจะหา DO มาจากไหน ถ้ากรณีที่ไม่มีใน TDELIVERY จะไปดูจาก TDELIVERY_SAP ว่ามีไหม ถ้ามีจะเอามาจาก TDELIVERY_SAP แต่ถ้าไม่มีจะเอามาจาก TRANS_DO
                    bool ISGET_DO = false;
                    if (dtDELIVERY.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        if (deliveryPad.Contains(";"))
                        {

                        }
                        else
                        {
                            ISGET_DO = true;
                            DO_GET(deliveryPad);
                            dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");
                        }
                    }
                    Dtemp = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? ConvertToDateNull(dt.Rows[i][3] + "").Value : new DateTime();
                    //string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
                    //string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy") : "";
                    string sDate = Dtemp.ToString("dd/MM/yyyy");
                    string eDate = Dtemp.ToString("dd/MM/yyyy");

                    //string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
                    //string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy") : "";
                    string SVENDORID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[4] + "" : "";
                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[0] + "" : "";
                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[2] + "" : "";
                    string STERMINALNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[3] + "" : "";
                    //string sSupplyPlant = "";
                    //if (dtDELIVERY.Rows.Count > 0)
                    //{
                    //    sSupplyPlant = GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "");
                    //}
                    //STERMINALID = sSupplyPlant;// (sSupplyPlant != "")? sSupplyPlant : STERMINALID; 
                    STERMINALID = /*sSupplyPlant; (sSupplyPlant != "") ? sSupplyPlant :*/ (dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SPLNT_CODE"] + "") ? dtDELIVERY.Rows[0]["SPLNT_CODE"] + "" : GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "")) : "");
                    STERMINALNAME = CommonFunction.Get_Value(conn, "SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID='" + STERMINALID + "'");
                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["OTHER"] + "") ? dtDELIVERY.Rows[0]["OTHER"] + "" : "null") : "null";
                    string SHIP_TO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SHIP_TO"] + "") ? dtDELIVERY.Rows[0]["SHIP_TO"] + "" : "null") : "null";

                    string SVENDORNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[5] + "" : "";
                    string SCONTRACTNO = dt.Rows[i][1] + "";
                    string ORDERTYPE = dt.Rows[i][2] + "";
                    string NWINDOWTIMEID = dt.Rows[i][4] + "";
                    string SDELIVERYNO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "") ? (dtDELIVERY.Rows[0]["DELIVERY_NO"] + "").Trim().PadLeft(10, '0') : "null") : "";
                    int Check = CommonFunction.Count_Value(conn, "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN WHERE SDELIVERYNO = '" + SDELIVERYNO + @"' AND NVL(CACTIVE,'Y') = 'Y'");

                    string LateDay = dt.Rows[i]["LATEDAY"] + string.Empty;
                    string LateTime = dt.Rows[i]["LATETIME"] + string.Empty;
                    //ถ้ามีในระบบไม่อัพเดท และให้ StrSave ค่าว่าง ไม่งั้นจะเกิด Error กรณี StrSave ไม่ได้โดนเคลียค่ามา
                    if (Check > 0)
                    {
                        StrSave = "";
                    }
                    else //ถ้าไม่มีในระบบให้ Save
                    {
                        StrSave = @"
INSERT INTO TBL_ORDERPLAN (ORDERID,SVENDORID,SABBREVIATION,SCONTRACTID,SCONTRACTNO,ORDERTYPE,DDELIVERY,NWINDOWTIMEID, SDELIVERYNO,DATE_CREATE, SCREATE,STERMINALID,STERMINALNAME,NVALUE,CACTIVE,SHIP_TO,LATEDAY,LATETIME) 
VALUES ( '" + genID() + @"',
 '" + SVENDORID + @"',
 '" + SVENDORNAME + @"',
 '" + SCONTRACTID + @"',
 '" + SCONTRACTNO + @"',
 '" + ORDERTYPE + @"',
 TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
 " + NWINDOWTIMEID + @",
 '" + SDELIVERYNO + @"',
 SYSDATE,
 '" + UserID + @"',
 '" + STERMINALID + @"',
 '" + STERMINALNAME + @"',
 " + CAPACITY + @",
  'Y',
 '" + SHIP_TO + @"',
" + (string.IsNullOrEmpty(LateDay) ? (object)"NULL" : txtLateDay.Text.Trim()) + @",
'" + LateTime + @"')";

                        ERROR += CheckError(SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, STERMINALID, STERMINALNAME, SHIP_TO, CAPACITY, SDELIVERYNO, ORDERTYPE, NWINDOWTIMEID, eDate, deliveryPad, ISGET_DO, Dtemp);

                    }

                    //เช็คว่ามี string ที่เป็น SDELIVERYNO ไหม ถ้ามีไม่ต้องเซฟ
                    if (ERROR.Contains(deliveryPad))
                    {
                        // DetailAlertList.Append(ERROR);

                    }
                    else
                    {
                        AddTODB(StrSave);
                        Update_PLAN_Capacity(SDELIVERYNO, CAPACITY);//อัพเดทความจุของแผน

                    }



                }


            }

            if (string.IsNullOrEmpty(ERROR))
            {
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
            }
            else
            {
                string TextError = "<div class='sco'>" + ERROR + "</div>";


                CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + ERROR + "')");
            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่พบช้อมูลเอกสารแนบใหม่อีกครั้ง')");
        }
    }

    private void DO_GET(string SDELIVERYNO)
    {

        string QUERY = @"BEGIN GET_DO_ORDERPLAN('" + SDELIVERYNO + "'); END;";
        AddTODB(QUERY);
    }

    private void Update_PLAN_Capacity(string SDELIVERYNO, string NVALUE)
    {
        decimal OTHER = 0;
        bool CHECK = decimal.TryParse(NVALUE, out OTHER);


        if (!string.IsNullOrEmpty(NVALUE) && CHECK == true && !string.IsNullOrEmpty(SDELIVERYNO))
        {
            string UPDATE_TPLANSCHEDULELIST = "UPDATE TPLANSCHEDULELIST SET NVALUE = " + NVALUE + " WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(SDELIVERYNO) + "'  AND CACTIVE = '1'";
            AddTODB(UPDATE_TPLANSCHEDULELIST);
        }
    }
    private void ListT1()
    {

        gvwT1.EndUpdate();
        string ConditionTerminal = "";
        string UserID = Session["UserID"] + "";
        DataTable dt_TERMINAL = CommonFunction.Get_Data(conn, "SELECT SUID,SVENDORID FROM TUSER WHERE CGROUP = '2' AND SUID = '" + UserID + "'");

        string Condition = "";
        //if (!string.IsNullOrEmpty(cboTeminal.Text))
        //{
        //    Condition += @" AND TCT.STERMINALID||TCT.SCONT_PLNT LIKE '%" + CommonFunction.ReplaceInjection(cboTeminal.Value + "") + "%'";
        //}
        if (ddlWarehouse.SelectedIndex > 0)
        {
            Condition += @" AND TCT.STERMINALID||TCT.SCONT_PLNT LIKE '%" + ddlWarehouse.Value + "%'";
        }
        else
        {
            if (dt_TERMINAL.Rows.Count > 0)
            {
                Condition += @" AND TCT.STERMINALID||TCT.SCONT_PLNT LIKE '%" + CommonFunction.ReplaceInjection(dt_TERMINAL.Rows[0]["SVENDORID"] + "") + "%'";
            }
        }

        string CONDITIONTIME_HEAD = "";
        string CONDITIONTIME = "";
        //if (!string.IsNullOrEmpty(edtCFT.Text))
        //{
        //    DateTime datestart = DateTime.Parse(edtCFT.Value.ToString());
        //    CONDITIONTIME_HEAD = "  AND  TRUNC(DDATE+1) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        //    CONDITIONTIME = "  WHERE TRUNC(TCF.DDATE+1) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        //}
        if (!string.IsNullOrEmpty(txtDateConfirm.Text))
        {
            DateTime datestart = DateTime.ParseExact(txtDateConfirm.Text.Trim(), "dd/MM/yyyy", null);
            CONDITIONTIME_HEAD = "  AND  TRUNC(DDATE+1) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
            CONDITIONTIME = "  WHERE TRUNC(TCF.DDATE+1) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        }

        //        string QUERY = @"SELECT VEN.SVENDORID,VEN.SABBREVIATION,TCT.SCONTRACTID,TCT.SCONTRACTNO,TUC.SHEADID
        //,CASE WHEN NVL(TUC.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TUC.SHEADREGISTERNO||' - '||TUC.STRAILERREGISTERNO ELSE TUC.SHEADREGISTERNO END as STRUCKREGIS
        //,TUC.CAPACITY 
        //,CASE 
        //WHEN NVL(TCT.STERMINALID,'xxx') <> 'xxx' THEN TCT.STERMINALID 
        //WHEN NVL(TCT.STERMINALID,'xxx') = 'xxx' THEN SUBSTR(TCT.SCONT_PLNT,2,4)
        //ELSE '' END  as STERMINALID
        //FROM TCONTRACT TCT
        //INNER JOIN TVENDOR VEN
        //ON VEN.SVENDORID = TCT.SVENDORID
        //INNER JOIN
        //(
        //    SELECT TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO,TCFL.CAPACITY FROM TTRUCKCONFIRM TCF
        //    INNER JOIN 
        //    (
        //        SELECT TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO as STRAILERREGISTERNO 
        //        ,CASE WHEN NVL(TRKH.SHEADREGISTERNO,'xxx') <> 'xxx' THEN TRKT.NTOTALCAPACITY ELSE TRKT.NTOTALCAPACITY END as CAPACITY
        //        FROM TTRUCKCONFIRMLIST TRKC
        //        INNER JOIN TTRUCK TRKH
        //        ON TRKH.STRUCKID = TRKC.SHEADID
        //        INNER JOIN TTRUCK TRKT
        //        ON TRKT.STRUCKID = TRKC.STRAILERID
        //        WHERE TRKC.CCONFIRM = '1'
        //        GROUP BY TRKC.NCONFIRMID,TRKC.SHEADID,TRKH.SHEADREGISTERNO,TRKT.SHEADREGISTERNO,CASE WHEN NVL(TRKH.SHEADREGISTERNO,'xxx') <> 'xxx' THEN TRKT.NTOTALCAPACITY ELSE TRKT.NTOTALCAPACITY END
        //    ) TCFL
        //    ON TCFL.NCONFIRMID = TCF.NCONFIRMID AND TCF.CCONFIRM = '1'
        //    " + CONDITIONTIME + @"
        //    GROUP BY TCF.SCONTRACTID,TCFL.SHEADID,TCFL.SHEADREGISTERNO,TCFL.STRAILERREGISTERNO,TCFL.CAPACITY
        //)TUC
        //ON TUC.SCONTRACTID = TCT.SCONTRACTID
        //WHERE 1=1 " + Condition + "";
        string ORDER = "";
        if (string.IsNullOrEmpty(Session["GSORT"] + ""))
        {
            Session["GSORT"] = "ASC";
        }
        else
        {
            switch (Session["GSORT"] + "")
            {
                case "ASC": Session["GSORT"] = "DESC";
                    break;
                case "DESC": Session["GSORT"] = "ASC";
                    break;
            }
        }

        switch (txtOrder.Text)
        {
            case "VENDORNAME": ORDER = "ORDER BY SVEN.SVENDORID " + Session["GSORT"] + ",STCT.SCONTRACTID ASC";
                break;
            case "CONTRACT": ORDER = "ORDER BY SVEN.SVENDORID ASC,STCT.SCONTRACTID " + Session["GSORT"] + "";
                break;
            //case "": ORDER = "ORDER BY SVEN.SVENDORID,STCT.SCONTRACTID";
            //    break;
            //case "": ORDER = "ORDER BY SVEN.SVENDORID,STCT.SCONTRACTID";
            //    break;
            default: ORDER = "ORDER BY SVEN.SVENDORID ASC,STCT.SCONTRACTID ASC";
                break;
        }

        string newWHere = "";
        //if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
        //{
        //    newWHere = "WHERE SVEN.SABBREVIATION||';'||STCT.SCONTRACTNO||';'||STRK.STRUCKREGIS LIKE '%" + txtSearch.Text.Trim() + "%'";
        //}
        newWHere = " WHERE 1=1";
        if (ddlCompany.SelectedIndex > 0)
            newWHere = " AND SVEN.SABBREVIATION LIKE '%" + ddlCompany.SelectedValue + "%'";

        if (ddlContract.SelectedIndex > 0)
            newWHere = " AND STCT.SCONTRACTNO LIKE '%" + ddlContract.SelectedValue + "%'";

        string QUERY = @"SELECT SVEN.SVENDORID,SVEN.SABBREVIATION,SVEN.COUNTCONTRACT,CTRK.COUNTTRUCKCONALL,STCT.SCONTRACTID,STCT.SCONTRACTNO,TC.COUNTTRUCKCON,STRK.STRUCKREGIS,STRK.CAPACITY,TCP.STERMINALNAME FROM 
(
    SELECT VEN.SVENDORID,VEN.SABBREVIATION,COUNT(TCT.SCONTRACTID) as COUNTCONTRACT FROM TVENDOR VEN
    INNER JOIN TCONTRACT TCT
    ON TCT.SVENDORID = VEN.SVENDORID AND NVL(TCT.CACTIVE,'XXX') = 'Y'
    INNER JOIN (SELECT SCONTRACTID FROM  TTRUCKCONFIRM WHERE CCONFIRM = '1' " + CONDITIONTIME_HEAD + @"  GROUP BY SCONTRACTID  ) TCF
    ON TCT.SCONTRACTID = TCF.SCONTRACTID 
    WHERE 1=1 " + Condition + @"
    GROUP BY VEN.SVENDORID,VEN.SABBREVIATION
) SVEN
INNER JOIN 
TCONTRACT
STCT
ON STCT.SVENDORID = SVEN.SVENDORID AND NVL(STCT.CACTIVE,'XXX') = 'Y'
INNER JOIN
(
    SELECT SCO.SCONTRACTID,COUNT(SCO.SCONTRACTID) as COUNTTRUCKCON FROM
    (
        -- นำ TTRUCKCONFIRM มาจอย TTRUCKCONFIRMLIST เพื่อหารถที่คอนเฟิมอยู่ในสัญญาอะไร
        SELECT TCF.SCONTRACTID,TCL.SHEADID||TCL.STRAILERID FROM TTRUCKCONFIRM TCF
        INNER JOIN 
        (
                 SELECT NCONFIRMID ,SHEADID,STRAILERID FROM   TTRUCKCONFIRMLIST     WHERE CCONFIRM = '1'
                 GROUP BY NCONFIRMID ,SHEADID,STRAILERID
        )TCL
        ON TCL.NCONFIRMID = TCF.NCONFIRMID
        INNER JOIN TTRUCK TRK
        ON TRK.STRUCKID = TCL.SHEADID 
        LEFT JOIN TTRUCK TRKT
        ON  NVL(TRKT.STRUCKID,'TRXX') =  NVL(TCL.STRAILERID,'TRXX') 
        " + CONDITIONTIME + @"
        GROUP BY TCF.SCONTRACTID,TCL.SHEADID||TCL.STRAILERID
    )SCO
    GROUP BY SCO.SCONTRACTID
)TC
ON TC.SCONTRACTID = STCT.SCONTRACTID
INNER JOIN
(
    SELECT TCF.SCONTRACTID, TCL.SHEADID,TCL.STRAILERID,
    CASE WHEN NVL(TRKT.SHEADREGISTERNO ,'xxx') <> 'xxx' THEN TRK.SHEADREGISTERNO||' - '||TRKT.SHEADREGISTERNO  ELSE TRK.SHEADREGISTERNO  END as STRUCKREGIS ,
    CASE WHEN NVL(TRKT.NTOTALCAPACITY ,0) <> 0 THEN TRKT.NTOTALCAPACITY  ELSE TRK.NTOTALCAPACITY  END as CAPACITY 
    FROM TTRUCKCONFIRM TCF
    INNER JOIN 
    (
        SELECT NCONFIRMID ,SHEADID,STRAILERID FROM   TTRUCKCONFIRMLIST     WHERE CCONFIRM = '1'
        GROUP BY NCONFIRMID ,SHEADID,STRAILERID
    )TCL
    ON TCL.NCONFIRMID = TCF.NCONFIRMID
    INNER JOIN TTRUCK TRK
    ON TRK.STRUCKID = TCL.SHEADID 
    LEFT JOIN TTRUCK TRKT
    ON  NVL(TRKT.STRUCKID,'TRXX') =  NVL(TCL.STRAILERID,'TRXX') 
    " + CONDITIONTIME + @"
    GROUP BY TCF.SCONTRACTID, TCL.SHEADID,TCL.STRAILERID,
    CASE WHEN NVL(TRKT.SHEADREGISTERNO ,'xxx') <> 'xxx' THEN TRK.SHEADREGISTERNO||' - '||TRKT.SHEADREGISTERNO  ELSE TRK.SHEADREGISTERNO  END,
    CASE WHEN NVL(TRKT.NTOTALCAPACITY ,0) <> 0 THEN TRKT.NTOTALCAPACITY  ELSE TRK.NTOTALCAPACITY  END
) STRK
ON STRK.SCONTRACTID = STCT.SCONTRACTID
INNER JOIN 
(    
    SELECT SCO.STRANSPORTID,COUNT(SCO.CSTRUCK) as COUNTTRUCKCONALL  FROM
    (
        -- นำ TTRUCKCONFIRM มาจอย TTRUCKCONFIRMLIST เพื่อหารถที่คอนเฟิมอยู่ในสัญญาอะไร
        SELECT TCF.SCONTRACTID,TCL.SHEADID||TCL.STRAILERID as CSTRUCK, 
         TCC.SVENDORID as STRANSPORTID
        --CASE WHEN NVL(TRKT.STRANSPORTID,'xxx') <> 'xxx' THEN TRKT.STRANSPORTID ELSE TRK.STRANSPORTID END  as STRANSPORTID
        FROM TCONTRACT TCC
        LEFT JOIN   TTRUCKCONFIRM TCF
        ON TCC.SCONTRACTID = TCF.SCONTRACTID
        INNER JOIN 
        (
                 SELECT NCONFIRMID ,SHEADID,STRAILERID FROM   TTRUCKCONFIRMLIST     WHERE CCONFIRM = '1'
                 GROUP BY NCONFIRMID ,SHEADID,STRAILERID
        )TCL
        ON TCL.NCONFIRMID = TCF.NCONFIRMID
        INNER JOIN TTRUCK TRK
        ON TRK.STRUCKID = TCL.SHEADID 
        LEFT JOIN TTRUCK TRKT
        ON  NVL(TRKT.STRUCKID,'TRXX') =  NVL(TCL.STRAILERID,'TRXX') 
        " + CONDITIONTIME + @"  AND NVL(TCC.CACTIVE,'xx') ='Y'
        GROUP BY TCF.SCONTRACTID,TCL.SHEADID||TCL.STRAILERID,TCC.SVENDORID --CASE WHEN NVL(TRKT.STRANSPORTID,'xxx') <> 'xxx' THEN TRKT.STRANSPORTID ELSE TRK.STRANSPORTID END
    )SCO
    GROUP BY SCO.STRANSPORTID
) CTRK
ON CTRK.STRANSPORTID = SVEN.SVENDORID
LEFT JOIN 
(
    SELECT TP.SCONTRACTID,TP.STERMINALID,TT.SABBREVIATION as STERMINALNAME  FROM TCONTRACT_PLANT TP
    LEFT JOIN TTERMINAL TT
    ON TP.STERMINALID = TT.STERMINALID
    WHERE   TP.CMAIN_PLANT = '1'
) TCP
ON TCP.SCONTRACTID = STCT.SCONTRACTID --AND  TCP.CMAIN_PLANT = '1'
" + newWHere + @"
GROUP BY SVEN.SVENDORID,SVEN.SABBREVIATION,SVEN.COUNTCONTRACT,STCT.SCONTRACTID,STCT.SCONTRACTNO,TC.COUNTTRUCKCON,CTRK.COUNTTRUCKCONALL,STRK.STRUCKREGIS,STRK.CAPACITY,TCP.STERMINALNAME
" + ORDER + "";

        LogUser(MENUID, "S", "ค้าหาข้อมูล รถที่ยืนยันตามสัญญา", "");
        DataTable dt = MergeSubGroup(QUERY);

        if (dt.Rows.Count > 0)
        {
            DataTable dts = CommonFunction.Get_Data(conn, QUERY);
            lblDetailTeminal.Text = dts.Rows.Count + "";
            gvwT1.DataSource = dt;
        }
        gvwT1.DataBind();
    }

    private void ListData()
    {
        #region แบบเก่า
        //string Condition = "";
        //string ConditionD = "";
        //string DateShipment = "";

        //string STARTDATE = "";
        //string ENDDATE = "";
        //if (!string.IsNullOrEmpty(edtStart.Text) && !string.IsNullOrEmpty(edtEnd.Text))
        //{
        //    //DateTime datestart = DateTime.ParseExact(edtStart.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));
        //    //DateTime dateend = DateTime.ParseExact(edtEnd.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));
        //    //Condition += " AND (TRUNC(ODP.DDELIVERY) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
        //    //ConditionD += " AND (TRUNC(DDELIVERY) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
        //    //DateShipment = " AND (TRUNC(DATE_CREATED) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";

        //    STARTDATE = "TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        //    ENDDATE = "TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        //}
        //if (!string.IsNullOrEmpty(cboWindowTime.Value + ""))
        //{
        //    Condition += " AND ODP.NWINDOWTIMEID = '" + CommonFunction.ReplaceInjection(cboWindowTime.Value + "") + "'";
        //}

        ////if (!string.IsNullOrEmpty(txtSearch.Text))
        ////{
        ////    Condition += " AND SDELIVERYNO LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%'";
        ////}

        //if (!string.IsNullOrEmpty(txtSearch2.Text))
        //{
        //    Condition += " AND ODP.SABBREVIATION||ODP.SCONTRACTNO||ODP.SDELIVERYNO||TPC.TRANTRUCK||ODP.SVENDORID||ODP.SCONTRACTID LIKE '%" + CommonFunction.ReplaceInjection(txtSearch2.Text) + "%'";
        //}
        //ไม่เช็ค Shipment
        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, ODP.NVALUE, ODP.STERMINALID,ODP.STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END as CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //WHERE 1=1 AND ODP.CACTIVE = 'Y' AND 
        //CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END = '1' 
        //" + Condition + @"
        //ORDER BY DDELIVERY ASC";

        //        string QUERY = @"
        //SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, ODP.NVALUE, ODP.STERMINALID,ODP.STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END as CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,TPC.TRANTRUCK
        //,NVL(SHM.DELIVERY_NO,'xxx') as SHIPMENT
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN TSHIPMENT SHM
        //ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN 
        //(
        //     SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE
        //     ,CASE WHEN NVL(TPS.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TPS.SHEADREGISTERNO||'-'||TPS.STRAILERREGISTERNO ELSE TPS.SHEADREGISTERNO END as TRANTRUCK
        //     FROM TPLANSCHEDULE TPS
        //     LEFT JOIN TPLANSCHEDULELIST TPL
        //     ON TPS.NPLANID = TPL.NPLANID
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO
        //WHERE 1=1 AND ODP.CACTIVE = 'Y' AND TPC.CACTIVE ='1' AND NVL(TPC.STRUCKID,'xxx') <> 'xxx' AND 
        //CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END = '1' 
        //" + Condition + @" " + ConditionTerminal + @"
        //ORDER BY DDELIVERY ASC";

        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, ODP.NVALUE, ODP.STERMINALID,ODP.STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') NOT IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <>NVL(ODP.DDELIVERY,'xxx') THEN '0'
        //ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,NVL(TPC.TRANTRUCK,'xxx') as TRANTRUCK
        //,NVL(SHM.DELIVERY_NO,'xxx') as SHIPMENT
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN 
        //(
        //    SELECT DISTINCT T.SHIPMENT_NO,T.DELIVERY_NO FROM TSHiPMENT T 
        //    WHERE T.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN)  " + DateShipment + @"
        //)SHM
        //ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
        // AND   TWT.CDAYTYPE = CASE WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') NOT IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <>NVL(ODP.DDELIVERY,'xxx') THEN '0'
        //ELSE '' END
        //LEFT JOIN 
        //(
        //     SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID
        //     ,CASE WHEN NVL(TPS.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TPS.SHEADREGISTERNO||'-'||TPS.STRAILERREGISTERNO ELSE TPS.SHEADREGISTERNO END as TRANTRUCK
        //     FROM TPLANSCHEDULE TPS
        //     LEFT JOIN TPLANSCHEDULELIST TPL
        //     ON TPS.NPLANID = TPL.NPLANID
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //WHERE 1=1 AND ODP.CACTIVE = 'Y' AND NVL(TPC.TRANTRUCK,'xxx') = 'xxx'
        //" + Condition + @" " + ConditionTerminal + @"
        //ORDER BY DDELIVERY ASC";
        //        string ConditionTerminal = Teminal_ID();
        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, SHM.OTHER as NVALUE, ODP.STERMINALID,SHM.CUST_ABBR as STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') THEN '0'
        //ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,NVL(TPC.TRANTRUCK,'xxx') as TRANTRUCK
        //,NVL(SHM.DELIVERY_NO,'xxx') as SHIPMENT,TT.STERMINALNAME as STERMINALNAME_FROM
        //--,PNT.SCONTRACTID
        //FROM TBL_ORDERPLAN ODP
        //INNER JOIN 
        //(
        //      SELECT SCONTRACTID FROM TCONTRACT  WHERE STERMINALID||','||SCONT_PLNT LIKE '%" + ConditionTerminal + @"%'
        //)PNT
        //ON ODP.SCONTRACTID = PNT.SCONTRACTID
        //LEFT JOIN 
        //(
        //            SELECT d.DELIVERY_NO,d.SPLNT_CODE,d.OTHER,C.CUST_ABBR,
        //            (CASE 
        //            WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') 
        //            WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)  
        //            ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO
        //            FROM 
        //            (
        //                  --SELECT * FROM TDELIVERY
        //                  --WHERE DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND TRUNC(DDELIVERY) BETWEEN " + STARTDATE + @" AND " + ENDDATE + @")
        //                     SELECT LM.* FROM TDELIVERY LM
        //                    INNER JOIN 
        //                    (
        //                        SELECT DELIVERY_NO,MAX(DATE_CREATED) as DATE_CREATED,MAX(DELIVERY_DATE) as DELIVERY_DATE FROM TDELIVERY 
        //                        GROUP BY DELIVERY_NO
        //                    )LL
        //                    ON LM.DELIVERY_NO = LL.DELIVERY_NO AND LM.DATE_CREATED = LL.DATE_CREATED AND LM.DELIVERY_DATE = LL.DELIVERY_DATE
        //                    WHERE  
        //                    LM.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND TRUNC(DDELIVERY) BETWEEN " + STARTDATE + @" AND " + ENDDATE + @")
        //            )d  
        //            LEFT JOIN 
        //            (
        //                --SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
        //                    SELECT  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE FROM TRANS_ORDER   LM
        //                    INNER JOIN 
        //                    (
        //                        SELECT  DOC_NO,MAX(DBSYS_DATE) as DBSYS_DATE  FROM TRANS_ORDER  
        //                        WHERE NVL(VALTYP,'-') !='REBRAND' AND NVL(STATUS,'N') IN ('N','G')
        //                        GROUP BY DOC_NO
        //                    )LL
        //                    ON LM.DOC_NO = LL.DOC_NO AND LM.DBSYS_DATE = LL.DBSYS_DATE
        //                    WHERE NVL(LM.VALTYP,'-') !='REBRAND' AND NVL(LM.STATUS,'N') IN ('N','G')
        //                    GROUP BY  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE
        //            )TRN
        //            ON TRN.DOC_NO = d.SALES_ORDER
        //            LEFT JOIN 
        //            (
        //                --SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
        //                    SELECT  LM.DOCNO,LM.RECIEVEPOINT FROM TBORDER LM
        //                    INNER JOIN
        //                    (
        //                        SELECT  DOCNO,MAX(UPDATEDATE) as UPDATEDATE FROM TBORDER 
        //                        WHERE RECIEVEPOINT IS NOT NULL
        //                        GROUP BY DOCNO
        //                    )LL
        //                    ON LM.DOCNO = LL.DOCNO AND LM.UPDATEDATE = LL.UPDATEDATE
        //                    GROUP BY LM.DOCNO,LM.RECIEVEPOINT
        //            )TBO
        //            ON TBO.DOCNO = d.SALES_ORDER
        //            LEFT JOIN TCUSTOMER C ON  C.SHIP_TO =  (CASE  WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0')  WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)   ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) 
        //)SHM
        //ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
        // AND   TWT.CDAYTYPE = CASE WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY')  IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') THEN '0'
        //ELSE '' END
        //LEFT JOIN 
        //(
        //     SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID
        //     ,CASE WHEN NVL(TPS.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TPS.SHEADREGISTERNO||'-'||TPS.STRAILERREGISTERNO ELSE TPS.SHEADREGISTERNO END as TRANTRUCK
        //     FROM TPLANSCHEDULE TPS
        //     LEFT JOIN TPLANSCHEDULELIST TPL
        //     ON TPS.NPLANID = TPL.NPLANID
        //     WHERE TPL.CACTIVE = '1'
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //LEFT JOIN TTERMINAL_SAP TT
        //ON TT.STERMINALID = SHM.SPLNT_CODE
        //WHERE 1=1 AND ODP.CACTIVE = 'Y' AND NVL(TPC.TRANTRUCK,'xxx') = 'xxx'
        //" + Condition + @" 
        //ORDER BY DDELIVERY ASC,NVL(ODP.DATE_UPDATE,ODP.DATE_CREATE) DESC";
        #endregion


        //LogUser(MENUID, "S", "ค้าหาข้อมูล แบ่งงานให้ผู้ขนส่ง", "");
        gvw.DataSource = GetData();

        gvw.DataBind();
        //cboContract_Callback(cboContractSearch, new CallbackEventArgsBase("Search"));
    }
    private DataTable GetData()
    {
        return OrderPlanBLL.Instance.GetOrder1(edtStart.Text, edtEnd.Text, cboWindowTime.SelectedValue, ddlSTERMINAL.Value + string.Empty, txtSearch2.Text, ddlGroup.SelectedValue, cboVendorSearch.Value + string.Empty, cboContractSearch.Value + string.Empty);
    }
    private void ListData2()
    {


        DataTable dtORDER = GetData2();
        if (dtORDER.Rows.Count > 0)
        {
            LogUser(MENUID, "S", "ค้าหาข้อมูล งานที่ผู้ขนส่งจัดลงแผน", "");
            gvwT3.DataSource = dtORDER;
        }
        gvwT3.DataBind();

    }

    private DataTable GetData2()
    {

        #region แบบเดิม
        //string Condition = "";
        //string ConditionD = "";
        //string DateShipment = "";

        //string STARTDATE = "";
        //string ENDDATE = "";
        //if (!string.IsNullOrEmpty(edtStartT3.Text) && !string.IsNullOrEmpty(edtEndT3.Text))
        //{
        //    //DateTime datestart = DateTime.Parse(edtStartT3.Value.ToString());
        //    //DateTime dateend = DateTime.Parse(edtEndT3.Value.ToString());
        //    DateTime datestart = DateTime.ParseExact(edtStartT3.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));
        //    DateTime dateend = DateTime.ParseExact(edtEndT3.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));
        //    Condition += " AND (TRUNC(ODP.DDELIVERY) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
        //    ConditionD += " AND (TRUNC(DDELIVERY) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
        //    DateShipment = " AND (TRUNC(DATE_CREATED) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";


        //    STARTDATE = "TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        //    ENDDATE = "TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        //}

        //if (!string.IsNullOrEmpty(cboWindowTimeT3.Value + ""))
        //{
        //    Condition += " AND ODP.NWINDOWTIMEID = '" + CommonFunction.ReplaceInjection(cboWindowTimeT3.Value + "") + "'";
        //}


        //if (!string.IsNullOrEmpty(txtSearch2T3.Text))
        //{
        //    Condition += " AND ODP.SABBREVIATION||ODP.SCONTRACTNO||ODP.SDELIVERYNO||TPC.TRANTRUCK||ODP.SVENDORID||ODP.SCONTRACTID LIKE '%" + CommonFunction.ReplaceInjection(txtSearch2T3.Text) + "%'";
        //}
        //ไม่ได้ Join Shipment
        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, ODP.NVALUE, ODP.STERMINALID,ODP.STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END as CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,TPC.TRANTRUCK
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN 
        //(
        //     SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE
        //     ,CASE WHEN NVL(TPS.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TPS.SHEADREGISTERNO||'-'||TPS.STRAILERREGISTERNO ELSE TPS.SHEADREGISTERNO END as TRANTRUCK
        //     FROM TPLANSCHEDULE TPS
        //     LEFT JOIN TPLANSCHEDULELIST TPL
        //     ON TPS.NPLANID = TPL.NPLANID
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO
        //WHERE 1=1 AND ODP.CACTIVE = 'Y' AND NVL(TPC.STRUCKID,'xxx') <> 'xxx' AND 
        //CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END = '1' 
        //" + Condition + @"
        //ORDER BY DDELIVERY ASC";


        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, ODP.NVALUE, ODP.STERMINALID,ODP.STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END as CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,TPC.TRANTRUCK
        //,NVL(SHM.DELIVERY_NO,'xxx') as SHIPMENT
        //,CASE WHEN TPC.CACTIVE = '1' THEN '' WHEN TPC.CACTIVE = '0' THEN 'ยกเลิกแผน'  ELSE '' END as CACTIVE
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN TSHIPMENT SHM
        //ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN 
        //(
        //     SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE
        //     ,CASE WHEN NVL(TPS.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TPS.SHEADREGISTERNO||'-'||TPS.STRAILERREGISTERNO ELSE TPS.SHEADREGISTERNO END as TRANTRUCK
        //     FROM TPLANSCHEDULE TPS
        //     LEFT JOIN TPLANSCHEDULELIST TPL
        //     ON TPS.NPLANID = TPL.NPLANID
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO
        //WHERE 1=1 AND ODP.CACTIVE = 'Y'  AND TPC.CACTIVE = '1' AND NVL(TPC.STRUCKID,'xxx') <> 'xxx' AND 
        //CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
        //ELSE '' END = '1' 
        //" + Condition + @"
        //ORDER BY DDELIVERY ASC";
        //        string ConditionTerminal = Teminal_ID();
        //        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
        //CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
        //ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
        //ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, SHM.OTHER as NVALUE, ODP.STERMINALID,SHM.CUST_ABBR as STERMINALNAME 
        //,CASE 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') THEN '0'
        //ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
        //,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,NVL(TPC.TRANTRUCK,'xxx') as TRANTRUCK
        //,NVL(SHM.DELIVERY_NO,'xxx') as SHIPMENT
        //,TT.STERMINALNAME as STERMINALNAME_FROM
        //--,PNT.SCONTRACTID
        //FROM TBL_ORDERPLAN ODP
        //INNER JOIN 
        //(
        //      SELECT SCONTRACTID FROM TCONTRACT WHERE STERMINALID||','||SCONT_PLNT LIKE '%" + ConditionTerminal + @"%'
        //)PNT
        //ON ODP.SCONTRACTID = PNT.SCONTRACTID
        //LEFT JOIN 
        //(
        //    SELECT d.DELIVERY_NO,d.SPLNT_CODE,d.OTHER,C.CUST_ABBR,
        //    (CASE 
        //    WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') 
        //    WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)  
        //    ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO
        //    FROM 
        //    (
        //            --SELECT * FROM TDELIVERY
        //            --WHERE DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND TRUNC(DDELIVERY) BETWEEN  " + STARTDATE + @" AND " + ENDDATE + @")
        //             SELECT LM.* FROM TDELIVERY LM
        //            INNER JOIN 
        //            (
        //                SELECT DELIVERY_NO,MAX(DATE_CREATED) as DATE_CREATED,MAX(DELIVERY_DATE) as DELIVERY_DATE FROM TDELIVERY 
        //                GROUP BY DELIVERY_NO
        //            )LL
        //            ON LM.DELIVERY_NO = LL.DELIVERY_NO AND LM.DATE_CREATED = LL.DATE_CREATED AND LM.DELIVERY_DATE = LL.DELIVERY_DATE
        //            WHERE  
        //            LM.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND TRUNC(DDELIVERY) BETWEEN " + STARTDATE + @" AND " + ENDDATE + @")
        //    )d  
        //    LEFT JOIN 
        //    (
        //        --SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
        //            SELECT  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE FROM TRANS_ORDER   LM
        //            INNER JOIN 
        //            (
        //                SELECT  DOC_NO,MAX(DBSYS_DATE) as DBSYS_DATE  FROM TRANS_ORDER  
        //                WHERE NVL(VALTYP,'-') !='REBRAND' AND NVL(STATUS,'N') IN ('N','G')
        //                GROUP BY DOC_NO
        //            )LL
        //            ON LM.DOC_NO = LL.DOC_NO AND LM.DBSYS_DATE = LL.DBSYS_DATE
        //            WHERE NVL(LM.VALTYP,'-') !='REBRAND' AND NVL(LM.STATUS,'N') IN ('N','G')
        //            GROUP BY  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE
        //    )TRN
        //    ON TRN.DOC_NO = d.SALES_ORDER
        //    LEFT JOIN 
        //    (
        //        --SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
        //            SELECT  LM.DOCNO,LM.RECIEVEPOINT FROM TBORDER LM
        //            INNER JOIN
        //            (
        //                SELECT  DOCNO,MAX(UPDATEDATE) as UPDATEDATE FROM TBORDER 
        //                WHERE RECIEVEPOINT IS NOT NULL
        //                GROUP BY DOCNO
        //            )LL
        //            ON LM.DOCNO = LL.DOCNO AND LM.UPDATEDATE = LL.UPDATEDATE
        //            GROUP BY LM.DOCNO,LM.RECIEVEPOINT
        //    )TBO
        //    ON TBO.DOCNO = d.SALES_ORDER
        //    LEFT JOIN TCUSTOMER C ON  C.SHIP_TO =  (CASE  WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0')  WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)   ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) 
        //)SHM
        //ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
        //LEFT JOIN LSTHOLIDAY LHD
        //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
        //LEFT JOIN TWINDOWTIME TWT
        //ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
        // AND   TWT.CDAYTYPE = CASE WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
        //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') THEN '0'
        //ELSE '' END
        //LEFT JOIN 
        //(
        //     SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID
        //     ,CASE WHEN NVL(TPS.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TPS.SHEADREGISTERNO||'-'||TPS.STRAILERREGISTERNO ELSE TPS.SHEADREGISTERNO END as TRANTRUCK
        //     FROM TPLANSCHEDULE TPS
        //     LEFT JOIN TPLANSCHEDULELIST TPL
        //     ON TPS.NPLANID = TPL.NPLANID
        //     WHERE TPL.CACTIVE = '1'
        //)TPC
        //ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //LEFT JOIN TTERMINAL_SAP TT
        //ON TT.STERMINALID = SHM.SPLNT_CODE
        //WHERE 1=1 AND ODP.CACTIVE = 'Y' AND NVL(TPC.TRANTRUCK,'xxx') <> 'xxx'
        //" + Condition + @"
        //ORDER BY DDELIVERY ASC,NVL(ODP.DATE_UPDATE,ODP.DATE_CREATE) DESC";
        #endregion
        return OrderPlanBLL.Instance.GetOrder2(edtStartT3.Text, edtEndT3.Text, ddlNLINET3.SelectedValue, ddlSTERMINALT3.Value + string.Empty, txtSearch3.Text, ddlGroupT3.SelectedValue, cboVendorSearchT3.Value + string.Empty, cboContractSearchT3.Value + string.Empty);
    }

    private void ListData3()
    {


        string Condition = "";
        string ConditionD = "";
        string DateShipment = "";

        string STARTDATE = "";
        string ENDDATE = "";
        if (!string.IsNullOrEmpty(edtStartT4.Text) && !string.IsNullOrEmpty(edtEndT4.Text))
        {
            //DateTime datestart = DateTime.Parse(edtStartT4.Value.ToString());
            //DateTime dateend = DateTime.Parse(edtEndT4.Value.ToString());
            DateTime datestart = DateTime.ParseExact(edtStartT4.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));
            DateTime dateend = DateTime.ParseExact(edtEndT4.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));

            Condition += " AND (TRUNC(ODP.DDELIVERY) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
            ConditionD += " AND (TRUNC(DDELIVERY) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
            DateShipment = " AND (TRUNC(DATE_CREATED) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";

            STARTDATE = "TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
            ENDDATE = "TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        }

        if (!string.IsNullOrEmpty(cboWindowTimeT4.Value + ""))
        {
            Condition += " AND ODP.NWINDOWTIMEID = '" + CommonFunction.ReplaceInjection(cboWindowTimeT4.Value + "") + "'";
        }


        if (!string.IsNullOrEmpty(txtSearch3T4.Text))
        {
            Condition += " AND ODP.SABBREVIATION||ODP.SCONTRACTNO||ODP.SDELIVERYNO||TPC.TRANTRUCK||ODP.SVENDORID||ODP.SCONTRACTID LIKE '%" + CommonFunction.ReplaceInjection(txtSearch3T4.Text) + "%'";
        }


        string ConditionTerminal = Teminal_ID();

        string QUERY = @"SELECT ODP.ORDERID, ODP.SVENDORID, ODP.SABBREVIATION, ODP.SCONTRACTID, ODP.SCONTRACTNO, 
CASE WHEN ODP.ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as TRAN_TYPE,
ODP.DDELIVERY, ODP.NWINDOWTIMEID, ODP.SDELIVERYNO,ODP.DATE_CREATE, ODP.ORDERTYPE,
ODP.SCREATE,ODP.DATE_UPDATE,ODP.SUPDATE, SHM.OTHER as NVALUE, ODP.STERMINALID,SHM.CUST_ABBR as STERMINALNAME 
,CASE 
WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') THEN '0'
ELSE '' END AS CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
,'เที่ยวที่ '||TWT.NLINE||' เวลา '||TWT.TSTART||'-'||TWT.TEND||' น.' as SWINDOWTIMENAME,TPC.ROUND,TPC.STRUCKID,TPC.DPLAN,TPC.TRANTRUCK
,TT.STERMINALNAME as STERMINALNAME_FROM
--,PNT.SCONTRACTID
FROM TBL_ORDERPLAN ODP
INNER JOIN 
(
      SELECT SCONTRACTID FROM TCONTRACT  WHERE STERMINALID||','||SCONT_PLNT LIKE '%" + ConditionTerminal + @"%'
)PNT
ON ODP.SCONTRACTID = PNT.SCONTRACTID
LEFT JOIN 
(
    SELECT d.DELIVERY_NO,d.SPLNT_CODE,d.OTHER,C.CUST_ABBR,
    (CASE 
    WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') 
    WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)  
    ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO
    FROM 
    (
            --SELECT * FROM TDELIVERY
            --WHERE DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND TRUNC(DDELIVERY) BETWEEN  " + STARTDATE + @" AND " + ENDDATE + @")
            SELECT LM.* FROM TDELIVERY LM
            INNER JOIN 
            (
                SELECT DELIVERY_NO,MAX(DATE_CREATED) as DATE_CREATED,MAX(DELIVERY_DATE) as DELIVERY_DATE FROM TDELIVERY 
                GROUP BY DELIVERY_NO
            )LL
            ON LM.DELIVERY_NO = LL.DELIVERY_NO AND LM.DATE_CREATED = LL.DATE_CREATED AND LM.DELIVERY_DATE = LL.DELIVERY_DATE
            WHERE  
            LM.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND TRUNC(DDELIVERY) BETWEEN " + STARTDATE + @" AND " + ENDDATE + @")
    )d  
    LEFT JOIN 
    (
        --SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
            SELECT  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE FROM TRANS_ORDER   LM
            INNER JOIN 
            (
                SELECT  DOC_NO,MAX(DBSYS_DATE) as DBSYS_DATE  FROM TRANS_ORDER  
                WHERE NVL(VALTYP,'-') !='REBRAND' AND NVL(STATUS,'N') IN ('N','G')
                GROUP BY DOC_NO
            )LL
            ON LM.DOC_NO = LL.DOC_NO AND LM.DBSYS_DATE = LL.DBSYS_DATE
            WHERE NVL(LM.VALTYP,'-') !='REBRAND' AND NVL(LM.STATUS,'N') IN ('N','G')
            GROUP BY  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE
    )TRN
    ON TRN.DOC_NO = d.SALES_ORDER
    LEFT JOIN 
    (
        --SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
                    SELECT  LM.DOCNO,LM.RECIEVEPOINT FROM TBORDER LM
                    INNER JOIN
                    (
                        SELECT  DOCNO,MAX(UPDATEDATE) as UPDATEDATE FROM TBORDER 
                        WHERE RECIEVEPOINT IS NOT NULL
                        GROUP BY DOCNO
                    )LL
                    ON LM.DOCNO = LL.DOCNO AND LM.UPDATEDATE = LL.UPDATEDATE
                    GROUP BY LM.DOCNO,LM.RECIEVEPOINT
    )TBO
    ON TBO.DOCNO = d.SALES_ORDER
    LEFT JOIN TCUSTOMER C ON  C.SHIP_TO =  (CASE  WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0')  WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)   ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) 
)SHM
ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
LEFT JOIN LSTHOLIDAY LHD
ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy')  
LEFT JOIN TWINDOWTIME TWT
ON TWT.STERMINALID =  ODP.STERMINALID AND TWT.NLINE = ODP.NWINDOWTIMEID 
 AND   TWT.CDAYTYPE = CASE WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') OR TO_CHAR(ODP.DDELIVERY,'DAY') IN('เสาร์','อาทิตย์') THEN '1' 
WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> NVL(TO_CHAR(ODP.DDELIVERY,'dd/MM/yyyy'),'xxx') THEN '0'
ELSE '' END
LEFT JOIN 
(
     SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID
     ,CASE WHEN NVL(TPS.STRAILERREGISTERNO,'xxx') <> 'xxx' THEN TPS.SHEADREGISTERNO||'-'||TPS.STRAILERREGISTERNO ELSE TPS.SHEADREGISTERNO END as TRANTRUCK
     FROM TPLANSCHEDULE TPS
     LEFT JOIN TPLANSCHEDULELIST TPL
     ON TPS.NPLANID = TPL.NPLANID
     WHERE TPS.CACTIVE = '0'
)TPC
ON ODP.SDELIVERYNO = TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
LEFT JOIN TTERMINAL_SAP TT
ON TT.STERMINALID = SHM.SPLNT_CODE
WHERE ODP.CACTIVE = 'N'
" + Condition + @"
ORDER BY ODP.DDELIVERY ASC,NVL(ODP.DATE_UPDATE,ODP.DATE_CREATE) DESC";

        DataTable dtORDER = CommonFunction.Get_Data(conn, QUERY);
        if (dtORDER.Rows.Count > 0)
        {
            LogUser(MENUID, "S", "ค้าหาข้อมูล แผนที่โดนดึงกลับ", "");
            gvwT4.DataSource = dtORDER;
        }
        gvwT4.DataBind();
    }

    private string Teminal_ID()
    {
        string Result = "";

        string UserID = Session["UserID"] + "";
        DataTable dt_TERMINAL = CommonFunction.Get_Data(conn, "SELECT SUID,SVENDORID FROM TUSER WHERE CGROUP = '2' AND SUID = '" + UserID + "'");
        if (dt_TERMINAL.Rows.Count > 0)
        {
            Result = dt_TERMINAL.Rows[0]["SVENDORID"] + "";
        }
        else
        {
            dt_TERMINAL = CommonFunction.Get_Data(conn, "SELECT SUID,SVENDORID FROM TUSER WHERE CGROUP = '1' AND SUID = '" + UserID + "'");
            if (dt_TERMINAL.Rows.Count > 0)
            {
                Result = "";
            }
            else
            {
                Result = "XXX";
            }
        }

        return Result;
    }

    private void AddTODB(string strQuery)
    {
        if (!string.IsNullOrEmpty(strQuery))
        {
            using (OracleConnection con = new OracleConnection(conn))
            {

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                else
                {

                }

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }

                con.Close();

            }
        }
    }

    #region ลง TPLANSCHEDULE และ List

    //    private void ListData2()
    //    {
    //        string Condition = "";
    //        if (!string.IsNullOrEmpty(edtStart.Text) && !string.IsNullOrEmpty(edtEnd.Text))
    //        {
    //            DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
    //            DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());
    //            Condition += " AND (TRUNC(DDELIVERY) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')) ";
    //        }

    //        if (!string.IsNullOrEmpty(cboWindowTime.Text))
    //        {
    //            Condition += " AND NWINDOWTIMEID = '" + CommonFunction.ReplaceInjection(cboWindowTime.Value + "") + "'";
    //        }

    //        if (!string.IsNullOrEmpty(txtSearch.Text))
    //        {
    //            Condition += " AND SDELIVERYNO = '" + CommonFunction.ReplaceInjection(txtSearch.Text) + "'";
    //        }

    //        if (!string.IsNullOrEmpty(txtSearch2.Text))
    //        {
    //            Condition += " AND SABBREVIATION||SCONTRACTNO LIKE '%" + CommonFunction.ReplaceInjection(txtSearch2.Text) + "%'";
    //        }

    //        //        string QUERY = @"SELECT ORDERID, SVENDORID, SABBREVIATION, SCONTRACTID, SCONTRACTNO, 
    //        //                          CASE WHEN ORDERTYPE = '1' THEN 'งานขนส่งภายในวัน' ELSE 'งานขนส่งล่วงหน้า' END as ORDERTYPE,
    //        //                          DATE_ORDER, NWINDOWTIMEID,SWINDOWTIMENAME, SDELIVERYNO,DATE_CREATE, 
    //        //                          SCREATE, DATE_UPDATE,SUPDATE, CAPACITY, STERMINALID,STERMINALNAME FROM TBL_ORDERPLAN 
    //        //                          WHERE 1=1 " + Condition + " ORDER BY DATE_ORDER ASC";

    //        string QUERY = @"SELECT TPS.NPLANID,TPS.SVENDORID,VEN.SABBREVIATION,TPS.STERMINALID,TML.SABBREVIATION as STERMINALNAME,TPS.SCONTRACTID,TPS.SCONTRACTNO,TPS.DDELIVERY
    //,TPS.STIMEWINDOW,'เที่ยวที่ '||TWT.NLINE||' เวลา'||TWT.TSTART||'-'||TWT.TEND as SWINDOWTIMENAME
    //,TPS.DPLAN,TPL.NVALUE,TPL.SDELIVERYNO
    //,CASE 
    //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
    //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
    //ELSE '' END as CDAYTYPE -- เช็คว่า ถ้าไม่มีวันตรงกะวันหยุด และ CDAYTYPE = 0  ให้ค่าเป็น 1 และถ้า เป็นวันหยุด และ  CDAYTYPE = 1 ให้ค่าเป็น 1
    //,CASE WHEN TPL.TRAN_TYPE  = '1' THEN 'งานขนส่งภายในวัน' WHEN TPL.TRAN_TYPE  = '2' THEN 'งานขนส่งล่วงหน้า' ELSE '' END  as TRAN_TYPE
    //FROM TPLANSCHEDULE TPS
    //LEFT JOIN TVENDOR VEN
    //ON VEN.SVENDORID =  TPS.SVENDORID
    //LEFT JOIN TTERMINAL TML
    //ON TML.STERMINALID =  TPS.STERMINALID 
    //--LEFT JOIN TCONTRACT TCT
    //--ON TCT.SCONTRACTID =  TPS.SCONTRACTID 
    //LEFT JOIN TWINDOWTIME TWT
    //ON TWT.STERMINALID =  TPS.STERMINALID AND TWT.NLINE = TPS.STIMEWINDOW 
    //LEFT JOIN TPLANSCHEDULELIST TPL
    //ON TPL.NPLANID =  TPS.NPLANID
    //LEFT JOIN LSTHOLIDAY LHD
    //ON TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy')  =  TO_CHAR(TPS.DDELIVERY,'dd/MM/yyyy')                               
    //WHERE 1=1  " + Condition + @"
    //AND CASE 
    //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') = 'xxx' AND TWT.CDAYTYPE = '0' THEN '1' 
    //WHEN NVL(TO_CHAR(LHD.DHOLIDAY,'dd/MM/yyyy'),'xxx') <> 'xxx' AND TWT.CDAYTYPE = '1' THEN '1'
    //ELSE '' END = '1' ORDER BY TPS.DDELIVERY ASC";

    //        DataTable dtORDER = CommonFunction.Get_Data(conn, QUERY);
    //        if (dtORDER.Rows.Count > 0)
    //        {
    //            gvw.DataSource = dtORDER;
    //        }
    //        gvw.DataBind();
    //    }

    //    private void AddDataToDataBase2()
    //    {
    //        if (dt.Rows.Count > 0)
    //        {

    //            DataTable dtVendor = CommonFunction.Get_Data(conn, "SELECT SVENDORID,SABBREVIATION FROM TVENDOR");
    //            DataTable dtCONTRACT = CommonFunction.Get_Data(conn, @"SELECT TCR.SCONTRACTID,TCR.SCONTRACTNO,TCR.STERMINALID,TEM.SABBREVIATION FROM TCONTRACT TCR
    //                                                                       LEFT JOIN TTERMINAL TEM
    //                                                                       ON TCR.STERMINALID = TEM.STERMINALID");
    //            // string StrSave = "";
    //            if (rblChoice.Value == "A")
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][5] + "") + "'");
    //                    string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
    //                    string SVENDORID = dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'").Count() > 0 ? dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[2] + "" : "";
    //                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? dtDELIVERY.Rows[0]["OTHER"] + "" : "null";
    //                    string SHIP_TO = dtDELIVERY.Rows.Count > 0 ? dtDELIVERY.Rows[0]["SHIP_TO"] + "" : "null";
    //                    string SWINDOWTIME = dt.Rows[i][4] + "";
    //                    string DELIVERY_NO = dt.Rows[i][5] + "";
    //                    string TRAN_TYPE = dt.Rows[i][2] + "";
    //                    string SCONTRACTNO = dt.Rows[i][1] + "";
    //                    int Check = CommonFunction.Count_Value(conn, "SELECT SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + DELIVERY_NO + @"'");
    //                    //ถ้ามีในระบบให้อัพเดท
    //                    if (Check > 0)
    //                    {
    //                        //dynamic data = gvw.GetRowValues(i, "NPLANID");
    //                        //string NPLANID = data+"";

    //                        UPDATE_TO_TPLANSCHEDULE(SVENDORID, STERMINALID, SCONTRACTID, DELIVERY_NO, sDate, SWINDOWTIME, SCONTRACTNO);
    //                        UPDATE_TO_TPLANSCHEDULELIST(CAPACITY, SHIP_TO, TRAN_TYPE, DELIVERY_NO);
    //                    }
    //                    else //ถ้าไม่มีในระบบให้ Save
    //                    {
    //                        //สร้างไอดี TPLANSCHEDULE
    //                        string genid = CommonFunction.Gen_ID(conn, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");
    //                        //สร้างไอดี TPLANSCHEDULELIST
    //                        string genidLIST = CommonFunction.Gen_ID(conn, "SELECT SPLANLISTID FROM (SELECT SPLANLISTID FROM TPLANSCHEDULELIST ORDER BY SPLANLISTID DESC) WHERE ROWNUM <= 1");
    //                        INSERT_TO_TPLANSCHEDULE(genid, SVENDORID, STERMINALID, SCONTRACTID, sDate, SWINDOWTIME, SCONTRACTNO);
    //                        string NDROP = "1";//เพราะในเฟสสี่จะทำการ split ความจุให้ไม่เกินรถเลยใส่ 1 ได้เลย เพราะจะมีแค่แผนละคัน
    //                        INSERT_TO_TPLANSCHEDULELIST(genidLIST, genid, NDROP, DELIVERY_NO, CAPACITY, SHIP_TO, TRAN_TYPE);
    //                    }

    //                }

    //            }
    //            else//กรณีที่เลือก ให้ทำแต่รายการใหม่
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][5] + "") + "'");
    //                    string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
    //                    string SVENDORID = dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'").Count() > 0 ? dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[2] + "" : "";
    //                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? dtDELIVERY.Rows[0]["OTHER"] + "" : "null";
    //                    string SHIP_TO = dtDELIVERY.Rows.Count > 0 ? dtDELIVERY.Rows[0]["SHIP_TO"] + "" : "null";
    //                    string SWINDOWTIME = dt.Rows[i][4] + "";
    //                    string DELIVERY_NO = dt.Rows[i][5] + "";
    //                    string TRAN_TYPE = dt.Rows[i][2] + "";
    //                    string SCONTRACTNO = dt.Rows[i][1] + "";
    //                    int Check = CommonFunction.Count_Value(conn, "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN WHERE SDELIVERYNO = '" + dt.Rows[i][5] + @"'");
    //                    //ถ้ามีในระบบไม่อัพเดท
    //                    if (Check > 0)
    //                    {

    //                    }
    //                    else //ถ้าไม่มีในระบบให้ Save
    //                    {
    //                        //สร้างไอดี TPLANSCHEDULE
    //                        string genid = CommonFunction.Gen_ID(conn, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");
    //                        //สร้างไอดี TPLANSCHEDULELIST
    //                        string genidLIST = CommonFunction.Gen_ID(conn, "SELECT SPLANLISTID FROM (SELECT SPLANLISTID FROM TPLANSCHEDULELIST ORDER BY SPLANLISTID DESC) WHERE ROWNUM <= 1");
    //                        INSERT_TO_TPLANSCHEDULE(genid, SVENDORID, STERMINALID, SCONTRACTID, sDate, SWINDOWTIME, SCONTRACTNO);
    //                        string NDROP = "1";//เพราะในเฟสสี่จะทำการ split ความจุให้ไม่เกินรถเลยใส่ 1 ได้เลย เพราะจะมีแค่แผนละคัน
    //                        INSERT_TO_TPLANSCHEDULELIST(genidLIST, genid, NDROP, DELIVERY_NO, CAPACITY, SHIP_TO, TRAN_TYPE);
    //                    }
    //                }


    //            }


    //        }
    //    }

    //    private void INSERT_TO_TPLANSCHEDULE(string genid, string SVENDORID, string STERMINALID, string SCONTRACTID, string sDate, string STIMEWINDOW, string SCONTRACTNO)
    //    {
    //        string sUserID = Session["UserID"] + "";

    //        string QUERY = @"
    //            INSERT INTO TPLANSCHEDULE(NPLANID,NNO,SVENDORID,STERMINALID,SCONTRACTID,DPLAN,SPLANDATE,DDELIVERY,STIMEWINDOW,CACTIVE,DCREATE,SCREATE,SCONTRACTNO) 
    //            VALUES ( " + genid + @",
    //             1,
    //             '" + SVENDORID + @"',
    //             '" + STERMINALID + @"',
    //             '" + SCONTRACTID + @"',
    //             SYSDATE,
    //             TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    //             TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    //             " + CommonFunction.ReplaceInjection(STIMEWINDOW) + @",
    //             '1',
    //             SYSDATE,
    //             '" + sUserID + @"',
    //             '" + SCONTRACTNO + "')";

    //        AddTODB(QUERY);
    //    }

    //    private void INSERT_TO_TPLANSCHEDULELIST(string genid, string NPLANID, string NDROP, string SDELIVERYNO, string CAPACITY, string SSHIPTO, string TRAN_TYPE)
    //    {
    //        string QUERY = @"
    //            INSERT INTO TPLANSCHEDULELIST(SPLANLISTID,NPLANID,NDROP,SDELIVERYNO,NVALUE,CACTIVE,SSHIPTO,TRAN_TYPE) 
    //            VALUES ( '" + genid + @"',
    //             " + NPLANID + @",
    //             '" + NDROP + @"',
    //             '" + SDELIVERYNO + @"',
    //             " + CAPACITY + @",
    //             '1',
    //             '" + CommonFunction.ReplaceInjection(SSHIPTO) + @"',
    //             '" + CommonFunction.ReplaceInjection(TRAN_TYPE) + "')";

    //        AddTODB(QUERY);
    //    }

    //    private void UPDATE_TO_TPLANSCHEDULE(string SVENDORID, string STERMINALID, string SCONTRACTID, string SDELIVERYNO, string sDate, string STIMEWINDOW, string SCONTRACTNO)
    //    {
    //        string sUserID = Session["UserID"] + "";
    //        string CHK = "SELECT NPLANID,SDELIVERYNO FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + SDELIVERYNO + "'";
    //        DataTable dt = CommonFunction.Get_Data(conn, CHK);
    //        string PLANID = dt.Rows[0]["NPLANID"] + "";
    //        if (dt.Rows.Count > 0)
    //        {
    //            string QUERY = @"UPDATE TPLANSCHEDULE 
    //                         SET
    //                            SVENDORID = '" + SVENDORID + @"',
    //                            STERMINALID = '" + STERMINALID + @"',
    //                            SCONTRACTID = '" + SCONTRACTID + @"',
    //                            SPLANDATE =   TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    //                            DDELIVERY =   TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    //                            STIMEWINDOW =  " + CommonFunction.ReplaceInjection(STIMEWINDOW) + @",
    //                            DUPDATE = sysdate,
    //                            SUPDATE = '" + sUserID + @"',
    //                            SCONTRACTNO = '" + SCONTRACTNO + @"'
    //                            WHERE NPLANID = " + PLANID + @"";

    //            AddTODB(QUERY);
    //        }
    //    }

    //    private void UPDATE_TO_TPLANSCHEDULELIST(string CAPACITY, string SSHIPTO, string TRAN_TYPE, string SDELIVERYNO)
    //    {
    //        string sUserID = Session["UserID"] + "";
    //        string QUERY = @"UPDATE TPLANSCHEDULELIST 
    //                                SET
    //                                    NVALUE =   " + CAPACITY + @",
    //                                    SSHIPTO =   '" + CommonFunction.ReplaceInjection(SSHIPTO) + @"',
    //                                    TRAN_TYPE =  '" + CommonFunction.ReplaceInjection(TRAN_TYPE) + @"'
    //                                    WHERE SDELIVERYNO = '" + SDELIVERYNO + @"'";

    //        AddTODB(QUERY);
    //    }

    #endregion

    private string genID()
    {
        string sql = @"SELECT ORDERID FROM TBL_ORDERPLAN WHERE  ORDERID LIKE '%" + DateTime.Now.ToString("yyMMdd") + "%' ORDER BY ORDERID DESC";
        string Result = "";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, sql);
        if (dt.Rows.Count > 0)
        {
            string sss = dt.Rows[0]["ORDERID"] + "";

            if (!string.IsNullOrEmpty(sss))
            {
                sss = sss.Substring(7, 4);

                Result = DateTime.Now.ToString("yyMMdd") + "-" + (int.Parse(sss) + 1).ToString().PadLeft(4, '0') + "";
            }
            else
            {

            }
        }
        else
        {

            Result = DateTime.Now.ToString("yyMMdd") + "-0001";
        }

        return Result;
    }

    private string CheckError(string SVENDORID, string SVENDORNAME, string SCONTRACTID, string SCONTRACTNO, string STERMINALID, string STERMINALNAME, string SHIP_TO, string CAPACITY, string SDELIVERYNO, string ORDERTYPE, string NWINDOWTIMEID, string sDate, string DO_SHOW, bool IS_GETDO, DateTime Dtemp)
    {
        string Result = "";

        //if (!string.IsNullOrEmpty(SDELIVERYNO))
        //{

        string ErrorText = "";
        string ErrorRemark = "";

        //ใส่ Remark
        if (string.IsNullOrEmpty(SVENDORID))
        {
            ErrorRemark += "- ไม่พบบริษัท " + SVENDORNAME + " ในระบบ<br>";
        }

        if (string.IsNullOrEmpty(SCONTRACTID))
        {
            ErrorRemark += "- ไม่พบเลขที่สัญญา " + SCONTRACTNO + " ในระบบ<br>";
        }

        if (string.IsNullOrEmpty(ORDERTYPE))
        {
            ErrorRemark += "- ไม่ได้กำหนดประเภทการขนส่ง<br>";
        }

        if (string.IsNullOrEmpty(sDate))
        {
            ErrorRemark += "- ไม่ได้กำหนดวันที่ขนส่ง<br>";
        }

        if (string.IsNullOrEmpty(NWINDOWTIMEID))
        {
            ErrorRemark += "- ไม่ได้กำหนดกะการขนส่ง<br>";
        }

        if (string.IsNullOrEmpty(SDELIVERYNO))
        {
            if (IS_GETDO)
            {
                ErrorRemark += "- ไม่สามารถดึง DO จาก TRANS_DO เนื่องจากไม่พบเลข DO " + DO_SHOW + "<br>";
            }
            else
            {
                ErrorRemark += "- ไม่พบเลข DO " + DO_SHOW + "<br>";
            }
        }

        if (string.IsNullOrEmpty(STERMINALID))
        {
            ErrorRemark += "- ไม่พบคลังจากสัญญา<br>";
        }

        //if (!string.IsNullOrEmpty(SHIP_TO))
        //{
        //    ErrorText += " ไม่พบ" + SHIP_TO + "ในระบบ";
        //}

        //if (string.IsNullOrEmpty(CAPACITY))
        //{
        //    ErrorRemark += "- ไม่มีปริมาตรการขนส่งในระบบ</br>";
        //}

        DataTable dtFIFO = CommonFunction.Get_Data(conn, @"SELECT PCL.NPLANID,PCL.SDELIVERYNO,PS.CFIFO
FROM  TPLANSCHEDULELIST PCL 
LEFT JOIN TPLANSCHEDULE PS
ON PS.NPLANID = PCL.NPLANID
WHERE PCL.SDELIVERYNO = '" + CommonFunction.ReplaceInjection(DO_SHOW) + "' AND NVL(PS.CFIFO,'xxx') = '1' AND NVL(PCL.CACTIVE,'1') = '1'");
        if (dtFIFO.Rows.Count > 0)
        {
            ErrorRemark += "- " + DO_SHOW + " เลข DO นี้ได้มีการจัดแผนแบบ FIFO แล้ว<br>";
        }

        DataTable dtMAP_DO = CommonFunction.Get_Data(conn, @"SELECT LPAD(M_NO ,10,0) as M_NO FROM  TBMAP_DO@ESS WHERE  LPAD(M_NO ,10,0)  = '" + CommonFunction.ReplaceInjection(DO_SHOW) + "'");
        if (dtMAP_DO.Rows.Count > 0)
        {
            ErrorRemark += "- " + DO_SHOW + " เลข DO นี้ได้มีการจัดแผนจากทาง Iterminal แล้ว<br>";
        }




        //ถ้ามี error นำไปใส่ Result เพื่อรีเทินข้อความกลับ
        if (!string.IsNullOrEmpty(ErrorText))
        {
            if (!string.IsNullOrEmpty(ErrorRemark))
            {
                //เช็คว่าถ้ามี SVENDORID SCONTRACTID STERMINALID SDELIVERYNO ถือว่าไม่ Error
                //if (!string.IsNullOrEmpty(SVENDORID) && !string.IsNullOrEmpty(SCONTRACTID) && !string.IsNullOrEmpty(STERMINALID) && !string.IsNullOrEmpty(SDELIVERYNO) && dtFIFO.Rows.Count == 0)
                if (!string.IsNullOrEmpty(SVENDORID) && !string.IsNullOrEmpty(SCONTRACTID) && !string.IsNullOrEmpty(STERMINALID) && !string.IsNullOrEmpty(SDELIVERYNO))
                {
                    if (dtFIFO.Rows.Count > 0)
                    {
                        Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
                    }
                    else if (dtMAP_DO.Rows.Count > 0)
                    {
                        Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
                    }
                    else
                    {
                        Result = "<tr>" + ErrorText + "<td>&nbsp;</td></tr>";
                    }
                }
                else
                {
                    Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
                }
            }
            else
            {
                //เช็คว่าถ้ามี SVENDORID SCONTRACTID STERMINALID SDELIVERYNO ถือว่าไม่ Error
                if (!string.IsNullOrEmpty(SVENDORID) && !string.IsNullOrEmpty(SCONTRACTID) && !string.IsNullOrEmpty(STERMINALID) && !string.IsNullOrEmpty(SDELIVERYNO))
                {
                    Result = "";
                }
                else
                {
                    Result = "<tr>" + ErrorText + "<td>&nbsp;</td></tr>";
                }
            }
        }
        //}
        DateTime dtt = DateTime.Today.AddDays(6);
        if (!(Dtemp >= DateTime.Today && Dtemp <= dtt))
        {
            //ErrorText = DO_SHOW;

            ErrorRemark = "- " + Dtemp.ToString("dd/MM/yyyy") + " วันที่ไม่อยู่ในช่วงที่สามารถเพิ่ม DO ได้<br>";
            Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
        }

        return Result;
    }

    protected void cboContract_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxComboBox cboVendor;//= gvw.FindEditFormTemplateControl("cboVendor") as ASPxComboBox;
        ASPxComboBox cboContrat = source as ASPxComboBox;
        switch (e.Parameter)
        {
            case "Add":
                cboVendor = this.cboVendor;
                break;
            case "Search":
                switch (ASPxPageControl1.ActiveTabIndex)
                {

                    case 1:
                        cboVendor = this.cboVendorSearch;
                        break;
                    case 2:
                        cboVendor = this.cboVendorSearchT3;
                        break;
                    default:
                        cboVendor = this.cboVendorSearch;
                        break;
                }


                break;
            default:
                cboVendor = gvw.FindEditFormTemplateControl("cboVendor") as ASPxComboBox;
                break;
        }

        if (!string.IsNullOrEmpty(cboVendor.Value + ""))
        {

            DataTable dt = CommonFunction.Get_Data(conn, "SELECT SCONTRACTID,SCONTRACTNO,SVENDORID FROM TCONTRACT WHERE SVENDORID = '" + cboVendor.Value + "' AND NVL(CACTIVE,'Y') = 'Y'");
            if (dt.Rows.Count > 0)
            {
                ViewState["DataCONTRACT"] = dt;
                DropDownListHelper.BindDropDownList(ref cboContrat, dt, "SCONTRACTID", "SCONTRACTNO", true);
                //cboContrat.DataSource = dt;
                //cboContrat.DataBind();
            }
        }
    }

    protected void cboContractT3_Callback(object source, CallbackEventArgsBase e)
    {

        ASPxComboBox cboVendor = gvwT3.FindEditFormTemplateControl("cboVendor") as ASPxComboBox;
        ASPxComboBox cboContrat = gvwT3.FindEditFormTemplateControl("cboContractT3") as ASPxComboBox;

        if (!string.IsNullOrEmpty(cboVendor.Value + ""))
        {

            DataTable dt = CommonFunction.Get_Data(conn, "SELECT SCONTRACTID,SCONTRACTNO,SVENDORID FROM TCONTRACT WHERE SVENDORID = '" + cboVendor.Value + "'  AND NVL(CACTIVE,'Y') = 'Y'");
            if (dt.Rows.Count > 0)
            {
                cboContrat.DataSource = dt;
                cboContrat.DataBind();
            }
        }
    }

    #region  OldCode Add To DB
    //    private string genID()
    //    {
    //        string sql = @"SELECT ORDERID FROM TBL_ORDERPLAN ORDER BY ORDERID DESC";
    //        string Result = "";
    //        DataTable dt = new DataTable();
    //        dt = CommonFunction.Get_Data(conn, sql);
    //        if (dt.Rows.Count > 0)
    //        {
    //            string sss = dt.Rows[0]["ORDERID"] + "";

    //            if (!string.IsNullOrEmpty(sss))
    //            {
    //                sss = sss.Substring(7, 4);

    //                Result = DateTime.Now.ToString("yyMMdd") + "-" + (int.Parse(sss) + 1).ToString().PadLeft(4, '0') + "";
    //            }
    //            else
    //            {

    //            }
    //        }
    //        else
    //        {

    //            Result = DateTime.Now.ToString("yyMMdd") + "-0001";
    //        }

    //        return Result;
    //    }

    //    private void AddDataToDataBase()
    //    {
    //        if (dt.Rows.Count > 0)
    //        {

    //            DataTable dtVendor = CommonFunction.Get_Data(conn, "SELECT SVENDORID,SABBREVIATION FROM TVENDOR");
    //            DataTable dtCONTRACT = CommonFunction.Get_Data(conn, @"SELECT TCR.SCONTRACTID,TCR.SCONTRACTNO,TCR.STERMINALID,TEM.SABBREVIATION FROM TCONTRACT TCR
    //                                                                   LEFT JOIN TTERMINAL TEM
    //                                                                   ON TCR.STERMINALID = TEM.STERMINALID");

    //            DataTable dtWINDOWTIME = CommonFunction.Get_Data(conn, "SELECT STERMINALID,NLINE,'เที่ยวที '||NLINE||' เวลา '||TSTART||'-'||TEND as Detail FROM TWINDOWTIME WHERE CACTIVE = '1'");



    //            string StrSave = "";
    //            if (rblChoice.Value == "A")
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][5] + "") + "'");
    //                    string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
    //                    string SVENDORID = dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'").Count() > 0 ? dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[2] + "" : "";
    //                    string STERMINALNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[3] + "" : "";
    //                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? dtDELIVERY.Rows[0]["OTHER"] + "" : "null";
    //                    string WINDOWTIMENAME = dtWINDOWTIME.Select("NLINE = " + CommonFunction.ReplaceInjection(dt.Rows[i][4] + "") + " AND STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + "'").Count() > 0 ? dtWINDOWTIME.Select("NLINE = " + CommonFunction.ReplaceInjection(dt.Rows[i][4] + "") + @" AND STERMINALID ='" + CommonFunction.ReplaceInjection(STERMINALID) + "'")[0].ItemArray[2] + "" : "";

    //                    int Check = CommonFunction.Count_Value(conn, "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN WHERE SDELIVERYNO = '" + dt.Rows[i][5] + @"'");
    //                    //ถ้ามีในระบบให้อัพเดท
    //                    if (Check > 0)
    //                    {
    //                        StrSave = @"
    //                                    UPDATE TBL_ORDERPLAN
    //                                    SET    SVENDORID     = '" + SVENDORID + @"',
    //                                           SABBREVIATION = '" + dt.Rows[i][0] + @"',
    //                                           SCONTRACTID   = '" + SCONTRACTID + @"',
    //                                           SCONTRACTNO   = '" + dt.Rows[i][1] + @"',
    //                                           ORDERTYPE     = '" + dt.Rows[i][2] + @"',
    //                                           DATE_ORDER    = TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    //                                           NWINDOWTIMEID = " + dt.Rows[i][4] + @",
    //                                           SWINDOWTIMENAME = '" + WINDOWTIMENAME + @"',
    //                                           DATE_UPDATE   = SYSDATE,
    //                                           SUPDATE       = '" + UserID + @"',
    //                                           STERMINALID    = '" + STERMINALID + @"',
    //                                           STERMINALNAME  = '" + STERMINALNAME + @"',
    //                                           CAPACITY       = " + CAPACITY + @"
    //                                    WHERE  SDELIVERYNO   = '" + dt.Rows[i][5] + @"'
    //                        ";
    //                    }
    //                    else //ถ้าไม่มีในระบบให้ Save
    //                    {
    //                        StrSave = @"
    //INSERT INTO TBL_ORDERPLAN (ORDERID,SVENDORID,SABBREVIATION,SCONTRACTID,SCONTRACTNO,ORDERTYPE,DATE_ORDER,NWINDOWTIMEID,SWINDOWTIMENAME, SDELIVERYNO,DATE_CREATE, SCREATE,STERMINALID,STERMINALNAME,CAPACITY) 
    //VALUES ( '" + genID() + @"',
    // '" + SVENDORID + @"',
    // '" + dt.Rows[i][0] + @"',
    // '" + SCONTRACTID + @"',
    // '" + dt.Rows[i][1] + @"',
    // '" + dt.Rows[i][2] + @"',
    // TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    // " + dt.Rows[i][4] + @",
    // '" + WINDOWTIMENAME + @"',
    // '" + dt.Rows[i][5] + @"',
    // SYSDATE,
    // '" + UserID + @"',
    // '" + STERMINALID + @"',
    // '" + STERMINALNAME + @"',
    // " + CAPACITY + @")";
    //                    }
    //                    AddTODB(StrSave);
    //                }

    //            }
    //            else
    //            {
    //                for (int i = 0; i < dt.Rows.Count; i++)
    //                {
    //                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][5] + "") + "'");
    //                    string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
    //                    string SVENDORID = dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'").Count() > 0 ? dtVendor.Select("SABBREVIATION = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'")[0].ItemArray[0] + "" : "";
    //                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'")[0].ItemArray[2] + "" : "";
    //                    string STERMINALNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][0] + "") + @"'")[0].ItemArray[3] + "" : "";
    //                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? dtDELIVERY.Rows[i]["OTHER"] + "" : "null";
    //                    string WINDOWTIMENAME = dtWINDOWTIME.Select("NWINDOWTIMEID = '" + CommonFunction.ReplaceInjection(dt.Rows[i][4] + "") + "'").Count() > 0 ? dtCONTRACT.Select("NWINDOWTIMEID = '" + CommonFunction.ReplaceInjection(dt.Rows[i][4] + "") + @"'")[0].ItemArray[1] + "" : "";

    //                    int Check = CommonFunction.Count_Value(conn, "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN WHERE SDELIVERYNO = '" + dt.Rows[i][5] + @"'");
    //                    //ถ้ามีในระบบไม่อัพเดท
    //                    if (Check > 0)
    //                    {

    //                    }
    //                    else //ถ้าไม่มีในระบบให้ Save
    //                    {
    //                        StrSave = @"
    //INSERT INTO TBL_ORDERPLAN (ORDERID,SVENDORID,SABBREVIATION,SCONTRACTID,SCONTRACTNO,ORDERTYPE,DATE_ORDER,NWINDOWTIMEID,SWINDOWTIMENAME, SDELIVERYNO,DATE_CREATE, SCREATE,STERMINALID,STERMINALNAME,CAPACITY) 
    //VALUES ( '" + genID() + @"',
    // '" + SVENDORID + @"',
    // '" + dt.Rows[i][0] + @"',
    // '" + SCONTRACTID + @"',
    // '" + dt.Rows[i][1] + @"',
    // '" + dt.Rows[i][2] + @"',
    // TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
    // " + dt.Rows[i][4] + @",
    // '" + WINDOWTIMENAME + @"',
    // '" + dt.Rows[i][5] + @"',
    // SYSDATE,
    // '" + UserID + @"',
    // '" + STERMINALID + @"',
    // '" + STERMINALNAME + @"',
    // " + CAPACITY + @")";
    //                    }

    //                    AddTODB(StrSave);
    //                }


    //            }


    //        }
    //    }
    #endregion

    #region AutoComplete

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
          FROM ( 
                SELECT TVENDOR.SVENDORID,TVENDOR.SABBREVIATION FROM TVENDOR
                INNER JOIN 
                (
                    SELECT 
                       SVENDORID, TO_DATE(DEND) - TO_DATE(sysdate) as DateCountdown
                    FROM TCONTRACT
                    WHERE   TO_DATE(DEND) - TO_DATE(sysdate)  >= 0
                )O
                ON TVENDOR.SVENDORID = O.SVENDORID
                GROUP BY TVENDOR.SVENDORID,TVENDOR.SABBREVIATION)
         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboContract_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsConTract.SelectCommand = @"SELECT SCONTRACTID , SCONTRACTTYPEID FROM (SELECT ROW_NUMBER()OVER(ORDER BY SCONTRACTID) AS RN , SCONTRACTID , SCONTRACTTYPEID
          FROM TCONTRACT
          WHERE SCONTRACTTYPEID LIKE :fillter OR SCONTRACTID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsConTract.SelectParameters.Clear();
        sdsConTract.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsConTract.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsConTract.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsConTract;
        comboBox.DataBind();

    }

    protected void cboContract_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsDelivery.SelectCommand = @"SELECT DELIVERY_NO,SHIP_TO,SCUSTOMER,NVALUE FROM(
SELECT d.DELIVERY_NO,d.SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
, ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN 
FROM TDELIVERY d  
LEFT JOIN TCUSTOMER_SAP ct ON D.SHIP_TO = CT.SHIP_TO  
WHERE  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-30 AND d.DELIVERY_NO LIKE :fillter 
) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsDelivery.SelectParameters.Clear();
        sdsDelivery.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsDelivery.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsDelivery.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsDelivery;
        comboBox.DataBind();

        if (comboBox.Items.Count <= 0)
        {
            comboBox.Value = "";
        }

    }

    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboTeminal_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTeminal.SelectCommand = @"SELECT STERMINALID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY STERMINALID) AS RN , STERMINALID , SABBREVIATION
          FROM TTERMINAL WHERE SUBSTR(STERMINALID,0,1) IN ('5','6','8') AND CACTIVE='1' AND STERMINALID||SABBREVIATION LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTeminal.SelectParameters.Clear();
        sdsTeminal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTeminal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTeminal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTeminal;
        comboBox.DataBind();


    }

    protected void cboTeminal_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    #endregion

    protected void ASPxPageControl1_ActiveTabChanged(object source, TabControlEventArgs e)
    {

    }

    private void INS_TO_HISTORY(string ORDERID, string NVERSION)
    {
        string QUERY = @"INSERT INTO TBL_ORDERPLAN_HISTORY 
(ORDERID,SVENDORID,SABBREVIATION,SCONTRACTID,SCONTRACTNO,ORDERTYPE,DDELIVERY,NWINDOWTIMEID,SDELIVERYNO
,DATE_CREATE,SCREATE,DATE_UPDATE,SUPDATE,NVALUE,STERMINALID,STERMINALNAME,CACTIVE,NVERSION)
    SELECT
       ODP.ORDERID ,
       ODP.SVENDORID ,
       ODP.SABBREVIATION,
       ODP.SCONTRACTID,
       ODP.SCONTRACTNO,
       ODP.ORDERTYPE,
       ODP.DDELIVERY,
       ODP.NWINDOWTIMEID,
       ODP.SDELIVERYNO,
       ODP.DATE_CREATE,
       ODP.SCREATE,
       ODP.DATE_UPDATE,
       ODP.SUPDATE,
       ODP.NVALUE,
       ODP.STERMINALID,
       ODP.STERMINALNAME,
       ODP.CACTIVE,
       " + NVERSION + @"
    FROM TBL_ORDERPLAN ODP WHERE ODP.ORDERID = '" + ORDERID + "'";

        AddTODB(QUERY);
    }

    private bool SendMail(string ORDERID)
    {
        string sHTML_O = "";
        string sMsg_O = "";
        string sHTML_N = "";
        string sMsg_N = "";


        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
               , _to_O = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
               , _to_N = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
               , sSubject = "มีการเปลี่ยนแผน";

        string QUERY_O = "SELECT ORDERID,SVENDORID,NVERSION FROM  TBL_ORDERPLAN_HISTORY WHERE ORDERID = '" + ORDERID + "' ORDER BY NVERSION DESC";
        string QUERY_N = "SELECT ORDERID,SVENDORID FROM  TBL_ORDERPLAN WHERE ORDERID = '" + ORDERID + "'";

        DataTable dt_O = CommonFunction.Get_Data(conn, QUERY_O);
        DataTable dt_N = CommonFunction.Get_Data(conn, QUERY_N);

        string SVENDERID_O = dt_O.Rows.Count > 0 ? dt_O.Rows[0]["SVENDORID"] + "" : "";
        string SVENDERID_N = dt_N.Rows.Count > 0 ? dt_N.Rows[0]["SVENDORID"] + "" : "";

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            string mail_O = SystemFunction.GetUserMailForSend(SVENDERID_O, MENUID);
            string mail_N = SystemFunction.GetUserMailForSend(SVENDERID_N, MENUID);
            _to_O = mail_O;
            _to_N = mail_N;
        }

        sHTML_O = "มีการแก้ไขแผนผู้ขนส่งรายเก่า";
        sHTML_N = "มีการแก้ไขแผนผู้ขนส่งรายใหม่";

        sMsg_O = sHTML_O;
        sMsg_N = sHTML_N;

        OracleConnection con = new OracleConnection(conn);
        con.Open();

        bool SMAIL_O = CommonFunction.SendNetMail(_from, _to_O, sSubject, sMsg_O, con, "", "", "", "", "", "0");
        bool SMAIL_N = CommonFunction.SendNetMail(_from, _to_N, sSubject, sMsg_N, con, "", "", "", "", "", "0");

        bool sendercompletall = true;

        if (SMAIL_O == false || SMAIL_N == false)
        {
            sendercompletall = false;
        }

        return sendercompletall;
    }

    private bool SendMailDel(string ORDERID)
    {
        //string sHTML_O = "";
        //string sMsg_O = "";
        string sHTML_N = "";
        string sMsg_N = "";


        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            // , _to_O = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
               , _to_N = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
               , sSubject = "มีการเปลี่ยนแผน";

        //  string QUERY_O = "SELECT ORDERID,SVENDORID,NVERSION FROM  TBL_ORDERPLAN_HISTORY WHERE ORDERID = '" + ORDERID + "' ORDER BY NVERSION DESC";

        ORDERID = !string.IsNullOrEmpty(ORDERID) ? "(" + ORDERID.Remove(0, 1).Replace(";", ",") + ")" : "";

        string QUERY_N = "SELECT ORDERID,SVENDORID FROM  TBL_ORDERPLAN WHERE ORDERID IN " + ORDERID + "";

        //  DataTable dt_O = CommonFunction.Get_Data(conn, QUERY_O);
        DataTable dt_N = CommonFunction.Get_Data(conn, QUERY_N);

        //   string SVENDERID_O = dt_O.Rows.Count > 0 ? dt_O.Rows[0]["SVENDORID"] + "" : "";
        string SVENDERID_N = dt_N.Rows.Count > 0 ? dt_N.Rows[0]["SVENDORID"] + "" : "";

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            // string mail_O = SystemFunction.GetUserMailForSend(SVENDERID_O, MENUID);
            string mail_N = SystemFunction.GetUserMailForSend(SVENDERID_N, MENUID);
            // _to_O = mail_O;
            _to_N = mail_N;
        }

        // sHTML_O = "มีการแก้ไขแผนผู้ขนส่งรายเก่า";
        sHTML_N = "มีการดึงแผนกลับ";

        // sMsg_O = sHTML_O;
        sMsg_N = sHTML_N;

        OracleConnection con = new OracleConnection(conn);
        con.Open();

        // bool SMAIL_O = CommonFunction.SendNetMail(_from, _to_O, sSubject, sMsg_O, con, "", "", "", "", "", "0");
        bool SMAIL_N = CommonFunction.SendNetMail(_from, _to_N, sSubject, sMsg_N, con, "", "", "", "", "", "0");

        //bool sendercompletall = true;

        //if (SMAIL_O == false || SMAIL_N == false)
        //{
        //    sendercompletall = false;
        //}

        return SMAIL_N;
    }

    private DataTable MergeSubGroup(string QUERY)
    {

        DataTable dt = new DataTable();
        dt.Rows.Clear();
        dt.Columns.Add("SABBREVIATION", typeof(string));
        dt.Columns.Add("SCONTRACTNO", typeof(string));
        dt.Columns.Add("STERMINALNAME", typeof(string));
        dt.Columns.Add("STRUCKREGIS", typeof(string));
        dt.Columns.Add("CAPACITY", typeof(int));
        dt.Columns.Add("SCONTRACTID", typeof(string));
        DataTable dt_DATA = CommonFunction.Get_Data(conn, QUERY);
        DataRow rows;
        int nInsertemp = 0;
        int nNo = 0;
        for (int i = 0; i < dt_DATA.Rows.Count; i++)
        {
            string HEADER = dt_DATA.Rows[i]["SABBREVIATION"] + "" + dt_DATA.Rows[i]["COUNTCONTRACT"] + "" + dt_DATA.Rows[i]["COUNTTRUCKCONALL"] + "";
            string SUBHEADER = dt_DATA.Rows[i]["SCONTRACTNO"] + "" + dt_DATA.Rows[i]["COUNTTRUCKCON"] + "";
            string DETAIL = dt_DATA.Rows[i]["STRUCKREGIS"] + "" + dt_DATA.Rows[i]["CAPACITY"] + "" + dt_DATA.Rows[i]["STERMINALNAME"] + "";

            string SVENDORNAME = "";
            string CONTRACT = "";
            string STERMINALNAME = "";
            string STRUCK = "";
            string CAPACITY = "";
            string SCONTRACTID = "";

            if (("" + ViewState["HEADER"]) != HEADER)
            {
                ViewState["HEADER"] = HEADER;

                SVENDORNAME = dt_DATA.Rows[i]["SABBREVIATION"] + "";
                CONTRACT = !string.IsNullOrEmpty(dt_DATA.Rows[i]["COUNTCONTRACT"] + "") ? dt_DATA.Rows[i]["COUNTCONTRACT"] + " สัญญา" : " - สัญญา";
                STRUCK = !string.IsNullOrEmpty(dt_DATA.Rows[i]["COUNTTRUCKCONALL"] + "") ? dt_DATA.Rows[i]["COUNTTRUCKCONALL"] + " คัน" : " - คัน";
                CAPACITY = "";
                STERMINALNAME = "";
                dt.Rows.Add(SVENDORNAME, CONTRACT, null, STRUCK, null, null);


                if (("" + ViewState["SUBHEADER"]) != SUBHEADER)
                {
                    ViewState["SUBHEADER"] = SUBHEADER;

                    SVENDORNAME = "";
                    CONTRACT = dt_DATA.Rows[i]["SCONTRACTNO"] + "";
                    STRUCK = !string.IsNullOrEmpty(dt_DATA.Rows[i]["COUNTTRUCKCON"] + "") ? dt_DATA.Rows[i]["COUNTTRUCKCON"] + " คัน" : " - คัน";
                    CAPACITY = "";
                    STERMINALNAME = "";
                    dt.Rows.Add(null, CONTRACT, null, STRUCK, null, null);
                }

                if (("" + ViewState["DETAIL"]) != DETAIL)
                {
                    ViewState["DETAIL"] = DETAIL;

                    SVENDORNAME = "";
                    CONTRACT = "";
                    STRUCK = dt_DATA.Rows[i]["STRUCKREGIS"] + "";
                    CAPACITY = !string.IsNullOrEmpty(dt_DATA.Rows[i]["CAPACITY"] + "") ? dt_DATA.Rows[i]["CAPACITY"] + "" : null;
                    STERMINALNAME = dt_DATA.Rows[i]["STERMINALNAME"] + "";
                    SCONTRACTID = dt_DATA.Rows[i]["SCONTRACTID"] + "";
                    dt.Rows.Add(null, CONTRACT, STERMINALNAME, STRUCK, CAPACITY, SCONTRACTID);
                }
            }
            else
            {
                if (("" + ViewState["SUBHEADER"]) != SUBHEADER)
                {
                    ViewState["SUBHEADER"] = SUBHEADER;

                    SVENDORNAME = "";
                    CONTRACT = dt_DATA.Rows[i]["SCONTRACTNO"] + "";
                    STRUCK = dt_DATA.Rows[i]["COUNTTRUCKCON"] + "";
                    CAPACITY = "";
                    STERMINALNAME = "";
                    dt.Rows.Add(null, CONTRACT, null, STRUCK, null);
                }

                if (("" + ViewState["DETAIL"]) != DETAIL)
                {
                    ViewState["DETAIL"] = DETAIL;

                    SVENDORNAME = "";
                    CONTRACT = "";
                    STRUCK = dt_DATA.Rows[i]["STRUCKREGIS"] + "";
                    CAPACITY = !string.IsNullOrEmpty(dt_DATA.Rows[i]["CAPACITY"] + "") ? dt_DATA.Rows[i]["CAPACITY"] + "" : null;
                    STERMINALNAME = dt_DATA.Rows[i]["STERMINALNAME"] + "";
                    SCONTRACTID = dt_DATA.Rows[i]["SCONTRACTID"] + "";
                    dt.Rows.Add(null, CONTRACT, STERMINALNAME, STRUCK, CAPACITY, SCONTRACTID);
                }
            }
        }
        ViewState["HEADER"] = "";
        ViewState["SUBHEADER"] = "";
        ViewState["DETAIL"] = "";
        return dt;
    }
    protected string GetSupplyPlant(string DO_NO)
    {
        DataTable _dt = CommonFunction.Get_Data(conn, @"SELECT TBL.DO_NO ,TBL.PLNT_CODE ,TBL_MAXSTATUS.STATUS 
FROM(
    select DO_NO, PLNT_CODE ,(CASE WHEN NVL(STATUS,'N')='N' THEN 0 ELSE 9 END)  STATUS
    from TRANS_DO 
    WHERE 1=1 AND DO_NO='" + DO_NO + @"'
    GROUP BY DO_NO, PLNT_CODE ,(CASE WHEN NVL(STATUS,'N')='N' THEN 0 ELSE 9 END) 
) TBL
LEFT JOIN (
    select DO_NO, PLNT_CODE  ,MAX(CASE WHEN NVL(STATUS,'N')='N' THEN 0 ELSE 9 END)  STATUS
    from TRANS_DO 
    WHERE 1=1 and  DO_NO='" + DO_NO + @"'
    GROUP BY DO_NO, PLNT_CODE
) TBL_MAXSTATUS ON TBL.DO_NO=TBL_MAXSTATUS.DO_NO AND TBL.STATUS=TBL_MAXSTATUS.STATUS AND  TBL.PLNT_CODE =TBL_MAXSTATUS.PLNT_CODE
WHERE 1=1 AND TBL.DO_NO='" + DO_NO + @"'");
        //เช็คจาก TRANDO ถ้ามีให้เอามา
        if (_dt.Rows.Count > 0)
        {
            DO_NO = _dt.Rows[0]["PLNT_CODE"] + "";
        }
        else //ถ้าไม่มีให้เอามาจาก TDEIVERYSAP
        {
            string SAP_PLY = "SELECT DELIVERY_NO,SUPPLY_PLANT FROM TDELIVERY_SAP WHERE DELIVERY_NO = '" + DO_NO + "'";
            _dt = CommonFunction.Get_Data(conn, SAP_PLY);
            if (_dt.Rows.Count > 0)
            {
                DO_NO = _dt.Rows[0]["SUPPLY_PLANT"] + "";
            }
            else//ถ้าไม่มีให้เอามาจาก TDEIVERY
            {
                SAP_PLY = "SELECT DELIVERY_NO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + DO_NO + "'";
                _dt = CommonFunction.Get_Data(conn, SAP_PLY);
                if (_dt.Rows.Count > 0)
                {
                    DO_NO = _dt.Rows[0]["SPLNT_CODE"] + "";
                }
                else
                {
                    DO_NO = "";
                }
            }
        }
        return DO_NO;
    }
    void CancelEdit()
    {
        switch (ASPxPageControl1.ActiveTabIndex)
        {
            case 0: gvwT4.CancelEdit(); gvw.CancelEdit(); gvwT3.CancelEdit();
                break;
            case 1: gvwT1.CancelEdit(); gvwT4.CancelEdit(); gvwT3.CancelEdit();
                break;
            case 2: gvwT1.CancelEdit(); gvw.CancelEdit(); gvwT4.CancelEdit();
                break;
            case 3: gvwT1.CancelEdit(); gvw.CancelEdit(); gvwT3.CancelEdit();
                break;

        }
    }

    private string ITEMINAL_DELETE(string Delivery, string TimeWindow, string Vehno, string Tuno, string Teminal, string PlanID, string PlanAdd)
    {
        string Error = "";
        if (string.IsNullOrEmpty(Delivery))
        {
            return "ไม่มี Do นี้";
        }
        else if (string.IsNullOrEmpty(TimeWindow))
        {
            return "ไม่มีเที่ยวการขนส่งนี้";
        }
        else if (string.IsNullOrEmpty(Vehno))
        {
            return "ไม่พบรถในระบบ";
        }
        else if (string.IsNullOrEmpty(Teminal))
        {
            return "ไม่พบคลัง";
        }
        else if (string.IsNullOrEmpty(PlanID))
        {
            return "ไม่พบแผนในระบบ";
        }
        else
        {
            string Result = "";
            #region Webservices iterminal
            DateTime outdate;
            DateTime dateplan = (DateTime.TryParseExact(PlanAdd, "d/M/yyyy HH.mm", new CultureInfo("th-TH"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now);

            iTerminal itm = new iTerminal();

            string USERID = Session["UserName"] + "";
            string CGROUP = Session["CGROUP"] + "";

            Result = itm.DeleteMAP(Delivery, TimeWindow, Vehno, Tuno, Teminal, "", PlanID, USERID, "", CGROUP, dateplan);
            //Result = itm.UpdateMAP(Delivery, TimeWindow, Vehno, Tuno, Teminal, "", PlanID, USERID, "", CGROUP, dateplan);
            #endregion

            //Result = !string.IsNullOrEmpty(Result) ? Result.Substring(0, 1) : "";
            Result = !string.IsNullOrEmpty(Result) ? Result : "";


            return Result;
        }
    }

    private string ITEMINAL_REPLAN(string Delivery, string TimeWindow, string Vehno, string Tuno, string Teminal, string PlanID, string PlanAdd)
    {
        string Error = "";
        if (string.IsNullOrEmpty(Delivery))
        {
            return "ไม่มี Do นี้";
        }
        else if (string.IsNullOrEmpty(TimeWindow))
        {
            return "ไม่มีเที่ยวการขนส่งนี้";
        }
        else if (string.IsNullOrEmpty(Vehno))
        {
            return "ไม่พบรถในระบบ";
        }
        else if (string.IsNullOrEmpty(Teminal))
        {
            return "ไม่พบคลัง";
        }
        else if (string.IsNullOrEmpty(PlanID))
        {
            return "ไม่พบแผนในระบบ";
        }
        else
        {
            string Result = "";
            #region Webservices iterminal
            DateTime outdate;
            DateTime dateplan = (DateTime.TryParseExact(PlanAdd, "d/M/yyyy HH.mm", new CultureInfo("th-TH"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now);

            iTerminal itm = new iTerminal();

            string USERID = Session["UserName"] + "";
            string CGROUP = Session["CGROUP"] + "";


            Result = itm.UpdateMAP(Delivery, TimeWindow, Vehno, Tuno, Teminal, "", PlanID, USERID, "", CGROUP, dateplan);
            #endregion

            Result = !string.IsNullOrEmpty(Result) ? Result : "";


            return Result;
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void xcpnPopup_Load(object sender, EventArgs e)
    {


        if (gvwPop.IsCallback)
        {
            string DO = Session["DO_ID"] + "";
            ListDataPopup(DO);
        }
    }

    protected void xcpnPopup_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');


        switch (paras[0])
        {
            case "ListData":
                int Inx = 0;
                if (!string.IsNullOrEmpty(txtDeliveryNo.Text))
                {
                    Inx = int.Parse(txtDeliveryNo.Text);
                }

                string DO_ID = "";
                dynamic data = "";

                switch (txtSelectgvw.Text)
                {
                    case "0":
                        data = gvwT1.GetRowValues(Inx, "SCONTRACTID");
                        DO_ID = data + "";

                        break;
                    case "1":
                        data = gvw.GetRowValues(Inx, "SDELIVERYNO");
                        DO_ID = data + "";

                        break;
                    case "2":
                        data = gvwT3.GetRowValues(Inx, "SDELIVERYNO");
                        DO_ID = data + "";

                        break;
                    case "3":
                        data = gvwT4.GetRowValues(Inx, "SDELIVERYNO");
                        DO_ID = data + "";

                        break;
                }

                if (txtSelectgvw.Text == "0")
                {
                    tbl_DOSHOW.Style.Add("display", "none");
                    string QUERY = @"SELECT TP.SCONTRACTID,TP.STERMINALID,TT.STERMINALNAME,TP.CMAIN_PLANT  FROM TCONTRACT_PLANT TP
                                    LEFT JOIN TTERMINAL_SAP TT
                                    ON TP.STERMINALID = TT.STERMINALID
                                    WHERE   TP.SCONTRACTID = '" + DO_ID + "' ORDER BY TP.CMAIN_PLANT DESC , TP.STERMINALID ASC";
                    DataTable dt_TERMINAL = CommonFunction.Get_Data(conn, QUERY);

                    string DISPLAY = @"<table width='100%' cellpadding='3' cellspacing='1' border='1'>
                                        <tr>
                                            <td valign='top' align='right' width='15%'>คลังต้นทาง :</td><td>{0}</td>
                                        </tr>
                                         <tr>
                                            <td valign='top' align='right'>คลังต้นทางอื่น :</td><td>{1}</td>
                                         </tr>
                                    </table>";
                    string MAINTERMINAL = "";
                    string OTHERTERMINAL = "";
                    for (int i = 0; i < dt_TERMINAL.Rows.Count; i++)
                    {
                        if (dt_TERMINAL.Rows[i]["CMAIN_PLANT"] + "" == "1")
                        {
                            MAINTERMINAL += dt_TERMINAL.Rows[i]["STERMINALNAME"] + "<br>";
                        }
                        else
                        {
                            OTHERTERMINAL += dt_TERMINAL.Rows[i]["STERMINALNAME"] + "<br>";
                        }

                    }

                    ltr_PANT.Text = string.Format(DISPLAY, MAINTERMINAL, OTHERTERMINAL);

                }
                else
                {
                    ltr_PANT.Text = "";
                    tbl_DOSHOW.Style.Add("display", "");
                    lblDoNo.Text = "เลขที่ DO : " + DO_ID;
                    Session["DO_ID"] = DO_ID;
                    ListDataPopup(DO_ID);
                }
                break;
        }
    }

    void gvwPop_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ลำดับ")
        {
            e.DisplayText = (e.VisibleRowIndex + 1) + ".";
        }
    }

    private void ListDataPopup(string DO_ID)
    {


        try
        {
            //            string QUERY = @"SELECT TDO.DOC_NO,TDO.DO_NO,TDO.DEV_DATE,TROD.SHIP_ID,TDO.ITEM_NO
            //                        ,TDO.MAT_CODE,TDO.QTY_TRAN,TDO.UOM_TRAN,PLNT_CODE SUPPLY_PLANT 
            //                        ,CASE WHEN TDO.DO_NO LIKE '008%' THEN REV_CODE ELSE SHIP_ID  END PLANT
            //                        , NVL(TDO.STATUS,'N') STATUS 
            //                        ,SYSDATE DATE_CREATED ,'SYS_SP' USER_CREATED 
            //                        ,TDO.DBSYS_DATE DUPDATE ,MS_PRD.MATERIAL_NAME
            //                         FROM (
            //                                 SELECT TRN_DO.* 
            //                                FROM TRANS_DO  TRN_DO 
            //                                INNER JOIN (
            //                                     SELECT DO_NO,DBSYS_DATE ,CASE WHEN MAX(NSTATUS)=1 THEN 'G' ELSE 'N' END STATUS
            //                                     FROM(
            //                                        SELECT DO_NO ,MAX(NSTATUS) NSTATUS,MAX(DBSYS_DATE) DBSYS_DATE 
            //                                        FROM(
            //                                             SELECT DO_NO,(CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END) NSTATUS ,MAX(DBSYS_DATE) DBSYS_DATE 
            //                                            FROM TRANS_DO 
            //                                            WHERE NVL(STATUS,'N') IN ('G','N')  
            //                                            GROUP BY DO_NO,CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END 
            //                                       )  GROUP BY DO_NO 
            //                                    )  
            //                                    WHERE 1=1 
            //                                    GROUP BY DO_NO,DBSYS_DATE
            //                                ) MAXTRN_DO ON TRN_DO.DO_NO=MAXTRN_DO.DO_NO AND NVL(TRN_DO.STATUS,'N')=MAXTRN_DO.STATUS AND TRN_DO.DBSYS_DATE=MAXTRN_DO.DBSYS_DATE
            //                         ) TDO
            //                        LEFT JOIN (
            //                            SELECT ORD.* 
            //                            FROM TRANS_ORDER  ORD 
            //                            INNER JOIN (
            //                                 SELECT DOC_NO,ITEM_NO,MAX(DBSYS_DATE) DBSYS_DATE ,CASE WHEN MAX(NSTATUS)=1 THEN 'G' ELSE 'N' END STATUS
            //                                 FROM(
            //                                     SELECT DOC_NO,ITEM_NO
            //                                    ,(CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END) NSTATUS
            //                                    ,MAX(DBSYS_DATE) DBSYS_DATE
            //                                    FROm TRANS_ORDER TROD 
            //                                    where 1=1
            //                                    AND  NVL(TROD .STATUS,'N') IN('N','G')
            //                                    GROUP BY DOC_NO,ITEM_NO,CASE WHEN NVL(STATUS,'N') ='G' THEN 1 ELSE 0 END 
            //                                )
            //                                GROUP BY DOC_NO,ITEM_NO --,DBSYS_DATE
            //                            ) MAXTROD ON ORD.DOC_NO=MAXTROD.DOC_NO  AND  ORD.ITEM_NO=MAXTROD.ITEM_NO  AND NVL(ORD.STATUS,'N')=MAXTROD.STATUS AND ORD.DBSYS_DATE=MAXTROD.DBSYS_DATE
            //                        ) TROD ON TDO.DOC_NO=TROD.DOC_NO AND  TDO.ITEM_NO=TROD.ITEM_NO 
            //                        INNER JOIN (SELECT DO_NO,NVL(STATUS,'N') STATUS,MAX(DBSYS_DATE) MAXDBSYSDATE FROM TRANS_DO WHERE NVL(STATUS,'N') IN ('G','N')  GROUP BY DO_NO,STATUS) TDOMAX
            //                        ON TDO.DO_NO=TDOMAX.DO_NO AND NVL(TDO.STATUS,'N')=TDOMAX.STATUS AND TDO.DBSYS_DATE=TDOMAX.MAXDBSYSDATE
            //                        LEFT JOIN  SAP_MATERIAL_MASTER_DATA MS_PRD ON TDO.MAT_CODE=MS_PRD.MATERIAL_NUMBER
            //                        WHERE 1=1 AND NVL(TDO.STATUS,'N') IN('N','G') 
            //                        AND TDO.DO_NO='" + CommonFunction.ReplaceInjection(DO_ID) + @"'
            //                        GROUP BY TDO.DOC_NO,TDO.DO_NO,TDO.DEV_DATE,TROD.SHIP_ID,TDO.ITEM_NO,TDO.MAT_CODE,TDO.QTY_TRAN,TDO.UOM_TRAN,PLNT_CODE 
            //                        ,CASE WHEN TDO.DO_NO LIKE '008%' THEN REV_CODE ELSE SHIP_ID  END , NVL(TDO.STATUS,'N')   
            //                        ,TDO.DBSYS_DATE,MS_PRD.MATERIAL_NAME ORDER BY TDO.ITEM_NO ASC";

            string QUERY = @"SELECT TGRPDATE.DOC_NO  ,TGRPDATE.DO_NO ,TVOL.DEV_DATE ,(CASE WHEN TGRPDATE.DO_NO LIKE '008%' THEN (CASE WHEN LENGTH(NVL(TORD.REV_CODE,iORD.REV_CODE))=4  THEN LPAD('9'||NVL(TORD.REV_CODE,iORD.REV_CODE),10,'0') ELSE NVL(TORD.REV_CODE,iORD.REV_CODE) END) ELSE  TORD.SHIP_ID END) SHIP_TO 
 ,ROUND(NVL(TVOL.QTY_TRAN,0),-1) QTY_TRAN
 ,TVOL.MAT_CODE 
 ,TGRPDATE.DBSYS_DATE ,'SCHD_GET3' USER_CREATED, SYSDATE DATE_UPDATED,'SCHD_GET3' USER_UPDATED ,TVOL.PLNT_CODE 
 ,MS_PRD.MATERIAL_NAME
 FROM (
     SELECT TDO.DOC_NO  ,TDO.DO_NO, MIN(NVL(TDO.STATUS,'N')) STATUS ,MAX(TDO.DBSYS_DATE) DBSYS_DATE  
     FROM TRANS_DO TDO
     WHERE 1=1  AND TDO.DO_NO='" + CommonFunction.ReplaceInjection(DO_ID) + @"'
    AND NVL(TDO.STATUS,'N') IN('N'  ,'G')   AND NVL(TDO.VALTYP,'-') !='REBRAND' 
    GROUP BY TDO.DOC_NO ,TDO.DO_NO
) TGRPDATE      
LEFT JOIN TRANS_DO TVOL ON TGRPDATE.DOC_NO=TVOL.DOC_NO  AND TGRPDATE.DO_NO=TVOL.DO_NO AND TGRPDATE.DBSYS_DATE=TVOL.DBSYS_DATE AND TGRPDATE.STATUS=NVL(TVOL.STATUS,'N')
LEFT JOIN  (SELECT DISTINCT DOC_NO,REV_CODE,SHIP_ID FROM TRANS_ORDER )  TORD ON TGRPDATE.DOC_NO=TORD.DOC_NO
LEFT JOIN (SELECT DISTINCT DOCNO DOC_NO,RECIEVEPOINT REV_CODE FROM TBORDER  WHERE RECIEVEPOINT IS NOT NULL ) IORD ON  TGRPDATE.DOC_NO=IORD.DOC_NO
LEFT JOIN  SAPECP1000087.SAP_MATERIAL_MASTER_DATA@MASTER MS_PRD ON TVOL.MAT_CODE = MS_PRD.MATERIAL_NUMBER AND TVOL.PLNT_CODE = MS_PRD.PLANT
WHERE 1=1     
GROUP BY TGRPDATE.DOC_NO  ,TGRPDATE.DO_NO,TVOL.DEV_DATE ,(CASE WHEN TGRPDATE.DO_NO LIKE '008%' THEN (CASE WHEN LENGTH(NVL(TORD.REV_CODE,iORD.REV_CODE))=4  THEN LPAD('9'||NVL(TORD.REV_CODE,iORD.REV_CODE),10,'0') ELSE NVL(TORD.REV_CODE,iORD.REV_CODE) END) ELSE  TORD.SHIP_ID END),TGRPDATE.DBSYS_DATE
,TVOL.PLNT_CODE,TVOL.MAT_CODE ,MS_PRD.MATERIAL_NAME ,ROUND(NVL(TVOL.QTY_TRAN,0),-1)";

            DataTable dt = CommonFunction.Get_Data(conn, QUERY);
            if (dt.Rows.Count > 0)
            {
                LogUser(MENUID, "R", "เปิดดูข้อมูลรายละเอียดผลิตภัณฑ์", DO_ID);
                gvwPop.DataSource = dt;
                gvwPop.DataBind();
            }
        }
        catch (Exception e)
        {
            DataTable dt = new DataTable();
            dt.Rows.Clear();
            dt.Columns.Add("SNO", typeof(int));
            dt.Columns.Add("MATERIAL_NAME", typeof(string));
            dt.Columns.Add("QTY_TRAN", typeof(int));
            dt.Columns.Add("DEV_DATE", typeof(DateTime));

            //dt.Rows.Add(1, "ERROR", 16000, DateTime.Now);

            gvwPop.DataSource = dt;
            gvwPop.DataBind();
            //CommonFunction.SetPopupOnLoad(gvwPop, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + e + "')");
        }

    }

    #region class
    [Serializable]
    class TData
    {
        public string ROW { get; set; }
        public string SVENDOR { get; set; }
        public string SCONTRAC { get; set; }
        public string DO { get; set; }
    }

    #endregion


    protected void BindTruckWithDataInGridEditing(ASPxGridView _gvw, params string[] sArrayParams)
    {
        try
        {
            int visibleindex = (xgvw.IsEditing) ? xgvw.EditingRowVisibleIndex : 0;
            dynamic data = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE");

            //DOC_Query = string.Format(@"SELECT DOC_ID,DOC_ID IMP_ID,SCONTRACTID,DDATE,CACTIVE,DCREATE,DUPDATE,DDELETE,DOC_TYPE,DOC_SYSNAME SystemFileName,DOC_ORGNAME OriginalFileName,DOC_CODE,SREMARK FROM TTRUCKCONFIRM_DOC WHERE 1=1 AND NVL(CACTIVE,'1')='1' AND SCONTRACTID='{0}' AND DDATE=TO_DATE('{1}','DD/MM/YYYY')"
            //, "" + data[0], Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")));

            #region TRUCK DATA

            // , "" + data[0], "" + data[1], Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")), ddlWarehouse.SelectedIndex <= 0 ? (object)DBNull.Value : ddlWarehouse.Value);
            DataTable _dtTruck = ContractConfirmBLL.Instance.GetTruckBySContractid("" + data[0], ddlWarehouse.Value + string.Empty, "%" + txtSheadregisterno.Text.Trim() + "%", Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")), (cbCSTANBY.Checked ? "N" : "Y"));
            DataView _dvTruck = new DataView(_dtTruck);
            //_dvTruck.Sort = "SHEADREGISTERNO ASC";
            _gvw.DataSource = _dvTruck;
            _gvw.DataBind();
            #endregion
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }

    protected void imbedit_Click(object sender, EventArgs e)
    {
        ASPxButton btn = (ASPxButton)sender;
        int visibleindex = (xgvw.IsEditing) ? xgvw.EditingRowVisibleIndex : 0;
        dynamic data = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE");
        string CommandArgument = btn.CommandArgument;
        string[] CommandArguments = CommandArgument.Split('|');
        string I_CONTRACT_ID = CommandArguments[0];
        string _Query = string.Empty;
        if (!string.IsNullOrEmpty(I_CONTRACT_ID))
        {
            #region TRUCK DATA
            DataTable dt = ContractConfirmBLL.Instance.GetTruckBySContractidImport("" + data[0], ddlWarehouse.Value + string.Empty, "%" + txtSheadregisterno.Text.Trim() + "%", Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")));

            //_dvTruck.Sort = "SHEADREGISTERNO ASC";

            #endregion
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/ConfirmTruckGPSFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["ConfirmTruck"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = CommandArguments[1].Replace("/", "_") + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = GetData();
        dt.Columns.Remove("ORDERID");
        dt.Columns.Remove("SVENDORID");
        dt.Columns.Remove("SCONTRACTID");
        dt.Columns.Remove("TRAN_TYPE");
        dt.Columns.Remove("NWINDOWTIMEID");
        dt.Columns.Remove("DATE_CREATE");
        dt.Columns.Remove("ORDERTYPE");
        dt.Columns.Remove("SCREATE");
        dt.Columns.Remove("DATE_UPDATE");
        dt.Columns.Remove("SUPDATE");
        dt.Columns.Remove("STERMINALID");
        dt.Columns.Remove("CDAYTYPE");
        dt.Columns.Remove("ROUND");
        dt.Columns.Remove("STRUCKID");
        dt.Columns.Remove("DPLAN");
        dt.Columns.Remove("TRANTRUCK");
        dt.Columns.Remove("SHIPMENT");
        dt.Columns.Remove("NLINE");
        dt.Columns.Remove("TSTART");
        dt.Columns.Remove("TEND");

        dt.Columns["DDELIVERY"].ColumnName = "วันที่รับงาน";
        dt.Columns["SWINDOWTIMENAME"].ColumnName = "เที่ยวที่ขนส่ง";
        dt.Columns["GROUPNAME"].ColumnName = "กลุ่มงานที่";
        dt.Columns["SABBREVIATION"].ColumnName = "ผู้ขนส่ง";
        dt.Columns["SCONTRACTNO"].ColumnName = "เลขที่สัญญา";
        dt.Columns["SDELIVERYNO"].ColumnName = "Delivery No.";
        dt.Columns["STERMINALNAME_FROM"].ColumnName = "คลังต้นทาง";
        dt.Columns["STERMINALNAME"].ColumnName = "ปลายทาง";
        dt.Columns["NVALUE"].ColumnName = "ปริมาณ";
        //DataRow drExport = dt.Rows[1];

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
        ExcelFile workbook = new ExcelFile();
        workbook.Worksheets.Add("Export");
        ExcelWorksheet worksheet = workbook.Worksheets["Export"];
        worksheet.InsertDataTable(dt, new InsertDataTableOptions(1, 0) { ColumnHeaders = false });
        for (int j = 0; j < dt.Columns.Count; j++)
        {//Export Detail
            this.SetFormatCell(worksheet.Cells[0, j], dt.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
            worksheet.Columns[j].AutoFit();
        }
        string Path = this.CheckPath();
        string FileName = "OrderPlan_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";

        workbook.Save(Path + "\\" + FileName);
        this.DownloadFile(Path, FileName);
    }
    protected void btnExport2_Click(object sender, EventArgs e)
    {
        DataTable dt = GetData2();
        dt.Columns.Remove("ORDERID");
        dt.Columns.Remove("SVENDORID");
        dt.Columns.Remove("SCONTRACTID");
        dt.Columns.Remove("TRAN_TYPE");
        dt.Columns.Remove("NWINDOWTIMEID");
        dt.Columns.Remove("DATE_CREATE");
        dt.Columns.Remove("ORDERTYPE");
        dt.Columns.Remove("SCREATE");
        dt.Columns.Remove("DATE_UPDATE");
        dt.Columns.Remove("SUPDATE");
        dt.Columns.Remove("STERMINALID");
        dt.Columns.Remove("CDAYTYPE");
        dt.Columns.Remove("STRUCKID");
        dt.Columns.Remove("DPLAN");
        dt.Columns.Remove("SHIPMENT");
        dt.Columns.Remove("NLINE");
        dt.Columns.Remove("TSTART");
        dt.Columns.Remove("TEND");

        dt.Columns["DDELIVERY"].ColumnName = "วันที่รับงาน";
        dt.Columns["SWINDOWTIMENAME"].ColumnName = "เที่ยวที่ขนส่ง";
        dt.Columns["GROUPNAME"].ColumnName = "กลุ่มงานที่";
        dt.Columns["SABBREVIATION"].ColumnName = "ผู้ขนส่ง";
        dt.Columns["SCONTRACTNO"].ColumnName = "เลขที่สัญญา";
        dt.Columns["SDELIVERYNO"].ColumnName = "Delivery No.";
        dt.Columns["STERMINALNAME_FROM"].ColumnName = "คลังต้นทาง";
        dt.Columns["STERMINALNAME"].ColumnName = "ปลายทาง";
        dt.Columns["NVALUE"].ColumnName = "ปริมาณ";
        dt.Columns["TRANTRUCK"].ColumnName = "ทะเบียนรถ";
        dt.Columns["ROUND"].ColumnName = "Drop No.";
        //DataRow drExport = dt.Rows[1];

        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
        ExcelFile workbook = new ExcelFile();
        workbook.Worksheets.Add("Export");
        ExcelWorksheet worksheet = workbook.Worksheets["Export"];
        worksheet.InsertDataTable(dt, new InsertDataTableOptions(1, 0) { ColumnHeaders = false });
        for (int j = 0; j < dt.Columns.Count; j++)
        {//Export Detail
            this.SetFormatCell(worksheet.Cells[0, j], dt.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
            worksheet.Columns[j].AutoFit();
        }
        string Path = this.CheckPath();
        string FileName = "OrderPlan_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";

        workbook.Save(Path + "\\" + FileName);
        this.DownloadFile(Path, FileName);
    }

    private void CheckBoxHeader()
    {
        CheckBox cbSelectHeader = gvw.FindHeaderTemplateControl(gvw.Columns["CHECKLATE"], "cbSelectHeader") as CheckBox;
        if (cbSelectHeader != null)
        {
            cbSelectHeader.InputAttributes.Add("onchange", "ddlOnChange($(this).is(':checked'),-1,\"IS_SELECTHEADER\",this);");
        }
    }
}