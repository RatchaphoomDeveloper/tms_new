﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

public partial class TruckManagemant : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["UserID"] = "40"; Session["CGROUP"] = "1";
        gvw.Settings.ShowFilterRowMenu = true;
    } 
     
    protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        int index = -1;
        if (int.TryParse(e.Parameters, out index))
            gvw.SettingsEditing.Mode = (GridViewEditingMode)index;
    }
    protected void cboHeadRegistfifo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
            ASPxComboBox comboBox = (ASPxComboBox)source;
            Literal ltrSVENDORID = (Literal)gvw.FindEditFormTemplateControl("ltrSVENDORID");

            sdsVEHICLE.SelectCommand = @"SELECT TTRUCK.STRUCKID    ,TTRUCK.SHEADREGISTERNO  
,TTRUCK.STRANSPORTID ,TTRUCK.CACTIVE ,NVL(TTRUCK.CHOLD,'0') CHOLD
,TRUNC(TTRUCK.DWATEREXPIRE ) DWATEREXPIRE 
,TTRUCKTYPE.SCARTYPENAME 
,CASE WHEN NVL(TTRUCK.CHOLD,'0')='0' THEN
CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 'N/A' ELSE 'SAP' END 
ELSE 'TMS' END MS_HOLD
FROM TTRUCK  
LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
WHERE TTRUCK.CACTIVE='Y' AND TTRUCK.SCARTYPEID IN ('0','3' )   
AND TTRUCK.SHEADREGISTERNO LIKE :fillter  
AND rownum >= :startIndex AND rownum <= :endIndex ";

            sdsVEHICLE.SelectParameters.Clear();
            sdsVEHICLE.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsVEHICLE.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsVEHICLE.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsVEHICLE;
            comboBox.DataBind();
         

    }
    protected void cboHeadRegistfifo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    } 
    protected void cboTrailer_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboTrailer_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsVEHICLE.SelectCommand = @"SELECT TTRUCK.STRUCKID    ,TTRUCK.SHEADREGISTERNO  
,TTRUCK.STRANSPORTID ,TTRUCK.CACTIVE ,NVL(TTRUCK.CHOLD,'0') CHOLD
,TO_DATE(TTRUCK.DWATEREXPIRE,'DD/MM/YYYY' ) DWATEREXPIRE 
,TTRUCKTYPE.SCARTYPENAME 
,CASE WHEN NVL(TTRUCK.CHOLD,'0')='0' THEN
CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 'N/A' ELSE 'SAP' END 
ELSE 'TMS' END MS_HOLD
FROM TTRUCK  
LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
WHERE TTRUCK.CACTIVE='Y' AND TTRUCK.SCARTYPEID IN ('4' ) 
AND TTRUCK.SHEADREGISTERNO LIKE :fillter 
AND rownum >= :startIndex AND rownum <= :endIndex ";

        sdsVEHICLE.SelectParameters.Clear();
        sdsVEHICLE.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVEHICLE.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVEHICLE.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVEHICLE;
        comboBox.DataBind();


    }

}