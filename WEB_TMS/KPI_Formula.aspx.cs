﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class KPI_Formula : PageBase
{
    #region + View State +
    private DataTable dtKPI
    {
        get
        {
            if ((DataTable)ViewState["dtKPI"] != null)
                return (DataTable)ViewState["dtKPI"];
            else
                return null;
        }
        set
        {
            ViewState["dtKPI"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            this.AssignAuthen();
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                
            }
            if (!CanWrite)
            {
                //int getCoumn = 0;
                //getCoumn = findGridColumnByCSS(ref gvwWaiting, "xxSearch")
                dgvKPI.Columns[4].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialForm()
    {
        try
        {
            
            this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SearchData(string Condition)
    {
        try
        {
            dtKPI = KPIBLL.Instance.KPISelectBLL();
            GridViewHelper.BindGridView(ref dgvKPI, dtKPI);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvKPI_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvKPI.DataKeys[e.RowIndex]["FORMULA_ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            MsgAlert.OpenForm("KPI_Formula_AddEdit.aspx?FORMULA_ID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}