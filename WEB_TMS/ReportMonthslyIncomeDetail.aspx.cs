﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;

public partial class ReportMonthslyIncomeDetail : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            gvw.Visible = false;
            SetCboYear();
            //ListData();

        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //ListData();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": //ListData();
                break;
            case "ReportPDF": ListReport("P");
                break;
            case "ReportExcel": ListReport("E");
                break;

        }
    }

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    void ListData()
    {

        string Condition = "";
        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    string Date = "1/" + cboMonth.Value + "/" + cboYear.Text + "";
        //    DateTime DMONTH = DateTime.Parse(Date);
        //    //DateTime DYEAR = DateTime.Parse(cboMonth.Value.ToString());
        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
        //    lblsTail.Text = DMONTH.ToString("MMM", new CultureInfo("th-TH")) + " " + cboYear.Text;
        //}
        //else
        //{

        //}



        string QUERY = @"SELECT ROWNUM||'.' as NO,REQ.REQUEST_ID
,REQ.VEH_NO
,REQ.TU_NO
,TCK.SCAR_NUM,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
, TO_CHAR(add_months(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),6516),'dd/MM/yyyy') as APPOINTMENT_DATE
,STU.STATUSREQ_NAME
FROM TBL_REQUEST REQ
LEFT JOIN TTRUCK TCK
ON TCK.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN TBL_STATUSREQ STU ON STU.STATUSREQ_ID = REQ.STATUS_FLAG
" + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;

        }
        gvw.DataBind();
    }

    private void ListReport(string Type)
    {
        MonthlyIncomeDetail report = new MonthlyIncomeDetail();
        string Condition = "";
        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            Condition = " AND TO_CHAR(REQ.SERVICE_DATE,'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
            ((XRLabel)report.FindControl("xrLabel7", true)).Text = "1.2 ยอดสะสมถึงเดือน " + cboMonth.Text + " " + cboYear.Text + "";
            ((XRLabel)report.FindControl("xrLabel8", true)).Text = "2.1 ค่าใช้จ่ายในเดือน " + cboMonth.Text + " " + cboYear.Text + "";
            ((XRLabel)report.FindControl("xrLabel9", true)).Text = "2.2 ต้นทุนค่าใช้จ่ายสะสมถึงเดือน " + cboMonth.Text + " " + cboYear.Text + "";
            ((XRLabel)report.FindControl("xrLabel10", true)).Text = "3.1 กำไรในเดือน " + cboMonth.Text + " " + cboYear.Text + "";
            ((XRLabel)report.FindControl("xrLabel11", true)).Text = "3.2 กำไรสะสมถึงเดือน " + cboMonth.Text + " " + cboYear.Text + "";



        }
        else
        {

        }


        //        string QUERY = @"SELECT REQ.REQUEST_ID,VEN.SABBREVIATION,REQ.STATUS_FLAG,REQ.TOTLE_CAP
        //,CASE WHEN NVL(TU_NO,'xxx') = 'xxx' THEN VEH_NO ELSE VEH_NO||' - '||TU_NO   END  as REGIS 
        //,TCK.SCAR_NUM,TCK.SCHASIS,TCK.CARCATE_ID,
        //CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE 'X' END as C1C1,
        //CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE 'X' END as C1C2,
        //CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE 'X' END as C1C3,
        //CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE 'X' END as C1C4,
        //CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE 'X' END as C1C5,
        //CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE 'X' END as C1C6,
        //CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE 'X' END as C2C1,
        //CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE 'X' END as C2C2,
        //CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE 'X' END as C2C3,
        //CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE 'X' END as C2C4,
        //CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE 'X' END as C2C5,
        //CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE 'X' END as C2C6,
        //CASE WHEN REQ.REQTYPE_ID = '02' AND REQ.CAUSE_ID NOT IN ('00011','00008') THEN '/' ELSE 'X' END as C3,
        //CASE WHEN REQ.CAUSE_ID IN ('00011','00008') AND NVL(Tserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C4,
        //CASE WHEN NVL(RIT2.Nservc,0) > 0 THEN '/' ELSE 'X' END as C5,
        //CASE WHEN NVL(Qserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C6C1,
        //CASE WHEN NVL(Aserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C6C2,
        //CASE WHEN NVL(CPserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C6C3,
        //CASE WHEN NVL(Wserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C6C4,
        //CASE WHEN NVL(APserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C6C5,
        //CASE WHEN NVL(CHJserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C6C6,
        //CASE WHEN  REQ.REQTYPE_ID = '04' THEN '/' ELSE 'X' END as C6C7,
        //CASE WHEN  REQ.REQTYPE_ID = '03' THEN '/' ELSE 'X' END as C6C8,
        //CASE WHEN NVL(NSserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C7C1,
        //CASE WHEN NVL(GSserv.Nservc,0) > 0 THEN '/' ELSE 'X' END as C7C2
        //FROM TBL_REQUEST REQ
        //LEFT JOIN TVENDOR VEN ON VEN.SVENDORID = REQ.VENDOR_ID 
        //LEFT JOIN TTRUCK TCK ON TCK.STRUCKID = CASE WHEN NVL(TU_ID,'xxx') = 'xxx' THEN VEH_ID ELSE TU_ID END 
        //--รถทุจริต
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM  
        //    WHERE SERVICE_ID = '00023' GROUP BY  REQUEST_ID,SERVICE_ID
        //)Tserv ON Tserv.REQUEST_ID = REQ.REQUEST_ID  
        //--รถไม่พร้อม
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM2  
        //    WHERE SERVICE_ID = '00017' GROUP BY  REQUEST_ID,SERVICE_ID
        //)RIT2 ON RIT2.REQUEST_ID = REQ.REQUEST_ID  
        //--บริการเร่งด่วน
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM2  
        //    WHERE SERVICE_ID = '00016' GROUP BY  REQUEST_ID,SERVICE_ID
        //)Qserv ON Qserv.REQUEST_ID = REQ.REQUEST_ID  
        //--ค่าติดอุปกรณ์เพิ่มติดรถอากาศยาน และติดแป้นระดับ
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM2  
        //    WHERE SERVICE_ID = '00024' GROUP BY  REQUEST_ID,SERVICE_ID
        //)Aserv ON Aserv.REQUEST_ID = REQ.REQUEST_ID  
        //--ตัดเจียรแป้น
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM2  
        //    WHERE SERVICE_ID = '00012' GROUP BY  REQUEST_ID,SERVICE_ID
        //)CPserv ON CPserv.REQUEST_ID = REQ.REQUEST_ID
        //--เชื่อมตองรั่ว
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM2  
        //    WHERE SERVICE_ID = '00013' GROUP BY  REQUEST_ID,SERVICE_ID
        //)Wserv ON Wserv.REQUEST_ID = REQ.REQUEST_ID
        //--ติดตั้งแผ่นเพลทใหม่
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM2  
        //    WHERE SERVICE_ID = '00014' GROUP BY  REQUEST_ID,SERVICE_ID
        //)APserv ON APserv.REQUEST_ID = REQ.REQUEST_ID
        //--ค่าตัดเจียรร์หูจรวด
        //LEFT JOIN 
        //(
        //    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
        //    FROM TBL_REQUEST_ITEM2  
        //    WHERE SERVICE_ID = '00015' GROUP BY  REQUEST_ID,SERVICE_ID
        //)CHJserv ON CHJserv.REQUEST_ID = REQ.REQUEST_ID
        //-- ไม่เก็บเงินค่าธรรมเนียมจากผู้รับจ้าง
        //LEFT JOIN 
        //(
        //SELECT REQUEST_ID,  FLAGE_SERVICECANCEL,COUNT(FLAGE_SERVICECANCEL) as Nservc FROM
        //(
        //    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
        //    FROM TBL_REQUEST_ITEM
        //    UNION
        //    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
        //    FROM TBL_REQUEST_ITEM2
        //)
        //WHERE FLAGE_SERVICECANCEL = 'Y'
        //GROUP BY  REQUEST_ID,FLAGE_SERVICECANCEL
        //)NSserv ON NSserv.REQUEST_ID = REQ.REQUEST_ID
        //-- เก็บเงินค่าธรรมเนียมจากผู้รับจ้าง
        //LEFT JOIN 
        //(
        //SELECT REQUEST_ID,  FLAGE_SERVICECANCEL,COUNT(FLAGE_SERVICECANCEL) as Nservc FROM
        //(
        //    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
        //    FROM TBL_REQUEST_ITEM
        //    UNION
        //    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
        //    FROM TBL_REQUEST_ITEM2
        //)
        //WHERE FLAGE_SERVICECANCEL = 'N'
        //GROUP BY  REQUEST_ID,FLAGE_SERVICECANCEL
        //)GSserv ON GSserv.REQUEST_ID = REQ.REQUEST_ID
        //" + Condition + "";

        string QUERY = @"SELECT REQ.REQUEST_ID,VEN.SABBREVIATION,REQ.STATUS_FLAG,REQ.TOTLE_CAP
,CASE WHEN NVL(TU_NO,'xxx') = 'xxx' THEN VEH_NO ELSE VEH_NO||' - '||TU_NO   END  as REGIS 
,TCK.SCAR_NUM,TCK.SCHASIS,TCK.CARCATE_ID,
CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE '' END as C1C1,
CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE '' END as C1C2,
CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE '' END as C1C3,
CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE '' END as C1C4,
CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE '' END as C1C5,
CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') = 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')   THEN '/' ELSE '' END as C1C6,
CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE '' END as C2C1,
CASE WHEN TCK.CARCATE_ID = '01' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE '' END as C2C2,
CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE '' END as C2C3,
CASE WHEN TCK.CARCATE_ID = '02' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE '' END as C2C4,
CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP < 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE '' END as C2C5,
CASE WHEN TCK.CARCATE_ID = '03' AND REQ.TOTLE_CAP > 16000 AND NVL(TCK.SCAR_NUM,'xxx') <> 'xxx' AND REQ.REQTYPE_ID = '01' AND REQ.CAUSE_ID NOT IN ('00011','00008')  THEN '/' ELSE '' END as C2C6,
CASE WHEN REQ.REQTYPE_ID = '02' AND REQ.CAUSE_ID NOT IN ('00011','00008') THEN '/' ELSE '' END as C3,
CASE WHEN REQ.CAUSE_ID IN ('00011','00008') AND NVL(Tserv.Nservc,0) > 0 THEN '/' ELSE '' END as C4,
CASE WHEN NVL(RIT2.Nservc,0) > 0 THEN '/' ELSE '' END as C5,
CASE WHEN NVL(Qserv.Nservc,0) > 0 THEN '/' ELSE '' END as C6C1,
CASE WHEN NVL(Aserv.Nservc,0) > 0 THEN '/' ELSE '' END as C6C2,
CASE WHEN NVL(CPserv.Nservc,0) > 0 THEN '/' ELSE '' END as C6C3,
CASE WHEN NVL(Wserv.Nservc,0) > 0 THEN '/' ELSE '' END as C6C4,
CASE WHEN NVL(APserv.Nservc,0) > 0 THEN '/' ELSE '' END as C6C5,
CASE WHEN NVL(CHJserv.Nservc,0) > 0 THEN '/' ELSE '' END as C6C6,
CASE WHEN  REQ.REQTYPE_ID = '04' THEN '/' ELSE '' END as C6C7,
CASE WHEN  REQ.REQTYPE_ID = '03' THEN '/' ELSE '' END as C6C8,
CASE WHEN NVL(NSserv.Nservc,0) > 0 THEN '/' ELSE '' END as C7C1,
CASE WHEN NVL(GSserv.Nservc,0) > 0 THEN '/' ELSE '' END as C7C2
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN ON VEN.SVENDORID = REQ.VENDOR_ID 
LEFT JOIN TTRUCK TCK ON TCK.STRUCKID = CASE WHEN NVL(TU_ID,'xxx') = 'xxx' THEN VEH_ID ELSE TU_ID END 
--รถทุจริต
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM  
    WHERE SERVICE_ID = '00023' GROUP BY  REQUEST_ID,SERVICE_ID
)Tserv ON Tserv.REQUEST_ID = REQ.REQUEST_ID  
--รถไม่พร้อม
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM2  
    WHERE SERVICE_ID = '00017' GROUP BY  REQUEST_ID,SERVICE_ID
)RIT2 ON RIT2.REQUEST_ID = REQ.REQUEST_ID  
--บริการเร่งด่วน
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM2  
    WHERE SERVICE_ID = '00016' GROUP BY  REQUEST_ID,SERVICE_ID
)Qserv ON Qserv.REQUEST_ID = REQ.REQUEST_ID  
--ค่าติดอุปกรณ์เพิ่มติดรถอากาศยาน และติดแป้นระดับ
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM2  
    WHERE SERVICE_ID = '00024' GROUP BY  REQUEST_ID,SERVICE_ID
)Aserv ON Aserv.REQUEST_ID = REQ.REQUEST_ID  
--ตัดเจียรแป้น
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM2  
    WHERE SERVICE_ID = '00012' GROUP BY  REQUEST_ID,SERVICE_ID
)CPserv ON CPserv.REQUEST_ID = REQ.REQUEST_ID
--เชื่อมตองรั่ว
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM2  
    WHERE SERVICE_ID = '00013' GROUP BY  REQUEST_ID,SERVICE_ID
)Wserv ON Wserv.REQUEST_ID = REQ.REQUEST_ID
--ติดตั้งแผ่นเพลทใหม่
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM2  
    WHERE SERVICE_ID = '00014' GROUP BY  REQUEST_ID,SERVICE_ID
)APserv ON APserv.REQUEST_ID = REQ.REQUEST_ID
--ค่าตัดเจียรร์หูจรวด
LEFT JOIN 
(
    SELECT REQUEST_ID,SERVICE_ID,COUNT(SERVICE_ID)  as Nservc
    FROM TBL_REQUEST_ITEM2  
    WHERE SERVICE_ID = '00015' GROUP BY  REQUEST_ID,SERVICE_ID
)CHJserv ON CHJserv.REQUEST_ID = REQ.REQUEST_ID
-- ไม่เก็บเงินค่าธรรมเนียมจากผู้รับจ้าง
LEFT JOIN 
(
SELECT REQUEST_ID,  FLAGE_SERVICECANCEL,COUNT(FLAGE_SERVICECANCEL) as Nservc FROM
(
    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
    FROM TBL_REQUEST_ITEM
    UNION
    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
    FROM TBL_REQUEST_ITEM2
)
WHERE FLAGE_SERVICECANCEL = 'Y'
GROUP BY  REQUEST_ID,FLAGE_SERVICECANCEL
)NSserv ON NSserv.REQUEST_ID = REQ.REQUEST_ID
-- เก็บเงินค่าธรรมเนียมจากผู้รับจ้าง
LEFT JOIN 
(
SELECT REQUEST_ID,  FLAGE_SERVICECANCEL,COUNT(FLAGE_SERVICECANCEL) as Nservc FROM
(
    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
    FROM TBL_REQUEST_ITEM
    UNION
    SELECT  REQUEST_ID,  FLAGE_SERVICECANCEL
    FROM TBL_REQUEST_ITEM2
)
WHERE FLAGE_SERVICECANCEL = 'N'
GROUP BY  REQUEST_ID,FLAGE_SERVICECANCEL
)GSserv ON GSserv.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 {0}";

        DataTable dt = CommonFunction.Get_Data(conn, string.Format(QUERY, Condition));
        //DataRow dr = null;
        //for (int i = dt.Rows.Count; i <= 10; i++)
        //{
        //    dr = dt.NewRow();
        //    dr[0] = ""; // or you could generate some random string.
        //    dt.Rows.Add(dr);
        //}
        //เงินที่ได้รับ
        string QUERYGIVE = @"
        SELECT COUNT(REQ.REQUEST_ID) as NCAR ,SUM(RQI.NPRICE) as NPRICE
        FROM TBL_REQUEST REQ
        LEFT JOIN
        (
            SELECT REQUEST_ID ,  SUM(NPRICE) as NPRICE
            FROM TBL_REQUEST_ITEM
            WHERE NVL(FLAGE_SERVICECANCEL,'xxx') <> 'Y' 
            GROUP BY REQUEST_ID
        )RQI
        ON  REQ.REQUEST_ID = RQI.REQUEST_ID
        WHERE 1=1   {0}";
        DataTable dtGiveMoney = CommonFunction.Get_Data(conn, string.Format(QUERYGIVE, Condition));

        if (dtGiveMoney.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel13", true)).Text = decimal.Parse(dtGiveMoney.Rows[0]["NCAR"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
            ((XRLabel)report.FindControl("xrLabel15", true)).Text = decimal.Parse(dtGiveMoney.Rows[0]["NPRICE"] + "").ToString(SystemFunction.CheckFormatNuberic(2));
        }
        DataTable dtIncrementGiveMoney = CommonFunction.Get_Data(conn, string.Format(QUERYGIVE, ""));

        if (dtIncrementGiveMoney.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel25", true)).Text = decimal.Parse(dtIncrementGiveMoney.Rows[0]["NCAR"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
            ((XRLabel)report.FindControl("xrLabel23", true)).Text = decimal.Parse(dtIncrementGiveMoney.Rows[0]["NPRICE"] + "").ToString(SystemFunction.CheckFormatNuberic(2));
        }

        //ต้นทุน
        string QUERYCOST = @"
        SELECT COUNT(REQ.REQUEST_ID) as NCAR,SUM(
CASE 
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 16000 AND LCS.LEVEL_NO = 1 THEN 4418.28
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 20000 AND LCS.LEVEL_NO = 1 THEN 4424.28
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 32000 AND LCS.LEVEL_NO = 1 THEN 4442.28
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 40000 AND LCS.LEVEL_NO = 1 THEN 4454.28
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 16000 AND LCS.LEVEL_NO = 2 THEN 5793.28
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 20000 AND LCS.LEVEL_NO = 2 THEN 5799.28
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 32000 AND LCS.LEVEL_NO = 2 THEN 5817.28
WHEN REQ.TOTLE_SLOT < 6 AND REQ.TOTLE_CAP <= 40000 AND LCS.LEVEL_NO = 2 THEN 5829.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 16000 AND LCS.LEVEL_NO = 1 THEN 7038.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 20000 AND LCS.LEVEL_NO = 1 THEN 7044.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 32000 AND LCS.LEVEL_NO = 1 THEN 7062.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 40000 AND LCS.LEVEL_NO = 1 THEN 7074.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 16000 AND LCS.LEVEL_NO = 2 THEN 9788.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 20000 AND LCS.LEVEL_NO = 2 THEN 9794.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 32000 AND LCS.LEVEL_NO = 2 THEN 9812.28
WHEN REQ.TOTLE_SLOT > 5 AND REQ.TOTLE_CAP <= 40000 AND LCS.LEVEL_NO = 2 THEN 9824.28
ELSE 

CASE 
WHEN LCS.LEVEL_NO = 1 AND REQ.TOTLE_SLOT < 6 THEN 4454.28
WHEN LCS.LEVEL_NO = 1 AND REQ.TOTLE_SLOT > 5 THEN 7074.28
WHEN LCS.LEVEL_NO = 2 AND REQ.TOTLE_SLOT < 6 THEN 5829.28
WHEN LCS.LEVEL_NO = 2 AND REQ.TOTLE_SLOT > 5 THEN 9824.28
ELSE 0 END 

END 
)
as NPRICE
FROM TBL_REQUEST REQ
LEFT JOIN
(
    SELECT REQUEST_ID,MAX(SLOT_NO) as SLOT_NO,MAX(LEVEL_NO) as LEVEL_NO,SUM(CAPACITY) as CAPACITY FROM
    (
        SELECT REQUEST_ID, SLOT_NO, MAX(LEVEL_NO) as LEVEL_NO,  MAX(CAPACITY) as CAPACITY
        FROM TBL_REQSLOT
        WHERE NVL(STATUS_PAN3,'xxx') <> 'Y'
        GROUP BY REQUEST_ID,SLOT_NO
    )
    GROUP BY REQUEST_ID
)LCS ON LCS.REQUEST_ID = REQ.REQUEST_ID 
WHERE 1=1 {0}";
        DataTable dtCostMoney = CommonFunction.Get_Data(conn, string.Format(QUERYCOST, Condition));
        if (dtCostMoney.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel18", true)).Text = decimal.Parse(dtCostMoney.Rows[0]["NCAR"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
            ((XRLabel)report.FindControl("xrLabel20", true)).Text = decimal.Parse(dtCostMoney.Rows[0]["NPRICE"] + "").ToString(SystemFunction.CheckFormatNuberic(2));
        }
        DataTable dtIncrementCostMoney = CommonFunction.Get_Data(conn, string.Format(QUERYCOST, ""));
        if (dtIncrementCostMoney.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel27", true)).Text = decimal.Parse(dtIncrementCostMoney.Rows[0]["NPRICE"] + "").ToString(SystemFunction.CheckFormatNuberic(2));
        }



        if (!string.IsNullOrEmpty(dtGiveMoney.Rows[0]["NPRICE"] + ""))
        {
            if (!string.IsNullOrEmpty(dtCostMoney.Rows[0]["NPRICE"] + ""))
            {
                ((XRLabel)report.FindControl("xrLabel29", true)).Text = (decimal.Parse(dtGiveMoney.Rows[0]["NPRICE"] + "") - decimal.Parse(dtCostMoney.Rows[0]["NPRICE"] + "")).ToString(SystemFunction.CheckFormatNuberic(2));
            }
            else
            {
                ((XRLabel)report.FindControl("xrLabel29", true)).Text = decimal.Parse(dtGiveMoney.Rows[0]["NPRICE"] + "").ToString(SystemFunction.CheckFormatNuberic(2));
            }

        }
        else
        {
            if (!string.IsNullOrEmpty(dtCostMoney.Rows[0]["NPRICE"] + ""))
            {
                ((XRLabel)report.FindControl("xrLabel29", true)).Text = (0 - decimal.Parse(dtCostMoney.Rows[0]["NPRICE"] + "")).ToString(SystemFunction.CheckFormatNuberic(2));
            }
            else
            {
                ((XRLabel)report.FindControl("xrLabel29", true)).Text = "";
            }

        }

        if (!string.IsNullOrEmpty(dtIncrementGiveMoney.Rows[0]["NPRICE"] + ""))
        {
            if (!string.IsNullOrEmpty(dtIncrementCostMoney.Rows[0]["NPRICE"] + ""))
            {
                ((XRLabel)report.FindControl("xrLabel32", true)).Text = (decimal.Parse(dtIncrementGiveMoney.Rows[0]["NPRICE"] + "") - decimal.Parse(dtIncrementCostMoney.Rows[0]["NPRICE"] + "")).ToString(SystemFunction.CheckFormatNuberic(2));
            }
            else
            {
                ((XRLabel)report.FindControl("xrLabel32", true)).Text = decimal.Parse(dtIncrementGiveMoney.Rows[0]["NPRICE"] + "").ToString(SystemFunction.CheckFormatNuberic(2));
            }

        }
        else
        {
            if (!string.IsNullOrEmpty(dtIncrementCostMoney.Rows[0]["NPRICE"] + ""))
            {
                ((XRLabel)report.FindControl("xrLabel32", true)).Text = (0 - decimal.Parse(dtIncrementCostMoney.Rows[0]["NPRICE"] + "")).ToString(SystemFunction.CheckFormatNuberic(2));
            }
            else
            {
                ((XRLabel)report.FindControl("xrLabel32", true)).Text = "";
            }

        }




        #region function report

        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    string Date = "1/" + cboMonth.Value + "/" + cboYear.Text + "";
        //    DateTime DMONTH = DateTime.Parse(Date);
        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = DMONTH.ToString("MMMM", new CultureInfo("th-TH")) + " " + cboYear.Text;
        //}
        //else
        //{
        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = " - ";
        //}

        report.Name = "รายได้แจกแจงรายคัน";
        report.DataSource = dt;
        string fileName = "รายได้แจกแจงรายคัน_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();


        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ListReport("P");
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ListReport("E");
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion
}