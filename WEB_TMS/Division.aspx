﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Division.aspx.cs" Inherits="Division" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">เงื่อนไขการค้นหา&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract">
                    <div>
                        <br />
                        <asp:Table runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <%--<asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>--%>
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblDivision" runat="server" Text="Division :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlDivision" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <br />
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSearch" runat="server" Text="&nbsp;ค้นหา" OnClick="cmdSearch_Click" CssClass="btn btn-success" Width="100px" />
                            <asp:Button ID="cmdClear" runat="server" Text="&nbsp;เคลียร์" OnClick="cmdClear_Click" CssClass="btn btn-success" Width="100px" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                </div>
                <div class="panel-body" align="center">
                    <br />
                    <div class="">
                        <div class="panel-collapse collapse in" id="collapseFindContract2">
                            <div class="table-responsive">
                                <asp:GridView ID="dgvData" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" AutoGenerateColumns="false"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                    OnPageIndexChanging="dgvData_PageIndexChanging" OnRowCommand="dgvData_RowCommand" DataKeyNames="DIVISION_ID">
                                    <Columns>
                                        <asp:BoundField DataField="DIVISION_ID" HeaderText="" Visible="false">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DIVISION_NAME" HeaderText="DIVISION_NAME">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DOC_PREFIX" HeaderText="DIVISION_PREFIX">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DEPARTMENT_NAME" HeaderText="DEPARTMENT_NAME">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CREATE_DATETIME" HeaderText="CREATE DATE">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <%-- <asp:TemplateField HeaderText="สถานะ">
                                                        <HeaderStyle Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" Value='' ID="CACTIVE" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="สถานะ">
                                            <HeaderStyle Width="10%" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" Checked='<%# Convert.ToString(Eval("CACTIVE")) == "1" ? true : false %>' Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aView" runat="server" ImageUrl="~/Images/blue-37.png" CommandName="view" Width="24px" Height="24px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aEdit" runat="server" ImageUrl="~/Images/blue-23.png" CommandName="editData" Width="26px" Height="26px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="panel-footer" style="text-align: right">
                            <%--<a href="#" id="cmdAdd" runat="server" class="btn btn-md btn-hover btn-success" style="width: 100px" onserverclick="cmdAdd_ServerClick"><i class="fa fa-edit"></i>&nbsp;เพิ่มข้อมูล</a>--%>
                            <asp:Button ID="cmdAdd" runat="server" Text="&nbsp;เพิ่มข้อมูล" OnClick="cmdAdd_ServerClick" CssClass="btn btn-success" Width="100px" />
                            <%--<a href="#" id="cmdRefresh" runat="server" class="btn btn-md btn-hover btn-success" style="width: 100px" onserverclick="cmdRefresh_ServerClick"><i class="fa fa-refresh"></i>&nbsp;รีเฟรช</a>--%>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
