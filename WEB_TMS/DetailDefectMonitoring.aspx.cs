﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;
using System.IO;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using System.Web.UI.HtmlControls;
using System.Configuration;
using DevExpress.XtraPrinting;
using System.Net.Mime;

public partial class DetailDefectMonitoring : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler


        #endregion
        if (!IsPostBack)
        {

            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            cboType.DataBind();
            cboType.Items.Insert(0, new ListEditItem(" - ทั้งหมด - ", ""));
            cboType.SelectedIndex = 0;

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

        }

    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        BindData();
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {


    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "Search":
                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                BindData();
                break;

            case "hlProcessNameClick":
                int index111 = int.Parse(paras[1]);
                dynamic data = gvw.GetRowValues(index111, "SREDUCENAME", "SPROCESSID", "SPROCESSNAME", "SREFERENCEID");

                string sProcess = data[1] + "";

                string sDetail = data[0] + "";

                string query = "";

                switch (sProcess)
                {

                    case "080":


                        query = string.Format(@"SELECT T.STOPICNAME,THEADCOMPLAIN.SDETAIL SHEADCOMLIAINNAME

 FROM (TCOMPLAIN c 
 LEFT JOIN (SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'C_1_1' ) THEADCOMPLAIN ON NVL(c.SHEADCOMLIAIN,-1)=THEADCOMPLAIN.NID
 LEFT JOIN TTOPIC t ON c.STOPICID = T.STOPICID AND C.SVERSION = T.SVERSION )

 WHERE SSERVICEID = '{0}'", data[3] + "");

                        DataTable dt = CommonFunction.Get_Data(sql, query);

                        if (dt.Rows.Count > 0)
                        {
                            sDetail = string.Format("เรื่องที่ร้องเรียน : {0} <br/> หัวข้อการหักคะแนน : {1}", dt.Rows[0]["SHEADCOMLIAINNAME"] + "", dt.Rows[0]["STOPICNAME"] + "");
                        }

                        break;

                    case "090":

                        query = string.Format(@"SELECT 
(SELECT MAX(SDEFINE) FROM TCATEGORY WHERE (SCATEGORYTYPEID = '10') AND (CACTIVE = '1')  AND NIMPACTLEVEL = a.NORGANIZATIONEFFECT ) as dt1,
(SELECT MAX(SDEFINE) FROM TCATEGORY WHERE (SCATEGORYTYPEID = '07') AND (CACTIVE = '1')  AND NIMPACTLEVEL = a.NDRIVEREFFECT) as dt2,
(SELECT MAX(SDEFINE) FROM TCATEGORY WHERE (SCATEGORYTYPEID = '08') AND (CACTIVE = '1')  AND NIMPACTLEVEL = a.NVALUEEFFECT ) as dt3,
(SELECT MAX(SDEFINE) FROM TCATEGORY WHERE (SCATEGORYTYPEID ='09') AND (CACTIVE = '1')  AND NIMPACTLEVEL = a.NENVIRONMENTEFFECT ) as dt4
  FROM TACCIDENT a  WHERE A.SACCIDENTID = '{0}'", data[3] + "");


                        DataTable dt1 = CommonFunction.Get_Data(sql, query);

                        if (dt1.Rows.Count > 0)
                        {
                            sDetail = string.Format("ภาพลักษณ์องค์กร : {0} <br/> เสียชีวิต : {1} <br/> ทรัพย์สิน : {2} <br/> สิ่งแวดล้อม : {3}", dt1.Rows[0]["dt1"] + "", dt1.Rows[0]["dt2"] + "", dt1.Rows[0]["dt3"] + "", dt1.Rows[0]["dt4"] + "");
                        }

                        break;


                }



                ((Literal)popupControl.FindControl("ltrContent")).Text = sDetail;
                popupControl.ShowOnPageLoad = true;

                break;


        }
    }
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    void BindData()
    {
        string strsql = "";
        string Type = CommonFunction.ReplaceInjection(cboType.Value + "");

        strsql = @"

SELECT o.ID1,o.DREDUCE,o.DREDUCE, o.SABBREVIATION,o.SCONTRACTNO,   o.SPROCESSNAME , o.SHEADREGISTERNO,o.STRAILERREGISTERNO , o.SPROCESSID, o.DELIVIERYNO,
 
CASE WHEN nvl(o.DELIVIERYNO,'1') = '1' THEN '' 
ELSE 
(SELECT wm_concat( DISTINCT st.SABBREVIATION)  FROM TDELIVERY_SAP sds 
LEFT JOIN TTERMINAL st ON sds.SUPPLY_PLANT = ST.STERMINALID
WHERE  nvl(st.CACTIVE,'1') != '0' AND sds.DELIVERY_NO IN (o.DELIVIERYNO) ) 

END    AS STARTTERMINAL,

 CASE WHEN nvl(o.DELIVIERYNO,'1') = '1' THEN '' 

ELSE 
(SELECT wm_concat(DISTINCT sv.SABBREVIATION)  FROM TDELIVERY_SAP sds 
LEFT JOIN TVENDOR sv ON SV.SVENDORID = sds.PLANT
WHERE  nvl(sv.CACTIVE,'Y') != 'N' and sds.DELIVERY_NO IN (o.DELIVIERYNO) ) 
END    AS ENDTERMINAL,

o.REDUCEBY
 , o.SREFERENCEID , o.SREDUCENAME

FROM
(
SELECT ROW_NUMBER () OVER (ORDER BY r.DREDUCE DESC) AS ID1, r.nreduceid,v.SVENDORID,  r.DREDUCE, V.SABBREVIATION,C.SCONTRACTNO,   P.SPROCESSNAME , r.SHEADREGISTERNO,r.STRAILERREGISTERNO ,
CASE  r.SPROCESSID 
WHEN '010'  THEN   ''
WHEN '011'  THEN   ''
WHEN '020'  THEN   (SELECT wm_concat(spl.SDELIVERYNO)  AS STERMINALNAME FROM
(TPLANSCHEDULE sp LEFT JOIN TPlanScheduleList spl ON sp.NPLANID = sPL.NPLANID)
WHERE (sP.CCONFIRM = '0' OR sP.CCONFIRM IS NULL ) AND sp.CACTIVE = '1' AND sPL.CACTIVE = '1' AND sPL.CREDUCE = '1' 
AND sp.NPLANID = REPLACE(r.SREFERENCEID ,'-','') )
WHEN '030'  THEN   '' 
WHEN '040'  THEN   ''
WHEN '050'  THEN   ''
WHEN '060'  THEN   ''
WHEN '080'  THEN   (SELECT MAX(scp.SDELIVERYNO) FROM TCOMPLAIN scp WHERE scp.SSERVICEID = REPLACE(r.SREFERENCEID ,'-','')) 
WHEN '090'  THEN   (SELECT MAX(sa.SDELIVERYNO) FROM TACCIDENT sa WHERE sa.SACCIDENTID = REPLACE(r.SREFERENCEID ,'-','') )
WHEN '021'  THEN   ''
ELSE '' END  AS DELIVIERYNO 

, CASE WHEN NVL(U.SFIRSTNAME,'') != '' THEN  r.SREDUCEBY || ' - ' || U.SFIRSTNAME || '  ' || U.SLASTNAME ELSE r.SREDUCEBY END  AS REDUCEBY
, r.SPROCESSID , r.SREFERENCEID , r.SREDUCENAME
FROM TREDUCEPOINT r
LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO and NVL(R.STRAILERREGISTERNO,'1') = NVL(T.STRAILERREGISTERNO,'1')
LEFT JOIN TCONTRACT_TRUCK ct ON CT.STRUCKID = T.STRUCKID AND NVL(CT.STRAILERID,'1') = NVL(T.STRAILERID,'1')
LEFT JOIN TCONTRACT c ON C.SCONTRACTID = NVL(CT.SCONTRACTID,R.SCONTRACTID)
LEFT JOIN TVENDOR v ON V.SVENDORID = C.SVENDORID
LEFT JOIN TPROCESS p ON p.SPROCESSID = R.SPROCESSID
LEFT JOIN TUSER u ON U.SUID = R.SREDUCEBY

WHERE NVL(r.CACTIVE,'1') != '0'
AND NVL(T.CACTIVE,'Y') != 'N' AND 
NVL(C.CACTIVE,'Y') != 'N'
AND  NVL(V.CACTIVE, 'Y') != 'N'
AND r.DREDUCE  BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') 
" + (!string.IsNullOrEmpty(txtSearch.Text.Trim()) ? "" : "") + @" AND (c.SCONTRACTNO LIKE '%' || :oSearch || '%' OR r.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR r.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR V.SABBREVIATION LIKE '%' || :oSearch || '%')
" + (Type != "" ? " AND r.SPROCESSID = '" + Type + "'" : "") + ") o where 1=1 --and o.SVENDORID <> '0010001392'";
        //Response.Write(strsql);


        sds.SelectCommand = strsql;
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add(":oSearch", txtSearch.Text.Trim());
        sds.SelectParameters.Add(":dStart", dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
        sds.SelectParameters.Add(":dEnd", dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));

        sds.DataBind();
        gvw.DataBind();

    }

    protected void btnPdfExport2_Click(object sender, EventArgs e)
    {

        BindData();
        PdfExportOptions options = new PdfExportOptions();


        gridExport.GridViewID = "gvw";
        gridExport.PageHeader.Center = "DetailDefectMonitoring";
        gridExport.PageHeader.Font.Bold = true;
        gridExport.Landscape = true;
        gridExport.MaxColumnWidth = 180;
        // gridExport.WritePdfToResponse("DetailDefectMonitoring", options);



        using (MemoryStream ms = new MemoryStream())
        {
            PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
            pcl.Component = gridExport;
            pcl.Margins.Left = pcl.Margins.Right = 50;
            pcl.Landscape = true;
            pcl.CreateDocument(false);
            pcl.PrintingSystem.Document.AutoFitToPagesWidth = 1;
            pcl.ExportToPdf(ms);
            WriteResponse(this.Response, ms.ToArray(), System.Net.Mime.DispositionTypeNames.Inline.ToString());
        }

    }
    protected void btnXlsExport2_Click(object sender, EventArgs e)
    {
        BindData();
        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;


        gridExport.GridViewID = "gvw";
        gridExport.PageHeader.Center = "DetailDefectMonitoring";
        gridExport.PageHeader.Font.Bold = true;
        //gridExport.ExportToXls("File.xls", options);
        gridExport.WriteXlsToResponse("DetailDefectMonitoring", options);
        //gridExport.WriteXlsToResponse();
    }

    public static void WriteResponse(HttpResponse response, byte[] filearray, string type)
    {
        response.ClearContent();
        response.Buffer = true;
        response.Cache.SetCacheability(HttpCacheability.Private);
        response.ContentType = "application/pdf";
        ContentDisposition contentDisposition = new ContentDisposition();
        contentDisposition.FileName = "test.pdf";
        contentDisposition.DispositionType = type;
        response.AddHeader("Content-Disposition", contentDisposition.ToString());
        response.BinaryWrite(filearray);
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        try
        {
            response.End();
        }
        catch (System.Threading.ThreadAbortException)
        {
        }

    }

}
