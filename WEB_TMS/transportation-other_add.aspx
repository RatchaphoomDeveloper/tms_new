﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="transportation-other_add.aspx.cs" Inherits="transportation_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            width: 50%;
            height: 31px;
        }
        .style14
        {
            width: 20%;
            height: 61px;
        }
        .style15
        {
            width: 30%;
            height: 61px;
        }
        .style16
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" class="style14">
                            เลข Outbound<font color='red'>*</font></td>
                        <td class="style15">
                            <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" MaxLength="10"
                                ClientInstanceName="cboDelivery" EnableCallbackMode="True" OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SDELIVERYNO" Width="180px">
                                <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){cboVendor.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORID'));cboHeadRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHEADREGISTERNO')); txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'));txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));};xcpn.PerformCallback('SelectChange');}" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="Outbound No" FieldName="SDELIVERYNO" Width="100px" />
                                    <dx:ListBoxColumn Caption="รหัสผู้ประกอบการ" FieldName="SVENDORID" Width="100px" />
                                    <dx:ListBoxColumn Caption="ทะเบียนรถ(หัว)" FieldName="SHEADREGISTERNO" Width="100px" />
                                    <dx:ListBoxColumn Caption="ทะเบียนรถ(ท้าย)" FieldName="STRAILERREGISTERNO" Width="100px" />
                                    <dx:ListBoxColumn Caption="STRUCKID" FieldName="STRUCKID" Width="100px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left;" class="style14">
                            ชื่อผู้ประกอบการขนส่ง<font color='red'>*</font></td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left;" class="style15">
                            <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SVENDORID" Width="180px">
                                <ClientSideEvents ValueChanged="function (s, e) {xcpn.PerformCallback('SelectChange');}" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            <span style="text-align: left">ทะเบียนหัว<font color='red'>*</font></span></td>
                        <td align="left" class="style27">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxComboBox ID="cboHeadRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboHeadRegist"
                                            EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL"
                                            OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                            SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="180px">
                                            <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')) ;txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));};xcpn.PerformCallback('SelectChange');}" />
                                            <Columns>
                                                <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                                <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                <dx:ListBoxColumn Caption="STRUCKID" FieldName="STRUCKID" Width="100px" />
                                            </Columns>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                        </asp:SqlDataSource>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style18">
                            
                            ทะเบียนท้าย</td>
                        <td align="left" class="style18">
                          
                            <dx:ASPxTextBox ID="txtTrailerRegist" runat="server" ClientEnabled="False" 
                                ClientInstanceName="txtTrailerRegist" Width="180px">
                                <Border BorderColor="#CCCCCC" />
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtTruckID" runat="server" ClientInstanceName="txtTruckID" 
                                ClientVisible="False" Width="180px">
                            </dx:ASPxTextBox>
                          
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                           <span style="text-align: left">ชื่อพนักงานขับรถ<font color='red'>*</font></span>
                        </td>
                        <td align="left" colspan="3">
                             <dx:ASPxComboBox ID="cmbEMPLOYEE" runat="server" CallbackPageSize="30" ClientInstanceName="cmbEMPLOYEE"
                                EnableCallbackMode="True" OnItemRequestedByValue="cmbEMPLOYEE_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cmbEMPLOYEE_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="SEMPLOYEEID" Width="220px">
                                <Columns>
                                    <dx:ListBoxColumn Caption="เลขที่บัตรประชาชน" FieldName="SPERSONELNO" />
                                    <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                    <dx:ListBoxColumn Caption="หมายเลขโทรศัพท์" FieldName="STEL" />
                                    <dx:ListBoxColumn Caption="หมายเลขใบขับขี่" FieldName="SDRIVERNO" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" style="width: 20%">
                            <span style="text-align: left">วันที่เกิดปัญหา </span>
                        </td>
                        <td style="width: 30%">
                            <dx:ASPxDateEdit ID="dteDate" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 20%;">
                            
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                           
                        </td>
                    </tr>
                    
                       <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                           <span style="text-align: left">สถานที่เกิดปัญหา</span>
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxMemo ID="txtProblemPlace" runat="server" Height="80px" Width="500px">
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" class="style25">
                            ประเภทปัญหา
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxTextBox ID="txtTypeProblem" runat="server" Width="300px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                            รายละเอียดปัญหา
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxMemo ID="txtProblemDetail" runat="server"  Width="500px" Rows="5"  >
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            แนวทางการแก้ไข
                        </td>
                        <td colspan="3">
                            <dx:ASPxMemo ID="txtSolution" runat="server" Width="500px" Rows="5"  />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            ชื่อผู้แจ้ง
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtCreateName" runat="server" Width="200px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                             Allowed file types: ฯลฯ Max file size:
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                        </td>
                        <td align="left" colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        <span style="text-align: left">ชื่อหลักฐาน</span>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtEvidence" runat="server" Width="170px">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <span style="text-align: left">ไฟล์แนบ </span>
                                    </td>
                                    <td>
                                        <dx:ASPxUploadControl ID="uploader" runat="server" ClientInstanceName="uploader"
                                            NullText="Click here to browse files..." Size="35" OnFileUploadComplete="UploadControl_FileUploadComplete">
                                            <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData!=''){xcpn.PerformCallback('Upload');}else{dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpeg,.jpg หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');} }">
                                            </ClientSideEvents>
                                           <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>" AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                                            </ValidationSettings>
                                            <BrowseButton Text="แนบไฟล์">
                                            </BrowseButton>
                                        </dx:ASPxUploadControl>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add">
                                            <ClientSideEvents Click="function(s,e){uploader.Upload();}"></ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                            &nbsp;
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" Settings-ShowColumnHeaders="false"
                                Style="margin-top: 0px" ClientInstanceName="sgvw" Width="100%" KeyFieldName="dtID"
                                OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SkinID="_gvw">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                        VisibleIndex="0">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="รหัส" FieldName="dtID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="FilePath" FieldName="dtFilePath" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn VisibleIndex="4" Width="10%" FieldName="dd" CellStyle-ForeColor="#0066FF">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lblScore" runat="server" Text="ชื่อหลักฐาน">
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle ForeColor="#0066FF" HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" VisibleIndex="5" Width="85%">
                                        <DataItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxLabel ID="lblEvidence" runat="server" Text='<%# Eval("dtEvidenceName") %>'
                                                            Width="250">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="imbView0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                            Text="View" Width="15px">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('ViewList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                            Text="Delete" Width="15px">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('DeleteList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings GridLines="None" ShowColumnHeaders="False"></Settings>
                                <Border BorderStyle="None" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                        </td>
                        <td align="left" colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtScore" runat="server" Width="100px" CssClass="dxeLineBreakFix"
                                            Text="0" Visible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" class="style24">
                            &nbsp;
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style21" colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')){ return false; }else{xcpn.PerformCallback('Save');} }" />
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2" class="style13">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'transportation-other.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
