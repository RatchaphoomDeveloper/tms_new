﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KMView.aspx.cs" Inherits="KMView" EnableEventValidation="true" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div style="font-family: 'Century Gothic'">
        <asp:UpdatePanel ID="UpdatePanel" runat="server">
            <ContentTemplate>
                <div class="panel panel-info" style="font-family: 'Century Gothic'">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>
                        <asp:Label ID="lblHeaderTab1" runat="server" Font-Names="Century Gothic" Font-Size="16px" Text="Knowledge Management (KM) Organizational Learning Center"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <asp:Label ID="lblView" runat="server" ForeColor="Blue" Font-Names="Century Gothic" Font-Size="16px"></asp:Label>&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="col-md-10">
                                        <asp:ImageButton ID="imgLikeYes" runat="server" ImageUrl="~/Images/ImageForKM/Like_active.png" Width="10%" Height="10%" Style="cursor: pointer" OnClick="imgLikeYes_Click" />
                                        <asp:ImageButton ID="imgLikeNo" runat="server" ImageUrl="~/Images/ImageForKM/Like_inactive.png" Width="10%" Height="10%" Style="cursor: pointer" OnClick="imgLikeNo_Click" />

                                        &nbsp;&nbsp;&nbsp;

                                <asp:ImageButton ID="imgBookYes" runat="server" ImageUrl="~/Images/ImageForKM/Bookmark_active.png" Width="10%" Height="10%" Style="cursor: pointer" OnClick="imgBookYes_Click" />
                                        <asp:ImageButton ID="imgBookNo" runat="server" ImageUrl="~/Images/ImageForKM/Bookmark_inactive.png" Width="10%" Height="10%" Style="cursor: pointer" OnClick="imgBookNo_Click" />
                                    </div>
                                </div>

                                <br />
                                <br />

                                <asp:Table runat="server" Width="100%" BorderStyle="None">
                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblTopicName" runat="server" Text="Topic name :&nbsp;" Font-Bold="true" Font-Names="Century Gothic" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtTopicName" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">

                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblTopicID" runat="server" Text="Topic ID :&nbsp;" Font-Bold="true" Font-Names="Century Gothic" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtTopicID" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblAuthorName" runat="server" Text="Author name :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtAuthorName" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblDepartment" runat="server" Text="Department :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtDepartment" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblDivisioin" runat="server" Text="Division :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtDivisioin" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblCoP" runat="server" Text="CoP :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtCoP" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblKnowledgeType" runat="server" Text="Knowledge type :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtKnowledgeType" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblProductTag" runat="server" Text="Keywords (Tags) :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtProductTag" runat="server" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblKeyword" runat="server" Text="Other keywords :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtKeyword" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right; vertical-align: top;">
                                            <asp:Label ID="lblExecutive" runat="server" Text="Executive summary/abstract :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtExecutive" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right; vertical-align: top;">
                                            <asp:Label ID="lblDocumentContent" runat="server" Text="Document content :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%" BorderStyle="None">
                                            <asp:GridView ID="dgvItem" runat="server" CssClass="table table-hover" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                                EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader" ShowHeader="False"
                                                HorizontalAlign="Center" AutoGenerateColumns="false" Font-Size="16px" BorderWidth="0"
                                                CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH,FILENAME_USER,FILENAME_SYSTEM" OnRowCommand="dgvItem_RowCommand"
                                                OnRowUpdating="dgvItem_RowUpdating" OnRowDataBound="dgvItem_RowDataBound" EmptyDataText="[ ไม่มีข้อมูล ]">
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="" ItemStyle-BorderWidth="0">
                                                        <ItemStyle Font-Size="16px" />
                                                        <ItemTemplate>
                                                            <b>
                                                                <asp:LinkButton ID="LinkDownload" CommandName="Download" runat="server" Text='<%# Eval("FILENAME_USER")%>' Style="font-family: 'Century Gothic'"></asp:LinkButton></b>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            </asp:GridView>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblCreatedDate" runat="server" Text="Created date :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtCreatedDate" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="30%" Style="text-align: right">
                                            <asp:Label ID="lblUpdatedDate" runat="server" Text="Last modified :&nbsp;" Font-Bold="true" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="45%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="txtUpdatedDate" runat="server" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right">
                        <asp:Button ID="cmdEdit" runat="server" Text="Edit" CssClass="btn btn-warning" Width="90px" OnClick="cmdEdit_Click" Font-Size="16px"/>
                        <asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="btn btn-danger" Width="90px" data-toggle="modal" data-target="#ModalConfirmDelete" UseSubmitBehavior="false"  Font-Size="16px"/>
                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="btn btn-info" Width="90px" OnClick="cmdCancel_Click" Font-Size="16px" />
                    </div>

                    <br />

                    <asp:GridView ID="dgvKMView" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" DataKeyNames="KM_COMMENT_ID" ShowHeader="false"
                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" OnRowDataBound="dgvKMView_RowDataBound"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" Font-Size="16px" BorderWidth="0"
                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" EmptyDataRowStyle-BorderWidth="0" OnRowCommand="dgvKMView_RowCommand"
                        AllowPaging="true" OnPageIndexChanging="dgvKMView_PageIndexChanging" PageSize="10" OnRowDeleting="dgvKMView_RowDeleting"
                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                        <Columns>
                            <asp:BoundField ItemStyle-Width="20%" ItemStyle-BorderWidth="0"></asp:BoundField>
                            <asp:TemplateField HeaderText="Comment" ItemStyle-BorderWidth="0">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--Posted date: 6/23/2017 1:48 PM by MISS--%>
                                    <asp:Label ID="lblComment" runat="server" Text='<%# Eval("COMMENT_DETAIL")%>' Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Label ID="lblPostDate" runat="server" Text='<%# Eval("POST_DATE")%>' ForeColor="LightGray" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                    &nbsp;&nbsp;<asp:LinkButton ID="LinkEdit" CommandName="EditData" runat="server" Text="Edit" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:LinkButton>
                                    &nbsp;&nbsp;<asp:LinkButton ID="LinkDelete" CommandName="Delete" runat="server" Text="Delete" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:LinkButton>
                                    <br />
                                    <asp:Label ID="lblEndPost" runat="server" Text="----------------------------------------------------------------------------------------------------------------------------------" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>

                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-Width="20%" ItemStyle-BorderWidth="0"></asp:BoundField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle CssClass="gvHeader-white"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />

                    </asp:GridView>
                    <asp:Table ID="tbComment" runat="server" Width="100%" BorderStyle="None">
                        <asp:TableRow>
                            <asp:TableCell Width="20%">

                            </asp:TableCell>
                            <asp:TableCell Width="60%">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtComment" runat="server" Width="500px" Height="100px" TextMode="MultiLine" CssClass="form-control" placeholder="Please give me feedback here !" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                            <div style="text-align: right">
                                                <asp:Button ID="cmdPostComment" runat="server" Text="Post" CssClass="btn btn-success" Width="90px" Style="font-family: 'Century Gothic'" Font-Size="16px" OnClick="cmdPostComment_Click" />
                                            </div>
                                        </asp:TableCell>
                                        <asp:TableCell>

                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                                <br />
                            </asp:TableCell>
                            <asp:TableCell Width="20%">

                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <br />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgvItem" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hidBookmark" runat="server" />
        <asp:HiddenField ID="hidCreate" runat="server" />
        <uc1:ModelPopup runat="server" ID="mpConfirmDelete" IDModel="ModalConfirmDelete"
            IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmDelete_ClickOK"
            TextTitle="ยืนยันการลบข้อมูล" TextDetail="คุณต้องการลบข้อมูลใช่หรือไม่ ?" />
        <div class="modal fade" id="ShowEvent" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog modal-lg">
                <asp:UpdatePanel ID="updShowEvent" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Picture/imgInformation.png" Width="48" Height="48" />
                                    <asp:Label ID="lblEvent" runat="server" Text="Edit Comment" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:Table runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell Width="15%">
                                            <div class="form-group">
                                                <asp:Label ID="lblCommentReset" runat="server" Text="Comment :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                            </div>
                                        </asp:TableCell>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                        <asp:TableCell Width="85%">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtCommentReset" runat="server" Width="500px" Height="100px" TextMode="MultiLine" CssClass="form-control" placeholder="Please give me feedback here !" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                            </div>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="cmdDel" runat="server" Text="Delete" Width="100" CssClass="btn btn-md btn-hover btn-danger" OnClick="cmdDel_Click" UseSubmitBehavior="false" Style="font-family: 'Century Gothic'" Font-Size="16px" />
                                <asp:Button ID="cmdReset" runat="server" Text="Save" Width="90" CssClass="btn btn-md btn-hover btn-info" OnClick="cmdReset_Click" UseSubmitBehavior="false" Style="font-family: 'Century Gothic'" Font-Size="16px" />
                                <asp:Button ID="CloseModal" runat="server" class="btn btn-md btn-hover btn-info" data-dismiss="modal" Width="90px" aria-hidden="true" Text="Cancel" UseSubmitBehavior="false" Style="font-family: 'Century Gothic'" Font-Size="16px" />
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="cmdDel" />
                        <asp:PostBackTrigger ControlID="cmdReset" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
