﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AccidentTab2.aspx.cs" Culture="en-US" UICulture="en-US" Inherits="AccidentTab2" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">

                <ul class="nav nav-tabs" runat="server" id="tabtest">
                    <li class="" id="liTab1" runat="server"><a href="#TabGeneral" runat="server" id="GeneralTab1">แจ้งเรื่องอุบัติเหตุ</a></li>
                    <li class="active" id="liTab2" runat="server"><a href="#TabGeneral2" runat="server" id="GeneralTab2" data-toggle="tab" aria-expanded="true">รายงานเบื้องต้น</a></li>
                    <li class="" id="liTab3" visible="false" runat="server"><a href="#TabGeneral3" runat="server" id="GeneralTab3">รายงานวิเคราะห์สาเหตุ</a></li>
                    <li class="" id="liTab4" visible="false" runat="server"><a href="#TabGeneral4" runat="server" id="GeneralTab4">รายงานผลการพิจารณา</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="TabGeneral2">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse7" id="acollapse7">ความเสียหาย(ประเมินโดยผู้ขนส่ง)</a>
                                <asp:HiddenField runat="server" ID="hidcollapse7" />
                            </div>
                            <div id="collapse7" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group ">
                                        <div class="col-md-12">
                                            <h5>
                                                <asp:Label Text="" ID="lblType" runat="server" /></h5>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">ข้อมูลการเกิดอุบัติเหตุ</a>
                                <asp:HiddenField runat="server" ID="hidcollapse1" />
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">Accident ID</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAccidentID" CssClass="form-control" ReadOnly="true" Text="Generate by System" />
                                        </div>

                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">วันที่-เวลาเกิดเหตุ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAccidentDate" CssClass="form-control datetimepicker" ReadOnly="true" />
                                        </div>
                                        <label class="col-md-2 control-label">วันที่-เวลาที่แจ้งเรื่องในระบบ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAccidentDateSystem" CssClass="form-control datetimepicker" ReadOnly="true" />
                                        </div>

                                    </div>
                                    <div class="row form-group" id="divREPORTER">
                                        <label class="col-md-3 control-label">ผขส. แจ้งเหตุให้ ปตท.</label>
                                        <div class="col-md-3">
                                            <div class="col-md-4 PaddingLeftRight0">
                                                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblREPORTER" Enabled="false">
                                                    <asp:ListItem Text="No" Value="0" />
                                                    <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="col-md-8 PaddingLeftRight0">
                                                <asp:TextBox runat="server" ID="txtAccidentDatePtt" CssClass="form-control datetimepicker" ReadOnly="true" />
                                            </div>

                                        </div>
                                        <label class="col-md-2 control-label">ชื่อผู้แจ้ง(ผู้ขนส่ง)</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAccidentName" CssClass="form-control" ReadOnly="true" />
                                        </div>

                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">วันที่-เวลาจัดทำอุบัติเหตุเบื้องต้น</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtTimeCreate" CssClass="form-control datetimepicker" ReadOnly="true" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:Panel runat="server" ID="plTab1" Enabled="false">
                            <div class="panel panel-info hidden">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">ข้อมูลเบื้องต้นของอุบัติเหตุ</a>
                                    <asp:HiddenField runat="server" ID="hidcollapse2" />
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">ขั้นตอนการขนส่งขณะเกิดเหตุ<asp:Label ID="lblReqddlAccidentType" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlAccidentType">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">อ้างอิงหมายเลข Shipment<asp:Label ID="lblReqtxtShipmentNo" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <div class="col-md-10 PaddingLeftRight0">
                                                    <asp:TextBox runat="server" ID="txtShipmentNo" CssClass="form-control" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnShipmentNoSearch" runat="server" Text="..." Width="30px" />
                                                </div>

                                            </div>
                                            <label class="col-md-2 control-label">ทะเบียนรถ<asp:Label ID="lblReqtxtTruck" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">

                                                <div class="col-md-10 PaddingLeftRight0">
                                                    <asp:TextBox runat="server" ID="txtTruck" CssClass="form-control" ReadOnly="true" />
                                                    <asp:HiddenField runat="server" ID="hidSTRUCKID" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnTruck" runat="server" Text="..." Width="30px" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">ชื่อผู้ขนส่ง<asp:Label ID="lblReqddlVendor" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">เลขที่สัญญาจ้างขนส่ง<asp:Label ID="lblReqddlContract" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">กลุ่มงาน<asp:Label ID="lblReqddlWorkGroup" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlWorkGroup">
                                                </asp:DropDownList>

                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มที่<asp:Label ID="lblReqddlGroup" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGroup">
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse21" id="acollapse21">พนักงานคนที่ 1</a>
                                                <asp:HiddenField runat="server" ID="HiddenField1" />
                                            </div>
                                            <div id="collapse21" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE" runat="server" Text="..." Width="30px" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODEClear" runat="server" Text="X" Width="30px" />
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" Checked="true" GroupName="DRIVER_NO" ID="rbDRIVER_NO1" runat="server" />
                                                        </div>

                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse22" id="acollapse22">พนักงานคนที่ 2</a>
                                                <asp:HiddenField runat="server" ID="HiddenField2" />
                                            </div>
                                            <div id="collapse22" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE2" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE2" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID2" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE2" runat="server" Text="..." Width="30px" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE2Clear" runat="server" Text="X" Width="30px" />
                                                            </div>
                                                        </div>

                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" ID="rbDRIVER_NO2" GroupName="DRIVER_NO" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME2" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE2" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE2" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse23" id="acollapse23">พนักงานขับรถคนที่ 3</a>
                                                <asp:HiddenField runat="server" ID="HiddenField4" />
                                            </div>
                                            <div id="collapse23" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE3" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE3" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID3" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE3" runat="server" Text="..." Width="30px" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE3Clear" runat="server" Text="X" Width="30px" />
                                                            </div>
                                                        </div>

                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" ID="rbDRIVER_NO3" GroupName="DRIVER_NO" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME3" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE3" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE3" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse24" id="acollapse24">พนักงานขับรถคนที่ 4</a>
                                                <asp:HiddenField runat="server" ID="HiddenField6" />
                                            </div>
                                            <div id="collapse24" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE4" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE4" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID4" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE4" runat="server" Text="..." Width="30px" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE4Clear" runat="server" Text="X" Width="30px" />
                                                            </div>
                                                        </div>

                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" ID="rbDRIVER_NO4" GroupName="DRIVER_NO" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME4" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE4" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE4" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">จุดรับ-จ่ายผลิตภัณฑ์ต้นทาง<asp:Label ID="lblReqtxtSABBREVIATION" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">

                                                <div class="col-md-9 PaddingLeftRight0">
                                                    <asp:TextBox runat="server" ID="txtSABBREVIATION" CssClass="form-control" ReadOnly="true" />
                                                    <asp:HiddenField runat="server" ID="hidSTERMINALID" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnTERMINAL" runat="server" Text="..." Width="30px" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnTERMINALClear" runat="server" Text="X" Width="30px" />
                                                </div>
                                            </div>
                                            <label class="col-md-1 control-label">พิกัด GPS<asp:Label ID="lblReqtxtGPS" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <label class="col-md-1 control-label">Latitude</label>
                                            <div class="col-md-1">
                                                <asp:TextBox runat="server" ID="txtGPSL" CssClass="form-control " />
                                            </div>
                                            <label class="col-md-1 control-label">Longitude</label>
                                            <div class="col-md-1">
                                                <asp:TextBox runat="server" ID="txtGPSR" CssClass="form-control " />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">สถานที่เกิดเหตุ<asp:Label ID="lblReqtxtLocation" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-8">
                                                <asp:TextBox runat="server" ID="txtLocation" CssClass="form-control" TextMode="MultiLine" Rows="5" />
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView runat="server" ID="gvDO"
                                                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AllowPaging="false">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="เลข DO" DataField="DELIVERY_NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="ผลิตภัณฑ์" DataField="MATERIAL_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="ปริมาณ" DataField="DLV" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="ปลายทาง" DataField="CUST_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="วันที่ LOADING" DataField="LOADING_END" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>

                        </asp:Panel>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse3" id="acollapse3">การเกิดอุบัติเหตุ</a>
                                <asp:HiddenField runat="server" ID="hidcollapse3" />
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ลักษณะเหตุการณ์<asp:Label ID="lblReqrblEvent" Text="&nbsp;*" ForeColor="Red" Visible="false" runat="server" /></label></label>
                                        <div class="col-md-4">
                                            <asp:RadioButtonList runat="server" ID="rblEvent" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblEvent_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtRemark" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">คู่กรณี<asp:Label ID="lblReqrblParty" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-3">
                                            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblParty" OnSelectedIndexChanged="rblParty_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="&nbsp;ไม่มีคู่กรณี&nbsp;" Value="0" />
                                                <asp:ListItem Text="&nbsp;มีคู่กรณี&nbsp;" Value="1" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <asp:Panel runat="server" ID="plParties">
                                            <div class="col-md-6">

                                                <asp:GridView runat="server" ID="gvParties"
                                                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowUpdating="gvParties_RowUpdating"
                                                    AllowPaging="True">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ลักษณะ" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtDetailParties" CssClass="form-control" Text='<%#Eval("DETAIL") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ทะเบียนรถ" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="txtSTRAILERREGISTERNO" CssClass="form-control" Text='<%#Eval("STRAILERREGISTERNO") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>

                                                                <div class="col-sm-1">
                                                                    <asp:LinkButton ID="lnkChooseTERMINAL" runat="server" Text="ลบ" CommandName="update"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                            </div>
                                            <div class="col-md-1">
                                                <asp:Button Text="เพิ่ม" runat="server" ID="btnAddParties" class="btn btn-md bth-hover btn-info" OnClick="btnAddParties_Click" />
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">การประเมินความผิด(โดยผู้ขนส่ง)<asp:Label ID="lblReqrblEstimate" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-3">
                                            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblEstimate">
                                                <asp:ListItem Text="&nbsp;ฝ่ายถูก&nbsp;" Value="0" />
                                                <asp:ListItem Text="&nbsp;ฝ่ายผิด&nbsp;" Value="1" />
                                                <asp:ListItem Text="&nbsp;ประมาทร่วม&nbsp;" Value="2" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">รายละเอียดของเหตุการณ์ที่เกิดขึ้น<asp:Label ID="lblReqtxtDetail" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-9">
                                            <asp:TextBox runat="server" ID="txtDetail" CssClass="form-control " TextMode="MultiLine" Rows="5" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ข้อมูลความเร็วจากระบบ IVMS (km/hr)<asp:Label ID="lblReqtxtSpeed_TMS" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtSpeed_TMS" CssClass="form-control number" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ผลการตรวจวัดแอลกอฮอล์ (mg%)<asp:Label ID="lblReqtxtAlcoholt" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAlcohol" CssClass="form-control number" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4">การบาดเจ็บของบุคคล(จำนวนผู้บาดเจ็บ/เสียชีวิต)</a>
                                <asp:HiddenField runat="server" ID="hidcollapse4" />
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <label class="col-md-1 control-label">พขร</label>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">เสียชีวิต(คน)<asp:Label ID="lblReqtxtVENDOR_DEAD" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-1">
                                            <asp:TextBox runat="server" CssClass="form-control number" ID="txtVENDOR_DEAD" />
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">บาดเจ็บ(คน)<asp:Label ID="lblReqtxtVENDOR_INJURY" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-1">
                                            <asp:TextBox runat="server" CssClass="form-control number" ID="txtVENDOR_INJURY" />
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">หยุดงาน(วัน)<asp:Label ID="lblReqtxtVENDOR_STOP" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-1">
                                            <asp:TextBox runat="server" CssClass="form-control number" ID="txtVENDOR_STOP" />
                                        </div>
                                        <label class="col-md-2 control-label" style="padding-left: 0; padding-right: 0;">อาการบาดเจ็บ<asp:Label ID="lblReqtxtVENDOR_DETAIL" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtVENDOR_DETAIL" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-1 control-label">คู่กรณี</label>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">เสียชีวิต(คน)<asp:Label ID="lblReqtxtPARTY_DEAD" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-1">
                                            <asp:TextBox runat="server" CssClass="form-control number" ID="txtPARTY_DEAD" />
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">บาดเจ็บ(คน)<asp:Label ID="lblReqtxtPARTY_INJURY" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-1">
                                            <asp:TextBox runat="server" CssClass="form-control number" ID="txtPARTY_INJURY" />
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">อาการบาดเจ็บ<asp:Label ID="lblReqtxtPARTY_DETAIL" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtPARTY_DETAIL" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-1 control-label">บุคคลที่ 3</label>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">เสียชีวิต(คน)<asp:Label ID="lblReqtxtOTHER_DEAD" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-1">
                                            <asp:TextBox runat="server" CssClass="form-control number" ID="txtOTHER_DEAD" />
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">บาดเจ็บ(คน)<asp:Label ID="lblReqtxtOTHER_INJURY" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-1">
                                            <asp:TextBox runat="server" CssClass="form-control number" ID="txtOTHER_INJURY" />
                                        </div>
                                        <label class="col-md-1 control-label" style="padding-left: 0; padding-right: 0;">อาการบาดเจ็บ<asp:Label ID="lblReqtxtOTHER_DETAIL" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtOTHER_DETAIL" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse5" id="acollapse5">ผลกระทบต่อสังคมและชุมชน</a>
                                <asp:HiddenField runat="server" ID="hidcollapse5" />
                            </div>
                            <div id="collapse5" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">การหกรั่วไหลของผลิตภัณฑ์ที่ขนส่ง<asp:Label ID="lblReqrblLeak" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-3">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblLeak" OnSelectedIndexChanged="rblLeak_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="&nbsp;มี&nbsp;" Value="0" />
                                                        <asp:ListItem Text="&nbsp;ไม่มี&nbsp;" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label"></label>
                                                <asp:Panel runat="server" ID="plProduct">
                                                    <div class="col-md-6">
                                                        <asp:GridView runat="server" ID="gvProduct"
                                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowUpdating="gvProduct_RowUpdating"
                                                            AllowPaging="True">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ชนิด" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox runat="server" ID="txtType" CssClass="form-control" Text='<%# Eval("TYPE") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ปริมาณ" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" Text='<%# Eval("AMOUNT") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>

                                                                        <div class="col-sm-1">
                                                                            <asp:LinkButton ID="lnkChooseTERMINAL" runat="server" Text="ลบ" CommandName="update"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                                            <PagerStyle CssClass="pagination-ys" />
                                                        </asp:GridView>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <asp:Button Text="เพิ่ม" runat="server" ID="btnAddProduct" class="btn btn-md bth-hover btn-info" OnClick="btnAddProduct_Click" />
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">เกิดการปนเปื้อน<asp:Label ID="lblReqrblContaminated" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-1">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblContaminated" OnSelectedIndexChanged="rblContaminated_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="&nbsp;มี&nbsp;" Value="0" />
                                                        <asp:ListItem Text="&nbsp;ไม่มี&nbsp;" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                                <label class="col-md-3 control-label">ชนิดผลิตภัณฑ์/ถูกปนเปื้อนด้วย<asp:Label ID="lblReqtxtContaminated_Detail" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-3">
                                                    <asp:TextBox runat="server" ID="txtContaminated_Detail" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <div class="row form-group">

                                                <label class="col-md-2 control-label">ไฟไหม้/ระเบิด</label>
                                                <label class="col-md-2 control-label">รถบรรทุกผลิตภัณฑ์<asp:Label ID="lblReqrblFire_Truck" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-3">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblFire_Truck">
                                                        <asp:ListItem Text="&nbsp;มี&nbsp;" Value="0" />
                                                        <asp:ListItem Text="&nbsp;ไม่มี&nbsp;" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">พื้นที่ชุมชนรอบข้าง<asp:Label ID="lblReqrblFire_Commu" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-3">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblFire_Commu">
                                                        <asp:ListItem Text="&nbsp;มี&nbsp;" Value="0" />
                                                        <asp:ListItem Text="&nbsp;ไม่มี&nbsp;" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label">ความเสียหายของรถ</label>
                                                <label class="col-md-2 control-label">ผู้ขนส่ง<asp:Label ID="lblReqtxtDamage_Vendor" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-4">
                                                    <asp:TextBox runat="server" ID="txtDamage_Vendor" CssClass="form-control " />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">คู่กรณี<asp:Label ID="lblReqtxtDamage_Party" Text="&nbsp;*" ForeColor="Red" runat="server" /></label></label>
                                                <div class="col-md-4">
                                                    <asp:TextBox runat="server" ID="txtDamage_Party" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">บุคคที่ 3<asp:Label ID="lblReqtxtDamage_Other" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-4">
                                                    <asp:TextBox runat="server" ID="txtDamage_Other" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <label class="col-md-4 control-label">รถขนส่งสามารถเดินรถ เพื่อทำการขนส่งต่อไป<asp:Label ID="lblReqrblDamage_Truck" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblDamage_Truck" OnSelectedIndexChanged="rblDamage_Truck_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="&nbsp;ได้&nbsp;" Value="0" />
                                                        <asp:ListItem Text="&nbsp;ไม่ได้&nbsp;" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <asp:Panel runat="server" ID="plDamage_Truck">
                                                <div class="panel panel-info">
                                                    <div class="panel-body">
                                                        <div class="row form-group">
                                                            <label class="col-md-4 control-label">วิธีการแก้ไข<asp:Label ID="lblReqrblC_SENT_HEAD" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-8">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblC_SENT_HEAD" OnSelectedIndexChanged="rblC_SENT_HEAD_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="&nbsp;เปลี่ยนหัวลากเป็น&nbsp;" Value="0" />
                                                        <asp:ListItem Text="&nbsp;สูบถ่ายผลิตภัณฑ์ไปยัง&nbsp;" Value="1" />
                                                        <asp:ListItem Text="&nbsp;อื่นๆ&nbsp;" Value="2" />
                                                    </asp:RadioButtonList>
                                                </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <label class="col-md-4 control-label"></label>
                                                            <label class="col-md-1 control-label">
                                                                <asp:Label Text="ทะเบียนหัว" runat="server" ID="lblTransfer" /><asp:Label ID="lblReqtxtTransfer" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>


                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtTransfer" CssClass="form-control" />
                                                </div>

                                                        </div>
                                                        <div class="row form-group">
                                                            <label class="col-md-4 control-label"></label>
                                                            <label class="col-md-1 control-label">
                                                                <asp:Label Text="ทะเบียนหาง" runat="server" ID="lblLicense" /><asp:Label ID="lblReqtxtLicense" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-6">
                                                    <asp:TextBox runat="server" ID="txtLicense" CssClass="form-control" />
                                                </div>
                                                            <label class="col-md-2 text-left control-label" style="text-align: left"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-info">
                                                    <div class="panel-body">
                                                        <div class="row form-group">
                                                            <label class="col-md-4 control-label">เพื่อทำการขนส่ง<asp:Label ID="lblReqrbC_Sent_Destination" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList runat="server" ID="rbC_Sent_Destination" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="&nbsp;ขนส่งต่อไปยังปลายทาง&nbsp;" Value="0" />
                                                        <asp:ListItem Text="&nbsp;ขนส่งกลับคลังต้นทาง&nbsp;" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>

                                                            <div class="col-md-2">
                                                                <asp:TextBox runat="server" ID="txtC_Sent_Destination" CssClass="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label">ผลกระทบต่อการจราจร<asp:Label ID="lblReqcbDamage_Traffic1" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>

                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-10">
                                                    <asp:RadioButton Text="&nbsp;กีดขวาง ต้องปิดการจราจร ไม่สามารถเดินรถผ่านได้" runat="server" ID="cbDamage_Traffic1" GroupName="cbDamage_Traffic" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-10">
                                                    <asp:RadioButton Text="&nbsp;กีดขวางการจราจรบางส่วน สามารถเปิดการจราจรบางส่วนได้(รวมถึงการเปิดช่องจราจรพิเศษด้วย ให้สามารถเดินรถได้)" runat="server" ID="cbDamage_Traffic2" GroupName="cbDamage_Traffic" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-10">
                                                    <asp:RadioButton Text="&nbsp;ไม่มีผลกระทบต่อการจราจร" runat="server" ID="cbDamage_Traffic3" GroupName="cbDamage_Traffic" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label">ความเสียหายอื่นๆ<asp:Label ID="lblReqtxtDamage_ETC" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>

                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-7">
                                                    <asp:TextBox runat="server" ID="txtDamage_ETC" CssClass="form-control" TextMode="MultiLine" Rows="5" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse6" id="acollapse6">ภาพลักษณ์และชื่อเสียงองค์กร</a>
                                <asp:HiddenField runat="server" ID="hidcollapse6" />
                            </div>
                            <div id="collapse6" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <label class="col-md-5 control-label">สื่อมวลชนในสถานที่เกิดเหตุ/ข้อมูลเหตุการณ์ผู้เผยแพร่สู่สาธารณะชน<asp:Label ID="lblReqrblMedia" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-4">
                                            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblMedia">
                                                <asp:ListItem Text="&nbsp;มี&nbsp;" Value="0" />
                                                <asp:ListItem Text="&nbsp;ไม่มี&nbsp;" Value="1" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-5 control-label">รายละเอียด</label>
                                        <div class="col-md-4">
                                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ID="txtAction_Media" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-5 control-label">มีประชาชน/ผู้ที่ไม่มีส่วนเกี่ยวข้องกับเหตุการณ์อยู่ในสถานที่เกิดเหตุ<asp:Label ID="lblReqrblPeople" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label></label>
                                        <div class="col-md-4">
                                            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblPeople">
                                                <asp:ListItem Text="&nbsp;มี&nbsp;" Value="0" />
                                                <asp:ListItem Text="&nbsp;ไม่มี&nbsp;" Value="1" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-5 control-label">รายละเอียด</label>
                                        <div class="col-md-4">
                                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ID="txtAction_People" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">เอกสารที่ต้องแนบประกอบการพิจารณา&nbsp;(บังคับ)</a>
                                    <asp:HiddenField runat="server" ID="HiddenField3" />
                                </div>
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView ID="dgvRequestFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                                    <Columns>

                                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse8" id="acollapse8">เอกสารหลักฐานที่เกี่ยวข้อง</a>
                                    <asp:HiddenField runat="server" ID="hidcollapse8" />
                                </div>
                                <div id="collapse8" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col col-md-5 control-label">ประเภทไฟล์เอกสาร</label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" ID="ddlUploadType" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col col-md-5 control-label">เลือกไฟล์</label>
                                            <div class="col-md-3">
                                                <asp:FileUpload ID="fileUpload" runat="server" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:Button Text="Upload" ID="btnUpload" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUpload_Click" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView ID="dgvUploadFile" runat="server" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false"
                                                    CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH" OnRowDeleting="dgvUploadFile_RowDeleting"
                                                    OnRowUpdating="dgvUploadFile_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound" EmptyDataText="[ ไม่มีข้อมูล ]">
                                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="No.">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                        <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                        <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                        <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>

                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="dgvUploadFile" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="row form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <input id="btnSave" runat="server" type="button" value="บันทึกและส่งข้อมูล" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" />

            <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-info" />
            <input id="btnApprove" runat="server" type="button" value="บันทึก(กรณีแก้ไขข้อมูล)" data-toggle="modal" data-target="#ModalConfirmBeforeApprove" class="btn btn-md bth-hover btn-info" />
        </div>
    </div>
    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeApprove" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSave_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpSaveSend" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSaveSend_ClickOK" TextTitle="ยืนยันการบันทึกและส่งข้อมูล" TextDetail="คุณต้องการบันทึกและส่งข้อมูลใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpBack" IDModel="ModalConfirmBack" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpBack_ClickOK" TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
    <asp:HiddenField ID="hidID" runat="server" />
    <asp:HiddenField ID="hidCactive" runat="server" />
    <asp:HiddenField ID="hidCGROUP" runat="server" />
    <asp:HiddenField ID="hidDRIVER_NO" runat="server" />
    <script type="text/javascript">
        function SetEnabledControlByID(id) {
            if ($('#<%=hidCGROUP.ClientID%>').val() == '0') {
                $('#divREPORTER').addClass("hide");
            }
            if ($('#<%=hidCGROUP.ClientID%>').val() != '0') {
                $('#btnSave,#btnApprove').prop('disabled', true);
            }
            else if (id == '2') {
                $('#btnApprove').prop('disabled', true);
                $('#btnSave').prop('disabled', false);
            }
            else if (id == '4' || id == '7') {
                $('#btnApprove').prop('disabled', false);
                $('#btnSave').prop('disabled', true);
            }
            else {
                $('#btnSave,#btnApprove').prop('disabled', true);
                $('#btnSave,#btnApprove').prop('disabled', false);
            }

            var last = $('#<%= hidcollapse1.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse1").removeClass('in');
            }
            last = $('#<%= hidcollapse2.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse2").removeClass('in');
            }
            last = $('#<%= hidcollapse3.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse3").removeClass('in');
            }
            last = $('#<%= hidcollapse4.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse4").removeClass('in');
            }
            last = $('#<%= hidcollapse5.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse5").removeClass('in');
            }
            last = $('#<%= hidcollapse6.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse6").removeClass('in');
            }
            last = $('#<%= hidcollapse7.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse7").removeClass('in');
            }
            last = $('#<%= hidcollapse8.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse8").removeClass('in');
            }
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        $(document).ready(function () {

            fcollapse();
        });


        function EndRequestHandler(sender, args) {
            fcollapse();
        }
        function fcollapse() {
            $("#acollapse1").on('click', function () {
                var active = $("#collapse1").attr('class');
                console.log(active);
                $('#<%= hidcollapse1.ClientID %>').val(active);
            });
            $("#acollapse2").on('click', function () {
                var active = $("#collapse2").attr('class');
                $('#<%= hidcollapse2.ClientID %>').val(active);
            });
            $("#acollapse3").on('click', function () {
                var active = $("#collapse3").attr('class');
                $('#<%= hidcollapse3.ClientID %>').val(active);
            });
            $("#acollapse4").on('click', function () {
                var active = $("#collapse4").attr('class');
                $('#<%= hidcollapse4.ClientID %>').val(active);
            });
            $("#acollapse5").on('click', function () {
                var active = $("#collapse5").attr('class');
                $('#<%= hidcollapse5.ClientID %>').val(active);
            });
            $("#acollapse6").on('click', function () {
                var active = $("#collapse6").attr('class');
                $('#<%= hidcollapse6.ClientID %>').val(active);
            });
            $("#acollapse7").on('click', function () {
                var active = $("#collapse7").attr('class');
                $('#<%= hidcollapse7.ClientID %>').val(active);
            });
            $("#acollapse8").on('click', function () {
                var active = $("#collapse8").attr('class');
                $('#<%= hidcollapse8.ClientID %>').val(active);
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

