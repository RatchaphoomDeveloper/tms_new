﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraRichEdit.Layout;
using System.IO;
using System.Globalization;
using DevExpress.XtraReports.UI;

public partial class Requesting : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string sREQUESTID = "";
    private static decimal nSlotAll = 0;
    private static DataTable dtMainData = new DataTable();
    private static int nNextYeatCheckWater = 3;
    private static string sTruckID_CheckWater = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Events
        gvwdoc.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwdoc_HtmlDataCellPrepared);
        #endregion
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            if (!string.IsNullOrEmpty(str))
            {
                dtMainData = new DataTable();
                string[] QueryString = STCrypt.DecryptURL(str);
                txtREQ_ID.Text = "" + QueryString[0];
                sREQUESTID = "" + QueryString[0];
                DisplayDatas("" + QueryString[0]);

                ListDataToPage("" + QueryString[0]);
              
            }
        }
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});"); return;
        }
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "VIEWCARHIS":
                break;
            case "VIEWSERVICEHIS":
                break;
            case "SUBMIT":
                SaveDatas();
                break;
        }
    }
    protected void gvwdoc_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("คอนโทรนดูเอกสาร"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePath = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePath") as ASPxTextBox;
            ASPxTextBox txtFileName = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileName") as ASPxTextBox;
            ASPxButton btnView = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnView") as ASPxButton;

            txtFilePath.ClientInstanceName = txtFilePath.ID + "_" + VisibleIndex;
            txtFileName.ClientInstanceName = txtFileName.ID + "_" + VisibleIndex;
            btnView.ClientInstanceName = btnView.ID + "_" + VisibleIndex;

            //Add Event
            btnView.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePath.ClientInstanceName + ".GetValue() +" + txtFileName.ClientInstanceName + ".GetValue());}";
        }

        if (e.DataColumn.Caption.Equals("ListRdl"))
        {
            //int VisibleIndex = e.VisibleIndex;


            //ASPxLabel lblStatus = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblStatus") as ASPxLabel;
            //string rblValue = !string.IsNullOrEmpty(gvwdoc.GetRowValues(VisibleIndex, "CONSIDER") + "") ? gvwdoc.GetRowValues(VisibleIndex, "CONSIDER") + "" : "";

            //if (!string.IsNullOrEmpty(rblValue))
            //{
            //    if (rblValue == "Y")
            //    {
            //        lblStatus.Text = "<span style='vertical-align:middle;'><img src='Images/action_check.png' width='16' height='16' alt='' /> ผ่าน</span>";
            //    }
            //    else
            //    {
            //        lblStatus.Text = "<span style='vertical-align:middle;'><img src='Images/action_delete.png' width='16' height='16' alt='' /> ไม่ผ่าน</span>";
            //    }
            //}
        }
    }
    protected void btnPrintDoc_Click(object sender, EventArgs e)
    {
        string REQ_ID = txtREQ_ID.Text;
        ListReport(REQ_ID);
    }
    private void DisplayDatas(string REQ_ID)
    {
        #region DATA#1 ข้อมูลพิ้นฐาน และ รายละเอียดค่าธรรมเนียม
        string _sql = @"SELECT   TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TO_DATE(TREQ.REQUEST_DATE,'dd/MM/yyyy') as REQUEST_DATE
                        ,TREQ.Status_Flag,TO_DATE(TRUCK.DWATEREXPIRE,'dd/MM/yyyy')  as DWATEREXPIRE ,TO_DATE(TREQ.APPOINTMENT_DATE,'dd/MM/yyyy') as APPOINTMENT_DATE,TVEN.SABBREVIATION
                        ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_NAME  ,TCAR.CARCATE_NAME
                        ,serPRICE.NPRICE   as SERVCHAGE_PRICE
                        ,addPRICE.NPRICE  as ADDITIONAL_PRICE
                        ,TREQ.TOTLE_SERVCHAGE,TSTATUS.STATUSREQ_ID,TREQCAUSE.CAUSE_NAME,TREQ.TOTLE_CAP As NTOTALCAPACITY
                        FROM TBL_Request TREQ
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR 
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_CAUSE TREQCAUSE
                       ON TREQCAUSE.CAUSE_ID = TREQ.CAUSE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        LEFT JOIN TBL_CARCATE TCAR
                        ON TCAR.CARCATE_ID = TREQ.CARCATE_ID
                       LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID = '00002') addPRICE
                       ON addPRICE.REQUEST_ID = TREQ.REQUEST_ID
                          LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID <> '00002') serPRICE
                        ON serPRICE.REQUEST_ID = TREQ.REQUEST_ID
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,
                            CASE  SCARTYPEID
                            WHEN 0 THEN SHEADREGISTERNO
                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
                            ELSE ''
                            END REGISTERNO
                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                            FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID
                        WHERE 1=1 AND TREQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(REQ_ID) + "'";
        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        if (dt.Rows.Count > 0)
        {
            lblDReq.Text = ChecknullDate(dt.Rows[0]["REQUEST_DATE"] + "");
            lblDExpWat.Text = ChecknullDate(dt.Rows[0]["DWATEREXPIRE"] + "");
            lblCause.Text = Checknull(dt.Rows[0]["CAUSE_NAME"] + "");
            lblReqType.Text = Checknull(dt.Rows[0]["REQTYPE_NAME"] + "");
            lblCarType.Text = string.Format("{0} - ความจุ {1:N0} ลิตร", dt.Rows[0]["CARCATE_NAME"] + "", Convert.ToDouble("0" + dt.Rows[0]["NTOTALCAPACITY"]));
            lblVeh_No.Text = dt.Rows[0]["VEH_NO"] + "" + (!string.IsNullOrEmpty(dt.Rows[0]["TU_NO"] + "") ? " / " + dt.Rows[0]["TU_NO"] + "" : "");
            lblVendor.Text = Checknull(dt.Rows[0]["SABBREVIATION"] + "");
            lblFee.Text = CheckNum(dt.Rows[0]["SERVCHAGE_PRICE"] + "");
            lblDAppoint.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            if ("" + dt.Rows[0]["Status_Flag"] == "10")
            {
                rblStatus.Value = "" + dt.Rows[0]["Status_Flag"];
                btnSubmit.ClientVisible = false;
            }

        }
        #endregion
        #region DATA#2 เอกสาร
        string QueryDoc = @"SELECT TBL_REQDOC.REQUEST_ID, TBL_REQDOC.DOC_ID, TBL_REQDOC.DOC_TYPE, TBL_REQDOC.DOC_ITEM, TBL_REQDOC.FILE_NAME, TBL_REQDOC.FILE_SYSNAME, 
TBL_REQDOC.FILE_PATH, TBL_REQDOC.CONSIDER,TBL_DOCTYPE.DOC_DESCRIPTION
FROM TBL_REQDOC
LEFT JOIN TBL_DOCTYPE
ON TBL_REQDOC.DOC_TYPE = TBL_DOCTYPE.DOCTYPE_ID
WHERE NVL(TBL_REQDOC.FILE_NAME,'xxx') <> 'xxx' AND NVL(TBL_REQDOC.FILE_SYSNAME,'xxx') <> 'xxx' AND 
NVL(TBL_REQDOC.FILE_PATH,'xxx') <> 'xxx' AND TBL_REQDOC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(REQ_ID) + "'";

        DataTable dtDoc = CommonFunction.Get_Data(conn, QueryDoc);
        if (dtDoc.Rows.Count > 0)
        {
            gvwdoc.ClientVisible = true;
            gvwdoc.DataSource = dtDoc;
            gvwdoc.DataBind();
        }
        else
        {
            gvwdoc.ClientVisible = false;
            trDoc1.Visible = false;
            trDoc2.Visible = false;
        }

        #endregion
        #region DATA#3 หมายเหตุ
        gvwRemark.DataSource = SystemFunction.List_TBL_REQREMARK(REQ_ID, "" + Session["UserID"]);
        gvwRemark.DataBind();
        #endregion
    }
    private void SaveDatas()
    {
        if ("" + rblStatus.Value == "10")
        {
            string REQ_ID = txtREQ_ID.Text;
            string _sql = "UPDATE TBL_REQUEST SET STATUS_FLAG='" + CommonFunction.ReplaceInjection("" + rblStatus.Value) + "' WHERE REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + "'";
            SystemFunction.SQLExecuteNonQuery(conn, _sql);
        }
        xcpn.JSProperties["cpRedirectTo"] = "approve_mv.aspx";
    }
    private void ListDataToPage(string sREQID)
    {
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
                        TRQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TRK.SCARTYPEID,TRQ.RESULT_CHECKING_DATE,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,TRQ.CHECKWATER_NVERSION,
                        TVD.SVENDORID,TVD.SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME,TRK.NWHEELS,TRQ.TOTLE_SLOT,TRK.DPREV_SERV,TRK.DLAST_SERV,TRKN.SCAR_NUM,INC.DATE_CREATED
                        FROM TBL_REQUEST TRQ 
                        LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = TRQ.STRUCKID
                        LEFT JOIN TTRUCK TRKN ON TRKN.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        LEFT JOIN (SELECT REQ_ID, MAX(NVL( DATE_UPDATED, DATE_CREATED)) as DATE_CREATED FROM TBL_INNER_CHECKINGS GROUP BY REQ_ID)INC
                        ON TRQ.REQUEST_ID = INC.REQ_ID
                        WHERE TRQ.REQUEST_ID = '{0}'";

        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(conn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        SetDataToPage();
    }
    private void SetDataToPage()
    {
        if (dtMainData.Rows.Count > 0)
        {
            DataRow dr = null;
            dr = dtMainData.Rows[0];
          
            //เซ็ตช่องแต่ละช่อง ในการแสดง
            string sTruckID = "";
            switch (dr["SCARTYPEID"] + "")
            {
                case "0": sTruckID = dr["VEH_ID"] + ""; break; // 10 ล้อ
                case "3": sTruckID = dr["TU_ID"] + ""; break; // หัวลาก
            }
            sTruckID_CheckWater = sTruckID;
        }
    }

    private void ListReport(string sReqID)
    {

        //ตรวจสอบจากคำขอวัดน้ำ
        string sql = @"SELECT * FROM TBL_REQSLOT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'";
        DataTable dt_SLOT = new DataTable();
        dt_SLOT = CommonFunction.Get_Data(conn, sql);
        if (dt_SLOT.Rows.Count > 0)
        {
            var query_SLOT = dt_SLOT.AsEnumerable().Select(s => new { SLOT_NO = s.Field<decimal>("SLOT_NO"), LEVEL_NO = s.Field<decimal>("LEVEL_NO"), CAPACITY = s.Field<decimal>("CAPACITY") }).ToList();
            decimal minslot = query_SLOT.OrderBy(o => o.SLOT_NO).FirstOrDefault().SLOT_NO;
            nSlotAll = query_SLOT.OrderByDescending(o => o.SLOT_NO).FirstOrDefault().SLOT_NO;
        }

        #region data table //
        //        string sql1 = @"SELECT TCC.REQUEST_ID,TCC.COMPART_NO, TIC.PAN_LEVEL,TRC.NCAPACITY,TIC.HEIGHT1,TIC.HEIGHT2,
        //                                CASE WHEN TCC.EMER_VALUE = '0' THEN 'ไม่มี' WHEN TCC.EMER_VALUE = '1' THEN 'มี' ELSE '-' END AS EMER_VALUE,
        //                                TCC.SEAL_NO,
        //                                CASE WHEN TCC.SEAL_ERR = '0' THEN 'ไม่ชำรุด'  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
        //                                CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
        //                                TCC.NEW_SEAL_NO
        //                                 FROM TBL_CHECKING_COMPART TCC
        //                                 INNER JOIN TBL_INNER_CHECKINGS TIC ON TCC.REQUEST_ID = TIC.REQ_ID AND TCC.COMPART_NO = TIC.COMPART_NO
        //                                 LEFT JOIN TTRUCK_COMPART TRC ON  TCC.STRUCKID  = TRC.STRUCKID AND  TIC.COMPART_NO = TRC.NCOMPARTNO AND TIC.PAN_LEVEL = TRC.NPANLEVEL
        //                                 WHERE REQUEST_ID = '{0}'
        //                                 ORDER BY TCC.COMPART_NO,TIC.PAN_LEVEL ASC";

        //        string sql1 = @"SELECT TCC.REQUEST_ID,TCC.COMPART_NO, TIC.PAN_LEVEL,NVL(TRC.NCAPACITY,REQ.CAPACITY) as NCAPACITY,TIC.HEIGHT1,TIC.HEIGHT2,
        //                                CASE WHEN TCC.EMER_VALUE = '0' THEN '' WHEN TCC.EMER_VALUE = '1' THEN '*' ELSE '-' END AS EMER_VALUE,
        //                                TCC.SEAL_NO,
        //                                CASE WHEN TCC.SEAL_ERR = '0' THEN 'ไม่ชำรุด'  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
        //                                CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
        //                                TCC.NEW_SEAL_NO
        //                                 FROM TBL_CHECKING_COMPART TCC
        //                                 INNER JOIN TBL_INNER_CHECKINGS TIC ON TCC.REQUEST_ID = TIC.REQ_ID AND TCC.COMPART_NO = TIC.COMPART_NO
        //                                 LEFT JOIN TTRUCK_COMPART TRC ON  TCC.STRUCKID  = TRC.STRUCKID AND  TIC.COMPART_NO = TRC.NCOMPARTNO AND TIC.PAN_LEVEL = TRC.NPANLEVEL
        //                                 LEFT JOIN TBL_REQSLOT REQ ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO   AND TIC.PAN_LEVEL = REQ.LEVEL_NO
        //                                 WHERE REQUEST_ID = '{0}'
        //                                 ORDER BY TCC.COMPART_NO,TIC.PAN_LEVEL ASC";

        string sql1 = @"SELECT REQ.REQUEST_ID,REQ.SLOT_NO as COMPART_NO, REQ.LEVEL_NO as PAN_LEVEL,REQ.CAPACITY as NCAPACITY  ,TIC.HEIGHT1,TIC.HEIGHT2,
CASE WHEN TCC.EMER_VALUE = '0' THEN '' WHEN TCC.EMER_VALUE = '1' THEN '*' ELSE '-' END AS EMER_VALUE,
TCC.SEAL_NO,
CASE WHEN TCC.SEAL_ERR = '0' THEN 'ไม่ชำรุด'  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
TCC.NEW_SEAL_NO
FROM TBL_REQSLOT REQ
LEFT JOIN  TBL_CHECKING_COMPART TCC  ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO  -- AND TCC. = REQ.LEVEL_NO
LEFT JOIN TBL_INNER_CHECKINGS TIC ON REQ.REQUEST_ID = TIC.REQ_ID AND REQ.SLOT_NO = TIC.COMPART_NO AND TIC.PAN_LEVEL = REQ.LEVEL_NO
 WHERE REQ.REQUEST_ID = '{0}'
ORDER BY REQ.SLOT_NO,REQ.LEVEL_NO  ASC";


        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, string.Format(sql1, CommonFunction.ReplaceInjection(sReqID)));



        var query = dt.AsEnumerable().Select(s => new 
        { COMPART_NO = s.Field<decimal>("COMPART_NO"),
            PAN_LEVEL = s.Field<decimal>("PAN_LEVEL"),
            NCAPACITY = s["NCAPACITY"] != DBNull.Value ? s.Field<decimal>("NCAPACITY") : 0//s.Field<decimal>("NCAPACITY") 
        }).ToList();
        decimal nSum_Capacity = 0;
        if (query.Count > 0)
        {
            var query2 = query.GroupBy(g => new { g.COMPART_NO }).Select(s => new { s.Key.COMPART_NO, NCAPACITY = s.Max(x => x.NCAPACITY) }).ToList();
            nSum_Capacity = query2.Sum(s => s.NCAPACITY);
        }

        //if (dt.Rows.Count > 0)
        //{

        //}
        //else
        //{
        //    DataRow dr = null;
        //    for (int i = dt.Rows.Count; i <= 18; i++)
        //    {
        //        dr = dt.NewRow();
        //        dr[0] = ""; // or you could generate some random string.
        //        dt.Rows.Add(dr);
        //    }
        //}
        #endregion

        rpt_InnerChecking_FM001 report = new rpt_InnerChecking_FM001();

        // parameter
        //SENGINE หมายเลขเคื่อง


        // set control
        ((XRLabel)report.FindControl("xrlblsDay", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("rxlblsYear", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));

        string[] arrNo1 = sREQUESTID.Split('-');
        ((XRLabel)report.FindControl("xrlblNo1", true)).Text = arrNo1[arrNo1.Length - 1];
        ((XRLabel)report.FindControl("xrlblNo2", true)).Text = DateTime.Now.ToString("yy", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrLabel44", true)).Text = sREQUESTID;

        ((XRLabel)report.FindControl("xrLabel55", true)).Text = "Rev.2 (" + DateTime.Now.ToString("dd/MM/yy", new CultureInfo("th-TH")) + ")";
        ((XRLabel)report.FindControl("xrLabel56", true)).Text = "FM-มว.-001";

        //
        string sql2 = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TRQ.VEH_ID,TRQ.TU_ID,
TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS
 FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
 LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
 WHERE TRQ.REQUEST_ID = '{0}'";

        DataTable dtPara1 = new DataTable();
        dtPara1 = CommonFunction.Get_Data(conn, string.Format(sql2, sReqID));

        //sTruckID_CheckWater เก็บ id รถที่ใช้วัดน้ำ
        string sql3 = @"SELECT TRK.*,TRKT.SCARTYPENAME FROM TTRUCK  TRK LEFT JOIN TTRUCKTYPE TRKT ON  TRK.CARCATE_ID = TRKT.SCARTYPEID WHERE  TRK.STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";
        DataTable dtTruck = new DataTable();
        dtTruck = CommonFunction.Get_Data(conn, sql3);

        //เวลา
        string sql4 = @"SELECT * FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NID";
        DataTable dtTime = new DataTable();
        dtTime = CommonFunction.Get_Data(conn, sql4);
        decimal nTemp = 0;

        string vStartH = "", vStartM = "", vEndH = "", vEndM = "";




        if (dtPara1.Rows.Count == 1 && dtTruck.Rows.Count == 1)
        {
            DataRow dr1 = null, dr2 = null, dr3 = null;

            dr1 = dtPara1.Rows[0];
            dr2 = dtTruck.Rows[0];
            dr3 = dtMainData.Rows[0];

            report.Parameters["sCompName"].Value = dr1["SABBREVIATION"] + "";
            report.Parameters["sTU_NO"].Value = dr1["TU_NO"] + "" != "" ? dr1["TU_NO"] + "" : "-";
            report.Parameters["sTruckID"].Value = sTruckID_CheckWater;
            report.Parameters["sSerialNumber"].Value = dr2["SENGINE"] + "" != "" ? dr2["SENGINE"] + "" : "-";
            report.Parameters["sTU_CHASSIS"].Value = dr1["TU_CHASSIS"] + "" != "" ? dr1["TU_CHASSIS"] + "" : "-";
            report.Parameters["sCarType"].Value = dr2["SCARTYPENAME"] + "";
            report.Parameters["sVEH_NO"].Value = dr1["VEH_NO"] + "" != "" ? dr1["VEH_NO"] + "" : "-";
            report.Parameters["sBRAND"].Value = dr2["SBRAND"] + "" != "" ? dr2["SBRAND"] + "" : "-";
            report.Parameters["sVEH_CHASSIS"].Value = dr1["VEH_CHASSIS"] + "" != "" ? dr1["VEH_CHASSIS"] + "" : "-";
            report.Parameters["nNumSlot"].Value = nSlotAll + "";
            nTemp = decimal.TryParse(dr2["NTOTALCAPACITY"] + "", out nTemp) ? nTemp : 0;
            report.Parameters["nSum_Capacity"].Value = dr2["NTOTALCAPACITY"] + "" != "" ? (nTemp > 0 ? nTemp.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "-";
            report.Parameters["nWeight_Sum"].Value = dr2["NWEIGHT"] + "" != "" ? dr2["NWEIGHT"] + "" : "-";
            report.Parameters["sNB"].Value = dr2["NLOAD_WEIGHT"] + "" != "" ? dr2["NLOAD_WEIGHT"] + "" : "-";
            report.Parameters["dLastCheckWater"].Value = dr2["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DLAST_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";   // dr1["TU_NO"] + "";
            report.Parameters["sHeight_H2T"].Value = dr2["NTANK_HIGH_HEAD"] + "";
            report.Parameters["sHeight_EndT"].Value = dr2["NTANK_HIGH_TAIL"] + "";
            report.Parameters["sTypeMaterailTank"].Value = dr2["STANK_MATERAIL"] + "" != "" ? dr2["STANK_MATERAIL"] + "" : "-";
            report.Parameters["sAddress"].Value = dr1["SNO"] + "  " + dr1["SDISTRICT"] + "  " + dr1["SREGION"] + "  จังหวัด " + dr1["SPROVINCE"] + "  " + dr1["SPROVINCECODE"] + "";
            report.Parameters["dService"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";


            string sShow = "";
            decimal nTempH = 0, nTempM = 0;
            foreach (DataRow row in dtTime.Rows)
            {
                nTempH = 0; nTempM = 0;
                nTempH = decimal.TryParse(row["NSTART_HOUR"] + "", out nTempH) ? nTempH : 0;
                nTempM = decimal.TryParse(row["NSTART_MINUTE"] + "", out nTempM) ? nTempM : 0;
                if (nTempH < 10)
                {
                    sShow = "0" + nTempH + ":";
                }
                else
                {
                    sShow = nTempH + ":";
                }

                if (nTempM < 10)
                {
                    sShow = sShow + "0" + nTempM;
                }
                else
                {
                    sShow = sShow + nTempM;
                }


                switch (row["NID"] + "")
                {
                    case "1": //เวลานำรถเข้าตรวจ
                        report.Parameters["sTime_dService"].Value = sShow;
                        break;
                    case "2"://เวลานำรถเข้าตรวจสภาพภายนอก/ใน
                        break;
                    case "3": //เวลาเริ่มต้นลงน้ำ
                        report.Parameters["sTime_DownWater"].Value = sShow;
                        vStartH = row["NSTART_HOUR"] + "";
                        vStartM = row["NSTART_MINUTE"] + "";
                        break;
                    case "4": //หยุดน้ำ/วัดแป้น
                        report.Parameters["sTime_StopCheckWater"].Value = sShow;
                        break;
                    case "5": //สูบน้ำคืนกลับ
                        report.Parameters["sTime_ReturnWater"].Value = sShow;
                        vEndH = row["NSTART_HOUR"] + "";
                        vEndM = row["NSTART_MINUTE"] + "";
                        break;
                }
            }

            string st1 = vStartH + ":" + vStartM;
            string st2 = vEndH + ":" + vEndM;
            int hours = 0, minutes = 0;

            if (!string.IsNullOrEmpty(vStartH) && !string.IsNullOrEmpty(vStartM) && !string.IsNullOrEmpty(vEndH) && !string.IsNullOrEmpty(vEndM))
            {
                TimeSpan timeStart = TimeSpan.Parse(st1);
                TimeSpan timeEnd = TimeSpan.Parse(st2);

                TimeSpan difference = timeEnd - timeStart;

                hours = difference.Hours;
                minutes = difference.Minutes;
            }

            report.Parameters["nSumTimeChecking"].Value = hours + "." + minutes;

            string sDateNextCheckWater = "";
            DateTime dNextCheckWater;
            if (!string.IsNullOrEmpty(dr2["DNEXT_SERV"] + ""))
            {
                dNextCheckWater = Convert.ToDateTime(dr2["DNEXT_SERV"] + "", new CultureInfo("th-TH"));
                if (dNextCheckWater.Year == DateTime.Now.Year) // ยังไม่ได้อัพเดทข้อมูลวัดน้ำใน TTRUCK
                {
                    sDateNextCheckWater = dNextCheckWater.AddYears(nNextYeatCheckWater).ToString("dd MMM yyyy");
                }
                else
                {
                    sDateNextCheckWater = dNextCheckWater.ToString("dd MMM yyyy");
                }
            }
            else
            {
                sDateNextCheckWater = DateTime.Now.ToString("dd MMM yyyy");
            }

            report.Parameters["dNextCheckWater"].Value = sDateNextCheckWater;
            report.Parameters["dCompletCheckWater"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            report.Parameters["nSumCapacity"].Value = nSum_Capacity.ToString(SystemFunction.CheckFormatNuberic(0));

        }


        string sUSER = @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME,SUID.SPOSITION FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'  AND ISACTIVE_FLAG = 'Y'";
        DataTable dtUSER = CommonFunction.Get_Data(conn, sUSER);
        if (dtUSER.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrlblFullNameControl", true)).Text = dtUSER.Rows[0]["SNAME"] + "";
            ((XRLabel)report.FindControl("xrblbPositionControl", true)).Text = dtUSER.Rows[0]["SPOSITION"] + "";
        }


        ((XRLabel)report.FindControl("xrlblsDay2", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrlblsMonth2", true)).Text = DateTime.Now.ToString("MMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblsYear2", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblComment", true)).Text = GetComment(sReqID);


        report.Name = "รายงานวัดน้ำ";
        report.DataSource = dt;
        string fileName = "รายงานวัดน้ำ_FM-มว.-001_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private void PrintDoc(string REQ_ID)
    {
        #region Prepare SOURCE

        string _sql = "";

        _sql = @"SELECT REQ.REQUEST_ID,VEN.SABBREVIATION,TRU.SHEADREGISTERNO As REG_NO,TRU.SCERT_NO As SCERT_NO,TRU.SENGINE,TRU.SCHASIS,TYP.SCARCATEGORY
    ,CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.VEH_NO END As HTRAILER
    ,(CASE WHEN TRU.SCARTYPEID IN ('3','4') THEN (SELECT SBRAND FROM TTRUCK WHERE STRUCKID=REQ.STRUCKID AND ROWNUM=1) ELSE TRU.SBRAND END) As SBRAND
    ,CASE WHEN NVL(REQ.TU_CHASSIS,'')<>'' THEN REQ.VEH_CHASSIS END As VEH_CHASSIS
    ,REQ.TOTLE_SLOT,REQ.TOTLE_CAP,TRU.NWEIGHT,TRU.NLOAD_WEIGHT,TRU.DLAST_SERV,TRU.NTANK_HIGH_HEAD,TRU.NTANK_HIGH_TAIL,TRU.STANK_MATERAIL
    ,(SELECT SUM(NCAPACITY) FROM TTRUCK_COMPART WHERE STRUCKID=TRU.STRUCKID) As TCAPACITY
    FROM TBL_REQUEST REQ 
    LEFT JOIN TTRUCK TRU ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=TRU.STRUCKID
    LEFT JOIN TTRUCKTYPE TYP ON TRU.SCARTYPEID=TYP.SCARTYPEID
    LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID
    WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'";

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        dt.TableName = "dtReport";

        _sql = @"SELECT REQ.REQUEST_ID,COMP.NCOMPARTNO,COMP.NPANLEVEL,COMP.NCAPACITY FROM TBL_REQUEST REQ
                     LEFT JOIN TTRUCK_COMPART COMP ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=COMP.STRUCKID
                     WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'
                     ORDER BY COMP.NCOMPARTNO,COMP.NPANLEVEL";
        DataTable dt2 = CommonFunction.Get_Data(conn, _sql);
        dt2.TableName = "dtCompart";

        dsReportRequesting ds = new dsReportRequesting();
        ds.dtReport.Merge(dt);
        ds.dtCompart.Merge(dt2);

        #endregion

        xrtReportRequesting report = new xrtReportRequesting();
        report.DataSource = ds;
        report.Parameters["sDate"].Value = DateTime.Now.Date;
        report.Parameters["Auditor"].Value = "(นายอำนาจ  บุญเรือง)";
        report.Parameters["Position"].Value = "ช่างเทคนิค";

        string fileName = "FM-มว.-001_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    private string Checknull(string Data)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Data))
        {
            Result = Data;
        }
        else
        {
            Result = " - ";
        }


        return Result;
    }
    private string ChecknullDate(string Data)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Data))
        {
            Result = DateTime.Parse(Data).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
        }
        else
        {
            Result = " - ";
        }


        return Result;
    }
    private string CheckNum(string Data)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(Data))
        {
            if (Data != "0")
            {
                decimal num = decimal.Parse(Data);
                Result = num.ToString("#,###,###,###,###,###") + " บาท";
            }
            else
            {
                Result = "0 บาท";
            }
        }
        else
        {
            Result = " - ";
        }

        return Result;
    }
    private string GetComment(string sReqID)
    {
        string sResult = "";
        //คอมเม้น/บันทึก
        string sql5 = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dtComment = new DataTable();
        dtComment = CommonFunction.Get_Data(conn, sql5);

        DataRow dr4 = dtComment.Rows.Count > 0 ? dtComment.Rows[0] : null;

        sResult = dr4 != null ? dr4["SCOMMENT"] + "" : "";

        return sResult;
    }
}