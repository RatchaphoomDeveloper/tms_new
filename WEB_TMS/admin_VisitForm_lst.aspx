﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="admin_VisitForm_lst.aspx.cs" Inherits="admin_VisitForm_lst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align:inherit !important;
            align-content:inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="uplMain" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnAdd" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
            <asp:PostBackTrigger ControlID="btnSaveCopy" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top:20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div class="form-group form-horizontal row">
                                <label for="txtSearchFormName" class="col-md-2 control-label">ชื่อแบบฟอร์ม</label>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtSearchFormName" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                </div>
                                <label class="col-md-2 control-label">ช่วงเวลาแก้ไข</label>
                                <div class="col-md-5">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtDateStart" runat="server" CssClass="datepicker"
                                                    Style="text-align: center"></asp:TextBox>
                                            </td>
                                            <td>
                                                <label class="control-label">&nbsp;ถึง&nbsp;</label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="datepicker"
                                                    Style="text-align: center"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="rdoStatus" class="col-md-2 control-label">สถานะการใช้งาน</label>
                                <div class="col-md-3 radio">
                                    <asp:RadioButtonList ID="rdoStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                        <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                        <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-5">
                                    <asp:Button ID="btnAdd" runat="server" Text="เพิ่มแบบฟอร์ม" CssClass="btn btn-hover btn-info" OnClick="btnAdd_Click" />
                                    <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" CssClass="btn btn-hover btn-info" OnClick="btnSearch_Click" />
                                    <asp:Button ID="btnClear" runat="server" Text="เคลียร์" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ข้อมูลแบบฟอร์ม
                </div>
                <div class="panel-body">
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnPageIndexChanging="grvMain_PageIndexChanging"
                        HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" DataKeyNames="NTYPEVISITFORMID,CACTIVE"
                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อแบบฟอร์ม" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="txtSTYPEVISITFORMNAME" runat="server" Text='<%# Eval("STYPEVISITFORMNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จำนวนหมวด" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="txtNG" runat="server" Text='<%# Eval("NG") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จำนวนคำถาม" ItemStyle-Width="100px">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="txtNL" runat="server" Text='<%# Eval("NL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkActive" runat="server" Checked="false" Enabled="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-Width="100px" HeaderStyle-Width="100px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="dateC" runat="server" Text='<%# Eval("DCREATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การแก้ไขล่าสุด" ItemStyle-Width="120px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="dateU" runat="server" Text='<%# Eval("DUPDATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เรียกดูข้อมูล" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/btnSearch.png" Width="16px" Height="16px" Style="cursor: pointer" CommandName="View" CausesValidation="False" BorderWidth="0" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="แก้ไขข้อมูล" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageEdit" runat="server" ImageUrl="~/Images/document.gif" Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CommandName="RowSelect" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="คัดลอก" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <img src='<%=ResolveUrl("~/Images/copy.gif" ) %>' alt="คัดลอก" style="cursor: pointer; width:16px; height:16px" onclick="CopyForm('<%# Eval("STYPEVISITFORMNAME").ToString().Replace("'", "\'") + "\',\'" + Eval("NTYPEVISITFORMID").ToString()%>')"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
            <div class="modal fade" style="z-index: 1060" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" id="alertCopy">
                <div class="modal-dialog">
                    <div class="modal-content" style="text-align: left;">
                        <div class="modal-header">
                            <h4 class="modal-title">แจ้งเตือน</h4>
                        </div>
                        <div class="modal-body" style="word-wrap: break-word;">
                            <table style="width: 100%">
                                <tr>
                                    <td valign="top" align="left" style="font-size: 16px; vertical-align: central;" class="TextDetail">
                                        <div style="height: 40px; text-align: center; display: table-cell; vertical-align: middle;">
                                            <asp:Label ID="lblComfirmText" runat="server" Text="[ข้อความ]"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Button runat="server" ID="btnSaveCopy" Text="คัดลอกฟอร์ม" CssClass="btn btn-md bth-hover btn-info" OnClientClick="clickSave()"/>
                            <asp:Button runat="server" ID="btnNotSave" Text="ยกเลิก" CssClass="btn btn-md bth-hover btn-warning" OnClick="btnNotSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
     <div style="display: none;">
        <input type="text" name="hdnNum" id="hdnNum" runat="server" value="0"> 
         <asp:Button runat="server" ID="btnCopyForm" Text="" CssClass="btn btn-md bth-hover btn-info" OnClick="btnSaveCopy_Click" />
    </div>
    <script type="text/javascript">
        function CopyForm(formName, num) {
            $('#<%=lblComfirmText.ClientID%>').text(" คัดลอกข้อมูลฟอร์ม: " + formName + "?");
            $('#<%=hdnNum.ClientID%>').attr('value', num);
            AlertCopy();
        }
        function clickSave() {
            $("#<%=btnCopyForm.ClientID%>").click();
        }
        function AlertCopy() {
            $('#alertCopy').find('input').attr('data-dismiss', 'modal');
            var $dialog = $('#alertCopy').find(".modal-dialog");
            var offset = (($(window).height() - $dialog.height()) / 2 - $dialog.height() / 2);
            if ($dialog.height() < 0) {
                offset = $(window).height() / 2 - 200;
            }

            offset = $(window).height() / 2 - 200;
            $('#alertCopy').find('.modal-dialog').css('margin-top', offset);
            $('#alertCopy').modal('show');
        }

        $(document).ready(function () {
            // Stop user to press enter in textbox
            preventEnter();
        });

        function preventEnter() {
            $("input:text").keypress(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            preventEnter();
        });
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

