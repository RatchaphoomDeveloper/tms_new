﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Web.Configuration;

public partial class calculate : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetValueNotPostBack();


            string sql = "SELECT SERVICECHARGE FROM TBL_SERVICECOST WHERE SERVICE_ID ='00002'";
            DataTable dt = CommonFunction.Get_Data(conn, sql);
            txtpan.Text = dt.Rows[0]["SERVICECHARGE"] + "";

        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string[] param = e.Parameter.Split(';');

            string sType = cboReqType.Value + "";
            string Case = cboCause.Value + "";
            switch (param[0])
            {

                case "ChangeReqType":
                    //GetForm("", "");
                    SetcboCause(sType, "", "Y");
                    SetTotalFromCallBack(sType, Case);
                    break;

                case "ChangeCause":
                    GetForm(sType, Case);
                    SetcboCause(sType, "", "N");
                    SetTotalFromCallBack(sType, Case);
                    break;

            }

        }
    }

    private void SetcboCause(string sType, string Case, string Bind)
    {

        string REQTYPE_ID = "";
        switch (sType)
        {
            case "01":
                REQTYPE_ID = "01";
                cboReqType.Width = Unit.Percentage(30);
                cboCause.Width = Unit.Percentage(50);
                lblMaihate.Text = "ค่าใช้จ่าย";
                trCause.Visible = true;
                break;
            case "02":
                REQTYPE_ID = "02";
                cboReqType.Width = Unit.Percentage(70);
                cboCause.Width = Unit.Percentage(95);
                lblMaihate.Text = "ค่ารับรอง/ตีซีล";
                trCause.Visible = true;
                break;
            case "03":
                lblMaihate.Text = "ค่าเอกสารต่อชุด";
                trCause.Visible = false;
                break;
            case "04":
                lblMaihate.Text = "ค่าพ่นรหัสสาระส้าคัญใหม่";
                trCause.Visible = false;
                break;
        }

        string strsql = @"SELECT CAUSE_ID, REQTYPE_ID, CAUSE_LEVEL, CAUSE_NAME, ISACTIVE_FLAG FROM TBL_CAUSE WHERE ISACTIVE_FLAG ='Y' AND REQTYPE_ID = '" + CommonFunction.ReplaceInjection(REQTYPE_ID) + "'";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);
        cboCause.Items.Clear();
        cboCause.DataSource = dt;
        cboCause.DataBind();
        cboCause.Items.Insert(0, new ListEditItem("- ระบุสาเหตุ -", ""));
        if (Bind == "Y")
        {
            cboCause.SelectedIndex = 0;
        }
    }

    private void GetForm(string sType, string Case)
    {
        tracc.Style.Add("display", "none");
        trCarspecies.Style.Add("display", "none");
        trCartype.Style.Add("display", "none");
        trCapacity.Style.Add("display", "none");
        trSetpan.Style.Add("display", "none");
        txtSetpan.Text = "";
        chbSetpan.Checked = false;

      

        switch (sType)
        {
            case "01":
                switch (Case)
                {
                    case "00001":
                        trCarspecies.Style.Add("display", "");
                        trCartype.Style.Add("display", "");
                        trCapacity.Style.Add("display", "");
                        trSetpan.Style.Add("display", "");
                        break;

                    case "00002":
                        tracc.Style.Add("display", "");
                        break;

                    case "00003":
                        trCartype.Style.Add("display", "");
                        trCapacity.Style.Add("display", "");
                        trSetpan.Style.Add("display", "");
                        break;

                    case "00004":
                        trCartype.Style.Add("display", "");
                        trCapacity.Style.Add("display", "");
                        trSetpan.Style.Add("display", "");
                        break;

                    case "00005":
                        trCartype.Style.Add("display", "");
                        trCapacity.Style.Add("display", "");
                        trSetpan.Style.Add("display", "");
                        break;
                    case "00011":
                        trCartype.Style.Add("display", "");
                        trCapacity.Style.Add("display", "");
                        trSetpan.Style.Add("display", "");
                        break;
                }
                
                break;
            case "02":
                //lblTotal.Text = dt.Select(" SERVICE_ID = '00003'")[0]["SERVICECHARGE"] + "";
                break;
            case "03":
                //lblTotal.Text = dt.Select(" SERVICE_ID = '00005'")[0]["SERVICECHARGE"] + "";
                break;
            case "04":
                //lblTotal.Text = dt.Select(" SERVICE_ID = '00004'")[0]["SERVICECHARGE"] + "";
                break;
        }
    }

    private void SetValueNotPostBack()
    {
        rblCapacity.SelectedIndex = 0;
        rblCarspecies.SelectedIndex = 0;
        rblCartype.SelectedIndex = 0;
        cboReqType.DataBind();
        cboReqType.Items.Insert(0, new ListEditItem("- ระบุประเภทคำขอ -", ""));
        cboCause.DataBind();
        cboCause.Items.Insert(0, new ListEditItem("- ระบุประเภทคำขอ -", ""));
        lblTotal.Text = " - ";
        txtSetpan.ClientEnabled = false;

        tracc.Style.Add("display", "none");
        trCarspecies.Style.Add("display", "none");
        trCartype.Style.Add("display", "none");
        trCapacity.Style.Add("display", "none");
        trSetpan.Style.Add("display", "none");
        lblMaihate.Text = "ค่าใช้จ่าย";
    }

    private void SetTotalFromCallBack(string sType, string Case)
    {
        string sql = "";
        if (sType != "01")
        {
            sql = @"SELECT SERVICE_ID, REQTYPE_ID, CAUSE_ID, SERVICECHARGE, UNIT,  DESCRIPTION, CAUSE_ISALL, ISACTIVE_FLAG
                       FROM TBL_SERVICECOST
                       WHERE REQTYPE_ID = '" + CommonFunction.ReplaceInjection(sType) + "' AND ISACTIVE_FLAG = 'Y'";
        }
        else
        {
            sql = @"SELECT  NROWADD||CAR_GRADE||CARCATE_ID as sID,
                    SERVICE_ID, REQTYPE_ID,START_CAPACITY, END_CAPACITY, CARCATE_ID,CAR_GRADE, SERVICECHARGE,CAUSE_ISALL,NROWADD
                    FROM TBL_SERVICECOST
                    WHERE ISACTIVE_FLAG = 'Y' AND SERVICE_ID = '00001'
                    ORDER BY NROWADD,CAR_GRADE,CARCATE_ID ASC";
        }

        DataTable dt = CommonFunction.Get_Data(conn, sql);
        if (dt.Rows.Count > 0)
        {
            switch (sType)
            {
                case "01":
                    gvw.DataSource = dt;
                    gvw.DataBind();
                    txtgvwrow.Text = dt.Rows.Count + "";
                    switch (Case)
                    {
                        case "00001":

                            break;

                        case "00002":

                            break;

                        case "00003":

                            break;

                        case "00004":

                            break;

                        case "00005":

                            break;
                    }

                    break;
                case "02":
                    if (!string.IsNullOrEmpty(Case))
                    {
                        lblTotal.Text = dt.Select(" SERVICE_ID = '00003'")[0]["SERVICECHARGE"] + "";
                    }
                    break;
                case "03":
                    lblTotal.Text = dt.Select(" SERVICE_ID = '00005'")[0]["SERVICECHARGE"] + "";
                    break;
                case "04":
                    lblTotal.Text = dt.Select(" SERVICE_ID = '00004'")[0]["SERVICECHARGE"] + "";
                    break;
            }
        }
    }
}