﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="service_add.aspx.cs" Inherits="service_add" StylesheetTheme="Aqua" %>

<%@ Register Src="DocumentCharges.ascx" TagName="UserControl" TagPrefix="myUsr" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript" language="javascript">
        function IsValidNumber() {
            //ตรวจเช็ค keyCode
            if ((event.keyCode >= 48) && (event.keyCode <= 57))
            { }
            else
            { event.returnValue = false; } //หรือ { event.keyCode = 0 ; }
        }
       
        function hidcontrol() {


            if (cboCause.GetValue() == '00010') {
                $("#cph_Main_xcpn_trOhtercause").show();
            }
            else {
                $("#cph_Main_xcpn_trOhtercause").hide();
                txtRemarkCause.SetText('');
            }


        }

        function hidTR() {


            if (cboRequestType.GetValue() == '03') {
                $("#cph_Main_xcpn_trCause").hide();
            }
            else if (cboRequestType.GetValue() == '04') {
                $("#cph_Main_xcpn_trCause").hide();
            }
            else {
                $("#cph_Main_xcpn_trCause").show();
            }


        }

        
    </script>
    <style type="text/css">
        .dxeHLC
        {
            display: none;
        }
        .dxeLTM
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ hidcontrol();hidTR(); eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td height="35" colspan="3" align="right">
                            <myUsr:UserControl ID="myUserControl" runat="server"></myUsr:UserControl>
                        </td>
                    </tr>
                </table>
                <table runat="server" width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td class="td1" width="25%" bgcolor="#B9EAEF" align="center">ประเภทคำขอ </td>
                        <td width="50%">
                            <dx:ASPxComboBox ID="cboRequestType" runat="server" Width="75%" ClientInstanceName="cboRequestType">
                                <ClientSideEvents SelectedIndexChanged="function(e,s) {xcpn.PerformCallback('ChangeReq');}">
                                </ClientSideEvents>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsReqType" runat="server" SelectCommand="
                            SELECT   '00' as ID ,'- ระบุประเภทคำขอ -' as NAME ,0 NORDER  FROM DUAL   
                            UNION
                            (
                                SELECT 
                                    REQTYPE_ID as ID, REQTYPE_NAME as NAME ,NORDER
                                FROM TBL_REQTYPE
                                Where
                                ISACTIVE_FLAG = 'Y' 
                                
                            )
                            ORDER BY NORDER ASC " ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td rowspan="6" width="25%" valign="top">
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="center" bgcolor="#B9EAEF">ค่าใช้จ่าย </td>
                                </tr>
                                <tr valign="top">
                                    <td height="35" align="center" valign="middle" bgcolor="#FFFFCC"><strong class="active">
                                        <b>
                                            <dx:ASPxLabel ID="lblTotal" runat="server" Text="" CssClass="active">
                                            </dx:ASPxLabel>
                                        </b></strong>บาท </td>
                                </tr>
                                <tr valign="top">
                                    <td align="center" valign="middle" bgcolor="#FFFFCC">*หมายเหตุ เป็นค่าบริการประมาณการ
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td height="35" align="center" valign="middle" bgcolor="#FFFFCC"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trCause" runat="server">
                        <td class="td1" align="center" bgcolor="#B9EAEF">สาเหตุ </td>
                        <td>
                            <dx:ASPxComboBox ID="cboCause" runat="server" Width="100%" ClientInstanceName="cboCause">
                                <ClientSideEvents ValueChanged="function(){hidcontrol();}" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="- ระบุสาเหตุ -"
                                    RequiredField-IsRequired="true" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="- ระบุสาเหตุ -"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr id="trOhtercause" style="display: none;">
                        <td align="center" bgcolor="#B9EAEF">หมายเหตุกรณีอื่นๆ </td>
                        <td>
                            <dx:ASPxTextBox ID="txtRemarkCause" runat="server" ClientInstanceName="txtRemarkCause"
                                Width="90%" ToolTip="ระบุสาเหตุ">
                                <ValidationSettings RequiredField-ErrorText="ระบุหมายเหตุกรณีสาเหตุอื่นๆ" RequiredField-IsRequired="true"
                                    ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="ระบุหมายเหตุกรณีสาเหตุอื่นๆ"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">บริษัทขนส่ง </td>
                        <td>
                            <dx:ASPxLabel ID="lblVendorName" runat="server" Text="ASPxLabel">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">เลือกรถ </td>
                        <td>
                            <dx:ASPxComboBox ID="cboCarregis" runat="server" CallbackPageSize="30" ClientInstanceName="cboCarregis"
                                ItemStyle-Wrap="True" EnableCallbackMode="True" OnItemRequestedByValue="cboCarregis_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboCarregis_ItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} - {1}{3}" ValueField="STRUCKID" Width="400px"
                                CssClass="dxeLineBreakFix" Height="25">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="STRUCKID" Width="100px" Caption="รหัส"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="head" Width="150px" Caption="ทะเบียนหัว"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="tail" Width="150px" Caption="ทะเบียนหาง"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="tail1" Width="0px"></dx:ListBoxColumn>
                                </Columns>
                                <%--<ItemStyle Wrap="True"></ItemStyle>--%>
                                <ClientSideEvents ValueChanged="function(s,e){ xcpn.PerformCallback('ListData'); }" />
                                <ItemStyle Wrap="True"></ItemStyle>
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic" RequiredField-IsRequired="true"
                                    RequiredField-ErrorText="ระบุทะเบียรถ" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsCarregis" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CacheKeyDependency="CacheCarregis" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">ประเภทรถ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblTruckType" Text="">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">วันหมดอายุวัดน้ำ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblWaterExpire" Text="">
                            </dx:ASPxLabel>
                             <dx:ASPxTextBox runat="server" ID="txtWaterExpire" Text="" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr id="trHead" runat="server">
                        <td colspan="3"><img src="images/btnQuest1.gif" width="23" height="23" alt="" /> โปรดระบุข้อมูลสำคัญตามที่แบบฟอร์ม
                            &quot;<span class="corp">ตรวจสอบวัดน้ำ</span>&quot;
                            <asp:SqlDataSource ID="sdsCapacity" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" id="tbPan1" runat="server">
                    <tr>
                        <td height="25" colspan="10" bgcolor="#E4E4E4">ความจุที่ระดับแป้น 1(ลิตร) </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 1 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 2 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 3 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 4 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 5 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 6 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 7 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 8 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 9 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 10 </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity11" runat="server" Width="80px" ClientInstanceName="cboCapacity11"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity12" runat="server" Width="80px" ClientInstanceName="cboCapacity12"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity13" runat="server" Width="80px" ClientInstanceName="cboCapacity13"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity14" runat="server" Width="80px" ClientInstanceName="cboCapacity14"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity15" runat="server" Width="80px" ClientInstanceName="cboCapacity15"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity16" runat="server" Width="80px" ClientInstanceName="cboCapacity16"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity17" runat="server" Width="80px" ClientInstanceName="cboCapacity17"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity18" runat="server" Width="80px" ClientInstanceName="cboCapacity18"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity19" runat="server" Width="80px" ClientInstanceName="cboCapacity19"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity110" runat="server" Width="80px" ClientInstanceName="cboCapacity110"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" id="tbPan2" runat="server">
                    <tr>
                        <td height="25" colspan="10" bgcolor="#E4E4E4">ความจุที่ระดับแป้น 2(ลิตร) </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 1 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 2 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 3 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 4 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 5 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 6 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 7 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 8 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 9 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 10 </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity21" runat="server" Width="80px" ClientInstanceName="cboCapacity21"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity22" runat="server" Width="80px" ClientInstanceName="cboCapacity22"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity23" runat="server" Width="80px" ClientInstanceName="cboCapacity23"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity24" runat="server" Width="80px" ClientInstanceName="cboCapacity24"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity25" runat="server" Width="80px" ClientInstanceName="cboCapacity25"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity26" runat="server" Width="80px" ClientInstanceName="cboCapacity26"
                                CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity27" runat="server" Width="80px" ClientInstanceName="cboCapacity27"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity28" runat="server" Width="80px" ClientInstanceName="cboCapacity28"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity29" runat="server" Width="80px" ClientInstanceName="cboCapacity29"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCapacity210" runat="server" Width="80px" ClientInstanceName="cboCapacity210"
                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" ClientEnabled="false" HorizontalAlign="Right">
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" id="tbPan3" runat="server"
                    visible="false">
                    <tr>
                        <td height="25" colspan="10" bgcolor="#E4E4E4">ความจุที่ระดับแป้น 3(ลิตร) </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 1 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 2 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 3 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 4 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 5 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 6 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 7 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 8 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 9 </td>
                        <td align="center" bgcolor="#B9EAEF">ช่อง 10 </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity31" runat="server" Text="" ClientInstanceName="lblCapacity31">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity32" runat="server" Text="" ClientInstanceName="lblCapacity32">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity33" runat="server" Text="" ClientInstanceName="lblCapacity33">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity34" runat="server" Text="" ClientInstanceName="lblCapacity34">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity35" runat="server" Text="" ClientInstanceName="lblCapacity35">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity36" runat="server" Text="" ClientInstanceName="lblCapacity36">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity37" runat="server" Text="" ClientInstanceName="lblCapacity37">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity38" runat="server" Text="" ClientInstanceName="lblCapacity38">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity39" runat="server" Text="" ClientInstanceName="lblCapacity39">
                            </dx:ASPxLabel>
                        </td>
                        <td align="right">
                            <dx:ASPxLabel ID="lblCapacity310" runat="server" Text="" ClientInstanceName="lblCapacity310">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="5" cellspacing="1" id="tbTotal" runat="server">
                    <tr>
                        <td colspan="10" align="right"><span class="corp">จำนวนช่อง:
                            <dx:ASPxLabel ID="lblCompartTotal" runat="server" Text="" Font-Bold="True">
                            </dx:ASPxLabel>
                            ช่อง ความจุรวม:
                            <dx:ASPxLabel ID="lblCapacityTotal" runat="server" Text="" Font-Bold="True">
                            </dx:ASPxLabel>
                            ลิตร</span> </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>&nbsp; &nbsp; </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ASPxButton ID="btnSave" ClientInstanceName="btnSave" runat="server" SkinID="_submitandprint"
                                CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('saveNewReq');}" />
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="btnBack" ClientInstanceName="btnBack" runat="server" SkinID="_back"
                                CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('back');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
