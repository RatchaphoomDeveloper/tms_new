﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="approve.aspx.cs" Inherits="approve" StylesheetTheme="Aqua" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPanel" Assembly="DevExpress.Web.v11.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <dx:ASPxPageControl runat="server" ID="pct" Width="100%">
                    <TabPages>
                        <dx:TabPage Name="p1" Text="รอพิจารณา">
                            <ContentCollection>
                                <dx:ContentControl>
                                    <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                                    <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                                    <table cellpadding="3" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxLabel runat="server" ID="lblsss">
                                                </dx:ASPxLabel>
                                                <dx:ASPxButton runat="server" ID="btnReq" Text="เพิ่มคำขอเฉพาะรถรับจ้าง" AutoPostBack="false">
                                                    <ClientSideEvents Click="function(){ xcpn.PerformCallback('newbill');}" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right" width="10%">
                                                            <dx:ASPxLabel runat="server" ID="lblSearch" Text="ค้นหา:" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxTextBox runat="server" ID="txtSearch" CssClass="dxeLineBreakFix" NullText="บริษัทขนส่ง,ทะเบียนรถ"
                                                                Width="100%">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxComboBox runat="server" ID="cboReatype" CssClass="dxeLineBreakFix" DataSourceID="sdsReqType"
                                                                TextField="REQTYPE_NAME" ValueField="REQTYPE_ID" Width="100%">
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsReqType" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT &quot;REQTYPE_ID&quot;, &quot;REQTYPE_NAME&quot;, &quot;ISACTIVE_FLAG&quot; FROM &quot;TBL_REQTYPE&quot; WHERE (&quot;ISACTIVE_FLAG&quot; = :ISACTIVE_FLAG)">
                                                                <SelectParameters>
                                                                    <asp:Parameter DefaultValue="Y" Name="ISACTIVE_FLAG" Type="String" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                        <td width="9%">
                                                            <dx:ASPxLabel ID="lblSearchDate" runat="server" Text="วันที่ยื่นคำขอ:" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                        <td width="10%">
                                                            <%--<dx:ASPxDateEdit runat="server" ID="dedtStart" CssClass="dxeLineBreakFix" SkinID="xdte">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="dedtStart" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td width="1%" align="center">-
                                                        </td>
                                                        <td width="10%">
                                                            <%--<dx:ASPxDateEdit runat="server" ID="dedtEnd" CssClass="dxeLineBreakFix" SkinID="xdte">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="dedtEnd" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td width="10%">
                                                            <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" CssClass="dxeLineBreakFix" OnClick="btnSearch_Click"
                                                                AutoPostBack="false">
                                                                <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('search');}" />--%>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvwWaiting" Width="100%" ClientInstanceName="gvwWaiting"
                                                    AutoGenerateColumns="false" KeyFieldName="REQUEST_ID">
                                                    <Columns>
                                                        <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" HeaderStyle-HorizontalAlign="Center"
                                                            CellStyle-HorizontalAlign="Center" Width="10%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="ทะเบียนท้าย" FieldName="TU_NO" HeaderStyle-HorizontalAlign="Center"
                                                            CellStyle-HorizontalAlign="Center" Width="10%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn Caption="ยื่นคำขอ" FieldName="REQUEST_DATE" Width="10%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                                            </PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="หมดอายุวัดน้ำ" FieldName="DWATEREXPIRE" Width="10%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                                            </PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                                            Width="21%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="ประเภทคำขอ" FieldName="REQTYPE_NAME" HeaderStyle-HorizontalAlign="Center"
                                                            Width="17%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="สถานะคำขอ" FieldName="STATUSREQ_NAME" HeaderStyle-HorizontalAlign="Center"
                                                            Width="17%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption=" " Width="5%" FooterCellStyle-CssClass="xxSearch8" >
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton runat="server" ID="btnEditwaiting" AutoPostBack="false" EnableTheming="false"
                                                                    EnableDefaultAppearance="False" Cursor="pointer">
                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Url="Images/ic_search.gif" Height="20px" Width="20px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="REQUEST_ID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="VENDOR_ID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="STATUS_FLAG" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p1" Text="ดำเนินการแล้ว">
                            <ContentCollection>
                                <dx:ContentControl>
                                    <table cellpadding="3" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right" width="10%">
                                                            <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text="ค้นหา:" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxTextBox runat="server" ID="txtsearchT2" CssClass="dxeLineBreakFix" Width="100%"
                                                                NullText="บริษัทขนส่ง,ทะเบียนรถ">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxComboBox runat="server" ID="cboReatypeT2" CssClass="dxeLineBreakFix" DataSourceID="sdsReqType"
                                                                Width="100%" TextField="REQTYPE_NAME" ValueField="REQTYPE_ID">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td width="9%">
                                                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="วันที่ยื่นคำขอ:" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                        <td width="10%">
                                                            <%--<dx:ASPxDateEdit runat="server" ID="dedtStartT2" CssClass="dxeLineBreakFix" SkinID="xdte">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="dedtStartT2" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td width="1%" align="center">-
                                                        </td>
                                                        <td width="10%">
                                                            <%--<dx:ASPxDateEdit runat="server" ID="dedtEndT2" CssClass="dxeLineBreakFix" SkinID="xdte">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="dedtEndT2" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td width="10%">
                                                            <dx:ASPxButton runat="server" ID="btnSearchT2" SkinID="_search" CssClass="dxeLineBreakFix" OnClick="btnSearchT2_Click"
                                                                AutoPostBack="false">
                                                                <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('searchT2');}" />--%>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvwtab2" Width="100%" ClientInstanceName="gvwWaiting"
                                                    AutoGenerateColumns="false" KeyFieldName="REQUEST_ID">
                                                    <Columns>
                                                        <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" HeaderStyle-HorizontalAlign="Center"
                                                            CellStyle-HorizontalAlign="Center" Width="10%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="ทะเบียนท้าย" FieldName="TU_NO" HeaderStyle-HorizontalAlign="Center"
                                                            CellStyle-HorizontalAlign="Center" Width="10%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn Caption="ยื่นคำขอ" FieldName="REQUEST_DATE" Width="10%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                                            </PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="วันที่พิจารณา" FieldName="APPROVE_DATE" Width="10%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                                            </PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="หมดอายุวัดน้ำ" FieldName="DWATEREXPIRE" Width="10%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                                            </PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                                            Width="21%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="ประเภทคำขอ" FieldName="REQTYPE_NAME" HeaderStyle-HorizontalAlign="Center"
                                                            Width="17%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="สถานะคำขอ" FieldName="STATUSREQ_NAME" HeaderStyle-HorizontalAlign="Center"
                                                            Width="17%">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption=" " Width="5%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton runat="server" ID="btnEditwaiting" AutoPostBack="false" EnableTheming="false"
                                                                    EnableDefaultAppearance="False" Cursor="pointer">
                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('view;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Url="Images/ic_search.gif" Height="20px" Width="20px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="REQUEST_ID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="VENDOR_ID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
