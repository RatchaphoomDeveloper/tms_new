﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Summary_Detail.aspx.cs" Inherits="KPI_Summary_Detail" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Src="~/UserControl/ucKPIDetail.ascx" TagPrefix="uc2" TagName="KPIDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="สรุปผลประเมินดัชนีชี้วัดประสิทธิภาพการทำงาน KPI แยกสัญญา (รายปี)"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblYear" runat="server" Text="ประจำปี :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtYear" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="Label1" runat="server" Text="ผลประเมิน KPI ประจำปี :&nbsp;"></asp:Label>                                                
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtKPIYear" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false" Text="0"></asp:TextBox>                                                
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblVendor" runat="server" Text="บริษัทผู้ขนส่ง :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                            <asp:Label ID="lblContract" runat="server" Text="เลขที่สัญญา :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtContract" runat="server" CssClass="form-control" Width="350px" style="text-align:center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <br />
                                    <br />
                                    <div id="divGrid" runat="server"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdClose" runat="server" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdClose_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>