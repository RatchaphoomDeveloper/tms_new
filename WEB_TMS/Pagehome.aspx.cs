﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pagehome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Session["SVDID"] + ""))
        {
            string ctype = Session["CGROUP"] + "";
            if (ctype == "0")
            {
                Response.Redirect("vendor_home.aspx");
            }
            else if (ctype == "1")
            {
                Response.Redirect("admin_home.aspx");
            }
        }
    }
}