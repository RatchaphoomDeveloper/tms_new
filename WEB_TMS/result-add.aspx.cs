﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Data.OracleClient;
using System.IO;
using DevExpress.Web.ASPxEditors;
using System.Globalization;


public partial class result_add : PageBase
{
    string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string sREQUESTID = "";
    private static string sDocOther = "0002";
    private static string sFomateDate = ConfigurationManager.AppSettings["FormatDate"] + "";
    private static string sCheckWaterID = "01";
    private static string CARCATE_ID = "";
    private static string sStatusID = "";
    private static string RK_FLAG = "";

    private static DataTable dtMainData = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        else
        {
            if (!IsPostBack)
            {
                string strREQID = Request.QueryString["strRQID"];
                if (!string.IsNullOrEmpty(strREQID + ""))
                {
                    dtMainData = new DataTable();

                    string[] arrREQID = STCrypt.DecryptURL(strREQID);
                    sREQUESTID = arrREQID[0];
                    if (!string.IsNullOrEmpty(sREQUESTID))
                    {
                        ListDataToPage(arrREQID[0]);
                        //ListDataHistory(arrREQID[0],"");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
                    }

                }

                this.AssignAuthen();
            }
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnToAdd2.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] spara = e.Parameter.Split(';');
        switch (spara[0])
        {
            case "RedirectT1": xcpn.JSProperties["cpRedirectTo"] = "result-add.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT2": xcpn.JSProperties["cpRedirectTo"] = "result-add2.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT3": xcpn.JSProperties["cpRedirectTo"] = "result-add3.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT4": xcpn.JSProperties["cpRedirectTo"] = "result-add4.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT5": xcpn.JSProperties["cpRedirectTo"] = "result-add5.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectImg": xcpn.JSProperties["cpRedirectTo"] = "CarImageAdd.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "History":
                int Indx = 0;
                if (!string.IsNullOrEmpty(spara[1] + ""))
                {
                    Indx = int.Parse(spara[1] + "");
                    dynamic data = gvwhistory.GetRowValues(Indx, "REQUEST_ID");
                    string REQID = data;
                    xcpn.JSProperties["cpRedirectTo"] = "History_Car.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(REQID));
                }
                break;

        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //ListDataHistory(sREQUESTID, "");
    }

    private void ListDataToPage(string sREQID)
    {
        //        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,
        //                        TRQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TRK.SCARTYPEID,
        //                        TCAT.CARCATE_NAME,
        //                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
        //                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,
        //                        TVD.SABBREVIATION,
        //                        TUS.SFIRSTNAME,TUS.SLASTNAME,
        //                        TRQ.CARCATE_ID,TRQ.RK_FLAG
        //                        FROM TBL_REQUEST TRQ 
        //                        INNER JOIN TTRUCK TRK ON TRQ.STRUCKID = TRK.STRUCKID
        //                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
        //                        INNER JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
        //                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
        //                        INNER JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
        //                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
        //                        WHERE TRQ.REQUEST_ID = '{0}'";
        
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,
                        TRQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TRK.SCARTYPEID,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,
                        NVL(TVD.SABBREVIATION,TRK.SOWNER_NAME) as SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME,
                        TRQ.CARCATE_ID,TRQ.RK_FLAG
                        FROM TBL_REQUEST TRQ 
                        INNER JOIN TTRUCK TRK ON TRQ.STRUCKID = TRK.STRUCKID
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        INNER JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        WHERE TRQ.REQUEST_ID = '{0}'";

        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        SetDataToPage();
    }

    private void SetDataToPage()
    {
        if (dtMainData.Rows.Count > 0)
        {
            DataRow dr = null;
            dr = dtMainData.Rows[0];
            decimal nTemp = 0;

            CARCATE_ID = dr["CARCATE_ID"] + "";
            sStatusID = dr["STATUS_FLAG"] + "";
            RK_FLAG = dr["RK_FLAG"] + "";

            lblREQUEST_DATE.Text = !string.IsNullOrEmpty(dr["REQUEST_DATE"] + "") ? Convert.ToDateTime(dr["REQUEST_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblDWATEREXPIRE.Text = !string.IsNullOrEmpty(dr["DWATEREXPIRE"] + "") ? Convert.ToDateTime(dr["DWATEREXPIRE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblSERVICE_DATE.Text = !string.IsNullOrEmpty(dr["SERVICE_DATE"] + "") ? Convert.ToDateTime(dr["SERVICE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblAPPOINTMENT_BY_DATE.Text = (!string.IsNullOrEmpty(dr["APPROVE_DATE"] + "") ? Convert.ToDateTime(dr["APPROVE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "") +
                                          (!string.IsNullOrEmpty(dr["SFIRSTNAME"] + "") && !string.IsNullOrEmpty(dr["SLASTNAME"] + "") ? " - คุณ" + dr["SFIRSTNAME"] + " " + dr["SLASTNAME"] : "-");


            lblREQTYPE_NAME.Text = dr["REQTYPE_NAME"] + "";
            lblCAUSE_NAME.Text = dr["CAUSE_NAME"] + "" + (!string.IsNullOrEmpty(dr["REMARK_CAUSE"] + "") ? (" (" + dr["REMARK_CAUSE"] + ")") : "");
            lblVendorName.Text = dr["SABBREVIATION"] + "";
            lblTypeCar.Text = dr["CARCATE_NAME"] + "";
            lblTotalCap.Text = (decimal.TryParse(dr["TOTLE_CAP"] + "", out nTemp) ? nTemp : 0).ToString(SystemFunction.CheckFormatNuberic(0));
            lblREGISTERNO.Text = dr["VEH_NO"] + "" + (!string.IsNullOrEmpty(dr["TU_NO"] + "") ? "/" + dr["TU_NO"] + "" : "");
            lblTOTLE_SERVCHAGE.Text = (decimal.TryParse(dr["TOTLE_SERVCHAGE"] + "", out nTemp) ? nTemp : 0).ToString(SystemFunction.CheckFormatNuberic(2));

            //if (dr["REQTYPE_ID"] + "" == sCheckWaterID)
            //{
            // set data to grid
            string sTruckID = "";
            switch (dr["SCARTYPEID"] + "")
            {
                case "0": sTruckID = dr["VEH_ID"] + ""; break; // 10 ล้อ
                case "3": sTruckID = dr["TU_ID"] + ""; break; // หัวลาก
            }
            ListCompareData_Water_RQ(sREQUESTID, sTruckID);
            ListDataHistory(dr["VEH_ID"] + "", dr["TU_ID"] + "");
            //}

            // set data document
            SetDocument(sREQUESTID, dr["REQTYPE_ID"] + "");

            // Set to control for javascript
            txtCheckReqType.Text = dr["REQTYPE_ID"] + "";

        }
    }

    private void SetDocument(string _sREQID, string _REQTYPE_ID)
    {
        List<TData_File> lstFile = new List<TData_File>(ListAttacFile(_sREQID, ""));

        string sMark = "<span class='active'>*</span>";
        string sImg1 = @"<img src='images/action_check.png' width='16' height='16'  alt=''/>";
        string sImg2 = @"<img src='images/stat_unchecked.gif' width='14' height='14'  alt=''/>";

        List<TData_DocType> lstDocType = new List<TData_DocType>(ListDocType(_REQTYPE_ID));

        var query = (from ld in lstDocType//.Where(w => w.CDYNAMIC == "Y")
                     from lf in lstFile.Where(w => w.sFileType == ld.DOCTYPE_ID).DefaultIfEmpty()
                     select new
                     {
                         nFileID = lf != null ? lf.nFileID : 0,
                         sNameShow = lf != null ? lf.sNameShow : "",
                         sSysFileName = lf != null ? lf.sSysFileName : "",
                         sPath = lf != null ? lf.sPath : "",
                         sOpenFile = lf != null ? ((!string.IsNullOrEmpty(lf.sSysFileName) && !string.IsNullOrEmpty(lf.sPath)) ? EncryptLinkToOpenFile(lf.sPath + lf.sSysFileName) : "N") : "N",
                         sFileType = ld.DOCTYPE_ID,
                         sNewFile = lf != null ? lf.sNewFile : "",
                         nOrder = ReturnDocOrder(ld.DOCTYPE_ID),
                         sConsider = lf != null ? lf.sConsider : "X",
                         sConsiderShow = lf != null ? (lf.sConsider == "Y" ? (sImg1 + " ผ่าน") : (sImg2 + " ไม่ผ่าน")) : sImg2 + " ไม่มี",
                         DOCTYPE_ID = ld.DOCTYPE_ID,
                         DOC_DESCRIPTION = ld.DOCTYPE_ID != sDocOther ? (ld.DOC_DESCRIPTION + (_REQTYPE_ID == "01" ? (ld.CATTACH_DOC01 == "Y" ? sMark : "") : _REQTYPE_ID == "02" ? (ld.CATTACH_DOC02 == "Y" ? sMark : "") : _REQTYPE_ID == "03" ? (ld.CATTACH_DOC03 == "Y" ? sMark : "") : _REQTYPE_ID == "04" ? (ld.CATTACH_DOC04 == "Y" ? sMark : "") : "")) : ld.DOC_DESCRIPTION
                     }).OrderBy(o => o.nOrder).ToList();

        gvwDocument.DataSource = query;
        gvwDocument.DataBind();
    }

    private int ReturnDocOrder(string DOCTYPE)
    {
        int Result = 0;

        switch (DOCTYPE)
        {
            case "0005":
                Result = 97;
                break;
            case "0006":
                Result = 98;
                break;
            case "0002":
                Result = 99;
                break;
        }

        return Result;

    }

    private void ListCompareData_Water_RQ(string sReqID, string sTruckID)
    {
      
        DataTable dt = new DataTable();
        string str1 = "<span class=\"active\">";
        string str2 = "*</span>";

        // ข้อมูลเดิมของรถ
        string sqlCOMPART = @"SELECT * FROM TTRUCK_COMPART WHERE STRUCKID = '{0}'";
        dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sqlCOMPART, CommonFunction.ReplaceInjection(sTruckID)));
        List<TTRUCK_COMPART> lstTruckSlot = new List<TTRUCK_COMPART>();
        lstTruckSlot = dt.AsEnumerable().Select(s => new TTRUCK_COMPART
        {
            STRUCKID = s.Field<string>("STRUCKID"),
            NCOMPARTNO = s.Field<decimal>("NCOMPARTNO"), // ช่องที่
            NPANLEVEL = s.Field<decimal>("NPANLEVEL"), // แป้นที่           
            NCAPACITY = s["NCAPACITY"] != DBNull.Value ? s.Field<decimal>("NCAPACITY") : 0 //s.Field<decimal>("NCAPACITY") // ความจุ

        }).ToList();

        // ข้อมูลจาการ Request
        //dt = CommonFunction.Get_Data(strConn, string.Format(sqlRQ, CommonFunction.ReplaceInjection(sReqID)));
        dt = SystemFunction.LISTCAPACITY(sReqID);
        List<TData_REQSLOT> lstRQSlolt = new List<TData_REQSLOT>();

        lstRQSlolt = dt.AsEnumerable().Where(w => !string.IsNullOrEmpty(w.Field<decimal?>("LEVEL_NO") + "")).Select(s => new TData_REQSLOT
        {
            SNAME = s.Field<string>("SNAME"),
            LEVEL_NO = s.Field<decimal?>("LEVEL_NO"), // แป้นที่
            SLOT1 = CheckChangCapacityTruck(lstTruckSlot, 1, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT1")) == true ? s.Field<string>("SLOT1") : (str1 + s.Field<string>("SLOT1") + str2), // ช่องที่ true คือ ไม่เปลี่ยนความจุุ
            SLOT2 = CheckChangCapacityTruck(lstTruckSlot, 2, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT2")) == true ? s.Field<string>("SLOT2") : (str1 + s.Field<string>("SLOT2") + str2),
            SLOT3 = CheckChangCapacityTruck(lstTruckSlot, 3, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT3")) == true ? s.Field<string>("SLOT3") : (str1 + s.Field<string>("SLOT3") + str2),
            SLOT4 = CheckChangCapacityTruck(lstTruckSlot, 4, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT4")) == true ? s.Field<string>("SLOT4") : (str1 + s.Field<string>("SLOT4") + str2),
            SLOT5 = CheckChangCapacityTruck(lstTruckSlot, 5, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT5")) == true ? s.Field<string>("SLOT5") : (str1 + s.Field<string>("SLOT5") + str2),
            SLOT6 = CheckChangCapacityTruck(lstTruckSlot, 6, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT6")) == true ? s.Field<string>("SLOT6") : (str1 + s.Field<string>("SLOT6") + str2),
            SLOT7 = CheckChangCapacityTruck(lstTruckSlot, 7, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT7")) == true ? s.Field<string>("SLOT7") : (str1 + s.Field<string>("SLOT7") + str2),
            SLOT8 = CheckChangCapacityTruck(lstTruckSlot, 8, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT8")) == true ? s.Field<string>("SLOT8") : (str1 + s.Field<string>("SLOT8") + str2),
            SLOT9 = CheckChangCapacityTruck(lstTruckSlot, 9, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT9")) == true ? s.Field<string>("SLOT9") : (str1 + s.Field<string>("SLOT9") + str2),
            SLOT10 = CheckChangCapacityTruck(lstTruckSlot, 10, s.Field<decimal>("LEVEL_NO"), s.Field<string>("SLOT10")) == true ? s.Field<string>("SLOT10") : (s.Field<string>("SLOT10") + str2)

        }).ToList();

        // หาจำนวนช่องและความจุ
        List<TTRUCK_COMPART> lstCalSlot = new List<TTRUCK_COMPART>();

        #region loop data slot
        foreach (DataRow row in dt.Rows)
        {
            if (!string.IsNullOrEmpty(row["LEVEL_NO"] + ""))
            {
                // ช่องที่ 1
                if (!string.IsNullOrEmpty(row["SLOT1"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 1,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT1"] + "")
                    });
                }

                // ช่องที่ 2
                if (!string.IsNullOrEmpty(row["SLOT2"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 2,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT2"] + "")
                    });
                }

                // ช่องที่ 3
                if (!string.IsNullOrEmpty(row["SLOT3"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 3,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT3"] + "")
                    });
                }

                // ช่องที่ 4
                if (!string.IsNullOrEmpty(row["SLOT4"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 4,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT4"] + "")
                    });
                }

                // ช่องที่ 5
                if (!string.IsNullOrEmpty(row["SLOT5"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 5,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT5"] + "")
                    });
                }

                // ช่องที่ 6
                if (!string.IsNullOrEmpty(row["SLOT6"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 6,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT6"] + "")
                    });
                }

                // ช่องที่ 7
                if (!string.IsNullOrEmpty(row["SLOT7"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 7,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT7"] + "")
                    });
                }

                // ช่องที่ 8
                if (!string.IsNullOrEmpty(row["SLOT8"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 8,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT8"] + "")
                    });
                }

                // ช่องที่ 1
                if (!string.IsNullOrEmpty(row["SLOT9"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 9,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT9"] + "")
                    });
                }

                // ช่องที่ 1
                if (!string.IsNullOrEmpty(row["SLOT10"] + ""))
                {
                    lstCalSlot.Add(new TTRUCK_COMPART
                    {
                        NCOMPARTNO = 10,
                        NPANLEVEL = (decimal)row["LEVEL_NO"],
                        NCAPACITY = ConverToDecimal(row["SLOT10"] + "")
                    });
                }
            }
        }
        #endregion

        var queryTotalSlot = lstCalSlot.GroupBy(g => new { g.NCOMPARTNO }).Select(s => new
            {
                s.Key.NCOMPARTNO,
                nSum = s.Max(x => x.NCAPACITY)
            }).ToList();

        decimal TotalCapacilty = queryTotalSlot.Count > 0 ? queryTotalSlot.Sum(s => s.nSum) : 0; 
        decimal nMaxSlot = queryTotalSlot.Count > 0 ? queryTotalSlot.Max(x => x.NCOMPARTNO) : 0;

        lblMaxSlot.Text = nMaxSlot + "";
        lblTotalCapacity_SumMaxSlot.Text = TotalCapacilty.ToString(SystemFunction.CheckFormatNuberic(0));



        gvwCapCar.DataSource = lstRQSlolt;
        gvwCapCar.DataBind();
        //if (lstRQSlolt.Count == 3)
        //{
        //    gvwCapCar.SettingsPager.PageSize = 3;
        //}
        //else
        //{
        //    gvwCapCar.SettingsPager.PageSize = 2;
        //}
        #region เช็คว่ามีแป้นเท่าไหร่
        string _chkPan3 = @"SELECT MAX(R.LEVEL_NO)
FROM TBL_REQSLOT r  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"' AND NVL(STATUS_PAN3,'xxx') <> '0'";
        DataTable _dtChkPan3 = new DataTable();

        _dtChkPan3 = CommonFunction.Get_Data(strConn, _chkPan3);
        if (_dtChkPan3.Rows.Count > 0)
        {
            string _nPan = _dtChkPan3.Rows[0][0].ToString();
            if (_nPan != "3")
            {
                //ถ้าไม่มีแป้นสามให้แสดง 2 แป้น
                gvwCapCar.SettingsPager.PageSize = 2;
            }
            else
            {
                gvwCapCar.SettingsPager.PageSize = 3;
            }
        }
        #endregion
    }

    private decimal ConverToDecimal(string sValue)
    {
        decimal nTemp = 0;

        nTemp = decimal.TryParse(sValue, out nTemp) ? nTemp : 0;


        return nTemp;
    }

    private bool CheckChangCapacityTruck(List<TTRUCK_COMPART> lst, int nCompartNo, decimal NPanLevel, string sCapacity)
    {
        bool cCheck = false;
        decimal nTemp = 0;
        nTemp = decimal.TryParse(sCapacity, out nTemp) ? nTemp : 0;

        var query = lst.Where(w => w.NCOMPARTNO == nCompartNo && w.NPANLEVEL == NPanLevel).ToList();
        if (query.Count == 1)
        {
            var queryCheck = query.FirstOrDefault();
            cCheck = (queryCheck.NCAPACITY == nTemp);
        }
        else
        {
            // ไม่มีแป้น
            if (query.Count == 0 && string.IsNullOrEmpty(sCapacity))
            {
                cCheck = true;
            }
        }

        return cCheck;
    }

    private List<TData_File> ListAttacFile(string sREQID, string sFileTypeID)
    {
        List<TData_File> lstTemp = new List<TData_File>();
        string scondition1 = "AND DOC_TYPE  = '" + CommonFunction.ReplaceInjection(sFileTypeID) + "'";
        string sql = @"SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '{0}'" + (!string.IsNullOrEmpty(sFileTypeID) ? scondition1 : "");
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        if (dt.Rows.Count > 0)
        {
            lstTemp = dt.AsEnumerable().Select(s => new TData_File
            {
                nFileID = s.Field<decimal>("DOC_ID"),
                sSysFileName = s.Field<string>("FILE_SYSNAME"),
                sNameShow = s.Field<string>("FILE_NAME"),
                sPath = s.Field<string>("FILE_PATH"),
                nOrder = s.Field<decimal>("DOC_ITEM"),
                sFileType = s.Field<string>("DOC_TYPE"),
                sNewFile = "N",
                sConsider = s.Field<string>("CONSIDER") == null ? "N" : s.Field<string>("CONSIDER")
            }).ToList();
        }

        return lstTemp;
    }

    private List<TData_DocType> ListDocType(string sreqtypeid)
    {
        string scondition = "";
        switch (sreqtypeid)
        {
            case "01": scondition = "AND DOC_01 = 'Y' "; break;
            case "02": scondition = "AND DOC_02 = 'Y' "; break;
            case "03": scondition = "AND DOC_03 = 'Y' "; break;
            case "04": scondition = "AND DOC_04 = 'Y' "; break;
        }
        string Codition = "";
        Codition += SystemFunction.DOC_CLOSEWORK(sStatusID, sREQUESTID);

        List<TData_DocType> lstTemp = new List<TData_DocType>();
        string sql = @"SELECT * FROM TBL_DOCTYPE WHERE ISACTIVE_FLAG = 'Y' AND CARCATE_ID LIKE '%" + CommonFunction.ReplaceInjection(CARCATE_ID) + "%' " + scondition + Codition + " ORDER BY DOCTYPE_ID";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);
        lstTemp = dt.AsEnumerable().Select(s => new TData_DocType
        {
            DOCTYPE_ID = s.Field<string>("DOCTYPE_ID"),
            DOC_DESCRIPTION = s.Field<string>("DOC_DESCRIPTION"),
            DOC_01 = s.Field<string>("DOC_01"),
            DOC_02 = s.Field<string>("DOC_02"),
            DOC_03 = s.Field<string>("DOC_03"),
            DOC_04 = s.Field<string>("DOC_04"),
            ISACTIVE_FLAG = s.Field<string>("ISACTIVE_FLAG"),
            CDYNAMIC = s.Field<string>("CDYNAMIC"),
            CATTACH_DOC01 = s.Field<string>("CATTACH_DOC01"),
            CATTACH_DOC02 = s.Field<string>("CATTACH_DOC02"),
            CATTACH_DOC03 = s.Field<string>("CATTACH_DOC03"),
            CATTACH_DOC04 = s.Field<string>("CATTACH_DOC04")

        }).ToList();

        //Remove ใบเทียบแป้นออกเมื่อเป็นรถลูกค้า
        if (CARCATE_ID == "01")
        {
            lstTemp.RemoveAll(w => w.DOCTYPE_ID == "0006");
        }

        //Remove ใบเทียบแป้นออกเมื่อเป็นรถลูกค้า
        if (RK_FLAG == "Y")
        {
            lstTemp.RemoveAll(w => w.DOCTYPE_ID == "0001");
        }

        return lstTemp;
    }

    private string EncryptLinkToOpenFile(string str)
    {
        string strEncrypt = "";
        /*
        if (ConfigurationManager.AppSettings["Encrypt"].ToString().Equals("1"))
        {
            strEncrypt = Server.UrlEncode(STCrypt.Encrypt(str));
        }
        else
        {
            strEncrypt = str;
        }*/
        strEncrypt = str;
        return "openFile.aspx?str=" + strEncrypt;
    }

    private void ListDataHistory(string VehId, string TuId)
    {
        string Codition = "";
        if (!string.IsNullOrEmpty(VehId))
        {
            Codition += " AND VEH_ID = '" + CommonFunction.ReplaceInjection(VehId) + "'";
        }
        if (!string.IsNullOrEmpty(VehId))
        {
            Codition += " AND TU_ID = '" + CommonFunction.ReplaceInjection(TuId) + "'";
        }
        else
        {
            Codition += " AND TU_ID = 'xxx'";
        }


        string QUERY = @"SELECT REQ.REQUEST_ID ,STYPE.REQTYPE_NAME,CAUSE.CAUSE_NAME, TRUNC(TO_DATE(TIC.EXAMDATE,'dd/MM/yyyy')) as EXAMDATE,REQ.VEH_ID ,NVL(REQ.TU_ID,'xxx') as TU_ID  FROM TBL_REQUEST REQ
LEFT JOIN TBL_REQTYPE STYPE
ON REQ.REQTYPE_ID = STYPE.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAUSE
ON REQ.CAUSE_ID = CAUSE.CAUSE_ID
INNER JOIN( SELECT REQUEST_ID,MAX(EXAMDATE) as EXAMDATE FROM TBL_TIME_INNER_CHECKINGS GROUP BY   REQUEST_ID)TIC
ON REQ.REQUEST_ID = TIC.REQUEST_ID
WHERE 1=1 " + Codition + "";


        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, QUERY);
        gvwhistory.DataSource = dt;
        gvwhistory.DataBind();
    }

    #region class
    [Serializable]

    class TData_REQSLOT
    {
        public string SNAME { get; set; }
        public decimal? LEVEL_NO { get; set; }
        public string SLOT1 { get; set; }
        public string SLOT2 { get; set; }
        public string SLOT3 { get; set; }
        public string SLOT4 { get; set; }
        public string SLOT5 { get; set; }
        public string SLOT6 { get; set; }
        public string SLOT7 { get; set; }
        public string SLOT8 { get; set; }
        public string SLOT9 { get; set; }
        public string SLOT10 { get; set; }
    }

    class TTRUCK_COMPART
    {
        public string STRUCKID { get; set; }
        public decimal NCOMPARTNO { get; set; }
        public decimal NPANLEVEL { get; set; }
        public decimal NCAPACITY { get; set; }
    }


    class TData_File
    {
        public decimal nFileID { get; set; }
        public string sSysFileName { get; set; }
        public string sNameShow { get; set; }
        public string sPath { get; set; }
        public string sOpenFile { get; set; }
        public decimal nOrder { get; set; }
        public string sFileType { get; set; }
        public string sNewFile { get; set; } // ไฟล์ใหม่หรือไฟล์เดิม
        public string sConsider { get; set; } // สถานะเอกสาร // Y = ตรวจสอบผ่านแล้ว, N = ไม่ผ่าน/ยังไม่ตรวจาสอบ

        public string DOCTYPE_ID { get; set; }
        public string DOC_DESCRIPTION { get; set; }
        public string sAllowEdit { get; set; }
    }

    class TData_DocType
    {
        public string DOCTYPE_ID { get; set; }
        public string DOC_DESCRIPTION { get; set; }
        public string DOC_01 { get; set; }
        public string DOC_02 { get; set; }
        public string DOC_03 { get; set; }
        public string DOC_04 { get; set; }
        public string ISACTIVE_FLAG { get; set; }
        public string CDYNAMIC { get; set; }
        public string CATTACH_DOC01 { get; set; }
        public string CATTACH_DOC02 { get; set; }
        public string CATTACH_DOC03 { get; set; }
        public string CATTACH_DOC04 { get; set; }
    }

    #endregion
}