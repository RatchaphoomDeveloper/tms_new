﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="surprisechk_show.aspx.cs" 
     Inherits="surprisechk_show" StylesheetTheme="Aqua" MaintainScrollPositionOnPostback="true" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server" >
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />

<%--    <link href="Css/sb-admin/bootstrap.css" rel="stylesheet"/>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/bootstrap.js" type="text/javascript"></script>--%>

    

<%--    <!-- new link for bootstrap start-->
    <link href="Css/sb-admin/bootstrap.min.css" rel="stylesheet" />
    <link href="Css/sb-admin/all.min.css" rel="stylesheet" />
    <link href="Css/sb-admin/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="Css/sb-admin/sb-admin.css" rel="stylesheet" />
    <!-- new link for bootstrap end -->--%>

    <%--<!-- Bootstrap DatePicker Start-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function () {
            $('[id*=dteStart]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
    </script>--%>
    <!-- Bootstrap DatePicker End-->
    <script type="text/javascript">

      function gvrowtoggle(row) {
        try {
          row_num = row; 
          ctl_row = row - 1; 
          rows = document.getElementById('<%= gvw.ClientID %>').rows; 
          rowElement = rows[ctl_row];
          img = rowElement.cells[0].firstChild; 

          if (rows[row_num].className !== 'hidden') 
          {
            rows[row_num].className = 'hidden'; 
            img.src = '~/Images/autocomplete.gif'; 
          } 
          else {
            rows[row_num].className = ''; 
            img.src = '~/Images/autocomplete.gif'; 
          } 
        } 
        catch (ex) {alert(ex) }
      }
    </script> 

    <style type="text/css">
        .GridPager a,
        .GridPager span {
    display: inline-block;
    padding: 0px 9px;
    margin-right: 4px;
    border-radius: 3px;
    border: solid 1px #c0c0c0;
    background: #e9e9e9;
    box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
    font-size: .875em;
    font-weight: bold;
    text-decoration: none;
    color: #717171;
    text-shadow: 0px 1px 0px rgba(255,255,255, 1);
}

.GridPager a {
    background-color: #f5f5f5;
    color: #969696;
    border: 1px solid #969696;
}

.GridPager span {

    background: #616161;
    box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
    color: #f0f0f0;
    text-shadow: 0px 0px 3px rgba(0,0,0, .5);
    border: 1px solid #3AC0F2;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" Runat="Server" ViewStateMode="Disabled" EnableViewState="False">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <table width="100%">
                    <tr>
                        <td width="50%">
                        <table>
                         <tr>
                         <td>
                             <asp:Button CssClass="btn btn-info" Text="เพิ่มรายการ" runat="server" ID="btnSave" Width="180px" BackColor="#169bd5"  Font-Size="Small" OnClick="btnSave_Click" />
                         </td>
                         <td>

                         </td>
                         <td>

                         </td>
                         </tr>
                         
                        </table>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="false" CssClass="form-control input-lg" Font-Size="Small" Width="250px" placeholder="เลขที่สัญญา,ชื่อผู้ประกอบการขนส่ง" Height="33px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="dteStart" runat="server" ClientIDMode="Static" CssClass="datepicker" width="250px" placeholder="วันที่ ที่อยู่ในช่วงสัญญา" ></asp:TextBox>
                            <%--<asp:TextBox ID="dteStart" runat="server" ClientIDMode="Static" CssClass="m-wrap span12 date form_datetime datetimepick" width="250px" placeholder="วันที่ ที่อยู่ในช่วงสัญญา" ></asp:TextBox>--%>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboStatus" runat="server"  Width="200px" CssClass="form-control"  Font-Size="Small" >
                            </asp:DropDownList>
                            <%--DataSourceID="sdsTerminal" DataTextField="STERMINALNAME" DataValueField="STERMINALID"
                            <asp:SqlDataSource ID="sdsTerminal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT STERMINALID, STERMINALNAME FROM TTERMINAL_SAP WHERE STERMINALID LIKE 'H%' OR STERMINALID LIKE 'J%' OR STERMINALID LIKE 'K%'"></asp:SqlDataSource>
                            --%>
                        </td>
                        <td>
                            <asp:Button CssClass="btn btn-info" BackColor="#169bd5" Text="ค้นหา" runat="server" ID="btnSearch" Width="180px" OnClick="btnSearch_Click"  Font-Size="Small" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="8">
                            จำนวนรายการ&nbsp;<asp:Label ID="lblCarCount" runat="server" Text="0"></asp:Label>                            
                            &nbsp;รายการ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <div class=”table-responsive”>     
                                
                                <asp:GridView ID="gvw" runat="server" CssClass="table table-primary"  
                                    AutoGenerateColumns="False" Width="100%" KeyFieldName="ID1" AllowPaging="true" 
                                    ShowFooter="false"
                                    CellPadding="4" ForeColor="Black" GridLines="Both"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                    OnRowCommand="gvw_RowCommand" OnRowDataBound="gvw_RowDataBound" 
                                    OnPageIndexChanging="gvw_PageIndexChanging" OnRowCreated="gvw_RowCreated" > 
                                    
                                    <Columns>                                        
                                        <asp:BoundField DataField="ID1" HeaderText="ที่"  HeaderStyle-BackColor="#c6defb" />                                                                             
                                        <asp:BoundField DataField="SVENDORNAME" HeaderText="ชื่อผู้ประกอบการ" HeaderStyle-BackColor="#c6defb"/>
                                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="SCONTRACTNO" HeaderText="สัญญา"   HeaderStyle-BackColor="#c6defb" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="NTRUCK" HeaderStyle-HorizontalAlign="Center" HeaderText="ในสัญญา"  HeaderStyle-CssClass="text-center" HeaderStyle-BackColor="#c6defb"  HeaderStyle-Font-Bold="false"/>
                                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="STANDBYCAR"  HeaderStyle-HorizontalAlign="Center" HeaderText="รถสำรอง"  HeaderStyle-CssClass="text-center"  HeaderStyle-BackColor="#c6defb" HeaderStyle-Font-Bold="false" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="COUNTTRUCK" HeaderStyle-HorizontalAlign="Center" HeaderText="ตรวจแล้ว(คัน)" HeaderStyle-CssClass="text-center" HeaderStyle-BackColor="#c6defb" HeaderStyle-Font-Bold="false" />
                                        <asp:BoundField DataField="DCHECK" HeaderText="วันที่ตรวจล่าสุด"  HeaderStyle-BackColor="#c6defb" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center"  HeaderStyle-BackColor="#c6defb">
                                          <ItemTemplate>
                                          </ItemTemplate> 
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SCONTRACTID" HeaderText="ContactID"  HeaderStyle-CssClass="HeaderStyle" Visible="false"/>   
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle HorizontalAlign = "Center" CssClass="GridPager"/>
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle CssClass="HeaderStyle" HorizontalAlign="Center"/>
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView> 
                                
                            </div>                           
                        </td>
                    </tr>
     </table>


</asp:Content>


