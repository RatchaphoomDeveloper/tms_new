﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" EnableViewState="true" AutoEventWireup="true" CodeFile="Vehicle_detail.aspx.cs" Inherits="Vehicle_detail" StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <h5>
        รายละเอียดข้อมูลรถ</h5>
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ชนิดรถ
        </div>
        <div class="panel-body">
            <asp:Table ID="Table3" runat="server" Height="76px" Width="958px">
                <asp:TableRow ID="TableRow13" runat="server">
                    <asp:TableCell ID="TableCell53" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    ชนิดรถ:
                    <asp:Label ID="Label28" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell54" runat="server" Width="30%">
                        <asp:RadioButtonList ID="Rdo_TypeCar" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                        <asp:ListItem Value="0" Text="รถเดี่ยว"></asp:ListItem>
                        <asp:ListItem Value="1" Text="รถเซมิเทรเลอร์"></asp:ListItem>
                        </asp:RadioButtonList>
                </asp:TableCell>
                    <asp:TableCell ID="TableCell55" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell56" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell57" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </div>
    <br />
    <div class="panel panel-info" id="divtruck" runat="server" visible="false">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ข้อมูลรถ
        </div>
        <div class="panel-body">
            <asp:Table ID="Table1" runat="server" Height="76px" Width="958px">
            <asp:TableRow ID="TableRow8" runat="server">
                    <asp:TableCell ID="TableCell35" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                        บริษัทผู้ขนส่ง:
                    <asp:Label ID="Label8" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell36" runat="server" Width="30%">
                    <asp:DropDownList ID="ddlVendorTruck" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlTruckSelectedValue">
                    </asp:DropDownList>                        
                </asp:TableCell>
                    <asp:TableCell ID="TableCell37" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell38" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell39" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
            <asp:TableRow ID="TableRow6" runat="server">
                    <asp:TableCell ID="TableCell17" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    สัญญาประเภทรถ:
                    <asp:Label ID="Label6" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell26" runat="server" Width="30%">
                    <asp:DropDownList ID="rdo_contratType" runat="server" CssClass="form-control">
                    </asp:DropDownList>                        
                </asp:TableCell>
                    <asp:TableCell ID="TableCell27" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell28" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell29" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow1" runat="server">
                    <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    รถเดี่ยว:
                    <asp:Label ID="Label1" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell2" runat="server" Width="30%">
                        <asp:DropDownList ID="ddl_truck" runat="server"  AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="TruckVeh_Text">
                        </asp:DropDownList>
                </asp:TableCell>
                    <asp:TableCell ID="TableCell3" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell4" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell5" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server">
                    <asp:TableCell ID="TableCell6" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    เลขที่สัญญาที่ต้องการ:
                   
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell runat="server" Width="30%">
                        <asp:DropDownList ID="Truck_contract" runat="server" CssClass="form-control">
                        
                        </asp:DropDownList>
                        
                </asp:TableCell>
                    <asp:TableCell runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow2" runat="server">
                    <asp:TableCell ID="TableCell7" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                        VEH_TEXT:
                   &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell8" runat="server" Width="30%">
                        <asp:TextBox ID="VEH_TEXT" runat="server" CssClass="form-control"  Enabled="false">
                        </asp:TextBox>
                </asp:TableCell>
                    <asp:TableCell ID="TableCell9" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell10" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell16" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>                
            </asp:Table>
        </div>
    </div>
    <div class="panel panel-info" id="divSemi" runat="server" visible="false">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ข้อมูลรถ
        </div>
        <div class="panel-body">
            <asp:Table ID="Table2" runat="server" Height="76px" Width="958px">
                <asp:TableRow ID="TableRow10" runat="server">
                    <asp:TableCell ID="TableCell45" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                        บริษัทผู้ขนส่ง:
                    <asp:Label ID="Label10" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell46" runat="server" Width="30%">
                    <asp:DropDownList ID="ddlvendorSemiTruck" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSemiSelectValue">
                    </asp:DropDownList>                        
                </asp:TableCell>
                    <asp:TableCell ID="TableCell47" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell48" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell49" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow7" runat="server">
                    <asp:TableCell ID="TableCell30" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    สัญญาประเภทรถ:
                    <asp:Label ID="Label7" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell31" runat="server" Width="30%">
                    <asp:DropDownList ID="rdo_contratType1" runat="server" CssClass="form-control" >
                    </asp:DropDownList>                   
                </asp:TableCell>
                    <asp:TableCell ID="TableCell32" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell33" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell34" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow3" runat="server">
                    <asp:TableCell ID="TableCell11" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    หัวลาก:
                    <asp:Label ID="Label3" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell12" runat="server" Width="30%">
                        <asp:DropDownList ID="truck_head" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                </asp:TableCell>
                    <asp:TableCell ID="TableCell13" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell14" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell15" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow5" runat="server">
                    <asp:TableCell ID="TableCell21" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    หางลาก:
                    <asp:Label ID="Label5" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell22" runat="server" Width="30%">
                        <asp:DropDownList ID="Truck_Semi" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="SemitTruckVeh_Text">
                        </asp:DropDownList>
                </asp:TableCell>
                    <asp:TableCell ID="TableCell23" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell24" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell25" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow9" runat="server">
                    <asp:TableCell ID="TableCell40" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                    เลขที่สัญญาที่ต้องการ:
                    
                    &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell41" runat="server" Width="30%">
                    <asp:DropDownList ID="TruckSemi_contract" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    
                </asp:TableCell>
                    <asp:TableCell ID="TableCell42" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell43" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell44" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow4" runat="server">
                    <asp:TableCell ID="TableCell18" runat="server" HorizontalAlign="Right" 
                        Width="20%">
                        VEH_TEXT:
                   &nbsp;&nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell19" runat="server" Width="30%">
                        <asp:TextBox ID="semi_veh_text" runat="server" CssClass="form-control"  Enabled="false">
                        </asp:TextBox>
                </asp:TableCell>
                    <asp:TableCell ID="TableCell20" runat="server" Width="2%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell50" runat="server" HorizontalAlign="Right" 
                        Width="18%">
                    &nbsp;
                </asp:TableCell>
                    <asp:TableCell ID="TableCell51" runat="server" Width="30%">
                    &nbsp;
                </asp:TableCell>
                </asp:TableRow>                    
            </asp:Table>
        </div>
    </div>
<br />
    <div id="buttonsave" runat="server" style="text-align: center" visible="true">
&nbsp; 
<asp:Button ID="cmdCancelDoc" runat="server" Enabled="true" Text="กลับสู่เมนู" CssClass="btn btn-md btn-hover btn-danger" Style="width: 120px;" data-toggle="modal" data-target="#ModalConfirmBeforeCancel" OnClick="ResponseMenu" />
 </div>   
</asp:Content>


