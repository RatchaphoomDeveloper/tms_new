﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Admin_VisitForm_Config.aspx.cs" Inherits="Admin_VisitForm_Config" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnAdd" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div class="form-group form-horizontal row">
                                <label for="ddlYearSearch" class="col-md-2 control-label">ปีใช้งาน</label>
                                <div class="col-md-3">
                                    <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                                <label for="txtSearchFormName" class="col-md-2 control-label">ชื่อแบบฟอร์ม</label>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtSearchFormName" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="rdoStatus" class="col-md-2 control-label">สถานะการใช้งาน</label>
                                <div class="col-md-3 radio">
                                    <asp:RadioButtonList ID="rdoStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                        <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                        <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-5">
                                    <asp:Button ID="btnAdd" runat="server" Text="ตั้งค่าการใช้งาน" CssClass="btn btn-hover btn-info" OnClick="btnAdd_Click" />
                                    <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" CssClass="btn btn-hover btn-info" OnClick="btnSearch_Click" />
                                    <asp:Button ID="btnClear" runat="server" Text="เคลียร์" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ข้อมูลการตั้งค่าใช้งาน
                </div>
                <div class="panel-body">
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" DataKeyNames="ID"
                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="ปีที่ใช้งาน" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblYEAR" runat="server" Text='<%# Eval("YEAR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสฟอร์ม" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblNTYPEVISITFORMID" runat="server" Text='<%# Eval("NTYPEVISITFORMID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
<asp:TemplateField HeaderText="ชื่อแบบฟอร์ม"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSTYPEVISITFORMNAME" runat="server" Text='<%# Eval("FORMNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkActive" runat="server" Checked="false" Enabled="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-Width="100px" HeaderStyle-Width="100px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="dateC" runat="server" Text='<%# Eval("CREATE_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การแก้ไขล่าสุด" ItemStyle-Width="120px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="dateU" runat="server" Text='<%# Eval("UPDATE_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เรียกดูข้อมูล" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/btnSearch.png" Width="16px" Height="16px" Style="cursor: pointer" CommandName="View" CausesValidation="False" BorderWidth="0" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="แก้ไขข้อมูล" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageEdit" runat="server" ImageUrl="~/Images/document.gif" Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CommandName="RowSelect" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

