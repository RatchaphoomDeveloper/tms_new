﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="SupportReport.aspx.cs" Inherits="SupportReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">

    <div style="text-align: center">
        <table width="100%">
            <tr>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    <a href="ExportData.aspx?str=CONFTRCK">ข้อมูลการยืนยันรถตามสัญญา(ผู้ขนส่ง)</a>
                </td>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    <a href="ExportData.aspx?str=PSCD">ข้อมูลการจัดแผนงาน(คลัง,ปง.)</a>
                </td>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    <a href="ExportData.aspx?str=CONFPLNT">ข้อมูลการยืนยันแผน(ผู้ขนส่ง)</a>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    <a href="monitor_LogEmail.aspx">ข้อมูลการส่งEmail</a>
                </td>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    <a href="loguser.aspx">ข้อมูลการเข้าใช้งานระบบ</a>
                </td>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    <a href="FAQINFORMATION.aspx">FAQ</a>
                </td>
            </tr>
              <tr>
                <td colspan="6">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    <a href="AdminHelper.aspx">อัพโหลดข้อมูลพื้นฐาน(รถ,สัญญา,พขร.,ผู้ขนส่ง)</a>
                </td>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="width: 10%">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
