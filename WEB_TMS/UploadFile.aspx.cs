﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class UploadFile : PageBase
{
    #region + View State +
    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SearchData(string Condition)
    {
        try
        {
            dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectAllBLL(string.Empty);
            GridViewHelper.BindGridView(ref dgvTopic, dtUploadType);

            DataTable dt = UploadTypeBLL.Instance.UploadTypeSelectSearchBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlSearchType, dt, "UPLOAD_TYPE_VALUE", "UPLOAD_TYPE", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvTopic_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvTopic.DataKeys[e.RowIndex]["UPLOAD_ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            MsgAlert.OpenForm("UploadFileAdd.aspx?UPLOAD_ID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTopic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvTopic.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvTopic, dtUploadType);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectAllBLL(" AND UPLOAD_TYPE = '" + ddlSearchType.SelectedValue + "'");
            GridViewHelper.BindGridView(ref dgvTopic, dtUploadType);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("UploadFileAdd.aspx");
    }
}