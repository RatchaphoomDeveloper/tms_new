﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;

public partial class KPI_Index : PageBase
{
    DataSet ds;
    #region + View State +
    private DataTable dtKPI
    {
        get
        {
            if ((DataTable)ViewState["dtKPI"] != null)
                return (DataTable)ViewState["dtKPI"];
            else
                return null;
        }
        set
        {
            ViewState["dtKPI"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            if (Session["CGROUP"] + string.Empty == "0")
            {
                Response.Redirect("KPI_Summary.aspx");
            }
           
            this.InitialForm();
        }
        this.SetColor(ref gvKPI, dt);
        this.AssignAuthen();
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdForm.Enabled = false;
                lblViewYear.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdMappingForm.Enabled = false;
                cmdKPISave.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialForm()
    {
        try
        {
            SetDrowDownList();
            GetData();
            SetData();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdForm_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Evaluation_Kit.aspx");
    }
    protected void cmdMappingForm_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Mapping_Form.aspx");
    }
    protected void cmdKPISave_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Manual.aspx");
    }
    protected void cmdFormula_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Formula.aspx");
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetData();
        SetData();
    }

    #region DrowDownList
    private void SetDrowDownList()
    {
        for (int i = 2015; i < DateTime.Now.Year + 1; i++)
        {
            ddlYear.Items.Add(new ListItem()
            {
                Text = i + string.Empty,
                Value = i + string.Empty
            });
        }
        ddlYear.SelectedValue = DateTime.Now.Year + string.Empty;
    }

    #endregion

    #region GetData
    private void GetData()
    {
        ds = KPI2BLL.Instance.KPIIndexSelect(ddlYear.SelectedValue);
        if (ds.Tables.Count > 0)
        {
            DataRow[] drs = ds.Tables["DATA"].Select("STATUS_VALUE = 0");
            if (drs.Any())
            {
                DataRow dr = drs[0];
                dr["MONTH1"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty) 
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH1"] + string.Empty))
                    + int.Parse(dr["MONTH1"] + string.Empty);
                dr["MONTH2"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH2"] + string.Empty))
                    + int.Parse(dr["MONTH2"] + string.Empty);
                dr["MONTH3"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH3"] + string.Empty))
                    + int.Parse(dr["MONTH3"] + string.Empty);
                dr["MONTH4"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH4"] + string.Empty))
                    + int.Parse(dr["MONTH4"] + string.Empty);
                dr["MONTH5"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH5"] + string.Empty))
                    + int.Parse(dr["MONTH5"] + string.Empty);
                dr["MONTH6"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH6"] + string.Empty))
                    + int.Parse(dr["MONTH6"] + string.Empty);
                dr["MONTH7"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH7"] + string.Empty))
                    + int.Parse(dr["MONTH7"] + string.Empty);
                dr["MONTH8"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH8"] + string.Empty))
                    + int.Parse(dr["MONTH8"] + string.Empty);
                dr["MONTH9"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH9"] + string.Empty))
                    + int.Parse(dr["MONTH9"] + string.Empty);
                dr["MONTH10"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH10"] + string.Empty))
                    + int.Parse(dr["MONTH10"] + string.Empty);
                dr["MONTH11"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH11"] + string.Empty))
                    + int.Parse(dr["MONTH11"] + string.Empty);
                dr["MONTH12"] = int.Parse(ds.Tables["TOTAL"].Rows[0][0] + string.Empty)
                    - ds.Tables["DATA"].AsEnumerable().Sum(it => int.Parse(it["MONTH12"] + string.Empty))
                    + int.Parse(dr["MONTH12"] + string.Empty);
            }
            dt = ds.Tables["DATA"];
        }
    }
    #endregion

    #region SetData
    private void SetData()
    {
        gvKPI.DataSource = dt;
        gvKPI.DataBind();

        this.SetColor(ref gvKPI, dt);

        lblItem.Text = "ประจำปี : " + ddlYear.SelectedValue;
    }

    public void SetColor(ref GridView dgvSource, DataTable dataSource)
    {
        try
        {
            string ColorName = string.Empty;
            var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(Color));
            Label lbl = new Label();
            for (int i = 0; i < dgvSource.Rows.Count; i++)
            {
                lbl = (Label)dgvSource.Rows[i].FindControl("lblRowColor");
                if (lbl != null)
                {
                    if (!string.Equals(lbl.Text.Replace("&nbsp;", string.Empty), string.Empty))
                    {
                        ColorName = lbl.Text;
                        //dgvSource.Rows[i].ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblStatusName");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth1");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth2");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth3");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth4");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth5");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth6");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth7");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth8");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth9");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth10");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth11");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth12");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region gvKPI_RowUpdating
    protected void gvKPI_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        
    }
    #endregion

    #region gvKPI_PageIndexChanging
    protected void gvKPI_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvKPI.PageIndex = e.NewPageIndex;
        //btnSearch_Click(null, null);
    }
    #endregion

    protected void imgView_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        ImageButton img = (ImageButton)sender;

        string month = "1";
        if (img != null)
        {
            month = img.CommandArgument;
        }
        string strData = ddlYear.SelectedValue + "&" + month;
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
        string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

        Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('KPI_Result.aspx?str=" + encryptedValue + "' ,'_blank');", true);
    }
    protected void lblViewYear_Click(object sender, EventArgs e)
    {
        string strData = ddlYear.SelectedValue;
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
        string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('KPI_SUMMARY.aspx?str=" + encryptedValue + "' ,'_blank');", true);
    }
}