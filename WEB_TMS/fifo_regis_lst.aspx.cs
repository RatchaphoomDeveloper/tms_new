﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;

public partial class fifo_regis_lst : System.Web.UI.Page
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler



        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            cboTerminal1.DataBind();
            cboTerminal1.Items.Insert(0, new ListEditItem("ระบุคลังน้ำมันต้นทาง", ""));
            cboTerminal1.Value = Session["SVDID"] + "";

            dteStart.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            LogUser("17", "R", "เปิดดูข้อมูลหน้า ลงคิวเข้ารับงาน", "");

        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {
        sds.SelectCommand = @"SELECT F.NID,F.NNO,F.DDATE,F.SHEADREGISTERNO,F.STRAILERREGISTERNO,
CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT nvl(nvl(Tt.NTOTALCAPACITY,0), SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(nvl(T.NTOTALCAPACITY,0), SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY, 
 
 CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT COUNT(ttc.NCOMPARTNO) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE COUNT(TC1.NCOMPARTNO) END AS NCOMPARTNO, 


VP.SVENDORNAME,F.SEMPLOYEENAME,F.STEL  
FROM ((TFIFO F LEFT JOIN TTRUCK T ON F.SHEADREGISTERNO = T.SHEADREGISTERNO ) LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) TC1 ON T.STRUCKID = TC1.STRUCKID ) 
LEFT JOIN (TVENDOR V INNER JOIN TVENDOR_SAP VP ON V.SVENDORID = VP.SVENDORID) ON F.SVENDORID = V.SVENDORID WHERE NVL(F.CACTIVE,'1') = '1' AND nvl(F.CPLAN,'0') != '1'  AND F.STERMINAL LIKE '%' || :oTerminal || '%' AND (F.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR F.SEMPLOYEENAME LIKE '%' || :oSearch || '%' OR F.SPERSONALNO LIKE '%' || :oSearch || '%' OR VP.SVENDORNAME LIKE '%' || :oSearch || '%') AND (F.DDATE BETWEEN TO_DATE(:oBegin,'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(:oEnd,'dd/MM/yyyy HH24:MI:SS')) 
GROUP BY F.NID,F.NNO,F.DDATE,F.SHEADREGISTERNO,F.STRAILERREGISTERNO,VP.SVENDORNAME,F.SEMPLOYEENAME,F.STEL,t.NTOTALCAPACITY ORDER BY  F.NNO,F.NID";

        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("oSearch", CommonFunction.ReplaceInjection(txtSearch.Text.Trim()));
        sds.SelectParameters.Add("oTerminal", CommonFunction.ReplaceInjection(cboTerminal1.Value + ""));
        sds.SelectParameters.Add("oBegin", CommonFunction.ReplaceInjection(dteStart.Date.ToString("dd/MM/yyyy 00:00:00", new CultureInfo("en-US"))));
        sds.SelectParameters.Add("oEnd", CommonFunction.ReplaceInjection(dteEnd.Date.ToString("dd/MM/yyyy 23:59:59", new CultureInfo("en-US"))));

        sds.DataBind();
        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;

            case "delete":
                int index = int.Parse(paras[1]);
                string del = gvw.GetRowValues(index, "NID") + "";

                using (OracleConnection con11 = new OracleConnection(sql))
                {
                    con11.Open();
                    using (OracleCommand com11 = new OracleCommand("DELETE FROM TFIFO WHERE NID = " + CommonFunction.ReplaceInjection(del), con11))
                    {
                        com11.ExecuteNonQuery();
                    }
                }
                LogUser("17", "D", "ลบข้อมูลหน้า ลงคิวเข้ารับงาน ", del);

                gvw.DataBind();
                break;
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
