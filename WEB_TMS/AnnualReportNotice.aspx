﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AnnualReportNotice.aspx.cs" Inherits="AnnualReportNotice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">

    <asp:UpdatePanel ID="uplMain" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdUpload" />
            <asp:PostBackTrigger ControlID="btnExport" />
            <asp:PostBackTrigger ControlID="btnExportPdf" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>แบบฟอร์มเอกสาร
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body" style="padding-bottom: 0px;">
                            <div id="divVendorView" runat="server">
                                <div class="form-group form-horizontal row">
                                    <label for="<%=ddlYearSearch.ClientID %>" class="col-md-1 control-label">ปี</label>
                                    <div class="col-md-2">
                                        <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control marginTp">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Button ID="Button2" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 80px" />
                                    </div>
                                </div>
                            </div>
                            <div id="divFormNotice" runat="server">
                                <div class="form-group form-horizontal row">
                                    <label for="<%=lblYear.ClientID %>" class="col-md-2 control-label">ปี:</label>
                                    <div style="text-align: left" class="col-md-2 control-label">
                                        <asp:Label runat="server" ID="lblYear">2016</asp:Label>
                                    </div>
                                    <label for="<%=lblVendor.ClientID %>" class="col-md-1 control-label">บริษัท:</label>
                                    <div style="text-align: left" class="col-md-3 control-label">
                                        <asp:Label runat="server" ID="lblVendor"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group form-horizontal row">
                                    <label for="<%=txtDOCNO.ClientID %>" class="col-md-2 control-label">เลขที่เอกสาร :</label>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="txtDOCNO" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                    </div>
                                    <label for="<%=txtDOCDATE.ClientID %>" class="col-md-1 control-label">วันที่ :</label>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="txtDOCDATE" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label class="col-md-2 control-label"></label>
                                <asp:Button ID="btnBfSend" runat="server" Text="แจ้งผู้ขนส่ง" CssClass="btn btn-hover btn-info" OnClick="btnBfSend_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="ยกเลิกเอกสาร" CssClass="btn btn-hover btn-info" OnClick="btnCancel_Click" Enabled="false" />
                                <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btn btn-hover btn-info" OnClick="btnPreview_Click" />
                                <asp:Button ID="btnExport" runat="server" Text="Export Word" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />
                                <asp:Button ID="btnExportPdf" runat="server" Text="Export PDF" CssClass="btn btn-hover btn-info" OnClick="btnExportPdf_Click" /> 
                                <asp:Button ID="btnClose" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 80px" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table width="100%" id="tblFormNotice" runat="server">
                <tr align="center">
                    <td align="center">
                        <div runat="server" id="divEmail" style="width: 768px">
                        </div>
                    </td>
                </tr>
            </table>
            <div class="" style="display: none;">
                <div style="width: 768px; float: left" id="divBody" runat="server">
                </div>
            </div>


            <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" PageSize="50"
                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" DataKeyNames="ID" OnDataBound="grvMain_DataBound"
                EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                <Columns>
                    <asp:TemplateField HeaderText="ปี" ItemStyle-Width="180px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                        <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="50px" />
                        <ItemTemplate>
                            <asp:Label ID="lblYear" runat="server" Text='<%# Eval("YEAR") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อเอกสาร" ItemStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                        <ItemTemplate>
                            <asp:Label ID="lblDocDesc" runat="server" Text='ผลประเมินการปปฏิบัติงานในรอบ 12 เดือน ประจำปี {0}'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-Width="180px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                        <ItemTemplate>
                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Eval("DOCNO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ผลประเมินการทำงาน" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="100px" />
                        <ItemTemplate>
                            <asp:Button ID="btnExportPdfVendor" runat="server" Text="View" CssClass="btn btn-hover btn-info" OnClientClick="clickDownload()" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="panel panel-info" style="margin-top: 20px">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>เอกสารแนบ
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div id="dvFlie" runat="server">
                                <label for="<%=cboUploadType.ClientID %>" class="col-md-2 control-label">ประเภทเอกสาร</label>
                                <div class="col-md-3">
                                    <asp:Table ID="Table3" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                                    Width="350px" DataValueField="UPLOAD_ID">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:FileUpload ID="fileUpload" runat="server" />
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" OnClick="cmdUpload_Click" Style="width: 100px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>

                                </div>
                            </div>
                            <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                    <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                    <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                    <asp:BoundField DataField="FULLPATH" Visible="false" />
                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="16px" CausesValidation="True" Height="16px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="16px" Height="16px" Style="cursor: pointer" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="form-group form-horizontal row">
                        <div class="col-md-2"></div>
                        <div class="col-md-3">
                            <asp:Button ID="btnSave" runat="server" Text="บันทึกเอกสารแนบ" CssClass="btn btn-hover btn-info" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" style="z-index: 1060" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" id="alertWithClose">
                <div class="modal-dialog">
                    <div class="modal-content" style="text-align: left;">
                        <div class="modal-header">
                            <h4 class="modal-title">แจ้งเตือน</h4>
                        </div>
                        <div class="modal-body" style="word-wrap: break-word;">
                            <table style="width: 100%">
                                <tr>
                                    <td valign="top" style="width: 50px;">
                                        <asp:Image ImageUrl="~/Images/Icon_Lamp.png" ID="Image1" runat="server" /></td>
                                    <td valign="top" align="left" style="font-size: 16px; vertical-align: central;" class="TextDetail">
                                        <div style="height: 40px; text-align: center; display: table-cell; vertical-align: middle;">แจ้งผลการประเมินการทำงานประจำปี ให้ผู้ขนส่ง?</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnSend" runat="server" Text="แจ้งผู้ขนส่ง" CssClass="btn btn-hover btn-info" OnClientClick="clickChangeGroup()" />
                            <asp:Button runat="server" ID="Button1" Text="ไม่ใช่" CssClass="btn btn-md bth-hover btn-info" OnClientClick="CloseModalSave()" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <asp:Button ID="btnSendEmail" runat="server" OnClick="btnSend_Click" />
        <input type="text" name="hdnNum" id="hdnNum" runat="server" value="0">
        <asp:Button ID="btnVendorDownload" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnExportPdf_Click" />
 
    </div>

    <script type="text/javascript">

        function clickChangeGroup() {
            $("#<%=btnSendEmail.ClientID%>").click();
        }

        function clickDownload() {
            $("#<%=btnVendorDownload.ClientID%>").click();
        }

        function AlertSave() {
            $('#alertWithClose').find('input').attr('data-dismiss', 'modal');
            var $dialog = $('#alertChange').find(".modal-dialog");
            var offset = (($(window).height() - $dialog.height()) / 2 - $dialog.height() / 2);
            if ($dialog.height() < 0) {
                offset = $(window).height() / 2 - 200;
            }

            offset = $(window).height() / 2 - 200;
            $('#alertWithClose').find('.modal-dialog').css('margin-top', offset);

            $('#alertWithClose').modal('show');
        }

        function CloseModalSave() {
            $('#alertWithClose').modal('hide');
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

