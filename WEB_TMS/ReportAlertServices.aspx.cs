﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;

public partial class ReportAlertServices : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListData();
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData();
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": ListData();
                break;
            case "ReportPDF": ListReport("P");
                break;
            case "ReportExcel": ListReport("E");
                break;

        }
    }

    void ListData()
    {
        string Condition = "";
        if (!string.IsNullOrEmpty(EdtsDate.Text))
        {
            DateTime datestart = DateTime.Parse(EdtsDate.Value.ToString());
            lblsTail.Text = datestart.ToString("dd", new CultureInfo("th-TH")) + " " + datestart.ToString("MMM", new CultureInfo("th-TH")) + " " + datestart.ToString("yyyy", new CultureInfo("th-TH"));
            Condition = "WHERE TRUNC(TO_DATE(REQ.SERVICE_DATE,'dd/MM/yyyy')) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        }
        else
        {
            lblsTail.Text = " - ";
        }



        string QUERY = @"SELECT ROWNUM||'.' as NO,REQ.REQUEST_ID, TO_CHAR(add_months(REQ.APPROVE_DATE,6516),'dd/MM/yyyy') as APPROVE_DATE
, TO_CHAR(add_months(TCK.DWATEREXPIRE,6516),'dd/MM/yyyy') as DWATEREXPIRE
,VEN.SABBREVIATION,CRT.CARCATE_NAME,TCP.NCAPTOTAL,TCP.NCOMPARTNO,TCP.NPANLEVEL,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
,TRUNC(TO_DATE(REQ.SERVICE_DATE,'dd/MM/yyyy')) as SERVICE_DATE FROM TBL_REQUEST REQ
LEFT JOIN TTRUCK TCK
ON TCK.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
LEFT JOIN
(
     SELECT STRUCKID ,COUNT(NCOMPARTNO) as NCOMPARTNO,SUM(CAP) as NCAPTOTAL,NPANLEVEL
        FROM 
        (
            SELECT STRUCKID,NCOMPARTNO,MAX(NCAPACITY) as CAP,MAX(NPANLEVEL) as NPANLEVEL
            FROM TTRUCK_COMPART 
            WHERE NPANLEVEL <> 0
            GROUP BY STRUCKID,NCOMPARTNO
        )
    GROUP BY STRUCKID,NPANLEVEL
)TCP
ON TCP.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
LEFT JOIN TBL_CARCATE CRT ON CRT.CARCATE_ID = TCK.CARCATE_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN TVENDOR VEN ON VEN.SVENDORID = REQ.VENDOR_ID
" + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;
           
        }
        gvw.DataBind();
    }

    private void ListReport(string Type)
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(EdtsDate.Text))
        {
            DateTime datestart = DateTime.Parse(EdtsDate.Value.ToString());

            Condition = "WHERE TRUNC(TO_DATE(REQ.SERVICE_DATE,'dd/MM/yyyy')) = TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy')";
        }
        else
        {

        }



        string QUERY = @"SELECT ROWNUM||'.' as NO,REQ.REQUEST_ID, TO_CHAR(add_months(REQ.APPROVE_DATE,6516),'dd/MM/yyyy') as APPROVE_DATE
, TO_CHAR(add_months(TCK.DWATEREXPIRE,6516),'dd/MM/yyyy') as DWATEREXPIRE
,VEN.SABBREVIATION,CRT.CARCATE_NAME,TCP.NCAPTOTAL,TCP.NCOMPARTNO,TCP.NPANLEVEL,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
,TRUNC(TO_DATE(REQ.SERVICE_DATE,'dd/MM/yyyy')) as SERVICE_DATE FROM TBL_REQUEST REQ
LEFT JOIN TTRUCK TCK
ON TCK.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
LEFT JOIN
(
     SELECT STRUCKID ,COUNT(NCOMPARTNO) as NCOMPARTNO,SUM(CAP) as NCAPTOTAL,NPANLEVEL
        FROM 
        (
            SELECT STRUCKID,NCOMPARTNO,MAX(NCAPACITY) as CAP,MAX(NPANLEVEL) as NPANLEVEL
            FROM TTRUCK_COMPART 
            WHERE NPANLEVEL <> 0
            GROUP BY STRUCKID,NCOMPARTNO
        )
    GROUP BY STRUCKID,NPANLEVEL
)TCP
ON TCP.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
LEFT JOIN TBL_CARCATE CRT ON CRT.CARCATE_ID = TCK.CARCATE_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN TVENDOR VEN ON VEN.SVENDORID = REQ.VENDOR_ID
" + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        //if (dt.Rows.Count > 0)
        //{
        //    gvw.DataSource = dt;
        //    gvw.DataBind();
        //}


        ReportAlertService report = new ReportAlertService();

        #region function report

        if (!string.IsNullOrEmpty(EdtsDate.Text))
        {
            DateTime datestart = DateTime.Parse(EdtsDate.Value.ToString());
            ((XRLabel)report.FindControl("xrLabel2", true)).Text = datestart.Day + " " + datestart.ToString("MMMM", new CultureInfo("th-TH")) + " " + datestart.ToString("yyyy", new CultureInfo("th-TH")) + "";
        }
        else 
        {
            ((XRLabel)report.FindControl("xrLabel2", true)).Text = " - ";
        }

        report.Name = "ใบงาน แจ้งเตือนเข้ารับบริการ (ตารางงานประจำวัน)";
        report.DataSource = dt;
        string fileName = "ใบงาน แจ้งเตือนเข้ารับบริการ (ตารางงานประจำวัน)_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
       

        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }
    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ListReport("P");
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ListReport("E");
    }
}