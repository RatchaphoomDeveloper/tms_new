﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

public partial class document_history : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        string str = Request.QueryString["str"];
        string[] strQuery;
        if (!string.IsNullOrEmpty(str))
        {

            strQuery = STCrypt.DecryptURL(str);
            ViewState["setdata"] = strQuery[0];


            #region เช็ค Permission
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("46", "R", "เปิดดูข้อมูลหน้า ประวัติเอกสารสำคัญของบริษัท", ViewState["setdata"]+"");

            #endregion
         
            Listdata();
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
      
        Listdata();

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');
        switch (param[0])
        {
            case "Search": Listdata();
                break;

            case "CancelSearch":
                txtSearch.Text = "";
                Listdata();
                break;
        }


    }

    protected void gvwDoc_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePathgvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathgvwDoc") as ASPxTextBox;
            ASPxTextBox txtFileNamegvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNamegvwDoc") as ASPxTextBox;
            ASPxLabel lblData2 = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwDoc") as ASPxButton;
            ASPxButton btnDelFilegvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwDoc") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePathgvwDoc.Text))
            {
                btnViewgvwDoc.ClientVisible = true;
                btnDelFilegvwDoc.ClientVisible = false;
                btnViewgvwDoc.ClientEnabled = true;
                btnDelFilegvwDoc.ClientEnabled = false;

            }
            else
            {
                btnViewgvwDoc.ClientVisible = false;
                btnDelFilegvwDoc.ClientVisible = false;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }


            txtFileNamegvwDoc.ClientInstanceName = txtFileNamegvwDoc.ClientInstanceName + "_" + VisibleIndex;
            txtFilePathgvwDoc.ClientInstanceName = txtFilePathgvwDoc.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwDoc.ClientInstanceName = btnViewgvwDoc.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwDoc.ClientInstanceName = btnDelFilegvwDoc.ClientInstanceName + "_" + VisibleIndex;

            string txtFilePath = "";
            string txtFileName = "";
            if (!string.IsNullOrEmpty(txtFilePathgvwDoc.Text))
            {
                txtFilePath = txtFilePathgvwDoc.Text.Replace("VendorDoc/", "VendorDoc/History/");
            }
            if (!string.IsNullOrEmpty(txtFileNamegvwDoc.Text))
            {
                txtFileName = txtFileNamegvwDoc.Text;
            }
            string path = CommonFunction.ReplaceInjection(txtFilePath + txtFileName);
            //Add Event
            //btnViewgvwDoc.ClientSideEvents.Click = "function(s,e){window.open(openFile.aspx?str=" + txtFilePath + txtFileName + ");}";
            btnViewgvwDoc.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str=" + path + "');}";
            btnDelFilegvwDoc.ClientSideEvents.Click = "function (s, e) { xcpn.PerformCallback(\"deleteFilegvwsign; " + txtFilePathgvwDoc.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + " ;" + txtFileNamegvwDoc.ClientInstanceName.ToString() + ";" + txtFilePathgvwDoc.ClientInstanceName.ToString() + " ;" + btnViewgvwDoc.ClientInstanceName.ToString() + " ; " + btnDelFilegvwDoc.ClientInstanceName.ToString() + "\");}";
            // btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    void Listdata()
    {

        string VendorID = ViewState["setdata"] + "";

        string condition = "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            condition = " AND  SFILENAME||SDESCRIPTION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "%'";
        }

        if (!string.IsNullOrEmpty(VendorID))
        {
            string strsql = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, 
                              SDESCRIPTION, CACTIVE, DCREATE,SCREATE, DUPDATE, SUPDATE, DEXPIRE, SPATH,DATERECEIVE
                              FROM TVENDOR_DOC_HISTORY
                              WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                              AND SSYSFILENAME IS NOT NULL 
                              " + condition + @"
                              ORDER BY DATERECEIVE DESC";
            DataTable dt = CommonFunction.Get_Data(conn, strsql);



            gvwDoc.DataSource = dt;
            gvwDoc.DataBind();

        }




    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}