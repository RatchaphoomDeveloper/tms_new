﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="faqinformation.aspx.cs" Inherits="FAQINFORMATION" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v11.2" Namespace="DevExpress.Web.ASPxSpellChecker"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v11.2" Namespace="DevExpress.Web.ASPxHtmlEditor"
    TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function VisibleControl() {

            var bool = txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null;
            uploader1.SetClientVisible(bool);
            txtFileName.SetClientVisible(!bool);
            btnView.SetEnabled(!bool);
            btnDelFile.SetEnabled(!bool);
            if (!(txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null)) {
                chkUpload1.SetValue('1');
            } else {
                chkUpload1.SetValue('');
            }
        }
    </script>
    <style type="text/css">
        .style13
        {
            width: 100%;
        }
        .style15
        {
            width: 74px;
            height: 23px;
        }
        .style16
        {
            height: 23px;
        }
        .style18
        {
            width: 74px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnLoad="xcpn_Load" OnCallback="xcpn_Callback">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                   <script type="text/javascript">
    // <![CDATA[
                       var MaxLength = 40000;
                       var CustomErrorText = "Custom validation fails because the HTML content&prime;s length exceeds " + MaxLength.toString() + " characters.";
                       function ValidationHandler(s, e) {
                           if (e.html.length > MaxLength) {
                               e.isValid = false;
                               e.errorText = CustomErrorText;
                             
                           }
                       }
                       function HtmlChangedHandler(s, e) {
                           ContentLength.SetText(s.GetHtml().length);
                       }
    // ]]> 
    </script>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table class="style13">
                    <tr>
                        <td class="style18" bgcolor="#FFFFFF">&nbsp;<dx:ASPxLabel ID="lblQA" runat="server"
                            Text="หัวข้อ :">
                        </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxComboBox runat="server" CallbackPageSize="30" EnableCallbackMode="True" ValueField="FAQ_ID"
                                TextFormatString="{1}" Width="500px" Height="25px" ClientInstanceName="cboSelect"
                                CssClass="dxeLineBreakFix" SkinID="xcbbATC" ID="cboSelect" OnItemRequestedByValue="cboSelect_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboSelect_ItemsRequestedByFilterConditionSQL">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="FAQ_ID" Width="10%" Caption="ไอดี"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="FAQ_QUEST" Width="90%" Caption="ปัญหา"></dx:ListBoxColumn>
                                </Columns>
                                <ItemStyle Wrap="True"></ItemStyle>
                                <ClientSideEvents ValueChanged="function(s,e){xcpn.PerformCallback('binddata');}" />
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource runat="server" ID="sdsSelectdata" CacheKeyDependency="Selectdata"
                                CancelSelectOnNullParameter="false" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT FAQ_ID , FAQ_QUEST FROM (SELECT ROW_NUMBER()OVER(ORDER BY FAQ_ID) AS RN , FAQ_ID , FAQ_QUEST
          FROM TFAQ )"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15" bgcolor="#FFFFFF">
                            <dx:ASPxLabel runat="server" ID="lblQuest" Text="ปัญหา :">
                            </dx:ASPxLabel>
                        </td>
                        <td class="style16">
                            <dx:ASPxTextBox ID="txtQuest" runat="server" Width="100%" MaxLength="250" ClientInstanceName="txtQuest">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุหัวข้อ" IsRequired="True" />
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_Name %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style18" valign="top" bgcolor="#FFFFFF">
                            <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text="รายละเอียด :">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxHtmlEditor ID="txtAnswer" ClientInstanceName="txtAnswer" runat="server" Width="900px" Height="450px">
                                <ClientSideEvents HtmlChanged="function(s,e){ ContentLength.SetText(s.GetHtml().length);if(txtAnswer.GetHtml().length > 40000) {dxWarning('แจ้งเตือน','Custom validation fails because the HTML content&#8242;s length exceeds 40000 characters'); return false;} }"  Validation="ValidationHandler"  />
                               
                                <Settings AllowHtmlView="false" />
                                <Settings AllowPreview="false" />
                                <SettingsImageUpload UploadImageFolder="~/UploadFile\FAQ/" UseAdvancedUploadMode="True">
                                    <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                                    </ValidationSettings>
                                </SettingsImageUpload>
                                <SettingsImageSelector>
                                    <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
                                </SettingsImageSelector>
                                <SettingsDocumentSelector>
                                    <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp">
                                    </CommonSettings>
                                </SettingsDocumentSelector>
                                <SettingsValidation>
                                <RequiredField IsRequired="True" />
                                </SettingsValidation>
                            </dx:ASPxHtmlEditor>
                        </td>
                    </tr>
                    <tr>
                        <td class="style18" bgcolor="#FFFFFF">&nbsp;</td>
                        <td>
                            <div style="margin: 8px 0;">
                                จำกัดจำนวนตัวอักษรที่ใช้ 40,000 ตัว ตอนนี้มีทั้งหมด :
                                <dx:ASPxLabel ID="lblContentLength" runat="server" ClientInstanceName="ContentLength"
                                    Text="0" Font-Bold="True">
                                </dx:ASPxLabel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" class="style18">
                            <dx:ASPxLabel ID="lblUpload" runat="server" Text="แนบไฟล์ :">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td align="left" valign="middle">
                                        <dx:ASPxUploadControl ID="upload" runat="server" ClientInstanceName="uploader1" CssClass="dxeLineBreakFix"
                                            NullText="Click here to browse files..." OnFileUploadComplete="upload_FileUploadComplete"
                                            Width="200px">
                                            <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                            </ValidationSettings>
                                            <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath.SetValue((e.callbackData+'').split('|')[0]);txtFileName.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }"
                                                TextChanged="function(s,e){uploader1.Upload();}" />
                                            <BrowseButton Text="แนบไฟล์">
                                            </BrowseButton>
                                        </dx:ASPxUploadControl>
                                        <dx:ASPxTextBox ID="txtFilePath" runat="server" ClientInstanceName="txtFilePath"
                                            ClientVisible="False" Width="200px">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox ID="txtFileName" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName"
                                            ClientVisible="False" Width="200px">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox ID="chkUpload1" runat="server" ClientInstanceName="chkUpload1" CssClass="dxeLineBreakFix"
                                            ForeColor="White" Width="1px">
                                            <Border BorderStyle="None" />
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td valign="middle">
                                        <dx:ASPxButton ID="btnView" runat="server" AutoPostBack="False" CausesValidation="False"
                                            ClientEnabled="False" ClientInstanceName="btnView" CssClass="dxeLineBreakFix"
                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                            Width="25px">
                                            <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath.GetValue());}" />
                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                            </Image>
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnDelFile" runat="server" AutoPostBack="False" CausesValidation="False"
                                            ClientEnabled="False" ClientInstanceName="btnDelFile" CssClass="dxeLineBreakFix"
                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                            Width="25px">
                                            <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath.GetValue() +';1');}" />
                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                            </Image>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="style18"></td>
                        <td align="center">
                            <dx:ASPxButton ID="btnSave" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix"
                                Text="บันทึก" Height="25" Width="60px">
                                <%--<ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); window.location = 'faqinformation.aspx'; }" />--%>
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false;  
                                if(cboSelect.GetValue()!= null){xcpn.PerformCallback('Update'); } 
                                else if(cboSelect.GetValue() == null){ xcpn.PerformCallback('Save');  } }" />
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix"
                                Text="ยกเลิก" Height="25" Width="60px">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'faqsearch.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
