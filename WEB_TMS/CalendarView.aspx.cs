﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CalendarView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            Calendar.GenerateJavaScriptFullMonthCalendarDetailOnly(this.ltrmonthcalendar, month, year);

        }
    }
}