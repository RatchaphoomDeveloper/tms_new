﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="EmailSchedule.aspx.cs"
    Inherits="EmailSchedule" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblHeaderTab1" runat="server" Text="ตารางการส่งอีเมล์"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" Width="120px">
                                        <asp:Label ID="Label1" runat="server" Text="ประเภทอีเมล์ :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="210px">
                                        <asp:DropDownList ID="ddlEmailType" runat="server" CssClass="form-control" Width="200px"></asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" Width="120px">
                                        <asp:Label ID="lblTemplateName" runat="server" Text="Template Name :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="210px">
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-info" Width="80px" OnClick="cmdSearch_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>

                            <br />

                            <asp:GridView ID="dgvTemplate" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" PageSize="10" AllowPaging="true"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnPageIndexChanging="dgvTemplate_PageIndexChanging"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="SCHEDULE_ID" OnRowUpdating="dgvTemplate_RowUpdating"
                                AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับที่">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SCHEDULE_ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EMAIL_TYPE_NAME" HeaderText="ประเภทอีเมล์">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TEMPLATE_NAME" HeaderText="Template Name">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SCHEDULE_NO" HeaderText="ครั้งที่">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SCHEDULE_DAY" HeaderText="แจ้งเตือนล่วงหน้า (วัน)">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ISACTIVE" HeaderText="สถานะ">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="การกระทำ">
                                        <ItemTemplate>
                                            <asp:Button ID="cmdEdit" runat="server" Text="แก้ไข" CommandName="update" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Button ID="cmdAdd" runat="server" Text="เพิ่มข้อมูล" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdAdd_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
