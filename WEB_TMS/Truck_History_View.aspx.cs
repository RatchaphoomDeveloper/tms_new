﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Globalization;
using System.Text;
using System.Web.Services;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using DevExpress.XtraEditors;
using System.Web.UI.HtmlControls;
using System.Drawing;
using TMS_Entity.CarEntity;
using TMS_BLL.Transaction.Report;
using EmailHelper;
using System.Web.Security;

public partial class Truck_History_View : PageBase
{
    const string UploadDirectory = "UploadFile/Truck/";
    //เก็บหมายเลขทะเบียนส่งTques

    #region + ViewState +
    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    //เก็บหมายเลขทะเบียนส่งTques
    private string registerno
    {
        get
        {
            if ((string)ViewState["registerno"] != null)
                return (string)ViewState["registerno"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["registerno"] = value;
        }
    }
    private string SVDID
    {
        get
        {
            if ((string)ViewState["SVDID"] != null)
                return (string)ViewState["SVDID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SVDID"] = value;
        }
    }
    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }
    private DataTable dtHeader
    {
        get
        {
            if ((DataTable)ViewState["dtHeader"] != null)
                return (DataTable)ViewState["dtHeader"];
            else
                return null;
        }
        set
        {
            ViewState["dtHeader"] = value;
        }
    }

    private DataTable dtTopic
    {
        get
        {
            if ((DataTable)ViewState["dtTopic"] != null)
                return (DataTable)ViewState["dtTopic"];
            else
                return null;
        }
        set
        {
            ViewState["dtTopic"] = value;
        }
    }
    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }

    private DataTable dtRequire
    {
        get
        {
            if ((DataTable)ViewState["dtRequire"] != null)
                return (DataTable)ViewState["dtRequire"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequire"] = value;
        }
    }
    private DataTable dtTruck
    {
        get
        {
            if ((DataTable)ViewState["dtTruck"] != null)
                return (DataTable)ViewState["dtTruck"];
            else
                return null;
        }
        set
        {
            ViewState["dtTruck"] = value;
        }
    }
    private DataTable Valuecapacity
    {
        get
        {
            if ((DataTable)ViewState["Valuecapacity"] != null)
                return (DataTable)ViewState["Valuecapacity"];
            else
                return null;
        }
        set
        {
            ViewState["Valuecapacity"] = value;
        }
    }
    private string V_TRUCKID
    {
        get
        {
            if ((string)ViewState["V_TRUCKID"] != null)
                return (string)ViewState["V_TRUCKID"];
            else
                return null;
        }
        set
        {
            ViewState["V_TRUCKID"] = value;
        }
    }
    private string VendorID
    {
        get
        {
            if ((string)ViewState["VendorID"] != null)
                return (string)ViewState["VendorID"];
            else
                return null;
        }
        set
        {
            ViewState["VendorID"] = value;
        }

    }

    private DataTable dtUploadTypeTPN
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTPN"] != null)
                return (DataTable)ViewState["dtUploadTypeTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTPN"] = value;
        }
    }

    private DataTable dtUploadTPN
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTPN"] != null)
                return (DataTable)ViewState["dtUploadTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTPN"] = value;
        }
    }

    private DataTable dtRequestFileTPN
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFileTPN"] != null)
                return (DataTable)ViewState["dtRequestFileTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFileTPN"] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        CultureInfo ThaiCulture = new CultureInfo("th-TH");

        //Get User expire
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
        }
        else
        {
            CGROUP = Session["CGROUP"].ToString();
            SVDID = Session["SVDID"].ToString();
        }
        cmbCarcate.Enabled = false;
        if (!IsPostBack)
        {

            DataTable dtWarning = MonthlyReportBLL.Instance.TruckSelectSpecialValueBLL();
            lblWarning.Text = "หมายเหตุ ในกรณีที่ยังไม่ได้รับ ธพ.น.2 สามารถแนบ ธพ.น.3 (แบบคําขอต่ออายุใบอนุญาตประกอบกิจการควบคุมประเภทที่ ๓) ทดแทนก่อนได้ และขอความอนุเคราะห์แนบ เอกสาร ธพ.น.2 ปี " + DateTime.Now.ToString("yyyy", ThaiCulture) + " ก่อนวันที " + dtWarning.Rows[0]["SPECIALDATE"];
            string str = Request.QueryString["str"];
            #region BindData
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                str = decryptedValue;

                decryptedBytes = MachineKey.Decode(Request.QueryString["STTRUCKID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                V_TRUCKID = decryptedValue;

                decryptedBytes = MachineKey.Decode(Request.QueryString["CarTypeID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                Session["SCARTYPEID"] = "" + decryptedValue;                
                DataTable TableDataHeader = CarBLL.Instance.LoadDataHeader(V_TRUCKID);
                DataTable TableDataInsurance = CarBLL.Instance.LoadDataInsurance(V_TRUCKID);
                DataTable TableDataCompart = CarBLL.Instance.LoadDataCompart(V_TRUCKID);

                //Get Doc MV
                DataTable dtMVDoc = CarBLL.Instance.GetDocMVBLL(V_TRUCKID);
                GridViewHelper.BindGridView(ref dgvMV, dtMVDoc);

                Valuecapacity = TableDataCompart;
                //ตัวแปรเก็บ Vendor PTT
                VendorID = string.Equals(TableDataHeader.Rows.Count.ToString(), "0") ? "" : TableDataHeader.Rows[0]["STRANSPORTID"].ToString();
                this.InitialForm(CGROUP, TableDataHeader);

                InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");

                this.bindDataForm(V_TRUCKID, TableDataHeader, TableDataInsurance, TableDataCompart);
				cActive.Value = TableDataHeader.Rows[0].Field<string>("CACTIVE");
                //ข้อมูล Max Colomns
                if (TableDataHeader.Rows[0]["SCARTYPEID"].ToString() != "3")
                {
                    if (!string.Equals(TableDataCompart.Rows.Count.ToString(), "0"))
                    {
                        DataTable MaxValue = TableDataCompart.Select("NPANLEVEL > 0", "NPANLEVEL DESC").CopyToDataTable();
                        int MaxColumn = int.Parse(MaxValue.Rows[0]["NPANLEVEL"].ToString());
                        capacityCell.SelectedValue = MaxColumn.ToString();
                        table_capacity.Rows.Clear();
                        CreateHeadCapacity(int.Parse(capacityCell.SelectedValue));
                        if (ddl_addition.SelectedValue.ToString() == null) ddl_addition.SelectedValue = TableDataCompart.Rows.Count.ToString();
                        GetDataCapacity(Valuecapacity, int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));
                    }
                    else
                    {
                        table_capacity.Rows.Clear();
                        CreateHeadCapacity(1);
                        this.GenRowCapacity(1,1);
                    }
                }
            }
            #endregion
            dtRequestFile = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'TRUCK' AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 0");
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            if (cmbCarcate.SelectedValue == "0")
            {
                dtRequestFileTPN = MonthlyReportBLL.Instance.TruckTPNRequestFileBLL();
                this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);
            }
            else if (cmbCarcate.SelectedValue == "3")
            {
                dtRequestFileTPN = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'TRUCK' AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 1");
                this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);
            }
            else if (cmbCarcate.SelectedValue == "4")
            {
                dtRequestFileTPN = MonthlyReportBLL.Instance.TruckTPNRequestFileBLL();
                this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);
            }

            this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
            this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);
        }
        else
        {
            table_capacity.Rows.Clear();
            CreateHeadCapacity(int.Parse(capacityCell.SelectedValue));
            this.GenRowCapacity(int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));
        }

		/*******************/
		#region SetPermissionControl
		//Check รถอนุมัติ
		if(cActive.Value == "Y")
		{
			//Check รถในสัญญา
			if(ContractBLL.Instance.CheckTruckInContract(V_TRUCKID))
			{
				SetPermissionControl(true, true);
			}
			else
			{
				SetPermissionControl(true, false);
			}
		}
		else
		{
			SetPermissionControl(false, false);
		} 
		#endregion
    }

    private void CheckUploadFileMV(int TruckType, DataTable dtUpload)
    {
        try
        {
            lblPlaceMV.Visible = true;
            radPlaceMV.Visible = true;

            if (radSpot.SelectedIndex == 0)
            {
                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    if (string.Equals(dtUpload.Rows[i]["UPLOAD_ID"].ToString(), "41"))
                    {
                        dtUpload.Rows.RemoveAt(i);

                        radPlaceMV.ClearSelection();
                        lblPlaceMV.Visible = false;
                        radPlaceMV.Visible = false;

                        break;
                    }
                }
            }
            else if (radSpot.SelectedIndex == 1)
            {
                if (TruckType == 2)                     //หางลาก
                {
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        if (string.Equals(dtUpload.Rows[i]["UPLOAD_ID"].ToString(), "41"))
                        {
                            dtUpload.Rows.RemoveAt(i);

                            radPlaceMV.ClearSelection();
                            lblPlaceMV.Visible = false;
                            radPlaceMV.Visible = false;

                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region bindDataForm
    private void bindDataForm(string TTRUCKID, DataTable TableDataHeader, DataTable TableDataInsurance, DataTable TableDataCompart)
	{
		string TypeId = TableDataHeader.Rows[0]["SCARTYPEID"].ToString();
		//ส่งทะเบียนรถ
		registerno = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString();
		ComplainImportFileBLL complainImportFileBLL = new ComplainImportFileBLL();
		DataTable Blacklist_ds;
		int tmp;
		//ข้อมูลวัดน้ำ

		placewater.SelectedValue = TableDataHeader.Rows[0]["PLACE_WATER_MEASURE"].ToString();

        radPlaceMV.SelectedValue = TableDataHeader.Rows[0]["PLACE_WATER_MEASURE"].ToString();

        txtcodewater.Text = TableDataHeader.Rows[0]["SLAST_REQ_ID"].ToString();
		scale_start.Text = string.Equals(TableDataHeader.Rows[0]["DPREV_SERV"].ToString(), string.Empty) ? "" : Convert.ToDateTime(TableDataHeader.Rows[0]["DPREV_SERV"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
		scale_end.Text = string.Equals(TableDataHeader.Rows[0]["DWATEREXPIRE"].ToString(), string.Empty) ? "" : Convert.ToDateTime(TableDataHeader.Rows[0]["DWATEREXPIRE"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
		switch(TypeId)
		{
			case "0":
				Truck.Visible = true;
				insurance.Visible = true;
				carrier.Visible = true;
				capacity.Visible = true;
				semitruck.Visible = false;
				document.Visible = true;
				companyTank.Visible = true;
				//bindValue
				cmbCarcate.SelectedValue = TypeId;
				txtHRegNo.Text = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString();
				first_regis.Text = Convert.ToDateTime(TableDataHeader.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				cmbHsHolder.Text = TableDataHeader.Rows[0]["SOWNER_NAME"].ToString();
				cmbHRoute.SelectedValue = TableDataHeader.Rows[0]["ROUTE"].ToString();
				cmbHTru_Cate.SelectedValue = TableDataHeader.Rows[0]["TRUCK_CATEGORY"].ToString();
				txtHsEngine.Text = TableDataHeader.Rows[0]["SENGINE"].ToString();
				txtHsModel.SelectedValue = TableDataHeader.Rows[0]["SMODEL"].ToString();
				txtHsChasis.Text = TableDataHeader.Rows[0]["SCHASIS"].ToString();
				cboHsVibration.SelectedValue = TableDataHeader.Rows[0]["SVIBRATION"].ToString();
				wheel.SelectedValue = TableDataHeader.Rows[0]["NWHEELS"].ToString();
				cboPressure.SelectedValue = TableDataHeader.Rows[0]["MATERIALOFPRESSURE"].ToString();
				txtRnShaftDriven.Text = TableDataHeader.Rows[0]["NSHAFTDRIVEN"].ToString();
				txtRwightDriven.SelectedValue = TableDataHeader.Rows[0]["RWIGHTDRIVEN"].ToString();
				txtRnWeight.Text = TableDataHeader.Rows[0]["NWEIGHT"].ToString();
				cboHPumpPower_type.SelectedValue = TableDataHeader.Rows[0]["PUMPPOWER_TYPE"].ToString();
				txtVEH_Volume.SelectedValue = TableDataHeader.Rows[0]["VOL_UOM"].ToString();
				cboHGPSProvider.SelectedValue = TableDataHeader.Rows[0]["GPS_SERVICE_PROVIDER"].ToString();
				TruckTank.Text = TableDataHeader.Rows[0]["STANK_MAKER"].ToString();
				materialTank.SelectedValue = TableDataHeader.Rows[0]["STANK_MATERAIL"].ToString();
				txtHPowermover.Text = TableDataHeader.Rows[0]["POWERMOVER"].ToString();
				if(int.TryParse(TableDataHeader.Rows[0]["VALVETYPE"].ToString(), out tmp))
				{
					rblRValveType.SelectedValue = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
				}
				else
				{
					rblRValveType.SelectedValue = "3";
					txtRValveType.Text = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
				}
				if(int.TryParse(TableDataHeader.Rows[0]["FUELTYPE"].ToString(), out tmp))
				{

					rblRFuelType.SelectedValue = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
				}
				else
				{
					rblRFuelType.SelectedValue = "3";
					txtRFuelType.Text = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
				}

				cboHLoadMethod.SelectedValue = TableDataHeader.Rows[0]["SLOADING_METHOD"].ToString();
				ddl_addition.SelectedValue = TableDataHeader.Rows[0]["NSLOT"].ToString();
				type_vendor.SelectedValue = TableDataHeader.Rows[0]["ISUSE"].ToString();

                //เอกสารอัพโหลด
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(TTRUCKID, "TRUCK", "0");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                //เอกสารอัพโหลด
                dtUploadTPN = complainImportFileBLL.ImportFileSelectBLL3(TTRUCKID, "TRUCK", "1");
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                radSpot.SelectedValue = TableDataHeader.Rows[0]["SPOT_STATUS"].ToString();

                //บริษัทประกัน
                if (TableDataInsurance.Rows.Count > 0)
				{
					company_name.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SCOMPANY"].ToString();
					detailinsurance.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SDETAIL"].ToString();
					start_duration.Text = string.Equals(TableDataInsurance.Rows[0]["START_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
					end_duration.Text = string.Equals(TableDataInsurance.Rows[0]["END_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
					budget.Text = TableDataInsurance.Rows[0]["INSURE_BUDGET"].ToString();
					ninsure.Text = TableDataInsurance.Rows[0]["NINSURE"].ToString();
					ddlTypeInsurance.SelectedValue = TableDataInsurance.Rows[0]["INSURE_TYPE"].ToString();
					string[] sHolding = TableDataInsurance.Rows[0]["HOLDING"].ToString().Split(',');
					if(!string.IsNullOrEmpty(sHolding[0]))
					{
						foreach(ListItem items in cbHolding.Items)
						{
							for(int i = 0; i < sHolding.Length; i++)
							{
								if(items.Value == sHolding[i])
									items.Selected = true;
							}
						}
					}
				}
				//blacklist

				break;
			case "3":
				Truck.Visible = true;
				insurance.Visible = true;
				carrier.Visible = true;
				capacity.Visible = false;
				semitruck.Visible = false;
				document.Visible = true;
				companyTank.Visible = false;
				scalewater.Visible = false;
				//bindValue
				cmbCarcate.SelectedValue = TypeId;
				first_regis.Text = Convert.ToDateTime(TableDataHeader.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				txtHRegNo.Text = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString();
				cmbHsHolder.Text = TableDataHeader.Rows[0]["SOWNER_NAME"].ToString();
				cmbHRoute.SelectedValue = TableDataHeader.Rows[0]["ROUTE"].ToString();
				txtHsEngine.Text = TableDataHeader.Rows[0]["SENGINE"].ToString();
				cmbHTru_Cate.SelectedValue = TableDataHeader.Rows[0]["TRUCK_CATEGORY"].ToString();
				txtHsModel.Text = TableDataHeader.Rows[0]["SMODEL"].ToString();
				txtHsChasis.Text = TableDataHeader.Rows[0]["SCHASIS"].ToString();
				cboHsVibration.SelectedValue = TableDataHeader.Rows[0]["SVIBRATION"].ToString();
				wheel.SelectedValue = TableDataHeader.Rows[0]["NWHEELS"].ToString();
				cboPressure.SelectedValue = TableDataHeader.Rows[0]["MATERIALOFPRESSURE"].ToString();
				txtRnShaftDriven.Text = TableDataHeader.Rows[0]["NSHAFTDRIVEN"].ToString();
				txtRwightDriven.SelectedValue = TableDataHeader.Rows[0]["RWIGHTDRIVEN"].ToString();
				txtRnWeight.Text = TableDataHeader.Rows[0]["NWEIGHT"].ToString();
				cboHPumpPower_type.SelectedValue = TableDataHeader.Rows[0]["PUMPPOWER_TYPE"].ToString();
				txtVEH_Volume.SelectedValue = TableDataHeader.Rows[0]["VOL_UOM"].ToString();
				cboHGPSProvider.SelectedValue = TableDataHeader.Rows[0]["GPS_SERVICE_PROVIDER"].ToString();
				txtHPowermover.Text = TableDataHeader.Rows[0]["POWERMOVER"].ToString();
				if(int.TryParse(TableDataHeader.Rows[0]["VALVETYPE"].ToString(), out tmp))
				{
					rblRValveType.SelectedValue = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
				}
				else
				{
					rblRValveType.SelectedValue = "3";
					txtRValveType.Text = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
				}
				if(int.TryParse(TableDataHeader.Rows[0]["FUELTYPE"].ToString(), out tmp))
				{

					rblRFuelType.SelectedValue = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
				}
				else
				{
					rblRFuelType.SelectedValue = "3";
					txtRFuelType.Text = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
				}
                //เอกสารอัพโหลด
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(TTRUCKID, "TRUCK", "0");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                //เอกสารอัพโหลด
                dtUploadTPN = complainImportFileBLL.ImportFileSelectBLL3(TTRUCKID, "TRUCK", "1");
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                radSpot.SelectedValue = TableDataHeader.Rows[0]["SPOT_STATUS"].ToString();

                //สถานะรถ
                type_vendor.SelectedValue = TableDataHeader.Rows[0]["ISUSE"].ToString();
				//บริษัทประกัน
				if(TableDataInsurance.Rows.Count > 0)
				{
					company_name.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SCOMPANY"].ToString();
					detailinsurance.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SDETAIL"].ToString();
					start_duration.Text = string.Equals(TableDataInsurance.Rows[0]["START_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
					end_duration.Text = string.Equals(TableDataInsurance.Rows[0]["END_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
					budget.Text = TableDataInsurance.Rows[0]["INSURE_BUDGET"].ToString();
					ninsure.Text = TableDataInsurance.Rows[0]["NINSURE"].ToString();
					ddlTypeInsurance.SelectedValue = TableDataInsurance.Rows[0]["INSURE_TYPE"].ToString();
					string[] sHolding = TableDataInsurance.Rows[0]["HOLDING"].ToString().Split(',');
					if(!string.IsNullOrEmpty(sHolding[0]))
					{
						foreach(ListItem items in cbHolding.Items)
						{
							for(int i = 0; i < sHolding.Length; i++)
							{
								if(items.Value == sHolding[i])
									items.Selected = true;
							}
						}
					}
				}

				//blacklist

				break;
			case "4":
				semitruck.Visible = true;
				insurance.Visible = true;
				carrier.Visible = true;
				capacity.Visible = true;
				Truck.Visible = false;
				document.Visible = true;
				cmbCarcate.SelectedValue = TypeId;
				semifirst_regis.Text = Convert.ToDateTime(TableDataHeader.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				txtSemiNo.Text = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString();
				cmbSemiHolder.Text = TableDataHeader.Rows[0]["SOWNER_NAME"].ToString();
				cmbCarSemiSap.SelectedValue = TableDataHeader.Rows[0]["TRUCK_CATEGORY"].ToString();
				txtSemiChasis.Text = TableDataHeader.Rows[0]["SCHASIS"].ToString();
				txtsemimodal.SelectedValue = TableDataHeader.Rows[0]["SMODEL"].ToString();
				SemiWheel.SelectedValue = TableDataHeader.Rows[0]["NWHEELS"].ToString();
				txtSemiShaftDriven.Text = TableDataHeader.Rows[0]["NSHAFTDRIVEN"].ToString();
				rblSemiPumpPower.SelectedValue = TableDataHeader.Rows[0]["PUMPPOWER"].ToString();
				txtSemiWeight.Text = TableDataHeader.Rows[0]["NWEIGHT"].ToString();
				txtownTank.Text = TableDataHeader.Rows[0]["STANK_MAKER"].ToString();
				cboRTankMaterial.SelectedValue = TableDataHeader.Rows[0]["STANK_MATERAIL"].ToString();
				cboHLoadMethod.SelectedValue = TableDataHeader.Rows[0]["SLOADING_METHOD"].ToString();
				ddl_addition.SelectedValue = TableDataHeader.Rows[0]["NSLOT"].ToString();
                //เอกสารอัพโหลด
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(TTRUCKID, "TRUCK", "0");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                //เอกสารอัพโหลด
                dtUploadTPN = complainImportFileBLL.ImportFileSelectBLL3(TTRUCKID, "TRUCK", "1");
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                radSpot.SelectedValue = TableDataHeader.Rows[0]["SPOT_STATUS"].ToString();

                //ข้อมูลสถานะรถ
                semitype_vendor.SelectedValue = TableDataHeader.Rows[0]["ISUSE"].ToString();
				//บริษัทประกัน
				//บริษัทประกัน
				if(TableDataInsurance.Rows.Count > 0)
				{
					company_name.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SCOMPANY"].ToString();
					detailinsurance.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SDETAIL"].ToString();
					start_duration.Text = string.Equals(TableDataInsurance.Rows[0]["START_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
					end_duration.Text = string.Equals(TableDataInsurance.Rows[0]["END_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
					budget.Text = TableDataInsurance.Rows[0]["INSURE_BUDGET"].ToString();
					ninsure.Text = TableDataInsurance.Rows[0]["NINSURE"].ToString();
					ddlTypeInsurance.SelectedValue = TableDataInsurance.Rows[0]["INSURE_TYPE"].ToString();
					string[] sHolding = TableDataInsurance.Rows[0]["HOLDING"].ToString().Split(',');
					if(!string.IsNullOrEmpty(sHolding[0]))
					{
						foreach(ListItem items in cbHolding.Items)
						{
							for(int i = 0; i < sHolding.Length; i++)
							{
								if(items.Value == sHolding[i])
									items.Selected = true;
							}
						}
					}
				}
				//blacklist


				break;
			default:
				semitruck.Visible = false;
				insurance.Visible = false;
				carrier.Visible = false;
				capacity.Visible = false;
				Truck.Visible = false;
				document.Visible = false;
				break;

		}
	} 
	#endregion
	#region InitialForm
	private void InitialForm(string CGROUP, DataTable TableDataHeader)
	{
		try
		{
			switch(CGROUP)
			{

				case "0":
					DataTable dt_vendor = VendorBLL.Instance.SelectName(SVDID);
					NameVendor.Text = dt_vendor.Rows[0]["SABBREVIATION"].ToString();
					STRANSPORTID.Visible = false;
					owncar.Visible = true;
					semiowncar.Visible = true;
					scalewater.Visible = false;

					break;
				case "2":

					STRANSPORTID.Visible = true;
					NameVendor.Visible = false;
					break;
				case "1":
					DataTable test = VendorBLL.Instance.SelectName("");
					STRANSPORTID.Visible = true;

					owncar.Visible = false;
					semiowncar.Visible = false;
					scalewater.Visible = true;
					divcomment.Visible = string.Equals(TableDataHeader.Rows[0]["CACTIVE"].ToString(), "Y") ? false : true;
					try
					{
						STRANSPORTID.SelectedValue = VendorID;
						STRANSPORTID.Enabled = false;

					}
					catch(Exception)
					{

						STRANSPORTID.SelectedValue = "";
					}

					NameVendor.Visible = false;
					break;
				default:
					break;
			}
			this.InitialData();
			this.InitialUpload();

		}
		catch(Exception ex)
		{
			alertFail(ex.Message);
		}
	} 
	#endregion
    #region Upload
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                if (!string.Equals(txtSuggestFileName.Text.Trim(), System.IO.Path.GetFileNameWithoutExtension(FileNameUser)))
                    throw new Exception("ชื่อไฟล์ไม่ถูกต้อง ตามที่กำหนด");

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
            }
        }
        catch (Exception ex)
        {
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Truck" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

            this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string FileName = dgvUploadFile.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName);
    }

    private void DownloadFile(string Path, string FileName, string FileNameDownload)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileNameDownload);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL2("TRUCK", "0");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUploadTypeTPN = UploadTypeBLL.Instance.UploadTypeSelectBLL3("TRUCK", "1");
        DropDownListHelper.BindDropDownList(ref ddlUploadTypeTPN, dtUploadTypeTPN, "UPLOAD_ID", "UPLOAD_NAME", true);

        if (dtUpload == null || dtUpload.Rows.Count == 0)
        {
            dtUpload = new DataTable();
            dtUpload.Columns.Add("UPLOAD_ID");
            dtUpload.Columns.Add("UPLOAD_NAME");
            dtUpload.Columns.Add("FILENAME_SYSTEM");
            dtUpload.Columns.Add("FILENAME_USER");
            dtUpload.Columns.Add("FULLPATH");
        }

        if (dtUploadTPN == null || dtUploadTPN.Rows.Count == 0)
        {
            dtUploadTPN = new DataTable();
            dtUploadTPN.Columns.Add("UPLOAD_ID");
            dtUploadTPN.Columns.Add("UPLOAD_NAME");
            dtUploadTPN.Columns.Add("FILENAME_SYSTEM");
            dtUploadTPN.Columns.Add("FILENAME_USER");
            dtUploadTPN.Columns.Add("FULLPATH");
            dtUploadTPN.Columns.Add("START_DATE");
            dtUploadTPN.Columns.Add("STOP_DATE");
            dtUploadTPN.Columns.Add("DOC_NUMBER");
            dtUploadTPN.Columns.Add("YEAR");
        }
    }
    #endregion
	#region InitialData
	protected void InitialData()
	{
		try
		{
			DataTable dt = CarBLL.Instance.LoadDataRoute();
			DataTable dt_CartypeSap = CarBLL.Instance.LoadDataCarSap();
			DataTable dt_typecar = CarBLL.Instance.LoadDataCartype();
			DataTable dt_brand = CarBLL.Instance.LoadBrand();
			DataTable dt_gps = CarBLL.Instance.LoadGps();
			// รอรับการเลือกพนักงานว่าสังกัดพนักงานอะไร
			string condition = string.Empty;
			DataTable dt_vendor = VendorBLL.Instance.SelectName(condition);
			DropDownListHelper.BindDropDownList(ref cmbCarcate, dt_typecar, "config_value", "config_name", true);
			DropDownListHelper.BindDropDownList(ref cmbHRoute, dt, "ROUTE", "DESCRIPTION", false);
			DropDownListHelper.BindDropDownList(ref cmbHTru_Cate, dt_CartypeSap, "TYPE_ID", "TYPE_NAME", false);
			DropDownListHelper.BindDropDownList(ref cmbCarSemiSap, dt_CartypeSap, "TYPE_ID", "TYPE_NAME", false);
			DropDownListHelper.BindDropDownList(ref txtHsModel, dt_brand, "BRANDID", "BRANDNAME", true);
			DropDownListHelper.BindDropDownList(ref txtsemimodal, dt_brand, "BRANDID", "BRANDNAME", false);
			DropDownListHelper.BindDropDownList(ref cboHGPSProvider, dt_gps, "GPSID", "GPSNAME", true);
			DropDownListHelper.BindDropDownList(ref STRANSPORTID, dt_vendor, "SVENDORID", "SABBREVIATION", true);
			this.GetCapacityNum(ref ddl_addition, 20);
			this.CalculateWheel(wheel, 26);
			this.CalculateWheel(SemiWheel, 26);
			this.SetShaftDriven(txtRnShaftDriven, 10);
			this.SetShaftDriven(txtRwightDriven, 10);
			this.SetShaftDriven(txtSemiShaftDriven, 10);
		}
		catch(Exception ex)
		{
			alertFail(ex.Message);
		}
	} 
	#endregion
    #region RowAndDataCapacity สร้าง ตาราง
    #region InitalTable
    public void CreateHeadCapacity(int Columns)
    {
        if (!int.Equals(Columns, 0))
        {
            TableRow tRow;
            // Total number of rows.
            int cellCtr;
            // Current cell counter.
            int cellCnt;
            cellCnt = Columns;
            // Create a new row and add it to the table.
            tRow = new TableRow();

            TableCell tCell = new TableCell();
            Label label = new Label();
            label.Text = "CompartmentNo.";

            tCell.Controls.Add(label);
            tCell.HorizontalAlign = HorizontalAlign.Center;
            tCell.Width = Unit.Pixel(150);
            tRow.Cells.Add(tCell);
            //กำหนดาช่องเติม
            string stringText = "";

            for (cellCtr = 1; cellCtr <= cellCnt; cellCtr++)
            {

                tCell = new TableCell();
                switch (cellCtr)
                {
                    case 1:
                        stringText = " (High) ";
                        break;
                    case 2:
                        stringText = " (Low) ";
                        break;
                    case 3:
                        stringText = " (Middle) ";
                        break;
                    default:
                        break;
                }
                label = new Label();
                // tCell = new TableCell();
                label.Text = "ระดับแป้นที่" + cellCtr.ToString() + stringText.ToString();
                tCell.Controls.Add(label);
                tCell.HorizontalAlign = HorizontalAlign.Center;
                tRow.Cells.Add(tCell);
            }
            tRow.Font.Bold = true;
            table_capacity.Rows.Add(tRow);

        }
    }
    public void GenRowCapacity(int RowsData, int ColumnData)
    {
        // Total number of rows.
        int rowCnt;
        // Current row count.
        int rowCtr;
        // Total number of cells per row (columns).
        int cellCtr;
        // Current cell counter.
        int cellCnt;
        rowCnt = RowsData;
        cellCnt = ColumnData;
        for (rowCtr = 1; rowCtr < rowCnt + 1; rowCtr++)
        {
            // Create a new row and add it to the table.
            //No.
            TableRow tRow = new TableRow();
            TableCell tCell = new TableCell();
            Label label = new Label();
            label.Text = "ช่องที่ " + rowCtr;
            tCell.Controls.Add(label);
            tCell.HorizontalAlign = HorizontalAlign.Center;
            tCell.Width = Unit.Pixel(150);
            tRow.Cells.Add(tCell);
            table_capacity.Rows.Add(tRow);
            for (cellCtr = 1; cellCtr < cellCnt + 1; cellCtr++)
            {

                // Create a new cell and add it to the row.
                tCell = new TableCell();
                tCell.HorizontalAlign = HorizontalAlign.Center;
                tRow.Cells.Add(tCell);
                // Mock up a product ID.

                TextBox txt = new TextBox();
                txt.Width = 150;
                txt.EnableViewState = true;
                txt.CssClass = "form-control number placehoderKg";
                txt.MaxLength = 5;
                tCell.HorizontalAlign = HorizontalAlign.Center;
                //txt.Text = "TxT" + (i + 1).ToString() + "_" + (j + 1).ToString();
                txt.ID = "TxT" + (rowCtr + 1).ToString() + "_" + (cellCtr + 1).ToString();
                // pnl.Controls.Add(txt);
                tCell.Controls.Add(txt);
            }
        }
    }
    #region GETDATACAPACITY
    public void GetDataCapacity(DataTable Valuecapacity, int RowsData, int ColumnData)
    {
        // Total number of rows.
        int rowCnt;
        // Current row count.
        int rowCtr;
        // Total number of cells per row (columns).
        int cellCtr;
        // Current cell counter.
        int cellCnt;
        rowCnt = RowsData;
        cellCnt = ColumnData;
        int RowIndex = 0;
        for (rowCtr = 1; rowCtr < rowCnt + 1; rowCtr++)
        {
            // Create a new row and add it to the table.
            //No.
            TableRow tRow = new TableRow();
            TableCell tCell = new TableCell();
            Label label = new Label();
            label.Text = "ช่องที่ " + rowCtr;
            tCell.Controls.Add(label);
            tCell.HorizontalAlign = HorizontalAlign.Center;
            tCell.Width = Unit.Pixel(150);
            tRow.Cells.Add(tCell);
            table_capacity.Rows.Add(tRow);
            for (cellCtr = 1; cellCtr < cellCnt + 1; cellCtr++)
            {

                // Create a new cell and add it to the row.
                tCell = new TableCell();
                tRow.Cells.Add(tCell);
                // Mock up a product ID.

                TextBox txt = new TextBox();
                txt.Width = 150;
                txt.EnableViewState = true;
                txt.MaxLength = 5;
                txt.CssClass = "form-control number";
                txt.ID = "TxT" + (rowCtr + 1).ToString() + "_" + (cellCtr + 1).ToString();
                txt.Text = Valuecapacity.Rows[RowIndex]["NCAPACITY"].ToString();
                tCell.HorizontalAlign = HorizontalAlign.Center;
                // pnl.Controls.Add(txt);
                tCell.Controls.Add(txt);
                RowIndex++;
            }
        }
    }
    #endregion
    #endregion
    #region SetCapacityToDatatable
    public DataTable DataCapacity(int Columncapacity)
    {
        DataTable dt = new DataTable();
        DataTable Data_dt = new DataTable();
        // Get columns from first row
        if (table_capacity.Rows.Count > 0)
        {
            for (int i = 1; i < Columncapacity + 1; i++)
            {
                dt.Columns.Add("Column_" + i);
            }
            int rowIndex = 2;
            table_capacity.Rows.RemoveAt(0);
            foreach (TableRow row in table_capacity.Rows)
            {
                DataRow dr = dt.NewRow();
                for (int i = 1; i < Columncapacity + 1; i++)
                {
                    dr["Column_" + i] = ((TextBox)table_capacity.FindControl("TxT" + rowIndex + "_" + (i + 1))).Text;

                }
                dt.Rows.Add(dr);
                rowIndex++;
            }

        }
        int rowsindex = 0;
        Data_dt.Columns.Add("NCOMPARTNO");
        Data_dt.Columns.Add("NPANLEVEL");
        Data_dt.Columns.Add("NCAPACITY");
        for (int row = 0; row < dt.Rows.Count; row++)
        {
            for (int column = 1; column < dt.Columns.Count + 1; column++)
            {
                string value_capacity = dt.Rows[row]["Column_" + column].ToString();
                Data_dt.Rows.Add(rowsindex + 1, int.Parse(column.ToString()), value_capacity);
            }
            rowsindex++;
        }
        return Data_dt;
    }
    #endregion
    #endregion
    #region Miscellaneous ฟังชันเปลี่ยนค่า
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    private string RadioToTextBox(string p, string Returnstring)
    {
        if (string.Equals(p, "3"))
        {
            return Returnstring;
        }
        else
        {
            return p;
        }
    }

    private string nullvalue(string p)
    {
        if (string.Equals(p, ""))
        {
            return "0";
        }
        else
        {
            return p;
        }

    }
    //ฟังชันล้อ
    private void CalculateWheel(DropDownList dropdown, Int32 max_wheel)
    {
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 6; i <= max_wheel; i += 2)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    //สร้างแถว
    public void GetCapacityNum(ref DropDownList Name, int Num)
    {
        int count = 20;
        for (int i = 1; i < count + 1; i++)
        {

            Name.Items.Add(i.ToString());

        }

    }
    private void SetShaftDriven(DropDownList dropdown, int Num)
    {
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 1; i < Num; i++)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    #endregion
    #region Ajax
    [System.Web.Services.WebMethod]
    public static List<string> VendorJson(string Vendor)
    {
        DataTable dtVendor = VendorBLL.Instance.LoadNameVendor(Vendor);
        List<string> DataVendor = new List<string>();
        for (int i = 0; i < dtVendor.Rows.Count; i++)
        {
            
            DataVendor.Add(dtVendor.Rows[i][0].ToString());
        }

        return DataVendor;

    }
    #endregion
    private void ValidateUploadFile()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }

            StringBuilder sb2 = new StringBuilder();
            sb2.Append("<table>");
            if (dtUploadTPN.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFileTPN.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb2.Append("<tr>");
                        sb2.Append("<td>โปรดแนบเอกสาร</td>");
                        sb2.Append("<td>");
                        sb2.Append(">" + dtRequestFileTPN.Rows[j]["UPLOAD_NAME"].ToString());
                        sb2.Append("</td>");
                        sb2.Append("</tr>");
                    }
                    else
                    {
                        sb2.Append("<tr>");
                        sb2.Append("<td></td>");
                        sb2.Append("<td>");
                        sb2.Append(">" + dtRequestFileTPN.Rows[j]["UPLOAD_NAME"].ToString());
                        sb2.Append("</td>");
                        sb2.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFileTPN.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUploadTPN.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFileTPN.Rows[i]["UPLOAD_ID"].ToString(), dtUploadTPN.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUploadTPN.Rows.Count - 1)
                        {
                            string UploadIDTPN2 = string.Empty;
                            string UploadIDTPN3 = string.Empty;
                            bool IsFoundTPN3 = false;

                            for (int k = 0; k < dtUploadTypeTPN.Rows.Count; k++)
                            {//ถ้าไม่ได้แนบ ธพ.น.2 ให้แนบ ธพ.น.3 แทนได้
                                if (string.Equals(dtUploadTypeTPN.Rows[k]["SPECIAL_FLAG"].ToString(), "1"))
                                    UploadIDTPN2 = dtUploadTypeTPN.Rows[k]["UPLOAD_ID"].ToString();

                                if (string.Equals(dtUploadTypeTPN.Rows[k]["SPECIAL_FLAG"].ToString(), "2"))
                                    UploadIDTPN3 = dtUploadTypeTPN.Rows[k]["UPLOAD_ID"].ToString();
                            }

                            if (string.Equals(dtRequestFileTPN.Rows[i]["UPLOAD_ID"].ToString(), UploadIDTPN2))
                            {
                                for (int m = 0; m < dtUploadTPN.Rows.Count; m++)
                                {
                                    if (string.Equals(UploadIDTPN3, dtUploadTPN.Rows[m]["UPLOAD_ID"].ToString()))
                                    {
                                        IsFoundTPN3 = true;
                                        break;
                                    }
                                }
                            }

                            if (!IsFoundTPN3)
                            {
                                if (string.Equals(sb2.ToString(), "<table>"))
                                {
                                    sb2.Append("<tr>");
                                    sb2.Append("<td>โปรดแนบเอกสาร</td>");
                                    sb2.Append("<td>");
                                    sb2.Append(">" + dtRequestFileTPN.Rows[i]["UPLOAD_NAME"].ToString());
                                    sb2.Append("</td>");
                                    sb2.Append("</tr>");
                                }
                                else
                                {
                                    sb2.Append("<tr>");
                                    sb2.Append("<td></td>");
                                    sb2.Append("<td>");
                                    sb2.Append(">" + dtRequestFileTPN.Rows[i]["UPLOAD_NAME"].ToString());
                                    sb2.Append("</td>");
                                    sb2.Append("</tr>");
                                }
                            }
                        }
                    }
                }
            }
            sb2.Append("</table>");

            if (!string.Equals(sb2.ToString(), "<table></table>"))
            {
                throw new Exception(sb2.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region DataToDatatable
    #region บันทึกข้อมูลTRUCK
    protected DataTable DataToDatatable()
    {
        DataTable dt;
        switch (cmbCarcate.SelectedValue.ToString())
        {
            case "4":
                dt = new DataTable();
                dt.Columns.Add("TypeCar");
                dt.Columns.Add("semifirst_regis");
                dt.Columns.Add("txtHRegNo");
                dt.Columns.Add("cmbHsHolder");
                dt.Columns.Add("STRANSPORTID");
                dt.Columns.Add("cmbHTru_Cate");
                dt.Columns.Add("txtHsModel");
                dt.Columns.Add("cboHLoadMethod");
                dt.Columns.Add("txtRnShaftDriven");
                dt.Columns.Add("txtHsChasis");
                dt.Columns.Add("wheel");
                dt.Columns.Add("txtRnWeight");
                dt.Columns.Add("txtownTank");
                dt.Columns.Add("cboRTankMaterial");
                dt.Columns.Add("cboHPumpPower_type");
                dt.Columns.Add("nslot");
                dt.Columns.Add("sessionId");
                dt.Columns.Add("type_vendor");
                //ข้อมูลวัดน้ำ
                dt.Columns.Add("PLACE_WATER_MEASURE");
                dt.Columns.Add("SLAST_REQ_ID");
                dt.Columns.Add("DPREV_SERV");
                dt.Columns.Add("DWATEREXPIRE");
                dt.Rows.Add(
                    int.Parse(cmbCarcate.SelectedValue.ToString()),
                    DateTime.ParseExact(semifirst_regis.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    txtSemiNo.Text,
                    cmbSemiHolder.Text.ToString(),
                    string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID,
                    cmbCarSemiSap.SelectedValue.ToString(),
                    txtsemimodal.SelectedValue.ToString(),
                    cboHLoadMethod.SelectedValue.ToString(),
                    txtSemiShaftDriven.SelectedValue.ToString(),
                    txtSemiChasis.Text,
                    SemiWheel.SelectedValue,
                    txtSemiWeight.Text,
                    txtownTank.Text.Trim().ToString(),
                    cboRTankMaterial.SelectedValue,
                    rblSemiPumpPower.SelectedValue.ToString(),
                    ddl_addition.SelectedValue.ToString(),
                    Session["UserID"].ToString(),                    
                    semitype_vendor.SelectedValue.ToString(),
                    placewater.SelectedValue,
                    txtcodewater.Text.Trim(),
                    string.Equals(scale_start.Text, string.Empty) ? "" : DateTime.ParseExact(scale_start.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    string.Equals(scale_end.Text, string.Empty) ? "" : DateTime.ParseExact(scale_end.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture) 
                    );
                break;
            default:
                dt = new DataTable();
                dt.Columns.Add("TypeCar");
                dt.Columns.Add("first_regis");
                dt.Columns.Add("txtHRegNo");
                dt.Columns.Add("STRANSPORTID");
                dt.Columns.Add("cmbHsHolder");
                dt.Columns.Add("cmbHRoute");
                dt.Columns.Add("txtHsEngine");
                dt.Columns.Add("cmbHTru_Cate");
                dt.Columns.Add("cboHLoadMethod");
                dt.Columns.Add("txtHsModel");
                dt.Columns.Add("txtHsChasis");
                dt.Columns.Add("cboHsVibration");
                dt.Columns.Add("wheel");
                dt.Columns.Add("cboRMaterialOfPressure");
                dt.Columns.Add("txtRnShaftDriven");
                dt.Columns.Add("txtRwightDriven");
                dt.Columns.Add("txtRnWeight");
                dt.Columns.Add("cboHPumpPower_type");
                dt.Columns.Add("txtVEH_Volume");
                dt.Columns.Add("cboHGPSProvider");
                dt.Columns.Add("txtownTank");
                dt.Columns.Add("cboRTankMaterial");
                dt.Columns.Add("rblRValveType");
                dt.Columns.Add("rblRFuelType");
                dt.Columns.Add("nslot");
                dt.Columns.Add("sessionId");
                dt.Columns.Add("type_vendor");
                //ข้อมูลวัดน้ำ
                dt.Columns.Add("PLACE_WATER_MEASURE");
                dt.Columns.Add("SLAST_REQ_ID");
                dt.Columns.Add("DPREV_SERV");
                dt.Columns.Add("DWATEREXPIRE");
                dt.Rows.Add(
                    int.Parse(cmbCarcate.SelectedValue.ToString()),
                    DateTime.ParseExact(first_regis.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    txtHRegNo.Text,
                    string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID,
                    cmbHsHolder.Text.ToString(),
                    cmbHRoute.SelectedValue.ToString(),
                    txtHsEngine.Text,
                    cmbHTru_Cate.SelectedValue.ToString(),
                    cboHLoadMethod.SelectedValue.ToString(),
                    txtHsModel.SelectedValue,
                    txtHsChasis.Text,
                    cboHsVibration.SelectedValue,
                    wheel.SelectedValue,
                    cboPressure.SelectedValue.ToString(),
                    txtRnShaftDriven.SelectedValue,
                    txtRwightDriven.SelectedValue,
                    int.Parse(nullvalue(txtRnWeight.Text)),
                    cboHPumpPower_type.SelectedValue.ToString(),
                    txtVEH_Volume.SelectedValue.ToString(),
                    cboHGPSProvider.SelectedValue.ToString(),
                    TruckTank.Text.Trim().ToString(),
                    materialTank.SelectedValue.ToString(),
                    RadioToTextBox(rblRValveType.Text, txtRValveType.Text),
                    RadioToTextBox(rblRFuelType.Text.ToString(), txtRFuelType.Text),
                    ddl_addition.SelectedValue.ToString(),
                    Session["UserID"].ToString(),
                    type_vendor.SelectedValue.ToString(),
                    placewater.SelectedValue,
                    txtcodewater.Text.Trim(),
                    string.Equals(scale_start.Text, string.Empty) ? "" : DateTime.ParseExact(scale_start.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    string.Equals(scale_end.Text, string.Empty) ? "" : DateTime.ParseExact(scale_end.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture) 
                    );
                
                break;
        }
        return dt;

    }
    #endregion
    #region Insurance
    protected DataTable DataInsurance()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("SCOMPANY");
        dt.Columns.Add("SDETAIL");
        dt.Rows.Add(company_name.Text.Trim(), detailinsurance.Text.Trim()
       );
        return dt;
    }
    #endregion
   
    #endregion
    #region ResponseMenu
    protected void ResponseMenu(object sender, EventArgs e)
    {
        if (string.Equals(CGROUP.ToString(), "1"))
        {
            Response.Redirect("approve_pk.aspx");
        }
        else
        {
            Response.Redirect("vendor_request.aspx");

        }
    }
    #endregion
	#region SetPermissionControl
	public void SetPermissionControl(bool isActive, bool isContract)
	{
		if(isActive)
		{
			if(CGROUP == "0")
			{
				#region ผู้ขนส่ง
				if(isContract)
				{
					#region อนุมัติแล้ว อยู่ในสัญญา
					//หัวลาก
					txtHRegNo.ReadOnly = true;
					txtHsEngine.ReadOnly = true;
					txtHsChasis.ReadOnly = true;
					txtRnShaftDriven.Enabled = false;
					txtRwightDriven.Enabled = false;
					first_regis.Enabled = false;
					first_regis.ReadOnly = true;

					//หางลาก
					txtSemiNo.ReadOnly = true;
					txtSemiChasis.ReadOnly = true;
					txtSemiShaftDriven.Enabled = false;
					semifirst_regis.ReadOnly = true;
					semifirst_regis.Enabled = false;

					//ข้อมูลวัดน้ำ
					placewater.Enabled = false;
					txtcodewater.Enabled = false;
					scale_start.Enabled = false;
					scale_end.Enabled = false;

					//ข้อมูลช่องเติม
					Table6.Enabled = false;

					//ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
					txtcomment.ReadOnly = true;
					#endregion
				}
				else
				{
					#region อนุมัติแล้ว ไม่อยู่ในสัญญา
					//หัวลาก
					txtHRegNo.ReadOnly = true;
					txtHsEngine.ReadOnly = true;
					txtHsChasis.ReadOnly = true;
					txtRnShaftDriven.Enabled = true;
					txtRwightDriven.Enabled = true;
					first_regis.Enabled = false;
					first_regis.ReadOnly = true;

					//หางลาก
					txtSemiNo.ReadOnly = true;
					txtSemiChasis.ReadOnly = true;
					txtSemiShaftDriven.Enabled = true;
					semifirst_regis.ReadOnly = true;
					semifirst_regis.Enabled = false;

					//ข้อมูลวัดน้ำ
					placewater.Enabled = true;
					txtcodewater.Enabled = true;
					scale_start.Enabled = true;
					scale_end.Enabled = true;

					//ข้อมูลช่องเติม
					Table6.Enabled = true;

					//ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
					txtcomment.ReadOnly = true;
					#endregion
				}
				#endregion
			}
			else
			{
				#region ปตท.
				if(isContract)
				{
					#region อนุมัติแล้ว อยู่ในสัญญา
					//หัวลาก
					txtHRegNo.ReadOnly = true;
					txtHsEngine.ReadOnly = true;
					txtHsChasis.ReadOnly = true;
					txtRnShaftDriven.Enabled = true;
					txtRwightDriven.Enabled = true;
					first_regis.Enabled = false;
					first_regis.ReadOnly = true;

					//หางลาก
					txtSemiNo.ReadOnly = true;
					txtSemiChasis.ReadOnly = true;
					txtSemiShaftDriven.Enabled = true;
					semifirst_regis.ReadOnly = true;
					semifirst_regis.Enabled = false;

					//ข้อมูลวัดน้ำ
					placewater.Enabled = false;
					txtcodewater.Enabled = false;
					scale_start.Enabled = false;
					scale_end.Enabled = false;

					//ข้อมูลช่องเติม
					Table6.Enabled = true;

					//ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
					txtcomment.ReadOnly = true;
					#endregion
				}
				else
				{
					#region อนุมัติแล้ว ไม่อยู่ในสัญญา
					//หัวลาก
					txtHRegNo.ReadOnly = true;
					txtHsEngine.ReadOnly = true;
					txtHsChasis.ReadOnly = true;
					txtRnShaftDriven.Enabled = true;
					txtRwightDriven.Enabled = true;
					first_regis.Enabled = false;
					first_regis.ReadOnly = true;

					//หางลาก
					txtSemiNo.ReadOnly = true;
					txtSemiChasis.ReadOnly = true;
					txtSemiShaftDriven.Enabled = true;
					semifirst_regis.ReadOnly = true;
					semifirst_regis.Enabled = false;

					//ข้อมูลวัดน้ำ
					placewater.Enabled = true;
					txtcodewater.Enabled = true;
					scale_start.Enabled = true;
					scale_end.Enabled = true;

					//ข้อมูลช่องเติม
					Table6.Enabled = true;

					//ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
					txtcomment.ReadOnly = true;
					#endregion
				}
				#endregion
			}
		}
		else
		{
			if(CGROUP == "0")
			{
				#region ผู้ขนส่ง  สถานะไม่อนุมัติ
				//หัวลาก
				txtHRegNo.ReadOnly = false;
				txtHsEngine.ReadOnly = false;
				txtHsChasis.ReadOnly = false;
				txtRnShaftDriven.Enabled = true;
				txtRwightDriven.Enabled = true;
				first_regis.Enabled = true;
				first_regis.ReadOnly = false;

				//หางลาก
				txtSemiNo.ReadOnly = false;
				txtSemiChasis.ReadOnly = false;
				txtSemiShaftDriven.Enabled = true;
				semifirst_regis.ReadOnly = false;
				semifirst_regis.Enabled = true;

				//ข้อมูลวัดน้ำ
				placewater.Enabled = true;
				txtcodewater.Enabled = true;
				scale_start.Enabled = true;
				scale_end.Enabled = true;

				//ข้อมูลช่องเติม
				Table6.Enabled = true;

				//ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
				txtcomment.ReadOnly = true;
				#endregion
			}
			else
			{
				#region ปตท สถานะไม่อนุมัติ
				//หัวลาก
				txtHRegNo.ReadOnly = false;
				txtHsEngine.ReadOnly = false;
				txtHsChasis.ReadOnly = false;
				txtRnShaftDriven.Enabled = true;
				txtRwightDriven.Enabled = true;
				first_regis.Enabled = true;
				first_regis.ReadOnly = false;

				//หางลาก
				txtSemiNo.ReadOnly = false;
				txtSemiChasis.ReadOnly = false;
				txtSemiShaftDriven.Enabled = true;
				semifirst_regis.ReadOnly = false;
				semifirst_regis.Enabled = true;

				//ข้อมูลวัดน้ำ
				placewater.Enabled = true;
				txtcodewater.Enabled = true;
				scale_start.Enabled = true;
				scale_end.Enabled = true;

				//ข้อมูลช่องเติม
				Table6.Enabled = true;

				//ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
				txtcomment.ReadOnly = false;
				#endregion
			}
		}
	}
	#endregion

    protected void btnUploadTPN_Click(object sender, EventArgs e)
    {
        this.StartUploadTPN();
    }

    private void StartUploadTPN()
    {
        try
        {
            if (fileUploadTPN.HasFile)
            {

                if (string.Equals(txtDocUploadTPN.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน หมายเลขเอกสาร");

                string FileNameUser = fileUploadTPN.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                if (!string.Equals(txtSuggestFileNameTPN.Text.Trim(), System.IO.Path.GetFileNameWithoutExtension(FileNameUser)))
                    throw new Exception("ชื่อไฟล์ไม่ถูกต้อง ตามที่กำหนด");

                this.ValidateUploadFileTPN(System.IO.Path.GetExtension(FileNameUser), fileUploadTPN.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadTPN.SaveAs(Path + "\\" + FileNameSystem);

                string Year = string.Empty;
                if (ddlYearTPN.Items.Count > 0)
                    Year = ddlYearTPN.SelectedValue;

                string DateStart = string.Empty;
                if (!string.Equals(txtdateStartTPN.Text.Trim(), string.Empty))
                    DateStart = txtdateStartTPN.Text.Trim();

                string DateStop = string.Empty;
                if (!string.Equals(txtdateEndTPN.Text.Trim(), string.Empty))
                    DateStop = txtdateEndTPN.Text.Trim();

                dtUploadTPN.Rows.Add(ddlUploadTypeTPN.SelectedValue, ddlUploadTypeTPN.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem, DateStart, DateStop, txtDocUploadTPN.Text.Trim(), Year);
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);
            }

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFileTPN(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFileTPN_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string FullPath = dgvUploadFileTPN.DataKeys[e.RowIndex]["FULLPATH"].ToString();
            string FileName = dgvUploadFileTPN.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
            this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvUploadFileTPN_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadTPN.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

            this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvUploadFileTPN_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlUploadType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.Equals(cmbCarcate.SelectedValue, "4"))
        {
            if (string.Equals(txtHRegNo.Text.Trim(), string.Empty))
            {
                ddlUploadType.SelectedIndex = 0;
                throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
            }

            if (ddlUploadType.SelectedIndex < 1)
                txtSuggestFileName.Text = string.Empty;
            else
                txtSuggestFileName.Text = txtHRegNo.Text.Trim() + "_" + ddlUploadType.SelectedItem.ToString();
        }
        else
        {
            if (string.Equals(txtSemiNo.Text.Trim(), string.Empty))
            {
                ddlUploadType.SelectedIndex = 0;
                throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
            }

            if (ddlUploadType.SelectedIndex < 1)
                txtSuggestFileName.Text = string.Empty;
            else
                txtSuggestFileName.Text = txtSemiNo.Text.Trim() + "_" + ddlUploadType.SelectedItem.ToString();
        }
    }

    private void CheckRequestFile(GridView dgvRequestFile, DataTable dtRequestFile, DataTable dtUpload)
    {
        try
        {
            string UploadID = string.Empty;
            CheckBox chk = new CheckBox();

            for (int j = 0; j < dtRequestFile.Rows.Count; j++)
            {
                chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                if (chk != null)
                    chk.Checked = false;
            }

            for (int i = 0; i < dtUpload.Rows.Count; i++)
            {
                UploadID = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (string.Equals(UploadID, dtRequestFile.Rows[j]["UPLOAD_ID"].ToString()))
                    {
                        chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                        if (chk != null)
                            chk.Checked = true;
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlUploadTypeTPN_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(cmbCarcate.SelectedValue, "4"))
            {
                if (string.Equals(txtHRegNo.Text.Trim(), string.Empty))
                {
                    ddlUploadTypeTPN.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }
            }
            else
            {
                if (string.Equals(txtSemiNo.Text.Trim(), string.Empty))
                {
                    ddlUploadTypeTPN.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }
            }

            this.ClearUploadTPN();

            if (ddlUploadTypeTPN.SelectedIndex > 0)
            {
                if (!string.Equals(cmbCarcate.SelectedValue, "4"))
                    txtSuggestFileNameTPN.Text = txtHRegNo.Text.Trim() + "_" + ddlUploadTypeTPN.SelectedItem.ToString();
                else
                    txtSuggestFileNameTPN.Text = txtSemiNo.Text.Trim() + "_" + ddlUploadTypeTPN.SelectedItem.ToString();

                DataTable dtYear = new DataTable();
                dtYear.Columns.Add("YEAR");
                dtYear.Columns.Add("YEAR_DISPLAY");

                if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "1"))
                {//ธพ.น.2
                    dtYear.Rows.Add(DateTime.Now.ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.ToString("yyyy", new CultureInfo("en-US")));
                    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                    ddlYearTPN.SelectedIndex = 0;
                    ddlYearTPN.Enabled = true;

                    txtdateStartTPN.Text = "01/01/" + ddlYearTPN.SelectedValue;
                    txtdateEndTPN.Text = "31/12/" + ddlYearTPN.SelectedValue;
                }
                else if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "2"))
                {//ธพ.น.3
                    dtYear.Rows.Add(DateTime.Now.ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.ToString("yyyy", new CultureInfo("en-US")));
                    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                    ddlYearTPN.SelectedIndex = 0;
                    ddlYearTPN.Enabled = true;
                }
                else
                {
                    txtdateStartTPN.Enabled = true;
                    txtdateEndTPN.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ClearUploadTPN()
    {
        try
        {
            txtSuggestFileNameTPN.Text = string.Empty;
            txtDocUploadTPN.Text = string.Empty;

            ddlYearTPN.Items.Clear();
            ddlYearTPN.Enabled = false;

            txtdateStartTPN.Text = string.Empty;
            txtdateStartTPN.Enabled = false;

            txtdateEndTPN.Text = string.Empty;
            txtdateEndTPN.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlYearTPN_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            txtdateStartTPN.Text = "01/01/" + ddlYearTPN.SelectedValue;
            txtdateEndTPN.Text = "31/12/" + ddlYearTPN.SelectedValue;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvMV_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvMV.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string FileName = dgvMV.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName + Path.GetExtension(FullPath));
    }
}