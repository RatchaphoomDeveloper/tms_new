﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class admin_vendor_lst : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
       
        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            Session["oSVENDORID1"] = null;

            LogUser("39", "R", "เปิดดูข้อมูลหน้า ข้อมูลผู้ประกอบการ", "");

        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

        switch (cbxGroup.SelectedIndex)
        {
            case 0:
                sds.SelectCommand = "SELECT v.SVENDORID, s.SVENDORNAME, s.SPROVINCE, wm_concat(' ' || c.SCOORDINATORNAME) As SCONAMEAPPEND,wm_concat(' ' || c.STEL)  As SCOTELAPPEND FROM (TVENDOR v INNER JOIN TVENDOR_SAP s ON v.SVENDORID = s.SVENDORID) LEFT JOIN TCO_VENDOR c ON v.SVENDORID = c.SVENDORID WHERE nvl(V.CACTIVE,'Y') != 'N' AND NVL(v.CCATEGORY,'C') = 'C' and  s.SVENDORNAME LIKE '%' || :oSearch || '%' GROUP BY v.SVENDORID,s.SVENDORNAME, s.SPROVINCE";
                break;
            case 1:
                sds.SelectCommand = "SELECT v.SVENDORID, s.SVENDORNAME, s.SPROVINCE, wm_concat(' ' || c.SCOORDINATORNAME) As SCONAMEAPPEND,wm_concat(' ' || c.STEL)  As SCOTELAPPEND FROM (TVENDOR v INNER JOIN TVENDOR_SAP s ON v.SVENDORID = s.SVENDORID) LEFT JOIN TCO_VENDOR c ON v.SVENDORID = c.SVENDORID WHERE nvl(V.CACTIVE,'Y') != 'N' AND NVL(v.CCATEGORY,'C') = 'C' and  s.SPROVINCE LIKE '%' || :oSearch || '%' GROUP BY v.SVENDORID,s.SVENDORNAME, s.SPROVINCE";
                break;
        }
        sds.DataBind();
        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                break;

            case "edit":

                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SVENDORID");
                string stmID = Convert.ToString(data);

                Session["oSVENDORID1"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_Vendor_desc.aspx";

                break;

        }
    }
   
    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
