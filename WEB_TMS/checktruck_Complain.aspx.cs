﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Globalization;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using DevExpress.XtraEditors;
using System.Web.UI.HtmlControls;
using System.Drawing;
using EmailHelper;
using System.Configuration;
using System.Web.Security;
using TMS_BLL.Transaction.SurpriseCheck;

public partial class checktruck_Complain : PageBase
{
    #region + ViewState +
    private int SumCar
    {
        get
        {
            if ((int)ViewState["SumCar"] != null)
                return (int)ViewState["SumCar"];
            else
                return 0;
        }
        set
        {
            ViewState["SumCar"] = value;
        }
    }

    private int DocStatusID
    {
        get
        {
            if ((int)ViewState["DocStatusID"] != null)
                return (int)ViewState["DocStatusID"];
            else
                return 0;
        }
        set
        {
            ViewState["DocStatusID"] = value;
        }
    }

    private string CancelDoc
    {
        get
        {
            if ((string)ViewState["CancelDoc"] != null)
                return (string)ViewState["CancelDoc"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CancelDoc"] = value;
        }
    }

    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }

    private string SurpriseCheckID
    {
        get
        {
            if ((string)ViewState["SurpriseCheckID"] != null)
                return (string)ViewState["SurpriseCheckID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SurpriseCheckID"] = value;
        }
    }

    private DataTable dtCusScoreType
    {
        get
        {
            if ((DataTable)ViewState["dtCusScoreType"] != null)
                return (DataTable)ViewState["dtCusScoreType"];
            else
                return null;
        }
        set
        {
            ViewState["dtCusScoreType"] = value;
        }
    }

    private DataTable dtCarDistinct
    {
        get
        {
            if ((DataTable)ViewState["dtCarDistinct"] != null)
                return (DataTable)ViewState["dtCarDistinct"];
            else
                return null;
        }
        set
        {
            ViewState["dtCarDistinct"] = value;
        }
    }

    private DataTable dtScoreList
    {
        get
        {
            if ((DataTable)ViewState["dtScoreList"] != null)
                return (DataTable)ViewState["dtScoreList"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreList"] = value;
        }
    }

    private DataTable dtScoreListCar
    {
        get
        {
            if ((DataTable)ViewState["dtScoreListCar"] != null)
                return (DataTable)ViewState["dtScoreListCar"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreListCar"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtUploadTypeTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTab3"] != null)
                return (DataTable)ViewState["dtUploadTypeTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTab3"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtUploadTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTab3"] != null)
                return (DataTable)ViewState["dtUploadTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTab3"] = value;
        }
    }

    private DataTable dtScore
    {
        get
        {
            if ((DataTable)ViewState["dtScore"] != null)
                return (DataTable)ViewState["dtScore"];
            else
                return null;
        }
        set
        {
            ViewState["dtScore"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }
    private DataTable dtHeader
    {
        get
        {
            if ((DataTable)ViewState["dtHeader"] != null)
                return (DataTable)ViewState["dtHeader"];
            else
                return null;
        }
        set
        {
            ViewState["dtHeader"] = value;
        }
    }

    private DataTable dtTopic
    {
        get
        {
            if ((DataTable)ViewState["dtTopic"] != null)
                return (DataTable)ViewState["dtTopic"];
            else
                return null;
        }
        set
        {
            ViewState["dtTopic"] = value;
        }
    }

    private DataTable dtComplainType
    {
        get
        {
            if ((DataTable)ViewState["dtComplainType"] != null)
                return (DataTable)ViewState["dtComplainType"];
            else
                return null;
        }
        set
        {
            ViewState["dtComplainType"] = value;
        }
    }

    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }

    private DataTable dtRequire
    {
        get
        {
            if ((DataTable)ViewState["dtRequire"] != null)
                return (DataTable)ViewState["dtRequire"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequire"] = value;
        }
    }

    private string ServiceIDEdit
    {
        get
        {
            if ((string)ViewState["ServiceIDEdit"] != null)
                return (string)ViewState["ServiceIDEdit"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ServiceIDEdit"] = value;
        }
    }

    private int IndexEdit
    {
        get
        {
            if ((int)ViewState["IndexEdit"] != null)
                return (int)ViewState["IndexEdit"];
            else
                return -1;
        }
        set
        {
            ViewState["IndexEdit"] = value;
        }
    }

    private DataTable dtTruck
    {
        get
        {
            if ((DataTable)ViewState["dtTruck"] != null)
                return (DataTable)ViewState["dtTruck"];
            else
                return null;
        }
        set
        {
            ViewState["dtTruck"] = value;
        }
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        this.CheckPostBack();
        //this.GenerateTable(4, 3);

        if (!IsPostBack)
        {
            this.InitialForm();
        }
        this.EnableTab();
        //pnlMain.Controls.Clear();
        //GenerateTable();
    }
    #endregion

    #region CheckPostBack
    private void CheckPostBack()
    {
        try
        {
            var ctrlName = Request.Params[Page.postEventSourceID];
            var args = Request.Params[Page.postEventArgumentID];

            //if (ctrlName == txtScore1Final.UniqueID && args == "txtScore1Final_OnKeyPress")
            //    txtScore1Final_OnKeyPress(ctrlName, args);

            //if (ctrlName == txtScore2Final.UniqueID && args == "txtScore2Final_OnKeyPress")
            //    txtScore2Final_OnKeyPress(ctrlName, args);

            //if (ctrlName == txtScore3Final.UniqueID && args == "txtScore3Final_OnKeyPress")
            //    txtScore3Final_OnKeyPress(ctrlName, args);

            //if (ctrlName == txtScore4Final.UniqueID && args == "txtScore4Final_OnKeyPress")
            //    txtScore4Final_OnKeyPress(ctrlName, args);

            if (ctrlName == txtZealQty.UniqueID && args == "txtZealQty_OnKeyPress")
                txtZealQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilQty.UniqueID && args == "txtOilQty_OnKeyPress")
                txtOilQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilPrice.UniqueID && args == "txtOilPrice_OnKeyPress")
                txtOilPrice_OnKeyPress(ctrlName, args);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region InitialCreateDoc
    private void InitialCreateDoc()
    {
        try
        {
            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            txtFName.Text = dtLogin.Rows[0]["SFULLNAME"].ToString();
            txtFNameDivision.Text = dtLogin.Rows[0]["SUNITNAME"].ToString();

            txtComplainBy.Value = dtLogin.Rows[0]["SFULLNAME"].ToString();
            txtComplainDivisionBy.Value = dtLogin.Rows[0]["SUNITNAME"].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region LoadWarehouse คลังต้นทาง
    private void LoadWarehouse()
    {
        try
        {
            DataTable dtWarehouse = new DataTable();
            dtWarehouse = ContractBLL.Instance.TTERMINALSelect();
            DropDownListHelper.BindDropDownList(ref cbxOrganiz, dtWarehouse, "STERMINALID", "STERMINALNAME", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region LoadContract
    //private void LoadContract(string Condition)
    //{
    //    try
    //    {
    //        DataTable dtContract = new DataTable();
    //        dtContract = ComplainBLL.Instance.ContractSelectBLL(Condition);
    //        DropDownListHelper.BindDropDownList(ref cboContract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
    //    }
    //    catch (Exception ex)
    //    {
    //        alertFail(ex.Message);
    //    }
    //}
    #endregion

    #region LoadVendor
    //private void LoadVendor()
    //{
    //    try
    //    {
    //        DataTable dtVendor = new DataTable();
    //        dtVendor = VendorBLL.Instance.TVendorSapSelect();

    //        DropDownListHelper.BindDropDownList(ref cboVendor, dtVendor, "SVENDORID", "SABBREVIATION", true);

    //    }
    //    catch (Exception ex)
    //    {
    //        alertFail(ex.Message);
    //    }
    //}
    #endregion

    #region LoadEmployee
    //private void LoadEmployee()
    //{
    //    try
    //    {
    //        DataTable dtEmployee = new DataTable();
    //        dtEmployee = ComplainBLL.Instance.EmployeeSelectBLL();
    //        DropDownListHelper.BindDropDownList(ref cmbPersonalNo, dtEmployee, "SPERSONELNO", "FULLNAME", true);
    //    }
    //    catch (Exception ex)
    //    {
    //        alertFail(ex.Message);
    //    }
    //}
    #endregion

    #region InitialForm
    private void InitialForm()
    {
        try
        {
            ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = false;

            if (Request.QueryString["DocID"] != null)
            {

                var decryptedBytes = MachineKey.Decode(Request.QueryString["DocID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                DocID = decryptedValue;
            }
            else if (Request.QueryString["id"] != null)
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["id"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string[] ids = decryptedValue.Split('_');
                SurpriseCheckID = ids[0];
                DocID = ids[1];
            }

            dtRequestFile = new DataTable();
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", DateTimeFormatInfo.CurrentInfo);
            txtDateComplain.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", DateTimeFormatInfo.CurrentInfo);
            txtCreateDate.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", DateTimeFormatInfo.CurrentInfo);

            this.InitialCreateDoc();
            this.LoadTopic();
            this.LoadComplainType();
            this.InitialUpload();
            this.InitialHeader();
            this.LoadWarehouse();
            this.LoadData();

            //dteDateClose.CalendarProperties.ShowClearButton = false;
            IndexEdit = -1;
            chkLock.Checked = true;

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
        //string s = QueryStringModule.Decrypt(Request.QueryString.ToString().Replace("enc=", string.Empty)).Replace("DocID=", string.Empty);
    }
    #endregion

    #region EnableTab
    private void EnableTab()
    {
        if ((DocID != null) && (!string.Equals(DocID, string.Empty)))
        {
            ((HtmlAnchor)this.FindControlRecursive(Page, "GeneralTab")).Visible = true;
            ((HtmlAnchor)this.FindControlRecursive(Page, "ProcessTab")).Visible = true;

            this.CheckCanEdit();
        }
        else
        {//New Document
            cmdSaveTab1.Enabled = true;
            cmdSaveTab2.Enabled = true;
            cmdSaveTab2Draft.Enabled = true;

            ((HtmlAnchor)this.FindControlRecursive(Page, "GeneralTab")).Visible = true;
            ((HtmlAnchor)this.FindControlRecursive(Page, "ProcessTab")).Visible = false;
        }

        if (string.Equals(CancelDoc, "Y"))
            this.DisplayCancelDoc();
    }

    private void EnableTab3()
    {
        try
        {
            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            //ถ้าเป็น รข ให้เปิด Tab3
            if (string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode) && (DocStatusID == ConfigValue.DocStatus3 || DocStatusID == ConfigValue.DocStatus4 || DocStatusID == ConfigValue.DocStatus5))
            {
                lblCusScoreHeader.Text = string.Format(lblCusScoreHeader.Text, dtHeader.Rows[0]["CONTRACTNAME"].ToString());

                ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = true;
                dtScore = ComplainBLL.Instance.ScoreSelectBLL();
                DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
                this.SelectDuplicateCar();

                dtScoreList = new DataTable();
                dtScoreList.Columns.Add("STOPICID");
                dtScoreList.Columns.Add("STOPICNAME");
                dtScoreList.Columns.Add("Cost");
                dtScoreList.Columns.Add("CostDisplay");
                dtScoreList.Columns.Add("DisableDriver");
                dtScoreList.Columns.Add("DisableDriverDisplay");
                dtScoreList.Columns.Add("Score1");
                dtScoreList.Columns.Add("Score2");
                dtScoreList.Columns.Add("Score3");
                dtScoreList.Columns.Add("Score4");
                dtScoreList.Columns.Add("Score5");
                dtScoreList.Columns.Add("Score6");
                dtScoreList.Columns.Add("Score6Display");
                dtScoreList.Columns.Add("TotalCar");

                dtScoreListCar = new DataTable();
                dtScoreListCar.Columns.Add("STOPICID");
                dtScoreListCar.Columns.Add("TRUCKID");
                dtScoreListCar.Columns.Add("CARHEAD");
                dtScoreListCar.Columns.Add("CARDETAIL");

                dtUploadTab3 = new DataTable();
                dtUploadTab3.Columns.Add("UPLOAD_ID");
                dtUploadTab3.Columns.Add("UPLOAD_NAME");
                dtUploadTab3.Columns.Add("FILENAME_SYSTEM");
                dtUploadTab3.Columns.Add("FILENAME_USER");
                dtUploadTab3.Columns.Add("FULLPATH");

                DropDownListHelper.BindDropDownList(ref ddlScore1, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore2, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore3, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore4, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);

                dtUploadTypeTab3 = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN_SCORE");
                DropDownListHelper.BindDropDownList(ref cboUploadTypeTab3, dtUploadTypeTab3, "UPLOAD_ID", "UPLOAD_NAME", true);

                int sum = 0;
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    if (!string.Equals(dtHeader.Rows[i]["TotalCar"].ToString(), string.Empty) && int.Parse(dtHeader.Rows[i]["TotalCar"].ToString()) > 0)
                        sum += int.Parse(dtHeader.Rows[i]["TotalCar"].ToString());
                }

                if (sum > 0)
                {
                    SumCar = sum;
                    rowTotalCarTab3.Visible = true;
                    txtTotalCarTab3.Text = SumCar.ToString();
                }
                else
                {
                    rowListCarTab3.Visible = true;
                }

                dtCusScoreType = ComplainBLL.Instance.CusScoreSelectBLL();
                DropDownListHelper.BindRadioButton(ref radCusScoreType, dtCusScoreType, "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME");

                dtUploadTab3 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_SCORE", string.Empty);
                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);

                dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailBLL(DocID);

                dtScoreList.Columns["COST"].ColumnName = "Cost";
                dtScoreList.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
                dtScoreList.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
                dtScoreList.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
                dtScoreList.Columns["SCORE1"].ColumnName = "Score1";
                dtScoreList.Columns["SCORE2"].ColumnName = "Score2";
                dtScoreList.Columns["SCORE3"].ColumnName = "Score3";
                dtScoreList.Columns["SCORE4"].ColumnName = "Score4";
                dtScoreList.Columns["SCORE5"].ColumnName = "Score5";
                dtScoreList.Columns["SCORE6"].ColumnName = "Score6";
                dtScoreList.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
                dtScoreList.Columns["TOTALCAR"].ColumnName = "TotalCar";

                GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);

                DataTable dt = new DataTable();
                dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreBLL(DocID);
                if (dt.Rows.Count > 0)
                {
                    if (!string.Equals(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString(), "-1"))
                    {
                        radCusScoreType.SelectedValue = dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString();

                        txtPointFinal.Enabled = false;
                        txtCostFinal.Enabled = false;
                        txtDisableFinal.Enabled = false;

                        lblShowSumPoint.Text = "(Sum : {0})";
                        lblShowSumCost.Text = "(Sum : {0})";
                        lblShowSumDisable.Text = "(Sum : {0})";

                        lblShowSumPoint.Visible = false;
                        lblShowSumCost.Visible = false;
                        lblShowSumDisable.Visible = false;

                        int row = this.GetSelectRow(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString());

                        string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                        switch (CusType)
                        {
                            //case "MAX": this.FindMax(); break;
                            //case "SUM": this.FindSum("Normal"); break;
                            case "FREETEXT":
                                this.FindSum(string.Empty);

                                txtPointFinal.Enabled = true;
                                txtCostFinal.Enabled = true;
                                txtDisableFinal.Enabled = true;

                                lblShowSumPoint.Visible = true;
                                lblShowSumCost.Visible = true;
                                lblShowSumDisable.Visible = true; break;
                            //this.FreeText(); break;
                            default:
                                break;
                        }
                    }

                    if (!string.Equals(dt.Rows[0]["TOTAL_POINT"].ToString(), "-1"))
                        txtPointFinal.Text = dt.Rows[0]["TOTAL_POINT"].ToString();

                    if (!string.Equals(dt.Rows[0]["TOTAL_COST"].ToString(), "-1"))
                        txtCostFinal.Text = dt.Rows[0]["TOTAL_COST"].ToString();

                    if (!string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-1"))
                        txtDisableFinal.Text = dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region LoadData
    private void LoadData()
    {
        try
        {
            if ((DocID != null) && (!string.Equals(DocID, string.Empty)))
            {// Edit Complain
                dtData = SurpriseCheckBLL.Instance.CheckTruckComplainSelect(" AND c.DOC_ID = '" + DocID + "'");
                dtHeader = SurpriseCheckBLL.Instance.CheckTruckComplainSelectHeader(" AND T_CHECKTRUCK_COMPLAIN.DOC_ID = '" + DocID + "'");
                if (dtData.Rows.Count > 0)
                {
                    DocStatusID = int.Parse(dtData.Rows[0]["DOC_STATUS_ID"].ToString());

                    txtDocID.Value = dtData.Rows[0]["DOC_ID"].ToString();
                    txtComplainBy.Value = dtData.Rows[0]["FULLNAME"].ToString();
                    txtComplainDivisionBy.Value = dtData.Rows[0]["SUNITNAME"].ToString();
                    txtDateComplain.Text = (dtData.Rows[0]["DDATECOMPLAIN"] != null) ? dtData.Rows[0]["DDATECOMPLAIN"].ToString() : string.Empty;
                    txtDetail.Value = dtData.Rows[0]["SDETAIL"] + "";

                    txtOperate.Text = dtData.Rows[0]["SSOLUTION"].ToString();
                    txtProtect.Text = dtData.Rows[0]["SPROTECT"].ToString();
                    txtsEmployee.Text = dtData.Rows[0]["SRESPONSIBLE"].ToString();

                    txtCCEmail.Text = dtData.Rows[0]["CC_EMAIL"].ToString();

                    txtCreateDate.Text = dtData.Rows[0]["CREATE_DATE"].ToString();

                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                        dtHeader.Rows[i]["DELIVERYDATE"] = (dtHeader.Rows[i]["DELIVERYDATE"] != null) ? dtHeader.Rows[i]["DELIVERYDATE"].ToString() : string.Empty;

                    GridViewHelper.BindGridView(ref dgvHeader, dtHeader);

                    dtUpload = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "CHECKTRUCK_COMPLAIN", string.Empty);
                    GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                    this.InitialRequestFile();
                    this.CheckCanEdit();

                    CancelDoc = dtData.Rows[0]["DOC_CANCEL"].ToString();
                    this.EnableTab3();

                    if (DocStatusID == ConfigValue.DocStatus4)
                        chkSaveTab3Draft.Checked = true;
                }

            }
            else
            {//New Document
                cmdSaveTab1.Enabled = true;
                cmdSaveTab2.Enabled = true;
                cmdSaveTab2Draft.Enabled = true;
                ((HtmlAnchor)this.FindControlRecursive(Page, "GeneralTab")).Visible = false;
                ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = false;
                if ((SurpriseCheckID != null) && (!string.Equals(SurpriseCheckID, string.Empty)))
                {
                    dtData = SurpriseCheckBLL.Instance.SurpriseCheckSelect("  AND S.NSURPRISECHECKID = '" + SurpriseCheckID + "'");
                    if (dtData.Rows.Count > 0)
                    {
                        DataRow dr = dtData.Rows[0];
                        cboVendor.Items.Clear();
                        cboVendor.Items.Add(new ListItem()
                        {
                            Text = dr["SVENDORNAME"] + string.Empty,
                            Value = dr["SVENDORID"] + string.Empty,
                        });

                        cboContract.Items.Clear();
                        cboContract.Items.Add(new ListItem()
                        {
                            Text = dr["SCONTRACTNO"] + string.Empty,
                            Value = dr["SCONTRACTID"] + string.Empty,
                        });

                        cboContract.Items.Clear();
                        cboContract.Items.Add(new ListItem()
                        {
                            Text = dr["SCONTRACTNO"] + string.Empty,
                            Value = dr["SCONTRACTID"] + string.Empty,
                        });

                        cboHeadRegist.Items.Clear();
                        cboHeadRegist.Items.Add(new ListItem()
                        {
                            Text = dr["SHEADREGISTERNO"] + string.Empty,
                            Value = dr["STRUCKID"] + string.Empty,
                        });
                        txtTrailerRegist.Value = dr["STRAILERREGISTERNO"] + string.Empty;
                        cboVendor_SelectedIndexChanged(null, null);
                        DropDownListHelper.BindDropDownList(ref cboDelivery, ComplainBLL.Instance.OutboundSelectBLL(this.GetConditionVendor() + this.GetConditionContract() + this.GetConditionTruck() + " AND TPLANSCHEDULELIST.SDELIVERYNO IS NOT NULL"), "SDELIVERYNO", "SDELIVERYNO", true);
                    }

                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region CheckCanEdit
    private void CheckCanEdit()
    {
        try
        {//ต้องแก้ไขเอกสารภายในวันที่ 15 ของเดือนถัดไป
            if ((DocID != null) && (!string.Equals(DocID, string.Empty)) && (dtData != null))
            {//  ต้องเป็นผู้สร้างเอกสารเท่านั้น  จึงจะมีสิทธิ์แก้ไขข้อมูลใน Tab1, Tab2
                DataTable dt = SurpriseCheckBLL.Instance.CheckTruckComplainCanEdit(DocID);
                lblEditDate.Visible = true;
                lblEditDate.Text = string.Format(lblEditDate.Text, dt.Rows[0]["DATE_EDIT"].ToString());

                if (string.Equals(dt.Rows[0]["CAN_EDIT"].ToString(), "1") && (DocStatusID == ConfigValue.DocStatus1 || DocStatusID == ConfigValue.DocStatus2 || DocStatusID == ConfigValue.DocStatus3 || DocStatusID == ConfigValue.DocStatus7))
                {
                    DataTable dtLogin = new DataTable();
                    dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

                    if (string.Equals(Session["UserID"].ToString(), dtData.Rows[0]["SCREATE"].ToString()))
                    {
                        cmdSaveTab1.Enabled = true;
                        cmdSaveTab2.Enabled = true;
                        cmdSaveTab2Draft.Enabled = true;

                        if (DocStatusID == ConfigValue.DocStatus1 || DocStatusID == ConfigValue.DocStatus2 || DocStatusID == ConfigValue.DocStatus3)
                            cmdCancelDoc.Enabled = false;
                        else
                            cmdCancelDoc.Enabled = true;
                    }
                    else
                    {
                        if (!string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode))
                            txtFName.Text = "***********";

                        cmdSaveTab1.Enabled = false;
                        cmdSaveTab2.Enabled = false;
                        cmdSaveTab2Draft.Enabled = false;
                        cmdCancelDoc.Enabled = false;
                    }
                }
                else
                {
                    if (DocStatusID == ConfigValue.DocStatus4)
                    {
                        lblEditDate.Text = "อยู่ระหว่างการพิจารณาของ รข.";

                        cmdSaveTab1.Enabled = false;
                        cmdCancelDoc.Enabled = false;

                        cmdSaveTab2.Enabled = false;
                        cmdSaveTab2Draft.Enabled = false;
                    }
                    else if (DocStatusID == ConfigValue.DocStatus8)
                    {
                        cmdSaveTab2.Enabled = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region LoadTopic
    private void LoadTopic()
    {
        try
        {
            dtTopic = TopicBLL.Instance.TopicSelect(" AND M_TOPIC.TYPE = 'CHECKTRUCK'");
            DropDownListHelper.BindDropDownList(ref cmbTopic, dtTopic, "TOPIC_ID", "TOPIC_NAME", true);
            cmbTopic.SelectedValue = "6";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region InitialRequestFile
    private void InitialRequestFile()
    {
        try
        {
            dtRequestFile = new DataTable();
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            if (dtHeader.Rows.Count > 0)
            {
                string tmp = string.Empty;

                dtRequestFile = ComplainBLL.Instance.ComplainRequestFileBLL(this.GetConditionRequestFile());
                if (dtRequestFile.Rows.Count > 0)
                {
                    tmp = dtRequestFile.Rows[0]["COMPLAIN_TYPE_NAME"].ToString();

                    for (int i = 1; i < dtRequestFile.Rows.Count; i++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"].ToString(), tmp))
                            dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"] = string.Empty;
                        else
                            tmp = dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"].ToString();
                    }
                    GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetConditionRequestFile
    private string GetConditionRequestFile()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID IN (");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                sb.Append("'");
                sb.Append(dtHeader.Rows[i]["COMPLAINID"].ToString());
                sb.Append("',");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append(")");

            sb.Append(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'CHECKTRUCK_COMPLAIN'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region cmbTopic_SelectedIndexChanged
    protected void cmbTopic_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.LoadComplainType();
        chkLock.Checked = true;
        this.ClearRequire();
    }
    #endregion

    #region ClearRequire
    private void ClearRequire()
    {
        cmbTopic.Enabled = true;
        cboComplainType.Enabled = true;
        //cboVendor.Enabled = true;
        //cboContract.Enabled = true;
        //cboHeadRegist.Enabled = true;
        cboDelivery.Enabled = true;
        cmbPersonalNo.Enabled = true;
        txtDateTrans.Enabled = true;
        cbxOrganiz.Enabled = true;

        txtTotalCar.Disabled = false;

        lblReqcmbTopic.Visible = true;
        lblReqtxtComplainAddress.Visible = true;
        lblReqcboComplainType.Visible = true;
        lblReqcboDelivery.Visible = false;
        lblReqcboHeadRegist.Visible = false;
        lblReqcboVendor.Visible = false;
        lblReqcmbPersonalNo.Visible = true;
        lblReqtxtDateTrans.Visible = false;
        lblReqcbxOrganiz.Visible = false;
        lblReqcboContract.Visible = false;
        lblReqtxtTotalCar.Visible = false;

        txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", DateTimeFormatInfo.CurrentInfo);
    }
    #endregion

    #region LoadComplainType
    private void LoadComplainType()
    {
        try
        {
            cboComplainType.Items.Clear();
            if (cmbTopic.SelectedIndex > 0)
            {
                dtComplainType = ComplainTypeBLL.Instance.ComplainTypeSelect(this.GetConditionComplain());
                DropDownListHelper.BindDropDownList(ref cboComplainType, dtComplainType, "COMPLAIN_TYPE_ID", "COMPLAIN_TYPE_NAME", true);
            }
            this.ComplainTypeSelectedIndex();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region GetConditionComplain
    private string GetConditionComplain()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_TOPIC.TOPIC_ID = '" + cmbTopic.SelectedValue + "'");
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region InitialUpload
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("CHECKTRUCK_COMPLAIN");
        DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");
    }

    #endregion

    #region InitialHeader
    private void InitialHeader()
    {
        try
        {
            dtHeader = new DataTable();
            dtHeader.Columns.Add("TOPICID");
            dtHeader.Columns.Add("TOPICNAME");
            dtHeader.Columns.Add("ORGANIZID");
            dtHeader.Columns.Add("ORGANIZNAME");
            dtHeader.Columns.Add("COMPLAINID");
            dtHeader.Columns.Add("COMPLAINNAME");
            dtHeader.Columns.Add("LOCKDRIVERID");
            dtHeader.Columns.Add("LOCKDRIVERNAME");
            dtHeader.Columns.Add("DELIVERYID");
            dtHeader.Columns.Add("DELIVERYNAME");
            dtHeader.Columns.Add("TYPEPRODUCTID");
            dtHeader.Columns.Add("TYPEPRODUCTNAME");
            dtHeader.Columns.Add("TRUCKID");
            dtHeader.Columns.Add("CARHEAD");
            dtHeader.Columns.Add("CARDETAIL");
            dtHeader.Columns.Add("VENDORID");
            dtHeader.Columns.Add("VENDORNAME");
            dtHeader.Columns.Add("CONTRACTID");
            dtHeader.Columns.Add("CONTRACTNAME");
            dtHeader.Columns.Add("PERSONALID");
            dtHeader.Columns.Add("EMPLOYEENAME");
            dtHeader.Columns.Add("DELIVERYDATE");
            dtHeader.Columns.Add("COMPLAINADDRESS");
            dtHeader.Columns.Add("TOTALCAR");
            dtHeader.Columns.Add("SSERVICEID");
            dtHeader.Columns.Add("ISNEW");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region CheckPath
    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "CHECKTRUCK_Complain" + "\\" + DocID;
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region Upload
    #region cmdUpload_Click
    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }
    #endregion

    #region StartUpload
    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ValidateUploadFile
    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region ValidateSaveUpload
    private void ValidateSaveUpload()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            for (int i = 0; i < dtUpload.Rows.Count; i++)
                data.Add(decimal.Parse(dtUpload.Rows[i]["UPLOAD_ID"].ToString()));

            DataTable dt = this.GetFilteredData(dtRequestFile, data);
            if (dt.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("เอกสารที่ต้องทำการอัพโหลด : ");
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("\t- ");
                    sb.Append(dt.Rows[i]["UPLOAD_NAME"].ToString());
                    sb.Append("\r\n");
                }
                sb.Append("จึงจะทำการบันทึกข้อมูลได้");
                sb.Append("\r\n");
                sb.Append("ถ้าเอกสารยังไม่ครบ กรุณากดปุ่ม บันทึก(ร่าง) เพื่อบันทึกข้อมูลไว้ก่อน");

                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion

    #region GetFilteredData
    private DataTable GetFilteredData(DataTable table, List<decimal> filterValues)
    {
        DataView dv = new DataView(table);
        var filter = string.Join(",", filterValues);
        dv.RowFilter = "UPLOAD_ID NOT IN (" + filter + ")";
        return dv.ToTable();
    }
    #endregion

    #region dgvUploadFile_RowDeleting
    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region dgvUploadFile_RowUpdating
    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    #endregion

    #region DownloadFile
    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    #endregion

    #region dgvUploadFile_RowDataBound
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion

    #endregion

    #region cmdAddHeader_Click
    protected void cmdAddHeader_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateAddHeader();

            DataTable dtTmp = new DataTable();
            dtTmp = ComplainBLL.Instance.OutboundSelectBLL(this.GetConditionHeader());

            string LockDriverID = "0", LockDriverName = "ไม่";
            if (chkLock.Checked)
            {
                LockDriverID = "1";
                LockDriverName = "ใช่";
            }
            string STRAILERREGISTERNO = string.Empty;
            if (dtTmp.Rows.Count > 0)
                STRAILERREGISTERNO = dtTmp.Rows[0]["STRAILERREGISTERNO"].ToString();

            if (IndexEdit > -1)
            {
                dtHeader.Rows.RemoveAt(IndexEdit);
                dtHeader.Rows.Add(cmbTopic.SelectedValue + ""
                                    , cmbTopic.SelectedItem
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedValue + "" : null
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedItem.ToString() : string.Empty
                                    , cboComplainType.SelectedValue + ""
                                    , cboComplainType.SelectedItem
                                    , LockDriverID
                                    , LockDriverName
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedValue + "" : null
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedItem.ToString() : string.Empty
                                    , rblTypeProduct.Value + ""
                                    , rblTypeProduct.SelectedItem
                                    , cboHeadRegist.SelectedValue
                                    , cboHeadRegist.SelectedItem.ToString()
                                    , txtTrailerRegist.Value
                                    , cboVendor.SelectedValue
                                    , cboVendor.SelectedItem.ToString()
                                    , cboContract.SelectedValue
                                    , cboContract.SelectedItem.ToString()
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedValue + "" : null
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedItem.ToString() : string.Empty
                                    , txtDateTrans.Text
                                    , txtComplainAddress.Value.Trim()
                                    , txtTotalCar.Value.Trim()
                                    , ServiceIDEdit
                                    , "N");
                IndexEdit = -1;
            }
            else
            {
                dtHeader.Rows.Add(cmbTopic.SelectedValue + ""
                                    , cmbTopic.SelectedItem
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedValue + "" : null
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedItem.ToString() : string.Empty
                                    , cboComplainType.SelectedValue + ""
                                    , cboComplainType.SelectedItem
                                    , LockDriverID
                                    , LockDriverName
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedValue + "" : null
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedItem.ToString() : string.Empty
                                    , rblTypeProduct.Value + ""
                                    , rblTypeProduct.SelectedItem
                                    , cboHeadRegist.SelectedValue
                                    , cboHeadRegist.SelectedItem.ToString()
                                    , txtTrailerRegist.Value
                                    , cboVendor.SelectedValue
                                    , cboVendor.SelectedItem.ToString()
                                    , cboContract.SelectedValue
                                    , cboContract.SelectedItem.ToString()
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedValue + "" : null
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedItem.ToString() : string.Empty
                                    , txtDateTrans.Text
                                    , txtComplainAddress.Value.Trim()
                                    , txtTotalCar.Value.Trim()
                                    , "-1"
                                    , "Y");
            }

            ServiceIDEdit = string.Empty;
            cmdAddHeader.Text = "เพิ่มรายการ";
            cmdCancel.Visible = false;

            GridViewHelper.BindGridView(ref dgvHeader, dtHeader);
            this.InitialRequestFile();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region GetConditionHeader
    private string GetConditionHeader()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append(" AND TPLANSCHEDULELIST.SDELIVERYNO = '" + cboDelivery.SelectedValue + "'");
        sb.Append(" AND TCONTRACT_TRUCK.SCONTRACTID = '" + cboContract.SelectedValue + "'");
        sb.Append(" AND TPLANSCHEDULE.SHEADREGISTERNO = '" + cboHeadRegist.SelectedValue + "'");

        return sb.ToString();
    }
    #endregion

    #region ValidateAddHeader
    private void ValidateAddHeader()
    {
        try
        {
            if (cmbTopic.SelectedIndex < 1)
                throw new Exception("กรุณาป้อน เรื่องที่ร้องเรียน !!!");

            if (cboComplainType.SelectedIndex < 1)
                throw new Exception("กรุณาป้อน ประเภทเรื่องร้องเรียน - หลัก !!!");

            if ((chkLock.Checked) && (cmbPersonalNo.SelectedIndex < 1))
                throw new Exception("กรุณาป้อน ชื่อ พขร. ที่ถูกร้องเรียน !!!");
            if (string.IsNullOrEmpty(txtComplainAddress.Value))
            {
                throw new Exception("กรุณาป้อน สถานที่เกิดเหตุ !!!");
            }
            //dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(cboComplainType.SelectedValue.ToString()), "COMPLAIN");

            StringBuilder sb = new StringBuilder();
            object c;
            string Type;

            //sb.Append("<table>");
            //for (int i = 0; i < dtRequire.Rows.Count; i++)
            //{
            //    if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
            //    {
            //        c = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

            //        Type = c.GetType().ToString();
            //        if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxComboBox"))
            //        {
            //            ASPxComboBox cbo = (ASPxComboBox)c;
            //            if (cbo.Value == null)
            //            {
            //                sb.Append("<tr><td>");
            //                sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!" + Environment.NewLine);
            //                sb.Append("</td></tr>");
            //            }
            //        }
            //        else if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxTextBox"))
            //        {
            //            ASPxTextBox txt = (ASPxTextBox)c;
            //            if (string.Equals(txt.Text.Trim(), string.Empty))
            //            {
            //                sb.Append("<tr><td>");
            //                sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!" + Environment.NewLine);
            //                sb.Append("</td></tr>");
            //            }
            //        }
            //        else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
            //        {
            //            TextBox txt = (TextBox)c;
            //            if (string.Equals(txt.Text.Trim(), string.Empty))
            //            {
            //                sb.Append("<tr><td>");
            //                sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!" + Environment.NewLine);
            //                sb.Append("</td></tr>");
            //            }
            //        }
            //        else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
            //        {
            //            DropDownList cbo = (DropDownList)c;
            //            if (cbo.SelectedIndex < 1)
            //            {
            //                sb.Append("<tr><td>");
            //                sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!" + Environment.NewLine);
            //                sb.Append("</td></tr>");
            //            }
            //        }
            //        else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
            //        {
            //            HtmlInputText txt = (HtmlInputText)c;
            //            if (string.Equals(txt.Value.Trim(), string.Empty))
            //            {
            //                sb.Append("<tr><td>");
            //                sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!" + Environment.NewLine);
            //                sb.Append("</td></tr>");
            //            }
            //        }
            //    }
            //}
            //sb.Append("</table>");
            //if (!string.Equals(sb.ToString(), "<table></table>"))
            //    throw new Exception(sb.ToString());

            //เชคซ้ำ
            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (i == IndexEdit)
                    continue;

                if (string.Equals(dtHeader.Rows[i]["TopicID"].ToString(), cmbTopic.SelectedValue + "")
                    && string.Equals(dtHeader.Rows[i]["ComplainID"].ToString(), cboComplainType.SelectedValue + "")
                    && string.Equals(dtHeader.Rows[i]["TruckID"].ToString(), cboHeadRegist.SelectedValue)
                    && string.Equals(dtHeader.Rows[i]["ContractID"].ToString(), cboContract.SelectedValue + "")
                    && string.Equals(dtHeader.Rows[i]["PERSONALID"].ToString(), cmbPersonalNo.SelectedValue)
                    && string.Equals(dtHeader.Rows[i]["DeliveryDate"].ToString(), txtDateTrans.Text)
                    )
                    throw new Exception("ข้อมูลดังกล่าว ได้ถูกเพิ่มรายการเข้ามาแล้ว");
            }

            //1 เอกสาร สามารถเลือกได้เพียง 1 เลขที่สัญญา
            //for (int i = 0; i < dtHeader.Rows.Count; i++)
            //{
            //    if (i == IndexEdit)
            //        continue;

            //    if (!string.Equals(dtHeader.Rows[i]["CONTRACTID"].ToString(), cboContract.SelectedValue + ""))
            //        throw new Exception("1 เอกสาร สามารถเลือกได้เพียง 1 เลขที่สัญญา");
            //}

            //1 เอกสาร สามารถเลือกได้เพียง 1 พขร
            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (i == IndexEdit)
                    continue;

                if (!string.Equals(dtHeader.Rows[i]["PERSONALID"].ToString(), cmbPersonalNo.SelectedValue + ""))
                    throw new Exception("1 เอกสาร สามารถเลือกได้เพียง 1 พขร.");
            }

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (i == IndexEdit)
                    continue;

                if (string.Equals(this.CheckIsAddMultipleHeader(dtHeader.Rows[i]["COMPLAINID"].ToString()), "0"))
                {
                    //if (!string.Equals(dtHeader.Rows[i]["TruckID"].ToString(), cboHeadRegist.SelectedValue + ""))
                    //    throw new Exception("ข้อร้องเรียน " + dtHeader.Rows[i]["COMPLAINNAME"].ToString() + "\r\n" + "สามารถเลือกได้เพียง 1 ทะเบียนรถ");

                    if (!string.Equals(dtHeader.Rows[i]["DELIVERYDATE"].ToString().Substring(0, 10), txtDateTrans.Text.Substring(0, 10)))
                        throw new Exception("ข้อร้องเรียน " + dtHeader.Rows[i]["COMPLAINNAME"].ToString() + "\r\n" + "สามารถเลือกวันที่ขนส่ง ได้เพียงวันเดียว");
                }
                else
                {
                    if (i == IndexEdit)
                        continue;

                    if (!string.Equals(dtHeader.Rows[i]["COMPLAINID"].ToString(), cboComplainType.SelectedValue + ""))
                        throw new Exception("ข้อร้องเรียน " + dtHeader.Rows[i]["COMPLAINNAME"].ToString() + "\r\n" + "ไม่สามารถเลือกได้หลายประเภทเรื่องร้องเรียน");
                }
            }

            //1 เอกสาร สามารถเลือกเหตุการณ์ ได้เพียง 1 เหตุการณ์
            //for (int i = 0; i < dtHeader.Rows.Count; i++)
            //{
            //    if (i == IndexEdit)
            //        continue;

            //    if (!string.Equals(dtHeader.Rows[i]["TOPICID"].ToString(), cmbTopic.SelectedValue + ""))
            //        throw new Exception("1 เอกสาร สามารถเลือกได้เพียง 1 เหตุการณ์");
            //}

            int tmp;

            if ((lblReqtxtTotalCar.Visible) && (!int.TryParse(txtTotalCar.Value.Trim(), out tmp)))
                throw new Exception("จำนวนรถ ต้องเป็นตัวเลขเท่านั้น !!!");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region CheckComplainTypeName
    private string CheckComplainTypeName(string ComplainTypeID)
    {
        try
        {
            for (int i = 0; i < dtComplainType.Rows.Count; i++)
            {
                if (string.Equals(dtComplainType.Rows[i]["COMPLAIN_TYPE_ID"].ToString(), "ComplainTypeID"))
                    return dtComplainType.Rows[i]["COMPLAIN_TYPE_NAME"].ToString();
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region CheckIsAddMultipleHeader
    private string CheckIsAddMultipleHeader(string ComplainTypeID)
    {
        try
        {
            for (int i = 0; i < dtComplainType.Rows.Count; i++)
            {
                if (string.Equals(dtComplainType.Rows[i]["COMPLAIN_TYPE_ID"].ToString(), ComplainTypeID))
                    return dtComplainType.Rows[i]["IS_ADD_MULTIPLE_CAR"].ToString();
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region ValidateSave
    private void ValidateSave()
    {
        try
        {
            if (dtHeader.Rows.Count == 0)
                throw new Exception("กรุณา เพิ่มรายการ ก่อนบันทึก");

            if (string.Equals(txtFName.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน หน่วยงานผู้ร้องเรียน");

            if (string.Equals(txtFNameDivision.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ชื่อผู้ร้องเรียน");

            if (string.Equals(txtDetail.Value.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน สาเหตุ / รายละเอียด");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region dgvHeader_RowDeleting
    protected void dgvHeader_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtHeader.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvHeader, dtHeader);

            IndexEdit = -1;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cboComplainType_SelectedIndexChanged
    protected void cboComplainType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.ComplainTypeSelectedIndex();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ComplainTypeSelectedIndex
    private void ComplainTypeSelectedIndex()
    {
        this.ClearValue();
        this.ClearRequire();

        if (cboComplainType.SelectedIndex > 0)
        {
            if (string.Equals(dtComplainType.Rows[cboComplainType.SelectedIndex]["LOCK_DRIVER"].ToString(), "0"))
            {
                chkLock.Enabled = false;
                chkLock.Checked = false;
            }
            else
            {
                chkLock.Enabled = true;
                chkLock.Checked = true;
            }

            //this.InitialRequireField();
        }
        else
        {
            chkLock.Enabled = false;
            chkLock.Checked = false;
        }
    }
    #endregion

    #region FindControlRecursive
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    #endregion

    #region InitialRequireField
    private void InitialRequireField()
    {
        try
        {
            this.ClearRequire();

            dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(cboComplainType.SelectedValue.ToString()), "CHECKTRUCK_COMPLAIN");

            object tmp;
            Label tmpReq;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                tmp = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                if (tmp != null)
                {
                    tmpReq = (Label)this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID_REQ"].ToString());

                    if (tmpReq != null)
                    {
                        if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                        {//Require Field
                            tmpReq.Visible = true;
                            if (string.Equals(dtRequire.Rows[i]["CONTROL_ID"].ToString(), "txtDateTrans"))
                                txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", DateTimeFormatInfo.CurrentInfo);
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "2"))
                        {//Optional Field
                            tmpReq.Visible = false;
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "3"))
                        {//Disable Field
                            Type = tmp.GetType().ToString();
                            if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxComboBox"))
                            {
                                ASPxComboBox cbo = (ASPxComboBox)tmp;
                                if (cbo.SelectedIndex > 0)
                                    cbo.SelectedIndex = 0;
                                cbo.Value = string.Empty;
                                cbo.ClientEnabled = false;
                            }
                            else if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxTextBox"))
                            {
                                ASPxTextBox txt = (ASPxTextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Value = string.Empty;
                                txt.ClientEnabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                            {
                                TextBox txt = (TextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Enabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                            {
                                DropDownList cbo = (DropDownList)tmp;
                                if (cbo.SelectedIndex > 0)
                                    cbo.SelectedIndex = 0;
                                cbo.Enabled = false;
                                cbo.CssClass = "form-control";
                            }
                            else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
                            {
                                HtmlInputText txt = (HtmlInputText)tmp;
                                txt.Value = string.Empty;
                                txt.Disabled = true;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region dgvHeader_RowDataBound
    protected void dgvHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    ImageButton imgDelete;
            //    imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDeleteComplain");
            //    imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลนี้\\nใช่หรือไม่ ?')==false){return false;}");
            //}
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region dgvHeader_RowCommand
    protected void dgvHeader_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            IndexEdit = Index;

            if (string.Equals(e.CommandName, "Select"))
            {
                DataTable dtTmp = dtHeader.Copy();
                string[] display = { "เรื่องที่ร้องเรียน", "คลังต้นทาง", "เรื่องร้องเรียน - หลัก", "หยุดพักการขนส่งชั่วคราว (พขร.)", "เลข Outbound", "ประเภทผลิตภัณฑ์", "ทะเบียนรถ (หัว)", "ทะเบียนรถ (หาง)", "บริษัทผู้ประกอบการขนส่ง", "เลขที่สัญญา", "รหัสพนักงาน", "พขร. ที่ถูกร้องเรียน", "วันที่ขนส่ง", "สถานที่เกิดเหตุ", "จำนวนรถ (คัน)" };

                dtTmp.Columns["TopicName"].ColumnName = "เรื่องที่ร้องเรียน";
                dtTmp.Columns["OrganizName"].ColumnName = "คลังต้นทาง";
                dtTmp.Columns["ComplainName"].ColumnName = "เรื่องร้องเรียน - หลัก";
                dtTmp.Columns["LockDriverName"].ColumnName = "หยุดพักการขนส่งชั่วคราว (พขร.)";
                dtTmp.Columns["DeliveryName"].ColumnName = "เลข Outbound";
                dtTmp.Columns["TypeProductName"].ColumnName = "ประเภทผลิตภัณฑ์";
                dtTmp.Columns["CarHead"].ColumnName = "ทะเบียนรถ (หัว)";
                dtTmp.Columns["CarDetail"].ColumnName = "ทะเบียนรถ (หาง)";
                dtTmp.Columns["VendorName"].ColumnName = "บริษัทผู้ประกอบการขนส่ง";
                dtTmp.Columns["ContractName"].ColumnName = "เลขที่สัญญา";
                dtTmp.Columns["PersonalID"].ColumnName = "รหัสพนักงาน";
                dtTmp.Columns["EmployeeName"].ColumnName = "พขร. ที่ถูกร้องเรียน";
                dtTmp.Columns["DeliveryDate"].ColumnName = "วันที่ขนส่ง";
                dtTmp.Columns["ComplainAddress"].ColumnName = "สถานที่เกิดเหตุ";
                dtTmp.Columns["TotalCar"].ColumnName = "จำนวนรถ (คัน)";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                for (int i = 0; i < dtTmp.Columns.Count; i++)
                {
                    if (display.Contains(dtTmp.Columns[i].ColumnName))
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dtTmp.Columns[i].ColumnName + " : " + dtTmp.Rows[Index][i].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
            else if (string.Equals(e.CommandName, "EditHeader"))
            {
                ServiceIDEdit = dtHeader.Rows[Index]["SSERVICEID"].ToString();
                cmdAddHeader.Text = "บันทึก";
                cmdCancel.Visible = true;

                cmbTopic.SelectedValue = dtHeader.Rows[Index]["TOPICID"].ToString();
                cmbTopic_SelectedIndexChanged(null, null);

                cboComplainType.SelectedValue = dtHeader.Rows[Index]["COMPLAINID"].ToString();
                cboComplainType_SelectedIndexChanged(null, null);
                cbxOrganiz.SelectedValue = dtHeader.Rows[Index]["ORGANIZID"].ToString();
                chkLock.Checked = (string.Equals(dtHeader.Rows[Index]["LOCKDRIVERID"].ToString(), "0") ? false : true);
                rblTypeProduct.Value = dtHeader.Rows[Index]["TYPEPRODUCTID"].ToString();
                txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

                //cboVendor.SelectedValue = dtHeader.Rows[Index]["VENDORID"].ToString();
                //cboVendor_SelectedIndexChanged(null, null);

                //cboContract.SelectedValue = dtHeader.Rows[Index]["CONTRACTID"].ToString();
                //cboContract_SelectedIndexChanged(null, null);

                //cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                //cboHeadRegist_SelectedIndexChanged(null, null);

                if (!string.Equals(dtHeader.Rows[Index]["DELIVERYID"].ToString(), string.Empty))
                    cboDelivery.SelectedValue = dtHeader.Rows[Index]["DELIVERYID"].ToString();

                cmbPersonalNo.SelectedValue = dtHeader.Rows[Index]["PERSONALID"].ToString();
                txtDateTrans.Text = (dtHeader.Rows[Index]["DELIVERYDATE"] != null) ? dtHeader.Rows[Index]["DELIVERYDATE"].ToString() : string.Empty;
                //teTimeTrans.Text = dtHeader.Rows[Index]["DELIVERYTIME"].ToString();
                txtComplainAddress.Value = dtHeader.Rows[Index]["COMPLAINADDRESS"].ToString();
                txtTotalCar.Value = dtHeader.Rows[Index]["TOTALCAR"].ToString();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cmdCancel_Click
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            ServiceIDEdit = string.Empty;
            cmdAddHeader.Text = "เพิ่มรายการ";
            cmdCancel.Visible = false;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ClearScreen
    private void ClearScreen()
    {
        try
        {
            this.InitialForm();

            this.ClearValue();

            this.ClearRequire();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ClearValue
    private void ClearValue()
    {
        try
        {
            //cboDelivery.Items.Clear();
            //cboHeadRegist.Items.Clear();
            //cboContract.Items.Clear();
            //cmbPersonalNo.Items.Clear();

            //cboVendor.Items.Clear();
            //this.LoadVendor();

            chkLock.Checked = true;
            txtComplainAddress.Value = string.Empty;
            txtTotalCar.Value = string.Empty;
            txtTrailerRegist.Value = string.Empty;

            txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", DateTimeFormatInfo.CurrentInfo);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cmdSaveTab1_Click
    protected void cmdSaveTab1_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();

            DataTable dtDocument = SurpriseCheckBLL.Instance.CheckTruckComplainInsert((DocID == null) ? string.Empty : DocID,
                                                                    dtHeader,
                                                                    txtDetail.Value,
                                                                    txtFName.Text,
                                                                    string.Empty,
                                                                    txtFNameDivision.Text,
                                                                    txtDateComplain.Text,
                                                                    "00:00",
                                                                    "1",
                                                                    Session["UserID"].ToString(),
                                                                    Session["UserID"].ToString(),
                                                                    Session["UserID"].ToString(),SurpriseCheckID);

            txtDocID.Value = dtDocument.Rows[0]["Doc_ID"].ToString();
            DocID = dtDocument.Rows[0]["Doc_ID"].ToString();

            //รถงับ พขร.
            this.LockDriver();

            this.CheckCanEdit();
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")");
            ((HtmlAnchor)this.FindControlRecursive(Page, "ProcessTab")).Visible = true;
            this.LoadData();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cboVendor_SelectedIndexChanged
    protected void cboVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref cmbPersonalNo, ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal()), "SEMPLOYEEID", "FULLNAME", true);

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cboContract_SelectedIndexChanged
    protected void cboContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            txtTrailerRegist.Value = string.Empty;

            DropDownListHelper.BindDropDownList(ref cboHeadRegist, null, string.Empty, string.Empty, false);        //ทะเบียนรถ
            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //เลข Outbound

            if (cboContract.SelectedIndex > 0)
            {
                dtTruck = ComplainBLL.Instance.TruckSelectBLL(this.GetConditionVendor() + this.GetConditionContract());
                DropDownListHelper.BindDropDownList(ref cboHeadRegist, dtTruck, "STRUCKID", "SHEADREGISTERNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cboHeadRegist_SelectedIndexChanged
    protected void cboHeadRegist_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //เลข Outbound
            txtTrailerRegist.Value = string.Empty;

            if (cboHeadRegist.SelectedIndex > 0)
            {
                txtTrailerRegist.Value = dtTruck.Rows[cboHeadRegist.SelectedIndex]["STRAILERREGISTERNO"].ToString();
                DropDownListHelper.BindDropDownList(ref cboDelivery, ComplainBLL.Instance.OutboundSelectBLL(this.GetConditionVendor() + this.GetConditionContract() + this.GetConditionTruck() + " AND TPLANSCHEDULELIST.SDELIVERYNO IS NOT NULL"), "SDELIVERYNO", "SDELIVERYNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region GetConditionPersonal
    private string GetConditionPersonal()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" AND TEMPLOYEE_SAP.CARRIER = '" + cboVendor.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetConditionTruck
    private string GetConditionTruck()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" AND TTRUCK.STRUCKID = '" + cboHeadRegist.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetConditionVendor
    private string GetConditionVendor()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboVendor.SelectedIndex > 0)
                sb.Append(" AND TCONTRACT.SVENDORID = '" + cboVendor.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetConditionContract
    private string GetConditionContract()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(" AND TCONTRACT_TRUCK.SCONTRACTID = '" + cboContract.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region cmdSaveTab2_Click
    protected void cmdSaveTab2_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSaveTab2();
            ComplainImportFileBLL.Instance.ComplainAttachInsertBLL(dtUpload, DocID, int.Parse(Session["UserID"].ToString()), txtOperate.Text.Trim(), txtProtect.Text.Trim(), txtsEmployee.Text.Trim(), ConfigValue.DocStatus3, txtCCEmail.Text.Trim(), "CHECKTRUCK_COMPLAIN");
            //this.SendEmail(ConfigValue.EmailCreateComplain);
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")");
            DocStatusID = ConfigValue.DocStatus3;
            this.EnableTab3();

            //cmdSaveTab1.Enabled = false;
            //cmdSaveTab2.Enabled = false;
            //cmdSaveTab2Draft.Enabled = false;
            cmdCancelDoc.Enabled = false;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion

    #region ValidateSaveTab2
    private void ValidateSaveTab2()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region cmdSaveTab2Draft_Click
    protected void cmdSaveTab2Draft_Click(object sender, EventArgs e)
    {
        try
        {
            ComplainImportFileBLL.Instance.ComplainAttachInsertBLL(dtUpload, DocID, int.Parse(Session["UserID"].ToString()), txtOperate.Text.Trim(), txtProtect.Text.Trim(), txtsEmployee.Text.Trim(), ConfigValue.DocStatus2, txtCCEmail.Text.Trim(), "CHECKTRUCK_COMPLAIN");
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cmdCancelDoc_Click
    protected void cmdCancelDoc_Click(object sender, EventArgs e)
    {
        try
        {
            SurpriseCheckBLL.Instance.CheckTruckComplainCancelDoc(DocID);
            alertSuccess("ยกเลิกเอกสาร เรียบร้อย");
            this.DisplayCancelDoc();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region DisplayCancelDoc
    private void DisplayCancelDoc()
    {
        try
        {
            cmdSaveTab1.Enabled = false;
            cmdCancelDoc.Enabled = false;
            cmdSaveTab2.Enabled = false;
            cmdSaveTab2Draft.Enabled = false;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region SendEmail
    private void SendEmail(int TemplateID)
    {
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            if (dtTemplate.Rows.Count > 0)
            {
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                Body = Body.Replace("{ComplainName}", "แต่งกายไม่สุภาพ");
                Body = Body.Replace("{EmployeeName}", "ศักดิ์ชัย เปี่ยมสมบัติ");
                Body = Body.Replace("{EmployeeID}", "E001");

                MailService.SendMail("zsakchai.p@pttict.com", dtTemplate.Rows[0]["SUBJECT"].ToString(), Body);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion

   

    

    #region cButtonDelete_ClickDelete
    protected void cButtonDelete_ClickDelete(object sender, EventArgs e)
    {
        try
        {
            string indexstr = string.Empty;
            Button btnDelete = (Button)sender;
            indexstr = btnDelete.CommandArgument.Split('_')[1];
            int index = int.Parse(indexstr);
            dtHeader.Rows.RemoveAt(index);
            GridViewHelper.BindGridView(ref dgvHeader, dtHeader);

            IndexEdit = -1;

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region Tab3
    #region Upload
    #region cmdUploadTab3_Click
    protected void cmdUploadTab3_Click(object sender, EventArgs e)
    {
        this.StartUploadTab3();
    }
    #endregion

    #region StartUploadTab3
    private void StartUploadTab3()
    {
        try
        {
            if (cboUploadTypeTab3.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUploadTab3.HasFile)
            {
                string FileNameUser = fileUploadTab3.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFileTab3(System.IO.Path.GetExtension(FileNameUser), fileUploadTab3.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadTab3.SaveAs(Path + "\\" + FileNameSystem);

                dtUploadTab3.Rows.Add(cboUploadTypeTab3.SelectedValue, cboUploadTypeTab3.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion

    #region ValidateUploadFileTab3
    private void ValidateUploadFileTab3(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region dgvUploadFileTab3_RowDeleting
    protected void dgvUploadFileTab3_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadTab3.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    protected void dgvUploadFileTab3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFileTab3.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    protected void dgvUploadFileTab3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region txtZealQty_OnKeyPress
    private void txtZealQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            int tmp;
            if (!int.TryParse(txtZealQty.Text.Trim(), out tmp))
            {
                txtZealQty.Text = "0";
                txtCost.Text = "0";
                return;
            }

            txtZealQty.Text = int.Parse(txtZealQty.Text.Trim()).ToString();
            txtCost.Text = (int.Parse(txtZealQty.Text.Trim()) * int.Parse(txtZeal.Text.Trim())).ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtOilQty_OnKeyPress(string ctrlName, string args)
    {
        decimal tmp;
        if (!decimal.TryParse(txtOilQty.Text.Trim(), out tmp))
            txtZealQty.Text = "0";

        txtOilQty.Text = int.Parse(txtOilQty.Text.Trim()).ToString();
        this.CalculateOil();
    }

    private void txtOilPrice_OnKeyPress(string ctrlName, string args)
    {
        decimal tmp;
        if (!decimal.TryParse(txtOilPrice.Text.Trim(), out tmp))
            txtOilPrice.Text = "0";

        txtOilPrice.Text = int.Parse(txtOilPrice.Text.Trim()).ToString();
        this.CalculateOil();
    }
    #endregion

    #region CalculateOil
    private void CalculateOil()
    {
        try
        {
            if (!string.Equals(txtOilQty.Text.Trim(), string.Empty) && !string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                txtCost.Text = (int.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString()) * decimal.Parse(txtOilPrice.Text.Trim()) * decimal.Parse(txtOilQty.Text.Trim())).ToString();
            else
                txtCost.Text = "0";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GenerateTable
    private void GenerateTable(int Rows, int Cols)
    {
        //Panel pnl;
        //for (int i = 0; i < Rows; i++)
        //{
        //    pnl = new Panel();
        //    pnl.EnableViewState = true;
        //    for (int j = 0; j < Cols; j++)
        //    {
        //        if (j != 0)
        //        {
        //            Label lbl = new Label();
        //            lbl.EnableViewState = true;
        //            lbl.Width = 20;
        //            lbl.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //            pnl.Controls.Add(lbl);
        //        }

        //        TextBox txt = new TextBox();
        //        txt.Width = 150;
        //        txt.EnableViewState = true;
        //        //txt.Text = "TxT" + (i + 1).ToString() + "_" + (j + 1).ToString();
        //        txt.ID = "TxT" + (i + 1).ToString() + "_" + (j + 1).ToString();
        //        pnl.Controls.Add(txt);
        //    }
        //    pnlMain.Controls.Add(pnl);
        //}

        //this.FindControlRecursive(Page, "TxT1_1");
        //this.FindControlRecursive(Page, "TxT1_2");
    }
    #endregion

    #region cmdTest_Click
    protected void cmdTest_Click(object sender, EventArgs e)
    {
        //string data = ((TextBox)pnlMain.Controls[0].FindControl("TxT1_1")).Text;
        //alertSuccess(data);
    }
    #endregion

    #region radCusScoreType_SelectedIndexChanged
    protected void radCusScoreType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region FindMax
    private void FindMax()
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString()) > PointMax)
                    PointMax = decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            }
            txtPointFinal.Text = PointMax.ToString();

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString()) > CostMax)
                    CostMax = decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            }
            txtCostFinal.Text = CostMax.ToString();

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString()) > DisableDriverMax)
                    DisableDriverMax = int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            }
            txtDisableFinal.Text = DisableDriverMax.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FindSum(string ShowType)
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                PointMax += decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtPointFinal.Text = PointMax.ToString();
            else
                lblShowSumPoint.Text = string.Format(lblShowSumPoint.Text, PointMax.ToString());

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                CostMax += decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtCostFinal.Text = CostMax.ToString();
            else
                lblShowSumCost.Text = string.Format(lblShowSumCost.Text, CostMax.ToString());

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                DisableDriverMax += int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtDisableFinal.Text = DisableDriverMax.ToString();
            else
                lblShowSumDisable.Text = string.Format(lblShowSumDisable.Text, DisableDriverMax.ToString());
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FreeText()
    {
        try
        {
            txtPointFinal.Text = string.Empty;
            txtCostFinal.Text = string.Empty;
            txtDisableFinal.Text = string.Empty;

            this.FindSum("ForFreeText");

            txtPointFinal.Enabled = true;
            txtCostFinal.Enabled = true;
            txtDisableFinal.Enabled = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetSelectRow
    private int GetSelectRow(string Value)
    {
        try
        {
            for (int i = 0; i < dtCusScoreType.Rows.Count; i++)
            {
                if (string.Equals(dtCusScoreType.Rows[i]["CUSSCORE_TYPE_ID"].ToString(), Value))
                    return i;
            }

            return -1;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region cmdSaveTab3_Click
    protected void cmdSaveTab3_Click(object sender, EventArgs e)
    {
        this.SaveTab3(true, ConfigValue.DocStatus5);
        this.LockAndUnlockDriver();
    }
    #endregion

    #region SaveTab3
    private void SaveTab3(bool isValiDate, int DocStatusID)
    {
        try
        {
            if (isValiDate)
                this.ValidateSaveTab3();

            ComplainImportFileBLL.Instance.ComplainTab3SaveBLL(dtUploadTab3, DocID, int.Parse(Session["UserID"].ToString()), "CHECKTRUCK_COMPLAIN_SCORE", (radCusScoreType.SelectedIndex > -1) ? int.Parse(radCusScoreType.SelectedValue) : -1, (!string.Equals(txtPointFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtPointFinal.Text.Trim()) : -1, (!string.Equals(txtCostFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostFinal.Text.Trim()) : -1, (!string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? int.Parse(txtDisableFinal.Text.Trim()) : -1, dtScoreList, DocStatusID, 0, null,null, false, 0, 0, string.Empty, 0, 0, string.Empty);
            //this.SendEmail(ConfigValue.EmailCreateComplain);
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ValidateSaveTab3
    private void ValidateSaveTab3()
    {
        try
        {
            if (dtScoreList.Rows.Count == 0)
                throw new Exception("กรุณาเลือก หัวข้อการตัดคะแนน");

            if (radCusScoreType.SelectedIndex < 0)
                throw new Exception("กรุณาเลือก เงื่อนไขการตัดคะแนน");

            if (string.Equals(txtPointFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน คะแนน");

            if (string.Equals(txtCostFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ค่าปรับ");

            if (string.Equals(txtDisableFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ระงับ พขร. (วัน)");

            int tmp1;
            decimal tmp2;

            if (!int.TryParse(txtDisableFinal.Text.Trim(), out tmp1))
                throw new Exception("กรุณาป้อน ระงับ พขร. (วัน) เป็นตัวเลข");

            if (!decimal.TryParse(txtPointFinal.Text.Trim(), out tmp2))
                throw new Exception("กรุณาป้อน คะแนน (วัน) เป็นตัวเลข");

            if (!decimal.TryParse(txtCostFinal.Text.Trim(), out tmp2))
                throw new Exception("กรุณาป้อน ค่าปรับ เป็นตัวเลข");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region LockDriver
    private void LockDriver()
    {
        try
        {
            //PersonalID = "4700000232";
            DataTable dtDriver = SurpriseCheckBLL.Instance.DriverUpdateSelect(DocID);
            DataTable dtDriverDetail;
            for (int i = 0; i < dtDriver.Rows.Count; i++)
            {
                dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dtDriver.Rows[i]["SEMPLOYEEID"].ToString());

                SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                {
                    //4700000011
                    //DRIVER_CODE = "4700000130",         //รหัสพนักงาน
                    //PERS_CODE = "3250400993513",        //หมายเลขบัตรประชาชน
                    //LICENSE_NO = "สป.00330/51",         //หมายเลขใบขับขี่ประเภท 4                     EMP.SDRIVERNO
                    //LICENSENOE_FROM = "2015.01.23",     //ช่วงระยะเวลาอนุญาตใบขับขี่ ประเภท 4 Start      EMP.DDRIVERBEGIN
                    //LICENSENO_TO = "2016.12.31",        //ช่วงระยะเวลาอนุญาตใบขับขี่ ประเภท 4 End        EMP.DDRIVEREXPIRE
                    //Phone_1 = null,                     //หมายเลขโทรศัพท์หลัก          EMP.STEL
                    //Phone_2 = null,                     //หมายเลขโทรศัพท์สำรอง        EMP.STEL2
                    //FIRST_NAME = string.Empty,          //ชื่อ                        EMP_SAP.FNAME
                    //LAST_NAME = "LAST_NAME",            //นามสกุล                     EMP_SAP.LNAME
                    //CARRIER = "0010000020",             //บริษัทผู้ขนส่ง                 EMP.STRANSID
                    //DRV_STATUS = Status                 

                    DRIVER_CODE = dtDriver.Rows[i]["SEMPLOYEEID"].ToString(),
                    PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                    LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                    LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                    LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                    Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                    Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                    FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                    LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                    CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                    DRV_STATUS = (dtDriver.Rows[i]["CHANGE_TO"].ToString() == "0") ? ConfigValue.DriverEnable : ConfigValue.DriverDisable
                };

                string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                string resultCheck = result.Substring(0, 1);

                if (string.Equals(resultCheck, "N"))
                    throw new Exception("ไม่สามารถระงับ พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                else
                    SurpriseCheckBLL.Instance.DriverUpdateLog(dtDriver.Rows[i]["ID"].ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region cmdChangeStatus_Click
    protected void cmdChangeStatus_Click(object sender, EventArgs e)
    {
        try
        {
            SurpriseCheckBLL.Instance.CheckTruckComplainChangeStatus(DocID, ConfigValue.DocStatus8, int.Parse(Session["UserID"].ToString()));
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region SelectDuplicateCar
    private void SelectDuplicateCar()
    {
        try
        {
            dtCarDistinct = new DataTable();
            dtCarDistinct.Columns.Add("TRUCKID");
            dtCarDistinct.Columns.Add("CARHEAD");
            dtCarDistinct.Columns.Add("CARDETAIL");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (string.Equals(dtHeader.Rows[i]["TRUCKID"].ToString(), string.Empty))
                    continue;

                if (dtCarDistinct.Rows.Count == 0)
                {
                    dtCarDistinct.Rows.Add(dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
                else
                {
                    int j;
                    for (j = 0; j < dtCarDistinct.Rows.Count; j++)
                    {
                        if (string.Equals(dtCarDistinct.Rows[j]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString()))
                            break;
                    }
                    if ((j == dtCarDistinct.Rows.Count) && (!string.Equals(dtCarDistinct.Rows[j - 1]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString())))
                        dtCarDistinct.Rows.Add(dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
            }

            GridViewHelper.BindGridView(ref dgvScore, dtCarDistinct);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cboScore_SelectedIndexChanged
    protected void cboScore_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.ReadOnlyScore(true);

            if (cboScore.SelectedIndex == 0)
            {
                this.ClearScreenTab3();
            }
            else
            {
                txtCost.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString();
                txtDisableDriver.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString();
                txtScore1.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL02"].ToString();
                txtScore2.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL03"].ToString();
                txtScore3.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL04"].ToString();
                txtScore4.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL06"].ToString();
                txtScore5.Text = dtScore.Rows[cboScore.SelectedIndex]["NMULTIPLEIMPACT"].ToString();
                txtScore6.Text = dtScore.Rows[cboScore.SelectedIndex]["NPOINT"].ToString();
                lblShowOption.Visible = (dtScore.Rows[cboScore.SelectedIndex]["IS_CORRUPT"].ToString() == "0") ? false : true;

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("น้ำมัน"))
                {
                    txtOilPrice.Text = "0";
                    txtOilQty.Text = "0";
                    rowOil.Visible = true;
                }
                else
                {
                    txtOilPrice.Text = string.Empty;
                    txtOilQty.Text = string.Empty;
                    rowOil.Visible = false;
                }

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("ซีล"))
                {
                    txtZeal.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString();
                    txtZealQty.Text = "0";
                    rowZeal.Visible = true;
                }
                else
                {
                    txtZeal.Text = string.Empty;
                    txtZealQty.Text = string.Empty;
                    rowZeal.Visible = false;
                }

                //if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["CAN_EDIT"].ToString(), "1"))
                //{//สามารถแก้ไขคะแนนได้
                //    this.ReadOnlyScore(false);
                //}
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion

    #region ReadOnlyScore
    private void ReadOnlyScore(bool ReadOnly)
    {
        txtScore1.ReadOnly = ReadOnly;
        txtScore2.ReadOnly = ReadOnly;
        txtScore3.ReadOnly = ReadOnly;
        txtScore4.ReadOnly = ReadOnly;
    }
    #endregion

    #region chkChooseScoreAll_CheckedChanged
    protected void chkChooseScoreAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region cmdAddScore_Click
    protected void cmdAddScore_Click(object sender, EventArgs e)
    {
        try
        {
            int CountCar = this.ValidateAddScore();

            string CostDisplay = string.Empty;
            string Score6Display = string.Empty;
            string DisableDriverDisplay = string.Empty;

            CostDisplay = txtCost.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString();
            Score6Display = txtScore6.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString();
            DisableDriverDisplay = txtDisableDriver.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString();

            dtScoreList.Rows.Add(cboScore.SelectedValue, cboScore.SelectedItem, (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString(), CostDisplay, (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString(), DisableDriverDisplay, txtScore1.Text.Trim(), txtScore2.Text.Trim(), txtScore3.Text.Trim(), txtScore4.Text.Trim(), txtScore5.Text.Trim(), (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString(), Score6Display, CountCar);

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                if (Chk.Checked)
                    dtScoreListCar.Rows.Add(cboScore.SelectedValue, dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtCarDistinct.Rows[i]["CARHEAD"].ToString(), dtCarDistinct.Rows[i]["CARDETAIL"].ToString());
            }

            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);
            this.ClearScreenTab3();
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ClearScreenTab3
    private void ClearScreenTab3()
    {
        try
        {
            cboScore.SelectedIndex = 0;
            txtCost.Text = string.Empty;
            txtDisableDriver.Text = string.Empty;
            txtScore1.Text = string.Empty;
            txtScore2.Text = string.Empty;
            txtScore3.Text = string.Empty;
            txtScore4.Text = string.Empty;
            txtScore5.Text = string.Empty;
            txtScore6.Text = string.Empty;
            lblShowOption.Visible = false;

            txtOilPrice.Text = string.Empty;
            txtOilQty.Text = string.Empty;
            txtZeal.Text = string.Empty;
            txtZealQty.Text = string.Empty;

            rowOil.Visible = false;
            rowZeal.Visible = false;

            this.ReadOnlyScore(true);

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = true;
            }
            ((CheckBox)this.FindControlRecursive(dgvScore, "chkChooseScoreAll")).Checked = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region ValidateAddScore
    private int ValidateAddScore()
    {
        try
        {
            if (cboScore.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก หัวข้อตัดคะแนน");

            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (string.Equals(dtScoreList.Rows[i]["STOPICID"].ToString(), cboScore.SelectedValue))
                    throw new Exception("เลือกหัวข้อ ตัดคะแนน ซ้ำกัน");
            }

            if (string.Equals(txtScore1.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ภาพลักษณ์องค์กร");

            if (string.Equals(txtScore2.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ความพึงพอใจลูกค้า");

            if (string.Equals(txtScore3.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน กฎ/ระเบียบฯ");

            if (string.Equals(txtScore4.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน แผนงานขนส่ง");

            if (rowOil.Visible)
            {
                if (string.Equals(txtOilQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนน้ำมัน");

                if (string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน ค่าน้ำมัน");
            }

            if (rowZeal.Visible)
            {
                if (string.Equals(txtZealQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนซิล");
            }

            if (rowTotalCarTab3.Visible)
            {
                if (string.Equals(txtTotalCarTab3.Text.Trim(), string.Empty))
                    throw new Exception("จำนวนรถ (คัน)");

                int tmp;
                if (!int.TryParse(txtTotalCarTab3.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน จำนวนรถ (คัน) เป็นตัวเลข");

                if (SumCar < int.Parse(txtTotalCarTab3.Text.Trim()))
                    throw new Exception("จำนวนรถที่ป้อน มีค่ามากกว่า จำนวนรถที่ถูกร้องเรียน");
            }

            return this.CountCar();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region CountCar
    private int CountCar()
    {
        try
        {
            int CountCar = 0;
            if (rowTotalCarTab3.Visible)
            {
                CountCar = int.Parse(txtTotalCarTab3.Text.Trim());
            }
            else
            {
                CheckBox Chk;
                for (int i = 0; i < dgvScore.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                    {
                        CountCar += 1;
                    }
                }

                if (CountCar == 0)
                    throw new Exception("กรุณาเลือก ทะเบียนรถ ที่ต้องการตัดคะแนน");
            }

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region dgvScoreList_RowCommand
    protected void dgvScoreList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            IndexEdit = Index;

            if (string.Equals(e.CommandName, "Select"))
            {
                DataTable dtTmp = dtScoreList.Copy();
                //string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ภาพลักษณ์องค์กร", "ความพึงพอใจลูกค้า", "กฎ/ระเบียบฯ", "แผนงานขนส่ง", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };

                string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };
                dtTmp.Columns["STOPICNAME"].ColumnName = "หัวข้อตัดคะแนน";
                dtTmp.Columns["Cost"].ColumnName = "ค่าปรับ";
                dtTmp.Columns["DisableDriver"].ColumnName = "ระงับ พขร. (วัน)";
                //dtTmp.Columns["Score1"].ColumnName = "ภาพลักษณ์องค์กร";
                //dtTmp.Columns["Score2"].ColumnName = "ความพึงพอใจลูกค้า";
                //dtTmp.Columns["Score3"].ColumnName = "กฎ/ระเบียบฯ";
                //dtTmp.Columns["Score4"].ColumnName = "แผนงานขนส่ง";
                dtTmp.Columns["Score5"].ColumnName = "ผลคูณความรุนแรง";
                dtTmp.Columns["Score6"].ColumnName = "หักคะแนนต่อครั้ง";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                for (int i = 0; i < dtTmp.Columns.Count; i++)
                {
                    if (display.Contains(dtTmp.Columns[i].ColumnName))
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dtTmp.Columns[i].ColumnName + " : " + dtTmp.Rows[Index][i].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    if (i == dtTmp.Columns.Count - 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        //sb.Append("ทะเบียนรถ : " + this.GetCarList(dtTmp.Rows[Index]["STOPICID"].ToString()));
                        if (rowTotalCarTab3.Visible)
                            sb.Append("จำนวนรถ : " + dtTmp.Rows[Index]["TotalCar"].ToString() + " คัน");
                        else
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
            //else if (string.Equals(e.CommandName, "EditHeader"))
            //{
            //    ServiceIDEdit = dtHeader.Rows[Index]["SSERVICEID"].ToString();
            //    cmdAddHeader.Text = "บันทึก";
            //    cmdCancel.Visible = true;

            //    cmbTopic.SelectedValue = dtHeader.Rows[Index]["TOPICID"].ToString();
            //    cmbTopic_SelectedIndexChanged(null, null);

            //    cboComplainType.SelectedValue = dtHeader.Rows[Index]["COMPLAINID"].ToString();
            //    cbxOrganiz.SelectedValue = dtHeader.Rows[Index]["ORGANIZID"].ToString();
            //    chkLock.Checked = (string.Equals(dtHeader.Rows[Index]["LOCKDRIVERID"].ToString(), "0") ? false : true);
            //    rblTypeProduct.Value = dtHeader.Rows[Index]["TYPEPRODUCTID"].ToString();
            //    txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

            //    cboVendor.SelectedValue = dtHeader.Rows[Index]["VENDORID"].ToString();
            //    cboVendor_SelectedIndexChanged(null, null);

            //    cboContract.SelectedValue = dtHeader.Rows[Index]["CONTRACTID"].ToString();
            //    cboContract_SelectedIndexChanged(null, null);

            //    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
            //    cboHeadRegist_SelectedIndexChanged(null, null);

            //    if (!string.Equals(dtHeader.Rows[Index]["DELIVERYID"].ToString(), string.Empty))
            //        cboDelivery.SelectedValue = dtHeader.Rows[Index]["DELIVERYID"].ToString();

            //    cmbPersonalNo.SelectedValue = dtHeader.Rows[Index]["PERSONALID"].ToString();
            //    txtDateTrans.Text = (dtHeader.Rows[Index]["DELIVERYDATE"] != null) ? dtHeader.Rows[Index]["DELIVERYDATE"].ToString() : string.Empty;
            //    //teTimeTrans.Text = dtHeader.Rows[Index]["DELIVERYTIME"].ToString();
            //    txtComplainAddress.Value = dtHeader.Rows[Index]["COMPLAINADDRESS"].ToString();
            //    txtTotalCar.Value = dtHeader.Rows[Index]["TOTALCAR"].ToString();
            //}
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion

    #region GetCarListCount
    private int GetCarListCount(string STOPICID)
    {
        try
        {
            int CountCar = 0;
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    CountCar += 1;
                }
            }

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region dgvScore_RowDeleting
    protected void dgvScore_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dtTmp = dtScoreListCar.Copy();
            dtTmp.Rows.Clear();

            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (!string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[e.RowIndex]["STOPICID"].ToString()))
                    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
            }
            dtScoreListCar = dtTmp;

            dtScoreList.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlScore1_SelectedIndexChanged
    protected void ddlScore1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore1", "txtScore1Final", "Score1");
    }

    protected void ddlScore2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore2", "txtScore2Final", "Score2");
    }

    protected void ddlScore3_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore3", "txtScore3Final", "Score3");
    }

    protected void ddlScore4_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore4", "txtScore4Final", "Score4");
    }

    #endregion

    #region CalculateScore
    private void CalculateScore(string ddlScoreName, string txtScoreName, string ColumnName)
    {
        try
        {
            //DropDownList ddlScore = (DropDownList)this.FindControlRecursive(divScore, ddlScoreName);
            //TextBox txtScore = (TextBox)this.FindControlRecursive(divScore, txtScoreName);

            //txtScore.ReadOnly = true;

            //if (ddlScore.SelectedIndex == 0)
            //{
            //    txtScore.Text = string.Empty;
            //}
            //else
            //{
            //    switch (ddlScore.SelectedItem.ToString())
            //    {
            //        case "MAX": txtScore.Text = this.FindMaxScore(ColumnName).ToString(); break;
            //        case "SUM": txtScore.Text = this.FindSumScore(ColumnName).ToString(); break;
            //        case "CUSTOM": txtScore.ReadOnly = false;
            //                       txtScore.Text = string.Empty;
            //                       MsgAlert.ajaxFocus(txtScore.ClientID, Page); break;
            //        default:
            //            break;
            //    }
            //}
            //this.CalulateTotal();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region FindMaxScore
    private int FindMaxScore(string ColumnName)
    {
        try
        {
            int max = 0;
            int tmp = 0;

            for (int i = 0; i < dtCarDistinct.Rows.Count; i++)
            {
                for (int j = 0; j < dtScoreList.Rows.Count; j++)
                {
                    if (this.Check(dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtScoreList.Rows[j]["STOPICID"].ToString()) && (int.Parse(dtScoreList.Rows[j][ColumnName].ToString()) > tmp))
                    {
                        tmp = int.Parse(dtScoreList.Rows[j][ColumnName].ToString());
                    }
                }
                max += tmp;
                tmp = 0;
            }

            return max;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region Check
    private bool Check(string TruckID, string TopicID)
    {
        try
        {
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["TRUCKID"].ToString(), TruckID) && string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), TopicID))
                    return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region FindSumScore
    private int FindSumScore(string ColumnName)
    {
        try
        {
            int sum = 0;
            int tmp = 0;

            for (int i = 0; i < dtCarDistinct.Rows.Count; i++)
            {
                for (int j = 0; j < dtScoreList.Rows.Count; j++)
                {
                    if (this.Check(dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtScoreList.Rows[j]["STOPICID"].ToString()))
                    {
                        tmp += int.Parse(dtScoreList.Rows[j][ColumnName].ToString());
                    }
                }
                sum += tmp;
                tmp = 0;
            }

            return sum;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region CalulateTotal
    private void CalulateTotal()
    {
        try
        {
            int sum = 0;
            sum += (txtScore1Final.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore1Final.Text.Trim());
            sum += (txtScore2Final.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore2Final.Text.Trim());
            sum += (txtScore3Final.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore3Final.Text.Trim());
            sum += (txtScore4Final.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore4Final.Text.Trim());
            txtScore5Final.Text = sum.ToString();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region RefreshCusScore
    private void RefreshCusScore()
    {
        try
        {
            if (!string.Equals(radCusScoreType.SelectedValue, string.Empty))
            {
                int row = this.GetSelectRow(radCusScoreType.SelectedValue);

                txtPointFinal.Enabled = false;
                txtCostFinal.Enabled = false;
                txtDisableFinal.Enabled = false;

                lblShowSumPoint.Text = "(Sum : {0})";
                lblShowSumCost.Text = "(Sum : {0})";
                lblShowSumDisable.Text = "(Sum : {0})";

                lblShowSumPoint.Visible = false;
                lblShowSumCost.Visible = false;
                lblShowSumDisable.Visible = false;

                string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                switch (CusType)
                {
                    case "MAX": this.FindMax(); break;
                    case "SUM": this.FindSum("Normal"); break;
                    case "FREETEXT":
                        lblShowSumPoint.Visible = true;
                        lblShowSumCost.Visible = true;
                        lblShowSumDisable.Visible = true;
                        this.FreeText(); break;
                    default:
                        break;
                }
            }
            else
            {
                //txtPointFinal.Text = "0";
                //txtCostFinal.Text = "0";
                //txtDisableFinal.Text = "0";
            }

            //this.CalculateScore("ddlScore1", "txtScore1Final", "Score1");
            //this.CalculateScore("ddlScore2", "txtScore2Final", "Score2");
            //this.CalculateScore("ddlScore3", "txtScore3Final", "Score3");
            //this.CalculateScore("ddlScore4", "txtScore4Final", "Score4");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region txtScore1Final_OnKeyPress
    private void txtScore1Final_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            //this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore2Final_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            //this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore3Final_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            //this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore4Final_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            //this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region cmdSaveTab3Draft_Click
    protected void cmdSaveTab3Draft_Click(object senderf, EventArgs e)
    {
        try
        {
            this.SaveTab3(false, (chkSaveTab3Draft.Checked) ? ConfigValue.DocStatus4 : ConfigValue.DocStatus3);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #region LockAndUnlockDriver
    private void LockAndUnlockDriver()
    {
        try
        {
            int TotalLock = (string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtDisableFinal.Text.Trim());
            SurpriseCheckBLL.Instance.CheckTruckComplainLockDriver(DocID, TotalLock, int.Parse(Session["UserID"].ToString()));
            DataTable dtDriver = SurpriseCheckBLL.Instance.DriverUpdateSelect2(DocID);
            DataTable dtDriverDetail;

            for (int i = 0; i < dtDriver.Rows.Count; i++)
            {
                //dtDriver.Rows[i]["SEMPLOYEEID"] = "4700000232";
                dtDriverDetail = SurpriseCheckBLL.Instance.PersonalDetailSelect(dtDriver.Rows[i]["SEMPLOYEEID"].ToString());

                SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                {
                    DRIVER_CODE = dtDriver.Rows[i]["SEMPLOYEEID"].ToString(),
                    PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                    LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                    LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                    LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                    Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                    Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                    FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                    LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                    CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                    DRV_STATUS = (dtDriver.Rows[i]["CHANGE_TO"].ToString() == "0") ? ConfigValue.DriverEnable : ConfigValue.DriverDisable
                };

                string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                string resultCheck = result.Substring(0, 1);

                if (string.Equals(resultCheck, "N"))
                    throw new Exception("ไม่สามารถระงับ พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                else
                    SurpriseCheckBLL.Instance.DriverUpdateLog(dtDriver.Rows[i]["ID"].ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #endregion
}