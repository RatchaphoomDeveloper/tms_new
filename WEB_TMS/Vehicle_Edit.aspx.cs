﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS_BLL.Master;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TMS_BLL.Transaction.Complain;
using System.Text;
using EmailHelper;
using System.Globalization;
using TMS_BLL.Transaction.Report;
using System.Web.Security;

public partial class Vehicle_edit : PageBase
{
    #region ViewState
    public string VendorId
    {
        get
        {
            if (ViewState["VendorId"] != null)
                return (string)ViewState["VendorId"];
            else
                return "";
        }
        set
        {
            ViewState["VendorId"] = value;
        }
    }
    public string CGROUP
    {
        get
        {
            if (ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return "";
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    public string STRUCKID
    {
        get
        {
            if (ViewState["STRUCKID"] != null)
                return (string)ViewState["STRUCKID"];
            else
                return "";
        }
        set
        {
            ViewState["STRUCKID"] = value;
        }
    }
    public string SHEADREGISTERNO
    {
        get
        {
            if (ViewState["SHEADREGISTERNO"] != null)
                return (string)ViewState["SHEADREGISTERNO"];
            else
                return "";
        }
        set
        {
            ViewState["SHEADREGISTERNO"] = value;
        }
    }
    public string STRANSPORTID
    {
        get
        {
            if (ViewState["STRANSPORTID"] != null)
                return (string)ViewState["STRANSPORTID"];
            else
                return "";
        }
        set
        {
            ViewState["STRANSPORTID"] = value;
        }
    }
    public string CACTIVE_STATUS
    {
        get
        {
            if (ViewState["CACTIVE_STATUS"] != null)
                return (string)ViewState["CACTIVE_STATUS"];
            else
                return "";
        }
        set
        {
            ViewState["CACTIVE_STATUS"] = value;
        }
    }
    #endregion
    private string registerno = "";
    private string CartypeId;
    protected void Page_Load(object sender, EventArgs e)
    {


        this.Culture = "en-US";
        this.UICulture = "en-US";
        //Get User expire
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;

        }
        else
        {
            VendorId = Session["SVDID"].ToString();
            CGROUP = Session["CGROUP"].ToString();
        }


        if (!IsPostBack)
        {
            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");
            DataTable dtClassGroup = VehicleBLL.Instance.ClassGroupSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlClassGroup, dtClassGroup, "CLASS_GROUP_CODE", "CLASS_GROUP_CODE", true);
            //radSpot.SelectedValue = "1";

            ViewState["GoBackTo"] = Request.UrlReferrer;
            #region BindData
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string str = decryptedValue;
                decryptedBytes = MachineKey.Decode(Request.QueryString["STTRUCKID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                STRUCKID = decryptedValue;
                decryptedBytes = MachineKey.Decode(Request.QueryString["CarTypeID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                Session["SCARTYPEID"] = decryptedValue;
                CartypeId = string.Equals(Session["SCARTYPEID"], "0") ? "0" : "1";
                Rdo_TypeCar.SelectedValue = CartypeId;
                Rdo_TypeCar.Enabled = false;
                radSpot.Enabled = false;

                if (STRUCKID != "") { }
                DataTable dt = VehicleBLL.Instance.LoadDataSelectEdit(STRUCKID);
                VendorId = dt.Rows[0]["STRANSPORTID"].ToString();
                STRANSPORTID = dt.Rows[0]["STRANSPORTID"].ToString();
                this.initalFrom(CGROUP, dt.Rows[0]["SHEADREGISTERNO"].ToString());
                this.ChoseTypeCar(CartypeId, dt.Rows[0]["SHEADREGISTERNO"].ToString(), dt.Rows[0]["STRAILERREGISTERNO"].ToString());
                this.BindDataInForm(STRUCKID, Rdo_TypeCar.SelectedIndex.ToString(), dt);
            }
            #endregion
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {
                btnSave.Enabled = false;
                btnVehicleChange.Disabled = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void BindDataInForm(string STRUCKID, string TypeCar, DataTable dt)
    {
        CACTIVE_STATUS = dt.Rows[0]["CACTIVE"].ToString();
        switch (TypeCar)
        {
            case "0":
                ddlVendorTruck.SelectedValue = dt.Rows[0]["STRANSPORTID"].ToString();
                rdo_contratType.SelectedValue = dt.Rows[0]["CARCATE_ID"].ToString();
                ddl_truck.SelectedValue = dt.Rows[0]["STRUCKID"].ToString();
                Truck_contract.SelectedValue = dt.Rows[0]["TMPCONTRACTID"].ToString();
                VEH_TEXT.Text = dt.Rows[0]["VEH_TEXT"].ToString();
                active_blacklist.SelectedValue = dt.Rows[0]["CACTIVE"].ToString();

                if (string.Equals(dt.Rows[0]["CSPACIALCONTRAC"].ToString(), "Y"))
                {
                    radSpot.SelectedValue = "1";
                    ddlClassGroup.SelectedValue = dt.Rows[0]["CLASSGRP"].ToString();
                }

                break;
            case "1":
                ddlvendorSemiTruck.SelectedValue = dt.Rows[0]["STRANSPORTID"].ToString();
                rdo_contratType1.SelectedValue = dt.Rows[0]["CARCATE_ID"].ToString();
                truck_head.SelectedValue = dt.Rows[0]["STRUCKID"].ToString();
                Truck_Semi.SelectedValue = dt.Rows[0]["STRAILERID"].ToString();
                TruckSemi_contract.SelectedValue = dt.Rows[0]["TMPCONTRACTID"].ToString();
                semi_veh_text.Text = dt.Rows[0]["VEH_TEXT"].ToString();
                active_blacklist.SelectedValue = dt.Rows[0]["CACTIVE"].ToString();

                if (string.Equals(dt.Rows[0]["CSPACIALCONTRAC"].ToString(), "Y"))
                {
                    radSpot.SelectedValue = "1";
                    ddlClassGroup.SelectedValue = dt.Rows[0]["CLASSGRP"].ToString();
                }

                break;

        }


    }
    #region EVENT
    protected void ChoseTypeCar(string CartypeId, string SHEADREGISTERNO, string STRAILERREGISTERNO)
    {
        DataTable LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, VendorId, radSpot.SelectedValue);
        switch (CartypeId)
        {
            case "0":
                DataTable dt_truck = VehicleBLL.Instance.LoadDataTruckVehicleCancel(VendorId, SHEADREGISTERNO);

                if (dt_truck.Rows.Count == 0)
                {
                    initalTruck(VendorId);
                    return;
                }

                DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "STRUCKID", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case "1":
                DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadVehicleCancel(VendorId, SHEADREGISTERNO);

                if (dt_headcar.Rows.Count == 0)
                {
                    initalTruck(VendorId);
                    return;
                }

                DataTable dt_semi = VehicleBLL.Instance.LoadDataSemiVehicleCancel(VendorId, STRAILERREGISTERNO);
                DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "STRUCKID", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "STRUCKID", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
        // }


    }

    protected void ddlTruckSelectedValue(object sender, EventArgs e)
    {
        this.initalTruck(ddlVendorTruck.SelectedValue);

    }

    protected void ddlSemiSelectValue(object sender, EventArgs e)
    {
        this.initalTruck(ddlvendorSemiTruck.SelectedValue);
    }

    protected void initalTruck(string value)
    {
        DataTable LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, value, radSpot.SelectedValue);
        DataTable dt_classgrp = VehicleBLL.Instance.LoadDataClassgrp();
        switch (Rdo_TypeCar.SelectedIndex)
        {
            case 0:
                DataTable dt_truck = VehicleBLL.Instance.LoadDataTruck(value, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", true);
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case 1:
                DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadCar(value, radSpot.SelectedValue);
                DataTable dt_semi = VehicleBLL.Instance.LoadDataSemi(VendorId, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
    }
    protected void TruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(ddl_truck.SelectedValue);
        int totalcapacity = 0;
        string veh_text = "";
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
        {
            totalcapacity += int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString());
            veh_text += LoadGetVeh_Text.Rows[i]["VEH_TEXT"].ToString() + ",";
        }
        veh_text = veh_text.TrimEnd(',');
        VEH_TEXT.Text = "SU" + totalcapacity + "(" + veh_text + ")";
    }
    #region คำนวณ VEH_TEXT Semi


    protected void SemitTruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(Truck_Semi.SelectedValue);
        int totalcapacity = 0;
        string veh_text = "";
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
        {
            totalcapacity += int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString());
            veh_text += LoadGetVeh_Text.Rows[i]["VEH_TEXT"].ToString() + ",";
        }
        veh_text = veh_text.TrimEnd(',');
        semi_veh_text.Text = "SM" + totalcapacity + "(" + veh_text + ")";
    }
    #endregion
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        this.Save_TMS();

    }
    protected void cmdChangeVehicle(object sender, EventArgs e)
    {
        this.ChangeVehicle();
    }
    #endregion
    #region InitalData
    private void initalFrom(string CGROUP, string regisNumber)
    {
        DataTable dt_vehicle_car = VehicleBLL.Instance.LoadDataVehicleCar();
        string condition = VendorId;
        DataTable dt_vendor = VendorBLL.Instance.SelectName(condition);
        switch (CGROUP)
        {
            case "1":
                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", true);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car, "config_value", "config_name", false);
                DataTable Blacklist_ds = CarBLL.Instance.initalBlacklist(regisNumber);
                GridViewHelper.BindGridView(ref Data_cencal, Blacklist_ds);
                Truck_Semi.Enabled = false;
                ConfrimStatus.Visible = true;
                btnVehicleChange.Visible = false;
                break;
            default:

                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", true);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car, "config_value", "config_name", false);
                btnSave.Visible = false;
                break;
        }

    }
    #endregion
    #region Micellouse
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    private string GetConditionVendor()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlVendorTruck.SelectedIndex > 0)
                sb.Append(" AND TCONTRACT.SVENDORID = '" + Truck_contract.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string StringReCarTyperegisNum(string Type)
    {
        if (string.Equals(Type, "0"))
        {
            return ddl_truck.SelectedItem.Text;
        }
        else
        {
            return truck_head.SelectedItem.Text;
        }
    }
    #endregion
    #region บันทึกข้อมูลลง
    protected DataTable DataToDatatable()
    {
        DataTable dt;
        switch (Rdo_TypeCar.SelectedIndex)
        {
            case 0:
                dt = new DataTable();
                dt.Columns.Add("STRUCKID");
                dt.Columns.Add("typecar");
                dt.Columns.Add("transportid");
                dt.Columns.Add("contrattype");
                dt.Columns.Add("truck");
                dt.Columns.Add("contratid");
                dt.Columns.Add("veh_text");
                //userId
                dt.Columns.Add("UserId");


                dt.Rows.Add(STRUCKID, Rdo_TypeCar.SelectedIndex, ddlVendorTruck.SelectedValue, rdo_contratType.SelectedValue, ddl_truck.SelectedItem.Text.Trim(), Truck_contract.SelectedValue, VEH_TEXT.Text.Trim(),

                    //date_startdata, date_enddata, detaill_cencal.Text.Trim(),
                    Session["UserID"]
                    );

                break;
            default:
                dt = new DataTable();
                dt.Columns.Add("STRUCKID");
                dt.Columns.Add("typecar");
                dt.Columns.Add("transportid");
                dt.Columns.Add("contrattype");
                dt.Columns.Add("truck");
                dt.Columns.Add("semitruck");
                dt.Columns.Add("contratid");
                dt.Columns.Add("Semiveh_text");
                //userId
                dt.Columns.Add("UserId");

                dt.Rows.Add(STRUCKID, Rdo_TypeCar.SelectedIndex, ddlvendorSemiTruck.SelectedValue, rdo_contratType1.SelectedValue, truck_head.SelectedItem.Text.Trim(), Truck_Semi.SelectedItem.Text.Trim(), TruckSemi_contract.SelectedValue, semi_veh_text.Text.Trim(),
                    Session["UserID"]
                    );
                break;
        }
        return dt;

    }
    #region Status_Vehicle
    protected DataTable DataStatus()
    {

        DataTable dt = new DataTable();
        dt.Columns.Add("STRUCKID");
        dt.Columns.Add("CACTIVE");
        dt.Columns.Add("SHEADREGISTERNO");
        dt.Columns.Add("STRAILERREGISTERNO");
        dt.Rows.Add
         (
            STRUCKID,
            active_blacklist.SelectedValue,
            StringReCarTyperegisNum(Rdo_TypeCar.SelectedValue.ToString()),
            string.Equals(Rdo_TypeCar.SelectedIndex.ToString(), "1") ? Truck_Semi.SelectedItem.Text.ToString() : ""
         );
        return dt;

    }
    #endregion
    #region Blacklist
    protected DataTable DataBlacklist()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("ACTIVE");
        dt.Columns.Add("Data_start");
        dt.Columns.Add("Data_end");
        dt.Columns.Add("Detail_cencal");
        dt.Columns.Add("sessionId");
        string date_startdata = "";
        string date_enddata = "";

        //date_startdata = string.Equals(date_start.Text.Trim().ToString(), "") ? string.Empty : DateTime.ParseExact(date_start.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture);
        //date_enddata = string.Equals(date_end.Text.Trim().ToString(), "") ? string.Empty : DateTime.ParseExact(date_end.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture);

        dt.Rows.Add(active_blacklist.SelectedValue, date_startdata, date_enddata, detaill_cencal.Text.Trim(), Session["UserID"]
       );
        return dt;
    }
    #endregion
    #endregion
    #region Function SAVE_TMS
    private void Save_TMS()
    {
        try
        {
            if (string.Equals(CGROUP, "1"))
            {

                try
                {
                    //ให้ปตท.อนุมัติกับระงับได้เท่านั้น
                    string Log_active = active_blacklist.SelectedValue.ToString();

                    DataTable Dt = VehicleBLL.Instance.StatusTMS(DataStatus(), DataBlacklist());
                    if (string.Equals(Dt.Rows[0][0].ToString(), "01"))
                    {
                        this.GetTypeTruckSave(Rdo_TypeCar.SelectedValue.ToString(), Log_active);
                        this.SetSapstatus(active_blacklist.SelectedValue, StringReCarTyperegisNum(Rdo_TypeCar.SelectedValue.ToString()));
                        this.registerno = string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)";
                        if (string.Equals(active_blacklist.SelectedValue, "Y"))
                        {

                            this.SendEmail(ConfigValue.EmailApproveCar, STRUCKID);
                        }
                        else if (string.Equals(active_blacklist.SelectedValue, "N"))
                        {
                            this.SendEmail(ConfigValue.EmailStopCar, STRUCKID);
                        }
                        else if (string.Equals(active_blacklist.SelectedValue, "D"))
                        {
                            this.SendEmail(ConfigValue.EmailBlacklistCar, STRUCKID);
                        }
                        alertSuccess("แก้ไขข้อมูลรถใน SAP สำเร็จ <br/> แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", string.IsNullOrEmpty(ViewState["GoBackTo"].ToString()) ? "truck_info.aspx" : ViewState["GoBackTo"].ToString());
                    }
                    else
                    {
                        alertFail(Dt.Rows[0][0].ToString());
                    }
                }
                catch (Exception ex)
                {
                    alertFail(ex.Message);

                    //alertSuccess("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ <br/> แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ", string.IsNullOrEmpty(ViewState["GoBackTo"].ToString()) ? "truck_info.aspx" : ViewState["GoBackTo"].ToString());
                }

                //บันทึกข้อมูลสำเร็จ

            }
            //else
            //{
            //    //บันทึกข้อมูลลงTMS(ให้ผู้ขนส่งสลับหางได้เท่านั้น
            //    DataTable valuereturn = VehicleBLL.Instance.ChangeVehicle(Rdo_TypeCar.SelectedIndex.ToString(), DataToDatatable());
            //    this.GetTypeTruckSave(Rdo_TypeCar.SelectedValue.ToString(), active_blacklist.SelectedValue.ToString());
            //    if (string.Equals(valuereturn.Rows[0][0].ToString(), "01"))
            //    {
            //        try
            //        {
            //            this.registerno = string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)";
            //            this.SendEmail(ConfigValue.EmailVehicleAdd, valuereturn.Rows[0][1].ToString());
            //            SystemFunction.AddToTREQ_DATACHANGE(valuereturn.Rows[0][1].ToString(), "H", Session["UserID"] + "", "0", string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + "(รถกึ่งพวง)", valuereturn.Rows[0][2].ToString(), "Y", "");
            //            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ <br /> รอการอนุมัติ", "Vendor_Detail.aspx");
            //        }
            //        catch (Exception)
            //        {

            //            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ <br /> รอการอนุมัติ", "Vendor_Detail.aspx");
            //        }

            //    }
            //    else
            //    {
            //        alertFail(valuereturn.Rows[0][0].ToString());
            //    }
            //}
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }
    private void ChangeVehicle()
    {
        try
        {

            //บันทึกข้อมูลลงTMS(ให้ผู้ขนส่งสลับหางได้เท่านั้น
            DataTable valuereturn = VehicleBLL.Instance.ChangeVehicle(Rdo_TypeCar.SelectedIndex.ToString(), DataToDatatable());
            if (string.Equals(valuereturn.Rows[0][0].ToString(), "01"))
            {
                try
                {
                    this.LogChangeTruck();
                    this.registerno = string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)";
                    this.SendEmail(ConfigValue.EmailVehicleAdd, valuereturn.Rows[0][1].ToString());
                    alertSuccess("แยกหัวลากหางลากข้อมูลรถใน TMS สำเร็จ <br/>", "Vendor_Detail.aspx");
                }
                catch (Exception)
                {

                    alertSuccess("แยกหัวลากหางลากข้อมูลรถใน TMS สำเร็จ <br/>", "Vendor_Detail.aspx");
                }

            }
            else
            {
                alertFail(valuereturn.Rows[0][0].ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region ResponseMenu
    protected void ResponseMenu(object sender, EventArgs e)
    {
        if (string.Equals(CGROUP.ToString(), "1"))
        {
            Response.Redirect(ViewState["GoBackTo"].ToString());
        }
        else
        {
            Response.Redirect("vendor_request.aspx");
        }

    }
    #endregion
    #region SentMail
    private void SendEmail(int TemplateID, string STTRUCKID)
    {
        try
        {
            DataSet dsSendEmail = new DataSet();
            string EmailList = string.Empty;

            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                Subject = Subject.Replace("{regisNo}", registerno);
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailVehicleAdd)
                {
                    string MonthName1 = string.Empty;
                    string MonthName2 = string.Empty;
                    string MonthName3 = string.Empty;
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Vehicle_approve.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedIndex.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? ddlVendorTruck.SelectedValue.ToString() : VendorId);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", Session["vendoraccountname"].ToString());
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{Comment}", " ");
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //Body = Body.Replace("{Remark}", txtReport1Remark.Value.Trim());
                    //MailService.SendMail("APIPAT.K@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,thanyavit.k@pttor.com," + EmailList + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body);
                    MailService.SendMail(EmailList + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body, "", "EmailVehicleAdd", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailTruckApprove)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Truck_History_View.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL("", string.Equals(CGROUP, "1") ? ddlVendorTruck.SelectedValue.ToString() : VendorId);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", CarBLL.Instance.LoadVendorName(STRANSPORTID));
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{Comment}", " ");
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //MailService.SendMail("raviwan.t@pttor.com,APIPAT.K@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,thanyavit.k@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,CHUTAPHA.C@PTTOR.COM," + EmailList + "," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body);
                    MailService.SendMail(EmailList + "," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body, "", "EmailTruckApprove", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailStopCar)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Truck_History_View.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL("", string.Equals(CGROUP, "1") ? ddlVendorTruck.SelectedValue.ToString() : VendorId);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", CarBLL.Instance.LoadVendorName(STRANSPORTID));
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{comment}", detaill_cencal.Text.ToString());
                    Body = Body.Replace("{link}", string.Empty);//ConfigValue.GetClickHere(Link));
                    Body = Body.Replace("{ownner_name}", string.IsNullOrEmpty(Session["vendoraccountname"].ToString()) ? "ส่วนระบบและประมวลผลการขนส่ง " : Session["vendoraccountname"].ToString());
                    //MailService.SendMail("APIPAT.K@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,thanyavit.k@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,CHUTAPHA.C@PTTOR.COM," + EmailList + "," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body);
                    MailService.SendMail(EmailList + "," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body, "", "EmailStopCar", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailApproveCar)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Truck_History_View.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL("", string.Equals(CGROUP, "1") ? ddlVendorTruck.SelectedValue.ToString() : VendorId);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", CarBLL.Instance.LoadVendorName(STRANSPORTID));
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{comment}", detaill_cencal.Text.ToString());
                    Body = Body.Replace("{link}", string.Empty);//ConfigValue.GetClickHere(Link));
                    Body = Body.Replace("{ownner_name}", string.IsNullOrEmpty(Session["vendoraccountname"].ToString()) ? "ส่วนระบบและประมวลผลการขนส่ง " : Session["vendoraccountname"].ToString());
                    //MailService.SendMail("APIPAT.K@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body, "", "EmailApproveCar", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailBlacklistCar)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Truck_History_View.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL("", string.Equals(CGROUP, "1") ? ddlVendorTruck.SelectedValue.ToString() : VendorId);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", CarBLL.Instance.LoadVendorName(STRANSPORTID));
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{comment}", detaill_cencal.Text.ToString());
                    Body = Body.Replace("{link}", string.Empty);//ConfigValue.GetClickHere(Link));
                    Body = Body.Replace("{ownner_name}", string.IsNullOrEmpty(Session["vendoraccountname"].ToString()) ? "ส่วนระบบและประมวลผลการขนส่ง " : Session["vendoraccountname"].ToString());
                    //MailService.SendMail("APIPAT.K@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,thanyavit.k@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,CHUTAPHA.C@PTTOR.COM," + EmailList + "," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body);
                    MailService.SendMail(EmailList + "," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorId, true, false), Subject, Body, "", "EmailBlacklistCar", ColumnEmailName);
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #region stautsAll
    public void SetSapstatus(string active_blacklist, string SHEADREGISTERNO)
    {
        try
        {
            SapStatus status = new SapStatus();
            if (string.Equals(active_blacklist, "Y"))
            {
                status.ApproveSap(SHEADREGISTERNO);
            }
            else if (string.Equals(active_blacklist, "N"))
            {
                status.SapLockStatus(SHEADREGISTERNO);
            }
            else if (string.Equals(active_blacklist, "D"))
            {
                status.SapBlacklist(SHEADREGISTERNO);
            }
            else
            {
                alertFail("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อผู้ดูแลระบบ");
            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }

    }
    #endregion
    #region Log
    #region Log สำหรับ อนุญาติและระงับใช่งาน
    private void GetTypeTruckSave(string TypeTruck, string Approve)
    {
        try
        {
            string OldData_Active = string.Empty;
            string NewData_Active = string.Empty;
            if (string.Equals(CACTIVE_STATUS, "Y"))
            {
                OldData_Active = "<span class=\"green\">อนุญาตใช้งาน</span>";
                NewData_Active = string.Equals(Approve, "Y") ? "<span class=\"green\">อนุญาตใช้งาน</span>" : "<span class=\"red\">ระงับการใช้งานชั่วคราว</span>";
            }
            else
            {
                OldData_Active = "<span class=\"red\">ระงับการใช้งานชั่วคราว</span>";
                NewData_Active = string.Equals(Approve, "Y") ? "<span class=\"green\">อนุญาตใช้งาน</span>" : "<span class=\"red\">ระงับการใช้งานชั่วคราว</span>";
            }

            switch (TypeTruck)
            {
                case "0":
                    #region บันทึก Log
                    LOG.Instance.SaveLog("รข.เปลี่ยนแปลงอนุญาตการใช้งานจาก", ddl_truck.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + NewData_Active, OldData_Active);
                    #endregion
                    break;
                default:
                    #region บันทึก Logหัว
                    LOG.Instance.SaveLog("รข.เปลี่ยนแปลงอนุญาตการใช้งานจาก", truck_head.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + NewData_Active, OldData_Active);
                    #endregion
                    #region บันทึก Logหาง
                    LOG.Instance.SaveLog("รข.เปลี่ยนแปลงอนุญาตการใช้งานจาก", Truck_Semi.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + NewData_Active, OldData_Active);
                    #endregion
                    break;
            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region Log สำหรับผขส แยกรถ
    private void LogChangeTruck()
    {
        try
        {
            #region บันทึก Logหัว
            LOG.Instance.SaveLog("ผขส.ปลดการจับคู่จากทะเบียนรถ", truck_head.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"red\">" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก) <br/>" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก)</span>", "");
            #endregion
            #region บันทึก Logหาง
            LOG.Instance.SaveLog("ผขส.ปลดการจับคู่จากทะเบียนรถ", Truck_Semi.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"red\">" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก) <br/>" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก)</span>", "");
            #endregion
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }

    }
    #endregion

    #endregion
}
class SapStatus
{
    #region SapBlacklist And stopprocess
    public void ApproveSap(string SHEADREGISTERNO)
    {
        SAP_STATUS veh_update = new SAP_STATUS();
        veh_update.VEH_NO = SHEADREGISTERNO;
        string sMsg = veh_update.ApproveUse_Vehicle_SYNC_OUT_SI();
    }
    public string SapLockStatus(string regisnumber)
    {

        SAP_STATUS veh_update = new SAP_STATUS();
        veh_update.VEH_NO = regisnumber;
        return veh_update.LOCK_Vehicle_SYNC_OUT_SI();
    }
    public string SapBlacklist(string regisnumber)
    {
        SAP_STATUS veh_update = new SAP_STATUS();
        veh_update.VEH_NO = regisnumber;
        return veh_update.BlACKLIST_Vehicle_SYNC_OUT_SI();
    }

    #endregion
}


