﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SAP_VEH.aspx.cs" Inherits="SAP_VEH"
    StylesheetTheme="Aqua" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            color: #FF9900;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-left: 10%">
        <dx:ASPxRoundPanel ID="rplUpdateSAP" ClientInstanceName="rplUpdateSAP" runat="server"
            Width="90%" HeaderText="SAP_SYNCOUT_SI">
            <PanelCollection>
                <dx:PanelContent>
                    <table width="100%">
                        <tr>
                            <td>
                                ทะเบียนหัว
                                <dx:ASPxComboBox ID="cboHeadRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboHeadRegist"
                                    EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL"
                                    OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="IsAvailable" Width="180px">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                        <dx:ListBoxColumn Caption="STRUCKID" FieldName="STRUCKID" Width="100px" />
                                    </Columns>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                </asp:SqlDataSource>
                            </td>
                            <td>
                                CARRIER :
                                <dx:ASPxTextBox ID="txtCARRIER" ClientInstanceName="txtCARRIER" runat="server" NullText="TD carrier (vendor account number)">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                CLASS_GRP :
                                <dx:ASPxTextBox ID="txtCLASS_GRP" ClientInstanceName="txtCLASS_GRP" runat="server"
                                    NullText="TD-Vehicle Classification Group">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                DEPOT :
                                <dx:ASPxTextBox ID="txtDEPOT" ClientInstanceName="txtDEPOT" runat="server" NullText="Plant">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ROUTE :
                                <dx:ASPxTextBox ID="txtROUTE" ClientInstanceName="txtROUTE" runat="server" NullText="TD-F vehicle route"
                                    Text="ZTD001">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                TPPOINT :
                                <dx:ASPxTextBox ID="txtTPPOINT" ClientInstanceName="txtTPPOINT" runat="server" NullText="Transportation planning point">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                VEH_TEXT :
                                <dx:ASPxTextBox ID="txtVEH_TEXT" ClientInstanceName="txtVEH_TEXT" runat="server"
                                    NullText="TD vehicle header text">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                VHCLSIGN :
                                <dx:ASPxTextBox ID="txtVHCLSIGN" ClientInstanceName="txtVHCLSIGN" runat="server"
                                    NullText="TD-Vehicle Call Sign">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnUpdateVehicle" ClientInstanceName="btnUpdateVehicle" runat="server"
                                    Text="UpdateVehicle" OnClick="btnUpdateVehicle_Click">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnUpdateVehicleByClass" ClientInstanceName="btnUpdateVehicleByClass"
                                    runat="server" Text="UpdateVehicleByClass" OnClick="btnUpdateVehicleByClass_Click">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <hr />
        <dx:ASPxRoundPanel ID="rplSAP_VEHICLE_MASTER_DATA" ClientInstanceName="rplSAP_VEHICLE_MASTER_DATA"
            runat="server" Width="90%" HeaderText="SAP_VEHICLE_MASTER_DATA">
            <PanelCollection>
                <dx:PanelContent>
                    <table width="100%">
                        <thead>
                            <tr>
                                <td>
                                    <dx:ASPxComboBox ID="cboHeadRegist0" runat="server" CallbackPageSize="30" ClientInstanceName="cboHeadRegist"
                                        EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist0_OnItemRequestedByValueSQL"
                                        OnItemsRequestedByFilterCondition="cboHeadRegist0_OnItemsRequestedByFilterConditionSQL"
                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="IsAvailable" Width="180px"
                                        OnInit="cboHeadRegist0_Init">
                                    </dx:ASPxComboBox>
                                    <asp:SqlDataSource ID="sdsTruck0" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <hr />
        <dx:ASPxRoundPanel ID="SAP_Create_VEHICLE_Class" ClientInstanceName="rplSAP_Create_VEHICLE_Class"
            runat="server" Width="90%" HeaderText="SAP_Create_VEHICLE_Class">
            <PanelCollection>
                <dx:PanelContent>
                    <table width="100%">
                        <tr>
                            <td>
                                ทะเบียนหัว
                                <dx:ASPxComboBox ID="cbbVehicle_Create" runat="server" CallbackPageSize="30" ClientInstanceName="cbbVehicle_Create"
                                    EnableCallbackMode="True" OnItemRequestedByValue="cbbVehicle_Create_OnItemRequestedByValueSQL"
                                    OnItemsRequestedByFilterCondition="cbbVehicle_Create_OnItemsRequestedByFilterConditionSQL"
                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="IsAvailable" Width="180px"
                                    OnInit="cbbVehicle_Create_Init">
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="sdsVehicle_Create" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                </asp:SqlDataSource>
                            </td>
                            <td>
                                LANGUAGE :
                                <dx:ASPxTextBox ID="txtLANGUAGE_Create" ClientInstanceName="txtLANGUAGE" runat="server"
                                    Text="TH" Width="30px" NullText="LANGUAGE">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                VEH_TEXT :
                                <dx:ASPxTextBox ID="txtVEH_TEXT_Create" ClientInstanceName="txtVEH_TEXT" runat="server"
                                    NullText="TD vehicle header text">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                VEH_TYPE :
                                <dx:ASPxTextBox ID="txtVEH_TYPE_Create" ClientInstanceName="txtVEH_TYPE_Create" runat="server"
                                    NullText="TD vehicle type look up LSTVEH_TYPE">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                ROUTE :
                                <dx:ASPxTextBox ID="txtROUTE_Create" ClientInstanceName="txtROUTE_Create" runat="server"
                                    NullText="TD-F vehicle route" Text="ZTD001">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                DEPOT :
                                <dx:ASPxTextBox ID="txtDEPOT_create" ClientInstanceName="txtDEPOT_create" runat="server"
                                    NullText="Plant">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                TPPOINT :
                                <dx:ASPxTextBox ID="txtTPPOINT_create" ClientInstanceName="txtTPPOINT_create" runat="server"
                                    NullText="Transportation planning point">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                VHCLSIGN :
                                <dx:ASPxTextBox ID="txtVHCLSIGN_Create" ClientInstanceName="txtVHCLSIGN_Create" runat="server"
                                    NullText="TD-Vehicle Call Sign">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                CLASS_GRP :
                                <dx:ASPxTextBox ID="txtCLASS_GRP_Create" ClientInstanceName="txtCLASS_GRP_Create"
                                    runat="server" NullText="TD-Vehicle Classification Group">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                CARRIER :
                                <dx:ASPxTextBox ID="txtCARRIER_Create" ClientInstanceName="txtCARRIER_Create" runat="server"
                                    NullText="TD carrier (vendor account number)">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                VEH_STATUS :
                                <dx:ASPxTextBox ID="txtVEH_STATUS_Create" ClientInstanceName="txtVEH_STATUS_Create"
                                    runat="server" NullText="TD vehicle status (Look up : LSTVEH_STAT)">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                VEH_ID :
                                <dx:ASPxTextBox ID="txtVEH_ID_Create" ClientInstanceName="txtVEH_ID_Create" runat="server"
                                    NullText="TD vehicle identifier (หมายเลขแชชซีย์)">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                REG_DATE :
                                <dx:ASPxTextBox ID="txtREG_DATE_Create" ClientInstanceName="txtREG_DATE_Create" runat="server"
                                    NullText="TD-Vehicle Registered Date (วันทีจดทะเบียน)">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                SEQ_NMBR :
                                <dx:ASPxTextBox ID="txtSEQ_NMBR_Create" ClientInstanceName="txtSEQ_NMBR_Create" runat="server"
                                    NullText="TD master data sequence number">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                TU_NUMBER :
                                <dx:ASPxTextBox ID="txtTU_NUMBER_Create" ClientInstanceName="txtTU_NUMBER_Create"
                                    runat="server" NullText="TD transport unit number">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnCreateVehicle" ClientInstanceName="btnCreateVehicle" runat="server"
                                    Text="CreateVehicleByClass" OnClick="btnCreateVehicle_Click">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnCreateVehicle1" ClientInstanceName="btnCreateVehicle1" runat="server"
                                    Text="CreateVehicleFixTest" OnClick="btnCreateVehicle1_Click1">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnCreateTUFix" ClientInstanceName="btnCreateTUFix" runat="server"
                                    Text="CreateTUFix" OnClick="btnCreateTUFix_Click">
                                </dx:ASPxButton>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <dx:ASPxMemo ID="memNote" runat="server" Width="90%" AutoResizeWithContainer="true"
                                    Height="71px">
                                </dx:ASPxMemo>
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <hr />
        <dx:ASPxRoundPanel ID="SAP_Create_Driver" ClientInstanceName="rplSAP_Create_Driver_Class"
            runat="server" Width="90%" HeaderText="SAP_Create_Driver_Class">
            <PanelCollection>
                <dx:PanelContent>
                    <table width="100%">
                    <thead><tr><td colspan="4">/*4700021913	ที	เจตนา	1/18/2013 2:57:25 PM	SP_TMS	4/29/2014 1:00:02 AM	SP_TMS	3301400871168	PHTDPRT	6นม.00048/54	1*/</td></tr></thead>
                        <tr>
                            <td>
                                <span class="style1">FIRST_NAME</span>:<br />
                                <dx:ASPxTextBox ID="txtDrvFIRST_NAME" runat="server">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <span class="style1">LAST_NAME</span>:<br />
                                <dx:ASPxTextBox ID="txtDrvLAST_NAME" runat="server">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <span class="style2">CARRIER</span>:<br />
                                <dx:ASPxTextBox ID="txtDrvCARRIER" runat="server" Text="PHTDPRT">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                Phone_1:
                                <dx:ASPxTextBox ID="txtDrvPhone_1" runat="server">
                                </dx:ASPxTextBox>
                                Phone_2:
                                <dx:ASPxTextBox ID="txtDrvPhone_2" runat="server">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="style1">LICENSE_NO</span>:<br />
                                <dx:ASPxTextBox ID="txtDrvLICENSE_NO" runat="server">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <span class="style2">LICENSENOE_FROM</span>:<br />
                                <dx:ASPxTextBox ID="txtDrvLICENSENOE_FROM" runat="server">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <span class="style2">LICENSENOE_TO</span>:<br />
                                <dx:ASPxTextBox ID="txtDrvLICENSENOE_TO" runat="server">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <span class="style1">PERS_CODE</span>:
                                <dx:ASPxTextBox ID="txtDrvPERS_CODE" runat="server">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <dx:ASPxMemo ID="txtDriver_output" runat="server" Width="96%" Height="42px">
                                </dx:ASPxMemo>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                Output:<br />
                                <dx:ASPxButton ID="btnCreateDriver" ClientInstanceName="btnCreateDriver" runat="server"
                                    Text="CreateDriverByClass" OnClick="btnCreateDriver_Click">
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnUpdateDriver" ClientInstanceName="btnUpdateDriver" runat="server"
                                    Text="UpdateDriverByClass" OnClick="btnUpdateDriver_Click">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
    </div>
    <asp:TextBox ID="txtVENNO_OnHold" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTUNO_OnHold" runat="server"></asp:TextBox>
    <asp:Button ID="btnTestTruckOnHold" runat="server" Text="Test Send Mail Truck On Hold"
        OnClick="btnTestTruckOnHold_Click" /><hr />
        <asp:TextBox ID="txtMailTo" runat="server"></asp:TextBox>
        <asp:Button ID="btnTestmail" runat="server" Text="Test Send Mail " 
        onclick="btnTestmail_Click" />
    </form>
</body>
</html>
