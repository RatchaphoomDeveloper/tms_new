﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPanel;
using System.Text;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxCallback;

public partial class planschedule : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/bookcar/{0}/{2}/{1}/";
    double def_Double;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw2.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw3.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        #endregion

        //lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        lblDate2.Text = DateTime.Today.ToString("dd/MM/yyyy");

        if (!IsPostBack)
        {
            Session["CheckPermission"] = null;

            bool chkurl = false;
            string AddEdit = "";
            //if (Session["cPermission"] != null)
            //{
            //    string[] url = (Session["cPermission"] + "").Split('|');
            //    string[] chkpermision;
            //    bool sbreak = false;

            //    foreach (string inurl in url)
            //    {
            //        chkpermision = inurl.Split(';');
            //        if (chkpermision[0] == "11")
            //        {
            //            switch (chkpermision[1])
            //            {
            //                case "0":
            //                    chkurl = false;
            //                    break;
            //                case "1":
            //                    chkurl = true;
            //                    break;

            //                case "2":
            //                    chkurl = true;
            //                    AddEdit = "1";
            //                    break;
            //            }
            //            sbreak = true;
            //        }

            //        if (sbreak == true) break;
            //    }
            //}

            //if (chkurl == false)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //}

            Session["CheckPermission"] = AddEdit;

            if ("" + Session["CGROUP"] == "2")
            {
                cboTerminal1.Value = Session["SVDID"] + "";
                //cboTerminal.Value = Session["SVDID"] + "";
                cboTerminal2.Value = Session["SVDID"] + "";
            }

            Cache.Remove(sds2.CacheKeyDependency);
            Cache[sds2.CacheKeyDependency] = new object();
            sds2.DataBind();
            gvw2.DataBind();

            Cache.Remove(sds3.CacheKeyDependency);
            Cache[sds3.CacheKeyDependency] = new object();
            sds3.DataBind();
            gvw3.DataBind();

            Cache.Remove(sds1.CacheKeyDependency);
            Cache[sds1.CacheKeyDependency] = new object();
            sds1.DataBind();
            gvw1.DataBind();

            dtePlan1.Value = DateTime.Now;
            var dt = new List<BDT1>();
            Session["bdt1"] = dt;
            Session["tTerminalID"] = null;

            LogUser("11", "R", "เปิดดูข้อมูลหน้า จัดแผนการขนส่ง", "");

        }

    }


    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {


    }


    protected void xcpn_Load(object sender, EventArgs e)
    {
        if (ASPxPageControl1.ActiveTabIndex == 0)
        {
            //gvw.DataBind();
        }
        else if (ASPxPageControl1.ActiveTabIndex == 1)
        {
            gvw2.DataBind();
        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {
            gvw1.DataBind();

        }
        else
        {
            gvw3.DataBind();
        }

        listImportFileDetail();
        ShowCountCar();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        DateTime outdate;

        switch (paras[0])
        {
            case "AddNewRow":

                if (ASPxPageControl1.ActiveTabIndex == 0)
                {
                    //gvw.AddNewRow();
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {

                    gvw1.AddNewRow();
                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                    cmbTerminal1.Value = cboTerminal1.Value + "";
                }


                SetVisibleControl();
                break;
            case "Save":
                #region save
                if ("" + Session["CheckPermission"] == "1")
                {
                    string para = (e.Parameter.Split(';')[1] == "btnSave") ? "-1" : e.Parameter.Split(';')[1];


                    // อ้างอิงคอนโทรลในเทมเพลดของ grid 
                    object NPLANID = null;
                    object NNO = null;
                    object NPLANLISTID = null;
                    object NID = null;

                    ASPxDateEdit dtePlan = new ASPxDateEdit();
                    ASPxTextBox txtTime = new ASPxTextBox();
                    ASPxComboBox cmbTerminal = new ASPxComboBox();
                    ASPxDateEdit dteDelivery = new ASPxDateEdit();
                    ASPxComboBox cbxTimeWindow = new ASPxComboBox();
                    ASPxComboBox cboHeadRegist = new ASPxComboBox();
                    ASPxComboBox cboTrailerRegist = new ASPxComboBox();
                    ASPxTextBox txtEmployeeID = new ASPxTextBox();

                    ASPxTextBox txtDrop = new ASPxTextBox();
                    ASPxComboBox cboDelivery = new ASPxComboBox();
                    ASPxTextBox txtShipto = new ASPxTextBox();
                    ASPxTextBox txtValue = new ASPxTextBox();


                    if (ASPxPageControl1.ActiveTabIndex == 0)
                    {

                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 1)
                    {
                        NID = gvw2.GetRowValues(int.Parse(para), "NID");

                        dtePlan = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dtePlan");
                        txtTime = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtTime");
                        cmbTerminal = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
                        dteDelivery = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dteDelivery");
                        cbxTimeWindow = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cbxTimeWindow");
                        cboHeadRegist = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboHeadRegist");
                        cboTrailerRegist = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboTrailerRegist");
                        txtEmployeeID = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtEmployeeID");
                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 2)
                    {
                        NPLANID = gvw1.GetRowValues(int.Parse(para), "NPLANID");
                        NNO = gvw1.GetRowValues(int.Parse(para), "NNO");
                        NPLANLISTID = gvw1.GetRowValues(int.Parse(para), "SPLANLISTID");

                        dtePlan = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dtePlan");
                        txtTime = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtTime");
                        cmbTerminal = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                        dteDelivery = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dteDelivery");
                        cbxTimeWindow = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cbxTimeWindow");
                        cboHeadRegist = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboHeadRegist");
                        cboTrailerRegist = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboTrailerRegist");

                        txtDrop = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtDrop");
                        cboDelivery = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboDelivery");
                        txtShipto = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShipto");
                        txtValue = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtValue");
                    }

                    if (!(ASPxPageControl1.ActiveTabIndex == 3))//ถ้าไม่ใช่แท๊บสรุปการจัดแผนประจำวัน
                    {
                        //แก้ไข
                        if (NPLANID + "" != "")
                        {

                            int PID = Convert.ToInt32(NPLANID);

                            string OUTBOUNDCAPACITY = CommonFunction.Get_Value(sql, "SELECT SUM(nvl(PL.NVALUE,0)) as SUMVALUE FROM TPLANSCHEDULEList pl WHERE pl.CACTIVE = '1' AND PL.NPLANID = '" + PID + "' AND PL.SPLANLISTID != '" + NPLANLISTID + "'");

                            string SCAPACITY;

                            string ValueTruck;

                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");


                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            int SUMCAPACITY = int.TryParse(OUTBOUNDCAPACITY, out num) ? num : 0;
                            int nValue1 = int.TryParse(txtValue.Text, out num) ? num : 0;

                            SUMCAPACITY = SUMCAPACITY + nValue1;

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TPLANSCHEDULE SET SVENDORID = :SVENDORID,SPLANDATE = :SPLANDATE,SPLANTIME= :SPLANTIME, STERMINALID = :STERMINALID, DDELIVERY= :DDELIVERY, STIMEWINDOW = :STIMEWINDOW, SHEADREGISTERNO = :SHEADREGISTERNO, STRAILERREGISTERNO = :STRAILERREGISTERNO, DUPDATE = sysdate, SUPDATE = :SUPDATE WHERE NPLANID = :NPLANID ";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    string venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "')");

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                                    com.Parameters.Add(":SPLANDATE", OracleType.DateTime).Value = DateTime.TryParseExact(dtePlan.Text + " " + txtTime.Text.Trim(), "d/M/yyyy HH.mm", new CultureInfo("th-TH"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now;
                                    com.Parameters.Add(":SPLANTIME", OracleType.VarChar).Value = txtTime.Text.Trim();
                                    com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cmbTerminal.Value.ToString();
                                    com.Parameters.Add(":DDELIVERY", OracleType.DateTime).Value = Convert.ToDateTime(dteDelivery.Value);
                                    com.Parameters.Add(":STIMEWINDOW", OracleType.Number).Value = Convert.ToInt32(cbxTimeWindow.Value);
                                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = PID;
                                    com.ExecuteNonQuery();

                                }

                                if (NPLANLISTID + "" != "")
                                {
                                    int PlanListID = Convert.ToInt32(NPLANLISTID);
                                    int num1;
                                    int num2;
                                    string strsql1 = "UPDATE TPLANSCHEDULEList SET NDROP = :NDROP, SDELIVERYNO = :SDELIVERYNO, SSHIPTO = :SSHIPTO, NVALUE = :NVALUE WHERE SPLANLISTID = :SPLANLISTID";
                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NDROP", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                        com1.Parameters.Add(":SSHIPTO", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":NVALUE", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.Parameters.Add(":SPLANLISTID", OracleType.Number).Value = PlanListID;
                                        com1.ExecuteNonQuery();

                                    }
                                }
                                else
                                {
                                    //ถ้าไม่มี ID sPlanListID ให้ทำการ Insert ข้อมูล ลง TPLANSCHDULELIST
                                    int num1;
                                    int num2;

                                    string genidsPlanListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULEList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                    string strsql1 = "INSERT INTO TPLANSCHEDULEList(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,nValue,cActive) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,:nValue,'1')";

                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {

                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genidsPlanListID;
                                        com1.Parameters.Add(":nPlanID", OracleType.Number).Value = PID;
                                        com1.Parameters.Add(":nDrop", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = cboDelivery.Value + "";
                                        com1.Parameters.Add(":nValue", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.ExecuteNonQuery();

                                    }
                                }
                            }

                            LogUser("11", "E", "แก้ไขข้อมูลหน้า จัดแผนการขนส่งสินค้า", PID + "");
                        }//insert ข้อมูล
                        else
                        {

                            var bdt1 = (List<BDT1>)Session["bdt1"];

                            string SCAPACITY;

                            string ValueTruck;

                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            int SUMCAPACITY = bdt1.Select(o => o.dtValue).Sum();

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }


                            //กรณีเพิ่มหลาย Timewindows
                            var oTimeWindow = bdt1.Select(o => o.sTimeWindow).Distinct();

                            foreach (var TimeWindows in oTimeWindow)
                            {
                                string genidnPlanID = CommonFunction.Gen_ID(sql, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                                string strsql = "INSERT INTO TPLANSCHEDULE(SEMPLOYEEID,SVENDORID,nPlanID,nNo,CFIFO,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:SEMPLOYEEID,:SVENDORID,:nPlanID,1,:CFIFO,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate)";
                                string strsql1 = "INSERT INTO TPLANSCHEDULEList(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,nValue) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:nValue)";

                                //ข็อมูล

                                using (OracleConnection con1 = new OracleConnection(sql))
                                {
                                    con1.Open();
                                    using (OracleCommand com = new OracleCommand(strsql, con1))
                                    {

                                        string venderid = "";
                                        string tmpEmployeeID = "";

                                        venderid = CommonFunction.Get_Value(con1, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value.ToString() + "')");

                                        com.Parameters.Clear();
                                        com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                                        com.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                        //ถ้าpagecontrol เป็นหน้าที่จัดแผนแบบเร่งด่วนให้ CFIFO เท่ากับ 1

                                        if (ASPxPageControl1.ActiveTabIndex == 1)
                                        {
                                            com.Parameters.Add(":CFIFO", OracleType.Char).Value = '1';
                                            com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now;
                                            tmpEmployeeID = txtEmployeeID.Text;
                                        }
                                        else
                                        {
                                            com.Parameters.Add(":CFIFO", OracleType.Char).Value = '0';
                                            com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.TryParseExact(dtePlan1.Text + " " + DateTime.Now.ToString("HH.mm"), "d/M/yyyy HH.mm", new CultureInfo("th-TH"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now;
                                        }

                                        com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = tmpEmployeeID;
                                        com.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = cmbTerminal.Value.ToString();
                                        com.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = DateTime.TryParseExact(dtePlan.Text + " " + txtTime.Text.Trim(), "d/M/yyyy HH.mm", new CultureInfo("th-TH"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now;
                                        com.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = txtTime.Text;
                                        com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = Convert.ToDateTime(dteDelivery.Value);
                                        com.Parameters.Add(":sTimeWindow", OracleType.Number).Value = TimeWindows;
                                        com.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = cboHeadRegist.Value.ToString();
                                        com.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                        com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com.ExecuteNonQuery();
                                    }


                                    var sbdt1 = bdt1.Where(o => o.sTimeWindow == TimeWindows).ToList();

                                    foreach (var dr in sbdt1)
                                    {
                                        string genListID = CommonFunction.Gen_ID(con1, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULEList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");

                                        using (OracleCommand com1 = new OracleCommand(strsql1, con1))
                                        {

                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genListID;
                                            com1.Parameters.Add(":nPlanID", OracleType.Number).Value = genidnPlanID;
                                            com1.Parameters.Add(":nDrop", OracleType.Number).Value = dr.dtDrop;
                                            com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = dr.dtShipto;
                                            com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = dr.dtDeliveryNo;
                                            com1.Parameters.Add(":nValue", OracleType.Number).Value = dr.dtValue;
                                            com1.ExecuteNonQuery();
                                        }
                                    }


                                    if (ASPxPageControl1.ActiveTabIndex == 1)
                                    {
                                        string strsql2 = "UPDATE TFIFO SET CPLAN = '1' WHERE NID = :NID";
                                        using (OracleCommand com2 = new OracleCommand(strsql2, con1))
                                        {
                                            com2.Parameters.Clear();
                                            com2.Parameters.Add(":NID", OracleType.Number).Value = Convert.ToInt32(NID);
                                            com2.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }

                            LogUser("11", "I", "บันทึกข้อมูลหน้า จัดแผนการขนส่งสินค้า", "");
                        }
                    }
                    else //แท็บสรุปการจัดแผนประจำวัน
                    {
                        NPLANID = gvw3.GetRowValues(int.Parse(para), "NPLANID");
                        NPLANLISTID = gvw3.GetRowValues(int.Parse(para), "SPLANLISTID");

                        int NPlanID = Convert.ToInt32(NPLANID);

                        ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

                        cboHeadRegist = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");
                        cboTrailerRegist = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboTrailerRegist");

                        ASPxComboBox cmbPersonalNo = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cmbPersonalNo");
                        ASPxTextBox txtRemark = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtRemark");

                        txtDrop = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDrop");
                        cboDelivery = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboDelivery");
                        txtShipto = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShipto");
                        txtValue = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtValue");

                        if (rblCheck.SelectedIndex == 0)
                        {
                            string OUTBOUNDCAPACITY = CommonFunction.Get_Value(sql, "SELECT SUM(nvl(PL.NVALUE,0)) as SUMVALUE FROM TPLANSCHEDULEList pl WHERE pl.CACTIVE = '1' AND PL.NPLANID = '" + NPLANID + "' AND PL.SPLANLISTID != '" + NPLANLISTID + "'");

                            string ValueTruck;
                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            int SUMCAPACITY = int.TryParse(OUTBOUNDCAPACITY, out num) ? num : 0;
                            int nValue1 = int.TryParse(txtValue.Text, out num) ? num : 0;

                            SUMCAPACITY = SUMCAPACITY + nValue1;

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TPLANSCHEDULE SET SVENDORID = :SVENDORID,SEMPLOYEEID = :SEMPLOYEEID,SREMARK = :SREMARK,DPLAN = sysdate +1,SHEADREGISTERNO = :SHEADREGISTERNO, STRAILERREGISTERNO = :STRAILERREGISTERNO, DUPDATE = sysdate, SUPDATE = :SUPDATE WHERE NPLANID = :NPLANID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    string venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "') ");

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                                    com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value;
                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com.ExecuteNonQuery();

                                }

                                if (NPLANLISTID + "" != "")
                                {
                                    int PlanListID = Convert.ToInt32(NPLANLISTID);
                                    int num1;
                                    int num2;
                                    string strsql1 = "UPDATE TPLANSCHEDULEList SET NDROP = :NDROP, SDELIVERYNO = :SDELIVERYNO, SSHIPTO = :SSHIPTO, NVALUE = :NVALUE WHERE SPLANLISTID = :SPLANLISTID";
                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NDROP", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                        com1.Parameters.Add(":SSHIPTO", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":NVALUE", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.Parameters.Add(":SPLANLISTID", OracleType.Number).Value = PlanListID;
                                        com1.ExecuteNonQuery();

                                    }
                                }
                                else
                                {
                                    //ถ้าไม่มี ID sPlanListID ให้ทำการ Insert ข้อมูล ลง TPLANSCHDULELIST
                                    int num1;
                                    int num2;

                                    string genidsPlanListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULEList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                    string strsql1 = "INSERT INTO TPLANSCHEDULEList(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,nValue,cActive) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,:nValue,'1')";

                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {

                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genidsPlanListID);
                                        com1.Parameters.Add(":nPlanID", OracleType.Number).Value = NPlanID;
                                        com1.Parameters.Add(":nDrop", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = cboDelivery.Value + "";
                                        com1.Parameters.Add(":nValue", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.ExecuteNonQuery();

                                    }
                                }
                            }
                        }
                        else if (rblCheck.SelectedIndex == 1)
                        {
                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TPLANSCHEDULE SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com.ExecuteNonQuery();
                                }

                                string strsql1 = "UPDATE TPLANSCHEDULEList SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }
                        else if (rblCheck.SelectedIndex == 2)
                        {
                            var bdt2 = (List<BDT1>)Session["bdt1"];

                            string ValueTruck;
                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            int SUMCAPACITY = bdt2.Select(o => o.dtValue).Sum();

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();

                                string strsql = "UPDATE TPLANSCHEDULE SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com.ExecuteNonQuery();
                                }

                                string strsql1 = "UPDATE TPLANSCHEDULEList SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com1.ExecuteNonQuery();
                                }

                                DataTable dt = new DataTable();
                                dt = CommonFunction.Get_Data(con, "SELECT nvl(CFIFO,'0') AS CFIFO,STERMINALID,SPLANDATE,SPLANTIME,DDELIVERY,STIMEWINDOW from TPLANSCHEDULE WHERE CACTIVE = '1' AND NPLANID = " + NPlanID);

                                string genidnPlanID = CommonFunction.Gen_ID(sql, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                                string strsql2 = "INSERT INTO TPLANSCHEDULE(SEMPLOYEEID,SREMARK,SVENDORID,nPlanID,nNo,CFIFO,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:SEMPLOYEEID,:SREMARK,:SVENDORID,:nPlanID,1,:CFIFO,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate)";
                                string strsql3 = "INSERT INTO TPLANSCHEDULEList(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,nValue) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:nValue)";


                                if (dt.Rows.Count > 0)
                                {
                                    //ข็อมูล
                                    using (OracleCommand com2 = new OracleCommand(strsql2, con))
                                    {

                                        string venderid = "";

                                        venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "') ");

                                        com2.Parameters.Clear();
                                        com2.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value;
                                        com2.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                        com2.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                                        com2.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                        com2.Parameters.Add(":CFIFO", OracleType.Char).Value = Convert.ToChar(dt.Rows[0]["CFIFO"] + "");
                                        com2.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = dt.Rows[0]["STERMINALID"] + "";
                                        com2.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now.AddDays(1);
                                        com2.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = Convert.ToDateTime(dt.Rows[0]["SPLANDATE"]);
                                        com2.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = dt.Rows[0]["SPLANTIME"] + "";
                                        com2.Parameters.Add(":dDelivery", OracleType.DateTime).Value = Convert.ToDateTime(dt.Rows[0]["DDELIVERY"]);
                                        com2.Parameters.Add(":sTimeWindow", OracleType.Number).Value = Convert.ToInt32(dt.Rows[0]["STIMEWINDOW"]);
                                        com2.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = cboHeadRegist.Value.ToString();
                                        com2.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                        com2.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com2.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com2.ExecuteNonQuery();
                                    }

                                    foreach (var dr in bdt2)
                                    {
                                        string genListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULEList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");

                                        using (OracleCommand com3 = new OracleCommand(strsql3, con))
                                        {

                                            com3.Parameters.Clear();
                                            com3.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genListID);
                                            com3.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                            com3.Parameters.Add(":nDrop", OracleType.Number).Value = dr.dtDrop;
                                            com3.Parameters.Add(":sShipTo", OracleType.VarChar).Value = dr.dtShipto;
                                            com3.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = dr.dtDeliveryNo;
                                            com3.Parameters.Add(":nValue", OracleType.Number).Value = dr.dtValue;
                                            com3.ExecuteNonQuery();
                                        }
                                    }

                                }

                            }
                        }




                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");

                    var dtt = new List<BDT1>();
                    Session["bdt1"] = dtt;

                    //gvw.CancelEdit();
                    gvw1.CancelEdit();
                    gvw2.CancelEdit();
                    gvw3.CancelEdit();


                    if (ASPxPageControl1.ActiveTabIndex == 0)
                    {

                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 2)
                    {
                        Cache.Remove(sds1.CacheKeyDependency);
                        Cache[sds1.CacheKeyDependency] = new object();
                        sds1.Select(new System.Web.UI.DataSourceSelectArguments());
                        sds1.DataBind();
                        gvw1.DataBind();
                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 1)
                    {
                        Cache.Remove(sds2.CacheKeyDependency);
                        Cache[sds2.CacheKeyDependency] = new object();
                        sds2.Select(new System.Web.UI.DataSourceSelectArguments());
                        sds2.DataBind();
                        gvw2.DataBind();
                    }
                    else
                    {
                        Cache.Remove(sds3.CacheKeyDependency);
                        Cache[sds3.CacheKeyDependency] = new object();
                        sds3.Select(new System.Web.UI.DataSourceSelectArguments());
                        sds3.DataBind();
                        gvw3.DataBind();
                    }

                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                ShowCountCar();
                #endregion
                break;

            case "InsertExceltoDatabase":
                #region InsertExceltoDatabase
                #region ImportExcel มาเก็บไว้ใน DataTable

                string _fileimport = Server.MapPath(paras[1]);
                // Connection String < v.2007
                string _version = "<2007";
                string connectionString = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source='" + _fileimport + "';Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'";
                try
                {
                    int dataindex = 1;
                    if (Path.GetExtension(_fileimport).ToLower().Equals(".xlsx"))
                    {
                        // Connection String = v.2007
                        _version = "=2007";
                        connectionString = "Provider='Microsoft.ACE.OLEDB.12.0';Data Source='" + _fileimport + "';Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text'";
                        //dataindex = 2;
                    }
                    OleDbConnection excelcon = new OleDbConnection(connectionString);
                    excelcon.Open();
                    // datatable รวม
                    //DataTable alldt = new DataTable();
                    // หาชื่อ sheet

                    DataRowCollection _ar_dr = excelcon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows;

                    //var qq = _ar_dr[1].Table.Rows[0].ToString();
                    //_ar_dr[1] ตำแหน่ง sheet ของข้อมูลทีต้องการ
                    string _exxcelsheet = _ar_dr[0]["TABLE_NAME"].ToString();
                    string sqlstr = "SELECT [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL] FROM [" + _exxcelsheet + "] GROUP BY [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL] ";
                    string sqlstr1 = "SELECT * FROM [" + _exxcelsheet + "] ";
                    DataTable dt = new DataTable();
                    DataTable dt1 = new DataTable();
                    new OleDbDataAdapter(sqlstr, excelcon).Fill(dt);
                    new OleDbDataAdapter(sqlstr1, excelcon).Fill(dt1);

                    excelcon.Close();
                #endregion
                    //DataRow[] sdtchk = dt1.Select("'' + [DELIV DATE] = '' AND ([VEHICLE-HEAD] is not null or [VEHICLE-HEAD] <> '')");
                    DataRow[] sdtchk = dt1.Select("'' + isnull('' + [DELIV DATE],'') = '' AND (([VEHICLE-HEAD] is not null or [VEHICLE-HEAD] <> '') OR ([VEHICLE-TAIL] is not null or [VEHICLE-TAIL] <> '')) ");
                    if (sdtchk.Count() > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณากรอกวันที่จัดแผน!');");
                        return;
                    }

                    DataRow[] sdtchkt = dt1.Select("'' + isnull('' + [VEHICLE-HEAD],'') = '' AND ([VEHICLE-TAIL] is not null or [VEHICLE-TAIL] <> '')");
                    if (sdtchkt.Count() > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาระบุทะเบียนหัว!');");
                        return;
                    }

                    int OldRecord = 0;
                    int newRecord = 0;

                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        if (con.State == ConnectionState.Closed) con.Open();

                        string NPLANPLUS = "";

                        if (dt.Rows.Count > 0)
                        {

                            DateTime datee;
                            for (int i = 1; i <= dt.Rows.Count - 1; i++)
                            {

                                if (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", out datee))
                                {

                                    string[] dDelivery = ("" + dt.Rows[i][0]).Split('/');
                                    string formateDelivery = "";
                                    formateDelivery = (dDelivery[2].StartsWith("20")) ? "en-US" : "th-TH";

                                    DateTime datetime;
                                    DateTime DDELIVERY = (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", new CultureInfo(formateDelivery), DateTimeStyles.None, out datetime)) ? datetime : DateTime.Now;

                                    string tmpTerminal = dt.Rows[i]["SUPPLY PLNT"] + "";

                                    string sqlCheckTruck = @"SELECT Tc.SHEADREGISTERNO
FROM  (TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE  TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE '%" + dt.Rows[i]["VEHICLE-HEAD"] + "%' AND TF.DDATE = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') -1  AND nvl(ct.STERMINALID,'" + tmpTerminal + "') LIKE '" + tmpTerminal + "'  GROUP BY  Tc.SHEADREGISTERNO";


                                    int numTruckConfirm = CommonFunction.Count_Value(con, sqlCheckTruck);
                                    /*
                                    if (numTruckConfirm < 1)
                                    {
                                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถนำเข้าข้อมูลได้เนื่องจาก รถทะเบียน : " + dt.Rows[i]["VEHICLE-HEAD"] + " วันที่จัดส่ง : " + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + " คลังต้นทาง : " + tmpTerminal + " ยังไม่ได้ถูกยืนยันรถตามสัญญา');");
                                        return;
                                    }
                                    */
                                    string chkNPLAN = CommonFunction.Get_Value(con, "SELECT nPlanID FROM TPLANSCHEDULE WHERE CACTIVE = '1' AND To_Date(DDELIVERY,'dd/MM/yyyy') = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND STIMEWINDOW = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND STERMINALID = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND SHEADREGISTERNO = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND nvl(STRAILERREGISTERNO,'1') = nvl('" + dt.Rows[i]["VEHICLE-TAIL"] + "','1')");
                                    //ข็อมูล
                                    int num1 = 0;
                                    int num2 = 0;

                                    DataRow[] sdt = dt1.Select("isnull([DELIV DATE],'') = '" + dt.Rows[i]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND isnull([VEHICLE-TAIL],'') = '" + dt.Rows[i]["VEHICLE-TAIL"] + "' ");

                                    if (sdt.Count() > 0)
                                    {

                                        int num = 0;

                                        string ValueTruck;
                                        if (!string.IsNullOrEmpty(dt.Rows[i]["VEHICLE-TAIL"] + ""))
                                        {
                                            ValueTruck = dt.Rows[i]["VEHICLE-TAIL"] + "";
                                        }
                                        else
                                        {
                                            ValueTruck = dt.Rows[i]["VEHICLE-HEAD"] + "";
                                        }

                                        string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                                        int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num + 100 : 0;

                                        string OutboundPlus = "";
                                        for (int o = 0; o < sdt.Count(); o++)
                                        {

                                            if (!string.IsNullOrEmpty(sdt[o]["DELIVERY"] + ""))
                                            {
                                                DataRow[] drr = dt1.Select("DELIVERY = '" + sdt[o]["DELIVERY"] + "'");

                                                if (drr.Count() <= 1)
                                                {

                                                    DataRow[] chkdrr = dt1.Select("isnull([DELIV DATE],'') = '" + sdt[o]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + sdt[o]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + sdt[o]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + sdt[o]["VEHICLE-HEAD"] + "' AND isnull([DROP NO],'') = '" + sdt[o]["DROP NO"] + "'");

                                                    if (chkdrr.Count() > 1)
                                                    {
                                                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมี DELIV DATE : " + sdt[o]["DELIV DATE"] + " TIMEWINDOW : " + sdt[o]["TIMEWINDOW"] + " SUPPLY PLNT : " + sdt[o]["SUPPLY PLNT"] + " VEHICLE-HEAD : " + sdt[o]["VEHICLE-HEAD"] + " DROP NO : " + sdt[o]["DROP NO"] + " อยู่ในรายการนำเข้าข้อมูล ซ้ำกัน');");
                                                        return;
                                                    }

                                                    OutboundPlus += ",'" + sdt[o]["DELIVERY"] + "'";
                                                    if (chkNPLAN == "") //ถ้าไม่มีรายการเดิม
                                                    {

                                                        OracleCommand comcount = new OracleCommand("SELECT COUNT(*) FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND SDELIVERYNO = '" + sdt[o]["DELIVERY"] + "'", con);
                                                        int countDeliveryno = int.Parse("" + comcount.ExecuteScalar());
                                                        if (countDeliveryno > 0)
                                                        {
                                                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมี Outbound " + sdt[o]["DELIVERY"] + " อยู่ในระบบแล้ว !!!  รายการข้อมูล DELIV DATE : " + dt.Rows[i]["DELIV DATE"] + " TIMEWINDOW : " + dt.Rows[i]["TIMEWINDOW"] + " SUPPLY PLNT : " + dt.Rows[i]["SUPPLY PLNT"] + " VEHICLE-HEAD : " + dt.Rows[i]["VEHICLE-HEAD"] + " VEHICLE-TAIL : " + dt.Rows[i]["VEHICLE-TAIL"] + " DROP NO : " + sdt[o]["DROP NO"] + " DELIVERY : " + sdt[o]["DELIVERY"] + " SHIP-TO : " + sdt[o]["SHIP-TO"] + "');");
                                                            return;
                                                        }


                                                        int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULEList pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + sdt[o]["VEHICLE-HEAD"] + "' AND P.STIMEWINDOW = '" + sdt[o]["TIMEWINDOW"] + "' AND To_Date(DDELIVERY,'dd/MM/yyyy') = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND p.STERMINALID ='" + sdt[o]["TIMEWINDOW"] + "' AND pl.NDROP = '" + sdt[o]["DROP NO"] + "'");

                                                        if (CheckCar > 0)
                                                        {
                                                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + sdt[o]["VEHICLE-HEAD"] + " เที่ยวที่ :" + sdt[o]["TIMEWINDOW"] + " วันที่จัดส่ง : " + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + " คลังต้นทาง : " + sdt[o]["TIMEWINDOW"] + " Drop No. : " + sdt[o]["DROP NO"] + " อยู่ในระบบแล้ว');");
                                                            return;
                                                        }

                                                    }
                                                    else
                                                    {

                                                        OracleCommand comcount = new OracleCommand("SELECT COUNT(*) FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND SDELIVERYNO = '" + sdt[o]["DELIVERY"] + "' AND NPLANID != '" + chkNPLAN + "'", con);
                                                        int countDeliveryno = int.Parse("" + comcount.ExecuteScalar());
                                                        if (countDeliveryno > 0)
                                                        {
                                                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมี Outbound " + sdt[o]["DELIVERY"] + " อยู่ในระบบแล้ว !!!  รายการข้อมูล DELIV DATE : " + dt.Rows[i]["DELIV DATE"] + " TIMEWINDOW : " + dt.Rows[i]["TIMEWINDOW"] + " SUPPLY PLNT : " + dt.Rows[i]["SUPPLY PLNT"] + " VEHICLE-HEAD : " + dt.Rows[i]["VEHICLE-HEAD"] + " VEHICLE-TAIL : " + dt.Rows[i]["VEHICLE-TAIL"] + " DROP NO : " + sdt[o]["DROP NO"] + " DELIVERY : " + sdt[o]["DELIVERY"] + " SHIP-TO : " + sdt[o]["SHIP-TO"] + "');");
                                                            return;
                                                        }

                                                        //int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULEList pl ON P.NPLANID = PL.NPLANID WHERE P.SHEADREGISTERNO = '" + sdt[o]["VEHICLE-HEAD"] + "' AND P.STIMEWINDOW = '" + sdt[o]["TIMEWINDOW"] + "' AND To_Date(DDELIVERY,'dd/MM/yyyy') = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND p.STERMINALID ='" + sdt[o]["TIMEWINDOW"] + "' AND pl.NDROP = '" + sdt[o]["DROP NO"] + "' AND p.NPLANID != '" + chkNPLAN + "'");

                                                        //if (CheckCar > 0)
                                                        //{
                                                        //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + sdt[o]["VEHICLE-HEAD"] + " เที่ยวที่ :" + sdt[o]["TIMEWINDOW"] + " วันที่จัดส่ง : " + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + " คลังต้นทาง : " + sdt[o]["TIMEWINDOW"] + " Drop No. : " + sdt[o]["DROP NO"] + " อยู่ในระบบแล้ว');");
                                                        //    return;
                                                        //}
                                                    }

                                                }
                                                else if (drr.Count() > 1)
                                                {
                                                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมี Outbound " + sdt[o]["DELIVERY"] + " อยู่ในรายการซ้ำกัน !!!  รายการข้อมูล DELIV DATE : " + dt.Rows[i]["DELIV DATE"] + " TIMEWINDOW : " + dt.Rows[i]["TIMEWINDOW"] + " SUPPLY PLNT : " + dt.Rows[i]["SUPPLY PLNT"] + " VEHICLE-HEAD : " + dt.Rows[i]["VEHICLE-HEAD"] + " VEHICLE-TAIL : " + dt.Rows[i]["VEHICLE-TAIL"] + " DROP NO : " + sdt[o]["DROP NO"] + " DELIVERY : " + sdt[o]["DELIVERY"] + " SHIP-TO : " + sdt[o]["SHIP-TO"] + "');");
                                                    return;
                                                }
                                            }
                                        }

                                        DataTable dtGRP_DETWTRCK = CommonFunction.GroupDataTable(dt1, "DELIV DATE,TIMEWINDOW,VEHICLE-HEAD,VEHICLE-TAIL");
                                        foreach (DataRow drGRP_DETWTRCK in dtGRP_DETWTRCK.Rows)
                                        {
                                            string sDO_NO = "";
                                            if (!string.IsNullOrEmpty("" + drGRP_DETWTRCK["DELIV DATE"]) && !string.IsNullOrEmpty("" + drGRP_DETWTRCK["TIMEWINDOW"]) && !string.IsNullOrEmpty("" + drGRP_DETWTRCK["VEHICLE-HEAD"]))
                                            {
                                                foreach (DataRow drDO in dt1.Select("[DELIV DATE]='" + drGRP_DETWTRCK["DELIV DATE"] + "' AND [TIMEWINDOW]='" + drGRP_DETWTRCK["TIMEWINDOW"] + "' AND [VEHICLE-HEAD]='" + drGRP_DETWTRCK["VEHICLE-HEAD"] + "' "))
                                                {
                                                    sDO_NO += "," + drDO["DELIVERY"];
                                                }
                                            }
                                            sDO_NO = (sDO_NO.Length > 0) ? sDO_NO.Remove(0, 1) : "";
                                            string DO_TOTLE_VOLUMN = CommonFunction.Get_Value(con, "SELECT  SUM(nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0)) AS NVALUE FROM TDELIVERY WHERE DELIVERY_NO in ( '" + sDO_NO.Replace(",", "','") + "' )");

                                            string sTruck;

                                            if (!string.IsNullOrEmpty("" + drGRP_DETWTRCK["VEHICLE-TAIL"].ToString().Trim()))
                                            {
                                                sTruck = drGRP_DETWTRCK["VEHICLE-TAIL"] + "";
                                            }
                                            else
                                            {
                                                sTruck = drGRP_DETWTRCK["VEHICLE-HEAD"] + "";
                                            }

                                            string STOTALCAP = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + sTruck.Trim() + "' GROUP BY  T.NTOTALCAPACITY");
                                            double TRCK_TOTALCAP = double.TryParse(STOTALCAP, out def_Double) ? double.Parse(STOTALCAP) + 100 : 0.00;
                                            double DO_VOLUMN = double.TryParse(DO_TOTLE_VOLUMN, out def_Double) ? def_Double : 0.00;
                                            if (TRCK_TOTALCAP < DO_VOLUMN)
                                            {
                                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','<b>ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากเกินปริมาณความจุของรถ ความจุของรถ : </b>" + ((TRCK_TOTALCAP > 0) ? (TRCK_TOTALCAP - 100) : 0) + "<b> ปริมาณที่จัดแผน : </b>" + DO_VOLUMN + " !!!<br /><b>รายการข้อมูลดังนี้<br /> DELIV DATE : </b>" + drGRP_DETWTRCK["DELIV DATE"] + " <b>TIMEWINDOW : </b>" + drGRP_DETWTRCK["TIMEWINDOW"] + " <br /> <b>VEHICLE-HEAD :</b> " + drGRP_DETWTRCK["VEHICLE-HEAD"] + " <br /><b>DELIVERY : </b>" + sDO_NO.Replace("'", " ") + "<br /><b> รวมกันเกินปริมาณความจุของรถ</b>');");
                                                return;
                                            }
                                        }
                                        //string ssOutbound = OutboundPlus.Remove(0, 1);
                                        //if (ssOutbound.Length > 0)
                                        //{
                                        //    string NVALUE = CommonFunction.Get_Value(con, "SELECT  SUM(nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0)) AS NVALUE FROM TDELIVERY WHERE DELIVERY_NO in ( " + ssOutbound + ")");

                                        //    int NNVALUE = int.TryParse(NVALUE, out num) ? num : 0;

                                        //    if (NCAPACITY < NNVALUE)
                                        //    {
                                        //        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากเกินปริมาณความจุของรถ ความจุของรถ : " + (NCAPACITY - 100) + " ปริมาณที่จัดแผน : " + NNVALUE + " !!!<br />รายการข้อมูล DELIV DATE : " + dt.Rows[i]["DELIV DATE"] + " TIMEWINDOW : " + dt.Rows[i]["TIMEWINDOW"] + " SUPPLY PLNT : " + dt.Rows[i]["SUPPLY PLNT"] + " VEHICLE-HEAD : " + dt.Rows[i]["VEHICLE-HEAD"] + " VEHICLE-TAIL : " + dt.Rows[i]["VEHICLE-TAIL"] + " DELIVERY : " + ssOutbound.Replace("'", " ") + " รวมกันเกินปริมาณความจุของรถ');");
                                        //        return;
                                        //    }
                                        //}



                                    }

                                }
                            }

                        }



                        if (dt.Rows.Count > 0)
                        {//หาขอบเขตของข้อมูลที่ต้องการนำเข้า
                            string strsql = "INSERT INTO TPLANSCHEDULE(SVENDORID,nPlanID,nNo,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:SVENDORID,:nPlanID,1,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate)";
                            string strsql1 = "INSERT INTO TPLANSCHEDULEList(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,NVALUE) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:NVALUE)";
                            DateTime datee;
                            for (int i = 1; i <= dt.Rows.Count - 1; i++)
                            {
                                if (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", out datee))
                                {

                                    string[] dDelivery = ("" + dt.Rows[i][0]).Split('/');
                                    string formateDelivery = "";
                                    formateDelivery = (dDelivery[2].StartsWith("20")) ? "en-US" : "th-TH";

                                    DateTime datetime;
                                    DateTime DDELIVERY = (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", new CultureInfo(formateDelivery), DateTimeStyles.None, out datetime)) ? datetime : DateTime.Now;

                                    DateTime Now = DateTime.Now;
                                    DateTime DDELIVERYCONVERT = DDELIVERY.Date.Add(new TimeSpan(Now.Hour, Now.Minute, Now.Second));

                                    string chkNPLAN = CommonFunction.Get_Value(con, "SELECT nPlanID FROM TPLANSCHEDULE WHERE CACTIVE = '1' AND To_Date(DDELIVERY,'dd/MM/yyyy') = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND STIMEWINDOW = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND STERMINALID = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND SHEADREGISTERNO = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND nvl(STRAILERREGISTERNO,'1') = nvl('" + dt.Rows[i]["VEHICLE-TAIL"] + "','1')");
                                    //ข็อมูล
                                    int num1 = 0;
                                    int num2 = 0;

                                    if (chkNPLAN == "") //ถ้าไม่มีรายการเดิม
                                    {
                                        string genid = CommonFunction.Gen_ID(con, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                                        using (OracleCommand com = new OracleCommand(strsql, con))
                                        {

                                            string venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE REPLACE(REPLACE(T.SHEADREGISTERNO,'-',''),'.','') = REPLACE(REPLACE('" + dt.Rows[i]["VEHICLE-HEAD"] + "','-',''),'.','') ");

                                            com.Parameters.Clear();
                                            com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                                            com.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
                                            com.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = dt.Rows[i]["SUPPLY PLNT"] + "";
                                            com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now;
                                            com.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = DDELIVERYCONVERT;
                                            com.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = "";
                                            com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = DDELIVERYCONVERT;
                                            com.Parameters.Add(":sTimeWindow", OracleType.Number).Value = int.TryParse(dt.Rows[i]["TIMEWINDOW"] + "", out num1) ? num1 : 0;
                                            com.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = dt.Rows[i]["VEHICLE-HEAD"] + "";
                                            com.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = dt.Rows[i]["VEHICLE-TAIL"] + "";
                                            com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com.ExecuteNonQuery();
                                        }

                                        DataRow[] sdt = dt1.Select("isnull([DELIV DATE],'') = '" + dt.Rows[i]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND isnull([VEHICLE-TAIL],'') = '" + dt.Rows[i]["VEHICLE-TAIL"] + "' ");

                                        if (sdt.Count() > 0)
                                        {
                                            int num = 0;

                                            string ValueTruck;
                                            if (!string.IsNullOrEmpty(dt.Rows[i]["VEHICLE-TAIL"] + ""))
                                            {
                                                ValueTruck = dt.Rows[i]["VEHICLE-TAIL"] + "";
                                            }
                                            else
                                            {
                                                ValueTruck = dt.Rows[i]["VEHICLE-HEAD"] + "";
                                            }

                                            string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num + 100 : 0;

                                            for (int o = 0; o <= sdt.Count() - 1; o++)
                                            {
                                                newRecord += 1;
                                                string genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULEList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                                int NDROP = int.TryParse(sdt[o]["DROP NO"] + "", out num2) ? num2 : 0;

                                                DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE DELIVERY_NO = '" + sdt[o]["DELIVERY"] + "'");

                                                int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;

                                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                                {

                                                    com1.Parameters.Clear();
                                                    com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genid1;
                                                    com1.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
                                                    com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
                                                    com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = sdt[o]["DELIVERY"] + "";
                                                    com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = (dtData.Rows.Count > 0) ? dtData.Rows[0][0] + "" : "";
                                                    com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
                                                    com1.ExecuteNonQuery();
                                                }

                                            }
                                        }
                                    }
                                    else // มีรายการเดิม
                                    {
                                        if (("" + rblImport.Value).Contains("1")) //ทับรายการเก่า
                                        {

                                            using (OracleCommand delcom = new OracleCommand("DELETE FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND NPLANID = '" + chkNPLAN + "'", con))
                                            {
                                                delcom.ExecuteNonQuery();
                                            }

                                            DataRow[] sdt = dt1.Select("isnull([DELIV DATE],'') = '" + dt.Rows[i]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND isnull([VEHICLE-TAIL],'') = '" + dt.Rows[i]["VEHICLE-TAIL"] + "' ");


                                            if (sdt.Count() > 0)
                                            {
                                                int num = 0;

                                                string ValueTruck;
                                                if (!string.IsNullOrEmpty(dt.Rows[i]["VEHICLE-TAIL"] + ""))
                                                {
                                                    ValueTruck = dt.Rows[i]["VEHICLE-TAIL"] + "";
                                                }
                                                else
                                                {
                                                    ValueTruck = dt.Rows[i]["VEHICLE-HEAD"] + "";
                                                }

                                                string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                                                int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num + 100 : 0;

                                                for (int o = 0; o <= sdt.Count() - 1; o++)
                                                {
                                                    OldRecord += 1;
                                                    int NDROP = int.TryParse(sdt[o]["DROP NO"] + "", out num2) ? num2 : 0;

                                                    DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE DELIVERY_NO = '" + sdt[o]["DELIVERY"] + "'");

                                                    int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;


                                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                                    {
                                                        string genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULEList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");

                                                        com1.Parameters.Clear();
                                                        com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genid1);
                                                        com1.Parameters.Add(":nPlanID", OracleType.Number).Value = chkNPLAN;
                                                        com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
                                                        com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = sdt[o]["DELIVERY"].ToString();
                                                        com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = dtData.Rows[0][0] + "";
                                                        com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
                                                        com1.ExecuteNonQuery();
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','บันทึกข้อมูลเรียบร้อยแล้ว มีรายการใหม่ " + newRecord + " รายการ ปรับปรุงรายการเก่า " + OldRecord + " รายการ ');");
                    SaveImportDetail(paras[1], paras[1]);
                }
                catch (Exception ex)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถอ่านไฟล์ข้อมูลที่นำเข้าได้ กรุณาตรวจสอบไฟล์ข้อมูลที่นำเข้าใหม่อีกครั้ง<br > เนื่องจาก Error Massege: " + ex.Message + "<br >StackTrace :" + ex.StackTrace + "');");
                }


                Cache.Remove(sds1.CacheKeyDependency);
                Cache[sds1.CacheKeyDependency] = new object();
                sds1.Select(new System.Web.UI.DataSourceSelectArguments());
                sds1.DataBind();
                gvw1.DataBind();

                #endregion
                break;

            case "EditClick":
                #region EditClick
                var ggg = new List<BDT1>();
                Session["bdt1"] = ggg;
                int index = int.Parse(paras[1]);

                if (ASPxPageControl1.ActiveTabIndex == 0)
                {


                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    gvw2.StartEdit(index);

                    object sterminal = gvw2.GetRowValues(index, "STERMINAL");
                    object sHeadRegist = gvw2.GetRowValues(index, "SHEADREGISTERNO");
                    object sTrailerRegist = gvw2.GetRowValues(index, "STRAILERREGISTERNO");

                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxComboBox cboTrailerRegist1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboTrailerRegist");
                    ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cbxTimeWindow");

                    dtePlan1.Value = DateTime.Now;

                    cmbTerminal1.Value = sterminal + "";
                    cboHeadRegist1.Value = sHeadRegist + "";
                    cboTrailerRegist1.Value = sTrailerRegist + "";
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    gvw1.StartEdit(index);

                    object sterminal = gvw1.GetRowValues(index, "STERMINALID");
                    object sHeadRegist = gvw1.GetRowValues(index, "SHEADREGISTERNO");
                    object sTrailerRegist = gvw1.GetRowValues(index, "STRAILERREGISTERNO");
                    object stimeWindow = gvw1.GetRowValues(index, "STIMEWINDOW");
                    object sDELIVERYNO = gvw1.GetRowValues(index, "SDELIVERYNO");

                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxComboBox cboTrailerRegist1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboTrailerRegist");
                    ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cbxTimeWindow");
                    ASPxComboBox cboDelivery1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboDelivery");

                    object SSHIPTO = gvw1.GetRowValues(index, "SSHIPTO");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShiptoName");
                    string sSSHIPTO = CommonFunction.Get_Value(sql, "SELECT C.CUST_NAME FROM TCUSTOMER_SAP c WHERE C.SHIP_TO = '" + SSHIPTO + "'");
                    txtShiptoName1.Text = sSSHIPTO;

                    cmbTerminal1.Value = sterminal + "";
                    cboHeadRegist1.Value = sHeadRegist + "";
                    cboTrailerRegist1.Value = sTrailerRegist + "";
                    cbxTimeWindow1.Value = stimeWindow + "";

                    cboDelivery_OnItemRequestedByValueSQL(cboDelivery1, new ListEditItemRequestedByValueEventArgs(sDELIVERYNO + ""));
                    cboDelivery1.Value = sDELIVERYNO + "";

                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    gvw3.StartEdit(index);

                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxComboBox cboTrailerRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboTrailerRegist");
                    ASPxComboBox cboDelivery1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboDelivery");

                    object sHeadRegist = gvw3.GetRowValues(index, "SHEADREGISTERNO");
                    object sTrailerRegist = gvw3.GetRowValues(index, "STRAILERREGISTERNO");
                    object cType = gvw3.GetRowValues(index, "CFIFO");
                    object sDELIVERYNO = gvw3.GetRowValues(index, "SDELIVERYNO");

                    object SSHIPTO = gvw3.GetRowValues(index, "SSHIPTO");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShiptoName");
                    string sSSHIPTO = CommonFunction.Get_Value(sql, "SELECT C.CUST_NAME FROM TCUSTOMER_SAP c WHERE C.SHIP_TO = '" + SSHIPTO + "'");
                    txtShiptoName1.Text = sSSHIPTO;

                    txtCTYPE.Text = cType + "";
                    cboHeadRegist1.Value = sHeadRegist + "";
                    cboTrailerRegist1.Value = sTrailerRegist + "";


                    cboDelivery_OnItemRequestedByValueSQL(cboDelivery1, new ListEditItemRequestedByValueEventArgs(sDELIVERYNO + ""));
                    cboDelivery1.Value = sDELIVERYNO + "";


                    ASPxComboBox cboPersonalNo1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cmbPersonalNo");
                    object SEMPLOYEE = gvw3.GetRowValues(index, "SEMPLOYEEID");

                    cmbPersonalNo_OnItemRequestedByValueSQL(cboPersonalNo1, new ListEditItemRequestedByValueEventArgs(SEMPLOYEE + ""));
                    cboPersonalNo1.Value = SEMPLOYEE + "";

                }

                SetVisibleControl();
                #endregion
                break;


            case "AddDataToList":
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {

                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cbxTimeWindow");
                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxDateEdit dteDelivery1 = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dteDelivery");

                    ASPxTextBox txtDrop1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtDrop");
                    ASPxComboBox cboDelivery1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboDelivery");
                    ASPxTextBox txtShipto1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtShipto");
                    ASPxTextBox txtValue1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtValue");
                    ASPxGridView sgvw1 = (ASPxGridView)gvw2.FindEditFormTemplateControl("sgvw");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtShiptoName");


                    var bdt2 = (List<BDT1>)Session["bdt1"];


                    int num = 0;
                    int InDrop = Int32.TryParse(txtDrop1.Text, out num) ? num : 0;

                    if (InDrop > 10)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่ม Drop No. เกินกว่า 10 Drop ได้');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int InParse = 0;
                    string outbounds = cboDelivery1.Value + "";
                    var ddd = bdt2.FirstOrDefault(s => s.dtDeliveryNo == outbounds);
                    if (ddd.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในรายการแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }


                    string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND SDELIVERYNO = '" + outbounds + "'");
                    int CountOutbound = Int32.TryParse(CountValue, out num) ? num : 0;

                    if (CountOutbound > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int nTimeWindow = Int32.TryParse("" + cbxTimeWindow1.Value, out InParse) ? InParse : 0;

                    var CheckCarList = bdt2.FirstOrDefault(s => s.sTimeWindow == nTimeWindow && s.dtDrop == InDrop);

                    if (CheckCarList.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Drop No. " + InDrop + " เที่ยวที่ " + nTimeWindow + " นี้ ในรายการแล้ว ');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULEList pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + cboHeadRegist1.Value + "' AND P.STIMEWINDOW = '" + nTimeWindow + "' AND To_Date(P.DDELIVERY,'dd/MM/yyyy') = To_Date('" + dteDelivery1.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND p.STERMINALID ='" + cmbTerminal1.Value + "' AND pl.NDROP = " + InDrop);

                    if (CheckCar > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + cboHeadRegist1.Value + " เที่ยวที่ :" + nTimeWindow + " วันที่จัดส่ง : " + dteDelivery1.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + " คลังต้นทาง : " + cmbTerminal1.Value + " Drop No. : " + InDrop + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }


                    bdt2.Add(new BDT1
                    {
                        dtDrop = InDrop,
                        dtDeliveryNo = cboDelivery1.Value + "",
                        dtShipto = txtShipto1.Text,
                        dtValue = Int32.TryParse(txtValue1.Text, out InParse) ? InParse : 0,
                        sTimeWindow = nTimeWindow,
                        dtShiptoName = txtShiptoName1.Text
                    });

                    sgvw1.DataSource = bdt2;
                    sgvw1.DataBind();

                    Session["bdt1"] = bdt2;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cbxTimeWindow");
                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxDateEdit dteDelivery1 = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dteDelivery");

                    ASPxTextBox txtDrop1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtDrop");
                    ASPxComboBox cboDelivery1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboDelivery");
                    ASPxTextBox txtShipto1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShipto");
                    ASPxTextBox txtValue1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtValue");
                    ASPxGridView sgvw1 = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShiptoName");

                    var bdt2 = (List<BDT1>)Session["bdt1"];


                    int num = 0;
                    int InDrop = Int32.TryParse(txtDrop1.Text, out num) ? num : 0;

                    if (InDrop > 10)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่ม Drop No. เกินกว่า 10 Drop ได้');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int InParse = 0;
                    string outbounds = cboDelivery1.Value + "";
                    var ddd = bdt2.FirstOrDefault(s => s.dtDeliveryNo == outbounds);
                    if (ddd.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในรายการแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }


                    string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND SDELIVERYNO = '" + outbounds + "'");
                    int CountOutbound = Int32.TryParse(CountValue, out num) ? num : 0;

                    if (CountOutbound > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int nTimeWindow = Int32.TryParse("" + cbxTimeWindow1.Value, out InParse) ? InParse : 0;

                    var CheckCarList = bdt2.FirstOrDefault(s => s.sTimeWindow == nTimeWindow && s.dtDrop == InDrop);

                    if (CheckCarList.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Drop No. " + InDrop + " เที่ยวที่ " + nTimeWindow + " นี้ ในรายการแล้ว ');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULEList pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + cboHeadRegist1.Value + "' AND P.STIMEWINDOW = '" + nTimeWindow + "' AND To_Date(P.DDELIVERY,'dd/MM/yyyy') = To_Date('" + dteDelivery1.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND p.STERMINALID ='" + cmbTerminal1.Value + "' AND pl.NDROP = " + InDrop);

                    if (CheckCar > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + cboHeadRegist1.Value + " เที่ยวที่ :" + nTimeWindow + " วันที่จัดส่ง : " + dteDelivery1.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + " คลังต้นทาง : " + cmbTerminal1.Value + " Drop No. : " + InDrop + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    bdt2.Add(new BDT1
                    {
                        dtDrop = InDrop,
                        dtDeliveryNo = cboDelivery1.Value + "",
                        dtShipto = txtShipto1.Text,
                        dtValue = Int32.TryParse(txtValue1.Text, out InParse) ? InParse : 0,
                        sTimeWindow = nTimeWindow,
                        dtShiptoName = txtShiptoName1.Text
                    });

                    sgvw1.DataSource = bdt2;
                    sgvw1.DataBind();

                    Session["bdt1"] = bdt2;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    int indexx = gvw3.EditingRowVisibleIndex;
                    string sTimeWindow1 = gvw3.GetRowValues(indexx, "STIMEWINDOW") + "";
                    string sDDELIVERY = gvw3.GetRowValues(indexx, "DDELIVERY") + "";
                    string sSTIMEWINDOW = gvw3.GetRowValues(indexx, "STIMEWINDOW") + "";
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");

                    ASPxTextBox txtDrop1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDrop");
                    ASPxComboBox cboDelivery1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboDelivery");
                    ASPxTextBox txtShipto1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShipto");
                    ASPxTextBox txtValue1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtValue");
                    ASPxGridView sgvw1 = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShiptoName");


                    var bdt2 = (List<BDT1>)Session["bdt1"];


                    int num = 0;
                    int InDrop = Int32.TryParse(txtDrop1.Text, out num) ? num : 0;
                    if (InDrop > 10)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่ม Drop No. เกินกว่า 10 Drop ได้');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int InParse = 0;
                    string outbounds = cboDelivery1.Value + "";
                    var ddd = bdt2.FirstOrDefault(s => s.dtDeliveryNo == outbounds);
                    if (ddd.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในรายการแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }


                    string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND SDELIVERYNO = '" + outbounds + "'");
                    int CountOutbound = Int32.TryParse(CountValue, out num) ? num : 0;

                    if (CountOutbound > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int nTimeWindow = Int32.TryParse(sTimeWindow1, out InParse) ? InParse : 0;

                    var CheckCarList = bdt2.FirstOrDefault(s => s.sTimeWindow == nTimeWindow && s.dtDrop == InDrop);


                    if (CheckCarList.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Drop No. " + InDrop + " เที่ยวที่ " + nTimeWindow + " นี้ ในรายการแล้ว ');");
                        SetVisibleControlAdd();
                        return;
                    }

                    DateTime Date1;
                    DateTime csDDELIVERY = DateTime.TryParse(sDDELIVERY, out Date1) ? Date1 : DateTime.Now;
                    int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULEList pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + cboHeadRegist1.Value + "' AND P.STIMEWINDOW = '" + nTimeWindow + "' AND To_Date(P.DDELIVERY,'dd/MM/yyyy') = To_Date('" + csDDELIVERY.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND p.STERMINALID ='" + sSTIMEWINDOW + "' AND pl.NDROP = " + InDrop);

                    if (CheckCar > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + cboHeadRegist1.Value + " เที่ยวที่ :" + nTimeWindow + " วันที่จัดส่ง : " + csDDELIVERY.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + " คลังต้นทาง : " + sSTIMEWINDOW + " Drop No. : " + InDrop + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    bdt2.Add(new BDT1
                    {
                        dtDrop = InDrop,
                        dtDeliveryNo = cboDelivery1.Value + "",
                        dtShipto = txtShipto1.Text,
                        dtValue = Int32.TryParse(txtValue1.Text, out InParse) ? InParse : 0,
                        sTimeWindow = nTimeWindow,
                        dtShiptoName = txtShiptoName1.Text
                    });

                    sgvw1.DataSource = bdt2;
                    sgvw1.DataBind();

                    Session["bdt1"] = bdt2;
                }

                SetVisibleControlAdd();

                break;

            case "DelClick":
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {

                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    ASPxGridView sgvw2 = (ASPxGridView)gvw2.FindEditFormTemplateControl("sgvw");
                    string IndexValue = paras[1];
                    var bdt3 = (List<BDT1>)Session["bdt1"];

                    bdt3.RemoveAt(Convert.ToInt32(IndexValue));

                    sgvw2.DataSource = bdt3;
                    sgvw2.DataBind();

                    Session["bdt1"] = bdt3;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    ASPxGridView sgvw2 = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
                    string IndexValue = paras[1];
                    var bdt3 = (List<BDT1>)Session["bdt1"];

                    bdt3.RemoveAt(Convert.ToInt32(IndexValue));

                    sgvw2.DataSource = bdt3;
                    sgvw2.DataBind();

                    Session["bdt1"] = bdt3;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    ASPxGridView sgvw2 = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
                    string IndexValue = paras[1];
                    var bdt3 = (List<BDT1>)Session["bdt1"];

                    bdt3.RemoveAt(Convert.ToInt32(IndexValue));

                    sgvw2.DataSource = bdt3;
                    sgvw2.DataBind();

                    Session["bdt1"] = bdt3;
                }

                break;

            case "delete":
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {

                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    var ld = gvw2.GetSelectedFieldValues("ID1", "NPLANID", "NNO", "SPLANDATE", "SPLANTIME", "STERMINALNAME", "STERMINALID", "DDELIVERY", "STIMEWINDOW", "NDROP", "SDELIVERYNO", "SSHIPTO", "SHEADREGISTERNO", "STRAILERREGISTERNO", "NVALUE", "SPLANLISTID")
                  .Cast<object[]>()
                  .Select(s => new { ID1 = s[0].ToString(), NPLANID = s[1].ToString(), NNO = s[2].ToString(), SPLANDATE = s[3].ToString(), SPLANTIME = s[4].ToString(), STERMINALNAME = s[5].ToString(), STERMINALID = s[6].ToString(), DDELIVERY = s[7].ToString(), STIMEWINDOW = s[8].ToString(), NDROP = s[9].ToString(), SDELIVERYNO = s[10].ToString(), SSHIPTO = s[11].ToString(), SHEADREGISTERNO = s[12].ToString(), STRAILERREGISTERNO = s[13].ToString(), NVALUE = s[14].ToString(), SPLANLISTID = s[15].ToString() });

                    string delid = "";
                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();
                        foreach (var l in ld)
                        {
                            // Session["delPlanList"] = l.SPLANLISTID;

                            string strsql1 = "UPDATE FIFO SET CACTIVE = '0' WHERE NID =" + l.NPLANID;
                            using (OracleCommand com = new OracleCommand(strsql1, con))
                            {
                                com.ExecuteNonQuery();
                            }


                            // sds2.Delete();
                            delid += l.SPLANLISTID + ",";

                            if (CommonFunction.Count_Value(con, "SELECT sPlanListID FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND nPlanID = " + Convert.ToInt32(l.NPLANID)) == 0)
                            {
                                string strsql = "UPDATE TPLANSCHEDULE SET CACTIVE = '0' WHERE nPlanID =" + l.NPLANID;
                                //  string strsql = "DELETE FROM TPLANSCHEDULE WHERE nPlanID =" + l.NPLANID;
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.ExecuteNonQuery();
                                }
                            }

                            //Session["delPlanList"] = l.SPLANLISTID;
                            //sds2.Delete();
                            //delid += l.SPLANLISTID + ",";

                            //if (CommonFunction.Count_Value(con, "SELECT sPlanListID FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND nPlanID = " + Convert.ToInt32(l.NPLANID)) == 0)
                            //{
                            //    string strsql = "DELETE FROM TPLANSCHEDULE WHERE nPlanID =" + l.NPLANID;
                            //    using (OracleCommand com = new OracleCommand(strsql, con))
                            //    {
                            //        com.ExecuteNonQuery();
                            //    }
                            //}
                        }
                    }

                    LogUser("11", "D", "ลบข้อมูลหน้า จัดแผนการขนส่ง รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                    gvw2.Selection.UnselectAll();
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    var ld = gvw1.GetSelectedFieldValues("ID1", "NPLANID", "NNO", "SPLANDATE", "SPLANTIME", "STERMINALNAME", "STERMINALID", "DDELIVERY", "STIMEWINDOW", "NDROP", "SDELIVERYNO", "SSHIPTO", "SHEADREGISTERNO", "STRAILERREGISTERNO", "NVALUE", "SPLANLISTID")
                   .Cast<object[]>()
                   .Select(s => new { ID1 = s[0].ToString(), NPLANID = s[1].ToString(), NNO = s[2].ToString(), SPLANDATE = s[3].ToString(), SPLANTIME = s[4].ToString(), STERMINALNAME = s[5].ToString(), STERMINALID = s[6].ToString(), DDELIVERY = s[7].ToString(), STIMEWINDOW = s[8].ToString(), NDROP = s[9].ToString(), SDELIVERYNO = s[10].ToString(), SSHIPTO = s[11].ToString(), SHEADREGISTERNO = s[12].ToString(), STRAILERREGISTERNO = s[13].ToString(), NVALUE = s[14].ToString(), SPLANLISTID = s[15].ToString() });

                    string delid = "";

                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();
                        foreach (var l in ld)
                        {
                            // Session["delPlanList"] = l.SPLANLISTID;
                            //  sds1.Delete();

                            string strsql11 = "UPDATE TPLANSCHEDULEList SET CACTIVE = '0' WHERE SPLANLISTID = '" + l.SPLANLISTID + "'";
                            using (OracleCommand com = new OracleCommand(strsql11, con))
                            {
                                com.ExecuteNonQuery();
                            }


                            delid += l.SPLANLISTID + ",";

                            if (CommonFunction.Count_Value(con, "SELECT sPlanListID FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND nPlanID = " + Convert.ToInt32(l.NPLANID)) == 0)
                            {
                                string strsql = "UPDATE TPLANSCHEDULE SET CACTIVE = '0' WHERE nPlanID =" + l.NPLANID;
                                // string strsql = "DELETE FROM TPLANSCHEDULE WHERE nPlanID =" + l.NPLANID;
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.ExecuteNonQuery();
                                }
                            }

                            //Session["delPlanList"] = l.SPLANLISTID;
                            //sds1.Delete();

                            //delid += l.SPLANLISTID + ",";

                            //if (CommonFunction.Count_Value(con, "SELECT sPlanListID FROM TPLANSCHEDULEList WHERE nPlanID = " + Convert.ToInt32(l.NPLANID)) == 0)
                            //{
                            //    string strsql = "DELETE FROM TPLANSCHEDULE WHERE nPlanID =" + l.NPLANID;
                            //    using (OracleCommand com = new OracleCommand(strsql, con))
                            //    {
                            //        com.ExecuteNonQuery();
                            //    }
                            //}
                        }
                    }

                    LogUser("11", "D", "ลบข้อมูลหน้า จัดแผนการขนส่ง รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                    gvw1.Selection.UnselectAll();
                }


                break;

            case "Cancel":
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {
                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    gvw2.CancelEdit();
                    var dt = new List<BDT1>();
                    Session["bdt1"] = dt;

                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    gvw1.CancelEdit();
                    var dt = new List<BDT1>();
                    Session["bdt1"] = dt;

                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    gvw3.CancelEdit();
                    var dt = new List<BDT1>();
                    Session["bdt1"] = dt;
                }
                break;

        }

    }


    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "" && (_Filename[1].ToLower().ToString().Trim().Equals("xls") || _Filename[1].ToLower().ToString().Trim().Equals("xlsx")))
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;
            string genName = _Filename[0].Replace(" ", "") + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                //SaveImportDetail(_Filename[0], genName);
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[1];
                e.CallbackData = data;
            }
        }
        else
        {

            return;

        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    void SaveImportDetail(string filename, string genname)
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            string strsql = "INSERT INTO TIMPORTFILE(NIMPORTID,SFILENAME,SGENFILENAME,SUSERNAME,SCREATE,DCREATE) VALUES (:NIMPORTID,:SFILENAME,:SGENFILENAME,:SUSERNAME,:SCREATE,SYSDATE)";
            using (OracleCommand com = new OracleCommand(strsql, con))
            {
                string genImportID = CommonFunction.Gen_ID(con, "SELECT NIMPORTID FROM (SELECT NIMPORTID FROM TIMPORTFILE ORDER BY NIMPORTID DESC) WHERE ROWNUM <= 1");
                com.Parameters.Clear();
                com.Parameters.Add(":NIMPORTID", OracleType.Number).Value = Convert.ToInt32(genImportID);
                com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = filename;
                com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = genname;
                com.Parameters.Add(":SUSERNAME", OracleType.VarChar).Value = Session["UserName"] + "";
                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                com.ExecuteNonQuery();
            }
        }
    }

    protected void cmbTerminal_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTerminal.SelectCommand = @"SELECT STERMINALID, STERMINALNAME FROM (SELECT t.STERMINALID, ts.STERMINALNAME ,ROW_NUMBER()OVER(ORDER BY t.STERMINALID) AS RN 
FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON t.STERMINALID = ts.STERMINALID WHERE t.STERMINALID || ts.STERMINALNAME LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex ";


        sdsTerminal.SelectParameters.Clear();
        sdsTerminal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTerminal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTerminal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTerminal;
        comboBox.DataBind();
    }

    protected void cmbTerminal_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxDateEdit dtePlan = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");


        if (dtePlan != null && Terminal != null)
        {
            DateTime date;
            DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;
            sdsTruck.SelectCommand = @"SELECT STRANSPORTID, SHEADREGISTERNO,STRAILERREGISTERNO, '' AS DWATEREXPIRE,SVENDORNAME,NCAPACITY 
FROM(SELECT T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,CASE WHEN Tc.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = Tc.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.SHEADREGISTERNO) AS RN 
FROM  ((((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON TC.SHEADID = T.STRUCKID) 
LEFT JOIN TVENDOR_SAP v ON T.STRANSPORTID = V.SVENDORID)
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE('" + date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/mm/yyyy') - 1 AND nvl(ct.STERMINALID,'" + Terminal.Value + "') LIKE '%" + Terminal.Value + "%'  GROUP BY  T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }

    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }


    protected void cboTrailerRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxDateEdit dtePlan = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
        ASPxTextBox VendorID = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtVendorID");

        if (dtePlan != null && Terminal != null && VendorID != null)
        {
            DateTime date;
            DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;
            sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY 
FROM(SELECT Tc.STRAILERREGISTERNO, nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.STRAILERREGISTERNO) AS RN 
FROM  (((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON T.SHEADREGISTERNO =  Tc.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE Tc.STRAILERREGISTERNO IS NOT NULL AND nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'
 AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE('" + date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/mm/yyyy') - 1 AND nvl(ct.STERMINALID,'" + Terminal.Value + "') LIKE '%" + Terminal.Value + "%' AND ct.SVENDORID = '" + VendorID.Text + "' GROUP BY Tc.STRAILERREGISTERNO,T.NTOTALCAPACITY ) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }

    protected void cboTrailerRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cbxTimeWindow_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox cmbTerminal1 = null;
        ASPxDateEdit dteDelivery1 = null;

        if (ASPxPageControl1.ActiveTabIndex == 1)
        {

            cmbTerminal1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
            dteDelivery1 = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dteDelivery");
        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {

            cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
            dteDelivery1 = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dteDelivery");
        }

        if (cmbTerminal1 != null && dteDelivery1 != null)
        {
            if ("" + cmbTerminal1.Value == "H102")
            {
                DateTime date1;
                DateTime date = DateTime.TryParse(dteDelivery1.Value + "", out date1) ? date1 : DateTime.Now;
                if ((int)date.DayOfWeek == 6 || (int)date.DayOfWeek == 0)
                {
                    txtCDAYTYPE.Text = "1";
                }
                else if (CommonFunction.Count_Value(sql, "SELECT * FROM LSTHOLIDAY WHERE TRUNC(DHOLIDAY) = TO_DATE('" + date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')") > 0)
                {
                    txtCDAYTYPE.Text = "1";
                }
                else
                {
                    txtCDAYTYPE.Text = "0";
                }
            }
            else
            {
                txtCDAYTYPE.Text = "0";
            }


            if (cmbTerminal1 != null)
            {

                ASPxComboBox comboBox = (ASPxComboBox)source;
                sdsTimeWindow.SelectCommand = @"SELECT        NLINE, 'เที่ยวที่ ' || NLINE || ' เวลา ' ||  TSTART || ' - ' || TEND AS WINDOWTIME
FROM            TWINDOWTIME
WHERE 'เที่ยวที่ ' || NLINE || ' เวลา ' ||  TSTART || ' - ' || TEND LIKE :fillter AND STERMINALID = :STERMINALID AND CDAYTYPE = :CDAYTYPE ORDER BY NLINE";

                sdsTimeWindow.SelectParameters.Clear();
                sdsTimeWindow.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
                sdsTimeWindow.SelectParameters.Add("STERMINALID", TypeCode.String, cmbTerminal1.Value + "");
                sdsTimeWindow.SelectParameters.Add("CDAYTYPE", TypeCode.String, txtCDAYTYPE.Text);

                comboBox.DataSource = sdsTimeWindow;
                comboBox.DataBind();
            }
        }

    }

    protected void cbxTimeWindow_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboHeadRegisTFIFO_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxDateEdit dtePlan = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");

        if (dtePlan != null && Terminal != null)
        {
            DateTime date;
            DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;

            sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRANSPORTID,SVENDORNAME,'' AS DWATEREXPIRE,NCAPACITY FROM( 

SELECT f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID AS STRANSPORTID,V.SVENDORNAME,
  CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN
  
 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) END AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.SHEADREGISTERNO) AS RN 
FROM  ((TFIFO f  LEFT JOIN TVENDOR_SAP v ON F.SVENDORID  = V.SVENDORID) LEFT JOIN TTRUCK t ON F.SHEADREGISTERNO = T.SHEADREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID
WHERE NVL(F.CACTIVE,'1') = '1' AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID ,V.SVENDORNAME,T.NTOTALCAPACITY  ) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal.Value + ""));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboHeadRegisTFIFO_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboTrailerRegisTFIFO_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxDateEdit dtePlan = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
        ASPxTextBox VendorID = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtVendorID");

        if (dtePlan != null && Terminal != null && VendorID != null)
        {
            DateTime date;
            DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;
            sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY FROM( 

SELECT f.STRAILERREGISTERNO , nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0)))  AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.STRAILERREGISTERNO) AS RN 
FROM  (TFIFO f   LEFT JOIN TTRUCK t ON t.SHEADREGISTERNO = f.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID 
WHERE  NVL(F.CACTIVE,'1') = '1' AND f.STRAILERREGISTERNO IS NOT NULL AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL AND SVENDORID = :SVENDORID  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO ,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal.Value + ""));
            sdsTruck.SelectParameters.Add("SVENDORID", TypeCode.String, VendorID.Text + "");
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboTrailerRegisTFIFO_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID,SPERSONELNO,FULLNAME,STEL,SPERSONELNO FROM (SELECT E.SEMPLOYEEID,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,E.SPERSONELNO, ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND E.INAME || ES.FNAME || ' ' || ES.LNAME || E.SPERSONELNO LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT E.SEMPLOYEEID,E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE  E.SEMPLOYEEID = :fillter ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, e.Value + "");

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();
    }

    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsDelivery.SelectCommand = @"SELECT DELIVERY_NO,SHIP_TO,SCUSTOMER,NVALUE FROM(SELECT d.DELIVERY_NO,d.SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
, ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN FROM TDELIVERY d  LEFT JOIN TCUSTOMER_SAP ct ON D.SHIP_TO = CT.SHIP_TO  WHERE d.DELIVERY_NO LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsDelivery.SelectParameters.Clear();
        sdsDelivery.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsDelivery.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsDelivery.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsDelivery;
        comboBox.DataBind();

        if (comboBox.Items.Count <= 0)
        {
            comboBox.Value = "";
        }

    }

    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    [Serializable]
    struct BDT1
    {
        public int dtDrop { get; set; }
        public string dtDeliveryNo { get; set; }
        public string dtShipto { get; set; }
        public int dtValue { get; set; }
        public int sTimeWindow { get; set; }
        public string dtShiptoName { get; set; }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void SetVisibleControl()
    {
        if (ASPxPageControl1.ActiveTabIndex == 0)
        {

        }
        else if (ASPxPageControl1.ActiveTabIndex == 1)
        {
            ASPxDateEdit dtePlan1 = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dtePlan");
            ASPxTextBox txtTime1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtTime");
            ASPxDateEdit dteDelivery1 = (ASPxDateEdit)gvw2.FindEditFormTemplateControl("dteDelivery");

            dtePlan1.Value = DateTime.Now;
            txtTime1.Text = DateTime.Now.ToString("HH.mm");
            dteDelivery1.Value = DateTime.Now;
        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw1.FindEditFormTemplateControl("btnsAdd");
            ASPxLabel lblShow = (ASPxLabel)gvw1.FindEditFormTemplateControl("lblShow");
            ASPxDateEdit dtePlan1 = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dtePlan");
            ASPxTextBox txtTime1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtTime");
            ASPxDateEdit dteDelivery1 = (ASPxDateEdit)gvw1.FindEditFormTemplateControl("dteDelivery");

            if (gvw1.IsNewRowEditing == true)
            {
                lblShow.Text = "เพิ่ม / Add";
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;
                dtePlan1.Value = DateTime.Now;
                txtTime1.Text = DateTime.Now.ToString("HH.mm");
                dteDelivery1.Value = DateTime.Now;
            }
            else
            {
                lblShow.Text = "แก้ไข / Edit";
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }
        else if (ASPxPageControl1.ActiveTabIndex == 3)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw3.FindEditFormTemplateControl("btnsAdd");
            ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

            if (rblCheck.SelectedIndex == 2)
            {
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;

            }
            else
            {
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }

    }

    void SetVisibleControlAdd()
    {
        if (ASPxPageControl1.ActiveTabIndex == 0)
        {

        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw1.FindEditFormTemplateControl("btnsAdd");
            ASPxLabel lblShow = (ASPxLabel)gvw1.FindEditFormTemplateControl("lblShow");

            if (gvw1.IsNewRowEditing == true)
            {
                lblShow.Text = "เพิ่ม / Add";
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;
            }
            else
            {
                lblShow.Text = "แก้ไข / Edit";
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }
        else if (ASPxPageControl1.ActiveTabIndex == 3)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw3.FindEditFormTemplateControl("btnsAdd");
            ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

            if (rblCheck.SelectedIndex == 2)
            {
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;

            }
            else
            {
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }

    }

    void listImportFileDetail()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT ROW_NUMBER() OVER(ORDER BY NIMPORTID) AS ID,SUSERNAME,DCREATE FROM TIMPORTFILE WHERE to_char(DCREATE,'dd/MM/yyyy') = to_char(sysdate,'dd/MM/yyyy')");
        if (dt.Rows.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            string str = "<tr><td><img src='images/ic_ms_excel.gif' width='16' height='16'> ครั้งที่ {0} {1} {2} น.</td></tr>";
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append(string.Format(str, dr["ID"], dr["SUSERNAME"], Convert.ToDateTime(dr["DCREATE"]).ToShortTimeString()));
            }
            sb.Append("</table>");

            //ltlHistory.Text = sb.ToString();
            ltlHistory1.Text = sb.ToString();

        }
    }
    protected void gvw1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        switch (e.CallbackName)
        {
            case "SORT":
                //gvw.CancelEdit();
                gvw1.CancelEdit();
                gvw2.CancelEdit();
                gvw3.CancelEdit();

                var dtt = new List<BDT1>();
                Session["bdt1"] = dtt;
                break;
        }
    }

    private void ShowCountCar()
    {

        string nCounTFIFO = CommonFunction.Get_Value(sql, "SELECT COUNT(*) AS nCounTFIFO FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' AND STERMINAL = '" + CommonFunction.ReplaceInjection(cboTerminal2.Value + "") + "' AND (nvl(CPLAN,'0') = '0') AND TO_DATE(DDATE,'dd/MM/yyyy') = TO_DATE(SYSDATE,'dd/MM/yyyy')");
        lblCarWait.Text = nCounTFIFO;

        string _ddate = (ASPxPageControl1.ActiveTabIndex == 2) ? dtePlan1.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) : DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"))
            , _condition = (ASPxPageControl1.ActiveTabIndex == 3 ? "" : " AND nvl(STERMINALID,'" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "') LIKE '%" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "%'")
            , _condition1 = (ASPxPageControl1.ActiveTabIndex == 3 ? "" : " AND nvl(ct.STERMINALID,'" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "') LIKE '%" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "%'");

        string nCarConfirm = CommonFunction.Get_Value(sql, "SELECT COUNT(tc.sHeadRegisterNo) AS nCarCount FROM (TTRUCKCONFIRM t INNER JOIN TTruckConfirmList tc ON t.NCONFIRMID = tc.NCONFIRMID)LEFT JOIN TCONTRACT ct ON t.SCONTRACTID = ct.SCONTRACTID WHERE tc.cConfirm = '1'  AND TO_DATE(t.DDATE,'dd/MM/yyyy') = TO_DATE('" + CommonFunction.ReplaceInjection(_ddate) + "','dd/MM/yyyy') - 1 " + _condition1);
        string nCarPlan = CommonFunction.Get_Value(sql, "SELECT COUNT(tc.sHeadRegisterNo) AS nCarCount FROM ((TTRUCKCONFIRM t INNER JOIN TTruckConfirmList tc ON t.NCONFIRMID = tc.NCONFIRMID) LEFT JOIN TCONTRACT ct ON t.SCONTRACTID = ct.SCONTRACTID) INNER JOIN (SELECT sHeadRegisterNo FROM TPLANSCHEDULE WHERE CACTIVE = '1' AND TO_DATE(SPLANDATE,'dd/MM/yyyy') = TO_DATE('" + CommonFunction.ReplaceInjection(_ddate) + "','dd/MM/yyyy') " + _condition + " GROUP BY sHeadRegisterNo) p ON tc.sHeadRegisterNo = p.sHeadRegisterNo   WHERE (tc.cConfirm = '1'  AND TO_DATE(t.DDATE,'dd/MM/yyyy') = TO_DATE('" + CommonFunction.ReplaceInjection(_ddate) + "','dd/MM/yyyy') - 1)  " + _condition1);

        int num;
        int nnCarConfirm = int.TryParse(nCarConfirm, out num) ? num : 0;
        int nnCarPlan = int.TryParse(nCarPlan, out num) ? num : 0;
        int sumCarConfirm = nnCarConfirm - nnCarPlan;

        lblShowConfirm1.Text = sumCarConfirm + "";
        lblShowBookCar1.Text = nCarPlan;

        lblShowConfirm2.Text = sumCarConfirm + "";
        lblShowBookCar2.Text = nCarPlan;
        lblDateNow.Text = DateTime.Today.ToString("dd/MM/yyyy");
        //}
    }

    protected void cboHeadRegisTFIFOPLAN_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxTextBox VendorID = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtVendorID");

        if (VendorID != null)
        {
            string Terminal = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "STERMINALID") + "";
            string DPLAN = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "SPLANDATE") + "";
            DateTime date;
            DateTime date1 = DateTime.TryParse(DPLAN + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;


            if (("" + txtCTYPE.Text).Equals("0"))
            {
                sdsTruck.SelectCommand = @"SELECT STRANSPORTID, SHEADREGISTERNO,STRAILERREGISTERNO, '' AS DWATEREXPIRE,SVENDORNAME,NCAPACITY 
FROM(SELECT T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,CASE WHEN Tc.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = Tc.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.SHEADREGISTERNO) AS RN 
FROM  ((((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON TC.SHEADID = T.STRUCKID) 
LEFT JOIN TVENDOR_SAP v ON T.STRANSPORTID = V.SVENDORID)
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID    WHERE nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE(:DPLAN,'dd/MM/yyyy') - 1 AND nvl(ct.STERMINALID,:TERMINAL) LIKE :TERMINAL  GROUP BY  T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }
            else
            {
                sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRANSPORTID,SVENDORNAME,'' AS DWATEREXPIRE,NCAPACITY FROM( 

SELECT f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID AS STRANSPORTID,V.SVENDORNAME,
  CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN
  
 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) END AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.SHEADREGISTERNO) AS RN 
FROM  ((TFIFO f  LEFT JOIN TVENDOR_SAP v ON F.SVENDORID  = V.SVENDORID) LEFT JOIN TTRUCK t ON F.SHEADREGISTERNO = T.SHEADREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID
WHERE NVL(F.CACTIVE,'1') = '1' AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID ,V.SVENDORNAME,T.NTOTALCAPACITY  ) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal + ""));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboHeadRegisTFIFOPLAN_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboTrailerRegisTFIFOPLAN_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxTextBox VendorID = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtVendorID");

        if (VendorID != null)
        {
            string Terminal = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "STERMINALID") + "";
            string DPLAN = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "SPLANDATE") + "";

            DateTime date;
            DateTime date1 = DateTime.TryParse(DPLAN + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;

            if (("" + txtCTYPE.Text).Equals("0"))
            {
                sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY 
FROM(SELECT Tc.STRAILERREGISTERNO, nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.STRAILERREGISTERNO) AS RN 
FROM  (((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON T.SHEADREGISTERNO =  Tc.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE Tc.STRAILERREGISTERNO IS NOT NULL AND  nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE(:DPLAN,'dd/mm/yyyy') - 1 AND nvl(ct.STERMINALID,:TERMINAL) LIKE :TERMINAL AND ct.SVENDORID = :SVENDORID  GROUP BY Tc.STRAILERREGISTERNO,T.NTOTALCAPACITY ) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }
            else
            {
                sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY FROM( 

SELECT f.STRAILERREGISTERNO , nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0)))  AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.STRAILERREGISTERNO) AS RN 
FROM  (TFIFO f   LEFT JOIN TTRUCK t ON t.SHEADREGISTERNO = f.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID 
WHERE NVL(F.CACTIVE,'1') = '1' AND  f.STRAILERREGISTERNO IS NOT NULL AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL AND SVENDORID = :SVENDORID  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal + ""));
            sdsTruck.SelectParameters.Add("SVENDORID", TypeCode.String, VendorID.Text + "");
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboTrailerRegisTFIFOPLAN_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void lkbconfirmtruck_Click(object sender, EventArgs e)
    {
        Response.Redirect("ConfirmTruck.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt(CommonFunction.ReplaceInjection(dtePlan1.Date.ToString("dd/MM/yyyy")) + "&" + CommonFunction.ReplaceInjection(cboTerminal1.Value + ""))));
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void xcpnOutBound_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string paras = CommonFunction.ReplaceInjection(e.Parameter);

        using (OracleConnection con = new OracleConnection(sql))
        {
            if ("" + paras == "") return;
            int count = CommonFunction.Count_Value(con, "SELECT * FROM TDELIVERY_SAP WHERE DELIVERY_NO ='" + CommonFunction.ReplaceInjection(paras) + "'");

            string para = "";
            if (paras.Substring(0, 3) == "008")
            {
                para = paras.Remove(0, 2);
            }
            else
            {
                para = paras;
            }

            //if (count == 0)
            {
                if (con.State == ConnectionState.Closed) con.Open();

                using (OracleCommand com = new OracleCommand("SCHD_DELIVERY_GET", con))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add("S_OUTBOUND", OracleType.VarChar).Value = para;

                    com.ExecuteNonQuery();
                }

            }

        }

    }
}

