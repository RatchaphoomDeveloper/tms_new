﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="User.aspx.cs" Inherits="User" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">เงื่อนไขการค้นหา&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract">
                    <div>
                        <br />
                        <asp:Table runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblUserGroup" runat="server" Text="กลุ่มผู้ใช้งาน :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlUserGroup" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:HiddenField ID="hCGroup" runat="server" />
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblUnitNameTab1" runat="server" Text="หน่วยงาน :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlUnitNameTab1" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:TextBox ID="txtShipTo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    <a style="cursor: pointer;" href="#" id="aShipTo" runat="server" onserverclick="aShipTo_ServerClick">
                                        <asp:Image ID="imgShipTo" ImageUrl="~/Images/blue-37.png" Width="24px" Height="24px" runat="server" />
                                    </a>
                                    <a style="cursor: pointer;" href="#" id="aShipToClear" runat="server" onserverclick="aShipToClear_ServerClick">
                                        <asp:Image ID="imgDelete" ImageUrl="~/Images/bin1.png" Width="24px" Height="24px" runat="server" />
                                    </a>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Enabled="false"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblDivision" runat="server" Text="Division :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlDivision" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblFirstNameTab1" runat="server" Text="ชื่อ :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtFirstNameTab1" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblLastNameTab1" runat="server" Text="นามสกุล :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtLastNameTab1" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblUserNameTab1" runat="server" Text="Username :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtUserNameTab1" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblEmailTab1" runat="server" Text="Email :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtEmailTab1" runat="server" CssClass="form-control" />
                                </asp:TableCell><asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblStatusTab1" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlStatusTab1" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <br />
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdExport" runat="server" Text="&nbsp;Export" OnClick="cmdExport_Click" CssClass="btn btn-info" Width="100px" />
                            <asp:Button ID="cmdSearchTab1" runat="server" Text="&nbsp;Search" OnClick="cmdSearchTab1_Click" CssClass="btn btn-success" Width="100px" />
                            <asp:Button ID="cmdRefresh" runat="server" Text="&nbsp;Clear" OnClick="cmdRefresh_ServerClick" CssClass="btn btn-warning" Width="100px" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                </div>
                <div class="panel-body" align="center">
                    <br />
                    <div class="">
                        <div class="panel-collapse collapse in" id="collapseFindContract2">
                            <div class="table-responsive">
                                <asp:GridView ID="dgvData" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" AutoGenerateColumns="false"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                    OnPageIndexChanging="dgvData_PageIndexChanging" OnRowCommand="dgvData_RowCommand" DataKeyNames="SUID">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SUID" HeaderText="" Visible="false">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SUSERNAME" HeaderText="Username">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FULLNAME" HeaderText="ชื่อ - นามสกุล">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VENDOR_NAME" HeaderText="หน่วยงาน">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Department">
                                            <ItemTemplate>
                                                <div style="width: 1px; white-space: nowrap; text-overflow: ellipsis">
                                                    <%# Eval("DEPARTMENT_NAME") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField DataField="DEPARTMENT_NAME" HeaderText="DEPARTMENT">
                                            <HeaderStyle Wrap="false" HorizontalAlign="Center" Width="5%" />
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="5%" />
                                        </asp:BoundField>--%>
                                        <asp:BoundField DataField="DIVISION_NAME" HeaderText="Division">
                                            <HeaderStyle Wrap="false" HorizontalAlign="Center" Width="5%" />
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="5%" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="width: 100px; white-space: normal; text-overflow: ellipsis">
                                                    <%# Eval("SPOSITION") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField DataField="SPOSITION" HeaderText="ตำแหน่ง">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="5%" />
                                            <HeaderStyle Wrap="false" HorizontalAlign="Center" Width="5%" />
                                        </asp:BoundField>--%>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <div style="width: 100px; white-space: normal; text-overflow: inherit; word-wrap: break-word">
                                                    <%# Eval("SEMAIL") %>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField DataField="SEMAIL" HeaderText="อีเมล์">
                                            <HeaderStyle Wrap="false" HorizontalAlign="Center" Width="5%" />
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="5%" />
                                        </asp:BoundField>--%>
                                        <asp:BoundField DataField="USERGROUP_NAME" HeaderText="กลุ่มผู้ใช้งาน">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="STEL" HeaderText="เบอร์โทรศัพท์">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CREATE_DATE" HeaderText="Create Date">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MAXDCREATE" HeaderText="Last Login">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <%-- <asp:TemplateField HeaderText="สถานะ">
                                                        <HeaderStyle Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" Value='' ID="CACTIVE" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                        <asp:BoundField DataField="STATUS_NAME" HeaderText="สถานะ">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="View">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aView" runat="server" ImageUrl="~/Images/blue-37.png" CommandName="view" Width="24px" Height="24px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aEdit" runat="server" ImageUrl="~/Images/blue-23.png" CommandName="editData" Width="26px" Height="26px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="panel-footer" style="text-align: right">
                            <%--<a href="#" id="cmdAdd" runat="server" class="btn btn-md btn-hover btn-success" style="width: 100px" onserverclick="cmdAdd_ServerClick"><i class="fa fa-edit"></i>&nbsp;เพิ่มข้อมูล</a>--%>
                            <asp:Button ID="cmdAdd" runat="server" Text="&nbsp;เพิ่มข้อมูล" OnClick="cmdAdd_ServerClick" CssClass="btn btn-success" Width="100px" />
                            <%--<a href="#" id="cmdRefresh" runat="server" class="btn btn-md btn-hover btn-success" style="width: 100px" onserverclick="cmdRefresh_ServerClick"><i class="fa fa-refresh"></i>&nbsp;รีเฟรช</a>--%>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmdExport" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="modal fade" id="ShowOTP" role="dialog" aria-labelledby="myModalLabel"
        data-keyboard="false" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updOTP" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content" style="width: auto">
                        <div class="modal-header">
                            <button type="button" id="Button6" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="modal-title">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/blue-37.png" Width="48" Height="48" />
                                <asp:Label ID="lblOTP" runat="server" Text=""></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:Table ID="Table9" runat="server">
                                <%--<asp:TableRow>
                                    <asp:TableCell Width="520" ColumnSpan="3">
                                        <asp:Label ID="lblComplainUrgent" runat="server" Text="asdf"></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="110" ColumnSpan="3">
                                        <asp:Label ID="lblComplainUrgent2" runat="server" Text=""></asp:Label></asp:TableCell>
                                </asp:TableRow>--%>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell Width="210">
                                        <input id="txtInput" runat="server" class="form-control" style="width: 300px; text-align: center" placeholder="Ship To, Sold To" />
                                    </asp:TableCell><asp:TableCell Width="200">
                                        &nbsp;<asp:Button ID="Button1" runat="server" CssClass="btn btn-info" Text="ค้นหา" SkinID="ButtonSuccess" OnClick="cmdSearch_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:GridView ID="dgvSearch" runat="server" DataKeyNames="SHIP_ID,SOLD_ID,SHIP_NAME,SOLD_NAME" SkinID="GridNoPaging" OnRowCommand="dgvSearch_RowCommand" AutoGenerateColumns="false" GridLines="None" 
                                CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White">
                                <HeaderStyle Wrap="false" />
                                <%--<rowstyle wrap="false" />--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <a style="cursor: pointer;" href="#" id="aChoose" runat="server">
                                                <asp:ImageButton ImageUrl="~/Images/blue-23.png" Width="24px" Height="24px" runat="server" CommandName="ChooseData" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SHIP_ID" HeaderText="Ship To"></asp:BoundField>
                                    <asp:BoundField DataField="SHIP_NAME" HeaderText="Ship To"></asp:BoundField>
                                    <asp:BoundField DataField="SOLD_ID" HeaderText="Sold To"></asp:BoundField>
                                    <asp:BoundField DataField="SOLD_NAME" HeaderText="Sold To"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <button type="button" id="Button9" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="hdfShipID" runat="server" />
            <asp:HiddenField ID="hdfSoldID" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
