﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_edit_p4.aspx.cs" Inherits="vendor_edit_p4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
 // <![CDATA[
        var fieldSeparator = "|";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }
        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + fieldSeparator.length);
                var date = new Date();
                var imgSrc = "UploadImages/" + pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }
        // ]]>

        $(document).ready(function () {
            VisibleControlRadio();
        });
        function VisibleControlDel() {


            var bool = txtFilePath.GetValue() != "" || txtFilePath.GetValue() != null;
            uploader1.SetClientVisible(bool);
            txtFileName.SetClientVisible(!bool);
            btnView.SetEnabled(!bool);
            btnDelFile.SetEnabled(!bool);
            if (!(txtFilePath.GetValue() != "" || txtFilePath.GetValue() != null)) {
                chkUpload1.SetValue('1');
            } else {
                chkUpload1.SetValue('');
            }

        }


        function VisibleControl() {


            var bool = txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null;
            uploader1.SetClientVisible(bool);
            txtFileName.SetClientVisible(!bool);
            btnView.SetEnabled(!bool);
            btnDelFile.SetEnabled(!bool);
            if (!(txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null)) {
                chkUpload1.SetValue('1');
            } else {
                chkUpload1.SetValue('');
            }

        }

        function VisibleControl2() {


            var bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
            uploader2.SetClientVisible(bool);
            txtFileName2.SetClientVisible(!bool);
            btnView2.SetEnabled(!bool);
            btnDelFile2.SetEnabled(!bool);
            if (!(txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null)) {
                chkUpload2.SetValue('1');
            } else {
                chkUpload2.SetValue('');
            }

        }

        function VisibleControl3() {


            var bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
            uploader3.SetClientVisible(bool);
            txtFileName3.SetClientVisible(!bool);
            btnView3.SetEnabled(!bool);
            btnDelFile3.SetEnabled(!bool);
            if (!(txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null)) {
                chkUpload3.SetValue('1');
            } else {
                chkUpload3.SetValue('');
            }

        }

        function VisibleControl4() {

            var bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
            uploader4.SetClientVisible(bool);
            txtFileName4.SetClientVisible(!bool);
            btnView4.SetEnabled(!bool);
            btnDelFile4.SetEnabled(!bool);
            if (!(txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null)) {
                chkUpload4.SetValue('1');
            } else {
                chkUpload4.SetValue('');
            }

        }

        function VisibleControl5() {
            if (txtFilePath5.GetValue() != "");
            {
                var bool = txtFilePath5.GetValue() == "" || txtFilePath5.GetValue() == null;
                uploader5.SetClientVisible(bool);
                txtFileName5.SetClientVisible(!bool);
                btnView5.SetEnabled(!bool);
                btnDelFile5.SetEnabled(!bool);
                if (!(txtFilePath5.GetValue() == "" || txtFilePath5.GetValue() == null)) {
                    chkUpload5.SetValue('1');
                } else {
                    chkUpload5.SetValue('');
                }
            }
        }

        function VisibleControlRadio() {



            switch (rblStatus.GetSelectedIndex()) {
                case 0: document.getElementById("<%=Typedefault.ClientID %>").style.display = "table-row";
                    document.getElementById("<%=status.ClientID %>").style.display = "none";
                    document.getElementById("<%=status2.ClientID %>").style.display = "none";

                    break;
                case 1: document.getElementById("<%=Typedefault.ClientID %>").style.display = "none";
                    document.getElementById("<%=status.ClientID %>").style.display = "table-row";
                    document.getElementById("<%=status2.ClientID %>").style.display = "none";
                    break;
                case 2: document.getElementById("<%=Typedefault.ClientID %>").style.display = "none";
                    document.getElementById("<%=status.ClientID %>").style.display = "none";
                    document.getElementById("<%=status2.ClientID %>").style.display = "table-row";
                    break;
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined ; if(s.cpSetjava != undefined)VisibleControlRadio(); }">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <%------------------------------------------------------------------ข้อมูลของบริษัท---------------------------------------------------------------%>
                <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="ข้อมูลของบริษัท">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="arp">
                            <table width="100%" runat="server">
                                <tr>
                                    <td colspan="4">
                                        <dx:ASPxTextBox runat="server" ID="txtLineNo" ClientInstanceName="txtLineNo" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" align="right">
                                        รหัสบริษัท:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSVENDORID" Width="200px" Enabled="false" SkinID="_txtRead">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td width="15%" align="right">
                                        ชื่อบริษัท:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSABBREVIATION" Width="200px" SkinID="_txtRead">
                                      <%--      <ValidationSettings ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField ErrorText="ระบุชื่อบริษัท" IsRequired="true" />
                                            </ValidationSettings>--%>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" align="right">
                                        ที่อยู่:
                                    </td>
                                    <td width="35%" colspan="3">
                                        <%--  <dx:ASPxTextBox runat="server" ID="txtSNO" Width="200px" Enabled="false" SkinID="_txtRead">
                                        </dx:ASPxTextBox>--%>
                                        <dx:ASPxMemo runat="server" ID="txtSNO" Width="350px" Height="75" SkinID="_memoRead">
                                          <%--  <ValidationSettings ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField ErrorText="ระบุที่อยู่" IsRequired="true" />
                                            </ValidationSettings>--%>
                                        </dx:ASPxMemo>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" align="right">
                                        ตำบล:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSDISTRICT" Width="200px" SkinID="_txtRead">
                                           <%-- <ValidationSettings ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField ErrorText="ระบุตำบล" IsRequired="true" />
                                            </ValidationSettings>--%>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td width="15%" align="right">
                                        อำเภอ:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSREGION" Width="200px" SkinID="_txtRead">
                                           <%-- <ValidationSettings ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField ErrorText="ระบุอำเภอ" IsRequired="true" />
                                            </ValidationSettings>--%>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" align="right">
                                        จังหวัด:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSPROVINCE" Width="200px" SkinID="_txtRead">
                                        <%--    <ValidationSettings ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField ErrorText="ระบุจังหวัด" IsRequired="true" />
                                            </ValidationSettings>--%>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td width="15%" align="right">
                                        รหัสไปรษณีย์:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSPROVINCECODE" Width="200px" SkinID="_txtRead">
                                            <%--    <ValidationSettings ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                <RequiredField ErrorText="ระบุรหัสไปรษณีย์" IsRequired="true" />
                                            </ValidationSettings>--%>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" align="right">
                                        หมายเลขโทรศัพท์:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSTEL" Width="200px">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td width="15%" align="right">
                                        หมายเลขโทรสาร:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxTextBox runat="server" ID="txtSFAX" Width="200px">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" align="right">
                                        วันที่ที่ลงทะเบียนคู่ค้า:
                                    </td>
                                    <td width="35%">
                                        <dx:ASPxDateEdit runat="server" ID="dedtDSTARTPTT" Width="150px" SkinID="xdte_Readonly">
                                        </dx:ASPxDateEdit>
                                    </td>
                                    <td width="14%" align="right">
                                        ทุนจดทะเบียน:
                                    </td>
                                    <td width="36%">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td width="80%">
                                                    <dx:ASPxTextBox runat="server" ID="txtNCAPITAL" Width="200px" CssClass="dxeLineBreakFix"
                                                        DisplayFormatString="{0:#,###}">
                                                        <ValidationSettings EnableCustomValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                            SetFocusOnError="true" ValidationGroup="add">
                                                            <RegularExpression ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>"
                                                                ErrorText="กรอกได้เฉพาะตัวเลขเท่านั้น" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td width="20%">
                                                    <dx:ASPxLabel runat="server" ID="lblBath" Text="บาท" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" align="right">
                                        อายุใบอนุญาตขนส่ง:
                                    </td>
                                    <td width="35%" style="padding: 0 0 0 0">
                                        <dx:ASPxDateEdit runat="server" ID="dedtDBEGINTRANSPORT" SkinID="xdte" Width="150px"
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxDateEdit>
                                        <dx:ASPxDateEdit runat="server" ID="dedtDEXPIRETRANSPORT" SkinID="xdte" Width="150px"
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxDateEdit>
                                    </td>
                                    <td width="15%" align="right">
                                        อายุมาตรา 12:
                                    </td>
                                    <td width="35%" style="padding: 0 0 0 0">
                                        <dx:ASPxDateEdit runat="server" ID="dedtDBEGIN13BIS" SkinID="xdte" Width="150px"
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxDateEdit>
                                        <dx:ASPxDateEdit runat="server" ID="dedtDEXPIRE13BIS" SkinID="xdte" Width="150px"
                                            CssClass="dxeLineBreakFix" ClientVisible="false">
                                        </dx:ASPxDateEdit>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
                <%------------------------------------------------------------------เอกสารสำคัญของบริษัท---------------------------------------------------------------%>
                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="เอกสารสำคัญของบริษัท">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="PanelContent1">
                            <table width="100%" runat="server">
                                <tr>
                                    <td colspan="1">
                                        <dx:ASPxTextBox runat="server" ID="Up1" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtchkUpdate" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="Up2" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtchkUpdate2" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="Up3" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtchkUpdate3" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="Up4" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtchkUpdate4" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td align="right">
                                        <dx:ASPxButton runat="server" ID="btnHistoryDoc" AutoPostBack="false" Text="เอกสารเก่า">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('viewHistory'); }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <font color="black">คำแนะนำระเบียบการใช้งาน<br>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สามารถอัพโหลดไฟล์ .jpg,.jpeg,.jpe,.gif,.png,.doc,.docx,.xlsx,.xls,.txt,.pdf,.ppt,.pptx,.mm,.zip,.rar
                                            เท่านั้น และขนาดไม่เกิน 1 MB</font></td>
                                </tr>
                                <tr>
                                    <td width="20%" align="right">
                                        แนบหนังสือรับรองผู้ขนส่ง:
                                    </td>
                                    <td width="80%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" valign="middle" width="22%">
                                                    <dx:ASPxUploadControl ID="uploader1" runat="server" ClientInstanceName="uploader1"
                                                        CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="upload_FileUploadComplete"
                                                        Width="200px">
                                                        <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>"
                                                            MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>" MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                        </ValidationSettings>
                                                        <ClientSideEvents TextChanged="function(s,e){uploader1.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath.SetValue((e.callbackData+'').split('|')[0]);txtFileName.SetValue((e.callbackData+'').split('|')[1]);txtTruePath.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename.SetValue((e.callbackData+'').split('|')[3]);VisibleControl();} }" />
                                                        <BrowseButton Text="แนบไฟล์">
                                                        </BrowseButton>
                                                    </dx:ASPxUploadControl>
                                                    <dx:ASPxTextBox ID="txtFilePath" runat="server" ClientInstanceName="txtFilePath"
                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtFileName" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName"
                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtTruePath" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath"
                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtSysfilename" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilename"
                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="chkUpload1" runat="server" ClientInstanceName="chkUpload1" CssClass="dxeLineBreakFix"
                                                        ClientVisible="false" ForeColor="White" Width="1px">
                                                        <Border BorderStyle="None" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td width="78%" valign="middle" align="left">
                                                    <dx:ASPxButton ID="btnView" runat="server" AutoPostBack="False" CausesValidation="False"
                                                        ClientEnabled="False" ClientInstanceName="btnView" CssClass="dxeLineBreakFix"
                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath.GetValue()+ txtSysfilename.GetValue());}" />
                                                        <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnDelFile" runat="server" AutoPostBack="False" CausesValidation="False"
                                                        ClientEnabled="False" ClientInstanceName="btnDelFile" CssClass="dxeLineBreakFix"
                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath.GetValue() +';1');}" />
                                                        <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                    <%--  <dx:ASPxButton ID="btnUpload" runat="server" AutoPostBack="false" Text="Upload" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e){uploader1.Upload();}" />
                                                    </dx:ASPxButton>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="right">
                                        แนบใบอนุญาตขนส่ง:
                                    </td>
                                    <td width="80%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" valign="middle" width="22%">
                                                    <dx:ASPxUploadControl ID="uploader2" runat="server" ClientInstanceName="uploader2"
                                                        CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="upload2_FileUploadComplete"
                                                        Width="200px">
                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                            MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                        </ValidationSettings>
                                                        <ClientSideEvents TextChanged="function(s,e){uploader2.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath2.SetValue((e.callbackData+'').split('|')[0]);txtFileName2.SetValue((e.callbackData+'').split('|')[1]);txtTruePath2.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename2.SetValue((e.callbackData+'').split('|')[3]); 
                 if(dedtUpload2.GetText() != '')
                { 
                    txtValidation1.SetValue('.')
                    txtValidation1.SetClientVisible(false);
                    txtValidation2.SetClientVisible(false);
                    txtValidation2.SetValue('.')
                } 
                else
                {
                    txtValidation1.SetValue('.')
                    txtValidation1.SetClientVisible(false);
                    txtValidation2.SetClientVisible(true);
                    txtValidation2.SetValue('')
                };VisibleControl2();} }" />
                                                        <BrowseButton Text="แนบไฟล์">
                                                        </BrowseButton>
                                                    </dx:ASPxUploadControl>
                                                    <dx:ASPxTextBox ID="txtFilePath2" runat="server" ClientInstanceName="txtFilePath2"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtFileName2" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName2"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtTruePath2" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath2"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtSysfilename2" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilename2"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="chkUpload2" runat="server" ClientInstanceName="chkUpload2" CssClass="dxeLineBreakFix"
                                                        ClientVisible="false" ForeColor="White" Width="1px">
                                                        <Border BorderStyle="None" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td width="9%">
                                                    สิ้นอายุ
                                                </td>
                                                <td align="left" valign="middle" width="26%">
                                                    <dx:ASPxButton ID="btnView2" runat="server" AutoPostBack="False" CausesValidation="False"
                                                        ClientEnabled="False" ClientInstanceName="btnView2" CssClass="dxeLineBreakFix"
                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath2.GetValue()+ txtSysfilename2.GetValue());}" />
                                                        <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnDelFile2" runat="server" AutoPostBack="False" CausesValidation="False"
                                                        ClientEnabled="False" ClientInstanceName="btnDelFile2" CssClass="dxeLineBreakFix"
                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile2;'+ txtFilePath2.GetValue() +';1');}" />
                                                        <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxDateEdit runat="server" ID="dedtUpload2" SkinID="xdte" CssClass="dxeLineBreakFix"
                                                        ClientInstanceName="dedtUpload2">
                                                        <ClientSideEvents ValueChanged="function(s,e){
                                                        if(dedtUpload2.GetText() != '')
                                                        { 
	                                                        if(txtSysfilename2.GetText() != '') 
	                                                        { 
		                                                        txtValidation1.SetValue('.');
		                                                        txtValidation2.SetValue('.');
                                                                txtValidation1.SetClientVisible(false);
		                                                        txtValidation2.SetClientVisible(false);
	                                                        } 
	                                                        else 
	                                                        { 
		                                                        txtValidation1.SetValue('');
				                                                txtValidation1.SetClientVisible(true);
		                                                        txtValidation2.SetClientVisible(false);
		                                                        txtValidation2.SetValue('.');
		                                                    }
                                                        }
                                                        else
                                                        {
                                                            if(txtSysfilename2.GetText() != '') 
	                                                        { 
		                                                        txtValidation1.SetValue('.');
		                                                        txtValidation2.SetValue('');
                                                                txtValidation1.SetClientVisible(false);
		                                                        txtValidation2.SetClientVisible(true);
	                                                        } 
	                                                        else 
	                                                        { 
		                                                        txtValidation1.SetValue('.');
				                                                txtValidation1.SetClientVisible(false);
		                                                        txtValidation2.SetClientVisible(false);
		                                                        txtValidation2.SetValue('.');
		                                                    }
                                                        }}" />
                                                    </dx:ASPxDateEdit>
                                                    <%--    <dx:ASPxButton ID="btnUpload2" runat="server" AutoPostBack="false" Text="Upload"
                                                        CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e){ uploader2.Upload();}" />
                                                    </dx:ASPxButton>--%>
                                                </td>
                                                <td width="44%">
                                                    <dx:ASPxTextBox runat="server" ID="txtValidation1" ClientInstanceName="txtValidation1">
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField ErrorText="ระบุวันสิ้นสุดของเอกสาร" IsRequired="true" />
                                                        </ValidationSettings>
                                                        <Border BorderWidth="0px" />
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="txtValidation2" ClientInstanceName="txtValidation2">
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField ErrorText="ระบุวันสิ้นสุดของเอกสาร" IsRequired="true" />
                                                        </ValidationSettings>
                                                        <Border BorderWidth="0px" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" align="right">
                                        แนบเอกสารภาษีอากรมาตรา12:
                                    </td>
                                    <td width="80%">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" valign="middle" width="22%">
                                                    <dx:ASPxUploadControl ID="uploader3" runat="server" ClientInstanceName="uploader3"
                                                        CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="upload3_FileUploadComplete"
                                                        Width="200px">
                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                            MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                        </ValidationSettings>
                                                        <ClientSideEvents TextChanged="function(s,e){uploader3.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath3.SetValue((e.callbackData+'').split('|')[0]);txtFileName3.SetValue((e.callbackData+'').split('|')[1]);txtTruePath3.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename3.SetValue((e.callbackData+'').split('|')[3]);
                 if(dedtUpload3.GetText() != '')
                { 
                    txtValidation3.SetValue('.')
                    txtValidation3.SetClientVisible(false);
                    txtValidation4.SetClientVisible(false);
                    txtValidation4.SetValue('.')
                } 
                else
                {
                    txtValidation3.SetValue('.')
                    txtValidation3.SetClientVisible(false);
                    txtValidation4.SetClientVisible(true);
                    txtValidation4.SetValue('')
                };VisibleControl3();} }" />
                                                        <BrowseButton Text="แนบไฟล์">
                                                        </BrowseButton>
                                                    </dx:ASPxUploadControl>
                                                    <dx:ASPxTextBox ID="txtFilePath3" runat="server" ClientInstanceName="txtFilePath3"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtFileName3" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName3"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtTruePath3" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath3"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtSysfilename3" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilename3"
                                                        ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="chkUpload3" runat="server" ClientInstanceName="chkUpload3" CssClass="dxeLineBreakFix"
                                                        ClientVisible="false" ForeColor="White" Width="1px">
                                                        <Border BorderStyle="None" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <%--<td width="9%">
                                                    สิ้นอายุ
                                                </td>--%>
                                                <td width="26%" valign="middle" align="left">
                                                    <dx:ASPxButton ID="btnView3" runat="server" AutoPostBack="False" CausesValidation="False"
                                                        ClientEnabled="False" ClientInstanceName="btnView3" CssClass="dxeLineBreakFix"
                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath3.GetValue()+ txtSysfilename3.GetValue());}" />
                                                        <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnDelFile3" runat="server" AutoPostBack="False" CausesValidation="False"
                                                        ClientEnabled="False" ClientInstanceName="btnDelFile3" CssClass="dxeLineBreakFix"
                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile3;'+ txtFilePath3.GetValue() +';1');}" />
                                                        <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxDateEdit runat="server" ID="dedtUpload3" SkinID="xdte" CssClass="dxeLineBreakFix"
                                                        ClientInstanceName="dedtUpload3" ClientVisible="false">
                                                        <ClientSideEvents ValueChanged="function(s,e){
                                                        if(dedtUpload3.GetText() != '')
                                                        { 
	                                                        if(txtSysfilename3.GetText() != '') 
	                                                        { 
		                                                        txtValidation3.SetValue('.');
		                                                        txtValidation4.SetValue('.');
                                                                txtValidation3.SetClientVisible(false);
		                                                        txtValidation4.SetClientVisible(false);
	                                                        } 
	                                                        else 
	                                                        { 
		                                                        txtValidation3.SetValue('');
				                                                txtValidation3.SetClientVisible(true);
		                                                        txtValidation4.SetClientVisible(false);
		                                                        txtValidation4.SetValue('.');
		                                                    }
                                                        }
                                                        else
                                                        {
                                                            if(txtSysfilename3.GetText() != '') 
	                                                        { 
		                                                        txtValidation3.SetValue('.');
		                                                        txtValidation4.SetValue('');
                                                                txtValidation3.SetClientVisible(false);
		                                                        txtValidation4.SetClientVisible(true);
	                                                        } 
	                                                        else 
	                                                        { 
		                                                        txtValidation3.SetValue('.');
				                                                txtValidation3.SetClientVisible(false);
		                                                        txtValidation4.SetClientVisible(false);
		                                                        txtValidation4.SetValue('.');
		                                                    }
                                                        }}" />
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="44%">
                                                    <dx:ASPxTextBox runat="server" ID="txtValidation3" ClientInstanceName="txtValidation3">
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField ErrorText="คุณยังไม่ได้ UploadFile" IsRequired="true" />
                                                        </ValidationSettings>
                                                        <Border BorderWidth="0px" />
                                                        <ClientSideEvents GotFocus="" />
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="txtValidation4" ClientInstanceName="txtValidation4">
                                                        <%--<ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField ErrorText="ระบุวันสิ้นสุดของเอกสาร" IsRequired="true" />
                                                        </ValidationSettings>--%>
                                                        <Border BorderWidth="0px" />
                                                        <ClientSideEvents GotFocus="" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" valign="top" align="right">
                                        <table cellpadding="0px" cellspacing="0px">
                                            <tr>
                                                <td height="7px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    แนบเอกสารใบประจำต่อ:
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="80%">
                                        <dx:ASPxTextBox runat="server" ID="txtUploadchk4" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxGridView runat="server" ID="gvwdoc4" AutoGenerateColumns="false" Width="100%"
                                            ClientInstanceName="gvwdoc4" OnHtmlDataCellPrepared="gvwdoc4_OnHtmlDataCellPrepared"
                                            KeyFieldName="SVENDORID" EnableRowsCache="False" OnRowInserting="gvwdoc4_RowInserting"
                                            OnAfterPerformCallback="gvwdoc4_AfterPerformCallback" Border-BorderWidth="0px"
                                            Border-BorderStyle="None" SkinID="sds" SettingsPager-AlwaysShowPager="true">
                                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                            </ClientSideEvents>
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="" FieldName="SVENDORID" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Visible="false" CellStyle-Border-BorderWidth="0px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="lblPROD_ID" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"SVENDORID") %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <EditItemTemplate>
                                                        <dx:ASPxUploadControl ID="uploaderDoc4" runat="server" ClientInstanceName="uploaderDoc4"
                                                            CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="uploaderDoc4_FileUploadComplete"
                                                            Width="200px">
                                                            <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                            <%--<ClientSideEvents FileUploadComplete="function(s, e) { gvwdoc4.CancelEdit();} " />--%>
                                                            <ClientSideEvents TextChanged="function(s,e){uploaderDoc4.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwdoc4.CancelEdit();} }" />
                                                            <BrowseButton Text="แนบไฟล์">
                                                            </BrowseButton>
                                                        </dx:ASPxUploadControl>
                                                    </EditItemTemplate>
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox ID="txbDesc" runat="server" Enabled="false" Width="200px" Text='<%# DataBinder.Eval(Container.DataItem,"SFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="10%" CellStyle-Cursor="hand" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="การจัดการ" CellStyle-Border-BorderWidth="0px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblData2" ClientVisible="false">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxTextBox ID="txtFilePath4" runat="server" ClientInstanceName="txtFilePath4"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SPATH") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtFileName4" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName4"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtTruePath4" runat="server" ClientInstanceName="txtTruePath4"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("STRUEPATH") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtSysfilename4" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilename4"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SSYSFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="chkUploadgvwdoc4" runat="server" ClientInstanceName="chkUploadgvwdoc4"
                                                            ClientVisible="false" CssClass="dxeLineBreakFix" ForeColor="White" Width="1px">
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxButton ID="btnViewgvwdoc4" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnViewgvwdoc4" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                            EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd" Width="25px">
                                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnDelFilegvwdoc4" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnDelFilegvwdoc4" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                            EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd" Width="25px">
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <EditItemTemplate>
                                                        <dx:ASPxButton ID="btnViewgvwdoc4show" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnViewgvwdoc4show" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                            Enabled="false" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnDelFilegvwdoc4show" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnDelFilegvwdoc4show" Enabled="false" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </EditItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn CellStyle-Border-BorderWidth="0px" Width="55%">
                                                    <DataItemTemplate>
                                                        <%--<dx:ASPxButton ID="btnSaveImagedata" runat="server" AutoPostBack="false" SkinID="_Upload">
                                                        </dx:ASPxButton>--%>
                                                    </DataItemTemplate>
                                                    <EditItemTemplate>
                                                        <%-- <dx:ASPxButton ID="btnSaveImage" runat="server" AutoPostBack="false" SkinID="_Upload">
                                                            <ClientSideEvents Click="function (s, e) {uploaderDoc4.Upload();}" />
                                                        </dx:ASPxButton>--%>
                                                    </EditItemTemplate>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SDOCID" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SDOCTYPE" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SPATH" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SSYSFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <SettingsEditing Mode="Inline" NewItemRowPosition="Bottom" />
                                            <Settings ShowColumnHeaders="False" GridLines="None" />
                                            <Styles>
                                                <CommandColumn>
                                                    <BorderBottom BorderWidth="0px" />
                                                </CommandColumn>
                                            </Styles>
                                            <Border BorderWidth="0px"></Border>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td width="13%">
                                                </td>
                                                <td width="11%" align="right">
                                                    <dx:ASPxButton runat="server" ID="ASPxButton1" AutoPostBack="false" ToolTip="เพิ่มเอกสารใบประจำต่อ"
                                                        CausesValidation="False" EnableTheming="False" EnableDefaultAppearance="False"
                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                        <ClientSideEvents Click="function(s,e){gvwdoc4.PerformCallback('Newgvwdoc4')}" />
                                                        <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td valign="middle" width="76%">
                                                    เพิ่มเอกสารใบประจำต่อ
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" valign="top" align="right">
                                        <table cellpadding="0px" cellspacing="0px">
                                            <tr>
                                                <td height="7px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    แนบเอกสารอื่นๆ:
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="80%">
                                        <dx:ASPxTextBox runat="server" ID="txtUploadother" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxGridView runat="server" ID="gvwother" AutoGenerateColumns="false" Width="100%"
                                            ClientInstanceName="gvwother" OnHtmlDataCellPrepared="gvwother_OnHtmlDataCellPrepared"
                                            KeyFieldName="SVENDORID" EnableRowsCache="False" OnRowInserting="gvwother_RowInserting"
                                            OnAfterPerformCallback="gvwother_AfterPerformCallback" Border-BorderWidth="0px"
                                            Border-BorderStyle="None" SkinID="sds" SettingsPager-AlwaysShowPager="true">
                                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                            </ClientSideEvents>
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="" FieldName="SVENDORID" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Visible="false" CellStyle-Border-BorderWidth="0px">
                                                    <%--<DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="lblPROD_ID" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"SVENDORID") %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>--%>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <EditItemTemplate>
                                                        <dx:ASPxUploadControl ID="uploaderother" runat="server" ClientInstanceName="uploaderother"
                                                            CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="uploaderother_FileUploadComplete"
                                                            Width="200px">
                                                            <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                            <%--<ClientSideEvents FileUploadComplete="function(s, e) { gvwdoc4.CancelEdit();} " />--%>
                                                            <ClientSideEvents TextChanged="function(s,e){uploaderother.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwother.CancelEdit();} }" />
                                                            <BrowseButton Text="แนบไฟล์">
                                                            </BrowseButton>
                                                        </dx:ASPxUploadControl>
                                                    </EditItemTemplate>
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox ID="txbDesc" runat="server" Enabled="false" Width="200px" Text='<%# DataBinder.Eval(Container.DataItem,"SFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="10%" CellStyle-Cursor="hand" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="การจัดการ" CellStyle-Border-BorderWidth="0px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblData2" ClientVisible="false">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxTextBox ID="txtFilePathother" runat="server" ClientInstanceName="txtFilePathother"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SPATH") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtFileNameother" runat="server" ClientEnabled="False" ClientInstanceName="txtFileNameother"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtTruePathother" runat="server" ClientInstanceName="txtTruePathother"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("STRUEPATH") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtSysfilenameother" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilenameother"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SSYSFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="chkUploadgvwdocother" runat="server" ClientInstanceName="chkUploadgvwdocother"
                                                            ClientVisible="false" CssClass="dxeLineBreakFix" ForeColor="White" Width="1px">
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxButton ID="btnViewgvwother" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnViewgvwother" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                            EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd" Width="25px">
                                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnDelFilegvwother" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnDelFilegvwother" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                            EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd" Width="25px">
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <EditItemTemplate>
                                                        <dx:ASPxButton ID="btnViewgvwothershow" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnViewgvwothershow" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                            Enabled="false" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnDelFilegvwothershow" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientInstanceName="btnDelFilegvwothershow" Enabled="false" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </EditItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn CellStyle-Border-BorderWidth="0px" Width="55%">
                                                    <DataItemTemplate>
                                                        <%--<dx:ASPxButton ID="btnSaveImagedata" runat="server" AutoPostBack="false" SkinID="_Upload">
                                                        </dx:ASPxButton>--%>
                                                    </DataItemTemplate>
                                                    <EditItemTemplate>
                                                        <%-- <dx:ASPxButton ID="btnSaveImage" runat="server" AutoPostBack="false" SkinID="_Upload">
                                                            <ClientSideEvents Click="function (s, e) {uploaderDoc4.Upload();}" />
                                                        </dx:ASPxButton>--%>
                                                    </EditItemTemplate>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SDOCID" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SDOCTYPE" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SPATH" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="" FieldName="SSYSFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle>
                                                        <Border BorderWidth="0px"></Border>
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <SettingsEditing Mode="Inline" NewItemRowPosition="Bottom" />
                                            <Settings ShowColumnHeaders="False" GridLines="None" />
                                            <Styles>
                                                <CommandColumn>
                                                    <BorderBottom BorderWidth="0px" />
                                                </CommandColumn>
                                            </Styles>
                                            <Border BorderWidth="0px"></Border>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td width="13%">
                                                </td>
                                                <td width="11%" align="right">
                                                    <dx:ASPxButton runat="server" ID="ASPxButton2" AutoPostBack="false" ToolTip="เพิ่มเอกสารอื่นๆ"
                                                        CausesValidation="False" EnableTheming="False" EnableDefaultAppearance="False"
                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                        <ClientSideEvents Click="function(s,e){gvwother.PerformCallback('Newgvwother')}" />
                                                        <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td valign="middle" width="76%">
                                                    เพิ่มเอกสารอื่นๆ
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <dx:ASPxGridView runat="server" ID="gvwDoc" AutoGenerateColumns="false" Width="70%"
                                            SkinID="_gvw" OnHtmlDataCellPrepared="gvwDoc_OnHtmlDataCellPrepared" KeyFieldName="SDOCVERSION">
                                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                            </ClientSideEvents>
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ID" FieldName="SDOCID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="version" FieldName="SDOCVERSION" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทเอกสาร" FieldName="SDESCRIPTION" HeaderStyle-HorizontalAlign="Center"
                                                    Width="35%" CellStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ชื่อเอกสาร" FieldName="SFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                    Width="30%" CellStyle-HorizontalAlign="Left">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="SPATH" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="SDOCTYPE" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันหมดอายุ" FieldName="DEXPIRE" HeaderStyle-HorizontalAlign="Center"
                                                    Width="20%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="15%" CellStyle-Cursor="hand" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="การจัดการ">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblData2" ClientVisible="false">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxTextBox ID="txtFilePathgvwDoc" runat="server" ClientInstanceName="txtFilePathgvwDoc"
                                                            ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SPATH") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtFileNamegvwDoc" runat="server" ClientEnabled="False" ClientInstanceName="txtFileNamegvwDoc"
                                                            ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SSYSFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="chkUploadgvwDoc" runat="server" ClientInstanceName="chkUploadgvwDoc"
                                                            CssClass="dxeLineBreakFix" ForeColor="White" Width="1px">
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxButton ID="btnViewgvwDoc" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientEnabled="False" ClientInstanceName="btnViewgvwDoc" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <%--<ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePathgvwDoc.GetValue()+ txtFileNamegvwDoc.GetValue());}" />--%>
                                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnDelFilegvwDoc" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientEnabled="False" ClientInstanceName="btnDelFilegvwDoc" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <%--<ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFilegvwDoc;'+ txtFilePathgvwDoc.GetValue() +';1');}" />--%>
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
                <%------------------------------------------------------------------การลงนาม---------------------------------------------------------------%>
                <dx:ASPxRoundPanel ID="ASPxRoundPanel2" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="การลงนาม">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="PanelContent2">
                            <table width="100%" runat="server">
                                <%--<tr>
                        <td>การลงนาม </td>
                    </tr>--%>
                                <tr>
                                    <td colspan="4">
                                        รายชื่อผู้มีสิทธิ์ลงนาม และคณะกรรมการ
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <dx:ASPxGridView runat="server" ID="gvwsign" AutoGenerateColumns="false" Width="100%"
                                            SkinID="_gvw" ClientInstanceName="gvwsign" OnHtmlDataCellPrepared="gvwsign_OnHtmlDataCellPrepared"
                                            OnAfterPerformCallback="gvwsign_AfterPerformCallback" KeyFieldName="LINE_NO">
                                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                            </ClientSideEvents>
                                            <Columns>
                                                <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false" />
                                                <dx:GridViewDataColumn FieldName="SFILENAME" Visible="false" />
                                                <dx:GridViewDataColumn FieldName="LINE_NO" Visible="false" />
                                                <dx:GridViewDataColumn FieldName="SPATH" Visible="false" />
                                                <dx:GridViewDataColumn FieldName="SSYSFILENAME" Visible="false" />
                                                <dx:GridViewDataColumn FieldName="SNAME" VisibleIndex="1" Caption="ชื่อ - สกุล" HeaderStyle-HorizontalAlign="Center"
                                                    Width="24%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="SPOSITION" Caption="ตำแหน่ง" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                                    Width="17%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="PHONE" Caption="หมายเลขโทรศัพท์หลัก" VisibleIndex="3"
                                                    HeaderStyle-HorizontalAlign="Center" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="PHONE2" Caption="หมายเลขโทรศัพท์รอง" VisibleIndex="4"
                                                    HeaderStyle-HorizontalAlign="Center" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="EMAIL" Caption="e-mail" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                                    Width="18%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="11%" CellStyle-Cursor="hand" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="รูปภาพ">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblData" ClientVisible="false">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxTextBox ID="txtFilePathgvwsign" runat="server" ClientInstanceName="txtFilePathgvwsign"
                                                            Text='<%# Eval("SPATH") %>' ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtFileNamegvwsign" runat="server" ClientEnabled="False" ClientInstanceName="txtFileNamegvwsign"
                                                            ClientVisible="false" Text='<%# Eval("SSYSFILENAME") %>' Width="200px" CssClass="dxeLineBreakFix">
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxButton ID="btnViewgvwsign" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientEnabled="False" ClientInstanceName="btnViewgvwsign" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnDelFilegvwsign" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientEnabled="False" ClientInstanceName="btnDelFilegvwsign" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxLabel ID="lblShow" runat="server" Text="." ForeColor="White">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="10%" Caption="การจัดการ" VisibleIndex="9">
                                                    <DataItemTemplate>
                                                        <dx:ASPxButton runat="server" ID="hplEdit" ToolTip="ปรับปรุง/แก้ไขรายการ" CausesValidation="False"
                                                            AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind"
                                                            Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                            <Image Width="25px" Height="25px" Url="Images/26466_Settings.png" />
                                                            <ClientSideEvents Click="function (s, e) {gvwsign.PerformCallback('editgvwsign;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton runat="server" ID="hplDelete" ToolTip="ลบรายการ" CausesValidation="False"
                                                            AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind"
                                                            Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                            <ClientSideEvents Click="function (s, e) {gvwsign.PerformCallback('deletegvwsign;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                            <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <Templates>
                                                <EditForm>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                ชื่อ - นามสกุล
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtName" Width="200px">
                                                                    <ValidationSettings EnableCustomValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true" ValidationGroup="add2">
                                                                        <RequiredField ErrorText="กรุณาระบุชื่อและนามสกุลของผู้ลงนาม" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ตำแหน่ง
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtPosition" Width="200px">
                                                                    <ValidationSettings EnableCustomValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true" ValidationGroup="add2">
                                                                        <RequiredField ErrorText="กรุณาระบุตำแหน่งของผู้ลงนาม" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                หมายเลขโทรศัพท์หลัก
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtTel" Width="200px">
                                                                    <ValidationSettings EnableCustomValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true" ValidationGroup="add2">
                                                                        <RequiredField ErrorText="กรุณาระบุเบอร์โทรศัพท์ของผู้ลงนาม" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                หมายเลขโทรศัพท์รอง
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtTel2" Width="200px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                e-mail
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtEmail" Width="200px">
                                                                    <ValidationSettings EnableCustomValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true" ValidationGroup="add2">
                                                                        <RequiredField ErrorText="กรุณาระบุE-mailของผู้ลงนาม" IsRequired="true" />
                                                                        <RegularExpression ErrorText="ใส่แบบฟอร์มอีเมลล์ไม่ถูกต้อง" ValidationExpression="<%$ Resources:ValidationResource, Valid_Email %>" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                รูปภาพ
                                                            </td>
                                                            <td>
                                                                <dx:ASPxUploadControl ID="uploader5" runat="server" ClientInstanceName="uploader5"
                                                                    CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="uploadgvw5_FileUploadComplete"
                                                                    Width="200px">
                                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MultiSelectionErrorText="Attention!  

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath5.SetValue((e.callbackData+'').split('|')[0]);txtFileName5.SetValue((e.callbackData+'').split('|')[1]);txtTruePath5.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename5.SetValue((e.callbackData+'').split('|')[3]);VisibleControl5();} }"
                                                                        TextChanged="function(s,e){uploader5.Upload();}" />
                                                                    <BrowseButton Text="แนบไฟล์">
                                                                    </BrowseButton>
                                                                </dx:ASPxUploadControl>
                                                                <dx:ASPxTextBox ID="txtFilePath5" runat="server" ClientInstanceName="txtFilePath5"
                                                                    ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtFileName5" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName5"
                                                                    ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtTruePath5" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath5"
                                                                    ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtSysfilename5" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilename5"
                                                                    ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="chkUpload5" runat="server" ClientInstanceName="chkUpload5" CssClass="dxeLineBreakFix"
                                                                    ForeColor="White" Width="1px">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxButton ID="btnView5" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnView5" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath5.GetValue()+txtSysfilename5.GetValue());}" />
                                                                    <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <dx:ASPxButton ID="btnDelFile5" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnDelFile5" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function (s, e) { gvwsign.PerformCallback('deleteEditform;'+ txtFilePath5.GetValue() +';1');}" />
                                                                    <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%--คอนโทรเก็บค่าใช้เช็คว่าเป็นEdit หรือ Save--%>
                                                                <dx:ASPxTextBox runat="server" ID="txtLINE_NO" Width="100px" ClientVisible="false">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%" align="right" colspan="2">
                                                                <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submitsigh">
                                                                    <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add2')) return false;gvwsign.PerformCallback('Savegvw;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td style="width: 50%">
                                                                <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                                                    <ClientSideEvents Click="function (s, e) {gvwsign.PerformCallback('Cancelgvw');}" />
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditForm>
                                            </Templates>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <dx:ASPxButton runat="server" ID="btnNew" AutoPostBack="false" ToolTip="เพิ่มรายชื่อ"
                                                        CausesValidation="False" EnableTheming="False" EnableDefaultAppearance="False"
                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                        <ClientSideEvents Click="function(s,e){gvwsign.PerformCallback('New')}" />
                                                        <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td valign="middle">
                                                    เพิ่มรายชื่อ
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr  style="display:none">
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td width="23%" align="right">
                                                    สถานะการใช้งาน :
                                                </td>
                                                <td width="77%">
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal"
                                                        ClientInstanceName="rblStatus" ReadOnly="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="อนุญาต" Value="1" />
                                                            <dx:ListEditItem Text="ระงับ" Value="0" />
                                                            <dx:ListEditItem Text="ยกเลิกใช้งาน" Value="2" />
                                                        </Items>
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                            SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาเลือกสถานะการใช้งาน" IsRequired="true" />
                                                        </ValidationSettings>
                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){ VisibleControlRadio();}" />
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="Typedefault">
                                                <td align="right">
                                                    สาเหตุการอนุญาติ :
                                                </td>
                                                <td>
                                                    <dx:ASPxMemo runat="server" ID="txtConfirm" Width="350px" Height="100px" CssClass="dxeLineBreakFix"
                                                        ClientInstanceName="txtConfirm" SkinID="_memoRead">
                                                        <%--<ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุสาเหตุการอนุญาต" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                            <%-- <tr runat="server" id="Type">
                                                <td>
                                                    <table id="Table20" width="100%" runat="server">
                                                        <tr>
                                                            <td width="22%" align="right">ประเภทการระงับ : </td>
                                                            <td width="78%">
                                                                <dx:ASPxComboBox runat="server" ID="cboStatus" DataSourceID="sdsStatus" ValueField="STATUS_ID"
                                                                    TextField="STATUS_TYPE">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาเลือกสาเหตุที่ระงับ" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sdsStatus" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                    SelectCommand="SELECT &quot;STATUS_ID&quot;, &quot;STATUS_TYPE&quot; FROM &quot;TEMPLOYEESTATUS&quot;">
                                                                </asp:SqlDataSource>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>--%>
                                            <tr runat="server" id="status">
                                                <td align="right">
                                                    สาเหตุที่ระงับจากระบบ SAP :
                                                </td>
                                                <td>
                                                    <dx:ASPxMemo runat="server" ID="txtComment" Width="350px" Height="100px" CssClass="dxeLineBreakFix"
                                                        SkinID="_memoRead">
                                                        <%--<ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุสาเหตุที่ระงับ" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                            <%--  <tr runat="server" id="Type2">
                                                <td>
                                                    <table id="Table22" width="100%" runat="server">
                                                        <tr>
                                                            <td width="22%" align="right">ประเภทการยกเลิกใช้งาน : </td>
                                                            <td width="78%">
                                                                <dx:ASPxComboBox runat="server" ID="cboStatus2" DataSourceID="sdsStatusCancel" TextField="STATUS_TYPECANCEL"
                                                                    ValueField="STATUS_IDCANCEL">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาเลือกประเภทการยกเลิกใช้งาน" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sdsStatusCancel" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                    SelectCommand="SELECT &quot;STATUS_IDCANCEL&quot;, &quot;STATUS_TYPECANCEL&quot; FROM &quot;TEMPLOYEESTATUSCANCEL&quot;">
                                                                </asp:SqlDataSource>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>--%>
                                            <tr runat="server" id="status2">
                                                <td align="right">
                                                    สาเหตุที่ยกเลิกใช้งานจากระบบ SAP :
                                                </td>
                                                <td>
                                                    <dx:ASPxMemo runat="server" ID="txtstatus2" Width="350px" Height="100px" CssClass="dxeLineBreakFix"
                                                        SkinID="_memoRead">
                                                        <%--<ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุสาเหตุที่ยกเลิกใช้งาน" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="right">
                                                    รายละเอียดการลงนาม :
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxMemo runat="server" ID="txtDescriptionSigh" Width="300px" Height="75px" SkinID="_memoRead">
                                                                </dx:ASPxMemo>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxCheckBox runat="server" ID="chkNotfill" Text="ไม่เติมเต็มข้อมูล" Visible="false">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" runat="server">
                                <tr>
                                    <td bgcolor="#FFFFFF" align="center">
                                        <dx:ASPxButton ID="btnSubmitgvw" runat="server" Text="บันทึก และส่งข้อมูล" CssClass="dxeLineBreakFix" AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnCancelgvw" runat="server" SkinID="_back" CssClass="dxeLineBreakFix"  AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('cancel'); }" />
                                        </dx:ASPxButton>
                                        <%--  <dx:ASPxButton ID="btnApprove" runat="server" Text="อนุมัติ" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); return false;  xcpn.PerformCallback('approve'); }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnNotApprove" runat="server" Text="ปฏิเสธ" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function (s, e) {  xcpn.PerformCallback('noapprove'); }" />
                                        </dx:ASPxButton>--%>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
