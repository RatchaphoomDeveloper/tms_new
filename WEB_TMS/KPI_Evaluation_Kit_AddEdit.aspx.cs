﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class KPI_Evaluation_Kit_AddEdit : PageBase
{
    #region + View State +
    private bool IsUse
    {
        get
        {
            if ((bool)ViewState["IsUse"] != null)
                return (bool)ViewState["IsUse"];
            else
                return false;
        }
        set
        {
            ViewState["IsUse"] = value;
        }
    }
    private int TopicHeaderID
    {
        get
        {
            if ((int)ViewState["TopicHeaderID"] != null)
                return (int)ViewState["TopicHeaderID"];
            else
                return 0;
        }
        set
        {
            ViewState["TopicHeaderID"] = value;
        }
    }

    private int IndexEdit
    {
        get
        {
            if ((int)ViewState["IndexEdit"] != null)
                return (int)ViewState["IndexEdit"];
            else
                return 0;
        }
        set
        {
            ViewState["IndexEdit"] = value;
        }
    }

    private DataTable dtTopic
    {
        get
        {
            if ((DataTable)ViewState["dtTopic"] != null)
                return (DataTable)ViewState["dtTopic"];
            else
                return null;
        }
        set
        {
            ViewState["dtTopic"] = value;
        }
    }

    private DataTable dtFormula
    {
        get
        {
            if ((DataTable)ViewState["dtFormula"] != null)
                return (DataTable)ViewState["dtFormula"];
            else
                return null;
        }
        set
        {
            ViewState["dtFormula"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            if (Session["chkurl"] != null && Session["chkurl"] == "1")
            {
                cmdSave.Visible = false;
                cmdAdd.Visible = false;
            }
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                cmdAdd.Enabled = false;
                cmdFormula.Enabled = false;
                cmdSave.Enabled = false;
                cmdSaveEdit.Enabled = false;
                cmdCancelEdit.Enabled = false;

                dgvTopic.Columns[1].Visible = false;
                dgvTopic.Columns[2].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialForm()
    {
        try
        {
            IndexEdit = -1;
            IsUse = false;

            dtTopic = new DataTable();
            dtTopic.Columns.Add("FORMULA_ID");
            dtTopic.Columns.Add("METHOD_ID");
            dtTopic.Columns.Add("TOPIC_NAME");
            dtTopic.Columns.Add("FORMULA_NAME");
            dtTopic.Columns.Add("LEVEL1_START");
            dtTopic.Columns.Add("LEVEL1_END");
            dtTopic.Columns.Add("LEVEL2_START");
            dtTopic.Columns.Add("LEVEL2_END");
            dtTopic.Columns.Add("LEVEL3_START");
            dtTopic.Columns.Add("LEVEL3_END");
            dtTopic.Columns.Add("LEVEL4_START");
            dtTopic.Columns.Add("LEVEL4_END");
            dtTopic.Columns.Add("LEVEL5_START");
            dtTopic.Columns.Add("LEVEL5_END");
            dtTopic.Columns.Add("WEIGHT");

            this.LoadMethod();
            this.LoadFormula();

            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                txtFormName.Enabled = false;

                var decryptedBytes = MachineKey.Decode(Request.QueryString["TOPIC_HEADER_ID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                TopicHeaderID = int.Parse(decryptedValue);

                DataTable dt = new DataTable();
                dt = KPIBLL.Instance.KPIFormSelectBLL(TopicHeaderID);

                //เชคว่าแบบประเมินนี้ ได้ถูกนำไปใช้ และบันทึก หรือ แจ้งผู้ขนส่งรึยัง
                DataTable dtCheck = KPIBLL.Instance.KPICheckBLL(TopicHeaderID);
                if (dtCheck.Rows.Count > 0)
                {
                    if (!string.Equals(dtCheck.Rows[0]["COUNT"].ToString(), string.Empty))
                    {
                        int count = int.Parse(dtCheck.Rows[0]["COUNT"].ToString());
                        if (count > 0)
                        {
                            IsUse = true;
                            mpConfirmSave.TextDetail = "เนื่องจากมีการแก้ไขช่วงคะแนน <br /> ขอให้ทำการตรวจสอบข้อมูล KPI ที่แจ้งผลผู้ขนส่งไปแล้ว  ว่าครบถ้วนถูกต้องหรือไม่";
                        }
                    }
                }

                if (dt.Rows.Count > 0)
                {
                    txtFormName.Text = dt.Rows[0]["HEADER_NAME"].ToString();
                    txtDetail.Text = dt.Rows[0]["HEADER_DETAIL"].ToString();
                    radStatus.SelectedValue = dt.Rows[0]["IS_ACTIVE"].ToString();

                    dt.Columns.Remove("HEADER_NAME");
                    dt.Columns.Remove("HEADER_DETAIL");
                    dt.Columns.Remove("IS_ACTIVE");

                    dtTopic = dt.Copy();
                    GridViewHelper.BindGridView(ref dgvTopic, dtTopic);
                    this.DisableAdd();
                }
            }
            else
                TopicHeaderID = 0;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadMethod()
    {
        try
        {
            DataTable dtMethod = KPIBLL.Instance.LoadMethodBLL(" AND IS_ACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlMethod, dtMethod, "METHOD_ID", "METHOD_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadFormula()
    {
        try
        {
            dtFormula = KPIBLL.Instance.LoadFormulaBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlFormula, dtFormula, "FORMULA_ID", "FORMULA_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();

            KPIBLL.Instance.KPIFormSaveBLL(TopicHeaderID
                                         , txtFormName.Text.Trim()
                                         , txtDetail.Text.Trim()
                                         , int.Parse(radStatus.SelectedValue)
                                         , dtTopic
                                         , int.Parse(Session["UserID"].ToString()));

            alertSuccess("บันทึกข้อมูลเรียบร้อย", "KPI_Evaluation_Kit.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (string.Equals(txtFormName.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ชื่อแบบฟอร์ม");

            if ((dtTopic == null) || dtTopic.Rows.Count == 0)
                throw new Exception("ไม่มีข้อมูล หัวข้อประเมิน");

            decimal WeightSum = 0;
            for (int i = 0; i < dtTopic.Rows.Count; i++)
            {
                WeightSum += decimal.Parse(dtTopic.Rows[i]["WEIGHT"].ToString());
            }
            if (WeightSum == 0 || WeightSum > 100)
                throw new Exception("ผลรวมของ Weight ต้องมีค่ามากกว่า 0 และ ไม่เกิน 100");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Evaluation_Kit.aspx");
    }
    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateAddTopic();

            dtTopic.Rows.Add(ddlFormula.SelectedValue
                               , ddlMethod.SelectedValue
                               , txtTopicName.Text.Trim()
                               , ddlFormula.SelectedItem.ToString()
                               , txtLevel1Start.Text.Trim()
                               , txtLevel1End.Text.Trim()
                               , txtLevel2Start.Text.Trim()
                               , txtLevel2End.Text.Trim()
                               , txtLevel3Start.Text.Trim()
                               , txtLevel3End.Text.Trim()
                               , txtLevel4Start.Text.Trim()
                               , txtLevel4End.Text.Trim()
                               , txtLevel5Start.Text.Trim()
                               , txtLevel5End.Text.Trim()
                               , txtWeight.Text.Trim());

            GridViewHelper.BindGridView(ref dgvTopic, dtTopic);
            this.DisableAdd();
            this.ClearScreenTopic();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ClearScreenTopic()
    {
        try
        {
            txtTopicName.Text = string.Empty;
            ddlMethod.SelectedIndex = 0;
            ddlFormula.SelectedIndex = 0;
            txtFrequency.Text = string.Empty;
            txtLevel1Start.Text = string.Empty;
            txtLevel1End.Text = string.Empty;
            txtLevel2Start.Text = string.Empty;
            txtLevel2End.Text = string.Empty;
            txtLevel3Start.Text = string.Empty;
            txtLevel3End.Text = string.Empty;
            txtLevel4Start.Text = string.Empty;
            txtLevel4End.Text = string.Empty;
            txtLevel5Start.Text = string.Empty;
            txtLevel5End.Text = string.Empty;
            txtWeight.Text = string.Empty;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateAddTopic()
    {
        try
        {
            if (string.Equals(txtTopicName.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน หัวข้อประเมิน");

            if (ddlMethod.SelectedIndex < 1)
                throw new Exception("กรุณาเลือก วิธีคำนวณผลรวม");

            if (ddlFormula.SelectedIndex < 1)
                throw new Exception("กรุณาเลือก สูตรการคำนวณ");

            if (string.Equals(txtWeight.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน Weight");

            decimal tmp;

            if (!decimal.TryParse(txtWeight.Text.Trim(), out tmp))
                throw new Exception("กรุณาป้อน Weight เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");
            else
            {
                if (decimal.Parse(txtWeight.Text.Trim()) < 0)
                    throw new Exception("Weight ต้องมีค่ามากกว่า หรือเท่ากับ ศูนย์");
                this.CountDigit(txtWeight, "Weight ต้องเป็น ทศนิยม 3 ตำแหน่ง");
            }

            if (!string.Equals(txtLevel1Start.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel1Start.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 1 (เริ่มต้น) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel1Start, "คำแนนช่วงที่ 1 (เริ่มต้น) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel1End.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 1 (สิ้นสุด)");
            }

            if (!string.Equals(txtLevel1End.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel1End.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 1 (สิ้นสุด) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel1End, "คำแนนช่วงที่ 1 (สิ้นสุด) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel1Start.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 1 (เริ่มต้น)");

                if (decimal.Parse(txtLevel1End.Text.Trim()) < decimal.Parse(txtLevel1Start.Text.Trim()))
                    throw new Exception("คำแนนช่วงที่ 1 (สิ้นสุด) ห้ามน้อยกว่า คำแนนช่วงที่ 1 (เริ่มต้น)");
            }

            if (!string.Equals(txtLevel2Start.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel2Start.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 2 (เริ่มต้น) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel2Start, "คำแนนช่วงที่ 2 (เริ่มต้น) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel2End.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 2 (สิ้นสุด)");
            }

            if (!string.Equals(txtLevel2End.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel2End.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 2 (สิ้นสุด) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel2End, "คำแนนช่วงที่ 2 (สิ้นสุด) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel2Start.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 2 (เริ่มต้น)");

                if (decimal.Parse(txtLevel1End.Text.Trim()) < decimal.Parse(txtLevel1Start.Text.Trim()))
                    throw new Exception("คำแนนช่วงที่ 2 (สิ้นสุด) ห้ามน้อยกว่า คำแนนช่วงที่ 2 (เริ่มต้น)");
            }

            if (!string.Equals(txtLevel3Start.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel3Start.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 3 (เริ่มต้น) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel3Start, "คำแนนช่วงที่ 3 (เริ่มต้น) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel3End.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 3 (สิ้นสุด)");
            }

            if (!string.Equals(txtLevel3End.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel3End.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 3 (สิ้นสุด) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel3End, "คำแนนช่วงที่ 3 (สิ้นสุด) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel3Start.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 3 (เริ่มต้น)");

                if (decimal.Parse(txtLevel1End.Text.Trim()) < decimal.Parse(txtLevel1Start.Text.Trim()))
                    throw new Exception("คำแนนช่วงที่ 3 (สิ้นสุด) ห้ามน้อยกว่า คำแนนช่วงที่ 3 (เริ่มต้น)");
            }

            if (!string.Equals(txtLevel4Start.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel4Start.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 4 (เริ่มต้น) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel4Start, "คำแนนช่วงที่ 4 (เริ่มต้น) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel4End.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 4 (สิ้นสุด)");
            }

            if (!string.Equals(txtLevel4End.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel4End.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 4 (สิ้นสุด) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel4End, "คำแนนช่วงที่ 4 (สิ้นสุด) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel4Start.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 4 (เริ่มต้น)");

                if (decimal.Parse(txtLevel1End.Text.Trim()) < decimal.Parse(txtLevel1Start.Text.Trim()))
                    throw new Exception("คำแนนช่วงที่ 4 (สิ้นสุด) ห้ามน้อยกว่า คำแนนช่วงที่ 4 (เริ่มต้น)");
            }

            if (!string.Equals(txtLevel5Start.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel5Start.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 5 (เริ่มต้น) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel5Start, "คำแนนช่วงที่ 5 (เริ่มต้น) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel5End.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 5 (สิ้นสุด)");
            }

            if (!string.Equals(txtLevel5End.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtLevel5End.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 5 (สิ้นสุด) เป็นตัวเลขเท่านั้น (ทศนิยม 3 ตำแหน่ง)");

                this.CountDigit(txtLevel5End, "คำแนนช่วงที่ 5 (สิ้นสุด) ต้องเป็น ทศนิยม 3 ตำแหน่ง");

                if (string.Equals(txtLevel5Start.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน คำแนนช่วงที่ 5 (เริ่มต้น)");

                if (decimal.Parse(txtLevel1End.Text.Trim()) < decimal.Parse(txtLevel1Start.Text.Trim()))
                    throw new Exception("คำแนนช่วงที่ 5 (สิ้นสุด) ห้ามน้อยกว่า คำแนนช่วงที่ 5 (เริ่มต้น)");
            }

            string[] arr = new string[10];
            if (!string.Equals(txtLevel1Start.Text.Trim(), string.Empty))
            {
                arr[this.CountArray(arr)] = txtLevel1Start.Text.Trim();
                arr[this.CountArray(arr)] = txtLevel1End.Text.Trim();
            }
            if (!string.Equals(txtLevel2Start.Text.Trim(), string.Empty))
            {
                arr[this.CountArray(arr)] = txtLevel2Start.Text.Trim();
                arr[this.CountArray(arr)] = txtLevel2End.Text.Trim();
            }
            if (!string.Equals(txtLevel3Start.Text.Trim(), string.Empty))
            {
                arr[this.CountArray(arr)] = txtLevel3Start.Text.Trim();
                arr[this.CountArray(arr)] = txtLevel3End.Text.Trim();
            }
            if (!string.Equals(txtLevel4Start.Text.Trim(), string.Empty))
            {
                arr[this.CountArray(arr)] = txtLevel4Start.Text.Trim();
                arr[this.CountArray(arr)] = txtLevel4End.Text.Trim();
            }
            if (!string.Equals(txtLevel5Start.Text.Trim(), string.Empty))
            {
                arr[this.CountArray(arr)] = txtLevel5Start.Text.Trim();
                arr[this.CountArray(arr)] = txtLevel5End.Text.Trim();
            }

            decimal Num = 0;
            decimal NumNext = 0;

            if (this.CountArray(arr) > 2)
            {
                if (decimal.Parse(arr[0]) < decimal.Parse(arr[2]))
                {
                    for (int i = 1; i < this.CountArray(arr) - 1; i = i + 2)
                    {
                        Num = decimal.Parse(arr[i]);
                        NumNext = decimal.Parse(arr[i + 1]);

                        if (Num >= NumNext)
                            throw new Exception("ช่วงคะแนนทับซ้อนกัน");
                    }
                }
                else if (decimal.Parse(arr[0]) > decimal.Parse(arr[2]))
                {
                    for (int i = 0; i < this.CountArray(arr) - 2; i = i + 2)
                    {
                        Num = decimal.Parse(arr[i]);
                        NumNext = decimal.Parse(arr[i + 3]);

                        if (Num <= NumNext)
                            throw new Exception("ช่วงคะแนนทับซ้อนกัน");
                    }
                }
                else if (decimal.Parse(arr[0]) == decimal.Parse(arr[2]))
                    throw new Exception("ช่วงคะแนนทับซ้อนกัน");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int CountArray(string[] arr)
    {
        try
        {
            int Counter = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != null)
                    Counter++;
            }

            return Counter;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void CountDigit(TextBox txt, string ErrorMessage)
    {
        try
        {
            string[] s = txt.Text.Trim().Split('.');
            if (s.Length > 1)
            {
                if (s[1].Length > 3)
                    throw new Exception(ErrorMessage);
            }

            txt.Text = decimal.Parse(txt.Text.Trim()).ToString("0.000");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdFormula_Click(object sender, EventArgs e)
    {
        MsgAlert.OpenForm("KPI_Formula.aspx", Page);
    }
    protected void ddlFormula_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlFormula.SelectedIndex < 1)
                txtFrequency.Text = string.Empty;
            else
                txtFrequency.Text = dtFormula.Rows[ddlFormula.SelectedIndex]["FREQUENCY"].ToString();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTopic_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
    {
        try
        {
            dtTopic.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvTopic, dtTopic);
            this.DisableAdd();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTopic_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (string.Equals(e.CommandName, "EditData"))
            {
                IndexEdit = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;

                txtTopicName.Text = dtTopic.Rows[IndexEdit]["TOPIC_NAME"].ToString();
                ddlMethod.SelectedValue = dtTopic.Rows[IndexEdit]["METHOD_ID"].ToString();
                ddlFormula.SelectedValue = dtTopic.Rows[IndexEdit]["FORMULA_ID"].ToString();
                ddlFormula_SelectedIndexChanged(null, null);
                txtLevel1Start.Text = dtTopic.Rows[IndexEdit]["LEVEL1_START"].ToString();
                txtLevel1End.Text = dtTopic.Rows[IndexEdit]["LEVEL1_END"].ToString();
                txtLevel2Start.Text = dtTopic.Rows[IndexEdit]["LEVEL2_START"].ToString();
                txtLevel2End.Text = dtTopic.Rows[IndexEdit]["LEVEL2_END"].ToString();
                txtLevel3Start.Text = dtTopic.Rows[IndexEdit]["LEVEL3_START"].ToString();
                txtLevel3End.Text = dtTopic.Rows[IndexEdit]["LEVEL3_END"].ToString();
                txtLevel4Start.Text = dtTopic.Rows[IndexEdit]["LEVEL4_START"].ToString();
                txtLevel4End.Text = dtTopic.Rows[IndexEdit]["LEVEL4_END"].ToString();
                txtLevel5Start.Text = dtTopic.Rows[IndexEdit]["LEVEL5_START"].ToString();
                txtLevel5End.Text = dtTopic.Rows[IndexEdit]["LEVEL5_END"].ToString();
                txtWeight.Text = dtTopic.Rows[IndexEdit]["WEIGHT"].ToString();

                cmdAdd.Visible = false;
                cmdSaveEdit.Visible = true;
                cmdCancelEdit.Visible = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdCancelEdit_Click(object sender, EventArgs e)
    {
        try
        {
            IndexEdit = -1;
            cmdAdd.Visible = true;
            cmdSaveEdit.Visible = false;
            cmdCancelEdit.Visible = false;

            this.ClearScreenTopic();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSaveEdit_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateAddTopic();

            dtTopic.Rows[IndexEdit]["FORMULA_ID"] = ddlFormula.SelectedValue;
            dtTopic.Rows[IndexEdit]["METHOD_ID"] = ddlMethod.SelectedValue;
            dtTopic.Rows[IndexEdit]["TOPIC_NAME"] = txtTopicName.Text.Trim();
            dtTopic.Rows[IndexEdit]["FORMULA_NAME"] = ddlFormula.SelectedItem.ToString();
            dtTopic.Rows[IndexEdit]["LEVEL1_START"] = txtLevel1Start.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL1_END"] = txtLevel1End.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL2_START"] = txtLevel2Start.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL2_END"] = txtLevel2End.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL3_START"] = txtLevel3Start.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL3_END"] = txtLevel3End.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL4_START"] = txtLevel4Start.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL4_END"] = txtLevel4End.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL5_START"] = txtLevel5Start.Text.Trim();
            dtTopic.Rows[IndexEdit]["LEVEL5_END"] = txtLevel5End.Text.Trim();
            dtTopic.Rows[IndexEdit]["WEIGHT"] = txtWeight.Text.Trim();

            IndexEdit = -1;
            cmdAdd.Visible = true;
            cmdSaveEdit.Visible = false;
            cmdCancelEdit.Visible = false;

            GridViewHelper.BindGridView(ref dgvTopic, dtTopic);
            this.DisableAdd();
            this.ClearScreenTopic();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void DisableAdd()
    {
        try
        {
            if (IsUse)
            {
                cmdAdd.Visible = false;
                ImageButton imgDelete = new ImageButton();

                for (int i = 0; i < dgvTopic.Rows.Count; i++)
                {
                    imgDelete = (ImageButton)dgvTopic.Rows[i].FindControl("imgDelete");
                    if (imgDelete != null)
                        imgDelete.Visible = false;
                }

                txtTopicName.Enabled = false;
                ddlMethod.Enabled = false;
                ddlFormula.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}