﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;

public partial class Information_standard : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    static List<sInformation> _list = new List<sInformation>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw.CellEditorInitialize += new ASPxGridViewEditorEventHandler(gvw_CellEditorInitialize);
        gvw.StartRowEditing += new DevExpress.Web.Data.ASPxStartRowEditingEventHandler(gvw_StartRowEditing);
        gvw.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvw_HtmlRowPrepared);
        if (!IsPostBack)
        {

            #region เช็ค Permission
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("49", "R", "เปิดดูข้อมูลหน้า ข้อมูล Average Standard Trip", "");
          
            #endregion

            cboTruckType.DataBind();
            cboTruckType.SelectedIndex = 0;

            _list.RemoveAll(o => 1 == 1);

            DataTable dt = CommonFunction.Get_Data(conn, @" select SPRODUCTTYPENAME as FieldName, SPRODUCTTYPEID AS FieldValue  from TPRODUCTTYPE ");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    _list.Add(new sInformation
                    {
                        SPRODUCTTYPENAME = dt.Rows[i]["FieldName"] + ""
                    });
                }
            }
        }
        //cboTruckType.DataSource = sds;
        //cboTruckType.Text =
        //cboTruckType.ValueField = "SCARTYPEID";
        //cboTruckType.TextField = "SCARTYPENAME";

        Listgvw();

    }

    void gvw_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        string Chkin = e.GetValue("CHECKIN") + "";
        if (Chkin == "1")
        {
            e.Row.BackColor = System.Drawing.Color.White;
        }
        else
        {
            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
        }
    }

    void gvw_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        string ProID = gvw.GetRowValuesByKeyValue(e.EditingKeyValue, "STERMINALID") + "";

        #region checkว่าเคยเข้ามาดุข้อมูลไหม
        string strCheckdata = @"SELECT STERMINALID,CHECKIN
                                            FROM TTERMINAL WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(ProID) + @"' AND CHECKIN ='1' ";


        DataTable dtchk = CommonFunction.Get_Data(conn, strCheckdata);
        if (dtchk.Rows.Count > 0)
        {

        }
        else
        {
            string strCompany = @"UPDATE TTERMINAL
                                         SET CHECKIN=:cCheckIn
                                         WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(ProID) + "'";
            using (OracleConnection con = new OracleConnection(conn))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                else
                {

                }
                DataTable dt = CommonFunction.Get_Data(con, strCheckdata);
                if (dt.Rows.Count > 0)
                {

                }
                else
                {
                    //AddhistoryVendor(ProID);
                    using (OracleCommand com1 = new OracleCommand(strCompany, con))
                    {
                        com1.Parameters.Clear();
                        com1.Parameters.Add(":cCheckIn", OracleType.Char).Value = "1";
                        com1.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion
    }

    void gvw_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {

        DataTable dt = CommonFunction.Get_Data(conn, @" select SPRODUCTTYPENAME as FieldName, SPRODUCTTYPEID AS FieldValue from TPRODUCTTYPE ORDER BY NORDER");

        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {

            string field = dt.Rows[i]["FieldValue"] + "";

            if (e.Column.FieldName == field)
            {
                ASPxTextBox editor = e.Editor as ASPxTextBox;
                editor.ValidationSettings.Display = Display.Dynamic;
                editor.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                editor.ValidationSettings.RegularExpression.ValidationExpression = "(^\\d*\\.?\\d*[0-9]+\\d*$)|(^[0-9]+\\d*\\.\\d*$)";
                editor.ValidationSettings.ErrorText = "เฉพาะตัวเลขเท่านั้น";
                editor.ValidationSettings.ValidationGroup = "add";

            }
        }

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string[] param = e.Parameter.Split(';');
            int num = 0;
            switch (param[0])
            {
                case "edit": gvw.AddNewRow();
                    break;

                case "Search": Listgvw();
                    break;

                case "CancelSearch": txtSearch.Text = "";
                    cboTruckType.SelectedIndex = 0;
                    Listgvw();
                    break;
                case "viewHistoryInformation":
                    int index = int.TryParse(param[1] + "", out num) ? num : 0;
                    dynamic ID = gvw.GetRowValues(index, "STERMINALID");

                    xcpn.JSProperties["cpRedirectTo"] = "Information_standard_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + ID + ";" + cboTruckType.SelectedItem.Value + ""));
                    break;
                case "chagevalidate": Listgvw();
                    break;
            }
        }
    }

    protected void gvw_Init(object sender, EventArgs e)
    {
        //Listgvw();
    }

    protected void gvwSum_Init(object sender, EventArgs e)
    {

        DataTable dt = CommonFunction.Get_Data(conn, @" select SPRODUCTTYPENAME as FieldName, SPRODUCTTYPEID AS FieldValue from TPRODUCTTYPE ORDER BY NORDER");

        GridViewBandColumn sGridViewBandColumn = new GridViewBandColumn("จำนวนเที่ยวการขนส่งต่อวัน(เที่ยวต่อวันต่อคัน)");
        sGridViewBandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {
            GridViewDataColumn sGridViewDataColumn = new GridViewDataColumn((dt.Rows[i]["FieldValue"] + "").Replace("\r\n", ""), dt.Rows[i]["FieldName"] + "");
            sGridViewDataColumn.Width = Unit.Percentage(5);
            sGridViewDataColumn.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
            sGridViewDataColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            sGridViewDataColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            sGridViewBandColumn.Columns.Insert(i, sGridViewDataColumn);


        }


        gvw.Columns.Insert(2, sGridViewBandColumn);
    }

    void Listgvw()
    {
        string Condition = "";
        string Condition2 = "";
        string Condition3 = "";
        string Condition4 = "";
        string TruckType = cboTruckType.Value + "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Condition = " AND NVL(TERM.STERMINALID,'')||NVL(TERM.SABBREVIATION,'') LIKE UPPER('%" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "%') OR NVL(TERM.STERMINALID,'')||NVL(TERM.SABBREVIATION,'') LIKE LOWER('%" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "%')";
        }
        if (!string.IsNullOrEmpty(cboTruckType.Value + ""))
        {
            Condition2 = " AND SCARTYPEID = " + CommonFunction.ReplaceInjection(cboTruckType.Value + "") + "";
        }
        else
        {
            // Condition = " AND SCARTYPEID = null";
        }


        DataTable dt2 = CommonFunction.Get_Data(conn, @" select SPRODUCTTYPENAME as FieldName,
                                                       SPRODUCTTYPEID AS FieldValue from TPRODUCTTYPE");
        if (dt2.Rows.Count > 0)
        {
            for (int i = 0; i <= dt2.Rows.Count - 1; i++)
            {
                Condition3 += "," + dt2.Rows[i]["FieldValue"] + "" + ".VALUESTTERMINAL AS " + dt2.Rows[i]["FieldValue"] + "";
                Condition4 += @"LEFT JOIN (SELECT * FROM TINFORMATION_STABDARD WHERE 1=1   AND SPRODUCTTYPEID='" + dt2.Rows[i]["FieldValue"] + "" + "' AND SCARTYPEID='" + CommonFunction.ReplaceInjection(TruckType) + "') " + dt2.Rows[i]["FieldValue"] + "" + " ON  TERM.STERMINALID = " + dt2.Rows[i]["FieldValue"] + "" + ".STERMINALID ";
            }

        }

        string strsql = @"SELECT TERM.STERMINALID , TERM.SABBREVIATION " + Condition3 + @"
,sDate.DUPDATE,sDate.CHECKIN,sDate.SCARTYPEID
FROM TTERMINAL TERM " + Condition4 + @"
LEFT JOIN (SELECT MAX(DUPDATE) AS DUPDATE,STERMINALID,CHECKIN,SCARTYPEID FROM TINFORMATION_STABDARD WHERE SCARTYPEID='" + CommonFunction.ReplaceInjection(TruckType) + @"' GROUP BY STERMINALID,CHECKIN,SCARTYPEID)sDate
ON sDate.STERMINALID=TERM.STERMINALID 
where SUBSTR(TERM.STERMINALID,1,1) in ('2','5','8')" + Condition + "ORDER BY sDate.DUPDATE DESC NULLS LAST,sDate.CHECKIN DESC NULLS LAST";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);



        gvw.DataSource = dt;
        gvw.DataBind();

    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {

            case "CUSTOMCALLBACK":


                string[] param = e.Args[0].Split(';');

                int index = 0;
                if (!string.IsNullOrEmpty(param[1]))
                {
                    index = int.Parse(param[1]);
                }
                if (CanWrite)
                {
                    switch (param[0])
                    {
                        case "edit": gvw.AddNewRow();

                            break;


                        case "Startedit":
                            gvw.StartEdit(index);
                            //ASPxComboBox cbo = gvw.FindEditRowCellTemplateControl(null, "cboPROD_CATEGORY") as ASPxComboBox;
                            //string Values = gvw.GetRowValues(index, "PROD_CATEGORY") + "";
                            //// cbo.Value = Values;
                            //cbo.Value = cbo.Items.FindByText(Values).Value;
                            break;
                        case "Updateedit":


                            gvw.UpdateEdit();

                            gvw.CancelEdit();

                            break;
                        case "cancel":
                            break;
                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(gvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }

                break;





        }

    }

    protected void gvw_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        dynamic keyValue = gvw.GetRowValues(gvw.EditingRowVisibleIndex, "STERMINALID", "SABBREVIATION");
        string STERMINALID = keyValue[0] + "";
        string SABBREVIATION = keyValue[1] + "";
        //string SCARTYPEID = keyValue[1] + "";


        string Nversion = @"SELECT STERMINALID, NVERSION FROM TINFORMATION_STABDARD_HISTORY
WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"' 
GROUP BY STERMINALID,NVERSION 
ORDER BY NVERSION DESC";

        DataTable dtNversion = CommonFunction.Get_Data(conn, Nversion);

        string nversion = "";
        if (dtNversion.Rows.Count > 0)
        {
            nversion = (int.Parse(dtNversion.Rows[0]["NVERSION"] + "") + 1) + "";
        }

        DataTable dt = CommonFunction.Get_Data(conn, @" select SPRODUCTTYPENAME as FieldName, SPRODUCTTYPEID AS FieldValue  from TPRODUCTTYPE ");


        string USER = CommonFunction.ReplaceInjection(Session["UserID"] + "");
        string SCARTYPEID = CommonFunction.ReplaceInjection(cboTruckType.SelectedItem.Value + "");
        string DATAVALUE = "";
        string NEWDATAVALUE = "";

        //if (DATAVALUE != NEWDATAVALUE)
        //{
        //    if (dt.Rows.Count > 0)
        //    {
        //        LogUser("49", "E", "บันทึกข้อมูล Average Standard Trip", "");
        //    }
        //    else
        //    {

        //    }
        //}
        if (CanWrite)
        {
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dynamic checkdata = gvw.GetRowValues(gvw.EditingRowVisibleIndex, dt.Rows[i]["FieldValue"] + "");
                DATAVALUE += checkdata + "";
                NEWDATAVALUE += e.NewValues[dt.Rows[i]["FieldValue"] + ""] + "";
            }
            if (dt.Rows.Count > 0)
            {


                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    string SPRODUCTTYPEID = CommonFunction.ReplaceInjection(dt.Rows[i]["FieldValue"] + "").Trim();
                    string VALUE = e.NewValues[dt.Rows[i]["FieldValue"] + ""] + "" != "" ? e.NewValues[dt.Rows[i]["FieldValue"] + ""] + "" : "NULL";

                    string chkData = @"SELECT  STERMINALID, SABBREVIATION, DCREATE, SCREATE, DUPDATE, SUPDATE,  CHECKIN, VALUESTTERMINAL, SPRODUCTTYPEID,  SCARTYPEID
                             FROM TINFORMATION_STABDARD
                             WHERE  STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"' 
                             AND  SPRODUCTTYPEID = '" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + @"'
                             AND  SCARTYPEID = " + CommonFunction.ReplaceInjection(SCARTYPEID) + @"
                             ";

                    DataTable dt2 = CommonFunction.Get_Data(conn, chkData);


                    if (dt2.Rows.Count > 0)
                    {

                        //                    string Codition = "";
                        //                    if (VALUE != "NULL")
                        //                    {
                        //                        Codition = " AND  VALUESTTERMINAL = '" + CommonFunction.ReplaceInjection(VALUE) + "'";
                        //                    }

                        //                    string chkDatahis = @"SELECT  STERMINALID, SABBREVIATION, DCREATE, SCREATE, DUPDATE, SUPDATE,  CHECKIN, VALUESTTERMINAL, SPRODUCTTYPEID,  SCARTYPEID
                        //                             FROM TINFORMATION_STABDARD
                        //                             WHERE  STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"' 
                        //                             AND  SPRODUCTTYPEID = '" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + @"'
                        //                             AND  SCARTYPEID = " + CommonFunction.ReplaceInjection(SCARTYPEID) + @"
                        //                            " + Codition + "";
                        //                    DataTable dtchk = CommonFunction.Get_Data(conn, chkDatahis);
                        //                    if (dtchk.Rows.Count > 0)
                        //                    {


                        //                    }
                        //                    else
                        //                    {
                        //เช็คว่าข้อมูลเก่าและข้อมูลใหม่เหมือนกันไหม ถ้าไม่เหมือนกันอับเดทข้อมูลลงเบส
                        if (DATAVALUE != NEWDATAVALUE)
                        {
                            LogUser("49", "E", "บันทึกข้อมูล Average Standard Trip", STERMINALID + "," + SPRODUCTTYPEID + "," + SCARTYPEID);
                            string updsql = @"UPDATE TINFORMATION_STABDARD
                                                                SET VALUESTTERMINAL= " + CommonFunction.ReplaceInjection(VALUE) + @"
                                                                ,SUPDATE= " + CommonFunction.ReplaceInjection(USER) + @"
                                                                ,DUPDATE=sysdate
                                                                WHERE SCARTYPEID ='" + CommonFunction.ReplaceInjection(SCARTYPEID) + @"' 
                                                                AND SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + @"'
                                                                AND STERMINALID ='" + CommonFunction.ReplaceInjection(STERMINALID) + "'";
                            using (OracleConnection con = new OracleConnection(conn))
                            {
                                if (con.State == ConnectionState.Closed)
                                {
                                    con.Open();
                                }
                                else
                                {

                                }

                                Addhistoryinformationstaddard(STERMINALID, SPRODUCTTYPEID, SCARTYPEID, nversion);
                                using (OracleCommand com = new OracleCommand(updsql, con))
                                {

                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                        //}
                    }
                    else
                    {
                        if (DATAVALUE != NEWDATAVALUE)
                        {
                            LogUser("49", "I", "บันทึกข้อมูล Average Standard Trip", STERMINALID + "," + SPRODUCTTYPEID + "," + SCARTYPEID);

                            using (OracleConnection con = new OracleConnection(conn))
                            {
                                if (con.State == ConnectionState.Closed)
                                {
                                    con.Open();
                                }
                                else
                                {

                                }
                                //เช็คว่าเคยมีข้อมูลไหม
                                string chkDataUpdate = @"SELECT  STERMINALID, SABBREVIATION, DCREATE, SCREATE, DUPDATE, SUPDATE,  CHECKIN, VALUESTTERMINAL, SPRODUCTTYPEID,  SCARTYPEID
                             FROM TINFORMATION_STABDARD
                             WHERE  STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"' 
                             AND  SPRODUCTTYPEID = '" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + @"'
                             AND  SCARTYPEID = " + CommonFunction.ReplaceInjection(SCARTYPEID) + @"";

                                DataTable dt3 = CommonFunction.Get_Data(conn, chkDataUpdate);



                                string inssql = @"INSERT INTO TINFORMATION_STABDARD  (SCARTYPEID,SABBREVIATION,SPRODUCTTYPEID ,STERMINALID,  VALUESTTERMINAL ,  DCREATE, SCREATE,CHECKIN) 
                                        VALUES ('" + SCARTYPEID + "','" + SABBREVIATION + "','" + SPRODUCTTYPEID + "','" + STERMINALID + "'," + VALUE + ",sysdate,'" + USER + "','1')  ";

                                //                        string updsql = @"UPDATE TINFORMATION_STABDARD
                                //                                                                SET VALUESTTERMINAL= " + CommonFunction.ReplaceInjection(VALUE) + @"
                                //                                                                ,SUPDATE= " + CommonFunction.ReplaceInjection(USER) + @"
                                //                                                                ,DUPDATE=sysdate
                                //                                                                WHERE SCARTYPEID ='" + CommonFunction.ReplaceInjection(SCARTYPEID) + @"' 
                                //                                                                AND SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + @"'
                                //                                                                AND STERMINALID ='" + CommonFunction.ReplaceInjection(STERMINALID) + "'";
                                //มีอัพเดทไม่มีเก็บใหม่
                                if (dt3.Rows.Count > 0)
                                {
                                    //Addhistoryinformationstaddard(STERMINALID, SPRODUCTTYPEID, SCARTYPEID, nversion);
                                    //using (OracleCommand com = new OracleCommand(updsql, con))
                                    //{

                                    //    com.ExecuteNonQuery();
                                    //}
                                }
                                else
                                {
                                    // Addhistoryinformationstaddard(STERMINALID, SPRODUCTTYPEID, SCARTYPEID);
                                    Addhistoryinformationstaddard(STERMINALID, SPRODUCTTYPEID, SCARTYPEID, nversion);
                                    using (OracleCommand com = new OracleCommand(inssql, con))
                                    {

                                        com.ExecuteNonQuery();
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(gvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
        e.Cancel = true;
        gvw.CancelEdit();
        Listgvw();
    }

    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        var Listw = _list.Where(w => w.SPRODUCTTYPENAME == e.Column.Caption);

        if (Listw.Count() > 0)
        {
            if (string.IsNullOrEmpty(e.Value + ""))
            {

            }
            else
            {
                decimal Ovaluse = Convert.ToDecimal(e.Value);
                if (Ovaluse == 0)
                {
                    e.DisplayText = "";
                }
                else
                {
                    e.DisplayText = Ovaluse.ToString("#,##0.00");
                }
            }
        }
    }

    //    private void Addhistoryinformationstaddard(string STERMINALID, string sSPRODUCTTYPEID, string sSCARTYPEID, string sNVERSION)
    //    {


    //        string senddatatohistory = @"SELECT STERMINALID, SABBREVIATION,VALUESTTERMINAL, DCREATE, SCREATE, DUPDATE, SUPDATE,SCARTYPEID,SPRODUCTTYPEID
    //                                        FROM TINFORMATION_STABDARD
    //                                           WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"'
    //                                              AND SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(sSPRODUCTTYPEID) + @"'
    //                                              AND SCARTYPEID ='" + CommonFunction.ReplaceInjection(sSCARTYPEID) + @"'";

    //        using (OracleConnection con = new OracleConnection(conn))
    //        {

    //            if (con.State == ConnectionState.Closed)
    //            {
    //                con.Open();
    //            }
    //            else
    //            {

    //            }
    //            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

    //            //เช็คว่า TPRODUCT_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
    //            if (dt.Rows.Count > 0)
    //            {


    //                string datatohistory = @"SELECT STERMINALID, SABBREVIATION, DCREATE, SCREATE, DUPDATE, SUPDATE, VALUESTTERMINAL, SPRODUCTTYPEID, SCARTYPEID, NVERSION  
    //                                            FROM TINFORMATION_STABDARD_HISTORY
    //                                            WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"'
    //                                              AND SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(sSPRODUCTTYPEID) + @"'
    //                                              AND SCARTYPEID ='" + CommonFunction.ReplaceInjection(sSCARTYPEID) + @"'
    //                                              ORDER BY NVERSION DESC";
    //                int num = 0;
    //                // string VALUESTTERMINAL = (int.TryParse(dt.Rows[0]["VALUESTTERMINAL"] + "", out num) ? num : 0) + "";
    //                //string SCARTYPEID = dt.Rows[0]["SCARTYPEID"] + "" != "" ? dt.Rows[0]["SCARTYPEID"] + "" : "null";
    //                string SCARTYPEID = sSCARTYPEID;
    //                string SPRODUCTTYPEID = sSPRODUCTTYPEID;
    //                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
    //                string VALUESTTERMINAL = dt.Rows[0]["VALUESTTERMINAL"] + "" != "" ? dt.Rows[0]["VALUESTTERMINAL"] + "" : "NULL";
    //                //เช็คว่า TVendor_signer_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
    //                if (dt2.Rows.Count > 0)
    //                {
    //                    //ถ้าเคยมีข้อมูลแล้ว



    //                    string strQuery = @"INSERT INTO TINFORMATION_STABDARD_HISTORY(STERMINALID,SABBREVIATION,VALUESTTERMINAL,SCREATE,DCREATE
    //                                          ,SUPDATE,DUPDATE,NVERSION,SCARTYPEID,SPRODUCTTYPEID) 
    //                                      VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["STERMINALID"] + "") + @"'
    //                                      ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
    //                                      ," + CommonFunction.ReplaceInjection(VALUESTTERMINAL) + @"
    //                                      ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
    //                                      ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)
    //                                      ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
    //                                      ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)                      
    //                                      ," + CommonFunction.ReplaceInjection(sNVERSION) + @"
    //                                      ," + CommonFunction.ReplaceInjection(SCARTYPEID) + @"
    //                                      ,'" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + "')";
    //                    using (OracleCommand com = new OracleCommand(strQuery, con))
    //                    {
    //                        com.ExecuteNonQuery();
    //                    }
    //                }
    //                else
    //                {
    //                    //ถ้าไม่่เคยข้อมูล

    //                    string strQuery = @"INSERT INTO TINFORMATION_STABDARD_HISTORY(STERMINALID,SABBREVIATION,VALUESTTERMINAL,SCREATE,DCREATE
    //                                          ,SUPDATE,DUPDATE,NVERSION,SCARTYPEID,SPRODUCTTYPEID) 
    //                                      VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["STERMINALID"] + "") + @"'
    //                                      ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
    //                                      ," + CommonFunction.ReplaceInjection(VALUESTTERMINAL) + @"
    //                                      ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
    //                                      ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)
    //                                      ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
    //                                      ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)                      
    //                                      ,1
    //                                      ," + CommonFunction.ReplaceInjection(SCARTYPEID) + @"
    //                                      ,'" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + "')";
    //                    using (OracleCommand com = new OracleCommand(strQuery, con))
    //                    {
    //                        com.ExecuteNonQuery();
    //                    }
    //                }
    //            }
    //            else
    //            {

    //            }




    //        }
    //    }

    private void Addhistoryinformationstaddard(string STERMINALID, string sSPRODUCTTYPEID, string sSCARTYPEID, string sNVERSION)
    {

        string USER = Session["UserID"] + "";
        string senddatatohistory = @"SELECT STERMINALID, SABBREVIATION,VALUESTTERMINAL, DCREATE, SCREATE, DUPDATE, SUPDATE,SCARTYPEID,SPRODUCTTYPEID
                                    FROM TINFORMATION_STABDARD
                                       WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"'
                                          AND SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(sSPRODUCTTYPEID) + @"'
                                          AND SCARTYPEID ='" + CommonFunction.ReplaceInjection(sSCARTYPEID) + @"'";

        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TPRODUCT_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {


                string datatohistory = @"SELECT STERMINALID, SABBREVIATION, DCREATE, SCREATE, DUPDATE, SUPDATE, VALUESTTERMINAL, SPRODUCTTYPEID, SCARTYPEID, NVERSION  
                                        FROM TINFORMATION_STABDARD_HISTORY
                                        WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"'
                                          AND SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(sSPRODUCTTYPEID) + @"'
                                          AND SCARTYPEID ='" + CommonFunction.ReplaceInjection(sSCARTYPEID) + @"'
                                          ORDER BY NVERSION DESC";
                int num = 0;
                // string VALUESTTERMINAL = (int.TryParse(dt.Rows[0]["VALUESTTERMINAL"] + "", out num) ? num : 0) + "";
                //string SCARTYPEID = dt.Rows[0]["SCARTYPEID"] + "" != "" ? dt.Rows[0]["SCARTYPEID"] + "" : "null";
                string SCARTYPEID = sSCARTYPEID;
                string SPRODUCTTYPEID = sSPRODUCTTYPEID;
                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                string VALUESTTERMINAL = dt.Rows[0]["VALUESTTERMINAL"] + "" != "" ? dt.Rows[0]["VALUESTTERMINAL"] + "" : "NULL";
                //เช็คว่า TVendor_signer_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว



                    string strQuery = @"INSERT INTO TINFORMATION_STABDARD_HISTORY(STERMINALID,SABBREVIATION,VALUESTTERMINAL,SCREATE,DCREATE
                                      ,SUPDATE,DUPDATE,NVERSION,SCARTYPEID,SPRODUCTTYPEID) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["STERMINALID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(VALUESTTERMINAL) + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)                      
                                  ," + CommonFunction.ReplaceInjection(sNVERSION) + @"
                                  ," + CommonFunction.ReplaceInjection(SCARTYPEID) + @"
                                  ,'" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + "')";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล

                    string strQuery = @"INSERT INTO TINFORMATION_STABDARD_HISTORY(STERMINALID,SABBREVIATION,VALUESTTERMINAL,SCREATE,DCREATE
                                      ,SUPDATE,DUPDATE,NVERSION,SCARTYPEID,SPRODUCTTYPEID) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["STERMINALID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(VALUESTTERMINAL) + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516)                      
                                  ,1
                                  ," + CommonFunction.ReplaceInjection(SCARTYPEID) + @"
                                  ,'" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + "')";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                //                string datatohistory = @"SELECT STERMINALID, SABBREVIATION, DCREATE, SCREATE, DUPDATE, SUPDATE, VALUESTTERMINAL, SPRODUCTTYPEID, SCARTYPEID, NVERSION  
                //                                        FROM TINFORMATION_STABDARD_HISTORY
                //                                        WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"'
                //                                          AND SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(sSPRODUCTTYPEID) + @"'
                //                                          AND SCARTYPEID ='" + CommonFunction.ReplaceInjection(sSCARTYPEID) + @"'
                //                                          ORDER BY NVERSION DESC";
                string SCARTYPEID = sSCARTYPEID;
                string SPRODUCTTYPEID = sSPRODUCTTYPEID;
                // DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                string VALUESTTERMINAL = "NULL";
                dynamic sData = gvw.GetRowValues(gvw.EditingRowVisibleIndex, "SABBREVIATION");
                string strQuery = @"INSERT INTO TINFORMATION_STABDARD_HISTORY(STERMINALID,SABBREVIATION,VALUESTTERMINAL,SCREATE,DCREATE
                                      ,SUPDATE,DUPDATE,NVERSION,SCARTYPEID,SPRODUCTTYPEID) 
                                  VALUES('" + CommonFunction.ReplaceInjection(STERMINALID) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(sData + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(VALUESTTERMINAL) + @"
                                  ,'" + CommonFunction.ReplaceInjection(USER) + @"'
                                  ,sysdate
                                  ,'" + CommonFunction.ReplaceInjection(USER) + @"'
                                  ,sysdate                     
                                  ,1
                                  ," + CommonFunction.ReplaceInjection(SCARTYPEID) + @"
                                  ,'" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + "')";
                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }
            }




        }
    }

    [Serializable]
    class sInformation
    {
        public string SPRODUCTTYPENAME { get; set; }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}