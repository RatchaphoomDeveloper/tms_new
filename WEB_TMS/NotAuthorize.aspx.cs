﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NotAuthorize : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lblModalTitle.Text = "Not Authorize";
            lblModalBody.Text = "Your user information no access to the system.";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowInformation", "$('#ShowInformation').modal();", true);
            upModal.Update();
        }
    }

    protected void cmdLogin_ServerClick(object sender, EventArgs e)
    {
        //Session.Clear();
        //Session.Abandon();
        //
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowInformation", "$('#ShowInformation').modal('hide');", true);
        Response.Redirect("admin_home.aspx");
    }
}