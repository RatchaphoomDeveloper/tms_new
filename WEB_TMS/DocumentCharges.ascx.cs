﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCharges : System.Web.UI.UserControl
{
    private const string sPathDownload = @"UploadFile/FileDownload/charge/";
    private const string sFileDownload = "2 คู่มือการคิดค่าธรรมเนียมตรวจสอบวัดน้ำรถบรรทุกน้ำมัน.pdf";

    protected void Page_Load(object sender, EventArgs e)
    {
        txtFileDownload.Text = EncryptLinkToOpenFile(sPathDownload + Server.UrlEncode(sFileDownload));
    }
    private string EncryptLinkToOpenFile(string str)
    {
        string strEncrypt = "";
        //if (ConfigurationManager.AppSettings["Encrypt"].ToString().Equals("1"))
        //{
        //    strEncrypt = Server.UrlEncode(STCrypt.Encrypt(str));
        //}
        //else
        //{
        strEncrypt = str;
        //}
        return "openFile.aspx?str=" + strEncrypt;
    }
}