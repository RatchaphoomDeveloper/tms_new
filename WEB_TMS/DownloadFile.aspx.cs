﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DownloadFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Appeal_Download"] != null)
        {
            string FullPath = Session["Appeal_Download"].ToString();
            this.Download(FullPath);

            Session.Remove("Appeal_Download");
        }
        else if (Request.QueryString["row"] != null)
        {
            string Row = Request.QueryString["row"].ToString();
            DataTable dt = (DataTable)Session["dtAppealComplain"];
            string FullPath = dt.Rows[int.Parse(Row)]["FULLPATH"].ToString();

            this.Download(FullPath);
        }
    }

    private void Download(string FullPath)
    {
        string FileName = Path.GetFileName(FullPath);

        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(FullPath);
        Response.End();
    }
}