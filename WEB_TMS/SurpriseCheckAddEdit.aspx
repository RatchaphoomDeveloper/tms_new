﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="SurpriseCheckAddEdit.aspx.cs" Inherits="SurpriseCheckAddEdit" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .divImgTruck {
            position: relative;
            height: 400px;
        }

            .divImgTruck img {
                position: absolute;
                height: 400px;
                width: 100%;
            }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">ข้อมูลรถบรรทุก</a>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <div class="row form-group">
                                <div class="col-md-1">
                                </div>
                                <label class="col-md-2 control-label">ทะเบียนหัว</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtSHEADREGISTERNO" CssClass="form-control " ReadOnly="true" />
                                </div>
                                <label class="col-md-2 control-label">ทะเบียนท้าย</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtSTRAILERREGISTERNO" CssClass="form-control " ReadOnly="true" />
                                </div>

                            </div>
                            <div class="row form-group" id="divREPORTER">
                                <label class="col-md-3 control-label">เลขที่สัญญา</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtSCONTRACTNO" CssClass="form-control " ReadOnly="true" />

                                </div>
                                <label class="col-md-2 control-label">วันที่ทำการ Surprise Check ล่าสุด</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtCheckerDate" CssClass="form-control" ReadOnly="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField runat="server" ID="hidcollapse1" />
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">รูปภาพรถ</a>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row form-group">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8 divImgTruck">
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck.png" runat="server" ID="imgTruck" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_5.png" runat="server" ID="imgTruck_5" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_6.png" runat="server" ID="imgTruck_6" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_8.png" runat="server" ID="imgTruck_8" Style="z-index: 98" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_9.png" runat="server" ID="imgTruck_9" Style="z-index: 100" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_10.png" runat="server" ID="imgTruck_10" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_11.png" runat="server" ID="imgTruck_11" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_12.png" runat="server" ID="imgTruck_12" Style="z-index: 97" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_13.png" runat="server" ID="imgTruck_13" Style="z-index: 95" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_15.png" runat="server" ID="imgTruck_15" Style="z-index: 99" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_16.png" runat="server" ID="imgTruck_16" Style="z-index: 96" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_17.png" runat="server" ID="imgTruck_17" Style="z-index: 92" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_18.png" runat="server" ID="imgTruck_18" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_19.png" runat="server" ID="imgTruck_19" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_20.png" runat="server" ID="imgTruck_20" Style="z-index: 93" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_21.png" runat="server" ID="imgTruck_21" Style="z-index: 2" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_22.png" runat="server" ID="imgTruck_22" Style="z-index: 94" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_23.png" runat="server" ID="imgTruck_23" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_24.png" runat="server" ID="imgTruck_24" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_25.png" runat="server" ID="imgTruck_25" Style="z-index: 1" />
                                    <asp:Image ImageUrl="Images/SurpriseCheckTruck/Truck_26.png" runat="server" ID="imgTruck_26" Style="z-index: 1" />
                                </div>
                                <label class="col-md-2 control-label"></label>

                            </div>

                        </div>
                    </div>
                    <asp:HiddenField runat="server" ID="HiddenField4" />
                </div>
                <ul class="nav nav-tabs" runat="server" id="tabtest">
                    <li class="active" id="liTab1" runat="server"><a href="#TabGeneral" data-toggle="tab" aria-expanded="true" runat="server" id="GeneralTab1">ปัญหาการขนส่ง</a></li>
                    <li class="" id="liTab2" runat="server"><a href="#TabGeneral2" data-toggle="tab" aria-expanded="false" runat="server" id="GeneralTab2">หลักฐานการกระทำผิดและปัญหาอื่นๆ</a></li>
                    <li class="" id="liTab3" runat="server">
                        <a href="#TabGeneral3" data-toggle="tab" aria-expanded="false" runat="server" id="GeneralTab3">ประวัติการตรวจ</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="TabGeneral">
                        <asp:Panel runat="server" ID="plCheckList">

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">สภาพรถ</a>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <asp:GridView runat="server" ID="gvCheckList"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="SCHECKLISTID,SCHECKLISTNAME,SVERSIONLIST,STYPECHECKLISTID" OnRowDataBound="gvCheckList_RowDataBound" OnRowCommand="gvCheckList_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รายการตรวจสอบ">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%# Eval("SCHECKLISTNAME") %>' runat="server" />
                                                        <asp:Label Text="" ForeColor="Red" ID="lblCHOLD" runat="server" />
                                                        <asp:Label Text="" ForeColor="Red" ID="lblMAnDay" runat="server" />
                                                        <asp:Label Text="" ForeColor="Blue" ID="lblCheckBy" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STEP 1: (ทั้งหมด)<br/>แจ้งตรวจพบปัญหา" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="แจ้งตรวจพบปัญหา" ID="btnStep1" runat="server" CommandArgument="Step1" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STEP 2: (ผู้ขนส่ง)<br/>	แจ้งผลการแก้ไข" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="แจ้งผลการแก้ไข" ID="btnStep2" runat="server" CommandArgument="Step2" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STEP 3: (ปง.และคลัง)<br/>ตรวจสอบผลการแก้ไข" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="ตรวจสอบผลการแก้ไข" ID="btnStep3" runat="server" CommandArgument="Step3" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse2" />
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse3" id="acollapse3">พนักงานประจำรถ</a>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <asp:GridView runat="server" ID="gvCheckListEmp"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="SCHECKLISTID,SCHECKLISTNAME,SVERSIONLIST,STYPECHECKLISTID" OnRowDataBound="gvCheckList_RowDataBound" OnRowCommand="gvCheckList_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รายการตรวจสอบ">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%# Eval("SCHECKLISTNAME") %>' runat="server" />
                                                        <asp:Label Text="" ForeColor="Red" ID="lblCHOLD" runat="server" />
                                                        <asp:Label Text="" ForeColor="Red" ID="lblMAnDay" runat="server" />
                                                        <asp:Label Text="" ForeColor="Blue" ID="lblCheckBy" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STEP 1: (ทั้งหมด)<br/>แจ้งตรวจพบปัญหา" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="แจ้งตรวจพบปัญหา" ID="btnStep1" runat="server" CommandArgument="Step1" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STEP 2: (ผู้ขนส่ง)<br/>	แจ้งผลการแก้ไข" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="แจ้งผลการแก้ไข" ID="btnStep2" runat="server" CommandArgument="Step2" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STEP 3: (ปง.และคลัง)<br/>ตรวจสอบผลการแก้ไข" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="ตรวจสอบผลการแก้ไข" ID="btnStep3" runat="server" CommandArgument="Step3" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse3" />
                            </div>

                            <div class="panel-footer text-right">
                                <asp:Button Text="บันทึกปัญหาการขนส่ง" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-info" runat="server" />
                                <asp:Button Text="ยกเลิก" ID="btnClose" OnClick="btnClose_Click" CssClass="btn btn-warning" runat="server" />
                            </div>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="plStep1" CssClass="hidden">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4">
                                        <asp:Label Text="" ID="lblSCHECKLISTNAME" runat="server" /></a>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse in">
                                    <div class="panel-body">

                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">เลือกรูปภาพ<asp:Label ID="Label1" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:FileUpload ID="fileUploadStep1" accept="image/*" runat="server" />
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtDetailStep1" CssClass="form-control " />
                                            </div>

                                            <div class="col-md-2">

                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:Button runat="server" ID="btnAddStep1" Text="Add" OnClick="btnAddStep1_Click" CssClass="btn btn-md bth-hover btn-info " />
                                                    </ContentTemplate>
                                                    <Triggers>

                                                        <asp:PostBackTrigger ControlID="btnAddStep1" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                            </div>
                                        </div>
                                        <asp:GridView runat="server" ID="gvFileStep1"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="SCHECKLISTID" OnRowDeleting="gvFileStep1_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รูปภาพ">
                                                    <ItemTemplate>
                                                        <a href='<%# Eval("SFILEPATH") %>' target="_blank">
                                                            <asp:Image ID="imgStep1" runat="server" ImageUrl='<%# Eval("SFILEPATH") %>' Width="50%" /><br />
                                                        </a>

                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetail" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="50">
                                                    <ItemTemplate>
                                                        <asp:Button Text="ลบ" CssClass="btn btn-danger" CommandName="delete" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="panel-footer text-right">
                                    <asp:Button Text="บันทึกรายการตรวจสอบ" ID="btnCloseStep1" OnClick="btnCloseStep1_Click" CssClass="btn btn-info" runat="server" />
                                    <asp:Button Text="กลับ" ID="Button1" OnClick="btnCloseStep1_Click" CssClass="btn btn-warning" runat="server" />
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse4" />
                            </div>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="plStep2" CssClass="hidden">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse5" id="acollapse5">
                                        <asp:Label Text="" ID="lblSCHECKLISTNAMEStep2" runat="server" /></a>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">เลือกรูปภาพที่ต้องการแก้ไข<asp:Label ID="Label3" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" ID="ddlFileStep2" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">เลือกรูปภาพ<asp:Label ID="Label2" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:FileUpload ID="fileUploadStep2" accept="image/*" runat="server" />
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtDetailStep2" CssClass="form-control " />
                                            </div>

                                            <div class="col-md-2">

                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:Button runat="server" ID="btnAddStep2" Text="Add" OnClick="btnAddStep2_Click" CssClass="btn btn-md bth-hover btn-info " />
                                                    </ContentTemplate>
                                                    <Triggers>

                                                        <asp:PostBackTrigger ControlID="btnAddStep2" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                            </div>
                                        </div>
                                        <asp:GridView runat="server" ID="gvFileStep2"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="SCHECKLISTID" OnRowDataBound="gvFileStep2_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รูปภาพ" HeaderStyle-Width="45%">
                                                    <ItemTemplate>
                                                        <a href='<%# Eval("SFILEPATH") %>' target="_blank">
                                                            <asp:Image ID="imgStep1" runat="server" ImageUrl='<%# Eval("SFILEPATH") %>' Width="100%" /><br />
                                                        </a>
                                                        <br />
                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetail" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รูปภาพหลังแก้ไข" HeaderStyle-Width="45%">
                                                    <ItemTemplate>
                                                        <asp:Repeater ID="rpFileStep2Detail" runat="server">
                                                            <ItemTemplate>
                                                                <div class="row form-group">
                                                                    <div class="col-md-10">
                                                                        <a href='<%# Eval("SFILEPATH") %>' target="_blank">
                                                                            <asp:Image ID="imgStep2" runat="server" ImageUrl='<%# Eval("SFILEPATH") %>' Width="100%" /><br />
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <asp:Button Text="ลบ" CssClass="btn btn-danger" runat="server" CommandName='<%# Container.ItemIndex %>' CommandArgument='<%# Eval("SMAINPICTURE") %>' ID="btnDelete" OnClick="btnDelete_Click" />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetailStep2" runat="server" />

                                                                    </div>
                                                                </div>
                                                                <hr />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="panel-footer text-right">
                                    <asp:Button Text="บันทึกรายการตรวจสอบ" ID="Button2" OnClick="btnCloseStep2_Click" CssClass="btn btn-info" runat="server" />
                                    <asp:Button Text="กลับ" ID="btnCloseStep2" OnClick="btnCloseStep2_Click" CssClass="btn btn-warning" runat="server" />
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse5" />
                            </div>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="plStep3" CssClass="hidden">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse6" id="acollapse6">
                                        <asp:Label Text="" ID="lblSCHECKLISTNAMEStep3" runat="server" /></a>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">ตรวจสอบแล้วมีผลดังนี้<asp:Label ID="Label6" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:RadioButtonList runat="server" ID="rblCCHECKED" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="อนุมัติผลการแก้ไข" Value="1" Selected="True" />
                                                    <asp:ListItem Text="ไม่อนุมัติ" Value="0" />
                                                </asp:RadioButtonList>
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtSREMARKCHECKED" CssClass="form-control " />
                                            </div>

                                        </div>

                                        <div class="row form-group text-right">
                                            <div class="col-md-12">
                                                <asp:Button runat="server" ID="btnApprove" Text="บันทึกรายการตรวจสอบ" OnClick="btnApprove_Click" CssClass="btn btn-md bth-hover btn-info " />
                                                <asp:Button Text="กลับ" ID="btnCloseStep3" OnClick="btnCloseStep3_Click" CssClass="btn btn-warning" runat="server" />

                                            </div>
                                        </div>
                                        <asp:GridView runat="server" ID="gvFileStep3"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="SCHECKLISTID" OnRowDataBound="gvFileStep3_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รูปภาพก่อนแก้ไข" HeaderStyle-Width="45%">
                                                    <ItemTemplate>
                                                        <a href='<%# Eval("SFILEPATH") %>' target="_blank">
                                                            <asp:Image ID="imgStep1" runat="server" ImageUrl='<%# Eval("SFILEPATH") %>' Width="100%" /><br />
                                                        </a>
                                                        <br />
                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetail" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รูปภาพหลังแก้ไข" HeaderStyle-Width="45%">
                                                    <ItemTemplate>
                                                        <asp:Repeater ID="rpFileStep3Detail" runat="server">
                                                            <ItemTemplate>
                                                                <div class="row form-group">
                                                                    <div class="col-md-11">
                                                                        <a href='<%# Eval("SFILEPATH") %>' target="_blank">
                                                                            <asp:Image ID="imgStep3" runat="server" ImageUrl='<%# Eval("SFILEPATH") %>' Width="100%" /><br />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetailStep3" runat="server" />

                                                                    </div>
                                                                </div>
                                                                <hr />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse6" />
                            </div>
                        </asp:Panel>
                        <asp:HiddenField ID="hidRowIndex" runat="server" />
                        <asp:HiddenField ID="hidStep" runat="server" />
                        <asp:HiddenField ID="hidSCHECKLISTID" runat="server" />
                        <asp:HiddenField ID="hidSVERSIONLIST" runat="server" />
                        <asp:HiddenField ID="hidSTYPECHECKLISTID" runat="server" />
                        <asp:HiddenField ID="hidEmp" runat="server" />
                    </div>
                    <div class="tab-pane fade" id="TabGeneral2">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse7" id="acollapse7">
                                    <asp:Label Text="
ปัญหาอื่นๆ"
                                        ID="Label7" runat="server" /></a>
                            </div>
                            <div id="collapse7" class="panel-collapse collapse in">
                                <div class="panel-body">

                                    <div class="row form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-4">
                                            <asp:CheckBox Text="ปัญหาอื่นๆ ระบุปัญหา" runat="server" ID="cbCOTHER" />
                                        </div>
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-4">
                                        </div>


                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-4">
                                            <asp:TextBox runat="server" ID="txtSOTHERREMARK" CssClass="form-control " TextMode="MultiLine" Rows="4" />
                                        </div>
                                        <label class="col-md-2 control-label">รายละเอียดของปัญหา </label>
                                        <div class="col-md-4">
                                            <asp:TextBox runat="server" ID="txtSREMARK" CssClass="form-control " TextMode="MultiLine" Rows="4" />
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField runat="server" ID="HiddenField2" />
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4">
                                    <asp:Label Text="รูปภาพ" ID="Label4" runat="server" /></a>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse in">
                                <div class="panel-body">

                                    <div class="row form-group">
                                        <label class="col-md-2 control-label">เลือกรูปภาพ<asp:Label ID="Label5" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                        <div class="col-md-3">
                                            <asp:FileUpload ID="fileUploadOther" accept="image/*" runat="server" />
                                        </div>
                                        <label class="col-md-2 control-label">รายละเอียด</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtDetailOther" CssClass="form-control " />
                                        </div>

                                        <div class="col-md-2">

                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <asp:Button runat="server" ID="btnAddOther" Text="Add" OnClick="btnAddOther_Click" CssClass="btn btn-md bth-hover btn-info " />
                                                </ContentTemplate>
                                                <Triggers>

                                                    <asp:PostBackTrigger ControlID="btnAddOther" />
                                                </Triggers>
                                            </asp:UpdatePanel>


                                        </div>
                                    </div>
                                    <asp:GridView runat="server" ID="gvOther"
                                        Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                        HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                        AllowPaging="false" DataKeyNames="SCHECKID" OnRowDeleting="gvOther_RowDeleting">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="รูปภาพ">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("SPATH") %>' target="_blank">
                                                        <asp:Image ID="imgStep1" runat="server" ImageUrl='<%# Eval("SPATH") %>' Width="100%" /><br />
                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetail" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="50">
                                                <ItemTemplate>
                                                    <asp:Button Text="ลบ" CssClass="btn btn-danger" CommandName="delete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <asp:HiddenField runat="server" ID="HiddenField1" />
                        </div>

                        <div class="panel-footer text-right">
                            <asp:Button Text="บันทึก" ID="btnSaveOther" OnClick="btnSaveOther_Click" CssClass="btn btn-info" runat="server" />
                            <asp:Button Text="ยกเลิก" ID="btnCloseOther" OnClick="btnClose_Click" CssClass="btn btn-warning" runat="server" />
                        </div>
                    </div>
                    <div class="tab-pane fade" id="TabGeneral3">
                        <asp:Panel runat="server" ID="plHistory">

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse10" id="acollapse10">ประวัติการตรวจ</a>
                                </div>
                                <div id="collapse10" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <asp:GridView runat="server" ID="gvHistory"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="SCHECKID,SCHECKLISTID,SCHECKLISTNAME" OnRowCommand="gvHistory_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DUPDATECHECKED" HeaderText="วันที่ตรวจ" />
                                                <asp:BoundField DataField="SCREATENAME" HeaderText="ผู้ตรวจ" />
                                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:CheckBox Text="" Checked='<%# Eval("CCHECKED") + string.Empty == "1" ? true : false %>' ID="cbCCHECKED" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SCHECKLISTNAME" HeaderText="รายการ" />
                                                <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="รายละเอียด" ID="btnHistory" runat="server" CommandArgument="History" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="HiddenField3" />
                            </div>

                        </asp:Panel>
                        <asp:Panel runat="server" ID="plHistoryDetail" CssClass="hidden">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse11" id="acollapse11">
                                        <asp:Label Text="" ID="lblSCHECKLISTNAMEDetil" runat="server" /></a>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">ตรวจสอบแล้วมีผลดังนี้<asp:Label ID="Label8" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:RadioButtonList runat="server" ID="rblCCHECKEDHistory" RepeatDirection="Horizontal" Enabled="false">
                                                    <asp:ListItem Text="อนุมัติผลการแก้ไข" Value="1" Selected="True" />
                                                    <asp:ListItem Text="ไม่อนุมัติ" Value="0" />
                                                </asp:RadioButtonList>
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtSREMARKCHECKEDHistory" CssClass="form-control " ReadOnly="true" />
                                            </div>

                                        </div>
                                        <asp:GridView runat="server" ID="gvHistoryDetail"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="SCHECKLISTID" OnRowDataBound="gvHistoryDetail_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รูปภาพก่อนแก้ไข" HeaderStyle-Width="45%">
                                                    <ItemTemplate>
                                                        <a href='<%# Eval("SFILEPATH") %>' target="_blank">
                                                            <asp:Image ID="imgStep1" runat="server" ImageUrl='<%# Eval("SFILEPATH") %>' Width="100%" /><br />
                                                        </a>
                                                        <br />
                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetail" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รูปภาพหลังแก้ไข" HeaderStyle-Width="45%">
                                                    <ItemTemplate>
                                                        <asp:Repeater ID="rpFileStep3Detail" runat="server">
                                                            <ItemTemplate>
                                                                <div class="row form-group">
                                                                    <div class="col-md-11">
                                                                        <a href='<%# Eval("SFILEPATH") %>' target="_blank">
                                                                            <asp:Image ID="imgStep3" runat="server" ImageUrl='<%# Eval("SFILEPATH") %>' Width="100%" /><br />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                        <asp:Label Text='<%# "รายละเอียด : " + Eval("SEVIDENCENAME") %>' ID="lblDetailStep3" runat="server" />

                                                                    </div>
                                                                </div>
                                                                <hr />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="panel-footer text-right">
                                    <asp:Button Text="กลับ" ID="btnCloseHistoryDetail" OnClick="btnCloseHistoryDetail_Click" CssClass="btn btn-warning" runat="server" />
                                </div>
                                <asp:HiddenField runat="server" ID="HiddenField5" />
                                <asp:HiddenField runat="server" ID="hidRowIndexHistory" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="hidSCHECKID" runat="server" Value="0" />
            <asp:HiddenField ID="hidNSURPRISECHECKID" runat="server" Value="0" />
            <asp:HiddenField ID="hidCactive" runat="server" />
            <asp:HiddenField ID="hidCGROUP" runat="server" />
            <asp:HiddenField ID="hidSTRUCKID" runat="server" />
            <asp:HiddenField ID="hidSCONTRACTID" runat="server" />
            <asp:HiddenField ID="hidSVENDORNAME" runat="server" />
            <asp:HiddenField ID="hidSVENDORID" runat="server" />
            <asp:HiddenField ID="hidYear" runat="server" />
            <asp:HiddenField ID="hidYearID" runat="server" />

        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

