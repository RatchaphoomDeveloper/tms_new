﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" 
    CodeFile="Authenticate.aspx.cs" Inherits="Authenticate" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register Src="~/UserControl/SuccessModel.ascx" TagPrefix="uc1" TagName="ModalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style15
        {
            color: #03527C;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">   
    <div class="container" style="width: 100%;">
                <div class="jumbotron" style="background-color: white">
                    <asp:UpdatePanel ID="udpMain" runat="server">
                        <ContentTemplate>
                            <h2 class="page-header" style="margin-top: 10px">
                                <font style="font-family: 'Angsana New'; color: #009120"> สิทธิ์การใช้งาน</font>
                            </h2>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ตั้งค่าสิทธิ์การใช้งาน&#8711;</a>
                                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract">
                                    <div class="panel-body">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                            <li id="liUserGroup" runat="server" class="active"><a href="#UserGroupo" data-toggle="tab" aria-expanded="true">กลุ่มผู้ใช้งาน</a></li>
                                            <li id="liWaterCheck" runat="server" class=""><a href="#TabWaterCheck" data-toggle="tab" aria-expanded="false">ตรวจวัดน้ำ</a></li>
                                            <li id="liOperationTransit" runat="server" class=""><a href="#TabOperationTransit" data-toggle="tab" aria-expanded="false">การปฏิบัติงานขนส่ง</a></li>
                                            <li id="liTransitProblem" runat="server" class=""><a href="#TabTransitProblem" data-toggle="tab" aria-expanded="false">ปัญหาการขนส่ง</a></li>
                                            <li id="liDatabase" runat="server" class=""><a href="#TabDatabase" data-toggle="tab" aria-expanded="false">ฐานข้อมูลระบบ</a></li>
                                            <li id="liCheckPerformance" runat="server" class=""><a href="#TabCheckPerformance" data-toggle="tab" aria-expanded="false">ระบบประเมินผล</a></li>
                                            <li id="liOther" runat="server" class=""><a href="#TabOther" data-toggle="tab" aria-expanded="false">อื่นๆ</a></li>
                                            <li id="liMobile" runat="server" class=""><a href="#TabMobile" data-toggle="tab" aria-expanded="false">Mobile</a></li>
                                            <%--<li class=""><a href="#TabReport" data-toggle="tab" aria-expanded="false">รายงาน</a></li>--%>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content" style="padding-left: 30px">
                                            <div class="tab-pane fade active in" id="UserGroupo">
                                                <br />
                                                <br />
                                                <asp:DropDownList ID="ddlUserGroup" runat="server" CssClass="form-control" Style="width: 30%" AutoPostBack="true" OnSelectedIndexChanged="ddlUserGroup_SelectedIndexChanged"></asp:DropDownList>
                                                <br />
                                            </div>
                                            <div class="tab-pane fade" id="TabWaterCheck">
                                                <br />
                                                <div class="dataTable_wrapper">
                                                   <%-- <asp:GridView ID="dgvMaster" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2" 
                                                        OnPageIndexChanging="dgvMaster_PageIndexChanging" SkinID="GridNoPaging">--%>
                                                          <asp:GridView ID="dgvWaterCheck" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2"
                                                            BackColor="White" ForeColor="#284775">
                                                        <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                             
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="TabOperationTransit">
                                                <br />
                                                <div class="dataTable_wrapper">
                                                    <%--<asp:GridView ID="dgvRequest" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2" OnPageIndexChanging="dgvRequest_PageIndexChanging" SkinID="GridNoPaging">--%>
                                                        <asp:GridView ID="dgvOperationTransit" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2"
                                                            BackColor="White" ForeColor="#284775">
                                                            <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="TabTransitProblem">
                                                <br />
                                                <div class="dataTable_wrapper">
                                                    <%--<asp:GridView ID="dgvReport" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2" OnPageIndexChanging="dgvReport_PageIndexChanging" SkinID="GridNoPaging">--%>
                                                    <asp:GridView ID="dgvTransitProblem" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2"
                                                            BackColor="White" ForeColor="#284775">
                                                        <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="TabDatabase" >
                                                <br />
                                                <div class="dataTable_wrapper">
                                                    <%--<asp:GridView ID="dgvAdmin" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2" OnPageIndexChanging="dgvAdmin_PageIndexChanging" SkinID="GridNoPaging">--%>
                                                        <asp:GridView ID="dgvDatabase" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2"
                                                            BackColor="White" ForeColor="#284775">
                                                            <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                             <div class="tab-pane fade" id="TabCheckPerformance">
                                                <br />
                                                <div class="dataTable_wrapper">
                                                    <%--<asp:GridView ID="dgvReport" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2" OnPageIndexChanging="dgvReport_PageIndexChanging" SkinID="GridNoPaging">--%>
                                                    <asp:GridView ID="dgvCheckPerformance" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2"
                                                            BackColor="White" ForeColor="#284775">
                                                        <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                             <div class="tab-pane fade" id="TabOther" >
                                                <br />
                                                <div class="dataTable_wrapper">
                                                    <%--<asp:GridView ID="dgvReport" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2" OnPageIndexChanging="dgvReport_PageIndexChanging" SkinID="GridNoPaging">--%>
                                                    <asp:GridView ID="dgvOther" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2"
                                                            BackColor="White" ForeColor="#284775">
                                                        <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                             <div class="tab-pane fade" id="TabMobile" >
                                                <br />
                                                <div class="dataTable_wrapper">
                                                    <%--<asp:GridView ID="dgvReport" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2" OnPageIndexChanging="dgvReport_PageIndexChanging" SkinID="GridNoPaging">--%>
                                                    <asp:GridView ID="dgvMobile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2"
                                                            BackColor="White" ForeColor="#284775">
                                                        <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                             <%--    <div class="tab-pane fade" id="TabReport">
                                                <br />
                                                <div class="dataTable_wrapper">
                                                    <asp:GridView ID="dgvReport" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                            BackColor="White" ForeColor="#284775">
                                                        <Columns>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="VISIBLE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="READ">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="WRITE">
                                                                <HeaderStyle Wrap="false" />
                                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer" style="text-align: right; vertical-align: bottom">
                                    <asp:LinkButton ID="cmdCloseAll" runat="server" CssClass="btn btn-md btn-hover btn-success" Style="width: 100px" OnClick="cmdCloseAll_Click">
                                        <span aria-hidden="true" class="fa fa-circle-o"></span>&nbsp;ปิดทั้งหมด
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="cmdOpenAll" runat="server" CssClass="btn btn-md btn-hover btn-success" Style="width: 110px" OnClick="cmdOpenAll_Click">
                                <span aria-hidden="true" class="fa fa-circle-o"></span>&nbsp;เปิดทั้งหมด
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="cmdSave" runat="server" CssClass="btn btn-md btn-hover btn-success" Style="width: 100px" UseSubmitBehavior="false" data-toggle="modal"
                                        data-target="#ModalConfirmBeforeSave">
                                <span aria-hidden="true" class="fa fa-save"></span>&nbsp;บันทึก
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="cmdCancel" runat="server" CssClass="btn btn-md btn-hover btn-success" Style="width: 100px" OnClick="cmdCancel_Click">
                                <span aria-hidden="true" class="fa fa-undo"></span>&nbsp;ยกเลิก
                                    </asp:LinkButton>
                                </div>
                            </div>

                            <uc1:ModalPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
                                IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
                                TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกข้อมูล ใช่หรือไม่ ?" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
    <script>
                $(document).ready(function () {
        
                    $(window).resize(function () {
              
                    });
                });
   </script>
</asp:Content>

