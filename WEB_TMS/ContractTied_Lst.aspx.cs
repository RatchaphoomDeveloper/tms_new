﻿using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class ContractTied_Lst : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DrowDownList();
        }
        else
        {
            cmbVendor.DataSource = ViewState["DataTVendorSap"];
            cmbVendor.DataBind();
        }
    }

    private void DrowDownList()
    {
        ddlWordGroup.DataTextField = "NAME";
        ddlWordGroup.DataValueField = "ID";
        ddlWordGroup.DataSource = ContractTiedBLL.Instance.WorkGroupSelect();
        ddlWordGroup.DataBind();
        ddlWordGroup.Items.Insert(0, new ListItem() { 
            Text = "เลือกกลุ่มงาน",
            Value = "0"
        });

        //ddlGroups.DataTextField = "NAME";
        //ddlGroups.DataValueField = "ID";
        //ddlGroups.DataSource = ContractTiedBLL.Instance.GroupSelect();
        //ddlGroups.DataBind();
        ddlGroups.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });
        ViewState["DataTVendorSap"] = VendorBLL.Instance.TVendorSapSelect();
        cmbVendor.DataSource = ViewState["DataTVendorSap"];
        cmbVendor.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    private void BindData()
    {
        gvContract.DataSource = ContractTiedBLL.Instance.MContractSelect(ddlWordGroup.SelectedValue, ddlGroups.SelectedValue, cmbVendor.Value + string.Empty, cmbConTract.Value + string.Empty);
        gvContract.DataBind();
    }
    protected void cmbConTract_ItemRequestedByValue(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbConTract_ItemsRequestedByFilterCondition(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        comboBox.DataSource = ContractTiedBLL.Instance.MContractSelectByPagesize(String.Format("%{0}%", e.Filter), e.BeginIndex + 1, e.EndIndex + 1);
        comboBox.DataBind();
    }
    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgEdit = (ImageButton)sender;
        string sEncrypt = Server.UrlEncode(STCrypt.Encrypt(imgEdit.CommandArgument));
        Response.Redirect("~/ContractTied.aspx?id=" + sEncrypt);
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ContractTied.aspx");
    }
    protected void ddlWordGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroups.DataTextField = "NAME";
        ddlGroups.DataValueField = "ID";
        ddlGroups.DataSource = ContractTiedBLL.Instance.GroupSelect(ddlWordGroup.SelectedValue);
        ddlGroups.DataBind();
        ddlGroups.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });
    }
    protected void cButtonDelete_ClickDelete(object sender, EventArgs e)
    {
        
        try
        {

            string idstr = string.Empty;
            Button btnDelete = (Button)sender;
            idstr = btnDelete.CommandArgument.Split('_')[1];
            DataTable dt = ContractTiedBLL.Instance.MContractDelete(idstr);
            if (dt.Rows[0]["MESSAGE"] == DBNull.Value)
            {
                alertSuccess("บันทึกสำเร็จ");
                BindData();
            }
            else if ((dt.Rows[0]["MESSAGE"] + "").Trim() == "HAVE")
            {
                alertFail("สัญญา : " + (dt.Rows[0]["NAME"] + "").Trim() + " ถูกนำไปใช้ในการสร้างสัญญาแล้ว");
            }
        }
        catch (Exception ex)
        {
            alertFail("บันทึกไม่สำเร็จ : " + ex.Message);
        }
    }
}