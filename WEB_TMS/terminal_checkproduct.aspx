﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="terminal_checkproduct.aspx.cs" Inherits="terminal_checkproduct" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
// <![CDATA[

        //Upload File Control
        var fieldSeparator = "|";
        var fieldPath = "#";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }

        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexPath = e.callbackData.indexOf(fieldPath);
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + (fieldSeparator.length), indexPath);
                var sPath = e.callbackData.substring(indexPath + (fieldPath.length));
                var date = new Date();
                var imgSrc = sPath + pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }


        function UpdateCheckListItem(ctrlid, contractid, truckid, checklistid, scheckid) {
            var inner_HTML = $('#' + ctrlid)[0].innerHTML;
            $('#' + ctrlid)[0].innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            $('#' + ctrlid).css({ 'text-decoration': 'none' });
            $('#' + ctrlid).addClass('loading_algRight');
            var datas = contractid + '^' + truckid + '^' + checklistid + '^' + scheckid;
            jQuery.ajax({
                type: "POST",
                url: "ashx/Terminal_CheckList.ashx",
                cache: false,
                async: false,
                dataType: "html",
                data: { contractid: encodeURIComponent(contractid), data: encodeURIComponent(datas) },
                error: function (response) {
                    $('#' + ctrlid).removeClass('loading_algRight');
                    alert(response);
                },
                success: function (response) {
                    if (response == '1') {
                        $('#' + ctrlid)[0].innerHTML = inner_HTML;
                        $('#' + ctrlid).hide();
                    }

                    $('#' + ctrlid).removeClass('loading_algRight');
                }
            });
        }

        function RemoveFileOnServer(ctrlid, contractid, truckid, scheckid, nattachment) {
            ctrlid = ctrlid + "" + scheckid + "_" + truckid + "_" + contractid + "_" + nattachment;
            var inner_HTML = $('#' + ctrlid)[0].innerHTML;

            $('#' + ctrlid).addClass('loading_algRight');
            var datas = contractid + '^' + truckid + '^' + scheckid + '^' + nattachment;
            jQuery.ajax({
                type: "POST",
                url: "ashx/RemoveFileOnServer.ashx",
                cache: false,
                async: false,
                dataType: "html",
                data: { data: encodeURIComponent(datas) },
                error: function (response) {
                    $('#' + ctrlid).removeClass('loading_algRight');
                    alert(response);
                },
                success: function (response) {
                    if (response == '1') {
                        $('#' + ctrlid).hide();
                        $('#' + ctrlid).removeClass('loading_algRight');
                    }
                }
            });
        }
// ]]> 
    </script>
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="50%">
                            <dx:ASPxButton ID="btnAdd" runat="server" AutoPostBack="False" Text="ยืนยันการตรวจสภาพรถ"
                                Width="150px">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('senddata')}"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td style=" width:180px;">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="180px" NullText="ค้นหาจาก ทะเบียนรถ">
                            </dx:ASPxTextBox>
                        </td>
                        <td  style="display:none;">
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ValidationGroup="search">
                                    <RequiredField ErrorText="กรุณาระบุ" IsRequired="true" />
                                    <RequiredField IsRequired="True" ErrorText="กรุณาระบุ"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td style="display:none;">
                            - </td>
                        <td style=" width:80px;">
                            <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ValidationGroup="search">
                                    <RequiredField ErrorText="กรุณาระบุ" IsRequired="true" />
                                    <RequiredField IsRequired="True" ErrorText="กรุณาระบุ"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td  style=" width:120px;">
                            <dx:ASPxComboBox ID="cboTerminal" runat="server" ClientInstanceName="cboTerminal"
                                DataSourceID="sqlTerminal1" TextField="STERMINALNAME" ValueField="STERMINALID"
                                Width="120px">
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sqlTerminal1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT t.STERMINALID, t.STERMINALID||' - '||ts.STERMINALNAME STERMINALNAME FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON t.STERMINALID = ts.STERMINALID WHERE 1=1 AND NVL(t.STERMINALID, :S_TERMINALID) LIKE NVL( CASE WHEN Length(:S_TERMINALID)>5 THEN NULL ELSE :S_TERMINALID END,'')||'%' ORDER BY t.STERMINALID">
                                <SelectParameters>
                                    <asp:SessionParameter SessionField="SVDID" Name=":S_TERMINALID" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td  style=" width:100px; display:none;">
                            <dx:ASPxComboBox ID="cboTimeWindow" runat="server" Width="100px" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Text="ระบุเที่ยว" Value="" />
                                    <dx:ListEditItem Text="เที่ยวที่ 1 เวลา 09.00-12.00 น." Value="1" />
                                    <dx:ListEditItem Text="เที่ยวที่ 2 เวลา 14.00-16.00 น." Value="2" />
                                    <dx:ListEditItem Text="เที่ยวที่ 3 เวลา 18.00-20.00 น." Value="3" />
                                    <dx:ListEditItem Text="เที่ยวที่ 4 เวลา 04.00-07.00 น." Value="4" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td  style=" width:80px;">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="true"
                                ValidationGroup="search">
                                <ClientSideEvents Click="function (s, e) { if(ASPxClientEdit.ValidateGroup('search')){ xcpn.PerformCallback('Search');} }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" align="right" style="color: Red">
                            มีรายการรอตรวจสภาพรถ/สินค้า
                            <dx:ASPxLabel ID="lblConfirmCar" runat="server" Text="0">
                            </dx:ASPxLabel>
                            &nbsp;รายการ </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" ClientInstanceName="gvw" AutoGenerateColumns="False"
                                Width="100%" KeyFieldName="SKEYID" SkinID="_gvw" DataSourceID="sds" OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared"
                                OnAfterPerformCallback="gvw_AfterPerformCallback" OnHtmlRowPrepared="gvw_HtmlRowPrepared">
                                <Columns>
                                      <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="1%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="cbSelectAll" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ลำดับที่" Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SDELIVERYNO" Caption="Outbound" Visible="true" Width="9%">
                                      <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DDELIVERY" Caption="วันที่จัดส่ง" Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="STIMEWINDOW" Caption="เที่ยว" Visible="true"
                                        Width="3%">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เวลาปลายทาง" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ข้อมูลรถ">
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนรถ<br>(หัว)"
                                                ReadOnly="True" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนรถ<br>(ท้าย)"
                                                ReadOnly="True" Width="8%">
                                                <CellStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="สถานะ<br>รถ" ReadOnly="True" Width="4%">
                                                <DataItemTemplate>
                                                    <dx:ASPxLabel ID="lblStatusCar" ClientInstanceName="lblStatusCar" runat="server"
                                                        Text='<%# (""+Eval("sStatus")=="HOLD"?"ห้ามวิ่ง":(""+Eval("sStatus")=="OK"?"ปกติ":"แก้ไขภายใน "+Eval("NDAY_MA")+" วัน")) %>'>
                                                    </dx:ASPxLabel>
                                                </DataItemTemplate>
                                                <CellStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn FieldName="NVALUE" Caption="ปริมาณ<br>สินค้า" Visible="true" Width="9%">
                                      <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ<br>ข้อมูล" Width="3%" Visible="true">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lblcConfirm" ClientInstanceName="lblStatusCar" runat="server" Text='<%# (""+Eval("CCHECKTRUCKB")=="1"?"ยืนยันแล้ว":"รอยืนยัน") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เวลาถึงปลายทาง" Width="3%" Visible="true">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox ID="txtconfirm" runat="server" ClientInstanceName="txtconfirm" Text='<%# (""+Eval("CCONFIRM")=="0"?"0":"1") %>'
                                                ClientVisible="false">
                                            </dx:ASPxTextBox>
                                            <dx:ASPxTimeEdit ID="dteArrive" runat="server" ClientInstanceName="dteArrive" DisplayFormatString="dd/MM/yyyy HH.mm"
                                                EditFormat="DateTime" EditFormatString="dd/MM/yyyy HH.mm" Width="135px">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ValidationGroup="save">
                                                    <RequiredField ErrorText="กรุณาระบุ" IsRequired="true" />
                                                    <RequiredField IsRequired="True" ErrorText="กรุณาระบุ"></RequiredField>
                                                </ValidationSettings>
                                            </dx:ASPxTimeEdit>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ใบตรวจสภาพรถ" ReadOnly="True" Width="18%">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox ID="txtPassed" runat="server" ClientInstanceName="txtPassed" Text='<%# (""+Eval("CPASSED")=="0")?"0":"1"  %>'
                                                ClientVisible="false">
                                            </dx:ASPxTextBox>
                                            <dx:ASPxButton ID="imbPassed" runat="server" SkinID="_passed" ClientInstanceName="imbPassed"
                                                CausesValidation="False" CssClass="dxeLineBreakFix" ClientEnabled="false" Text=" ผ่าน ">
                                            </dx:ASPxButton>
                                            <dx:ASPxButton ID="imbIssue" runat="server" SkinID="_issue" ClientInstanceName="imbIssue"
                                                CausesValidation="False" CssClass="dxeLineBreakFix">
                                                <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('CHECKTRUCK$'+s.name.substring(s.name.split('imbIssue')[0].lastIndexOf('_')+1,s.name.length).split('_')[1]+'$imbIssue'); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ใบตรวจสินค้า" ReadOnly="True" Width="23%" >
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox ID="txtPassed" runat="server" ClientInstanceName="txtPassed" Text='<%# (""+Eval("CPASSED")=="0")?"0":"1"  %>'
                                                ClientVisible="false">
                                            </dx:ASPxTextBox><div style="display:none;">
                                            <dx:ASPxButton ID="imbPassed" runat="server" SkinID="_passed" ClientInstanceName="imbPassed"
                                                CausesValidation="False" CssClass="dxeLineBreakFix" ClientEnabled="false" Text=" ผ่าน ">
                                            </dx:ASPxButton>
                                           <%-- gvw.PerformCallback('STARTEDIT$'+s.name.substring(s.name.split('imbIssueProd')[0].lastIndexOf('_')+1,s.name.length).split('_')[1]+'$imbIssueProd'); --%>
                                            <dx:ASPxButton ID="imbIssueProd" runat="server" SkinID="_issue" ClientInstanceName="imbIssue"
                                                CausesValidation="False" CssClass="dxeLineBreakFix">
                                                <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('CHECKTRUCK;'+s.name.substring(s.name.split('imbIssueProd')[0].lastIndexOf('_')+1,s.name.length).split('_')[1]);}" />
                                            </dx:ASPxButton></div>
                                            <dx:ASPxButton ID="imbReportIssue" runat="server" ClientInstanceName="imbReportIssue"  Text="แจ้งปัญหาสินค้า"
                                                CausesValidation="False" CssClass="dxeLineBreakFix" AutoPostBack="false" >
                                                <ClientSideEvents Click="function(s,e){  xcpn.PerformCallback('complain$'+s.name.substring(s.name.split('imbReportIssue')[0].lastIndexOf('_')+1,s.name.length).split('_')[1]+'$imbReportIssue'); }" />
                                            </dx:ASPxButton>
                                            <dx:ASPxButton ID="imbReportCarBan" runat="server"  ClientInstanceName="imbReportCarBan" Text="แจ้งรถตกค้าง"
                                                CausesValidation="False" CssClass="dxeLineBreakFix" AutoPostBack="false" >
                                                <ClientSideEvents Click="function(s,e){  xcpn.PerformCallback('carban$'+s.name.substring(s.name.split('imbReportCarBan')[0].lastIndexOf('_')+1,s.name.length).split('_')[1]+'$imbReportCarBan'); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn> 
                                    <dx:GridViewDataTextColumn FieldName="STIMEWINDOW" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="sStatus" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CCONFIRM" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CPASSED" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SHEADID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="STRAILID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NPLANID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SPLANLISTID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NDROP" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CCHECKTRUCKA" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CCHECKTRUCKB" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="dArrive" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SSHIPTO" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="STERMINALID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SHIPMENT_NO" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True" Mode="ShowAllRecords">
                                </SettingsPager>
                                <Templates>
                                    <EditForm>
                                        <dx:ASPxPageControl ID="pageControl" ClientInstanceName="pageControl" runat="server"
                                            EnableCallBacks="false" Width="100%" ActiveTabIndex="0">
                                            <TabPages>
                                                <dx:TabPage Text="ปัญหาการขนส่ง" Visible="true">
                                                    <ContentCollection>
                                                        <dx:ContentControl ID="ctctrl1" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxGridView ID="gvwGroupCheckList" runat="server" ClientInstanceName="gvwGroupCheckList"
                                                                            SkinID="_gvwChild" AutoGenerateColumns="FALSE" KeyFieldName="SKEYID" OnHtmlDataCellPrepared="gvwGroupCheckList_HtmlDataCellPrepared">
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn Caption="ปัญหา" ReadOnly="True" Width="18%">
                                                                                    <DataItemTemplate>
                                                                                        <dx:ASPxLabel ID="lblGroupname" runat="server" ClientInstanceName="lblGroupname"
                                                                                            Font-Bold="true" Text='<%# Eval("SGROUPNAME") %>'>
                                                                                        </dx:ASPxLabel>
                                                                                        <br />
                                                                                        <dx:ASPxGridView ID="gvwItemCheckList" runat="server" ClientInstanceName="gvwItemCheckList"
                                                                                            SkinID="_gvwChild" AutoGenerateColumns="FALSE" KeyFieldName="SKEYID">
                                                                                            <Columns>
                                                                                                <dx:GridViewDataTextColumn VisibleIndex="0">
                                                                                                    <DataItemTemplate>
                                                                                                        <table width="100%">
                                                                                                            <tr>
                                                                                                                <td style="width: 40%;">
                                                                                                                    <dx:ASPxCheckBox ID="cbxSCHECKLISTID" ClientInstanceName="cbxSCHECKLISTID" runat='server'
                                                                                                                        Text='<%# Eval("SCHECKLISTNAME") %>'>
                                                                                                                        <ClientSideEvents CheckedChanged="function(s,e){ if (s.GetValue() == true) { var cflag = document.getElementById(s.name.replace('cbxSCHECKLISTID', 'txtSCHECKLISTID') + '_I').value;  if (cflag == '2') { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = 'none'; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = ''; }else if (cflag == '1') { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = ''; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = 'none'; } else { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = 'none'; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = 'none';} }else { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = 'none'; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = 'none'; }  }" />
                                                                                                                    </dx:ASPxCheckBox>
                                                                                                                    <dx:ASPxTextBox ID="txtIsChecked" runat="server" ClientVisible="false" Text=""> </dx:ASPxTextBox>
                                                                                                                    <dx:ASPxTextBox ID="txtSCHECKLISTID" runat="server" Text='<%# (Eval("CBAN")+""=="1")?"2":((Eval("NDAY_MA")+""!="")?"1":"0") %>'
                                                                                                                        ClientVisible="false">
                                                                                                                    </dx:ASPxTextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 25%; text-align: center;">
                                                                                                                    <dx:ASPxLabel ID="lblCHOLD" ClientInstanceName="lblCHOLD" ForeColor="Red" runat="server"
                                                                                                                        Text="ห้ามวิ่ง" ClientVisible="false">
                                                                                                                    </dx:ASPxLabel>
                                                                                                                    <dx:ASPxLabel ID="lblMAnDay" ClientInstanceName="lblMAnDay" ForeColor="#fecd85" runat="server"
                                                                                                                        Text='<%# (Eval("NDAY_MA")+""=="")?"":"แก้ไขภายใน "+Eval("NDAY_MA")+" วัน" %>'
                                                                                                                        ClientVisible="false">
                                                                                                                    </dx:ASPxLabel>
                                                                                                                </td>
                                                                                                                <td style="width: 35%; text-align: right;" class="divTable">
                                                                                                                    <asp:Label ID="lblCheckBy" runat="server"></asp:Label>
                                                                                                                    <dx:ASPxHyperLink ID="lnkEdited" ClientInstanceName="lnkEdited" Cursor="pointer"
                                                                                                                        title="คลิีกเพื่อปรับสถานะเป็นตรวจสอบเรียบร้อย" ImageUrl="~/Images/btnLoop1.gif"
                                                                                                                        runat="server">
                                                                                                                    </dx:ASPxHyperLink>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </DataItemTemplate>
                                                                                                    <FooterTemplate>
                                                                                                    </FooterTemplate>
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn Caption="SVERSIONLIST" FieldName="SVERSIONLIST" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="SVERSION" FieldName="SVERSION" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="STOPICID" FieldName="STOPICID" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="SCHECKLISTNAME" FieldName="SCHECKLISTNAME" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="SCHECKLISTID" FieldName="SCHECKLISTID" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="NLIST" FieldName="NLIST" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="NDAY_MA" FieldName="NDAY_MA" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="NPOINT" FieldName="NPOINT" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="CCUT" FieldName="CCUT" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="CBAN" FieldName="CBAN" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="CL_CACTIVE" FieldName="CL_CACTIVE" Visible="false" />
                                                                                                <dx:GridViewDataTextColumn Caption="STYPECHECKLISTID" FieldName="STYPECHECKLISTID"
                                                                                                    Visible="false" />
                                                                                            </Columns>
                                                                                            <Settings ShowColumnHeaders="false" />
                                                                                            <SettingsPager Mode="ShowAllRecords" ></SettingsPager>
                                                                                        </dx:ASPxGridView>
                                                                                    </DataItemTemplate>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="SGROUPID" FieldName="SGROUPID" Visible="false" />
                                                                                <dx:GridViewDataTextColumn Caption="SGROUPNAME" FieldName="SGROUPNAME" Visible="false" />
                                                                                <dx:GridViewDataTextColumn Caption="COIL" FieldName="COIL" Visible="false" />
                                                                                <dx:GridViewDataTextColumn Caption="CGAS" FieldName="CGAS" Visible="false" />
                                                                                <dx:GridViewDataTextColumn Caption="GOCL_CACTIVE" FieldName="GOCL_CACTIVE" Visible="false" />
                                                                            </Columns>
                                                                        </dx:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <dx:ASPxButton ID="btnsubmit" ClientInstanceName="btnsubmit" runat="server" SkinID="_submit"
                                                                            CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {  gvw.PerformCallback('SAVE$'+s.name.substring(s.name.split('btnsubmit')[0].lastIndexOf('_')+1,s.name.length).split('_')+'$1'); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnclose" ClientInstanceName="btnclose" runat="server" SkinID="_close"
                                                                            CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {gvw.CancelEdit(); }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </dx:ContentControl>
                                                    </ContentCollection>
                                                </dx:TabPage>
                                                <dx:TabPage Text="หลักฐานการกระทำผิดและปัญหาอื่นๆ" Visible="true">
                                                    <ContentCollection>
                                                        <dx:ContentControl ID="ctctrl2" runat="server">
                                                            <table cellpadding="2" cellspacing="1" style="border-collapse: collapse; width: 100%">
                                                                <tr>
                                                                    <td style="width: 48%">
                                                                        <b>ปัญหาอื่นๆ</b></td>
                                                                    <td style="width: 50%">
                                                                        <b>รายละเอียดปัญหา</b> <span id="spnNMA" runat="server"><%--ต้องแก้ไขสภาพภายใน--%>
                                                                            <dx:ASPxTextBox ID="txtNMA" runat="server" Width="25px" Text="0" CssClass="dxeLineBreakFix" ClientVisible="false">
                                                                            </dx:ASPxTextBox>
                                                                            <%--วัน--%> </span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="cbxOtherIssue" runat='server' Text="ปัญหาอื่นๆ ระบุปัญหา">
                                                                        </dx:ASPxCheckBox>
                                                                        <br />
                                                                        <dx:ASPxMemo ID="txtOtherIssue" ClientInstanceName="txtOtherIssue" Height="90px"
                                                                            Width="75%" runat="server" MaxLength="2000">
                                                                        </dx:ASPxMemo>
                                                                    </td>
                                                                    <td>
                                                                        <br />
                                                                        <br />
                                                                        <dx:ASPxMemo ID="txtDetailIssue" ClientInstanceName="txtDetailIssue" Height="90px"
                                                                            Width="75%" runat="server" MaxLength="2000">
                                                                        </dx:ASPxMemo>
                                                                    </td>
                                                                </tr>
                                                                <tr id="tr_ProdIssue" runat="server" style="display: none;">
                                                                    <td colspan="2">
                                                                        <table width="99%" border='0' cellspacing='0' cellpadding='0'>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <b>ปัญหาคุณภาพ / ปริมาณน้ำมันปลายทาง</b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <dx:ASPxCheckBox ID="cbxHasDirty" runat='server' Text="คุณภาพของน้ำมันเสียหาย">
                                                                                    </dx:ASPxCheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:TextBox ID="txtLossID" runat="server" Visible="false"></asp:TextBox>
                                                                                    <dx:ASPxCheckBox ID="cbxHasLoss" runat='server' Text="เกิดการสูญหายของผลิตภัณฑ์">
                                                                                    </dx:ASPxCheckBox>
                                                                                    <span id="spn_Loss" runat="server">
                                                                                        <dx:ASPxComboBox ID="cbxProduct" runat="server" CssClass="dxeLineBreakFix" ClientInstanceName="cbxProduct"
                                                                                            DataSourceID="sdsProduct" TextField="PROD_ABBR" ValueField="PROD_ID" DropDownStyle="DropDown"
                                                                                            IncrementalFilteringMode="Contains" FilterMinLength="0">
                                                                                        </dx:ASPxComboBox>
                                                                                        <dx:ASPxTextBox ID="txtnLoss" Width="80px" runat="server" CssClass="dxeLineBreakFix">
                                                                                        </dx:ASPxTextBox>
                                                                                        <dx:ASPxComboBox ID="cbxProdUnits" runat="server" CssClass="dxeLineBreakFix" ClientInstanceName="cbxProdUnits"
                                                                                            Width="100px" DataSourceID="sdsProductUnits" TextField="SPRODUCTUNITNAME" ValueField="NPRODUCTUNITID"
                                                                                            DropDownStyle="DropDown" IncrementalFilteringMode="Contains" FilterMinLength="0">
                                                                                        </dx:ASPxComboBox>
                                                                                    </span></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="width: 50%">
                                                                        <b>หลักฐานการกระทำผิด</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top"><div title="Allowed file types: <%= Resources.CommonResource.FileUploadType.Replace("."," ") %>">Allowed file types: <%= Resources.CommonResource.FileUploadType.Replace("."," ").Substring(0,30) %> ฯลฯ<br>Max file size: <%= Resources.CommonResource.TooltipMaxFileSize1MB %></div>
                                                                        <dx:ASPxUploadControl ID="UploadControl" runat="server" ShowAddRemoveButtons="True"  
                                                                            Width="95%" ShowUploadButton="True" AddUploadButtonsHorizontalPosition="Right"
                                                                            ShowClearFileSelectionButton='true' ShowProgressPanel="True" ClientInstanceName="UploadControl"
                                                                            OnFileUploadComplete="UploadControl_FileUploadComplete" FileInputCount="3" AddButton-ImagePosition="Right" > 
                                                                            <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>" AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                            </ValidationSettings> 
                                                                            <ClientSideEvents FileUploadComplete="function(s, e) { FileUploaded(s, e) }" FileUploadStart="function(s, e) { FileUploadStart(); }" />
                                                                        </dx:ASPxUploadControl>
                                                                        <%--FileUploadComplete="function(s, e) { FileUploaded(s, e) }"--%>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="45%" ClientInstanceName="RoundPanel"
                                                                            HeaderText="Uploaded files" Height="100%" CssClass="dxeLineBreakFix">
                                                                            <PanelCollection>
                                                                                <dx:PanelContent ID="PanelContent1" runat="server">
                                                                                    <div id="uploadedListFiles" style="height: 150px; font-family: Arial;">
                                                                                    </div>
                                                                                    <%--<div id="dvClearFile" style="vertical-align: bottom; text-align: right;">
                                                                                                            <a href="javascript:void(0);" onclick="javascript: document.getElementById('uploadedListFiles').innerHTML=''; ">
                                                                                                                Clear</a>
                                                                                                        </div>--%>
                                                                                </dx:PanelContent>
                                                                            </PanelCollection>
                                                                        </dx:ASPxRoundPanel>
                                                                        <dx:ASPxRoundPanel ID="rplAttachmented" runat="server" Width="45%" ClientInstanceName="rplAttachmented"
                                                                            HeaderText="Attachmented files" Height="100%" CssClass="dxeLineBreakFix">
                                                                            <PanelCollection>
                                                                                <dx:PanelContent ID="pnctTab2" runat="server">
                                                                                    <div id="AttachmentedListFiles" runat="server" style="height: 150px; font-family: Arial;">
                                                                                    </div>
                                                                                </dx:PanelContent>
                                                                            </PanelCollection>
                                                                        </dx:ASPxRoundPanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="right">
                                                                        <dx:ASPxButton ID="btnsubmitTab2" ClientInstanceName="btnsubmitTab2" runat="server"
                                                                            SkinID="_submit" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {  gvw.PerformCallback('SAVE$'+s.name.substring(s.name.split('btnclose')[0].lastIndexOf('_')+1,s.name.length).split('_')+'$2'); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btncloseTab2" ClientInstanceName="btncloseTab2" runat="server"
                                                                            SkinID="_close" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { gvw.CancelEdit(); }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </dx:ContentControl>
                                                    </ContentCollection>
                                                </dx:TabPage>
                                            </TabPages>
                                        </dx:ASPxPageControl>
                                        <dx:ASPxTextBox ID="txtMode" runat="server" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </EditForm>
                                </Templates>
                            </dx:ASPxGridView>
                            <dx:ASPxPopupControl ID="popupControl" runat="server" CloseAction="OuterMouseClick"
                                HeaderText="รายละเอียด" ClientInstanceName="popupControl" Width="600px" Modal="true"
                                SkinID="popUp">
                                <ContentCollection>
                                    <dx:PopupControlContentControl>
                                        <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                                            <tr>
                                                <td width='20'>
                                                    <img src='images/bd001_a.jpg' width='20' height='20'> </td>
                                                <td background='images/bd001_b.jpg'>
                                                    <img src='images/bd001_b.jpg' width='20' height='20'> </td>
                                                <td width='20' align='left' valign='top' background='images/bd001_i.jpg'>
                                                    <img src='images/bd001_c.jpg' width='20' height='20'> </td>
                                            </tr>
                                            <tr>
                                                <td width='20' align='left' valign='top' background='images/bd001_dx.jpg'>
                                                    <img src='images/bd001_d.jpg' width='20' height='164'> </td>
                                                <td align='left' valign='top' background='images/bd001_i.jpg' bgcolor='#FFFFFF' style='background-repeat: repeat-x'>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                รายละเอียดการส่งมอบ </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                                                </td>
                                                <td width='20' align='left' valign='top' background='images/bd001_ex.jpg'>
                                                    <img src='images/bd001_e.jpg' width='20' height='164'> </td>
                                            </tr>
                                            <tr>
                                                <td width='20'>
                                                    <img src='images/bd001_f.jpg' width='20' height='20'> </td>
                                                <td background='images/bd001_g.jpg'>
                                                    <img src='images/bd001_g.jpg' width='20' height='20'> </td>
                                                <td width='20'>
                                                    <img src='images/bd001_h.jpg' width='20' height='20'> </td>
                                            </tr>
                                        </table>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:ASPxPopupControl>
                            <asp:SqlDataSource ID="sds" runat="server" EnableCaching="True" CancelSelectOnNullParameter="false"  CacheKeyDependency="sds" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsProduct" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                CacheKeyDependency="ckdProduct" EnableCaching="True"></asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsProductUnits" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                CacheKeyDependency="ckdProductUnits" EnableCaching="True" SelectCommand="SELECT * FROM TPRODUCTUNIT ">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsCheckLists" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                CacheKeyDependency="ckdCheckLists" EnableCaching="True" CancelSelectOnNullParameter="False"
                                SelectCommand="SELECT rownum SKEYID, TOCL.STYPECHECKLISTID,TOCL.STYPECHECKLISTNAME,TOCL.CACTIVE TOCL_CACTIVE,TOCL.CCASE,TOCL.CDESPOA,TOCL.CDESPOB,TOCL.CSUREPRISE,TOCL.CVENDOR,TOCL.CTYPE,TOCL.SMENUID
 ,GOCL.SGROUPID ,GOCL.SGROUPNAME ,GOCL.CGAS ,GOCL.COIL ,GOCL.CACTIVE GOCL_CACTIVE
 ,CL.SVERSIONLIST ,CL.SVERSION ,CL.STOPICID ,CL.SCHECKLISTNAME ,CL.SCHECKLISTID ,CL.NLIST ,CL.NDAY_MA ,CL.CCUT ,CL.CBAN ,CL.CACTIVE CL_CACTIVE ,TP.NPOINT
FROM TTYPEOFCHECKLIST TOCL
LEFT JOIN TGROUPOFCHECKLIST GOCL ON  TOCL.STYPECHECKLISTID=GOCL.STYPECHECKLISTID
LEFT JOIN TCHECKLIST CL ON  TOCL.STYPECHECKLISTID=CL.STYPECHECKLISTID AND GOCL.SGROUPID=CL.SGROUPID
LEFT JOIN TTOPIC TP ON CL.STOPICID=TP.STOPICID
where 1=1 
AND NVL(TOCL.CACTIVE,'0')='1' AND NVL(GOCL.CACTIVE,'0')='1' AND NVL(CL.CACTIVE,'0')='1' AND NVL(TP.CACTIVE,'0')='1'
AND TOCL.CTYPE='1' AND  NVL(CDESPOB,'0')='1' 
--AND TOCL.STYPECHECKLISTID='1'"></asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsCheckListsProd" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                CacheKeyDependency="ckdCheckListsProd" EnableCaching="True" CancelSelectOnNullParameter="False"
                                SelectCommand="SELECT rownum SKEYID, TOCL.STYPECHECKLISTID,TOCL.STYPECHECKLISTNAME,TOCL.CACTIVE TOCL_CACTIVE,TOCL.CCASE,TOCL.CDESPOA,TOCL.CDESPOB,TOCL.CSUREPRISE,TOCL.CVENDOR,TOCL.CTYPE,TOCL.SMENUID
 ,GOCL.SGROUPID ,GOCL.SGROUPNAME ,GOCL.CGAS ,GOCL.COIL ,GOCL.CACTIVE GOCL_CACTIVE
 ,CL.SVERSIONLIST ,CL.SVERSION ,CL.STOPICID ,CL.SCHECKLISTNAME ,CL.SCHECKLISTID ,CL.NLIST ,CL.NDAY_MA ,CL.CCUT ,CL.CBAN ,CL.CACTIVE CL_CACTIVE ,TP.NPOINT
FROM TTYPEOFCHECKLIST TOCL
LEFT JOIN TGROUPOFCHECKLIST GOCL ON  TOCL.STYPECHECKLISTID=GOCL.STYPECHECKLISTID
LEFT JOIN TCHECKLIST CL ON  TOCL.STYPECHECKLISTID=CL.STYPECHECKLISTID AND GOCL.SGROUPID=CL.SGROUPID
LEFT JOIN TTOPIC TP ON CL.STOPICID=TP.STOPICID
where 1=1 
AND NVL(TOCL.CACTIVE,'0')='1' AND NVL(GOCL.CACTIVE,'0')='1' AND NVL(CL.CACTIVE,'0')='1' AND NVL(TP.CACTIVE,'0')='1'
AND TOCL.CTYPE='2' AND  NVL(CDESPOB,'0')='1' --AND TOCL.STYPECHECKLISTID='2'"></asp:SqlDataSource>
                            <asp:TextBox ID="txtUploadMode" runat="server" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
