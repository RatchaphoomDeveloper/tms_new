﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity;
using System.IO;
using OfficeOpenXml;

public partial class AnnualReportHistory : PageBase
{
    string GroupPermission = "01";

    #region " Prop "
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected DataTable dtStatus
    {
        get { return (DataTable)ViewState[this.ToString() + "dtStatus"]; }
        set { ViewState[this.ToString() + "dtStatus"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected DataTable VendorDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "VendorDDL"]; }
        set { ViewState[this.ToString() + "VendorDDL"] = value; }
    }

    protected FinalGade SearchCriteria
    {
        get { return (FinalGade)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }


    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            InitForm();
            LoadMain();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
                
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitForm()
    {
        try
        {
            int StartYear = 2015;
            int cYear = DateTime.Today.Year;
            int endYear = cYear + 4;
            int index = 0;
            for (int i = StartYear; i <= endYear; i++)
            {
                ddlYearSearch.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
            }

            // เลือกปีปัจจุบัน
            ListItem sel = ddlYearSearch.Items.FindByValue(cYear.ToString());
            ddlYearSearch.ClearSelection();
            if (sel != null) sel.Selected = true;
            else ddlYearSearch.SelectedIndex = 0;

            ddlVendorSearch.SelectedIndex = 0;
            LoadVendor();
            LoadStatus();

        }
        catch (Exception)
        {
            throw;
        }
    }


    public void LoadStatus()
    {
        try
        {
            string _err = string.Empty;
            dtStatus = new FinalGradeBLL().GetStatus(ref _err, true);
            chkStatus.DataTextField = "CONFIG_NAME";
            chkStatus.DataValueField = "CONFIG_VALUE";
            chkStatus.DataSource = dtStatus;
            chkStatus.DataBind();
            foreach (ListItem itm in chkStatus.Items) itm.Selected = true;
        }
        catch (Exception ex)
        {

            alertFail(ex.Message);
        }
    }
    public void LoadVendor()
    {
        try
        {
            VendorDDL = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = VendorDDL.NewRow();
            dr["SABBREVIATION"] = "";
            VendorDDL.Rows.InsertAt(dr, 0);
            ddlVendorSearch.DataSource = VendorDDL;
            ddlVendorSearch.DataBind();

            if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
            {
                ddlVendorSearch.ClearSelection();
                ListItem lst = ddlVendorSearch.Items.FindByValue(user_profile.SVDID);
                if (lst != null) lst.Selected = true;
                ddlVendorSearch.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }

    private void LoadMain()
    {
        try
        {
            string _err = string.Empty;
            SearchCriteria = new FinalGade() { YEAR = string.Empty, VENDORID = string.Empty };
            if (ddlYearSearch.SelectedItem != null) SearchCriteria.YEAR = ddlYearSearch.SelectedItem.Value;
            if (ddlVendorSearch.SelectedItem != null) SearchCriteria.VENDORID = ddlVendorSearch.SelectedItem.Value;

            var selected = chkStatus.Items.Cast<ListItem>().Where(li => li.Selected).Select(x => x.Value).ToList();
            if (selected != null) SearchCriteria.LstStatus = selected;
            SearhData = new FinalGradeBLL().GetHistory(ref _err, SearchCriteria);
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
        //    GotoDefault();
        //    return;
        //}
    }

    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {

        }
    }

    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblStatusVendor = e.Row.Cells[1].FindControl("lblStatusVendor") as Label;
                if (lblStatusVendor != null)
                {
                    string FIRST_VIEW = DataBinder.Eval(e.Row.DataItem, "FIRST_VIEW").ToString();
                    if (!string.IsNullOrEmpty(FIRST_VIEW)) lblStatusVendor.Text = "รับทราบผล";
                    else lblStatusVendor.Text = "ยังไม่รับผล";
                }

                var ID = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                var ImageView = e.Row.Cells[1].FindControl("ImageView") as ImageButton;
                if (ImageView != null)
                {
                    ImageView.Attributes.Add("onclick", "gotoView(\'" + ID + "\');");
                }

                var ImagePDF = e.Row.Cells[1].FindControl("ImagePDF") as ImageButton;
                if (ImagePDF != null)
                {
                    ImagePDF.Attributes.Add("onclick", "gotoNotice(\'" + ID + "\');");
                }
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        LoadMain();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadMain();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        InitForm();
        LoadMain();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        using (ExcelPackage pck = new ExcelPackage(new MemoryStream(), new MemoryStream(File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath("~/FileFormat/Admin/AnnualReportHistoryFormat.xlsx")))))
        {
            ExcelWorksheet ws = pck.Workbook.Worksheets[1];
            int StartRow = 3;
            int index = 1;
            foreach (DataRow dr in SearhData.Rows)
            {
                ws.Cells["A" + StartRow.ToString()].Value = "" + index.ToString();
                ws.Cells["B" + StartRow.ToString()].Value = dr["SABBREVIATION"].ToString();
                ws.Cells["C" + StartRow.ToString()].Value = "ผลประเมินการปปฏิบัติงานในรอบ 12 เดือน ประจำปี " + SearchCriteria.YEAR;
                ws.Cells["D" + StartRow.ToString()].Value = dr["DOCNO"].ToString();
                ws.Cells["E" + StartRow.ToString()].Value = dr["YEAR"].ToString();
                ws.Cells["F" + StartRow.ToString()].Value = dr["SENDER_NAME"].ToString();
                ws.Cells["G" + StartRow.ToString()].Value = dr["UPDATE_DATE_STR"].ToString();
                ws.Cells["H" + StartRow.ToString()].Value = dr["CONFIG_NAME"].ToString();
                string FIRST_VIEW = dr["FIRST_VIEW"].ToString();
                string lblStatusVendor = string.Empty;
                if (!string.IsNullOrEmpty(FIRST_VIEW)) lblStatusVendor = "รับทราบผล";
                else lblStatusVendor = "ยังไม่รับผล";
                ws.Cells["I" + StartRow.ToString()].Value = lblStatusVendor;

                StartRow++;
                index++;
            }

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=AnnualReportHistory.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }

    protected void btnExportPdf_Click(object sender, EventArgs e)
    {

        /*Response.AddHeader("content-disposition",
    "attachment;filename=GridViewExport.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        divEmail.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End(); */

        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            #region " Save Log For Vendor "
            try
            {
                string file_name = "view_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");

            }
            catch (Exception)
            {
                throw;
            }

            #endregion " Save Log For Vendor "

        }
        Byte[] res = null;
        //using (MemoryStream ms = new MemoryStream())
        //{
        //    var _strExport = string.Format(@"<table ><tr align='center'><td align='center'><div runat='server' style='width: 768px'>{0}</div></td></tr></table>", ContentForm);
        //    var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(_strExport, PdfSharp.PageSize.A4);
        //    pdf.Save(ms);
        //    res = ms.ToArray();
        //}

        Response.Clear();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Disposition", "attachment;filename=\"AnnualFileForm.pdf\"");

        Response.BinaryWrite(res);
        Response.Flush();
        Response.End();
        //divBody.InnerHtml = ContentForm;
        //int MarginSize = 45;
        //HtmlToPdf converter = new HtmlToPdf();
        //converter.Options.MarginTop = MarginSize;
        //converter.Options.MarginLeft = MarginSize;
        //converter.Options.MarginBottom = MarginSize;
        //converter.Options.MarginRight = MarginSize;
        //var sb = new StringBuilder();
        //divBody.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
        /////exgap1ch.jpeg
        //string s = sb.ToString();
        //// convert the url to pdf 
        //PdfDocument doc = converter.ConvertHtmlString(sb.ToString());

        //string Path = this.CheckPath();
        //string FileName = "Annual_Repoert_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".pdf";

        //doc.Save(Response, false, FileName);
        ////this.DownloadFile(Path, FileName);
        //doc.Close();
    }
    protected void grvMain_DataBound(object sender, EventArgs e)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        for (var i = 0; i < 5; i++)
        {
            TableHeaderCell cell = new TableHeaderCell();
            cell.Text = "";
            cell.ColumnSpan = 1;
            row.Controls.Add(cell);
        }

        TableHeaderCell cell1 = new TableHeaderCell();
        cell1.Text = "แจ้งผล ผขส.";
        cell1.ColumnSpan = 2;
        row.Controls.Add(cell1);
        int _endindex = user_profile.CGROUP == GROUP_PERMISSION.UNIT ? 4 : 3;
        for (var i = 0; i < _endindex; i++)
        {
            TableHeaderCell cell = new TableHeaderCell();
            cell.Text = "";
            cell.ColumnSpan = 1;
            row.Controls.Add(cell);
        }

        grvMain.HeaderRow.Parent.Controls.AddAt(0, row);

    }
    protected void btnVendorDownload_Click(object sender, EventArgs e)
    {

    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        string Id = hdnNum.Value;
        if (Id == "0") return;
        Button btn = sender as Button;
        if (btn.ID == "btnView")
        {
            string expression = string.Format(" ID='{0}' ", Id);
            DataRow[] foundRows = null;
            foundRows = SearhData.Select(expression);
            if (foundRows != null)
            {
                Session["AARN_SABBREVIATION"] = foundRows[0]["SABBREVIATION"];
            }
            Session["AARN_YEAR"] = SearchCriteria.YEAR;
            Session["AARN_HID"] = Id;
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
        else if (btn.ID == "btnNotice")
        {
            string expression = string.Format(" ID='{0}' ", Id);
            DataRow[] foundRows = null;
            foundRows = SearhData.Select(expression);
            if (foundRows != null)
            {
                Session["AARN_SABBREVIATION"] = foundRows[0]["SABBREVIATION"];
                Session["AARN_SVENDORID"] = foundRows[0]["VENDOR_ID"];
            }
            Session["AARN_YEAR"] = SearchCriteria.YEAR;
            Session["AARN_QDocID"] = Id;
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
    } 
}