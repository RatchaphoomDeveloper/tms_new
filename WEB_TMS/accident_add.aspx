﻿<%@ Page Title="" MasterPageFile="~/MP.Master" Language="C#" AutoEventWireup="true"
    CodeFile="accident_add.aspx.cs" Inherits="accident_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function VisibleControl() {

            var bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
            uploader0.SetClientVisible(bool);
            txtFileName0.SetClientVisible(!bool);
            btnView0.SetEnabled(!bool);
            btnDelFile0.SetEnabled(!bool);
            if (!(txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null)) {
                chkUpload1.SetValue('1');
            } else {
                chkUpload1.SetValue('');
            }

            bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
            uploader1.SetClientVisible(bool);
            txtFileName1.SetClientVisible(!bool);
            btnView1.SetEnabled(!bool);
            btnDelFile1.SetEnabled(!bool);
            if (!(txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null)) {
                chkUpload2.SetValue('1');
            } else {
                chkUpload2.SetValue('');
            }

            bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
            uploader2.SetClientVisible(bool);
            txtFileName2.SetClientVisible(!bool);
            btnView2.SetEnabled(!bool);
            btnDelFile2.SetEnabled(!bool);

            bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
            uploader3.SetClientVisible(bool);
            txtFileName3.SetClientVisible(!bool);
            btnView3.SetEnabled(!bool);
            btnDelFile3.SetEnabled(!bool);

            bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
            uploader4.SetClientVisible(bool);
            txtFileName4.SetClientVisible(!bool);
            btnView4.SetEnabled(!bool);
            btnDelFile4.SetEnabled(!bool);

            bool = txtFilePath5.GetValue() == "" || txtFilePath5.GetValue() == null;
            uploader5.SetClientVisible(bool);
            txtFileName5.SetClientVisible(!bool);
            btnView5.SetEnabled(!bool);
            btnDelFile5.SetEnabled(!bool);

            bool = txtFilePath6.GetValue() == "" || txtFilePath6.GetValue() == null;
            uploader6.SetClientVisible(bool);
            txtFileName6.SetClientVisible(!bool);
            btnView6.SetEnabled(!bool);
            btnDelFile6.SetEnabled(!bool);

            bool = txtFilePath7.GetValue() == "" || txtFilePath7.GetValue() == null;
            uploader7.SetClientVisible(bool);
            txtFileName7.SetClientVisible(!bool);
            btnView7.SetEnabled(!bool);
            btnDelFile7.SetEnabled(!bool);

            bool = txtFilePath8.GetValue() == "" || txtFilePath8.GetValue() == null;
            uploader8.SetClientVisible(bool);
            txtFileName8.SetClientVisible(!bool);
            btnView8.SetEnabled(!bool);
            btnDelFile8.SetEnabled(!bool);
        }

        function VisibleTab() {
            switch (txtTabVisible.GetValue()) {
                case "1":
                    PageControl.tabs[1].SetEnabled(false);
                    PageControl.tabs[2].SetEnabled(false);
                    PageControl.tabs[3].SetEnabled(false);
                    break;
                case "2":
                    PageControl.tabs[1].SetEnabled(true);
                    PageControl.tabs[2].SetEnabled(false);
                    PageControl.tabs[3].SetEnabled(false);
                    PageControl.SetActiveTabIndex(1);
                    break;
                case "3":
                    PageControl.tabs[1].SetEnabled(true);
                    PageControl.tabs[2].SetEnabled(true);
                    PageControl.tabs[3].SetEnabled(false);
                    PageControl.SetActiveTabIndex(2);
                    break;
                case "4":
                    PageControl.tabs[1].SetEnabled(true);
                    PageControl.tabs[2].SetEnabled(true);
                    PageControl.tabs[3].SetEnabled(true);
                    PageControl.SetActiveTabIndex(3);
                    break;
            }

        }
    </script>
    <style type="text/css">
        .style13
        {
            height: 24px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;VisibleTab();}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <dx:ASPxTextBox ID="txtTabVisible" ClientInstanceName="txtTabVisible" ClientVisible="false"
                    runat="server" Width="20px">
                </dx:ASPxTextBox>
                <dx:ASPxPageControl ID="PageControl" ClientInstanceName="PageControl" runat="server"
                    Width="100%" ActiveTabIndex="0">
                    <TabPages>
                        <dx:TabPage Name="t1" Text="ข้อมูลการเกิดเหตุ">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                        <tr>
                                            <td width="20%" height="23" bgcolor="#FFFFFF">
                                                วันที่ - เวลาเกิดเหตุ<font color="#FF0000">*</font>
                                            </td>
                                            <td width="30%">
                                                <div style="float: left">
                                                    <dx:ASPxDateEdit ID="dteDateAccident" ClientInstanceName="dteDateAccident" runat="server"
                                                        SkinID="xdte">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุวันที่เกิดเหตุ"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxDateEdit>
                                                </div>
                                                <div style="float: left">
                                                    เวลา
                                                </div>
                                                <div style="float: left">
                                                    <dx:ASPxTimeEdit ID="tdeAcccidentTime" runat="server" Width="60px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxTimeEdit>
                                                </div>
                                            </td>
                                            <td width="20%">
                                                วันที่ - เวลาแจ้งเหตุ
                                            </td>
                                            <td width="30%">
                                                <dx:ASPxTextBox ID="txtDate" runat="server" Border-BorderStyle="None" Width="100px">
                                                    <Border BorderStyle="None"></Border>
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                <span style="text-align: left">ทะเบียนรถ(หัว) <font color="#FF0000">*</font></span>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                <dx:ASPxComboBox ID="cboHeadRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboHeadRegist"
                                                    ItemStyle-Wrap="True" EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="180px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));txtVendorID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORID'));txtContractID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTID'));
                                                   };cboTrailerRegist.PerformCallback();cmbPersonalNo.PerformCallback();cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'))}">
                                                    </ClientSideEvents>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORNAME" Width="140px" />
                                                        <dx:ListBoxColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="รหัสรถ" FieldName="STRUCKID" Width="80px" />
                                                        <dx:ListBoxColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" Width="60px" />
                                                        <dx:ListBoxColumn Caption="รหัสผู้ประกอบการ" FieldName="SVENDORID" Width="100px" />
                                                    </Columns>
                                                    <ItemStyle Wrap="True"></ItemStyle>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุทะเบียนหัว"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                <span style="text-align: left">ทะเบียนรถ (ท้าย)</span>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                <dx:ASPxComboBox ID="cboTrailerRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboTrailerRegist"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="cboTrailerRegist_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboTrailerRegist_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="STRAILERREGISTERNO" Width="180px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <dx:ASPxTextBox ID="txtTruckID" ClientInstanceName="txtTruckID" runat="server" Width="10px"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtContractID" runat="server" ClientEnabled="False" ClientVisible="false"
                                                    ClientInstanceName="txtContractID" Width="10px">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtVendorID" runat="server" ClientEnabled="False" ClientVisible="false"
                                                    ClientInstanceName="txtVendorID" Width="10px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="23" bgcolor="#FFFFFF">
                                                <div style="float: left">
                                                    สำเนาบัตรประชาชน<font color="#FF0000">*</font></div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="chkUpload1" runat="server" CssClass="dxeLineBreakFix" ClientInstanceName="chkUpload1"
                                                        Width="1px" Border-BorderStyle="None" ForeColor="White">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                        </ValidationSettings>
                                                        <Border BorderStyle="None"></Border>
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel0" runat="server" ClientInstanceName="uploader0"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader0.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png,.zip,.rar หรือขนาดไฟล์ เกินกว่าที่กำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath0.SetValue((e.callbackData+'').split('|')[0]);txtFileName0.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName0" runat="server" Width="200px" ClientInstanceName="txtFileName0"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath0" runat="server" Width="200px" ClientInstanceName="txtFilePath0"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath0.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath0.GetValue() +';1');}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    แนบไฟล์สำเนาใบขับขี่<font color="#FF0000">*<br />
                                                        (กรณีเป็นรถก็าซต้องแนบใบ ปล2 ด้วย)</font></div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="chkUpload2" runat="server" CssClass="dxeLineBreakFix" ClientInstanceName="chkUpload2"
                                                        Width="1px" Border-BorderStyle="None" ForeColor="White">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                        </ValidationSettings>
                                                        <Border BorderStyle="None"></Border>
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel1" runat="server" ClientInstanceName="uploader1"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader1.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath1.SetValue((e.callbackData+'').split('|')[0]);txtFileName1.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName1" runat="server" Width="200px" ClientInstanceName="txtFileName1"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath1" runat="server" Width="200px" ClientInstanceName="txtFilePath1"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath1.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath1.GetValue() +';2');}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="23" bgcolor="#FFFFFF">
                                                พนักงานขับรถ<font color="#FF0000">*</font>
                                            </td>
                                            <td colspan="3">
                                                <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="30" ClientInstanceName="cmbPersonalNo"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SPERSONELNO" Width="240px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' == '-1'){s.SetValue('');}else{txtEmployeeID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SEMPLOYEEID'));}}">
                                                    </ClientSideEvents>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="หมายเลขบัตรประชาชน" FieldName="SPERSONELNO" />
                                                        <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                                        <dx:ListBoxColumn Caption="รหัสพนักงาน" FieldName="SEMPLOYEEID" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุรหัสบัตรประชาชน" IsRequired="True" />
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุรหัสบัตรประชาชน"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                                <dx:ASPxTextBox ID="txtEmployeeID" runat="server" ClientInstanceName="txtEmployeeID"
                                                    Width="10px" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="23" bgcolor="#FFFFFF">
                                                สำเนาใบ Invoice
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel7" runat="server" ClientInstanceName="uploader7"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader7.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png,.zip,.rar หรือขนาดไฟล์ เกินกว่าที่กำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath7.SetValue((e.callbackData+'').split('|')[0]);txtFileName7.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName7" runat="server" Width="200px" ClientInstanceName="txtFileName7"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath7" runat="server" Width="200px" ClientInstanceName="txtFilePath7"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView7" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView7" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath7.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile7" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile7" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath7.GetValue() +';8');}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                             <td height="23" bgcolor="#FFFFFF">
                                                หนังสือยินยอมชดใช้ค่าเสียหาย(ปริมาณน้ำมัน)
                                            </td>
                                            <td>
                                                  <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel8" runat="server" ClientInstanceName="uploader8"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader8.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png,.zip,.rar หรือขนาดไฟล์ เกินกว่าที่กำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath8.SetValue((e.callbackData+'').split('|')[0]);txtFileName8.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName8" runat="server" Width="200px"  ClientInstanceName="txtFileName8"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath8" runat="server" Width="200px" ClientInstanceName="txtFilePath8"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView8" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView8" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath8.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile8" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile8" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath8.GetValue() +';9');}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                สถานที่เกิดเหตุ
                                            </td>
                                            <td colspan="3" align="left" bgcolor="#FFFFFF">
                                                <dx:ASPxTextBox ID="txtACCIDENTLOCALE" runat="server" Width="500px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                รูปสถานที่เกิดเหตุ (จากGPS)
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel2" runat="server" ClientInstanceName="uploader2"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader2.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath2.SetValue((e.callbackData+'').split('|')[0]);txtFileName2.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName2" runat="server" Width="200px" ClientInstanceName="txtFileName2"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath2" runat="server" Width="200px" ClientInstanceName="txtFilePath2"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath2.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath2.GetValue() +';3');}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                รูปสถานที่เกิดเหตุ (รูปภาพสถานที่เกิดเหตุจริง)
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel3" runat="server" ClientInstanceName="uploader3"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader3.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath3.SetValue((e.callbackData+'').split('|')[0]);txtFileName3.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName3" runat="server" Width="200px" ClientInstanceName="txtFileName3"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath3" runat="server" Width="200px" ClientInstanceName="txtFilePath3"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath3.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath3.GetValue() +';4');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                <font color="#FF0000">ขั้นตอนการขนส่งขณะเกิดเหตุ</font>
                                            </td>
                                            <td colspan="3" align="left" bgcolor="#FFFFFF">
                                                <dx:ASPxRadioButtonList ID="rblTRANSPORTPROCESS" runat="server">
                                                    <Items>
                                                        <dx:ListEditItem Text="ระหว่างขนส่งสินค้า (รถหนัก บรรจุผลิตภัณฑ์ในรถ)" Value="1"
                                                            Selected="true" />
                                                        <dx:ListEditItem Text="ระหว่างเดินทางกลับมารับสินค้า (รถเบา ไม่มีผลิตภัณฑ์ในรถ)"
                                                            Value="2" />
                                                        <dx:ListEditItem Text="ระหว่างเข้ารับ/ลงสินค้า" Value="3" />
                                                    </Items>
                                                    <Border BorderStyle="None" />
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" valign="top">
                                                ลักษณะเหตุการณ์
                                            </td>
                                            <td colspan="3" align="left" bgcolor="#FFFFFF">
                                                <div style="float: left">
                                                    <dx:ASPxRadioButtonList ID="rblEVENT" runat="server" SkinID="rblStatus">
                                                        <Items>
                                                            <dx:ListEditItem Value="1" Text="เฉี่ยวชน" Selected="true" />
                                                            <dx:ListEditItem Value="2" Text="พลิกคว่ำ" />
                                                            <dx:ListEditItem Value="3" Text="ตกลงไหล่ทาง" />
                                                            <dx:ListEditItem Value="4" Text="อื่นๆ" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtEVENTOTHER" runat="server" Width="150px">
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                คู่กรณี
                                            </td>
                                            <td colspan="3" align="left" bgcolor="#FFFFFF">
                                                <div style="float: left">
                                                    <dx:ASPxRadioButtonList ID="rblLITIGANT" ClientInstanceName="rblLITIGANT" runat="server"
                                                        SkinID="rblStatus">
                                                        <Items>
                                                            <dx:ListEditItem Value="1" Text="ไม่มีคู่กรณี" Selected="true" />
                                                            <dx:ListEditItem Value="2" Text="มีคู่กรณี ระบุ" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtLITIGANT" runat="server" Width="150px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="ChkAdd">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุข้อมูล" IsRequired="True" />
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                ลักษณะความผิด
                                            </td>
                                            <td colspan="3" align="left" bgcolor="#FFFFFF">
                                                <dx:ASPxRadioButtonList ID="rblFAULT" runat="server" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Value="0" Text="ฝ่ายถูก" Selected="true" />
                                                        <dx:ListEditItem Value="1" Text="ฝ่ายผิด" />
                                                        <dx:ListEditItem Value="2" Text="ประมาทร่วม" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ประเภทความสูญเสีย
                                            </td>
                                            <td colspan="3" align="left" bgcolor="#FFFFFF">
                                                <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkVALUE" runat="server" Text="ทรัพย์สินบริษัทฯ เสียหาย รวมคิดเป็นเงิน">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtVALUE" runat="server" Width="150px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="add">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        บาท (หัว+หาง+สินค้า)
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkVALUETERMINAL" runat="server" Text="ทรัพย์สินคลัง/ลูกค้า เสียหาย">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtVALUETERMINAL" runat="server" Width="150px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="add">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        บาท
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkPRODUCT" runat="server" Text="ชนิดน้ำมันที่รั่วไหล">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtPRODUCT" runat="server" Width="150px">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        ปริมาณที่รั่วไหล
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtAMOUNT" runat="server" Width="150px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="add">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        ลิตร
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkENVIRONMENT" runat="server" Text="สิ่งแวดล้อม (">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkWATER" runat="server" Text="แหล่งน้ำสาธรณะ">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkCOMMUNITY" runat="server" Text="ชุมชน">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkFARM" runat="server" Text="สถานที่เพาะปลูก">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        )
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkFIRE" runat="server" Text="ไฟไหม้ (">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkVEHICLEFIRE" runat="server" Text="พาหนะขนส่ง/สินค้า">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkCOMMUNITYFIRE" runat="server" Text="ชุมชน">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        )
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkDAMAGEPRODUCT" runat="server" Text="สินค้าเสียหาย/ปนเปื้อน">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtDAMAGEPRODUCT" runat="server" Width="150px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="add">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        บาท
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkCORPORATE" runat="server" Text="เกิดผลกระทบต่อภาพลักษณ์องค์กร (">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkMEDIACOUNTRY" runat="server" Text="ออกสื่อทั่วประเทศ">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkMEDIACOMMUNITY" runat="server" Text="ออกสื่อชุมชน">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        )
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxCheckBox ID="chkPersonal" ClientInstanceName="chkPersonal" runat="server"
                                                                            Text="คน">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cmb1" runat="server" Width="150px">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="-กลุ่มคน-" />
                                                                                <dx:ListEditItem Value="1" Text="พนักงานบริษัทผู้ขนส่ง" />
                                                                                <dx:ListEditItem Value="2" Text="พนักงานลูกค้า" />
                                                                                <dx:ListEditItem Value="3" Text="บุคคลภายนอก" />
                                                                            </Items>
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="addItem">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RequiredField ErrorText="กรุณาระบุข้อมูล" IsRequired="True" />
                                                                                <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cmb2" runat="server" Width="150px">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="-ประเภทความบาดเจ็บ-" />
                                                                                <dx:ListEditItem Value="1" Text="บาดเจ็บ-ปฐมพยาบาล" />
                                                                                <dx:ListEditItem Value="2" Text="บาดเจ็บ-เข้ารับการรักษา" />
                                                                                <dx:ListEditItem Value="3" Text="ทุพพลภาพ-พิการ" />
                                                                                <dx:ListEditItem Value="4" Text="เสียชีวิต" />
                                                                            </Items>
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="addItem">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RequiredField ErrorText="กรุณาระบุข้อมูล" IsRequired="True" />
                                                                                <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtName" runat="server" Width="150px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="addItem">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RequiredField ErrorText="กรุณาระบุข้อมูล" IsRequired="True" />
                                                                                <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add">
                                                                            <ClientSideEvents Click="function(s,e){if(chkPersonal.GetChecked()){if(ASPxClientEdit.ValidateGroup('addItem')){sgvw.PerformCallback('AddList');};}}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" Settings-ShowColumnHeaders="false"
                                                                Style="margin-top: 0px" ClientInstanceName="sgvw" Width="100%" KeyFieldName="dtID"
                                                                OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" OnAfterPerformCallback="sgvw_AfterPerformCallback"
                                                                SkinID="_gvw">
                                                                <Columns>
                                                                    <dx:GridViewDataColumn Caption="รหัส" VisibleIndex="0" Width="3%">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                                ClientInstanceName="imbDel0" CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function (s, e) {sgvw.PerformCallback('Del;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                <Image Width="12px" Height="12px" Url="Images/del.gif">
                                                                                </Image>
                                                                            </dx:ASPxButton>
                                                                        </DataItemTemplate>
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="1%"
                                                                        VisibleIndex="0">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataColumn Caption="รหัส" FieldName="dtID" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataTextColumn VisibleIndex="4" Width="96%" FieldName="dd" CellStyle-ForeColor="#0066FF">
                                                                        <DataItemTemplate>
                                                                            คุณ&nbsp;
                                                                            <dx:ASPxLabel ID="lblScore1" runat="server" Text='<%#Eval("dtsName") %>'>
                                                                            </dx:ASPxLabel>
                                                                            &nbsp;
                                                                            <dx:ASPxLabel ID="lblScore2" runat="server" Text='<%#Eval("dtsGroup") %>'>
                                                                            </dx:ASPxLabel>
                                                                            -
                                                                            <dx:ASPxLabel ID="lblScore3" runat="server" Text='<%#Eval("dtsType") %>'>
                                                                            </dx:ASPxLabel>
                                                                        </DataItemTemplate>
                                                                        <CellStyle ForeColor="#0066FF">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                                <Settings GridLines="None" ShowColumnHeaders="False"></Settings>
                                                                <Border BorderStyle="None" />
                                                            </dx:ASPxGridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td colspan="2">
                                                <div style="float: left">
                                                    <dx:ASPxButton ID="ASPxButton5" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) {if(ASPxClientEdit.ValidateGroup('add') && ('' + rblLITIGANT.GetValue() == '1' ? true: ASPxClientEdit.ValidateGroup('ChkAdd'))){txtTabVisible.SetValue('2');xcpn.PerformCallback('Save');}else{ return false;}}" />
                                                    </dx:ASPxButton>
                                                </div>
                                                <div style="float: left">
                                                    <dx:ASPxButton ID="ASPxButton4" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                    </dx:ASPxButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t2" Text="ข้อมูลวิเคราะห์สาเหตุ" ClientEnabled="false">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                        <tr>
                                            <td colspan="4" class="active">
                                                1) ข้อมูลการบำรุงรักษา (ย้อนหลัง 1 ปี)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20%" bgcolor="#FFFFFF">
                                                ประวัติการตรวจสภาพรถประจำวัน <font color="#FF0000">*</font>
                                            </td>
                                            <td width="30%" align="left" bgcolor="#FFFFFF">
                                                <dx:ASPxRadioButtonList ID="rblINSPECTIONHISTORY" runat="server" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Value="1" Text="มี" Selected="true" />
                                                        <dx:ListEditItem Value="2" Text="ไม่มี" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td width="20%" align="left" bgcolor="#FFFFFF">
                                                แนบไฟล์การตรวจสภาพรถ
                                            </td>
                                            <td width="30%" align="left" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel4" runat="server" ClientInstanceName="uploader4"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader4.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath4.SetValue((e.callbackData+'').split('|')[0]);txtFileName4.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName4" runat="server" Width="200px" ClientInstanceName="txtFileName4"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath4" runat="server" Width="200px" ClientInstanceName="txtFilePath4"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath4.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath4.GetValue() +';5');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                ประวัติการซ่อมบำรุง <font color="#FF0000">*</font>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                <dx:ASPxRadioButtonList ID="rblMAINTENANCEHISTORY" runat="server" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Value="1" Text="มี" Selected="true" />
                                                        <dx:ListEditItem Value="2" Text="ไม่มี" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                แนบหลักฐานการซ่อมบำรุง
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel5" runat="server" ClientInstanceName="uploader5"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader5.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath5.SetValue((e.callbackData+'').split('|')[0]);txtFileName5.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName5" runat="server" Width="200px" ClientInstanceName="txtFileName5"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath5" runat="server" Width="200px" ClientInstanceName="txtFilePath5"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView5" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView5" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath5.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile5" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile5" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath5.GetValue() +';6');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="23" colspan="4" bgcolor="#FFFFFF">
                                                <span class="active">2) ข้อมูลการปฏิบัติงานขนส่ง</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                2.1 วัน-เวลาที่ พนักงานปฏิบัติงานครั้งสุดท้าย
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td valign="top">
                                                            <div style="float: left">
                                                                <dx:ASPxDateEdit ID="dteLASTWORK" ClientInstanceName="dteLASTWORK" runat="server"
                                                                    SkinID="xdte">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <ErrorFrameStyle ForeColor="Red">
                                                                        </ErrorFrameStyle>
                                                                        <RequiredField ErrorText="กรุณาระบุวันที่" IsRequired="True" />
                                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุวันที่"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                            </div>
                                                            <div style="float: left">
                                                                เวลา
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxTimeEdit ID="tedLASTWORK" runat="server" Width="60px">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <ErrorFrameStyle ForeColor="Red">
                                                                        </ErrorFrameStyle>
                                                                        <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTimeEdit>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                วัน-เวลาที่ พนักงานเริ่มปฏิบัติงานใหม่
                                            </td>
                                            <td valign="top">
                                                <div style="float: left">
                                                    <dx:ASPxDateEdit ID="dteBEGINWORK" ClientInstanceName="dteBEGINWORK" runat="server"
                                                        SkinID="xdte">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุวันที่" IsRequired="True" />
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุวันที่"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxDateEdit>
                                                </div>
                                                <div style="float: left">
                                                    เวลา
                                                </div>
                                                <div style="float: left">
                                                    <dx:ASPxTimeEdit ID="tedBEGINWORK" runat="server" Width="60px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTimeEdit>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                เริ่มเดินทางจาก
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtSTART" runat="server" Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                ปลายทาง
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtEND" runat="server" Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ระยะทางจากจุดเริ่มต้น-ปลายทาง
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtSTARTTODESTINATION" ClientInstanceName="txtSTARTTODESTINATION"
                                                        runat="server" Width="80px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    กม.</div>
                                            </td>
                                            <td>
                                                ระยะทางจากจุดเริ่มต้น-จุดเกิดเหตุ
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtSTARTTOACCIDENT" ClientInstanceName="txtSTARTTOACCIDENT" runat="server"
                                                        Width="80px">
                                                        <ClientSideEvents TextChanged="function(s,e){if(parseFloat(txtSTARTTODESTINATION.GetValue()) < parseFloat(s.GetValue())){s.SetValue('')} }" />
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    กม.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                จำนวน ชม. การขับรถติดต่อกันก่อนเกิดเหตุ
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtDRIVEHOUR" runat="server" Width="80px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    ชม.</div>
                                            </td>
                                            <td>
                                                มีการจอดรถในระหว่างการขนส่งหรือไม่
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    <dx:ASPxRadioButtonList ID="rblPARK" runat="server" SkinID="rblStatus">
                                                        <Items>
                                                            <dx:ListEditItem Value="1" Text="ไม่มี" Selected="true" />
                                                            <dx:ListEditItem Value="2" Text="มี จอดเป็นเวลา " />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtPARKTIME" runat="server" Width="80px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    ชม.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ระยะทางจากจุดจอดสุดท้าย-จุดเกิดเหตุ
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtPARKTOACCIDENT" ClientInstanceName="txtPARKTOACCIDENT" runat="server"
                                                        Width="80px">
                                                        <ClientSideEvents TextChanged="function(s,e){if(parseFloat(txtSTARTTODESTINATION.GetValue()) < parseFloat(s.GetValue())){s.SetValue('')} }" />
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    กม.</div>
                                            </td>
                                            <td>
                                                เกิดเหตุหลังจากทานอาหาร 1 ชม.
                                            </td>
                                            <td>
                                                <dx:ASPxRadioButtonList ID="rblAFTERONEHOUR" runat="server" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Value="1" Text="มี" Selected="true" />
                                                        <dx:ListEditItem Value="2" Text="ไม่มี" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ปริมาณแอลกอฮอล์ก่อนรับงาน
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtBEFOREJOB" runat="server" Width="80px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    มก.%</div>
                                            </td>
                                            <td>
                                                ปริมาณแอลกอฮอร์ ณ จุดเกิดเหตุ
                                            </td>
                                            <td>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtALCOHOLACCIDENT" runat="server" Width="80px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    มก.%</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ผลการตรวจสารเสพติด ณ จุดเกิดเหตุ
                                            </td>
                                            <td>
                                                <dx:ASPxRadioButtonList ID="rblDRUGACCIDENT" runat="server" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Value="1" Text="พบ" Selected="true" />
                                                        <dx:ListEditItem Value="2" Text="ไม่พบ" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td>
                                                แนบไฟล์ผลการตรวจ
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel6" runat="server" ClientInstanceName="uploader6"
                                                                NullText="Click here to browse files..." OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                Width="200px">
                                                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                </ValidationSettings>
                                                                <ClientSideEvents TextChanged="function(s,e){uploader6.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath6.SetValue((e.callbackData+'').split('|')[0]);txtFileName6.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                </ClientSideEvents>
                                                                <BrowseButton Text="แนบไฟล์">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                            <dx:ASPxTextBox ID="txtFileName6" runat="server" Width="200px" ClientInstanceName="txtFileName6"
                                                                ClientEnabled="false" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtFilePath6" runat="server" Width="200px" ClientInstanceName="txtFilePath6"
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnView6" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnView6" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath6.GetValue());}">
                                                                </ClientSideEvents>
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelFile6" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                ClientInstanceName="btnDelFile6" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath6.GetValue() +';7');}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" colspan="3">
                                                <span class="active">3) ข้อมูลสุขภาพพนักงานขับรถ</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ผลการตรวจสุขภาพประจำปี
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            สายตา
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblSIGHTCHECK" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ปกติ" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ไม่ปกติเนื่องจาก" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtSIGHTCHECK" runat="server" Width="100px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            โรคประจำตัว
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblDISEASECHECK" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ไม่มี" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="มี ระบุ" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtDISEASECHECK" runat="server" Width="100px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ผลการตรวจ ณ จุดเกิด เหตุ
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            มีอาการป่วยไข้
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblILLNESS" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="มี" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ไม่มี" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ทานยาที่ทำให้ง่วงซึม
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblMEDICINE" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ไม่ได้ทาน" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ทาน ครั้งสุดท้ายเมื่อเวลา" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTimeEdit ID="tedMEDICINE" runat="server" Width="60px">
                                                            </dx:ASPxTimeEdit>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF" colspan="4">
                                                <span class="active">4) การปฎิบัติตามหลักสูตร Defensive Driving Course (DDC)</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ผ่านการอบรมหลักสูตร DDC หรือไม่
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblDDC" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ผ่าน" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ไม่ผ่าน เนื่องจาก" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtNOTDDC" runat="server" Width="100px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ประสบการณ์ขับขี่ขนส่งวัตถุอันตราย
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtEXPERIENCEYEAR" runat="server" Width="80px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    &nbsp;ปี&nbsp;</div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtEXPERIENCEMONTH" runat="server" Width="80px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    &nbsp;เดือน</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                มีการใช้โทรศัพท์ระหว่างขับขี่
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblUSEPHONE" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ไม่ใช้" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ใช้ เนื่องจาก" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtUSEPHONE" runat="server" Width="100px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                การใช้ความเร็วในการขับขี่เกินกฎหมายกำหนด
                                            </td>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                <dx:ASPxRadioButtonList ID="rblSPEEDOVERLIMIT" runat="server" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Value="1" Text="ไม่ใช้" Selected="true" />
                                                        <dx:ListEditItem Value="2" Text="ใช้" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td valign="top">
                                                ความเร็วสุดท้ายก่อนเกิดเหตุ
                                            </td>
                                            <td valign="top">
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtSPEEDOVERLIMIT" runat="server" Width="100px">
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div style="float: left">
                                                    กม./ชม</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                สถาณการณ์ในการขับขี่ก่อนเกิดเหตุ
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblURGENT" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ไม่เร่งรีบ" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="เร่งรีบ เนื่องจาก" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtURGENT" runat="server" Width="300px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ป้ายสัญญาณเตือนต่างๆ ก่อนถึงจุดเกิดเหตุมีหรือไม่
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblWARNINGSIGN" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ไม่มี" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="มีแต่ไม่ได้สังเกต เนื่องจาก" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtWARNINGSIGN" runat="server" Width="300px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                ประวัติการเกิดอุบัติเหตุที่ผ่านมาใน 1 ปี
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <div style="float: left">
                                                    ครั้งที่ 1 เนื่องจาก &nbsp;</div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtACCIDENTHISTORY1" runat="server" Width="300px">
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="3">
                                                <div style="float: left">
                                                    ครั้งที่ 2 เนื่องจาก &nbsp;</div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtACCIDENTHISTORY2" runat="server" Width="300px">
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="3">
                                                <div style="float: left">
                                                    ครั้งที่ 3 เนื่องจาก &nbsp;</div>
                                                <div style="float: left">
                                                    <dx:ASPxTextBox ID="txtACCIDENTHISTORY3" runat="server" Width="300px">
                                                    </dx:ASPxTextBox>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF" class="style13">
                                                <span class="active">5) สภาพแวดล้อม ณ จุดเกิดเหตุ</span>
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF" class="style13">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#FFFFFF">
                                                สถานที่ ณ จุดเกิดเหตุ
                                            </td>
                                            <td colspan="3" valign="top" bgcolor="#FFFFFF">
                                                <dx:ASPxRadioButtonList ID="rblPLACE" runat="server" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Value="1" Text="สถานที่โล่ง" Selected="true" />
                                                        <dx:ListEditItem Value="2" Text="สถานที่แออัด/ในเมือง" />
                                                        <dx:ListEditItem Value="3" Text="สถานที่คับขัน/โค้งอันตราย/สะพาน/ถนนมีการก่อสร้างทางขึ้น/ลง เขา" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                ทัศนวิสัยในการขับขี่
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            แสงสว่าง
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblLIGHTING" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ขับรถย้อนแสง" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ขับรถตามแสง" />
                                                                    <dx:ListEditItem Value="3" Text="ขับรถในที่มืด (แสงสว่างไม่เพียงพอ)" />
                                                                    <dx:ListEditItem Value="4" Text="อื่นๆ" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtLIGHTING" runat="server" Width="200px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ฝนตก
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblRAIN" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ฝนตกเบาบาง (เริ่มตก/ใกล้หยุด)" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ฝนตกหนัก" />
                                                                    <dx:ListEditItem Value="3" Text="ฝนไม่ตก" />
                                                                    <dx:ListEditItem Value="4" Text="อื่นๆ" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtRAIN" runat="server" Width="250px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ความเคยชิน จุดที่เกิดเหตุเป็นเส้นทางประจำ
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblROUTINE" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Value="1" Text="ใช่" Selected="true" />
                                                                    <dx:ListEditItem Value="2" Text="ไม่ใช่" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td colspan="2">
                                                <div style="float: left">
                                                    <dx:ASPxButton ID="ASPxButton7" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false;txtTabVisible.SetValue(3); xcpn.PerformCallback('Save1');}" />
                                                    </dx:ASPxButton>
                                                </div>
                                                <div style="float: left">
                                                    <dx:ASPxButton ID="ASPxButton8" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                    </dx:ASPxButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t3" Text="ความเสียหาย">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                ความเสียหายที่เกิดขึ้น <font color="#ff0000">*</font>
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td width="120px">
                                                            ภาพลักษณ์องค์กร
                                                        </td>
                                                        <td>
                                                            &nbsp;ตัวคูณความรุนแรง<font color="#ff0000">*</font>&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cmbOrganizationEffect" runat="server" ClientInstanceName="cmbOrganizationEffect"
                                                                ItemStyle-Wrap="True" Width="100px" TextField="NIMPACTLEVEL" TextFormatString="{0}"
                                                                ValueField="NIMPACTLEVEL" CssClass="dxeLineBreakFix" DataSourceID="sdsCORGANIZATIONEFFECT">
                                                                <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                    SetFocusOnError="True" Display="Dynamic">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsCORGANIZATIONEFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '10') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style27" colspan="3">
                                                <table>
                                                    <tr>
                                                        <td width="120px">
                                                            เสียชีวิต
                                                        </td>
                                                        <td>
                                                            &nbsp;ตัวคูณความรุนแรง<font color="#ff0000">*</font>&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cmbDriverEffect" ClientInstanceName="cmbDriverEffect" runat="server"
                                                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" TextField="NIMPACTLEVEL" TextFormatString="{0}"
                                                                ValueField="NIMPACTLEVEL" Width="100px" DataSourceID="sdsDRIVEREFFECT">
                                                                <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                    SetFocusOnError="True" Display="Dynamic">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsDRIVEREFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '07') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td colspan="3" class="style19">
                                                <table>
                                                    <tr>
                                                        <td width="120px">
                                                            ทรัพย์สิน
                                                        </td>
                                                        <td>
                                                            &nbsp;ตัวคูณความรุนแรง<font color="#ff0000">*</font>&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cmbValueEffect" ClientInstanceName="cmbValueEffect" runat="server"
                                                                ItemStyle-Wrap="True" CssClass="dxeLineBreakFix" Width="100px" TextFormatString="{0}"
                                                                TextField="NIMPACTLEVEL" ValueField="NIMPACTLEVEL" DataSourceID="sdsVALUEEFFECT">
                                                                <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                    SetFocusOnError="True" Display="Dynamic">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsVALUEEFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '08') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td width="120px">
                                                            สิ่งแวดล้อม
                                                        </td>
                                                        <td>
                                                            &nbsp;ตัวคูณความรุนแรง<font color="#ff0000">*</font>&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cmbEnvironmentEffect" ClientInstanceName="cmbEnvironmentEffect"
                                                                ItemStyle-Wrap="True" runat="server" CssClass="dxeLineBreakFix" TextFormatString="{0}"
                                                                TextField="NIMPACTLEVEL" ValueField="NIMPACTLEVEL" Width="100px" DataSourceID="sdsENVIRONEMTNEFFECT">
                                                                <ClientSideEvents ValueChanged="function(s,e){if (cmbOrganizationEffect.GetValue() != null && cmbDriverEffect.GetValue() != null && cmbValueEffect.GetValue() != null && cmbEnvironmentEffect.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}"></ClientSideEvents>
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="50px" />
                                                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="200px" />
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                    SetFocusOnError="True" Display="Dynamic">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsENVIRONEMTNEFFECT" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE (SCATEGORYTYPEID = '09') AND (CACTIVE = '1') ORDER BY NIMPACTLEVEL">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ฝ่ายถูก / ผิด <font color="#ff0000">*</font>
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td width="120px">
                                                            <dx:ASPxComboBox ID="cbxTruePK" runat="server" SelectedIndex="0" Width="80px">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="True" Text="ฝ่ายถูก" Value="0" />
                                                                    <dx:ListEditItem Text="ฝ่ายผิด" Value="1" />
                                                                    <dx:ListEditItem Text="ประมาทร่วม" Value="2" />
                                                                </Items>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            หักคะแนน
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtScoreSubtract" runat="server" ClientEnabled="False" Width="50px">
                                                                <Border BorderColor="#CCCCCC" />
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            คะแนน
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" class="style21">
                                                ประมาณการความเสียหาย&nbsp;
                                            </td>
                                            <td class="style22">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtForecastDamage" runat="server" Width="130px">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            บาท
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;" class="style23">
                                                วงเงินปรับ
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;" class="style24">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtPenalty" runat="server" Width="130px">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            บาท
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 20%">
                                                ผู้บันทึก
                                            </td>
                                            <td style="width: 30%">
                                                <dx:ASPxTextBox ID="txtBeginHuman" runat="server" Width="200px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 15%;">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 35%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                ความคิดเห็นผู้สอบสวน
                                            </td>
                                            <td>
                                                <dx:ASPxMemo ID="txtComment" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                รายละเอียดการปฏิบัติผิดสัญญา
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                <dx:ASPxMemo ID="txtDetailWrongContract" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                การจัดการและการแก้ไข
                                            </td>
                                            <td>
                                                <dx:ASPxMemo ID="txtManageAndEdit" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                การดำเนินการป้องกัน
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                <dx:ASPxMemo ID="txtProtection" runat="server" Height="71px" Width="220px">
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false;txtTabVisible.SetValue(4); xcpn.PerformCallback('Save2');}" />
                                                </dx:ASPxButton>
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton2" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t4" Text="บันทึกตัดคะแนน">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                การนำไปใช้ประเมินผล <font color="#ff0000">*</font>
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxRadioButtonList ID="rblUse" runat="server" RepeatDirection="Horizontal" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="ใช้" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="ไม่ใช้" Value="0" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาเลือก" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                            </td>
                                            <td style="width: 40%">
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                สถานะจัดส่งรายงานการเกิดอุบัติเหตุ
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxRadioButtonList ID="rblDelivery" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="จัดส่งแล้ว" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="ไม่จัดส่ง" Value="0" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                สถานะปัจจุบัน <font color="#ff0000">*</font>
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxRadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="ดำเนินการ" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="อุทธรณ์" Value="2" />
                                                        <dx:ListEditItem Text="ปิดเรื่อง" Value="3" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาเลือก" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                <dx:ASPxCheckBox ID="chkReturnScore" runat="server" ClientInstanceName="chkReturnScore"
                                                    CheckState="Unchecked" Text="คลิกเลือกเพื่อคืนคะแนน">
                                                </dx:ASPxCheckBox>
                                            </td>
                                            <td style="width: 40%">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 30%">
                                                เหตุผลการคืนคะแนน
                                            </td>
                                            <td style="width: 40%">
                                                <dx:ASPxMemo ID="txtReturnScoreRemark" runat="server" Height="71px" Width="300px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="ChkAdd">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุเหตุผลการคืนคะแนน" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxMemo>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <dx:ASPxButton ID="ASPxButton3" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) {if(ASPxClientEdit.ValidateGroup('add') && (chkReturnScore.GetChecked() ? ASPxClientEdit.ValidateGroup('ChkAdd'):true)){xcpn.PerformCallback('Save3');}else{ return false;}}" />
                                                </dx:ASPxButton>
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton6" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'accident_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" align="left" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
