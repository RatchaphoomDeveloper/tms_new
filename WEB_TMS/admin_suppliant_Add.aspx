﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_suppliant_Add.aspx.cs" Inherits="admin_suppliant_Add" StylesheetTheme="Aqua" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="server">
    <div class="container" style="width: 100%">
        <div class="form-horizontal">
            <h2 class="page-header" style="margin-top: 10px; font-family: 'Angsana New'; color: #009120">
                <asp:Label Text="" ID="lblHeader" runat="server" />
            </h2>
            <ul class="nav nav-tabs" runat="server" id="tabtest">
                <li class="active" id="Tab1" runat="server">
                    <a href="#TabSuppliant" data-toggle="tab"
                        aria-expanded="true" runat="server" id="SuppliantTab">ยื่นอุทธรณ์</a>

                </li>
                <li class="" id="Tab2" runat="server">
                    <a href="#TabPTTDoc" data-toggle="tab" aria-expanded="false"
                        runat="server" id="PTTDocTab">ปตท.ขอเอกสารเพิ่มเติม</a>

                </li>

            </ul>
            <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
                <br />
                <br />
                <div class="tab-pane fade active in" id="TabSuppliant">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">
                                        <asp:Label Text="ข้อมูลอุทธรณ์" runat="server" ID="lblSearchResult" />&#8711;</a></a>
                            <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract2">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView ID="gvData"
                                                    runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                    <Columns>
                                                        <asp:BoundField HeaderText="รหัส" DataField="SAPPEALID" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="วันที่เกิดเหตุ" DataField="DINCIDENT" ShowHeader="true" DataFormatString="{0:dd/MM/yyyy}">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ชื่อผู้ประกอบการขนส่ง" DataField="SABBREVIATION" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ประเภท" DataField="SPROCESSNAME" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="หัวข้อปัญหา" DataField="STOPICNAME" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Delivery No" DataField="SDELIVERYNO" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="คะแนนที่ตัดก่อน" DataField="NPOINT">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="คะแนนที่ตัดหลัง" DataField="TOTAL_POINT">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ค่าปรับก่อน" DataField="COST">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ค่าปรับหลัง" DataField="TOTAL_COST">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField HeaderText="เวลาดำเนินการ<br/>(นับจากเกิดเหตุ)" HtmlEncode="false" DataField="DINCIDENT" ShowHeader="true" DataFormatString="{0:dd/MM/yyyy}">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="สถานะ<br/>การอุทธรณ์" HtmlEncode="false" DataField="STATUS" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField HeaderText="CHKSTATUS" DataField="CHKSTATUS" Visible="false"></asp:BoundField>
                                                        <asp:BoundField HeaderText="SVENDORID" DataField="SVENDORID" Visible="false"></asp:BoundField>
                                                        <asp:BoundField HeaderText="SAPPEALNO" DataField="SAPPEALNO" Visible="false"></asp:BoundField>
                                                        <asp:BoundField HeaderText="SPROCESSID" DataField="SPROCESSID" Visible="false"></asp:BoundField>
                                                    </Columns>

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer" style="text-align: right">
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseContractData" id="acollapseContractData">
                                        <asp:Label Text="ข้อมูลยื่นอุทธรณ์" ID="lblHeaderContract" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hiddencollapseContractData" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseContractData">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="รายละเอียดการขออุทธรณ์" runat="server" ID="lblSAPPEALTEXT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSAPPEALTEXT" TextMode="MultiLine" Rows="4" CssClass="form-control"/>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapse5" id="acollapse5">เอกสารแนบ&#8711;</a>

                                </div>
                                <div id="collapse5" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse6" id="acollapse6">เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ)&#8711;</a>
                                            </div>
                                            <div id="collapse6" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="dgvUploadRequestFile" DataKeyNames="UPLOAD_ID"
                                                                runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="แนบเอกสาร">
                                                                        <HeaderStyle Width="110px" />
                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkUploaded" runat="server" Enabled="false" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>

                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12 ">
                                                <label class="control-label col-md-4">
                                                    <asp:Label ID="lblUploadType" runat="server" Text="ประเภทไฟล์เอกสาร :&nbsp;"></asp:Label></label>
                                                <div class="col-md-3">
                                                    <asp:DropDownList ID="ddlUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:FileUpload ID="fileUpload1" runat="server" Width="100%" />
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" OnClick="cmdUpload_Click" />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:GridView ID="dgvUploadFile" OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                            DataKeyNames="UPLOAD_ID,FULLPATH" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/imgView.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/imgDelete.png" Width="23px"
                                                Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Width="80px" Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FULLPATH" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="hidSAPPEALID" runat="server" />
                            <asp:HiddenField ID="hidITYPE" runat="server" />
                            <asp:HiddenField ID="hidV_ID" runat="server" />
                            <asp:HiddenField ID="hidSYSTEM_ID" runat="server" />
                            <asp:HiddenField ID="hidCHKSTATUS" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="cmdUpload" />
                            <asp:PostBackTrigger ControlID="dgvUploadFile" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <footer class="form-group" runat="server" id="divButton">
                        <%--navbar-fixed-bottom --%>
                        <div class="container text-right">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    
                                        <asp:Button Text="บันทึก" runat="server" ID="btnSave" CssClass="btn btn-md btn-hover btn-info" data-toggle="modal" data-target="#ModalConfirmBeforeSave" UseSubmitBehavior="false" OnClick="Button2_Click"/>
                                        
                                    <asp:Button Text="ยกเลิก" runat="server" ID="btnCancel" CssClass="btn btn-md btn-hover btn-danger" data-toggle="modal" data-target="#ModalConfirmBeforeCancel" UseSubmitBehavior="false" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </footer>
                </div>
                <div class="tab-pane fade active" id="TabPTTDoc">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapsePTTDoc" id="acollapsePTTDoc">
                                        <asp:Label Text="ปตท.ขอเอกสารเพิ่มเติม" ID="Label1" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hidden1" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapsePTTDoc">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ปตท.ขอเอกสารเพิ่มเติม" runat="server" ID="lblSWAITDOCUMENT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSWAITDOCUMENT" TextMode="MultiLine" Rows="4" />
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>

                </div>

            </div>
        </div>
    </div>
    <br />


    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSave_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpCancel" IDModel="ModalConfirmBeforeCancel" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpCancel_ClickOK" TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);

        $(document).ready(function () {

            fcollapse();
        });



        function EndRequestHandler(sender, args) {

            fcollapse();
            console.log('sss');
        }

        function fcollapse() {
        }



      
    </script>
</asp:Content>


