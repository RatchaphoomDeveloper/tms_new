﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Data.OracleClient;
using System.IO;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using DevExpress.Web.ASPxGridView;
using System.Reflection;
using DevExpress.XtraReports.UI;
using System.Drawing;
using System.Data.SqlClient;
using TMS_BLL.Master;

public partial class result_add3 : System.Web.UI.Page
{
    #region + ViewState +
    private string STRUCKID_HEAD
    {
        get
        {
            if ((string)ViewState["STRUCKID_HEAD"] != null)
                return (string)ViewState["STRUCKID_HEAD"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["STRUCKID_HEAD"] = value;
        }
    }
    #endregion

    string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    private string sFomateDate = ConfigurationManager.AppSettings["FormatDate"] + "";
    private string sCheckWaterID = "01";
    string cSAP_SYNC = WebConfigurationManager.AppSettings["SAP_SYNCOUT"].ToString();
    private int nNextYeatCheckWater = 3;

    // slot คือ ช่อง

    //private static int nIndxNowSlot = 0; // เก็ยช่องในปัจุบันที่แสดงอยุ่หน้าเว็บเพื่อนำไปเก็บลง list ก่อนเปลี่ยนช่อง
    //private static decimal nSlotAll = 0;
    //private static int MAXSLOT = 0;

    //private static List<TINNER_CHECKINGS> lstDataCapaciltyWater = new List<TINNER_CHECKINGS>(); // ข้อมูลแป้น แต่ละ slot
    //private static List<TTRUCK_COMPART> lstTruckSlot = new List<TTRUCK_COMPART>(); // ข้อมูลพิกัดความจุุของแต่ละ slot
    //private static List<TData_CheckSlot> lstDataSlot = new List<TData_CheckSlot>(); // ข้อมูลแต่ละ slot
    //private static List<SLOT> lstSlot = new List<SLOT>(); // ข้อมูล slot
    //private static TimeSpan nTotalTimeChecking = new TimeSpan();

    //private static string sTruckID_CheckWater = "";
    //private static string sREQUESTID = "";
    //private static string TRUCKID_COMPART = "";
    //private static string USER_ID = "";
    //private static string STATUSFLAG_ID = "";
    //private static string REQTYPE_ID = "";
    //private static string VENDOR_ID = "";

    //** Note SCARTYPEID = 0 คือ 10 ล้อ, 3 คือหัวลาก ในกรณีหัวลากให้ส่งหางไปหาข้อมูลการวัดน้ำ(SCARTYPEID = 4)
    //private static DataTable dtMainData = new DataTable();
    //private static TimeSpan nTotalTimeChecking = new TimeSpan();

    private int nIndxNowSlot { get { return SystemFunction.ConvertObjectInt(Session["nIndxNowSlot"] + ""); } set { Session["nIndxNowSlot"] = value; } }
    private decimal nSlotAll { get { return SystemFunction.ConvertObjectInt(Session["nSlotAll"] + ""); } set { Session["nSlotAll"] = value; } }
    private int MAXSLOT { get { return SystemFunction.ConvertObjectInt(Session["MAXSLOT"] + ""); } set { Session["MAXSLOT"] = value; } }

    private List<TINNER_CHECKINGS> lstDataCapaciltyWater { get { return SystemFunction.ConvertObject<TINNER_CHECKINGS>(Session["lstDataCapaciltyWater"]); } set { Session["lstDataCapaciltyWater"] = value; } }
    private List<TTRUCK_COMPART> lstTruckSlot { get { return SystemFunction.ConvertObject<TTRUCK_COMPART>(Session["lstTruckSlot"]); } set { Session["lstTruckSlot"] = value; } }
    private List<TData_CheckSlot> lstDataSlot { get { return SystemFunction.ConvertObject<TData_CheckSlot>(Session["lstDataSlot"]); } set { Session["lstDataSlot"] = value; } }
    private List<SLOT> lstSlot { get { return SystemFunction.ConvertObject<SLOT>(Session["lstSlot"]); } set { Session["lstSlot"] = value; } }

    private DataTable dtMainData { get { return SystemFunction.ConvertObject(Session["dtMainData"]); } set { Session["dtMainData"] = value; } }
    private TimeSpan nTotalTimeChecking { get { return SystemFunction.ConvertObjectTime(Session["nTotalTimeChecking"]); } set { Session["nTotalTimeChecking"] = value; } }

    private string sTruckID_CheckWater { get { return Session["sTruckID_CheckWater"] + ""; } set { Session["sTruckID_CheckWater"] = value; } }
    private string sREQUESTID { get { return Session["sREQUESTID"] + ""; } set { Session["sREQUESTID"] = value; } }
    private string TRUCKID_COMPART { get { return Session["TRUCKID_COMPART"] + ""; } set { Session["TRUCKID_COMPART"] = value; } }
    private string USER_ID { get { return Session["USER_ID"] + ""; } set { Session["USER_ID"] = value; } }
    private string STATUSFLAG_ID { get { return Session["STATUSFLAG_ID"] + ""; } set { Session["STATUSFLAG_ID"] = value; } }
    private string REQTYPE_ID { get { return Session["REQTYPE_ID"] + ""; } set { Session["REQTYPE_ID"] = value; } }
    private string VENDOR_ID { get { return Session["VENDOR_ID"] + ""; } set { Session["VENDOR_ID"] = value; } }
    private string STRUCKID { get { return Session["STRUCKID"] + ""; } set { Session["STRUCKID"] = value; } }
    string SMENUID = "57";
    private void ClearSessionStatic()
    {
        Session["nIndxNowSlot"] = "";
        Session["nSlotAll"] = "";
        Session["MAXSLOT"] = "";

        Session["lstDataCapaciltyWater"] = "";
        Session["lstTruckSlot"] = "";
        Session["lstDataSlot"] = "";
        Session["lstSlot"] = "";
        Session["dtMainData"] = "";
        Session["nTotalTimeChecking"] = "";

        Session["sTruckID_CheckWater"] = "";
        Session["sREQUESTID"] = "";
        Session["TRUCKID_COMPART"] = "";
        Session["STATUSFLAG_ID"] = "";
        Session["REQTYPE_ID"] = "";
        Session["VENDOR_ID"] = "";
    }

    private void NewLstStatic()
    {
        nIndxNowSlot = 0;
        nSlotAll = 0;
        MAXSLOT = 0;

        lstDataCapaciltyWater = new List<TINNER_CHECKINGS>();
        lstTruckSlot = new List<TTRUCK_COMPART>();
        lstDataSlot = new List<TData_CheckSlot>();
        lstSlot = new List<SLOT>();
        dtMainData = new DataTable();
        nTotalTimeChecking = new TimeSpan();

        sTruckID_CheckWater = "";
        sREQUESTID = "";
        TRUCKID_COMPART = "";
        STATUSFLAG_ID = "";
        REQTYPE_ID = "";
        VENDOR_ID = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        else
        {
            if (!IsPostBack)
            {
                ClearSessionStatic();
                NewLstStatic();
                USER_ID = Session["UserID"] + "";
                string strREQID = Request.QueryString["strRQID"];
                if (!string.IsNullOrEmpty(strREQID + ""))
                {

                    string[] arrREQID = STCrypt.DecryptURL(strREQID);
                    sREQUESTID = arrREQID[0];
                    if (!string.IsNullOrEmpty(sREQUESTID))
                    {
                        SetTimeToCombobox();
                        LogUser(SMENUID, "R", "เปิดดูข้อมูลหน้า ผลตรวจลงน้ำ", sREQUESTID);
                        ListDataToPage(sREQUESTID);
                        SLOTALL(sREQUESTID);
                        // ListReportTeabpan(arrREQID[0]);
                        //ChangeSlot("1");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
                }
            }
        }
    }

    public void ListDataToPage(string sREQID)
    {
        //        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
        //                        TRQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TRK.SCARTYPEID,TRQ.RESULT_CHECKING_DATE,
        //                        TCAT.CARCATE_NAME,
        //                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
        //                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,TRQ.CHECKWATER_NVERSION,
        //                        TVD.SVENDORID,TVD.SABBREVIATION,
        //                        TUS.SFIRSTNAME,TUS.SLASTNAME,TRK.NWHEELS,TRQ.TOTLE_SLOT,
        //                        CASE WHEN TRK.SCARTYPEID = '0' THEN TRK.DPREV_SERV ELSE TRKN.DPREV_SERV END  as DPREV_SERV,
        //                        CASE WHEN TRK.SCARTYPEID = '0' THEN TRK.DLAST_SERV ELSE TRKN.DLAST_SERV END  as DLAST_SERV,
        //                        TRKN.SCAR_NUM,INC.DATE_CREATED,
        //                        TRKN.NTANK_HIGH_HEAD,TRKN.NTANK_HIGH_TAIL
        //                        FROM TBL_REQUEST TRQ 
        //                        LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = TRQ.STRUCKID
        //                        LEFT JOIN TTRUCK TRKN ON TRKN.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
        //                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
        //                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
        //                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
        //                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
        //                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
        //                        LEFT JOIN (SELECT REQ_ID, MAX(NVL( DATE_UPDATED, DATE_CREATED)) as DATE_CREATED FROM TBL_INNER_CHECKINGS GROUP BY REQ_ID)INC
        //                        ON TRQ.REQUEST_ID = INC.REQ_ID
        //                        WHERE TRQ.REQUEST_ID = '{0}'";



        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
                        TRQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TRK.SCARTYPEID,TRQ.RESULT_CHECKING_DATE,TRK.SCAR_NUM AS SCAR_NUM1,TRQ.DOCNO,TRKN.SCAR_NUM AS SCAR_NUM2,
                        TRKN.DWATEREXPIRE as DWATER,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,TRQ.CHECKWATER_NVERSION,
                        TVD.SVENDORID,TVD.SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME,TRK.NWHEELS,TRQ.TOTLE_SLOT,
                        CASE WHEN TRK.SCARTYPEID = '0' THEN TRK.DPREV_SERV ELSE TRKN.DPREV_SERV END  as DPREV_SERV,
                        CASE WHEN TRK.SCARTYPEID = '0' THEN TRK.DLAST_SERV ELSE TRKN.DLAST_SERV END  as DLAST_SERV,
                        CASE WHEN TRK.SCARTYPEID = '0' THEN TRK.DNEXT_SERV ELSE TRKN.DNEXT_SERV END  as DNEXT_SERV,
                        TRKN.SCAR_NUM,INC.DATE_CREATED,
                        TRKN.NTANK_HIGH_HEAD,TRKN.NTANK_HIGH_TAIL
                        FROM TBL_REQUEST TRQ 
                        LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = TRQ.STRUCKID
                        LEFT JOIN TTRUCK TRKN ON TRKN.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        LEFT JOIN (SELECT REQ_ID, MAX(NVL( DATE_UPDATED, DATE_CREATED)) as DATE_CREATED FROM TBL_INNER_CHECKINGS GROUP BY REQ_ID)INC
                        ON TRQ.REQUEST_ID = INC.REQ_ID
                         WHERE TRQ.REQUEST_ID = '{0}'";


        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));

        hidHEAD.Value = dtMainData.Rows[0]["VEH_NO"] + string.Empty;
        hidDETAIL.Value = dtMainData.Rows[0]["TU_NO"] + string.Empty;
        SetDataToPage();


    }



    private void SetDataToPage()
    {
        if (dtMainData.Rows.Count > 0)
        {
            DataRow dr = null;
            dr = dtMainData.Rows[0];
            decimal nTemp = 0;

            if (dr["STATUS_FLAG"] + "" == "10")
            {
                btnSaveT3.ClientVisible = false;
                btnSaveThisSlot.ClientVisible = false;
            }
            else
            {
                btnSaveT3.ClientVisible = true;
                btnSaveThisSlot.ClientVisible = true;
            }
            REQTYPE_ID = dr["REQTYPE_ID"] + "";
            STATUSFLAG_ID = dr["STATUS_FLAG"] + "";
            VENDOR_ID = dr["SVENDORID"] + "";
            //** ป่านlblTotalCap
            lblREQUEST_DATE.Text = !string.IsNullOrEmpty(dr["REQUEST_DATE"] + "") ? Convert.ToDateTime(dr["REQUEST_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblDLAST_SERV.Text = !string.IsNullOrEmpty(dr["DPREV_SERV"] + "") ? Convert.ToDateTime(dr["DPREV_SERV"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";

            //lblDNEXT_SERV.Text = !string.IsNullOrEmpty(dr["DNEXT_SERV"] + "") ? Convert.ToDateTime(dr["DNEXT_SERV"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblSERVICE_DATE.Text = !string.IsNullOrEmpty(dr["SERVICE_DATE"] + "") ? Convert.ToDateTime(dr["SERVICE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";



            //ป่าน
            lblDateNextCheckWater.Text = !string.IsNullOrEmpty(dr["DWATER"] + "") ? Convert.ToDateTime(dr["DWATER"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";

            //lblDateNextCheckWater.Text = !string.IsNullOrEmpty(dr["SERVICE_DATE"] + "") ? Convert.ToDateTime(dr["SERVICE_DATE"] + "", new CultureInfo("th-TH")).AddYears(nNextYeatCheckWater).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";




            lblAPPOINTMENT_BY_DATE.Text = (!string.IsNullOrEmpty(dr["APPROVE_DATE"] + "") ? Convert.ToDateTime(dr["APPROVE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "") +
                                          (!string.IsNullOrEmpty(dr["SFIRSTNAME"] + "") && !string.IsNullOrEmpty(dr["SLASTNAME"] + "") ? " - คุณ" + dr["SFIRSTNAME"] + " " + dr["SLASTNAME"] : "-");


            lblREQTYPE_NAME.Text = dr["REQTYPE_NAME"] + "";
            lblCAUSE_NAME.Text = dr["CAUSE_NAME"] + "" + (!string.IsNullOrEmpty(dr["REMARK_CAUSE"] + "") ? (" (" + dr["REMARK_CAUSE"] + ")") : "");
            lblVendorName.Text = dr["SABBREVIATION"] + "";

            //เต้ย
            txtSCAR_NUM.Text = (!string.IsNullOrEmpty(dr["SCAR_NUM1"] + "") ? (dr["SCAR_NUM1"] + "") : (dr["SCAR_NUM2"] + ""));
            txtDOCNO.Text = dr["DOCNO"] + "";
            hidDOCNO.Value = dr["DOCNO"] + "";

            lblTypeCar.Text = dr["CARCATE_NAME"] + "";
            lblTotalCap.Text = (decimal.TryParse(dr["TOTLE_CAP"] + "", out nTemp) ? nTemp : 0).ToString(SystemFunction.CheckFormatNuberic(0));
            lblREGISTERNO.Text = dr["VEH_NO"] + "" + (!string.IsNullOrEmpty(dr["TU_NO"] + "") ? "/" + dr["TU_NO"] + "" : "");
            STRUCKID = !string.IsNullOrEmpty(dr["TU_NO"] + "") ? dr["TU_NO"] + "" : dr["VEH_NO"] + string.Empty;
            if (!string.IsNullOrEmpty(dr["CCHECKING_WATER"] + ""))
            {
                rblStatusCheckingWater.Value = dr["CCHECKING_WATER"] + "";
            }
            txtNversion.Text = dr["CHECKWATER_NVERSION"] + "";
            txtComment.Text = GetComment(sREQUESTID);
            txtNTANK_HIGHT_HEAD.Text = dr["NTANK_HIGH_HEAD"] + "";
            txtNTANK_HIGHT_TAIL.Text = dr["NTANK_HIGH_TAIL"] + "";

            //เซ็ตช่องแต่ละช่อง ในการแสดง
            //if (dr["REQTYPE_ID"] + "" == sCheckWaterID)
            //{
            // set data to grid
            string sTruckID = "";
            switch (dr["SCARTYPEID"] + "")
            {
                case "0": sTruckID = dr["VEH_ID"] + ""; break; // 10 ล้อ
                case "3": sTruckID = dr["TU_ID"] + ""; STRUCKID_HEAD = dr["VEH_ID"] + ""; break; // หัวลาก
            }

            //หาช่องของรถคันนี้ เพื่อใช้ในการเปลี่ยรช่องในการกดบันทึกข้อมูล
            TRUCKID_COMPART = sTruckID;
            SLOTINTRUCK(sTruckID);
            sTruckID_CheckWater = sTruckID;
            CheckDate_Checking_OldWater(sTruckID);
            SetTimeToPage(sREQUESTID);
            //}

        }




        //setผู้ตรวจ
        DataTable dtUser = CommonFunction.Get_Data(strConn, "SELECT  REQUEST_ID, USER_EXAMINER, NVERSION,EXAMDATE FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + sREQUESTID + "' AND ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID, USER_EXAMINER, NVERSION,EXAMDATE ORDER BY NVERSION DESC");
        if (dtUser.Rows.Count > 0)
        {
            DateTime? EXAMDATE = null;
            if (!string.IsNullOrEmpty(dtUser.Rows[0]["EXAMDATE"] + ""))
            {
                EXAMDATE = DateTime.Parse(dtUser.Rows[0]["EXAMDATE"] + "");
            }

            cmbUser.Value = dtUser.Rows[0]["USER_EXAMINER"] + "";
            edtCheck.Value = EXAMDATE;
        }
    }

    // ส่วนข้างล่าง
    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});"); return;
        }

        xcpn.JSProperties["cpValidate"] = "";
        string[] spara = e.Parameter.Split(';');
        switch (spara[0])
        {
            case "ChangeSlot":
                GetDataFromPage_AddToListSlot(nIndxNowSlot);
                ChangeSlot(spara[1]);
                CalculateTimeCauseCallBack();
                xcpn.JSProperties["cpValidate"] = "N";
                break;
            case "SAVETHISSLOT":
                GetDataFromPage_AddToListSlot(nIndxNowSlot);
                SaveThisSlotToDataBase(nIndxNowSlot);
                ChangeSlot(nIndxNowSlot + "");
                CalculateTimeCauseCallBack();


                if (!string.IsNullOrEmpty(spara[0]))
                {
                    var ss = lstSlot.Where(w => w.NSLOT == nIndxNowSlot).OrderBy(o => o.NSLOT).FirstOrDefault();
                    if (ss != null)
                    {
                        int inx = (ss.INDEX + 1);
                        LogUser(SMENUID, "I", "บันทึกผลตรวจลงน้ำช่องที่" + inx, sREQUESTID);
                        var kk = lstSlot.Where(w => w.INDEX == inx).OrderBy(o => o.NSLOT).FirstOrDefault();
                        if (kk != null)
                        {
                            ChangeSlot(kk.NSLOT + "");
                            CalculateTimeCauseCallBack();
                        }
                    }
                }

                break;
            case "SAVET3":
                if (string.Equals(lblDLAST_SERV.Text.Trim(), string.Empty))
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + "กรุณาระบุ วันที่วัดน้ำครั้งล่าสุด" + "')");
                    return;
                }

                if (string.Equals(lblDateNextCheckWater.Text.Trim(), string.Empty))
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + "กรุณาระบุ วันที่หมดอายุวัดน้ำ" + "')");
                    return;
                }

                LogUser(SMENUID, "I", "บันทึกผลตรวจลงน้ำ", sREQUESTID);
                string Message_error = SaveAllToDB();
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Message_error + "')");
                ChangeSlot(nIndxNowSlot + "");
                CalculateTimeCauseCallBack();
                SetTimeToCombobox();
                ListDataToPage(sREQUESTID);
                SLOTALL(sREQUESTID);
                break;
            case "RedirectT1": xcpn.JSProperties["cpRedirectTo"] = "result-add.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT2": xcpn.JSProperties["cpRedirectTo"] = "result-add2.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT3": xcpn.JSProperties["cpRedirectTo"] = "result-add3.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT4": xcpn.JSProperties["cpRedirectTo"] = "result-add4.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT5": xcpn.JSProperties["cpRedirectTo"] = "result-add5.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectImg": xcpn.JSProperties["cpRedirectTo"] = "CarImageAdd.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;

        };
    }

    private void ChangeSlot(string sIndxSlot) // เปลี่ยนช่อง 
    {
        int nTempSlot = int.TryParse(sIndxSlot, out nTempSlot) ? nTempSlot : 0;
        SetCssChangeSlot(sIndxSlot);

        // Set data check water
        SetDataCheckWater(nTempSlot);
    }

    private void SetCssChangeSlot(string sIndxSlot)
    {
        #region set css
        int nTempSlot = int.TryParse(sIndxSlot, out nTempSlot) ? nTempSlot : 0;
        if (nTempSlot > 0)
        {
            tdSlot1.Attributes.Add("class", "InActiveSlot");
            tdSlot2.Attributes.Add("class", "InActiveSlot");
            tdSlot3.Attributes.Add("class", "InActiveSlot");
            tdSlot4.Attributes.Add("class", "InActiveSlot");
            tdSlot5.Attributes.Add("class", "InActiveSlot");
            tdSlot6.Attributes.Add("class", "InActiveSlot");
            tdSlot7.Attributes.Add("class", "InActiveSlot");
            tdSlot8.Attributes.Add("class", "InActiveSlot");
            tdSlot9.Attributes.Add("class", "InActiveSlot");
            tdSlot10.Attributes.Add("class", "InActiveSlot");

            switch (nTempSlot)
            {
                case 1: tdSlot1.Attributes.Add("class", "ActiveSlot"); break;
                case 2: tdSlot2.Attributes.Add("class", "ActiveSlot"); break;
                case 3: tdSlot3.Attributes.Add("class", "ActiveSlot"); break;
                case 4: tdSlot4.Attributes.Add("class", "ActiveSlot"); break;
                case 5: tdSlot5.Attributes.Add("class", "ActiveSlot"); break;
                case 6: tdSlot6.Attributes.Add("class", "ActiveSlot"); break;
                case 7: tdSlot7.Attributes.Add("class", "ActiveSlot"); break;
                case 8: tdSlot8.Attributes.Add("class", "ActiveSlot"); break;
                case 9: tdSlot9.Attributes.Add("class", "ActiveSlot"); break;
                case 10: tdSlot10.Attributes.Add("class", "ActiveSlot"); break;
            };
        }
        #endregion
    }

    private void SetTimeToCombobox()
    {
        List<TTime> lstHour = new List<TTime>();
        List<TTime> lstMinute = new List<TTime>();

        for (int i = 1; i <= 24; i++)
        {
            lstHour.Add(new TTime { nHour = i, sTXT = ((i + "").Length == 1 ? ("0" + i) : i + "") });
        }

        for (int i = 0; i <= 59; i++)
        {
            lstMinute.Add(new TTime { nMinute = i, sTXT = ((i + "").Length == 1 ? ("0" + i) : i + "") });
        }

        #region Set combobox time
        cmbTimeCheckIn_H.DataSource = lstHour;
        cmbTimeCheckIn_H.DataBind();
        cmbTimeCheckIn_M.DataSource = lstMinute;
        cmbTimeCheckIn_M.DataBind();

        cmbTimeCheckIn_H.SelectedIndex = 0;
        cmbTimeCheckIn_M.SelectedIndex = 0;

        cmbTime_InnerChecking_H.DataSource = lstHour;
        cmbTime_InnerChecking_H.DataBind();
        cmbTime_InnerChecking_M.DataSource = lstMinute;
        cmbTime_InnerChecking_M.DataBind();

        cmbTime_InnerChecking_H.SelectedIndex = 0;
        cmbTime_InnerChecking_M.SelectedIndex = 0;

        cmbTime_EndInnerChecking_H.DataSource = lstHour;
        cmbTime_EndInnerChecking_H.DataBind();
        cmbTime_EndInnerChecking_M.DataSource = lstMinute;
        cmbTime_EndInnerChecking_M.DataBind();

        cmbTime_EndInnerChecking_H.SelectedIndex = 0;
        cmbTime_EndInnerChecking_M.SelectedIndex = 0;


        cmbTime_WaterChecking_H.DataSource = lstHour;
        cmbTime_WaterChecking_H.DataBind();
        cmbTime_WaterChecking_M.DataSource = lstMinute;
        cmbTime_WaterChecking_M.DataBind();

        cmbTime_WaterChecking_H.SelectedIndex = 0;
        cmbTime_WaterChecking_M.SelectedIndex = 0;


        cmbTime_EndWaterChecking_H.DataSource = lstHour;
        cmbTime_EndWaterChecking_H.DataBind();
        cmbTime_EndWaterChecking_M.DataSource = lstMinute;
        cmbTime_EndWaterChecking_M.DataBind();

        cmbTime_EndWaterChecking_H.SelectedIndex = 0;
        cmbTime_EndWaterChecking_M.SelectedIndex = 0;

        cmbTime_StopWaterChecking_H.DataSource = lstHour;
        cmbTime_StopWaterChecking_H.DataBind();
        cmbTime_StopWaterChecking_M.DataSource = lstMinute;
        cmbTime_StopWaterChecking_M.DataBind();

        cmbTime_StopWaterChecking_H.SelectedIndex = 0;
        cmbTime_StopWaterChecking_M.SelectedIndex = 0;

        cmbTime_EndStopWaterChecking_H.DataSource = lstHour;
        cmbTime_EndStopWaterChecking_H.DataBind();
        cmbTime_EndStopWaterChecking_M.DataSource = lstMinute;
        cmbTime_EndStopWaterChecking_M.DataBind();

        cmbTime_EndStopWaterChecking_H.SelectedIndex = 0;
        cmbTime_EndStopWaterChecking_M.SelectedIndex = 0;

        cmbTime_ReturnWaterChecking_H.DataSource = lstHour;
        cmbTime_ReturnWaterChecking_H.DataBind();
        cmbTime_ReturnWaterChecking_M.DataSource = lstMinute;
        cmbTime_ReturnWaterChecking_M.DataBind();

        cmbTime_ReturnWaterChecking_H.SelectedIndex = 0;
        cmbTime_ReturnWaterChecking_M.SelectedIndex = 0;

        cmbTime_EndReturnWaterChecking_H.DataSource = lstHour;
        cmbTime_EndReturnWaterChecking_H.DataBind();
        cmbTime_EndReturnWaterChecking_M.DataSource = lstMinute;
        cmbTime_EndReturnWaterChecking_M.DataBind();

        cmbTime_EndReturnWaterChecking_H.SelectedIndex = 0;
        cmbTime_EndReturnWaterChecking_M.SelectedIndex = 0;
        #endregion

    }

    private void CheckDate_Checking_OldWater(string sTruckID) //ตรวจสอบวันที่วัดน้ำล่าสุด, *** TruckID แยก ID หัวหางตามประเภทรถมาแล้ว
    {
        lstDataCapaciltyWater = new List<TINNER_CHECKINGS>();
        lstTruckSlot = new List<TTRUCK_COMPART>();

        List<TINNER_CHECKINGS> lstOLDCheckingWater = new List<TINNER_CHECKINGS>(); // ข้อมูลวัดน้ำเดิม
        string _sLAST_REQ_ID = "";

        string sql = @"SELECT * FROM TTRUCK WHERE STRUCKID = '{0}'";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sTruckID)));
        if (dt.Rows.Count == 1)
        {
            DataRow dr = null;
            dr = dt.Rows[0];

            _sLAST_REQ_ID = dr["SLAST_REQ_ID"] + "";

            string sqlCheckingkWater = @"SELECT * FROM TBL_INNER_CHECKINGS WHERE REQ_ID = '{0}'";
            DataTable dtCheckWaterNow = new DataTable();
            dtCheckWaterNow = CommonFunction.Get_Data(strConn, string.Format(sqlCheckingkWater, CommonFunction.ReplaceInjection(sREQUESTID)));

            // เคยวัดน้ำมาแล้ว//
            if (!string.IsNullOrEmpty(dr["SLAST_REQ_ID"] + ""))
            {
                // ข้อมูลวัดน้ำเดิม
                DataTable dtOldCheckingWater = new DataTable();
                dtOldCheckingWater = CommonFunction.Get_Data(strConn, string.Format(sqlCheckingkWater, CommonFunction.ReplaceInjection(dr["SLAST_REQ_ID"] + "")));

                //ถ้า SLAST_REQ_ID ไม่เจอในเทเบิ้ล TBL_INNER_CHECKINGS ให้ไปหาใน INNER_CHECKINGS	แทน

                if (dtOldCheckingWater.Rows.Count == 0)
                {
                    sqlCheckingkWater = @"SELECT INC.*, null as PASSSTANDARD, null as PASSSTANDARD_PAN3, null as PAN_ERR FROM INNER_CHECKINGS INC WHERE REQ_ID = '{0}'";
                    dtOldCheckingWater = new DataTable();
                    dtOldCheckingWater = CommonFunction.Get_Data(strConn, string.Format(sqlCheckingkWater, CommonFunction.ReplaceInjection(dr["SLAST_REQ_ID"] + "")));
                }


                lstOLDCheckingWater = SystemFunction.DataTableConvertToList<TINNER_CHECKINGS>(dtOldCheckingWater);

                // ข้อมูลวัดน้ำปัจจุบัน
                lstDataCapaciltyWater = SystemFunction.DataTableConvertToList<TINNER_CHECKINGS>(dtCheckWaterNow);


                // มีข้อมูลปัจุบัน
                if (dtCheckWaterNow.Rows.Count > 0)
                {
                    #region // มีข้อมูลวัดน้ำปัจุบันแล้ว แต่ต้องตรวจสอบว่าครบทุก slot หรือไม่

                    foreach (var item in lstOLDCheckingWater)
                    {
                        // ไม่มีข้อมูลปัจจุบันใน ช่อง และ แป้น นี้
                        if (lstDataCapaciltyWater.Where(w => w.COMPART_NO == item.COMPART_NO && w.PAN_LEVEL == item.PAN_LEVEL).Count() == 0)
                        {
                            lstDataCapaciltyWater.Add(new TINNER_CHECKINGS
                            {
                                REQ_ID = item.REQ_ID,
                                COMPART_NO = item.COMPART_NO,
                                PAN_LEVEL = item.PAN_LEVEL,
                                OLD_HEIGHT1 = item.HEIGHT1 ?? 0,
                                OLD_HEIGHT2 = item.HEIGHT2 ?? 0,
                                HEIGHT1 = null,
                                HEIGHT2 = null,
                                //COORDINATE1 = item.COORDINATE1,
                                //PAN1 = item.PAN1,
                                //COORDINATE2 = item.COORDINATE2,
                                //PAN2 = item.PAN2,
                                SEAL_NO = item.SEAL_NO,
                                //LEAD_SEAL = item.LEAD_SEAL,
                                //SEAL_ERR = item.SEAL_ERR,
                                //NEW_SEAL_NO = item.NEW_SEAL_NO,
                                //MANHOLD_ERR1 = item.MANHOLD_ERR1,
                                //MANHOLD_ERR2 = item.MANHOLD_ERR2,
                                //MANHOLD_NO1 = item.MANHOLD_NO1,
                                //MANHOLD_NO2 = item.MANHOLD_NO2,
                                //SD_VALUE = item.SD_VALUE,
                                //REMARK = item.REMARK,
                                PAN_FLOOD = item.PAN_FLOOD,
                                PAN_DRY = item.PAN_DRY,
                                PASSSTANDARD = item.PASSSTANDARD,
                                PASSSTANDARD_PAN3 = item.PASSSTANDARD_PAN3,
                                PAN_ERR = item.PAN_ERR
                            });
                        }
                    }

                    #endregion
                }
                else //ปัจจุบันยังไม่มีข้อมูล ต้องนำข้อมูลวัดน้ำเดิมมาใส่ให้
                {
                    #region set old to now
                    lstDataCapaciltyWater = dtOldCheckingWater.AsEnumerable().Select(s => new TINNER_CHECKINGS
                    {
                        REQ_ID = s.Field<string>("REQ_ID"),
                        COMPART_NO = s.Field<decimal>("COMPART_NO"),
                        PAN_LEVEL = s.Field<decimal>("PAN_LEVEL"),
                        OLD_HEIGHT1 = s["HEIGHT1"] != DBNull.Value ? s.Field<decimal>("HEIGHT1") : 0, //s.Field<decimal>("OLD_HEIGHT1"),
                        OLD_HEIGHT2 = s["HEIGHT2"] != DBNull.Value ? s.Field<decimal>("HEIGHT2") : 0, //s.Field<decimal>("OLD_HEIGHT2"),
                        HEIGHT1 = null, //s.Field<decimal>("HEIGHT1"),
                        HEIGHT2 = null,//s.Field<decimal>("HEIGHT2"),
                        //COORDINATE1 = s.Field<decimal>("COORDINATE1"),
                        //PAN1 = s.Field<decimal>("PAN1"),
                        //COORDINATE2 = s.Field<decimal>("COORDINATE2"),
                        //PAN2 = s.Field<decimal>("PAN2"),
                        SEAL_NO = s.Field<string>("SEAL_NO"),
                        //LEAD_SEAL = s.Field<string>("LEAD_SEAL"),
                        //SEAL_ERR = s.Field<string>("SEAL_ERR"),
                        //NEW_SEAL_NO = s.Field<string>("NEW_SEAL_NO"),
                        //MANHOLD_ERR1 = s.Field<string>("MANHOLD_ERR1"),
                        //MANHOLD_ERR2 = s.Field<string>("MANHOLD_ERR2"),
                        //MANHOLD_NO1 = s.Field<string>("MANHOLD_NO1"),
                        //MANHOLD_NO2 = s.Field<string>("MANHOLD_NO2"),
                        //SD_VALUE = s.Field<string>("SD_VALUE"),
                        //REMARK = s.Field<string>("REMARK"),
                        PAN_FLOOD = s.Field<decimal?>("PAN_FLOOD"),
                        PAN_DRY = s.Field<decimal?>("PAN_DRY"),
                        PASSSTANDARD = s.Field<string>("PASSSTANDARD"),
                        PASSSTANDARD_PAN3 = s.Field<string>("PASSSTANDARD_PAN3"),
                        PAN_ERR = s.Field<string>("PAN_ERR")

                    }).ToList();
                    #endregion
                }
            }
            else
            {
                #region กรณี ไม่เคยเข้าวัดน้ำมาก่อน
                if (dtCheckWaterNow.Rows.Count > 0)
                {
                    // ข้อมูลวัดน้ำปัจุบัน
                    lstDataCapaciltyWater = dtCheckWaterNow.AsEnumerable().Select(s => new TINNER_CHECKINGS
                    {
                        REQ_ID = s.Field<string>("REQ_ID"),
                        COMPART_NO = s.Field<decimal>("COMPART_NO"),
                        PAN_LEVEL = s.Field<decimal>("PAN_LEVEL"),
                        OLD_HEIGHT1 = s.Field<decimal>("OLD_HEIGHT1"),
                        OLD_HEIGHT2 = s.Field<decimal>("OLD_HEIGHT2"),
                        HEIGHT1 = s.Field<decimal?>("HEIGHT1"),
                        HEIGHT2 = s.Field<decimal?>("HEIGHT2"),
                        //COORDINATE1 = s.Field<decimal>("COORDINATE1"),
                        //PAN1 = s.Field<decimal>("PAN1"),
                        //COORDINATE2 = s.Field<decimal>("COORDINATE2"),
                        //PAN2 = s.Field<decimal>("PAN2"),
                        SEAL_NO = s.Field<string>("SEAL_NO"),
                        //LEAD_SEAL = s.Field<string>("LEAD_SEAL"),
                        //SEAL_ERR = s.Field<string>("SEAL_ERR"),
                        //NEW_SEAL_NO = s.Field<string>("NEW_SEAL_NO"),
                        //MANHOLD_ERR1 = s.Field<string>("MANHOLD_ERR1"),
                        //MANHOLD_ERR2 = s.Field<string>("MANHOLD_ERR2"),
                        //MANHOLD_NO1 = s.Field<string>("MANHOLD_NO1"),
                        //MANHOLD_NO2 = s.Field<string>("MANHOLD_NO2"),
                        //SD_VALUE = s.Field<string>("SD_VALUE"),
                        //REMARK = s.Field<string>("REMARK"),
                        PAN_FLOOD = s.Field<decimal?>("PAN_FLOOD"),
                        PAN_DRY = s.Field<decimal?>("PAN_DRY"),
                        PASSSTANDARD = s.Field<string>("PASSSTANDARD"),
                        PASSSTANDARD_PAN3 = s.Field<string>("PASSSTANDARD_PAN3"),
                        PAN_ERR = s.Field<string>("PAN_ERR")
                    }).ToList();

                }
                #endregion
            }
        }

        #region ข้อมูลในแต่ละช่อง
        // ข้อมูลแต่ละช่อง เป็นของมูลปัจจุ หากเคยบันทึกแล้ว
        string sqlNowSlot = @"SELECT * FROM TBL_CHECKING_COMPART WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(sREQUESTID)) + "' AND STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID) + "'";

        string sqlOldSlot = @"SELECT * FROM TBL_CHECKING_COMPART WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(_sLAST_REQ_ID)) + "' AND STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID) + "'";
        DataTable dtOldSlot = new DataTable();
        dtOldSlot = CommonFunction.Get_Data(strConn, sqlOldSlot);
        if (dtOldSlot.Rows.Count == 0)
        {
            sqlOldSlot = @"SELECT COMPART_NO,null as OLD_NTOTAL_HEIGHT,HEIGHT1+HEIGHT2 as NTOTAL_HEIGHT,SD_VALUE as EMER_VALUE,MANHOLD_ERR1,MANHOLD_ERR2,MANHOLD_NO1,MANHOLD_NO2,SEAL_ERR,SEAL_NO,NEW_SEAL_NO FROM INNER_CHECKINGS WHERE REQ_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(_sLAST_REQ_ID)) + "'";
            dtOldSlot = new DataTable();
            dtOldSlot = CommonFunction.Get_Data(strConn, sqlOldSlot);
        }


        List<TData_CheckSlot> lstOldSlot = new List<TData_CheckSlot>();
        lstOldSlot = SystemFunction.DataTableConvertToList<TData_CheckSlot>(dtOldSlot);

        DataTable dtNowSlot = new DataTable();
        dtNowSlot = CommonFunction.Get_Data(strConn, sqlNowSlot);
        if (dt.Rows.Count > 0)
        {
            lstDataSlot = dtNowSlot.AsEnumerable().Select(s => new TData_CheckSlot
            {
                COMPART_NO = s.Field<decimal>("COMPART_NO"),
                OLD_NTOTAL_HEIGHT = s.Field<decimal?>("OLD_NTOTAL_HEIGHT"),
                NTOTAL_HEIGHT = s.Field<decimal?>("NTOTAL_HEIGHT"),
                EMER_VALUE = s.Field<string>("EMER_VALUE"),
                MANHOLD_ERR1 = s.Field<string>("MANHOLD_ERR1"),
                MANHOLD_ERR2 = s.Field<string>("MANHOLD_ERR2"),
                MANHOLD_NO1 = s.Field<string>("MANHOLD_NO1"),
                MANHOLD_NO2 = s.Field<string>("MANHOLD_NO2"),
                SEAL_ERR = s.Field<string>("SEAL_ERR"),
                SEAL_NO = s.Field<string>("SEAL_NO"),
                NEW_SEAL_NO = s.Field<string>("NEW_SEAL_NO"),
                cCheckOldSelNo = false

            }).ToList();
        }

        foreach (var item in lstOldSlot)
        {
            // ในกรณีข้อมูลปัจุบันไม่มี slot นี้ ต้องนำข้อมูลเก่ามาใส่ให้
            if (lstDataSlot.Where(w => w.COMPART_NO == item.COMPART_NO).Count() == 0)
            {
                lstDataSlot.Add(new TData_CheckSlot
                {
                    COMPART_NO = item.COMPART_NO,
                    OLD_NTOTAL_HEIGHT = item.NTOTAL_HEIGHT ?? 0,
                    NTOTAL_HEIGHT = null,
                    EMER_VALUE = item.EMER_VALUE,
                    MANHOLD_ERR1 = item.MANHOLD_ERR1,
                    MANHOLD_ERR2 = item.MANHOLD_ERR2,
                    MANHOLD_NO1 = item.MANHOLD_NO1,
                    MANHOLD_NO2 = item.MANHOLD_NO2,
                    SEAL_ERR = item.SEAL_ERR,
                    SEAL_NO = item.NEW_SEAL_NO,
                    NEW_SEAL_NO = "",
                    cCheckOldSelNo = false
                });
            }
        }

        #endregion

        #region // ข้อมูลเดิมของรถ (ข้อมูลแป้น) นำไป set พิกัดแป้น
        string sqlCOMPART = @"SELECT * FROM TTRUCK_COMPART WHERE STRUCKID = '{0}'";
        dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sqlCOMPART, CommonFunction.ReplaceInjection(sTruckID)));
        lstTruckSlot = dt.AsEnumerable().Select(s => new TTRUCK_COMPART
        {
            STRUCKID = s.Field<string>("STRUCKID"),
            NCOMPARTNO = s["NCOMPARTNO"] != DBNull.Value ? s.Field<decimal>("NCOMPARTNO") : 0, //s.Field<decimal>("NCOMPARTNO"), // ช่องที่
            NPANLEVEL = s["NPANLEVEL"] != DBNull.Value ? s.Field<decimal>("NPANLEVEL") : 0, //s.Field<decimal>("NPANLEVEL"), // แป้นที่
            NCAPACITY = s["NCAPACITY"] != DBNull.Value ? s.Field<decimal>("NCAPACITY") : 0 //s.Field<decimal>("NCAPACITY") // ความจุ

        }).ToList();
        #endregion

        EnableSlot();
    }

    private void SetDataCheckWater(int nSlot) // **** กำหนดของมูลของแต่ละช่องกลับลงในหน้า
    {
        nIndxNowSlot = nSlot;

        var query = lstDataCapaciltyWater.Where(w => w.COMPART_NO == nSlot).ToList();
        if (query.Count > 0)
        {
            decimal? nTotalNewPhase = null;
            //ระยะรวม (1)
            var DataSlot = lstDataSlot.Where(s => s.COMPART_NO == nSlot).FirstOrDefault();
            if (DataSlot != null)
            {
                nTotalNewPhase = DataSlot.NTOTAL_HEIGHT != null ? DataSlot.NTOTAL_HEIGHT : null;
            }



            #region กรณีมีข้อมูล
            foreach (var item in query)
            {
                decimal? nCalCause2 = null;
                nCalCause2 = CaculateCase2(nTotalNewPhase, item.HEIGHT2);

                // แป้นที่ 1
                if (item.PAN_LEVEL == 1)
                {
                    //ระยะเดิม
                    lblNumOldPhaseCompartNo1_Case1.Text = item.OLD_HEIGHT2 != null ? (item.OLD_HEIGHT2 != 0 ? item.OLD_HEIGHT2.ToString(SystemFunction.CheckFormatNuberic(0)) : item.OLD_HEIGHT2 + "") : "0";//ระยะห่างจากก้นถังถึงแป้น (2)
                    lblNumOldPhaseCompartNo1_Case2.Text = item.OLD_HEIGHT1 != null ? (item.OLD_HEIGHT1 != 0 ? item.OLD_HEIGHT1.ToString(SystemFunction.CheckFormatNuberic(0)) : item.OLD_HEIGHT2 + "") : "0";//ระยะจากปากถังถึงแป้น (1) - (2)

                    //ระยะใหม่
                    txtNumNewPhaseCompartNo1_Case1.Text = item.HEIGHT2 != null ? (item.HEIGHT2.Value > 0 ? item.HEIGHT2.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "";//ระยะห่างจากก้นถังถึงแป้น (2)
                    lblNumNewPhaseCompartNo1_Case2.Text = nCalCause2 != null ? (nCalCause2.Value != 0 ? nCalCause2.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "";//ระยะจากปากถังถึงแป้น (1) - (2)

                    //ผลต่าง
                    lblDiffPhaseCompartNo1_Case1.Text = item.HEIGHT2 != null ? CalculateDifference(item.OLD_HEIGHT2, item.HEIGHT2.Value, "1") : "";
                    lblDiffPhaseCompartNo1_Case2.Text = nCalCause2 != null ? CalculateDifference(item.OLD_HEIGHT1, nCalCause2.Value, "2") : "";

                    // น้ำท่วมแป้น
                    ckbWaterOverPlant1.Checked = item.PAN_FLOOD != null ? item.PAN_FLOOD > 0 ? true : false : false;
                    //น้ำขาดแป้น
                    ckbWaterLackPlant1.Checked = item.PAN_FLOOD != null ? item.PAN_DRY > 0 ? true : false : false;

                    //ผ่านเกณฑ์
                    rblStatus.SelectedIndex = item.PASSSTANDARD != null ? (item.PASSSTANDARD == "1" ? 0 : 1) : 0;

                    int Pan3ind = item.PASSSTANDARD_PAN3 != null ? (item.PASSSTANDARD_PAN3 == "1" ? 0 : 1) : 0;
                    rblStatusP3.Value = Pan3ind;

                    chkPandefective.Checked = item.PAN_ERR != null ? (item.PAN_ERR == "1" ? true : false) : false;
                    //if (Pan3ind == 0)
                    //{
                    //    VisiblePan3(true);
                    //}
                    //else
                    //{
                    //    VisiblePan3(false);
                    //}
                }

                // แป้นที่ 2
                else if (item.PAN_LEVEL == 2)
                {
                    //ระยะเดิม
                    lblNumOldPhaseCompartNo2_Case1.Text = item.OLD_HEIGHT2 != null ? (item.OLD_HEIGHT2 != 0 ? item.OLD_HEIGHT2.ToString(SystemFunction.CheckFormatNuberic(0)) : item.OLD_HEIGHT2 + "") : "0";//ระยะห่างจากก้นถังถึงแป้น (2)
                    lblNumOldPhaseCompartNo2_Case2.Text = item.OLD_HEIGHT1 != null ? (item.OLD_HEIGHT1 != 0 ? item.OLD_HEIGHT1.ToString(SystemFunction.CheckFormatNuberic(0)) : item.OLD_HEIGHT2 + "") : "0";//ระยะจากปากถังถึงแป้น (1) - (2)

                    //ระยะใหม่
                    txtNumNewPhaseCompartNo2_Case1.Text = item.HEIGHT2 != null ? (item.HEIGHT2.Value > 0 ? item.HEIGHT2.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "";//ระยะห่างจากก้นถังถึงแป้น (2)
                    lblNumNewPhaseCompartNo2_Case2.Text = nCalCause2 != null ? (nCalCause2.Value != 0 ? nCalCause2.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "";//ระยะจากปากถังถึงแป้น (1) - (2)

                    //ผลต่าง
                    lblDiffPhaseCompartNo2_Case1.Text = item.HEIGHT2 != null ? CalculateDifference(item.OLD_HEIGHT2, item.HEIGHT2.Value, "1") : "";
                    lblDiffPhaseCompartNo2_Case2.Text = nCalCause2 != null ? CalculateDifference(item.OLD_HEIGHT1, nCalCause2.Value, "2") : "";

                    // น้ำท่วมแป้น
                    ckbWaterOverPlant2.Checked = item.PAN_FLOOD != null ? item.PAN_FLOOD > 0 ? true : false : false;
                    //น้ำขาดแป้น
                    ckbWaterLackPlant2.Checked = item.PAN_FLOOD != null ? item.PAN_DRY > 0 ? true : false : false;
                    //ผ่านเกณฑ์
                    rblStatus.SelectedIndex = item.PASSSTANDARD != null ? (item.PASSSTANDARD == "1" ? 0 : 1) : 0;
                    int Pan3ind = item.PASSSTANDARD_PAN3 != null ? (item.PASSSTANDARD_PAN3 == "1" ? 0 : 1) : 0;
                    rblStatusP3.SelectedIndex = Pan3ind;

                    chkPandefective.Checked = item.PAN_ERR != null ? (item.PAN_ERR == "1" ? true : false) : false;
                    //if (Pan3ind == 0)
                    //{
                    //    VisiblePan3(true);
                    //}
                    //else
                    //{
                    //    VisiblePan3(false);
                    //}
                }


                // แป้นที่ 3
                if (item.PAN_LEVEL == 3)
                {
                    //ระยะเดิม
                    lblNumOldPhaseCompartNo3_Case1.Text = item.OLD_HEIGHT2 != null ? (item.OLD_HEIGHT2 != 0 ? item.OLD_HEIGHT2.ToString(SystemFunction.CheckFormatNuberic(0)) : item.OLD_HEIGHT2 + "") : "0";//ระยะห่างจากก้นถังถึงแป้น (2)
                    lblNumOldPhaseCompartNo3_Case2.Text = item.OLD_HEIGHT1 != null ? (item.OLD_HEIGHT1 != 0 ? item.OLD_HEIGHT1.ToString(SystemFunction.CheckFormatNuberic(0)) : item.OLD_HEIGHT2 + "") : "0";//ระยะจากปากถังถึงแป้น (1) - (2)

                    //ระยะใหม่
                    lblNumNewPhaseCompartNo3_Case1.Text = item.HEIGHT2 != null ? item.HEIGHT2.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "";//ระยะห่างจากก้นถังถึงแป้น (2)
                    lblNumNewPhaseCompartNo3_Case2.Text = item.HEIGHT1 != null ? item.HEIGHT1.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "";//ระยะจากปากถังถึงแป้น (1) - (2)

                    //ผลต่าง
                    lblDiffPhaseCompartNo3_Case1.Text = item.HEIGHT2 != null ? CalculateDifference(item.OLD_HEIGHT2, item.HEIGHT2.Value, "1") : "";
                    lblDiffPhaseCompartNo3_Case2.Text = item.HEIGHT1 != null ? CalculateDifference(item.OLD_HEIGHT1, item.HEIGHT1.Value, "2") : "";
                }
            }
            #endregion
        }
        else // 
        {
            #region กรณีไม่มีข้อมูล
            // แป้นที่ 1

            //ระยะเดิม
            lblNumOldPhaseCompartNo1_Case1.Text = "0";//ระยะห่างจากก้นถังถึงแป้น (2)
            lblNumOldPhaseCompartNo1_Case2.Text = "0";//ระยะจากปากถังถึงแป้น

            //ระยะใหม่
            txtNumNewPhaseCompartNo1_Case1.Text = "";//ระยะห่างจากก้นถังถึงแป้น (2)
            lblNumNewPhaseCompartNo1_Case2.Text = "";//ระยะจากปากถังถึงแป้น

            //ผลต่าง
            lblDiffPhaseCompartNo1_Case1.Text = "";
            lblDiffPhaseCompartNo1_Case2.Text = "";


            // แป้นที่ 2

            //ระยะเดิม
            lblNumOldPhaseCompartNo2_Case1.Text = "0";//ระยะห่างจากก้นถังถึงแป้น (2)
            lblNumOldPhaseCompartNo2_Case2.Text = "0";//ระยะจากปากถังถึงแป้น

            //ระยะใหม่
            txtNumNewPhaseCompartNo2_Case1.Text = "";//ระยะห่างจากก้นถังถึงแป้น (2)
            lblNumNewPhaseCompartNo2_Case2.Text = ""; //ระยะจากปากถังถึงแป้น

            //ผลต่าง
            lblDiffPhaseCompartNo2_Case1.Text = "";
            lblDiffPhaseCompartNo2_Case2.Text = "";


            // แป้นที่ 3

            //ระยะเดิม
            lblNumOldPhaseCompartNo3_Case1.Text = "";//ระยะห่างจากก้นถังถึงแป้น (2)
            lblNumOldPhaseCompartNo3_Case2.Text = "";//ระยะจากปากถังถึงแป้น

            //ระยะใหม่
            lblNumNewPhaseCompartNo3_Case1.Text = "";//ระยะห่างจากก้นถังถึงแป้น (2)
            lblNumNewPhaseCompartNo3_Case2.Text = "";//ระยะจากปากถังถึงแป้น

            //ผลต่าง
            lblDiffPhaseCompartNo3_Case1.Text = "";
            lblDiffPhaseCompartNo3_Case2.Text = "";

            // น้ำท่วมแป้น
            ckbWaterOverPlant1.Checked = false;
            //น้ำขาดแป้น
            ckbWaterLackPlant1.Checked = false;

            // น้ำท่วมแป้น
            ckbWaterOverPlant2.Checked = false;
            //น้ำขาดแป้น
            ckbWaterLackPlant2.Checked = false;

            //ผ่านเกณฑ์
            rblStatus.Value = "1";
            rblStatusP3.Value = "1";

            chkPandefective.Checked = false;

            #endregion
        }

        #region ข้อมูลพิกัดแป้น

        var queryCapcilty = lstTruckSlot.Where(w => w.NCOMPARTNO == nSlot).ToList();
        if (queryCapcilty.Count > 0)
        {
            // แป้นที่ 1
            var qPan1 = queryCapcilty.Where(w => w.NPANLEVEL == 1).FirstOrDefault();
            if (qPan1 != null)
            {
                lblCapPan1.Text = qPan1.NCAPACITY != null ? qPan1.NCAPACITY.ToString(SystemFunction.CheckFormatNuberic(0)) : "";
            }
            else
            {
                lblCapPan1.Text = "";
            }

            // แป้นที่ 2
            var qPan2 = queryCapcilty.Where(w => w.NPANLEVEL == 2).FirstOrDefault();
            if (qPan2 != null)
            {
                lblCapPan2.ClientEnabled = true;
                txtNumNewPhaseCompartNo2_Case1.ClientEnabled = true;
                lblCapPan2.Text = qPan2.NCAPACITY != null ? (qPan2.NCAPACITY > 0 ? qPan2.NCAPACITY.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "";
            }
            else
            {
                lblCapPan2.ClientEnabled = false;
                txtNumNewPhaseCompartNo2_Case1.ClientEnabled = false;
                lblCapPan2.Text = "-";
            }

            // แป้นที่ 3
            var qPan3 = queryCapcilty.Where(w => w.NPANLEVEL == 3).FirstOrDefault();
            if (qPan3 != null)
            {
                lblCapPan3.Text = qPan3.NCAPACITY != null ? (qPan3.NCAPACITY > 0 ? qPan3.NCAPACITY.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "";
                if (rblStatusP3.SelectedItem.Value == "1")
                {
                    VisiblePan3(true);
                }
                else
                {
                    VisiblePan3(false);
                }
            }
            else
            {
                lblCapPan3.Text = "";
                VisiblePan3(false);
            }

            // พิกัดแป้นรวม
            lblCapacityCompart.Text = queryCapcilty.Max(x => x.NCAPACITY).ToString(SystemFunction.CheckFormatNuberic(0));
        }
        else
        {
            lblCapPan1.Text = "";
            lblCapPan2.Text = "";
            lblCapPan3.Text = "";
            lblCapacityCompart.Text = "";
        }
        #endregion

        #region ข้อมูลการตรวจช่อง
        var qDataSlot = lstDataSlot.Where(s => s.COMPART_NO == nSlot).FirstOrDefault();
        if (qDataSlot != null)
        {
            //วาล์วฉุกเฉิน
            ckbEmergencyValve.Checked = !string.IsNullOrEmpty(qDataSlot.EMER_VALUE + "") ? qDataSlot.EMER_VALUE == "1" ? true : false : false;

            //สภาพซีลฝา 1 ชำรุด
            ckbRuinedSel1.Checked = !string.IsNullOrEmpty(qDataSlot.MANHOLD_ERR1 + "") ? qDataSlot.MANHOLD_ERR1 == "1" ? true : false : false;

            //สภาพซีลฝา 1 ชำรุด
            ckbRuinedSel2.Checked = !string.IsNullOrEmpty(qDataSlot.MANHOLD_ERR2 + "") ? qDataSlot.MANHOLD_ERR2 == "1" ? true : false : false;

            //สภาพซีลเดิมชำรุด
            ckbOldSelRuine.Checked = !string.IsNullOrEmpty(qDataSlot.SEAL_ERR + "") ? qDataSlot.SEAL_ERR == "1" ? true : false : false;

            //หมายเลขซีลเดิม
            txtOldNoSel.Text = qDataSlot.SEAL_NO;
            lblOldNoSel.Text = qDataSlot.SEAL_NO;
            //ckbOldNoSel.Checked = qDataSlot.cCheckOldSelNo != null ? qDataSlot.cCheckOldSelNo : false;
            txtNewNoSel.ClientEnabled = qDataSlot.cCheckOldSelNo != null ? !qDataSlot.cCheckOldSelNo : true;

            // หมายเลขซีลใหม่
            txtNewNoSel.Text = qDataSlot.NEW_SEAL_NO;

            //ระยะรวมเดิม
            lblNumOldPhase.Text = qDataSlot.OLD_NTOTAL_HEIGHT != null ? (qDataSlot.OLD_NTOTAL_HEIGHT.Value > 0 ? qDataSlot.OLD_NTOTAL_HEIGHT.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "0";

            // ระยะรวม
            txtNumNewPhase.Text = qDataSlot.NTOTAL_HEIGHT != null ? (qDataSlot.NTOTAL_HEIGHT.Value > 0 ? qDataSlot.NTOTAL_HEIGHT.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "";

            //ผลต่างระยะรวม
            lblNumDifferencePhase.Text = qDataSlot.NTOTAL_HEIGHT != null ? CalculateDifference(qDataSlot.NTOTAL_HEIGHT.Value, (qDataSlot.OLD_NTOTAL_HEIGHT ?? 0), "1") : "";

            //หมายเลขฝา Manhold
            txtMANHOLD_NO1.Text = qDataSlot.MANHOLD_NO1 + "";
            txtMANHOLD_NO2.Text = qDataSlot.MANHOLD_NO2 + "";

            //rblStatus.SelectedIndex = qDataSlot.;
        }
        else
        {
            //วาล์วฉุกเฉิน
            ckbEmergencyValve.Checked = false;

            //สภาพซีลฝา 1 ชำรุด
            ckbRuinedSel1.Checked = false;

            //สภาพซีลฝา 1 ชำรุด
            ckbRuinedSel2.Checked = false;

            //สภาพซีลเดิมชำรุด
            ckbOldSelRuine.Checked = false;

            //หมายเลขซีลเดิม
            txtOldNoSel.Text = "";
            lblOldNoSel.Text = "";
            //ckbOldNoSel.Checked = false;

            // หมายเลขซีลใหม่
            txtNewNoSel.Text = "";

            lblNumOldPhase.Text = "0";
            // ระยะรวม
            txtNumNewPhase.Text = "";

            //ผลต่างระยะรวม
            lblNumDifferencePhase.Text = "";

            //หมายเลขฝา Manhold
            txtMANHOLD_NO1.Text = "";
            txtMANHOLD_NO2.Text = "";

            rblStatus.Value = "1";
            rblStatusP3.Value = "1";
        }
        #endregion
    }

    private decimal? CaculateCase2(decimal? value1, decimal? value2)
    {
        decimal? ntotal = null;
        //string sResult = "";
        ntotal = value1 - value2;
        //sResult = ntotal != null ? ntotal > 0 ? ntotal.Value.ToString(SystemFunction.CheckFormatNuberic(0)) : "0" : "";
        return ntotal;
    }

    private string CalculateDifference(decimal Value1, decimal Value2, string sCause)
    {
        string sResult = "0";

        decimal nTemp = 0;
        if (sCause == "1") // เก่า - ใหม่
        {
            nTemp = Value1 - Value2;
        }
        else if (sCause == "2") // ใหม่ - เก่า
        {
            nTemp = Value2 - Value1;
        }

        if (nTemp > 0)
        {
            sResult = "+" + nTemp.ToString(SystemFunction.CheckFormatNuberic(0));
        }
        else if (nTemp < 0)
        {
            sResult = nTemp.ToString(SystemFunction.CheckFormatNuberic(0));
        }

        return sResult;
    }

    private void EnableSlot()
    {
        #region old code ตรวจสอบจากข้อมูลพื้นฐาน
        /*
        if (lstTruckSlot.Count > 0)
        {
            decimal minslot = lstTruckSlot.OrderBy(o => o.NCOMPARTNO).FirstOrDefault().NCOMPARTNO;
            ChangeSlot(minslot + "");

            btnSlot1.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 1).Count() > 0);
            btnSlot2.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 2).Count() > 0);
            btnSlot3.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 3).Count() > 0);
            btnSlot4.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 4).Count() > 0);
            btnSlot5.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 5).Count() > 0);
            btnSlot6.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 6).Count() > 0);
            btnSlot7.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 7).Count() > 0);
            btnSlot8.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 8).Count() > 0);
            btnSlot9.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 9).Count() > 0);
            btnSlot10.Enabled = (lstTruckSlot.Where(w => w.NCOMPARTNO == 10).Count() > 0);
        }
        else
        {
            btnSlot1.Enabled = false;
            btnSlot2.Enabled = false;
            btnSlot3.Enabled = false;
            btnSlot4.Enabled = false;
            btnSlot5.Enabled = false;
            btnSlot6.Enabled = false;
            btnSlot7.Enabled = false;
            btnSlot8.Enabled = false;
            btnSlot9.Enabled = false;
            btnSlot10.Enabled = false;
        }
        */
        #endregion

        //ตรวจสอบจากคำขอวัดน้ำ
        string sql = @"SELECT * FROM TBL_REQSLOT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);
        if (dt.Rows.Count > 0)
        {
            var query = dt.AsEnumerable().Select(s => new { SLOT_NO = s.Field<decimal>("SLOT_NO"), LEVEL_NO = s.Field<decimal>("LEVEL_NO"), CAPACITY = s.Field<decimal>("CAPACITY") }).ToList();
            decimal minslot = query.OrderBy(o => o.SLOT_NO).FirstOrDefault().SLOT_NO;
            nSlotAll = query.OrderByDescending(o => o.SLOT_NO).FirstOrDefault().SLOT_NO;
            ChangeSlot(minslot + "");

            btnSlot1.Enabled = (query.Where(w => w.SLOT_NO == 1).Count() > 0);
            btnSlot2.Enabled = (query.Where(w => w.SLOT_NO == 2).Count() > 0);
            btnSlot3.Enabled = (query.Where(w => w.SLOT_NO == 3).Count() > 0);
            btnSlot4.Enabled = (query.Where(w => w.SLOT_NO == 4).Count() > 0);
            btnSlot5.Enabled = (query.Where(w => w.SLOT_NO == 5).Count() > 0);
            btnSlot6.Enabled = (query.Where(w => w.SLOT_NO == 6).Count() > 0);
            btnSlot7.Enabled = (query.Where(w => w.SLOT_NO == 7).Count() > 0);
            btnSlot8.Enabled = (query.Where(w => w.SLOT_NO == 8).Count() > 0);
            btnSlot9.Enabled = (query.Where(w => w.SLOT_NO == 9).Count() > 0);
            btnSlot10.Enabled = (query.Where(w => w.SLOT_NO == 10).Count() > 0);
        }
        else
        {
            btnSlot1.Enabled = false;
            btnSlot2.Enabled = false;
            btnSlot3.Enabled = false;
            btnSlot4.Enabled = false;
            btnSlot5.Enabled = false;
            btnSlot6.Enabled = false;
            btnSlot7.Enabled = false;
            btnSlot8.Enabled = false;
            btnSlot9.Enabled = false;
            btnSlot10.Enabled = false;
        }
    }

    private void GetDataFromPage_AddToListSlot(int nSlot) // ดึงข้อมูลจากหน้าเก็บเข้า list เพื่อใช้ตอนเปลี่ยนช่อง
    {
        // ข้อมูล ของแต่ละช่อง
        if (lstDataSlot.Where(w => w.COMPART_NO == nSlot).Count() > 0)
        {
            int nIndx = lstDataSlot.FindIndex(x => x.COMPART_NO == nSlot);

            lstDataSlot[nIndx].EMER_VALUE = ckbEmergencyValve.Checked ? "1" : "0";
            lstDataSlot[nIndx].MANHOLD_ERR1 = ckbRuinedSel1.Checked ? "1" : "0";
            lstDataSlot[nIndx].MANHOLD_ERR2 = ckbRuinedSel2.Checked ? "1" : "0";
            lstDataSlot[nIndx].SEAL_ERR = ckbOldSelRuine.Checked ? "1" : "0";
            lstDataSlot[nIndx].NEW_SEAL_NO = txtNewNoSel.Text.Trim();
            lstDataSlot[nIndx].cCheckOldSelNo = false;// ckbOldNoSel.Checked;
            lstDataSlot[nIndx].NTOTAL_HEIGHT = ConvertToNullOrNumber(txtNumNewPhase.Text);
            lstDataSlot[nIndx].MANHOLD_NO1 = txtMANHOLD_NO1.Text;
            lstDataSlot[nIndx].MANHOLD_NO2 = txtMANHOLD_NO2.Text;
        }
        else
        {
            lstDataSlot.Add(new TData_CheckSlot
            {
                COMPART_NO = nSlot,
                NTOTAL_HEIGHT = ConvertToNullOrNumber(txtNumNewPhase.Text),
                EMER_VALUE = ckbEmergencyValve.Checked ? "1" : "0",
                MANHOLD_ERR1 = ckbRuinedSel1.Checked ? "1" : "0",
                MANHOLD_ERR2 = ckbRuinedSel2.Checked ? "1" : "0",
                SEAL_ERR = ckbOldSelRuine.Checked ? "1" : "0",
                NEW_SEAL_NO = txtNewNoSel.Text.Trim(),
                cCheckOldSelNo = false, //ckbOldNoSel.Checked
                MANHOLD_NO1 = txtMANHOLD_NO1.Text,
                MANHOLD_NO2 = txtMANHOLD_NO2.Text
            });
        }

        // ข้อมูล ของแป้นแต่ละช่อง
        if (lstDataCapaciltyWater.Where(w => w.COMPART_NO == nSlot).Count() > 0)
        {
            for (int i = 1; i <= 2; i++)
            {
                int nIndx = lstDataCapaciltyWater.FindIndex(x => x.COMPART_NO == nSlot && x.PAN_LEVEL == i);
                if (nIndx != -1)
                {
                    if (i == 1)
                    {
                        lstDataCapaciltyWater[nIndx].HEIGHT2 = ConvertToNullOrNumber(txtNumNewPhaseCompartNo1_Case1.Text);
                        lstDataCapaciltyWater[nIndx].HEIGHT1 = ConvertToNullOrNumber(lblNumNewPhaseCompartNo1_Case2.Text);
                        lstDataCapaciltyWater[nIndx].PAN_FLOOD = ckbWaterOverPlant1.Checked ? 1 : 0;
                        lstDataCapaciltyWater[nIndx].PAN_DRY = ckbWaterLackPlant1.Checked ? 1 : 0;
                        lstDataCapaciltyWater[nIndx].PASSSTANDARD = rblStatus.SelectedItem != null ? rblStatus.SelectedItem.Value + "" : string.Empty;
                        lstDataCapaciltyWater[nIndx].PASSSTANDARD_PAN3 = rblStatusP3.SelectedItem != null ? rblStatusP3.SelectedItem.Value + "" : string.Empty;
                        lstDataCapaciltyWater[nIndx].PAN_ERR = chkPandefective.Checked ? "1" : "0";
                    }
                    else if (i == 2)
                    {
                        lstDataCapaciltyWater[nIndx].HEIGHT2 = ConvertToNullOrNumber(txtNumNewPhaseCompartNo2_Case1.Text);
                        lstDataCapaciltyWater[nIndx].HEIGHT1 = ConvertToNullOrNumber(lblNumNewPhaseCompartNo2_Case2.Text);
                        lstDataCapaciltyWater[nIndx].PAN_FLOOD = ckbWaterOverPlant2.Checked ? 1 : 0;
                        lstDataCapaciltyWater[nIndx].PAN_DRY = ckbWaterLackPlant2.Checked ? 1 : 0;
                        lstDataCapaciltyWater[nIndx].PASSSTANDARD = rblStatus.SelectedItem != null ? rblStatus.SelectedItem.Value + "" : string.Empty;
                        lstDataCapaciltyWater[nIndx].PASSSTANDARD_PAN3 = rblStatusP3.SelectedItem != null ? rblStatusP3.SelectedItem.Value + "" : string.Empty;
                        lstDataCapaciltyWater[nIndx].PAN_ERR = chkPandefective.Checked ? "1" : "0";
                    }
                    else
                    {
                        lstDataCapaciltyWater[nIndx].HEIGHT2 = ConvertToNullOrNumber(lblNumNewPhaseCompartNo3_Case1.Text);
                        lstDataCapaciltyWater[nIndx].HEIGHT1 = ConvertToNullOrNumber(lblNumNewPhaseCompartNo3_Case2.Text);
                        lstDataCapaciltyWater[nIndx].PASSSTANDARD = rblStatus.SelectedItem != null ? rblStatus.SelectedItem.Value + "" : string.Empty;
                        lstDataCapaciltyWater[nIndx].PASSSTANDARD_PAN3 = rblStatusP3.SelectedItem != null ? rblStatusP3.SelectedItem.Value + "" : string.Empty;
                    }
                }
            }
        }
        else // ไม่มีข้อมูล
        {
            for (int i = 1; i <= 2; i++)
            {

                lstDataCapaciltyWater.Add(new TINNER_CHECKINGS
                {
                    REQ_ID = sREQUESTID,
                    COMPART_NO = nSlot,
                    PAN_LEVEL = i,
                    OLD_HEIGHT1 = 0,
                    OLD_HEIGHT2 = 0,
                    HEIGHT1 = i == 1 ? ConvertToNullOrNumber(lblNumNewPhaseCompartNo1_Case2.Text) :
                              i == 2 ? ConvertToNullOrNumber(lblNumNewPhaseCompartNo2_Case2.Text) : ConvertToNullOrNumber(lblNumNewPhaseCompartNo3_Case2.Text),
                    HEIGHT2 = i == 1 ? ConvertToNullOrNumber(txtNumNewPhaseCompartNo1_Case1.Text) :
                              i == 2 ? ConvertToNullOrNumber(txtNumNewPhaseCompartNo2_Case1.Text) : ConvertToNullOrNumber(lblNumNewPhaseCompartNo3_Case1.Text),
                    PAN_FLOOD = i == 1 ? (ckbWaterOverPlant1.Checked ? 1 : 0) :
                                i == 2 ? (ckbWaterOverPlant2.Checked ? 1 : 0) : 0,
                    PAN_DRY = i == 1 ? (ckbWaterLackPlant1.Checked ? 1 : 0) :
                              i == 2 ? (ckbWaterLackPlant1.Checked ? 2 : 0) : 0,
                    PASSSTANDARD = "1",
                    PASSSTANDARD_PAN3 = "1",
                    PAN_ERR = "0"
                });

            }

        }


    }

    private decimal? ConvertToNullOrNumber(string value)
    {
        decimal? sResult = null;
        decimal nTemp = 0;
        if (!string.IsNullOrEmpty(value))
        {
            sResult = decimal.TryParse(value, out nTemp) ? nTemp : 0;
        }

        return sResult;
    }

    private void SaveThisSlotToDataBase(int nSlot)
    {
        if (!string.IsNullOrEmpty(sTruckID_CheckWater) && lstDataSlot.Count > 0 && lstDataCapaciltyWater.Count > 0)
        {
            #region ข้อมูลแต่ละช่อง TBL_CHECKING_COMPART
            string sqlInsert1 = @"INSERT INTO TBL_CHECKING_COMPART(REQUEST_ID,COMPART_NO,STRUCKID,EMER_VALUE,MANHOLD_ERR1,MANHOLD_ERR2,SEAL_ERR,SEAL_NO,NEW_SEAL_NO,NTOTAL_HEIGHT,OLD_NTOTAL_HEIGHT,MANHOLD_NO1,MANHOLD_NO2)
                                                            VALUES(:REQUEST_ID, :COMPART_NO, :STRUCKID, :EMER_VALUE, :MANHOLD_ERR1, :MANHOLD_ERR2, :SEAL_ERR, :SEAL_NO, :NEW_SEAL_NO, :NTOTAL_HEIGHT, :OLD_NTOTAL_HEIGHT,:MANHOLD_NO1,:MANHOLD_NO2)";

            string sqlDel1 = @"DELETE FROM TBL_CHECKING_COMPART WHERE REQUEST_ID = '{0}' AND COMPART_NO = '{1}' AND STRUCKID = '{2}'";

            var querySlot = lstDataSlot.Where(w => w.COMPART_NO == nSlot).FirstOrDefault();

            using (OracleConnection con = new OracleConnection(strConn))
            {
                con.Open();
                if (querySlot != null)
                {
                    //ลบข้อมูลเดิมก่อนเพิ่มใหม่
                    SystemFunction.SQLExecuteNonQuery(strConn, string.Format(sqlDel1, sREQUESTID, nSlot, sTruckID_CheckWater));

                    using (OracleCommand com = new OracleCommand(sqlInsert1, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQUESTID;
                        com.Parameters.Add(":COMPART_NO", OracleType.Number).Value = nSlot;
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = sTruckID_CheckWater;
                        com.Parameters.Add(":EMER_VALUE", OracleType.VarChar).Value = querySlot.EMER_VALUE;
                        com.Parameters.Add(":MANHOLD_ERR1", OracleType.VarChar).Value = querySlot.MANHOLD_ERR1;
                        com.Parameters.Add(":MANHOLD_ERR2", OracleType.VarChar).Value = querySlot.MANHOLD_ERR2;


                        if (querySlot.MANHOLD_NO1 != null)
                        {
                            com.Parameters.Add(":MANHOLD_NO1", OracleType.VarChar).Value = querySlot.MANHOLD_NO1;
                        }
                        else
                        {
                            com.Parameters.Add(":MANHOLD_NO1", OracleType.VarChar).Value = DBNull.Value;
                        }
                        if (querySlot.MANHOLD_NO2 != null)
                        {
                            com.Parameters.Add(":MANHOLD_NO2", OracleType.VarChar).Value = querySlot.MANHOLD_NO2;
                        }
                        else
                        {
                            com.Parameters.Add(":MANHOLD_NO2", OracleType.VarChar).Value = DBNull.Value;
                        }
                        if (querySlot.SEAL_ERR != null)
                        {
                            com.Parameters.Add(":SEAL_ERR", OracleType.VarChar).Value = querySlot.SEAL_ERR;
                        }
                        else
                        {
                            com.Parameters.Add(":SEAL_ERR", OracleType.VarChar).Value = DBNull.Value;
                        }

                        if (querySlot.SEAL_NO != null)
                        {
                            com.Parameters.Add(":SEAL_NO", OracleType.VarChar).Value = querySlot.SEAL_NO;
                        }
                        else
                        {
                            com.Parameters.Add(":SEAL_NO", OracleType.VarChar).Value = DBNull.Value;
                        }


                        if (querySlot.NEW_SEAL_NO != null)
                        {
                            com.Parameters.Add(":NEW_SEAL_NO", OracleType.VarChar).Value = querySlot.NEW_SEAL_NO;
                        }
                        else
                        {
                            com.Parameters.Add(":NEW_SEAL_NO", OracleType.VarChar).Value = DBNull.Value;
                        }

                        if (querySlot.NTOTAL_HEIGHT != null)
                        {
                            com.Parameters.Add(":NTOTAL_HEIGHT", OracleType.Number).Value = querySlot.NTOTAL_HEIGHT.Value;
                        }
                        else
                        {
                            com.Parameters.Add(":NTOTAL_HEIGHT", OracleType.Number).Value = DBNull.Value;
                        }

                        if (querySlot.OLD_NTOTAL_HEIGHT != null)
                        {
                            com.Parameters.Add(":OLD_NTOTAL_HEIGHT", OracleType.Number).Value = querySlot.OLD_NTOTAL_HEIGHT.Value;
                        }
                        else
                        {
                            com.Parameters.Add(":OLD_NTOTAL_HEIGHT", OracleType.Number).Value = DBNull.Value;
                        }

                        com.ExecuteNonQuery();
                    }
                }
            }
            #endregion

            // ข้อมูลแต่ละแป้น
            #region sql 2
            string sqlInsert2 = @"INSERT INTO TBL_INNER_CHECKINGS(REQ_ID,COMPART_NO,PAN_LEVEL,OLD_HEIGHT1,OLD_HEIGHT2,HEIGHT1,HEIGHT2,PAN_FLOOD,PAN_DRY,DATE_CREATED,USER_CREATED,USER_UPDATED,PASSSTANDARD,PASSSTANDARD_PAN3,PAN_ERR)
                                  VALUES(:REQ_ID,:COMPART_NO,:PAN_LEVEL,:OLD_HEIGHT1,:OLD_HEIGHT2,:HEIGHT1,:HEIGHT2,:PAN_FLOOD,:PAN_DRY,SYSDATE,:USER_CREATED,:USER_UPDATED,:PASSSTANDARD,:PASSSTANDARD_PAN3,:PAN_ERR)";
            string sqlCheck2 = "SELECT * FROM TBL_INNER_CHECKINGS WHERE REQ_ID = '{0}' AND COMPART_NO = '{1}' AND  PAN_LEVEL = '{2}'";
            string sqlUpdate = @"UPDATE TBL_INNER_CHECKINGS 
                                        SET 
                                        OLD_HEIGHT1 = :OLD_HEIGHT1,
                                        OLD_HEIGHT2 = :OLD_HEIGHT2,
                                        HEIGHT1= :HEIGHT1,
                                        HEIGHT2= :HEIGHT2,
                                        PAN_FLOOD= :PAN_FLOOD,
                                        PAN_DRY = :PAN_DRY ,
                                        DATE_UPDATED = SYSDATE,
                                        USER_UPDATED = :USER_UPDATED,
                                        PASSSTANDARD = :PASSSTANDARD,
                                        PASSSTANDARD_PAN3 = :PASSSTANDARD_PAN3,
                                        PAN_ERR = :PAN_ERR
                                        WHERE REQ_ID = :REQ_ID AND COMPART_NO = :COMPART_NO AND  PAN_LEVEL = :PAN_LEVEL ";
            #endregion

            if (querySlot != null)
            {

                using (OracleConnection con = new OracleConnection(strConn))
                {
                    con.Open();
                    using (OracleCommand com = new OracleCommand(sqlInsert2, con))
                    {
                        var QPan = lstDataCapaciltyWater.Where(w => w.COMPART_NO == nSlot).OrderBy(o => o.PAN_LEVEL).ToList();
                        foreach (var item in QPan)
                        {
                            if (item.PASSSTANDARD_PAN3 == "0")
                            {
                                UpdateTBL_REQSLOT(sREQUESTID, item.COMPART_NO);
                            }

                            DataTable dtCheck = new DataTable();
                            dtCheck = CommonFunction.Get_Data(strConn, string.Format(sqlCheck2, sREQUESTID, item.COMPART_NO, item.PAN_LEVEL));
                            if (dtCheck.Rows.Count == 0)
                            {
                                #region insert
                                com.Parameters.Clear();
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = sREQUESTID;
                                com.Parameters.Add(":COMPART_NO", OracleType.VarChar).Value = item.COMPART_NO;
                                com.Parameters.Add(":PAN_LEVEL", OracleType.VarChar).Value = item.PAN_LEVEL;
                                com.Parameters.Add(":OLD_HEIGHT1", OracleType.VarChar).Value = item.OLD_HEIGHT1;
                                com.Parameters.Add(":OLD_HEIGHT2", OracleType.VarChar).Value = item.OLD_HEIGHT2;

                                decimal? nCalCause2 = null;
                                nCalCause2 = CaculateCase2(querySlot.NTOTAL_HEIGHT, item.HEIGHT2);

                                if (nCalCause2 != null)
                                {
                                    com.Parameters.Add(":HEIGHT1", OracleType.VarChar).Value = nCalCause2;
                                }
                                else
                                {
                                    com.Parameters.Add(":HEIGHT1", OracleType.VarChar).Value = DBNull.Value;
                                }

                                if (item.HEIGHT2 != null)
                                {
                                    com.Parameters.Add(":HEIGHT2", OracleType.VarChar).Value = item.HEIGHT2.Value;
                                }
                                else
                                {
                                    com.Parameters.Add(":HEIGHT2", OracleType.VarChar).Value = DBNull.Value;
                                }

                                com.Parameters.Add(":PAN_FLOOD", OracleType.Number).Value = item.PAN_FLOOD != null ? (object)item.PAN_FLOOD : DBNull.Value;
                                com.Parameters.Add(":PAN_DRY", OracleType.Number).Value = item.PAN_DRY != null ? (object)item.PAN_DRY : DBNull.Value;
                                com.Parameters.Add(":USER_CREATED", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":USER_UPDATED", OracleType.VarChar).Value = "";
                                com.Parameters.Add(":PASSSTANDARD", OracleType.Char).Value = item.PASSSTANDARD != null ? (object)item.PASSSTANDARD : DBNull.Value;
                                com.Parameters.Add(":PASSSTANDARD_PAN3", OracleType.Char).Value = item.PASSSTANDARD_PAN3 != null ? (object)item.PASSSTANDARD_PAN3 : DBNull.Value;
                                com.Parameters.Add(":PAN_ERR", OracleType.Char).Value = item.PAN_ERR != null ? (object)item.PAN_ERR : DBNull.Value;
                                com.ExecuteNonQuery();
                                #endregion
                            }
                            else // update
                            {
                                using (OracleCommand comUpdate = new OracleCommand(sqlUpdate, con))
                                {
                                    #region update
                                    comUpdate.Parameters.Clear();

                                    //Condition
                                    comUpdate.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = sREQUESTID;
                                    comUpdate.Parameters.Add(":COMPART_NO", OracleType.VarChar).Value = item.COMPART_NO;
                                    comUpdate.Parameters.Add(":PAN_LEVEL", OracleType.VarChar).Value = item.PAN_LEVEL;

                                    //data update
                                    comUpdate.Parameters.Add(":OLD_HEIGHT1", OracleType.VarChar).Value = item.OLD_HEIGHT1;
                                    comUpdate.Parameters.Add(":OLD_HEIGHT2", OracleType.VarChar).Value = item.OLD_HEIGHT2;

                                    decimal? nCalCause2 = null;
                                    nCalCause2 = CaculateCase2(querySlot.NTOTAL_HEIGHT, item.HEIGHT2);

                                    if (nCalCause2 != null)
                                    {
                                        comUpdate.Parameters.Add(":HEIGHT1", OracleType.VarChar).Value = nCalCause2;
                                    }
                                    else
                                    {
                                        comUpdate.Parameters.Add(":HEIGHT1", OracleType.VarChar).Value = DBNull.Value;
                                    }

                                    if (item.HEIGHT2 != null)
                                    {
                                        comUpdate.Parameters.Add(":HEIGHT2", OracleType.VarChar).Value = item.HEIGHT2.Value;
                                    }
                                    else
                                    {
                                        comUpdate.Parameters.Add(":HEIGHT2", OracleType.VarChar).Value = DBNull.Value;
                                    }

                                    comUpdate.Parameters.Add(":PAN_FLOOD", OracleType.Number).Value = item.PAN_FLOOD != null ? (object)item.PAN_FLOOD : DBNull.Value;
                                    comUpdate.Parameters.Add(":PAN_DRY", OracleType.Number).Value = item.PAN_DRY != null ? (object)item.PAN_DRY : DBNull.Value;
                                    comUpdate.Parameters.Add(":USER_UPDATED", OracleType.VarChar).Value = Session["UserID"] + "";
                                    comUpdate.Parameters.Add(":PASSSTANDARD", OracleType.Char).Value = item.PASSSTANDARD != null ? (object)item.PASSSTANDARD : DBNull.Value;
                                    comUpdate.Parameters.Add(":PASSSTANDARD_PAN3", OracleType.Char).Value = item.PASSSTANDARD_PAN3 != null ? (object)item.PASSSTANDARD_PAN3 : DBNull.Value;


                                    if (item.PAN_ERR != null)
                                    {
                                        comUpdate.Parameters.Add(":PAN_ERR", OracleType.Char).Value = item.PAN_ERR;
                                    }
                                    else
                                    {
                                        comUpdate.Parameters.Add(":PAN_ERR", OracleType.Char).Value = DBNull.Value;
                                    }
                                    comUpdate.ExecuteNonQuery();
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //อัพเดทช่อง 3 ตาม slot ที่มีค่า PASSSTANDARD_PAN3 = 0
    private void UpdateTBL_REQSLOT(string SREQ_ID, decimal SLOT)
    {
        string data = "SELECT REQ_ID,COMPART_NO,PASSSTANDARD_PAN3 FROM TBL_INNER_CHECKINGS WHERE REQ_ID = '" + CommonFunction.ReplaceInjection(SREQ_ID) + "' AND PASSSTANDARD_PAN3 = '0'  AND COMPART_NO = '" + CommonFunction.ReplaceInjection(SLOT + "") + "' GROUP BY REQ_ID,COMPART_NO,PASSSTANDARD_PAN3 ASC";

        //if (NAPANLEVEL == 3)
        //{
        string SQL = "UPDATE TBL_REQSLOT SET STATUS_PAN3 = '0' WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(SREQ_ID) + "' AND SLOT_NO = " + CommonFunction.ReplaceInjection(SLOT + "") + " AND LEVEL_NO = 3";
        SystemFunction.SQLExecuteNonQuery(strConn, SQL);
        //}
    }

    private string CompareTime(string vStartH, string vStartM, string vEndH, string vEndM, string cSum)
    {
        string sResult = "";
        string st1 = vStartH + ":" + vStartM;
        string st2 = vEndH + ":" + vEndM;

        if (!string.IsNullOrEmpty(vStartH) && !string.IsNullOrEmpty(vStartM) && !string.IsNullOrEmpty(vEndH) && !string.IsNullOrEmpty(vEndM))
        {
            TimeSpan timeStart = TimeSpan.Parse(st1);
            TimeSpan timeEnd = TimeSpan.Parse(st2);

            TimeSpan difference = timeEnd - timeStart;

            if (cSum == "Y")
            {
                nTotalTimeChecking += difference;
            }

            int hours = difference.Hours;
            int minutes = difference.Minutes;

            sResult = hours + "  ชม.  " + minutes + "  นาที";


        }

        return sResult;
    }

    private string SaveAllToDB()
    {
        string Message_error = "";
        if (nSlotAll > 0 && dtMainData.Rows.Count > 0)
        {
            GetDataFromPage_AddToListSlot(nIndxNowSlot);

            // ข้อมูลช่องและแป้น
            for (int i = 1; i <= nSlotAll; i++)
            {

                SaveThisSlotToDataBase(i);
            }

            // ข้อมูลเวลา
            /*
                5	สูบน้ำคืนกลับ
                4	หยุดน้ำ/วัดแป้น
                3	เวลาเริ่มต้นลงน้ำ
                2	เวลานำรถเข้าตรวจสภาพภายนอก/ใน
                1	เวลานำรถเข้าตรวจ
             */
            DataRow dr = null;
            dr = dtMainData.Rows[0];

            string sqlInsert = @"INSERT INTO TBL_TIME_INNER_CHECKINGS(REQUEST_ID,NID,STRUCKID,VEH_NO,TU_NO,VEH_CHASSIS,TU_CHASSIS,NSTART_HOUR,NSTART_MINUTE,NEND_HOUR,NEND_MINUTE,VEH_ID,TU_ID,USER_EXAMINER,ISACTIVE_FLAG,NVERSION,CCHECKING_WATER,EXAMDATE)
                                                                VALUES(:REQUEST_ID,:NID,:STRUCKID,:VEH_NO,:TU_NO,:VEH_CHASSIS,:TU_CHASSIS,:NSTART_HOUR,:NSTART_MINUTE,:NEND_HOUR,:NEND_MINUTE,:VEH_ID,:TU_ID,:USER_EXAMINER,:ISACTIVE_FLAG,:NVERSION,:CCHECKING_WATER,:EXAMDATE)";

            //string sqlDel = @"DELETE FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + sREQUESTID + "'";

            int CountData = CommonFunction.Count_Value(strConn, "SELECT * FROM TBL_TIME_INNER_CHECKINGS  WHERE  REQUEST_ID    = '" + sREQUESTID + @"'");

            if (CountData > 0)
            {
                //                string sqlDel = @"UPDATE TBL_TIME_INNER_CHECKINGS
                //                            SET     ISACTIVE_FLAG = 'N'
                //                            WHERE  REQUEST_ID    = '" + sREQUESTID + @"'
                //                            AND    NVERSION      = '" + txtNversion.Text + "'";
                string sqlDel = @"DELETE FROM TBL_TIME_INNER_CHECKINGS
                            WHERE  REQUEST_ID = '" + sREQUESTID + @"'";
                SystemFunction.SQLExecuteNonQuery(strConn, sqlDel);
            }

            //string sqldel = @"DELETE FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + sREQUESTID + "'";


            string NSTART_HOUR = "", NSTART_MINUTE = "", NEND_HOUR = "", NEND_MINUTE = "";
            using (OracleConnection con = new OracleConnection(strConn))
            {
                con.Open();
                using (OracleCommand com = new OracleCommand(sqlInsert, con))
                {
                    for (int i = 1; i <= 5; i++)
                    {
                        NSTART_HOUR = ""; NSTART_MINUTE = ""; NEND_HOUR = ""; NEND_MINUTE = "";
                        switch (i)
                        {
                            case 1:
                                NSTART_HOUR = cmbTimeCheckIn_H.SelectedItem.Value + "";
                                NSTART_MINUTE = cmbTimeCheckIn_M.SelectedItem.Value + "";
                                break;
                            case 2:
                                NSTART_HOUR = cmbTime_InnerChecking_H.SelectedItem.Value + "";
                                NSTART_MINUTE = cmbTime_InnerChecking_M.SelectedItem.Value + "";
                                NEND_HOUR = cmbTime_EndInnerChecking_H.SelectedItem.Value + "";
                                NEND_MINUTE = cmbTime_EndInnerChecking_M.SelectedItem.Value + "";
                                break;
                            case 3:
                                NSTART_HOUR = cmbTime_WaterChecking_H.SelectedItem.Value + "";
                                NSTART_MINUTE = cmbTime_WaterChecking_M.SelectedItem.Value + "";
                                NEND_HOUR = cmbTime_EndWaterChecking_H.SelectedItem.Value + "";
                                NEND_MINUTE = cmbTime_EndWaterChecking_M.SelectedItem.Value + "";
                                break;
                            case 4:
                                NSTART_HOUR = cmbTime_StopWaterChecking_H.SelectedItem.Value + "";
                                NSTART_MINUTE = cmbTime_StopWaterChecking_M.SelectedItem.Value + "";
                                NEND_HOUR = cmbTime_EndStopWaterChecking_H.SelectedItem.Value + "";
                                NEND_MINUTE = cmbTime_EndStopWaterChecking_M.SelectedItem.Value + "";
                                break;
                            case 5:
                                NSTART_HOUR = cmbTime_ReturnWaterChecking_H.SelectedItem.Value + "";
                                NSTART_MINUTE = cmbTime_ReturnWaterChecking_M.SelectedItem.Value + "";
                                NEND_HOUR = cmbTime_EndReturnWaterChecking_H.SelectedItem.Value + "";
                                NEND_MINUTE = cmbTime_EndReturnWaterChecking_M.SelectedItem.Value + "";
                                break;
                        }

                        com.Parameters.Clear();
                        com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQUESTID;
                        com.Parameters.Add(":NID", OracleType.Number).Value = i;
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = dr["STRUCKID"] + "";
                        com.Parameters.Add(":VEH_NO", OracleType.VarChar).Value = dr["VEH_NO"] + "";
                        if (!string.IsNullOrEmpty(dr["TU_NO"] + ""))
                        {
                            com.Parameters.Add(":TU_NO", OracleType.VarChar).Value = dr["TU_NO"] + "";
                        }
                        else
                        {
                            com.Parameters.Add(":TU_NO", OracleType.VarChar).Value = DBNull.Value;
                        }

                        com.Parameters.Add(":VEH_CHASSIS", OracleType.VarChar).Value = dr["VEH_CHASSIS"] + "";

                        if (!string.IsNullOrEmpty(dr["TU_CHASSIS"] + ""))
                        {
                            com.Parameters.Add(":TU_CHASSIS", OracleType.VarChar).Value = dr["TU_CHASSIS"] + "";
                        }
                        else
                        {
                            com.Parameters.Add(":TU_CHASSIS", OracleType.VarChar).Value = DBNull.Value;
                        }

                        com.Parameters.Add(":NSTART_HOUR", OracleType.Number).Value = NSTART_HOUR;
                        com.Parameters.Add(":NSTART_MINUTE", OracleType.Number).Value = NSTART_MINUTE;

                        if (i > 1)
                        {
                            com.Parameters.Add(":NEND_HOUR", OracleType.Number).Value = NEND_HOUR;
                            com.Parameters.Add(":NEND_MINUTE", OracleType.Number).Value = NEND_MINUTE;
                        }
                        else
                        {
                            com.Parameters.Add(":NEND_HOUR", OracleType.Number).Value = DBNull.Value;
                            com.Parameters.Add(":NEND_MINUTE", OracleType.Number).Value = DBNull.Value;
                        }

                        if (!string.IsNullOrEmpty(dr["VEH_ID"] + ""))
                        {
                            com.Parameters.Add(":VEH_ID", OracleType.VarChar).Value = dr["VEH_ID"] + "";
                        }
                        else
                        {
                            com.Parameters.Add(":VEH_ID", OracleType.VarChar).Value = DBNull.Value;
                        }

                        if (!string.IsNullOrEmpty(dr["TU_ID"] + ""))
                        {
                            com.Parameters.Add(":TU_ID", OracleType.VarChar).Value = dr["TU_ID"] + "";
                        }
                        else
                        {
                            com.Parameters.Add(":TU_ID", OracleType.VarChar).Value = DBNull.Value;
                        }


                        com.Parameters.Add(":USER_EXAMINER", OracleType.VarChar).Value = cmbUser.Value + "";
                        com.Parameters.Add(":EXAMDATE", OracleType.DateTime).Value = edtCheck.Value != null ? edtCheck.Value : DBNull.Value;
                        com.Parameters.Add(":ISACTIVE_FLAG", OracleType.VarChar).Value = "Y";


                        com.Parameters.Add(":NVERSION", OracleType.Number).Value = !string.IsNullOrEmpty(txtNversion.Text) ? (int.Parse(txtNversion.Text) + 1) : 1;
                        //ไม่ใช้แล้วเนื่องจากเป็นการตรวจทั้งหมด แต่ Passstandard ตรวจรายช่อง ใช้ CCHECKING_WATER แทน
                        //   com.Parameters.Add(":PASSSTANDARD", OracleType.VarChar).Value = rblStatusCheckingWater != null ? rblStatusCheckingWater.Value+"" : "N";
                        com.Parameters.Add(":CCHECKING_WATER", OracleType.VarChar).Value = rblStatusCheckingWater.SelectedItem.Value + "";


                        com.ExecuteNonQuery();
                    }
                }
            }

            // เพิ่มคอมเม้น
            AddComment(sREQUESTID);

            string _sREPORT2VEND = ckbSendMailToVendor.Checked ? "Y" : "N";
            int StatusLog = rblStatusCheckingWater.SelectedIndex == 1 ? 5 : 6;
            // อัพเดทสถานะผลการตรวจวัดน้ำ
            Update_TBL_REQUEST(sREQUESTID);
            //อัพเดทข้อมูล TRUCK
            UpdateTTRUCK_COMPART(sREQUESTID);

            //add log 
            SystemFunction.Add_To_TBL_REQREMARK(sREQUESTID, _sREPORT2VEND, SystemFunction.GetRemark_WorkFlowRequest(StatusLog), "04", "A", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(12), CommonFunction.ReplaceInjection(txtComment.Text), "M");

            if (ckbSendMailToVendor.Checked)
            {
                if (dtMainData.Rows.Count > 0)
                {
                    SendMailToVendor(sREQUESTID, dtMainData.Rows[0]["SABBREVIATION"] + "", dtMainData.Rows[0]["SVENDORID"] + "", rblStatusCheckingWater.SelectedItem.Value + "");
                }
            }

            #region Update SAP

            string STRUCK = dtMainData.Rows[0]["STRUCKID"] + "";
            string VEH_NO = CommonFunction.Get_Value(new OracleConnection(strConn), "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STRUCK + "'");
            string ScarType = dtMainData.Rows[0]["SCARTYPEID"] + "";
            string sMsg = "";
            if (VEH_NO.Trim() != "" && cSAP_SYNC == "1") // cSAP_SYNC = 1 ให้มีการ Syncข้อมูลไปยัง SAP
            {
                try
                {
                    SAP_SYNCOUT_SI veh_syncout = new SAP_SYNCOUT_SI();
                    veh_syncout.VEH_NO = VEH_NO + "";
                    sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                    if (sMsg != "")
                    {
                        //กรณีที่มีการอัพเดทข้อมูลรถ แต่รถยังไม่มีใน SAP
                        if (sMsg.Contains("does not exist"))
                        {
                            SAP_CREATE_VEH veh_create = new SAP_CREATE_VEH();
                            //กรณีที่หัวไม่มีใน SAP
                            if (sMsg.Contains("Entry " + VEH_NO + " does not exist"))
                            {
                                //รถบรรทุก
                                if (ScarType == "0")
                                {
                                    veh_create.sVehicle = VEH_NO + "";
                                    sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                                    if (sMsg.Contains("Transport unit " + VEH_NO + " already exists"))
                                    {
                                        veh_create.sVehicle = VEH_NO + "";
                                        //sMsg = veh_create.CRT_Vehicle_SOS();
                                    }
                                }
                                else
                                {
                                    veh_create.sVehicle = VEH_NO + "";
                                    //sMsg = veh_create.CRT_Vehicle_SOS();
                                }
                            }
                            else
                            {
                                veh_create.sVehicle = VEH_NO + "";
                                sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                            }
                        }
                    }

                    if (sMsg == "")
                    {
                        Message_error += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_check.gif />";
                    }
                    else
                    {
                        Message_error += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_unchecked.gif /><br />" + sMsg;


                    }
                    SystemFunction.SynSapError("RESULT-ADD3", sMsg);
                }
                catch (Exception ect)
                {
                    Message_error = Message_error += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_unchecked.gif /><br /> ไม่สามารถส่งข้อมูลไปยัง SAP ได้";


                    SystemFunction.SynSapError("RESULT-ADD3", ect.ToString());

                }
            }
            #endregion
            Message_error += "<br /> บันทึกข้อมูล(TMS) : <img src=images/stat_check.gif />";

        }
        return Message_error;
    }

    private void UpdateTTRUCK_COMPART(string SREQ_ID)
    {
        string QUERY_SEC = "SELECT * FROM TBL_REQSLOT WHERE REQUEST_ID ='" + CommonFunction.ReplaceInjection(SREQ_ID) + "' AND NVL(STATUS_PAN3,'xxx') = '0'";
        DataTable dt = CommonFunction.Get_Data(strConn, QUERY_SEC);
        if (dt.Rows.Count > 0)
        {
            //เก็บประวัติ
            UpdateTTRUCK_COMPART_HIS(TRUCKID_COMPART);
            //วนลบข้อมูลที่โดนเปลี่ยนใน TTRUCK_COMPART
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string SQL = "DELETE TTRUCK_COMPART  WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(TRUCKID_COMPART) + "' AND NCOMPARTNO = " + CommonFunction.ReplaceInjection(dt.Rows[i]["SLOT_NO"] + "") + " AND NPANLEVEL = " + CommonFunction.ReplaceInjection(dt.Rows[i]["LEVEL_NO"] + "") + "";
                SystemFunction.SQLExecuteNonQuery(strConn, SQL);
            }
        }


    }
    //แอดข้อมูลลงฮิทโทรี่
    private void UpdateTTRUCK_COMPART_HIS(string STRUCKID)
    {
        string SEC_VERSION = "SELECT STRUCKID, MAX(NITEM) as NITEM FROM TTRUCK_COMPART_HIS WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "' GROUP BY STRUCKID";
        DataTable dt = CommonFunction.Get_Data(strConn, SEC_VERSION);

        string INS = "INSERT INTO TTRUCK_COMPART_HIS (STRUCKID,NITEM,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE) VALUES ('{0}',{1},{2},{3},{4},{5},'{6}',{7},'{8}')";

        string SEC_COMPART = "SELECT * FROM TTRUCK_COMPART WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
        DataTable dt2 = CommonFunction.Get_Data(strConn, SEC_COMPART);
        if (dt.Rows.Count > 0)
        {
            string NITEM = (int.Parse(dt.Rows[0]["NITEM"] + "") + 1) + "";
            string DCREATE = "add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt2.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )";
            string DUPDATE = "add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt2.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )";
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                string QUERY = string.Format(INS, CommonFunction.ReplaceInjection(STRUCKID), NITEM, dt2.Rows[i]["NCOMPARTNO"] + "", dt2.Rows[i]["NPANLEVEL"] + "", dt2.Rows[i]["NCAPACITY"] + "", DCREATE, dt2.Rows[i]["SCREATE"] + "", DUPDATE, dt2.Rows[i]["SUPDATE"] + "");
                SystemFunction.SQLExecuteNonQuery(strConn, QUERY);
            }

        }
        else
        {

            string DCREATE = "add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt2.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )";
            string DUPDATE = "add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt2.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )";
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                string QUERY = string.Format(INS, CommonFunction.ReplaceInjection(STRUCKID), "1", dt2.Rows[i]["NCOMPARTNO"] + "", dt2.Rows[i]["NPANLEVEL"] + "", dt2.Rows[i]["NCAPACITY"] + "", DCREATE, dt2.Rows[i]["SCREATE"] + "", DUPDATE, dt2.Rows[i]["SUPDATE"] + "");
                SystemFunction.SQLExecuteNonQuery(strConn, QUERY);
            }
        }

    }

    private void AddComment(string sReqID)
    {
        //TBL_CHECKING_COMMENT
        string sql = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);
        decimal nVersion = 0;
        if (dt.Rows.Count > 0)
        {
            nVersion = decimal.Parse(dt.Rows[0]["NVERSION"] + "");
            nVersion = nVersion + 1;
        }
        else
        {
            nVersion = 1;
        }

        string txt = CommonFunction.ReplaceInjection(txtComment.Text.Trim());
        string sqlInsert = @"INSERT INTO TBL_CHECKING_COMMENT(REQUEST_ID,NVERSION,SCOMMENT) VALUES(:REQUEST_ID,:NVERSION,:SCOMMENT) ";
        if (!string.IsNullOrEmpty(txt))
        {
            using (OracleConnection con = new OracleConnection(strConn))
            {
                con.Open();
                using (OracleCommand com = new OracleCommand(sqlInsert, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sReqID;
                    com.Parameters.Add(":NVERSION", OracleType.Number).Value = nVersion;
                    com.Parameters.Add(":SCOMMENT", OracleType.VarChar).Value = txt;
                    com.ExecuteNonQuery();
                }
            }
        }

    }

    private string GetComment(string sReqID)
    {
        string sResult = "";
        //คอมเม้น/บันทึก
        string sql5 = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dtComment = new DataTable();
        dtComment = CommonFunction.Get_Data(strConn, sql5);

        DataRow dr4 = dtComment.Rows.Count > 0 ? dtComment.Rows[0] : null;

        sResult = dr4 != null ? dr4["SCOMMENT"] + "" : "";

        return sResult;
    }

    private void Update_TBL_REQUEST(string sReqID)
    {

        string STATUS_ID = rblStatusCheckingWater.SelectedIndex == 1 ? "07" : "06";

        //string sql = @"UPDATE TBL_REQUEST SET CCHECKING_WATER = '" + CommonFunction.ReplaceInjection(rblStatusCheckingWater.SelectedItem.Value + "") + "' , STATUS_FLAG = '" + CommonFunction.ReplaceInjection(STATUS_ID) + "' , CHECKWATER_NVERSION = " + (!string.IsNullOrEmpty(txtNversion.Text) ? (int.Parse(txtNversion.Text) + 1) : 1) + " WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ";
        //SystemFunction.SQLExecuteNonQuery(strConn, sql);

        string sql = "";

        if (rblStatusCheckingWater.Value == "Y")
        {

            sql = @"UPDATE TBL_REQUEST SET CCHECKING_WATER = '" + CommonFunction.ReplaceInjection(rblStatusCheckingWater.SelectedItem.Value + "") + "' , STATUS_FLAG = '" + CommonFunction.ReplaceInjection(STATUS_ID) + "' , CHECKWATER_NVERSION = " + (!string.IsNullOrEmpty(txtNversion.Text) ? (int.Parse(txtNversion.Text) + 1) : 1) + " ,WATERON_DATE = SYSDATE , WATEROFF_DATE = (SYSDATE+30) WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ";
            //ในกรณีที่ปิดงานแล้วจะอัพเดท SCAENUM รหัสวัดน้ำ และ SLAST_REQ 

            if (REQTYPE_ID == "01" || REQTYPE_ID == "02")
            {
                string SQLPREV = "SELECT DPREV_SERV,SLAST_REQ_ID,NVL(DNEXT_SERV,DWATEREXPIRE) as DNEXT_SERV FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";
                DataTable dtPrev = CommonFunction.Get_Data(strConn, SQLPREV);
                //ถ้า Request ไอดีเป็นตัวปัจจุบันไม่ต้องอัพเดทข้อมูล
                if (dtPrev.Select("SLAST_REQ_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'").Count() > 0)
                {

                }
                else
                {
                    if (dtPrev.Rows.Count > 0)
                    {
                        string DNEXT_SERV = "";
                        DateTime dtWaterExpire;


                        if (!string.IsNullOrEmpty(dtPrev.Rows[0]["DNEXT_SERV"] + ""))
                        {
                            dtWaterExpire = DateTime.Parse(dtPrev.Rows[0]["DNEXT_SERV"] + "");
                            //กรณีรถหมดอายุวัดน้ำแล้ว ใช้วันปัจจุบัน + 30 วัน
                            if (dtWaterExpire > DateTime.Now)
                            {
                                DNEXT_SERV = dtWaterExpire.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                            }
                            else
                            {
                                DNEXT_SERV = DateTime.Now.AddDays(30).ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                            }

                        }
                        else
                        {
                            DNEXT_SERV = DateTime.Now.AddDays(30).ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                        }


                        string SHEADHEIGHT = !string.IsNullOrEmpty(txtNTANK_HIGHT_HEAD.Text) ? Math.Round(decimal.Parse(txtNTANK_HIGHT_HEAD.Text), 2) + "" : "null";
                        string STAILHEIGHT = !string.IsNullOrEmpty(txtNTANK_HIGHT_TAIL.Text) ? Math.Round(decimal.Parse(txtNTANK_HIGHT_TAIL.Text), 2) + "" : "null";
                        //string QUERY = "UPDATE TTRUCK SET DWATEREXPIRE = TO_DATE('" +
                        //  CommonFunction.ReplaceInjection(DateTime.Parse(lblDateNextCheckWater.Text.Trim()).ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd'),NTANK_HIGH_HEAD = " +
                        //  SHEADHEIGHT + ",NTANK_HIGH_TAIL = " + STAILHEIGHT +
                        //  ",DPREV_SERV = TO_DATE('" +
                        //  CommonFunction.ReplaceInjection(lblDLAST_SERV.Date.ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd') " +
                        //  " WHERE STRUCKID = '" +
                        //  CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";



                        //SystemFunction.SQLExecuteNonQuery(strConn, QUERY);

                        string QUERY = "UPDATE TTRUCK SET DWATEREXPIRE = TO_DATE('" +
              CommonFunction.ReplaceInjection(DateTime.Parse(lblDateNextCheckWater.Text.Trim()).ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd'),NTANK_HIGH_HEAD = " +
              SHEADHEIGHT + ",NTANK_HIGH_TAIL = " + STAILHEIGHT +
              ",DNEXT_SERV = TO_DATE('" +
               CommonFunction.ReplaceInjection(DateTime.Parse(lblDateNextCheckWater.Text.Trim()).ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd') " +
              ",DPREV_SERV = TO_DATE('" +
              CommonFunction.ReplaceInjection(lblDLAST_SERV.Date.ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd') " +
              ",DLAST_SERV = TO_DATE('" +
              CommonFunction.ReplaceInjection(lblDLAST_SERV.Date.ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd') " +
              " WHERE STRUCKID = '" +
              CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "' AND ROWNUM < 3"; //OR STRUCKID = '" + STRUCKID_HEAD + "'


                        SystemFunction.SQLExecuteNonQuery(strConn, QUERY);

                        string QUERY_2 = "UPDATE TBL_REQUEST SET WATER_EXPIRE_DATE = TO_DATE('" +
              CommonFunction.ReplaceInjection(DateTime.Parse(lblDateNextCheckWater.Text.Trim()).ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd') " +
              " WHERE REQUEST_ID = '" +
              CommonFunction.ReplaceInjection(sReqID) + "'";

                        SystemFunction.SQLExecuteNonQuery(strConn, QUERY_2);
                    }
                    else
                    {

                    }
                }
            }
        }
        else
        {
            sql = @"UPDATE TBL_REQUEST SET CCHECKING_WATER = '" +
        CommonFunction.ReplaceInjection(rblStatusCheckingWater.SelectedItem.Value + "") + "' , STATUS_FLAG = '" +
        CommonFunction.ReplaceInjection(STATUS_ID) + "' , CHECKWATER_NVERSION = " + (!string.IsNullOrEmpty(txtNversion.Text) ? (int.Parse(txtNversion.Text) + 1) : 1) + " WHERE REQUEST_ID = '" +
        CommonFunction.ReplaceInjection(sReqID) + "' ";
        }
        string updReq = @"update TBL_REQUEST set SCAR_NUM = '" + txtSCAR_NUM.Text.Trim() + "' , DOCNO = '" + txtDOCNO.Text.Trim() + "' WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
        SystemFunction.SQLExecuteNonQuery(strConn, updReq);
        if (hidHEAD.Value.Trim() != "")
        {
            string updHead = @"update TTRUCK set SCAR_NUM = '" + txtSCAR_NUM.Text.Trim() + "' WHERE SHEADREGISTERNO = '" + CommonFunction.ReplaceInjection(hidHEAD.Value.Trim()) + "'";
            SystemFunction.SQLExecuteNonQuery(strConn, updHead);
        }
        if (hidDETAIL.Value.Trim() != "")
        {
            string updDetail = @"update TTRUCK set SCAR_NUM = '" + txtSCAR_NUM.Text.Trim() + "' WHERE SHEADREGISTERNO = '" + CommonFunction.ReplaceInjection(hidDETAIL.Value.Trim()) + "'";
            SystemFunction.SQLExecuteNonQuery(strConn, updDetail);
        }
        SystemFunction.SQLExecuteNonQuery(strConn, sql);

        string sql2 = @"UPDATE TBL_REQUEST SET STATUS_TAP3 = '" + CommonFunction.ReplaceInjection(STATUS_ID) + "', RESULT_CHECKING_DATE = SYSDATE  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
        SystemFunction.SQLExecuteNonQuery(strConn, sql2);

        //เชคสถานะ tap2 และ tap3 ถ้ามีสถานะ 07 tap ใด tap หนึ่ง ให้ STATUS_FLAG เป็น 07 
        string sql_tap3 = @"SELECT STATUS_TAP2 FROM TBL_REQUEST WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
        DataTable dttap3 = CommonFunction.Get_Data(strConn, sql_tap3);
        if (dttap3.Rows[0]["STATUS_TAP2"].ToString().Trim() == "07")
        {
            string sqlupstatus = @"UPDATE TBL_REQUEST SET STATUS_FLAG = '07', RESULT_CHECKING_DATE = SYSDATE  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
            SystemFunction.SQLExecuteNonQuery(strConn, sqlupstatus);
        }
    }

    private void SetTimeToPage(string sReqID)
    {
        string sql = @"SELECT * FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NID";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);

        string sTimeDiff = "";
        string sTime_H1 = "", sTime_M1 = "";
        if (dt.Rows.Count > 0)
        {

            btnPrintT3.Visible = true;
            foreach (DataRow dr in dt.Rows)
            {
                sTimeDiff = CompareTime(dr["NSTART_HOUR"] + "", dr["NSTART_MINUTE"] + "", dr["NEND_HOUR"] + "", dr["NEND_MINUTE"] + "", "Y");
                switch (dr["NID"] + "")
                {
                    case "1":
                        cmbTimeCheckIn_H.Value = dr["NSTART_HOUR"] + "";
                        cmbTimeCheckIn_M.Value = dr["NSTART_MINUTE"] + "";
                        sTime_H1 = dr["NSTART_HOUR"] + "";
                        sTime_M1 = dr["NSTART_MINUTE"] + "";
                        break;
                    case "2":
                        cmbTime_InnerChecking_H.Value = dr["NSTART_HOUR"] + "";
                        cmbTime_InnerChecking_M.Value = dr["NSTART_MINUTE"] + "";
                        cmbTime_EndInnerChecking_H.Value = dr["NEND_HOUR"] + "";
                        cmbTime_EndInnerChecking_M.Value = dr["NEND_MINUTE"] + "";
                        lblTimeInnerCheckingH.Text = sTimeDiff;
                        break;
                    case "3":
                        cmbTime_WaterChecking_H.Value = dr["NSTART_HOUR"] + "";
                        cmbTime_WaterChecking_M.Value = dr["NSTART_MINUTE"] + "";
                        cmbTime_EndWaterChecking_H.Value = dr["NEND_HOUR"] + "";
                        cmbTime_EndWaterChecking_M.Value = dr["NEND_MINUTE"] + "";
                        lblTimeWaterCheckingH.Text = sTimeDiff;
                        break;
                    case "4":
                        cmbTime_StopWaterChecking_H.Value = dr["NSTART_HOUR"] + "";
                        cmbTime_StopWaterChecking_M.Value = dr["NSTART_MINUTE"] + "";
                        cmbTime_EndStopWaterChecking_H.Value = dr["NEND_HOUR"] + "";
                        cmbTime_EndStopWaterChecking_M.Value = dr["NEND_MINUTE"] + "";
                        lblStopWaterChecking_H.Text = sTimeDiff;
                        break;
                    case "5":
                        cmbTime_ReturnWaterChecking_H.Value = dr["NSTART_HOUR"] + "";
                        cmbTime_ReturnWaterChecking_M.Value = dr["NSTART_MINUTE"] + "";
                        cmbTime_EndReturnWaterChecking_H.Value = dr["NEND_HOUR"] + "";
                        cmbTime_EndReturnWaterChecking_M.Value = dr["NEND_MINUTE"] + "";
                        lblReturnWaterChecking_H.Text = sTimeDiff;
                        lblnTotalTimeCheckingInArea.Text = CompareTime(sTime_H1, sTime_M1, dr["NEND_HOUR"] + "", dr["NEND_MINUTE"] + "", "N");
                        break;
                }
            }

            lblnTotalTimeChecking.Text = nTotalTimeChecking.Hours + "  ชม.  " + nTotalTimeChecking.Minutes + "  นาที";
        }
        else
        {

            btnPrintT3.Visible = false;
        }

        CheckNoPass(sReqID);
    }

    private void CalculateTimeCauseCallBack()
    {
        nTotalTimeChecking = new TimeSpan();
        string sTimeDiff = "";
        string sTime_H1 = "", sTime_M1 = "";
        string NSTART_HOUR = "", NSTART_MINUTE = "", NEND_HOUR = "", NEND_MINUTE = "";

        for (int i = 1; i <= 5; i++)
        {
            NSTART_HOUR = ""; NSTART_MINUTE = ""; NEND_HOUR = ""; NEND_MINUTE = "";
            switch (i)
            {
                case 1:
                    NSTART_HOUR = cmbTimeCheckIn_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTimeCheckIn_M.SelectedItem.Value + "";
                    break;
                case 2:
                    NSTART_HOUR = cmbTime_InnerChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_InnerChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndInnerChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndInnerChecking_M.SelectedItem.Value + "";
                    break;
                case 3:
                    NSTART_HOUR = cmbTime_WaterChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_WaterChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndWaterChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndWaterChecking_M.SelectedItem.Value + "";
                    break;
                case 4:
                    NSTART_HOUR = cmbTime_StopWaterChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_StopWaterChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndStopWaterChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndStopWaterChecking_M.SelectedItem.Value + "";
                    break;
                case 5:
                    NSTART_HOUR = cmbTime_ReturnWaterChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_ReturnWaterChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndReturnWaterChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndReturnWaterChecking_M.SelectedItem.Value + "";
                    break;
            }

            sTimeDiff = CompareTime(NSTART_HOUR, NSTART_MINUTE, NEND_HOUR, NEND_MINUTE, "Y");
            switch (i)
            {
                case 1:
                    sTime_H1 = NSTART_HOUR;
                    sTime_M1 = NSTART_MINUTE;
                    break;
                case 2: lblTimeInnerCheckingH.Text = sTimeDiff; break;
                case 3: lblTimeWaterCheckingH.Text = sTimeDiff; break;
                case 4: lblStopWaterChecking_H.Text = sTimeDiff; break;
                case 5:
                    lblReturnWaterChecking_H.Text = sTimeDiff;
                    lblnTotalTimeCheckingInArea.Text = CompareTime(sTime_H1, sTime_M1, NEND_HOUR, NEND_MINUTE, "N");
                    break;
            }
        }

        lblnTotalTimeChecking.Text = nTotalTimeChecking.Hours + "  ชม.  " + nTotalTimeChecking.Minutes + "  นาที";
    }

    private void CalculateTimeToReport()
    {
        nTotalTimeChecking = new TimeSpan();
        string sTimeDiff = "";
        string sTime_H1 = "", sTime_M1 = "";
        string NSTART_HOUR = "", NSTART_MINUTE = "", NEND_HOUR = "", NEND_MINUTE = "";

        for (int i = 1; i <= 5; i++)
        {
            NSTART_HOUR = ""; NSTART_MINUTE = ""; NEND_HOUR = ""; NEND_MINUTE = "";
            switch (i)
            {
                case 1:
                    NSTART_HOUR = cmbTimeCheckIn_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTimeCheckIn_M.SelectedItem.Value + "";
                    break;
                case 2:
                    NSTART_HOUR = cmbTime_InnerChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_InnerChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndInnerChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndInnerChecking_M.SelectedItem.Value + "";
                    break;
                case 3:
                    NSTART_HOUR = cmbTime_WaterChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_WaterChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndWaterChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndWaterChecking_M.SelectedItem.Value + "";
                    break;
                case 4:
                    NSTART_HOUR = cmbTime_StopWaterChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_StopWaterChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndStopWaterChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndStopWaterChecking_M.SelectedItem.Value + "";
                    break;
                case 5:
                    NSTART_HOUR = cmbTime_ReturnWaterChecking_H.SelectedItem.Value + "";
                    NSTART_MINUTE = cmbTime_ReturnWaterChecking_M.SelectedItem.Value + "";
                    NEND_HOUR = cmbTime_EndReturnWaterChecking_H.SelectedItem.Value + "";
                    NEND_MINUTE = cmbTime_EndReturnWaterChecking_M.SelectedItem.Value + "";
                    break;
            }

            sTimeDiff = CompareTime(NSTART_HOUR, NSTART_MINUTE, NEND_HOUR, NEND_MINUTE, "Y");
            switch (i)
            {
                case 1:
                    sTime_H1 = NSTART_HOUR;
                    sTime_M1 = NSTART_MINUTE;
                    break;
                case 2: lblTimeInnerCheckingH.Text = sTimeDiff; break;
                case 3: lblTimeWaterCheckingH.Text = sTimeDiff; break;
                case 4: lblStopWaterChecking_H.Text = sTimeDiff; break;
                case 5:
                    lblReturnWaterChecking_H.Text = sTimeDiff;
                    lblnTotalTimeCheckingInArea.Text = CompareTime(sTime_H1, sTime_M1, NEND_HOUR, NEND_MINUTE, "N");
                    break;
            }
        }

        lblnTotalTimeChecking.Text = nTotalTimeChecking.Hours + "  ชม.  " + nTotalTimeChecking.Minutes + "  นาที";
    }

    private void SLOTINTRUCK(string STRUCKID)
    {
        string Query = @"SELECT  STRUCKID, NCOMPARTNO FROM TTRUCK_COMPART WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "' AND NPANLEVEL <> 0  GROUP BY STRUCKID,NCOMPARTNO ORDER BY NCOMPARTNO ASC";
        DataTable dt = CommonFunction.Get_Data(strConn, Query);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lstSlot.Add(new SLOT
                {
                    REQ_ID = sREQUESTID,
                    NSLOT = int.Parse(dt.Rows[i]["NCOMPARTNO"] + ""),
                    INDEX = i
                });
            }
        }

    }

    #region class

    [Serializable]
    class TTime
    {
        public int nHour { get; set; }
        public int nMinute { get; set; }
        public string sTXT { get; set; }
    }

    [Serializable]
    class TINNER_CHECKINGS
    {
        public string REQ_ID { get; set; }
        public decimal COMPART_NO { get; set; } //ช่องที่ 
        public decimal PAN_LEVEL { get; set; } //ระดับแป้น หรือแป้นที่
        public decimal OLD_HEIGHT1 { get; set; } //ระยะช่องว่าง (มม.) จากปากถังถึงแป้น(เดิม)
        public decimal OLD_HEIGHT2 { get; set; } // ส่วนสูง (มม.) จากก้นถังถึงแป้น(เดิม)
        public decimal? HEIGHT1 { get; set; } //ระยะช่องว่าง (มม.) จากปากถังถึงแป้น
        public decimal? HEIGHT2 { get; set; } // ส่วนสูง (มม.) จากก้นถังถึงแป้น
        //public decimal COORDINATE1 { get; set; }
        //public decimal PAN1 { get; set; }
        //public decimal COORDINATE2 { get; set; }
        //public decimal PAN2 { get; set; }
        public string SEAL_NO { get; set; } // หมายเลขซีลเดิม
        //public string LEAD_SEAL { get; set; } // หมายเลขซีลตะกั่ว
        //public string SEAL_ERR { get; set; } // สภาพซีลเดิม (''=ยังไม่ตรวจ,'0'=ไม่ชำรุด,'1'=ชำรุด)
        //public string NEW_SEAL_NO { get; set; } // หมายเลขซีลใหม่
        //public string MANHOLD_ERR1 { get; set; } 
        //public string MANHOLD_ERR2 { get; set; }
        //public string MANHOLD_NO1 { get; set; }
        //public string MANHOLD_NO2 { get; set; }
        //public string SD_VALUE { get; set; }
        //public string REMARK { get; set; }
        public decimal? PAN_FLOOD { get; set; } // ระดับน้ำท่วมแป้น (ลิตร) // แต่เก็บใหม่เป็น 1 ท่วม , 0 คือไม่ท่วม
        public decimal? PAN_DRY { get; set; } // ระดับน้ำขาดแป้น (ลิตร) // แต่เก็บใหม่เป็น 1 ขา , 0 คือไม่ขาด
        public string PASSSTANDARD { get; set; }
        public string PASSSTANDARD_PAN3 { get; set; }
        public string PAN_ERR { get; set; }

    }
    [Serializable]
    class TTRUCK_COMPART
    {
        public string STRUCKID { get; set; }
        public decimal NCOMPARTNO { get; set; }
        public decimal NPANLEVEL { get; set; }
        public decimal NCAPACITY { get; set; }
    }
    [Serializable]
    class TData_CheckSlot
    {
        public decimal COMPART_NO { get; set; } // ช่องที่
        public decimal? OLD_NTOTAL_HEIGHT { get; set; } // ระยะรวมเดิม
        public decimal? NTOTAL_HEIGHT { get; set; } // ระยะรวม
        public string EMER_VALUE { get; set; }  // วาล์วฉุกเฉิน (''=ยังไม่ตรวจ,'0'=ไม่มี,'1'=มี)
        public string MANHOLD_ERR1 { get; set; } //สภาพซีลฝา manhold 1 (''=ยังไม่ตรวจ,'0'=ไม่ชำรุด,'1'=ชำรุด)
        public string MANHOLD_ERR2 { get; set; } //สภาพซีลฝา manhold 2 (''=ยังไม่ตรวจ,'0'=ไม่ชำรุด,'1'=ชำรุด)
        public string SEAL_ERR { get; set; } //  สภาพซีลเดิม (''=ยังไม่ตรวจ,'0'=ไม่ชำรุด,'1'=ชำรุด)
        public string SEAL_NO { get; set; } // หมายเลขซีลเดิม
        public string NEW_SEAL_NO { get; set; } //หมายเลขซีลใหม่
        public bool cCheckOldSelNo { get; set; } //หมายเลขซีล แป้นเดิม
        public string MANHOLD_NO1 { get; set; } // หมายเลขซีลฝา manhold 1
        public string MANHOLD_NO2 { get; set; } // หมายเลขซีลฝา manhold 2
        public string PAN_ERR { get; set; }
    }
    [Serializable]
    class SLOT
    {
        public string REQ_ID { get; set; }
        public int NSLOT { get; set; }
        public int INDEX { get; set; }
    }
    #endregion

    private void ListReport(string sReqID)
    {
        #region data table //
        //        string sql1 = @"SELECT TCC.REQUEST_ID,TCC.COMPART_NO, TIC.PAN_LEVEL,TRC.NCAPACITY,TIC.HEIGHT1,TIC.HEIGHT2,
        //                                CASE WHEN TCC.EMER_VALUE = '0' THEN 'ไม่มี' WHEN TCC.EMER_VALUE = '1' THEN 'มี' ELSE '-' END AS EMER_VALUE,
        //                                TCC.SEAL_NO,
        //                                CASE WHEN TCC.SEAL_ERR = '0' THEN 'ไม่ชำรุด'  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
        //                                CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
        //                                TCC.NEW_SEAL_NO
        //                                 FROM TBL_CHECKING_COMPART TCC
        //                                 INNER JOIN TBL_INNER_CHECKINGS TIC ON TCC.REQUEST_ID = TIC.REQ_ID AND TCC.COMPART_NO = TIC.COMPART_NO
        //                                 LEFT JOIN TTRUCK_COMPART TRC ON  TCC.STRUCKID  = TRC.STRUCKID AND  TIC.COMPART_NO = TRC.NCOMPARTNO AND TIC.PAN_LEVEL = TRC.NPANLEVEL
        //                                 WHERE REQUEST_ID = '{0}'
        //                                 ORDER BY TCC.COMPART_NO,TIC.PAN_LEVEL ASC";

        //        string sql1 = @"SELECT TCC.REQUEST_ID,TCC.COMPART_NO, TIC.PAN_LEVEL,NVL(TRC.NCAPACITY,REQ.CAPACITY) as NCAPACITY,TIC.HEIGHT1,TIC.HEIGHT2,
        //                                CASE WHEN TCC.EMER_VALUE = '0' THEN '' WHEN TCC.EMER_VALUE = '1' THEN '*' ELSE '-' END AS EMER_VALUE,
        //                                TCC.SEAL_NO,
        //                                CASE WHEN TCC.SEAL_ERR = '0' THEN 'ไม่ชำรุด'  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
        //                                CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
        //                                TCC.NEW_SEAL_NO
        //                                 FROM TBL_CHECKING_COMPART TCC
        //                                 INNER JOIN TBL_INNER_CHECKINGS TIC ON TCC.REQUEST_ID = TIC.REQ_ID AND TCC.COMPART_NO = TIC.COMPART_NO
        //                                 LEFT JOIN TTRUCK_COMPART TRC ON  TCC.STRUCKID  = TRC.STRUCKID AND  TIC.COMPART_NO = TRC.NCOMPARTNO AND TIC.PAN_LEVEL = TRC.NPANLEVEL
        //                                 LEFT JOIN TBL_REQSLOT REQ ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO   AND TIC.PAN_LEVEL = REQ.LEVEL_NO
        //                                 WHERE REQUEST_ID = '{0}'
        //                                 ORDER BY TCC.COMPART_NO,TIC.PAN_LEVEL ASC";

        string sql1 = @"SELECT REQ.REQUEST_ID,REQ.SLOT_NO as COMPART_NO, REQ.LEVEL_NO as PAN_LEVEL,REQ.CAPACITY as NCAPACITY  ,TIC.HEIGHT1,TIC.HEIGHT2,
CASE WHEN TCC.EMER_VALUE = '0' THEN '' WHEN TCC.EMER_VALUE = '1' THEN '*' ELSE '-' END AS EMER_VALUE,
TCC.SEAL_NO,
CASE WHEN TCC.SEAL_ERR = '0' THEN ' '  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
TCC.NEW_SEAL_NO
FROM TBL_REQSLOT REQ
LEFT JOIN  TBL_CHECKING_COMPART TCC  ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO  -- AND TCC. = REQ.LEVEL_NO
LEFT JOIN TBL_INNER_CHECKINGS TIC ON REQ.REQUEST_ID = TIC.REQ_ID AND REQ.SLOT_NO = TIC.COMPART_NO AND TIC.PAN_LEVEL = REQ.LEVEL_NO
 WHERE REQ.REQUEST_ID = '{0}'
ORDER BY REQ.SLOT_NO,REQ.LEVEL_NO  ASC";


        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql1, CommonFunction.ReplaceInjection(sReqID)));



        var query = dt.AsEnumerable().Select(s => new {
            COMPART_NO = s.Field<decimal>("COMPART_NO"),
            PAN_LEVEL = s.Field<decimal>("PAN_LEVEL"),
            NCAPACITY = s["NCAPACITY"] != DBNull.Value ? s.Field<decimal>("NCAPACITY") : 0 //s.Field<decimal>("NCAPACITY") 
        }).ToList();
        decimal nSum_Capacity = 0;
        if (query.Count > 0)
        {
            var query2 = query.GroupBy(g => new { g.COMPART_NO }).Select(s => new { s.Key.COMPART_NO, NCAPACITY = s.Max(x => x.NCAPACITY) }).ToList();
            nSum_Capacity = query2.Count > 0 ? query2.Sum(s => s.NCAPACITY) : 0;
            //nSum_Capacity = query2.Sum(s => s.NCAPACITY);
        }

        //if (dt.Rows.Count > 0)
        //{

        //}
        //else
        //{
        //    DataRow dr = null;
        //    for (int i = dt.Rows.Count; i <= 18; i++)
        //    {
        //        dr = dt.NewRow();
        //        dr[0] = ""; // or you could generate some random string.
        //        dt.Rows.Add(dr);
        //    }
        //}
        #endregion

        rpt_InnerChecking_FM001 report = new rpt_InnerChecking_FM001();
        rpt_InnerChecking_ForDriver_FM001 reportDriver = new rpt_InnerChecking_ForDriver_FM001();

        // parameter
        //SENGINE หมายเลขเคื่อง


        // set control
        ((XRLabel)report.FindControl("xrlblsDay", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("rxlblsYear", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));

        // set control
        ((XRLabel)reportDriver.FindControl("xrlblsDay", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)reportDriver.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)reportDriver.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)reportDriver.FindControl("rxlblsYear", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));

        //string[] arrNo1 = sREQUESTID.Split('-');

        string QUERY_AUOTNUM = @"SELECT d.ATN,d.REQUEST_ID, SERVICE_DATE FROM
(
SELECT ROWNUM as ATN,o.* FROM
(
    SELECT SERVICE_DATE,REQUEST_ID
    ,TO_NUMBER(SUBSTR(REQUEST_ID,0,2)) as YY 
    ,TO_NUMBER(SUBSTR(REQUEST_ID,3,2)) as MM
    ,TO_NUMBER(SUBSTR(REQUEST_ID,9,4)) as RNM  
    FROM TBL_REQUEST
    WHERE NVL(TO_CHAR(SERVICE_DATE),'XXX') <> 'XXX' 
    /* เพิ่ม เพื่อเปลียน ปี เลขที่RUN จะนับเริ่มนับใหม่ AND to_char(SERVICE_DATE,'yy') = to_char(sysdate,'yy') */
    --AND to_char(SERVICE_DATE,'yy') = to_char(sysdate,'yy')
    /*เงื่อนไขในการหาว่า จะดูจากวันที่เข้ารับบริการ ปี เดือน เลขที่RUN ตามลำดับ */
    ORDER BY 
    SERVICE_DATE ASC
    ,TO_NUMBER(SUBSTR(REQUEST_ID,0,2)) ASC
    ,TO_NUMBER(SUBSTR(REQUEST_ID,3,2)) ASC
    ,TO_NUMBER(SUBSTR(REQUEST_ID,9,4)) ASC
)o
)d
WHERE d.REQUEST_ID = '" + sREQUESTID + "'";

        DataTable dt_AUTONUM = CommonFunction.Get_Data(strConn, QUERY_AUOTNUM);

        //เต้ย

        string running = "";

        //string sqldoc = @"SELECT DOCNO FROM TBL_REQUEST WHERE REQUEST_ID = '" + sREQUESTID + "'";
        //DataTable doc = CommonFunction.Get_Data(strConn, sqldoc);
        if (txtDOCNO.Text.Trim() != "")
        {
            running = txtDOCNO.Text.Trim();
        }
        else
        {
            DataTable dtrunning = VehicleBLL.Instance.GENERATE_DOC_MV(string.Empty, USER_ID);
            running = dtrunning.Rows[0]["DOC_ID"].ToString().Trim();
        }
        string updoc = @"update TBL_REQUEST set DOCNO = '" + running + "' WHERE REQUEST_ID = '" + sREQUESTID + "'";
        SystemFunction.SQLExecuteNonQuery(strConn, updoc);

        string first = running.Substring(0, 4);
        string last = running.Substring(5);
        // ((XRLabel)report.FindControl("xrlblNo1", true)).Text = dt_AUTONUM.Rows.Count > 0 ? (dt_AUTONUM.Rows[0]["ATN"] + "").PadLeft(4, '0') : "";
        //((XRLabel)report.FindControl("xrlblNo2", true)).Text = DateTime.Now.ToString("yy", new CultureInfo("th-TH"));
        // ((XRLabel)report.FindControl("xrlblNo2", true)).Text = Convert.ToDateTime(dt_AUTONUM.Rows[0]["SERVICE_DATE"]).ToString("yy");
        //  ((XRLabel)report.FindControl("xrLabel44", true)).Text = sREQUESTID;
        ((XRLabel)report.FindControl("xrlblNo1", true)).Text = first;
        ((XRLabel)report.FindControl("xrlblNo2", true)).Text = last;
        ((XRLabel)report.FindControl("xrLabel55", true)).Text = "Rev.2 (" + DateTime.Now.ToString("dd/MM/yy", new CultureInfo("th-TH")) + ")";
        ((XRLabel)report.FindControl("xrLabel56", true)).Text = "FM-มว.-001";

        //((XRLabel)reportDriver.FindControl("xrlblNo1", true)).Text = dt_AUTONUM.Rows.Count > 0 ? (dt_AUTONUM.Rows[0]["ATN"] + "").PadLeft(4, '0') : "";
        //((XRLabel)reportDriver.FindControl("xrlblNo2", true)).Text = DateTime.Now.ToString("yy", new CultureInfo("th-TH"));
        //((XRLabel)reportDriver.FindControl("xrlblNo2", true)).Text = Convert.ToDateTime(dt_AUTONUM.Rows[0]["SERVICE_DATE"]).ToString("yy");
        ((XRLabel)reportDriver.FindControl("xrlblNo1", true)).Text = first;
        ((XRLabel)reportDriver.FindControl("xrlblNo2", true)).Text = last;
        //((XRLabel)reportDriver.FindControl("xrLabel44", true)).Text = sREQUESTID;

        ((XRLabel)reportDriver.FindControl("xrLabel55", true)).Text = "Rev.2 (" + DateTime.Now.ToString("dd/MM/yy", new CultureInfo("th-TH")) + ")";
        ((XRLabel)reportDriver.FindControl("xrLabel56", true)).Text = "FM-มว.-001";

        //
        string sql2 = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TRQ.VEH_ID,TRQ.TU_ID,
TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TSR.ENDPROCESS,TRQ.APPOINTMENT_DATE,
TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS, TRT.RENEWLICENSE
 FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
 LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
 WHERE TRQ.REQUEST_ID = '{0}'";

        DataTable dtPara1 = new DataTable();
        dtPara1 = CommonFunction.Get_Data(strConn, string.Format(sql2, sReqID));

        //sTruckID_CheckWater เก็บ id รถที่ใช้วัดน้ำ
        string sql3 = @"SELECT TRK.*,TRKT.SCARTYPENAME,NVL(TB.BRANDNAME,TRH.SBRAND) as SBRANDH,TRH.SBRAND as ss FROM TTRUCK  TRK 
LEFT JOIN TTRUCK TRH/*ข้อมูลส่วนหัว*/
ON  TRK.SHEADID = TRH.STRUCKID
LEFT JOIN TTRUCK_BRAND TB/*ข้อมูลยี่ห้อรถส่วนหัว*/
ON TB.BRANDID = CASE TRK.SCARTYPEID WHEN 0 THEN TRK.SBRAND ELSE TRH.SBRAND END
LEFT JOIN TTRUCKTYPE TRKT 
ON  TRK.SCARTYPEID = TRKT.SCARTYPEID
WHERE  TRK.STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";
        DataTable dtTruck = new DataTable();
        dtTruck = CommonFunction.Get_Data(strConn, sql3);

        //เวลา
        string sql4 = @"SELECT * FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NID";
        DataTable dtTime = new DataTable();
        dtTime = CommonFunction.Get_Data(strConn, sql4);
        decimal nTemp = 0;

        string vStartH = "", vStartM = "", vEndH = "", vEndM = "";

        if (dtPara1.Rows.Count == 1 && dtTruck.Rows.Count == 1)
        {
            DataRow dr1 = null, dr2 = null, dr3 = null;

            dr1 = dtPara1.Rows[0];
            dr2 = dtTruck.Rows[0];
            dr3 = dtMainData.Rows[0];

            report.Parameters["sCompName"].Value = dr1["SABBREVIATION"] + "";
            report.Parameters["sTU_NO"].Value = dr1["TU_NO"] + "" != "" ? dr1["TU_NO"] + "" : "-";
            report.Parameters["sTruckID"].Value = dr2["SCAR_NUM"];
            report.Parameters["sSerialNumber"].Value = dr2["SENGINE"] + "" != "" ? dr2["SENGINE"] + "" : "-";
            report.Parameters["sTU_CHASSIS"].Value = dr1["TU_CHASSIS"] + "" != "" ? dr1["TU_CHASSIS"] + "" : "-";
            report.Parameters["sCarType"].Value = dr2["SCARTYPENAME"] + "";
            report.Parameters["sVEH_NO"].Value = dr1["VEH_NO"] + "" != "" ? dr1["VEH_NO"] + "" : "-";
            report.Parameters["sBRAND"].Value = dr2["SBRAND"] + "" != "" ? dr2["SBRAND"] + "" : "-";
            report.Parameters["sVEH_CHASSIS"].Value = dr1["VEH_CHASSIS"] + "" != "" ? dr1["VEH_CHASSIS"] + "" : "-";
            report.Parameters["nNumSlot"].Value = nSlotAll + "";

            reportDriver.Parameters["sCompName"].Value = dr1["SABBREVIATION"] + "";
            reportDriver.Parameters["sTU_NO"].Value = dr1["TU_NO"] + "" != "" ? dr1["TU_NO"] + "" : "-";
            reportDriver.Parameters["sTruckID"].Value = dr2["SCAR_NUM"];
            reportDriver.Parameters["sSerialNumber"].Value = dr2["SENGINE"] + "" != "" ? dr2["SENGINE"] + "" : "-";
            reportDriver.Parameters["sTU_CHASSIS"].Value = dr1["TU_CHASSIS"] + "" != "" ? dr1["TU_CHASSIS"] + "" : "-";
            reportDriver.Parameters["sCarType"].Value = dr2["SCARTYPENAME"] + "";
            reportDriver.Parameters["sVEH_NO"].Value = dr1["VEH_NO"] + "" != "" ? dr1["VEH_NO"] + "" : "-";
            reportDriver.Parameters["sBRAND"].Value = dr2["SBRAND"] + "" != "" ? dr2["SBRAND"] + "" : "-";
            reportDriver.Parameters["sVEH_CHASSIS"].Value = dr1["VEH_CHASSIS"] + "" != "" ? dr1["VEH_CHASSIS"] + "" : "-";
            reportDriver.Parameters["nNumSlot"].Value = nSlotAll + "";

            nTemp = decimal.TryParse(dr2["NTOTALCAPACITY"] + "", out nTemp) ? nTemp : 0;
            report.Parameters["nSum_Capacity"].Value = dr2["NTOTALCAPACITY"] + "" != "" ? (nTemp > 0 ? nTemp.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "-";
            report.Parameters["nWeight_Sum"].Value = dr2["NWEIGHT"] + "" != "" ? dr2["NWEIGHT"] + "" : "-";
            report.Parameters["sNB"].Value = dr2["NLOAD_WEIGHT"] + "" != "" ? dr2["NLOAD_WEIGHT"] + "" : "-";
            report.Parameters["dLastCheckWater"].Value = dr2["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DLAST_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : (dr2["DPREV_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DPREV_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-");   // dr1["TU_NO"] + "";
            report.Parameters["sHeight_H2T"].Value = dr2["NTANK_HIGH_HEAD"] + "";
            report.Parameters["sHeight_EndT"].Value = dr2["NTANK_HIGH_TAIL"] + "";
            report.Parameters["sTypeMaterailTank"].Value = dr2["STANK_MATERAIL"] + "" != "" ? dr2["STANK_MATERAIL"] + "" : "-";
            report.Parameters["sAddress"].Value = dr1["SNO"] + "  " + dr1["SDISTRICT"] + "  " + dr1["SREGION"] + "  จังหวัด " + dr1["SPROVINCE"] + "  " + dr1["SPROVINCECODE"] + "";
            report.Parameters["dService"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";

            reportDriver.Parameters["nSum_Capacity"].Value = dr2["NTOTALCAPACITY"] + "" != "" ? (nTemp > 0 ? nTemp.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "-";
            reportDriver.Parameters["nWeight_Sum"].Value = dr2["NWEIGHT"] + "" != "" ? dr2["NWEIGHT"] + "" : "-";
            reportDriver.Parameters["sNB"].Value = dr2["NLOAD_WEIGHT"] + "" != "" ? dr2["NLOAD_WEIGHT"] + "" : "-";
            reportDriver.Parameters["dLastCheckWater"].Value = dr2["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DLAST_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : (dr2["DPREV_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DPREV_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-");   // dr1["TU_NO"] + "";
            reportDriver.Parameters["sHeight_H2T"].Value = dr2["NTANK_HIGH_HEAD"] + "";
            reportDriver.Parameters["sHeight_EndT"].Value = dr2["NTANK_HIGH_TAIL"] + "";
            reportDriver.Parameters["sTypeMaterailTank"].Value = dr2["STANK_MATERAIL"] + "" != "" ? dr2["STANK_MATERAIL"] + "" : "-";
            reportDriver.Parameters["sAddress"].Value = dr1["SNO"] + "  " + dr1["SDISTRICT"] + "  " + dr1["SREGION"] + "  จังหวัด " + dr1["SPROVINCE"] + "  " + dr1["SPROVINCECODE"] + "";
            reportDriver.Parameters["dService"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";

            string sShow = "";
            decimal nTempH = 0, nTempM = 0;
            foreach (DataRow row in dtTime.Rows)
            {
                nTempH = 0; nTempM = 0;
                nTempH = decimal.TryParse(row["NSTART_HOUR"] + "", out nTempH) ? nTempH : 0;
                nTempM = decimal.TryParse(row["NSTART_MINUTE"] + "", out nTempM) ? nTempM : 0;
                if (nTempH < 10)
                {
                    sShow = "0" + nTempH + ":";
                }
                else
                {
                    sShow = nTempH + ":";
                }

                if (nTempM < 10)
                {
                    sShow = sShow + "0" + nTempM;
                }
                else
                {
                    sShow = sShow + nTempM;
                }


                switch (row["NID"] + "")
                {
                    case "1": //เวลานำรถเข้าตรวจ
                        report.Parameters["sTime_dService"].Value = sShow;
                        reportDriver.Parameters["sTime_dService"].Value = sShow;
                        vStartH = row["NSTART_HOUR"] + "";
                        vStartM = row["NSTART_MINUTE"] + "";
                        break;
                    case "2"://เวลานำรถเข้าตรวจสภาพภายนอก/ใน
                        break;
                    case "3": //เวลาเริ่มต้นลงน้ำ
                        report.Parameters["sTime_DownWater"].Value = sShow;
                        reportDriver.Parameters["sTime_DownWater"].Value = sShow;
                        break;
                    case "4": //หยุดน้ำ/วัดแป้น
                        //report.Parameters["sTime_StopCheckWater"].Value = sShow;
                        report.Parameters["sTime_ReturnWater"].Value = sShow;
                        reportDriver.Parameters["sTime_ReturnWater"].Value = sShow;
                        break;
                    case "5": //สูบน้ำคืนกลับ
                        //report.Parameters["sTime_ReturnWater"].Value = sShow;
                        report.Parameters["sTime_StopCheckWater"].Value = sShow;
                        reportDriver.Parameters["sTime_StopCheckWater"].Value = sShow;
                        vEndH = row["NSTART_HOUR"] + "";
                        vEndM = row["NSTART_MINUTE"] + "";
                        break;
                }
            }

            string st1 = vStartH + ":" + vStartM;
            string st2 = vEndH + ":" + vEndM;
            int hours = 0, minutes = 0;

            if (!string.IsNullOrEmpty(vStartH) && !string.IsNullOrEmpty(vStartM) && !string.IsNullOrEmpty(vEndH) && !string.IsNullOrEmpty(vEndM))
            {
                TimeSpan timeStart = TimeSpan.Parse(st1);
                TimeSpan timeEnd = TimeSpan.Parse(st2);

                TimeSpan difference = timeEnd - timeStart;

                hours = difference.Hours;
                minutes = difference.Minutes;
            }

            report.Parameters["nSumTimeChecking"].Value = hours + "." + minutes;
            reportDriver.Parameters["nSumTimeChecking"].Value = hours + "." + minutes;

            string sDateNextCheckWater = "";
            DateTime dNextCheckWater;

            if (!string.IsNullOrEmpty(dr2["DNEXT_SERV"] + ""))
            {
                dNextCheckWater = Convert.ToDateTime(dr2["DNEXT_SERV"] + "", new CultureInfo("th-TH"));
                if (dr1["ENDPROCESS"].ToString() == "N" && dr1["RENEWLICENSE"].ToString() == "Y") // กรณีที่ยังไม่ปิดงาน & และเป็นใบงานที่ต่อวันหมดอายุวัดน้ำ
                //if (dNextCheckWater.Year == DateTime.Now.Year) // ยังไม่ได้อัพเดทข้อมูลวัดน้ำใน TTRUCK --- Agile Code
                {
                    //sDateNextCheckWater = dNextCheckWater.AddYears(nNextYeatCheckWater).ToString("dd MMM yyyy"); --- Agile Code
                    sDateNextCheckWater = dr3["SERVICE_DATE"] == null
                        ? string.Empty
                        : Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-TH")).AddYears(nNextYeatCheckWater).ToString("dd MMM yyyy");
                    // *** ต้องตรวจสอบประเด็น วัดน้ำก่อนวันหมดอายุวัดน้ำ (เพิ่มได้ไม่เกิน 75 วัน)
                }
                else
                {
                    sDateNextCheckWater = dNextCheckWater.ToString("dd MMM yyyy");
                }
            }
            else
            {
                sDateNextCheckWater = DateTime.Now.ToString("dd MMM yyyy");
            }




            //report.Parameters["dNextCheckWater"].Value = sDateNextCheckWater;

            report.Parameters["dNextCheckWater"].Value = dr3["DWATER"] + "" != "" ? Convert.ToDateTime(dr3["DWATER"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            report.Parameters["dCompletCheckWater"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            report.Parameters["nSumCapacity"].Value = nSum_Capacity.ToString(SystemFunction.CheckFormatNuberic(0));

            //reportDriver.Parameters["dNextCheckWater"].Value = sDateNextCheckWater;

            reportDriver.Parameters["dNextCheckWater"].Value = dr3["DWATER"] + "" != "" ? Convert.ToDateTime(dr3["DWATER"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            reportDriver.Parameters["dCompletCheckWater"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            reportDriver.Parameters["nSumCapacity"].Value = nSum_Capacity.ToString(SystemFunction.CheckFormatNuberic(0));
        }


        string sUSER = @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME,SUID.SPOSITION FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'  AND ISACTIVE_FLAG = 'Y'";
        DataTable dtUSER = CommonFunction.Get_Data(strConn, sUSER);
        if (dtUSER.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrlblFullNameControl", true)).Text = dtUSER.Rows[0]["SNAME"] + "";
            ((XRLabel)report.FindControl("xrblbPositionControl", true)).Text = dtUSER.Rows[0]["SPOSITION"] + "";

            ((XRLabel)reportDriver.FindControl("xrlblFullNameControl", true)).Text = dtUSER.Rows[0]["SNAME"] + "";
            ((XRLabel)reportDriver.FindControl("xrblbPositionControl", true)).Text = dtUSER.Rows[0]["SPOSITION"] + "";
        }


        ((XRLabel)report.FindControl("xrlblsDay2", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrlblsMonth2", true)).Text = DateTime.Now.ToString("MMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblsYear2", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblComment", true)).Text = GetComment(sReqID);

        ((XRLabel)reportDriver.FindControl("xrlblsDay2", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)reportDriver.FindControl("xrlblsMonth2", true)).Text = DateTime.Now.ToString("MMM", new CultureInfo("th-TH"));
        ((XRLabel)reportDriver.FindControl("xrlblsYear2", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
        ((XRLabel)reportDriver.FindControl("xrlblComment", true)).Text = GetComment(sReqID);

        report.Name = "รายงานวัดน้ำ";
        report.DataSource = dt;

        reportDriver.Name = "รายงานวัดน้ำ";
        reportDriver.DataSource = dt;

        string fileName = "รายงานวัดน้ำ_FM-มว.-001_" + DateTime.Now.ToString("MMddyyyyHHmmss");
        //MemoryStream stream = new MemoryStream();
        //report.ExportToPdf(stream);

        MemoryStream stream = new MemoryStream();
        report.CreateDocument();
        reportDriver.CreateDocument();
        report.Pages.AddRange(reportDriver.Pages);
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private void ListReportNoPass(string sReqID)
    {
        string CarType = "";
        string RegisID = "";
        string CHASSIS = "";
        string NWHEALL = "";
        string TOTLE_SLOT = "";
        string TOTALCAP = "";
        string DPREV_SERV = "";
        string DLAST_SERV = "";
        string SABBREVIATION = "";
        string SCAR_NUM = "";
        string RESULT_CHECKING_DATE = "";
        string DATE_CREATED = "";

        if (dtMainData.Rows.Count > 0)
        {
            //CarType = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? "หัว" : "หาง");
            RegisID = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_NO"] + "" : dtMainData.Rows[0]["TU_NO"] + "");
            CHASSIS = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_CHASSIS"] + "" : dtMainData.Rows[0]["TU_CHASSIS"] + "");
            NWHEALL = dtMainData.Rows[0]["NWHEELS"] + "";
            TOTLE_SLOT = dtMainData.Rows[0]["TOTLE_SLOT"] + "";
            TOTALCAP = dtMainData.Rows[0]["TOTLE_CAP"] + "";
            DPREV_SERV = dtMainData.Rows[0]["DPREV_SERV"] + "";
            DLAST_SERV = dtMainData.Rows[0]["DLAST_SERV"] + "";
            SABBREVIATION = dtMainData.Rows[0]["SABBREVIATION"] + "";
            SCAR_NUM = dtMainData.Rows[0]["SCAR_NUM"] + "";
            RESULT_CHECKING_DATE = dtMainData.Rows[0]["RESULT_CHECKING_DATE"] + "";
            DATE_CREATED = dtMainData.Rows[0]["DATE_CREATED"] + "";

        }

        rpt_nopasswatercheck report = new rpt_nopasswatercheck();

        // set control
        ((XRLabel)report.FindControl("xrLabel3", true)).Text = sReqID;
        ((XRLabel)report.FindControl("xrLabel4", true)).Text = "วันที่ " + DateTime.Now.Day + " เดือน " + DateTime.Now.ToString("MMMM", new CultureInfo("th-TH")) + " พ.ศ. " + DateTime.Now.ToString("yyyy", new CultureInfo("th-TH")) + "";
        ((XRLabel)report.FindControl("xrLabel10", true)).WidthF = 364;
        ((XRLabel)report.FindControl("xrLabel10", true)).Text = SABBREVIATION;
        ((XRLabel)report.FindControl("xrLabel12", true)).Text = SCAR_NUM;
        ((XRLabel)report.FindControl("xrLabel14", true)).Text = RegisID;
        ((XRLabel)report.FindControl("xrLabel16", true)).Text = CHASSIS;
        ((XRLabel)report.FindControl("xrLabel18", true)).Text = NWHEALL;
        ((XRLabel)report.FindControl("xrLabel20", true)).Text = TOTLE_SLOT;
        ((XRLabel)report.FindControl("xrLabel22", true)).Text = TOTALCAP;
        ((XRLabel)report.FindControl("xrLabel24", true)).Text = !string.IsNullOrEmpty(DPREV_SERV) ? DateTime.Parse(DPREV_SERV).Day + " " + DateTime.Parse(DPREV_SERV).ToString("MMMM", new CultureInfo("th-TH")) + " " + DateTime.Parse(DPREV_SERV).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel26", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).Day + " " : "";
        ((XRLabel)report.FindControl("xrLabel28", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).ToString("MMM", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel30", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel33", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).Day + " " : "";
        ((XRLabel)report.FindControl("xrLabel35", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).ToString("MMM", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel37", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        // ((XRLabel)report.FindControl("xrLabel41", true)).Text = txtComment.Text;
        ((XRLabel)report.FindControl("xrLabel41", true)).Text = GetComment(sReqID);

        ((XRLabel)report.FindControl("xrLabel39", true)).Text = "FM-มว.-006 (1/1)";
        ((XRLabel)report.FindControl("xrLabel55", true)).Text = "REV.2 (" + DateTime.Now.Day + "/" + DateTime.Now.ToString("MM", new CultureInfo("th-TH")) + "/" + DateTime.Now.ToString("yy", new CultureInfo("th-TH")) + ")";

        DataTable dtName = CommonFunction.Get_Data(strConn, @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "' AND ISACTIVE_FLAG = 'Y'");
        if (dtName.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel44", true)).Text = dtName.Rows[0]["SNAME"] + "";
        }

        string SQL_NOPASS = @"SELECT CCP.REQUEST_ID,CCP.COMPART_NO 
,CASE WHEN CCP.SEAL_ERR = '1' THEN '/'  WHEN CCP.SEAL_ERR = '0' THEN 'X' ELSE '' END  as  SEAL_ERR
,CASE WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '0' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '0' THEN 'X'
ELSE '' END AS MANHOLD
,CASE WHEN INC.RESULT = '1' THEN '/' WHEN INC.RESULT = '0' THEN 'X'    ELSE '' END  as  RESULT
,CASE WHEN INC.PAN_ERR = '1' THEN '/' WHEN INC.PAN_ERR = '0' THEN 'X' ELSE '' END  as  PAN_ERR
,CASE WHEN INC.PASSSTANDARD = '1' THEN '/' WHEN INC.PASSSTANDARD = '0' THEN 'X' ELSE '' END  as  PASSSTANDARD
,CCP.MANHOLD_NO1
,CCP.MANHOLD_NO2
FROM 
(
SELECT REQ.REQUEST_ID ,REQ.SLOT_NO as COMPART_NO,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2,DS.MANHOLD_NO1,DS.MANHOLD_NO2 FROM  TBL_REQSLOT REQ
LEFT JOIN  TBL_CHECKING_COMPART ds  ON REQ.REQUEST_ID = DS.REQUEST_ID AND REQ.SLOT_NO = DS.COMPART_NO
WHERE REQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
GROUP BY  REQ.REQUEST_ID ,REQ.SLOT_NO ,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2,DS.MANHOLD_NO1,DS.MANHOLD_NO2 
)
CCP
LEFT JOIN 
(
    SELECT REQ_ID,COMPART_NO
    ,MAX(PAN_LEVEL) as PAN_LEVEL
    ,PAN_ERR
    ,PASSSTANDARD
    ,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END AS RESULT
    --,(HEIGHT1 - OLD_HEIGHT1)  AS CAL1
    --,(HEIGHT2 - OLD_HEIGHT2)  AS CAL2
    FROM TBL_INNER_CHECKINGS
    GROUP BY REQ_ID,COMPART_NO,PAN_ERR,PASSSTANDARD,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END
)INC
 ON INC.REQ_ID = CCP.REQUEST_ID AND INC.COMPART_NO = CCP.COMPART_NO
WHERE CCP.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
ORDER BY CCP.COMPART_NO ASC";

        DataTable dtNopass = CommonFunction.Get_Data(strConn, SQL_NOPASS);
        DataRow dr = null;
        for (int i = dtNopass.Rows.Count; i <= 9; i++)
        {
            dr = dtNopass.NewRow();
            dr[0] = ""; // or you could generate some random string.
            dtNopass.Rows.Add(dr);
        }
        report.Name = "แจ้งสาเหตุการตรวจสอบสภาพรถ/การตวงวัดน้าและตรวจวัดระดับแป้นที่ไม่ผ่าน";
        report.DataSource = dtNopass;
        string fileName = "รายงานวัดน้ำ_FM-มว.-006_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    protected void btnPrintT3_Click(object sender, EventArgs e)
    {
        LogUser(SMENUID, "P", "พิมพ์ใบรายงานวัดน้ำ", sREQUESTID);
        //if (rblStatusCheckingWater.SelectedIndex == 0)
        //{
        ListReport(sREQUESTID);
        //}
        //else
        //{
        //    ListReportNoPass(sREQUESTID);
        //}
    }

    private bool SendMailToVendor(string sREQID, string sVendorName, string sVendorID, string cCheck)
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "แจ้งผลการตรวจสอบสภาพรถและตรวจรับรองความถูกต้อง";

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            _to = SystemFunction.GetUserMailForSend(USER_ID, STATUSFLAG_ID, VENDOR_ID, "M");
        }

        #region html

        string CarType = "";
        string RegisID = "";
        string CHASSIS = "";
        string NWHEALL = "";
        string TOTLE_SLOT = "";
        string TOTALCAP = "";
        string DPREV_SERV = "";
        string DLAST_SERV = "";
        string SCAR_NUM = "";
        if (dtMainData.Rows.Count > 0)
        {
            CarType = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? "หัว" : "หาง");
            RegisID = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_NO"] + "" : dtMainData.Rows[0]["TU_NO"] + "");
            CHASSIS = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_CHASSIS"] + "" : dtMainData.Rows[0]["TU_CHASSIS"] + "");
            NWHEALL = dtMainData.Rows[0]["NWHEELS"] + "";
            TOTLE_SLOT = dtMainData.Rows[0]["TOTLE_SLOT"] + "";
            TOTALCAP = dtMainData.Rows[0]["TOTLE_CAP"] + "";
            DPREV_SERV = dtMainData.Rows[0]["DPREV_SERV"] + "";

            DLAST_SERV = dtMainData.Rows[0]["DLAST_SERV"] + "";
            SCAR_NUM = dtMainData.Rows[0]["SCAR_NUM"] + "";
        }

        if (cCheck == "Y")
        {
            //{0} รีเควสไอดี{1}บริษัท{2}รหัสรถ{3}ทะเบียนหัวหรือหาง{4}ทะเบียนรถ{5}เลขแชชซี{6}ชนิดรถ(เช่น 18ล้อ){7}จำนอนช่อง{8}ความจุรวม{9}เข้ามาวัดน้ำครั้งสุดท้าบ{10}ตรวจสอบวัดน้ำล่าสุด
            sHTML = string.Format(SystemFunction.Form_Email("5", USER_ID), sREQID, lblVendorName.Text, SCAR_NUM, CarType, RegisID, CHASSIS, NWHEALL, TOTLE_SLOT, TOTALCAP, DPREV_SERV, DLAST_SERV);
        }
        else
        {
            #region ตรวจภายนอกภายใน


            //            DataTable dtCheckTruck = CommonFunction.Get_Data(strConn, @"SELECT MS.CHECKLIST_ID,MS.CHECKLIST_NAME,LAW.ITEM1_VAL,LAW.ITEM2_VAL,LAW.ITEM3_VAL,MS.RD_STATUSCT,REQUEST_ID,
            //CASE 
            //WHEN LAW.ITEM1_VAL = 'N' THEN MS.CHECKLIST_NAME 
            //WHEN LAW.ITEM1_VAL <> 'N' AND  LAW.ITEM2_VAL = 'N'   THEN REPLACE(MS.CHECKLIST_NAME,'{0}',LAW.ITEM1_VAL) 
            //WHEN LAW.ITEM1_VAL <> 'N' AND LAW.ITEM2_VAL <> 'N' THEN  REPLACE((REPLACE(MS.CHECKLIST_NAME,'{0}',LAW.ITEM1_VAL)),'{1}',LAW.ITEM2_VAL )
            //ELSE ''
            //END
            //AS DETAIL FROM TBL_CHECKLIST MS
            //LEFT JOIN TBL_SCRUTINEERINGLIST LAW
            //ON  MS.CHECKLIST_ID = LAW.CHECKLIST_ID
            //WHERE LAW.ITEM1_VAL = 'N' OR LAW.ITEM2_VAL = 'N'  OR (LAW.ITEM3_VAL = 'N'  AND MS.RD_STATUSCT = 'R' ) AND  REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + "'");

            //            string HTML11 = "";
            //            if (dtCheckTruck.Rows.Count > 0)
            //            {
            //                HTML11 = "1. ตรวจสภาพภายใน/ภายนอก พบว่า<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

            //                for (int i = 0; i < dtCheckTruck.Rows.Count; i++)
            //                {
            //                    HTML11 += "-" + dtCheckTruck.Rows[i]["DETAIL"] + " ไม่ผ่าน <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            //                }

            //            }

            #endregion

            #region ตรวจสอบวัดน้ำล่าสุด
            string SQL_NOPASS = @"SELECT CCP.REQUEST_ID,CCP.COMPART_NO 
,CASE WHEN CCP.SEAL_ERR = '1' THEN '/' ELSE 'X' END  as  SEAL_ERR
,CASE WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '0' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '0' THEN 'X'
ELSE '' END AS MANHOLD
,CASE WHEN INC.RESULT = '1' THEN '/' ELSE 'X' END  as  RESULT
,CASE WHEN INC.PAN_ERR = '1' THEN '/' ELSE 'X' END  as  PAN_ERR
,CASE WHEN INC.PASSSTANDARD = '1' THEN '/' ELSE 'X' END  as  PASSSTANDARD
FROM TBL_CHECKING_COMPART  CCP
LEFT JOIN 
(
    SELECT REQ_ID,COMPART_NO
    ,MAX(PAN_LEVEL) as PAN_LEVEL
    ,PAN_ERR
    ,PASSSTANDARD
    ,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END AS RESULT
    --,(HEIGHT1 - OLD_HEIGHT1)  AS CAL1
    --,(HEIGHT2 - OLD_HEIGHT2)  AS CAL2
    FROM TBL_INNER_CHECKINGS
    GROUP BY REQ_ID,COMPART_NO,PAN_ERR,PASSSTANDARD,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END
)INC
 ON INC.REQ_ID = CCP.REQUEST_ID AND INC.COMPART_NO = CCP.COMPART_NO
WHERE CCP.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + @"'
ORDER BY CCP.COMPART_NO ASC";

            DataTable dtNopass = CommonFunction.Get_Data(strConn, SQL_NOPASS);
            string HTML12 = "";
            if (dtNopass.Rows.Count > 0)
            {
                HTML12 = "1.ตรวจสภาพลงน้ำเพื่อติดตั้งแป้นระดับ พบว่า<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                HTML12 = "<table width='100%' border='1' cellpadding='3' cellspacing='1'>";

                HTML12 += @"<tr><td width='100px' align='center'>ตรวจแป้นช่องที่</td><td width='100px' align='center'>ตีซีล แป้น</td><td width='100px' align='center'>ตีซีล ขอบฝา</td><td width='100px' align='center'>วัดระยะแผ่นเพลท</td><td width='100px' align='center'>สภาพแป้น</td><td width='100px' align='center'>เทียบแป้นอยู่ในเกณฑ์</td></tr>";
                for (int i = 0; i < dtNopass.Rows.Count; i++)
                {
                    HTML12 += @"<tr><td align='center'>" + dtNopass.Rows[i]["COMPART_NO"] + "</td><td align='center'>" + dtNopass.Rows[i]["SEAL_ERR"] + "</td><td align='center'>" + dtNopass.Rows[i]["MANHOLD"] + "</td><td align='center'>" + dtNopass.Rows[i]["RESULT"] + "</td><td align='center'>" + dtNopass.Rows[i]["PAN_ERR"] + "</td><td align='center'>" + dtNopass.Rows[i]["PASSSTANDARD"] + "</td></tr>";
                }

                HTML12 += "</table>";

            }
            #endregion

            //{0} รีเควสไอดี{1}บริษัท{2}รหัสรถ{3}ทะเบียนหัวหรือหาง{4}ทะเบียนรถ{5}เลขแชชซี{6}ชนิดรถ(เช่น 18ล้อ){7}จำนอนช่อง{8}ความจุรวม{9}เข้ามาวัดน้ำครั้งสุดท้าบ{10}ตรวจสอบวัดน้ำล่าสุด{11}ตรวจสภาพภายใน/ภายนอก{12}ตรวจสภาพลงน้ำเพื่อติดตั้งแป้นระดับ{13}ความเห็นผู้ตรวจสอบ
            sHTML = string.Format(SystemFunction.Form_Email("5", USER_ID), sREQID, lblVendorName.Text, SCAR_NUM, CarType, RegisID, CHASSIS, NWHEALL, TOTLE_SLOT, TOTALCAP, DPREV_SERV, DLAST_SERV, "", HTML12, GetComment(sREQID));
        }
        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(strConn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }

    protected void cmbUser_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cmbUser_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsEmpWorker.SelectCommand = @"SELECT SUID,SFIRSTNAME,SLASTNAME, SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME
                                        FROM
                                        (
                                        SELECT ROW_NUMBER() OVER(ORDER BY TUS.SUID) AS RN ,TUS.*  FROM TUSER TUS 
                                        WHERE TUS.CACTIVE = '1' AND CGROUP <> '0'   AND  (TUS.SFIRSTNAME LIKE :fillter OR TUS.SLASTNAME LIKE :fillter)
                                        ) 
                                        WHERE RN BETWEEN :startIndex AND :endIndex ORDER BY SFIRSTNAME";

        sdsEmpWorker.SelectParameters.Clear();
        sdsEmpWorker.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsEmpWorker.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsEmpWorker.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsEmpWorker;
        comboBox.DataBind();
    }

    void SLOTALL(string Req_ID)
    {
        DataTable dt = CommonFunction.Get_Data(strConn, "SELECT REQUEST_ID ,MAX(SLOT_NO) as SMAX FROM TBL_REQSLOT WHERE REQUEST_ID = '" + Req_ID + "' GROUP BY REQUEST_ID");

        if (dt.Rows.Count > 0)
        {
            MAXSLOT = dt.Rows[0]["SMAX"] + "" != "" ? int.Parse(dt.Rows[0]["SMAX"] + "") : 0;
        }
        else
        {
            MAXSLOT = 0;
        }

    }

    protected void btnPrintTCompare_Click(object sender, EventArgs e)
    {
        LogUser(SMENUID, "P", "พิมพ์ใบรายงานเปรียบเทียบแป้น", sREQUESTID);
        ListReportCompare(sREQUESTID);
    }

    private void ListReportCompare(string sReqID)
    {
        #region data table //


        string sql1 = @"SELECT  INC.REQ_ID
,INC.COMPART_NO
,INC.PAN_LEVEL
,TCP.NCAPACITY
,INC.OLD_HEIGHT1
,INC.HEIGHT1
,CASE WHEN INC.OLD_HEIGHT1 = 0 THEN 0 ELSE INC.HEIGHT1 - INC.OLD_HEIGHT1 END as TOTOALH1
,INC.OLD_HEIGHT2
,INC.HEIGHT2
,CASE WHEN INC.OLD_HEIGHT2 = 0 THEN 0 ELSE INC.HEIGHT2 - INC.OLD_HEIGHT2 END as TOTOALH2
,CASE WHEN  TCC.EMER_VALUE = '1' THEN '*' ELSE '' END  as EMER_VALUE
,CASE WHEN  TCC.MANHOLD_ERR1 = '1' THEN '*' ELSE '' END  as MANHOLD_ERR1
,CASE WHEN  TCC.MANHOLD_ERR2 = '1' THEN '*' ELSE '' END  as MANHOLD_ERR2
,NVL(TCC.SEAL_NO,'-') as SEAL_NO
,CASE WHEN  TCC.SEAL_ERR = '1' THEN '*' ELSE '' END  as SEAL_ERR
,CASE WHEN  INC.PAN_FLOOD = 0 THEN null ELSE INC.PAN_FLOOD END  as PAN_FLOOD
,CASE WHEN  INC.PAN_DRY = 0 THEN null ELSE INC.PAN_DRY END  as PAN_DRY
,REQ.STRUCKID
FROM 
(
SELECT RQS.REQUEST_ID as REQ_ID, RQS.SLOT_NO as COMPART_NO,RQS.LEVEL_NO as PAN_LEVEL,JJJ.OLD_HEIGHT1,JJJ.HEIGHT1,JJJ.OLD_HEIGHT2,JJJ.HEIGHT2,JJJ.PAN_FLOOD,JJJ.PAN_DRY   FROM TBL_REQSLOT   RQS
LEFT JOIN  TBL_INNER_CHECKINGS JJJ
ON RQS.REQUEST_ID  = JJJ.REQ_ID AND RQS.SLOT_NO =  JJJ.COMPART_NO AND RQS.LEVEL_NO = JJJ.PAN_LEVEL
WHERE  RQS.REQUEST_ID = '{0}'
)INC
LEFT JOIN
(
   SELECT REQUEST_ID,NVL(TU_ID,VEH_ID) AS STRUCKID  FROM  TBL_REQUEST
) 
 REQ
ON INC.REQ_ID = REQ.REQUEST_ID
LEFT JOIN TTRUCK_COMPART TCP
ON  TCP.STRUCKID = REQ.STRUCKID AND INC.COMPART_NO = TCP.NCOMPARTNO AND INC.PAN_LEVEL = TCP.NPANLEVEL
LEFT JOIN TBL_CHECKING_COMPART TCC
ON  TCC.REQUEST_ID = REQ.REQUEST_ID AND TCC.COMPART_NO = TCP.NCOMPARTNO 
WHERE  INC.REQ_ID = '{0}' AND NVL(TCP.NCAPACITY,0) <> 0
ORDER BY INC.COMPART_NO ASC, INC.PAN_LEVEL ASC
";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql1, CommonFunction.ReplaceInjection(sReqID)));
        DataRow dr = null;
        for (int i = dt.Rows.Count; i <= 15; i++)
        {
            dr = dt.NewRow();
            dr[0] = ""; // or you could generate some random string.
            dt.Rows.Add(dr);
        }

        string SETDATA = @"
SELECT MS.STRUCKID ,MS.SREGISTERNO,MS.SCARTYPEID,MS.STRANSPORTID,MS.SABBREVIATION,CMP.NCOMPARTNO,TO_CHAR(CMP.NCAPACITY,'999,999') as NCAPACITY,MS.SCARCATEGORY,MS.DPREV_SERV,MS.SCAR_NUM FROM
(
SELECT  CASE WHEN TCK.SCARTYPEID = '0' THEN TCK.STRUCKID WHEN TCK.SCARTYPEID = '3' THEN TCK.STRAILERID ELSE TCK.STRUCKID END as STRUCKID  ,
 CASE WHEN TCK.SCARTYPEID = '0' THEN TCK.SHEADREGISTERNO ELSE TCK.STRAILERREGISTERNO END as SREGISTERNO  
,TCK.SCARTYPEID,TCK.STRANSPORTID,VEN.SABBREVIATION,TTYPE.SCARCATEGORY,TRUNC(TCKK.DPREV_SERV) as DPREV_SERV
,TCKK.SCAR_NUM FROM  TTRUCK TCK
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID = TCK.STRANSPORTID
LEFT JOIN TTRUCKTYPE TTYPE
ON TTYPE.SCARTYPEID = TCK.SCARTYPEID
LEFT JOIN   TTRUCK TCKK
ON TCKK.STRUCKID = CASE WHEN TCK.SCARTYPEID = '0' THEN TCK.SHEADID ELSE TCK.STRAILERID END
)MS
LEFT JOIN 
(
    SELECT MAX(NCOMPARTNO) as NCOMPARTNO,STRUCKID,SUM(NCAPACITY) as NCAPACITY FROM
    (
        SELECT  NCOMPARTNO,STRUCKID,MAX(NCAPACITY) as NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(dt.Rows[0]["STRUCKID"] + "") + @"' GROUP BY STRUCKID,NCOMPARTNO
    )GROUP BY STRUCKID
)
CMP
ON CMP.STRUCKID = MS.STRUCKID
WHERE MS.STRUCKID = '" + CommonFunction.ReplaceInjection(dt.Rows[0]["STRUCKID"] + "") + "'";
        DataTable dtSetdata = CommonFunction.Get_Data(strConn, SETDATA);

        string SLOT = "SELECT STRUCKID, NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(dt.Rows[0]["STRUCKID"] + "") + "' ORDER BY NCOMPARTNO ASC ,NPANLEVEL ASC";

        DataTable dtslot = CommonFunction.Get_Data(strConn, SLOT);
        string SCAPACITYALL = "";
        for (int i = 0; i < dtslot.Rows.Count; i++)
        {
            SCAPACITYALL += "," + dtslot.Rows[i]["NCAPACITY"] + "";
        }

        #endregion

        rptwatercomparerange report = new rptwatercomparerange();




        // set control
        ((XRLabel)report.FindControl("xrLabel47", true)).Text = "Rev.3 (" + DateTime.Now.ToString("dd/MM/yy", new CultureInfo("th-TH")) + ")";
        ((XRLabel)report.FindControl("xrLabel48", true)).Text = "FM-มว.-007 (1/1)";

        ((XRLabel)report.FindControl("xrLabel7", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrLabel8", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrLabel9", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrLabel51", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrLabel52", true)).Text = DateTime.Now.ToString("MMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrLabel53", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));

        if (dtSetdata.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel14", true)).Text = dtSetdata.Rows[0]["SREGISTERNO"] + "";
            ((XRLabel)report.FindControl("xrLabel15", true)).Text = dtSetdata.Rows[0]["SCAR_NUM"] + "";
            ((XRLabel)report.FindControl("xrLabel19", true)).Text = !string.IsNullOrEmpty(dtSetdata.Rows[0]["DPREV_SERV"] + "") ? DateTime.Parse(dtSetdata.Rows[0]["DPREV_SERV"] + "").ToString("dd/MM/yyyy") : "";
            ((XRLabel)report.FindControl("xrLabel20", true)).Text = dtSetdata.Rows[0]["NCAPACITY"] + "";
            ((XRLabel)report.FindControl("xrLabel21", true)).Text = dtSetdata.Rows[0]["NCOMPARTNO"] + "";
            ((XRLabel)report.FindControl("xrLabel26", true)).Text = !string.IsNullOrEmpty(SCAPACITYALL) ? SCAPACITYALL.Remove(0, 1) : "";
            ((XRLabel)report.FindControl("xrLabel27", true)).Text = dtSetdata.Rows[0]["SABBREVIATION"] + "";
            ((XRLabel)report.FindControl("xrLabel28", true)).Text = dtSetdata.Rows[0]["SCARCATEGORY"] + "";
            ((XRTableCell)report.FindControl("xrTableCell34", true)).Text = dtSetdata.Rows[0]["NCAPACITY"] + "";


        }


        DataTable dtName = CommonFunction.Get_Data(strConn, @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'  AND ISACTIVE_FLAG = 'Y'");
        if (dtName.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel45", true)).Text = dtName.Rows[0]["SNAME"] + "";
        }

        report.Name = "ตารางเปรียบเทียบการวัดระยะระดับแป้นเดิมกับการวัดระยะระดับแป้นที่ตรวจสอบใหม่";
        report.DataSource = dt;
        string fileName = "FM-มว.-007(ใบวัดแห้ง)_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    void VisiblePan3(bool ISBOOL)
    {
        tb31.Visible = ISBOOL;
        tb32.Visible = ISBOOL;
        tb33.Visible = ISBOOL;
        tb34.Visible = ISBOOL;
        rblStatusP3.Visible = ISBOOL;
    }

    protected void btnReportTeabapn_Click(object sender, EventArgs e)
    {
        LogUser(SMENUID, "P", "พิมพ์ใบเทียบแป้น", sREQUESTID);
        ListReportTeabpan(sREQUESTID);
    }

    private void ListReportTeabpan(string sReqID)
    {
        #region data table //

        //        string sql1 = @"SELECT TCC.REQUEST_ID,TCC.COMPART_NO, TIC.PAN_LEVEL,NVL(TRC.NCAPACITY,REQ.CAPACITY) as NCAPACITY
        //                                 FROM TBL_CHECKING_COMPART TCC
        //                                 INNER JOIN TBL_INNER_CHECKINGS TIC ON TCC.REQUEST_ID = TIC.REQ_ID AND TCC.COMPART_NO = TIC.COMPART_NO
        //                                 LEFT JOIN TTRUCK_COMPART TRC ON  TCC.STRUCKID  = TRC.STRUCKID AND  TIC.COMPART_NO = TRC.NCOMPARTNO AND TIC.PAN_LEVEL = TRC.NPANLEVEL
        //                                 LEFT JOIN TBL_REQSLOT REQ ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO   AND TIC.PAN_LEVEL = REQ.LEVEL_NO
        //                                 WHERE REQUEST_ID = '{0}'
        //                                 ORDER BY TCC.COMPART_NO,TIC.PAN_LEVEL ASC";
        string sql1 = "SELECT REQUEST_ID,SLOT_NO as COMPART_NO,LEVEL_NO as PAN_LEVEL,CAPACITY as NCAPACITY FROM TBL_REQSLOT  WHERE REQUEST_ID = '{0}' ORDER BY SLOT_NO,LEVEL_NO ASC";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql1, CommonFunction.ReplaceInjection(sReqID)));



        //var query = dt.AsEnumerable().Select(s => new { COMPART_NO = s.Field<decimal>("COMPART_NO"), PAN_LEVEL = s.Field<decimal>("PAN_LEVEL"), NCAPACITY = s.Field<decimal>("NCAPACITY") }).ToList();
        //decimal nSum_Capacity = 0;
        //if (query.Count > 0)
        //{
        //    var query2 = query.GroupBy(g => new { g.COMPART_NO }).Select(s => new { s.Key.COMPART_NO, NCAPACITY = s.Max(x => x.NCAPACITY) }).ToList();
        //    nSum_Capacity = query2.Sum(s => s.NCAPACITY);
        //}

        //if (dt.Rows.Count > 0)
        //{

        //}
        //else
        //{
        //    DataRow dr = null;
        //    for (int i = dt.Rows.Count; i <= 18; i++)
        //    {
        //        dr = dt.NewRow();
        //        dr[0] = ""; // or you could generate some random string.
        //        dt.Rows.Add(dr);
        //    }
        //}
        #endregion

        ReportTeabpan report = new ReportTeabpan();

        // parameter
        //SENGINE หมายเลขเคื่อง

        ((XRLabel)report.FindControl("xrLabel67", true)).Text = "REV.1 (" + DateTime.Now.ToString("dd/MM/yy", new CultureInfo("th-TH")) + ")";
        ((XRLabel)report.FindControl("xrLabel68", true)).Text = "FM-มว-09 (1/1)";

        // set control
        ((XRLabel)report.FindControl("xrLabel3", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrLabel5", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrLabel7", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));


        //string[] arrNo1 = sREQUESTID.Split('-');
        //((XRLabel)report.FindControl("xrlblNo1", true)).Text = arrNo1[arrNo1.Length - 1];
        //((XRLabel)report.FindControl("xrlblNo2", true)).Text = DateTime.Now.ToString("yy", new CultureInfo("th-TH"));

        //
        string sql2 = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TRQ.VEH_ID,TRQ.TU_ID,
TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS
 FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
 LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
 WHERE TRQ.REQUEST_ID = '{0}'";

        DataTable dtPara1 = new DataTable();
        dtPara1 = CommonFunction.Get_Data(strConn, string.Format(sql2, sReqID));
        //   ป่าน 06/11/58
        //sTruckID_CheckWater เก็บ id รถที่ใช้วัดน้ำ
        //string sql3 = @"SELECT TRK.*,TRKT.SCARTYPENAME FROM TTRUCK  TRK LEFT JOIN TTRUCKTYPE TRKT ON  TRK.CARCATE_ID = TRKT.SCARTYPEID WHERE  TRK.STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";
        //        string sql3 = @"SELECT TRK.*,TRKT.SCARTYPENAME,NVL(TB.BRANDNAME,TRH.SBRAND) as SBRANDH,TRH.SBRAND as ss FROM TTRUCK  TRK 
        //LEFT JOIN TTRUCK TRH/*ข้อมูลส่วนหัว*/
        //ON  TRK.SHEADID = TRH.STRUCKID
        //LEFT JOIN TTRUCK_BRAND TB/*ข้อมูลยี่ห้อรถส่วนหัว*/
        //ON  TB.BRANDID = TRH.SBRAND
        //LEFT JOIN TTRUCKTYPE TRKT 
        //ON  TRK.SCARTYPEID = TRKT.SCARTYPEID
        //WHERE  TRK.STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";

        // Chris 16/10/2019 join ฟิล ใน ตาราง TTRUCK (SBRAND) กับ TTRUCK_BRAND(BRANDID และ BRANDNAME)
        string sql3 = @"SELECT TRK.nwheels + coalesce((SELECT t.nwheels + 0 FROM ttruck t WHERE TRK.sheadid = t.struckid AND ROWNUM <= 1),0) NWHEELS1
  ,TRK.*,TRKT.SCARTYPENAME,NVL(TB.BRANDNAME,TRH.SBRAND) as SBRANDH,TRH.SBRAND as ss FROM TTRUCK  TRK 

LEFT JOIN TTRUCK TRH/*ข้อมูลส่วนหัว*/
ON  TRK.SHEADID = TRH.STRUCKID
LEFT JOIN TTRUCK_BRAND TB/*ข้อมูลยี่ห้อรถส่วนหัว*/
ON  TB.BRANDID = TRH.SBRAND
LEFT JOIN TTRUCKTYPE TRKT 
ON  TRK.SCARTYPEID = TRKT.SCARTYPEID
INNER JOIN TTRUCK_BRAND ON TRK.SBRAND = ON  TB.BRANDID = TRK.SBRAND
.BRANDID
OR TRK.SMODEL = TB.BRANDID
OR TRK.SBRAND = TB.BRANDNAME
WHERE  TRK.STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";
        DataTable dtTruck = new DataTable();
        dtTruck = CommonFunction.Get_Data(strConn, sql3);

        //เวลา 
        //string sql4 = @"SELECT * FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NID";
        //DataTable dtTime = new DataTable();
        //dtTime = CommonFunction.Get_Data(strConn, sql4);
        //decimal nTemp = 0;

        //string vStartH = "", vStartM = "", vEndH = "", vEndM = "";

        if (dtMainData.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel15", true)).Text = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_NO"] + "" : dtMainData.Rows[0]["TU_NO"] + "");
            ((XRLabel)report.FindControl("xrLabel17", true)).Text = dtMainData.Rows[0]["SCAR_NUM"] + "";
            // ป่าน 06/11/58
            //((XRLabel)report.FindControl("xrLabel19", true)).Text = dtMainData.Rows[0]["NWHEELS"] + "";
            ((XRLabel)report.FindControl("xrLabel19", true)).Text = dtTruck.Rows[0]["NWHEELS1"] + "";
            //((XRLabel)report.FindControl("xrLabel40", true)).Text = !string.IsNullOrEmpty(dtMainData.Rows[0]["DLAST_SERV"] + "") ? DateTime.Parse(dtMainData.Rows[0]["DLAST_SERV"] + "").ToString("dd/MM/yyyy") : (dtMainData.Rows[0]["DPREV_SERV"] + "" != "" ? Convert.ToDateTime(dtMainData.Rows[0]["DPREV_SERV"] + "", new CultureInfo("th-Th")).ToString("dd/MM/yyyy") : ""); //dtMainData.Rows[0]["DLAST_SERV"] + "";
            //((XRLabel)report.FindControl("xrLabel37", true)).Text = !string.IsNullOrEmpty(dtMainData.Rows[0]["DWATEREXPIRE"] + "") ? DateTime.Parse(dtMainData.Rows[0]["DWATEREXPIRE"] + "").ToString("dd/MM/yyyy") : "";

            if (txtComment.Text.Length < 257)
            {
                int Textnum = 257 - txtComment.Text.Length;
                txtComment.Text = txtComment.Text.PadRight(Textnum, ' ');
            }
            else
            {

            }

            ((XRLabel)report.FindControl("xrLabel46", true)).Text = "";
            //((XRLabel)report.FindControl("xrLabel72", true)).Text = txtComment.Text;
            ((XRLabel)report.FindControl("xrLabel72", true)).Text = "";


            switch (dtMainData.Rows[0]["SCARTYPEID"] + "")
            {
                case "1": //((XRCheckBox)report.FindControl("xrCheckBox3", true)).Checked = true;
                    break;
                case "2": //((XRCheckBox)report.FindControl("xrCheckBox4", true)).Checked = true;
                    break;
                case "3":
                    ((XRCheckBox)report.FindControl("xrCheckBox5", true)).Checked = true;
                    ((XRLabel)report.FindControl("xrLabel25", true)).Text = dtMainData.Rows[0]["VEH_NO"] + "";
                    break;
            }

            switch (dtMainData.Rows[0]["REQTYPE_ID"] + "")
            {
                case "01":
                    ((XRCheckBox)report.FindControl("xrCheckBox2", true)).Checked = true;
                    ((XRCheckBox)report.FindControl("xrCheckBox1", true)).Checked = false;
                    break;
                case "02":
                    ((XRCheckBox)report.FindControl("xrCheckBox1", true)).Checked = true;
                    ((XRCheckBox)report.FindControl("xrCheckBox2", true)).Checked = false;
                    break;

            }

            ((XRLabel)report.FindControl("xrLabel29", true)).Text = !string.IsNullOrEmpty(dtMainData.Rows[0]["TOTLE_CAP"] + "") ? decimal.Parse(dtMainData.Rows[0]["TOTLE_CAP"] + "").ToString(SystemFunction.CheckFormatNuberic(0)) : "";
            ((XRLabel)report.FindControl("xrLabel32", true)).Text = dtMainData.Rows[0]["TOTLE_SLOT"] + "";

            //จำนวนช่อง
            string SLOT = @"SELECT STRUCKID, MAX(NCAPACITY) as NCAPACITY,NCOMPARTNO FROM TTRUCK_COMPART 
WHERE STRUCKID = '" + CommonFunction.ReplaceInjection((dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_ID"] + "" : dtMainData.Rows[0]["TU_ID"] + "")) + @"' 
GROUP BY STRUCKID,NCOMPARTNO
ORDER BY NCOMPARTNO ASC";

            DataTable dtslot = CommonFunction.Get_Data(strConn, SLOT);
            string SCAPACITYALL = "";
            for (int i = 0; i < dtslot.Rows.Count; i++)
            {
                SCAPACITYALL += "," + dtslot.Rows[i]["NCAPACITY"] + "";
            }

            ((XRLabel)report.FindControl("xrLabel39", true)).Text = !string.IsNullOrEmpty(SCAPACITYALL) ? SCAPACITYALL.Remove(0, 1) : "";

        }



        //ลมยาง
        //        string Wheal = @"SELECT REQUEST_ID,ITEM2_VAL,CHECKLIST_ID FROM TBL_SCRUTINEERINGLIST 
        //WHERE 1=1 AND  CHECKLIST_ID IN ('13','1414','16') AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";

        //        DataTable dtNitroWheal = CommonFunction.Get_Data(strConn, Wheal);

        //        if (dtNitroWheal.Rows.Count > 0)
        //        {
        //            switch (dtMainData.Rows[0]["SCARTYPEID"] + "")
        //            {
        //                case "0":
        //                    ((XRLabel)report.FindControl("xrLabel37", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '13'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    ((XRLabel)report.FindControl("xrLabel41", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '1414'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    break;
        //                case "3":
        //                    ((XRLabel)report.FindControl("xrLabel37", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '13'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    ((XRLabel)report.FindControl("xrLabel41", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '16'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    break;
        //            }
        //        }


        if (dtPara1.Rows.Count == 1 && dtTruck.Rows.Count == 1)
        {
            DataRow dr1 = null, dr2 = null, dr3 = null;

            dr1 = dtPara1.Rows[0];
            dr2 = dtTruck.Rows[0];
            dr3 = dtMainData.Rows[0];

            ((XRLabel)report.FindControl("xrLabel13", true)).Text = dr1["SABBREVIATION"] + "";
            ((XRLabel)report.FindControl("xrLabel27", true)).Text = dr2["SBRAND"] + "" != "" ? dr2["SBRAND"] + "" : "";
            //report.Parameters["sTruckID"].Value = sTruckID_CheckWater;
            //report.Parameters["sSerialNumber"].Value = dr2["SENGINE"] + "" != "" ? dr2["SENGINE"] + "" : "-";
            //report.Parameters["sTU_CHASSIS"].Value = dr1["TU_CHASSIS"] + "" != "" ? dr1["TU_CHASSIS"] + "" : "-";
            //report.Parameters["sCarType"].Value = dr2["SCARTYPENAME"] + "";
            //report.Parameters["sVEH_NO"].Value = dr1["VEH_NO"] + "" != "" ? dr1["VEH_NO"] + "" : "-";

            //report.Parameters["sVEH_CHASSIS"].Value = dr1["VEH_CHASSIS"] + "" != "" ? dr1["VEH_CHASSIS"] + "" : "-";
            //report.Parameters["nNumSlot"].Value = nSlotAll + "";
            //nTemp = decimal.TryParse(dr2["NTOTALCAPACITY"] + "", out nTemp) ? nTemp : 0;
            //report.Parameters["nSum_Capacity"].Value = dr2["NTOTALCAPACITY"] + "" != "" ? (nTemp > 0 ? nTemp.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "-";
            //report.Parameters["nWeight_Sum"].Value = dr2["NWEIGHT"] + "" != "" ? dr2["NWEIGHT"] + "" : "-";
            //report.Parameters["sNB"].Value = dr2["NLOAD_WEIGHT"] + "" != "" ? dr2["NLOAD_WEIGHT"] + "" : "-";
            //report.Parameters["dLastCheckWater"].Value = dr2["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DLAST_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";   // dr1["TU_NO"] + "";
            //report.Parameters["sHeight_H2T"].Value = dr2["NTANK_HIGH_HEAD"] + "";
            //report.Parameters["sHeight_EndT"].Value = dr2["NTANK_HIGH_TAIL"] + "";
            //report.Parameters["sTypeMaterailTank"].Value = dr2["STANK_MATERAIL"] + "" != "" ? dr2["STANK_MATERAIL"] + "" : "-";
            //report.Parameters["sAddress"].Value = dr1["SNO"] + "  " + dr1["SDISTRICT"] + "  " + dr1["SREGION"] + "  จังหวัด " + dr1["SPROVINCE"] + "  " + dr1["SPROVINCECODE"] + "";
            //report.Parameters["dService"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";


            //string sShow = "";
            //decimal nTempH = 0, nTempM = 0;
            //foreach (DataRow row in dtTime.Rows)
            //{
            //    nTempH = 0; nTempM = 0;
            //    nTempH = decimal.TryParse(row["NSTART_HOUR"] + "", out nTempH) ? nTempH : 0;
            //    nTempM = decimal.TryParse(row["NSTART_MINUTE"] + "", out nTempM) ? nTempM : 0;
            //    if (nTempH < 10)
            //    {
            //        sShow = "0" + nTempH + ":";
            //    }
            //    else
            //    {
            //        sShow = nTempH + ":";
            //    }

            //    if (nTempM < 10)
            //    {
            //        sShow = sShow + "0" + nTempM;
            //    }
            //    else
            //    {
            //        sShow = sShow + nTempM;
            //    }


            //    switch (row["NID"] + "")
            //    {
            //        case "1": //เวลานำรถเข้าตรวจ
            //            report.Parameters["sTime_dService"].Value = sShow;
            //            break;
            //        case "2"://เวลานำรถเข้าตรวจสภาพภายนอก/ใน
            //            break;
            //        case "3": //เวลาเริ่มต้นลงน้ำ
            //            report.Parameters["sTime_DownWater"].Value = sShow;
            //            vStartH = row["NSTART_HOUR"] + "";
            //            vStartM = row["NSTART_MINUTE"] + "";
            //            break;
            //        case "4": //หยุดน้ำ/วัดแป้น
            //            report.Parameters["sTime_StopCheckWater"].Value = sShow;
            //            break;
            //        case "5": //สูบน้ำคืนกลับ
            //            report.Parameters["sTime_ReturnWater"].Value = sShow;
            //            vEndH = row["NSTART_HOUR"] + "";
            //            vEndM = row["NSTART_MINUTE"] + "";
            //            break;
            //    }
            //}

            //string st1 = vStartH + ":" + vStartM;
            //string st2 = vEndH + ":" + vEndM;
            //int hours = 0, minutes = 0;

            //if (!string.IsNullOrEmpty(vStartH) && !string.IsNullOrEmpty(vStartM) && !string.IsNullOrEmpty(vEndH) && !string.IsNullOrEmpty(vEndM))
            //{
            //    TimeSpan timeStart = TimeSpan.Parse(st1);
            //    TimeSpan timeEnd = TimeSpan.Parse(st2);

            //    TimeSpan difference = timeEnd - timeStart;

            //    hours = difference.Hours;
            //    minutes = difference.Minutes;
            //}

            //report.Parameters["nSumTimeChecking"].Value = hours + "." + minutes;

            //string sDateNextCheckWater = "";
            //DateTime dNextCheckWater;
            //if (!string.IsNullOrEmpty(dr2["DNEXT_SERV"] + ""))
            //{
            //    dNextCheckWater = Convert.ToDateTime(dr2["DNEXT_SERV"] + "", new CultureInfo("th-TH"));
            //    if (dNextCheckWater.Year == DateTime.Now.Year) // ยังไม่ได้อัพเดทข้อมูลวัดน้ำใน TTRUCK
            //    {
            //        sDateNextCheckWater = dNextCheckWater.AddYears(nNextYeatCheckWater).ToString("dd MMM yyyy");
            //    }
            //    else
            //    {
            //        sDateNextCheckWater = dNextCheckWater.ToString("dd MMM yyyy");
            //    }
            //}
            //else
            //{
            //    sDateNextCheckWater = DateTime.Now.ToString("dd MMM yyyy");
            //}

            //report.Parameters["dNextCheckWater"].Value = sDateNextCheckWater;
            //report.Parameters["dCompletCheckWater"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            //report.Parameters["nSumCapacity"].Value = nSum_Capacity.ToString(SystemFunction.CheckFormatNuberic(0));

        }


        //        string sUSER = @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME,SUID.SPOSITION,TIC.EXAMDATE FROM TBL_TIME_INNER_CHECKINGS TIC
        //LEFT JOIN TUSER SUID
        //ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'  AND ISACTIVE_FLAG = 'Y'";

        string sUSER = @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME,SUID.SPOSITION,TIC.EXAMDATE,TIC.NVERSION FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID 
WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + @"'  AND ISACTIVE_FLAG = 'Y'
GROUP BY TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME,SUID.SPOSITION,TIC.EXAMDATE,TIC.NVERSION
ORDER BY TIC.NVERSION DESC ";

        DataTable dtUSER = CommonFunction.Get_Data(strConn, sUSER);
        if (dtUSER.Rows.Count > 0)
        {
            DateTime DExam = !string.IsNullOrEmpty(dtUSER.Rows[0]["EXAMDATE"] + "") ? DateTime.Parse(dtUSER.Rows[0]["EXAMDATE"] + "") : DateTime.Now;

            ((XRLabel)report.FindControl("xrLabel52", true)).Text = dtUSER.Rows[0]["SNAME"] + "";
            ((XRLabel)report.FindControl("xrLabel42", true)).Text = dtUSER.Rows[0]["NVERSION"] + "";
            if (!string.IsNullOrEmpty(dtUSER.Rows[0]["EXAMDATE"] + ""))
            {
                // ((XRLabel)report.FindControl("xrLabel64", true)).Text = DExam.Day + " " + DExam.ToString("MMM", new CultureInfo("th-TH")) + " " + DExam.ToString("yyyy", new CultureInfo("th-TH"));
                ((XRLabel)report.FindControl("xrLabel64", true)).Text = DExam.Day + "";
                ((XRLabel)report.FindControl("xrLabel69", true)).Text = DExam.ToString("MMM", new CultureInfo("th-TH"));
                ((XRLabel)report.FindControl("xrLabel71", true)).Text = DExam.ToString("yyyy", new CultureInfo("th-TH"));

                ((XRLabel)report.FindControl("xrLabel50", true)).Text = DExam.Day + "";
                ((XRLabel)report.FindControl("xrLabel74", true)).Text = DExam.ToString("MMM", new CultureInfo("th-TH"));
                ((XRLabel)report.FindControl("xrLabel75", true)).Text = DExam.ToString("yyyy", new CultureInfo("th-TH"));

                ((XRLabel)report.FindControl("xrLabel22", true)).Text = DExam.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            }
            else
            {
                ((XRLabel)report.FindControl("xrLabel22", true)).Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                //((XRLabel)report.FindControl("xrLabel22", true)).Text = " ";
            }
            // ((XRLabel)report.FindControl("xrblbPositionControl", true)).Text = dtUSER.Rows[0]["SPOSITION"] + "";
        }


        var s_basePath = HttpContext.Current.Server.MapPath("./") + "Images/pttlogoreport.jpg".Replace("/", "\\");

        if (File.Exists(s_basePath))
        {
            Bitmap bimage = new Bitmap(s_basePath);
            ((XRPictureBox)report.FindControl("xrPictureBox1", false)).Image = bimage;
            ((XRPictureBox)report.FindControl("xrPictureBox1", true)).Visible = true;

        }



        //((XRLabel)report.FindControl("xrlblsDay2", true)).Text = DateTime.Now.Day + "";
        //((XRLabel)report.FindControl("xrlblsMonth2", true)).Text = DateTime.Now.ToString("MMM", new CultureInfo("th-TH"));
        //((XRLabel)report.FindControl("xrlblsYear2", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
        //((XRLabel)report.FindControl("xrlblComment", true)).Text = GetComment(sReqID);


        report.Name = "รายงานวัดน้ำ";
        report.DataSource = dt;
        string fileName = "รายงานวัดน้ำ_FM-มว.-001_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    protected void btnNopass_Click(object sender, EventArgs e)
    {
        LogUser(SMENUID, "P", "พิมพ์ใบรายงานไม่ผ่านวัดน้ำ", sREQUESTID);
        ListReportNoPass(sREQUESTID);
    }

    private void CheckNoPass(string sReqID)
    {
        string SQL_NOPASS = @"SELECT CCP.REQUEST_ID,CCP.COMPART_NO 
,CASE WHEN CCP.SEAL_ERR = '1' THEN '/'  WHEN CCP.SEAL_ERR = '0' THEN 'X' ELSE '' END  as  SEAL_ERR
,CASE WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '0' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '0' THEN 'X'
ELSE '' END AS MANHOLD
,CASE WHEN INC.RESULT = '1' THEN '/' WHEN INC.RESULT = '0' THEN 'X'    ELSE '' END  as  RESULT
,CASE WHEN INC.PAN_ERR = '1' THEN '/' WHEN INC.PAN_ERR = '0' THEN 'X' ELSE '' END  as  PAN_ERR
,CASE WHEN INC.PASSSTANDARD = '1' THEN '/' WHEN INC.PASSSTANDARD = '0' THEN 'X' ELSE '' END  as  PASSSTANDARD
,CCP.MANHOLD_NO1
,CCP.MANHOLD_NO2
FROM 
(
SELECT REQ.REQUEST_ID ,REQ.SLOT_NO as COMPART_NO,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2,DS.MANHOLD_NO1,DS.MANHOLD_NO2 FROM  TBL_REQSLOT REQ
LEFT JOIN  TBL_CHECKING_COMPART ds  ON REQ.REQUEST_ID = DS.REQUEST_ID AND REQ.SLOT_NO = DS.COMPART_NO
WHERE REQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
GROUP BY  REQ.REQUEST_ID ,REQ.SLOT_NO ,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2,DS.MANHOLD_NO1,DS.MANHOLD_NO2 
)
CCP
LEFT JOIN 
(
    SELECT REQ_ID,COMPART_NO
    ,MAX(PAN_LEVEL) as PAN_LEVEL
    ,PAN_ERR
    ,PASSSTANDARD
    ,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END AS RESULT
    --,(HEIGHT1 - OLD_HEIGHT1)  AS CAL1
    --,(HEIGHT2 - OLD_HEIGHT2)  AS CAL2
    FROM TBL_INNER_CHECKINGS
    GROUP BY REQ_ID,COMPART_NO,PAN_ERR,PASSSTANDARD,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END
)INC
 ON INC.REQ_ID = CCP.REQUEST_ID AND INC.COMPART_NO = CCP.COMPART_NO
WHERE CCP.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'  AND CASE WHEN INC.PASSSTANDARD = '1' THEN '/' WHEN INC.PASSSTANDARD = '0' THEN 'X' ELSE '' END = 'X'
ORDER BY CCP.COMPART_NO ASC";

        DataTable dtNopass = CommonFunction.Get_Data(strConn, SQL_NOPASS);

        if (dtNopass.Rows.Count > 0)
        {
            btnNopass.ClientVisible = true;
        }
        else
        {
            btnNopass.ClientVisible = false;
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, strConn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    public string sql { get; set; }

    public string sql_update { get; set; }


    protected void txtDOCNO_ValueChanged(object sender, EventArgs e)
    {
        try
        {
            string sqldoc = @"SELECT COUNT(*) as DOC_COUNT FROM TBL_REQUEST WHERE DOCNO = '" + txtDOCNO.Text.Trim() + "'";
            DataTable doc = CommonFunction.Get_Data(strConn, sqldoc);
            if (doc.Rows[0]["DOC_COUNT"].ToString().Trim() != "0")
            {
                txtDOCNO.Text = hidDOCNO.Value;
                throw new Exception("เลขสัญญานี้ถูกใช้ไปแล้ว");
            }
            else
            {
                hidDOCNO.Value = txtDOCNO.Text.Trim();
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.Message + "');</script>");
        }
    }
}

