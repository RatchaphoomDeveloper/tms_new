﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="final-grade_old.aspx.cs" Inherits="final_grade" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2.Export, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2.Export" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <div style="display: none;">
                    <asp:TextBox ID="txtratio_grade_lab" runat="server" Style="display: none;"></asp:TextBox><asp:TextBox ID="txtratio_grade_lab_Diff" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:TextBox ID="txtratio_grade_home" runat="server" Style="display: none;"></asp:TextBox><asp:TextBox ID="txtratio_grade_home_Diff" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:TextBox ID="txtratio_grade_KPI" runat="server" Style="display: none;"></asp:TextBox><asp:TextBox ID="txtratio_grade_KPI_Diff" runat="server" Style="display: none;"></asp:TextBox>
                </div>
                <table style=" margin-bottom:5px; ">
                    <tr>
                        <td style="padding-left:5px;">ผู้ขนส่ง: </td>
                        <td>
                            <dx:ASPxComboBox ID="cmbVendor" runat="server" TextField="SABBREVIATION" ValueField="SVENDORID">
                            </dx:ASPxComboBox>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" NullText="เลขที่สัญญา,ชื่อผู้ประกอบการขนส่ง"
                                Width="200px" Visible="false">
                            </dx:ASPxTextBox>
                        </td>
                        <td style="padding-left:5px;">ปี</td>
                        <td>
                            <dx:ASPxComboBox ID="cmbYear" runat="server" Width="100px" TextField="SYEAR" ValueField="SYEAR">
                            </dx:ASPxComboBox>
                        </td>
                        <td style="padding-left:5px;">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td style="padding-left:5px;">
                            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Export PDF">
                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td style="padding-left:5px;">
                            <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Export Excel">
                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>

                <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                    DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw"
                    Style="margin-top: 0px" Width="100%"
                    OnAfterPerformCallback="gvw_AfterPerformCallback">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                            Width="5%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SCONTRACTID" ShowInCustomizationForm="True"
                            VisibleIndex="1" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ " FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                            VisibleIndex="2" Width="20%">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                            VisibleIndex="3" Width="18%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewBandColumn Caption="ผลผระเมินการทำงานประจำไตรมาส" ShowInCustomizationForm="True"
                            VisibleIndex="4">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="ไตรมาสที่ 1" FieldName="T1" ShowInCustomizationForm="True"
                                    VisibleIndex="5" Width="8%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ไตรมาสที่ 2" FieldName="T2" ShowInCustomizationForm="True"
                                    VisibleIndex="6" Width="8%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ไตรมาสที่ 3" FieldName="T3" ShowInCustomizationForm="True"
                                    VisibleIndex="7" Width="8%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ไตรมาสที่ 4" FieldName="T4" ShowInCustomizationForm="True"
                                    VisibleIndex="7" Width="8%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewDataTextColumn Caption="ลประเมินการ<br/>บริหารงาน" FieldName="NVALUE"
                            VisibleIndex="11" Width="10%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataColumn Width="5%" CellStyle-Cursor="hand" VisibleIndex="12" Caption="ผลประเมิน KPI">
                            <DataItemTemplate>
                                <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" AutoPostBack="false"
                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                    <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                    <Image Width="16px" Height="16px" Url="Images/search.png">
                                    </Image>
                                </dx:ASPxButton>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <Templates>
                        <EditForm>
                            <table width="100%">
                                <tr align="left">
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                                            <tr>
                                                <td style="padding-right: 4px">
                                                    <div style="float: left">
                                                        <dx:ASPxButton ID="btnPdfExport" ToolTip="Export to PDF" runat="server" UseSubmitBehavior="False"
                                                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                            OnClick="btnPdfExport_Click">
                                                            <Image Width="16px" Height="16px" Url="Images/ic_pdf2.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div style="float: left">PDF</div>
                                                </td>
                                                <td style="padding-right: 4px">
                                                    <div style="float: left">
                                                        <dx:ASPxButton ID="btnXlsExport" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                                                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                            OnClick="btnXlsExport_Click">
                                                            <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div style="float: left">Excel</div>
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblGrade" runat="server"></dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="sgvw"
                                            DataSourceID="sds1" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px"
                                            Width="100%" SettingsBehavior-AllowSort="false" OnInit="sgvw_Init">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption='คะแนนปฏิบัติงาน' FieldName="SUMPOINT" VisibleIndex="1"
                                                    Width="24%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption='คะแนนบริหารงาน' FieldName="QUESTIONNAIRESCORE"
                                                    VisibleIndex="2" Width="24%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption='คะแนน KPI' FieldName="SUMKPI"
                                                    VisibleIndex="2" Width="24%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="คะแนนรวม" FieldName="NGRADE" VisibleIndex="3"
                                                    Width="27%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Grade" FieldName="SGRADENAME" VisibleIndex="4"
                                                    Width="25%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxGridView ID="sgvw11" runat="server" AutoGenerateColumns="False" ClientInstanceName="sgvw11"
                                            DataSourceID="sds11" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px"
                                            Width="100%" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SettingsBehavior-AllowSort="false">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                                    Width="1%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="เรื่องที่ตัดคะแนน" FieldName="SPROCESSNAME" ShowInCustomizationForm="True"
                                                    VisibleIndex="1" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn Caption="วันที่ตัดคะแนน" FieldName="DREDUCE" VisibleIndex="2"
                                                    Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                    VisibleIndex="3" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                    VisibleIndex="4" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ชื่อพนักงานขับ" FieldName="SEMPLOYEENAME" VisibleIndex="5"
                                                    Width="10%">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ผู้ตัดคะแนน" FieldName="SREDUCEBY" ShowInCustomizationForm="True"
                                                    VisibleIndex="6" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="รายละเอียด" FieldName="STOPICNAME" ShowInCustomizationForm="True"
                                                    VisibleIndex="7" Width="18%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="สถานะการอุทธรณ์" FieldName="STATUS" ShowInCustomizationForm="True"
                                                    VisibleIndex="8" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="คะแนนที่ตัด" FieldName="SUMNPOINT" ShowInCustomizationForm="True"
                                                    VisibleIndex="9" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <Settings ShowFooter="True" />
                                            <TotalSummary>
                                                <dx:ASPxSummaryItem FieldName="SUMNPOINT" ShowInColumn="รายละเอียด" SummaryType="Sum"
                                                    DisplayFormat="รวมตัดคะแนน {0}" />
                                            </TotalSummary>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                            SkinID="_close">
                                            <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </EditForm>
                    </Templates>
                    <SettingsPager AlwaysShowPager="True">
                    </SettingsPager>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY oo.SCONTRACTID) AS ID1,oo.SCONTRACTID,oo.SCONTRACTNO,oo.SVENDORNAME,oo.SVENDORID ,oo.T1,oo.T2, oo.T3, oo.T4,nvl(g.NVALUE,0) AS NVALUE
 FROM (
SELECT ct.CACTIVE, ct.SCONTRACTID,CT.SCONTRACTNO,VS.SVENDORNAME,ct.SVENDORID ,SUM(nvl(o.T1,0)) AS T1,SUM(nvl(o.T2,0)) AS T2,SUM(nvl(o.T3,0)) AS T3, SUM(nvl(o.T4,0)) AS T4
  FROM TCONTRACT ct LEFT JOIN 
 
 (SELECT CASE WHEN R.SPROCESSID = '020' THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
 ,R.DREDUCE , CASE WHEN To_Char(R.DREDUCE,'MM') IN  ('01','02','03') THEN R.NPOINT ELSE 0 END AS T1,
 CASE WHEN To_Char(R.DREDUCE,'MM') IN  ('04','05','06') THEN R.NPOINT ELSE 0 END AS T2,
  CASE WHEN To_Char(R.DREDUCE,'MM') IN  ('07','08','09') THEN R.NPOINT ELSE 0 END AS T3,
  CASE WHEN To_Char(R.DREDUCE,'MM') IN  ('10','11','12') THEN R.NPOINT ELSE 0 END AS T4

FROM (TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO)
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1)
LEFT JOIN TCONTRACT ON ct2.SCONTRACTID = TCONTRACT.SCONTRACTID
WHERE To_Char(R.DREDUCE ,'YYYY') = :Year)
 o ON o.SCONTRACTID = CT.SCONTRACTID LEFT JOIN TVENDOR_SAP vs ON Ct.SVENDORID = VS.SVENDORID
 WHERE nvl(CT.SCONTRACTNO,'') LIKE '%' || :oSearch || '%' OR nvl(VS.SVENDORNAME,'') LIKE '%' || :oSearch || '%'

 GROUP BY ct.CACTIVE,ct.SCONTRACTID,CT.SCONTRACTNO,VS.SVENDORNAME,ct.SVENDORID
 ) oo LEFT JOIN (SELECT sq.SVENDORID, AVG(nvl(sq.NVALUE,0)) AS NVALUE FROM TQUESTIONNAIRE sq WHERE To_Char(sq.DCHECK,'YYYY') = :Year GROUP BY sq.SVENDORID) g ON 
 oo.SVENDORID = g.SVENDORID where oo.CACTIVE ='Y'
  ">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtSearch" Name="oSearch" PropertyName="Text" />
                        <asp:ControlParameter ControlID="cmbYear" Name="Year" PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sds1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    EnableCaching="True" CacheKeyDependency="ckdUser1" SelectCommand="SELECT  ROW_NUMBER () OVER (ORDER BY t.SCONTRACTID) AS ID1,t.SUMPOINT,t.QUESTIONNAIRESCORE,t.NGRADE,g.SGRADENAME,t.SCONTRACTID,t.SUMKPI 
 FROM 
 (
    SELECT cc.SCONTRACTID
    , ROUND(SUM(nvl(RP.SUMPOINT,:ratio_grade_lab)),0) AS SUMPOINT 
    , ROUND(SUM(nvl(QN.QUESTIONNAIRESCORE,0)),0) AS QUESTIONNAIRESCORE
    ,ROUND(SUM(nvl(RP.SUMPOINT,:ratio_grade_lab)),0) + ROUND(SUM(nvl(QN.QUESTIONNAIRESCORE,0)),0) AS NGRADE 
    ,MAX(nvl(KP.SUMKPI,0)) as SUMKPI
    FROM TCONTRACT cc 
    LEFT JOIN 
     (
            SELECT TBL_LAB.SCONTRACTID,TBL_LAB.SCONTRACTNO
            ,((CASE WHEN NVL(TBL_LAB.SUM_M0103,0) &gt;=50 THEN TBL_LAB.SUM_M0103 ELSE 0 END 
            +CASE WHEN NVL(TBL_LAB.SUM_M0406,0) &gt;=50 THEN TBL_LAB.SUM_M0406 ELSE 0 END 
            +CASE WHEN NVL(TBL_LAB.SUM_M0709,0) &gt;=50 THEN TBL_LAB.SUM_M0709 ELSE 0 END  
            +CASE WHEN NVL(TBL_LAB.SUM_M1012,0) &gt;=50 THEN TBL_LAB.SUM_M1012 ELSE 0 END)/4)* :ratio_grade_lab_diff SUMPOINT 
            FROM
            (
                SELECT TBL.SCONTRACTID,TBL.SCONTRACTNO
                , TBL.SUM_M01, TBL.SUM_M02, TBL.SUM_M03 
                ,ROUND ((((100-TBL.SUM_M01)+ (100-TBL.SUM_M02)+ (100-TBL.SUM_M03))/3) )  SUM_M0103 
                , TBL.SUM_M04,TBL.SUM_M05, TBL.SUM_M06 
                ,ROUND ((((100-TBL.SUM_M04)+ (100-TBL.SUM_M05)+ (100-TBL.SUM_M06))/3) )  SUM_M0406
                , TBL.SUM_M07, TBL.SUM_M08, TBL.SUM_M09
                ,ROUND ((((100-TBL.SUM_M07)+ (100-TBL.SUM_M08)+ (100-TBL.SUM_M09))/3) )  SUM_M0709
                ,TBL.SUM_M10, TBL.SUM_M11, TBL.SUM_M12 
                ,ROUND ((((100-TBL.SUM_M10)+ (100-TBL.SUM_M11)+ (100-TBL.SUM_M12))/3) )  SUM_M1012
                FROM
                (
                        SELECT  C.SCONTRACTID ,c.SCONTRACTNO--,(((1200 - (SUM(nvl(R.NPOINT,0)))) / 1200) * 100) * 0.8 AS SUMPOINT 
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='01' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M01
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='02' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M02
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='03' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M03
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='04' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M04
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='05' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M05
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='06' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M06
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='07' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M07
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='08' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M08
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='09' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M09
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='10' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M10
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='11' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M11
                        , SUM(CASE WHEN TO_CHAR(R.DREDUCE ,'MM')='12' THEN NVL(R.NPOINT,0.000)  ELSE 0 END) SUM_M12
                        FROM TREDUCEPOINT r
                         LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO
                        LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID =  CT.STRUCKID
                        LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  
                        WHERE To_Char(R.DREDUCE,'YYYY') = :YEAR1  
                        GROUP BY C.SCONTRACTID ,c.SCONTRACTNO
              ) TBL  
          )TBL_LAB 
     )  
    RP ON cc.SCONTRACTID = RP.SCONTRACTID
    LEFT JOIN 
    (
        SELECT q.SVENDORID, ((AVG(Q.NVALUE)  /  (SELECT SUM(MAXWEIGHT) AS MAXWEIGHT FROM  ( SELECT nvl(NWEIGHT,0) * (SELECT MAX(NTYPEVISITLISTSCORE) AS MAXSCORE FROM TTYPEOFVISITFORMLIST)   AS MAXWEIGHT 
        FROM TVISITFORM  WHERE CACTIVE = '1' ))) * 100) * :ratio_grade_home_diff  AS QUESTIONNAIRESCORE
        FROM TQUESTIONNAIRE q 
        WHERE To_Char(q.DCHECK,'YYYY') = :YEAR1 
        GROUP BY q.SVENDORID
    )
     QN  ON QN.SVENDORID = cc.SVENDORID
    LEFT JOIN
    (
        SELECT ROUND(SUM(nvl(KPR.SUMKPI_WEIGHT,0) * :ratio_grade_KPI_Diff),0)   as SUMKPI,KPR.SCONTRACTID,KPR.SVENDORID FROM TBL_KPI_RATE  KPR
        WHERE  KPR.SYEAR = :YEAR1
        GROUP BY KPR.SCONTRACTID,KPR.SVENDORID
    )
    KP ON KP.SVENDORID = cc.SVENDORID AND   KP.SCONTRACTID = cc.SCONTRACTID
    GROUP BY cc.SCONTRACTID
) t LEFT JOIN TGRADE g ON t.NGRADE BETWEEN g.NSTARTPOINT AND g.NENDPOINT
WHERE t.SCONTRACTID = :CONTRACTID">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="fCONTRACTID" Name="CONTRACTID" />
                        <asp:ControlParameter ControlID="cmbYear" Name="YEAR1" PropertyName="Value" />
                        <asp:ControlParameter ControlID="txtratio_grade_lab" Name="ratio_grade_lab" PropertyName="Text" />
                        <asp:ControlParameter ControlID="txtratio_grade_lab_Diff" Name="ratio_grade_lab_diff" PropertyName="Text" />
                        <asp:ControlParameter ControlID="txtratio_grade_home_Diff" Name="ratio_grade_home_diff" PropertyName="Text" />
                        <asp:ControlParameter ControlID="txtratio_grade_KPI_Diff" Name="ratio_grade_KPI_Diff" PropertyName="Text" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sds11" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    EnableCaching="True" CacheKeyDependency="ckdUser11" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY o.NREDUCEID DESC) AS ID1, o.NREDUCEID,o.STOPICNAME  , o.DREDUCE, o.SREDUCEBY,o.SPROCESSNAME, o.SEMPLOYEENAME,o.SREDUCENAME, o.SUMNPOINT, o.STATUS,o.SHEADREGISTERNO,o.STRAILERREGISTERNO  FROM 
(
SELECT R.NREDUCEID,CASE WHEN R.SPROCESSID='090' THEN R.SREDUCENAME ELSE Tp.STOPICNAME END STOPICNAME  , PC.SPROCESSNAME, r.DREDUCE,R.SREDUCEBY || ' ' || U.SFIRSTNAME || ' ' || U.SLASTNAME AS SREDUCEBY  ,ES.FNAME || ' ' || ES.LNAME AS SEMPLOYEENAME , R.SREDUCENAME,R.SHEADREGISTERNO,R.STRAILERREGISTERNO , - nvl(r.NPOINT,0) AS SUMNPOINT,

CASE WHEN ap.SAPPEALID IS NOT NULL AND ap.CSTATUS = '0' THEN 'ยื่นอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '1' or ap.CSTATUS = '2' or ap.CSTATUS = '4' or ap.CSTATUS = '5' or ap.CSTATUS = '6' THEN 'กำลังดำเนินการ' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END  As STATUS , 

CASE WHEN R.SPROCESSID = '020' THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
FROM ((((((TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO )
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1))LEFT JOIN (SELECT STOPICID,STOPICNAME,SVERSION FROM TTOPIC) tp ON R.STOPICID = Tp.STOPICID AND nvl(R.SVERSIONLIST,'1') = TP.SVERSION ) LEFT JOIN TAPPEAL ap on R.NREDUCEID = ap.NREDUCEID)
LEFT JOIN TEMPLOYEE_SAP es ON R.SDRIVERNO = ES.SEMPLOYEEID ) LEFT JOIN TUSER u ON R.SREDUCEBY = U.SUID) LEFT JOIN TPROCESS pc ON R.SPROCESSID = PC.SPROCESSID
WHERE nvl(r.CACTIVE,'0') = '1' AND To_Char(r.DREDUCE,'YYYY') = :YEAR1 ) o      WHERE o.SCONTRACTID = :CONTRACTID">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="fCONTRACTID" Name="CONTRACTID" />
                        <asp:ControlParameter ControlID="cmbYear" Name="YEAR1" PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="sgvw11" PageHeader-Font-Names="Tahoma"
                    PageHeader-Font-Bold="true">
                    <Styles>
                        <Header Font-Names="Tahoma" Font-Size="10">
                        </Header>
                        <Cell Font-Names="Tahoma" Font-Size="8">
                        </Cell>
                    </Styles>
                    <PageHeader>
                        <Font Bold="True" Names="Tahoma"></Font>
                    </PageHeader>
                </dx:ASPxGridViewExporter>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
