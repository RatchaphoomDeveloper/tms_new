﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Text;

public partial class questionnaire_detail : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/questionnaire/{0}/{2}/{1}/";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {


            if (Session["oNQUESTIONID"] != null)
            {
                ListData();
            }

           
        }
    }

    void ClearControl()
    {



    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {


        }

    }



    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void detailGrid_DataSelect(object sender, EventArgs e)
    {
        Session["ONGROUPID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }


    protected void detailGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "#")
        {
            string keyValue = ((sender) as ASPxGridView).GetMasterRowKeyValue() + "";
            int VisibleIndex = e.VisibleIndex;
            ASPxRadioButtonList rblWEIGHT = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "rblWEIGHT") as ASPxRadioButtonList;
            ASPxTextBox txtWEIGHT = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtWEIGHT") as ASPxTextBox;
            ASPxTextBox txtOldSum = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtOldSum") as ASPxTextBox;

            rblWEIGHT.ClientInstanceName = rblWEIGHT.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;
            txtWEIGHT.ClientInstanceName = txtWEIGHT.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;
            txtOldSum.ClientInstanceName = txtOldSum.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;

            rblWEIGHT.ClientSideEvents.SelectedIndexChanged = "function (s,e) {var sum = (" + rblWEIGHT.ClientInstanceName + ".GetValue() * " + txtWEIGHT.ClientInstanceName + ".GetValue()) ;txtTotal.SetValue((txtTotal.GetValue() - " + txtOldSum.ClientInstanceName + ".GetValue()) + sum); " + txtOldSum.ClientInstanceName + ".SetValue(sum)}";
        }
    }

    void ListData()
    {


        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();

            DataTable dt = CommonFunction.Get_Data(con, "SELECT q.NQUESTIONNAIREID, q.SVENDORID, q.DCHECK, q.SADDRESS, q.SREMARK, q.NVALUE,q.NNO,VS.SVENDORNAME FROM TQUESTIONNAIRE q LEFT JOIN TVENDOR_SAP vs ON Q.SVENDORID = VS.SVENDORID WHERE q.NQUESTIONNAIREID = " + Session["oNQUESTIONID"]);

            if (dt.Rows.Count > 0)
            {

                DataTable dt1 = CommonFunction.Get_Data(con, "SELECT NID, SNAME FROM TQUESTIONNAIRE_BY WHERE NQUESTIONNAIREID = " + dt.Rows[0]["NQUESTIONNAIREID"]);
                DataTable dt4 = CommonFunction.Get_Data(con, "SELECT NVISITFORMID, NGROUPID, NTYPEVISITFORMID, NVALUE, SREMARK,NVALUEITEM FROM TQUESTIONNAIRELIST WHERE NQUESTIONNAIREID = " + dt.Rows[0]["NQUESTIONNAIREID"]);

                DateTime date;
                lblNNO.Text = "ครั้งที่ " + dt.Rows[0]["NNO"];
                lblVendor.Text = dt.Rows[0]["SVENDORNAME"] + "";
                lblDate.Text = (DateTime.TryParse(dt.Rows[0]["DCHECK"] + "", out date) ? date : DateTime.Now).ToShortDateString();
                lblAddress.Value = dt.Rows[0]["SADDRESS"] + "";
                ltrDetail.Text = dt.Rows[0]["SREMARK"] + "";
                txtTotal.Text = dt.Rows[0]["NVALUE"] + "";

                if (dt1.Rows.Count > 0)
                {

                        StringBuilder sb = new StringBuilder();

                        sb.Append("<table width='100%' border='0' cellspacing='2' cellpadding='5'>");

                        string formatPopup = @" <tr><td align='left'> {0}. คุณ {1}</td></tr>";
                        int i = 1;
                        foreach (DataRow dr in dt1.Rows)
                        {

                            sb.Append(string.Format(formatPopup, i, dr["SNAME"] + ""));
                            i++;
                        }
                        sb.Append("</table>");

                       ltrChackName.Text = sb.ToString(); 
                    }



                if (dt4.Rows.Count > 0)
                {
                    Session["sNTYPEVISITFORMID"] = dt4.Rows[0]["NTYPEVISITFORMID"] + "";
                    sdsMaster.DataBind();
                    grid.DataBind();
                    grid.DetailRows.ExpandAllRows();

                    for (int i = 0; i < grid.VisibleRowCount; i++)
                    {
                        string NGROUPID = grid.GetRowValues(i, "NGROUPID") + "";
                        DataRow[] dr = dt4.Select("NGROUPID =" + NGROUPID);
                        if (dr.Count() > 0)
                        {
                            ASPxGridView detailGrid = (ASPxGridView)grid.FindDetailRowTemplateControl(i, "detailGrid");
                            for (int o = 0; o < detailGrid.VisibleRowCount; o++)
                            {

                                string NVISITFORMID = detailGrid.GetRowValues(o, "NVISITFORMID") + "";
                                DataRow[] drr = dt4.Select("NGROUPID =" + NGROUPID + "and NVISITFORMID=" + NVISITFORMID);

                                if (drr.Count() > 0) 
                                {
                                    // ทำไมถึง Find แล้วไม่เจอ
                                    ASPxRadioButtonList rblWEIGHT = detailGrid.FindRowCellTemplateControl(o, null, "rblWEIGHT") as ASPxRadioButtonList;
                                    ASPxTextBox txtOldSum = detailGrid.FindRowCellTemplateControl(o, null, "txtOldSum") as ASPxTextBox;
                                    ASPxTextBox txtRemark = detailGrid.FindRowCellTemplateControl(o, null, "txtRemark") as ASPxTextBox;

                                    if (rblWEIGHT != null) rblWEIGHT.Value = drr[0]["NVALUEITEM"] + "";
                                    if (txtOldSum != null) txtOldSum.Text = drr[0]["NVALUE"] + "";
                                    if (txtRemark != null) txtRemark.Text = drr[0]["SREMARK"] + "";
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}