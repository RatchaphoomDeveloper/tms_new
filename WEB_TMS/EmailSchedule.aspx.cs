﻿using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using TMS_BLL.Master;
using System.Web.Security;


public partial class EmailSchedule : PageBase
{
    #region + View State +
    private DataTable dtSchedule
    {
        get
        {
            if ((DataTable)ViewState["dtSchedule"] != null)
                return (DataTable)ViewState["dtSchedule"];
            else
                return null;
        }
        set
        {
            ViewState["dtSchedule"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadEmailType();
            this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadEmailType()
    {
        try
        {
            DataTable dtEmailType = EmailTemplateBLL.Instance.EmailTypeSelectBLL(" AND M_EMAIL_TYPE.ISACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlEmailType, dtEmailType, "EMAIL_TYPE_ID", "EMAIL_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SearchData(string Condition)
    {
        try
        {
            dtSchedule = EmailTemplateBLL.Instance.EmailScheduleSelectAllBLL(Condition);
            GridViewHelper.BindGridView(ref dgvTemplate, dtSchedule);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmailType.SelectedIndex < 1)
                this.SearchData(" AND TEMPLATE_NAME LIKE '%" + txtSearch.Text.Trim() + "%'");
            else
                this.SearchData(" AND M_EMAIL_TEMPLATE.EMAIL_TYPE_ID = " + ddlEmailType.SelectedValue + " AND TEMPLATE_NAME LIKE '%" + txtSearch.Text.Trim() + "%'");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTemplate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvTemplate.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvTemplate, dtSchedule);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTemplate_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvTemplate.DataKeys[e.RowIndex]["SCHEDULE_ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            MsgAlert.OpenForm("EmailScheduleAdd.aspx?SCHEDULE_ID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmailScheduleAdd.aspx");
    }
}