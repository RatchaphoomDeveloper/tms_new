﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_suppliant_lst_New.aspx.cs" Inherits="admin_suppliant_lst_New" StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="server">
    <div class="container" style="width: 100%;">
        <div class="form-horizontal" style="background-color: white">
            <h2 class="page-header" style="margin-top: 10px; font-family: 'Angsana New'; color: #009120">
                <asp:Label Text="" ID="lblHeader" runat="server" />
            </h2>
            <asp:UpdatePanel ID="udpMain" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ค้นหา&#8711;</a>
                            <input type="hidden" id="hiddencollapseFindContract" value=" " />
                        </div>
                        <div class="panel-collapse collapse in" id="collapseFindContract">
                            <div class="panel-body">
                                <asp:HiddenField runat="server" ID="hideAppealID" />
                                <asp:HiddenField ID="hideReduceID" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hideSentence" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hideCheckFinal" runat="server"></asp:HiddenField>
                                <asp:HiddenField runat="server" ID="hidDocID" />
                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <label class="control-label col-md-4">
                                            <asp:Label ID="lblStartDate" Text="วันที่พิจารณา (จาก)" runat="server" />
                                        </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtStartDate" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label col-md-4">
                                            <asp:Label ID="lblEndDate" Text="วันที่พิจารณา (ถึง)" runat="server" />
                                        </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtEndDate" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <label class="control-label col-md-4">
                                            <asp:Label ID="lblSEARCH" Text="ค้นหาตาม" runat="server" />
                                        </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtSEARCH" runat="server" CssClass="form-control" placeholder="ทะเบียนรถ,เลขที่สัญญา,พนักงานขับรถ" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label col-md-4">
                                            <asp:Label Text="สถานะ" runat="server" />
                                        </label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlSTATUS" CssClass="form-control" runat="server">
                                                <asp:ListItem Selected="true" Text="- สถานะ -" Value="" />
                                                <asp:ListItem Text="กำลังพิจารณา" Value="1" />
                                                <asp:ListItem Text="ตัดสิน" Value="2" />
                                                <asp:ListItem Text="รอพิจารณา" Value="3" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group" runat="server" id="divType">
                                    <div class="col-md-6">
                                        <label class="control-label col-md-4">
                                            <asp:Label Text="ประเภท" runat="server" />
                                        </label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlType" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="- เลือก -" Value="0" />
                                                <asp:ListItem Text="ตรวจสภาพรถ" Value="1" />
                                                <asp:ListItem Text="อุบัติเหตุ" Value="2" />
                                                <asp:ListItem Text="เรื่องร้องเรียน" Value="3" Selected="true" />
                                                <asp:ListItem Text="ยืนยันรถตามสัญญา" Value="4" />
                                                <asp:ListItem Text="ยืนยันรถตามแผน" Value="5" />
                                                <asp:ListItem Text="รายงานประจำเดือน" Value="6" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="panel-footer text-right" runat="server" id="divButton">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" OnClick="btnSearch_Click" CssClass="btn btn-md btn-hover btn-info"></asp:Button>
                                    <asp:Button ID="btnClear" runat="server" Text="เคลียร์" OnClick="btnClear_Click" CssClass="btn btn-md btn-hover btn-danger" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">
                                <asp:Label Text="" runat="server" ID="lblSearchResult" />&#8711;</a></a>
                            <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                        </div>
                        <div class="panel-collapse collapse in" id="collapseFindContract2">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvData" OnRowDataBound="gvData_RowDataBound" OnPageIndexChanging="gvData_PageIndexChanging" AllowPaging="true" PageSize="20"
                                            runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" 
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <a style="cursor: pointer;" href="#" id="aView" target="_blank" runat="server">
                                                            <asp:Image ImageUrl="~/Images/64/blue-37.png" Width="24px" Height="24px" runat="server" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <a style="cursor: pointer;" href="#" id="aEdit" target="_blank" runat="server" visible='<%# (Eval("CSTATUS") + string.Empty == "0" && Eval("CHKSTATUS") + string.Empty != "E") || Eval("CSTATUS") + string.Empty == "1" || Eval("CSTATUS") + string.Empty == "2" %>'>
                                                            <asp:Image ImageUrl="~/Images/64/blue-23.png" Width="24px" Height="24px" runat="server" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="รหัส" DataField="SAPPEALID" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="เลขที่เอกสาร" DataField="SCHECKID" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="วันที่เกิดเหตุ" DataField="DINCIDENT" ShowHeader="true" DataFormatString="{0:dd/MM/yyyy}">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="ชื่อผู้ประกอบการขนส่ง" DataField="SABBREVIATION" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="ประเภท" DataField="SPROCESSNAME" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Delivery No" DataField="SDELIVERYNO" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="หัวข้อปัญหา" DataField="STOPICNAME" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="ทะเบียนหัว" DataField="SHEADREGISTERNO" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="ทะเบียนหาง" DataField="STRAILERREGISTERNO" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="คะแนนที่ตัดก่อนอุทธรณ์" DataField="NPOINT">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="คะแนนที่ตัดหลังอุทธรณ์" DataField="TOTAL_POINT">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="ค่าปรับก่อนอุทธรณ์" DataField="COST">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="ค่าปรับหลังอุทธรณ์" DataField="TOTAL_COST">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="วันสิ้นสุดการขออุทธรณ์" HtmlEncode="false" DataField="DINCIDENT7" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="สถานะ<br/>การอุทธรณ์" HtmlEncode="false" DataField="STATUS" ShowHeader="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="CHKSTATUS" DataField="CHKSTATUS" Visible="false"></asp:BoundField>
                                                <asp:BoundField HeaderText="SVENDORID" DataField="SVENDORID" Visible="false"></asp:BoundField>
                                                <asp:BoundField HeaderText="SAPPEALNO" DataField="SAPPEALNO" Visible="false"></asp:BoundField>
                                                <asp:BoundField HeaderText="SPROCESSID" DataField="SPROCESSID" Visible="false"></asp:BoundField>
                                            </Columns>
                                            
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
