﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Config_reportKPI.aspx.cs" Inherits="Config_reportKPI" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script src="JQuery/jquery.validate.min.js" type="text/javascript"></script>
    <script src="JQuery/messages_th.min.js" type="text/javascript"></script>
    <style type="text/css">
    th
    {
        text-align:center;
    }
    .error
    {
        color: red;
    }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=cmdSave.ClientID%>").click(function () {
                $('form').validate();
                //Add a custom class to your name mangled input and add rules like this
                $('.required').rules('add', {
                    required: true
                });
                $('.number').rules('add',
                    {
                        number: true
                    });
                $('.CheckChar').rules('add',
                    {
                        CheckChar: true
                    });
            });
            //เวลามีการอนุมัติหรือกลับสู่หน้าหลัก
            $("#cph_Main_mpConfirmBack_btnSave").click(function () {
                $("form").validate().cancelSubmit = true;
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <br />
    <h5>รายละเอียดแบบฟอร์ม KPI</h5>
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>แบบฟอร์ม KPI
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            ปี :
                        </div>
                        <div class="col-md-2">
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
                <p>&nbsp;</p>
                <div class="panal_Gridview" runat="server" id="panal_Gridview" visible="false">
                    <div class="col-md-12">
                        <asp:GridView ID="dgvMonthKpi" runat="server" Width="50%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowDataBound="dgvMonthKpi_RowDataBound">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="ที่">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="MONTH" HeaderText="เดือน" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>                                
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="val_month" runat="server" Value="<%# Container.DataItemIndex+1 %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="แบบประเมิน">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_report" runat="server" CssClass="form-control required">
                                            <asp:ListItem Text="KPI1" />
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        </asp:GridView>
                    </div>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <div class="col-md-12">
                        <div class="col-md-offset-4 col-md-2 text-right row">
                            <asp:Button runat="server" ID="cmdSave" Width="150px" CssClass="btn btn-primary" OnClick="cmdSave_Click" Text="บันทึก" />
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-danger" Style="width: 120px;" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
    <uc1:ModelPopup runat="server" ID="mpConfirmBack" IDModel="ModalConfirmBack" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmBack_ClickOK" TextTitle="ยืนยันการย้อนกลับ" TextDetail="คุณต้องการย้อนกลับใช่หรือไม่ ?" />       
</asp:Content>

