﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ComplaintTopic.aspx.cs" Inherits="ComplaintTopic" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="หัวข้อการประเมิน"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:GridView ID="dgvTopic" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="TOPIC_ID"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnRowUpdating="dgvTopic_RowUpdating">
                                        <Columns>
                                            <asp:BoundField DataField="TOPIC_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TOPIC_NAME" HeaderText="ประเภทร้องเรียน">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TYPE" HeaderText="ประเภท">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ISACTIVE" HeaderText="สถานะ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="การกระทำ">
                                                <ItemTemplate>
                                                    <asp:Button ID="cmdEdit" runat="server" Text="แก้ไข" CommandName="update" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdAddComplain" runat="server" Text="เพิ่มประเภทร้องเรียน" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdAddComplain_Click" />
                            <asp:Button ID="cmdAddDetail" runat="server" Text="เพิ่มหัวข้อร้องเรียน" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdAddDetail_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>