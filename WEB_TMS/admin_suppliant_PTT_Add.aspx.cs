﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.IO;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using System.Configuration;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using EmailHelper;
using TMS_BLL.Transaction.Appeal;
using TMS_BLL.Transaction.Accident;
using System.Web.Security;
using GemBox.Spreadsheet;
using System.Drawing;
using System.Web.UI.HtmlControls;
using TMS_BLL.Master;

public partial class admin_suppliant_PTT_Add : PageBase
{
    #region Member
    DataSet ds;
    DataTable dt;
    DataRow dr;
    string FieldType = "SUPPLIANTPTT", str = string.Empty, mode = Mode.Edit.ToString();
    #region + View State +
    private int IndexEdit
    {
        get
        {
            if ((int)ViewState["IndexEdit"] != null)
                return (int)ViewState["IndexEdit"];
            else
                return -1;
        }
        set
        {
            ViewState["IndexEdit"] = value;
        }
    }
    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }
    private DataTable dtHeader
    {
        get
        {
            if ((DataTable)ViewState["dtHeader"] != null)
                return (DataTable)ViewState["dtHeader"];
            else
                return null;
        }
        set
        {
            ViewState["dtHeader"] = value;
        }
    }

    private DataTable dtScore
    {
        get
        {
            if ((DataTable)ViewState["dtScore"] != null)
                return (DataTable)ViewState["dtScore"];
            else
                return null;
        }
        set
        {
            ViewState["dtScore"] = value;
        }
    }
    private DataTable dtScoreListData
    {
        get
        {
            if ((DataTable)ViewState["dtScoreListData"] != null)
                return (DataTable)ViewState["dtScoreListData"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreListData"] = value;
        }
    }
    private DataTable dtScoreList
    {
        get
        {
            if ((DataTable)ViewState["dtScoreList"] != null)
                return (DataTable)ViewState["dtScoreList"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreList"] = value;
        }
    }

    private DataTable dtScoreListCar
    {
        get
        {
            if ((DataTable)ViewState["dtScoreListCar"] != null)
                return (DataTable)ViewState["dtScoreListCar"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreListCar"] = value;
        }
    }

    private DataTable dtDup
    {
        get
        {
            if ((DataTable)ViewState["dtDup"] != null)
                return (DataTable)ViewState["dtDup"];
            else
                return null;
        }
        set
        {
            ViewState["dtDup"] = value;
        }
    }

    private DataTable dtCusScoreType
    {
        get
        {
            if ((DataTable)ViewState["dtCusScoreType"] != null)
                return (DataTable)ViewState["dtCusScoreType"];
            else
                return null;
        }
        set
        {
            ViewState["dtCusScoreType"] = value;
        }
    }

    private DataTable dtCarDistinct
    {
        get
        {
            if ((DataTable)ViewState["dtCarDistinct"] != null)
                return (DataTable)ViewState["dtCarDistinct"];
            else
                return null;
        }
        set
        {
            ViewState["dtCarDistinct"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtUploadRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtUploadRequestFile"] != null)
                return (DataTable)ViewState["dtUploadRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadRequestFile"] = value;
        }
    }

    private DataTable dtUploadTypePTTDoc
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypePTTDoc"] != null)
                return (DataTable)ViewState["dtUploadTypePTTDoc"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypePTTDoc"] = value;
        }
    }

    private DataTable dtUploadPTTDoc
    {
        get
        {
            if ((DataTable)ViewState["dtUploadPTTDoc"] != null)
                return (DataTable)ViewState["dtUploadPTTDoc"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadPTTDoc"] = value;
        }
    }

    private DataTable dtUploadRequestFilePTTDoc
    {
        get
        {
            if ((DataTable)ViewState["dtUploadRequestFilePTTDoc"] != null)
                return (DataTable)ViewState["dtUploadRequestFilePTTDoc"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadRequestFilePTTDoc"] = value;
        }
    }

    private DataTable dtUploadTypeVendor
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeVendor"] != null)
                return (DataTable)ViewState["dtUploadTypeVendor"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeVendor"] = value;
        }
    }

    private DataTable dtUploadVendor
    {
        get
        {
            if ((DataTable)ViewState["dtUploadVendor"] != null)
                return (DataTable)ViewState["dtUploadVendor"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadVendor"] = value;
        }
    }

    private DataTable dtUploadRequestFileVendor
    {
        get
        {
            if ((DataTable)ViewState["dtUploadRequestFileVendor"] != null)
                return (DataTable)ViewState["dtUploadRequestFileVendor"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadRequestFileVendor"] = value;
        }
    }

    private DataTable dtUploadTypeChange
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeChange"] != null)
                return (DataTable)ViewState["dtUploadTypeChange"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeChange"] = value;
        }
    }

    private DataTable dtUploadChange
    {
        get
        {
            if ((DataTable)ViewState["dtUploadChange"] != null)
                return (DataTable)ViewState["dtUploadChange"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadChange"] = value;
        }
    }

    private DataTable dtUploadRequestFileChange
    {
        get
        {
            if ((DataTable)ViewState["dtUploadRequestFileChange"] != null)
                return (DataTable)ViewState["dtUploadRequestFileChange"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadRequestFileChange"] = value;
        }
    }

    private DataTable dtSUPPLIANT
    {
        get
        {
            if ((DataTable)ViewState["dtSUPPLIANT"] != null)
                return (DataTable)ViewState["dtSUPPLIANT"];
            else
                return null;
        }
        set
        {
            ViewState["dtSUPPLIANT"] = value;
        }
    }
    #endregion
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            SetText();
            SetDropDownList();
            //dtUserLogin = (DataTable)Session["UserLogin"];
            //hidSYSTEM_ID.Value = SystemID + string.Empty;
            dtUploadType = GetUploadType(ref ddlUploadType, FieldType);
            dtUpload = GetTableUpload();
            //dtUploadRequestFile = GetUploadRequire(ref dgvUploadRequestFile, FieldType);

            dtUploadTypeChange = GetUploadType(ref ddlUploadTypeChange, FieldType + "_CHANGE");
            dtUploadChange = GetTableUpload();
            dtUploadRequestFileChange = GetUploadRequire(ref dgvUploadRequestFileChange, FieldType + "_CHANGE");

            if (Request.QueryString["str"] != null && !string.IsNullOrEmpty(Request.QueryString["str"]))
            {
                //mode  & SAPPEALID & STATUS
                string[] str = DecodeQueryString(Request.QueryString["str"]);
                if (str.Count() > 1)
                {
                    hidCHKSTATUS.Value = str[2];
                    hidSAPPEALID.Value = str[1];
                    mode = str[0];

                    GetData();
                }
            }
            else
            {
                ViewState["DataField"] = GetField(FieldType);
                lblHeader.Text = SetField((DataTable)ViewState["DataField"]);
            }
            SetVisible();

        }
    }

    #endregion

    #region Method
    #region SetDropDownList
    private void SetDropDownList()
    {
        dt = AppealBLL.Instance.AppealSentencerTypeSelect();

        DropDownListHelper.BindDropDownList(ref ddlSENTENCERNAME, dt, "SSENTENCERCODE", "ID1", true);
    }
    #endregion

    #region SetVisible
    private void SetVisible()
    {
        if (hidCSTATUS.Value == "3")
        {
            Tab6.Visible = true;
        }
        else
        {
            Tab6.Visible = false;
        }
        if (mode == Mode.View.ToString())
        {
            btnSave.Visible = false;
            cmdSaveTab3.Visible = false;
            cmdSaveTabCommittee.Visible = false;

        }
        //if (ddlCSENTENCE.SelectedValue == "2")
        //{
        //    Tab5.Visible = true;
        //}
        //else
        //{
        //    Tab5.Visible = false;
        //}

    }
    #endregion

    #region SetText
    private void SetText()
    {
        btnCancel.Text = "ยกเลิก";
        btnSave.Text = "บันทึก";


    }
    #endregion

    #region GetData
    private void GetData()
    {

        ds = AppealBLL.Instance.AppealSelectByID(hidSAPPEALID.Value);
        string Vendor = string.Empty;
        if (ds.Tables["APPEAL"].Rows.Count > 0)
        {
            Vendor = ds.Tables["APPEAL"].Rows[0]["SVENDORID"] + string.Empty;
            hidNREDUCEID.Value = ds.Tables["APPEAL"].Rows[0]["NREDUCEID"] + string.Empty;
            gvData.DataSource = ds.Tables["APPEAL"];
            gvData.DataBind();
        }

        //dtSUPPLIANT = AppealBLL.Instance.AppealComplainSelectBLL(Vendor, this.GetConditionSearchComplain());

        ViewState["DataField"] = GetField(FieldType);

        lblHeader.Text = SetFieldValue((DataTable)ViewState["DataField"], ds.Tables["APPEAL"], mode);
        if (ds.Tables["APPEAL_C"].Rows.Count > 0)
        {
            SetFieldValue((DataTable)ViewState["DataField"], ds.Tables["APPEAL_C"], mode);
        }
        if (ds.Tables["APPEAL_G"].Rows.Count > 0)
        {
            SetFieldValue((DataTable)ViewState["DataField"], ds.Tables["APPEAL_G"], mode);
        }
        if (ds.Tables["APPEAL_S"].Rows.Count > 0)
        {
            SetFieldValue((DataTable)ViewState["DataField"], ds.Tables["APPEAL_S"], mode);
        }
        if (ds.Tables["APPEAL_R"].Rows.Count > 0)
        {
            SetFieldValue((DataTable)ViewState["DataField"], ds.Tables["APPEAL_R"], mode);
            cbCHANGE.Checked = true;
        }
        ddlCSENTENCE_SelectedIndexChanged(null, null);
        rblSSENTENCER_SelectedIndexChanged(null, null);
        dr = ds.Tables["APPEAL"].Rows[0];
        rblSSENTENCER_CHANGE.SelectedValue = dr["SSENTENCER"] + string.Empty;
        ddlSENTENCERNAME.SelectedValue = dr["SSENTENCERCODE"] + string.Empty;
        txtNPOINT2.Text = dr["NPOINT"] + string.Empty;
        cbCHANGE_CheckedChanged(null, null);
        txtDCHANGE.Enabled = false;

        txtSDELIVERYNO.Text = ds.Tables["APPEAL"].Rows[0]["SDELIVERYNO"].ToString();
        txtDINCIDENT.Text = ds.Tables["APPEAL"].Rows[0]["DINCIDENT"].ToString();
        txtCUST_NAME.Text = ds.Tables["APPEAL"].Rows[0]["CUST_NAME"].ToString();
        txtDBEGIN.Text = Convert.ToDateTime(ds.Tables["APPEAL"].Rows[0]["DBEGIN"].ToString()).ToString("dd/MM/yyyy");
        txtDEND.Text = Convert.ToDateTime(ds.Tables["APPEAL"].Rows[0]["DEND"].ToString()).ToString("dd/MM/yyyy");
        txtDUPDATE.Text = Convert.ToDateTime(ds.Tables["APPEAL"].Rows[0]["DAPPEAL"].ToString()).ToString("dd/MM/yyyy");
        txtNPOINT.Text = ds.Tables["APPEAL"].Rows[0]["NPOINT"].ToString();

        dtScoreListData = new DataTable();
        dtScoreListData.Columns.Add("STOPICID");
        dtScoreListData.Columns.Add("STOPICNAME");
        dtScoreListData.Columns.Add("Cost");
        dtScoreListData.Columns.Add("CostDisplay");
        dtScoreListData.Columns.Add("SuspendDriver");
        dtScoreListData.Columns.Add("DisableDriver");
        dtScoreListData.Columns.Add("DisableDriverDisplay");
        dtScoreListData.Columns.Add("Score1");
        dtScoreListData.Columns.Add("Score2");
        dtScoreListData.Columns.Add("Score3");
        dtScoreListData.Columns.Add("Score4");
        dtScoreListData.Columns.Add("Score5");
        dtScoreListData.Columns.Add("Score6");
        dtScoreListData.Columns.Add("Score6Display");
        dtScoreListData.Columns.Add("TotalCar");
        dtScoreListData.Columns.Add("Blacklist");

        dtScoreListData = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailBLL(ds.Tables["APPEAL"].Rows[0]["SREFERENCEID"].ToString());

        dtScoreListData.Columns["COST"].ColumnName = "Cost";
        dtScoreListData.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
        dtScoreListData.Columns["SUSPENDDRIVER"].ColumnName = "SuspendDriver";
        dtScoreListData.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
        dtScoreListData.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
        dtScoreListData.Columns["SCORE1"].ColumnName = "Score1";
        dtScoreListData.Columns["SCORE2"].ColumnName = "Score2";
        dtScoreListData.Columns["SCORE3"].ColumnName = "Score3";
        dtScoreListData.Columns["SCORE4"].ColumnName = "Score4";
        dtScoreListData.Columns["SCORE5"].ColumnName = "Score5";
        dtScoreListData.Columns["SCORE6"].ColumnName = "Score6";
        dtScoreListData.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
        dtScoreListData.Columns["TOTALCAR"].ColumnName = "TotalCar";
        dtScoreListData.Columns["BLACKLIST"].ColumnName = "Blacklist";

        GridViewHelper.BindGridView(ref dgvScoreListData, dtScoreListData);

        #region UploadFile
        dtUpload = UploadBLL.Instance.UploadSelectBLL(hidSAPPEALID.Value, FieldType, string.Empty);
        GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload, false);
        //CheckRequestFile(ref dgvUploadRequestFile, dtUploadRequestFile, dtUpload);

        //เอาข้อมูลมาจากหน้าแจ้งเรื่องร้องเรียน tab3 เอกสารพิจารณาโทษ
        dtUploadPTTDoc = ComplainImportFileBLL.Instance.ImportFileSelectBLL(dr["SREFERENCEID"].ToString(), "COMPLAIN", string.Empty); //UploadBLL.Instance.UploadSelectBLL(dr["SREFERENCEID"] + string.Empty, "COMPLAIN_SCORE", string.Empty);
        GridViewHelper.BindGridView(ref dgvUploadFilePTTDoc, dtUploadPTTDoc, false);
        //(ref dgvUploadRequestFilePTTDoc, dtUploadRequestFilePTTDoc, dtUploadPTTDoc);

        //dtUploadVendor = ComplainImportFileBLL.Instance.ImportFileSelectBLL(dr["SREFERENCEID"].ToString(), "COMPLAIN_REQ", string.Empty);//UploadBLL.Instance.UploadSelectBLL(hidSAPPEALID.Value, FieldType.Replace("PTT", ""), string.Empty);
        //GridViewHelper.BindGridView(ref dgvUploadFileVendor, dtUploadVendor, false);
        dtUploadVendor = UploadBLL.Instance.UploadSelectBLL(hidSAPPEALID.Value, "SUPPLIANT", string.Empty);
        GridViewHelper.BindGridView(ref dgvUploadFileVendor, dtUploadVendor, false);
        //CheckRequestFile(ref dgvUploadRequestFileVendor, dtUploadRequestFileVendor, dtUploadVendor);

        dtUploadChange = UploadBLL.Instance.UploadSelectBLL(hidSAPPEALID.Value, FieldType + "_CHANGE", string.Empty);
        GridViewHelper.BindGridView(ref dgvUploadFileChange, dtUploadChange, false);
        CheckRequestFile(ref dgvUploadRequestFileChange, dtUploadRequestFileChange, dtUploadChange);
        #endregion

    }


    private string GetConditionSearchComplain()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            //if (!string.IsNullOrEmpty(hidID1.Value))
            //    return sb.Append(" AND TCOMPLAIN.DOC_ID || '/' || TCOMPLAIN_SCORE_CAR.DELIVERY_DATE || '/' || NVL(TCOMPLAIN_SCORE_CAR.STRUCKID, '') ='" + hidID1.Value + "'").ToString();

            if (!string.Equals(hidSAPPEALID.Value, string.Empty))
                sb.Append(" AND TAPPEAL.SAPPEALID = '" + hidSAPPEALID.Value + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #endregion

    #region Event
    #region mpSave_ClickOK
    protected void mpSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            dt = GetDataToSave(FieldType);
            dr = dt.Rows[0];
            if (!dt.Columns.Contains("CSTATUS"))
            {
                dt.Columns.Add("CSTATUS");
            }
            if (ddlCSENTENCE.SelectedValue == "1")
            {
                dr["CSTATUS"] = "3";
            }
            else if (ddlCSENTENCE.SelectedValue == "2")
            {
                dr["CSTATUS"] = "5";
            }
            else if (ddlCSENTENCE.SelectedValue == "3")
            {
                dr["CSTATUS"] = "6";
            }
            else if (ddlCSENTENCE.SelectedValue == "4")
            {
                dr["CSTATUS"] = "2";
            }
            string mess = string.Empty;
            ds = new DataSet("DS");
            dt.TableName = "DT";

            ds.Tables.Add(dt.Copy());

            dt = AppealBLL.Instance.AppealConsiderSave(hidSAPPEALID.Value, int.Parse(Session["UserID"].ToString()), ds, dtUpload);
            if (dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
                if (dr.IsNull("MESSAGE"))
                {
                    //กรณีเปลี่ยนผลการพิจารณา
                    if (ddlCSENTENCE.SelectedValue == "1" && rblSSENTENCER.SelectedValue == "3")
                    {
                        #region กรณีเปลี่ยนผลการพิจารณา
                        DataTable dtAppeal = AppealBLL.Instance.AppealSelectBLL(hidSAPPEALID.Value);
                        string ComplainDocID = dtAppeal.Rows[0]["SREFERENCEID"].ToString();

                        int Blacklist = (chkBlacklist.Checked) ? 1 : 0;
                        decimal CostFinal = 0, CostOther = 0, FinalCost = 0;
                        CostFinal = (!string.Equals(txtCostFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostFinal.Text.Trim()) : 0;
                        CostOther = (!string.Equals(txtCostOther.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostOther.Text.Trim()) : 0;
                        if (CostFinal == 0 && CostOther == 0)
                            FinalCost = -1;
                        else
                            FinalCost = CostFinal + CostOther;

                        ComplainImportFileBLL.Instance.ComplainTab3AppealSaveBLL(ComplainDocID, int.Parse(Session["UserID"].ToString()), (radCusScoreType.SelectedIndex > -1) ? int.Parse(radCusScoreType.SelectedValue) : -1, (!string.Equals(txtPointFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtPointFinal.Text.Trim()) : -1, FinalCost, (!string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? int.Parse(txtDisableFinal.Text.Trim()) : -1, dtScoreList, dtScoreListCar, decimal.Parse(txtCostOther.Text.Trim()), 0, txtRemarkTab3.Text.Trim(), decimal.Parse(txtOilLose.Text.Trim()), hidSAPPEALID.Value, Blacklist);

                        AppealBLL.Instance.ComplainCheckStatusBLL();
                        #endregion
                    }
                    else if (ddlCSENTENCE.SelectedValue == "1")
                    {
                        AppealBLL.Instance.ComplainCheckStatusBLL();
                    }
                    if ((ddlCSENTENCE.SelectedValue == "1") && (rblSSENTENCER.SelectedValue == "1" || rblSSENTENCER.SelectedValue == "3"))
                    {

                    }
                    this.SendEmail(ConfigValue.EmailComplainAppealConfirm, gvData.Rows[0].Cells[4].Text);
                    alertSuccess("บันทึกสำเร็จ", "admin_suppliant_lst_New.aspx");
                }
                else if (dr["MESSAGE"] + string.Empty == "CODE")
                {
                    alertFail("ไม่สามารถเพิ่มสัญญาได้<br/>เนื่องจาก รหัส สัญญา ซ้ำ !!!");
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion


    protected void mpSaveChange_ClickOK(object sender, EventArgs e)
    {
        try
        {
            if (cbCHANGE.Checked)
            {
                dt = GetDataToSave(FieldType);
                dr = dt.Rows[0];
                if (!dt.Columns.Contains("CSTATUS"))
                {
                    dt.Columns.Add("CSTATUS");
                }
                dr["CSTATUS"] = "3";
                dr["SSENTENCER"] = rblSSENTENCER_CHANGE.SelectedValue;
                string mess = string.Empty; //Validate(FieldType, string.Empty, dt);
                mess += ValidateFile(dgvUploadRequestFileChange);
                if (!string.IsNullOrEmpty(mess))
                {
                    alertFail(mess);
                    return;
                }
                ds = new DataSet("DS");
                dt.TableName = "DT";
                //dr = dt.Rows[0];

                ds.Tables.Add(dt.Copy());

                dt = AppealBLL.Instance.AppealChangeSave(hidSAPPEALID.Value, UserID, ds, dtUploadChange);
                if (dt.Rows.Count > 0)
                {
                    dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        alertSuccess("บันทึกสำเร็จ");
                    }
                    else if (dr["MESSAGE"] + string.Empty == "CODE")
                    {
                        alertFail("ไม่สามารถเพิ่มสัญญาได้<br/>เนื่องจาก รหัส สัญญา ซ้ำ !!!");
                    }
                }
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    #region mpCancel_ClickOK
    protected void mpCancel_ClickOK(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "close", "javascript:window.close();", true);
    }
    #endregion

    protected void ddlCSENTENCE_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCSENTENCE.SelectedValue == "1")
        {
            ddlSENTENCERNAME.Visible = false;
            txtSWAITDOCUMENT.Visible = false;
            btnSENTENCERNAME.Visible = false;
            rblSSENTENCER.Enabled = true;
        }
        else if (ddlCSENTENCE.SelectedValue == "2")
        {
            ddlSENTENCERNAME.Visible = true;
            txtSWAITDOCUMENT.Visible = false;
            btnSENTENCERNAME.Visible = true;
            rblSSENTENCER.Enabled = true;
        }
        else if (ddlCSENTENCE.SelectedValue == "3")
        {
            ddlSENTENCERNAME.Visible = false;
            txtSWAITDOCUMENT.Visible = false;
            btnSENTENCERNAME.Visible = false;
            rblSSENTENCER.Enabled = false;
        }
        else if (ddlCSENTENCE.SelectedValue == "4")
        {
            ddlSENTENCERNAME.Visible = false;
            txtSWAITDOCUMENT.Visible = true;
            btnSENTENCERNAME.Visible = false;
            rblSSENTENCER.Enabled = false;
        }

        this.SetVisible();
    }

    protected void rblSSENTENCER_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblSSENTENCER.SelectedValue == "3")
        {
            btnSSENTENCER.Visible = true;
        }
        else
        {
            btnSSENTENCER.Visible = false;

        }
    }

    protected void btnSENTENCERNAME_Click(object sender, EventArgs e)
    {
        if (ddlSENTENCERNAME.SelectedIndex > 0)
        {
            dt = AppealBLL.Instance.AppealSentencerTypeSelect(ddlSENTENCERNAME.SelectedValue);
            int i = 1;
            string TextDetail = string.Empty;
            foreach (DataRow item in dt.Rows)
            {
                TextDetail += "<br/><label class='col-md-12 control-label' style='font-weight:normal'>";
                TextDetail += "ลำดับที่ : " + i + "ชื่อ คุณ " + item["FNAME"] + "  ตำแหน่ง " + item["SJOB"] + "";
                TextDetail += "</label>";
                i++;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ModalSENTENCERNAM').find('.TextDetail').empty();$('#ModalSENTENCERNAM').find('.TextDetail').append(\"" + TextDetail + "\");$('#btnSave2').click();</script>", false);
        }
    }


    protected void cbCHANGE_CheckedChanged(object sender, EventArgs e)
    {
        if (cbCHANGE.Checked)
        {
            txtSBECUASE.Enabled = true;
            rblSSENTENCER_CHANGE.Enabled = true;
            txtSAPPROVEBY.Enabled = true;
            cmdUploadChange.Enabled = true;
        }
        else
        {
            txtSBECUASE.Enabled = false;
            rblSSENTENCER_CHANGE.Enabled = false;
            txtSAPPROVEBY.Enabled = false;
            cmdUploadChange.Enabled = false;
            txtSBECUASE.Text = string.Empty;
            rblSSENTENCER_CHANGE.ClearSelection();
            txtSAPPROVEBY.Text = string.Empty;
        }
    }
    #endregion

    #region UploadFile
    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        try
        {
            dtUpload = StartUpload(ref ddlUploadType, fileUpload1, dtUploadType, dtUploadRequestFile, dtUpload, ref dgvUploadFile, ref dgvUploadRequestFile);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload, false);

            CheckRequestFile(ref dgvUploadRequestFile, dtUploadRequestFile, dtUpload);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    #endregion

    #region UploadFilePTTDoc
    protected void dgvUploadFilePTTDoc_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFilePTTDoc, dtUploadPTTDoc, false);

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFilePTTDoc_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFilePTTDoc.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    #endregion

    #region UploadFileVendor
    protected void dgvUploadFileVendor_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadVendor.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileVendor, dtUploadVendor, false);

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFileVendor_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFileVendor.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    #endregion

    #region UploadFileChange
    protected void cmdUploadChange_Click(object sender, EventArgs e)
    {
        try
        {
            dtUpload = StartUpload(ref ddlUploadTypeChange, fileUploadChange, dtUploadTypeChange, dtUploadRequestFileChange, dtUploadChange, ref dgvUploadFileChange, ref dgvUploadRequestFileChange);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFileChange_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadChange.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileChange, dtUploadChange, false);

            CheckRequestFile(ref dgvUploadRequestFileChange, dtUploadRequestFileChange, dtUploadChange);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFileChange_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFileChange.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    #endregion

    protected void cmdCloseTab3_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "$('#ShowScore').modal('hide');", true);
        //if (Session["dtUpload"] != null)
        //{
        //    lblAttachTotal.Text = string.Format(lblAttachTotal.Text, ((DataTable)Session["dtUpload"]).Rows.Count.ToString());

        //}
        //else
        //{
        //    lblAttachTotal.Text = string.Format(lblAttachTotal.Text, "0");
        //}
        //rblStatus4.Value = Session["rblStatus"];
        //rblCSENTENCE4.Value = Session["rblCSENTENCE4"];
    }

    protected void cmdSaveTab3_Click(object sender, EventArgs e)
    {
        if (dtScoreList.Rows.Count == 0)
        {
            alertFail("กรุณาเลือกหัวข้อตัดคะแนน");
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "$('#ShowScore').modal('hide');", true);
        //lblAttachTotal.Text = string.Format(lblAttachTotal.Text, ((DataTable)Session["dtUpload"]).Rows.Count.ToString());
        //rblStatus4.Value = Session["rblStatus"];
        //rblCSENTENCE4.Value = Session["rblCSENTENCE4"];
    }


    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    protected void cmdAppeal_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["dtScoreList"] == null)
            {
                string AppealID = hidSAPPEALID.Value;//Session["SAPPEALID"].ToString();
                DataTable dtAppeal = AppealBLL.Instance.AppealSelectBLL(AppealID);
                string ComplainDocID = dtAppeal.Rows[0]["SREFERENCEID"].ToString();
                IndexEdit = -1;

                DataTable dtAppealData = AppealBLL.Instance.AppealSelectAllBLL(AppealID);

                if (ComplainDocID.IndexOf("ACC") > -1)//กรณึอุบัติเหตุ
                {
                    #region อุบัติเหตุ
                    ds = AccidentBLL.Instance.AccidentTab4Select(ComplainDocID);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables["ACCIDENT"];
                        if (dt != null && dt.Rows.Count > 0)
                        {

                            DataRow dr = dt.Rows[0];

                            txtRemarkTab3.Text = dr["REMARK_TAB4"] + string.Empty;
                            lblCusScoreHeader.Text = string.Format(lblCusScoreHeader.Text, dr["SCONTRACTNO"] + string.Empty);
                        }
                    }
                    dtScore = ComplainBLL.Instance.ScoreSelectBLL();

                    DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
                    this.SelectDuplicateCarAcc();

                    dtScoreList = new DataTable();
                    dtScoreList.Columns.Add("STOPICID");
                    dtScoreList.Columns.Add("STOPICNAME");
                    dtScoreList.Columns.Add("Cost");
                    dtScoreList.Columns.Add("CostDisplay");
                    dtScoreList.Columns.Add("SuspendDriver");
                    dtScoreList.Columns.Add("DisableDriver");
                    dtScoreList.Columns.Add("DisableDriverDisplay");
                    dtScoreList.Columns.Add("Score1");
                    dtScoreList.Columns.Add("Score2");
                    dtScoreList.Columns.Add("Score3");
                    dtScoreList.Columns.Add("Score4");
                    dtScoreList.Columns.Add("Score5");
                    dtScoreList.Columns.Add("Score6");
                    dtScoreList.Columns.Add("Score6Display");
                    dtScoreList.Columns.Add("TotalCar");
                    dtScoreList.Columns.Add("Blacklist");

                    dtScoreListCar = new DataTable();
                    dtScoreListCar.Columns.Add("STOPICID");
                    dtScoreListCar.Columns.Add("TRUCKID");
                    dtScoreListCar.Columns.Add("CARHEAD");
                    dtScoreListCar.Columns.Add("CARDETAIL");
                    dtScoreListCar.Columns.Add("DELIVERY_DATE");
                    dtScoreListCar.Columns.Add("TOTAL_CAR");

                    int sum = 0;

                    if (sum > 0)
                    {
                        rowListTotalCarTab3.Visible = true;
                        rowListCarTab3.Visible = false;

                        dtDup = AccidentBLL.Instance.AccidentDuplicateTotal(ComplainDocID);
                        GridViewHelper.BindGridView(ref dgvScoreTotalCar, dtDup);
                    }
                    else
                    {
                        rowListTotalCarTab3.Visible = false;
                        rowListCarTab3.Visible = true;
                    }

                    dtCusScoreType = ComplainBLL.Instance.CusScoreSelectBLL();
                    DropDownListHelper.BindRadioButton(ref radCusScoreType, dtCusScoreType, "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME");

                    if (!string.Equals(hidSAPPEALID.Value, string.Empty))//if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dtScoreList = AccidentBLL.Instance.AccidentScoreDetailASelect(ComplainDocID);
                        dtScoreListCar = AccidentBLL.Instance.AccidentScoreCarASelect(ComplainDocID);

                        if ((dtScoreList.Rows.Count == 0) || (dtScoreListCar.Rows.Count == 0))
                        {
                            dtScoreList = AccidentBLL.Instance.AccidentScoreDetailPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                            dtScoreListCar = AccidentBLL.Instance.AccidentScoreCarPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                        }
                    }
                    else
                    {
                        dtScoreList = AccidentBLL.Instance.AccidentScoreDetailPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                        dtScoreListCar = AccidentBLL.Instance.AccidentScoreCarPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                    }

                    dtScoreList.Columns["COST"].ColumnName = "Cost";
                    dtScoreList.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
                    dtScoreList.Columns["SUSPENDDRIVER"].ColumnName = "SuspendDriver";
                    dtScoreList.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
                    dtScoreList.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
                    dtScoreList.Columns["SCORE1"].ColumnName = "Score1";
                    dtScoreList.Columns["SCORE2"].ColumnName = "Score2";
                    dtScoreList.Columns["SCORE3"].ColumnName = "Score3";
                    dtScoreList.Columns["SCORE4"].ColumnName = "Score4";
                    dtScoreList.Columns["SCORE5"].ColumnName = "Score5";
                    dtScoreList.Columns["SCORE6"].ColumnName = "Score6";
                    dtScoreList.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
                    dtScoreList.Columns["TOTALCAR"].ColumnName = "TotalCar";

                    GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);

                    dt = new DataTable();
                    if (!string.Equals(hidSAPPEALID.Value, string.Empty))//if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dt = AccidentBLL.Instance.AccidentScoreASelect(ComplainDocID);

                        if (dt.Rows.Count == 0)
                            dt = AccidentBLL.Instance.AccidentScoreSelect(ComplainDocID);
                    }
                    else
                    {
                        dt = AccidentBLL.Instance.AccidentScoreSelect(ComplainDocID);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        if (!string.Equals(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString(), "-1"))
                        {
                            radCusScoreType.SelectedValue = dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString();

                            txtPointFinal.Enabled = false;
                            txtCostFinal.Enabled = false;
                            txtDisableFinal.Enabled = false;

                            lblShowSumPoint.Text = "(ผลรวม : {0})";
                            lblShowSumCost.Text = "(ผลรวม : {0})";
                            lblShowSumDisable.Text = "(ผลรวม : {0})";

                            lblShowSumPoint.Visible = false;
                            lblShowSumCost.Visible = false;
                            lblShowSumDisable.Visible = false;

                            int row = this.GetSelectRow(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString());

                            this.RefreshCusScore();

                            txtCostOther.Text = dt.Rows[0]["COST_OTHER"].ToString();
                            txtOilLose.Text = dt.Rows[0]["OIL_LOSE"].ToString();
                            this.CheckBlacklist();
                            if (string.Equals(dt.Rows[0]["BLACKLIST"].ToString(), "1"))
                            {
                                chkBlacklist.Checked = true;
                                chkBlacklist.Enabled = true;
                            }
                        }

                        Session["dtScoreList"] = dtScoreList;
                    }
                    #endregion
                }
                else//ข้อร้องเรียน
                {
                    #region ข้อร้องเรียน
                    StringBuilder ConditionData = new StringBuilder(); ;
                    ConditionData.Append(" AND TO_CHAR(c.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') = '" + dtAppealData.Rows[0]["DREDUCE"].ToString() + "'");
                    if (!string.Equals(dtAppealData.Rows[0]["SHEADID"].ToString(), string.Empty))
                        ConditionData.Append(" AND c.STRUCKID = '" + dtAppealData.Rows[0]["SHEADID"].ToString() + "'");

                    StringBuilder ConditionHeader = new StringBuilder();
                    ConditionHeader.Append(" AND TO_CHAR(TCOMPLAIN.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') = '" + dtAppealData.Rows[0]["DREDUCE"].ToString() + "'");
                    if (!string.Equals(dtAppealData.Rows[0]["SHEADID"].ToString(), string.Empty))
                        ConditionHeader.Append(" AND TCOMPLAIN.STRUCKID = '" + dtAppealData.Rows[0]["SHEADID"].ToString() + "'");

                    if (cmdSaveTab3.Visible)
                    {
                        dtData = ComplainBLL.Instance.ComplainSelectBLL(" AND c.DOC_ID = '" + ComplainDocID + "'" + ConditionData.ToString());
                        dtHeader = ComplainBLL.Instance.ComplainSelectHeaderBLL(" AND TCOMPLAIN.DOC_ID = '" + ComplainDocID + "'" + ConditionHeader.ToString());
                    }
                    else
                    {
                        dtData = ComplainBLL.Instance.ComplainSelectBLL(" AND c.DOC_ID = '" + ComplainDocID + "'" + ConditionData.ToString());
                        dtHeader = ComplainBLL.Instance.ComplainSelectHeaderBLL(" AND TCOMPLAIN.DOC_ID = '" + ComplainDocID + "'" + ConditionHeader.ToString());
                    }

                    txtRemarkTab3.Text = dtData.Rows[0]["REMARK_TAB3"].ToString();
                    lblCusScoreHeader.Text = string.Format(lblCusScoreHeader.Text, dtHeader.Rows[0]["CONTRACTNAME"].ToString());

                    dtScore = ComplainBLL.Instance.ScoreSelectBLL();

                    DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
                    this.SelectDuplicateCar();

                    dtScoreList = new DataTable();
                    dtScoreList.Columns.Add("STOPICID");
                    dtScoreList.Columns.Add("STOPICNAME");
                    dtScoreList.Columns.Add("Cost");
                    dtScoreList.Columns.Add("CostDisplay");
                    dtScoreList.Columns.Add("SuspendDriver");
                    dtScoreList.Columns.Add("DisableDriver");
                    dtScoreList.Columns.Add("DisableDriverDisplay");
                    dtScoreList.Columns.Add("Score1");
                    dtScoreList.Columns.Add("Score2");
                    dtScoreList.Columns.Add("Score3");
                    dtScoreList.Columns.Add("Score4");
                    dtScoreList.Columns.Add("Score5");
                    dtScoreList.Columns.Add("Score6");
                    dtScoreList.Columns.Add("Score6Display");
                    dtScoreList.Columns.Add("TotalCar");
                    dtScoreList.Columns.Add("Blacklist");

                    dtScoreListCar = new DataTable();
                    dtScoreListCar.Columns.Add("STOPICID");
                    dtScoreListCar.Columns.Add("TRUCKID");
                    dtScoreListCar.Columns.Add("CARHEAD");
                    dtScoreListCar.Columns.Add("CARDETAIL");
                    dtScoreListCar.Columns.Add("DELIVERY_DATE");
                    dtScoreListCar.Columns.Add("TOTAL_CAR");

                    int sum = 0;
                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                    {
                        if (!string.Equals(dtHeader.Rows[i]["TotalCar"].ToString(), string.Empty) && int.Parse(dtHeader.Rows[i]["TotalCar"].ToString()) > 0)
                            sum += int.Parse(dtHeader.Rows[i]["TotalCar"].ToString());
                    }

                    if (sum > 0)
                    {
                        rowListTotalCarTab3.Visible = true;
                        rowListCarTab3.Visible = false;

                        dtDup = ComplainBLL.Instance.SelectDuplicateTotalCarPartialBLL(ComplainDocID, " AND TO_CHAR(TCOMPLAIN.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') = '" + dtAppealData.Rows[0]["DREDUCE"].ToString() + "'");
                        GridViewHelper.BindGridView(ref dgvScoreTotalCar, dtDup, false);
                    }
                    else
                    {
                        rowListTotalCarTab3.Visible = false;
                        rowListCarTab3.Visible = true;
                    }

                    dtCusScoreType = ComplainBLL.Instance.CusScoreSelectBLL();
                    radCusScoreType.DataTextField = "CUSSCORE_TYPE_NAME";
                    radCusScoreType.DataValueField = "CUSSCORE_TYPE_ID";
                    radCusScoreType.DataSource = dtCusScoreType;
                    radCusScoreType.DataBind();
                    //DropDownListHelper.BindRadioButtonList(ref radCusScoreType, dtCusScoreType, "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true, string.Empty);

                    if (!string.Equals(hidSAPPEALID.Value, string.Empty))//if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailAppealBLL(ComplainDocID);
                        dtScoreListCar = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreCarAppealBLL(ComplainDocID);

                        if ((dtScoreList.Rows.Count == 0) || (dtScoreListCar.Rows.Count == 0))
                        {
                            dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                            dtScoreListCar = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreCarPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                        }
                    }
                    else
                    {
                        dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                        dtScoreListCar = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreCarPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                    }

                    dtScoreList.Columns["COST"].ColumnName = "Cost";
                    dtScoreList.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
                    dtScoreList.Columns["SUSPEND_DRIVER"].ColumnName = "SuspendDriver";
                    dtScoreList.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
                    dtScoreList.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
                    dtScoreList.Columns["SCORE1"].ColumnName = "Score1";
                    dtScoreList.Columns["SCORE2"].ColumnName = "Score2";
                    dtScoreList.Columns["SCORE3"].ColumnName = "Score3";
                    dtScoreList.Columns["SCORE4"].ColumnName = "Score4";
                    dtScoreList.Columns["SCORE5"].ColumnName = "Score5";
                    dtScoreList.Columns["SCORE6"].ColumnName = "Score6";
                    dtScoreList.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
                    dtScoreList.Columns["TOTALCAR"].ColumnName = "TotalCar";
                    dtScoreList.Columns["BLACKLIST"].ColumnName = "Blacklist";

                    GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList, false);

                    DataTable dt = new DataTable();
                    if (!string.Equals(hidSAPPEALID.Value, string.Empty))//if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreAppealBLL(ComplainDocID);

                        if (dt.Rows.Count == 0)
                            dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreBLL(ComplainDocID);
                    }
                    else
                    {
                        dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreBLL(ComplainDocID);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        if (!string.Equals(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString(), "-1"))
                        {
                            radCusScoreType.SelectedValue = dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString();

                            txtPointFinal.Enabled = false;
                            txtCostFinal.Enabled = false;
                            txtDisableFinal.Enabled = false;

                            lblShowSumPoint.Text = "(ผลรวม : {0})";
                            lblShowSumCost.Text = "(ผลรวม : {0})";
                            lblShowSumDisable.Text = "(ผลรวม : {0})";

                            lblShowSumPoint.Visible = false;
                            lblShowSumCost.Visible = false;
                            lblShowSumDisable.Visible = false;

                            int row = this.GetSelectRow(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString());

                            this.RefreshCusScore();

                            txtCostOther.Text = dt.Rows[0]["COST_OTHER"].ToString();
                            txtOilLose.Text = dt.Rows[0]["OIL_LOSE"].ToString();
                            this.CheckBlacklist();
                            if (string.Equals(dt.Rows[0]["BLACKLIST"].ToString(), "1"))
                            {
                                chkBlacklist.Checked = true;
                                chkBlacklist.Enabled = true;
                            }
                        }
                        if (string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-2"))
                        {
                            txtDisableFinal.Text = "Hold";
                            txtDisableFinal.ReadOnly = true;
                        }

                        Session["dtScoreList"] = dtScoreList;
                    }
                    #endregion
                }

            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowScore", "$('#ShowScore').modal();", true);
            updShowScore.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckBlacklist()
    {
        try
        {
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (this.CheckTopicBlacklist(dtScoreList.Rows[i]["STOPICID"].ToString()))
                {
                    chkBlacklist.Enabled = true;
                    return;
                }
            }

            chkBlacklist.Checked = false;
            chkBlacklist.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private bool CheckTopicBlacklist(string STOPICID)
    {
        try
        {
            for (int i = 0; i < dtScore.Rows.Count; i++)
            {
                if (string.Equals(dtScore.Rows[i]["STOPICID"].ToString(), STOPICID) && string.Equals(dtScore.Rows[i]["BLACKLIST"].ToString(), "1"))
                    return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FindSum(string ShowType)
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                PointMax += decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtPointFinal.Text = PointMax.ToString();
            else
                lblShowSumPoint.Text = string.Format(lblShowSumPoint.Text, PointMax.ToString());

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                CostMax += decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtCostFinal.Text = CostMax.ToString();
            else
                lblShowSumCost.Text = string.Format(lblShowSumCost.Text, CostMax.ToString());

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                DisableDriverMax += int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            if (DisableDriverMax == -2)
            {
                txtDisableFinal.Text = "Hold";
                lblShowSumDisable.Text = "Hold";
            }
            else
            {
                if (string.Equals(ShowType, "Normal"))
                    txtDisableFinal.Text = DisableDriverMax.ToString();
                else
                    lblShowSumDisable.Text = string.Format(lblShowSumDisable.Text, DisableDriverMax.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int GetSelectRow(string Value)
    {
        try
        {
            for (int i = 0; i < dtCusScoreType.Rows.Count; i++)
            {
                if (string.Equals(dtCusScoreType.Rows[i]["CUSSCORE_TYPE_ID"].ToString(), Value))
                    return i;
            }

            return -1;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SelectDuplicateCarAcc()
    {
        try
        {
            dtCarDistinct = new DataTable();
            dtCarDistinct.Columns.Add("DELIVERY_DATE");
            dtCarDistinct.Columns.Add("TRUCKID");
            dtCarDistinct.Columns.Add("CARHEAD");
            dtCarDistinct.Columns.Add("CARDETAIL");
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    dr = ds.Tables[0].Rows[i];
                    if (string.Equals(dr["STRUCKID"].ToString(), string.Empty))
                        continue;

                    if (dtCarDistinct.Rows.Count == 0)
                    {
                        dtCarDistinct.Rows.Add(DateTime.Parse(dr["ACCIDENT_DATE"].ToString()).ToString("dd/MM/yyyy HH:mm"), dr["STRUCKID"].ToString(), dr["SHEADREGISTERNO"].ToString(), dr["STRAILERREGISTERNO"].ToString());
                    }
                    else
                    {
                        int j;
                        for (j = 0; j < dtCarDistinct.Rows.Count; j++)
                        {
                            if (string.Equals(dtCarDistinct.Rows[j]["STRUCKID"].ToString(), dr["STRUCKID"].ToString()) && string.Equals(dtCarDistinct.Rows[j]["DELIVERY_DATE"].ToString(), dr["ACCIDENT_DATE"].ToString()))
                                break;
                        }
                        if ((j == dtCarDistinct.Rows.Count) && ((!string.Equals(dtCarDistinct.Rows[j - 1]["STRUCKID"].ToString(), dr["STRUCKID"].ToString())) || (!string.Equals(dtCarDistinct.Rows[j - 1]["DELIVERY_DATE"].ToString(), dr["ACCIDENT_DATE"].ToString()))))
                            dtCarDistinct.Rows.Add(dr["ACCIDENT_DATE"].ToString(), dr["STRUCKID"].ToString(), dr["SHEADREGISTERNO"].ToString(), dr["STRAILERREGISTERNO"].ToString());
                    }
                }
            }


            GridViewHelper.BindGridView(ref dgvScore, dtCarDistinct, false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SelectDuplicateCar()
    {
        try
        {
            dtCarDistinct = new DataTable();
            dtCarDistinct.Columns.Add("DELIVERY_DATE");
            dtCarDistinct.Columns.Add("TRUCKID");
            dtCarDistinct.Columns.Add("CARHEAD");
            dtCarDistinct.Columns.Add("CARDETAIL");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (string.Equals(dtHeader.Rows[i]["TRUCKID"].ToString(), string.Empty))
                    continue;

                if (dtCarDistinct.Rows.Count == 0)
                {
                    dtCarDistinct.Rows.Add(dtHeader.Rows[i]["DELIVERYDATE"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
                else
                {
                    int j;
                    for (j = 0; j < dtCarDistinct.Rows.Count; j++)
                    {
                        if (string.Equals(dtCarDistinct.Rows[j]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString()) && string.Equals(dtCarDistinct.Rows[j]["DELIVERY_DATE"].ToString(), dtHeader.Rows[i]["DELIVERYDATE"].ToString()))
                            break;
                    }
                    if ((j == dtCarDistinct.Rows.Count) && ((!string.Equals(dtCarDistinct.Rows[j - 1]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString())) || (!string.Equals(dtCarDistinct.Rows[j - 1]["DELIVERY_DATE"].ToString(), dtHeader.Rows[i]["DELIVERYDATE"].ToString()))))
                        dtCarDistinct.Rows.Add(dtHeader.Rows[i]["DELIVERYDATE"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
            }

            GridViewHelper.BindGridView(ref dgvScore, dtCarDistinct, false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cboScore_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.EnableScore(false);

            if (cboScore.SelectedIndex == 0)
            {
                this.ClearScreenTab3();
            }
            else
            {
                txtCost.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString();
                if (cboScore.SelectedValue == "115")
                {
                    txtDisableDriver.Text = "Hold";
                    txtDisableDriver.Enabled = false;
                }
                else
                {
                    txtDisableDriver.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString();
                }
                txtScore1.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL02"].ToString();
                txtScore2.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL03"].ToString();
                txtScore3.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL04"].ToString();
                txtScore4.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL06"].ToString();
                txtScore5.Text = dtScore.Rows[cboScore.SelectedIndex]["NMULTIPLEIMPACT"].ToString();
                txtScore6.Text = dtScore.Rows[cboScore.SelectedIndex]["NPOINT"].ToString();
                lblShowOption.Visible = (dtScore.Rows[cboScore.SelectedIndex]["IS_CORRUPT"].ToString() == "0") ? false : true;

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("น้ำมัน"))
                {
                    txtOilPrice.Text = "0";
                    txtOilQty.Text = "0";
                    rowOil.Visible = true;
                }
                else
                {
                    txtOilPrice.Text = string.Empty;
                    txtOilQty.Text = string.Empty;
                    rowOil.Visible = false;
                }

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("ซีล"))
                {
                    txtZeal.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString();
                    txtZealQty.Text = "0";
                    rowZeal.Visible = true;
                }
                else
                {
                    txtZeal.Text = string.Empty;
                    txtZealQty.Text = string.Empty;
                    rowZeal.Visible = false;
                }

                if ((!lblShowOption.Visible) && (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["CAN_EDIT"].ToString(), "1")))
                {//สามารถแก้ไขคะแนนได้
                    this.EnableScore(true);
                }

                if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["BLACKLIST"].ToString(), "1"))
                {
                    chkBlacklist.Checked = true;
                    chkBlacklist.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ClearScreenTab3()
    {
        try
        {
            cboScore.SelectedIndex = 0;
            txtCost.Text = string.Empty;
            txtDisableDriver.Text = string.Empty;
            txtScore1.Text = string.Empty;
            txtScore2.Text = string.Empty;
            txtScore3.Text = string.Empty;
            txtScore4.Text = string.Empty;
            txtScore5.Text = string.Empty;
            txtScore6.Text = string.Empty;
            lblShowOption.Visible = false;

            txtOilPrice.Text = string.Empty;
            txtOilQty.Text = string.Empty;
            txtZeal.Text = string.Empty;
            txtZealQty.Text = string.Empty;

            rowOil.Visible = false;
            rowZeal.Visible = false;

            this.EnableScore(false);

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = true;
            }
            ((CheckBox)this.FindControlRecursive(dgvScore, "chkChooseScoreAll")).Checked = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void EnableScore(bool Enable)
    {
        txtScore1.Enabled = Enable;
        txtScore2.Enabled = Enable;
        txtScore3.Enabled = Enable;
        txtScore4.Enabled = Enable;
    }

    protected void cmdAddScore_Click(object sender, EventArgs e)
    {
        try
        {
            int CountCar = this.ValidateAddScore();

            string CostDisplay = string.Empty;
            string Score6Display = string.Empty;
            string DisableDriverDisplay = string.Empty;

            CostDisplay = txtCost.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString();
            Score6Display = txtScore6.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString();
            if (txtDisableDriver.Text.Trim() == "Hold")
            {
                DisableDriverDisplay = "Hold";
            }
            else
            {
                DisableDriverDisplay = txtDisableDriver.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString();
            }

            dtScoreList.Rows.Add(cboScore.SelectedValue, cboScore.SelectedItem, (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString(), CostDisplay, txtDisableDriver.Text.Trim() == "Hold" ? "-2" : (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString(), DisableDriverDisplay, txtScore1.Text.Trim(), txtScore2.Text.Trim(), txtScore3.Text.Trim(), txtScore4.Text.Trim(), txtScore5.Text.Trim(), (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString(), Score6Display, CountCar);

            CheckBox Chk;
            if (rowListTotalCarTab3.Visible)
            {//ป้อนจำนวนคัน
                TextBox Txt;

                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                    {
                        Txt = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, string.Empty, string.Empty, string.Empty, dtDup.Rows[i]["DELIVERY_DATE"].ToString(), Txt.Text.Trim());
                    }
                }
            }
            else
            {//เลือกทะเบียนรถ
                for (int i = 0; i < dgvScore.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                    {
                        DateTime dtt = new DateTime();
                        if (DateTime.TryParseExact(dtCarDistinct.Rows[i]["DELIVERY_DATE"] + string.Empty, DateTimeFormat, new CultureInfo("en-US"), DateTimeStyles.None, out dtt))
                        {

                        }
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtCarDistinct.Rows[i]["CARHEAD"].ToString(), dtCarDistinct.Rows[i]["CARDETAIL"].ToString(), dtt.ToString(DateFormat), 0);
                    }

                }
            }

            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList, false);
            this.ClearScreenTab3();
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void RefreshCusScore()
    {
        try
        {
            if (!string.Equals(radCusScoreType.SelectedValue, string.Empty))
            {
                int row = this.GetSelectRow(radCusScoreType.SelectedValue);

                txtPointFinal.Enabled = false;
                txtCostFinal.Enabled = false;
                txtDisableFinal.Enabled = false;

                lblShowSumPoint.Text = "(Sum : {0})";
                lblShowSumCost.Text = "(Sum : {0})";
                lblShowSumDisable.Text = "(Sum : {0})";

                lblShowSumPoint.Visible = false;
                lblShowSumCost.Visible = false;
                lblShowSumDisable.Visible = false;

                string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                switch (CusType)
                {
                    case "MAX": this.FindMax(); break;
                    case "SUM": this.FindSum("Normal"); break;
                    case "FREETEXT":
                        lblShowSumPoint.Visible = true;
                        lblShowSumCost.Visible = true;
                        lblShowSumDisable.Visible = true;
                        this.FreeText(); break;
                    default:
                        break;
                }
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void FreeText()
    {
        try
        {
            txtPointFinal.Text = string.Empty;
            txtCostFinal.Text = string.Empty;
            txtDisableFinal.Text = string.Empty;

            this.FindSum("ForFreeText");

            txtPointFinal.Enabled = true;
            txtCostFinal.Enabled = true;
            txtDisableFinal.Enabled = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FindMax()
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString()) > PointMax)
                    PointMax = decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            }
            txtPointFinal.Text = PointMax.ToString();

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString()) > CostMax)
                    CostMax = decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            }
            txtCostFinal.Text = CostMax.ToString();

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (dtScoreList.Rows[i]["DisableDriver"] + string.Empty == "-2")
                {
                    DisableDriverMax = -2;
                }
                else if (decimal.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString()) > DisableDriverMax)
                    DisableDriverMax = int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            }
            if (DisableDriverMax == -2)
            {
                txtDisableFinal.Text = "Hold";
                txtDisableFinal.ReadOnly = true;
            }
            else
            {
                txtDisableFinal.Text = DisableDriverMax.ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int ValidateAddScore()
    {
        try
        {
            if (cboScore.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก หัวข้อตัดคะแนน");

            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (string.Equals(dtScoreList.Rows[i]["STOPICID"].ToString(), cboScore.SelectedValue))
                    throw new Exception("เลือกหัวข้อ ตัดคะแนน ซ้ำกัน");
            }

            if (string.Equals(txtScore1.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ภาพลักษณ์องค์กร");

            if (string.Equals(txtScore2.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ความพึงพอใจลูกค้า");

            if (string.Equals(txtScore3.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน กฎ/ระเบียบฯ");

            if (string.Equals(txtScore4.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน แผนงานขนส่ง");

            if (rowOil.Visible)
            {
                if (string.Equals(txtOilQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนน้ำมัน");

                if (string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน ค่าน้ำมัน");
            }

            if (rowZeal.Visible)
            {
                if (string.Equals(txtZealQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนซิล");
            }

            if (rowListTotalCarTab3.Visible)
            {
                CheckBox Chk;
                TextBox Txt1;
                int MaxNumber = 0;
                int tmp;
                int TotalCar = 0;
                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");

                    if (Chk.Checked)
                    {
                        MaxNumber = int.Parse(dtDup.Rows[i]["TOTAL_CAR"].ToString());

                        Txt1 = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        if (string.Equals(Txt1.Text.Trim(), string.Empty))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน)");
                        if (!int.TryParse(Txt1.Text.Trim(), out tmp))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน) เป็นตัวเลข");
                        if (MaxNumber < int.Parse(Txt1.Text.Trim()))
                            throw new Exception("จำนวนรถที่ป้อน มีค่ามากกว่า จำนวนรถที่ถูกร้องเรียน");

                        TotalCar += int.Parse(Txt1.Text.Trim());
                    }
                }
                return TotalCar;
            }
            else
                return this.CountCar();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int CountCar()
    {
        try
        {
            int CountCar = 0;

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                if (Chk.Checked)
                {
                    CountCar += 1;
                }
            }

            if (CountCar == 0)
                throw new Exception("กรุณาเลือก ทะเบียนรถ ที่ต้องการตัดคะแนน");

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void radCusScoreType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void chkChooseScoreAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvScore_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dtTmp = dtScoreListCar.Copy();
            dtTmp.Rows.Clear();

            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (i != e.RowIndex)
                    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
            }
            dtScoreListCar = dtTmp;

            dtScoreList.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList, false);
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvScoreList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            IndexEdit = Index;

            if (string.Equals(e.CommandName, "Select"))
            {
                DataTable dtTmp = dtScoreList.Copy();

                string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };
                dtTmp.Columns["STOPICNAME"].ColumnName = "หัวข้อตัดคะแนน";
                dtTmp.Columns["Cost"].ColumnName = "ค่าปรับ";
                dtTmp.Columns["DisableDriver"].ColumnName = "ระงับ พขร. (วัน)";
                dtTmp.Columns["Score5"].ColumnName = "ผลคูณความรุนแรง";
                dtTmp.Columns["Score6"].ColumnName = "หักคะแนนต่อครั้ง";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                for (int i = 0; i < dtTmp.Columns.Count; i++)
                {
                    if (display.Contains(dtTmp.Columns[i].ColumnName))
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dtTmp.Columns[i].ColumnName + " : " + dtTmp.Rows[Index][i].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    if (i == dtTmp.Columns.Count - 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        if (rowListTotalCarTab3.Visible)
                        {
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        else
                        {
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private int GetCarListCount(string STOPICID)
    {
        try
        {
            int CountCar = 0;
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (rowListTotalCarTab3.Visible)
                        CountCar += int.Parse(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                    else
                        CountCar += 1;
                }
            }

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetCarListString(string STOPICID)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (sb.Length > 0)
                        sb.Append(", ");

                    if (rowListTotalCarTab3.Visible)
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                        sb.Append(" คัน");
                    }
                    else
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["CARHEAD"].ToString());
                        if (!string.Equals(dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), string.Empty))
                        {
                            sb.Append("/");
                            sb.Append(dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
                        }
                    }
                }
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkChooseScoreListCarAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }


    private void CheckPostBack()
    {
        try
        {
            var ctrlName = Request.Params[Page.postEventSourceID];
            var args = Request.Params[Page.postEventArgumentID];

            if (ctrlName == txtScore1.UniqueID && args == "txtScore1_OnKeyPress")
                txtScore1_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore2.UniqueID && args == "txtScore2_OnKeyPress")
                txtScore2_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore3.UniqueID && args == "txtScore3_OnKeyPress")
                txtScore3_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore4.UniqueID && args == "txtScore4_OnKeyPress")
                txtScore4_OnKeyPress(ctrlName, args);

            if (ctrlName == txtZealQty.UniqueID && args == "txtZealQty_OnKeyPress")
                txtZealQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilQty.UniqueID && args == "txtOilQty_OnKeyPress")
                txtOilQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilPrice.UniqueID && args == "txtOilPrice_OnKeyPress")
                txtOilPrice_OnKeyPress(ctrlName, args);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void txtScore1_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore2_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore3_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore4_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CalulateTotal()
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtScore1.Text.Trim(), out tmp) || (decimal.Parse(txtScore1.Text.Trim()) < 0))
                txtScore1.Text = "0";

            if (!decimal.TryParse(txtScore2.Text.Trim(), out tmp) || (decimal.Parse(txtScore2.Text.Trim()) < 0))
                txtScore2.Text = "0";

            if (!decimal.TryParse(txtScore3.Text.Trim(), out tmp) || (decimal.Parse(txtScore3.Text.Trim()) < 0))
                txtScore3.Text = "0";

            if (!decimal.TryParse(txtScore4.Text.Trim(), out tmp) || (decimal.Parse(txtScore4.Text.Trim()) < 0))
                txtScore4.Text = "0";

            int sum = 1;
            sum *= (txtScore1.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore1.Text.Trim());
            sum *= (txtScore2.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore2.Text.Trim());
            sum *= (txtScore3.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore3.Text.Trim());
            sum *= (txtScore4.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore4.Text.Trim());
            txtScore5.Text = sum.ToString();

            DataTable dt = ComplainBLL.Instance.SelectScorePointBLL(txtScore5.Text.Trim());
            if (dt.Rows.Count > 0)
                txtScore6.Text = dt.Rows[0]["NSCORE"].ToString();
            else
                txtScore6.Text = "0";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void txtZealQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            int tmp;
            if (!int.TryParse(txtZealQty.Text.Trim(), out tmp) || decimal.Parse(txtZealQty.Text.Trim()) < 0)
            {
                txtZealQty.Text = "0";
                txtCost.Text = "0";
                return;
            }

            txtZealQty.Text = int.Parse(txtZealQty.Text.Trim()).ToString();
            txtCost.Text = (int.Parse(txtZealQty.Text.Trim()) * int.Parse(txtZeal.Text.Trim())).ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtOilQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtOilQty.Text.Trim(), out tmp) || decimal.Parse(txtOilQty.Text.Trim()) < 0)
                txtOilQty.Text = "0";

            if (decimal.Parse(txtOilQty.Text.Trim()) > 0)
            {
                if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString(), "0") && decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
                else if (!string.Equals(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString(), "0") && (decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()) || decimal.Parse(txtOilQty.Text.Trim()) > decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString())))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
            }

            txtOilQty.Text = decimal.Parse(txtOilQty.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }


    private void txtOilPrice_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            txtOilPrice.ReadOnly = true;
            decimal tmp;
            if (!decimal.TryParse(txtOilPrice.Text.Trim(), out tmp) || decimal.Parse(txtOilPrice.Text.Trim()) < 0)
                txtOilPrice.Text = "0";

            txtOilPrice.Text = decimal.Parse(txtOilPrice.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
        finally
        {
            txtOilPrice.ReadOnly = false;
        }
    }

    private void CalculateOil()
    {
        try
        {
            if (!string.Equals(txtOilQty.Text.Trim(), string.Empty) && !string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                txtCost.Text = (int.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString()) * decimal.Parse(txtOilPrice.Text.Trim()) * decimal.Parse(txtOilQty.Text.Trim())).ToString();
            else
                txtCost.Text = "0";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void mpSaveTabCommittee_ClickOK(object sender, EventArgs e)
    {
        try
        {
            #region//< TabPage Name="t5" Text="คณะกรรมการพิจารณาฯ" >

            //int Index2 = int.Parse(e.Parameter.Split(';')[1]);

            string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

            //dynamic GridData = gvw.GetRowValues(VisibleIndex, "SAPPEALID", "DINCIDENT", "SVENDORID", "SVENDORNAME", "SPROCESSNAME", "STOPICNAME", "SHEADREGISTERNO", "STRAILREGISTERNO", "NPOINT", "STATUS", "SAPPEALNO", "SPROCESSID");
            string sSubject = "", sStep = "", sStatus = "", cStat = "", sHeaderLink = "", sDetail = "", sMode = "";
            using (OracleConnection con = new OracleConnection(sql))
            {
                con.Open();
                string strsql = "UPDATE TAPPEAL SET DSENTENCER = SYSDATE,CSTATUS = '3',SSENTENCERTEXT = :SSENTENCERTEXT,SSENTENCER = :SSENTENCER  WHERE  SAPPEALID = :SAPPEALID";
                using (OracleCommand com = new OracleCommand(strsql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = rblSSENTENCER2.SelectedValue + "";
                    com.Parameters.Add(":SSENTENCERTEXT", OracleType.VarChar).Value = txtSSENTENCERTEXT.Text;
                    com.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = hidSAPPEALID.Value;
                    com.ExecuteNonQuery();

                }

                string strSentence = "INSERT INTO TAPPEAL_SENTENCER (SAPPEALID, NSENTENCE, SSENTENCERID,  SSENTENCERCODE) VALUES (:SAPPEALID,:NSENTENCE,:SSENTENCERID,:SSENTENCERCODE)";
                int i = 1;
                foreach (ListItem items in cblSENTENCER.Items)
                {
                    if (items.Selected == true)
                    {
                        using (OracleCommand com1 = new OracleCommand(strSentence, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = Session["SAPPEALID"] + "";
                            com1.Parameters.Add(":NSENTENCE", OracleType.Number).Value = i;
                            com1.Parameters.Add(":SSENTENCERID", OracleType.VarChar).Value = items.Value;
                            com1.Parameters.Add(":SSENTENCERCODE", OracleType.VarChar).Value = "";
                            com1.ExecuteNonQuery();
                            i++;
                        }
                    }
                }
            }

            if ("" + rblSSENTENCER2.SelectedValue == "1")
            {//ไม่มีความผิด
                SetActiveReducePoint(false);
            }
            else if ("" + rblSSENTENCER2.SelectedValue == "2")
            {
                SetActiveReducePoint(true);
            }
            this.SendEmail(ConfigValue.EmailComplainAppealConfirm, gvData.Rows[0].Cells[4].Text);
            alertSuccess("บันทึกข้อมูลเรียบร้อย", "admin_suppliant_lst_New.aspx");
            #endregion
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void SendEmail(int TemplateID, string DocID)
    {
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailComplainAppealConfirm)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                    Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                    Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                    Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                    Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());
                    Body = Body.Replace("{WAREHOUSE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "vendor_appeal_lst.aspx";
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;
                    string CreateMail = string.Empty;

                    //MailService.SendMail("YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CreateMail, Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CreateMail, Subject, Body, "", "EmailComplainAppealConfirm", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailAccident8)
                {
                    #region EmailAccident8
                    string accID = DocID;
                    DataTable dt = AccidentBLL.Instance.AccidentTab1Select(accID);
                    DataRow dr = dt.Rows[0];
                    string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, false, true);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}

                    //EmailList += "bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com";

                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                    Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                    Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                    Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                    Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                    Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                    Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                    //Body = Body.Replace("{REPORT_CHECK}", IsApprove);
                    Body = Body.Replace("{REMARK}", "");
                    Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                    Body = Body.Replace("{USER_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);

                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "vendor_appeal_lst.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident8", ColumnEmailName);
                    #endregion
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }

    protected void SetActiveReducePoint(bool active)
    {
        string NREDUCEID = hidNREDUCEID.Value;
        string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
        if (active)
        {

            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();


                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '1' WHERE  NREDUCEID = '" + NREDUCEID + "'", con))
                {
                    comUpdate.ExecuteNonQuery();
                }

            }
        }
        else
        {
            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();


                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE  NREDUCEID = '" + NREDUCEID + "'", con))
                {
                    comUpdate.ExecuteNonQuery();
                }
                //fix by j for TREDUCEPOINT don't Update
                if (con.State == ConnectionState.Closed) con.Open();
                using (OracleCommand ComdUpdateAllTREDUCEPOINT = new OracleCommand(@"UPDATE TREDUCEPOINT SET CACTIVE='0'
        WHERE NREDUCEID IN ( 
        
        SELECT  DISTINCT
        (SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE REPLACE(RP.SREFERENCEID,'-','') = TC.NCONFIRMID /*AND RP.SPROCESSID = '010' */AND rownum <=1 AND rp.SHEADREGISTERNO=NVL(RDP.SHEADREGISTERNO,TL.SHEADREGISTERNO))  as NREDUCEID
        -- ,RDP.CACTIVE
        FROM TREDUCEPOINT RDP
        LEFT JOIN TTRUCKCONFIRM tc ON REPLACE(RDP.SREFERENCEID,'-','') = TC.NCONFIRMID
        LEFT JOIN TTRUCKCONFIRMLIST tl ON TL.NCONFIRMID = TC.NCONFIRMID AND NVL(RDP.SHEADREGISTERNO,'TRxxxxxx')=NVL(TL.SHEADREGISTERNO,'TRxxxxxx')
        LEFT JOIN (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM TTOPIC WHERE CACTIVE = '1' /*AND CPROCESS LIKE '%,010%'*/ ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp ON 1 = 1 
        LEFT JOIN TAPPEAL ap ON NVL(tl.NLISTNO,TC.NCONFIRMID) =  REPLACE(AP.SREFERENCEID,'-','') AND NVL(AP.SHEADREGISTERNO,'TRxxxxxx')=NVL(RDP.SHEADREGISTERNO,'TRxxxxxx') AND NVL(AP.STRAILREGISTERNO,'TRxxxxxx')=NVL(RDP.STRAILERREGISTERNO,'TRxxxxxx') 
        /*AND AP.SAPPEALTYPE='010'*/
        --LEFT JOIN TAPPEAL ap ON tl.NLISTNO = AP.SREFERENCEID AND AP.SAPPEALTYPE='010'
        LEFT JOIN TCONTRACT cc ON tc.SCONTRACTID = CC.SCONTRACTID 
        WHERE 1=1 
        AND NVL(TL.CCONFIRM,'0') = '0' 
        --AND RDP.SPROCESSID='010'
        --AND CC.SVENDORID = '0010001322'
         AND TO_DATE(tc.DDATE+1,'DD/MM/YYYY') BETWEEN TO_DATE('01/01/2013','DD/MM/YYYY') AND TO_DATE(TRUNC(SYSDATE),'DD/MM/YYYY') 
         AND  ap.CSTATUS = '3'  AND ap.SSENTENCER = '1' AND RDP.CACTIVE !='0'
         
        )", con))
                {
                    ComdUpdateAllTREDUCEPOINT.ExecuteNonQuery();
                }

            }
        }

    }
    protected void mpCancelTabCommittee_ClickOK(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvScoreListData_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void dgvScoreListData_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
}
