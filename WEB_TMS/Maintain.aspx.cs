﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using TMS_BLL.Master;

public partial class Maintain : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if(!IsPostBack)
		{
			CheckPermission();
			new ASPxGridViewCellMerger(Maintain_GridView);
			Maintain_GridView.DataBind();
		}
		else
		{
			new ASPxGridViewCellMerger(Maintain_GridView);
		}
	}

	private void CheckPermission()
	{
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true);
		Session["CheckPermission"] = "Report";
	}

	protected void Maintain_GridView_DataBinding(object sender, EventArgs e)
	{
		Maintain_GridView.DataSource = GetDataSourceFromGridView();
	}

	protected void Maintain_GridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
	{
		if(e.Parameters == "post")
		{
			//for(int i = 0; i < list.Count; i++)
			//{
			//	AccessDataSource1.UpdateParameters["CategoryName"].DefaultValue = list[i].CategoryName;
			//	AccessDataSource1.UpdateParameters["Description"].DefaultValue = list[i].Description;
			//	AccessDataSource1.UpdateParameters["CategoryID"].DefaultValue = list[i].Id.ToString();
			//	//                AccessDataSource1.Update();  << Uncomment this line to update data!
			//}
			//ASPxGridView1.DataBind();
		}
		if(e.Parameters == "STARTEDIT")
		{
			int index = ((ASPxGridView)sender).EditingRowVisibleIndex;
			string val = ((ASPxGridView)sender).GetRowValues(index, "M_AMOUNT").ToString();
			ASPxTextBox box = ((ASPxGridView)sender).FindEditFormTemplateControl("txt_M_AMOUNT") as ASPxTextBox;
			box.Text = val;
		}
	}

	protected void cmdSave_Click(object sender, EventArgs e)
	{
		DataTable dt = GetDataSourceFromGridView();
		GridViewDataColumn column1 = Maintain_GridView.Columns["M_AMOUNT"] as GridViewDataColumn;
		GridViewDataColumn column2 = Maintain_GridView.Columns["M_ALTERNATION"] as GridViewDataColumn;
		for(int i = 0; i < Maintain_GridView.VisibleRowCount; i++)
		{
			ASPxTextBox txtValue1 = Maintain_GridView.FindRowCellTemplateControl(i, column1, "txt_M_AMOUNT") as ASPxTextBox;
			ASPxTextBox txtValue2 = Maintain_GridView.FindRowCellTemplateControl(i, column2, "txt_M_ALTERNATION") as ASPxTextBox;
			decimal amount0 = Convert.ToDecimal(txtValue1.Text);
			decimal amount1 = Convert.ToDecimal(txtValue2.Text);
			DataRow dr = Maintain_GridView.GetDataRow(i);
			dr["M_AMOUNT"] = amount0;
			dr["M_ALTERNATION"] = amount1;
		}
		MaintainBLL.Instance.MaintainSave(dt, Session["UserID"].ToString());
		Session.Remove("Data");

		new ASPxGridViewCellMerger(Maintain_GridView);
		Maintain_GridView.DataBind();
	}

	protected void cmdCancel_Click(object sender, EventArgs e)
	{
		Session.Remove("Data");
	}

	private DataTable GetDataSourceFromGridView()
	{
		DataTable dt;
		if(Session["Data"] == null)
		{
			dt = MaintainBLL.Instance.MaintainSelect();

			for(int i = 0; i < dt.Rows.Count; i++)
			{
				if(dt.Rows[i]["MAINTAIN_ID"] == DBNull.Value)
				{
					dt.Rows[i]["MAINTAIN_ID"] = -i;
				}
				if(dt.Rows[i]["M_AMOUNT"] == DBNull.Value)
				{
					dt.Rows[i]["M_AMOUNT"] = 0;
				}
				if(dt.Rows[i]["M_ALTERNATION"] == DBNull.Value)
				{
					dt.Rows[i]["M_ALTERNATION"] = 0;
				}
			}

			#region GroupData for total //หน้า Maintain ไม่แสดง Total แล้ว
			//var groupedData = from b in dt.AsEnumerable()
			//				  group b by new
			//				  {
			//					  GRPID = b["GROUPID"] == DBNull.Value ? 0 : b["GROUPID"],
			//					  TERID = b["STERMINALID"] == DBNull.Value ? 0 : b["STERMINALID"]
			//				  } into grp
			//				  select new
			//				  {
			//					  GRPID = grp.Key.GRPID,
			//					  TERID = grp.Key.TERID,
			//					  Total = grp.Sum(r => r["M_AMOUNT"] == DBNull.Value ? 0 : Convert.ToDecimal(r["M_AMOUNT"]) )
			//				  };

			//if(groupedData != null && groupedData.Any())
			//{
			//	var x = groupedData.ToList();
			//	int cnt = dt.Rows.Count;
			//	foreach(var item in groupedData)
			//	{
			//		DataRow dr = dt.NewRow();
			//		dr.SetField<long>("GROUPID", Convert.ToInt64(item.GRPID));
			//		dr.SetField<long>("STERMINALID", Convert.ToInt64(item.TERID));
			//		dr.SetField<Decimal>("M_AMOUNT", Convert.ToDecimal(item.Total));
			//		dr.SetField<Decimal>("M_AMOUNT", Convert.ToDecimal(item.Total));
			//		dt.Rows.Add(dr);
			//	}
			//	DataTable dtTemp = dt.Copy();
			//	dt = dtTemp.AsEnumerable().OrderBy(r => r["GROUPID"] == DBNull.Value ? 0 : Convert.ToInt64(r["GROUPID"])).ThenBy(r => r["STERMINALID"] == DBNull.Value ? "0" : r["STERMINALID"].ToString()).CopyToDataTable();
			//}
			#endregion

			Session.Add("Data", dt);
		}
		else
		{
			dt = (DataTable)Session["Data"];
		}
		return dt;
	}

	protected void Grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
	{
		Maintain_GridView.DataSource = UpdateItem(e.Keys, e.NewValues);
		CancelEditing(e);
	}

	protected DataTable UpdateItem(OrderedDictionary keys, OrderedDictionary newValues)
	{
		var id = Convert.ToInt64(keys["MAINTAIN_ID"]);
		DataTable dt = GetDataSourceFromGridView();
		DataRow dr = dt.Select("MAINTAIN_ID = " + id)[0];
		if(newValues["M_AMOUNT"] == null)
		{
			dr["M_AMOUNT"] = DBNull.Value;
		}
		else
		{
			dr["M_AMOUNT"] = newValues["M_AMOUNT"];
		}
		if(newValues["M_ALTERNATION"] == null)
		{
			dr["M_ALTERNATION"] = DBNull.Value;
		}
		else
		{
			dr["M_ALTERNATION"] = newValues["M_ALTERNATION"];
		}
		//var item = GridData.First(i => i.ID == id);
		//LoadNewValues(item, newValues);
		dt.AcceptChanges();
		Session["Data"] = dt;
		return dt;
	}

	protected void CancelEditing(CancelEventArgs e)
	{
		e.Cancel = true;
		Maintain_GridView.CancelEdit();
	}

	#region ResponseMenu
	protected void ResponseMenu(object sender, EventArgs e)
	{
		//if(string.Equals(CGROUP.ToString(), "1"))
		//{
		//	Response.Redirect("truck_info.aspx");
		//}
		//else
		//{
		//	Response.Redirect("Vendor_Detail.aspx");
		//}
	}
	#endregion

	#region ASPxGridViewCellMerger
	public class ASPxGridViewCellMerger
	{
		ASPxGridView grid;
		Dictionary<GridViewDataColumn, TableCell> mergedCells = new Dictionary<GridViewDataColumn, TableCell>();
		Dictionary<TableCell, int> cellRowSpans = new Dictionary<TableCell, int>();

		public ASPxGridViewCellMerger(ASPxGridView grid)
		{
			this.grid = grid;
			Grid.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(grid_HtmlRowCreated);
			Grid.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
		}

		public ASPxGridView Grid
		{
			get
			{
				return grid;
			}
		}
		void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
		{
			//add the attribute that will be used to find which column the cell belongs to
			e.Cell.Attributes.Add("ci", e.DataColumn.VisibleIndex.ToString());

			if(cellRowSpans.ContainsKey(e.Cell))
			{
				e.Cell.RowSpan = cellRowSpans[e.Cell];
			}
		}
		void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			if(Grid.GetRowLevel(e.VisibleIndex) != Grid.GroupCount)
				return;
			for(int i = e.Row.Cells.Count - 1; i >= 0; i--)
			{
				DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell dataCell = e.Row.Cells[i] as DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell;
				if(dataCell != null)
				{
					if(dataCell.DataColumn.VisibleIndex < 3)
					{
						MergeCells(dataCell.DataColumn, e.VisibleIndex, dataCell);
					}
				}
			}
		}

		void MergeCells(GridViewDataColumn column, int visibleIndex, TableCell cell)
		{
			bool isNextTheSame = IsNextRowHasSameData(column, visibleIndex);
			if(isNextTheSame)
			{
				if(!mergedCells.ContainsKey(column))
				{
					mergedCells[column] = cell;
				}
			}
			if(IsPrevRowHasSameData(column, visibleIndex))
			{
				((TableRow)cell.Parent).Cells.Remove(cell);
				if(mergedCells.ContainsKey(column))
				{
					TableCell mergedCell = mergedCells[column];
					if(!cellRowSpans.ContainsKey(mergedCell))
					{
						cellRowSpans[mergedCell] = 1;
					}
					cellRowSpans[mergedCell] = cellRowSpans[mergedCell] + 1;
				}
			}
			if(!isNextTheSame)
			{
				mergedCells.Remove(column);
			}
		}
		bool IsNextRowHasSameData(GridViewDataColumn column, int visibleIndex)
		{
			//is it the last visible row
			if(visibleIndex >= Grid.VisibleRowCount - 1)
				return false;

			return IsSameData(column.FieldName, visibleIndex, visibleIndex + 1);
		}
		bool IsPrevRowHasSameData(GridViewDataColumn column, int visibleIndex)
		{
			ASPxGridView grid = column.Grid;
			//is it the first visible row
			if(visibleIndex <= Grid.VisibleStartIndex)
				return false;

			return IsSameData(column.FieldName, visibleIndex, visibleIndex - 1);
		}
		bool IsSameData(string fieldName, int visibleIndex1, int visibleIndex2)
		{
			// is it a group row?
			if(Grid.GetRowLevel(visibleIndex2) != Grid.GroupCount)
				return false;

			return object.Equals(Grid.GetRowValues(visibleIndex1, fieldName), Grid.GetRowValues(visibleIndex2, fieldName));
		}
	} 
	#endregion
}
