﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="UploadFileAdd.aspx.cs" Inherits="UploadFileAdd" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="เพิ่ม, แก้ไข ไฟล์อัพโหลด"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblSearchType" runat="server" Text="กลุ่มเอกสาร :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:DropDownList ID="ddlUploadType" runat="server" CssClass="form-control" Width="350px" AutoPostBack="true" OnSelectedIndexChanged="ddlUploadType_SelectedIndexChanged"></asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%;"></asp:TableCell>
                                            <asp:TableCell style="width:30%;"></asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="text-align:right; vertical-align:top">
                                                <asp:Label ID="Label1" runat="server" Text="ประเภทไฟล์อัพโหลด :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtUploadTypeName" runat="server" CssClass="form-control" Width="350px" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right; vertical-align:top">
                                                <asp:Label ID="Label2" runat="server" Text="นามสกุลที่รองรับ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="vertical-align:top">
                                                <asp:TextBox ID="txtEntention" runat="server" CssClass="form-control" Width="350px" TextMode="MultiLine" Height="70px"></asp:TextBox>
                                                <asp:Label ID="lblExtention" runat="server" Text="&nbsp;*.* , *.jpg , *.png&nbsp;&nbsp; เป็นต้น"></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="text-align:right">
                                                <asp:Label ID="Label3" runat="server" Text="ขนาดไฟล์สูงสุด (MB) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtMaxFileSize" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblVendorDownload" runat="server" Text="ผู้ขนส่งดาวน์โหลดไฟล์ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:RadioButtonList ID="radVendorDownload" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="อนุญาต"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="ไม่อนุญาต"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="Label5" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:RadioButtonList ID="radStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="Active &nbsp;"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:TableCell>                                            
                                        </asp:TableRow>
                                        <asp:TableRow>
                                             <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="Label4" runat="server" Text="ตรวจสอบวันหมดอายุ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:RadioButtonList ID="radCheckEndDate" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="ตรวจสอบ และแจ้งเตือน&nbsp;"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="ไม่ตรวจสอบ"></asp:ListItem>
                                                </asp:RadioButtonList>                                                
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lNoteCheck" runat="server" Text="ประเภทการแจ้งเตือนของเอกสารเมื่อเอกสารหมดอายุ:&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:RadioButtonList ID="radCheckAlertType" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="ระงับ พขร. &nbsp;"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="ไม่ระงับ พขร."></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:TableCell>
                                         </asp:TableRow>
                                        <asp:TableRow runat="server" ID="rowTPN2Date" Visible="false">
                                             <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="Label6" runat="server" Text="ส่ง ธพ.น.2 ภายในวันที่ (dd/mm) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtTPN2Date" runat="server" placeholder="dd/mm" CssClass="form-control"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                
                                            </asp:TableCell>
                                         </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdCancel_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <asp:HiddenField id="hGetUploadType" runat="server"/>

     <script type="text/javascript">
         $(document).ready(function () {

            SetChange();

            $('#cph_Main_radCheckEndDate input').click(function () {
                SetChange();
            });
         });

         function SetChange() {

             if ($("#cph_Main_radCheckEndDate_0").is(":checked")) {
                 //$('#divtest').show();
                 $("#cph_Main_radCheckAlertType input").removeAttr("disabled");
                 $('#cph_Main_radCheckAlertType label').removeAttr("style");
                 $('#cph_Main_lNoteCheck').removeAttr("style");
             }
             else {
                 //$('#cph_Main_radCheckAlertType input').hide();
                 $('#cph_Main_radCheckAlertType input').attr("disabled", true);
                 $('#cph_Main_radCheckAlertType input').prop('checked', false);
                 $('#cph_Main_radCheckAlertType label').css({ "color": "gray" });
                 $('#cph_Main_lNoteCheck').css({ "color": "gray" });                 
             }
         }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>