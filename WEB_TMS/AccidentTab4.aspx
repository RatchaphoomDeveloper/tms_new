﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AccidentTab4.aspx.cs" Inherits="AccidentTab4" UICulture="en-US" Culture="en-US" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">

    <asp:Panel runat="server" ID="plTab3">
        <div class="form-horizontal">
            <ul class="nav nav-tabs" id="tabtest">
                <li class="" id="liTab1" runat="server"><a href="#TabGeneral" runat="server" id="GeneralTab1">แจ้งเรื่องอุบัติเหตุ</a></li>
                <li class="" id="liTab2" runat="server"><a href="#TabGeneral2" runat="server" id="GeneralTab2">รายงานเบื้องต้น</a></li>
                <li class="" id="liTab3" visible="false" runat="server"><a href="#TabGeneral3" runat="server" id="GeneralTab3">รายงานวิเคราะห์สาเหตุ</a></li>
                <li class="active" id="liTab4" runat="server"><a href="#TabGeneral4" data-toggle="tab" aria-expanded="true" runat="server" id="GeneralTab4">รายงานผลการพิจารณา</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="TabGeneral">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse8" id="acollapse8">ความเสียหาย(ประเมินโดยคณะกรรมการสอบสวนอุบัติเหตุร้ายแรง)</a>
                            <asp:HiddenField runat="server" ID="hidcollapse8" />
                        </div>
                        <div id="collapse8" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group ">
                                    <div class="col-md-12">
                                        <h5>
                                            <asp:Label Text="" ID="lblType" runat="server" /></h5>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">ข้อมูลการเกิดอุบัตเหตุ</a><asp:HiddenField runat="server" ID="hidcollapse1" />
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="plCollapse1" Enabled="false">

                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ชื่อเรื่องของอุบัติเหตุ (Accident ID)</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAccidentID" CssClass="form-control" Text="Generate by System" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">วันที่-เวลาเกิดเหตุ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAccidentDate" CssClass="form-control datetimepicker" />
                                        </div>
                                        <div class="col-md-4"></div>
                                        <label class="col-md-2 text-center">
                                            ระยะเวลาตั้งแต่เกิดเหตุ<br />
                                            (Day-hr-min.)</label>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">วันที่-เวลาที่แจ้งเรื่องในระบบ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtAccidentDateSystem" CssClass="form-control datetimepicker" />
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-2 text-center">
                                            <asp:Label Text="0-00-00" ID="lblAccidentDateSystem" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row form-group divhide" id="divREPORTER">
                                        <label class="col-md-3 control-label">ผขส. แจ้งเหตุให้ ปตท.</label>
                                        <div class="col-md-3">
                                            <div class="col-md-4 PaddingLeftRight0">
                                                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblREPORTER">
                                                    <asp:ListItem Text="&nbsp;No&nbsp;&nbsp;" Value="0" />
                                                    <asp:ListItem Text="&nbsp;Yes" Value="1" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="col-md-8 PaddingLeftRight0">
                                                <asp:TextBox runat="server" ID="txtAccidentDatePtt" CssClass="form-control datetimepicker" />
                                            </div>
                                        </div>
                                        <label class="col-md-2 control-label">ชื่อผู้แจ้ง(ผู้ขนส่ง)</label>
                                        <div class="col-md-2">
                                            <asp:TextBox runat="server" ID="txtAccidentName" CssClass="form-control" />
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <asp:Label Text="0-00-00" runat="server" ID="lblAccidentDatePtt" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">วันที่-เวลาจัดทำรายงานอุบัติเหตุเบื้องต้น</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" CssClass="form-control datetimepicker" ID="txtDatePrimary"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-2 text-center">
                                            <asp:Label Text="0-00-00" runat="server" ID="lblDatePrimary" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ผขส. ส่งรายงานวิเคราะห์สาเหตุ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" CssClass="form-control datetimepicker" ID="txtDateAnalysis"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-2 text-center">
                                            <asp:Label Text="0-00-00" runat="server" ID="lblDateAnalysis" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">วันที่-เวลา เจ้าหน้าที่ ปง.อนุมัติเอกสาร</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" CssClass="form-control datetimepicker" ID="txtApproveDate"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-2 text-center">
                                            <asp:Label Text="0-00-00" runat="server" ID="lblApproveDate" />
                                        </div>
                                    </div>

                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info divhide">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse10" id="acollapse10">สรุปเหตุการณ์รถขนส่งเกิดอุบัติเหตุโดย&nbsp;ปตท.</a><asp:HiddenField runat="server" ID="hidcollapse10" />
                        </div>
                        <div id="collapse10" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group">
                                    <label class="col-md-2 control-label">ประมาณการความเสียหาย (บาท)</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control number" ID="txtDAMAGE" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    <label class="col-md-3 control-label">ปริมาณน้ำมันเสียหายหรือสูญหายเนื่องจากอุบัติเหตุ(ลิตร)</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control number" ID="txtOil" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-2 control-label">1.ชีวิต(พขร.)</label>
                                    <div class="col-md-3 control-label text-left">
                                        <asp:TextBox runat="server" CssClass="form-control number" ID="txtLIFE" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    <label class="col-md-1 control-label" style="text-align:left">
                                        <asp:Label Text="({0})" runat="server" ID="lblLIFE" /></label>
                                    <label class="col-md-2 control-label">2.ทรัพย์สินเสียหาย</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control number" ID="txtPROPERTY" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    <label class="col-md-1 control-label" style="text-align:left"><asp:Label Text="({0})" runat="server" ID="lblPROPERTY" /></label>
                                </div>
                                <div class="row form-group">
                                    <label class="col-md-2 control-label">3.สิ่งแวดล้อม</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control number" ID="txtENVIRONMENT" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    <label class="col-md-1 control-label" style="text-align:left"><asp:Label Text="({0})" runat="server" ID="lblENVIRONMENT" /></label>
                                    <label class="col-md-2 control-label">4.ภาพลักษณ์องค์กร</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control number" ID="txtCORPORATE" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    <label class="col-md-1 control-label" style="text-align:left"><asp:Label Text="({0})" runat="server" ID="lblCORPORATE" /></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <asp:Label ID="lblCusScoreHeader" runat="server" Text="การตัดคะแนน (เลขที่สัญญา {0})"></asp:Label>
                                </div>
                                <div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <div class="panel-body">
                                            <asp:Table ID="Table111" Width="100%" runat="server">
                                                <asp:TableRow ID="Row1" runat="server">
                                                    <asp:TableCell Width="15%">&nbsp;</asp:TableCell><asp:TableCell Width="25%" ColumnSpan="2">
                                                        <asp:CheckBox ID="chkSaveTab3Draft" runat="server" Text="รับเรื่องรอพิจารณา" ForeColor="Red" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow ID="Row2" runat="server">
                                                    <asp:TableCell Width="15%">
                                                        <div id="divTopic" runat="server">
                                                            หัวข้อตัดคะแนนประเมิน<font color="#ff0000">*</font>
                                                        </div>
                                                    </asp:TableCell><asp:TableCell Width="25%" ColumnSpan="4">
                                                        <asp:DropDownList ID="cboScore" runat="server" class="form-control" DataTextField="STOPICNAME"
                                                            OnSelectedIndexChanged="cboScore_SelectedIndexChanged" DataValueField="STOPICID"
                                                            AutoPostBack="true" Width="100%">
                                                        </asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow ID="Row3" runat="server">
                                                    <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell ColumnSpan="4">
                                                        <asp:Label ID="lblShowOption" runat="server" Text="[พขร. ทุจริต]" Visible="false"
                                                            ForeColor="Red"></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow ID="Row4" runat="server">
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblScore1" runat="server" Text="ชีวิต(พขร.)"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore1" runat="server" Width="200px"
                                                            Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore1_OnKeyPress');" />
                                                    </asp:TableCell><asp:TableCell
                                                        Width="20%">&nbsp;</asp:TableCell><asp:TableCell Width="15%">
                                                            <asp:Label ID="lblScore2" runat="server" Text="ทรัพย์สินเสียหาย"></asp:Label>
                                                        </asp:TableCell><asp:TableCell
                                                            Width="25%">
                                                            <asp:TextBox CssClass="form-control" ID="txtScore2" runat="server" Width="200px"
                                                                Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore2_OnKeyPress');" />
                                                        </asp:TableCell></asp:TableRow><asp:TableRow ID="Row5" runat="server">
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblScore3" runat="server" Text="สิ่งแวดล้อม"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore3" runat="server" Width="200px"
                                                            Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore3_OnKeyPress');" />
                                                    </asp:TableCell><asp:TableCell
                                                        Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                            <asp:Label ID="lblScore4" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label>
                                                        </asp:TableCell><asp:TableCell>
                                                            <asp:TextBox CssClass="form-control" ID="txtScore4" runat="server" Width="200px"
                                                                Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore4_OnKeyPress');" />
                                                        </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow ID="rowListCarTab3" Visible="false">
                                                    <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px; vertical-align: top;">
                                                        <asp:GridView ID="dgvScore" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                            OnCheckedChanged="chkChooseScoreAll_CheckedChanged" />
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Width="36px" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="TRUCKID" Visible="false">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CARHEAD" HeaderText="ทะเบียนรถ (หัว)">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CARDETAIL" HeaderText="ทะเบียนรถ (หาง)">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                        </asp:GridView>
                                                    </asp:TableCell></asp:TableRow><asp:TableRow ID="rowListTotalCarTab3" runat="server" Visible="false">
                                                    <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px; vertical-align: top;">
                                                        <asp:GridView ID="dgvScoreTotalCar" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                            OnCheckedChanged="chkChooseScoreListCarAll_CheckedChanged" />
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Width="36px" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="TOTAL_CAR" HeaderText="จำนวนรถ (ตามเอกสาร)">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="จำนวนรถ (หักคะแนน)">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtChooseTotalCar" runat="server" Width="120px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                        </asp:GridView>
                                                    </asp:TableCell></asp:TableRow><asp:TableRow ID="rowTotalCarTab3" Visible="false">
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label12" runat="server" Text="จำนวนรถ (คัน)"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox ID="txtTotalCarTab3" runat="server" CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow runat="server" ID="rowOil" Visible="false">
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label10" runat="server" Text="ค่าน้ำมัน / ลิตร"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox ID="txtOilPrice" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilPrice_OnKeyPress');"
                                                            CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                    </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>
                                                        <asp:Label ID="Label11" runat="server" Text="จำนวนน้ำมัน (ลิตร)"></asp:Label>
                                                    </asp:TableCell><asp:TableCell
                                                        Width="20%">
                                                        <asp:TextBox ID="txtOilQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilQty_OnKeyPress');"
                                                            CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                    </asp:TableCell></asp:TableRow><asp:TableRow runat="server" ID="rowZeal" Visible="false">
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblZeal1" runat="server" Text="ค่าปรับต่อ 1 ซิล"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox ID="txtZeal" runat="server" Enabled="false" CssClass="form-control"
                                                            Style="text-align: center"></asp:TextBox>
                                                    </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>
                                                        <asp:Label ID="Label9" runat="server" Text="จำนวน ซิล"></asp:Label>
                                                    </asp:TableCell><asp:TableCell
                                                        Width="20%">
                                                        <asp:TextBox ID="txtZealQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtZealQty_OnKeyPress');"
                                                            CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow ID="Row6" runat="server">
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblCost" runat="server" Text="ค่าปรับ"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtCost" runat="server" Width="200px" ReadOnly="true"
                                                            Enabled="false" Style="text-align: center" />
                                                    </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                        <asp:Label ID="lblDisableDriverDay" runat="server" Text="ระงับ พชร. (วัน)"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtDisableDriver" runat="server" Width="200px"
                                                            Enabled="false" Style="text-align: center" />
                                                    </asp:TableCell></asp:TableRow><asp:TableRow ID="Row7" runat="server">
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblScore5" runat="server" Text="ผลคูณความรุนแรง"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore5" runat="server" Width="200px"
                                                            Enabled="false" Style="text-align: center" />
                                                    </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                        <asp:Label ID="lblScore6" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore6" runat="server" Width="200px"
                                                            Enabled="false" Style="text-align: center" />
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell ColumnSpan="5">
                                                        <asp:GridView ID="dgvScoreList" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                            OnRowDeleting="dgvScore_RowDeleting" OnRowCommand="dgvScoreList_RowCommand" AlternatingRowStyle-BackColor="White"
                                                            RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:BoundField DataField="STOPICID" Visible="false">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="STOPICNAME" HeaderText="หัวข้อตัดคะแนน">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle Width="430px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Score6Display" HeaderText="หักคะแนนต่อครั้ง">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle Wrap="false" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CostDisplay" HeaderText="ค่าปรับ(บาท)">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle Wrap="false" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="DisableDriverDisplay" HeaderText="ระงับ พขร.(วัน)">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle Wrap="false" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Action">
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgViewScore" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                            Width="23px" Height="23px" Style="cursor: pointer" CommandName="Select" />&nbsp;
                                                            <asp:ImageButton ID="imgDeleteScore" runat="server" ImageUrl="~/Images/bin1.png"
                                                                Width="23px" Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                        </asp:GridView>
                                                    </asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                                    <asp:Button ID="cmdAddScore" CssClass="btn btn-md btn-hover btn-info" Style="width: 100px"
                                        UseSubmitBehavior="false" runat="server" Text="เพิ่มรายการ" OnClick="cmdAddScore_Click" />
                                    <br />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblAttachFileTab3" runat="server" Text="แนบเอกสารพิจารณาโทษ - อื่นๆ"></asp:Label></div><div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="Table5" runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell Width="35%"> ประเภทไฟล์เอกสาร </asp:TableCell><asp:TableCell>
                                                <asp:DropDownList ID="cboUploadTypeTab3" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                                    Width="350px" DataValueField="UPLOAD_ID">
                                                </asp:DropDownList>
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell Width="35%">
                                                <asp:FileUpload ID="fileUploadTab3" runat="server" Width="100%" />
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Button ID="cmdUploadTab3" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                                    UseSubmitBehavior="false" OnClick="cmdUploadTab3_Click" Style="width: 100px" />
                                            </asp:TableCell></asp:TableRow></asp:Table><br /><asp:GridView ID="dgvUploadFileTab3" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" DataKeyNames="UPLOAD_ID,FULLPATH"
                                        OnRowDeleting="dgvUploadFileTab3_RowDeleting" OnRowUpdating="dgvUploadFileTab3_RowUpdating"
                                        OnRowDataBound="dgvUploadFileTab3_RowDataBound">
                                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                            <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                            <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                            <asp:BoundField DataField="FULLPATH" Visible="false" />
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                        Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                        Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    </asp:GridView>
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info" id="divSecond" runat="server" visible="false">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>ข้อมูลการอุทธรณ์ </div><div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <div class="panel-body">
                                            <asp:Table ID="Table1" Width="100%" runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblComplainDate" runat="server" Text="พิจารณาแล้ววันที่"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox ID="txtComplainDate" runat="server" CssClass="form-control" Width="240px"
                                                            Style="text-align: center" Enabled="false"></asp:TextBox>
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblSecondDate" runat="server" Text="อุทธรณ์ได้ภายในวันที่"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox ID="txtSecondDate" runat="server" CssClass="form-control" Width="240px"
                                                            Style="text-align: center" Enabled="false"></asp:TextBox>
                                                    </asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                                    <br />
                                </div>
                            </div>
                            <br />
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info" id="divScore" runat="server">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>สรุปผลการพิจารณา </div><div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <div class="panel-body">
                                            <asp:Table runat="server" Width="100%">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <div class="panel panel-info" id="div1" runat="server">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-table"></i>ก่อนอุทธรณ์
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="dataTable_wrapper">
                                                                    <div class="panel-body">
                                                                        <asp:Table runat="server" Width="100%">
                                                                            <asp:TableRow ID="Row8" runat="server">
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblSentencerType" runat="server" Text="พิจารณาโดย"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:RadioButtonList ID="radSentencerType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                                        OnSelectedIndexChanged="radSentencerType_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0" Text="ทีมงาน รข."></asp:ListItem>
                                                                                        <asp:ListItem Value="1" Text="คทง. พิจารณาโทษฯ"></asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow ID="Row9" runat="server">
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblSentencer" runat="server" Text="คณะกรรมการพิจารณาโทษ"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Table runat="server">
                                                                                        <asp:TableRow>
                                                                                            <asp:TableCell>
                                                                                                <asp:DropDownList ID="ddlSentencer" runat="server" class="form-control" Width="350px" Enabled="false"></asp:DropDownList>
                                                                                            </asp:TableCell>
                                                                                            <asp:TableCell>
                                                                                                &nbsp;&nbsp;<asp:LinkButton ID="lblSentencerDetail" runat="server" Text="รายชื่อคณะทำงาน" OnClick="lblSentencerDetail_Click"></asp:LinkButton>
                                                                                            </asp:TableCell>
                                                                                        </asp:TableRow>
                                                                                    </asp:Table>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:CheckBox ID="chkBlacklist" runat="server" Text="&nbsp;Blacklist" ForeColor="Red" Enabled="false" Visible="false" />
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Width="30%">
                                                                                    <asp:Label ID="lblCusScoreType" runat="server" Text="เงื่อนไขการตัดคะแนน"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="50%">
                                                                                    <asp:RadioButtonList ID="radCusScoreType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                                        OnSelectedIndexChanged="radCusScoreType_SelectedIndexChanged">
                                                                                    </asp:RadioButtonList>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="20%">&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="Label13" runat="server" Text="หักคะแนน"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtPointFinal" runat="server" Width="200px" Enabled="false" Style="text-align: center"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblShowSumPoint" Visible="false" runat="server" Text="(ผลรวม : {0})"></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="Label14" runat="server" Text="ค่าปรับ"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtCostFinal" runat="server" Width="200px" Enabled="false" Style="text-align: center"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblShowSumCost" Visible="false" runat="server" Text="(ผลรวม : {0})"></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblCostOther" runat="server" Text="ค่าเสียหายและหรือค่าใช้จ่าย"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtCostOther" runat="server" Width="200px" Style="text-align: center"> </asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="Label84" runat="server" Text="ระงับ พขร. (วัน)"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtDisableFinal" runat="server" Width="200px" Enabled="false" Style="text-align: center"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblShowSumDisable" Visible="false" runat="server" Text="(ผลรวม : {0})"></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblOilLose" runat="server" Text="ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร)"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtOilLose" runat="server" Width="200px" Style="text-align: center"> </asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblRemarkTab3" runat="server" Text="หมายเหตุ"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="100%" ColumnSpan="2">
                                                                                    <asp:TextBox CssClass="form-control" ID="txtRemarkTab3" runat="server" Width="100%" Height="100px"> </asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>
                                                        <div class="panel panel-info" id="div2" runat="server">
                                                            <div class="panel-heading">
                                                                <i class="fa fa-table"></i>หลังอุทธรณ์
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="dataTable_wrapper">
                                                                    <div class="panel-body">
                                                                        <asp:Table runat="server" Width="100%">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:CheckBox ID="chkBlacklistFinal" runat="server" Text="&nbsp;Blacklist" ForeColor="Red" Enabled="false" Visible="false" />
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Width="30%">
                                                                                    <asp:Label ID="lblAppealPoint" runat="server" Text="หักคะแนน" Visible="false"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="50%">
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealPoint" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Width="20%">&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblAppealCost" runat="server" Text="ค่าปรับ" Visible="false"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealCost" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblAppealCostOther" runat="server" Text="ค่าเสียหายและหรือค่าใช้จ่าย" Visible="false"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealCostOther" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblAppealDisable" runat="server" Text="ระงับ พขร. (วัน)" Visible="false"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealDisable" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblAppealOilLose" runat="server" Text="ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร)" Visible="false"></asp:Label>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealOilLose" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>&nbsp;</asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:TableCell></asp:TableRow></asp:Table><asp:Table runat="server" ID="tblScoreFinal" Width="100%" Visible="false">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label3" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:DropDownList ID="ddlScore1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore1_SelectedIndexChanged"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore1Final" runat="server" Width="200px"
                                                            ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore1Final_OnKeyPress');" />
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label4" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:DropDownList ID="ddlScore2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore2_SelectedIndexChanged"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore2Final" runat="server" Width="200px"
                                                            ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore2Final_OnKeyPress');" />
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label5" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:DropDownList ID="ddlScore3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore3_SelectedIndexChanged"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore3Final" runat="server" Width="200px"
                                                            ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore3Final_OnKeyPress');" />
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label6" runat="server" Text="แผนงานขนส่ง"></asp:Label>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:DropDownList ID="ddlScore4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore4_SelectedIndexChanged"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore4Final" runat="server" Width="200px"
                                                            ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore4Final_OnKeyPress');" />
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label7" runat="server" Text="ผลคูณความรุนแรง"></asp:Label>
                                                    </asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore5Final" runat="server" Width="200px"
                                                            ReadOnly="true" Style="text-align: center" />
                                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Label ID="Label8" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label>
                                                    </asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell>
                                                        <asp:TextBox CssClass="form-control" ID="txtScore6Final" runat="server" Width="200px"
                                                            ReadOnly="true" Style="text-align: center" />
                                                    </asp:TableCell><asp:TableCell Style="padding-left: 5px"> </asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                                    <asp:Button ID="cmdShowAppeal" runat="server" Text="ข้อมูลการยื่นอุทธรณ์" CssClass="btn btn-md btn-hover btn-info"
                                        UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdShowAppeal_Click" Visible="false" />
                                    &nbsp; <asp:Button ID="cmdSaveTab3" runat="server" Text="บันทึกและแจ้งผล" CssClass="btn btn-md btn-hover btn-info"
                                        UseSubmitBehavior="false" Style="" data-toggle="modal" data-target="#ModalConfirmSaveTab3" />
                                    &nbsp; <asp:Button ID="cmdSaveTab3Draft" runat="server" Text="บันทึก (ร่าง)" CssClass="btn btn-md btn-hover btn-warning"
                                        UseSubmitBehavior="false" OnClick="cmdSaveTab3Draft_Click" Style="width: 100px" />
                                    &nbsp; <asp:Button ID="cmdChangeStatus" runat="server" Text="ขอเอกสารเพิ่มเติม" CssClass="btn btn-md btn-hover btn-info"
                                        UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdChangeStatus_Click" Visible="false" />
                                    &nbsp; <asp:Button ID="cmdCloseTab3" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info"
                                        UseSubmitBehavior="false" OnClick="cmdCloseTab2_Click" Style="width: 100px" />
                                    <br />
                                </div>
                            </div>
                            <br />
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>--%>
                    <div class="panel panel-info" id="divCost" runat="server">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ค่าปรับ + ค่าเสียหายและหรือค่าใช้จ่าย </div><div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="Table10" runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell Style="width: 30%">
                                                <asp:Label ID="lblTotalCostAll" runat="server" Text="ค่าปรับ + ค่าเสียหายและหรือค่าใช้จ่าย"></asp:Label>
                                            </asp:TableCell><asp:TableCell
                                                Style="width: 40%">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>
                                                        <asp:RadioButtonList ID="radCostCheck" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="1" Text="ชำระครบถ้วน"> </asp:ListItem>
                                                            <asp:ListItem Value="2" Text="ชำระแล้วบางส่วน"> </asp:ListItem>
                                                            <asp:ListItem Value="0" Selected="True" Text="ยังไม่ได้ชำระ"> </asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblCostCheckDate" runat="server" Text="วันที่ชำระครบถ้วน "></asp:Label>
                                            </asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="txtCostCheckDate" runat="server" CssClass="datepicker" Style="text-align: center" > </asp:TextBox>
                                            </asp:TableCell></asp:TableRow></asp:Table><br /><asp:Table ID="Table12" runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell Width="30%">
                                                <asp:Label ID="Label1" runat="server" Text="ประเภทไฟล์เอกสาร"></asp:Label>
                                            </asp:TableCell><asp:TableCell>
                                                <asp:DropDownList ID="cboUploadCost" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                                    Width="350px" DataValueField="UPLOAD_ID">
                                                </asp:DropDownList>
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell Width="30%">
                                                <asp:FileUpload ID="fileUploadCost" runat="server" Width="100%" />
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Button ID="cmdUploadCost" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                                    UseSubmitBehavior="false" OnClick="cmdUploadCost_Click" Style="width: 100px" />
                                            </asp:TableCell></asp:TableRow></asp:Table><asp:GridView ID="dgvUploadCost" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" DataKeyNames="UPLOAD_ID,FULLPATH"
                                        OnRowDeleting="dgvUploadCost_RowDeleting" OnRowUpdating="dgvUploadCost_RowUpdating"
                                        OnRowDataBound="dgvUploadCost_RowDataBound">
                                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="ครั้งที่">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                            <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                            <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                            <asp:BoundField DataField="FULLPATH" Visible="false" />
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                        Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                        Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    </asp:GridView>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                        <ContentTemplate>
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse9" id="acollapse9">รายละเอียดและจำนวนเงิน</a> <asp:HiddenField runat="server" ID="hidcollapse9" />
                                                </div>
                                                <div id="collapse9" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="row form-group ">
                                                            <label class="col-md-4 control-label">จำวนวนเงินทั้งหมดที่ต้องชำระ(บาท)</label> <div class="col-md-4">
                                                                <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" />
                                                            </div>
                                                            <label class="col-md-4 ">ค่าปรับ + ค่าเสียหายและหรือค่าใช้จ่าย</label> </div><div class="panel panel-info">
                                                            <div class=" panel-body">
                                                                <div class="row form-group">
                                                                    <label class="col-md-1 control-label">ครั้งที่</label> <div class="col-md-1">
                                                                        <asp:TextBox runat="server" ID="txtNo" CssClass="form-control" />
                                                                    </div>
                                                                    <label class="col-md-2 control-label">ชื่อไฟล์(ตามผู้ใช้งาน)</label> <div class="col-md-3">

                                                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ddlFileName">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <label class="col-md-2 control-label">จำนวนเงิน(บาท)</label> <div class="col-md-2">
                                                                        <asp:TextBox runat="server" ID="txtAmountAdd" CssClass="form-control" />
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <asp:Button Text="เพิ่ม" runat="server" CssClass="btn btn-md btn-hover btn-info" ID="btnFineAdd" OnClick="btnFineAdd_Click" />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-12">
                                                                        <asp:GridView runat="server" ID="gvFine"
                                                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowUpdating="gvFine_RowUpdating"
                                                                            AllowPaging="True">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="NO" HeaderText="ครั้งที่" ItemStyle-HorizontalAlign="Center" />
                                                                                <asp:BoundField DataField="FILENAMES" HeaderText="ชื่อไฟล์(ตามผู้ใช้งาน)" />
                                                                                <asp:BoundField DataField="AMOUNT" HeaderText="จำนวนเงิน(บาท)" ItemStyle-HorizontalAlign="Right" />

                                                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="hidNO" runat="server" Value='<%#Eval("NO") %>' />
                                                                                        <asp:HiddenField ID="hidFILENAMES" runat="server" Value='<%#Eval("FILENAMES") %>' />
                                                                                        <asp:HiddenField ID="hidAMOUNT" runat="server" Value='<%#Eval("AMOUNT") %>' />
                                                                                        <div class="col-sm-1">
                                                                                            <asp:LinkButton ID="lnkChooseTERMINAL" runat="server" Text="ลบ" CommandName="update"></asp:LinkButton></ItemTemplate></asp:TemplateField></Columns><PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                                                            <PagerStyle CssClass="pagination-ys" />
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSaveIsCost" runat="server" Text="บันทึกค่าปรับ" Width="120px"
                                class="btn btn-md btn-hover btn-info" OnClick="cmdSaveIsCost_Click" />
                            <br />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </asp:Panel>



    <div class="modal fade" id="ShowWithAutoPostBack" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal3" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="cmdClose" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblModalTitle3" runat="server" Text=""></asp:Label></h4></div><div class="modal-body">
                            <asp:TextBox ID="txtEmailTab3" runat="server" Width="99%" TextMode="MultiLine" Height="100px"></asp:TextBox></div><div class="modal-footer">
                            <asp:Button ID="cmdChangeStatusConfirm" runat="server" Text="ยืนยันข้อมูล" CssClass="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdChangeStatusConfirm_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveTab3" IDModel="ModalConfirmSaveTab3"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab3_Click"
        TextTitle="บันทึกตัดคะแนน" TextDetail="ยืนยันผลการพิจารณากรณีรถขนส่งเกิดอุบัติเหตุ ?" />
    <asp:HiddenField ID="hidID" runat="server" />
    <asp:HiddenField ID="hidCactive" runat="server" />
    <asp:HiddenField ID="hidCGROUP" runat="server" />
    <asp:HiddenField ID="hidSVENDORID" runat="server" />
    <asp:HiddenField ID="hidPERS_CODE" runat="server" />
    <asp:HiddenField ID="hidSEMPLOYEEID" runat="server" />
    <asp:HiddenField ID="hidSEMPLOYEEID2" runat="server" />
    <asp:HiddenField ID="hidPERS_CODE2" runat="server" />
    <asp:HiddenField ID="hidSEMPLOYEEID3" runat="server" />
    <asp:HiddenField ID="hidPERS_CODE3" runat="server" />
    <asp:HiddenField ID="hidSEMPLOYEEID4" runat="server" />
    <asp:HiddenField ID="hidPERS_CODE4" runat="server" />
    <asp:HiddenField ID="hidApproveDate" runat="server" />
    <script type="text/javascript">
        function SetEnabledControlByID(id) {
            if ($('#<%=hidCGROUP.ClientID%>').val() == '0') {
                $('.divhide').addClass("hide");
            }
            if (id == '6') {
                $('#btnApprove,#btnEdit').prop('disabled', false);
            }
            else if (id == '8') {
                $('#btnApprove,#btnEdit').prop('disabled', true);
            }
            else {
                $('#btnApprove,#btnEdit').prop('disabled', true);
            }
            var last = $('#<%= hidcollapse1.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse1").removeClass('in');
            }
            <%--last = $('#<%= hidcollapse2.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse2").removeClass('in');
            }
            last = $('#<%= hidcollapse3.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse3").removeClass('in');
            }
            last = $('#<%= hidcollapse4.ClientID %>').val();
             if (last.indexOf("in") > -1) {
                 $("#collapse4").removeClass('in');
             }
             last = $('#<%= hidcollapse5.ClientID %>').val();
             if (last.indexOf("in") > -1) {
                 $("#collapse5").removeClass('in');
             }
             last = $('#<%= hidcollapse6.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse6").removeClass('in');
            }
            last = $('#<%= hidcollapse7.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse7").removeClass('in');
            }
            last = $('#<%= hidcollapse8.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse8").removeClass('in');
            }--%>
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        $(document).ready(function () {

            fcollapse();
        });


        function EndRequestHandler(sender, args) {
            fcollapse();
        }
        function fcollapse() {
            $("#acollapse1").on('click', function () {
                var active = $("#collapse1").attr('class');
                //console.log(active);
                $('#<%= hidcollapse1.ClientID %>').val(active);
            });
            <%--$("#acollapse2").on('click', function () {
                var active = $("#collapse2").attr('class');
                $('#<%= hidcollapse2.ClientID %>').val(active);
            });
            $("#acollapse3").on('click', function () {
                var active = $("#collapse3").attr('class');
                $('#<%= hidcollapse3.ClientID %>').val(active);
            });
            $("#acollapse4").on('click', function () {
                var active = $("#collapse4").attr('class');
                $('#<%= hidcollapse4.ClientID %>').val(active);
            });
             $("#acollapse5").on('click', function () {
                 var active = $("#collapse5").attr('class');
                 //console.log(active);
                 $('#<%= hidcollapse5.ClientID %>').val(active);
             });
             $("#acollapse6").on('click', function () {
                 var active = $("#collapse6").attr('class');
                 $('#<%= hidcollapse6.ClientID %>').val(active);
            });
            $("#acollapse7").on('click', function () {
                var active = $("#collapse7").attr('class');
                $('#<%= hidcollapse7.ClientID %>').val(active);
            });
            $("#acollapse8").on('click', function () {
                var active = $("#collapse8").attr('class');
                $('#<%= hidcollapse8.ClientID %>').val(active);
            });--%>
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

