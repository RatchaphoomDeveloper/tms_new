﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;

public partial class transportation : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler
        gvw3.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw3.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw3_HtmlDataCellPrepared);
        #endregion
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            var dt = new List<BDT1>();
            Session["bdt1"] = dt;

            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            Cache.Remove(sds3.CacheKeyDependency);
            Cache[sds3.CacheKeyDependency] = new object();
            Session["DT_SHIPTO"] = "";
            Get_SHIPTO();
            
            this.AssignAuthen();
        }

        LogUser("12", "R", "เปิดดูข้อมูลหน้า สรุปแผนการขนส่งสินค้า", "");
    }


    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {               
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        BindData();

    }

    //กด แสดงเลข Record
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            //case "Search":
            //    Cache.Remove(sds3.CacheKeyDependency);
            //    Cache[sds3.CacheKeyDependency] = new object();

            //    BindData();


            //    break;

            case "Save":

                if (CanWrite)
                {
                    iTerminal itm = new iTerminal();
                    object NPLANID = gvw3.GetRowValues(int.Parse(paras[1]), "NPLANID");
                    object NPLANLISTID = gvw3.GetRowValues(int.Parse(paras[1]), "SPLANLISTID");
                    string STIMEWINDOW = gvw3.GetRowValues(int.Parse(paras[1]), "STIMEWINDOW") + "";
                    string STERMINALID = gvw3.GetRowValues(int.Parse(paras[1]), "STERMINALID") + "";
                    string DDELIVERY = gvw3.GetRowValues(int.Parse(paras[1]), "DDELIVERY") + "";
                    string CFIFO = gvw3.GetRowValues(int.Parse(paras[1]), "CFIFO") + "";


                    int NPlanID = Convert.ToInt32(NPLANID);
                    string PLAN_OLD = GetDOFROMPLAN(NPlanID + "");


                    string genidnPlanID = CommonFunction.Gen_ID(sql, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                    ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

                    ASPxComboBox cboHeadRegist = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxComboBox cboTrailerRegist = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboTrailerRegist");

                    ASPxComboBox cmbPersonalNo = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cmbPersonalNo");
                    ASPxTextBox txtRemark = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtRemark");

                    ASPxTextBox txtDrop = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDrop");
                    ASPxComboBox cboDelivery = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboDelivery");
                    ASPxTextBox txtShipto = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShipto");
                    ASPxTextBox txtValue = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtValue");

                    if (rblCheck.SelectedIndex == 0)
                    {
                        string OUTBOUNDCAPACITY = CommonFunction.Get_Value(sql, "SELECT SUM(nvl(PL.NVALUE,0)) as SUMVALUE FROM TPlanScheduleList pl WHERE pl.CACTIVE = '1' AND PL.NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID + "") + "' AND PL.SPLANLISTID != '" + CommonFunction.ReplaceInjection(NPLANLISTID + "") + "'");

                        string SCAPACITY;

                        string ValueTruck;

                        if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                        {
                            ValueTruck = cboTrailerRegist.Value + "";
                        }
                        else
                        {
                            ValueTruck = cboHeadRegist.Value + "";
                        }

                        SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + CommonFunction.ReplaceInjection(ValueTruck) + "' GROUP BY  T.NTOTALCAPACITY");

                        int num = 0;
                        int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                        int SUMCAPACITY = int.TryParse(OUTBOUNDCAPACITY, out num) ? num : 0;
                        int nValue1 = int.TryParse(txtValue.Text, out num) ? num : 0;

                        SUMCAPACITY = SUMCAPACITY + nValue1;

                        if ((NCAPACITY + 100) < SUMCAPACITY)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                            return;

                        }

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strsql = "UPDATE TPlanSchedule SET SVENDORID = :SVENDORID,SEMPLOYEEID = :SEMPLOYEEID,SREMARK = :SREMARK,DPLAN = sysdate +1,SHEADREGISTERNO = :SHEADREGISTERNO, STRAILERREGISTERNO = :STRAILERREGISTERNO, DUPDATE = sysdate, SUPDATE = :SUPDATE WHERE NPLANID = :NPLANID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                string venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + CommonFunction.ReplaceInjection(cboHeadRegist.Value.ToString()) + "') ");

                                com.Parameters.Clear();
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                com.ExecuteNonQuery();

                            }

                            if (NPLANLISTID + "" != "")
                            {
                                int PlanListID = Convert.ToInt32(NPLANLISTID);
                                int num1;
                                int num2;
                                string strsql1 = "UPDATE TPlanScheduleList SET NDROP = :NDROP, SDELIVERYNO = :SDELIVERYNO, SSHIPTO = :SSHIPTO, NVALUE = :NVALUE WHERE SPLANLISTID = :SPLANLISTID";
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NDROP", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                    com1.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                    com1.Parameters.Add(":SSHIPTO", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                    com1.Parameters.Add(":NVALUE", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                    com1.Parameters.Add(":SPLANLISTID", OracleType.Number).Value = PlanListID;
                                    com1.ExecuteNonQuery();
                                    PLAN_OLD = PLAN_OLD.Length > 0 ? PLAN_OLD.Remove(0, 1) : "";
                                    itm.DeleteMAP(PLAN_OLD, STIMEWINDOW, cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID, cmbPersonalNo.Value + "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);

                                }
                            }
                            else
                            {
                                //ถ้าไม่มี ID sPlanListID ให้ทำการ Insert ข้อมูล ลง TPLANSCHDULELIST
                                int num1;
                                int num2;

                                string genidsPlanListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPlanScheduleList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                string strsql1 = "INSERT INTO TPlanScheduleList(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,nValue,cActive) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,:nValue,'1')";

                                NPLANLISTID = genidsPlanListID;

                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {

                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genidsPlanListID);
                                    com1.Parameters.Add(":nPlanID", OracleType.Number).Value = NPlanID;
                                    com1.Parameters.Add(":nDrop", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                    com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                    com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = cboDelivery.Value + "";
                                    com1.Parameters.Add(":nValue", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                    com1.ExecuteNonQuery();

                                }
                            }

                            #region Webservices iterminal
                            DateTime dTemp;
                            DateTime dateplan = DateTime.TryParse(DDELIVERY, out dTemp) ? dTemp : DateTime.Now;
                            string PLAN_NEW = GetDOFROMPLAN(NPlanID + "");
                            PLAN_NEW = PLAN_NEW.Length > 0 ? PLAN_NEW.Remove(0, 1) : "";
                            itm.UpdateMAP(PLAN_NEW, STIMEWINDOW, cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID, cmbPersonalNo.Value + "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", dateplan);
                            #endregion
                        }
                    }
                    else if (rblCheck.SelectedIndex == 1)
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();


                            string sValueDelivery = CommonFunction.Get_Value(con, "SELECT wm_concat(SDELIVERYNO) AS SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + NPlanID + "'");
                            itm.DeleteMAP(sValueDelivery, STIMEWINDOW + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID + "", "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);


                            string strsql = "UPDATE TPlanSchedule SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                com.ExecuteNonQuery();
                            }

                            string strsql1 = "UPDATE TPlanScheduleList SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                            using (OracleCommand com1 = new OracleCommand(strsql1, con))
                            {
                                com1.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                com1.ExecuteNonQuery();
                            }
                        }
                    }
                    else if (rblCheck.SelectedIndex == 2)
                    {
                        var bdt2 = (List<BDT1>)Session["bdt1"];

                        string SCAPACITY;

                        string ValueTruck;

                        if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                        {
                            ValueTruck = cboTrailerRegist.Value + "";
                        }
                        else
                        {
                            ValueTruck = cboHeadRegist.Value + "";
                        }

                        SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + CommonFunction.ReplaceInjection(ValueTruck) + "' GROUP BY  T.NTOTALCAPACITY");

                        int num = 0;
                        int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                        double SUMCAPACITY = bdt2.Select(o => o.dtValue).Sum();

                        if ((NCAPACITY + 100) < SUMCAPACITY)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                            return;

                        }

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();


                            string sValueDelivery = CommonFunction.Get_Value(con, "SELECT wm_concat(SDELIVERYNO) AS SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + NPlanID + "'");

                            itm.DeleteMAP(sValueDelivery, STIMEWINDOW + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID + "", "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);


                            string strsql = "UPDATE TPlanSchedule SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                com.ExecuteNonQuery();
                            }

                            string strsql1 = "UPDATE TPlanScheduleList SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                            using (OracleCommand com1 = new OracleCommand(strsql1, con))
                            {
                                com1.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                com1.ExecuteNonQuery();
                            }

                            DataTable dt = new DataTable();
                            dt = CommonFunction.Get_Data(con, "SELECT nvl(CFIFO,'0') AS CFIFO,STERMINALID,SPLANDATE,SPLANTIME,DDELIVERY,STIMEWINDOW from TPLANSCHEDULE WHERE /*CACTIVE = '1' AND*/ NPLANID = " + CommonFunction.ReplaceInjection(NPlanID + ""));

                            string strsql2 = "INSERT INTO TPLANSCHEDULE(SEMPLOYEEID,SREMARK,SVENDORID,nPlanID,nNo,CFIFO,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:SEMPLOYEEID,:SREMARK,:SVENDORID,:nPlanID,1,:CFIFO,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate)";
                            string strsql3 = "INSERT INTO TPlanScheduleList(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,nValue) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:nValue)";


                            if (dt.Rows.Count > 0)
                            {
                                //ข็อมูล
                                using (OracleCommand com2 = new OracleCommand(strsql2, con))
                                {

                                    string venderid = "";

                                    venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + CommonFunction.ReplaceInjection(cboHeadRegist.Value + "") + "')");

                                    com2.Parameters.Clear();
                                    com2.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value;
                                    com2.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                    com2.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                                    com2.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                    com2.Parameters.Add(":CFIFO", OracleType.Char).Value = Convert.ToChar(dt.Rows[0]["CFIFO"] + "");
                                    com2.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = dt.Rows[0]["STERMINALID"] + "";
                                    com2.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now.AddDays(1);
                                    com2.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = Convert.ToDateTime(dt.Rows[0]["SPLANDATE"]);
                                    com2.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = dt.Rows[0]["SPLANTIME"] + "";
                                    com2.Parameters.Add(":dDelivery", OracleType.DateTime).Value = Convert.ToDateTime(dt.Rows[0]["DDELIVERY"]);
                                    com2.Parameters.Add(":sTimeWindow", OracleType.Number).Value = Convert.ToInt32(dt.Rows[0]["STIMEWINDOW"]);
                                    com2.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                    com2.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                    com2.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com2.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com2.ExecuteNonQuery();
                                }

                                string _DL = "";

                                foreach (var dr in bdt2)
                                {
                                    string genListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPlanScheduleList ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");

                                    using (OracleCommand com3 = new OracleCommand(strsql3, con))
                                    {

                                        com3.Parameters.Clear();
                                        com3.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genListID);
                                        com3.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                        com3.Parameters.Add(":nDrop", OracleType.Number).Value = dr.dtDrop;
                                        com3.Parameters.Add(":sShipTo", OracleType.VarChar).Value = dr.dtShipto;
                                        com3.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = dr.dtDeliveryNo;
                                        com3.Parameters.Add(":nValue", OracleType.Number).Value = dr.dtValue;
                                        com3.ExecuteNonQuery();

                                        _DL += "," + dr.dtDeliveryNo;
                                    }

                                }

                                #region Webservices iterminal

                                DateTime dTemp;
                                DateTime dPlan1 = DateTime.TryParse(dt.Rows[0]["SPLANDATE"] + "", out dTemp) ? dTemp : DateTime.Now;
                                itm.UpdateMAP(((_DL.Length > 0) ? _DL.Remove(0, 1) : ""), dt.Rows[0]["STIMEWINDOW"] + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", dt.Rows[0]["STERMINALID"] + "", cmbPersonalNo.Value + "", genidnPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", dPlan1);
                                #endregion

                            }

                        }
                    }
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");

                    var dtt = new List<BDT1>();
                    Session["bdt1"] = dtt;

                    gvw3.CancelEdit();

                    Cache.Remove(sds3.CacheKeyDependency);
                    Cache[sds3.CacheKeyDependency] = new object();

                    BindData();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                break;

            case "EditClick":

                var ggg = new List<BDT1>();
                Session["bdt1"] = ggg;
                int index = int.Parse(paras[1]);

                gvw3.StartEdit(index);


                ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");
                ASPxComboBox cboTrailerRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboTrailerRegist");
                ASPxComboBox cboDelivery12 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboDelivery");

                object sHeadRegist = gvw3.GetRowValues(index, "SHEADREGISTERNO");
                object sTrailerRegist = gvw3.GetRowValues(index, "STRAILERREGISTERNO");
                object cType = gvw3.GetRowValues(index, "CFIFO");
                object sDELIVERYNO = gvw3.GetRowValues(index, "SDELIVERYNO");

                txtCTYPE.Text = cType + "";
                cboHeadRegist1.Value = sHeadRegist + "";
                cboTrailerRegist1.Value = sTrailerRegist + "";

                cboDelivery_OnItemRequestedByValueSQL(cboDelivery12, new ListEditItemRequestedByValueEventArgs(sDELIVERYNO + ""));

                //if (cboDelivery12.Items.FindByValue(sDELIVERYNO + "")+"" == sDELIVERYNO + "")
                //{
                cboDelivery12.Value = sDELIVERYNO + "";
                //}
                //else
                //{
                //    cboDelivery12.Text = sDELIVERYNO + "";
                //}

                object SSHIPTO = gvw3.GetRowValues(index, "SSHIPTO");
                ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShiptoName");
                ASPxTextBox txtShipto_Set = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShipto");

                if (string.IsNullOrEmpty(SSHIPTO + ""))
                {
                    dynamic SDELIVERY = gvw3.GetRowValues(index, "SDELIVERYNO");
                    DataTable dt_SHIPTO = Session["DT_SHIPTO"] as DataTable;
                    if (dt_SHIPTO.Rows.Count > 0)
                    {
                        DataView DV = new DataView(dt_SHIPTO);
                        DV.RowFilter = "DELIVERY_NO = '" + SDELIVERY + "'";
                        if (DV.Count > 0)
                        {
                            SSHIPTO = DV[0]["SSHIPTO"] + "";
                        }
                    }
                    txtShipto_Set.Text = SSHIPTO + "";
                }


                string sSSHIPTO = CommonFunction.Get_Value(sql, "SELECT C.CUST_NAME FROM TCUSTOMER_SAP c WHERE C.SHIP_TO = '" + CommonFunction.ReplaceInjection(SSHIPTO + "") + "'");
                txtShiptoName1.Text = sSSHIPTO;

                ASPxComboBox cboPersonalNo1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cmbPersonalNo");
                object SEMPLOYEE = gvw3.GetRowValues(index, "SEMPLOYEEID");

                cmbPersonalNo_OnItemRequestedByValueSQL(cboPersonalNo1, new ListEditItemRequestedByValueEventArgs(SEMPLOYEE + ""));
                cboPersonalNo1.Value = SEMPLOYEE + "";

                SetVisibleControl();
                break;


            case "AddDataToList":
                int indexx = gvw3.EditingRowVisibleIndex;
                string sTimeWindow1 = gvw3.GetRowValues(indexx, "STIMEWINDOW") + "";
                string sDDELIVERY = gvw3.GetRowValues(indexx, "DDELIVERY") + "";
                string sSTIMEWINDOW = gvw3.GetRowValues(indexx, "STIMEWINDOW") + "";
                ASPxComboBox cboHeadRegist11 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");

                ASPxTextBox txtDrop1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDrop");
                ASPxComboBox cboDelivery1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboDelivery");
                ASPxTextBox txtShipto1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShipto");
                ASPxTextBox txtValue1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtValue");
                ASPxGridView sgvw1 = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
                ASPxTextBox txtShiptoName11 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShiptoName");

                var bdt2s = (List<BDT1>)Session["bdt1"];


                int nums = 0;
                int InDrop = Int32.TryParse(txtDrop1.Text, out nums) ? nums : 0;

                if (InDrop > 10)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่ม Drop No. เกินกว่า 10 Drop ได้');");
                    SetVisibleControl();
                    return;
                }

                double InParse = 0;
                string outbounds = CommonFunction.ReplaceInjection(cboDelivery1.Value + "");
                var ddd = bdt2s.FirstOrDefault(s => s.dtDeliveryNo == outbounds);
                if (ddd.dtDeliveryNo != null)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในรายการแล้ว');");
                    SetVisibleControl();
                    return;
                }


                string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPlanScheduleList WHERE CACTIVE = '1' AND SDELIVERYNO = '" + outbounds + "'");
                int CountOutbound = Int32.TryParse(CountValue, out nums) ? nums : 0;

                if (CountOutbound > 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในระบบแล้ว');");
                    SetVisibleControl();
                    return;
                }

                int tempParse = 0;
                int nTimeWindow = Int32.TryParse(sTimeWindow1, out tempParse) ? tempParse : 0;

                var CheckCarList = bdt2s.FirstOrDefault(s => s.sTimeWindow == nTimeWindow && s.dtDrop == InDrop);


                if (CheckCarList.dtDeliveryNo != null)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Drop No. " + InDrop + " เที่ยวที่ " + nTimeWindow + " นี้ ในรายการแล้ว ');");
                    SetVisibleControl();
                    return;
                }

                DateTime Date1;
                DateTime csDDELIVERY = DateTime.TryParse(sDDELIVERY, out Date1) ? Date1 : DateTime.Now;
                int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPlanScheduleList pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + CommonFunction.ReplaceInjection(cboHeadRegist11.Value + "") + "' AND P.STIMEWINDOW = '" + CommonFunction.ReplaceInjection(nTimeWindow + "") + "' AND To_Date(P.DDELIVERY,'dd/MM/yyyy') = To_Date('" + CommonFunction.ReplaceInjection(csDDELIVERY.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','dd/MM/yyyy') AND p.STERMINALID ='" + CommonFunction.ReplaceInjection(sSTIMEWINDOW) + "' AND pl.NDROP = " + CommonFunction.ReplaceInjection(InDrop + ""));

                if (CheckCar > 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + cboHeadRegist11.Value + " เที่ยวที่ :" + nTimeWindow + " วันที่จัดส่ง : " + csDDELIVERY.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + " คลังต้นทาง : " + sSTIMEWINDOW + " Drop No. : " + InDrop + " อยู่ในระบบแล้ว');");
                    SetVisibleControl();
                    return;
                }

                bdt2s.Add(new BDT1
                {
                    dtDrop = InDrop,
                    dtDeliveryNo = cboDelivery1.Value + "",
                    dtShipto = txtShipto1.Text,
                    dtValue = double.TryParse(txtValue1.Text, out InParse) ? InParse : 0,
                    sTimeWindow = nTimeWindow,
                    dtShiptoName = txtShiptoName11.Text

                });

                sgvw1.DataSource = bdt2s;
                sgvw1.DataBind();

                Session["bdt1"] = bdt2s;

                SetVisibleControl();
                break;

            case "DelClick":
                ASPxGridView sgvw2 = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
                string IndexValue = paras[1];
                var bdt3 = (List<BDT1>)Session["bdt1"];

                bdt3.RemoveAt(Convert.ToInt32(IndexValue));

                sgvw2.DataSource = bdt3;
                sgvw2.DataBind();

                Session["bdt1"] = bdt3;
                break;

            case "Cancel":

                gvw3.CancelEdit();
                var dt11 = new List<BDT1>();
                Session["bdt1"] = dt11;

                break;

        }
    }

    protected void cboHeadRegisTFIFOPLAN_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxTextBox VendorID = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtVendorID");

        if (VendorID != null)
        {
            string Terminal = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "STERMINALID") + "";
            string DPLAN = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "SPLANDATE") + "";
            DateTime date;
            DateTime date1 = DateTime.TryParse(DPLAN + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;


            if (("" + txtCTYPE.Text).Equals("0"))
            {
                sdsTruck.SelectCommand = @"SELECT STRANSPORTID, SHEADREGISTERNO,STRAILERREGISTERNO, '' AS DWATEREXPIRE,SVENDORNAME,NCAPACITY 
FROM(SELECT T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,CASE WHEN Tc.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = Tc.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.SHEADREGISTERNO) AS RN 
FROM  ((((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON TC.SHEADID = T.STRUCKID) 
LEFT JOIN TVENDOR_SAP v ON T.STRANSPORTID = V.SVENDORID)
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID    WHERE nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1' AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE(:DPLAN,'dd/MM/yyyy') -1 AND nvl(ct.STERMINALID,:TERMINAL) LIKE :TERMINAL  GROUP BY  T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }
            else
            {
                sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRANSPORTID,SVENDORNAME,'' AS DWATEREXPIRE,NCAPACITY FROM( 

SELECT f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID AS STRANSPORTID,V.SVENDORNAME,
  CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN
  
 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) END AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.SHEADREGISTERNO) AS RN 
FROM  ((TFIFO f  LEFT JOIN TVENDOR_SAP v ON F.SVENDORID  = V.SVENDORID) LEFT JOIN TTRUCK t ON F.SHEADREGISTERNO = T.SHEADREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID
WHERE NVL(F.CACTIVE,'1') = '1' AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID ,V.SVENDORNAME,T.NTOTALCAPACITY  ) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", CommonFunction.ReplaceInjection(e.Filter)));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", CommonFunction.ReplaceInjection(Terminal + "")));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboHeadRegisTFIFOPLAN_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboTrailerRegisTFIFOPLAN_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxTextBox VendorID = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtVendorID");

        if (VendorID != null)
        {
            string Terminal = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "STERMINALID") + "";
            string DPLAN = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "SPLANDATE") + "";

            DateTime date;
            DateTime date1 = DateTime.TryParse(DPLAN + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;

            if (("" + txtCTYPE.Text).Equals("0"))
            {
                sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY 
FROM(SELECT Tc.STRAILERREGISTERNO, nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.STRAILERREGISTERNO) AS RN 
FROM  (((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON T.SHEADREGISTERNO =  Tc.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE Tc.STRAILERREGISTERNO IS NOT NULL AND  nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1' AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE(:DPLAN,'dd/mm/yyyy') -1 AND nvl(ct.STERMINALID,:TERMINAL) LIKE :TERMINAL AND ct.SVENDORID = :SVENDORID  GROUP BY Tc.STRAILERREGISTERNO,T.NTOTALCAPACITY ) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }
            else
            {
                sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY FROM( 

SELECT f.STRAILERREGISTERNO , nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0)))  AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.STRAILERREGISTERNO) AS RN 
FROM  (TFIFO f   LEFT JOIN TTRUCK t ON t.SHEADREGISTERNO = f.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID 
WHERE NVL(F.CACTIVE,'1') = '1' AND  f.STRAILERREGISTERNO IS NOT NULL AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL AND SVENDORID = :SVENDORID  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", CommonFunction.ReplaceInjection(e.Filter)));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", CommonFunction.ReplaceInjection(Terminal + "")));
            sdsTruck.SelectParameters.Add("SVENDORID", TypeCode.String, CommonFunction.ReplaceInjection(VendorID.Text + ""));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboTrailerRegisTFIFOPLAN_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }


    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID,SPERSONELNO,FULLNAME,STEL,SPERSONELNO FROM (SELECT E.SEMPLOYEEID,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,E.SPERSONELNO, ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND E.INAME || ES.FNAME || ' ' || ES.LNAME || E.SPERSONELNO LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", CommonFunction.ReplaceInjection(e.Filter)));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT E.SEMPLOYEEID,E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE  E.SEMPLOYEEID = :fillter ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, CommonFunction.ReplaceInjection(e.Value + ""));

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();
    }

    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {


        ASPxComboBox comboBox = (ASPxComboBox)source;
//        sdsDelivery.SelectCommand = @"SELECT DELIVERY_NO,SHIP_TO,SCUSTOMER,NVALUE FROM(
//        SELECT d.DELIVERY_NO,d.SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
//        ,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
//        , ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN 
//        FROM TDELIVERY d  
//        LEFT JOIN TCUSTOMER_SAP ct ON D.SHIP_TO = CT.SHIP_TO  
//        WHERE  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-3 AND trunc(d.DELIVERY_DATE) <= TRUNC(SYSDATE)+3 AND d.DELIVERY_NO = :fillter 
//        ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsDelivery.SelectCommand = @"SELECT DELIVERY_NO,SHIP_TO,SCUSTOMER,NVALUE FROM(
SELECT d.DELIVERY_NO,(CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') ELSE NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
, ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN 
FROM TDELIVERY d  
LEFT JOIN 
(
    SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
)TRN
ON TRN.DOC_NO = d.SALES_ORDER
LEFT JOIN 
(
    SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
)TBO
ON TBO.DOCNO = d.SALES_ORDER
LEFT JOIN TCUSTOMER_SAP ct ON CT.SHIP_TO  =  (CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') ELSE NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END)
WHERE  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-7 AND trunc(d.DELIVERY_DATE) <= TRUNC(SYSDATE)+7 AND d.DELIVERY_NO = :fillter 
) WHERE RN BETWEEN :startIndex AND :endIndex ";
        string SFILTER = "";
        if (!string.IsNullOrEmpty(e.Filter))
        {
            SFILTER = e.Filter.PadLeft(10, '0');
        }
        else
        {
            SFILTER = comboBox.Text;
        }

        sdsDelivery.SelectParameters.Clear();
        sdsDelivery.SelectParameters.Add("fillter", TypeCode.String, String.Format("{0}", SFILTER));
        sdsDelivery.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsDelivery.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsDelivery;
        comboBox.DataBind();

        if (comboBox.Items.Count <= 0)
        {
            comboBox.Value = "";
        }

    }

    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    [Serializable]
    struct BDT1
    {
        public int dtDrop { get; set; }
        public string dtDeliveryNo { get; set; }
        public string dtShipto { get; set; }
        public double dtValue { get; set; }
        public int sTimeWindow { get; set; }
        public string dtShiptoName { get; set; }
    }

    void SetVisibleControl()
    {
        ASPxGridView sgvw = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
        ASPxButton btnsAdd = (ASPxButton)gvw3.FindEditFormTemplateControl("btnsAdd");
        ASPxButton btnSave = (ASPxButton)gvw3.FindEditFormTemplateControl("btnSave");
        ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

        if (rblCheck.SelectedIndex == 2)
        {
            sgvw.ClientVisible = true;
            btnsAdd.ClientVisible = true;
        }
        else
        {
            sgvw.ClientVisible = false;
            btnsAdd.ClientVisible = false;
        }

        if (!CanWrite)
        {
           btnsAdd.Enabled = false;
           btnSave.Enabled = false;
        }
    }

    void BindData()
    {
        Get_SHIPTO();

        string datetype;
        string datetype_Orderplan;
        if (CommonFunction.ReplaceInjection("" + cmbDate.Value) == "1")
        {
            datetype = "P.SPLANDATE";
            datetype_Orderplan = "ODP.DATE_CREATE";
        }
        else
        {
            datetype = "P.DDELIVERY";
            datetype_Orderplan = "ODP.DDELIVERY";
        }


        string status = "";
        string statusOrderPlan = "";
        if (CommonFunction.ReplaceInjection("" + cmbStatus.Value) == "2")
        {
            status = "and p.CCONFIRM is null";
            statusOrderPlan = " and TPC.CCONFIRM is null";
        }
        else if (CommonFunction.ReplaceInjection("" + cmbStatus.Value) == "1")
        {
            status = "and p.CCONFIRM = '1'";
            statusOrderPlan = " and TPC.CCONFIRM = '1'";
        }
        else if (CommonFunction.ReplaceInjection("" + cmbStatus.Value) == "0")
        {
            status = "and p.CCONFIRM = '0'";
            statusOrderPlan = " and TPC.CCONFIRM = '0'";
        }

        //        string strsql22 = @"SELECT ROW_NUMBER () OVER (ORDER BY P.CFIFO, P.SPLANDATE) AS ID1,CASE WHEN nvl(P.CFIFO,'0')  = '0' THEN 'ประจำวัน'  ELSE 'FIFO' END AS STYPE,nvl(P.CFIFO,0) AS CFIFO, P.NPLANID,P.NNO,P.SPLANDATE,P.SPLANTIME,TS.STERMINALNAME,P.STERMINALID,P.DDELIVERY,P.STIMEWINDOW,PL.NDROP,
        //PL.SDELIVERYNO,PL.SSHIPTO,P.SHEADREGISTERNO,P.STRAILERREGISTERNO,PL.NVALUE,PL.SPLANLISTID,P.SREMARK,P.SEMPLOYEEID,CASE WHEN p.CCONFIRM = '1' THEN 'action_check.png' ELSE CASE WHEN p.CCONFIRM = '0' THEN 'action_delete.png' ELSE '05.png' END END AS SIMAGE , 
        //CASE WHEN o. SHIPMENT_NO IS NULL THEN 'unload1.png' ELSE 'load1.png' END AS SSHIPMENT
        //FROM (((TPlanSchedule p LEFT JOIN TPlanScheduleList pl ON P.NPLANID = PL.NPLANID) LEFT JOIN TTERMINAL t ON P.STERMINALID = T.STERMINALID) 
        //LEFT JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID) LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o ON PL.SDELIVERYNO = o.DELIVERY_NO
        //       WHERE 1=1 AND  p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.STERMINALID LIKE '%' || CASE WHEN SUBSTR('' || :oTml,1,5) = '80000' THEN '' ELSE '' ||  :oTml END || '%' AND P.STERMINALID LIKE '%' || :oTerminal || '%' AND (p.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR p.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR PL.SDELIVERYNO LIKE '%' || :oSearch || '%' OR PL.SSHIPTO LIKE '%' || :oSearch || '%' ) AND (" + CommonFunction.ReplaceInjection(datetype) + " BETWEEN TO_DATE(:oBegin,'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(:oEnd,'dd/MM/yyyy HH24:MI:SS')) " + CommonFunction.ReplaceInjection(status);

        //        string strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY MS.CFIFO, MS.SPLANDATE) AS ID1,MS.* FROM
        //(
        //SELECT CASE WHEN nvl(P.CFIFO,'0')  = '0' THEN 'ประจำวัน'  ELSE 'FIFO' END AS STYPE,nvl(P.CFIFO,0) AS CFIFO, P.NPLANID,P.NNO,P.SPLANDATE,P.SPLANTIME,TS.STERMINALNAME,P.STERMINALID,P.DDELIVERY,P.STIMEWINDOW,PL.NDROP,
        //PL.SDELIVERYNO,PL.SSHIPTO,P.SHEADREGISTERNO,P.STRAILERREGISTERNO,PL.NVALUE,PL.SPLANLISTID,P.SREMARK,P.SEMPLOYEEID,CASE WHEN p.CCONFIRM = '1' THEN 'action_check.png' ELSE CASE WHEN p.CCONFIRM = '0' THEN 'action_delete.png' ELSE '05.png' END END AS SIMAGE , 
        //CASE WHEN o. SHIPMENT_NO IS NULL THEN 'unload1.png' ELSE 'load1.png' END AS SSHIPMENT
        //FROM (((TPLANSCHEDULE p LEFT JOIN TPlanScheduleList pl ON NVL(P.NPLANID,0) = NVL(PL.NPLANID,0)) LEFT JOIN TTERMINAL t ON P.STERMINALID = T.STERMINALID) 
        //LEFT JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID) LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o ON PL.SDELIVERYNO = o.DELIVERY_NO
        //WHERE 1=1 AND  p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.STERMINALID LIKE '%' || CASE WHEN SUBSTR('' || :oTml,1,5) = '80000' THEN '' ELSE '' ||  :oTml END || '%' AND P.STERMINALID LIKE '%' || :oTerminal || '%' AND (p.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR p.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR PL.SDELIVERYNO LIKE '%' || :oSearch || '%' OR PL.SSHIPTO LIKE '%' || :oSearch || '%' ) AND (" + CommonFunction.ReplaceInjection(datetype) + " BETWEEN TO_DATE(:oBegin,'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(:oEnd,'dd/MM/yyyy HH24:MI:SS')) " + CommonFunction.ReplaceInjection(status) + @"
        //UNION
        //SELECT CASE WHEN nvl(TPC.CFIFO,'0')  = '0' THEN 'ประจำวัน'  ELSE 'FIFO' END AS STYPE,nvl(TPC.CFIFO,0) AS CFIFO, NVL( TPC.NPLANID,0) as NPLANID,NVL(TPC.NNO,1) as NNO
        //, ODP.DATE_CREATE as SPLANDATE ,TPC.SPLANTIME,ODP.STERMINALNAME,ODP.STERMINALID,ODP.DDELIVERY,ODP.NWINDOWTIMEID as STIMEWINDOW,TPC.NDROP,
        //ODP.SDELIVERYNO,TPC.SSHIPTO,TPC.SHEADREGISTERNO,TPC.STRAILERREGISTERNO,TPC.NVALUE,TPC.SPLANLISTID,TPC.SREMARK,TPC.SEMPLOYEEID,
        //CASE WHEN TPC.CCONFIRM = '1' THEN 'action_check.png' ELSE CASE WHEN TPC.CCONFIRM = '0' THEN 'action_delete.png' ELSE '05.png' END END AS SIMAGE , 
        //CASE WHEN o. SHIPMENT_NO IS NULL THEN 'unload1.png' ELSE 'load1.png' END AS SSHIPMENT
        //FROM TBL_ORDERPLAN ODP
        //LEFT JOIN 
        //(
        //    SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID,TPS.CFIFO,TPS.CCONFIRM,TPS.NNO,
        //    TPS.SPLANTIME,TPS.SEMPLOYEEID,TPS.SREMARK ,TPS.SHEADREGISTERNO ,TPS.STRAILERREGISTERNO,TPL.SPLANLISTID,TPL.SSHIPTO,TPL.NDROP  FROM TPLANSCHEDULE TPS
        //    LEFT JOIN TPLANSCHEDULELIST TPL
        //    ON TPS.NPLANID = TPL.NPLANID
        //    WHERE TPL.CACTIVE = '1'  AND NVL(TPS.CFIFO,'XXX') <> '1' AND TPL.CACTIVE = '1' AND  (TPS.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR TPS.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR TPL.SDELIVERYNO LIKE '%' || :oSearch || '%' OR TPL.SSHIPTO LIKE '%' || :oSearch || '%' )
        //)TPC
        //ON ODP.SDELIVERYNO =  TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
        //LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o ON ODP.SDELIVERYNO = o.DELIVERY_NO
        //WHERE 1=1  
        //AND ODP.STERMINALID LIKE '%' || CASE WHEN SUBSTR('' || :oTml,1,5) = '80000' THEN '' ELSE '' ||  :oTml END || '%' 
        //AND ODP.STERMINALID LIKE '%' || :oTerminal || '%' 
        //AND (" + CommonFunction.ReplaceInjection(datetype_Orderplan) + " BETWEEN TO_DATE(:oBegin,'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(:oEnd,'dd/MM/yyyy HH24:MI:SS'))  " + statusOrderPlan + @"
        //)MS";
        string strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY MS.CFIFO, MS.SPLANDATE) AS ID1,MS.* FROM
(
SELECT CASE WHEN nvl(P.CFIFO,'0')  = '0' THEN 'ประจำวัน'  ELSE 'FIFO' END AS STYPE,nvl(P.CFIFO,0) AS CFIFO, P.NPLANID,P.NNO,TRUNC(P.SPLANDATE) as SPLANDATE,P.SPLANTIME,TS.STERMINALNAME,P.STERMINALID,P.DDELIVERY,P.STIMEWINDOW,PL.NDROP,
PL.SDELIVERYNO,PL.SSHIPTO,P.SHEADREGISTERNO,P.STRAILERREGISTERNO,PL.NVALUE,PL.SPLANLISTID,P.SREMARK,P.SEMPLOYEEID,CASE WHEN p.CCONFIRM = '1' THEN 'action_check.png' ELSE CASE WHEN p.CCONFIRM = '0' THEN 'action_delete.png' ELSE '05.png' END END AS SIMAGE , 
CASE WHEN o. SHIPMENT_NO IS NULL THEN 'unload1.png' ELSE 'load1.png' END AS SSHIPMENT
FROM (((TPLANSCHEDULE p LEFT JOIN TPlanScheduleList pl ON NVL(P.NPLANID,0) = NVL(PL.NPLANID,0)) LEFT JOIN TTERMINAL t ON P.STERMINALID = T.STERMINALID) 
LEFT JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID) LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o ON PL.SDELIVERYNO = o.DELIVERY_NO
WHERE 1=1 AND  p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.STERMINALID LIKE '%' || CASE WHEN SUBSTR('' || :oTml,1,5) = '80000' THEN '' ELSE '' ||  :oTml END || '%' AND P.STERMINALID LIKE '%' || :oTerminal || '%' AND (p.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR p.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR PL.SDELIVERYNO LIKE '%' || :oSearch || '%' OR PL.SSHIPTO LIKE '%' || :oSearch || '%' ) AND (" + CommonFunction.ReplaceInjection(datetype) + " BETWEEN TO_DATE(:oBegin,'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(:oEnd,'dd/MM/yyyy HH24:MI:SS')) " + CommonFunction.ReplaceInjection(status) + @"
UNION
SELECT CASE WHEN nvl(TPC.CFIFO,'0')  = '0' THEN 'ประจำวัน'  ELSE 'FIFO' END AS STYPE,nvl(TPC.CFIFO,0) AS CFIFO, NVL( TPC.NPLANID,0) as NPLANID,NVL(TPC.NNO,1) as NNO
, TRUNC(TPC.SPLANDATE) as SPLANDATE ,TPC.SPLANTIME,TPC.STERMINALNAME,TPC.STERMINALID,TPC.DDELIVERY,TPC.STIMEWINDOW,TPC.NDROP,
ODP.SDELIVERYNO,TPC.SSHIPTO,TPC.SHEADREGISTERNO,TPC.STRAILERREGISTERNO,TPC.NVALUE,TPC.SPLANLISTID,TPC.SREMARK,TPC.SEMPLOYEEID,
CASE WHEN TPC.CCONFIRM = '1' THEN 'action_check.png' ELSE CASE WHEN TPC.CCONFIRM = '0' THEN 'action_delete.png' ELSE '05.png' END END AS SIMAGE , 
CASE WHEN o. SHIPMENT_NO IS NULL THEN 'unload1.png' ELSE 'load1.png' END AS SSHIPMENT
FROM TBL_ORDERPLAN ODP
LEFT JOIN 
(
    SELECT TPS.NPLANID,TPS.DPLAN,TPS.STRUCKID,TPL.SDELIVERYNO,TPL.ROUND,TPL.NVALUE,TPL.CACTIVE,TPS.SVENDORID,TPS.CFIFO,TPS.CCONFIRM,TPS.NNO,TPS.DDELIVERY,
    TPS.STIMEWINDOW,TPS.SPLANDATE,TPS.SPLANTIME,TPS.SEMPLOYEEID,TPS.SREMARK ,TPS.SHEADREGISTERNO ,TPS.STRAILERREGISTERNO,TPL.SPLANLISTID,TPL.SSHIPTO,TPL.NDROP,TM.STERMINALNAME,TPS.STERMINALID  FROM TPLANSCHEDULE TPS
    LEFT JOIN TPLANSCHEDULELIST TPL
    ON TPS.NPLANID = TPL.NPLANID
    LEFT JOIN TTERMINAL_SAP TM ON TM.STERMINALID = TPS.STERMINALID
    WHERE TPS.CACTIVE = '1'  AND NVL(TPS.CFIFO,'XXX') <> '1' AND TPL.CACTIVE = '1' AND  (TPS.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR TPS.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR TPL.SDELIVERYNO LIKE '%' || :oSearch || '%' OR TPL.SSHIPTO LIKE '%' || :oSearch || '%' )
)TPC
ON ODP.SDELIVERYNO =  TPC.SDELIVERYNO AND  ODP.SVENDORID =  TPC.SVENDORID
LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o ON ODP.SDELIVERYNO = o.DELIVERY_NO
WHERE 1=1   AND ODP.CACTIVE = 'Y' AND NPLANID != 0
AND ODP.STERMINALID LIKE '%' || CASE WHEN SUBSTR('' || :oTml,1,5) = '80000' THEN '' ELSE '' ||  :oTml END || '%' 
AND ODP.STERMINALID LIKE '%' || :oTerminal || '%' 
AND (" + CommonFunction.ReplaceInjection(datetype_Orderplan) + " BETWEEN TO_DATE(:oBegin,'dd/MM/yyyy HH24:MI:SS') AND TO_DATE(:oEnd,'dd/MM/yyyy HH24:MI:SS'))  " + statusOrderPlan + @"
)MS";

        string dStart = dteStart.Date.ToString("dd/MM/yyyy 00:00:00", new CultureInfo("en-US"));
        string dEnd = dteEnd.Date.ToString("dd/MM/yyyy 23:59:59", new CultureInfo("en-US"));
        string QUERY = strsql.Replace(":oSearch", "'" + txtSearch.Text.Trim() + "'").Replace(":oTerminal", "'" + cboTerminal1.Value + "'").Replace(":oTml", Session["SVDID"] + "").Replace(":oBegin", "'" + dStart + "'").Replace(":oEnd", "'" + dEnd + "'");

        sds3.SelectCommand = strsql;
        sds3.SelectParameters.Clear();
        sds3.SelectParameters.Add("oSearch", CommonFunction.ReplaceInjection(txtSearch.Text.Trim()));
        sds3.SelectParameters.Add("oTerminal", CommonFunction.ReplaceInjection(cboTerminal1.Value + ""));
        sds3.SelectParameters.Add("oTml", CommonFunction.ReplaceInjection(Teminal_ID()));
        sds3.SelectParameters.Add("oBegin", CommonFunction.ReplaceInjection(dStart));
        sds3.SelectParameters.Add("oEnd", CommonFunction.ReplaceInjection(dEnd));

        sds3.DataBind();
        gvw3.DataBind();


        //        string sqlCount = @"SELECT MAX(p.DUPDATE) AS DATEUPDATE, COUNT(p.SHEADREGISTERNO) As COUNTCAR,COUNT(oo.SHIPMENT) AS COUNTSHIPMENT,COUNT(CASE WHEN nvl(oo.CCHECKTRUCK,0) = 0 THEN 1 END ) AS COUNTTRUCK  FROM TPLANSCHEDULE p LEFT JOIN 
        //        (
        //            SELECT PL.NPLANID,MAX(o.SHIPMENT_NO) AS SHIPMENT,MAX(pl.CCHECKTRUCKA) AS CCHECKTRUCK 
        //            FROM TPlanScheduleList pl 
        //            LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o 
        //            ON PL.SDELIVERYNO = o.DELIVERY_NO 
        //            WHERE pl.CACTIVE = '1'  AND  PL.SDELIVERYNO LIKE '%' || '" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "' || '%' OR PL.SSHIPTO LIKE '%' || '" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + @"' || '%' 
        //            GROUP BY PL.NPLANID
        //        ) oo
        //        ON P.NPLANID = oo.NPLANID
        //        WHERE 1=1 AND  p.CACTIVE = '1' 
        //        AND P.STERMINALID LIKE '%' || CASE WHEN SUBSTR('' || '" + CommonFunction.ReplaceInjection(Session["SVDID"] + "") + @"',1,5) = '80000' THEN '' ELSE '' ||  '" + CommonFunction.ReplaceInjection(Session["SVDID"] + "") + @"' END || '%' 
        //        AND P.STERMINALID LIKE '%' || '" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + @"' || '%' 
        //        AND (p.SHEADREGISTERNO LIKE '%' || '" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "' || '%' OR p.STRAILERREGISTERNO LIKE '%' || '" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + @"' || '%') 
        //        AND (" + CommonFunction.ReplaceInjection(datetype) + " BETWEEN TO_DATE('" + dStart + "','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('" + dEnd + "','dd/MM/yyyy HH24:MI:SS')) " + statusOrderPlan + @"";

        //        DataSourceSelectArguments args = new DataSourceSelectArguments();
        //        DataView view = (DataView)sds3.Select(args);
        //        DataTable dt3 = view.ToTable();
        string sqlCount = @"SELECT MAX(p.DUPDATE) AS DATEUPDATE, COUNT(p.SHEADREGISTERNO) As COUNTCAR,COUNT(oo.SHIPMENT) AS COUNTSHIPMENT,COUNT(CASE WHEN nvl(oo.CCHECKTRUCK,0) = 0 THEN 1 END ) AS COUNTTRUCK  FROM TPLANSCHEDULE p LEFT JOIN 
(SELECT PL.NPLANID,MAX(o.SHIPMENT_NO) AS SHIPMENT,MAX(pl.CCHECKTRUCKA) AS CCHECKTRUCK FROM TPlanScheduleList pl LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o ON PL.SDELIVERYNO = o.DELIVERY_NO WHERE pl.CACTIVE = '1'
GROUP BY PL.NPLANID
) oo
ON P.NPLANID = oo.NPLANID
  WHERE p.CACTIVE = '1' AND (" + CommonFunction.ReplaceInjection(datetype) + " BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(dStart) + "','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('" + CommonFunction.ReplaceInjection(dEnd) + "','dd/MM/yyyy HH24:MI:SS'))";


        DataTable dt = CommonFunction.Get_Data(sql, sqlCount);
        if (dt.Rows.Count > 0)
        {
            lblLastUpdate.Text = dt.Rows[0]["DATEUPDATE"] + "";
            lblTotalPlan.Text = dt.Rows[0]["COUNTCAR"] + "";
            lblTotalInProduct.Text = dt.Rows[0]["COUNTSHIPMENT"] + "";
            lblWaitCheck.Text = dt.Rows[0]["COUNTTRUCK"] + "";
        }
    }


    protected void gvw1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        switch (e.CallbackName)
        {
            case "SORT":

                gvw3.CancelEdit();

                var dtt = new List<BDT1>();
                Session["bdt1"] = dtt;
                break;
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    void gvw3_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "Ship-to")
        {
            if (string.IsNullOrEmpty(e.CellValue + ""))
            {
                dynamic SDELIVERY = gvw3.GetRowValues(e.VisibleIndex, "SDELIVERYNO");
                DataTable dt_SHIPTO = Session["DT_SHIPTO"] as DataTable;
                if (dt_SHIPTO.Rows.Count > 0)
                {
                    DataView DV = new DataView(dt_SHIPTO);
                    DV.RowFilter = "DELIVERY_NO = '" + SDELIVERY + "'";
                    if (DV.Count > 0)
                    {
                        e.Cell.Text = DV[0]["SSHIPTO"] + "";
                    }
                }
            }
        }
    }

    private void Get_SHIPTO()
    {

        string datetype;
        if (CommonFunction.ReplaceInjection("" + cmbDate.Value) == "1")
        {
            datetype = "TP.SPLANDATE";
        }
        else
        {
            datetype = "TP.DDELIVERY";
        }

        string dStart = dteStart.Date.ToString("dd/MM/yyyy 00:00:00", new CultureInfo("en-US"));
        string dEnd = dteEnd.Date.ToString("dd/MM/yyyy 23:59:59", new CultureInfo("en-US"));

        string QUERY = @"SELECT d.DELIVERY_NO,(CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SSHIPTO
FROM 
(
      SELECT * FROM TPLANSCHEDULE TP
      LEFT JOIN TPLANSCHEDULELIST TPL
      ON TP.NPLANID = TPL.NPLANID
      LEFT JOIN    TDELIVERY TD
      ON TPL.SDELIVERYNO = TD.DELIVERY_NO
      WHERE  " + datetype + @"  BETWEEN TO_DATE('" + dStart + @"','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('" + dEnd + @"','dd/MM/yyyy HH24:MI:SS')
)d  
LEFT JOIN 
(
    SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
)TRN
ON TRN.DOC_NO = d.SALES_ORDER
LEFT JOIN 
(
    SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
)TBO
ON TBO.DOCNO = d.SALES_ORDER";

        DataTable dt_SHIPTO = CommonFunction.Get_Data(sql, QUERY);
        Session["DT_SHIPTO"] = dt_SHIPTO;
    }

    private string Teminal_ID()
    {
        string Result = "";

        string UserID = Session["UserID"] + "";
        DataTable dt_TERMINAL = CommonFunction.Get_Data(sql, "SELECT SUID,SVENDORID FROM TUSER WHERE CGROUP = '2' AND SUID = '" + UserID + "'");
        if (dt_TERMINAL.Rows.Count > 0)
        {
            Result = dt_TERMINAL.Rows[0]["SVENDORID"] + "";
        }
        else
        {
            dt_TERMINAL = CommonFunction.Get_Data(sql, "SELECT SUID,SVENDORID FROM TUSER WHERE CGROUP IN ('1', '3') AND SUID = '" + UserID + "'");
            if (dt_TERMINAL.Rows.Count > 0)
            {
                Result = "";
            }
            else
            {
                Result = "XXX";
            }
        }

        return Result;
    }

    private string GetDOFROMPLAN(string NPLANID)
    {
        string RESULT = "";

        DataTable DT_PLAN = CommonFunction.Get_Data(sql, "SELECT SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID) + "' AND CACTIVE = '1'");
        if (DT_PLAN.Rows.Count > 0)
        {

            for (int i = 0; i < DT_PLAN.Rows.Count; i++)
            {
                RESULT += "," + DT_PLAN.Rows[i]["SDELIVERYNO"];
            }

        }
        return RESULT;
    }
}
