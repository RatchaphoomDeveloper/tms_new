﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="OrderPlan-progress.aspx.cs"
    EnableSessionState="ReadOnly" MasterPageFile="~/MP1.master" Inherits="OrderPlan_progress" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        function createElementMock(element) {
            var div = document.createElement('DIV');
            div.style.top = "0px";
            div.style.left = "0px";
            div.visibility = "hidden";
            div.style.position = _aspxGetCurrentStyle(element).position;
            return div;
        }
        function _aspxGetExperimentalPositionOffset(element, isX) {
            var div = createElementMock(element);
            if (div.style.position == "static")
                div.style.position = "absolute";
            element.parentNode.appendChild(div);
            var realPos = isX ? _aspxGetAbsoluteX(div) : _aspxGetAbsoluteY(div);
            element.parentNode.removeChild(div);
            return Math.round(realPos);
        }
        window.onload = function () {
            window._aspxGetPositionElementOffset = _aspxGetExperimentalPositionOffset;
            window._aspxGetAbsolutePositionY_Safari = window._aspxGetAbsolutePositionY_FF3;
            window._aspxGetAbsolutePositionX_Safari = window._aspxGetAbsolutePositionX_FF3;
        };
        $(document).ready(function () {
            xcpn.PerformCallback('InsertExceltoDatabase');

            pbProgressing2.SetPosition(0);
            pbProgressing3.SetPosition(0);
            myTimer.SetEnabled(true);
        });


        function window_close() {

            //            window.opener.document.getElementById("cph_Main_xcpn_btnCallback_B").click();
            //            window.close();

            // window.location = 'book-car.aspx?s=' + 2;
        }

    </script>
    <style type="text/css">
        .tbDetailAlert
        {
            font-size: 12px;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        .tbDetailAlert th
        {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #b9c9fe !important;
            border: 1px solid #aabcfe;
            color: #039 !important;
        }
        .tbDetailAlert td
        {
            padding: 8px;
            background: #e8edff;
            color: #669;
            border: 1px solid #aabcfe;
        }
        .tbDetailAlert tr:hover td
        {
            background: #d0dafd;
            color: #339;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxPopupControl ID="popupControl" runat="server" CloseAction="OuterMouseClick"
        HeaderText="รายละเอียด" ClientInstanceName="popupControl" Width="600px" Modal="true"
        SkinID="popUp">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td width='20' background='images/bd001_a.jpg' height='9'>
                        </td>
                        <td background='images/bd001_b.jpg'>
                            &nbsp;
                        </td>
                        <td width='20' height='20' align='left' valign='top' background='images/bd001_c.jpg'>
                        </td>
                    </tr>
                    <tr>
                        <td width='20' align='left' valign='top' background='images/bd001_dx.jpg'>
                            <img src='images/bd001_d.jpg' width='20' height='164'>
                        </td>
                        <td align='left' valign='top' background='images/bd001_i.jpg' bgcolor='#FFFFFF' style='background-repeat: repeat-x'>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="4">
                                        <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
                                            OnCallback="xcpn_Callback" ShowLoadingPanel="false">
                                            <ClientSideEvents EndCallback="function(s,e){
                                ;if(s.cpError != undefined){
                                pbProgressing2.SetPosition('0');
                                pbProgressing3.SetPosition('0');
                                myTimer.SetEnabled(false);
                                lblDetail.SetValue(s.cpError);
                                s.cpError = undefined;
                                window.parent.popupControl.Hide()
                                 }else{
                                pbProgressing2.SetPosition('100');
                                pbProgressing3.SetPosition('100');
                                myTimer.SetEnabled(false);
                                window.parent.popupControl.Hide()
                                 };eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;
                                }"></ClientSideEvents>
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxCallbackPanel>
                                        <dx:ASPxTimer ID="myTimer" runat="server" Enabled="False" Interval="500" ClientInstanceName="myTimer">
                                            <ClientSideEvents Tick="function(s, e) {
	            timerCallback.PerformCallback();
            }" />
                                        </dx:ASPxTimer>
                                        <dx:ASPxCallback ID="timerCallback" runat="server" ClientInstanceName="timerCallback"
                                            OnCallback="timerCallback_Callback">
                                            <ClientSideEvents CallbackComplete="function(s, e) {

                            var data =  e.result.split(';');

                            switch(data[0]){ 
                            case '1' :
                               pbProgressing2.SetPosition(data[1]);
                               break;
                            case '2' :
                               pbProgressing2.SetPosition('100');
                               pbProgressing3.SetPosition(data[1]);
                               break;
                            }

            }" />
                                        </dx:ASPxCallback>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        ตรวจสอบข้อมูล :
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <dx:ASPxProgressBar ID="pbProgressing2" ClientInstanceName="pbProgressing2" runat="server"
                                            Width="100%">
                                        </dx:ASPxProgressBar>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        นำเข้าข้อมูล :
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <dx:ASPxProgressBar ID="pbProgressing3" ClientInstanceName="pbProgressing3" runat="server"
                                            Width="100%">
                                        </dx:ASPxProgressBar>
                                    </td>
                                </tr>
                            </table>
                            <td width='20' align='left' valign='top' background='images/bd001_ex.jpg'>
                                <img src='images/bd001_e.jpg' width='20' height='164'>
                            </td>
                    </tr>
                    <tr>
                        <td width='20'>
                            <img src='images/bd001_f.jpg' width='20' height='20'>
                        </td>
                        <td background='images/bd001_g.jpg'>
                            <img src='images/bd001_g.jpg' width='20' height='20'>
                        </td>
                        <td width='20'>
                            <img src='images/bd001_h.jpg' width='20' height='20'>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <table>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxButton ID="btnBack" runat="server" AutoPostBack="false" Text="Back">
                    <%--<ClientSideEvents Click="function(s,e){window.location='OrderPlan.aspx?s=2';}" />--%>
                    <ClientSideEvents Click="function(s,e){window.location='OrderPlan.aspx?str=1';}" />
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <span style="font-size: larger;">Detail : </span>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="lblDetail" runat="server" ClientInstanceName="lblDetail" ForeColor="Red">
                </dx:ASPxLabel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
