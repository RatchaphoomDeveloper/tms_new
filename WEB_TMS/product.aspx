﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="product.aspx.cs" Inherits="product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //        $("td#cph_Main_xcpn_rpnInformation_uploadEMP_ClearBox0").removeClass("dxucTextBox_Aqua");
            //        $("td#cph_Main_xcpn_rpnInformation_uploadEMP_TextBox0").removeClass("dxucTextBox_Aqua");
            $("input.dxgvCommandColumnItem_Aqua dxgv__cci").removeClass("dxgvCommandColumnItem_Aqua dxgv__cci").addClass("dxbButton_Aqua");
        });
    </script>
    <style type="text/css">
        .dxrpcontent
        {
            padding: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="การจัดการข้อมูลผลิตภัณฑ์ ">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="arp">
                            <table runat="server" width="100%">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox runat="server" ID="txtchkEditgrid" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <%--   <dx:ASPxButton runat="server" ID="btnHistoryPro" Text="ประวัติผลิตภัณฑ์" AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('viewHistoryemp'); }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table runat="server" width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="right" width="15%">กลุ่มผลิตภัณฑ์(SAP) :</td>
                                                            <td width="35%">
                                                                <dx:ASPxComboBox runat="server" CallbackPageSize="30" EnableCallbackMode="True" ValueField="PROD_ID"
                                                                    TextFormatString="{0}" ClientInstanceName="cboSAP" CssClass="dxeLineBreakFix"
                                                                    SkinID="xcbbATC" ID="cboSAP" OnItemRequestedByValue="cboSAP_OnItemRequestedByValueSQL"
                                                                    OnItemsRequestedByFilterCondition="cboSAP_ItemsRequestedByFilterConditionSQL" >
                                                                    <Columns>
                                                                        <dx:ListBoxColumn FieldName="PROD_ID" Width="200px" Caption="กลุ่มผลิตภัณฑ์(SAP)"
                                                                            Visible="false"></dx:ListBoxColumn>
                                                                        <dx:ListBoxColumn FieldName="PROD_GRP_NAME" Width="200px" Caption="กลุ่มผลิตภัณฑ์(SAP)">
                                                                        </dx:ListBoxColumn>
                                                                    </Columns>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sdspSap" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                                </asp:SqlDataSource>
                                                            </td>
                                                            <td align="right" width="15%">กลุ่มผลิตภัณฑ์(TMS) :</td>
                                                            <td width="35%">
                                                                <dx:ASPxComboBox runat="server" ID="cboTMS" DataSourceID="sdsTMS" TextField="SPRODUCTTYPENAME"
                                                                    ValueField="SPRODUCTTYPEID">
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sdsTMS" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                    SelectCommand="SELECT SPRODUCTTYPEID, SPRODUCTTYPENAME, NORDER
                                                                       FROM TPRODUCTTYPE
                                                                       ORDER BY NORDER ASC"></asp:SqlDataSource>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" width="15%">ค้นหาข้อมูลผลิตภัณฑ์ :</td>
                                                <td width="85%">
                                                    <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="รหัสผลิตภัณฑ์,ชื่อผลิตภัณฑ์,ชื่อกลุ่มผลิตภัณฑ์"
                                                        CssClass="dxeLineBreakFix" Width="300px">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CssClass="dxeLineBreakFix" AutoPostBack="false">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnCancelSearch" runat="server" SkinID="_cancelsearch" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('CancelSearch'); }">
                                                        </ClientSideEvents>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false" SkinID="_gvw"
                                                        ClientInstanceName="gvw" OnAfterPerformCallback="gvw_AfterPerformCallback" KeyFieldName="PROD_ID"
                                                        OnRowUpdating="gvw_RowUpdating" EnableRowsCache="False" OnDataBinding="grid_DataBinding">
                                                        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                                        </ClientSideEvents>
                                                        <Columns>
                                                            <dx:GridViewDataColumn Caption="รหัส<br>ผลิตภัณฑ์" FieldName="PROD_ID" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center" Width="7%" EditCellStyle-HorizontalAlign="Center" >
                                                                <EditItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblPROD_ID" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"PROD_ID") %>'>
                                                                    </dx:ASPxLabel>
                                                                </EditItemTemplate>
                                                                <EditCellStyle HorizontalAlign="Center">
                                                                </EditCellStyle>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxHyperLink runat="server" ID="hplSVENDORID" Text='<%# Eval("PROD_ID") %>'
                                                                        Cursor="pointer">
                                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('viewHistoryPro;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxHyperLink>
                                                                </DataItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="ชื่อ<br>ผลิตภัณฑ์(SAP)" FieldName="PROD_NAME" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Left" Width="14%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblPROD_NAME" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"PROD_NAME") %>'>
                                                                    </dx:ASPxLabel>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="กลุ่ม<br>ผลิตภัณฑ์(SAP)" HeaderStyle-HorizontalAlign="Center"
                                                                FieldName="PROD_GRP_NAME" Width="13%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblPROD_GRP_NAME" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"PROD_GRP_NAME") %>'>
                                                                    </dx:ASPxLabel>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="กลุ่ม<br>ผลิตภัณฑ์(TMS)" FieldName="PROD_CATEGORY"
                                                                HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="8%">
                                                                <EditItemTemplate>
                                                                    <%--<dx:ASPxTextBox runat="server" ID="txtPROD_CATEGORY" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"PROD_CATEGORY") %>'>
                                                                    </dx:ASPxTextBox>--%>
                                                                    <dx:ASPxComboBox runat="server" ID="cboPROD_CATEGORY" DataSourceID="sdsCmb" TextField="SPRODUCTTYPENAME"
                                                                        ValueField="SPRODUCTTYPEID" Width="80px">
                                                                    </dx:ASPxComboBox>
                                                                    <asp:SqlDataSource ID="sdsCmb" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                        SelectCommand="SELECT &quot;SPRODUCTTYPEID&quot;, &quot;SPRODUCTTYPENAME&quot; FROM &quot;TPRODUCTTYPE&quot;">
                                                                    </asp:SqlDataSource>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="ความหนาแน่น<br>(Kg\m3)" FieldName="DENSITY" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center" Width="6%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxTextBox runat="server" ID="txtDENSITY" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"DENSITY") %>'>
                                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                            CausesValidation="True">
                                                                            <RegularExpression ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>"
                                                                                ErrorText="ใช้ได้เฉพาะตัวเลข"></RegularExpression>
                                                                        </ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataCheckColumn Caption="ขนส่ง<br>ทางรถ" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center" FieldName="LANDTRANSPORT" Width="6%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxCheckBox runat="server" ID="chkLANDTRANSPORT" ClientInstanceName="chkLANDTRANSPORT"
                                                                        Checked='<%# Eval("LANDTRANSPORT").ToString() == "1" ? true: false %>'>
                                                                    </dx:ASPxCheckBox>
                                                                </EditItemTemplate>
                                                                <PropertiesCheckEdit ValueType="System.String" ValueChecked="1" ValueUnchecked="0" />
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataCheckColumn>
                                                            <dx:GridViewDataCheckColumn Caption="ขนส่ง<br>ทางเรือ" FieldName="WATERTRANSPORT"
                                                                HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="6%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxCheckBox runat="server" ID="chkWATERTRANSPORT" ClientInstanceName="WATERTRANSPORT"
                                                                        Checked='<%# Eval("WATERTRANSPORT").ToString() == "1" ? true: false %>' AutoPostBack="false">
                                                                    </dx:ASPxCheckBox>
                                                                </EditItemTemplate>
                                                                <PropertiesCheckEdit ValueType="System.String" ValueChecked="1" ValueUnchecked="0" />
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataCheckColumn>
                                                            <dx:GridViewDataCheckColumn Caption="ขนส่ง<br>ทางท่อ" FieldName="PIPELINETRANSPORT"
                                                                HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="6%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxCheckBox runat="server" ID="chkPIPELINETRANSPORT" ClientInstanceName="chkPIPELINETRANSPORT"
                                                                        Checked='<%# Eval("PIPELINETRANSPORT").ToString() == "1" ? true: false %>'>
                                                                    </dx:ASPxCheckBox>
                                                                </EditItemTemplate>
                                                                <PropertiesCheckEdit ValueType="System.String" ValueChecked="1" ValueUnchecked="0" />
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataCheckColumn>
                                                            <dx:GridViewDataCheckColumn Caption="ขนส่ง<br>ทางรถไฟ" FieldName="TRAINTRANSPORT"
                                                                HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="7%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxCheckBox runat="server" ID="chkTRAINTRANSPORT" ClientInstanceName="chkTRAINTRANSPORT"
                                                                        Checked='<%# Eval("TRAINTRANSPORT").ToString() == "1" ? true: false %>'>
                                                                    </dx:ASPxCheckBox>
                                                                </EditItemTemplate>
                                                                <PropertiesCheckEdit ValueType="System.String" ValueChecked="1" ValueUnchecked="0" />
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataCheckColumn>
                                                            <dx:GridViewDataColumn Caption="ชื่อย่อ<br>(จองรถ)" FieldName="PROD_ABBR" HeaderStyle-HorizontalAlign="Center"
                                                                Width="13%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxTextBox runat="server" ID="txtPROD_ABBR" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"PROD_ABBR") %>'>
                                                                    </dx:ASPxTextBox>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="ชื่อย่อ<br>(ขนส่งทางเรือ)" FieldName="PROD_ABBR_V"
                                                                HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Left" Width="7%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxTextBox runat="server" ID="txtPROD_ABBR_V" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"PROD_ABBR_V") %>'>
                                                                    </dx:ASPxTextBox>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Width="7%" Caption="การจัดการ" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxButton runat="server" ID="edt" SkinID="_edit" AutoPostBack="false">
                                                                        <ClientSideEvents Click="function(s,e){gvw.PerformCallback('Startedit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxButton>
                                                                </DataItemTemplate>
                                                                <EditItemTemplate>
                                                                    <dx:ASPxButton runat="server" ID="edt" SkinID="_update" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                        <ClientSideEvents Click="function(s,e){gvw.PerformCallback('Updateedit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton runat="server" ID="ASPxButton1" SkinID="_cancelth" AutoPostBack="false"
                                                                        CssClass="dxeLineBreakFix">
                                                                        <ClientSideEvents Click="function(s,e){gvw.CancelEdit(); }" />
                                                                    </dx:ASPxButton>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="CHECKIN" Visible="false">
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="LAND" Visible="false">
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="WATER" Visible="false">
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="PIPE" Visible="false">
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="TRAIN" Visible="false">
                                                            </dx:GridViewDataColumn>
                                                        </Columns>
                                                        <SettingsEditing Mode="Inline" />
                                                        <SettingsPager PageSize="50">
                                                        </SettingsPager>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
