﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    StylesheetTheme="Aqua" CodeFile="ReportDefectPercentage.aspx.cs" Inherits="ReportDefectPercentage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
<script type="text/javascript" src="Javascript/DevExpress/DevExpress.js" > </script>
    <table width="100%">
        <tr>
            <td style="width: 20%; text-align: right;">
                ผู้ขนส่ง
            </td>
            <td style="width: 20%;">
                <dx:ASPxComboBox ID="cbxVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cbxVendor"
                    EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL" nulltext="-- ทั้งหมด --"
                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SVENDORID" Width="180px">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td style="white-space: nowrap;">
                เลขที่สัญญา
            </td>
            <td style="width: 58%;">
                <dx:ASPxComboBox ID="cboSCONTRACTNO" runat="server" CallbackPageSize="30" ClientInstanceName="cboSCONTRACTNO"
                    EnableCallbackMode="True" SkinID="xcbbATC" TextFormatString="{0}" ValueField="SCONTRACTID" nulltext="-- ทั้งหมด --"
                    Width="180px" OnItemRequestedByValue="cboSCONTRACTNO_ItemRequestedByValue" OnItemsRequestedByFilterCondition="cboSCONTRACTNO_ItemsRequestedByFilterCondition">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="100px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="dscboSCONTRACTNO" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 10%; text-align: right;" colspan="1">
                ช่วงเวลา
            </td>
            <td colspan="1" style="white-space: nowrap;">
                <dx:ASPxRadioButtonList ID="rblDefactMode" ClientInstanceName="rblDefactMode" runat="server"
                    RepeatDirection="Horizontal" Width="280px">
                    <ClientSideEvents SelectedIndexChanged="function(s,e){var thismode=s.GetValue(); if(thismode=='Year'){ cboMonth.SetVisible(false); cboQuarter.SetVisible(false); cboYear.SetVisible(true); }else if(thismode=='Quarter'){cboMonth.SetVisible(false); cboQuarter.SetVisible(true); cboYear.SetVisible(true);} else{ cboMonth.SetVisible(true); cboQuarter.SetVisible(false); cboYear.SetVisible(true); }; }" />
                    <Items>
                        <dx:ListEditItem Text="รายเดือน" Value="Month" Selected="true" />
                        <dx:ListEditItem Text="รายไตรมาตร" Value="Quarter" />
                        <dx:ListEditItem Text="รายปี" Value="Year" />
                    </Items>
                    <Border BorderStyle="None" />
                </dx:ASPxRadioButtonList>
            </td>
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <div style="float: left;">
                                <dx:ASPxComboBox ID="cboMonth" ClientInstanceName="cboMonth" runat="server" Width="120px">
                                    <Items>
                                        <dx:ListEditItem Text="มกราคม" Value="01" />
                                        <dx:ListEditItem Text="กุมภาพันธ์" Value="02" />
                                        <dx:ListEditItem Text="มีนาคม" Value="03" />
                                        <dx:ListEditItem Text="เมษายน" Value="04" />
                                        <dx:ListEditItem Text="พฤษภาคม" Value="05" />
                                        <dx:ListEditItem Text="มิถุนายน" Value="06" />
                                        <dx:ListEditItem Text="กรกฎาคม" Value="07" />
                                        <dx:ListEditItem Text="สิงหาคม" Value="08" />
                                        <dx:ListEditItem Text="กันยายน" Value="09" />
                                        <dx:ListEditItem Text="ตุลาคม" Value="10" />
                                        <dx:ListEditItem Text="พฤศจิกายน" Value="11" />
                                        <dx:ListEditItem Text="ธันวาคม" Value="12" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td>
                            <div style="float: left;">
                                <dx:ASPxComboBox ID="cboQuarter" ClientInstanceName="cboQuarter" runat="server" Width="200px"
                                    ClientVisible="false">
                                    <Items>
                                        <dx:ListEditItem Text="ไตรมาตรที่ 1 (มกราคม-มีนาคม)" Value="Q_01_02_03" />
                                        <dx:ListEditItem Text="ไตรมาตรที่ 2 (เมษายน-มิถุนายน)" Value="Q_04_05_06" />
                                        <dx:ListEditItem Text="ไตรมาตรที่ 3 (กรกฎาคม-กันยายน)" Value="Q_07_08_09" />
                                        <dx:ListEditItem Text="ไตรมาตรที่ 4 (ตุลาคม-ธันวาคม)" Value="Q_10_11_12" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </div>
                            <div style="float: left;">
                                <dx:ASPxComboBox ID="cboYear" ClientInstanceName="cboYear" runat="server" Width="70px"
                                    ClientVisible="true" DataSourceID="sdsYear" TextField="stext" ValueField="svalue">
                                    <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                        CausesValidation="true" ValidationGroup="search">
                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="2">
            </td>
            <td>
                <asp:SqlDataSource ID="sdsYear" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    SelectCommand="SELECT MIN(svalue)-1 svalue,MIN(stext)-1 stext
FROM (
SELECT distinct EXTRACT(year from RDPT.DREDUCE ) svalue,EXTRACT(year from RDPT.DREDUCE )+543 stext
FROM TREDUCEPOINT RDPT
) TBL_MIN
UNION ALL
SELECT distinct EXTRACT(year from RDPT.DREDUCE ) svalue,EXTRACT(year from RDPT.DREDUCE )+543 stext
FROM TREDUCEPOINT RDPT 
UNION ALL
SELECT MAX(svalue)+1 svalue,MAX(stext)+1 stext
FROM (
SELECT distinct EXTRACT(year from RDPT.DREDUCE ) svalue,EXTRACT(year from RDPT.DREDUCE )+543 stext
FROM TREDUCEPOINT RDPT
) TBL_MAX"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="2">
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" ValidationGroup="search" OnClick="btnSubmit_Click"
                        Text="Pdf" Width="100">
                    </dx:ASPxButton>
                </div>
                <div style="float: left">
                    &nbsp;</div>
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" ValidationGroup="search" OnClick="ASPxButton2_Click"
                        Text="Excel" Width="100">
                    </dx:ASPxButton>
                </div>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
