﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_accident_lst.aspx.cs" Inherits="vendor_accident_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="34%">
                            <div style="float: left">
                                <dx:ASPxButton ID="btnAdd" runat="server" Text="แจ้งอุบัติเหตุ" Width="130px" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s,e){window.location ='vendor_accident_add.aspx';}" />
                                </dx:ASPxButton>
                            </div>
                            <div style="float: left">
                                &nbsp;</div>
                            <div style="float: left">
                                <dx:ASPxButton ID="ASPxButton1" runat="server" AutoPostBack="False" Text="ส่งข้อมูลให้ ปตท."
                                    Width="130px">
                                    <ClientSideEvents Click="function (s, e) {if(gvw.GetSelectedRowCount() > 0){dxConfirm('ยืนยันการทำงาน','<b>ท่านต้องการส่งข้อมูลอุบัติเหตุให้ ทางปตท.</b> ต่อใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('SendData');} ,function(s,e){ dxPopupConfirm.Hide(); })}else{dxWarning('แจ้งให้ทราบ', 'กรุณาเลือกรายการที่ต้องการส่ง');}}">
                                    </ClientSideEvents>
                                </dx:ASPxButton>
                            </div>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="ประเภทรถ,ทะเบียนรถ ,พขร">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus" Width="100px"
                                SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="- สถานะ -" />
                                    <dx:ListEditItem Text="ดำเนินการ" Value="1" />
                                    <dx:ListEditItem Text="อุทธรณ์" Value="2" />
                                    <dx:ListEditItem Text="ปิดเรื่อง" Value="3" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="8">
                            จำนวนรายการอุบัติเหตุ&nbsp;<dx:ASPxLabel ID="lblCarCount" runat="server" Text="0"
                                Font-Bold="true" ForeColor="Red">
                            </dx:ASPxLabel>
                            &nbsp;รายการ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                OnHtmlRowPrepared="gvw_HtmlRowPrepared">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" Width="3%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SACCIDENTID" ShowInCustomizationForm="True"
                                        Visible="False" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่แจ้ง" FieldName="DDATENOTIFY" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่เกิดเหตุ" FieldName="DACCIDENT" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="เวลา<br>ที่เกิดเหตุ" FieldName="TACCIDENTTIME"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn Caption="พนักงานขับรถ" FieldName="SEMPLOYEE" ShowInCustomizationForm="True"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เวลา<br>ดำเนิน<br>การ" FieldName="NOPERATTION"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUS" ShowInCustomizationForm="True"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ความเสียหาย" FieldName="NVALUE" ShowInCustomizationForm="True"
                                        UnboundType="Decimal" Width="10%">
                                        <PropertiesTextEdit DisplayFormatString="n">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ฝ่ายถูก/ผิด" FieldName="FAULT" ShowInCustomizationForm="True"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="15" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" AutoPostBack="false"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                <Image Width="16px" Height="16px" Url="Images/search.png">
                                                </Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn FieldName="CSENDTOPK" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <table>
                                <tr>
                                    <td>
                                        สถานะของสี
                                    </td>
                                    <td style="border-style: solid; border-width: 1px; border-color: Black; background-color: #F5F5F5"
                                        width="15px">
                                        &nbsp;
                                    </td>
                                    <td>
                                        = ยังไม่ได้ส่งข้อมูลให้ ปตท.
                                    </td>
                                    <td style="background-color: #CEECF5" width="15px">
                                    </td>
                                    <td>
                                        = ส่งข้อมูลให้ ปตท. แล้ว
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        
                    </tr>
                    <tr>
                      <td></td>
                        </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
