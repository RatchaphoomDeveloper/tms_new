﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;

public partial class admin_topic_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        
            //ตรวจสอบว่ามีค่า TopicID ที่ส่งมาจาก admin_topic_lst หรือไม่ถ้ามีให้ถือว่าเป็นการแก้ไข
            Session["oTopicID11"] = null;
            LoadTopicType();
            if (Session["oTopicID"] != null)
            {
                Session["oTopicID11"] = Session["oTopicID"];
                listData();
            }

            this.AssignAuthen();
        }
    }


    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnSubmit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadTopicType()
    {
        try
        {
            DataTable dtTopicType = CommonFunction.Get_Data(sql, "SELECT * FROM M_TTOPIC_TYPE");
            DropDownListHelper.BindDropDownList(ref ddlTopicType, dtTopicType, "TTOPIC_TYPE_ID", "TTOPIC_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    void ClearControl()
    {

        txtTopicName.Text = "";
        cboCal2.Value = "";
        cboCal3.Value = "";
        cboCal4.Value = "";
        cboCal6.Value = "";
        txtMultipleImpact.Text = "";
        txtPoint.Text = "";
        txtVersion.Text = "";
        txtTopicID.Text = "";
        txtDiffMultiple.Text = "";
        txtDiffPoint.Text = "";
        txtDescription.Text = "";
        chkProcess.UnselectAll();


    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        if (CanWrite)
        {
            switch (paras[0])
            {


                case "Save":

                    InactiveData();
                    // ตรวจสอบว่าถ้า txtTopicID ไม่มีข้อมูลให้ทำการ Insert ลงฐานข้อมูล ถ้ามีให้ Update ข้อมุล
                    if (String.IsNullOrEmpty(txtTopicID.Text))
                    {

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strSql = "INSERT INTO tTopic(CPROCESS,SREMARK,sTopicID,sVersion,sTopicName, sCol02, sCol03,sCol04,sCol05,sCol06,nMultipleImpact,nPoint,cActive,dCreate,sCreate,dUpdate,sUpdate, TOTAL_COST, TOTAL_BREAK, CAN_EDIT, IS_CORRUPT, BLACKLIST, TTOPIC_TYPE_ID) Values (:CPROCESS,:SREMARK,:sTopicID,:sVersion,:sTopicName, :sCol02, :sCol03,:sCol04,0,:sCol06,:nMultipleImpact,:nPoint,:cActive,SYSDATE,:sCreate,SYSDATE,:sUpdate, :sTOTAL_COST, :sTOTAL_BREAK, :sCAN_EDIT, :sIS_CORRUPT, :sBLACKLIST, :sTOPIC_TYPE_ID)";
                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();

                                string GenID = CommonFunction.Gen_ID(con, "SELECT sTopicID FROM (SELECT sTopicID FROM tTopic ORDER BY sTopicID DESC)  WHERE ROWNUM <= 1");
                                Session["oTopicID11"] = GenID;
                                double num = 0.0;
                                com.Parameters.Add(":CPROCESS", OracleType.VarChar).Value = AddProcess();
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDescription.Text;
                                com.Parameters.Add(":sTopicID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":sVersion", OracleType.Int32).Value = 1;
                                com.Parameters.Add(":sTopicName", OracleType.VarChar).Value = txtTopicName.Text;
                                com.Parameters.Add(":sCol02", OracleType.Double).Value = double.TryParse(cboCal2.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":sCol03", OracleType.Double).Value = double.TryParse(cboCal3.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":sCol04", OracleType.Double).Value = double.TryParse(cboCal4.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":sCol06", OracleType.Double).Value = double.TryParse(cboCal6.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":nMultipleImpact", OracleType.Double).Value = double.TryParse(txtMultipleImpact.Text + "", out num) ? num : 0;
                                com.Parameters.Add(":nPoint", OracleType.Double).Value = double.TryParse((txtPoint.Text + "").Replace("-", ""), out num) ? num : 0;
                                com.Parameters.Add(":cActive", OracleType.Char).Value = rblStatus.Value;
                                com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();

                                int tmp;
                                com.Parameters.Add(":sTOTAL_COST", OracleType.Number).Value = (int.TryParse(txtCost.Text.Trim(), out tmp)) ? int.Parse(txtCost.Text.Trim()) : 0;
                                com.Parameters.Add(":sTOTAL_BREAK", OracleType.Number).Value = (int.TryParse(txtDisableDay.Text.Trim(), out tmp)) ? int.Parse(txtDisableDay.Text.Trim()) : 0;
                                com.Parameters.Add(":sBLACKLIST", OracleType.Number).Value = (chkBlackList.Checked) ? "1" : "0";
                                com.Parameters.Add(":sTOPIC_TYPE_ID", OracleType.Number).Value = ddlTopicType.SelectedValue;
                                com.Parameters.Add(":sCAN_EDIT", OracleType.Char).Value = (chkCanEdit.Checked) ? "1" : "0";
                                com.Parameters.Add(":sIS_CORRUPT", OracleType.Char).Value = (chkCheck.Checked) ? "1" : "0";

                                com.ExecuteNonQuery();
                            }
                        }

                        LogUser("34", "I", "บันทึกข้อมูลหน้า หัวข้อตัดคะแนนประเมิน", Session["oTopicID11"] + "");
                    }
                    else
                    {
                        //ตรวจสอบว่าถ้า Multiple และ Point มีการเปลี่ยนเปลี่ยนแปลงให้ Version + 1 และ Insert ลงฐานข้อมูล
                        if (!txtDiffMultiple.Text.Equals(txtMultipleImpact.Text) || !txtDiffPoint.Text.Equals(txtPoint.Text))
                        {
                            string GenID = CommonFunction.Gen_ID(sql, "SELECT sTopicID FROM (SELECT sTopicID FROM tTopic ORDER BY sTopicID DESC)  WHERE ROWNUM <= 1");

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strSql = "INSERT INTO tTopic(CPROCESS,SREMARK,sTopicID,sVersion,sTopicName, sCol02, sCol03,sCol04,sCol05,sCol06,nMultipleImpact,nPoint,cActive,dCreate,sCreate,dUpdate,sUpdate, TOTAL_COST, TOTAL_BREAK, CAN_EDIT, IS_CORRUPT, BLACKLIST, TTOPIC_TYPE_ID) Values (:CPROCESS,:SREMARK,:sTopicID,:sVersion,:sTopicName, :sCol02, :sCol03,:sCol04,0,:sCol06,:nMultipleImpact,:nPoint,:cActive,SYSDATE,:sCreate,SYSDATE,:sUpdate, :sTOTAL_COST, :sTOTAL_BREAK, :sCAN_EDIT, :sIS_CORRUPT, :sBLACKLIST, :sTOPIC_TYPE_ID)";
                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    Session["oTopicID11"] = txtTopicID.Text;

                                    string GenVersion = CommonFunction.Gen_ID(sql, "SELECT sVersion FROM (SELECT sVersion FROM tTopic WHERE sTopicID = '" + txtTopicID.Text + "' ORDER BY sVersion DESC)  WHERE ROWNUM <= 1");
                                    double num = 0.0;
                                    com.Parameters.Add(":CPROCESS", OracleType.VarChar).Value = AddProcess();
                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDescription.Text;
                                    com.Parameters.Add(":sTopicID", OracleType.Number).Value = txtTopicID.Text;
                                    com.Parameters.Add(":sVersion", OracleType.Int32).Value = GenVersion;
                                    com.Parameters.Add(":sTopicName", OracleType.VarChar).Value = txtTopicName.Text;
                                    com.Parameters.Add(":sCol02", OracleType.Double).Value = double.TryParse(cboCal2.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":sCol03", OracleType.Double).Value = double.TryParse(cboCal3.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":sCol04", OracleType.Double).Value = double.TryParse(cboCal4.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":sCol06", OracleType.Double).Value = double.TryParse(cboCal6.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":nMultipleImpact", OracleType.Double).Value = double.TryParse(txtMultipleImpact.Text + "", out num) ? num : 0;
                                    com.Parameters.Add(":nPoint", OracleType.Double).Value = double.TryParse((txtPoint.Text + "").Replace("-", ""), out num) ? num : 0;
                                    com.Parameters.Add(":cActive", OracleType.Char).Value = rblStatus.Value;
                                    com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();

                                    int tmp;
                                    com.Parameters.Add(":sTOTAL_COST", OracleType.Number).Value = (int.TryParse(txtCost.Text.Trim(), out tmp)) ? int.Parse(txtCost.Text.Trim()) : 0;
                                    com.Parameters.Add(":sTOTAL_BREAK", OracleType.Number).Value = (int.TryParse(txtDisableDay.Text.Trim(), out tmp)) ? int.Parse(txtDisableDay.Text.Trim()) : 0;
                                    com.Parameters.Add(":sTOPIC_TYPE_ID", OracleType.Number).Value = ddlTopicType.SelectedValue;
                                    com.Parameters.Add(":sBLACKLIST", OracleType.Char).Value = (chkBlackList.Checked) ? "1" : "0";
                                    com.Parameters.Add(":sCAN_EDIT", OracleType.Char).Value = (chkCanEdit.Checked) ? "1" : "0";
                                    com.Parameters.Add(":sIS_CORRUPT", OracleType.Char).Value = (chkCheck.Checked) ? "1" : "0";
                                    com.ExecuteNonQuery();

                                }
                            }
                        }
                        else
                        {

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strSql = "UPDATE tTopic SET CPROCESS = :CPROCESS ,SREMARK = :SREMARK,sTopicName = :sTopicName, sCol02 = :sCol02, sCol03 = :sCol03,sCol04 = :sCol04,sCol06 = :sCol06,nMultipleImpact = :nMultipleImpact,nPoint = :nPoint,cActive = :cActive,dUpdate = SYSDATE,sUpdate = :sUpdate , TOTAL_COST=:sTOTAL_COST, TOTAL_BREAK=:sTOTAL_BREAK, CAN_EDIT=:sCAN_EDIT, IS_CORRUPT=:sIS_CORRUPT, BLACKLIST=:sBLACKLIST, TTOPIC_TYPE_ID=:sTOPIC_TYPE_ID  where sTopicID = :sTopicID and sVersion = :sVersion";
                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    double num = 0.0;
                                    Session["oTopicID11"] = txtTopicID.Text;
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":CPROCESS", OracleType.VarChar).Value = AddProcess();
                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDescription.Text;
                                    com.Parameters.Add(":sTopicID", OracleType.Int32).Value = txtTopicID.Text;
                                    com.Parameters.Add(":sVersion", OracleType.Int32).Value = txtVersion.Text;
                                    com.Parameters.Add(":sTopicName", OracleType.VarChar).Value = txtTopicName.Text;
                                    com.Parameters.Add(":sCol02", OracleType.Double).Value = double.TryParse(cboCal2.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":sCol03", OracleType.Double).Value = double.TryParse(cboCal3.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":sCol04", OracleType.Double).Value = double.TryParse(cboCal4.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":sCol06", OracleType.Double).Value = double.TryParse(cboCal6.Value + "", out num) ? num : 0;
                                    com.Parameters.Add(":nMultipleImpact", OracleType.Double).Value = double.TryParse(txtMultipleImpact.Text + "", out num) ? num : 0;
                                    com.Parameters.Add(":nPoint", OracleType.Double).Value = double.TryParse((txtPoint.Text + "").Replace("-", ""), out num) ? num : 0;
                                    com.Parameters.Add(":cActive", OracleType.Char).Value = rblStatus.Value;
                                    com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();

                                    int tmp;
                                    com.Parameters.Add(":sTOTAL_COST", OracleType.Number).Value = (int.TryParse(txtCost.Text.Trim(), out tmp)) ? int.Parse(txtCost.Text.Trim()) : 0;
                                    com.Parameters.Add(":sTOTAL_BREAK", OracleType.Number).Value = (int.TryParse(txtDisableDay.Text.Trim(), out tmp)) ? int.Parse(txtDisableDay.Text.Trim()) : 0;
                                    com.Parameters.Add(":sTOPIC_TYPE_ID", OracleType.Number).Value = ddlTopicType.SelectedValue;
                                    com.Parameters.Add(":sBLACKLIST", OracleType.Number).Value = (chkBlackList.Checked) ? "1" : "0";
                                    com.Parameters.Add(":sCAN_EDIT", OracleType.Char).Value = (chkCanEdit.Checked) ? "1" : "0";
                                    com.Parameters.Add(":sIS_CORRUPT", OracleType.Char).Value = (chkCheck.Checked) ? "1" : "0";

                                    com.ExecuteNonQuery();
                                }
                            }

                        }

                        LogUser("34", "E", "แก้ไขข้อมูลหน้า หัวข้อตัดคะแนนประเมิน", Session["oTopicID11"] + "");
                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");

                    ClearControl();
                    gvw.DataBind();

                    break;

                case "CheckScore":
                    double num1 = 0.0;
                    double sum1 = 0.0;
                    double c2 = double.TryParse("" + cboCal2.Value, out num1) ? num1 : 0;
                    double c3 = double.TryParse("" + cboCal3.Value, out num1) ? num1 : 0;
                    double c4 = double.TryParse("" + cboCal4.Value, out num1) ? num1 : 0;
                    double c6 = double.TryParse("" + cboCal6.Value, out num1) ? num1 : 0;
                    sum1 = c2 * c3 * c4 * c6;
                    txtMultipleImpact.Text = sum1 + "";
                    txtPoint.Text = chkCheck.Checked ? "30" : CommonFunction.Get_Value(sql, "SELECT NSCORE FROM LSTMULTIPLEIMPACT WHERE " + sum1 + " BETWEEN NSTART AND CASE WHEN NEND = 0 THEN " + sum1 + " ELSE NEND END");
                    break;

                case "delete":
                    if (CanWrite)
                    {
                        int indexxx = int.Parse(e.Parameter.Split(';')[1]);
                        dynamic dt = gvw.GetRowValues(indexxx, "STOPICID");
                        dynamic dt1 = gvw.GetRowValues(indexxx, "SVERSION");
                        Session["dTPID"] = dt;
                        Session["dVSN"] = dt1;

                        try
                        {
                            sds.Delete();
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                        }
                        catch (Exception ex)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถลบรายการนี้ได้ รหัส " + Session["dTPID"] + " เวอร์ชั่น " + Session["dVSN"] + " เนื่องจากรายการนี้ถูกนำไปใช้แล้ว!!');");
                        }

                        gvw.DataBind();
                    }

                    break;

                case "edit":
                    int index1 = Convert.ToInt32(paras[1]);
                    txtTopicID.Text = gvw.GetRowValues(index1, "STOPICID").ToString();
                    txtTopicName.Text = gvw.GetRowValues(index1, "STOPICNAME").ToString();
                    txtTopicName.IsValid = true;
                    cboCal2.Value = gvw.GetRowValues(index1, "SCOL02").ToString();
                    cboCal2.IsValid = true;
                    cboCal3.Value = gvw.GetRowValues(index1, "SCOL03").ToString();
                    cboCal3.IsValid = true;
                    cboCal4.Value = gvw.GetRowValues(index1, "SCOL04").ToString();
                    cboCal4.IsValid = true;
                    cboCal6.Value = gvw.GetRowValues(index1, "SCOL06").ToString();
                    cboCal6.IsValid = true;
                    txtMultipleImpact.Text = gvw.GetRowValues(index1, "NMULTIPLEIMPACT").ToString();
                    txtMultipleImpact.IsValid = true;
                    txtDiffMultiple.Text = gvw.GetRowValues(index1, "NMULTIPLEIMPACT").ToString();
                    txtPoint.Text = gvw.GetRowValues(index1, "NPOINT").ToString();
                    txtPoint.IsValid = true;
                    txtDiffPoint.Text = gvw.GetRowValues(index1, "NPOINT").ToString();
                    txtVersion.Text = gvw.GetRowValues(index1, "SVERSION").ToString();
                    rblStatus.Value = gvw.GetRowValues(index1, "CACTIVE").ToString();
                    ListProcess(gvw.GetRowValues(index1, "CPROCESS") + "");
                    txtDescription.Text = gvw.GetRowValues(index1, "SREMARK").ToString();
                    break;

                case "DelClick":
                    string id = paras[1];

                    CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('แจ้งเตือน','ท่านต้องการลบข้อมูลนี้ ใช่หรือไม่ ?',function(s,e){ dxPopupConfirm.Hide();xcpn.PerformCallback('delete;'+ '" + id + "'); } ,function(s,e){ dxPopupConfirm.Hide(); })");

                    break;

            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;
        ASPxButton imbDel = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbDel") as ASPxButton;

        if (!CanWrite)
        {
            imbedit.Enabled = false;
            imbDel.Enabled = false;
        }
    }

    void listData()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT CPROCESS,SREMARK, sTopicID, sTopicName, sCol02, sCol03,sCol04,sCol06, nMultipleImpact,nPoint,cActive,sVersion, TOTAL_COST, TOTAL_BREAK, NVL(CAN_EDIT, 0) AS CAN_EDIT, IS_CORRUPT, BLACKLIST, TTOPIC_TYPE_ID FROM tTopic WHERE sTopicID = " + Session["oTopicID"] + " AND sVersion = " + Session["oVersion"]);
        chkProcess.DataBind();
        if (dt.Rows.Count > 0)
        {
            txtTopicID.Text = dt.Rows[0]["STOPICID"] + "";
            txtTopicName.Text = dt.Rows[0]["STOPICNAME"] + "";
            cboCal2.Value = dt.Rows[0]["SCOL02"] + "";
            cboCal3.Value = dt.Rows[0]["SCOL03"] + "";
            cboCal4.Value = dt.Rows[0]["SCOL04"] + "";
            cboCal6.Value = dt.Rows[0]["SCOL06"] + "";
            txtMultipleImpact.Text = dt.Rows[0]["NMULTIPLEIMPACT"] + "";
            txtDiffMultiple.Text = dt.Rows[0]["NMULTIPLEIMPACT"] + "";
            txtPoint.Text = dt.Rows[0]["NPOINT"] + "";
            txtDiffPoint.Text = dt.Rows[0]["NPOINT"] + "";
            txtVersion.Text = dt.Rows[0]["SVERSION"] + "";
            rblStatus.Value = dt.Rows[0]["CACTIVE"] + "";
            ListProcess(dt.Rows[0]["CPROCESS"] + "");
            txtDescription.Text = dt.Rows[0]["SREMARK"] + "";
            txtCost.Text = dt.Rows[0]["TOTAL_COST"] + "";
            txtDisableDay.Text = dt.Rows[0]["TOTAL_BREAK"] + "";
            chkBlackList.Checked = (dt.Rows[0]["BLACKLIST"].ToString() == "" || dt.Rows[0]["BLACKLIST"].ToString() == "0") ? false : true;
            chkCanEdit.Checked = (dt.Rows[0]["CAN_EDIT"].ToString() == "0") ? false : true;
            chkCheck.Checked = (dt.Rows[0]["IS_CORRUPT"].ToString() == "0") ? false : true;
            ddlTopicType.SelectedValue = dt.Rows[0]["TTOPIC_TYPE_ID"].ToString();
        }
    }

    private void InactiveData()
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            con.Open();
            string strSql = "UPDATE tTopic SET cActive = :cActive where sTopicID = :sTopicID";
            using (OracleCommand com = new OracleCommand(strSql, con))
            {
                com.Parameters.Clear();
                com.Parameters.Add(":cActive", OracleType.Char).Value = "0";
                com.Parameters.Add(":sTopicID", OracleType.Int32).Value = Convert.ToInt32(Session["oTopicID11"]);
                com.ExecuteNonQuery();
            }
        }
    }

    private string AddProcess()
    {
        string SPROCESS = "" ;
        foreach (ListEditItem ListCheck in chkProcess.Items)
        {
            if (ListCheck.Selected)
            {
                SPROCESS += "," + ListCheck.Value;
            }
        }
        return SPROCESS;
    }

    private void ListProcess(string Process)
    {
        string[] SplitProcess;
        SplitProcess = Process.Split(',');

        for (int i = 0; i < SplitProcess.Count(); i++)
        {
           ListEditItem ListItem =  chkProcess.Items.FindByValue(SplitProcess[i]);
           if (ListItem != null)
           {
               ListItem.Selected = true;
           }
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}