﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;

public partial class vendor_confirm_plan : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler

        #endregion
        if (!IsPostBack)
        {
            Session["CheckPermission"] = null;
            string AddEdit = "";
            bool chkurl = false;
            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "2")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            Session["CheckPermission"] = AddEdit;

            dteStart.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();
            BindData();

            LogUser("2", "R", "เปิดดูข้อมูลหน้า ยืนยันรถตามแผนงาน", "");
        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {
        CountConfirmPlan();

    }

    //กด แสดงเลข Record
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "Search":

                BindData();

                break;

            case "EditClick":
                int index1 = int.Parse(paras[1]);
                gvw.StartEdit(index1);
                BindData();
                break;

            case "imbClick":
                int index111 = int.Parse(paras[1]);
                int data = Convert.ToInt32(gvw.GetRowValues(index111, "NPLANID"));


                string strsql = @"SELECT P.NPLANID, P.SCOMMENT , DLVR.SHIP_TO SSHIPTO,CS.CUST_NAME,CS.PROVINCE
, (NVL(ULG,0)+NVL(ULR,0)+NVL(HSD,0)+NVL(LSD,0)+NVL(IK,0)+NVL(GH,0)+NVL(PL,0)+NVL(GH95,0)+NVL(GH95E20,0)+NVL(GH95E85,0)+NVL(HSDB5,0)+NVL(OTHER,0)+NVL(LPG,0)) NVALUE
,LPAD(pl.SDELIVERYNO,10,'0') SDELIVERYNO
 FROM (TPLANSCHEDULE p 
 INNER JOIN (SELECT NPLANID,SSHIPTO,SDELIVERYNO, SUM(nvl(NVALUE,0)) as NVALUE FROM TPlanScheduleList WHERE CACTIVE = '1' GROUP BY NPLANID,SSHIPTO,SDELIVERYNO) pl ON P.NPLANID = PL.NPLANID)
 LEFT JOIN TDELIVERY DLVR ON LPAD(DLVR.DELIVERY_NO,10,'0') =LPAD(pl.SDELIVERYNO,10,'0') 
LEFT JOIN (SELECT SHIP_TO,CUST_NAME,PROVINCE FROM TCUSTOMER_SAP GROUP BY SHIP_TO,CUST_NAME,PROVINCE)  cs ON DLVR.SHIP_TO  = cs.SHIP_TO 
WHERE p.CACTIVE = '1' AND P.NPLANID = " + data;

                DataTable dt = new DataTable();
                dt = CommonFunction.Get_Data(sql, strsql);

                if (dt.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table width='100%' border='0' cellspacing='2' cellpadding='5'>");

                    string formatPopup = @" <tr><td align='left'> ที่ : {0}   เลข Outbound : {1} ชื่อลูกค้า : {2}  <br />ปริมาณน้ำมัน : {3} ลิตร   จังหวัด : {4}</td></tr>";
                    int i = 1;
                    foreach (DataRow dr in dt.Rows)
                    {

                        sb.Append(string.Format(formatPopup, i, dr["SDELIVERYNO"] + "", dr["CUST_NAME"] + "", dr["NVALUE"] + "", dr["PROVINCE"] + ""));
                        i++;
                    }
                    sb.Append("<tr><td  align='left'>หมายเหตุ : " + dt.Rows[0]["SCOMMENT"] + "" + "</td></tr>");
                    sb.Append("</table>");

                    ((Literal)popupControl.FindControl("ltrContent")).Text = sb.ToString();
                    popupControl.ShowOnPageLoad = true;
                }

                break;
        }
    }


    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        int VisibleIndex = e.VisibleIndex;

        if (e.DataColumn.Caption == " " || e.DataColumn.Caption == "พนักงานขับรถ")
        {

            ASPxTextBox txtConfirm = (ASPxTextBox)gvw.FindRowCellTemplateControl(VisibleIndex, null, "txtConfirm");
            ASPxButton btnConfirm = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "btnConfirm");
            ASPxButton btnCancel = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "btnCancel");

            ASPxComboBox cmbPersonalNo = (ASPxComboBox)gvw.FindRowCellTemplateControl(VisibleIndex, null, "cmbPersonalNo");

            bool IsExpire = Convert.ToBoolean(e.GetValue("ISEXPIRE"));
            DateTime DPLAN = Convert.ToDateTime(e.GetValue("SPLANDATE")).Date;

            if ("" + e.GetValue("CCONFIRM") == "0") txtConfirm.Value = "0";

            txtConfirm.ClientInstanceName = txtConfirm.ClientInstanceName + "_" + VisibleIndex;
            btnConfirm.ClientInstanceName = btnConfirm.ClientInstanceName + "_" + VisibleIndex;
            btnCancel.ClientInstanceName = btnCancel.ClientInstanceName + "_" + VisibleIndex;

            //Add Event
            btnCancel.ClientSideEvents.Click = "function (s, e) { " + txtConfirm.ClientInstanceName + ".SetValue('0');  s.SetEnabled(false); " + btnConfirm.ClientInstanceName + ".SetEnabled(true); }";
            btnConfirm.ClientSideEvents.Click = "function (s, e) { " + txtConfirm.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + btnCancel.ClientInstanceName + ".SetEnabled(true);}";

            if (DPLAN == DateTime.Today) //ยืนยันภายในวันนั้น
            {
                if (IsExpire == false) //ตรวจสอบเกินกำหนด TimeLine หรือไม่
                {
                    bool IsEnabled = ("" + txtConfirm.Value == "1") ? true : false;
                    //BindingData

                    btnConfirm.ClientEnabled = !IsEnabled;
                    btnCancel.ClientEnabled = IsEnabled;
                }
                else
                {
                    txtConfirm.Value = "2";
                    cmbPersonalNo.ClientEnabled = false;
                    btnConfirm.ClientEnabled = false;
                    btnCancel.ClientEnabled = false;

                }
            }
            else if (DPLAN < DateTime.Today) //เกินกำหนด
            {
                txtConfirm.Value = "2";
                cmbPersonalNo.ClientEnabled = false;
                btnConfirm.ClientEnabled = false;
                btnCancel.ClientEnabled = false;
            }
            else if (DPLAN > DateTime.Today)  //ยืนยันแผนล่วงหน้า
            {
                bool IsEnabled = ("" + txtConfirm.Value == "1") ? true : false;
                //BindingData

                btnConfirm.ClientEnabled = !IsEnabled;
                btnCancel.ClientEnabled = IsEnabled;
            }

            //cmbPersonalNo_OnItemRequestedByValueSQL(cmbPersonalNo, new ListEditItemRequestedByValueEventArgs(e.GetValue("SEMPLOYEEID") + ""));
            cmbPersonalNo.Value = e.GetValue("SEMPLOYEEID") + "";

            if ((e.GetValue("CFIFO") + "").Equals("1"))
            {
                btnCancel.ClientVisible = false;
            }
        }
    }

    protected void gvw_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":


                DateTime DPLAN = Convert.ToDateTime(e.GetValue("SPLANDATE")).Date;

                if (!("" + e.GetValue("CCONFIRM")).Equals("1"))
                {
                    if (DPLAN == DateTime.Today) //ภายในวันนั้น
                    {
                        if (!Convert.ToBoolean(e.GetValue("ISEXPIRE"))) //ตรวจสอบว่าเกิน TimeLine หรือป่าว
                        {
                            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                        }
                        else
                        {
                            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
                        }
                    }
                    else if (DPLAN < DateTime.Today)  //เกินกำหนด
                    {
                        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
                    }
                    else if (DPLAN > DateTime.Today)  //ยืนยันล่วงหน้า
                    {
                        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                    }
                }
                else
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#CEECF5");
                }

                break;
        }
    }



    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "SORT":
                BindData();
                gvw.CancelEdit();
                break;

            case "CANCELEDIT":
                gvw.CancelEdit();
                break;

            case "CUSTOMCALLBACK":
                if ("" + Session["CheckPermission"] == "1")
                {
                    string[] para = e.Args[0].Split(';');

                    if (para[0] == "SendData")
                    {


                        using (OracleConnection con = new OracleConnection(sql))
                        {

                            con.Open();
                            string strsql = "UPDATE TPLANSCHEDULE SET CCONFIRM = :CONFIRM ,SEMPLOYEEID = :SEMPLOYEEID WHERE NPLANID = :NPLANID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                for (int i = 0; i <= gvw.VisibleRowCount - 1; i++)
                                {
                                    if (gvw.Selection.IsRowSelected(i))
                                    {
                                        string NPLANID = gvw.GetRowValues(i, "NPLANID").ToString();

                                        ASPxTextBox txtConfirm = (ASPxTextBox)gvw.FindRowCellTemplateControl(i, null, "txtConfirm");
                                        ASPxComboBox cmbPersonalNo = (ASPxComboBox)gvw.FindRowCellTemplateControl(i, null, "cmbPersonalNo");


                                        int nplanid = Convert.ToInt32(NPLANID);

                                        if (txtConfirm.Value + "" == "1")
                                        {
                                            com.Parameters.Clear();
                                            com.Parameters.Add(":NPLANID", OracleType.Number).Value = nplanid;
                                            com.Parameters.Add(":CONFIRM", OracleType.Char).Value = '1';
                                            com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value + "";


                                            com.ExecuteNonQuery();
                                        }
                                        else if (txtConfirm.Value + "" == "0")
                                        {
                                            com.Parameters.Clear();
                                            com.Parameters.Add(":NPLANID", OracleType.Number).Value = nplanid;
                                            com.Parameters.Add(":CONFIRM", OracleType.Char).Value = '0';
                                            com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value + "";

                                            com.ExecuteNonQuery();
                                        }

                                    }

                                }
                            }
                        }

                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");



                        BindData();

                        LogUser("2", "I", "บันทึกข้อมูลหน้า ยืนยันรถตามแผนงาน", "");
                    }
                    CountConfirmPlan();

                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }

                break;
        }
    }

    void BindData()
    {
        /*
SELECT ROW_NUMBER () OVER (ORDER BY NPLANID) AS ID1,p.NPLANID,p.DDELIVERY,nvl(p.CFIFO,0) AS CFIFO,p.STIMEWINDOW,TS.STERMINALNAME ,p.SHEADREGISTERNO,p.STRAILERREGISTERNO,TK.CSTATUS,p.CCONFIRM,CASE WHEN p.CCONFIRM = '1' THEN 'ยืนยันรถ' ELSE 'รอการยืนยัน' END AS SCONFIRM ,p.CSPARE,p.SHEADNO,p.STRAILERNO, p.SCOMMENT, p.DPLAN,p.SPLANDATE ,p.SEMPLOYEEID
,case when nvl(T.CSTATUS,'0') = '0' then (case when To_Char(sysdate,'dd/MM/yyyy hh24.mi') <= To_Char(sysdate,'dd/MM/yyyy') || ' ' ||  nvl(T.TPLANCONFIRM,To_Char(p.SPLANDATE   +1/24,'hh24.mi')) then 'false' else 'true' end) else

(case when To_Char(sysdate ,'dd/MM/yyyy hh24.mi') <= To_Char(p.SPLANDATE  +1/24,'dd/MM/yyyy hh24.mi') then 'false' else 'true' end) end as IsExpire

 FROM (TPLANSCHEDULE p LEFT JOIN (TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID) ON P.STERMINALID = T.STERMINALID)LEFT JOIN TTRUCK tk ON P.SHEADREGISTERNO = TK.SHEADREGISTERNO 
 WHERE p.CACTIVE = '1' AND p.SVENDORID = :SVENDORID AND nvl(P.STERMINALID,:oTerminal) LIKE '%'|| :oTerminal || '%' AND (p.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR nvl(p.STRAILERREGISTERNO,1) LIKE '%' || :oSearch || '%'  ) AND (TO_DATE(p.SPLANDATE,'dd/MM/yyyy') = TO_DATE(:oBegin,'dd/MM/yyyy') )
         */
        string strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY NPLANID) AS ID1,p.NPLANID,p.DDELIVERY,nvl(p.CFIFO,0) AS CFIFO,p.STIMEWINDOW,TS.STERMINALNAME ,p.SHEADREGISTERNO,p.STRAILERREGISTERNO,TK.CSTATUS,p.CCONFIRM,CASE WHEN p.CCONFIRM = '1' THEN 'ยืนยันรถ' ELSE 'รอการยืนยัน' END AS SCONFIRM ,p.CSPARE,p.SHEADNO,p.STRAILERNO, p.SCOMMENT, p.DPLAN,p.SPLANDATE 
,CASE WHEN p.SEMPLOYEEID IS NOT NULL THEN p.SEMPLOYEEID ELSE CONF_PERS.SEMPLOYEEID  END SEMPLOYEEID
,case when nvl(T.CSTATUS,'0') = '0' then (case when To_Char(sysdate,'dd/MM/yyyy hh24.mi') <= To_Char(sysdate,'dd/MM/yyyy') || ' ' ||  nvl(T.TPLANCONFIRM,To_Char(p.SPLANDATE   +1/24,'hh24.mi')) then 'false' else 'true' end) else
(case when To_Char(sysdate ,'dd/MM/yyyy hh24.mi') <= To_Char(p.SPLANDATE  +1/24,'dd/MM/yyyy hh24.mi') then 'false' else 'true' end) end as IsExpire
 FROM (TPLANSCHEDULE p LEFT JOIN (TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID) ON P.STERMINALID = T.STERMINALID) LEFT JOIN TTRUCK tk ON P.SHEADREGISTERNO = TK.SHEADREGISTERNO
 LEFT JOIN (
  SELECT TO_DATE(TRCK_CONF.DDATE ,'dd/MM/yyyy')+1 DDELIVERY ,DDATE,PERSONAL_CODE ,EMPL.SEMPLOYEEID
  ,TRCK_CONF_LST.STRUCKID ,TRCK_CONF_LST.SHEADREGISTERNO SHEAD ,NVL(TRCK_CONF_LST.STRAILERID,'TRxxxxxxx') STRAILERID,NVL(TRCK_CONF_LST.STRAILERREGISTERNO,'xx.nn-nnnn')  STRAILER
    FROM TTRUCKCONFIRM TRCK_CONF
    LEFT JOIN TTRUCKCONFIRMLIST TRCK_CONF_LST ON TRCK_CONF.NCONFIRMID=TRCK_CONF_LST.NCONFIRMID
    LEFT JOIN TEMPLOYEE EMPL ON EMPL.SPERSONELNO=TRCK_CONF_LST.PERSONAL_CODE
    WHERE 1=1 AND PERSONAL_CODE is not null 
    --AND TO_DATE(TRCK_CONF.DDATE ,'dd/MM/yyyy')= TO_DATE(sysdate,'dd/MM/YYYY')
 ) CONF_PERS ON TO_DATE(p.DDELIVERY ,'dd/MM/yyyy')= TO_DATE(CONF_PERS.DDELIVERY ,'dd/MM/yyyy')  AND p.SHEADREGISTERNO=CONF_PERS.SHEAD
 WHERE p.CACTIVE = '1' 
--  AND (TO_DATE(p.DDELIVERY ,'dd/MM/yyyy') >= TO_DATE(SYSDATE,'dd/MM/yyyy') )
 AND p.SVENDORID = :SVENDORID AND nvl(P.STERMINALID,:oTerminal) LIKE '%'|| :oTerminal || '%' AND (p.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR nvl(p.STRAILERREGISTERNO,1) LIKE '%' || :oSearch || '%'  ) 
 AND (TO_DATE(p.SPLANDATE,'dd/MM/yyyy') = TO_DATE(:oBegin,'dd/MM/yyyy') )

";

        if (cboStatus.SelectedIndex == 0)
        {
            strsql += " AND nvl(p.CCONFIRM,'0') = '0'";
        }
        else if (cboStatus.SelectedIndex == 1)
        {
            strsql += " AND nvl(p.CCONFIRM,'0') = '1'";
        }
        else
        {
            strsql += " ";
        }

        sds.SelectCommand = strsql;
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("SVENDORID", Session["SVDID"] + "");
        sds.SelectParameters.Add("oSearch", txtSearch.Text.Trim());
        sds.SelectParameters.Add("oTerminal", cboTerminal1.Value + "");
        sds.SelectParameters.Add("oBegin", dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
        Cache.Remove(sds.CacheKeyDependency);
        Cache[sds.CacheKeyDependency] = new object();
        sds.Select(new System.Web.UI.DataSourceSelectArguments());
        sds.DataBind();
        gvw.DataBind();
    }

    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID,FULLNAME FROM (SELECT E.SEMPLOYEEID,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND E.STRANS_ID = '" + Session["SVDID"] + "" + "' AND  E.INAME || ES.FNAME || ' ' || ES.LNAME LIKE :fillter) ";
        //sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID,FULLNAME FROM (SELECT E.SEMPLOYEEID,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE E.STRANS_ID = '" + Session["SVDID"] + "" + "' AND  E.INAME || ES.FNAME || ' ' || ES.LNAME LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        //sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        //sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    private void CountConfirmPlan()
    {
        string CountPlan = CommonFunction.Get_Value(sql, @"SELECT COUNT(NPLANID) AS NPLANCOUNT FROM TPLANSCHEDULE 
WHERE CACTIVE = '1' AND SVENDORID = '" + Session["SVDID"] + "" + "' AND NVL(CCONFIRM,'0') = '0' AND TO_DATE(SPLANDATE,'DD/MM/YYYY') = TO_DATE('" + dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') ");
        lblConfirmCar.Text = CountPlan;
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}
