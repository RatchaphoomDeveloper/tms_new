﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="UserAddEdit.aspx.cs" Inherits="UserAddEdit" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info" id="divDetail" runat="server">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract6" id="acollapseFindContract6">ข้อมูลส่วนตัว&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract6" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract6">
                    <div>
                        <br />
                        <asp:Table runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblUserGroup" runat="server" Text="กลุ่มผู้ใช้งาน :&nbsp;"></asp:Label>
                                    <asp:Label ID="lblTruckTypeRequire" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlUserGroup" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUserGroup_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:HiddenField ID="hCGroup" runat="server" />
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblVendorID" runat="server" Text="บริษัทผู้ขนส่ง/คลัง/ลูกค้า :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlVendorID" runat="server" CssClass="form-control select2" Enabled="false"></asp:DropDownList>
                                    <asp:TextBox ID="txtShipTo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    <a style="cursor: pointer;" href="#" id="aShipTo" runat="server" onserverclick="aShipTo_ServerClick">
                                        <asp:Image ID="imgShipTo" ImageUrl="~/Images/blue-37.png" Width="24px" Height="24px" runat="server" />
                                    </a>
                                    <a style="cursor: pointer;" href="#" id="aShipToClear" runat="server" onserverclick="aShipToClear_ServerClick">
                                        <asp:Image ID="imgDelete" ImageUrl="~/Images/bin1.png" Width="24px" Height="24px" runat="server" />
                                    </a>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                        &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label17" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Enabled="false"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblDivision" runat="server" Text="Division :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label16" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlDivision" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                                &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblFirstName" runat="server" Text="ชื่อ :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label2" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblLastName" runat="server" Text="นามสกุล :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label3" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                        &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblPosition" runat="server" Text="ตำแหน่ง :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label5" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtPosition" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblTelephone" runat="server" Text="โทรศัพท์ :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label8" runat="server" Text="*&nbsp;" ForeColor="Red">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtTelephone" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                        &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblUsername" runat="server" Text="Username :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label4" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblPassword" runat="server" Text="Password :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label7" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password" AutoPostBack="true" OnTextChanged="txtPassword_TextChanged"></asp:TextBox>
                                    <asp:HiddenField ID="hidOldPassword" runat="server" />
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="linkForget" Text=" - Generate Password - " runat="server" OnClick="linkForget_Click" Visible="false" />
                                    &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblEmail" runat="server" Text="Email :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label6" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label9" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                        &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="Label12" runat="server" Text="หน่วยงาน (ทีม) :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label13" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlTeam" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                      <asp:Label ID="Label18" runat="server" Text="พื้นที่รับผิดชอบ :&nbsp;"></asp:Label>
                                    <asp:Label ID="Label19" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlSaleDist" runat="server" CssClass="form-control select2" Enabled="false">
                                    </asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                        &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <br />
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-success" Visible="false" Style="width: 120px;" data-toggle="modal"
                                data-target="#ModalConfirmBeforeSave" UseSubmitBehavior="false" />&nbsp;
                  <asp:Button ID="cmdCancelDoc" runat="server" Text="ยกเลิก"
                      CssClass="btn btn-md btn-hover btn-danger" Style="width: 120px;" data-toggle="modal"
                      UseSubmitBehavior="false" data-target="#ModalConfirmBeforeCancel" />
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="hdf" runat="server" />
            <asp:HiddenField ID="hdfDateOfBirth" runat="server" />
            <asp:HiddenField ID="hdfCardID" runat="server" />
            <asp:HiddenField ID="hdfWorkStartDate" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmCancel" IDModel="ModalConfirmBeforeCancel"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmCancel_ClickOK"
        TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
    <div class="modal fade" id="ShowEvent" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updShowEvent" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Picture/imgInformation.png" Width="48" Height="48" />
                                <asp:Label ID="lblEvent" runat="server" Text="Reset Password"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:Table runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="10%">
                                        <div class="form-group">
                                            <asp:Label ID="Label10" runat="server" Text="Username :&nbsp;"></asp:Label>
                                        </div>
                                    </asp:TableCell>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="90%">
                                        <div class="form-group">
                                            <asp:TextBox ID="txtForgetUser" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="10%">
                                        <div class="form-group">
                                            <asp:Label ID="Label11" runat="server" Text="กลุ่มผู้ใช้งาน :&nbsp;"></asp:Label>
                                        </div>
                                    </asp:TableCell>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="90%">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlForgetGroup" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlForgetGroup_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="10%">
                                        <div class="form-group">
                                            <asp:Label ID="Label14" runat="server" Text="หน่วยงาน :&nbsp;"></asp:Label>
                                        </div>
                                    </asp:TableCell>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="90%">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlForgetDepartment" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="10%">
                                        <div class="form-group">
                                            <asp:Label ID="Label15" runat="server" Text="Email :&nbsp;"></asp:Label>
                                        </div>
                                    </asp:TableCell>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="90%">
                                        <div class="form-group">
                                            <asp:TextBox runat="server" ID="txtForgetEmail" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="cmdReset" runat="server" Text="ตกลง" Width="80" CssClass="btn btn-md btn-hover btn-info" OnClick="cmdReset_Click" UseSubmitBehavior="false" />
                            <asp:Button ID="CloseModal" runat="server" class="btn btn-md btn-hover btn-info" data-dismiss="modal" Width="80px" aria-hidden="true" Text="ปิด" UseSubmitBehavior="false" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="cmdReset" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="modal fade" id="ShowOTP" role="dialog" aria-labelledby="myModalLabel"
        data-keyboard="false" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updOTP" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content" style="width: auto">
                        <div class="modal-header">
                            <button type="button" id="Button6" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>
                            <h4 class="modal-title">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/blue-37.png" Width="48" Height="48" />
                                <asp:Label ID="lblOTP" runat="server" Text=""></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:Table ID="Table9" runat="server">
                                <%--<asp:TableRow>
                                    <asp:TableCell Width="520" ColumnSpan="3">
                                        <asp:Label ID="lblComplainUrgent" runat="server" Text="asdf"></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="110" ColumnSpan="3">
                                        <asp:Label ID="lblComplainUrgent2" runat="server" Text=""></asp:Label></asp:TableCell>
                                </asp:TableRow>--%>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell Width="210">
                                        <input id="txtInput" runat="server" class="form-control" style="width: 300px; text-align: center" placeholder="Ship To, Sold To" />
                                    </asp:TableCell><asp:TableCell Width="200">
                                        &nbsp;<asp:Button ID="Button1" runat="server" CssClass="btn btn-info" Text="ค้นหา" SkinID="ButtonSuccess" OnClick="cmdSearch_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:GridView ID="dgvSearch" runat="server" DataKeyNames="SHIP_ID,SOLD_ID,SHIP_NAME,SOLD_NAME" SkinID="GridNoPaging" OnRowCommand="dgvSearch_RowCommand" AutoGenerateColumns="false" GridLines="None" 
                                CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White">
                                <HeaderStyle Wrap="false" />
                                <%--<rowstyle wrap="false" />--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <a style="cursor: pointer;" href="#" id="aChoose" runat="server">
                                                <asp:ImageButton ImageUrl="~/Images/blue-23.png" Width="24px" Height="24px" runat="server" CommandName="ChooseData" />
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SHIP_ID" HeaderText="Ship To"></asp:BoundField>
                                    <asp:BoundField DataField="SHIP_NAME" HeaderText="Ship To"></asp:BoundField>
                                    <asp:BoundField DataField="SOLD_ID" HeaderText="Sold To"></asp:BoundField>
                                    <asp:BoundField DataField="SOLD_NAME" HeaderText="Sold To"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <button type="button" id="Button9" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="hdfShipID" runat="server" />
            <asp:HiddenField ID="hdfSoldID" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
