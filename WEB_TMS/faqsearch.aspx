﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="faqsearch.aspx.cs" Inherits="faqsearch" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .style13
        {
            width: 100%;
        }
        .style16
        {
            width: 83px;
        }
        .dxeBase
        {
            font: 12px Tahoma;
        }
        .style17
        {
            width: 69px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="false" ClientInstanceName="xcpn"
        CausesValidation="False">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%">
                    <tr>
                        <td align="right">
                            <dx:ASPxComboBox ID="cboQuest" runat="server" CallbackPageSize="30" ClientInstanceName="cboQuest"
                                ItemStyle-Wrap="True" EnableCallbackMode="True" OnItemRequestedByValue="cboQuest_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboQuest_ItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{1}" ValueField="FAQ_ID" Width="500px" CssClass="dxeLineBreakFix"
                                Height="25">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="FAQ_ID" Width="100px" Caption="ไอดี"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="FAQ_QUEST" Width="100px" Caption="ปัญหา"></dx:ListBoxColumn>
                                </Columns>
                                <ItemStyle Wrap="True"></ItemStyle>
                            </dx:ASPxComboBox>
                            <dx:ASPxButton ID="btnSearch" runat="server" CssClass="dxeLineBreakFix" SkinID="_search"
                                Width="60px" Height="25">
                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('')}" />
                            </dx:ASPxButton>
                            <asp:SqlDataSource ID="FAQ" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr> 
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" Width="100%" 
                                DataSourceID="sds" KeyFieldName="FAQ_ID"  PreviewFieldName="FAQ_ANSWER"  EnableRowsCache="False"
                                ClientInstanceName="gvw">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="LINE_NO" Caption="ลำดับที่" HeaderStyle-HorizontalAlign="Center"
                                        VisibleIndex="0" Width="4%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="FAQ_QUEST" Caption="ปัญหา" ReadOnly="True"
                                        CellStyle-HorizontalAlign="Left" VisibleIndex="2" Width="70%">
                                        <EditCellStyle HorizontalAlign="Left">
                                        </EditCellStyle>
                                        <DataItemTemplate>
                                            <dx:ASPxHyperLink runat="server" ID="xhplQuest" ClientInstanceName="xhplQuest" Text='<%# Eval("FAQ_QUEST") %>'>
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Detail;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                            </dx:ASPxHyperLink>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="FAQ_ANSWER" Caption="FAQ_ANSWER" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SFIRSTNAME" Caption="ผู้โพส" HeaderStyle-HorizontalAlign="Center"
                                        VisibleIndex="2" Width="8%" CellStyle-HorizontalAlign="Center" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DATE_CREATED" Caption="วันเวลาที่โพส" HeaderStyle-HorizontalAlign="Center"
                                        VisibleIndex="3" Width="8%" CellStyle-HorizontalAlign="Center" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="FAQ_ID" Caption="FAQ_ID" Visible="false" />
                                    <dx:GridViewDataTextColumn FieldName="USER_CREATED" Caption="FAQ_ID" Visible="false" />
                                </Columns>
                                <Templates>
                                    <PreviewRow>
                                        <table style="border: none">
                                            <tbody>
                                                <tr>
                                                    <td style="border: none;">
                                                        <asp:Literal ID="ltrDetailRow" runat="server" Text="<%# Container.Text %>"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="border: none;">
                                                        เอกสารแนป  :<asp:Literal ID="ltrFile" runat="server" Text=""></asp:Literal>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </PreviewRow>
                                </Templates>
                                <Settings ShowPreview="true" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                CancelSelectOnNullParameter="False" SelectCommand="SELECT ROWNUM LINE_NO, TFAQ.FAQ_ID,  TFAQ.FAQ_QUEST,  TFAQ.FAQ_ANSWER,  TFAQ.USER_CREATED,  TFAQ.DATE_CREATED, TUSER.SFIRSTNAME FROM TFAQ
LEFT JOIN TUSER ON TFAQ.USER_CREATED = TUSER.SUID WHERE FAQ_QUEST LIKE '%' || :pLINE_NO || '%'">
                                <SelectParameters>
                                    <asp:ControlParameter Name="pLINE_NO" ControlID="cboQuest" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                     
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
