﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.XtraCharts;
using System.Drawing;
using DevExpress.Web.ASPxGridView;

public partial class ReportCompareDate : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static DataTable dtDetailGrid = new DataTable();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            WebChartControl1.ObjectSelected += new HotTrackEventHandler(WebChartControl1_ObjectSelected);
            WebChartControl1.CustomCallback += new DevExpress.XtraCharts.Web.CustomCallbackEventHandler(WebChartControl1_CustomCallback);

            SetCboYear();

        }
        //   CreateReport();
    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string[] paras = e.Args[0].Split(';');
                string ss = txtSeriesName.Text.Trim();
                switch (paras[0])
                {

                    case "ListData":
                        switch (ss)
                        {
                            case "0 - 5": ListData("1");
                                break;
                            case "6 - 10": ListData("2");
                                break;
                            case "11 - 15": ListData("3");
                                break;
                            case "16 - 30": ListData("4");
                                break;
                            case "31 - 45": ListData("5");
                                break;
                            case "46 - 60": ListData("6");
                                break;
                            case "61 - 75": ListData("7");
                                break;
                            case "มากกว่า 75": ListData("8");
                                break;
                        }
                        break;
                }
                break;
        }
    }

    void WebChartControl1_ObjectSelected(object sender, HotTrackEventArgs e)
    {
        //if (e.HitInfo.InSeries)
        //{
        //    SeriesPoint currentPoint = (SeriesPoint)e.AdditionalObject;
        //    if (currentPoint.Tag.GetType() == typeof(InventoryChart.BO.CategoryItem))
        //    {
        //        InventoryChart.BO.CategoryItem currentCategory = (InventoryChart.BO.CategoryItem)currentPoint.Tag;
        //        int categoryID = currentCategory.CategoryID;
        //        DataFilter currentCategoryFilter = new DataFilter("ProductItems.CategoryID", "System.Int32", DataFilterCondition.Equal, categoryID);
        //        this.WebChartControl1.Series["ProductUnits"].DataFilters.Clear();
        //        this.WebChartControl1.Series["ProductUnits"].DataFilters.Add(currentCategoryFilter);
        //        this.WebChartControl1.Series["CategoryUnits"].Visible = false;
        //        this.WebChartControl1.Series["ProductUnits"].Visible = true;
        //        this.WebChartControl1.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.RightOutside;
        //        this.WebChartControl1.Legend.AlignmentVertical = LegendAlignmentVertical.Center;
        //    }
        //}

    }



    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": bindChart11();//CreateReport();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    //    void CreateReport()
    //    {
    //        string Condition = "";
    //        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
    //        {
    //            Condition = " AND to_char(NVL(REQUEST_DATE,CREATE_DATE),'mm') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "' AND to_char(NVL(REQUEST_DATE,CREATE_DATE),'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
    //            lblsTail.Text = "";
    //        }

    //        #region Data
    //        string Query = @"SELECT RQT.REQTYPE_ID, RQT.REQTYPE_NAME,LD.STATUS,LD.NITEm
    //FROM TBL_REQTYPE RQT
    //LEFT JOIN 
    //(
    //    SELECT '01' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    //    UNION
    //     SELECT '01' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
    //        UNION
    //     SELECT '01' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
    //     UNION
    //         SELECT '02' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    //    UNION
    //     SELECT '02' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
    //        UNION
    //     SELECT '02' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
    //          UNION
    //         SELECT '03' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    //    UNION
    //     SELECT '03' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
    //        UNION
    //     SELECT '03' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
    //          UNION
    //         SELECT '04' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    //    UNION
    //     SELECT '04' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
    //        UNION
    //     SELECT '04' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
    //)FD
    //ON FD.REQTYPE_ID = RQT.REQTYPE_ID
    //LEFT JOIN
    //(
    //    SELECT REQTYPE_ID,
    //    CASE WHEN STATUS_FLAG = '10' THEN 'สำเร็จ' WHEN STATUS_FLAG IN ('02','12','04','07') THEN 'รอดำเนินการ' WHEN STATUS_FLAG IN ('09','01','03','06') THEN 'หน่วยอื่นดำเนินการ' ELSE  '' END as STATUS,
    //    COUNT(CASE WHEN STATUS_FLAG = '10' THEN 'สำเร็จ' WHEN STATUS_FLAG IN ('02','12','04','07') THEN 'รอดำเนินการ' WHEN STATUS_FLAG IN ('09','01','03','06') THEN 'หน่วยอื่นดำเนินการ' ELSE  '' END) as NITEm
    //    FROM TBL_REQUEST
    //    WHERE 1=1 " + Condition + @"
    //    GROUP BY REQTYPE_ID,CASE WHEN STATUS_FLAG = '10' THEN 'สำเร็จ'WHEN STATUS_FLAG IN ('02','12','04','07') THEN 'รอดำเนินการ'WHEN STATUS_FLAG IN ('09','01','03','06') THEN 'หน่วยอื่นดำเนินการ'ELSE  '' END
    //)LD
    //ON LD.REQTYPE_ID = RQT.REQTYPE_ID AND  FD.STATUS = LD.STATUS
    //
    //";

    //        DataTable dt = CommonFunction.Get_Data(conn, Query);
    //        #endregion

    //        string Query1 = @"SELECT '1' as NO FROM DUAL";

    //        DataTable dsFill = CommonFunction.Get_Data(conn, Query1);

    //        string Query2 = @"   SELECT 1 as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    //    UNION
    //     SELECT 2 as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
    //        UNION
    //     SELECT 3 as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
    //    ORDER BY REQTYPE_ID";
    //        DataTable dsSerie = CommonFunction.Get_Data(conn, Query2);
    //        string Query3 = @"SELECT  REQTYPE_ID, REQTYPE_NAME, ISACTIVE_FLAG, 
    //   NORDER
    //FROM TBL_REQTYPE WHERE ISACTIVE_FLAG = 'Y' ORDER BY NORDER";
    //        DataTable dsArugument = CommonFunction.Get_Data(conn, Query3);




    //        //gvw.DataSource = dt;
    //        //gvw.DataBind();SELECT  MS.STATUSREQ_ID,MS.STATUSREQ_NAME,LDS.CSTATUS_FLAG,LDS.SMONTH,LDS.SYEAR

    //        if (dt.Rows.Count > 0)
    //        {
    //            //1
    //            rpt_CarRequest report = new rpt_CarRequest();


    //            XRChart xrChart2 = report.FindControl("xrChart1", true) as XRChart;
    //            xrChart2.Series.Clear();
    //            //add Title
    //            ChartTitle chartTitle1 = new ChartTitle();
    //            chartTitle1.Text = "จำนวนธุรกรรมให้บริการ (คำขอ)";
    //            chartTitle1.Font = new Font("Tahoma", 15);
    //            xrChart2.Titles.Add(chartTitle1);

    //            if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
    //            {
    //                ChartTitle chartTitle2 = new ChartTitle();
    //                chartTitle2.Text = "เดือน " + cboMonth.Text + " " + cboYear.Text;
    //                chartTitle2.Font = new Font("Tahoma", 13);
    //                xrChart2.Titles.Add(chartTitle2);
    //            }
    //            if (dsSerie.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dsSerie.Rows.Count; i++)
    //                {
    //                    Series series = new Series(dsSerie.Rows[i]["STATUS"] + "", ViewType.Bar);

    //                    //series.Points.Add(new SeriesPoint("A", new double[] { 10 }));
    //                    //series.Points.Add(new SeriesPoint("B", new double[] { 20 }));
    //                    //series.Points.Add(new SeriesPoint("C", new double[] { 30 }));
    //                    //series.Points.Add(new SeriesPoint("D", new double[] { 40 }));

    //                    if (dt.Rows.Count > 0)
    //                    {
    //                        if (dsArugument.Rows.Count > 0)
    //                        {
    //                            for (int k = 0; k < dsArugument.Rows.Count; k++)
    //                            {
    //                                DataRow[] dr = dt.Select("STATUS = '" + dsSerie.Rows[i]["STATUS"] + "" + "' AND REQTYPE_ID = '" + dsArugument.Rows[k]["REQTYPE_ID"] + "" + "'", "REQTYPE_ID");

    //                                if (dr.Count() > 0)
    //                                {
    //                                    foreach (DataRow row in dr)
    //                                    {
    //                                        double Value = !string.IsNullOrEmpty(row["NITEM"] + "") ? double.Parse(row["NITEM"] + "") : 1;
    //                                        switch (row["REQTYPE_ID"] + "")
    //                                        {
    //                                            case "01": series.Points.Add(new SeriesPoint("ตรวจสอบวัดน้ำ", new double[] { Value }));
    //                                                break;
    //                                            case "02": series.Points.Add(new SeriesPoint("ตรวจสอบรับรองความถูกต้อง และตีซีล", new double[] { Value }));
    //                                                break;
    //                                            case "03": series.Points.Add(new SeriesPoint("ขอสำเนาเอกสาร", new double[] { Value }));
    //                                                break;
    //                                            case "04": series.Points.Add(new SeriesPoint("ขอพ่นสาระสำคัญ", new double[] { Value }));
    //                                                break;
    //                                        }
    //                                    }
    //                                }
    //                                else
    //                                {
    //                                    double Value = 0;
    //                                    switch (dsArugument.Rows[k]["REQTYPE_ID"] + "")
    //                                    {
    //                                        case "01": series.Points.Add(new SeriesPoint("ตรวจสอบวัดน้ำ", new double[] { Value }));
    //                                            break;
    //                                        case "02": series.Points.Add(new SeriesPoint("ตรวจสอบรับรองความถูกต้อง และตีซีล", new double[] { Value }));
    //                                            break;
    //                                        case "03": series.Points.Add(new SeriesPoint("ขอสำเนาเอกสาร", new double[] { Value }));
    //                                            break;
    //                                        case "04": series.Points.Add(new SeriesPoint("ขอพ่นสาระสำคัญ", new double[] { Value }));
    //                                            break;
    //                                    }
    //                                }
    //                            }

    //                        }
    //                    }

    //                    xrChart2.Series.Add(series);
    //                }
    //            }

    //            // xrChart2.DataSource = dt;
    //            //xrChart2.SeriesDataMember = "STATUS";

    //            //for (int i = 0; i < dt.Rows.Count; i++)
    //            //{

    //            //    xrChart2.Series[0].ArgumentScaleType = ScaleType.Qualitative;
    //            //    xrChart2.Series[0].ArgumentDataMember = "REQTYPE_NAME";
    //            //    xrChart2.Series[0].ValueDataMembers[0] = "NITEM";

    //            //}
    //            report.Name = "RequestCar";
    //            string DateShow = "วันที่จัดทำรายงาน " + DateTime.Now.ToString("dd MMMM yyyy");

    //            ((XRLabel)report.FindControl("XRLabel1", true)).Text = DateShow;


    //            report.DataSource = dsFill;
    //            rvw.Report = report;
    //            rvw.DataBind();
    //            //report.CreateDocument();
    //        }

    //    }

    private void bindChart11()
    {

        //        string QUERY = @"SELECT REQ.VENDOR_ID,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 0 AND 5 THEN 1 ELSE 0 END) as Q1,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 6 AND 10 THEN 1 ELSE 0 END) as Q2,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 11 AND 15 THEN 1 ELSE 0 END) as Q3,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 16 AND 30 THEN 1 ELSE 0 END) as Q4,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 31 AND 45 THEN 1 ELSE 0 END) as Q5,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 46 AND 60 THEN 1 ELSE 0 END) as Q6,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 61 AND 75 THEN 1 ELSE 0 END) as Q7,
        //SUM(CASE WHEN TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') > 75 THEN 1 ELSE 0 END) as Q8
        //FROM TBL_REQUEST REQ
        //GROUP BY  REQ.VENDOR_ID";
        string Condition = "";

        if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
        {
            lblsTail.Text = "ประจำเดือน" + cboMonth.Text + " " + cboYear.Text + " (จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            Condition = "WHERE TO_CHAR(REQ.REQUEST_DATE, 'MM/YYYY') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "/" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }
        else
        {
            lblsTail.Text = "-";
        }



        string QUERY = @"
SELECT MS.RPTID,MS.RPTNAME,Q1.Q1,Q2.Q2,Q3.Q3,Q4.Q4,Q5.Q5,Q6.Q6,Q7.Q7,Q8.Q8 FROM TBL_RPT_COMPARE MS
LEFT JOIN 
(
    SELECT '1' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 0 AND 5 THEN 1 ELSE 0 END) as Q1
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q1 ON Q1.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '2' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 6 AND 10 THEN 1 ELSE 0 END) as Q2
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q2 ON Q2.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '3' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 11 AND 15 THEN 1 ELSE 0 END) as Q3
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q3 ON Q3.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '4' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 16 AND 30 THEN 1 ELSE 0 END) as Q4
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q4 ON Q4.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '5' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 31 AND 45 THEN 1 ELSE 0 END) as Q5
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q5 ON Q5.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '6' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 46 AND 60 THEN 1 ELSE 0 END) as Q6
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q6 ON Q6.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '7' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 61 AND 75 THEN 1 ELSE 0 END) as Q7
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q7 ON Q7.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '8' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') > 75 THEN 1 ELSE 0 END) as Q8
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q8 ON Q8.RPTID = MS.RPTID";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        // DateTimeFormatInfo mfi = new DateTimeFormatInfo();
        if (dt.Rows.Count > 0)
        {
            Series series = new Series("0 - 5", ViewType.Bar);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string COLNAME = "Q" + (i + 1);
                double Value = !string.IsNullOrEmpty(dt.Rows[i][COLNAME] + "") ? double.Parse(dt.Rows[i][COLNAME] + "") : 0;

                series.Points.Add(new SeriesPoint(dt.Rows[i]["RPTNAME"] + "", new double[] { Value }));

            }
            WebChartControl1.Series.Add(series);
            //WebChartControl1.Series.Add(new Series("0 - 5", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("6 - 10", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("11 - 15", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("16 - 30", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("31 - 35", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("46 - 60", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("61 - 75", ViewType.Bar));
            //WebChartControl1.Series.Add(new Series("มากกว่า 78", ViewType.Bar));

            WebChartControl1.Legend.Visible = false;
            WebChartControl1.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative;
            WebChartControl1.SeriesTemplate.ValueScaleType = ScaleType.Numerical;
            WebChartControl1.SeriesTemplate.PointOptions.PointView = PointView.Values;
            WebChartControl1.SeriesTemplate.LegendPointOptions.PointView = PointView.Argument;
            // ((DevExpress.XtraCharts.XYDiagram)WebChartControl1.Diagram).AxisX.Label.Angle = 90;
            WebChartControl1.DataSource = dt;

            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q1" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q2" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q3" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q4" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q5" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q6" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q7" });
            //WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Q8" });



            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";
            //WebChartControl1.Series[0].ArgumentDataMember = "RPTNAME";


            WebChartControl1.DataBind();



        }


    }

    // Drill implementation
    protected void WebChartControl1_CustomCallback(object sender, DevExpress.XtraCharts.Web.CustomCallbackEventArgs e)
    {
        int detailsLevel = Convert.ToInt32(Session["detailsLevel"]);
        string argument = e.Parameter;

        if (argument != "")
        {
            //// DrillDown
            //if (detailsLevel == 0)
            //{
            //    FilterProducts(argument);
            //    ShowProducts();
            //}
            //else
            //{
            //    if (detailsLevel == 1)
            //        FilterOrderDetails(argument);
            //    ShowOrderDetails();
            //}

            //if (detailsLevel < 2)
            //    detailsLevel++;
        }
        else
        {
            //// DrillUp
            //if (detailsLevel > 0)
            //    detailsLevel--;

            //if (detailsLevel == 0)
            //    ShowCategories();
            //else if (detailsLevel == 1)
            //    ShowProducts();
        }

        Session["detailsLevel"] = detailsLevel;
    }

    void ListData(string Case)
    {
        string Condition = "";

        if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
        {
            Condition += " AND TO_CHAR(REQ.REQUEST_DATE, 'MM/YYYY') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "/" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }

        string QData = "";

        switch (Case)
        {
            case "1":
                QData = "SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 0 AND 5 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 0 AND 5 THEN 1 ELSE 0 END <> 0";
                break;
            case "2":
                QData = "SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 6 AND 10 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 6 AND 10 THEN 1 ELSE 0 END <> 0";
                break;
            case "3":
                QData = "SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 11 AND 15 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 11 AND 15 THEN 1 ELSE 0 END <> 0";
                break;
            case "4":
                QData = "SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 16 AND 30 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 16 AND 30 THEN 1 ELSE 0 END  <> 0";
                break;
            case "5":
                QData = " SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 31 AND 45 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 31 AND 45 THEN 1 ELSE 0 END  <> 0";
                break;
            case "6":
                QData = "SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 46 AND 60 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 46 AND 60 THEN 1 ELSE 0 END <> 0";
                break;
            case "7":
                QData = "SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 61 AND 75 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 61 AND 75 THEN 1 ELSE 0 END <> 0";
                break;
            case "8":
                QData = "SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') > 75 THEN 1 ELSE 0 END)";
                Condition += " AND CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') > 75 THEN 1 ELSE 0 END  <> 0";
                break;

        }

        string QUERY = @"SELECT REQ.VENDOR_ID,TVEN.SABBREVIATION,
          " + QData + @"  as Q1
        FROM TBL_REQUEST REQ
        LEFT JOIN TVENDOR TVEN
        ON TVEN.SVENDORID = REQ.VENDOR_ID
        WHERE 1=1 " + Condition + @" 
        GROUP BY  REQ.VENDOR_ID,TVEN.SABBREVIATION";

         dtDetailGrid = CommonFunction.Get_Data(conn, QUERY);
         gvw.DataSource = dtDetailGrid;
        gvw.DataBind();
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ListReport("P");
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ListReport("E");

    }

    private void ListReport(string Type)
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
        }
        else
        {

        }



        string QUERY = @"
SELECT MS.RPTID,MS.RPTNAME,Q1.Q1,Q2.Q2,Q3.Q3,Q4.Q4,Q5.Q5,Q6.Q6,Q7.Q7,Q8.Q8 FROM TBL_RPT_COMPARE MS
LEFT JOIN 
(
    SELECT '1' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 0 AND 5 THEN 1 ELSE 0 END) as Q1
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q1 ON Q1.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '2' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 6 AND 10 THEN 1 ELSE 0 END) as Q2
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q2 ON Q2.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '3' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 11 AND 15 THEN 1 ELSE 0 END) as Q3
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q3 ON Q3.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '4' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 16 AND 30 THEN 1 ELSE 0 END) as Q4
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q4 ON Q4.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '5' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 31 AND 45 THEN 1 ELSE 0 END) as Q5
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q5 ON Q5.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '6' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 46 AND 60 THEN 1 ELSE 0 END) as Q6
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q6 ON Q6.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '7' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') BETWEEN 61 AND 75 THEN 1 ELSE 0 END) as Q7
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q7 ON Q7.RPTID = MS.RPTID
LEFT JOIN 
(
    SELECT '8' as RPTID ,SUM(CASE WHEN TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') > 75 THEN 1 ELSE 0 END) as Q8
    FROM TBL_REQUEST REQ
    " + Condition + @"
)Q8 ON Q8.RPTID = MS.RPTID

";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        rpt_CompareDate report = new rpt_CompareDate();

        XRChart xrChart2 = report.FindControl("xrChart1", true) as XRChart;
        xrChart2.Series.Clear();
        //add Title
        ChartTitle chartTitle1 = new ChartTitle();
        chartTitle1.Text = "ระยะเวลาที่ได้คิวดำเนินการกับวันที่ยื่นคำขอ (คำขอ)";
        chartTitle1.Font = new Font("Tahoma", 15);
        xrChart2.Titles.Add(chartTitle1);
        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            ChartTitle chartTitle2 = new ChartTitle();
            chartTitle2.Text = "เดือน " + cboMonth.Text + " " + cboYear.Text;
            chartTitle2.Font = new Font("Tahoma", 13);
            xrChart2.Titles.Add(chartTitle2);
        }
        // DateTimeFormatInfo mfi = new DateTimeFormatInfo();
        if (dt.Rows.Count > 0)
        {
            Series series = new Series("0 - 5", ViewType.Bar);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string COLNAME = "Q" + (i + 1);
                double Value = !string.IsNullOrEmpty(dt.Rows[i][COLNAME] + "") ? double.Parse(dt.Rows[i][COLNAME] + "") : 0;

                series.Points.Add(new SeriesPoint(dt.Rows[i]["RPTNAME"] + "", new double[] { Value }));

            }
            xrChart2.Legend.Visible = false;
            xrChart2.Series.Add(series);
        }
       
       




        #region function report

        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            ((XRLabel)report.FindControl("xrLabel2", true)).Text = "วันที่จักทำรายงาน: "+DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
        }
        else
        {
            ((XRLabel)report.FindControl("xrLabel2", true)).Text = "";
        }

        string Query1 = @"SELECT '1' as NO FROM DUAL";

        DataTable dsFill = CommonFunction.Get_Data(conn, Query1);
        report.DataSource = dsFill;
        report.Name = "สถานะการรับบริการวันนั้น";
        report.DetailReport.DataSource = dtDetailGrid;



        string fileName = "ReportCompareDate_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();


        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }

    public class BO
    {
        public int ProductID { get; set; }
        public int? CategoryID { get; set; }
        public string ProductName { get; set; }
        public int? ProductQty { get; set; }
    }
    #endregion
}