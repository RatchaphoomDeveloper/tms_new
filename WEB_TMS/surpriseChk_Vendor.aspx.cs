﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Security;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.SurpriseCheckYear;
using EmailHelper;
using TMS_BLL.Transaction.Complain;

public partial class surpriseChk_Vendor : PageBase
{
    private string ID
    {
        get
        {
            if ((string)ViewState["ID"] != null)
                return (string)ViewState["ID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ID"] = value;
        }
    }
    private DataTable dtSurpriseCheckYearList
    {
        get
        {
            if ((DataTable)ViewState["dtSurpriseCheckYearList"] != null)
                return (DataTable)ViewState["dtSurpriseCheckYearList"];
            else
                return null;
        }
        set
        {
            ViewState["dtSurpriseCheckYearList"] = value;
        }
    }
    private DataTable dtTeamCheckData
    {
        get
        {
            if ((DataTable)ViewState["dtTeamCheckData"] != null)
                return (DataTable)ViewState["dtTeamCheckData"];
            else
                return null;
        }
        set
        {
            ViewState["dtTeamCheckData"] = value;
        }
    }

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string vendorID;
    string truckID;
    int contractID;
    string strChkTruck;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            AssignAuthen();
            this.LoadVendor();
            this.LoadUser();
            this.LoadTeamCheck();
            this.LoadData();
            this.InitialForm();
        }
        if (Session["vendorID"] != null)
        {
            vendorID = Session["vendorID"].ToString();
        }
        if (Session["truckID"] != null)
        {
            truckID = Session["truckID"].ToString();
        }
        if (Session["contractID"] != null)
        {
            contractID = Convert.ToInt32(Session["contractID"]);
        }
        if (Session["strChkTruck"] != null)
        {
            strChkTruck = Session["strChkTruck"].ToString();
        }
    }

    private void InitialForm()
    {
        try
        {
            if (Request.QueryString["str"] != null)
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                this.LoadDataEdit(decryptedValue);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadVendor()
    {
        try
        {
            DataTable dt_vendor = VendorBLL.Instance.SelectName(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadData()
    {
        try
        {
            dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(" AND SVENDORID = '" + Session["SVDID"].ToString() + "'");
            GridViewHelper.BindGridView(ref dgvSurpriseCheckYear, dtSurpriseCheckYearList, false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    private void LoadUser()
    {
        try
        {
            DataTable dtUser = UserBLL.Instance.UserSelectBLL(" AND USERGROUP_ID IN (" + ConfigValue.UserGroup2 + "," + ConfigValue.UserGroup3 + "," + ConfigValue.UserGroup4 + ")");
            DropDownListHelper.BindDropDownList(ref ddlUserCheck, dtUser, "SUID", "FULLNAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadTeamCheck()
    {
        try
        {
            DataTable dtTeam = UserBLL.Instance.TeamSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlTeamCheck, dtTeam, "USERGROUP_ID", "USERGROUP_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void btnSaveY_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet("DS");
            dtTeamCheckData.TableName = "DT";
            ds.Tables.Add(dtTeamCheckData.Copy());
            SurpriseCheckYearBLL.Instance.SurpriseCheckYearSaveBLL(ID, TotalCar.Text, ddlVendor.SelectedValue, ddlContract.SelectedValue, AppointDate.Text, AppointLocate.Text, ddlUserCheck.SelectedValue, ddlTeamCheck.SelectedValue, Session["UserID"].ToString(), "1", Remark.Text, ds);
            this.SendEmail(127, "ยืนยัน");
            alertSuccess("บันทึกสำเร็จ", "surpriseChk_Vendor.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtcontract = ContractBLL.Instance.ConTractSelectByVendor(ddlVendor.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlContract, dtcontract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvSurpriseCheckYear_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvSurpriseCheckYear.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvSurpriseCheckYear, dtSurpriseCheckYearList);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseCheckYear_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            this.LoadDataEdit(dgvSurpriseCheckYear.DataKeys[e.RowIndex].Value.ToString());
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void LoadDataEdit(string DATA_ID)
    {
        try
        {
            DataTable dtSurpriseCheckYear = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(" AND ID = " + DATA_ID);
            if (dtSurpriseCheckYear.Rows.Count > 0)
            {
                TotalCar.Text = dtSurpriseCheckYear.Rows[0]["TOTALCAR"].ToString();
                ddlVendor.SelectedValue = dtSurpriseCheckYear.Rows[0]["SVENDORID"].ToString();
                ddlVendor_SelectedIndexChanged(null, null);
                ddlContract.SelectedValue = dtSurpriseCheckYear.Rows[0]["SCONTRACTID"].ToString();
                AppointDate.Text = DateTime.Parse(dtSurpriseCheckYear.Rows[0]["APPOINTDATE"].ToString()).ToString("dd/MM/yyyy HH:mm", new CultureInfo("en-US"));
                AppointLocate.Text = dtSurpriseCheckYear.Rows[0]["APPOINTLOCATE"].ToString();
                ddlUserCheck.SelectedValue = dtSurpriseCheckYear.Rows[0]["USERCHECK"].ToString();
                //ddlTeamCheck.SelectedValue = dtSurpriseCheckYear.Rows[0]["TEAMCHECK"].ToString();
                Remark.Text = dtSurpriseCheckYear.Rows[0]["REMARK"].ToString();
                this.ID = dtSurpriseCheckYear.Rows[0]["ID"].ToString();

                dtTeamCheckData = SurpriseCheckYearBLL.Instance.SurpriseCheckTeamSelect(" AND DOC_ID = " + DATA_ID);
                GridViewHelper.BindGridView(ref dgvTeamCheck, dtTeamCheckData);

                if (!string.Equals(dtSurpriseCheckYear.Rows[0]["IS_ACTIVE"].ToString(), "0"))
                {
                    btnSaveY.Enabled = false;
                    btnCancle.Enabled = false;
                }
                else
                {
                    btnSaveY.Enabled = true;
                    btnCancle.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnCancle_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();
            DataSet ds = new DataSet("DS");
            dtTeamCheckData.TableName = "DT";
            ds.Tables.Add(dtTeamCheckData.Copy());
            SurpriseCheckYearBLL.Instance.SurpriseCheckYearSaveBLL(ID, TotalCar.Text, ddlVendor.SelectedValue, ddlContract.SelectedValue, AppointDate.Text, AppointLocate.Text, ddlUserCheck.SelectedValue, ddlTeamCheck.SelectedValue, Session["UserID"].ToString(), "2", Remark.Text, ds);
            this.SendEmail(127, "ยกเลิก");
            alertSuccess("ยกเลิกสำเร็จ", "surpriseChk_Vendor.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (string.Equals(Remark.Text, string.Empty))
                throw new Exception("กรุณากรอก หมายเหตุ");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvTeamCheck_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvTeamCheck.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvTeamCheck, dtTeamCheckData);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTeamCheck_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtTeamCheckData.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvTeamCheck, dtTeamCheckData);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTeamCheck_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    private void SendEmail(int TemplateID, string STATUS)
    {
        //return; // ไม่ต้องส่ง E-mail

        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == 127)
                {
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem.ToString());
                    Body = Body.Replace("{TOTALCAR}", TotalCar.Text.Trim());
                    Body = Body.Replace("{APPOINTDATE}", AppointDate.Text.Trim());
                    Body = Body.Replace("{APPOINTLOCATE}", AppointLocate.Text.Trim());
                    Body = Body.Replace("{STATUS}", STATUS);
                    Body = Body.Replace("{REMARK}", Remark.Text);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/surpriseChk_ShowData.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(this.ID), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,kritsada.h@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true), Subject, Body, "", "SurpriseCheck3", ColumnEmailName);
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }
}