﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="TruckWater.aspx.cs"
    Inherits="TruckWater" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphHeader">
</asp:Content>
<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="cph_Main">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <script type="text/javascript">
        //Get Value From Truck
        function GetHDATA(sV) {
            var retVal = 0.00;
            switch (sV) {
                case 'nWeight':
                    if (document.getElementById('cph_Main_xcpnHead_rpnHInfomation_txtHnWeight_I') != null)
                        retVal = document.getElementById('cph_Main_xcpnHead_rpnHInfomation_txtHnWeight_I').value;
                    break;
                case 'nDensity':
                    if (document.getElementById('cph_Main_xcpnHead_rpnHInfomation_cboHProdGRP_VI') != null)
                        retVal = document.getElementById('cph_Main_xcpnHead_rpnHInfomation_cboHProdGRP_VI').value;
                    break;
                case 'nCapacity':
                    if (document.getElementById('cph_Main_xcpnHead_rpnHInfomation_xlbHnTatolCapacity') != null)
                        retVal = document.getElementById('cph_Main_xcpnHead_rpnHInfomation_xlbHnTatolCapacity').outerText;
            }
            if (retVal == null || retVal == '')
                retVal = 0;
            return retVal.toString().replace(',', '');
        }
        //Get Value From Trailer
        function GetRDATA(sV) {
            var retVal = 0.00;
            switch (sV) {
                case 'nWeight':
                    if (document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_txtRnWeight_I') != null)
                        retVal = document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_txtRnWeight_I').value;
                    break;
                case 'nDensity':
                    if (document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_cboRProdGRP_VI') != null)
                        retVal = document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_cboRProdGRP_VI').value;
                    break;
                case 'nCapacity':
                    if (document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_xlbRnTatolCapacity') != null)
                        retVal = document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_xlbRnTatolCapacity').outerText;
                    break;
            }
            if (retVal == null || retVal == '')
                retVal = 0;
            return retVal.toString().replace(',', '');
        }
        //Insert Comma (,)
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
    <dx:ASPxTextBox ID="txtGlobal_STRUCKID" runat="server" ClientInstanceName="txtGlobal_STRUCKID" Visible="false" />
    <dx:ASPxTextBox ID="txtGlobal_RTRUCKID" runat="server" ClientInstanceName="txtGlobal_RTRUCKID" Visible="false" />
    <dx:ASPxTextBox ID="txtGlobal_TRUCKID" runat="server" ClientInstanceName="txtGlobal_TRUCKID" Visible="false" />
    <dx:ASPxTextBox ID="txtGlobal_SNITEM" runat="server" ClientInstanceName="txtGlobal_SNITEM" Visible="false" />
    <dx:ASPxTextBox ID="txtGlobal_RNITEM" runat="server" ClientInstanceName="txtGlobal_RNITEM" Visible="false" />
    <dx:ASPxTextBox ID="txtGlobal_TNITEM" runat="server" ClientInstanceName="txtGlobal_TNITEM" Visible="false" />
    <dx:ASPxCallbackPanel ID="xcpnMain" runat="server" CausesValidation="False" HideContentOnCallback="False"
        ClientInstanceName="xcpnMain" OnCallback="xcpnMain_Callback">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                    <tr>
                        <td align="left" style="padding-left: 25px;">
                            <dx:ASPxLabel ID="xlbHead" runat="Server" CssClass="highlight" Text="รายละเอียดข้อมูลรถ" />
                        </td>
                        <td align="right" width="150px">
                            <dx:ASPxButton ID="xbnEditMode" runat="Server" Text="เปลี่ยนเป็นโหมดแก้ไข" CausesValidation="false" OnClick="xbnEditMode_Click">
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="xbnViewMode" runat="Server" Text="เปลี่ยนเป็นโหมดเรียกดู" CausesValidation="false"
                                OnClick="xbnViewMode_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="125px">
                            <dx:ASPxButton ID="xbnLastHis" runat="Server" ClientInstanceName="xbnLastHis" Text="ประวัติรถที่สมบูรณ์"
                                AutoPostBack="false" CausesValidation="false">
                                <ClientSideEvents Click="function (s, e) { xcpnMain.PerformCallback('FullData');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <br />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <div id="dvHead" runat="Server">
        <dx:ASPxCallbackPanel ID="xcpnHead" runat="server" CausesValidation="False" HideContentOnCallback="False"
            ClientInstanceName="xcpnHead" OnCallback="xcpnHead_Callback">
            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxRoundPanel ID="rpnHInfomation" runat="Server" ClientInstanceName="rpnHInfomation" Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctHInfomation" runat="Server">
                                <asp:SqlDataSource ID="sdsSTRUCK" runat="server" OnInserted="sdsSTRUCK_Inserted" OnInserting="sdsSTRUCK_Inserting"
                                    EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdSTRUCK" SelectCommand="SELECT T.* ,TY.SCARTYPENAME ,TY.SCARCATEGORY ,C.SCONTRACTID CONTRACKID,C.SVENDORID,C.SCONTRACTNO  FROM TTRUCK T 
                                    LEFT JOIN TTRUCKTYPE TY ON T.SCARTYPEID=TY.SCARTYPEID  
                                    LEFT JOIN TCONTRACT_TRUCK CT ON T.STRUCKID=CT.STRUCKID AND T.STRAILERID=CT.STRAILERID
                                    LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID WHERE T.STRUCKID = :STRUCK_ID" InsertCommand="INSERT INTO TTRUCK_HIS
(STRUCKID,NITEM,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE,SHOLDERID,SHOLDERNAME,SWUPDATE,DWUPDATE,TRUCK_CATEGORY,VOL_UOM,VEH_TEXT) 

(SELECT STRUCKID,:NITEM
,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE,SHOLDERID,SHOLDERNAME,SWUPDATE,DWUPDATE,TRUCK_CATEGORY,VOL_UOM,VEH_TEXT
FROM TTRUCK WHERE STRUCKID=:STRUCK_ID)">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                    </SelectParameters>
                                    <InsertParameters>
                                        <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                        <asp:ControlParameter ControlID="txtGlobal_SNITEM" Name="NITEM" PropertyName="Text" />
                                    </InsertParameters>
                                </asp:SqlDataSource>
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="24%" style="background-color: #f9d0b5;">
                                            ทะเบียนรถ <font color="#FF0000">*</font>: </td>
                                        <td align="left" width="26%" style="background-color: #f9d0b5;">
                                            <dx:ASPxTextBox ID="txtHRegNo" runat="Server" Width="180px" ClientInstanceName="txtHRegNo" Enabled="False" />
                                        </td>
                                        <td align="right" width="24%">
                                            วันที่ขึ้นทะเบียนกับปตท. <font color="#FF0000">*</font>: </td>
                                        <td align="left" width="26%">
                                            <dx:ASPxDateEdit ID="xDetdHRegNo" runat="server" Width="150px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                NullText="ขึ้นทะเบียนกับปตทเมื่อ" ClientInstanceName="xDetdHRegNo">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุวันที่ขึ้นทะเบียนกับปตท" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ผู้ถือกรรมสิทธิ์ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cmbHsHolder" runat="Server" ClientInstanceName="cmbHsHolder" Width="230px" EnableCallbackMode="True"
                                                OnItemRequestedByValue="cmbHsHolder_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cmbHsHolder_OnItemsRequestedByFilterConditionSQL"
                                                SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID">
                                                <ClientSideEvents SelectedIndexChanged="function(s,e){ VendorID=document.getElementById('cph_Main_xcpnContract_rpnHolding_txtVenderID_I');
                                                                                                       MyHVendorID=document.getElementById('cph_Main_xcpnHead_rpnHInfomation_cmbHsHolder_VI');
                                                                                                       MyRVendorID=document.getElementById('cph_Main_xcpnTrail_rpnRInfomation_cmbRsHolder_VI');
                                                                                                        if(VendorID.value !='' && VendorID.value+' - '+xlbsTransport.GetValue() != MyHVendorID.value){ 
                                                                                                            dxWarningRedirect('แจ้งเตือน','ผู้ถือกรรมสิทธิ์จะต้องเป็นบริษัทเดียวกันกับผู้ข่นส่ง('+xlbsTransport.GetValue()+')'
                                                                                                                                ,function(s,e){ cmbHsHolder.SetValue(VendorID.value+' - '+xlbsTransport.GetValue()); if(MyRVendorID!=null) cmbRsHolder.SetValue(VendorID.value+' - '+xlbsTransport.GetValue());  return; }); 
                                                                                                        }
                                                                                                        else{ if(MyRVendorID!=null){ 
                                                                                                                if(MyHVendorID.value!=MyRVendorID.value) MyRVendorID.value=MyHVendorID.value; }
                                                                                                             } 
                                                                                                       }" />
                                                <Columns>
                                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="360px" />
                                                </Columns>
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุผู้ถือกรรมสิทธิ์" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                            <asp:SqlDataSource ID="sdsVendor" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                CacheKeyDependency="ckdVendor" SelectCommand="SELECt * FROM TVENDOR WHERE NVL(CACTIVE,'1')='1'">
                                            </asp:SqlDataSource>
                                        </td>
                                        <td align="right">
                                            วันที่จดทะเบียนครั้งแรก <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxDateEdit ID="xDetdHFRegNo" runat="server" Width="150px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                NullText="จดทะเบียนครั้งแรกเมื่อ" ClientInstanceName="xDetdHFRegNo">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุวันที่จดทะเบียนครั้งแรก" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <span id="lblHTru_Cate" runat="Server">ประเภทรถ <font color="#FF0000">*</font>:</span> </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtHTru_Cate" runat="Server" ClientInstanceName="txtHTru_Cate" Width="100px" MaxLength="15">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุประเภทรถ" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            อายุการใช้งาน : </td>
                                        <td align="left">
                                            <dx:ASPxLabel ID="xlbHnPeriod" runat="Server" ClientInstanceName="xlbHnPeriod" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            VEH_TEXT </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtVEH_Text" runat="Server" ClientInstanceName="txtVEH_Text" Width="200px" MaxLength="50">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            &nbsp; </td>
                                        <td align="left">
                                            &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            หมายเลขแชสซีย์ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtHsChasis" runat="Server" Width="180px" ClientInstanceName="txtHsChasis" MaxLength="20">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุหมายเลขแชสซีย์" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            หมายเลขเครื่องยนต์ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtHsEngine" runat="Server" Width="180px" ClientInstanceName="txtHsEngine" MaxLength="20">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุหมายเลขเครื่องยนต์" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ยี่ห้อ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboHsBrand" runat="Server" Width="180px" ClientInstanceName="cboHsBrand" ValueField="BRANDID"
                                                TextField="BRANDNAME">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุยี่ห้อ" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                            <asp:SqlDataSource ID="sdsTBrand" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                CacheKeyDependency="ckdTBrand" SelectCommand="SELECt 0,null BRANDID,' - เลือก - ' BRANDNAME FROM TTRUCK_BRAND UNION SELECt ROWNUM,BRANDID,BRANDNAME FROM TTRUCK_BRAND">
                                            </asp:SqlDataSource>
                                        </td>
                                        <td align="right">
                                            รุ่น : </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtHsModel" runat="Server" Width="180px" ClientInstanceName="txtHsModel" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            จำนวนล้อ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtHnWheel" runat="Server" Width="180px" ClientInstanceName="txtHnWheel">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุจำนวนล้อ" />
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;ล้อ </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            กำลังเครื่องยนต์ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtHPowermover" runat="Server" Width="180px" ClientInstanceName="txtHPowermover">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุกำลังเครื่องยนต์" />
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;แรงม้า </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtHnShaftDriven" runat="Server" Width="180px" ClientInstanceName="txtHnShaftDriven">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุจำนวนเพลา" />
                                                    <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            ระบบกันสะเทือน : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboHsVibration" runat="Server" Width="180px" ClientInstanceName="cboHsVibration">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="แหนบ" Text="แหนบ" />
                                                    <dx:ListEditItem Value="ถุงลม" Text="ถุงลม" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ปั๊ม Power ที่ตัวรถ : </td>
                                        <td align="left">
                                            <dx:ASPxRadioButtonList ID="rblHPumpPower" runat="Server" ClientInstanceName="rblHPumpPower" RepeatDirection="Horizontal"
                                                Border-BorderWidth="0" Paddings-Padding="0">
                                                <Items>
                                                    <dx:ListEditItem Value="มี" Text="มี" />
                                                    <dx:ListEditItem Value="ไม่มี" Text="ไม่มี" />
                                                </Items>
                                            </dx:ASPxRadioButtonList>
                                        </td>
                                        <td align="right">
                                            ชนิดของปั๊ม Power : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboHPumpPower_type" runat="Server" Width="180px" ClientInstanceName="cboHPumpPower_type">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="เกียร์ปั๊ม" Text="เกียร์ปั๊ม" />
                                                    <dx:ListEditItem Value="ไฟเบอร์โลตารี" Text="ไฟเบอร์โลตารี" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp; </td>
                                        <td align="right">
                                            วัสดุที่สร้าง Pressure ของปั๊ม Power : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboHMaterialOfPressure" runat="Server" Width="180px" ClientInstanceName="cboHMaterialOfPressure">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="ทองหลือง" Text="ทองหลือง" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ชนิดของวาล์วใต้ท้อง : </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxRadioButtonList ID="rblHValveType" runat="server" RepeatDirection="Horizontal" Border-BorderWidth="0"
                                                            Paddings-Padding="0" ClientInstanceName="rblHValveType">
                                                            <ClientSideEvents SelectedIndexChanged="function (s, e) { if(rblHValveType.GetValue()=='Other') txtHValveType.SetEnabled(true); else txtHValveType.SetEnabled(false); }" />
                                                            <Items>
                                                                <dx:ListEditItem Value="วาล์วสลิง" Text="วาล์วสลิง" />
                                                                <dx:ListEditItem Value="วาล์วลม" Text="วาล์วลม" />
                                                                <dx:ListEditItem Value="Other" Text="วาล์วอื่นๆโปรดระบุ" />
                                                            </Items>
                                                        </dx:ASPxRadioButtonList>
                                                    </td>
                                                    <td>
                                                        <font color="#FF0000">*</font>:&nbsp; </td>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtHValveType" runat="Server" Width="180px" ClientInstanceName="txtHValveType" MaxLength="50">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุชนิดของวาล์วใต้ท้อง" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ประเภทของเชื้อเพลิง : </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxRadioButtonList ID="rblHFuelType" runat="server" RepeatDirection="Horizontal" Border-BorderWidth="0"
                                                            Paddings-Padding="0" ClientInstanceName="rblHFuelType">
                                                            <ClientSideEvents SelectedIndexChanged="function (s, e) { if(rblHFuelType.GetValue()=='Other') txtHFuelType.SetEnabled(true); else txtHFuelType.SetEnabled(false);  }" />
                                                            <Items>
                                                                <dx:ListEditItem Value="DIESEL" Text="DIESEL" />
                                                                <dx:ListEditItem Value="NGV" Text="NGV" />
                                                                <dx:ListEditItem Value="Other" Text="อื่นๆโปรดระบุ" />
                                                            </Items>
                                                        </dx:ASPxRadioButtonList>
                                                    </td>
                                                    <td>
                                                        <font color="#FF0000">*</font>:&nbsp; </td>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtHFuelType" runat="Server" Width="180px" ClientInstanceName="txtHFuelType" MaxLength="20">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุประเภทของเชื้อเพลิง" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            หน่วยความจุเชื้อเพลิง : </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtVEH_Volume" runat="Server" ClientInstanceName="txtVEH_Volume" Width="200px" MaxLength="12">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            &nbsp; </td>
                                        <td align="left">
                                            &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            บริษัทที่ให้บริการระบบGPS <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboHGPSProvider" runat="Server" Width="180px" ClientInstanceName="cboHGPSProvider"
                                                ValueField="GPSID" TextField="GPSNAME">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุผู้ให้บริการระบบGPS" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                            <asp:SqlDataSource ID="sdsTGPS" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                CacheKeyDependency="ckdTGPS" SelectCommand="SELECt 0,null GPSID,' - เลือก - ' GPSNAME FROM TTRUCK_GPS UNION SELECt ROWNUM,GPSID,GPSNAME FROM TTRUCK_GPS">
                                            </asp:SqlDataSource>
                                        </td>
                                        <td align="right">
                                            น้ำหนักรถ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtHnWeight" runat="Server" Width="180px" ClientInstanceName="txtHnWeight">
                                                            <ClientSideEvents LostFocus="function(s,e){ var Hdensity=GetHDATA('nDensity'), Rdensity=GetRDATA('nDensity');
                                                                                                        var HnCapacity=GetHDATA('nCapacity'), RnCapacity=GetRDATA('nCapacity');
                                                                                                        var HnWeight=GetHDATA('nWeight'), RnWeight=GetRDATA('nWeight');
                                                                                                        var HnProdWeight= (parseFloat(HnCapacity) * parseFloat(Hdensity)), RnProdWeight= (parseFloat(RnCapacity) * parseFloat(Rdensity));
                                                                                                        var HnTotal= (parseFloat(HnWeight) + parseFloat(HnProdWeight)),RnTotal= (parseFloat(HnWeight) + parseFloat(RnWeight) + parseFloat(RnProdWeight));
                                                                                                        if(document.getElementById('cph_Main_xcpnHead_rpnHInfomation_xlbHnLoadWeight') != null && document.getElementById('cph_Main_xcpnHead_rpnHInfomation_xlbHCalcWeight') != null){
                                                                                                            xlbHnLoadWeight.SetValue(numberWithCommas(HnProdWeight));
                                                                                                            xlbHCalcWeight.SetValue(numberWithCommas(HnTotal));
                                                                                                            }
                                                                                                        if(document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_xlbRnLoadWeight') != null && document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_xlbRCalcWeight') != null){
                                                                                                            document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_xlbRnLoadWeight').outerText = numberWithCommas(RnProdWeight);
                                                                                                            document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_xlbRCalcWeight').outerText = numberWithCommas(RnTotal);
                                                                                                            } 
                                                                                                         }" />
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุน้ำหนักรถ" />
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;กก. </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <table id="tblCapacity" runat="Server" border="0" cellspacing="2" cellpadding="3" width="100%">
                                                <tr>
                                                    <td align="right" valign="top">
                                                        <br />
                                                        รายละเอียดความจุ : </td>
                                                    <td align="left" colspan="3">
                                                        <br />
                                                        <asp:SqlDataSource ID="sdsSTRUCKCompart" runat="server" OnInserted="sdsSTRUCKCompart_Inserted" OnInserting="sdsSTRUCKCompart_Inserting"
                                                            OnDeleted="sdsRTRUCKCompart_Deleted" OnDeleting="sdsRTRUCKCompart_Deleting" EnableCaching="True"
                                                            ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                            CancelSelectOnNullParameter="False" CacheKeyDependency="ckdSTRUCKCompart" SelectCommand="SELECT * FROM TTRUCK_COMPART WHERE STRUCKID = :STRUCK_ID  "
                                                            InsertCommand="INSERT INTO TTRUCK_COMPART_HIS(STRUCKID,NITEM,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE) 
                                                 (SELECT STRUCKID,:NITEM,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE FROM TTRUCK_COMPART WHERE STRUCKID=:STRUCK_ID)"
                                                            DeleteCommand="DELETE FROM TTRUCK_COMPART WHERE STRUCKID=:STRUCK_ID">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                            </SelectParameters>
                                                            <InsertParameters>
                                                                <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                                <asp:ControlParameter ControlID="txtGlobal_SNITEM" Name="NITEM" PropertyName="Text" />
                                                            </InsertParameters>
                                                            <DeleteParameters>
                                                                <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                            </DeleteParameters>
                                                        </asp:SqlDataSource>
                                                        <dx:ASPxLabel ID="xlbHnCap" runat="Server" ClientInstanceName="xlbHnCap" />
                                                        <dx:ASPxGridView ID="gvwHCompart" runat="server" ClientInstanceName="gvwHCompart" AutoGenerateColumns="False"
                                                            KeyFieldName="NCOMPARTNO" SkinID="_gvw">
                                                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = ''; if(s.cpCalc != undefined) xcpnHead.PerformCallback('Calc;'); }" />
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FieldName="NCOMPARTNO" Caption="ช่องที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <EditFormSettings Visible="False" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NPANLEVEL1" Caption="ความจุที่ระดับแป้น1 (ลิตร)" Width="17%" VisibleIndex="1">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NPANLEVEL2" Caption="ความจุที่ระดับแป้น2 (ลิตร)" Width="17%" VisibleIndex="1">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NPANLEVEL3" Caption="ความจุที่ระดับแป้น3 (ลิตร)" Width="17%" VisibleIndex="1">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="5%">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxButton ID="btnEditCompart" ClientInstanceName="btnEditCompart" runat="server" ToolTip="ปรับปรุง/แก้ไขรายการ"
                                                                            CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                            SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                            <ClientSideEvents Click="function (s, e) { gvwHCompart.PerformCallback('STARTEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                            <Image Width="25px" Height="25px" Url="Images/imagesCA1C5HPZ.jpg" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnDelCompart" ClientInstanceName="btnDelCompart" runat="server" ToolTip="ลบรายการ"
                                                                            CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                            SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                            <ClientSideEvents Click="function (s, e) { gvwHCompart.PerformCallback('DELCOMPART;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                            <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" ShowInCustomizationForm="false" />
                                                                <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" ShowInCustomizationForm="false" />
                                                                <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" ShowInCustomizationForm="false" />
                                                                <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" ShowInCustomizationForm="false" />
                                                            </Columns>
                                                            <Templates>
                                                                <EditForm>
                                                                    <div style="text-align: center;">
                                                                        <table width="90%">
                                                                            <tr>
                                                                                <td style="width: 25%;" align="right">
                                                                                    ช่องที่ : </td>
                                                                                <td style="width: 40%;" align="left">
                                                                                    <dx:ASPxTextBox ID="txtNCOMPARTNO" runat="server" ClientInstanceName="txtNCOMPARTNO" Width="250px">
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addHCompart">
                                                                                            <ErrorFrameStyle ForeColor="Red" />
                                                                                            <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td style="width: 25%;">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    ความจุที่ระดับแป้น1 : </td>
                                                                                <td align="left">
                                                                                    <dx:ASPxTextBox ID="txtNPANLEVEL1" runat="server" ClientInstanceName="txtNPANLEVEL1" Width="250px">
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addHCompart">
                                                                                            <ErrorFrameStyle ForeColor="Red" />
                                                                                            <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td align="left">
                                                                                    &nbsp;ลิตร</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    ความจุที่ระดับแป้น2 : </td>
                                                                                <td align="left">
                                                                                    <dx:ASPxTextBox ID="txtNPANLEVEL2" runat="server" ClientInstanceName="txtNPANLEVEL2" Width="250px">
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addHCompart">
                                                                                            <ErrorFrameStyle ForeColor="Red" />
                                                                                            <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td align="left">
                                                                                    &nbsp;ลิตร</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    ความจุที่ระดับแป้น3 : </td>
                                                                                <td align="left">
                                                                                    <dx:ASPxTextBox ID="txtNPANLEVEL3" runat="server" ClientInstanceName="txtNPANLEVEL3" Width="250px" ReadOnly="true">
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addHCompart">
                                                                                            <ErrorFrameStyle ForeColor="Red" />
                                                                                            <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td align="left">
                                                                                    &nbsp;ลิตร</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <dx:ASPxButton ID="btnSaveCompart" ClientInstanceName="btnSaveCompart" runat="server" CausesValidation="true"
                                                                                        ValidationGroup="addHCompart" SkinID="_confirm" CssClass="dxeLineBreakFix">
                                                                                        <ClientSideEvents Click="function (s, e) { if(ASPxClientEdit.ValidateGroup('addHCompart')){ gvwHCompart.PerformCallback('SAVECOMPART;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)) }else{ return false;}  }" />
                                                                                    </dx:ASPxButton>
                                                                                    <dx:ASPxButton ID="btnCancelSave" ClientInstanceName="btnCancelSave" runat="server" SkinID="_cancel"
                                                                                        CssClass="dxeLineBreakFix" CausesValidation="false" AutoPostBack="false">
                                                                                        <ClientSideEvents Click="function (s, e) {  gvwHCompart.PerformCallback('CANCELEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)) }" />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </EditForm>
                                                            </Templates>
                                                            <Settings ShowStatusBar="Auto" />
                                                            <SettingsPager Mode="ShowAllRecords" />
                                                            <SettingsLoadingPanel Mode="ShowAsPopup" Text="Please Wait..." />
                                                            <SettingsEditing Mode="EditFormAndDisplayRow" />
                                                        </dx:ASPxGridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td colspan="3">
                                                        <table id="tblHNewCap" runat="server">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnNewHCompart" ClientInstanceName="btnNewHCompart" runat="server" ToolTip="เพิ่มช่องความจุแป้น"
                                                                        CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                        <ClientSideEvents Click="function (s, e) { gvwHCompart.PerformCallback('NEWCOMPART;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                        <Image Width="25px" Height="25px" Url="Images/imagesCA1EXDYW.jpg" />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td valign="middle">
                                                                    เพิ่มช่องความจุแป้นอื่นๆ </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        จำนวนช่อง : </td>
                                                    <td align="left">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxLabel ID="xlbHnSlot" runat="Server" ClientInstanceName="xlbHnSlot" EncodeHtml="false" />
                                                                </td>
                                                                <td style="padding-left: 5px;">
                                                                    ช่อง </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="right">
                                                        ความจุรวม : </td>
                                                    <td align="left">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxLabel ID="xlbHnTatolCapacity" runat="Server" ClientInstanceName="xlbHnTatolCapacity" />
                                                                </td>
                                                                <td style="padding-left: 5px;">
                                                                    ลิตร </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        วัสดุที่ใช้ทำตัวถังบรรทุก : </td>
                                                    <td align="left">
                                                        <dx:ASPxComboBox ID="cboHTankMaterial" runat="Server" Width="180px" ClientInstanceName="cboHTankMaterial">
                                                            <Items>
                                                                <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                                <dx:ListEditItem Value="STEEL" Text="STEEL" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td align="right">
                                                        วิธีการเติมน้ำมัน : </td>
                                                    <td align="left">
                                                        <dx:ASPxComboBox ID="cboHLoadMethod" runat="server" Width="180px" ClientInstanceName="cboHLoadMethod">
                                                            <Items>
                                                                <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                                <dx:ListEditItem Value="Top Loading" Text="Top Loading" />
                                                                <dx:ListEditItem Value="Bottom Loading" Text="Bottom Loading" />
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        บริษัทผู้ผลิตตัวถัง : </td>
                                                    <td align="left" colspan="3">
                                                        <dx:ASPxTextBox ID="txtHTank_Maker" runat="Server" Width="230px" ClientInstanceName="txtHTank_Maker"
                                                            MaxLength="50" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        กลุ่มผลิตภัณฑ์ที่บรรทุก : </td>
                                                    <td align="left" colspan="3">
                                                        <dx:ASPxComboBox ID="cboHProdGRP" runat="Server" ClientInstanceName="cboHProdGRP" Width="230px" EnableCallbackMode="True"
                                                            OnItemRequestedByValue="cboHProdGRP_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboHProdGRP_OnItemsRequestedByFilterConditionSQL"
                                                            SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="DENSITY">
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){ var density=GetHDATA('nDensity'), nCapacity=GetHDATA('nCapacity'), HnWeight=GetHDATA('nWeight');
                                                                                                        var nProdWeight= parseFloat(nCapacity) * parseFloat(density);
                                                                                                        var nTotal= parseFloat(HnWeight) + parseFloat(nProdWeight);
                                                                                                        xlbHnLoadWeight.SetValue(numberWithCommas(nProdWeight));
                                                                                                        xlbHCalcWeight.SetValue(numberWithCommas(nTotal));}" />
                                                            <Columns>
                                                                <dx:ListBoxColumn Caption="รหัสผลิตภัณฑ์" FieldName="PROD_ID" Width="80px" />
                                                                <dx:ListBoxColumn Caption="ชื่อผลิตภัณฑ์" FieldName="PROD_NAME" Width="110px" />
                                                                <dx:ListBoxColumn Caption="ประเภท" FieldName="PROD_CATEGORY" Width="50px" />
                                                                <dx:ListBoxColumn Caption="ความหนาแน่น" FieldName="DENSITY" Width="70px" />
                                                            </Columns>
                                                        </dx:ASPxComboBox>
                                                        <asp:SqlDataSource ID="sdsProdGRP" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                            CacheKeyDependency="ckdProdGRP" SelectCommand="SELECT PROS.PROD_ID,PROS.PROD_NAME,PRO.PROD_CATEGORY,PRO.DENSITY 
FROM TPRODUCT PRO 
LEFT  JOIN TPRODUCT_SAP PROS ON PRO.PROD_ID=PROS.PROD_ID
"></asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        น้ำหนักผลิตภัณฑ์ที่บรรทุก : </td>
                                                    <td align="left" colspan="3">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxLabel ID="xlbHnLoadWeight" runat="Server" ClientInstanceName="xlbHnLoadWeight" />
                                                                </td>
                                                                <td style="padding-left: 5px;">
                                                                    กก. </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        น้ำหนักรถรวมผลิตภัณฑ์ : </td>
                                                    <td align="left" colspan="3">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxLabel ID="xlbHCalcWeight" runat="Server" ClientInstanceName="xlbHCalcWeight" />
                                                                </td>
                                                                <td style="padding-left: 5px;">
                                                                    กก. </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:SqlDataSource ID="sdsSTRUCKDoc" runat="server" OnInserted="sdsSTRUCKDoc_Inserted" OnInserting="sdsSTRUCKDoc_Inserting"
                                                            EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                            CancelSelectOnNullParameter="False" CacheKeyDependency="ckdSTRUCKDoc" SelectCommand="SELECT TTRUCK_DOC.*,'UploadFile/Truck/'|| STRUCKID ||'/INFO/' SPATH,'0' CNEW FROM TTRUCK_DOC WHERE STRUCKID = :STRUCK_ID AND CACTIVE='1'"
                                                            InsertCommand="INSERT INTO TTRUCK_DOC_HIS(DOCID,STRUCKID,NITEM,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) 
                                                 (SELECT DOCID,STRUCKID,:NITEM,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME FROM TTRUCK_DOC WHERE STRUCKID=:STRUCK_ID)">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                            </SelectParameters>
                                                            <InsertParameters>
                                                                <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                                <asp:ControlParameter ControlID="txtGlobal_SNITEM" Name="NITEM" PropertyName="Text" />
                                                            </InsertParameters>
                                                        </asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารใบจดทะเบียนรถ <font color="#FF0000">*</font>: </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldHDoc01" runat="Server" Width="380px" ClientInstanceName="uldHDoc01" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldHDoc01_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwSTRUCKDoc1.PerformCallback('BIND_TRUCK_HINFODOC1;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnHDoc01" runat="Server" SkinID="_upload" ClientInstanceName="xbnHDoc01" CssClass="dxeLineBreakFix"
                                                            AutoPostBack="false" CausesValidation="false">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldHDoc01.GetText()==''){msg+='<br>แนบเอกสารใบจดทะเบียนรถ';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldHDoc01.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <dx:ASPxGridView ID="gvwSTRUCKDoc1" ClientInstanceName="gvwSTRUCKDoc1" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc1.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc1.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc1.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบหลักฐานการเสียภาษี <font color="#FF0000">*</font>: </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldHDoc02" runat="Server" Width="380px" ClientInstanceName="uldHDoc02" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldHDoc02_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwSTRUCKDoc2.PerformCallback('BIND_TRUCK_HINFODOC2;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnHDoc02" runat="Server" SkinID="_upload" AutoPostBack="false" CausesValidation="false"
                                                            ClientInstanceName="xbnHDoc02" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldHDoc02.GetText()==''){msg+='<br>แนบหลักฐานการเสียภาษี';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldHDoc02.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwSTRUCKDoc2" ClientInstanceName="gvwSTRUCKDoc2" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc2.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc2.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc2.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสาร Compartment : </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldHDoc03" runat="Server" Width="380px" ClientInstanceName="uldHDoc03" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldHDoc03_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwSTRUCKDoc3.PerformCallback('BIND_TRUCK_HINFODOC3;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnHDoc03" runat="Server" SkinID="_upload" ClientInstanceName="xbnHDoc03" CausesValidation="false"
                                                            AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldHDoc03.GetText()==''){msg+='<br>แนบเอกสาร Compartment';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldHDoc03.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwSTRUCKDoc3" ClientInstanceName="gvwSTRUCKDoc3" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc3.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc3.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc3.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารใบจดทะเบียนรถครั้งแรก <font color="#FF0000">*</font>: </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldHDoc04" runat="Server" Width="380px" ClientInstanceName="uldHDoc04" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldHDoc04_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwSTRUCKDoc4.PerformCallback('BIND_TRUCK_HINFODOC4;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnHDoc04" runat="Server" SkinID="_upload" ClientInstanceName="xbnHDoc04" CausesValidation="false"
                                                            AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldHDoc04.GetText()==''){msg+='<br>แนบเอกสารใบจดทะเบียนรถครั้งแรก';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldHDoc04.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwSTRUCKDoc4" ClientInstanceName="gvwSTRUCKDoc4" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc4.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc4.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKDoc4.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                    <asp:SqlDataSource ID="sdsSTRUCKINSURE" runat="server" OnInserted="sdsSTRUCKINSURE_Inserted" OnInserting="sdsSTRUCKINSURE_Inserting"
                        EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdSTRUCKINSURE" SelectCommand="SELECT * FROM TTRUCK_INSURANCE WHERE STRUCKID = :STRUCK_ID "
                        InsertCommand="INSERT INTO TTRUCK_INSURANCE_HIS(STRUCKID,NITEM,CTYPE,SCOMPANY,START_DURATION,END_DURATION,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE) 
                         (SELECT STRUCKID,:NITEM,CTYPE,SCOMPANY,START_DURATION,END_DURATION,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE 
                        FROM TTRUCK_INSURANCE WHERE STRUCKID=:STRUCK_ID)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                        </SelectParameters>
                        <InsertParameters>
                            <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtGlobal_SNITEM" Name="NITEM" PropertyName="Text" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="sdsSTRUCKISDoc" runat="server" OnInserted="sdsSTRUCKISDoc_Inserted" OnInserting="sdsSTRUCKISDoc_Inserting"
                        EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdSTRUCKISDoc" SelectCommand="SELECT TTRUCK_INSUREDOC.*,'UploadFile/Truck/'|| STRUCKID ||'/'|| CTYPE ||'/' SPATH,'0' CNEW FROM TTRUCK_INSUREDOC WHERE STRUCKID = :STRUCK_ID AND CACTIVE='1'"
                        InsertCommand="INSERT INTO TTRUCK_INSUREDOC_HIS(DOCID,STRUCKID,NITEM,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) 
                         (SELECT DOCID,STRUCKID,:NITEM,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME FROM TTRUCK_INSUREDOC WHERE STRUCKID=:STRUCK_ID)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                        </SelectParameters>
                        <InsertParameters>
                            <asp:ControlParameter ControlID="txtGlobal_STRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtGlobal_SNITEM" Name="NITEM" PropertyName="Text" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxRoundPanel ID="rpnHStatute" runat="Server" ClientInstanceName="rpnHStatute" Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctHStatute" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="24%">
                                            บริษัทประกันภัย : </td>
                                        <td align="left" width="76%">
                                            <dx:ASPxTextBox ID="txtHSCompany" runat="Server" ClientInstanceName="txtHSCompany" Width="230px" MaxLength="100" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ระยะเวลาอายุ พรบ. : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxDateEdit ID="xDetHSDuration_Start" runat="server" Width="90px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                            NullText="ระหว่างวันที่" ClientInstanceName="xDetHSDuration_Start">
                                                        </dx:ASPxDateEdit>
                                                    </td>
                                                    <td style="padding-left: 5px; padding-right: 5px;">
                                                        ถึง </td>
                                                    <td>
                                                        <dx:ASPxDateEdit ID="xDetHSDuration_End" runat="server" Width="90px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                            NullText="ถึงวันที่" ClientInstanceName="xDetHSDuration_End">
                                                        </dx:ASPxDateEdit>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            รายละเอียดเพิ่มเติม : </td>
                                        <td align="left">
                                            <dx:ASPxMemo ID="xMemHSDetail" runat="Server" Width="300px" Rows="3" ClientInstanceName="xMemHSDetail">
                                            </dx:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารประกันภัย : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldHSDoc" runat="Server" Width="380px" ClientInstanceName="uldHSDoc" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldHSDoc_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwSTRUCKSDoc.PerformCallback('BIND_TRUCK_HSTATUTEDOC;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnHSDoc" runat="Server" SkinID="_upload" ClientInstanceName="xbnHSDoc" CssClass="dxeLineBreakFix"
                                                            AutoPostBack="false" CausesValidation="false">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldHSDoc.GetText()==''){msg+='<br>แนบเอกสารประกันภัย';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldHSDoc.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwSTRUCKSDoc" ClientInstanceName="gvwSTRUCKSDoc" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKSDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKSDoc.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKSDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CTYPE" Caption="CTYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                    <dx:ASPxRoundPanel ID="rpnHInsurance" runat="Server" ClientInstanceName="rpnHInsurance" Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctHInsurance" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="24%">
                                            บริษัทประกันภัย : </td>
                                        <td align="left" width="76%">
                                            <dx:ASPxTextBox ID="txtHICompany" runat="Server" ClientInstanceName="txtHICompany" Width="230px" MaxLength="100" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ทุนประกันภัย : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtHIBudget" runat="Server" ClientInstanceName="txtHIBudget" Width="230px">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        บาท </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ประเภทประกันภัย : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cbmHIType" runat="Server" ClientInstanceName="cbmHIType" Width="230px">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text="- เลือก -" />
                                                    <dx:ListEditItem Value="ประกันภัยชั้น1" Text="ประกันภัยชั้น1" />
                                                    <dx:ListEditItem Value="ประกันภัยชั้น3" Text="ประกันภัยชั้น3" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ความคุ้มครอง : </td>
                                        <td align="left">
                                            <dx:ASPxCheckBoxList ID="chkHIHolding" runat="Server" ClientInstanceName="chkHIHolding" RepeatDirection="Horizontal"
                                                Border-BorderWidth="0" Paddings-Padding="0">
                                                <Items>
                                                    <dx:ListEditItem Value="หัวลาก" Text="หัวลาก" />
                                                    <dx:ListEditItem Value="บุคคลที่3" Text="บุคคลที่3" />
                                                    <dx:ListEditItem Value="สินค้า" Text="สินค้า" />
                                                </Items>
                                            </dx:ASPxCheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            เบี้ยประกันต่อปี : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtHInInsure" runat="Server" ClientInstanceName="txtHInInsure" Width="230px">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        บาท </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            รายละเอียดเพิ่มเติม : </td>
                                        <td align="left">
                                            <dx:ASPxMemo ID="xMemHIDetail" runat="Server" ClientInstanceName="xMemHIDetail" Width="300px" Rows="3">
                                            </dx:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารประกันภัย : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldHIDoc" runat="Server" ClientInstanceName="uldHIDoc" Width="380px" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldHIDoc_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwSTRUCKIDoc.PerformCallback('BIND_TRUCK_HINSUREDOC;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnHIDoc" runat="Server" ClientInstanceName="xbnHIDoc" SkinID="_upload" CssClass="dxeLineBreakFix"
                                                            AutoPostBack="false" CausesValidation="false">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldHIDoc.GetText()==''){msg+='<br>แนบเอกสารประกันภัย';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldHIDoc.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwSTRUCKIDoc" ClientInstanceName="gvwSTRUCKIDoc" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKIDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKIDoc.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwSTRUCKIDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CTYPE" Caption="CTYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <div id="dvTrail" runat="Server">
        <dx:ASPxCallbackPanel ID="xcpnTrail" runat="server" CausesValidation="False" HideContentOnCallback="False"
            ClientInstanceName="xcpnTrail" OnCallback="xcpnTrail_Callback">
            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxRoundPanel ID="rpnTInfomation" runat="Server" ClientInstanceName="rpnTInfomation" Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctTInfomation" runat="Server">
                                <asp:SqlDataSource ID="sdsRTRUCK" runat="server" OnInserted="sdsRTRUCK_Inserted" OnInserting="sdsRTRUCK_Inserting"
                                    EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdRTRUCK" SelectCommand="SELECT T.* ,TY.SCARTYPENAME ,TY.SCARCATEGORY ,C.SCONTRACTID CONTRACKID,C.SVENDORID,C.SCONTRACTNO FROM TTRUCK T 
                                    LEFT JOIN TTRUCKTYPE TY ON T.SCARTYPEID=TY.SCARTYPEID 
                                    LEFT JOIN TCONTRACT_TRUCK CT ON T.STRUCKID=CT.STRAILERID AND T.SHEADID=CT.STRUCKID
                                    LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID  WHERE T.STRUCKID = :STRUCK_ID " InsertCommand="INSERT INTO TTRUCK_HIS
(STRUCKID,NITEM,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE,SHOLDERID,SHOLDERNAME,SWUPDATE,DWUPDATE,TRUCK_CATEGORY,VOL_UOM,VEH_TEXT) 

(SELECT STRUCKID,:NITEM
,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE,SHOLDERID,SHOLDERNAME,SWUPDATE,DWUPDATE,TRUCK_CATEGORY,VOL_UOM,VEH_TEXT
FROM TTRUCK WHERE STRUCKID=:STRUCK_ID)">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                    </SelectParameters>
                                    <InsertParameters>
                                        <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                        <asp:ControlParameter ControlID="txtGlobal_RNITEM" Name="NITEM" PropertyName="Text" />
                                    </InsertParameters>
                                </asp:SqlDataSource>
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="24%">
                                            ทะเบียนรถ <font color="#FF0000">*</font>: </td>
                                        <td align="left" width="26%">
                                            <dx:ASPxTextBox ID="txtRRegNo" runat="Server" Width="180px" ClientInstanceName="txtRRegNo" Enabled="False" />
                                        </td>
                                        <td align="right" width="24%">
                                            วันที่ขึ้นทะเบียนกับปตท. <font color="#FF0000">*</font>: </td>
                                        <td align="left" width="26%">
                                            <dx:ASPxDateEdit ID="xDetdRRegNo" runat="server" Width="90px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                NullText="ขึ้นทะเบียนกับปตท.เมื่อ" ClientInstanceName="xDetdRRegNo">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุวันที่ขึ้นทะเบียนกับปตท." />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ผู้ถือกรรมสิทธิ์ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cmbRsHolder" runat="Server" ClientInstanceName="cmbRsHolder" Width="230px" EnableCallbackMode="True"
                                                OnItemRequestedByValue="cmbRsHolder_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cmbRsHolder_OnItemsRequestedByFilterConditionSQL"
                                                SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID">
                                                <ClientSideEvents SelectedIndexChanged="function(s,e){  var VendorID=document.getElementById('cph_Main_xcpnContract_rpnHolding_txtVenderID_I');
                                                                                                        var MyHVendorID=document.getElementById('cph_Main_xcpnHead_rpnHInfomation_cmbHsHolder_VI');
                                                                                                        var MyRVendorID=document.getElementById('cph_Main_xcpnTrail_rpnTInfomation_cmbRsHolder_VI');
                                                                                                        if(VendorID.value !='' && VendorID.value+' - '+xlbsTransport.GetValue() != MyRVendorID.value){ 
	                                                                                                        dxWarningRedirect('แจ้งเตือน','ผู้ถือกรรมสิทธิ์จะต้องเป็นบริษัทเดียวกันกับผู้ข่นส่ง('+xlbsTransport.GetValue()+')'
		                                                                                                        ,function(s,e){ cmbRsHolder.SetValue(VendorID.value+' - '+xlbsTransport.GetValue()); if(MyHVendorID!=null) cmbHsHolder.SetValue(VendorID.value+' - '+xlbsTransport.GetValue()); return; }); 
                                                                                                                 }
                                                                                                         else{ if(MyHVendorID!=null){ 
		                                                                                                        if(MyHVendorID.value!=MyRVendorID.value) MyHVendorID.value=MyRVendorID.value; 
			                                                                                                        } 
                                                                                                         } 
                                                                                                        }" />
                                                <Columns>
                                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="360px" />
                                                </Columns>
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุผู้ถือกรรมสิทธิ์" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td align="right">
                                            วันที่จดทะเบียนครั้งแรก <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxDateEdit ID="xDetdRFRegNo" runat="server" Width="150px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                NullText="จดทะเบียนครั้งแรกเมื่อ" ClientInstanceName="xDetdRFRegNo">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุวันที่จดทะเบียนครั้งแรก" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ประเภทรถ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtRTru_Cate" runat="Server" ClientInstanceName="txtRTru_Cate" Width="100px" MaxLength="15">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุประเภทรถ" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            อายุการใช้งาน : </td>
                                        <td align="left">
                                            <dx:ASPxLabel ID="xlbRnPeriod" runat="Server" ClientInstanceName="xlbRnPeriod" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            TU_TEXT </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtTU_Text" runat="Server" ClientInstanceName="txtTU_Text" Width="200px" MaxLength="50">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            &nbsp; </td>
                                        <td align="left">
                                            &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            หมายเลขแชสซีย์ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtRsChasis" runat="Server" Width="180px" ClientInstanceName="txtRsChasis" MaxLength="20">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุหมายเลขแชสซีย์" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            หมายเลขเครื่องยนต์ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtRsEngine" runat="Server" Width="180px" ClientInstanceName="txtRsEngine" MaxLength="20">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุหมายเลขเครื่องยนต์" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ยี่ห้อ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboRsBrand" runat="Server" Width="180px" ClientInstanceName="cboRsBrand" ValueField="BRANDID"
                                                TextField="BRANDNAME">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุยี่ห้อ" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td align="right">
                                            รุ่น : </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtRsModel" runat="Server" Width="180px" ClientInstanceName="txtRsModel" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            จำนวนล้อ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtRnWheel" runat="Server" Width="180px" ClientInstanceName="txtRnWheel">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุจำนวนล้อ" />
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;ล้อ </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            กำลังเครื่องยนต์ : </td>
                                        <td align="left">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtRPowermover" runat="Server" Width="180px" ClientInstanceName="txtRPowermover">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;แรงม้า </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtRnShaftDriven" runat="Server" Width="180px" ClientInstanceName="txtRnShaftDriven">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุจำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก" />
                                                    <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            ระบบกันสะเทือน : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboRsVibration" runat="Server" Width="180px" ClientInstanceName="cboRsVibration">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="แหนบ" Text="แหนบ" />
                                                    <dx:ListEditItem Value="ถุงลม" Text="ถุงลม" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ปั๊ม Power ที่ตัวรถ : </td>
                                        <td align="left">
                                            <dx:ASPxRadioButtonList ID="rblRPumpPower" runat="Server" ClientInstanceName="rblRPumpPower" RepeatDirection="Horizontal"
                                                Border-BorderWidth="0" Paddings-Padding="0">
                                                <Items>
                                                    <dx:ListEditItem Value="มี" Text="มี" />
                                                    <dx:ListEditItem Value="ไม่มี" Text="ไม่มี" />
                                                </Items>
                                            </dx:ASPxRadioButtonList>
                                        </td>
                                        <td align="right">
                                            ชนิดของปั๊ม Power : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboRPumpPower_type" runat="Server" ClientInstanceName="cboRPumpPower_type" Width="180px">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="เกียร์ปั๊ม" Text="เกียร์ปั๊ม" />
                                                    <dx:ListEditItem Value="ไฟเบอร์โลตารี" Text="ไฟเบอร์โลตารี" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp; </td>
                                        <td align="right">
                                            วัสดุที่สร้าง Pressure ของปั๊ม Power : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboRMaterialOfPressure" runat="Server" ClientInstanceName="cboRMaterialOfPressure"
                                                Width="180px">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="ทองหลือง" Text="ทองหลือง" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ชนิดของวาล์วใต้ท้อง : </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxRadioButtonList ID="rblRValveType" runat="server" RepeatDirection="Horizontal" Border-BorderWidth="0"
                                                            Paddings-Padding="0" ClientInstanceName="rblRValveType">
                                                            <ClientSideEvents SelectedIndexChanged="function (s, e) {  if(rblRValveType.GetValue()=='Other') txtRValveType.SetEnabled(true); else txtRValveType.SetEnabled(false);  }" />
                                                            <Items>
                                                                <dx:ListEditItem Value="วาล์วสลิง" Text="วาล์วสลิง" />
                                                                <dx:ListEditItem Value="วาล์วลม" Text="วาล์วลม" />
                                                                <dx:ListEditItem Value="Other" Text="วาล์วอื่นๆโปรดระบุ" />
                                                            </Items>
                                                        </dx:ASPxRadioButtonList>
                                                    </td>
                                                    <td>
                                                        :&nbsp; </td>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtRValveType" runat="Server" Width="180px" ClientInstanceName="txtRValveType" MaxLength="50" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ประเภทของเชื้อเพลิง : </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxRadioButtonList ID="rblRFuelType" runat="server" RepeatDirection="Horizontal" Border-BorderWidth="0"
                                                            Paddings-Padding="0" ClientInstanceName="rblRFuelType">
                                                            <ClientSideEvents SelectedIndexChanged="function (s, e) {  if(rblRFuelType.GetValue()=='Other') txtRFuelType.SetEnabled(true); else txtRFuelType.SetEnabled(false);  }" />
                                                            <Items>
                                                                <dx:ListEditItem Value="DIESEL" Text="DIESEL" />
                                                                <dx:ListEditItem Value="NGV" Text="NGV" />
                                                                <dx:ListEditItem Value="Other" Text="อื่นๆโปรดระบุ" />
                                                            </Items>
                                                        </dx:ASPxRadioButtonList>
                                                    </td>
                                                    <td>
                                                        :&nbsp; </td>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtRFuelType" runat="Server" Width="180px" ClientInstanceName="txtRFuelType" MaxLength="20" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            หน่วยความจุเชื้อเพลิง : </td>
                                        <td align="left">
                                            <dx:ASPxTextBox ID="txtTU_Volume" runat="Server" ClientInstanceName="txtTU_Volume" Width="200px" MaxLength="12">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td align="right">
                                            &nbsp; </td>
                                        <td align="left">
                                            &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            บริษัทที่ให้บริการระบบGPS : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboRGPSProvider" runat="Server" Width="180px" ClientInstanceName="cboRGPSProvider"
                                                ValueField="GPSID" TextField="GPSNAME">
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td align="right">
                                            น้ำหนักรถ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtRnWeight" runat="Server" Width="180px" ClientInstanceName="txtRnWeight">
                                                            <ClientSideEvents LostFocus="function(s,e){ var density=GetRDATA('nDensity'),nCapacity=GetRDATA('nCapacity'),RnWeight=GetRDATA('nWeight');
                                                                                                        var HnWeight=GetHDATA('nWeight');
                                                                                                        var nProdWeight= parseFloat(nCapacity) * parseFloat(density);
                                                                                                        var nTotal= parseFloat(HnWeight) + parseFloat(RnWeight) + parseFloat(nProdWeight);
                                                                                                        xlbRnLoadWeight.SetValue(numberWithCommas(nProdWeight)); xlbRCalcWeight.SetValue(numberWithCommas(nTotal)); }" />
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุน้ำหนักรถ" />
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        &nbsp;กก. </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            <br />
                                            รายละเอียดความจุ : </td>
                                        <td align="left" colspan="3">
                                            <br />
                                            <asp:SqlDataSource ID="sdsRTRUCKCompart" runat="server" OnInserted="sdsRTRUCKCompart_Inserted" OnInserting="sdsRTRUCKCompart_Inserting"
                                                OnDeleted="sdsRTRUCKCompart_Deleted" OnDeleting="sdsRTRUCKCompart_Deleting" EnableCaching="True"
                                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                CancelSelectOnNullParameter="False" CacheKeyDependency="ckdRTRUCKCompart" SelectCommand="SELECT * FROM TTRUCK_COMPART WHERE STRUCKID = :STRUCK_ID "
                                                InsertCommand="INSERT INTO TTRUCK_COMPART_HIS(STRUCKID,NITEM,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE) 
                                                 (SELECT STRUCKID,:NITEM,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE FROM TTRUCK_COMPART WHERE STRUCKID=:STRUCK_ID)"
                                                DeleteCommand="DELETE FROM TTRUCK_COMPART WHERE STRUCKID=:STRUCK_ID">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                </SelectParameters>
                                                <InsertParameters>
                                                    <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                    <asp:ControlParameter ControlID="txtGlobal_RNITEM" Name="NITEM" PropertyName="Text" />
                                                </InsertParameters>
                                                <DeleteParameters>
                                                    <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                </DeleteParameters>
                                            </asp:SqlDataSource>
                                            <dx:ASPxLabel ID="xlbRnCap" runat="Server" ClientInstanceName="xlbRnCap" />
                                            <dx:ASPxGridView ID="gvwRCompart" runat="server" ClientInstanceName="gvwRCompart" AutoGenerateColumns="False"
                                                KeyFieldName="NCOMPARTNO" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = ''; if(s.cpCalc != undefined) xcpnTrail.PerformCallback('Calc;'+GetHDATA('nWeight')); }" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="NCOMPARTNO" Caption="ช่องที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NPANLEVEL1" Caption="ความจุที่ระดับแป้น1 (ลิตร)" Width="17%" VisibleIndex="1">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NPANLEVEL2" Caption="ความจุที่ระดับแป้น2 (ลิตร)" Width="17%" VisibleIndex="1">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NPANLEVEL3" Caption="ความจุที่ระดับแป้น3 (ลิตร)" Width="17%" VisibleIndex="1">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="5%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnEditCompart" ClientInstanceName="btnEditCompart" runat="server" ToolTip="ปรับปรุง/แก้ไขรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRCompart.PerformCallback('STARTEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/imagesCA1C5HPZ.jpg" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelCompart" ClientInstanceName="btnDelCompart" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRCompart.PerformCallback('DELCOMPART;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" ShowInCustomizationForm="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" ShowInCustomizationForm="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" ShowInCustomizationForm="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" ShowInCustomizationForm="false" />
                                                </Columns>
                                                <Templates>
                                                    <EditForm>
                                                        <div style="text-align: center;">
                                                            <table width="90%">
                                                                <tr>
                                                                    <td style="width: 25%;" align="right">
                                                                        ช่องที่ : </td>
                                                                    <td style="width: 40%;" align="left">
                                                                        <dx:ASPxTextBox ID="txtNCOMPARTNO" runat="server" ClientInstanceName="txtNCOMPARTNO" Width="250px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addHCompart">
                                                                                <ErrorFrameStyle ForeColor="Red" />
                                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 25%;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        ความจุที่ระดับแป้น1 : </td>
                                                                    <td align="left">
                                                                        <dx:ASPxTextBox ID="txtNPANLEVEL1" runat="server" ClientInstanceName="txtNPANLEVEL1" Width="250px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addRCompart">
                                                                                <ErrorFrameStyle ForeColor="Red" />
                                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;ลิตร</td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        ความจุที่ระดับแป้น2 : </td>
                                                                    <td align="left">
                                                                        <dx:ASPxTextBox ID="txtNPANLEVEL2" runat="server" ClientInstanceName="txtNPANLEVEL2" Width="250px">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addRCompart">
                                                                                <ErrorFrameStyle ForeColor="Red" />
                                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;ลิตร</td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        ความจุที่ระดับแป้น3 : </td>
                                                                    <td align="left">
                                                                        <dx:ASPxTextBox ID="txtNPANLEVEL3" runat="server" ClientInstanceName="txtNPANLEVEL3" Width="250px" ReadOnly="true">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addRCompart">
                                                                                <ErrorFrameStyle ForeColor="Red" />
                                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;ลิตร</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left">
                                                                        <dx:ASPxButton ID="btnSaveCompart" ClientInstanceName="btnSaveCompart" runat="server" CausesValidation="true"
                                                                            ValidationGroup="addRCompart" SkinID="_confirm" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { if(ASPxClientEdit.ValidateGroup('addRCompart')){ gvwRCompart.PerformCallback('SAVECOMPART;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }else{ return false;} }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnCancelSave" ClientInstanceName="btnCancelSave" runat="server" SkinID="_cancel"
                                                                            CssClass="dxeLineBreakFix" CausesValidation="false" AutoPostBack="false">
                                                                            <ClientSideEvents Click="function (s, e) {  gvwRCompart.PerformCallback('CANCELEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)) }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </EditForm>
                                                </Templates>
                                                <Settings ShowStatusBar="Auto" />
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <SettingsLoadingPanel Mode="ShowAsPopup" Text="Please Wait..." />
                                                <SettingsEditing Mode="EditFormAndDisplayRow" />
                                            </dx:ASPxGridView>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="3">
                                            <table id="tblRNewCap" runat="server">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxButton ID="btnNewRCompart" ClientInstanceName="btnNewRCompart" runat="server" ToolTip="เพิ่มช่องความจุแป้น"
                                                            CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                            SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                            <ClientSideEvents Click="function (s, e) { gvwRCompart.PerformCallback('NEWCOMPART;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                            <Image Width="25px" Height="25px" Url="Images/imagesCA1EXDYW.jpg" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td valign="middle">
                                                        เพิ่มช่องความจุแป้นอื่นๆ </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            จำนวนช่อง : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxLabel ID="xlbRnSlot" runat="Server" ClientInstanceName="xlbRnSlot" />
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        ช่อง </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right">
                                            ความจุรวม : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxLabel ID="xlbRnTatolCapacity" runat="Server" ClientInstanceName="xlbRnTatolCapacity" />
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        ลิตร </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            วัสดุที่ใช้ทำตัวถังบรรทุก <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboRTankMaterial" runat="Server" Width="230px" ClientInstanceName="cboRTankMaterial">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="STEEL" Text="STEEL" />
                                                </Items>
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุวัสดุที่ใช้ทำตัวถังบรรทุก" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td align="right">
                                            วิธีการเติมน้ำมัน <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cboRLoadMethod" runat="server" ClientInstanceName="cboRLoadMethod">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text=" - เลือก - " />
                                                    <dx:ListEditItem Value="Top Loading" Text="Top Loading" />
                                                    <dx:ListEditItem Value="Bottom Loading" Text="Bottom Loading" />
                                                </Items>
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุวิธีการเติมน้ำมัน" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            บริษัทผู้ผลิตตัวถัง : </td>
                                        <td align="left" colspan="3">
                                            <dx:ASPxTextBox ID="txtRTank_Maker" runat="Server" Width="230px" ClientInstanceName="txtRTank_Maker"
                                                MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            กลุ่มผลิตภัณฑ์ที่บรรทุก : </td>
                                        <td align="left" colspan="3">
                                            <dx:ASPxComboBox ID="cboRProdGRP" runat="Server" ClientInstanceName="cboRProdGRP" Width="200px" EnableCallbackMode="True"
                                                OnItemRequestedByValue="cboRProdGRP_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboRProdGRP_OnItemsRequestedByFilterConditionSQL"
                                                SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="DENSITY">
                                                <ClientSideEvents SelectedIndexChanged="function(s,e){ var density=GetRDATA('nDensity'),nCapacity=GetRDATA('nCapacity'),RnWeight=GetRDATA('nWeight');
                                                                                                        var HnWeight=GetHDATA('nWeight');
                                                                                                        var nProdWeight= parseFloat(nCapacity) * parseFloat(density);
                                                                                                        var nTotal= parseFloat(HnWeight) + parseFloat(RnWeight) + parseFloat(nProdWeight);
                                                                                                        xlbRnLoadWeight.SetValue(numberWithCommas(nProdWeight)); xlbRCalcWeight.SetValue(numberWithCommas(nTotal)); }" />
                                                <Columns>
                                                    <dx:ListBoxColumn Caption="รหัสผลิตภัณฑ์" FieldName="PROD_ID" Width="80px" />
                                                    <dx:ListBoxColumn Caption="ชื่อผลิตภัณฑ์" FieldName="PROD_NAME" Width="110px" />
                                                    <dx:ListBoxColumn Caption="ประเภท" FieldName="PROD_CATEGORY" Width="50px" />
                                                    <dx:ListBoxColumn Caption="ความหนาแน่น" FieldName="DENSITY" Width="70px" />
                                                </Columns>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            น้ำหนักผลิตภัณฑ์ที่บรรทุก : </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxLabel ID="xlbRnLoadWeight" runat="Server" ClientInstanceName="xlbRnLoadWeight" />
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        กก. </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            น้ำหนักรถรวมผลิตภัณฑ์ : </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxLabel ID="xlbRCalcWeight" runat="Server" ClientInstanceName="xlbRCalcWeight" />
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        กก. </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารใบจดทะเบียนรถ <font color="#FF0000">*</font>: </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldRDoc01" runat="Server" Width="380px" ClientInstanceName="uldRDoc01" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldRDoc01_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwRTRUCKDoc1.PerformCallback('BIND_TRUCK_RINFODOC1;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnRDoc01" runat="Server" SkinID="_upload" ClientInstanceName="xbnRDoc01" CausesValidation="false"
                                                            AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldRDoc01.GetText()==''){msg+='<br>แนบเอกสารใบจดทะเบียนรถ';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldRDoc01.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:SqlDataSource ID="sdsRTRUCKDoc" runat="server" OnInserted="sdsRTRUCKDoc_Inserted" OnInserting="sdsRTRUCKDoc_Inserting"
                                                EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                CancelSelectOnNullParameter="False" CacheKeyDependency="ckdRTRUCKDoc" SelectCommand="SELECT TTRUCK_DOC.*,'UploadFile/Truck/'|| STRUCKID ||'/INFO/' SPATH,'0' CNEW FROM TTRUCK_DOC WHERE STRUCKID = :STRUCK_ID AND CACTIVE='1'"
                                                InsertCommand="INSERT INTO TTRUCK_DOC_HIS(DOCID,STRUCKID,NITEM,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) 
                                                 (SELECT DOCID,STRUCKID,:NITEM,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME FROM TTRUCK_DOC WHERE STRUCKID=:STRUCK_ID)">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                </SelectParameters>
                                                <InsertParameters>
                                                    <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                    <asp:ControlParameter ControlID="txtGlobal_RNITEM" Name="NITEM" PropertyName="Text" />
                                                </InsertParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <dx:ASPxGridView ID="gvwRTRUCKDoc1" ClientInstanceName="gvwRTRUCKDoc1" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc1.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc1.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc1.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบหลักฐานการเสียภาษี <font color="#FF0000">*</font>: </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldRDoc02" runat="Server" Width="380px" ClientInstanceName="uldRDoc02" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldRDoc02_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{ gvwRTRUCKDoc2.PerformCallback('BIND_TRUCK_RINFODOC2;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnRDoc02" runat="Server" SkinID="_upload" ClientInstanceName="xbnRDoc02" CausesValidation="false"
                                                            AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldRDoc02.GetText()==''){msg+='<br>แนบหลักฐานการเสียภาษี';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldRDoc02.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwRTRUCKDoc2" ClientInstanceName="gvwRTRUCKDoc2" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc2.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc2.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc2.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสาร Compartment <font color="#FF0000">*</font>: </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldRDoc03" runat="Server" Width="380px" ClientInstanceName="uldRDoc03" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldRDoc03_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{ gvwRTRUCKDoc3.PerformCallback('BIND_TRUCK_RINFODOC3;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnRDoc03" runat="Server" SkinID="_upload" ClientInstanceName="xbnRDoc03" CausesValidation="false"
                                                            AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldRDoc03.GetText()==''){msg+='<br>แนบเอกสาร Compartment';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldRDoc03.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwRTRUCKDoc3" ClientInstanceName="gvwRTRUCKDoc3" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc3.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc3.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc3.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารใบจดทะเบียนรถครั้งแรก <font color="#FF0000">*</font>: </td>
                                        <td align="left" colspan="3">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldRDoc04" runat="Server" Width="380px" ClientInstanceName="uldRDoc04" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldRDoc04_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwRTRUCKDoc4.PerformCallback('BIND_TRUCK_RINFODOC4;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnRDoc04" runat="Server" SkinID="_upload" ClientInstanceName="xbnRDoc04" CssClass="dxeLineBreakFix"
                                                            CausesValidation="false" AutoPostBack="false">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldRDoc04.GetText()==''){msg+='<br>แนบเอกสารใบจดทะเบียนรถครั้งแรก';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldRDoc04.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwRTRUCKDoc4" ClientInstanceName="gvwRTRUCKDoc4" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc4.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc4.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKDoc4.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_TYPE" Caption="DOC_TYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                    <asp:SqlDataSource ID="sdsRTRUCKINSURE" runat="server" OnInserted="sdsRTRUCKINSURE_Inserted" OnInserting="sdsRTRUCKINSURE_Inserting"
                        EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdRTRUCKINSURE" SelectCommand="SELECT * FROM TTRUCK_INSURANCE WHERE STRUCKID = :STRUCK_ID "
                        InsertCommand="INSERT INTO TTRUCK_INSURANCE_HIS(STRUCKID,NITEM,CTYPE,SCOMPANY,START_DURATION,END_DURATION,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE) 
                        (SELECT STRUCKID,:NITEM,CTYPE,SCOMPANY,START_DURATION,END_DURATION,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE 
                        FROM TTRUCK_INSURANCE WHERE STRUCKID=:STRUCK_ID)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                        </SelectParameters>
                        <InsertParameters>
                            <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtGlobal_RNITEM" Name="NITEM" PropertyName="Text" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="sdsRTRUCKISDoc" runat="server" OnInserted="sdsRTRUCKISDoc_Inserted" OnInserting="sdsRTRUCKISDoc_Inserting"
                        EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdRTRUCKISDoc" SelectCommand="SELECT TTRUCK_INSUREDOC.*,'UploadFile/Truck/'|| STRUCKID ||'/'|| CTYPE ||'/' SPATH,'0' CNEW FROM TTRUCK_INSUREDOC WHERE STRUCKID = :STRUCK_ID AND CACTIVE='1'"
                        InsertCommand="INSERT INTO TTRUCK_INSUREDOC_HIS(DOCID,STRUCKID,NITEM,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) 
                         (SELECT DOCID,STRUCKID,:NITEM,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME FROM TTRUCK_INSUREDOC WHERE STRUCKID=:STRUCK_ID)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                        </SelectParameters>
                        <InsertParameters>
                            <asp:ControlParameter ControlID="txtGlobal_RTRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtGlobal_RNITEM" Name="NITEM" PropertyName="Text" />
                        </InsertParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxRoundPanel ID="rpnTStatute" runat="Server" ClientInstanceName="rpnTStatute" Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctTStatute" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="24%">
                                            บริษัทประกันภัย : </td>
                                        <td align="left" width="76%">
                                            <dx:ASPxTextBox ID="txtRSCompany" runat="Server" ClientInstanceName="txtRSCompany" Width="230px" MaxLength="100" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ระยะเวลาอายุ พรบ. : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxDateEdit ID="xDetRSDuration_Start" runat="server" Width="90px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                            NullText="ระหว่างวันที่" ClientInstanceName="xDetRSDuration_Start">
                                                        </dx:ASPxDateEdit>
                                                    </td>
                                                    <td style="padding-left: 5px; padding-right: 5px;">
                                                        ถึง </td>
                                                    <td>
                                                        <dx:ASPxDateEdit ID="xDetRSDuration_End" runat="server" Width="90px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                            NullText="ถึงวันที่" ClientInstanceName="xDetRSDuration_End">
                                                        </dx:ASPxDateEdit>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            รายละเอียดเพิ่มเติม : </td>
                                        <td align="left">
                                            <dx:ASPxMemo ID="xMemRSDetail" runat="Server" Width="300px" Rows="3" ClientInstanceName="xMemRSDetail">
                                            </dx:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารประกันภัย : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldRSDoc" runat="Server" Width="380px" ClientInstanceName="uldRSDoc" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldRSDoc_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{ gvwRTRUCKSDoc.PerformCallback('BIND_TRUCK_RSTATUTEDOC;'); } } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnRSDoc" runat="Server" SkinID="_upload" ClientInstanceName="xbnRSDoc" CausesValidation="false"
                                                            AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldRSDoc.GetText()==''){msg+='<br>แนบเอกสารประกันภัย';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldRSDoc.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwRTRUCKSDoc" ClientInstanceName="gvwRTRUCKSDoc" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKSDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKSDoc.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKSDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CTYPE" Caption="CTYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                    <dx:ASPxRoundPanel ID="rpnTInsurance" runat="Server" ClientInstanceName="rpnTInsurance" Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctTInsurance" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="24%">
                                            บริษัทประกันภัย : </td>
                                        <td align="left" width="76%">
                                            <dx:ASPxTextBox ID="txtRICompany" runat="Server" ClientInstanceName="txtRICompany" Width="230px" MaxLength="100" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ทุนประกันภัย : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtRIBudget" runat="Server" ClientInstanceName="txtRIBudget" Width="230px">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        บาท </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ประเภทประกันภัย : </td>
                                        <td align="left">
                                            <dx:ASPxComboBox ID="cbmRIType" runat="Server" ClientInstanceName="cbmRIType" Width="230px">
                                                <Items>
                                                    <dx:ListEditItem Value="" Text="- เลือก -" />
                                                    <dx:ListEditItem Value="ประกันภัยชั้น1" Text="ประกันภัยชั้น1" />
                                                    <dx:ListEditItem Value="ประกันภัยชั้น3" Text="ประกันภัยชั้น3" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ความคุ้มครอง : </td>
                                        <td align="left">
                                            <dx:ASPxCheckBoxList ID="chkRIHolding" runat="Server" ClientInstanceName="chkRIHolding" RepeatDirection="Horizontal"
                                                Border-BorderWidth="0" Paddings-Padding="0">
                                                <Items>
                                                    <dx:ListEditItem Value="หางลาก" Text="หางลาก" />
                                                    <dx:ListEditItem Value="บุคคลที่3" Text="บุคคลที่3" />
                                                    <dx:ListEditItem Value="สินค้า" Text="สินค้า" />
                                                </Items>
                                            </dx:ASPxCheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            เบี้ยประกันต่อปี : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtRInInsure" runat="Server" ClientInstanceName="txtRInInsure" Width="230px">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RegularExpression ValidationExpression="^[1-9]\d*" ErrorText="กรุณาระบุตัวเลขเท่านั้น" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td style="padding-left: 5px;">
                                                        บาท </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            รายละเอียดเพิ่มเติม : </td>
                                        <td align="left">
                                            <dx:ASPxMemo ID="xMemRIDetail" runat="Server" ClientInstanceName="xMemRIDetail" Width="300px" Rows="3">
                                            </dx:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            แนบเอกสารประกันภัย : </td>
                                        <td align="left">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxUploadControl ID="uldRIDoc" runat="Server" ClientInstanceName="uldRIDoc" Width="380px" CssClass="dxeLineBreakFix"
                                                            NullText="Click here to browse files..." OnFileUploadComplete="uldRIDoc_FileUploadComplete">
                                                            <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{ gvwRTRUCKIDoc.PerformCallback('BIND_TRUCK_RINSUREDOC;');} } " />
                                                            <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                    </td>
                                                    <td style="padding-left: 10px;">
                                                        <dx:ASPxButton ID="xbnRIDoc" runat="Server" ClientInstanceName="xbnRIDoc" SkinID="_upload" CausesValidation="false"
                                                            AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function(s,e){ var msg=''; if(uldRIDoc.GetText()==''){msg+='<br>แนบเอกสารประกันภัย';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldRIDoc.Upload();}  }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4">
                                            <dx:ASPxGridView ID="gvwRTRUCKIDoc" ClientInstanceName="gvwRTRUCKIDoc" runat="server" SkinID="_gvw">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </CellStyle>
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                        <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Left" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKIDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKIDoc.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { gvwRTRUCKIDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CTYPE" Caption="CTYPE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
    <div id="dvOther" runat="Server">
        <dx:ASPxCallbackPanel ID="xcpnMWater" runat="server" CausesValidation="False" HideContentOnCallback="False"
            ClientInstanceName="xcpnMWater" OnCallback="xcpnMWater_Callback">
            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo; if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxRoundPanel ID="rpnWater" runat="Server" ClientInstanceName="rpnWater" HeaderText="ข้อมูลวัดน้ำ"
                        Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctWater" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td width="40%">
                                            &nbsp; </td>
                                        <td align="right" width="60%">
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxButton ID="xbnAddWater" runat="server" ClientInstanceName="xbnAddWater" Text="เพิ่มประวัติวัดน้ำ"
                                                            AutoPostBack="false" CausesValidation="false">
                                                            <ClientSideEvents Click="function (s,e) { xcpnMWater.PerformCallback('AddWater'); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="xbnViewWater" runat="server" ClientInstanceName="xbnViewWater" Text="ประวัติวัดน้ำ"
                                                            AutoPostBack="false" CausesValidation="false">
                                                            <ClientSideEvents Click="function (s,e) { xcpnMWater.PerformCallback('ViewWater'); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td align="right" width="25%">
                                                        สถานที่วัดน้ำ <font color="#FF0000">*</font>: </td>
                                                    <td align="left" width="75%">
                                                        <dx:ASPxRadioButtonList ID="rblPlaceMWater" runat="Server" ClientInstanceName="rblPlaceMWater" RepeatDirection="Horizontal"
                                                            Border-BorderWidth="0" Paddings-Padding="0">
                                                            <ClientSideEvents SelectedIndexChanged="function (s, e) { var ibl=false,ebl=false; if(rblPlaceMWater.GetValue()=='INTERNAL') ibl=true; else ebl=true; txtIsCar_Num.SetEnabled(ibl); xDetIDPREV_SERV.SetEnabled(ibl); xDetIDNEXT_SERV.SetEnabled(ibl); txtEsCar_Num.SetEnabled(ebl); xDetEDPREV_SERV.SetEnabled(ebl); xDetEDNEXT_SERV.SetEnabled(ebl); uldMWaterDoc.SetEnabled(ebl); xbnMWaterDoc.SetEnabled(ebl);  }" />
                                                            <Items>
                                                                <dx:ListEditItem Value="INTERNAL" Text="วัดน้ำภายใน ปตท." />
                                                                <dx:ListEditItem Value="EXTERNAL" Text="วัดน้ำภายนอก ปตท." />
                                                            </Items>
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุสถานที่วัดน้ำ" IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxRadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table cellspacing="2" cellpadding="3" width="100%" style="border: 1px solid #d3e6fe;">
                                                <tr>
                                                    <td align="right" width="35%">
                                                        รหัสวัดน้ำ <font color="#FF0000">*</font>: </td>
                                                    <td align="left" width="65%">
                                                        <dx:ASPxTextBox ID="txtIsCar_Num" runat="Server" ClientInstanceName="txtIsCar_Num" Width="230px" MaxLength="20">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุรหัสวัดน้ำ" IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        วันหมดอายุวัดน้ำ <font color="#FF0000">*</font>: </td>
                                                    <td align="left">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxDateEdit ID="xDetIDPREV_SERV" runat="server" ClientInstanceName="xDetIDPREV_SERV" Width="90px"
                                                                        CssClass="dxeLineBreakFix" SkinID="xdte" NullText="วันที่วัดน้ำครั้งก่อนหน้า">
                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                            </ErrorFrameStyle>
                                                                            <RequiredField ErrorText="กรุณาระบุวันที่วัดน้ำครั้งก่อนหน้า" IsRequired="True" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxDateEdit>
                                                                </td>
                                                                <td>
                                                                    &nbsp;ถึง&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxDateEdit ID="xDetIDNEXT_SERV" runat="server" ClientInstanceName="xDetIDNEXT_SERV" Width="90px"
                                                                        CssClass="dxeLineBreakFix" SkinID="xdte" NullText="วันที่กำหนดวัดน้ำครั้งต่อไป">
                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                            </ErrorFrameStyle>
                                                                            <RequiredField ErrorText="กรุณาระบุวันที่กำหนดวัดน้ำครั้งต่อไป" IsRequired="True" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxDateEdit>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <table cellspacing="2" cellpadding="3" width="100%" style="border: 1px solid #d3e6fe;">
                                                <tr>
                                                    <td align="right" width="35%">
                                                        รหัสวัดน้ำ/เลขที่เอกสารอ้างอิง <font color="#FF0000">*</font>: </td>
                                                    <td align="left" width="65%">
                                                        <dx:ASPxTextBox ID="txtEsCar_Num" runat="Server" ClientInstanceName="txtEsCar_Num" Width="230px" MaxLength="20">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField ErrorText="กรุณาระบุรหัสวัดน้ำ" IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        วันหมดอายุวัดน้ำ <font color="#FF0000">*</font>: </td>
                                                    <td align="left">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxDateEdit ID="xDetEDPREV_SERV" runat="server" ClientInstanceName="xDetEDPREV_SERV" Width="90px"
                                                                        CssClass="dxeLineBreakFix" SkinID="xdte" NullText="วันที่วัดน้ำครั้งก่อนหน้า">
                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                            </ErrorFrameStyle>
                                                                            <RequiredField ErrorText="กรุณาระบุวันที่วัดน้ำครั้งก่อนหน้า" IsRequired="True" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxDateEdit>
                                                                </td>
                                                                <td>
                                                                    &nbsp;ถึง&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxDateEdit ID="xDetEDNEXT_SERV" runat="server" ClientInstanceName="xDetEDNEXT_SERV" Width="90px"
                                                                        CssClass="dxeLineBreakFix" SkinID="xdte" NullText="วันที่กำหนดวัดน้ำครั้งต่อไป">
                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                            </ErrorFrameStyle>
                                                                            <RequiredField ErrorText="กรุณาระบุวันที่กำหนดวัดน้ำครั้งต่อไป" IsRequired="True" />
                                                                        </ValidationSettings>
                                                                    </dx:ASPxDateEdit>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        เอกสารวัดน้ำ <font color="#FF0000">*</font>: </td>
                                                    <td align="left">
                                                        <table border="0" cellspacing="2" cellpadding="3">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxUploadControl ID="uldMWaterDoc" runat="Server" ClientInstanceName="uldMWaterDoc" Width="230px"
                                                                        CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="uldMWaterDoc_FileUploadComplete">
                                                                        <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{ gvwSTRUCKMWaterDoc.PerformCallback('BIND_TRUCK_MWATERDOC;');} } " />
                                                                        <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                            MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                        </ValidationSettings>
                                                                    </dx:ASPxUploadControl>
                                                                </td>
                                                                <td style="padding-left: 10px;">
                                                                    <dx:ASPxButton ID="xbnMWaterDoc" runat="Server" ClientInstanceName="xbnMWaterDoc" SkinID="_upload" CssClass="dxeLineBreakFix"
                                                                        CausesValidation="false" AutoPostBack="false">
                                                                        <ClientSideEvents Click="function(s,e){ var msg=''; if(uldMWaterDoc.GetText()==''){msg+='<br>เอกสารวัดน้ำ';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{uldMWaterDoc.Upload();}  }" />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="4">
                                                                    <asp:SqlDataSource ID="sdsSTRUCKMWaterDoc" runat="server" OnInserted="sdsSTRUCKMWaterDoc_Inserted" OnInserting="sdsSTRUCKMWaterDoc_Inserting"
                                                                        EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdSTRUCKMWaterDoc" SelectCommand="SELECT TTRUCK_MWATERDOC.*,'UploadFile/Truck/'|| STRUCKID ||'/MWATER/' SPATH,'0' CNEW FROM TTRUCK_MWATERDOC WHERE STRUCKID = :STRUCK_ID AND CACTIVE='1'"
                                                                        InsertCommand="INSERT INTO TTRUCK_MWATERDOC_HIS(DOCID,STRUCKID,NITEM,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,DOC_SYSNAME,CACTIVE) 
                                                                        (SELECT DOCID,STRUCKID,:NITEM,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,DOC_SYSNAME,CACTIVE FROM TTRUCK_MWATERDOC WHERE STRUCKID=:STRUCK_ID)">
                                                                        <SelectParameters>
                                                                            <asp:ControlParameter ControlID="txtGlobal_TRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                                        </SelectParameters>
                                                                        <InsertParameters>
                                                                            <asp:ControlParameter ControlID="txtGlobal_TRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                                            <asp:ControlParameter ControlID="txtGlobal_TNITEM" Name="NITEM" PropertyName="Text" />
                                                                        </InsertParameters>
                                                                    </asp:SqlDataSource>
                                                                    <dx:ASPxGridView ID="gvwSTRUCKMWaterDoc" ClientInstanceName="gvwSTRUCKMWaterDoc" runat="server" SkinID="_gvw">
                                                                        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                                        <Columns>
                                                                            <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                                                </CellStyle>
                                                                                <EditFormSettings Visible="False" />
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="DOC_NAME" Caption="เอกสาร" Width="85%">
                                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <CellStyle HorizontalAlign="Left" />
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                                                <DataItemTemplate>
                                                                                    <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                                        CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                        <ClientSideEvents Click="function (s, e) { gvwSTRUCKMWaterDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                                    </dx:ASPxButton>
                                                                                    <dx:ASPxButton ID="btnDelTruckDoc" ClientInstanceName="btnDelTruckDoc" runat="server" ToolTip="ลบรายการ"
                                                                                        CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                        <ClientSideEvents Click="function (s, e) { gvwSTRUCKMWaterDoc.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                                    </dx:ASPxButton>
                                                                                </DataItemTemplate>
                                                                                <CellStyle HorizontalAlign="Center">
                                                                                </CellStyle>
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%" Visible="false">
                                                                                <DataItemTemplate>
                                                                                    <dx:ASPxButton ID="btnViewTruckDoc" ClientInstanceName="btnViewTruckDoc" runat="server" ToolTip="ดูเอกสาร"
                                                                                        CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                        SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                        <ClientSideEvents Click="function (s, e) { gvwSTRUCKMWaterDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                                    </dx:ASPxButton>
                                                                                </DataItemTemplate>
                                                                                <CellStyle HorizontalAlign="Center">
                                                                                </CellStyle>
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="DOCID" Caption="DOCID" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="STRUCKID" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="DOC_SYSNAME" Caption="DOC_SYSNAME" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                                            <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                                        </Columns>
                                                                        <SettingsPager Mode="ShowAllRecords" />
                                                                        <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                                                    </dx:ASPxGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
        <br />
        <dx:ASPxCallbackPanel ID="xcpnContract" runat="server" CausesValidation="False" HideContentOnCallback="False"
            ClientInstanceName="xcpnContract" OnCallback="xcpnContract_Callback">
            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxRoundPanel ID="rpnHolding" runat="Server" ClientInstanceName="rpnHolding" HeaderText="ข้อมูลการใช้งานและถือครอง"
                        Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctHolding" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="25%">
                                            เลขที่สัญญา : </td>
                                        <td align="left" width="75%">
                                            <dx:ASPxHyperLink ID="lbkContract" runat="Server" ClientInstanceName="lbkContract" CausesValidation="false"
                                                AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false" EnableTheming="false" CssClass="dxeLineBreakFix">
                                                <ClientSideEvents Click="function (s, e) {xcpnContract.PerformCallback('VIEW;')}" />
                                            </dx:ASPxHyperLink>
                                            <table style="display: none;">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtContractID" runat="Server" ClientInstanceName="txtContractID">
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtVenderID" runat="Server" ClientInstanceName="txtVenderID">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            ผู้ขนส่ง : </td>
                                        <td align="left">
                                            <dx:ASPxLabel ID="xlbsTransport" runat="Server" ClientInstanceName="xlbsTransport" />
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
        <br />
        <dx:ASPxCallbackPanel ID="xcpnPermit" runat="server" CausesValidation="False" HideContentOnCallback="False"
            ClientInstanceName="xcpnPermit" OnCallback="xcpnPermit_Callback">
            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
            <PanelCollection>
                <dx:PanelContent>
                    <dx:ASPxRoundPanel ID="rpnPermit" runat="Server" ClientInstanceName="rpnPermit" HeaderText="การอนุญาตรับงาน"
                        Width="980px">
                        <PanelCollection>
                            <dx:PanelContent ID="pctPermit" runat="Server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="right" width="25%">
                                            การอนุญาตรับงาน <font color="#FF0000">*</font>: </td>
                                        <td align="left" width="75%">
                                            <dx:ASPxRadioButtonList ID="rblPermit" runat="Server" ClientInstanceName="rblPermit" RepeatDirection="Horizontal"
                                                Border-BorderWidth="0" Paddings-Padding="0" AutoPostBack="false">
                                                <ClientSideEvents SelectedIndexChanged="function (s, e) { if(rblPermit.GetValue()=='N'){ xDetdBlackListExp.SetEnabled(true); xMemBlackListCause.SetEnabled(true); }else{ xDetdBlackListExp.SetEnabled(false); xMemBlackListCause.SetEnabled(false); } }" />
                                                <Items>
                                                    <dx:ListEditItem Value="Y" Text="อนุญาต" />
                                                    <dx:ListEditItem Value="N" Text="ระงับ" />
                                                    <dx:ListEditItem Value="D" Text="ยกเลิก" />
                                                </Items>
                                            </dx:ASPxRadioButtonList>
                                            <dx:ASPxTextBox ID="txtPermit" runat="Server" ClientInstanceName="txtPermit" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            วันที่ปลดระงับ <font color="#FF0000">*</font>: </td>
                                        <td align="left">
                                            <dx:ASPxDateEdit ID="xDetdBlackListExp" runat="server" Width="90px" CssClass="dxeLineBreakFix" SkinID="xdte"
                                                NullText="ปลดระงับเมื่อ" ClientInstanceName="xDetdBlackListExp">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณาระบุวันที่ปลดระงับ" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            สาเหตุการระงับ : </td>
                                        <td align="left">
                                            <dx:ASPxMemo ID="xMemBlackListCause" runat="Server" ClientInstanceName="xMemBlackListCause" Width="300px"
                                                Rows="3">
                                            </dx:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            <br />
                                            ประวัติการระงับใช้ : </td>
                                        <td align="left">
                                            <br />
                                            <asp:SqlDataSource ID="sdsSTRUCKBlacklist" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                CacheKeyDependency="ckdSTRUCKBlacklist" SelectCommand="SELECT * FROM TTRUCK_BLACKLIST WHERE STRUCKID = :STRUCK_ID ORDER BY DBLACKLIST_START DESC">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="txtGlobal_TRUCKID" Name="STRUCK_ID" PropertyName="Text" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <dx:ASPxGridView ID="gvwBlacklist" runat="Server" ClientInstanceName="gvwBlacklist" SkinID="_gvw" Width="60%"
                                                Style="margin-top: 0px" AutoGenerateColumns="False" KeyFieldName="BLACKLISTID" OnPageIndexChanged="gvwBlacklist_PageIndexChanged">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="BLACKLISTID" Visible="false">
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="REMARK" Caption="หมายเหตุ" Width="20%">
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SDATE" Caption="วันที่ - เวลา(ถูกระงับ)" Width="20%">
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="TDATE" Caption="วันที่ - เวลา(ปลดระงับ)" Width="20%">
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SBLACKLIST" Caption="ผู้บันทึก" Width="20%">
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsPager AlwaysShowPager="false" />
                                            </dx:ASPxGridView>
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
        <br />
    </div>
    <table border="0" cellspacing="2" cellpadding="3" width="100%">
        <tr>
            <td align="right">
                <dx:ASPxButton ID="xbnSubmit" runat="Server" ClientInstanceName="xbnSubmit" SkinID="_submit" AutoPostBack="false"
                    ValidationGroup="add">
                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; dxConfirm('แจ้งเตือน','ท่านต้องการบันทึกเพื่อเปลี่ยนแปลงข้อมูลนี้ ใช่หรือไม่ ?',function(s,e){ dxPopupConfirm.Hide(); xcpnMain.PerformCallback('Save;'); } ,function(s,e){ dxPopupConfirm.Hide(); })  }" />
                </dx:ASPxButton>
            </td>
            <td align="left">
                <dx:ASPxButton ID="xbnCancel" runat="Server" ClientInstanceName="xbnCancel" SkinID="_cancel" AutoPostBack="false">
                    <ClientSideEvents Click="function (s, e) { window.location = 'truck_info.aspx'; }" />
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
</asp:Content>
