﻿using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Report;
using TMS_DAL.Master;

public partial class ReportFeedUtilization : PageBase
{
	#region Page_Load
	protected void Page_Load(object sender, EventArgs e)
	{
		this.Culture = "en-US";
		this.UICulture = "en-US";
		if(!IsPostBack)
		{
			DrowDownList();
            this.AssignAuthen();
		}
	}
	#endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

	#region DrowDownList
	private void DrowDownList()
	{
		ddlGroup.DataValueField = "VALUE";
		ddlGroup.DataTextField = "NAME";
		ddlGroup.DataSource = ReportFeedUtilizationBLL.Instance.GroupSelect();
		ddlGroup.DataBind();
		ddlGroup.Items.Insert(0, new ListItem()
		{
			Text = "เลือกทั้งหมด",
			Value = "0"
		});
	}
	#endregion

	#region btnSearch_Click
	protected void btnSearch_Click(object sender, EventArgs e)
	{

		if(string.IsNullOrEmpty(txtDateStart.Text))
		{
			alertFail("กรุณาป้อน วันที่เริ่มต้น !!!");
			return;
		}
		if(string.IsNullOrEmpty(txtDateEnd.Text))
		{
			alertFail("กรุณาป้อน วันที่สิ้นสุด !!!");
			return;
		}


		GetData();
		gvReport.CollapseAll();
		//gvReport.ExpandAll();
	}
	#endregion

	#region ddlGroup_SelectedIndexChanged
	protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
	{
		if(ddlGroup.SelectedIndex != 0)
		{
			txtWorkGroup.Text = ddlGroup.SelectedValue.Split(',')[1];
		}
		else
		{
			txtWorkGroup.Text = string.Empty;
		}

	}
	#endregion

	protected void gvReport_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
	{
		GetData();
		gvReport.CollapseAll();
		int index = int.Parse(e.Args[0]);
		switch(e.CallbackName.ToUpper())
		{
			case "COLLAPSEROW":
				gvReport.CollapseRow(index);
				break;
			case "EXPANDROW":
				gvReport.ExpandRow(index);
				break;
			default:
				break;
		}
	}

	#region GetData
	private void GetData()
	{
		int groupid = 0;
		try
		{
			groupid = int.Parse(ddlGroup.SelectedValue.Split(',')[0]);
		}
		catch(Exception ex)
		{
			groupid = 0;
		}
		string SessionID = Session.SessionID;
		DataTable dt = ReportFeedUtilizationBLL.Instance.ReportFeedUtilizationSelect(groupid, txtDateStart.Text, txtDateEnd.Text, SessionID);
		var ListGroupID = dt.AsEnumerable().GroupBy(it => it["ID"]).Select(it => new
		{
			ID = (it.First()["ID"] + string.Empty)
		}).ToList();
		DataRow[] drs;
		int sumNTRUCKSUM = 0, sumNTRUCKSUMCONFIRM = 0, sumGROUPCOUNTDO = 0, sumGROUPNTRANSIT = 0;
		decimal sumPERCENT_SUMGROUPCOUNTDO = 0, PERCENT_SUMCONFIRM = 0;
		foreach(var item in ListGroupID)
		{
			drs = dt.Select("ID = '" + item.ID + "'");
			if(drs.Any())
			{
				sumNTRUCKSUM = drs.Sum(it => int.Parse(it["NTRUCKSUM"] + string.Empty));
				sumNTRUCKSUMCONFIRM = drs.Sum(it => int.Parse(it["NTRUCKSUMCONFIRM"] + string.Empty));
				sumGROUPCOUNTDO = drs.Sum(it => int.Parse(it["COUNTDO"] + string.Empty));
				sumGROUPNTRANSIT = drs.Sum(it => int.Parse(it["NTRANSIT"] + string.Empty));
				sumPERCENT_SUMGROUPCOUNTDO = (sumNTRUCKSUM == 0 ? 0 : ((decimal)sumGROUPCOUNTDO / (decimal)sumNTRUCKSUM) * 100);
				PERCENT_SUMCONFIRM = (sumNTRUCKSUM == 0 ? 0 : ((decimal)sumNTRUCKSUMCONFIRM / (decimal)sumNTRUCKSUM) * 100);
				foreach(var dr in drs)
				{
					dr["SUMGROUPNTRUCKSUM"] = sumNTRUCKSUM;
					dr["SUMGROUPNTRUCKSUMCONFIRM"] = sumNTRUCKSUMCONFIRM;
					dr["SUMGROUPCOUNTDO"] = sumGROUPCOUNTDO;
					dr["SUMGROUPNTRANSIT"] = sumGROUPNTRANSIT;
					dr["PERCENT_SUMGROUPCOUNTDO"] = sumPERCENT_SUMGROUPCOUNTDO.ToString("N");
					dr["PERCENT_SUMCONFIRM"] = PERCENT_SUMCONFIRM.ToString("N");
				}
			}
		}
		gvReport.DataSource = dt;
		gvReport.DataBind();

		new ASPxGridViewCellMerger(gvReport);

		//gvReport.Columns[0].FixedStyle = GridViewColumnFixedStyle.Left;
		//gvReport.Columns[1].FixedStyle = GridViewColumnFixedStyle.Left;

		//gvReport.Columns[0].CellStyle.BackColor = Color.FromArgb(0xEE, 0xEE, 0xEE);
		//gvReport.Columns[1].CellStyle.BackColor = Color.FromArgb(0xEE, 0xEE, 0xEE);

		//gvReport.Settings.ShowHorizontalScrollBar = true;
	}
	#endregion

	public class ASPxGridViewCellMerger
	{
		ASPxGridView grid;
		Dictionary<GridViewDataColumn, TableCell> mergedCells = new Dictionary<GridViewDataColumn, TableCell>();
		Dictionary<TableCell, int> cellRowSpans = new Dictionary<TableCell, int>();

		public ASPxGridViewCellMerger(ASPxGridView grid)
		{
			this.grid = grid;
			Grid.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(grid_HtmlRowCreated);
			Grid.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
		}

		public ASPxGridView Grid
		{
			get
			{
				return grid;
			}
		}
		void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
		{
			//add the attribute that will be used to find which column the cell belongs to
			e.Cell.Attributes.Add("ci", e.DataColumn.VisibleIndex.ToString());

			if(cellRowSpans.ContainsKey(e.Cell))
			{
				e.Cell.RowSpan = cellRowSpans[e.Cell];
			}
		}
		void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			if(Grid.GetRowLevel(e.VisibleIndex) != Grid.GroupCount)
				return;

			for(int i = e.Row.Cells.Count - 1; i >= 0; i--)
			{

				DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell dataCell = e.Row.Cells[i] as DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell;
				if(dataCell != null && (i <= 4 || i > 14))
				{
					MergeCells(dataCell.DataColumn, e.VisibleIndex, dataCell);
				}
			}
		}

		void MergeCells(GridViewDataColumn column, int visibleIndex, TableCell cell)
		{
			bool isNextTheSame = IsNextRowHasSameData(column, visibleIndex);
			if(isNextTheSame)
			{
				if(!mergedCells.ContainsKey(column))
				{
					mergedCells[column] = cell;
				}
			}
			if(IsPrevRowHasSameData(column, visibleIndex))
			{
				((TableRow)cell.Parent).Cells.Remove(cell);
				if(mergedCells.ContainsKey(column))
				{
					TableCell mergedCell = mergedCells[column];
					if(!cellRowSpans.ContainsKey(mergedCell))
					{
						cellRowSpans[mergedCell] = 1;
					}
					cellRowSpans[mergedCell] = cellRowSpans[mergedCell] + 1;
				}
			}
			if(!isNextTheSame)
			{
				mergedCells.Remove(column);
			}
		}
		bool IsNextRowHasSameData(GridViewDataColumn column, int visibleIndex)
		{
			//is it the last visible row
			if(visibleIndex >= Grid.VisibleRowCount - 1)
				return false;

			return IsSameData(column.FieldName, visibleIndex, visibleIndex + 1);
		}
		bool IsPrevRowHasSameData(GridViewDataColumn column, int visibleIndex)
		{
			ASPxGridView grid = column.Grid;
			//is it the first visible row
			if(visibleIndex <= Grid.VisibleStartIndex)
				return false;

			return IsSameData(column.FieldName, visibleIndex, visibleIndex - 1);
		}
		bool IsSameData(string fieldName, int visibleIndex1, int visibleIndex2)
		{
			// is it a group row?
			if(Grid.GetRowLevel(visibleIndex2) != Grid.GroupCount)
				return false;

			return object.Equals(Grid.GetRowValues(visibleIndex1, fieldName), Grid.GetRowValues(visibleIndex2, fieldName));
		}
	}
}