﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using DevExpress.Web.ASPxGridView;
using System.Text;
using DevExpress.XtraEditors;
using TMS_BLL.Master;
using System.Web.Security;

public partial class Vendor_Detail : PageBase
{
    #region + ViewState +
    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    #endregion
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string VendorID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        #region EventHandler
        gvw1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        vehiclegrid.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        #endregion

        if (!IsPostBack)
        {
            if (Session["UserID"] == null || Session["UserID"] + "" == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
            }
            else
            {
                CGROUP = Session["CGROUP"].ToString();
            }
            VendorID = SystemFunction.GET_VENDORID(Session["UserID"] + "");
            // VendorID = "0010002679";
            txtVendorID.Text = VendorID;
            //dteStart.Text = DateTime.Now.AddMonths(-1).Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            //dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            dteStart.Text = DateTime.Now.AddMonths(-1).Date.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US"));

            //Response.Write("" + Session["oSVENDORID1"]);
            if (!string.IsNullOrEmpty(VendorID))
            {
                ListData();
            }
            this.AssignAuthen();
        }
        else
        {
            if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        }
    }


    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

        gvw.CancelEdit();
        gvw1.CancelEdit();
        gvw2.CancelEdit();


    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnSearch1.Enabled = false;
                btnSearch2.Enabled = false;
                ASPxButton1.Enabled = false;
            }
            if (!CanWrite)
            {
                btnEditT1.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {

            case "view":
                BindData();
                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SCONTRACTID");
                int conid = Convert.ToInt32(data);

                string rowsIndex = paras[1] + "";
                gvw.StartEdit(int.Parse(rowsIndex));
                Literal ltl = (Literal)gvw.FindEditFormTemplateControl("ltlGuarantee");
                Literal ltl1 = (Literal)gvw.FindEditFormTemplateControl("ltlTruck");
                DataTable dt = new DataTable();
                dt = CommonFunction.Get_Data(sql, "SELECT SGUARANTEETYPE,SBOOKNO,NAMOUNT,SREMARK FROM TCONTRACT_GUARANTEES WHERE NCONTRACTID = " + conid);
                if (dt.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    string st = @"<tr><td width='5%' align='center'><span class='style24'>{0}</span></td>
                                <td align='center'><span class='style24'>{1}</span></td>
                                <td align='center'><span class='style24'>{2}</span></td>
                                <td align='center'><span class='style24'>{3}</span></td>
                                <td align='center'><span class='style24'>{4}</span></td>
                              </tr>";
                    int i = 1;
                    foreach (DataRow dr in dt.Rows)
                    {

                        sb.Append(string.Format(st, i, dr["SGUARANTEETYPE"] + "", dr["SBOOKNO"] + "", dr["NAMOUNT"] + "", dr["SREMARK"] + ""));
                        i++;
                    }

                    ltl.Text = sb.ToString();
                    sb = null;
                }
                else
                {
                    if (ltl1 != null)
                    {
                        ltl.Text = "<tr><td align='center' colspan='5' style='color:red'>ไม่มีข้อมูล</td></tr>";
                    }
                }

                DataTable dt1 = new DataTable();
                dt1 = CommonFunction.Get_Data(sql, @"SELECT T.SHEADREGISTERNO,T.STRAILERREGISTERNO,TT.SCARTYPENAME, CASE CT.CSTANDBY WHEN 'N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END SCARTYPE, T.SCHASIS,
                                                    CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NTOTALCAPACITY ELSE T.NTOTALCAPACITY END NTOTALCAPACITY
                                                    ,CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NSLOT ELSE T.NSLOT END NSLOT
                                                    FROM (TCONTRACT_TRUCK ct 
                                                    LEFT JOIN TTRUCK t ON ct.STRUCKID = t.STRUCKID
                                                    LEFT JOIN TTRUCK TRCK_TAIL ON ct.STRAILERID = TRCK_TAIL.STRUCKID
                                                    ) 
                                                    LEFT JOIN TTRUCKTYPE tt ON T.SCARTYPEID = TT.SCARTYPEID WHERE ct.SCONTRACTID = '" + conid + @"' 
                                                    ORDER BY SCARTYPE");

                if (dt1.Rows.Count > 0)
                {
                    StringBuilder sb1 = new StringBuilder();
                    string st1 = @"<tr>
                                <td align='center'><span class='style24'>{0}</span></td>
                                <td width='15%'><span class='style24'>{1}</span></td>
                                <td width='15%'><span class='style24'>{2}</span></td>
                                <td><span class='style24'>{3}</span></td>
                                <td><span class='style24'>{4}</span></td>
                                <td width='15%'><span class='style24'>{5}</span></td>
                                <td align='center'><span class='style24'>{6}</span></td>
                                <td align='center'><span class='style24'>{7}</span></td>
                                </tr>";
                    int i = 1;
                    foreach (DataRow dr in dt1.Rows)
                    {

                        sb1.Append(string.Format(st1, i, dr["SHEADREGISTERNO"] + "", dr["STRAILERREGISTERNO"] + "", dr["SCARTYPENAME"] + "", dr["SCARTYPE"] + "", dr["SCHASIS"] + "", dr["NTOTALCAPACITY"] + "", dr["NSLOT"] + ""));
                        i++;
                    }

                    ltl1.Text = sb1.ToString();
                    sb1 = null;
                }
                else
                {
                    if (ltl1 != null)
                    {
                        ltl1.Text = "<tr><td align='center' colspan='8' style='color:red'>ไม่มีข้อมูล</td></tr>";
                    }
                }

                //ltl1.Text = "<tr><td colspan='5'>ssssssss</td></tr>";
                //gvw.DataBind();

                break;

            case "view1":
                //BindData();
                gvw1.DataBind();
                dynamic data1 = "";
                dynamic carTypeId = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SCARTYPEID");
                dynamic data2 = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRANSPORTTYPE");

                if (carTypeId == 0)
                {
                    data1 = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRUCKID");
                }
                else
                {
                    data1 = gvw1.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRAILERID");
                }

                string conid1 = data1 + "";
                int TypeTransport = Convert.ToInt32(data2);

                string rowsIndex1 = paras[1] + "";
                gvw1.StartEdit(int.Parse(rowsIndex1));

                Literal ltl3 = (Literal)gvw1.FindEditFormTemplateControl("ltlTruckTotal");
                Literal ltl4 = (Literal)gvw1.FindEditFormTemplateControl("ltlTruckTotal1");

                StringBuilder sb2 = new StringBuilder();
                string st2 = @"
                            <tr>
                              <td width='15%' align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสผู้ขนส่ง</span></td>
                              <td width='19%' align='left'><span class='style24'>{0}</span></td>
                              <td width='15%' align='left' bgcolor='#FFEBD7'><span class='style24'>ชื่อผู้ขนส่ง</span></td>
                              <td width='32%' align='left'><span class='style24'>{1}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ประเภทผู้ขนส่ง</span></td>
                              <td align='left'><span class='style24'>{2}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ชื่อเจ้าของรถ</span></td>
                              <td align='left'><span class='style24'>{3}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ทะเบียนรถ (หัว) </span></td>
                              <td align='left'><span class='style24'>{4}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ทะเบียนรถ(ท้าย)</span></td>
                              <td align='left'><span class='style24'>{5}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสรถ(หัว)</span></td>
                              <td align='left'><span class='style24'>{6}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสรถ(ท้าย)</span></td> 
                              <td align='left'><span class='style24'>{7}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ประเภทรถ</span></td>
                              <td align='left'><span class='style24'>{8}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'></span></td>
                              <td align='left'><span class='style24'></span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>หมายเลขแชชซีย์</span></td>
                              <td align='left'><span class='style24'>{9}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>หมายเลขเครื่องยนต์</span></td>
                              <td align='left'><span class='style24'>{10}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>จำนวนล้อ</span></td>
                              <td align='left'><span class='style24'>{11} ล้อ</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>วันที่จดทะเบียน</span></td>
                              <td align='left'><span class='style24'>{12}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>กลุ่มผลิตภัณฑ์ที่บรรทุก</span></td>
                              <td align='left'><span class='style24'>{13}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>วันหมดอายุวัดน้ำ</span></td>
                              <td align='left'><span class='style24'>{18}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>น้ำหนักบรรทุก</span></td>
                              <td align='left'><span class='style24'>{15} กก.</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>น้ำหนักรถ</span></td>
                              <td align='left'><span class='style24'>{14} กก.</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>จำนวนช่อง</span></td>
                              <td align='left'><span class='style24'>{16} ช่อง</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ความจุรวม</span></td>
                              <td align='left'><span class='style24'>{17} ลิตร </span></td>
                            </tr> ";

                DataTable dt3 = new DataTable();
                if (TypeTransport == 1)
                {
                    dt3 = CommonFunction.Get_Data(sql, @"SELECT T.DWATEREXPIRE,T.STRANSPORTID,C.SABBREVIATION CUST_ABBR,TT.STRANS_TYPE_DESC,T.SOWNER_NAME,T.SHEADREGISTERNO,T.STRAILERREGISTERNO,T.STRUCKID SHEADID,T.STRAILERID,TP.SCARTYPENAME
                                                        ,T.SCHASIS,T.SENGINE,T.NWHEELS,T.DREGISTER,T.SPROD_GRP
                                                        ,T.NWEIGHT
                                                        ,T.NLOAD_WEIGHT
                                                        ,CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NSLOT ELSE T.NSLOT END NSLOT
                                                        ,CASE WHEN T.SCARTYPEID='3' THEN TRCK_TAIL.NTOTALCAPACITY ELSE T.NTOTALCAPACITY END NTOTALCAPACITY
                                                        FROM ((TTRUCK T 
                                                                LEFT JOIN TTRUCKTYPE TP ON T.SCARTYPEID = TP.SCARTYPEID
                                                                LEFT JOIN TTRUCK TRCK_TAIL ON T.STRAILERID=TRCK_TAIL.STRUCKID
                                                        ) 
                                                        LEFT JOIN TTRANS_TYPE TT ON T.STRANSPORTTYPE = TT.STRANS_TYPE) 
                                                        LEFT JOIN TVENDOR C ON T.STRANSPORTID = C.SVENDORID 
                                                        WHERE T.STRUCKID ='" + CommonFunction.ReplaceInjection(conid1) + "'");
                    if (dt3.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt3.Rows)
                        {
                            sb2.Append(string.Format(st2, dr["STRANSPORTID"] + "", dr["CUST_ABBR"] + "", dr["STRANS_TYPE_DESC"] + "", dr["SOWNER_NAME"] + "", dr["SHEADREGISTERNO"] + "", dr["STRAILERREGISTERNO"] + "", dr["SHEADID"] + "", dr["STRAILERID"] + "", dr["SCARTYPENAME"] + "", dr["SCHASIS"] + "", dr["SENGINE"] + "", dr["NWHEELS"] + "", dr["DREGISTER"] + "", dr["SPROD_GRP"] + "", dr["NWEIGHT"] + "", dr["NLOAD_WEIGHT"] + "", dr["NSLOT"] + "", dr["NTOTALCAPACITY"] + "", dr["DWATEREXPIRE"] + ""));
                        }
                        ltl3.Text = sb2.ToString();
                        sb2 = null;
                    }
                    else
                    {
                        if (ltl3 != null)
                        {
                            sb2.Append(string.Format(st2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
                            ltl3.Text = sb2.ToString();
                        }
                    }
                }
                else
                {
                    dt3 = CommonFunction.Get_Data(sql, "SELECT T.DWATEREXPIRE,T.STRANSPORTID,v.SABBREVIATION,TT.STRANS_TYPE_DESC,T.SOWNER_NAME,T.SHEADREGISTERNO,T.STRAILERREGISTERNO,T.SHEADID,T.STRAILERID,TP.SCARTYPENAME,T.SCHASIS,T.SENGINE,T.NWHEELS,T.DREGISTER,T.SPROD_GRP,T.NWEIGHT,T.NLOAD_WEIGHT,T.NSLOT,T.NTOTALCAPACITY FROM ((TTRUCK T INNER JOIN TTRUCKTYPE TP ON T.SCARTYPEID = TP.SCARTYPEID)  LEFT JOIN TTRANS_TYPE TT ON T.STRANSPORTTYPE = TT.STRANS_TYPE) LEFT JOIN TVENDOR v ON T.STRANSPORTID = V.SVENDORID WHERE T.STRUCKID ='" + CommonFunction.ReplaceInjection(conid1) + "'");
                    if (dt3.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt3.Rows)
                        {
                            sb2.Append(string.Format(st2, dr["STRANSPORTID"] + "", dr["SABBREVIATION"] + "", dr["STRANS_TYPE_DESC"] + "", dr["SOWNER_NAME"] + "", dr["SHEADREGISTERNO"] + "", dr["STRAILERREGISTERNO"] + "", dr["SHEADID"] + "", dr["STRAILERID"] + "", dr["SCARTYPENAME"] + "", dr["SCHASIS"] + "", dr["SENGINE"] + "", dr["NWHEELS"] + "", dr["DREGISTER"] + "", dr["SPROD_GRP"] + "", dr["NWEIGHT"] + "", dr["NLOAD_WEIGHT"] + "", dr["NSLOT"] + "", dr["NTOTALCAPACITY"] + "", dr["DWATEREXPIRE"] + ""));
                        }
                        ltl3.Text = sb2.ToString();
                        sb2 = null;
                    }
                    else
                    {
                        if (ltl3 != null)
                        {
                            sb2.Append(string.Format(st2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
                            ltl3.Text = sb2.ToString();
                        }
                    }
                }

                DataTable dt4 = new DataTable();
                dt4 = CommonFunction.Get_Data(sql, "SELECT NCOMPARTNO,NPANLEVEL,NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID = '" + conid1 + "' ORDER BY NCOMPARTNO,NPANLEVEL");

                if (dt4.Rows.Count > 0)
                {
                    StringBuilder sb1 = new StringBuilder();
                    string st1 = @"<tr>
                                    <td align='center' class='style24'>{0}</td>
                                    <td align='center' class='style24'>{1}</td>
                                    <td align='center' class='style24'>{2}</td>
                                  </tr>";
                    int i = 1;
                    foreach (DataRow dr in dt4.Rows)
                    {
                        sb1.Append(string.Format(st1, dr["NCOMPARTNO"] + "", dr["NPANLEVEL"] + "", dr["NCAPACITY"] + ""));
                        i++;
                    }

                    ltl4.Text = sb1.ToString();
                    sb1 = null;
                }
                else
                {
                    if (ltl4 != null)
                    {
                        ltl4.Text = "<tr><td align='center' colspan='3' style='color:red'>ไม่มีข้อมูล</td></tr>";
                    }
                }
                break;

            case "view2":
                string rowsIndex2 = paras[1] + "";
                gvw2.StartEdit(int.Parse(rowsIndex2));
                break;

            case "Search":
                BindData();
                break;

            case "Search1":
                //Checkbox
                string condition = "";
                if (FloatCar.Checked)
                {
                    condition += @"AND TTR.ISUSE='0' OR (TTR.CACTIVE ='Y' AND TTR.ISUSE='0' AND (TTR.SHEADREGISTERNO LIKE '%'
                  || :oSearch
                  || '%'
                OR TTR.STRAILERREGISTERNO LIKE '%'
                  || :oSearch
                  || '%'
                OR TTR.SCHASIS LIKE '%'
                  || :oSearch
                  || '%'))";
                }
                /*แก้ไขการเรียกหารถลอย*/
                sds1.SelectCommand = @"
                SELECT DISTINCT TTR.STRUCKID,TTR.VIEHICLE_TYPE,TC.SCONTRACTNO,
  TTR.STRAILERID,
  CASE 
        WHEN TTR.CACTIVE ='N' THEN
        GET_C_CONFIG('10','TTRUCK_STATUS','1')    
        WHEN TTR.CACTIVE ='Y' THEN
        GET_C_CONFIG('11','TTRUCK_STATUS','1')            
    END STATUS,
  TTR.SCARTYPEID,
  SHEADREGISTERNO,
  TTR.STRAILERREGISTERNO,
  TT.CONFIG_NAME AS SCARTYPENAME,
  CASE TCT.CSTANDBY
    WHEN 'N'
    THEN 'รถในสัญญา'
    ELSE 'รถสำรอง'
  END SCARTYPE,
  TTR.SCHASIS,NVL(TTR.DUPDATE ,TTR.DCREATE) AS DUPDATE,
  (CASE SCARTYPEID 
  WHEN 3 THEN
  0
  ELSE
  TTR.NSLOT
  END)  NSLOT,TTR.NTOTALCAPACITY,TTR.ISUSE
FROM TTRUCK TTR
LEFT JOIN C_CONFIG TT
ON TTR.SCARTYPEID      =TT.CONFIG_VALUE AND TT.CONFIG_TYPE = 'VICHICLE'
LEFT JOIN TContract_Truck TCT
ON TTR.STRUCKID = TCT.STRUCKID AND NVL(TTR.STRAILERID,'xxx')= NVL(TCT.STRAILERID,'xxx')
LEFT JOIN TContract TC
ON TCT.SCONTRACTID        = TC.SCONTRACTID
AND NVL(TC.CACTIVE,'xxx') = 'Y'
LEFT JOIN
  (SELECT STRUCKID,
    COUNT(NCOMPARTNO) NSLOT,
    SUM(NCAPACITY) NTOTALCAPACITY
  FROM
    (SELECT STRUCKID,
      NCOMPARTNO,
      MAX(NCAPACITY) NCAPACITY
    FROM TTRUCK_COMPART TTC
    GROUP BY STRUCKID,
      NCOMPARTNO
    )
  GROUP BY STRUCKID
  ) TTC
ON TCT.STRAILERID      = TTC.STRUCKID
WHERE TTR.LOGDATA='1' AND  TTR.STRANSPORTID = :oTrans
AND (TTR.SHEADREGISTERNO LIKE '%'
  || :oSearch
  || '%'
OR TTR.SCHASIS LIKE '%'
  || :oSearch
  || '%')" + condition + "ORDER BY TTR.STRUCKID DESC";
                /*แก้ไขการเรียกหารถลอย*/

                gvw1.DataBind();
                break;

            case "Search2":
                gvw2.DataBind();
                break;
            case "SearchVehicle":
                vehiclegrid.DataBind();
                break;
            case "EditT1":
                xcpn.JSProperties["cpRedirectTo"] = "vendor_edit_p4.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + VendorID));
                break;

            case "EditT2":
                int inxT2 = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                dynamic dataT2 = gvw.GetRowValues(inxT2, "SCONTRACTID", "SVENDORID");
                string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dataT2[0] + "&" + dataT2[1])), vsUrl = "contract_add.aspx?str=";

                string SCONTRACTID = dataT2 + "";
                xcpn.JSProperties["cpRedirectTo"] = vsUrl + vsEncrypt;//"contract_add_p4.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + SCONTRACTID));
                break;

            case "EditT3":
                int inxT3 = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                dynamic dy_data = gvw1.GetRowValues(inxT3, "STRUCKID", "SCARTYPEID");
                string sEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(dy_data[0]) + "&CarTypeID=" + ConfigValue.GetEncodeText(dy_data[1] + "")
                     , sUrl = "truck_edit.aspx?str=";
                xcpn.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                break;
            case "AddT3":
                //int inxAddT3 = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                //dynamic dy_dataAdd = gvw1.GetRowValues(inxAddT3, "STRUCKID", "STRAILERID", "SCARTYPEID");
                //string sEncryptAd = Server.UrlEncode(STCrypt.Encrypt("Add&" + dy_dataAdd[0] + "&" + dy_dataAdd[1] + "&" + dy_dataAdd[2]))
                //    , sUrlAd = "truck_edit_p4.aspx?str=";
                //xcpn.JSProperties["cpRedirectTo"] = sUrlAd + sEncryptAd;
                string aEncrypt = ConfigValue.GetEncodeText("Add")
                   , aUrl = "truck_add.aspx?str=";
                xcpn.JSProperties["cpRedirectTo"] = aUrl + aEncrypt;
                break;
            case "AddVehicle":
                //int inxAddT3 = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                //dynamic dy_dataAdd = gvw1.GetRowValues(inxAddT3, "STRUCKID", "STRAILERID", "SCARTYPEID");
                //string sEncryptAd = Server.UrlEncode(STCrypt.Encrypt("Add&" + dy_dataAdd[0] + "&" + dy_dataAdd[1] + "&" + dy_dataAdd[2]))
                //    , sUrlAd = "truck_edit_p4.aspx?str=";
                //xcpn.JSProperties["cpRedirectTo"] = sUrlAd + sEncryptAd;
                string EncryptVehicle = ConfigValue.GetEncodeText("Add")
                   , Vehicle = "Vehicle.aspx?str=";
                xcpn.JSProperties["cpRedirectTo"] = Vehicle + EncryptVehicle;
                break;
            case "EditVehicle":
                int inxTVehicle = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                dynamic dy_data_Vehicle = vehiclegrid.GetRowValues(inxTVehicle, "STRUCKID", "STRAILERREGISTERNO", "SCARTYPEID");
                string EncryptVehicle_edit = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(dy_data_Vehicle[0]) + "&CarTypeID=" + ConfigValue.GetEncodeText(dy_data_Vehicle[2] + "")
                    , Vehicle_edit = "Vehicle_edit.aspx?str=";
                xcpn.JSProperties["cpRedirectTo"] = Vehicle_edit + EncryptVehicle_edit;
                break;
            case "EditT4":
                int inxT4 = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                dynamic dataT4 = gvw2.GetRowValues(inxT4, "SEMPLOYEEID");
                string SEMPLOYEEID = dataT4 + "";
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
                string P4 = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx?p4=" + P4;
                break;
            case "AddT4":
                byte[] plaintextBytess = Encoding.UTF8.GetBytes("NEW");
                string P4s = MachineKey.Encode(plaintextBytess, MachineKeyProtection.All);
                xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx?p4=" + P4s;
                break;
            case "ChkInUse":
                dynamic EmpList = gvw2.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SEMPLOYEEID", "SPERSONELNO", "EMPSAPID", "STRANS_ID", "FNAME", "LNAME", "SDRIVERNO", "STEL", "STEL2", "DDRIVEBEGIN", "DDRIVEEXPIRE", "DRVSTATUS");
                string SEMPLOYEEID2 = EmpList[0], Percode = EmpList[1]
                    , EMPSAPID = (object)EmpList[2] == DBNull.Value ? string.Empty : EmpList[2]
                    , vendor = (object)EmpList[3] == DBNull.Value ? string.Empty : EmpList[3]
                    , FIRST_NAME = (object)EmpList[4] == DBNull.Value ? string.Empty : EmpList[4]
                    , LAST_NAME = (object)EmpList[5] == DBNull.Value ? string.Empty : EmpList[5]
                    , LICENSE_NO = (object)EmpList[6] == DBNull.Value ? string.Empty : EmpList[6]
                    , Phone_1 = (object)EmpList[7] == DBNull.Value ? string.Empty : EmpList[7]
                    , Phone_2 = (object)EmpList[8] == DBNull.Value ? string.Empty : EmpList[8]
                    , DRV = (object)EmpList[11] == DBNull.Value ? string.Empty : EmpList[11];
                DateTime? LICENSENOE_FROM = (object)EmpList[9] == DBNull.Value ? null : EmpList[9]
                    , LICENSENO_TO = (object)EmpList[10] == DBNull.Value ? null : EmpList[10];
                try
                {
                    #region SAP
                    string Check = "N";
                    string sMsg = "";
                    string mess = string.Empty;
                    bool isRes = false;
                    #region SAP_INS

                    //string QUERY = "SELECT * FROM SAP_DRIVER_MASTER_DATA WHERE PERS_CODE = '" + CommonFunction.ReplaceInjection(Percode) + "'";
                    string QUERY = "SELECT * FROM SAPECP1000087.SAP_DRIVER_MASTER_DATA@MASTER WHERE PERS_CODE = '" + CommonFunction.ReplaceInjection(Percode) + "'";
                    DataTable dtSap = CommonFunction.Get_Data(sql, QUERY);

                    SAP_Create_Driver veh_driver = new SAP_Create_Driver();
                    veh_driver.DRIVER_CODE = dtSap.Rows.Count > 0 ? dtSap.Rows[0]["DRIVER_CODE"] + "" : EMPSAPID;
                    veh_driver.PERS_CODE = Percode;
                    veh_driver.CARRIER = ""; //ส่งเป็นค่าว่าง กรณีลาออก
                    veh_driver.FIRST_NAME = FIRST_NAME;
                    veh_driver.LAST_NAME = LAST_NAME;
                    veh_driver.DRV_STATUS = DRV;
                    veh_driver.LICENSE_NO = LICENSE_NO == "" ? "#!" : LICENSE_NO;
                    veh_driver.Phone_1 = Phone_1;
                    veh_driver.Phone_2 = Phone_2;

                    veh_driver.LICENSENOE_FROM = LICENSENOE_FROM == null ? null : LICENSENOE_FROM.Value.ToString("dd/MM/yyyy");
                    veh_driver.LICENSENO_TO = LICENSENO_TO == null ? null : LICENSENO_TO.Value.ToString("dd/MM/yyyy");


                    if (dtSap.Rows.Count > 0 || !string.IsNullOrEmpty(EMPSAPID))
                    {
                        //กรณีที่มีการอัพเดทข้อมูล พขร.
                        sMsg = veh_driver.UDP_Driver_SYNC_OUT_SI();
                        Check = sMsg.Substring(0, 1);
                        if (Check == "Y")
                        {
                            string[] datas = sMsg.Split(':');
                            if (datas.Length > 1)
                            {
                                isRes = true;
                                EMPSAPID = datas[1];
                                //UpdateEmpSAPID(EMPID, Percode, EMPSAPID);
                                mess += "แก้ไขข้อมูลพนักงานใน Sap สำเร็จ<br/ >";
                            }
                        }
                        else
                        {
                            isRes = false;
                            mess += "<span style=\"color:Red;\">แก้ไขข้อมูลพนักงานใน Sap ไม่สำเร็จสำเร็จ !!!<br/> " + sMsg + "</span><br/ >";
                        }
                    }
                    else
                    {

                        //กรณีที่มีการสร้างข้อมูล พขร.
                        sMsg = veh_driver.CRT_Driver_SYNC_OUT_SI();
                        Check = sMsg.Substring(0, 1);
                        if (Check == "Y")
                        {
                            string[] datas = sMsg.Split(':');
                            if (datas.Length > 1)
                            {
                                isRes = true;
                                EMPSAPID = datas[1];
                                //UpdateEmpSAPID(EMPID, Percode, EMPSAPID);
                                mess += "เพิ่มข้อมูลพนักงานเข้า Sap สำเร็จ<br/ >";
                            }

                        }
                        else
                        {
                            isRes = false;
                            mess += "<span style=\"color:Red;\">เพิ่มข้อมูลพนักงานเข้า Sap ไม่สำเร็จ !!!<br/> " + sMsg + "</span><br/ >";
                        }
                    }


                    #endregion

                    #endregion
                    if (isRes)
                    {
                        isRes = VendorBLL.Instance.VendorUpdateInuseBySemployeeid(SEMPLOYEEID2 + string.Empty, "0");
                        #region Log
                        string Detail = "ยกเลิกงานจ้างงาน พขร.<br/>";
                        Detail += "ชื่อ : " + FIRST_NAME + " " + LAST_NAME + "<br/>";

                        //Detail += "บริษัท : ผขส. ขอเปลี่ยนข้อมูล<br/>";
                        VendorBLL.Instance.LogInsert(Detail, Percode, null, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, 0);
                        #endregion

                    }

                    if (isRes)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ยกเลิกงานจ้างงาน พขร. เรียบร้อย<br/>" + mess + "',function(){ dxPopupInfo.Hide(); });");
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ไม่สามารถยกเลิกงานจ้างงาน พขร. ได้<br/>" + mess + "',function(){ dxPopupInfo.Hide(); });");
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

                gvw2.DataBind();
                break;

        }
    }

    void ListData()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT s.SVENDORNAME,v.sAbbreviation , (s.sNo || ' ' || s.sDistrict || ' ' || s.sRegion || ' ' || s.sProvince || ' ' || s.sProvinceCode) AS ADDRESS,v.sTel , v.sFax,v.dStartPTT,wm_concat(' ' || c.SCOORDINATORNAME) As SCONAMEAPPEND,v.email FROM (TVENDOR v INNER JOIN TVENDOR_SAP s ON v.SVENDORID = s.SVENDORID) LEFT JOIN TCO_VENDOR c ON v.SVENDORID = c.SVENDORID WHERE  s.SVENDORID = '" + VendorID + "' GROUP BY s.SVENDORNAME,v.sAbbreviation , s.sNo,s.sDistrict,s.sRegion,s.sProvince,s.sProvinceCode,v.sTel , v.sFax,v.dStartPTT,v.email");
        if (dt.Rows.Count > 0)
        {
            lblName.Text = dt.Rows[0]["SVENDORNAME"] + "";
            lblSubname.Text = dt.Rows[0]["SABBREVIATION"] + "";
            lblAddress.Text = dt.Rows[0]["ADDRESS"] + "";
            lblTelephone.Text = dt.Rows[0]["STEL"] + "";
            lblFax.Text = dt.Rows[0]["SFAX"] + "";
            lblEmail.Text = dt.Rows[0]["EMAIL"] + "";
            lblCo.Text = dt.Rows[0]["SCONAMEAPPEND"] + "";
            lblDate.Text = dt.Rows[0]["DSTARTPTT"] + "";
        }
    }
    void SetWebSitemap()
    {

    }
    void BindData()
    {
        if (ASPxPageControl1.ActiveTabIndex == 1)
        {
            string str = "";

            //Cache.Remove(sds.CacheKeyDependency);
            //Cache[sds.CacheKeyDependency] = new object();
            //sds.Select(new System.Web.UI.DataSourceSelectArguments());
            //sds.DataBind();
            sds.SelectCommand = "";
            sds.SelectParameters.Clear();
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                str = "AND (c.SCONTRACTNO LIKE '%' || :oSearch || '%' OR ct.SCONTRACTTYPENAME LIKE '%' || :oSearch || '%')  ";

                sds.SelectParameters.Add("oSearch", txtSearch.Text);

            }

            switch (cboGroup.SelectedIndex)
            {
                case 0:
                    sds.SelectCommand = @"SELECT C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME ,C.DBEGIN, C.DEND, C.NTRUCK, SUM(CASE WHEN  NVL(TCT.CSTANDBY,'N')='Y' THEN 1 ELSE 0 END)  NSTANDBYTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME ,REPLACE(wm_concat(' ' || CTR.SCOORDINATORNAME),',',NULL) As SCONAMEAPPEND 
                                        FROM (TCONTRACT C LEFT JOIN TCONTRACTTYPE CT ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID
                                               LEFT JOIN TVENDOR_SAP V ON C.SVENDORID = V.SVENDORID
                                               LEFT JOIN TTERMINAL T ON C.STERMINALID = T.STERMINALID)
                                        LEFT JOIN TCO_CONTRACT CTR ON C.SCONTRACTID = CTR.SCONTRACTID
                                        LEFT JOIN TCONTRACT_TRUCK TCT ON C.SCONTRACTID = TCT.SCONTRACTID
                                        WHERE 1 = 1 " + str + @"
                                        AND c.SVENDORID = :oSVENDORID AND c.cActive = 'Y' 
                                        AND  (
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  <= TO_DATE(c.DBEGIN,'dd/MM/yyyy')  AND  TO_DATE(c.DBEGIN,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  >= TO_DATE(c.DEND,'dd/MM/yyyy')  AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(c.DBEGIN,'DD/MM/YYYY')  <=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(:oEnd,'dd/MM/yyyy')  <= TO_DATE(c.DEND,'dd/MM/yyyy')  OR
                                            TO_DATE(c.DBEGIN,'dd/MM/yyyy')  >=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy')  
                                        )

                                        GROUP BY C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME, C.DBEGIN, C.DEND, C.NTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME";
                    break;
                case 1:
                    sds.SelectCommand = @"SELECT C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME ,C.DBEGIN, C.DEND, C.NTRUCK,SUM(CASE WHEN  NVL(TCT.CSTANDBY,'N')='Y' THEN 1 ELSE 0 END)  NSTANDBYTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME ,REPLACE(wm_concat(' ' || CTR.SCOORDINATORNAME),',',NULL) As SCONAMEAPPEND 
                                        FROM (TCONTRACT C LEFT JOIN TCONTRACTTYPE CT ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID
                                               LEFT JOIN TVENDOR_SAP V ON C.SVENDORID = V.SVENDORID
                                               LEFT JOIN TTERMINAL T ON C.STERMINALID = T.STERMINALID)
                                        LEFT JOIN TCO_CONTRACT CTR ON C.SCONTRACTID = CTR.SCONTRACTID
                                        LEFT JOIN TCONTRACT_TRUCK TCT ON C.SCONTRACTID = TCT.SCONTRACTID
                                        WHERE 1 = 1 " + str + @" AND c.SVENDORID = :oSVENDORID AND (c.cActive = 'N' Or c.cActive IS NULL) 
--AND c.DBEGIN >=  TO_DATE(:oBegin,'dd/MM/yyyy') AND c.DEND <= TO_DATE(:oEnd,'dd/MM/yyyy')  
                                        AND  (
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  <= TO_DATE(c.DBEGIN,'dd/MM/yyyy')  AND  TO_DATE(c.DBEGIN,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(:oBegin,'dd/MM/yyyy')  >= TO_DATE(c.DEND,'dd/MM/yyyy')  AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy') OR
                                            TO_DATE(c.DBEGIN,'DD/MM/YYYY')  <=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(:oEnd,'dd/MM/yyyy')  <= TO_DATE(c.DEND,'dd/MM/yyyy')  OR
                                            TO_DATE(c.DBEGIN,'dd/MM/yyyy')  >=  TO_DATE(:oBegin,'dd/MM/yyyy') AND  TO_DATE(c.DEND,'dd/MM/yyyy')  <= TO_DATE(:oEnd,'dd/MM/yyyy')  
                                        )
                                        GROUP BY C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME, C.DBEGIN, C.DEND, C.NTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME";
                    break;
            }
            sds.SelectParameters.Add("oSVENDORID", "" + VendorID);
            //sds.SelectParameters.Add("oBegin", dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            //sds.SelectParameters.Add("oEnd", dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sds.SelectParameters.Add("oBegin", dteStart.Text.Trim());
            sds.SelectParameters.Add("oEnd", dteEnd.Text.Trim());

            sds.DataBind();
            gvw.DataBind();
        }
    }
    protected void gvw2_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        string INUSE = e.GetValue("INUSE") + "";
        ASPxButton btn = gvw2.FindRowCellTemplateControl(e.VisibleIndex, null, "btn") as ASPxButton;
        if (INUSE != "0")
        {
            e.Row.BackColor = System.Drawing.Color.White;
            // chk.Checked = true;
            try
            {
                btn.Image.Url = "Images/k2.png";
            }
            catch
            {
            }
        }
        else
        {
            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
            try
            {
                btn.Image.Url = "Images/x2.png";
            }
            catch
            {

            }
            //chk.Checked = false;
        }
        string CACTIVE = e.GetValue("CACTIVE") + string.Empty;
        ASPxLabel lbl = gvw2.FindRowCellTemplateControl(e.VisibleIndex, null, "lblCACTIVE") as ASPxLabel;
        if (lbl != null)
        {
            lbl.Text = CACTIVE == "0" ? "ระงับการใช้งาน" : CACTIVE == "1" ? "อนุญาต" : "Black List";
        }
    }
    protected void vehiclegrid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.VisibleIndex == -1) return;
        int row = int.Parse(vehiclegrid.VisibleRowCount.ToString());
        string SemiTraller = e.GetValue("SCARTYPEID") + "";
        string STRUCKID = e.GetValue("STRUCKID") + "";
        ASPxButton btn = vehiclegrid.FindRowCellTemplateControl(e.VisibleIndex, null, "Editvehicle") as ASPxButton;
        if (string.Equals(SemiTraller, "0"))
        {
            btn.Visible = false;
        }
        else
        {
            btn.Visible = true;
        }

    }
    #region ResponseMenu
    protected void ResponseMenu(object sender, EventArgs e)
    {
        if (string.Equals(CGROUP.ToString(), "1"))
        {
            Response.Redirect("approve_pk.aspx");
        }
        else
        {
            Response.Redirect("vendor_request.aspx");
        }
    }
    #endregion

}