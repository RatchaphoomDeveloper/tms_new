﻿using EmailHelper;
using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;

public partial class KPI_Result : PageBase
{
    #region + View State +
    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }
    #endregion

    DataSet ds;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string[] QueryString = decryptedValue.Split('&');

                ddlYear.SelectedValue = QueryString[0];
                ddlMonth.SelectedValue = QueryString[1];
                btnSearch_Click(null, null);
            }
            if (Session["chkurl"] != null && Session["chkurl"] == "1")
            {
                btnResult.Visible = false;
            }
            //if (Session["CGROUP"] + string.Empty == "0")
            //{
            //    ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
            //    ddlVendor.Enabled = false;
            //    ddlVendor_SelectedIndexChanged(null, null);

            //}
            this.AssignAuthen();
        }
        if (dt != null)
            this.SetColor(ref gvKPI, dt);
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnSearch.CssClass = "btn btn-md bth-hover btn-info";
            }
            if (!CanWrite)
            {
                btnResult.Enabled = false;
                btnResult.CssClass = "btn btn-md bth-hover btn-info";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region DrowDownList
    private void SetDrowDownList()
    {
        for (int i = 2015; i < DateTime.Now.Year + 1; i++)
        {
            ddlYear.Items.Add(new ListItem()
            {
                Text = i + string.Empty,
                Value = i + string.Empty
            });
        }
        ViewState["DataMonth"] = KPI2BLL.Instance.MonthSelect();
        ddlMonth.DataTextField = "NAME_TH";
        ddlMonth.DataValueField = "MONTH_ID";
        ddlMonth.DataSource = ViewState["DataMonth"];
        ddlMonth.DataBind();
        //ddlMonth.Items.Insert(0, new ListItem()
        //{
        //    Text = "--เลือก--",
        //    Value = "0"
        //});

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;

        ViewState["DataStatus"] = KPI2BLL.Instance.KPIStatus("KPI");
        ddlStatus.DataTextField = "STATUS_NAME";
        ddlStatus.DataValueField = "STATUS_VALUE";
        ddlStatus.DataSource = ViewState["DataStatus"];
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });

    }

    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID", typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlContract_SelectedIndexChanged
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
            {
                //dt = AccidentBLL.Instance.GroupSelect(ddlContract.SelectedValue);
                //if (dt.Rows.Count > 0)
                //{
                //    DataRow dr = dt.Rows[0];
                //    ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                //    ddlWorkGroup_SelectedIndexChanged(null, null);
                //    ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                //}
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            gvKPI.DataSource = dt;
            gvKPI.DataBind();

            lblItem.Text = dt.Rows.Count + string.Empty;

            this.SetColor(ref gvKPI, dt);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    public void SetColor(ref GridView dgvSource, DataTable dataSource)
    {
        try
        {
            string ColorName = string.Empty;
            var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(Color));
            Label lbl = new Label();
            for (int i = 0; i < dgvSource.Rows.Count; i++)
            {
                lbl = (Label)dgvSource.Rows[i].FindControl("lblRowColor");
                if (lbl != null)
                {
                    if (!string.Equals(lbl.Text.Replace("&nbsp;", string.Empty), string.Empty))
                    {
                        ColorName = lbl.Text;
                        //dgvSource.Rows[i].ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblVendorName");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblContractName");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblStatus");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblVendorDateTime");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblMonth");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblVendorName");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);

                        lbl = (Label)dgvSource.Rows[i].FindControl("lblVendorName");
                        if (lbl != null)
                            lbl.ForeColor = System.Drawing.Color.FromName(ColorName);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetData
    private void GetData()
    {
        dt = KPI2BLL.Instance.KPIResultSelect(ddlYear.SelectedValue, ddlMonth.SelectedValue, ddlVendor.SelectedValue, ddlContract.SelectedValue, ddlStatus.SelectedValue);
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {

        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
        ddlMonth.SelectedIndex = 0;
        ddlYear.SelectedValue = DateTime.Now.Year + string.Empty;
        lblItem.Text = "0";
        ddlStatus.SelectedIndex = 0;
    }
    #endregion

    #region btnExport_Click
    //protected void btnExport_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        GetData();
    //        dt.Columns.Remove("SYS_TIME");
    //        dt.Columns.Remove("REPORT_PTT");
    //        dt.Columns.Remove("REPORTER");
    //        dt.Columns.Remove("VENDOR_REPORT_TIME");
    //        dt.Columns.Remove("SEND_CONSIDER");
    //        dt.Columns.Remove("PTT_APPROVE");
    //        dt.Columns.Remove("INFORMER_NAME");
    //        dt.Columns.Remove("CACTIVE");
    //        dt.Columns.Remove("CREATE_DATE");
    //        dt.Columns.Remove("CREATE_BY");
    //        dt.Columns.Remove("UPDATE_DATE");
    //        dt.Columns.Remove("UPDATE_BY");
    //        dt.Columns.Remove("ACCIDENTTYPE_ID");
    //        dt.Columns.Remove("SHIPMENT_NO");
    //        dt.Columns.Remove("STRUCKID");
    //        dt.Columns.Remove("SVENDORID");
    //        dt.Columns.Remove("GROUPID");
    //        dt.Columns.Remove("SCONTRACTID");
    //        dt.Columns.Remove("SEMPLOYEEID");
    //        dt.Columns.Remove("STERMINALID");
    //        dt.Columns.Remove("LOCATIONS");
    //        dt.Columns.Remove("GPS");
    //        dt.Columns.Remove("PRODUCT_ID");
    //        dt.Columns.Remove("WORKGROUPNAME");
    //        dt.Columns.Remove("TERMINALNAME");
    //        SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
    //        ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/AccidentFormat.xlsx"));
    //        ExcelWorksheet worksheet = workbook.Worksheets["Accident"];
    //        worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

    //        string Path = this.CheckPath();
    //        string FileName = "Accident_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

    //        workbook.Save(Path + "\\" + FileName);
    //        this.DownloadFile(Path, FileName);
    //    }
    //    catch (Exception ex)
    //    {
    //        alertFail(ex.Message);
    //    }
    //}

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region btnResult_Click
    protected void btnResult_Click(object sender, EventArgs e)
    {
        try
        {
            dt = new DataTable("DT");
            dt.Columns.Add("I_KPI_HEADER_ID", typeof(string));

            foreach (GridViewRow item in gvKPI.Rows)
            {
                CheckBox cb = (CheckBox)item.FindControl("cbResult");
                if (cb != null && cb.Checked)
                {
                    HiddenField hid = (HiddenField)item.FindControl("hidKPI_HEADER_ID");
                    if (hid != null)
                    {
                        dt.Rows.Add(hid.Value);
                    }
                }
            }
            ds = new DataSet("DS");
            ds.Tables.Add(dt.Copy());
            //stasusid = 3 แจ้งผลประเมิน
            if (KPI2BLL.Instance.UpdateKPIStatus(Session["UserID"] + string.Empty, 3, ds))
            {
                string mess = "แจ้งผลสำเร็จ<br/>", vendorOld = string.Empty, vendorIDOld = string.Empty, strContract = string.Empty;
                //send email
                List<string> listVendor = new List<string>(), listVendorID = new List<string>(), listContract = new List<string>();
                foreach (GridViewRow item in gvKPI.Rows)
                {
                    CheckBox cb = (CheckBox)item.FindControl("cbResult");
                    if (cb != null && cb.Checked)
                    {
                        Label lblVendorName = (Label)item.FindControl("lblVendorName");
                        Label lblContractName = (Label)item.FindControl("lblContractName");
                        HiddenField hidSVENDORID = (HiddenField)item.FindControl("hidSVENDORID");
                        if (lblVendorName != null)
                        {
                            if (vendorOld != lblVendorName.Text.Trim())
                            {

                                if (!string.IsNullOrEmpty(vendorOld))
                                {
                                    listVendor.Add(vendorOld);
                                    listVendorID.Add(vendorIDOld);
                                }

                                if (!string.IsNullOrEmpty(strContract))
                                {
                                    listContract.Add(strContract);
                                }
                                vendorOld = lblVendorName.Text.Trim();
                                vendorIDOld = hidSVENDORID.Value;
                                strContract = string.Empty;
                                strContract += lblContractName.Text.Trim();
                            }
                            else
                            {
                                strContract += "," + lblContractName.Text.Trim();
                            }

                        }
                    }
                }
                if (!string.IsNullOrEmpty(vendorOld))
                {
                    listVendor.Add(vendorOld);
                    listVendorID.Add(vendorIDOld);
                }

                if (!string.IsNullOrEmpty(strContract))
                {
                    listContract.Add(strContract);
                }
                for (int i = 0; i < listVendor.Count; i++)
                {
                    SendEmail(ConfigValue.EmailKPI1, listVendorID[i], listVendor[i], listContract[i]);
                }
                mess += "ส่ง E-Mail สำเร็จ";

                byte[] plaintextBytes = Encoding.UTF8.GetBytes(ddlYear.SelectedValue + "&" + ddlMonth.SelectedValue);
                string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess(mess, "KPI_Result.aspx?str=" + ID);
            }
            else
            {
                alertFail("ไม่สามารถแจ้งผลได้กรูณาตรวจสอบข้อมูล");
            }
        }
        catch (Exception ex)
        {

            alertFail("ไม่สามารถแจ้งผลได้เนื่องจาก : " + RemoveSpecialCharacters(ex.Message));
        }


    }
    #endregion

    #region btnCancle_Click
    protected void btnCancle_Click(object sender, EventArgs e)
    {

    }
    #endregion
    #endregion

    #region gvKPI_RowUpdating
    protected void gvKPI_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strData = "View" + "&" + gvKPI.DataKeys[e.RowIndex]["YEAR"] + "&" + gvKPI.DataKeys[e.RowIndex]["MONTH_ID"] + "&" + gvKPI.DataKeys[e.RowIndex]["SCONTRACTID"] + "&" + gvKPI.DataKeys[e.RowIndex]["SVENDORID"];
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
        string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

        Page.ClientScript.RegisterStartupScript(
   this.GetType(), "OpenWindow", "window.open('KPI_Manual_AddEdit.aspx?str=" + encryptedValue + "' ,'_blank');", true);
    }
    #endregion

    #region gvKPI_PageIndexChanging
    protected void gvKPI_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvKPI.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);
    }
    #endregion

    #region gvKPI_RowDataBound
    protected void gvKPI_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dv = (DataRowView)e.Row.DataItem;
            if (dv != null)
            {
                CheckBox cbResult = (CheckBox)e.Row.FindControl("cbResult");
                if (dv["STATUS_ID"] + string.Empty == "2")
                {
                    cbResult.Enabled = true;
                }
                else
                {
                    cbResult.Enabled = false;
                }
                if (Session["chkurl"] != null && Session["chkurl"] == "1")
                {
                    LinkButton lb = (LinkButton)e.Row.FindControl("lnkEditData");
                    lb.Visible = false;
                }
            }
        }
    }
    #endregion

    protected void cbCheckAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox cbResultAll = (CheckBox)sender;
            bool Checked = cbResultAll.Checked;

            CheckBox cbResult;
            for (int i = 0; i < gvKPI.Rows.Count; i++)
            {
                cbResult = (CheckBox)gvKPI.Rows[i].FindControl("cbResult");
                if (cbResult != null && cbResult.Enabled)
                    cbResult.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void gvKPI_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;

            if (string.Equals(e.CommandName, "editData"))
            {
                string strData = "Edit" + "&" + gvKPI.DataKeys[Index]["YEAR"] + "&" + gvKPI.DataKeys[Index]["MONTH_ID"] + "&" + gvKPI.DataKeys[Index]["SCONTRACTID"] + "&" + gvKPI.DataKeys[Index]["SVENDORID"];
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                Page.ClientScript.RegisterStartupScript(
           this.GetType(), "OpenWindow", "window.open('KPI_Manual_AddEdit.aspx?str=" + encryptedValue + "' ,'_blank');", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #region SendEmail
    private bool SendEmail(int TemplateID, string VendorID, string vendor, string strContract)
    {
        try
        {
            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                string EmailList = string.Empty;
                if (TemplateID == ConfigValue.EmailKPI1)
                {
                    #region EmailKPI1
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false);
                    //EmailList += ",zsuntipab.k@pttict.com,zchanaphon.p@pttict.com,zsakchai.p@pttict.com";

                    string SVENDORTITLE = "บริษัท", SVENDORNAME = string.Empty;
                    if (vendor.Contains("บริษัท"))
                    {
                        SVENDORNAME = vendor.Replace("บริษัท", "");
                    }
                    else
                    {
                        SVENDORNAME = vendor.Replace("ห้างหุ้นส่วนจำกัด", "");
                        SVENDORTITLE = "ห้างหุ้นส่วนจำกัด";
                    }
                    Subject = Subject.Replace("{Month}", ddlMonth.SelectedItem != null ? ddlMonth.SelectedItem.Text.Trim() : "-");
                    Subject = Subject.Replace("{Year}", ddlYear.SelectedItem != null ? ddlYear.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{Month}", ddlMonth.SelectedItem != null ? ddlMonth.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{Year}", ddlYear.SelectedItem != null ? ddlYear.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{Title}", SVENDORTITLE);
                    Body = Body.Replace("{Vendor}", SVENDORNAME);
                    Body = Body.Replace("{Contract}", strContract);
                    Body = Body.Replace("{CREATE_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);


                    byte[] plaintextBytes = Encoding.UTF8.GetBytes("&" + ddlYear.SelectedValue + "&" + ddlMonth.SelectedValue + "&&");
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "KPI_Manual.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailKPI1", ColumnEmailName);
                    #endregion

                }

                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion
}