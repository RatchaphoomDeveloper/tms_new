﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using DevExpress.Web.ASPxCallbackPanel;
using System.Web.Configuration;

public partial class Truck_History_Info : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = STCrypt.DecryptURL(str);
                Session["VTYPE"] = QueryString[0] + "";
                txtHTruckNo.Text = QueryString[2] + "";
                
                DataTable dt;
                dt = CommonFunction.Get_Data(conn, "SELECT * FROM TTRUCK WHERE STRUCKID='" + QueryString[2] + "'");
                foreach (DataRow dr in dt.Rows)
                {
                    txtHTruckNo.Text = dr["SHEADREGISTERNO"] + "";
                    txtHChasis.Text = dr["SCHASIS"] + "";
                }                
            }
            LoadData();
  
        }
    }
    private void LoadData()
    {
        dts1.SelectCommand = "SELECT G.UPDATE_DATE,T.SFIRSTNAME || T.SLASTNAME as FULLNAME,G.REGISNO,G.HEADDER , G.NEWDATA,G.OLDDATA FROM TTRUCK_LOG G,TUSER T WHERE T.SUID=G.USER_ID AND G.REGISNO='" + txtHTruckNo.Text + "' ORDER BY G.UPDATE_DATE DESC ";
        gvw.DataSource = dts1;
        gvw.DataBind();
        
        
    }
    #region DivLog
    public string DivLog(string log, string title, string drOld, string strNew)
    {
        string strData = drOld + string.Empty;
        if (log.Length <= 3900 && strData != strNew)
        {
            log += "<div class=\"row\">";
            log += "<label class=\"col-md-6  text-right\">" + title + " :</label>";
            log += "<div class=\"col-md-6\">" + IfIsNullOrEmtpy(strData) + "</div>";
            log += "</div>";
            log += "<div class=\"row \">";
            log += "<label class=\"col-md-6  text-right\">>></label>";
            log += "<div class=\"col-md-6\">" + IfIsNullOrEmtpy(strNew) + "</div>";
            log += "</div>";
        }
        return log;
    }
    #endregion
    #region IfIsNullOrEmtpy
    private string IfIsNullOrEmtpy(string strIf)
    {
        return string.IsNullOrEmpty(strIf) ? "ไม่ระบุ" : strIf;
    }
    private string IfIsNullOrEmtpy(string strIf, string strValue)
    {
        return string.IsNullOrEmpty(strIf) ? "ไม่ระบุ" : strValue;
    }
    #endregion
    
      
}