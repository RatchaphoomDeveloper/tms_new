﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckOldVersion.aspx.cs" Inherits="CheckOldVersion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <asp:TextBox ID="txtPassword" runat="server" Width="120px" style="text-align:center" TextMode="Password"></asp:TextBox>
        <asp:Button ID="cmdGo" runat="server" Text="ยืนยัน" Width="70px" 
                onclick="cmdGo_Click" />
        </div>
        <br />

        <div>
            <asp:RadioButtonList ID="radPage" runat="server" RepeatDirection="Vertical">
                <asp:ListItem Selected="True" Text="ข้อร้องเรียน" Value="admin_Complain_lst_Jang.aspx"></asp:ListItem>
                <asp:ListItem Text="การยืนยันรถ" Value="OrderPlan_Jang.aspx"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </form>
</body>
</html>
