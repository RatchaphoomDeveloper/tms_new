﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;
using System.IO;
using DevExpress.Web.ASPxGridView.Export;
using DevExpress.XtraPrinting;
using System.Configuration;
using TMS_BLL.Transaction.Complain;

public partial class final_grade : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {

            txtratio_grade_lab.Text = ConfigurationSettings.AppSettings["ratio_grade_lab"] + "";
            txtratio_grade_lab_Diff.Text = "" + (double.Parse(ConfigurationSettings.AppSettings["ratio_grade_lab"] + "") / 100);
            txtratio_grade_home.Text = ConfigurationSettings.AppSettings["ratio_grade_home"] + "";
            txtratio_grade_home_Diff.Text = "" + (double.Parse(ConfigurationSettings.AppSettings["ratio_grade_home"] + "") / 100);
            txtratio_grade_KPI.Text = ConfigurationSettings.AppSettings["ratio_grade_KPI"] + "";
            txtratio_grade_KPI_Diff.Text = "" + (double.Parse(ConfigurationSettings.AppSettings["ratio_grade_KPI"] + "") / 100);

            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            DataTable dt = CommonFunction.Get_Data(sql, "SELECT TO_CHAR(DREDUCE,'YYYY') AS SYEAR FROM TREDUCEPOINT GROUP BY TO_CHAR(DREDUCE,'YYYY') ORDER BY TO_CHAR(DREDUCE,'YYYY') DESC");

            cmbYear.DataSource = dt;
            cmbYear.DataBind();

            #region " Old Comment -> เปลี่ยนจาก พศ เป็น คศ "
            //if (dt.Rows.Count > 0)
            //{
            //    bool checkYear = false;
            //    string cYear = DateTime.Today.Year.ToString();
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        int Year = Convert.ToInt32(dt.Rows[i]["SYEAR"]);

            //        cmbYear.Items.Add(Year + 543 + "", Year);
            //        if ("" + dt.Rows[i]["SYEAR"] == cYear)
            //        {
            //            checkYear = true;
            //        }
            //    }
            //    if (checkYear == false)
            //    {

            //        cmbYear.Items.Insert(0, new ListEditItem(int.Parse(cYear) + 543 + "", cYear));
            //    }
            //    cmbYear.SelectedIndex = 0;
            //}
            #endregion " "

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            LoadVendor();

            LogUser("43", "R", "เปิดดูข้อมูลหน้า รายงานผลประเมินผู้ประกอบการประจำปี", "");
        }

    }

    private void LoadVendor()
    {
        try
        {
            DataTable dtVendor = new DataTable();
            dtVendor = ComplainBLL.Instance.VendorSelectBLL();
            cmbVendor.DataSource = dtVendor;
            cmbVendor.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        //string[] paras = e.Parameter.Split(';');

        //switch (paras[0])
        //{
        //    case "Search":

        //        Cache.Remove(sds.CacheKeyDependency);
        //        Cache[sds.CacheKeyDependency] = new object();
        //        sds.Select(new System.Web.UI.DataSourceSelectArguments());
        //        sds.DataBind();
        //        gvw.DataBind();
        //        break;

        //    case "edit":

        //        int Index = int.Parse(e.Parameter.Split(';')[1]);
        //        gvw.StartEdit(Index);
        //        dynamic SCONTRACTID = gvw.GetRowValues(Index, "SCONTRACTID");
        //        Session["fCONTRACTID"] = SCONTRACTID + "";

        //        break;

        //}

    }
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    protected void btnPdfExport_Click(object sender, EventArgs e)
    {
        int rownum = gvw.EditingRowVisibleIndex;
        dynamic data = gvw.GetRowValues(rownum, "SVENDORNAME", "SCONTRACTNO");
        ASPxGridView sgvw = gvw.FindEditFormTemplateControl("sgvw") as ASPxGridView;

        PdfExportOptions options = new PdfExportOptions();

        if (sgvw.VisibleRowCount > 0)
        {
            dynamic ddd = sgvw.GetRowValues(0, "SUMPOINT", "QUESTIONNAIRESCORE", "NGRADE", "SGRADENAME");

            gridExport.PageHeader.Left = "ชื่อผู้ประกอบการ : " + data[0] + " เลขที่สัญญา : " + data[1] + " คะแนนปฏิบัติงานที่ได้(" + ConfigurationSettings.AppSettings["ratio_grade_lab"] + "%) : " + ddd[0] + " คะแนนบริหารงานที่ได้(" + ConfigurationSettings.AppSettings["ratio_grade_home"] + "%) : " + ddd[1] + " คะแนนรวม : " + ddd[2] + " Grade : " + ddd[3];

        }
        else
        {
            gridExport.PageHeader.Left = "ชื่อผู้ประกอบการ : " + data[0] + " เลขที่สัญญา : " + data[1];
        }

        gridExport.Landscape = true;
        gridExport.MaxColumnWidth = 180;
        gridExport.WritePdfToResponse("รายงานผลประเมินผู้ประกอบการประจำปี", options);
    }
    protected void btnXlsExport_Click(object sender, EventArgs e)
    {
        int rownum = gvw.EditingRowVisibleIndex;
        dynamic data = gvw.GetRowValues(rownum, "SVENDORNAME", "SCONTRACTNO");
        ASPxGridView sgvw = gvw.FindEditFormTemplateControl("sgvw") as ASPxGridView;

        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;

        if (sgvw.VisibleRowCount > 0)
        {
            dynamic ddd = sgvw.GetRowValues(0, "SUMPOINT", "QUESTIONNAIRESCORE", "NGRADE", "SGRADENAME");

            gridExport.PageHeader.Left = "ชื่อผู้ประกอบการ : " + data[0] + " เลขที่สัญญา : " + data[1] + " คะแนนปฏิบัติงานที่ได้(" + ConfigurationSettings.AppSettings["ratio_grade_lab"] + "%) : " + ddd[0] + " คะแนนบริหารงานที่ได้(" + ConfigurationSettings.AppSettings["ratio_grade_home"] + "%) : " + ddd[1] + " คะแนนรวม : " + ddd[2] + " Grade : " + ddd[3];

        }
        else
        {
            gridExport.PageHeader.Left = "ชื่อผู้ประกอบการ : " + data[0] + " เลขที่สัญญา : " + data[1];
        }

        //gridExport.ExportToXls("File.xls", options);
        gridExport.WriteXlsToResponse("รายงานผลประเมินผู้ประกอบการประจำปี", options);
        //gridExport.WriteXlsToResponse();
    }
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string[] Para = e.Args[0].Split(';');

                switch (Para[0])
                {
                    case "Search":
                        Cache.Remove(sds.CacheKeyDependency);
                        Cache[sds.CacheKeyDependency] = new object();
                        sds.Select(new System.Web.UI.DataSourceSelectArguments());
                        sds.DataBind();
                        gvw.DataBind();
                        break;

                    case "edit":

                        int Index = int.Parse(Para[1]);
                        gvw.StartEdit(Index);
                        dynamic ContractID = gvw.GetRowValues(Index, "SCONTRACTID");
                        Session["fCONTRACTID"] = ContractID + "";

                        //string sss =      sds1.SelectCommand;
                        //DataTable dt = ((DataView)sds1.Select(DataSourceSelectArguments.Empty)).ToTable();

                        //((DataView)sds1.Select(DataSourceSelectArguments.Empty)).ToTable().Select("SCONTRACTID='" + ContractID + "'")
                        //((ASPxLabel)gvw.FindEditFormTemplateControl("lblGrade")).Text = "";
                        break;

                }
                break;
        }
    }

    protected void sgvw_Init(object sender, EventArgs e)
    {
        ASPxGridView _gvw = ((ASPxGridView)sender);
        _gvw.Columns[0].Caption = "คะแนนปฏิบัติงานที่ได้(" + ConfigurationSettings.AppSettings["ratio_grade_lab"] + "%)";
        _gvw.Columns[1].Caption = "คะแนนบริหารงานที่ได้(" + ConfigurationSettings.AppSettings["ratio_grade_home"] + "%)";
        _gvw.Columns[2].Caption = "คะแนน KPI ที่ได้(" + ConfigurationSettings.AppSettings["ratio_grade_KPI"] + "%)";
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {

    }
    protected void btnNoticeToVendor_Click(object sender, EventArgs e)
    {

    }
}
