﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="SurpriseCheckContractAddEditTab2.aspx.cs" Inherits="SurpriseCheckContractAddEditTab2" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <style>
        .CheckList td {
            border: 0px solid #a79d9d !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                <ul class="nav nav-tabs" runat="server" id="tabtest">
                    <li class="" id="liTab1" runat="server"><a href="#TabGeneral" runat="server" id="GeneralTab1">ข้อมูลเบื้องต้น</a></li>
                    <li class="active" id="liTab2" runat="server"><a href="#TabGeneral2" runat="server" id="GeneralTab2">ปัญหา</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="TabGeneral2">
                        <asp:Panel runat="server" ID="plCheckList">

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">รายการ</a>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <asp:GridView runat="server" ID="gvCheckList"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover " HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="ID,DETAIL" OnRowDataBound="gvCheckList_RowDataBound" OnRowCommand="gvCheckList_RowCommand">
                                            <Columns>
                                                <asp:BoundField DataField="LISTNO" HeaderText="ที่" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="50" HeaderStyle-CssClass="text-center" />
                                                <asp:BoundField DataField="DETAIL" HeaderText="หัวข้อการตรวจสอบ" ItemStyle-Width="45%" HeaderStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="DETAIL_TYPE" HeaderText="วิธีการตรวจสอบ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80" />
                                                <asp:TemplateField HeaderText="ถูกต้องครบถ้วน" ItemStyle-Wrap="false" ItemStyle-Width="200">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderTemplate>
                                                        <asp:RadioButtonList runat="server" ID="rblIS_ACTIVEALL" OnSelectedIndexChanged="rblIS_ACTIVEALL_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="CheckList">
                                                            <asp:ListItem Text="ผ่าน" Value="2" />
                                                            <asp:ListItem Text="แก้ไข" Value="3" />
                                                            <asp:ListItem Text="นัดหมายใหม่" Value="4" />
                                                        </asp:RadioButtonList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:RadioButtonList runat="server" ID="rblIS_ACTIVE" RepeatDirection="Horizontal" CssClass="CheckList" AutoPostBack="true" OnSelectedIndexChanged="rblIS_ACTIVE_SelectedIndexChanged">
                                                            <asp:ListItem Text="ผ่าน" Value="2" />
                                                            <asp:ListItem Text="แก้ไข" Value="3" />
                                                            <asp:ListItem Text="นัดหมายใหม่" Value="4" />
                                                        </asp:RadioButtonList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" ID="txtREMARK" TextMode="MultiLine" Rows="3" CssClass="form-control" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false" ItemStyle-Width="80">
                                                    <ItemStyle HorizontalAlign="Center" />

                                                    <ItemTemplate>
                                                        <asp:Button Text="แก้ไข" ID="btnStep1" runat="server" CommandArgument="File" CommandName='<%# Container.DataItemIndex %>' CssClass="btn btn-info " Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4">
                                            <input id="btnSave" runat="server" type="button" value="บันทึก" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" />
                                            <asp:Button runat="server" ID="mpBack" CssClass="btn btn-danger" Text="ยกเลิก" OnClick="btnClose_Click" />
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse2" />
                            </div>


                        </asp:Panel>
                        <asp:Panel runat="server" ID="plStep1" CssClass="hidden">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4">
                                        <asp:Label Text="" ID="lblSCHECKLISTNAME" runat="server" /></a>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse in">
                                    <div class="panel-body">

                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">เลือกรูปภาพ<asp:Label ID="Label1" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:FileUpload ID="fileUploadStep1" accept="image/*" runat="server" />
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtDetailStep1" CssClass="form-control " />
                                            </div>

                                            <div class="col-md-2">

                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:Button runat="server" ID="btnAddStep1" Text="Add" OnClick="btnAddStep1_Click" CssClass="btn btn-md bth-hover btn-info " />
                                                    </ContentTemplate>
                                                    <Triggers>

                                                        <asp:PostBackTrigger ControlID="btnAddStep1" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                            </div>
                                        </div>
                                        <asp:GridView runat="server" ID="gvFileStep1"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="false" DataKeyNames="ID" OnRowDeleting="gvFileStep1_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รูปภาพ">
                                                    <ItemTemplate>
                                                        <a href='<%# Eval("FULLPATH") %>' target="_blank">
                                                            <asp:Image ID="imgStep1" runat="server" ImageUrl='<%# Eval("FULLPATH") %>' Width="100%" /><br />

                                                            <asp:Label Text='<%# "รายละเอียด : " + Eval("REMARK") %>' ID="lblDetail" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="50">
                                                    <ItemTemplate>
                                                        <asp:Button Text="ลบ" CssClass="btn btn-danger" CommandName="delete" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" placeholder="หมายเหตุ" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right">
                                    <asp:Button Text="บันทึกรายการตรวจสอบ" ID="Button1" OnClick="btnCloseStep1_Click" CssClass="btn btn-info" runat="server" />
                                    <asp:Button Text="กลับ" ID="btnCloseStep1" OnClick="btnCloseStep1_Click" CssClass="btn btn-warning" runat="server" />
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse4" />
                            </div>
                        </asp:Panel>
                        <asp:HiddenField ID="hidRowIndex" runat="server" />
                        <asp:HiddenField ID="hidM_CHECK_CONTRACT_LIST_ID" runat="server" />

                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hidID" />
            <asp:HiddenField runat="server" ID="hidIS_ACTIVE" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="btnSave_Click" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

