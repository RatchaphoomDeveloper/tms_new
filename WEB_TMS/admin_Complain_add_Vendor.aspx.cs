﻿using System;
using System.Web.UI;
using System.Data;
using System.Text;
using TMS_BLL.Transaction.Complain;
using System.Web.UI.HtmlControls;
using System.Web.Security;

public partial class admin_Complain_add_Vendor : PageBase
{
    #region + ViewState +
    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {

                var decryptedBytes = MachineKey.Decode(Request.QueryString["DocID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                DocID = decryptedValue;
            }

            lblHeader.Text = string.Format(lblHeader.Text, DocID);

            DataTable dt = new DataTable();
            dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreBLL(DocID);
            if (dt.Rows.Count > 0)
            {
                txtPointFinal.Enabled = false;
                txtCostFinal.Enabled = false;
                txtDisableFinal.Enabled = false;

                txtPointFinal.Text = dt.Rows[0]["TOTAL_POINT"].ToString();
                txtCostFinal.Text = dt.Rows[0]["TOTAL_COST"].ToString();
                txtDisableFinal.Text = dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
}