﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Mapping_Form.aspx.cs" Inherits="KPI_Mapping_Form" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="form-horizontal">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>ตั้งค่าชุดประเมิน KPI
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <div class="row form-group">
                            <label class="col-md-4 control-label">ปี</label>
                            <div class="col-md-4">
                                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                </div>
                            <div class="col-md-8">
                            <asp:GridView runat="server" ID="gvKPIMapping"
                                Width="100%" HeaderStyle-HorizontalAlign="Center"
                                GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                AllowPaging="false" OnRowDataBound="gvKPIMapping_RowDataBound" >
                                <Columns>
                                    <asp:BoundField HeaderText="ที่" DataField="MONTH_ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

                                    <asp:BoundField HeaderText="เดือน" DataField="NAME_TH" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="แบบประเมิน" ItemStyle-Wrap="false">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hidMONTH_ID" Value='<%# Eval("MONTH_ID") %>' />
                                            <asp:DropDownList runat="server" ID="ddlKPITopic" CssClass="form-control">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                                </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


            </div>
            <div class="panel-footer text-right">
                <asp:Button Text="บันทึก" runat="server" ID="btnSave" class="btn btn-md bth-hover btn-info" OnClick="btnSave_Click"/>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <input type="button" id="btnConfirmBeforeSave" name="name" value=" " data-toggle="modal" data-target="#ModalConfirmBeforeSaveCheckVendor" />
    </div>
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSaveCheckVendor" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK" TextTitle="ยืนยันการบันทึกข้อมูล" TextDetail="คุณต้องการบันทึกข้อมูลใช่หรือไม่ ?" CommandArgument="ConfirmBeforeSaveCheckVendor" />
    <script>
        function confirmChangevendor() {
            $('#btnConfirmBeforeSave').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

