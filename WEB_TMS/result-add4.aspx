﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="result-add4.aspx.cs" Inherits="result_add4" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Jquery/Number/jquery.number.js" type="text/javascript"></script>
    <script src="Javascript/Jquery/Number/jquery.number.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">


        function CalGvw(i) {



            if (window["txtItem_" + i + ""].GetValue() == null || window["txtItem_" + i + ""].GetValue() == 0) {
                window["lblCal_" + i + ""].SetText('');
                window["lblCal_Show_" + i + ""].SetText('');
            }
            else {
                var Price = parseInt(window["lblSERVICECHARGE_" + i + ""].GetValue()) * parseInt(window["txtItem_" + i + ""].GetValue());



                window["lblCal_" + i + ""].SetText(Price);
                window["lblCal_Show_" + i + ""].SetText(format(parseInt(Price)));
            }
            Total();
        }


        function CalGvwChk(i) {

            if (window["chkService_" + i + ""].GetChecked()) {

                var Price = window["lblSERVICECHARGE_" + i + ""].GetValue();

                window["lblCal_" + i + ""].SetText(Price);
                window["lblCal_Show_" + i + ""].SetText(format(parseInt(Price)));
            }
            else {
                window["lblCal_" + i + ""].SetText('');
                window["lblCal_Show_" + i + ""].SetText('');
            }
            Total();
        }

        function IsValidNumber() {
            //ตรวจเช็ค keyCode
            if ((event.keyCode >= 48) && (event.keyCode <= 57))
            { }
            else
            { event.returnValue = false; } //หรือ { event.keyCode = 0 ; }
        }

        function format(num) {
            var sValue = num.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            var result = "";
            var chkZeroZero = sValue.split(".");
            if (chkZeroZero[1] == "00") {
                result = sValue.replace('.00', '');
            }
            else {
                result = sValue;
            }


            return result;
        }

        function Total() {
            var gvwrow = txtgvwrowcount.GetValue();

            var Result = "0";

            for (var i = 0; i < gvwrow; i++) {

                Result = parseInt(Result) + parseInt(CheckNullInJAVA(window["lblCal_" + i + ""].GetValue()));

            }

            lblTotal.SetText(Result);
            lblTotal_SHOW.SetText(format(parseInt(Result)));
        }

        function CheckNullInJAVA(Value) {
            var Result = "0";

            if (Value) {
                Result = Value;

            }
            return Result;
        }

        function GetCarNoCompleteTotal(i) {

            if (i) {

                var setPrice = parseInt(window["lblStdPriceServiec6"].GetValue()) * parseInt(CheckNullInJAVA(window["txtCarNoComplete"].GetValue()));
                var Price = format(parseInt(window["lblStdPriceServiec6"].GetValue()) * parseInt(CheckNullInJAVA(window["txtCarNoComplete"].GetValue())));
                window["txtCarNoComplete"].SetEnabled(true);
                window["lblCarnocomplete"].SetText(Price);
                window["txtshowCarnocomplete"].SetText(setPrice);


            }
            else {
                window["txtCarNoComplete"].SetEnabled(false);
                window["lblCarnocomplete"].SetText('');
                window["txtshowCarnocomplete"].SetText('');
            }

        }

    </script>
    <style type="text/css">
        .Hide
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" HideContentOnCallback="false"
        CausesValidation="False" OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                            <tr>
                                                <td colspan="4" align="right">
                                                    <%--<input type="submit" name="button3" id="button3" value="ดูประวัติรถ" onclick="javascript:location.href='car_history.htm'">--%>
                                                    <%--<input type="submit" name="button7" id="button7" value="ประวัติการรับบริการ" onclick="javascript:location.href='service_history.htm'">--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">วันที่ยืนคำขอ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQUEST_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">วันที่หมดอายุวัดน้ำ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblDWATEREXPIRE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="18%" bgcolor="#B9EAEF">วันที่นัดหมาย</td>
                                                <td width="32%" class="active">
                                                    <dx:ASPxLabel ID="lblSERVICE_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="19%" bgcolor="#B9EAEF">วันที่ รข. อนุมัติคำขอ</td>
                                                <td width="31%">
                                                    <dx:ASPxLabel ID="lblAPPOINTMENT_BY_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">ประเภทคำขอ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQTYPE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">สาเหตุ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblCAUSE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">ทะเบียนรถ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREGISTERNO" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">ประเภทรถ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblTypeCar" runat="server">
                                                    </dx:ASPxLabel>
                                                    - ความจุ
                                                    <dx:ASPxLabel ID="lblTotalCap" runat="server">
                                                    </dx:ASPxLabel>
                                                    ลิตร</td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">บริษัทขนส่ง </td>
                                                <td colspan="3">
                                                    <dx:ASPxLabel ID="lblVendorName" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trtab">
                                    <td>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT1" runat="server" Text="ประวัติการตรวจ" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT1;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT2" runat="server" Text="ตรวจสภาพภายนอก/ใน" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT2;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT3" runat="server" Text="ผลตรวจลงน้ำ" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT3;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#00CC33">
                                                    <dx:ASPxHyperLink ID="hplT4" runat="server" Text="คำนวณค่าธรรมเนียมเพิ่ม" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                  <td width="15%" align="center" bgcolor="#EDEDED">
                                                                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="รูปภาพการตรวจสอบ" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                    <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectImg;')}" />
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT5" runat="server" Text="บันทึกผลปิดงาน" Style="text-decoration: none;
                                                        cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT5;')}" />
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="10%">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp; </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                        <tr>
                                                            <td width="6%" align="left" bgcolor="#B9EAEF">ค่าธรรมเนียมเพิ่มเติม</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <dx:ASPxTextBox runat="server" ID="txtgvwrowcount" ClientInstanceName="txtgvwrowcount"
                                                                    ClientVisible="false">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxGridView runat="server" ID="gvwServiceOther" Width="100%" KeyFieldName="SERVICE_ID"
                                                                    Border-BorderWidth="0" ViewStateMode="Enabled" ClientInstanceName="gvwServiceOther">
                                                                  <%--  <TotalSummary>
                                                                        <dx:ASPxSummaryItem FieldName="NPRICE_SHOW" SummaryType="Max" ShowInGroupFooterColumn="ค่าบริการ"
                                                                            DisplayFormat="  {0}" />
                                                                        <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="TEXT" ShowInGroupFooterColumn="ประเภท"
                                                                            SummaryType="Max" />
                                                                        <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="UNITS" ShowInGroupFooterColumn="หน่วยค่าบริการ"
                                                                            SummaryType="Max" />
                                                                    </TotalSummary>--%>
                                                                    <Columns>
                                                                        <%--<dx:GridViewDataColumn FieldName="NPRICE" VisibleIndex="0" Visible="true">
                                                                        </dx:GridViewDataColumn>--%>
                                                                        <dx:GridViewDataColumn Caption="ประเภท" Width="40%" CellStyle-HorizontalAlign="Right"
                                                                            FieldName="TEXT" FooterCellStyle-HorizontalAlign="Right">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxCheckBox runat="server" ID="chkService" ClientVisible='<%# Eval("CHECKFROM").ToString()=="Y" ? true : false%>'
                                                                                    Checked='<%# Eval("NPRICE").ToString() != "" ? true: false %>'>
                                                                                    <ClientSideEvents ValueChanged="function(s,e){CalGvwChk(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxLabel runat="server" ID="lblDetail" Text='<%# Eval("SERVICE_NAME") %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="ราคา" Width="8%" CellStyle-HorizontalAlign="Right">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel runat="server" ID="lblSERVICECHARGE" Text='<%# Eval("SERVICECHARGE") %>'
                                                                                    ClientInstanceName="lblSERVICECHARGE">
                                                                                </dx:ASPxLabel>
                                                                                x
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="จำนวน" Width="8%">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxTextBox runat="server" ID="txtItem" Text='<%# Eval("USERITEM") %>' Width="90%"
                                                                                    ClientInstanceName="txtItem" HorizontalAlign="Center">
                                                                                    <ClientSideEvents KeyUp="function(s,e){CalGvw(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }"
                                                                                        KeyPress="function(s,e){IsValidNumber();}" />
                                                                                </dx:ASPxTextBox>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="หน่วย" Width="8%" CellStyle-HorizontalAlign="Right">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel runat="server" ID="lblunit" Text='<%# Eval("UNIT")%>' CssClass="dxeLineBreakFix">
                                                                                </dx:ASPxLabel>
                                                                                =
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="ค่าบริการ" Width="8%" FieldName="NPRICE_SHOW" CellStyle-HorizontalAlign="Right"
                                                                            FooterCellStyle-HorizontalAlign="Right">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxTextBox runat="server" ID="lblCal" ClientVisible="false" Text='<%# Eval("NPRICE")%>'>
                                                                                </dx:ASPxTextBox>
                                                                                <dx:ASPxLabel runat="server" ID="lblCal_Show" Text='<%# Eval("NPRICE_SHOW")%>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="หน่วยค่าบริการ" Width="28%" FieldName="UNITS">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel runat="server" ID="lblCalUnit2" Text="บาท">
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn FieldName="SERVICE_ID" VisibleIndex="0" Visible="false">
                                                                        </dx:GridViewDataColumn>
                                                                    </Columns>
                                                                    <Settings ShowColumnHeaders="false" GridLines="None" ShowFooter="false" />
                                                                    <Border BorderWidth="0px"></Border>
                                                                </dx:ASPxGridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="56%" align="right">
                                                                            <dx:ASPxLabel runat="server" ID="lbltexttotoal" Text="ค่าธรรมเนียมเพิ่มเติม">
                                                                            </dx:ASPxLabel>&nbsp;
                                                                        </td>
                                                                        <td width="7%" align="right">= </td>
                                                                        <td width="9%" align="right">&nbsp;
                                                                            <dx:ASPxLabel runat="server" ID="lblTotal" ClientInstanceName="lblTotal" ClientVisible="false">
                                                                            </dx:ASPxLabel>
                                                                            <dx:ASPxLabel runat="server" ID="lblTotal_SHOW" ClientInstanceName="lblTotal_SHOW">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td width="28%" align="left">บาท </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" bgcolor="#B9EAEF">ค่าปรับ</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                                    <tr>
                                                                        <td width="32%" align="right">
                                                                            <dx:ASPxCheckBox ID="ckbCarNotCheckingWater" runat="server" ClientInstanceName="ckbCarNotCheckingWater"
                                                                                Text="สภาพรถไม่พร้อมที่จะรับการตวงน้ำ และวัดปริมาตร" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents CheckedChanged="function(s,e) { var ccheck = s.GetValue(); GetCarNoCompleteTotal(ccheck);  }" />
                                                                            </dx:ASPxCheckBox>
                                                                        </td>
                                                                        <td width="68%">
                                                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                                                <tr>
                                                                                    <td width="8%">
                                                                                        <dx:ASPxTextBox runat="server" ID="txtCarNoComplete" ClientInstanceName="txtCarNoComplete"
                                                                                            HorizontalAlign="Center">
                                                                                            <ClientSideEvents KeyUp="function(s,e) { var ccheck = ckbCarNotCheckingWater.GetValue(); GetCarNoCompleteTotal(ccheck);  }"
                                                                                                KeyPress="function(s,e){IsValidNumber();}" />
                                                                                        </dx:ASPxTextBox>
                                                                                    </td>
                                                                                    <td width="2%">x </td>
                                                                                    <td width="10%">
                                                                                        <dx:ASPxLabel ID="lblStdPriceServiec6" runat="server" ClientInstanceName="lblStdPriceServiec6"
                                                                                            ClientVisible="true">
                                                                                        </dx:ASPxLabel>
                                                                                        = </td>
                                                                                    <td width="10%" align="right">
                                                                                        <dx:ASPxLabel ID="lblCarnocomplete" runat="server" ClientInstanceName="lblCarnocomplete">
                                                                                        </dx:ASPxLabel>
                                                                                        <dx:ASPxTextBox ID="txtshowCarnocomplete" runat="server" ClientInstanceName="txtshowCarnocomplete"
                                                                                            ClientVisible="false">
                                                                                        </dx:ASPxTextBox>
                                                                                    </td>
                                                                                    <td align="left" width="70%">บาท </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                        <tr>
                                                            <td align="left" valign="top" bgcolor="#E5E5E5">หมายเหตุ<br>
                                                            </td>
                                                            <td colspan="3" align="left">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxMemo ID="txtRemark" runat="server" Height="100" Width="400">
                                                                                <ClientSideEvents Validation="function(s,e) {
        if(ckbCarNotCheckingWater.GetChecked()) {
        var editorValue = e.Value;
        e.isValid = true;
        }
    }" />
                                                                                <ValidationSettings EnableCustomValidation="true" ValidationGroup="add">
                                                                                </ValidationSettings>
                                                                            </dx:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxCheckBox ID="ckbSendMailVendor" runat="server" Text="ส่งเมล์แจ้งผู้ขนส่ง">
                                                                            </dx:ASPxCheckBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <dx:ASPxButton ID="btnSaveAndReportT4" runat="server" ClientInstanceName="btnSaveAndReportT4"
                                            Text="บันทึก/พิมพ์ใบเรียกเก็บค่าธรรมเนียมเพิ่มเติม" AutoPostBack="true" CssClass="dxeLineBreakFix"
                                            OnClick="btnSaveAndReportT4_Click">
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="bntExitT3" runat="server" ClientInstanceName="bntExitT3" AutoPostBack="false"
                                            Text="ออกจากหน้านี้" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function(s,e) {  window.location = 'approve_mv.aspx';}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
