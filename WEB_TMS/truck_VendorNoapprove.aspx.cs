﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using DevExpress.Web.ASPxCallbackPanel;
using System.Web.Configuration;
using TMS_BLL.Master;
using DevExpress.XtraEditors;

public partial class truck_VendorNoapprove : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";        

        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            if (string.Equals(Session["CGROUP"].ToString(), "0"))
            {
                Div_Vendor.Visible = false;
                BindGridVehicle(VehicleTruck);
            }
            else
            {
                ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
                cboVendor.DataSource = ViewState["DataVendor"];
                cboVendor.DataBind();
                BindGridVehiclePTT(VehicleTruck);
            }
            
            //CheckBox Vehicle
            this.AssignAuthen();
        }
        else
        {
            if (string.Equals(Session["CGROUP"].ToString(), "0"))
            {
                Div_Vendor.Visible = false;
                BindGridVehicle(VehicleTruck);
            }
            else
            {
                ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
                cboVendor.DataSource = ViewState["DataVendor"];
                cboVendor.DataBind();
                BindGridVehiclePTT(VehicleTruck);
            }
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "SEARCH":
                BindGridSearchVehicle(VehicleTruck);
                break;
            case "SEARCH_CANCELVEHICLE":
                if (string.Equals(Session["CGROUP"].ToString(), "0"))
                {
                    txtsHeadRegisterNo.Text = "";
                    BindGridVehicle(VehicleTruck);
                }
                else
                {
                    cboVendor.Value = "";
                    txtsHeadRegisterNo.Text = "";
                    BindGridVehiclePTT(VehicleTruck);
                }
                break;
        }
    }




    protected void gvwTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            case "PAGERONCLICK":
                if (string.Equals(Session["CGROUP"].ToString(), "0"))
                {

                    BindGridVehicle(VehicleTruck);
                }
                else
                {

                    BindGridVehiclePTT(VehicleTruck);
                }
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "STARTEDIT":
                break;
        }
    }

    protected void gvwTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType)
        {
            case GridViewRowType.Header:
                ASPxButton btnAdd = (ASPxButton)e.Row.FindControl("btnAdd");
                ASPxLabel lblAdd = (ASPxLabel)e.Row.FindControl("lblAdd");
                string cPermission = Session["chkurl"] + "";
                switch (cPermission)
                {
                    case "1":
                        btnAdd.ClientVisible = false;
                        lblAdd.ClientVisible = true;
                        break;
                    case "2":
                        btnAdd.ClientVisible = true;
                        lblAdd.ClientVisible = false;
                        break;
                }

                break;
            //case GridViewRowType.Data:
            //    if (Convert.ToInt32(e.GetValue("LDUPDATE")) == 0)
            //        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
            //    e.Row.BackColor.GetBrightness();
            //    break;
        }
    }
    private void BindGridVehicle(ASPxGridView VehicleTruck)
    {
        string Condition = "";

        Condition += " AND V.SVENDORID = '" + Session["SVDID"].ToString() + "'";
        Condition += " AND TH.CACTIVE IN('N')";
        sdsTruckVehicle.SelectCommand = @"SELECT   TH.STRUCKID,V.SABBREVIATION,
                        CASE CT.CSTANDBY
                            WHEN 'N'
                            THEN 'รถในสัญญา'
                            ELSE 'รถสำรอง'
                        END SCARTYPE,
                        SHEADREGISTERNO,  
                        TH.STRAILERREGISTERNO,
                        TH.SCARTYPEID ,
                        CASE 
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '0' THEN
                          GET_C_CONFIG('00','VICHICLE_TYPE','1')
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '3' THEN
                          GET_C_CONFIG('34','VICHICLE_TYPE','1')
                          ELSE
                          TT.CONFIG_NAME
                          END  AS SCARCATEGORY,TH.NSLOT,
                        TH.NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,
                        C.SCONTRACTNO,ISUSE,
                          CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')                                    
                            END STATUS
                    FROM
                        TTRUCK TH
                        LEFT JOIN C_CONFIG TT
                    ON TH.VIEHICLE_TYPE=TT.CONFIG_VALUE
                    AND (TH.VIEHICLE_TYPE !='99'
                    AND TT.CONFIG_TYPE    ='VICHICLE_TYPE' )   
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                WHERE TH.LOGDATA='1' AND TH.ISUSE='1' AND TH.SCARTYPEID IN ('0','3') AND (VIEHICLE_TYPE IS NULL OR VIEHICLE_TYPE IN ('00','34'))" + Condition + "ORDER BY TH.STRUCKID DESC";
        string sdsID = VehicleTruck.DataSourceID;
        //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
        //Cache.Remove(__sds.CacheKeyDependency);
        //Cache[__sds.CacheKeyDependency] = new object();
        //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
        //__sds.DataBind();
        VehicleTruck.DataBind();
    }
    #region โชวค์ข้อมูลรถสำหรับ PTT
    private void BindGridVehiclePTT(ASPxGridView VehicleTruck)
    {
        string Condition = "";
        Condition += " AND TH.CACTIVE IN('N')";
        sdsTruckVehicle.SelectCommand = @"SELECT   TH.STRUCKID,V.SABBREVIATION,
                        CASE CT.CSTANDBY
                            WHEN 'N'
                            THEN 'รถในสัญญา'
                            ELSE 'รถสำรอง'
                        END SCARTYPE,
                        SHEADREGISTERNO,  
                        TH.STRAILERREGISTERNO,
                        TH.SCARTYPEID ,
                        CASE 
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '0' THEN
                          GET_C_CONFIG('00','VICHICLE_TYPE','1')
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '3' THEN
                          GET_C_CONFIG('34','VICHICLE_TYPE','1')
                          ELSE
                          TT.CONFIG_NAME
                          END  AS SCARCATEGORY,TH.NSLOT,
                        TH.NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,
                        C.SCONTRACTNO,ISUSE,
                          CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')                                    
                            END STATUS
                    FROM
                        TTRUCK TH
                        LEFT JOIN C_CONFIG TT
                    ON TH.VIEHICLE_TYPE=TT.CONFIG_VALUE
                    AND (TH.VIEHICLE_TYPE !='99'
                    AND TT.CONFIG_TYPE    ='VICHICLE_TYPE' )   
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                WHERE TH.LOGDATA='1' AND TH.ISUSE='1' AND TH.SCARTYPEID IN ('0','3') AND (VIEHICLE_TYPE IS NULL OR VIEHICLE_TYPE IN ('00','34'))" + Condition + "ORDER BY TH.STRUCKID DESC";
        string sdsID = VehicleTruck.DataSourceID;
        //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
        //Cache.Remove(__sds.CacheKeyDependency);
        //Cache[__sds.CacheKeyDependency] = new object();
        //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
        //__sds.DataBind();
        VehicleTruck.DataBind();
    }
    #endregion
    #region Search รถที่ถูกระงับใช้งาน
    private void BindGridSearchVehicle(ASPxGridView VehicleTruck)
    {
        string Condition = "";

        if (string.Equals(Session["CGROUP"].ToString(), "0"))
        {
            Condition += " AND V.SVENDORID = '" + Session["SVDID"].ToString() + "'";
        }
        else
        {
            if (cboVendor.Value != null)
            {
                Condition += " AND V.SVENDORID = '" + cboVendor.Value + "'";
            }
        }
        if (!string.IsNullOrEmpty(txtsHeadRegisterNo.Text.Trim().ToString()))
        {
            Condition += " AND TH.SCARTYPEID NOT IN ('4') AND (TH.SHEADREGISTERNO LIKE '%" + txtsHeadRegisterNo.Text.Trim().ToString() + "%' OR TH.STRAILERREGISTERNO LIKE '%" + txtsHeadRegisterNo.Text.Trim().ToString() + "%')";
        }
        sdsTruckVehicle.SelectCommand = @"SELECT   TH.STRUCKID,V.SABBREVIATION,
                        CASE CT.CSTANDBY
                            WHEN 'N'
                            THEN 'รถในสัญญา'
                            ELSE 'รถสำรอง'
                        END SCARTYPE,
                        SHEADREGISTERNO,  
                        TH.STRAILERREGISTERNO,
                        TH.SCARTYPEID ,
                        CASE 
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '0' THEN
                          GET_C_CONFIG('00','VICHICLE_TYPE','1')
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '3' THEN
                          GET_C_CONFIG('34','VICHICLE_TYPE','1')
                          ELSE
                          TT.CONFIG_NAME
                          END  AS SCARCATEGORY,TH.NSLOT,
                        TH.NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,
                        C.SCONTRACTNO,ISUSE,
                          CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')                                    
                            END STATUS
                    FROM
                        TTRUCK TH
                        LEFT JOIN C_CONFIG TT
                    ON TH.VIEHICLE_TYPE=TT.CONFIG_VALUE
                    AND (TH.VIEHICLE_TYPE !='99'
                    AND TT.CONFIG_TYPE    ='VICHICLE_TYPE' )   
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                WHERE TH.LOGDATA='1' AND TH.CACTIVE IN('N') AND TH.SCARTYPEID IN ('0','3') AND (VIEHICLE_TYPE IS NULL OR VIEHICLE_TYPE IN ('00','34'))" + Condition + "ORDER BY TH.STRUCKID DESC";
        string sdsID = VehicleTruck.DataSourceID;
        //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
        //Cache.Remove(__sds.CacheKeyDependency);
        //Cache[__sds.CacheKeyDependency] = new object();
        //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
        //__sds.DataBind();
        VehicleTruck.DataBind();
    }
    #endregion
    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;
                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;
                            break;

                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
}