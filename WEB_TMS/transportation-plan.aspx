﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="transportation-plan.aspx.cs" Inherits="transportation" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function SetVisibleControl(value) {
            if (value == 'หมดอายุ') {
                CPanel.SetClientVisible(true);
            } else {
                CPanel.SetClientVisible(false);
            }
        }

        function SetVisibleControl1(value) {
            if (value == '0' || value == '1') {
                btnsAdd.SetClientVisible(false);
                sgvw.SetClientVisible(false);
            } else {
                btnsAdd.SetClientVisible(true);
                sgvw.SetClientVisible(true);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="30%" align="right">
                            <dx:ASPxTextBox ID="txtCTYPE" runat="server" Width="5px" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" NullText="ทะเบียนรถ,Outbound No.,Ship-to"
                                Width="180px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cmbDate" runat="server" Width="120px">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="วันที่จัดแผน" Value="1" />
                                    <dx:ListEditItem Text="วันที่จัดส่ง" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboTerminal1" runat="server" ClientInstanceName="cboTerminal1"
                                DataSourceID="sqlTerminal1" TextFormatString="{1}" ValueField="STERMINALID" Width="120px">
                                <Columns>
                                    <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" />
                                    <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sqlTerminal1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT t.STERMINALID, ts.STERMINALNAME FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON t.STERMINALID = ts.STERMINALID WHERE T.STERMINALID LIKE 'H%' OR  T.STERMINALID LIKE 'K%' ORDER BY t.STERMINALID">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cmbStatus" runat="server" Width="100px">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="ทั้งหมด" />
                                    <dx:ListEditItem Text="รอการยืนยัน" Value="2" />
                                    <dx:ListEditItem Text="ยืนยันแผนแล้ว" Value="1" />
                                    <dx:ListEditItem Text="ไม่ยืนยันแผน" Value="0" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="8">
                            Last update
                            <dx:ASPxLabel ID="lblLastUpdate" runat="server" Text="" Font-Bold="true" ForeColor="Red">
                            </dx:ASPxLabel>
                            &nbsp;ยอดจัดแผน
                            <dx:ASPxLabel ID="lblTotalPlan" runat="server" Text="0" Font-Bold="true" ForeColor="Red">
                            </dx:ASPxLabel>
                            &nbsp;คัน ยอดเข้ารับสินค้า
                            <dx:ASPxLabel ID="lblTotalInProduct" runat="server" Text="0" Font-Bold="true" ForeColor="Red">
                            </dx:ASPxLabel>
                            &nbsp;คัน รอตรวจสภาพ
                            <dx:ASPxLabel ID="lblWaitCheck" runat="server" Text="0" Font-Bold="true" ForeColor="Red">
                            </dx:ASPxLabel>
                            &nbsp;คัน
                        </td>
                    </tr>
                    <tr>
                        <%--OnAfterPerformCallback="gvw1_AfterPerformCallback"--%>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw3" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw3"
                                DataSourceID="sds3" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px"
                                Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" VisibleIndex="1" Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ประเภทแผน" FieldName="STYPE" VisibleIndex="2"
                                        Width="6%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="NPLANID" Visible="False" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="วัน-ที่จัดแผน" FieldName="SPLANDATE" VisibleIndex="3"
                                        Width="6%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lbldd" runat="server" Text='<%# Eval("SPLANDATE", "{0:dd/MM/yyyy}") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รับสินค้าที่คลัง" FieldName="STERMINALNAME" VisibleIndex="4"
                                        Width="6%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่จัดส่ง" FieldName="DDELIVERY" VisibleIndex="5"
                                        Width="6%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="เที่ยว<br>ที่" FieldName="STIMEWINDOW" VisibleIndex="6"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Drop<br>No." FieldName="NDROP" VisibleIndex="7"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Outbound No." FieldName="SDELIVERYNO" VisibleIndex="8"
                                        Width="7%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Ship-to" FieldName="SSHIPTO" VisibleIndex="9"
                                        Width="7%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" VisibleIndex="10">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" VisibleIndex="10"
                                                Width="7%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" VisibleIndex="11"
                                                Width="7%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" VisibleIndex="12"
                                        Width="7%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="สถานะ" HeaderStyle-HorizontalAlign="Center" VisibleIndex="13">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="แผน" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                                Width="5%">
                                                <DataItemTemplate>
                                                    <dx:ASPxImage runat="server" ID="imgTemplate" ImageUrl='<%# "Images/" + Eval("SIMAGE") %>'>
                                                    </dx:ASPxImage>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="รับสินค้า" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                                Width="5%">
                                                <DataItemTemplate>
                                                    <dx:ASPxImage runat="server" ID="imgTemplate" ImageUrl='<%# "Images/" + Eval("SSHIPMENT") %>'>
                                                    </dx:ASPxImage>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="14" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="False" CausesValidation="False"
                                                Text="RePlan">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('EditClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="PlanListID" FieldName="SPLANLISTID" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="NNO" FieldName="NNO" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="STERMINALID" FieldName="STERMINALID" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="SEMPLOYEEID" FieldName="SEMPLOYEEID" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="SREMARK" FieldName="SREMARK" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="CFIFO" FieldName="CFIFO" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                                <Templates>
                                    <EditForm>
                                        <table border="0" cellpadding="3" cellspacing="2" width="100%">
                                            <tr style="background-color: Blue">
                                                <td colspan="4" align="center">
                                                    <dx:ASPxLabel ID="lblShow" runat="server" Text="แก้ไข / Edit" Font-Bold="true" ForeColor="Yellow"
                                                        Font-Italic="true">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#CCCCCC" width="14%">
                                                    ลักษณะการปรับแผน
                                                </td>
                                                <td colspan="3">
                                                    <dx:ASPxRadioButtonList ID="rblCheck" runat="server" RepeatDirection="Horizontal"
                                                        SkinID="rblStatus">
                                                        <ClientSideEvents ValueChanged="function (s, e) {SetVisibleControl1(s.GetValue()) }" />
                                                        <Items>
                                                            <dx:ListEditItem Text="แก้ไขแผน" Value="0" Selected="true" />
                                                            <dx:ListEditItem Text="ยกเลิกแผน" Value="1" />
                                                            <dx:ListEditItem Text="ยกเลิก/สร้างแผนใหม่" Value="2" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#CCCCCC" width="14%">
                                                    ทะเบียนรถ (หัว-ท้าย)
                                                </td>
                                                <td width="34%">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox ID="cboHeadRegist" runat="server" ClientInstanceName="cboHeadRegist"
                                                                    EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegisTFIFOPLAN_OnItemRequestedByValueSQL"
                                                                    OnItemsRequestedByFilterCondition="cboHeadRegisTFIFOPLAN_OnItemsRequestedByFilterConditionSQL"
                                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="150px">
                                                                    <ClientSideEvents SelectedIndexChanged="function (s, e) {if('' + s.GetSelectedIndex() != '-1'){  txtVendorID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANSPORTID'));cboTrailerRegist.PerformCallback();cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'));txtSumValue.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('NCAPACITY'));}else{s.SetValue('');txtVendorID.SetValue('');cboTrailerRegist.PerformCallback();cboTrailerRegist.SetValue('');txtSumValue.SetValue('');} }" />
                                                                    <Columns>
                                                                        <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                                        <dx:ListBoxColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORNAME" Width="150px" />
                                                                        <dx:ListBoxColumn Caption="ความจุ" FieldName="NCAPACITY" Width="80px" />
                                                                        <dx:ListBoxColumn Caption="รหัสผปก." FieldName="STRANSPORTID" Width="80px" />
                                                                    </Columns>
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <ErrorFrameStyle ForeColor="Red">
                                                                        </ErrorFrameStyle>
                                                                        <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="cboTrailerRegist" runat="server" ClientInstanceName="cboTrailerRegist"
                                                                    EnableCallbackMode="True" OnItemRequestedByValue="cboTrailerRegisTFIFOPLAN_OnItemRequestedByValueSQL"
                                                                    OnItemsRequestedByFilterCondition="cboTrailerRegisTFIFOPLAN_OnItemsRequestedByFilterConditionSQL"
                                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="STRAILERREGISTERNO" Width="150px">
                                                                    <Columns>
                                                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                                        <dx:ListBoxColumn Caption="ความจุ" FieldName="NCAPACITY" Width="80px" />
                                                                    </Columns>
                                                                </dx:ASPxComboBox>
                                                                <dx:ASPxTextBox ID="txtVendorID" ClientInstanceName="txtVendorID" runat="server"
                                                                    Width="170px" ClientVisible="false">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td colspan="2">
                                                                <dx:ASPxPanel ID="ASPxPanel1" runat="server" ClientInstanceName="CPanel" ClientVisible="false">
                                                                    <PanelCollection>
                                                                        <dx:PanelContent>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dx:ASPxImage ID="ximg" runat="server" ImageUrl="Images/05.png">
                                                                                        </dx:ASPxImage>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxLabel ID="lblAlert" runat="server" ForeColor="Red" Text="มาตรวัดน้ำหมดอายุ">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dx:PanelContent>
                                                                    </PanelCollection>
                                                                </dx:ASPxPanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td bgcolor="#CCCCCC" width="16%">
                                                    พนักงานขับรถ
                                                </td>
                                                <td width="36%">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="10" ClientInstanceName="cmbPersonalNo"
                                                                    EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                                                    OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                                                    SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="SEMPLOYEEID" Width="250px">
                                                                    <%--<ClientSideEvents ValueChanged="function (s, e) {lblName.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('FULLNAME'))}" />--%>
                                                                    <Columns>
                                                                        <dx:ListBoxColumn Caption="รหัสบัตรประชาชน" FieldName="SPERSONELNO" />
                                                                        <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                                                    </Columns>
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <ErrorFrameStyle ForeColor="Red">
                                                                        </ErrorFrameStyle>
                                                                        <RequiredField ErrorText="เลือกชื่อพนักงาน" IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>
                                                                <%--<dx:ASPxLabel ID="lblName" runat="server" ClientInstanceName="lblName" Text="">
                                                                                    </dx:ASPxLabel>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#CCCCCC">
                                                    สาเหตุการปรับแผน <span style="color: Red">*</span>
                                                </td>
                                                <td colspan="3">
                                                    <dx:ASPxTextBox ID="txtRemark" runat="server" Width="400px" Text='<%# Eval("SREMARK") %>'>
                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="add">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField ErrorText="กรุณาระบุสาเหตุการปรับแผน" IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <tr>
                                                    <td bgcolor="#CCCCCC">
                                                        ปริมาณความจุของรถ
                                                    </td>
                                                    <td>
                                                       <dx:ASPxTextBox ID="txtSumValue" ClientInstanceName="txtSumValue" runat="server" Width="100px" ClientEnabled="false" BackColor="Gainsboro">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                            </tr>
                                        </table>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                            <tr>
                                                <td width="10%" align="center" bgcolor="#CCCCCC">
                                                    Drop No.
                                                </td>
                                                <td width="20%" align="center" bgcolor="#CCCCCC">
                                                    Outbound No.
                                                </td>
                                                <td width="50%" align="center" bgcolor="#CCCCCC">
                                                    Ship-to
                                                </td>
                                                <td width="10%" align="center" bgcolor="#CCCCCC">
                                                    ปริมาณ
                                                </td>
                                                <td width="10%" rowspan="2" align="center">
                                                    <dx:ASPxButton ID="btnsAdd" runat="server" ClientInstanceName="btnsAdd" Text="เพิ่ม"
                                                        SkinID="_add">
                                                        <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('AddDataToList'); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <dx:ASPxTextBox ID="txtDrop" runat="server" Width="100%" Text='<%# Eval("NDROP") %>'>
                                                        <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                            SetFocusOnError="True" Display="Dynamic">
                                                            <ErrorFrameStyle ForeColor="Red">
                                                            </ErrorFrameStyle>
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุเลข Drop" />
                                                            <RegularExpression ErrorText="กรุณากรอกตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_Number %>" />
                                                        </ValidationSettings>--%>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td align="left">
                                                    <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" ClientInstanceName="cboDelivery"
                                                        EnableCallbackMode="True" MaxLength="10" OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL"
                                                        OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="DELIVERY_NO" Width="180px">
                                                        <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtShipto.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHIP_TO')) ; txtShiptoName.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCUSTOMER')) ;txtValue.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('NVALUE')) ;}else{s.SetValue('');txtShipto.SetValue('') ; txtShiptoName.SetValue('') ;txtValue.SetValue('') ;};}" />
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="Outbound No." FieldName="DELIVERY_NO" Width="100px" />
                                                            <dx:ListBoxColumn Caption="Ship-to" FieldName="SHIP_TO" Width="80px" />
                                                            <dx:ListBoxColumn Caption="ชื่อลูกค้า" FieldName="SCUSTOMER" Width="150px" />
                                                            <dx:ListBoxColumn Caption="ปริมาณ" FieldName="NVALUE" Width="100px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td align="left">
                                                    <div style="float: left">
                                                        <dx:ASPxTextBox ID="txtShipto" runat="server" ClientInstanceName="txtShipto" MaxLength="10"
                                                            Width="150px" ReadOnly="true" Text='<%# Eval("SSHIPTO") %>' Border-BorderColor="#cccccc">
                                                            <%--<ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                SetFocusOnError="True" Display="Dynamic">
                                                                <ErrorFrameStyle ForeColor="Red">
                                                                </ErrorFrameStyle>
                                                                <RequiredField IsRequired="True" ErrorText="กรุณาระบุ Shipto" />
                                                            </ValidationSettings>--%>
                                                        </dx:ASPxTextBox>
                                                    </div>
                                                    <div style="float: left">
                                                        -</div>
                                                    <div style="float: left">
                                                        <dx:ASPxTextBox ID="txtShiptoName" runat="server" ClientInstanceName="txtShiptoName"
                                                            MaxLength="10" Width="280px" ReadOnly="true" Border-BorderColor="#cccccc">
                                                        </dx:ASPxTextBox>
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <dx:ASPxTextBox ID="txtValue" runat="server" Width="100%" ClientInstanceName="txtValue"
                                                        Text='<%# Eval("NVALUE") %>' ClientEnabled="false" Border-BorderColor="#cccccc">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                                        Settings-ShowColumnHeaders="false" Style="margin-top: 0px" ClientInstanceName="sgvw"
                                                        Width="100%" KeyFieldName="dtDrop" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                                        SkinID="_gvw">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Drop" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                                                Width="10%" FieldName="dtDrop">
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="OutBound No." VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                                                Width="20%" FieldName="dtDeliveryNo">
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Ship-to" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                                                Width="18%" FieldName="dtShipto">
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Ship-toName" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                                Width="32%" FieldName="dtShiptoName">
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ปริมาณ" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                                Width="10%" FieldName="dtValue">
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataColumn Width="10%" CellStyle-Cursor="hand" VisibleIndex="10">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxButton ID="imbedit" runat="server" SkinID="_delete" CausesValidation="False"
                                                                        AutoPostBack="False">
                                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('DelClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxButton>
                                                                </DataItemTemplate>
                                                                <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="right" width="50%">
                                                                <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" Text="ยืนยัน">
                                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td width="50%">
                                                                <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" Text="ปิด">
                                                                    <%--<ClientSideEvents Click="function (s,e){gvw3.CancelEdit() ;}" />--%>
                                                                    <ClientSideEvents Click="function (s,e){xcpn.PerformCallback('Cancel');}" />
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </EditForm>
                                </Templates>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds3" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser"></asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsTerminal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" />
                            <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" />
                            <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            สถานะ
                            <img src="Images/action_check.png" width="20" height="20" />
                            = ยืนยันแผนแล้ว
                            <img src="Images/05.png" width="15" height="15" />
                            = รอการยืนยัน
                            <img src="Images/action_delete.png" width="20" height="20" />
                            = ไม่ยืนยันแผน
                            <img src="Images/load1.png" width="20" height="20" />
                            = เข้ารับสินค้าแล้ว
                            <img src="Images/unload1.png" width="20" height="20" />
                            = ยังไม่ได้เข้ารับสินค้า
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
