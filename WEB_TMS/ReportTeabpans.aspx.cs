﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;
using System.Drawing;

public partial class ReportTeabpans : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static int nNextYeatCheckWater = 3;
    private static DataTable dtMainData = new DataTable();
    private static string sTruckID_CheckWater = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            ListData();

        }
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "ใบรับรอง")
        {
            ASPxTextBox txtChecking = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtChecking") as ASPxTextBox;
            txtChecking.ClientInstanceName = txtChecking.ID + "_" + e.VisibleIndex;
            ASPxTextBox txtRequestID = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtRequestID") as ASPxTextBox;
            txtRequestID.ClientInstanceName = txtRequestID.ID + "_" + e.VisibleIndex;
        }


    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": ListData();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void ListData()
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Condition = " AND REQ.VEH_NO||REQ.TU_NO||VEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%'";
        }

        if (edtStart.Value != null && edtEnd.Value != null)
        {
            lblsTail.Text = edtStart.Text + " - " + edtEnd.Text;

            DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
            DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());

            Condition += " AND (TRUNC(TIC.EXAMDATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY')) ";
        }
        else
        {
            lblsTail.Text = " - ";
        }



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
, TO_CHAR(add_months(TIC.EXAMDATE,6516),'dd/MM/yyyy') as APPOINTMENT_DATE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
,NVL(REQ.CCHECKING_WATER,'x') as CCHECKING_WATER
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN 
(
    SELECT REQUEST_ID,MAX(EXAMDATE) as  EXAMDATE  FROM  TBL_TIME_INNER_CHECKINGS WHERE ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID
)
TIC ON TIC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;

        }
        gvw.DataBind();
    }

    //    private void ListReport(string Type)
    //    {

    //        string Condition = "";
    //        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
    //        //{
    //        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
    //        //}
    //        //else
    //        //{

    //        //}



    //        string QUERY = @"SELECT ROWNUM||'.' as NO,REQ.REQUEST_ID
    //,REQ.VEH_NO
    //,REQ.TU_NO
    //,TCK.SCAR_NUM,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
    //, TO_CHAR(add_months(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),6516),'dd/MM/yyyy') as APPOINTMENT_DATE
    //,STU.STATUSREQ_NAME
    //FROM TBL_REQUEST REQ
    //LEFT JOIN TTRUCK TCK
    //ON TCK.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
    //LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
    //LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
    //LEFT JOIN TBL_STATUSREQ STU ON STU.STATUSREQ_ID = REQ.STATUS_FLAG
    //" + Condition + "";

    //        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
    //        //if (dt.Rows.Count > 0)
    //        //{
    //        //    gvw.DataSource = dt;
    //        //    gvw.DataBind();
    //        //}


    //        ReportOperationMonth report = new ReportOperationMonth();

    //        #region function report

    //        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
    //        //{
    //        //    string Date = "1/" + cboMonth.Value + "/" + cboYear.Text + "";
    //        //    DateTime DMONTH = DateTime.Parse(Date);
    //        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = DMONTH.ToString("MMMM", new CultureInfo("th-TH")) + " " + cboYear.Text;
    //        //}
    //        //else
    //        //{
    //        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = " - ";
    //        //}

    //        report.Name = "รายงานผลการดำเนินงานประจำเดือน";
    //        report.DataSource = dt;
    //        string fileName = "รายงานผลการดำเนินงานประจำเดือน_" + DateTime.Now.ToString("MMddyyyyHHmmss");

    //        MemoryStream stream = new MemoryStream();


    //        string sType = "";
    //        if (Type == "P")
    //        {
    //            report.ExportToPdf(stream);
    //            Response.ContentType = "application/pdf";
    //            sType = ".pdf";
    //        }
    //        else
    //        {
    //            report.ExportToXls(stream);
    //            Response.ContentType = "application/xls";
    //            sType = ".xls";
    //        }


    //        Response.AddHeader("Accept-Header", stream.Length.ToString());
    //        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
    //        Response.AddHeader("Content-Length", stream.Length.ToString());
    //        Response.ContentEncoding = System.Text.Encoding.ASCII;
    //        Response.BinaryWrite(stream.ToArray());
    //        Response.End();
    //        #endregion
    //    }

    //    protected void btnPDF_Click(object sender, EventArgs e)
    //    {
    //        ListReport("P");
    //    }

    //    protected void btnExcel_Click(object sender, EventArgs e)
    //    {
    //        ListReport("E");
    //    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string REQID = txtReqID.Text;
        if (!string.IsNullOrEmpty(txtReqID.Text))
        {
            ListDataToPage(REQID);


            ListReportTeabpan(REQID);

        }

    }

    private void ListDataToPage(string sREQID)
    {
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
                        TRK.DWATEREXPIRE,TRK.SCARTYPEID,TRQ.RESULT_CHECKING_DATE,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,TRQ.CHECKWATER_NVERSION,
                        TVD.SVENDORID,TVD.SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME,TRK.NWHEELS,TRQ.TOTLE_SLOT,TRK.DPREV_SERV,TRK.DLAST_SERV,TRKN.SCAR_NUM,INC.DATE_CREATED
                        FROM TBL_REQUEST TRQ 
                        LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = TRQ.STRUCKID
                        LEFT JOIN TTRUCK TRKN ON TRKN.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        LEFT JOIN (SELECT REQ_ID, MAX(NVL( DATE_UPDATED, DATE_CREATED)) as DATE_CREATED FROM TBL_INNER_CHECKINGS GROUP BY REQ_ID)INC
                        ON TRQ.REQUEST_ID = INC.REQ_ID
                        WHERE TRQ.REQUEST_ID = '{0}'";

        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(conn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));

        string sTruckID = "";
        switch (dtMainData.Rows[0]["SCARTYPEID"] + "")
        {
            case "0": sTruckID = dtMainData.Rows[0]["VEH_ID"] + ""; break; // 10 ล้อ
            case "3": sTruckID = dtMainData.Rows[0]["TU_ID"] + ""; break; // หัวลาก
        }
        sTruckID_CheckWater = sTruckID;
    }



    private void ListReportTeabpan(string sReqID)
    {
        #region data table //

        //        string sql1 = @"SELECT TCC.REQUEST_ID,TCC.COMPART_NO, TIC.PAN_LEVEL,NVL(TRC.NCAPACITY,REQ.CAPACITY) as NCAPACITY
        //                                 FROM TBL_CHECKING_COMPART TCC
        //                                 INNER JOIN TBL_INNER_CHECKINGS TIC ON TCC.REQUEST_ID = TIC.REQ_ID AND TCC.COMPART_NO = TIC.COMPART_NO
        //                                 LEFT JOIN TTRUCK_COMPART TRC ON  TCC.STRUCKID  = TRC.STRUCKID AND  TIC.COMPART_NO = TRC.NCOMPARTNO AND TIC.PAN_LEVEL = TRC.NPANLEVEL
        //                                 LEFT JOIN TBL_REQSLOT REQ ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO   AND TIC.PAN_LEVEL = REQ.LEVEL_NO
        //                                 WHERE REQUEST_ID = '{0}'
        //                                 ORDER BY TCC.COMPART_NO,TIC.PAN_LEVEL ASC";
        string sql1 = "SELECT REQUEST_ID,SLOT_NO as COMPART_NO,LEVEL_NO as PAN_LEVEL,CAPACITY as NCAPACITY FROM TBL_REQSLOT  WHERE REQUEST_ID = '{0}' ORDER BY SLOT_NO,LEVEL_NO ASC";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, string.Format(sql1, CommonFunction.ReplaceInjection(sReqID)));



        //var query = dt.AsEnumerable().Select(s => new { COMPART_NO = s.Field<decimal>("COMPART_NO"), PAN_LEVEL = s.Field<decimal>("PAN_LEVEL"), NCAPACITY = s.Field<decimal>("NCAPACITY") }).ToList();
        //decimal nSum_Capacity = 0;
        //if (query.Count > 0)
        //{
        //    var query2 = query.GroupBy(g => new { g.COMPART_NO }).Select(s => new { s.Key.COMPART_NO, NCAPACITY = s.Max(x => x.NCAPACITY) }).ToList();
        //    nSum_Capacity = query2.Sum(s => s.NCAPACITY);
        //}

        //if (dt.Rows.Count > 0)
        //{

        //}
        //else
        //{
        //    DataRow dr = null;
        //    for (int i = dt.Rows.Count; i <= 18; i++)
        //    {
        //        dr = dt.NewRow();
        //        dr[0] = ""; // or you could generate some random string.
        //        dt.Rows.Add(dr);
        //    }
        //}
        #endregion

        ReportTeabpan report = new ReportTeabpan();

        // parameter
        //SENGINE หมายเลขเคื่อง


        // set control
        ((XRLabel)report.FindControl("xrLabel3", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrLabel5", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrLabel7", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));


        //string[] arrNo1 = sREQUESTID.Split('-');
        //((XRLabel)report.FindControl("xrlblNo1", true)).Text = arrNo1[arrNo1.Length - 1];
        //((XRLabel)report.FindControl("xrlblNo2", true)).Text = DateTime.Now.ToString("yy", new CultureInfo("th-TH"));

        //
        string sql2 = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TRQ.VEH_ID,TRQ.TU_ID,
TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS
 FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
 LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
 WHERE TRQ.REQUEST_ID = '{0}'";

        DataTable dtPara1 = new DataTable();
        dtPara1 = CommonFunction.Get_Data(conn, string.Format(sql2, sReqID));

        //sTruckID_CheckWater เก็บ id รถที่ใช้วัดน้ำ
        string sql3 = @"SELECT TRK.*,TRKT.SCARTYPENAME FROM TTRUCK  TRK LEFT JOIN TTRUCKTYPE TRKT ON  TRK.CARCATE_ID = TRKT.SCARTYPEID WHERE  TRK.STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";
        DataTable dtTruck = new DataTable();
        dtTruck = CommonFunction.Get_Data(conn, sql3);

        //เวลา
        //string sql4 = @"SELECT * FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NID";
        //DataTable dtTime = new DataTable();
        //dtTime = CommonFunction.Get_Data(strConn, sql4);
        //decimal nTemp = 0;

        //string vStartH = "", vStartM = "", vEndH = "", vEndM = "";

        if (dtMainData.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel15", true)).Text = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_NO"] + "" : dtMainData.Rows[0]["TU_NO"] + "");
            ((XRLabel)report.FindControl("xrLabel17", true)).Text = dtMainData.Rows[0]["SCAR_NUM"] + "";
            ((XRLabel)report.FindControl("xrLabel19", true)).Text = dtMainData.Rows[0]["NWHEELS"] + "";
            ((XRLabel)report.FindControl("xrLabel40", true)).Text = dtMainData.Rows[0]["DLAST_SERV"] + "";
            ((XRLabel)report.FindControl("xrLabel37", true)).Text = dtMainData.Rows[0]["DWATEREXPIRE"] + "";

            //if (txtComment.Text.Length < 257)
            //{
            //    int Textnum = 257 - txtComment.Text.Length;
            //    txtComment.Text = txtComment.Text.PadRight(Textnum, ' ');
            //}
            //else
            //{

            //}



            ((XRLabel)report.FindControl("xrLabel72", true)).Text = GetComment(sReqID);


            switch (dtMainData.Rows[0]["SCARTYPEID"] + "")
            {
                case "1": //((XRCheckBox)report.FindControl("xrCheckBox3", true)).Checked = true;
                    break;
                case "2": //((XRCheckBox)report.FindControl("xrCheckBox4", true)).Checked = true;
                    break;
                case "3": ((XRCheckBox)report.FindControl("xrCheckBox5", true)).Checked = true;
                    ((XRLabel)report.FindControl("xrLabel25", true)).Text = dtMainData.Rows[0]["VEH_NO"] + "";
                    break;
            }

            switch (dtMainData.Rows[0]["REQTYPE_ID"] + "")
            {
                case "01": ((XRCheckBox)report.FindControl("xrCheckBox2", true)).Checked = true;
                    ((XRCheckBox)report.FindControl("xrCheckBox1", true)).Checked = false;
                    break;
                case "02": ((XRCheckBox)report.FindControl("xrCheckBox1", true)).Checked = true;
                    ((XRCheckBox)report.FindControl("xrCheckBox2", true)).Checked = false;
                    break;

            }

            ((XRLabel)report.FindControl("xrLabel29", true)).Text = !string.IsNullOrEmpty(dtMainData.Rows[0]["TOTLE_CAP"] + "") ? decimal.Parse(dtMainData.Rows[0]["TOTLE_CAP"] + "").ToString(SystemFunction.CheckFormatNuberic(2)) : "";
            ((XRLabel)report.FindControl("xrLabel32", true)).Text = dtMainData.Rows[0]["TOTLE_SLOT"] + "";

            //จำนวนช่อง
            // string SLOT = "SELECT STRUCKID, NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID = '" + CommonFunction.ReplaceInjection((dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_ID"] + "" : dtMainData.Rows[0]["TU_ID"] + "")) + "' ORDER BY NCOMPARTNO ASC ,NPANLEVEL ASC";
            string SLOT = @"SELECT STRUCKID, MAX(NCAPACITY) as NCAPACITY,NCOMPARTNO FROM TTRUCK_COMPART 
WHERE STRUCKID = '" + CommonFunction.ReplaceInjection((dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_ID"] + "" : dtMainData.Rows[0]["TU_ID"] + "")) + @"'
GROUP BY STRUCKID,NCOMPARTNO
ORDER BY NCOMPARTNO ASC";
            DataTable dtslot = CommonFunction.Get_Data(conn, SLOT);
            string SCAPACITYALL = "";
            for (int i = 0; i < dtslot.Rows.Count; i++)
            {
                SCAPACITYALL += "," + dtslot.Rows[i]["NCAPACITY"] + "";
            }

            ((XRLabel)report.FindControl("xrLabel39", true)).Text = !string.IsNullOrEmpty(SCAPACITYALL) ? SCAPACITYALL.Remove(0, 1) : "";

        }



        //ลมยาง
        //        string Wheal = @"SELECT REQUEST_ID,ITEM2_VAL,CHECKLIST_ID FROM TBL_SCRUTINEERINGLIST 
        //WHERE 1=1 AND  CHECKLIST_ID IN ('13','1414','16') AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";

        //        DataTable dtNitroWheal = CommonFunction.Get_Data(strConn, Wheal);

        //        if (dtNitroWheal.Rows.Count > 0)
        //        {
        //            switch (dtMainData.Rows[0]["SCARTYPEID"] + "")
        //            {
        //                case "0":
        //                    ((XRLabel)report.FindControl("xrLabel37", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '13'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    ((XRLabel)report.FindControl("xrLabel41", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '1414'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    break;
        //                case "3":
        //                    ((XRLabel)report.FindControl("xrLabel37", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '13'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    ((XRLabel)report.FindControl("xrLabel41", true)).Text = dtNitroWheal.Select("CHECKLIST_ID = '16'").FirstOrDefault().Field<string>("ITEM2_VAL") + "";
        //                    break;
        //            }
        //        }


        if (dtPara1.Rows.Count == 1 && dtTruck.Rows.Count == 1)
        {
            DataRow dr1 = null, dr2 = null, dr3 = null;

            dr1 = dtPara1.Rows[0];
            dr2 = dtTruck.Rows[0];
            dr3 = dtMainData.Rows[0];

            ((XRLabel)report.FindControl("xrLabel13", true)).Text = dr1["SABBREVIATION"] + "";
            ((XRLabel)report.FindControl("xrLabel27", true)).Text = dr2["SBRAND"] + "" != "" ? dr2["SBRAND"] + "" : "-";
            //report.Parameters["sTruckID"].Value = sTruckID_CheckWater;
            //report.Parameters["sSerialNumber"].Value = dr2["SENGINE"] + "" != "" ? dr2["SENGINE"] + "" : "-";
            //report.Parameters["sTU_CHASSIS"].Value = dr1["TU_CHASSIS"] + "" != "" ? dr1["TU_CHASSIS"] + "" : "-";
            //report.Parameters["sCarType"].Value = dr2["SCARTYPENAME"] + "";
            //report.Parameters["sVEH_NO"].Value = dr1["VEH_NO"] + "" != "" ? dr1["VEH_NO"] + "" : "-";

            //report.Parameters["sVEH_CHASSIS"].Value = dr1["VEH_CHASSIS"] + "" != "" ? dr1["VEH_CHASSIS"] + "" : "-";
            //report.Parameters["nNumSlot"].Value = nSlotAll + "";
            //nTemp = decimal.TryParse(dr2["NTOTALCAPACITY"] + "", out nTemp) ? nTemp : 0;
            //report.Parameters["nSum_Capacity"].Value = dr2["NTOTALCAPACITY"] + "" != "" ? (nTemp > 0 ? nTemp.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "-";
            //report.Parameters["nWeight_Sum"].Value = dr2["NWEIGHT"] + "" != "" ? dr2["NWEIGHT"] + "" : "-";
            //report.Parameters["sNB"].Value = dr2["NLOAD_WEIGHT"] + "" != "" ? dr2["NLOAD_WEIGHT"] + "" : "-";
            //report.Parameters["dLastCheckWater"].Value = dr2["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DLAST_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";   // dr1["TU_NO"] + "";
            //report.Parameters["sHeight_H2T"].Value = dr2["NTANK_HIGH_HEAD"] + "";
            //report.Parameters["sHeight_EndT"].Value = dr2["NTANK_HIGH_TAIL"] + "";
            //report.Parameters["sTypeMaterailTank"].Value = dr2["STANK_MATERAIL"] + "" != "" ? dr2["STANK_MATERAIL"] + "" : "-";
            //report.Parameters["sAddress"].Value = dr1["SNO"] + "  " + dr1["SDISTRICT"] + "  " + dr1["SREGION"] + "  จังหวัด " + dr1["SPROVINCE"] + "  " + dr1["SPROVINCECODE"] + "";
            //report.Parameters["dService"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";


            //string sShow = "";
            //decimal nTempH = 0, nTempM = 0;
            //foreach (DataRow row in dtTime.Rows)
            //{
            //    nTempH = 0; nTempM = 0;
            //    nTempH = decimal.TryParse(row["NSTART_HOUR"] + "", out nTempH) ? nTempH : 0;
            //    nTempM = decimal.TryParse(row["NSTART_MINUTE"] + "", out nTempM) ? nTempM : 0;
            //    if (nTempH < 10)
            //    {
            //        sShow = "0" + nTempH + ":";
            //    }
            //    else
            //    {
            //        sShow = nTempH + ":";
            //    }

            //    if (nTempM < 10)
            //    {
            //        sShow = sShow + "0" + nTempM;
            //    }
            //    else
            //    {
            //        sShow = sShow + nTempM;
            //    }


            //    switch (row["NID"] + "")
            //    {
            //        case "1": //เวลานำรถเข้าตรวจ
            //            report.Parameters["sTime_dService"].Value = sShow;
            //            break;
            //        case "2"://เวลานำรถเข้าตรวจสภาพภายนอก/ใน
            //            break;
            //        case "3": //เวลาเริ่มต้นลงน้ำ
            //            report.Parameters["sTime_DownWater"].Value = sShow;
            //            vStartH = row["NSTART_HOUR"] + "";
            //            vStartM = row["NSTART_MINUTE"] + "";
            //            break;
            //        case "4": //หยุดน้ำ/วัดแป้น
            //            report.Parameters["sTime_StopCheckWater"].Value = sShow;
            //            break;
            //        case "5": //สูบน้ำคืนกลับ
            //            report.Parameters["sTime_ReturnWater"].Value = sShow;
            //            vEndH = row["NSTART_HOUR"] + "";
            //            vEndM = row["NSTART_MINUTE"] + "";
            //            break;
            //    }
            //}

            //string st1 = vStartH + ":" + vStartM;
            //string st2 = vEndH + ":" + vEndM;
            //int hours = 0, minutes = 0;

            //if (!string.IsNullOrEmpty(vStartH) && !string.IsNullOrEmpty(vStartM) && !string.IsNullOrEmpty(vEndH) && !string.IsNullOrEmpty(vEndM))
            //{
            //    TimeSpan timeStart = TimeSpan.Parse(st1);
            //    TimeSpan timeEnd = TimeSpan.Parse(st2);

            //    TimeSpan difference = timeEnd - timeStart;

            //    hours = difference.Hours;
            //    minutes = difference.Minutes;
            //}

            //report.Parameters["nSumTimeChecking"].Value = hours + "." + minutes;

            //string sDateNextCheckWater = "";
            //DateTime dNextCheckWater;
            //if (!string.IsNullOrEmpty(dr2["DNEXT_SERV"] + ""))
            //{
            //    dNextCheckWater = Convert.ToDateTime(dr2["DNEXT_SERV"] + "", new CultureInfo("th-TH"));
            //    if (dNextCheckWater.Year == DateTime.Now.Year) // ยังไม่ได้อัพเดทข้อมูลวัดน้ำใน TTRUCK
            //    {
            //        sDateNextCheckWater = dNextCheckWater.AddYears(nNextYeatCheckWater).ToString("dd MMM yyyy");
            //    }
            //    else
            //    {
            //        sDateNextCheckWater = dNextCheckWater.ToString("dd MMM yyyy");
            //    }
            //}
            //else
            //{
            //    sDateNextCheckWater = DateTime.Now.ToString("dd MMM yyyy");
            //}

            //report.Parameters["dNextCheckWater"].Value = sDateNextCheckWater;
            //report.Parameters["dCompletCheckWater"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            //report.Parameters["nSumCapacity"].Value = nSum_Capacity.ToString(SystemFunction.CheckFormatNuberic(0));

        }


        string sUSER = @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME,SUID.SPOSITION,TIC.EXAMDATE FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'  AND ISACTIVE_FLAG = 'Y'";
        DataTable dtUSER = CommonFunction.Get_Data(conn, sUSER);
        if (dtUSER.Rows.Count > 0)
        {
            DateTime DExam = !string.IsNullOrEmpty(dtUSER.Rows[0]["EXAMDATE"] + "") ? DateTime.Parse(dtUSER.Rows[0]["EXAMDATE"] + "") : DateTime.Now;

            ((XRLabel)report.FindControl("xrLabel52", true)).Text = dtUSER.Rows[0]["SNAME"] + "";
            if (!string.IsNullOrEmpty(dtUSER.Rows[0]["EXAMDATE"] + ""))
            {
                // ((XRLabel)report.FindControl("xrLabel64", true)).Text = DExam.Day + " " + DExam.ToString("MMM", new CultureInfo("th-TH")) + " " + DExam.ToString("yyyy", new CultureInfo("th-TH"));
                ((XRLabel)report.FindControl("xrLabel64", true)).Text = DExam.Day + "";
                ((XRLabel)report.FindControl("xrLabel69", true)).Text = DExam.ToString("MMM", new CultureInfo("th-TH"));
                ((XRLabel)report.FindControl("xrLabel71", true)).Text = DExam.ToString("yyyy", new CultureInfo("th-TH"));

                ((XRLabel)report.FindControl("xrLabel50", true)).Text = DExam.Day + "";
                ((XRLabel)report.FindControl("xrLabel74", true)).Text = DExam.ToString("MMM", new CultureInfo("th-TH"));
                ((XRLabel)report.FindControl("xrLabel75", true)).Text = DExam.ToString("yyyy", new CultureInfo("th-TH"));
            }
            else
            {
                //((XRLabel)report.FindControl("xrLabel22", true)).Text = " ";
            }
            // ((XRLabel)report.FindControl("xrblbPositionControl", true)).Text = dtUSER.Rows[0]["SPOSITION"] + "";
        }
        var s_basePath = HttpContext.Current.Server.MapPath("./") + "Images/pttlogoreport.jpg".Replace("/", "\\");

        if (File.Exists(s_basePath))
        {
            Bitmap bimage = new Bitmap(s_basePath);
            ((XRPictureBox)report.FindControl("xrPictureBox1", false)).Image = bimage;
            ((XRPictureBox)report.FindControl("xrPictureBox1", true)).Visible = true;

        }

        //((XRLabel)report.FindControl("xrlblsDay2", true)).Text = DateTime.Now.Day + "";
        //((XRLabel)report.FindControl("xrlblsMonth2", true)).Text = DateTime.Now.ToString("MMM", new CultureInfo("th-TH"));
        //((XRLabel)report.FindControl("xrlblsYear2", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
        //((XRLabel)report.FindControl("xrlblComment", true)).Text = GetComment(sReqID);


        report.Name = "รายงานวัดน้ำ";
        report.DataSource = dt;
        string fileName = "รายงานวัดน้ำ_FM-มว.-001_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private string GetComment(string sReqID)
    {
        string sResult = "";
        //คอมเม้น/บันทึก
        string sql5 = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dtComment = new DataTable();
        dtComment = CommonFunction.Get_Data(conn, sql5);

        DataRow dr4 = dtComment.Rows.Count > 0 ? dtComment.Rows[0] : null;

        sResult = dr4 != null ? dr4["SCOMMENT"] + "" : "";

        return sResult;
    }
}