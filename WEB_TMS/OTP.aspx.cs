﻿using System;
using System.Web.UI;
using TMS_BLL.Transaction.Complain;


public partial class OTP : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        this.AssignAuthen();
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdGenerate.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdSaveOTP.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string Generate()
    {
        try
        {
            char[] charArr = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            string strRandom = string.Empty;
            Random rnd = new Random();
            int MaxDigit = 6;
            int pos;

            for (int i = 0; i < MaxDigit; i++)
            {
                pos = rnd.Next(1, charArr.Length);
                if (!strRandom.Contains(charArr.GetValue(pos).ToString()))
                    strRandom += charArr.GetValue(pos);
                else
                    i--;
            }

            return strRandom;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdGenerate_Click(object sender, EventArgs e)
    {
        try
        {
            txtOTP.Text = this.Generate();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSaveOTP_Click(object sender, EventArgs e)
    {
        try
        {
            ComplainBLL.Instance.OTPSaveDAL(int.Parse(radCodeType.SelectedValue), txtOTP.Text.Trim(), int.Parse(Session["UserID"].ToString()));
            
            alertSuccess("บันทึกข้อมูลเรียบร้อย OTP : " + txtOTP.Text.Trim());
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
        finally
        {
            txtOTP.Text = string.Empty;
        }
    }
}