﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Evaluation_Kit.aspx.cs" Inherits="KPI_Evaluation_Kit" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="ชุดประเมิน KPI"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <br />

                                    <asp:GridView ID="dgvKPI" runat="server" Width="100%"
                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" PageSize="10"
                                        AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="TOPIC_HEADER_ID" AllowPaging="true" OnPageIndexChanging="dgvKPI_PageIndexChanging"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnRowUpdating="dgvKPI_RowUpdating" >
                                        <Columns>
                                            <%--<asp:TemplateField HeaderText = "ที่" ItemStyle-Width="80">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:BoundField DataField="TOPIC_HEADER_ID" Visible="false">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="HEADER_NAME" HeaderText="ชื่อแบบฟอร์ม">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="HEADER_DETAIL" HeaderText="รายละเอียดการประเมิน">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FULLNAME" HeaderText="ปรับปรุงล่าสุดโดย">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UPDATE_DATETIME" HeaderText="ปรัปรุงล่าสุดเวลา">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IS_ACTIVE" HeaderText="สถานะ">
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="การกระทำ">
                                                <ItemTemplate>
                                                    <asp:Button ID="cmdEdit" runat="server" Text="แก้ไข" CommandName="update" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle ForeColor="White"></EmptyDataRowStyle>
                                        <HeaderStyle CssClass="GridColorHeader"></HeaderStyle>
                                        <PagerStyle CssClass="pagination-ys" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdFormula" runat="server" CssClass="btn btn-primary" Text="สูตรการคำนวณ" Width="120px" UseSubmitBehavior="false" OnClick="cmdFormula_Click" />
                            <asp:Button ID="cmdAdd" runat="server" Text="เพิ่มชุดประเมิน KPI" Width="135px" UseSubmitBehavior="false" CssClass="btn btn-success" OnClick="cmdAdd_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>