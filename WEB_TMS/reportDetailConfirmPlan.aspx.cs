﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Globalization;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;

public partial class reportDetailConfirmPlan : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(STCrypt.encryptMD5("p@ssw0rd"));
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ExportFile("pdf");
    }
    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        ExportFile("xls");

    }
    public void ExportFile(string fileType)
    {
        int _COLUMN0 = 0;
        _COLUMN0 = adeEnd.Date.DayOfYear - adeStart.Date.DayOfYear + 1;
        string _comm = "", cond = "";
        string ODP_CON = "";
        string ODP_CON_TM = "";
        if (!string.IsNullOrEmpty(cbxTerminal.Value + ""))
        {
            _comm += " AND  NVL(sTerminalName,'')||' '|| NVL(TB2.sTerminalID,'') = '" + cbxTerminal.Text + "'";
            cond = "  AND NVL(TB2.sTerminalID,'') = '" + cbxTerminal.Value + "' ";
            ODP_CON_TM += " AND  NVL(TB4.sTerminalName,'')||' '|| NVL(ODP.STERMINALID,'') = '" + cbxTerminal.Text.Trim() + "'";

        }
        if (!string.IsNullOrEmpty(cbxVendor.Value + ""))
        {
            _comm += " AND TB3.SVENDORID = '" + cbxVendor.Value + "'";
            ODP_CON += " AND  SABBREVIATION = '" + cbxVendor.Text.Trim() + "'";
        }
        if (!string.IsNullOrEmpty(cboSCONTRACTNO.Value + ""))
        {
            _comm += " AND TB2.SCONTRACTNO = '" + cboSCONTRACTNO.Text + "'";
            ODP_CON += " AND  SCONTRACTNO = '" + cboSCONTRACTNO.Text.Trim() + "'";
        }
        #region comment
        //        string sqlquery = @"SELECT COLUMN0,COLUMN1,COLUMN2,SUM(COLUMN3) COLUMN3 ,SUM(COLUMN4) COLUMN4,SUM(COLUMN5) COLUMN5,SUM(COLUMN6) COLUMN6 
        //                ,SUM(COLUMN7) COLUMN7,SUM(COLUMN8) COLUMN8,SUM(COLUMN9) COLUMN9 ,0 SUMCOLUMN3 
        //                ,'" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("th-th")) + @"' dStart
        //                ,'" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("th-th")) + @"' dEnd
        //                  FROM (SELECT sTerminalName COLUMN0,SABBREVIATION COLUMN1 ,TB2.SCONTRACTNO COLUMN2
        //                ,COUNT(TB1.NPLANID) COLUMN3 ,SUM(COLUMN4) COLUMN4 ,SUM(COLUMN5) COLUMN5 ,SUM(COLUMN6) COLUMN6 
        //                ,SUM(COLUMN7) COLUMN7,SUM(COLUMN8) COLUMN8 ,SUM(COLUMN9) COLUMN9
        //                FROM (SELECT TPLANSCHEDULELIST.NPLANID,SSHIPTO
        //                ,CASE WHEN NVL(CCONFIRM,2) = 2 THEN 1 ELSE 0 END COLUMN4 
        //                ,CASE WHEN NVL(CCONFIRM,2) = 0 THEN 1 ELSE 0 END COLUMN5 
        //                ,CASE WHEN NVL(CCONFIRM,2) = 1 THEN 1 ELSE 0 END COLUMN6 
        //                FROM TPLANSCHEDULELIST INNER JOIN TPLANSCHEDULE ON TPLANSCHEDULELIST.NPLANID = TPLANSCHEDULE.NPLANID 
        //                GROUP BY TPLANSCHEDULELIST.NPLANID,CCONFIRM,CCOMMIT,SSHIPTO) TB1 
        //                LEFT JOIN TPLANSCHEDULE TB2 ON TB1.NPLANID = TB2.NPLANID 
        //                INNER JOIN TVENDOR TB3 ON TB3.SVENDORID = TB2.SVENDORID 
        //                INNER JOIN TTerminal_SAP TB4 ON TB2.sTerminalID = TB4.sTerminalID 
        //                INNER JOIN (SELECT SHIP_TO
        //                ,CASE WHEN NVL(DELIVERY_NO,'NULL') <> 'NULL' THEN 1 ELSE 0 END COLUMN7 
        //                ,CASE WHEN NVL(DELIVERY_NO,'NULL') = 'NULL' THEN 1 ELSE 0 END COLUMN8 
        //                ,CASE WHEN NVL(SHIPMET_STS,'4') = '6' THEN 1 ELSE 0 END COLUMN9 
        //                FROM TSHIPMENT) TB5 ON TB5.SHIP_TO = TB1.SSHIPTO 
        //                WHERE 1=1 AND TB2.dPlan BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
        //                AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') " + _comm + @"
        //                GROUP BY sTerminalName,SABBREVIATION,TB2.SCONTRACTNO
        //                ORDER BY sTerminalName,SABBREVIATION) GROUP BY COLUMN0,COLUMN1,COLUMN2";

        //        string sqlquery = @"SELECT COLUMN0,COLUMN1,COLUMN2,SUM(COLUMN3) COLUMN3 ,SUM(COLUMN4) COLUMN4,SUM(COLUMN5) COLUMN5,SUM(COLUMN6) COLUMN6 
        //        ,SUM(COLUMN7) COLUMN7,SUM(COLUMN8) COLUMN8,SUM(COLUMN9) COLUMN9 ,0 SUMCOLUMN3 
        //        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        //        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        //        FROM (SELECT sTerminalName COLUMN0 ,SABBREVIATION COLUMN1,SCONTRACTNO COLUMN2,1 COLUMN3
        //        ,CASE WHEN NVL(CCONFIRM,2) = 2 THEN 1 ELSE 0 END COLUMN4 
        //        ,CASE WHEN NVL(CCONFIRM,2) = 0 THEN 1 ELSE 0 END COLUMN5 
        //        ,CASE WHEN NVL(CCONFIRM,2) = 1 THEN 1 ELSE 0 END COLUMN6      
        //        --,CASE WHEN NVL(CCONFIRM,2) = 1 AND NVL(DELIVERY_NO,'NODATA') <> 'NODATA' THEN 1 ELSE 0 END COLUMN7 
        //        --,CASE WHEN NVL(DELIVERY_NO,'NODATA') = 'NODATA' THEN 1 ELSE 0 END COLUMN8 
        //       --,CASE WHEN NVL(SHIPMET_STS,'4') = '6' THEN 1 ELSE 0 END COLUMN9 
        //       ,0 COLUMN7,0 COLUMN8,0 COLUMN9 
        //        FROM TPLANSCHEDULELIST TB1 INNER JOIN TPLANSCHEDULE TB2 ON TB1.NPLANID = TB2.NPLANID 
        //        INNER JOIN TVENDOR TB3 ON TB3.SVENDORID = TB2.SVENDORID 
        //        INNER JOIN TTerminal_SAP TB4 ON TB2.sTerminalID = TB4.sTerminalID 
        //        --INNER JOIN TSHIPMENT TB5 ON TB5.SHIP_TO = TB1.SSHIPTO 
        //        WHERE 1=1 AND TB2.dPlan BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
        //        AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') " + _comm + @"
        //        ORDER BY sTerminalName,SABBREVIATION) GROUP BY COLUMN0,COLUMN1,COLUMN2";
        #endregion
        string sqlqueryGroup = @"SELECT distinct sTerminalName COLUMN0 ,SABBREVIATION COLUMN1 ,NVL(SCONTRACTNO,'ไม่ระบุสัญญา'||( CASE WHEN TB2.CFIFO='1' THEN '(แผนFIFO)' ELSE '(FIFO)' END) ||'') COLUMN2
        ,0 COLUMN3,0 COLUMN4,0 COLUMN5,0 COLUMN6,0 COLUMN7,0 COLUMN8,0 COLUMN9,0 SUMCOLUMN3,0 SUMCOLUMN4,0 SUMCOLUMN5,0 SUMCOLUMN6,0 SUMCOLUMN7,0 SUMCOLUMN8,0 SUMCOLUMN9
        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd 
,TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') - TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') nDate
        FROM TPLANSCHEDULELIST TB1 LEFT OUTER JOIN TPLANSCHEDULE TB2 ON TB1.NPLANID = TB2.NPLANID 
        INNER JOIN TVENDOR TB3 ON TB3.SVENDORID = TB2.SVENDORID 
        INNER JOIN TTerminal_SAP TB4 ON TB2.sTerminalID = TB4.sTerminalID 
        WHERE 1=1 AND NVL(SCONTRACTNO,'xxx') <> 'xxx' AND TRUNC(TB2.dPlan) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
        AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') " + _comm +@"
        UNION
        SELECT TB4.sTerminalName,ODP.SABBREVIATION,ODP.SCONTRACTNO          ,0 COLUMN3,0 COLUMN4,0 COLUMN5,0 COLUMN6,0 COLUMN7,0 COLUMN8,0 COLUMN9,0 SUMCOLUMN3,0 SUMCOLUMN4,0 SUMCOLUMN5,0 SUMCOLUMN6,0 SUMCOLUMN7,0 SUMCOLUMN8,0 SUMCOLUMN9
        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd 
,TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') - TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') nDate
FROM
(
    SELECT SABBREVIATION,SCONTRACTNO,STERMINALID,COUNT(SDELIVERYNO)  FROM TBL_ORDERPLAN
    WHERE NVL(CACTIVE,'Y') = 'Y'
    " + ODP_CON + @"
    AND TRUNC(DDELIVERY) BETWEEN  TO_DATE('" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy') AND  TO_DATE('" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy') 
    GROUP BY SABBREVIATION,SCONTRACTNO,STERMINALID
)ODP
LEFT  JOIN TTerminal_SAP TB4 ON TB4.sTerminalID = ODP.STERMINALID WHERE 1=1" + ODP_CON_TM;

        string sqlquery = @"SELECT distinct sTerminalName COLUMN0 ,SABBREVIATION COLUMN1,NVL(SCONTRACTNO,'ไม่ระบุสัญญา'||( CASE WHEN TB2.CFIFO='1' THEN '(แผนFIFO)' ELSE '(FIFO)' END) ||'') COLUMN2,
        CCONFIRM,CAST(NVL(SHIPMET_STS,0) AS number) SHIPMET_STS,TB1.SDELIVERYNO DELIVERY_NO,NVL(TB2.CFIFO,'9') as  CFIFO
        FROM TPLANSCHEDULE TB2 LEFT JOIN TPLANSCHEDULELIST TB1 ON TB1.NPLANID = TB2.NPLANID
        LEFT JOIN TSHIPMENT TB5 ON TB5.DELIVERY_NO = TB1.SDELIVERYNO
        INNER JOIN TVENDOR TB3 ON TB3.SVENDORID = TB2.SVENDORID 
        INNER JOIN TTerminal_SAP TB4 ON TB2.sTerminalID = TB4.sTerminalID 
        WHERE 1=1 --AND NVL(TB1.SDELIVERYNO,'None_Do') !='None_Do'
        AND NVL(SCONTRACTNO,'xxx') <> 'xxx' AND TRUNC(TB2.dPlan) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')  " + cond;

        string sqlquery2 = @"SELECT distinct TB2.SABBREVIATION COLUMN0,TRUNC(TB1.DELIVERY_DATE),TB1.DELIVERY_NO
FROM TDELIVERY_SAP TB1 INNER JOIN TTERMINAL_TMP TB2 ON TB1.SUPPLY_PLANT = STERMINALID
        WHERE TRUNC(TB1.DELIVERY_DATE)  BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
        AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') ";

        string Qry_ALLDO = @" SELECT distinct sTerminalName COLUMN0 ,SABBREVIATION COLUMN1,NVL(SCONTRACTNO,'ไม่ระบุสัญญา'||( CASE WHEN TB2.CFIFO='1' THEN '(แผนFIFO)' ELSE '(FIFO)' END) ||'') COLUMN2,
        CCONFIRM,CAST(NVL(SHIPMET_STS,0) AS number) SHIPMET_STS,DELIVERY_NO ,TB_SHPT.CARRIER 
        ,CASE WHEN SUBSTR(SHIPMET_STS,DELIVERY_NO,'008')='008' THEN 'STO' ELSE 'SO' END DO_TYPE
        FROM TPLANSCHEDULE TB2 LEFT JOIN TPLANSCHEDULELIST TB1 ON TB1.NPLANID = TB2.NPLANID
        LEFT JOIN (
             SELECT SHP_DLVR.SHIPMENT_NO ,SHP_DLVR.DELIVERY_NO 
            ,CASE WHEN SUBSTR(SHP_DLVR.DELIVERY_NO,1,3)='008' THEN 'STO' ELSE 'SO' END DELIVERY_TYPE
            ,DLVRSAP.SUPPLY_PLANT,SHP_VEHI.CARRIER
            from SAPECP1000087.SHIPMENT_DELIVERY_GROUP@MASTER SHP_DLVR
            LEFT JOIN SAPECP1000087.SHIPMENT_VEHICLE_GROUP@MASTER SHP_VEHI ON SHP_DLVR.SHIPMENT_NO=SHP_VEHI.SHIPMENT_NO 
            LEFT JOIN TDELIVERY_SAP DLVRSAP ON SHP_DLVR.DELIVERY_NO=DLVRSAP.DELIVERY_NO
            where 1=1 and SHP_VEHI.CARRIER != 'PHTDPRT' 
            and TRUNC(SHP_VEHI.DELIVERY_DATE) >= TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  AND TRUNC(SHP_VEHI.DELIVERY_DATE) <= TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') 
            --AND DLVRSAP.SUPPLY_PLANT is null
            GROUP BY SHP_DLVR.SHIPMENT_NO,SHP_VEHI.CARRIER
             ,CASE WHEN SUBSTR(SHP_DLVR.DELIVERY_NO,1,3)='008' THEN 'STO' ELSE 'SO' END
            ,SHP_DLVR.DELIVERY_NO  ,DLVRSAP.SUPPLY_PLANT
        ) TB_SHPT ON TB1.SDELIVERYNO =TB_SHPT. DELIVERY_NO
        LEFT JOIN TSHIPMENT TB5 ON TB5.DELIVERY_NO = TB1.SDELIVERYNO
        INNER JOIN TVENDOR TB3 ON TB3.SVENDORID = TB2.SVENDORID 
        INNER JOIN TTerminal_SAP TB4 ON TB2.sTerminalID = TB4.sTerminalID 
        WHERE 1=1 AND TRUNC(TB2.dPlan) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  
        --AND TB_SHPT.CARRIER !='PHTDPRT'
         " + cond;
        //Response.Write(sqlquery); return;

        string QUERY_ORDERPLAN = @"SELECT TB4.sTerminalName,ODP.SABBREVIATION,ODP.SCONTRACTNO,ODP.SDELIVERYNO,NVL(TSP.SDELIVERYNO,'XXX') as FLAG  FROM
(
    SELECT SABBREVIATION,SCONTRACTNO,SDELIVERYNO,STERMINALID,COUNT(SDELIVERYNO)  FROM TBL_ORDERPLAN
    WHERE NVL(CACTIVE,'Y') = 'Y'
    " + ODP_CON + @"
    AND TRUNC(DDELIVERY) BETWEEN  TO_DATE('" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy') AND  TO_DATE('" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy') 
    GROUP BY SABBREVIATION,SCONTRACTNO,SDELIVERYNO,STERMINALID
)ODP
LEFT JOIN 
(
    SELECT SDELIVERYNO FROM TPLANSCHEDULELIST
    WHERE NVL(CACTIVE,'1') = '1'
    GROUP BY SDELIVERYNO
)TSP
ON TSP.SDELIVERYNO = ODP.SDELIVERYNO
LEFT  JOIN TTerminal_SAP TB4 ON TB4.sTerminalID = ODP.STERMINALID WHERE 1=1 " + ODP_CON_TM;


        DataTable _dt = CommonFunction.Get_Data(sql, sqlquery);
        DataTable _dtAllDOInTime = CommonFunction.Get_Data(sql, Qry_ALLDO);
        DataTable _dtGroup = CommonFunction.Get_Data(sql, sqlqueryGroup);
        DataTable _dtODP = CommonFunction.Get_Data(sql, QUERY_ORDERPLAN);
        //foreach (DataRow _dr in _dt.Rows)
        //{
        //    _dr["SUMCOLUMN3"] = float.Parse(_dt.Compute("SUM(COLUMN3)", " COLUMN0 = '" + _dr["COLUMN0"] + "'").ToString());
        //}
        int COLUMN3 = 0;
        int COLUMN4 = 0;
        int COLUMN5 = 0;
        int COLUMN6 = 0;
        int COLUMN7 = 0;
        int COLUMN8 = 0;
        int COLUMN9 = 0;
        foreach (DataRow _dr in _dtGroup.Rows)
        {
            //Sum รายคลัง บริษัท สัญญา

            _dr["COLUMN3"] = (_dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "'  AND CFIFO <> '9' ").Length + _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "' AND SABBREVIATION = '" + _dr["COLUMN1"] + "' AND SCONTRACTNO = '" + _dr["COLUMN2"] + "'").Length);//where plant and vendor and contract
            //_dr["COLUMN4"] = _dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND CFIFO = '9' ").Length;
            _dr["COLUMN4"] = _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "' AND SABBREVIATION = '" + _dr["COLUMN1"] + "' AND SCONTRACTNO = '" + _dr["COLUMN2"] + "'").Length;
            _dr["COLUMN5"] = _dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND CFIFO <> '9' ").Length;
            //_dr["COLUMN6"] = _dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND CCONFIRM = '1'  AND CFIFO = '9' ").Length;
            _dr["COLUMN6"] = _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "' AND SABBREVIATION = '" + _dr["COLUMN1"] + "' AND SCONTRACTNO = '" + _dr["COLUMN2"] + "' AND FLAG <> 'XXX'").Length;
            _dr["COLUMN7"] = _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND SHIPMET_STS >= 4 ").Length;
            _dr["COLUMN8"] = _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND SHIPMET_STS < 4 ").Length;
            _dr["COLUMN9"] = _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND SHIPMET_STS = 6 ").Length;

            //Sum รายคลัง
            _dr["SUMCOLUMN3"] = (_dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "'  AND CFIFO <> '9'").Length + _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "'").Length);
            _dr["SUMCOLUMN4"] = _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "'").Length;
            _dr["SUMCOLUMN5"] = _dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND CFIFO <> '9' ").Length;
            _dr["SUMCOLUMN6"] = _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "'  AND FLAG <> 'XXX'").Length;
            _dr["SUMCOLUMN7"] = _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "'  AND SHIPMET_STS >= 4 ").Length;
            _dr["SUMCOLUMN8"] = _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "'  AND SHIPMET_STS < 4 ").Length;
            _dr["SUMCOLUMN9"] = _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "'  AND SHIPMET_STS = 6 ").Length;

            //Sum ทั้งหมด

            COLUMN3 = COLUMN3 + (_dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "'  AND CFIFO <> '9' ").Length + _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "' AND SABBREVIATION = '" + _dr["COLUMN1"] + "' AND SCONTRACTNO = '" + _dr["COLUMN2"] + "'").Length);
            //COLUMN4 = COLUMN4 + _dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND CFIFO = '9' ").Length;
            COLUMN4 = COLUMN4 + _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "' AND SABBREVIATION = '" + _dr["COLUMN1"] + "' AND SCONTRACTNO = '" + _dr["COLUMN2"] + "'").Length;
            COLUMN5 = COLUMN5 + _dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND CFIFO <> '9' ").Length;
            //COLUMN6 = COLUMN6 + _dt.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND CCONFIRM = '1'  AND CFIFO = '9' ").Length;
            COLUMN6 = COLUMN6 + _dtODP.Select("sTerminalName = '" + _dr["COLUMN0"] + "' AND SABBREVIATION = '" + _dr["COLUMN1"] + "' AND SCONTRACTNO = '" + _dr["COLUMN2"] + "' AND FLAG <> 'XXX'").Length;
            COLUMN7 = COLUMN7 + _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND SHIPMET_STS >= 4 ").Length;
            COLUMN8 = COLUMN8 + _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND SHIPMET_STS < 4 ").Length;
            COLUMN9 = COLUMN9 + _dtAllDOInTime.Select("COLUMN0 = '" + _dr["COLUMN0"] + "' AND COLUMN1 = '" + _dr["COLUMN1"] + "' AND COLUMN2 = '" + _dr["COLUMN2"] + "' AND SHIPMET_STS = 6 ").Length;

            //_dr["ALLSUMCOLUMN3"] = _dt.Rows.Count;
            //_dr["ALLSUMCOLUMN4"] = _dt.Select("CFIFO = '9' ").Length;
            //_dr["ALLSUMCOLUMN5"] = _dt.Select("CFIFO <> '9' ").Length;
            //_dr["ALLSUMCOLUMN6"] = _dt.Select("CCONFIRM = '1'  AND CFIFO = '9' ").Length;
            //_dr["ALLSUMCOLUMN7"] = _dtAllDOInTime.Select("SHIPMET_STS >= 4 ").Length;
            //_dr["ALLSUMCOLUMN8"] = _dtAllDOInTime.Select("SHIPMET_STS < 4 ").Length;
            //_dr["ALLSUMCOLUMN9"] = _dtAllDOInTime.Select("SHIPMET_STS = 6 ").Length;
        }
        if (_dtGroup.Rows.Count > 0)
        {
            DataColumn dc = new DataColumn();
            dc.ColumnName = "ALLSUMCOLUMN3";
            dc.DataType = typeof(decimal);
            dc.DefaultValue = COLUMN3 + "";
            _dtGroup.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "ALLSUMCOLUMN4";
            dc.DataType = typeof(decimal);
            dc.DefaultValue = COLUMN4 + "";
            _dtGroup.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "ALLSUMCOLUMN5";
            dc.DataType = typeof(decimal);
            dc.DefaultValue = COLUMN5 + "";
            _dtGroup.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "ALLSUMCOLUMN6";
            dc.DataType = typeof(decimal);
            dc.DefaultValue = COLUMN6 + "";
            _dtGroup.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "ALLSUMCOLUMN7";
            dc.DataType = typeof(decimal);
            dc.DefaultValue = COLUMN7 + "";
            _dtGroup.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "ALLSUMCOLUMN8";
            dc.DataType = typeof(decimal);
            dc.DefaultValue = COLUMN8 + "";
            _dtGroup.Columns.Add(dc);

            dc = new DataColumn();
            dc.ColumnName = "ALLSUMCOLUMN9";
            dc.DataType = typeof(decimal);
            dc.DefaultValue = COLUMN9 + "";
            _dtGroup.Columns.Add(dc);


        }

        xtrDetailConfirmPlan report = new xtrDetailConfirmPlan();

        report.DataSource = _dtGroup;
        string fileName = "reportDetailConfirmPlan_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        if (fileType == "xls")
            report.ExportToXls(stream);
        else
            report.ExportToPdf(stream);

        Response.ContentType = "application/" + fileType;
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName + "." + fileType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORNAME LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void TP01RouteOnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsOrganiz.SelectCommand = "SELECT STERMINALID, STERMINALNAME FROM (SELECT T.STERMINALID, TS.STERMINALNAME, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T  INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE T.STERMINALID || TS.STERMINALNAME LIKE :fillter AND (T.STERMINALID LIKE :Plant5 OR T.STERMINALID LIKE :Plant8))  WHERE RN BETWEEN :startIndex AND :endIndex";
        sdsOrganiz.SelectParameters.Clear();
        sdsOrganiz.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsOrganiz.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("Plant5", PlantHelper.Plant5);
        sdsOrganiz.SelectParameters.Add("Plant8", PlantHelper.Plant8);

        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();

    }

    protected void TP01RouteOnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboSCONTRACTNO_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        dscboSCONTRACTNO.SelectCommand = "SELECT * FROM TCONTRACT WHERE SCONTRACTNO  LIKE :fillter  ";
        dscboSCONTRACTNO.SelectParameters.Clear();
        dscboSCONTRACTNO.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        //sdsOrganiz.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        //sdsOrganiz.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = dscboSCONTRACTNO;
        comboBox.DataBind();
    }
    protected void cboSCONTRACTNO_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    private int CheckZero(int Int_Check)
    {
        int Result = 1;
        if (Int_Check > 0)
        {
            Result = Int_Check;
        }

        return Result;
    }
}