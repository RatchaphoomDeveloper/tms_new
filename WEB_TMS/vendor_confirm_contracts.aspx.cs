﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DevExpress.Web.ASPxCallbackPanel;
using System.Web.Configuration;
using TMS_BLL.Transaction.ContractConfirm;
using GemBox.Spreadsheet;
using System.Web.Security;
using TMS_BLL.Master;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

public partial class vendor_confirm_contracts : PageBase
{
    //#region + View State +
    //private string SCONTRACTID
    //{
    //    get
    //    {
    //        if ((string)ViewState["SCONTRACTID"] != null)
    //            return (string)ViewState["SCONTRACTID"];
    //        else
    //            return string.Empty;
    //    }
    //    set
    //    {
    //        ViewState["SCONTRACTID"] = value;
    //    }
    //}

    //private string SCONTRACTNO
    //{
    //    get
    //    {
    //        if ((string)ViewState["SCONTRACTNO"] != null)
    //            return (string)ViewState["SCONTRACTNO"];
    //        else
    //            return string.Empty;
    //    }
    //    set
    //    {
    //        ViewState["SCONTRACTNO"] = value;
    //    }
    //}

    //private string DDATE
    //{
    //    get
    //    {
    //        if ((string)ViewState["DDATE"] != null)
    //            return (string)ViewState["DDATE"];
    //        else
    //            return string.Empty;
    //    }
    //    set
    //    {
    //        ViewState["DDATE"] = value;
    //    }
    //}

    //private string NCONFIRMID
    //{
    //    get
    //    {
    //        if ((string)ViewState["NCONFIRMID"] != null)
    //            return (string)ViewState["NCONFIRMID"];
    //        else
    //            return string.Empty;
    //    }
    //    set
    //    {
    //        ViewState["NCONFIRMID"] = value;
    //    }
    //}

    //private string DEXPIRE
    //{
    //    get
    //    {
    //        if ((string)ViewState["DEXPIRE"] != null)
    //            return (string)ViewState["DEXPIRE"];
    //        else
    //            return string.Empty;
    //    }
    //    set
    //    {
    //        ViewState["DEXPIRE"] = value;
    //    }
    //}
    //#endregion
    private DataTable dtIVMSCheckService
    {
        get
        {
            if ((DataTable)ViewState["dtIVMSCheckService"] != null)
                return (DataTable)ViewState["dtIVMSCheckService"];
            else
                return null;
        }
        set
        {
            ViewState["dtIVMSCheckService"] = value;
        }
    }

    #region Member
    int defInt;
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    string connIVMS = WebConfigurationManager.ConnectionStrings["IVMSConnectionString"].ConnectionString;
    const string UploadDirectory = "~/UploadFile/EvidenceOfWrongdoing/";
    const string UploadGPSDirectory = "~/UploadFile/GPS/";
    const string UploadConfirmTruckDirectory = "~/UploadFile/ConfirmTruck/";
    const int ThumbnailSize = 100;

    public bool btnsubmitVisible
    {

        get { return Session["btnsubmitVisible"] == null ? true : (bool)Session["btnsubmitVisible"]; }
        set { Session["btnsubmitVisible"] = value; }
    }
    string[] ArrayGPSType = { "XLS", "PIC" };
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        try
        {
            if (!ConfigurationManager.AppSettings["EnableCR3"].Equals("TRUE"))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EnableCR3_FALSE", "if(confirm('การยืนยันรถตามสัญญาในเวอร์ชั่นใหม่ยังไม่เปิดให้งานจากเมนูนี้ กรุณาเข้าที่ ยืนยันรถตามสัญญา->ยืนยันรถตามสัญญา ท่านต้องการไปยังแมนูยืนยันรถตามสัญญาใช่หรือไม่?')){ window.location='vendor_confirm_contract.aspx';}else{ window.location='default.aspx'; }", true);
                return;
            }
            #region Event
            //xgvw.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(xgvw_CustomColumnDisplayText);
            //xgvw.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(xgvw_AfterPerformCallback);
            //xgvw.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(xgvw_HtmlRowPrepared);
            //xgvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(xgvw_HtmlDataCellPrepared);
            #endregion
            if (Session["UserID"] == null || Session["UserID"] + "" == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
            }
            if (!IsPostBack)
            {
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
                this.AssignAuthen();
                LogUser("1", "R", "เปิดดูข้อมูลหน้า ยืนยันรถตามสัญญา", "");

                //CommonFunction.CleanFileInFolder(7, "UploadFile/GPS/Temp_PIC");
                //CommonFunction.CleanFileInFolder(7, "UploadFile/GPS/Temp__Excel");
                //Cache.Remove(sdsContract.CacheKeyDependency);
                //Cache[sdsContract.CacheKeyDependency] = new object();

                //sdsContract.SelectParameters["DSTART"].DefaultValue = "12/16/2015 00:00:00";
                //sdsContract.SelectParameters["DEND"].DefaultValue = "12/16/2015 23:59:59";
                //sdsContract.Select(new System.Web.UI.DataSourceSelectArguments());

                //sdsContract.DataBind();

                //this.LoadContract(false);

                Cache.Remove(sdsTruck.CacheKeyDependency);
                Cache[sdsTruck.CacheKeyDependency] = new object();
                sdsTruck.Select(new System.Web.UI.DataSourceSelectArguments());
                sdsTruck.DataBind();

                Session.Remove("ss_data");
                Session.Remove("ss_filedata");
                Session.Remove("IssueData");
                Session.Remove("DataAttechment");
                Session.Remove("ss_GPSExcel");
                Session.Remove("ss_DataGPSExcel");
                Session.Remove("DataTcontractTruck");
                //dteStart.Value = DateTime.Now.ToString("dd/MM/yyyy",new CultureInfo("th-TH"));
                dteEnd.Text = DateTime.Now.ToString("dd/MM/yyyy");//.AddDays(1)
                SearchData("");
            }
            else
            {
                // ASPxButton btnsubmit = xgvw.FindEditFormTemplateControl("btnsubmit") as ASPxButton;

                //btnsubmit.ClientVisible = true;
                /*UploadControl.ShowProgressPanel = true;*/
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;

            }
            if (!CanWrite)
            {
                btnConfirm.Enabled = false;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadContract(bool ShowTotal)
    {
        try
        {
            DataTable dtContract = ContractConfirmBLL.Instance.SelectTruckConfirmBLL(Session["UserID"].ToString(), dteEnd.Text + " 00:00:00", dteEnd.Text + " 23:59:59", txtKeyword.Text + "", cmbStatus.SelectedValue + "");
            xgvw.DataSource = dtContract;
            xgvw.DataBind();

            if (ShowTotal)
                lblRecord.Text = dtContract.Select("CCONFIRM<>'1'").Length + "";

            Session["ss_DataGPSExcel"] = dtContract;

            if (Session["DataTTERMINAL"] == null)
            {
                Session["DataTTERMINAL"] = ContractBLL.Instance.TTERMINALSelect();
            }
            if (Session["DataMcarConfirmStatus"] == null)
            {
                Session["DataMcarConfirmStatus"] = ContractConfirmBLL.Instance.GetMcarConfirmStatus();
            }


        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void SearchData(string _mode)
    {
        try
        {
            if (!_mode.Equals("CANCELEDIT"))
            {
                Session.Remove("ss_data");
                Session.Remove("ss_filedata");
                Session.Remove("ss_GPSExcel");
                Session.Remove("ss_DataGPSExcel");
            }
            imp_TTRUCKCONFIRM();

            //Cache.Remove(sdsContract.CacheKeyDependency);
            //Cache[sdsContract.CacheKeyDependency] = new object();

            //sdsContract.SelectCommand.Replace("P_DSTART", "12/8/2015 00:00:00");
            //sdsContract.SelectCommand.Replace("P_DEND", "12/8/2015 23:59:59");

            //sdsContract.SelectParameters["DSTART"].DefaultValue = "12/16/2015 00:00:00";
            //sdsContract.SelectParameters["DEND"].DefaultValue = "12/16/2015 23:59:59";
            //sdsContract.Select(new System.Web.UI.DataSourceSelectArguments());

            //sdsContract.DataBind();
            //xgvw.DataBind();

            //DataView dv = (DataView)sdsContract.Select(DataSourceSelectArguments.Empty);
            //DataTable dt = dv.ToTable();
            //lblRecord.Text = dt.Select("CCONFIRM<>'1'").Length + "";
            //dt.Dispose();

            this.LoadContract(true);

            Cache.Remove(sdsTruck.CacheKeyDependency);
            Cache[sdsTruck.CacheKeyDependency] = new object();
            sdsTruck.Select(new System.Web.UI.DataSourceSelectArguments());
            sdsTruck.DataBind();

            Cache.Remove(sdsCheckLists.CacheKeyDependency);
            Cache[sdsCheckLists.CacheKeyDependency] = new object();
            sdsCheckLists.Select(new System.Web.UI.DataSourceSelectArguments());
            sdsCheckLists.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void imp_TTRUCKCONFIRM()
    {
        try
        {
            #region Update กรณีสั่งให้แก้ แล้วไม่แก้ไขและเกินวันที่กำหนดแล้ว ให้ระบบตีเป็นห้ามวิ่ง
            using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
            {

                OracleCommand TBL_TRUCK_ONHOLD = new OracleCommand();
                TBL_TRUCK_ONHOLD.Connection = OraConnection;
                if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();
                TBL_TRUCK_ONHOLD.CommandType = CommandType.Text;
                TBL_TRUCK_ONHOLD.CommandText = @"INSERT INTO TBL_TRUCK_ONHOLD
    SELECT DISTINCT chk.STRUCKID ,sysdate DATE_Add ,chklst.DEND_LIST ,VEH.STRUCKID VEHID,chk.SHEADERREGISTERNO  ,TU.STRUCKID TUID ,chk.STRAILERREGISTERNO,'' Send_Status
        FROM TCheckTruckItem  chklst
        LEFT JOIN TCHECKTRUCK chk ON chklst.SCHECKID =chk.SCHECKID 
        LEFT JOIN TTRUCK VEH ON chk.SHEADERREGISTERNO=VEH.SHEADREGISTERNO 
        LEFT JOIN TTRUCK TU ON chk.STRAILERREGISTERNO=TU.SHEADREGISTERNO 
        WHERE NVL(chklst.CEDITED,'0') !='1' AND chklst.CMA IN('2','1')
         AND chklst.DEND_LIST < SYSDATE AND NVL(VEH.CHOLD,'0')='0' 
         AND VEH.STRUCKID is not null
         AND TRUNC(chklst.DEND_LIST) > TRUNC(SYSDATE,'yyyy')";
                TBL_TRUCK_ONHOLD.ExecuteNonQuery();




                OracleCommand U_TTRUCK_Cmd = new OracleCommand();
                U_TTRUCK_Cmd.Connection = OraConnection;
                if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();
                U_TTRUCK_Cmd.CommandType = CommandType.Text;
                U_TTRUCK_Cmd.CommandText = @" UPDATE TTRUCK SET CHOLD='1' WHERE STRUCKID IN(
        SELECT DISTINCT TCHECKTRUCK.STRUCKID
        FROM TCheckTruckItem 
        LEFT JOIN TCHECKTRUCK ON TCheckTruckItem.SCHECKID =TCheckTruck.SCHECKID 
        LEFT JOIN TTRUCK ON TCHECKTRUCK.STRUCKID=TTRUCK.STRUCKID 
        WHERE NVL(TCheckTruckItem.CEDITED,'0') !='1' AND TCheckTruckItem.CMA IN('2','1')
         AND TCheckTruckItem.DEND_LIST < SYSDATE AND NVL(TTRUCK.CHOLD,'0')='0')";
                U_TTRUCK_Cmd.ExecuteNonQuery();
            }
            #endregion
            if ((!String.IsNullOrEmpty(dteEnd.Text + "")))
            {
                OracleCommand oraCmd = new OracleCommand();
                oraCmd.Connection = connection;
                if (connection.State == ConnectionState.Closed) connection.Open();

                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = "Imp_ContractConfirm";

                oraCmd.Parameters.Add("D_BEGIN", OracleType.NVarChar).Value = (object)DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                oraCmd.Parameters.Add("D_END", OracleType.NVarChar).Value = (object)ConvertToDateNull(dteEnd.Text).Value.AddDays(2).ToString("dd/MM/yyyy", new CultureInfo("en-US"));//แอดล่วงหน้า 1 วัน
                oraCmd.Parameters.Add("usr_SUID", OracleType.VarChar).Value = Session["UserID"] + "";

                oraCmd.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        try
        {
            DbCommand command = e.Command;
            DbConnection cx = command.Connection;
            cx.Open();
            DbTransaction tx = cx.BeginTransaction();
            command.Transaction = tx;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        try
        {
            DbCommand command = e.Command;
            DbTransaction tx = command.Transaction;

            bool isProcessSucceeded = (e.Exception == null);
            Session["FLAGIUCOMMAND"] = isProcessSucceeded;

            if (isProcessSucceeded)
            {
                tx.Commit();
            }
            else
            {
                tx.Rollback();
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    protected DataTable PrepareDataTable(string ss_name, string sdate, string contractid, params string[] fields)
    {
        try
        {
            DataTable dtData = new DataTable();
            if (Session[ss_name] != null)
            {
                dtData = (DataTable)Session[ss_name];
                foreach (DataRow drDel in dtData.Select("DDATE='" + sdate + "' AND SCONTRACTID='" + contractid + "'"))
                {
                    int idx = dtData.Rows.IndexOf(drDel);
                    dtData.Rows[idx].Delete();
                }
            }
            else
            {
                foreach (string field_type in fields)
                {
                    dtData.Columns.Add(field_type + "");
                }
            }
            return dtData;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    protected bool ReducePoint(string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    { //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO 
        try
        {
            bool IsReduce = true;
            TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection);
            repoint.NREDUCEID = "";
            //repoint.DREDUCE = "";
            repoint.CACTIVE = "1";
            repoint.SREDUCEBY = "" + Session["UserID"];
            repoint.SREDUCETYPE = REDUCETYPE;
            repoint.SPROCESSID = PROCESSID;
            repoint.SREDUCENAME = "" + sArrayParams[0];
            repoint.SCHECKLISTID = "" + sArrayParams[1];
            repoint.STOPICID = "" + sArrayParams[2];
            repoint.SVERSIONLIST = "" + sArrayParams[3];
            repoint.NPOINT = "" + sArrayParams[4];
            repoint.SREFERENCEID = "" + sArrayParams[5];
            repoint.SCONTRACTID = "" + sArrayParams[6];
            repoint.SHEADID = "" + sArrayParams[7];
            repoint.SHEADREGISTERNO = "" + sArrayParams[8];
            repoint.STRAILERID = "" + sArrayParams[9];
            repoint.STRAILERREGISTERNO = "" + sArrayParams[10];

            repoint.Insert();
            return IsReduce;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    #region BindTruckWithDataInGridEditing
    protected void BindTruckWithDataInGridEditing(ASPxGridView _gvw, params string[] sArrayParams)
    {
        try
        {
            DataTable dt;
            int visibleindex = (xgvw.IsEditing) ? xgvw.EditingRowVisibleIndex : 0;
            dynamic data = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE");
            //string _Query = "", DOC_Query = "";
            //DOC_Query = string.Format(@"SELECT DOC_ID,DOC_ID IMP_ID,SCONTRACTID,DDATE,CACTIVE,DCREATE,DUPDATE,DDELETE,DOC_TYPE,DOC_SYSNAME SystemFileName,DOC_ORGNAME OriginalFileName,DOC_CODE,SREMARK FROM TTRUCKCONFIRM_DOC WHERE 1=1 AND NVL(CACTIVE,'1')='1' AND SCONTRACTID='{0}' AND DDATE=TO_DATE('{1}','DD/MM/YYYY')"
            //    , "" + data[0], Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            //DataTable _dtDOC = CommonFunction.Get_Data(connection, DOC_Query);
            #region TRUCK DATA
            //            string _Query = string.Format(@"SELECT 
            //--'' SKEYID,
            //conf.STRUCKID,TRCK.SHEADREGISTERNO,conf.STRAILERID,CASE WHEN NVL(conf.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
            //,NDAY_MA,CONF.cpassed
            //,CONF.TCONFLST_CCONFIRM
            //,TRCK.SCARTYPENAME
            //,NVL(tcontract_truck.CSTANDBY,'N') CSTANDBY
            //,CONT.SCONTRACTID ,'9/3/2016 12:00:00 AM' DDATE,'N' CSEND
            //,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '2' ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END CMA, MAX(CMA) MAXCMA
            //,NVL(MIN(chktrck.DFINAL_MA),'') DFINAL_MA ,NVL(CONF.TCONFLST_CCONFIRM,'0') CCONFIRM
            //, CASE WHEN CONF.CSTANBY = 'Y' THEN 'รถปกติ' ELSE 'รถหมุนเวียน' END AS CARTYPE
            //,CONF.CSTANBY F18
            //, CONF.DELIVERY_PART
            //, CONF.CAR_STATUS
            //,CONF.CAR_STATUS_ID
            //, CONF.SABBREVIATION AS WAREHOUSE
            //,CONF.sterminalid
            //, CONF.DELIVERY_NUM
            //, CONF.GPS_STATUS
            //, CONF.CCTV_STATUS
            //, CONF.LAT_LONG
            //, CONF.KM2PLANT
            //, CONF.HR2PLANT
            //, CONF.ADDRESS_CURRENT
            //, CONF.REMARK
            //, CONF.PERSONAL_CODE
            //,CONF.IVMS_GPS_STAT,CONF.IVMS_MDVR_STAT,CONF.IVMS_STATUS,CONF.IVMS_RADIUS_KM,CONF.IVMS_CHECKDATE
            //FROM 
            //( 
            //  SELECT * 
            //  FROM TCONTRACT
            //  WHERE 1=1  AND SCONTRACTID='{0}' 
            //) cont
            //LEFT JOIN 
            //(
            //  SELECT tconf.NCONFIRMID ,tconf.SCONTRACTID  ,tconf.ddate,tconf.cconfirm tconf_cconfirm
            //  ,tconflst.nlistno ,tconflst.cconfirm tconflst_cconfirm ,tconflst.struckid ,tconflst.sheadid ,tconflst.sheadregisterno ,tconflst.strailerid ,tconflst.strailerregisterno ,tconflst.cstanby ,tconflst.cpassed
            //  , tconflst.DELIVERY_PART, M_CAR_CONFIRM_STATUS.CAR_STATUS_NAME AS CAR_STATUS, TTERMINAL.SABBREVIATION,tconflst.sterminalid, tconflst.DELIVERY_NUM, tconflst.GPS_STATUS, tconflst.CCTV_STATUS
            //  , tconflst.LAT_LONG, tconflst.KM2PLANT, tconflst.HR2PLANT, tconflst.ADDRESS_CURRENT, tconflst.REMARK, tconflst.PERSONAL_CODE,tconflst.CAR_STATUS_ID,tconflst.IVMS_GPS_STAT,tconflst.IVMS_MDVR_STAT,tconflst.IVMS_STATUS,tconflst.IVMS_RADIUS_KM,tconflst.IVMS_CHECKDATE
            // FROM ttruckconfirm tconf 
            //  LEFT JOIN ttruckconfirmlist tconflst ON tconf.nconfirmid=tconflst.nconfirmid
            //  LEFT JOIN M_CAR_CONFIRM_STATUS ON tconflst.CAR_STATUS_ID = M_CAR_CONFIRM_STATUS.CAR_STATUS_ID
            //  LEFT JOIN TTERMINAL ON tconflst.STERMINALID = TTERMINAL.STERMINALID
            //  where tconf.scontractid='{0}' AND tconf.DDATE=TO_DATE('{2}','DD/MM/YYYY')
            //) conf ON CONT.SCONTRACTID=conf.SCONTRACTID 
            //  LEFT JOIN tcontract_truck ON tcontract_truck.scontractid = cont.scontractid AND tcontract_truck.struckid = conf.struckid
            //LEFT JOIN 
            //(
            //   SELECT chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO , MAX(chktrk.CMA) CMA,MIN(DBEGIN_MA) DBEGIN_MA,MIN(DFINAL_MA) DFINAL_MA,MIN(chktrk.NDAY_MA) NDAY_MA,COUNT(STRUCKID) nClean
            //   FROM TCheckTruck  chktrk  LEFT JOIN TCheckTruckITEM ichktrk ON chktrk.SCHECKID=ichktrk.SCHECKID
            //   WHERE 1=1 AND cClean='0' AND NVL(ichktrk.COTHER,'0')!='1'
            //   GROUP BY chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO --,chktrk.CMA
            //) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=conf.STRUCKID
            //LEFT JOIN 
            //(
            //  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
            //  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
            //  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
            //
            //) trck ON  conf.STRUCKID=trck.STRUCKID
            //LEFT JOIN 
            //(
            //  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
            //  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
            //  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
            //  WHERE 1=1 
            //) trcktail ON  conf.STRAILERID=trcktail.STRUCKID
            //
            //WHERE 1=1 --AND TRCK.CACTIVE='Y' --AND  NVL(CTRK.CSTANDBY,'N')='N'
            //AND cont.SCONTRACTID='{0}'  
            //GROUP BY conf.STRUCKID,TRCK.SHEADREGISTERNO,conf.STRAILERID,TRCK.STRAILERREGISTERNO,NDAY_MA,CONF.cpassed,CONF.TCONFLST_CCONFIRM,TRCK.SCARTYPENAME,CONF.CSTANBY,CONT.SCONTRACTID
            //,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '2' ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO
            //, CONF.DELIVERY_PART, CONF.CAR_STATUS, CONF.SABBREVIATION, CONF.DELIVERY_NUM, CONF.GPS_STATUS, CONF.CCTV_STATUS
            //, CONF.LAT_LONG, CONF.KM2PLANT, CONF.HR2PLANT, CONF.ADDRESS_CURRENT, CONF.REMARK, CONF.PERSONAL_CODE ,tcontract_truck.CSTANDBY,CONF.CAR_STATUS_ID
            //,CONF.sterminalid
            //,CONF.IVMS_GPS_STAT,CONF.IVMS_MDVR_STAT,CONF.IVMS_STATUS,CONF.IVMS_RADIUS_KM,CONF.IVMS_CHECKDATE
            //ORDER BY TRCK.SHEADREGISTERNO"
            //                 , "" + data[0], "" + data[1], Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            //            dt = CommonFunction.Get_Data(connection, _Query);
            #endregion

            DateTime ddate = Convert.ToDateTime("" + data[1]).AddDays(1);
            DataSet ds = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailSelect(ddate.ToString("dd/MM/yyyy"), string.Empty, data[0] + string.Empty, string.Empty, "Y", string.Empty, string.Empty, true);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                dt = ds.Tables[0];
                _gvw.DataSource = dt;
                _gvw.DataBind();
                Session["DataTcontractTruck"] = dt;
            }
            //else
            //{
            //    //กรณีที่ยังไม่ส่งข้อมูลเข้ามา สำรองเอาไว้
            //    dt = ContractConfirmBLL.Instance.GetTcontractTruck(data[0] + string.Empty);
            //    _gvw.DataSource = dt;
            //    _gvw.DataBind();
            //}

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region BindTruckWithDataFromExcel
    protected void BindTruckWithDataFromExcel(ASPxGridView _gvw, DataTable _dtTruck, params string[] sArrayParams)
    {
        try
        {
            int visibleindex = (xgvw.IsEditing) ? xgvw.EditingRowVisibleIndex : 0;
            DataView _dvTruck = new DataView(_dtTruck);
            //_dvTruck.Sort = "SHEADREGISTERNO ASC";
            _gvw.DataSource = _dvTruck;
            _gvw.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    string SavePostedFiles(UploadedFile uploadedFile)
    {
        try
        {
            string PathUploadDirectory = UploadDirectory + "/" + "";
            dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
            string IssueData = "" + Session["IssueData"];
            /*
             *0     14$58
             *1     58
             *2     "TR4901473"
             *3     "กท.75-0887"
             *4     ""
             *5     "#"
            */
            string[] ArrayIssueData = IssueData.Split('^');
            PathUploadDirectory = ArrayIssueData[1] + "/" + ArrayIssueData[2] + "/" + Convert.ToDateTime("" + MasterData[1]).ToString("MMddyyyy", new CultureInfo("en-US")) + "/";

            if (!uploadedFile.IsValid)
                return string.Empty;

            FileInfo fileInfo = new FileInfo(uploadedFile.FileName);
            if (!Directory.Exists(UploadDirectory + PathUploadDirectory)) Directory.CreateDirectory(MapPath(UploadDirectory + PathUploadDirectory));
            string resFileName = MapPath(UploadDirectory + PathUploadDirectory) + fileInfo.Name;

            if (File.Exists(resFileName))
            {
                string sPath = Path.GetDirectoryName(resFileName)
                    , sFileName = Path.GetFileNameWithoutExtension(resFileName)
                    , sFileType = Path.GetExtension(resFileName);
                int nExists = Directory.GetFiles(Path.GetDirectoryName(resFileName), "*" + Path.GetFileName(resFileName)).Length;
                sPath = sPath + "\\" + "(" + nExists + ")" + sFileName + sFileType;
                File.Copy(resFileName, sPath, true);
            }

            string fileLabel = fileInfo.Name;
            string fileLength = uploadedFile.ContentLength / 1024 + "K";
            #region temp Data Attechment
            {//temp Data Attechment 
                DataTable dtAttechment = new DataTable();
                #region prepaire temp data Attechment storage
                if (Session["DataAttechment"] == null)
                {
                    dtAttechment.Columns.Add("SCHECKID", typeof(string));
                    dtAttechment.Columns.Add("NATTACHMENT", typeof(string));
                    dtAttechment.Columns.Add("STRUCKID", typeof(string));
                    dtAttechment.Columns.Add("SCONTRACTID", typeof(string));
                    dtAttechment.Columns.Add("SATTACHTYPEID", typeof(string));
                    dtAttechment.Columns.Add("SPATH", typeof(string));
                    dtAttechment.Columns.Add("SFILE", typeof(string));
                    dtAttechment.Columns.Add("SSYSFILE", typeof(string));
                    dtAttechment.Columns.Add("DCREATE", typeof(string));
                    dtAttechment.Columns.Add("SCREATE", typeof(string));
                    dtAttechment.Columns.Add("DUPDATE", typeof(string));
                    dtAttechment.Columns.Add("SUPDATE", typeof(string));
                    dtAttechment.Columns.Add("cRowFlag", typeof(string));
                }
                else
                {
                    dtAttechment = (DataTable)Session["DataAttechment"];
                    foreach (DataRow drRemove in dtAttechment.Select("cRowFlag='Page' AND SFILE='" + fileInfo.Name + "'")) dtAttechment.Rows[dtAttechment.Rows.IndexOf(drRemove)].Delete();

                }
                #endregion
                DataRow drAttechment;

                drAttechment = dtAttechment.NewRow();
                drAttechment["SCHECKID"] = "";
                drAttechment["NATTACHMENT"] = dtAttechment.Rows.Count + 1;
                drAttechment["SCONTRACTID"] = "" + ArrayIssueData[1];
                drAttechment["STRUCKID"] = "" + ArrayIssueData[2];
                drAttechment["SATTACHTYPEID"] = "";
                drAttechment["SPATH"] = "" + UploadDirectory + PathUploadDirectory;
                drAttechment["SFILE"] = "" + fileInfo.Name;
                drAttechment["SSYSFILE"] = "" + fileInfo.Name;
                drAttechment["DCREATE"] = "" + Convert.ToDateTime("" + MasterData[1]).ToString("MM/dd/yyyy", new CultureInfo("en-US"));
                drAttechment["SCREATE"] = "" + Session["SVDID"];
                drAttechment["DUPDATE"] = "";
                drAttechment["SUPDATE"] = "";
                drAttechment["cRowFlag"] = "Page";

                dtAttechment.Rows.Add(drAttechment);
                Session["DataAttechment"] = dtAttechment;

            }
            #endregion

            uploadedFile.SaveAs(resFileName);

            return string.Format("{0} ({1})|{2}#{3}", fileLabel, fileLength, fileInfo.Name, (UploadDirectory + PathUploadDirectory).Remove(0, 2));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    private string UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        try
        {
            string ServerMapPath = Server.MapPath("./") + pathFile.Replace("/", "\\");
            FileInfo File = new FileInfo(ServerMapPath + ful.FileName);
            string ResultFileName = ServerMapPath + File.Name;
            string sPath = Path.GetDirectoryName(ResultFileName)
                    , sFileName = Path.GetFileNameWithoutExtension(ResultFileName)
                    , sFileType = Path.GetExtension(ResultFileName);

            if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
            {
                #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
                if (!Directory.Exists(ServerMapPath))
                {
                    Directory.CreateDirectory(ServerMapPath);
                }
                #endregion
                string fileName = (GenFileName + "" + sFileType.Trim());
                ful.SaveAs(ServerMapPath + fileName);
                string uploadmode = (System.IO.File.Exists(ServerMapPath + fileName) && pathFile.Replace("\\", "/").Split('/')[2] + "" == "Temp_Excel") ? "Excel" : "Picture";
                LogUser("1", "I", "อัพโหลดไฟล์(" + uploadmode + ") หน้ายืนยันรถตามสัญญา", "");///แก้ไขให้ระบบเก็บLoglว่าอัพโหลดExcel||Picture
                if (System.IO.File.Exists(ServerMapPath + fileName) && pathFile.Replace("\\", "/").Split('/')[2] + "" == "Temp_Excel")
                {
                    OpenExcel2DataTable(ServerMapPath, "UPLOADED", fileName);
                }
                return fileName + "$" + sFileName.Replace("$", "") + sFileType;
            }
            else
                return "$";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    protected void OpenExcel2DataTable(string path_upload, string GPSMode, string SystemFileName)
    {
        try
        {
            StringBuilder _err = new StringBuilder();
            int VisibleIndex = xgvw.EditingRowVisibleIndex;
            dynamic MasterData = xgvw.GetRowValues(VisibleIndex, "SCONTRACTID", "DDATE");
            FileInfo _fi = new FileInfo(path_upload + "/" + SystemFileName);// Declare FileInfo
            string excelConnectionString = this.GetConnectionForFileExcel(_fi);// Create Connection to Excel Workbook
            #region OpenExcel2DT
            using (OleDbConnection excelcon = new OleDbConnection(excelConnectionString))
            {
                excelcon.Open();

                // หาชื่อ sheet
                string _exxcelsheet = this.GetExcelSheet(excelcon);
                if (_exxcelsheet.Equals("")) { excelcon.Close(); _err.AppendFormat("Not found sheet({0})<br />", _fi.Name); }
                else
                {
                    //
                    OleDbCommand command = new OleDbCommand("SELECT * FROM [" + _exxcelsheet + "] WHERE [ทะเบียนหัว] <> ' '", excelcon);

                    // Find effective date
                    string _effdate_en = "";
                    string _effdate_th = "";
                    DataSet _ds = new DataSet();
                    new OleDbDataAdapter(command).Fill(_ds);
                    if (_ds.Tables.Count > 0)
                    {
                        Session["ss_GPSExcel"] = _ds.Tables[0];
                        //CheckDataTempExcel(_ds.Tables[0]);
                    }
                    else
                    {
                        Session.Remove("ss_GPSExcel");
                        Session.Remove("ss_DataGPSExcel");
                    }
                    /// Close connect file
                    excelcon.Close();

                    /// Clear object
                    _ds.Dispose();
                    command.Dispose();
                }

                excelcon.Close();
            }
            #endregion
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void BackUpData(int visibleindex, ASPxGridView xgvwCar)
    {
        try
        {
            dynamic values = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE");
            /* "IS_ARRIVED", "KM2PLANT", "HR2PLANT", "IS_ACCIDENT", "IS_REPAIR", "REMARK"*/
            /*"SCONTRACTID", "DDATE", "STRUCKID", "SHEADID","SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED", "CSEND", "CCONFIRM", "CMA", "NDAY_MA", "TCONFLST_CCONFIRM" , "DFINAL_MA", "SCARTYPENAME", "PERSONAL_CODE"*/
            DataTable dtData = PrepareDataTable("ss_data", "" + values[1], "" + values[0], "SCONTRACTID", "DDATE", "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED", "CSEND"
                , "CCONFIRM", "CMA", "NDAY_MA", "TCONFLST_CCONFIRM", "DFINAL_MA", "SCARTYPENAME", "PERSONAL_CODE");
            Session.Remove("ss_data");
            DataTable dtFileData = PrepareDataTable("ss_filedata", "" + values[1], "" + values[0], "SCONTRACTID", "DDATE", "IMP_ID", "SystemFileName", "OriginalFileName", "DOC_TYPE", "SREMARK");
            Session.Remove("ss_filedata");
            DataRow dr;
            int csend = 0;
            string SystemFileName = ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtSystemFileName")).Text
                , OriginalFileName = ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtOriginalFileName")).Text
                , GPSMode = ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtGPSMode")).Text;
            string SystemPICName = ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtSystemPicName")).Text
                , OriginalPICName = ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtOriginalPICName")).Text
                , GPSPICMode = ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtGPSPICMode")).Text;
            string REMARKS = ((ASPxMemo)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtremark")).Text;
            #region BackUp_FileData
            //INSERT DATA FILE 4 IMPORT
            DataRow drFileData = dtFileData.NewRow();
            drFileData["SCONTRACTID"] = "" + values[0];
            drFileData["DDATE"] = "" + values[1];
            drFileData["IMP_ID"] = (GPSMode.Replace("UPLAODED", "") == "") ? "" : "" + GPSMode;
            drFileData["SystemFileName"] = "" + SystemFileName;
            drFileData["OriginalFileName"] = "" + OriginalFileName;
            drFileData["DOC_TYPE"] = "XLS";
            drFileData["SREMARK"] = "" + REMARKS;
            dtFileData.Rows.Add(drFileData);
            //INSERT DATA PIC 4 IMPORT
            drFileData = dtFileData.NewRow();
            drFileData["SCONTRACTID"] = "" + values[0];
            drFileData["DDATE"] = "" + values[1];
            drFileData["IMP_ID"] = (GPSPICMode.Replace("UPLAODED", "") == "") ? "" : "" + GPSPICMode;
            drFileData["SystemFileName"] = "" + SystemPICName;
            drFileData["OriginalFileName"] = "" + OriginalPICName;
            drFileData["DOC_TYPE"] = "PIC";
            drFileData["SREMARK"] = "" + REMARKS;
            dtFileData.Rows.Add(drFileData);

            Session["ss_filedata"] = dtFileData;
            #endregion
            for (int rows = 0; rows < xgvwCar.VisibleRowCount; rows++)
            {
                ASPxTextBox txtconfirm = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["#"], "txtconfirm") as ASPxTextBox;
                ASPxTextBox txtPassed = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["ใบตรวจสภาพรถ"], "txtPassed") as ASPxTextBox;

                dynamic dataCUSTOMCALLBACK = xgvwCar.GetRowValues(rows, "SCONTRACTID", "DDATE", "STRUCKID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED"
                    , "CCONFIRM", "PERSONAL_CODE");
                string val = "" + txtconfirm.Value;
                dr = dtData.NewRow();
                dr["SCONTRACTID"] = "" + dataCUSTOMCALLBACK[0];
                dr["DDATE"] = "" + dataCUSTOMCALLBACK[1];
                dr["STRUCKID"] = "" + dataCUSTOMCALLBACK[2];
                dr["SHEADID"] = "" + dataCUSTOMCALLBACK[2];
                dr["SHEADREGISTERNO"] = "" + dataCUSTOMCALLBACK[3];
                dr["STRAILERID"] = "" + dataCUSTOMCALLBACK[4];
                dr["STRAILERREGISTERNO"] = "" + dataCUSTOMCALLBACK[5];
                dr["CSTANDBY"] = "" + dataCUSTOMCALLBACK[6];
                dr["NTOTALCAPACITY"] = "" + dataCUSTOMCALLBACK[7];
                dr["CPASSED"] = (val == "1") ? "" + txtPassed.Value : "0";//ถ้าไม่สามารถยืนยันรถได้ จะตีเป็นตรวจสถาพไม่ผ่านโดยอัตโนมัติ
                dr["CSEND"] = "" + val;
                dr["CMA"] = "0";
                dr["CCONFIRM"] = "" + dataCUSTOMCALLBACK[9];
                dr["PERSONAL_CODE"] = "" + dataCUSTOMCALLBACK[10];
                csend += ((val == "1") ? 1 : 0);
                dtData.Rows.Add(dr);
            }
            Session["ss_data"] = dtData;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    protected void ClearUploadSet(ASPxCallbackPanel __cpn, string __mode)
    {
        try
        {
            //PIC
            ASPxTextBox txtSystemPICName = (ASPxTextBox)__cpn.FindControl("txtSystemPICName");
            ASPxTextBox txtOriginalPICName = (ASPxTextBox)__cpn.FindControl("txtOriginalPICName");
            ASPxTextBox txtGPSPICMode = (ASPxTextBox)__cpn.FindControl("txtGPSPICMode");
            ASPxButton btnDelGPSSpic = (ASPxButton)__cpn.FindControl("btnDelGPSSpic");
            ASPxButton btnViewGPSpic = (ASPxButton)__cpn.FindControl("btnViewGPSpic");
            //XLS
            ASPxTextBox txtSystemFileName = (ASPxTextBox)__cpn.FindControl("txtSystemPICName");
            ASPxTextBox txtOriginalFileName = (ASPxTextBox)__cpn.FindControl("txtOriginalPICName");
            ASPxTextBox txtGPSMode = (ASPxTextBox)__cpn.FindControl("txtGPSPICMode");
            ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");
            switch (__mode)
            {
                case "PIC":
                    bool FileIsDelete = false;//CanDeleteFileFromPath(UploadGPSDirectory + "Temp_PIC", txtSystemPICName.Text);
                    txtSystemPICName.Text = txtOriginalPICName.Text = txtGPSPICMode.Text = "";
                    btnDelGPSSpic.ClientVisible = btnViewGPSpic.ClientVisible = FileIsDelete;
                    break;
                case "XLS":
                    txtSystemFileName.Text = txtOriginalFileName.Text = txtGPSMode.Text = "";
                    break;

                case "XLSPIC":

                    txtSystemFileName.Text = txtOriginalFileName.Text = txtGPSMode.Text = "";
                    txtSystemPICName.Text = txtOriginalPICName.Text = txtGPSPICMode.Text = "";
                    btnDelGPSSpic.ClientVisible = btnViewGPSpic.ClientVisible = false;
                    xgvwCar.DataSource = null;
                    xgvwCar.DataBind();
                    break;
                default: break;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    protected bool CanDeleteFileFromPath(string __path, params string[] __params)
    {
        try
        {
            bool HasFileDelete = false;
            string ServerMapPath = Server.MapPath("./") + __path.Replace("~/", "").Replace("/", "\\") + "\\";
            if (__params.Length > 0)
            {
                foreach (string FileName in __params)
                {
                    FileInfo File = new FileInfo(ServerMapPath + FileName);
                    bool HasFile = (System.IO.File.Exists(ServerMapPath + FileName) ? true : false);
                    if (HasFile)
                    {
                        File.Delete();
                        HasFileDelete = (System.IO.File.Exists(ServerMapPath + FileName) ? false : true);
                    }
                }
            }
            return HasFileDelete;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    //DataSource
    protected void sdsContract_OnDeleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsContract_OnDeleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    //CallBlackPanel
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        try
        {
            string[] paras = e.Parameter.Split(';');

            switch (paras[0])
            {
                case "search":
                    SearchData("search");
                    xgvw.CancelEdit();
                    break;
                case "checkdata":
                    string msg = "";
                    string SCONTRACTNO = string.Empty;
                    for (int nRows = 0; nRows < xgvw.VisibleRowCount; nRows++)
                    {//loop grid 's contract
                        if (xgvw.Selection.IsRowSelected(nRows))
                        {
                            dynamic griddata = xgvw.GetRowValues(nRows, "SCONTRACTID", "DDATE", "NCONFIRMID", "NTRUCK", "NCONFIRM", "CCONFIRM", "DEXPIRE", "SCONTRACTNO");
                            Session["NCONFIRMID"] = griddata;
                            SCONTRACTNO += "," + griddata[7];
                            ASPxLabel txtnTruckConfirm = xgvw.FindRowCellTemplateControl(nRows, (GridViewDataColumn)xgvw.Columns["ยืนยันรถ<br/>(รวม)"], "txtnTruckConfirmSum") as ASPxLabel;
                            if ((Convert.ToDateTime(griddata[6] + "").Date < DateTime.Now.Date))
                            {//ถ้ายังลยระยะเวลาที่กำหนดแล้ว
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('ข้อความจากระบบ','ท่านมีบางรายการสัญญาที่ <b>ไม่ยืนยันรถ</b>ได้เนื่องจากเลยระยะเวลาที่กำหนด <br /> กรุณาตรวจสอบ อีกครั้ง!')");
                                break;
                            }
                            if ("" + griddata[4] != txtnTruckConfirm.Text)
                            {
                                msg += "1";
                            }
                        }
                    }
                    //if (msg != "")
                    //{
                    //    CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','ท่านต้องการทำการ <b>ยืนยันรถ</b> ต่อใช่หรือ<b>ไม่</b>?',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('senddata');} ,function(s,e){ dxPopupConfirm.Hide(); })");
                    //}
                    //else { CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','ท่านต้องการทำการ <b>ยืนยันรถ</b> ต่อ<b>ใช่</b>หรือไม่?',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('senddata');} ,function(s,e){ dxPopupConfirm.Hide(); })"); }
                    if (!string.IsNullOrEmpty(SCONTRACTNO))
                    {
                        SCONTRACTNO = SCONTRACTNO.Remove(0, 1);
                    }
                    if (msg != "")
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','ส่งข้อมูลการยืนยันสถานะรถให้ปทต. เลขที่สัญญา " + SCONTRACTNO + "',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('ConfirmTruck');} ,function(s,e){ dxPopupConfirm.Hide(); })");
                    }
                    else { CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','ส่งข้อมูลการยืนยันสถานะรถให้ปทต. เลขที่สัญญา " + SCONTRACTNO + "',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('ConfirmTruck');} ,function(s,e){ dxPopupConfirm.Hide(); })"); }
                    break;
                case "senddata":
                    if (CanWrite)
                    {

                        //string dStart = DateTime.Now.ToString("dd/MM/yyyy");
                        string dEnd = dteEnd.Text;
                        string sVendorID = "" + Session["SVDID"];
                        string exec = "";
                        DataTable dtContract_Truck = CommonFunction.Get_Data(connection, @"SELECT rownum NLISTNO, TRCK.STRUCKID,TRCK.SHEADID ,TRCK.SHEADREGISTERNO ,TRCK.STRAILERID ,TRCK.STRAILERREGISTERNO ,NVL(TRCK.CSTATUS,'0') CSTATUS ,NVL(TRCK.CACCIDENT,'0') CACCIDENT 
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END CHOLD
,CONT_TRCK.SCONTRACTID ,CONT.SCONTRACTNO ,CONT_TRCK.CSTANDBY ,CONT_TRCK.CREJECT ,CONT_TRCK.DSTART ,CONT_TRCK.DEND
 FROm TCONTRACT CONT
LEFT JOIN TCONTRACT_TRUCK CONT_TRCK ON CONT_TRCK.SCONTRACTID=CONT.SCONTRACTID
 LEFT JOIN (
        SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME 
        ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
        FROM TTRUCK 
        LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
        LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
        WHERE CACTIVE='Y' 
)TRCK ON CONT_TRCK.STRUCKID=TRCK.STRUCKID 
 WHERE CONT.CACTIVE='Y' AND NVL(CONT.NTRUCK,'0')<>'0'  AND  CONT.SVENDORID='" + sVendorID + @"'  
 AND TRCK.CACTIVE='Y' AND NVL(CSTANDBY,'N') ='N' ");
                        TTRUCKCONFIRMLIST conf_list = new TTRUCKCONFIRMLIST(connection);
                        if (Session["ss_data"] == null)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('ข้อความจากระบบ ','ระบบไม่สามารถยืนยันรถให้ท่านได้ เนื่องจากท่านยังไม่ได้อัพโหลดรายการยืนยันรถ/ข้อมูลGPS!')"); return;
                        }
                        else
                        {
                            DataTable dt_data = (DataTable)Session["ss_data"];
                            DataTable dt_filedata = (DataTable)Session["ss_filedata"];
                            #region Send
                            string msgUnSend = "";
                            for (int nGridRows = 0; nGridRows < xgvw.VisibleRowCount; nGridRows++)
                            {//loop grid 's contract
                                if (xgvw.Selection.IsRowSelected(nGridRows))
                                {
                                    dynamic griddata = xgvw.GetRowValues(nGridRows, "SCONTRACTID", "DDATE", "NCONFIRMID", "DEXPIRE", "SCONTRACTNO");
                                    bool IsB4DeadLine = true;
                                    if ((Convert.ToDateTime(griddata[3] + "") < DateTime.Now))
                                    {
                                        IsB4DeadLine = false;
                                        //break;//ถ้าเลยระยะเวลาที่กำหนดแล้ว
                                    }
                                    conf_list.NCONFIRMID = "" + griddata[2];
                                    DataRow[] drdata = dt_data.Select("SCONTRACTID='" + griddata[0] + "' AND DDATE='" + griddata[1].ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'");
                                    if (drdata.Length > 0)
                                    {
                                        #region FILE&PIC
                                        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                        {
                                            #region Disable Old DOC
                                            OracleCommand Disable_DOC_Cmd = new OracleCommand();
                                            Disable_DOC_Cmd.Connection = OraConnection;
                                            if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();
                                            Disable_DOC_Cmd.CommandType = CommandType.Text;
                                            Disable_DOC_Cmd.CommandText = @"UPDATE TTRUCKCONFIRM_DOC SET CACTIVE='0' WHERE SCONTRACTID='" + griddata[0] + "' AND DDATE=TO_DATE('" + griddata[1].ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') ";
                                            Disable_DOC_Cmd.ExecuteNonQuery();
                                            #endregion
                                            #region Add New DOC
                                            OracleCommand INS_DOC_Cmd = new OracleCommand();
                                            INS_DOC_Cmd.Connection = OraConnection;
                                            if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();
                                            INS_DOC_Cmd.CommandType = CommandType.Text;
                                            foreach (string gps_type in ArrayGPSType)
                                            {
                                                if (dt_filedata != null)
                                                {
                                                    foreach (DataRow drfiledata in dt_filedata.Select("SCONTRACTID='" + griddata[0] + "' AND DDATE='" + griddata[1] + "' AND DOC_TYPE='" + gps_type + "'"))
                                                    {
                                                        INS_DOC_Cmd.CommandText = @"INSERT INTO TTRUCKCONFIRM_DOC (DOC_ID, SCONTRACTID, DDATE, CACTIVE, DCREATE, DUPDATE, DOC_TYPE, DOC_SYSNAME, DOC_ORGNAME, SREMARK) 
VALUES (  FC_GENID_TTRUCKCONFIRM_DOC('') ,'" + griddata[0] + "' , TO_DATE('" + griddata[1].ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') ,'1' ,SYSDATE ,SYSDATE  ,'" + gps_type + "' ,'" + drfiledata["SystemFileName"] + "' ,'" + drfiledata["OriginalFileName"] + "' ,'" + drfiledata["SREMARK"] + "' )";
                                                        INS_DOC_Cmd.ExecuteNonQuery();
                                                    }
                                                }

                                            }
                                            #endregion
                                        }
                                        #endregion
                                        #region TRUCK
                                        int nrows = 0;
                                        foreach (DataRow dr_data in drdata)
                                        {// "SCONTRACTID", "DDATE", "STRUCKID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED", "CSEND"
                                            conf_list.NLISTNO = "";
                                            conf_list.STRUCKID = "" + dr_data["STRUCKID"];
                                            conf_list.SHEADID = "" + dr_data["STRUCKID"];
                                            conf_list.SHEADREGISTERNO = "" + dr_data["SHEADREGISTERNO"];
                                            conf_list.STRAILERID = "" + dr_data["STRAILERID"];
                                            conf_list.STRAILERREGISTERNO = "" + dr_data["STRAILERREGISTERNO"];
                                            conf_list.CCONFIRM = "" + dr_data["CSEND"];
                                            conf_list.CPASSED = "" + dr_data["CPASSED"];
                                            conf_list.CSTANBY = "" + dr_data["CSTANBY"];
                                            conf_list.SCREATE = "" + Session["UserID"]; ;
                                            conf_list.SUPDATE = "" + Session["UserID"];
                                            conf_list.SPERSONALCODE = "" + dr_data["PERSONAL_CODE"];
                                            //return;
                                            conf_list.Insert();
                                            nrows++;
                                        }
                                        #endregion
                                        if (nrows > 0)
                                        {
                                            conf_list.CommitChild();
                                            conf_list.CommitConfirmAmount(conf_list.NCONFIRMID, nrows, IsB4DeadLine);
                                            LogUser("1", "I", "ยืนยันรถตามสัญญา" + griddata[4] + " ผ่าน " + griddata[1].ToString("dd/MM/yyyy", new CultureInfo("en-US")), "");
                                        }
                                        else
                                        {
                                            msgUnSend += "<br>- " + griddata[4];
                                            LogUser("1", "I", "ยืนยันรถตามสัญญา" + griddata[4] + " ไม่ผ่าน " + griddata[1].ToString("dd/MM/yyyy", new CultureInfo("en-US")), "");
                                        }
                                    }
                                }
                            }
                            #endregion
                            if (msgUnSend != "")
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('ข้อความจากระบบ ','ท่านได้มีรายการยืนยันตามสัญญาไม่ผ่านดังนี้" + msgUnSend + "<br>กรุณาตรวจสอบหรือทำรายการใหม่อีกครั้ง!')"); return;
                            }
                        }
                        SearchData("search");
                        //ต้องเคลียร์รายการที่เลือกไว้ด้วย
                        //LogUser("1", "I", "ยืนยันรถตามสัญญา", "");
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }

                    break;
                case "ContractDetail":
                    int index111 = int.Parse(paras[1]);
                    int data = Convert.ToInt32(xgvw.GetRowValues(index111, "NPLANID"));

                    string strsql = "SELECT * FROM TCONTRACT ";

                    DataTable dt = new DataTable();
                    dt = CommonFunction.Get_Data(connection, strsql);

                    if (dt.Rows.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();

                        //sb.Append("<table width='100%' border='0' cellspacing='2' cellpadding='5'>");
                        //string formatPopup = @" <tr><td align='left'> ที่ : {0}   เลข Outbound : {1} ชื่อลูกค้า : {2}  <br />ปริมาณน้ำมัน : {3} ลิตร   จังหวัด : {4}</td></tr>";
                        //int i = 1;
                        //foreach (DataRow dr in dt.Rows)
                        //{
                        //    sb.Append(string.Format(formatPopup, i, dr["SDELIVERYNO"] + "", dr["CUST_NAME"] + "", dr["NVALUE"] + "", dr["PROVINCE"] + ""));
                        //    i++;
                        //}
                        //sb.Append("<tr><td  align='left'>หมายเหตุ : " + dt.Rows[0]["SCOMMENT"] + "" + "</td></tr>");
                        //sb.Append("</table>");

                        ((Literal)popupControl.FindControl("ltrContent")).Text = sb.ToString();
                        popupControl.ShowOnPageLoad = true;
                    }

                    break;
                case "ConfirmTruck":
                    decimal NConfirmID = ((dynamic)Session["NCONFIRMID"])[2];
                    //var NConfirmID = xgvw.GetRowValues(int.Parse(paras[1]), "NCONFIRMID");
                    Session.Remove("NConfirmID");
                    ContractConfirmBLL.Instance.UpdateConfirmTruckBLL(NConfirmID.ToString(), "1");
                    //alertSuccess("บันทึกข้อมูลเรียบร้อย");
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ส่งข้อมูลให้ ปตท.เรียบร้อย<br/>" + "',function(){ dxPopupInfo.Hide(); });");
                    SearchData(string.Empty);
                    break;
                case "CONFIRMSAVE":

                    DataTable dtContract = ContractConfirmBLL.Instance.SelectTruckConfirmBLL(Session["UserID"].ToString(), dteEnd.Text + " 00:00:00", dteEnd.Text + " 23:59:59", txtKeyword.Text + "", cmbStatus.SelectedValue + "");
                    dynamic datas = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "SCONTRACTNO");

                    string SCONTRACTID = datas[0] + string.Empty;
                    string SCONTRACTNO2 = datas[1] + string.Empty;
                    DataRow[] drs = dtContract.Select("SCONTRACTID = " + SCONTRACTID);
                    string mess = "บันทึกข้อมูล การยืนยันสถานะรถ เลขที่สัญญา " + SCONTRACTNO2;
                    if (drs.Any(it => !string.IsNullOrEmpty(it["NAVAILABLE"] + string.Empty)))
                    {
                        mess = "ระบบมีการบันทึกการส่งรถก่อนหน้านี้ท่านต้องการอัพเดทข้อมูลสถานะการยืนยันรถอีกครั้งใช่หรือไม่";
                    }
                    CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','" + mess + "',function(s,e){ dxPopupConfirm.Hide(); xgvw.PerformCallback('" + paras[1] + "');} ,function(s,e){ dxPopupConfirm.Hide(); })");
                    ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");

                    SetHeaderColumnxgvwCar(xgvwCar, null, null, null, null, null, null, null);
                    ASPxButton btnsubmit = (ASPxButton)xgvw.FindEditFormTemplateControl("btnsubmit");
                    if (btnsubmit != null)
                    {
                        btnsubmit.Enabled = true;
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void xcpnGPS_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        ASPxCallbackPanel xcpnGPS = ((ASPxCallbackPanel)sender);
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "deletepic":
                //waiting del PIC
                ClearUploadSet(xcpnGPS, "PIC");
                break;
            case "GPSCOMFIRM":
                break;
            case "GPSCANCEL":
                ClearUploadSet(xcpnGPS, "XLSPIC");
                break;
        }
    }
    //GridViewMaster
    protected void xgvw_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        try
        {
            if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    #region xgvw_AfterPerformCallback
    protected void xgvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        try
        {
            ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");
            if (xgvwCar == null) CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('ข้อความจากระบบ','ระบบไม่สามารถแสดงรายการ รถ <br>ได้เนื่องจากการทำงานที่ผิดพลาดหรือนานเกินไป กรุณาตรวจสอบ อีกครั้ง!')");

            //ASPxButton btnsubmit = xgvw.FindEditFormTemplateControl("btnsubmit") as ASPxButton;
            //btnsubmit.ClientVisible = true;

            int visibleindex = 0;
            string CallbackName = (e.CallbackName.Equals("STARTEDIT") || e.CallbackName.Equals("CANCELEDIT") || e.CallbackName.Equals("SORT")) ? e.CallbackName : e.Args[0].ToUpper();//switch CallbackName 
            Label lblError = (Label)xgvw.FindEditFormTemplateControl("lblError");
            switch (e.CallbackName)
            {
                case "SORT":
                case "CANCELEDIT":
                    xgvw.CancelEdit();
                    SearchData("CANCELEDIT");//xgvw.DataBind();
                    break;
                case "STARTEDIT":
                    //btnsubmit.ClientVisible = false;
                    visibleindex = xgvw.EditingRowVisibleIndex;
                    Session.Remove("DataTcontractTruck");
                    dynamic dy_data = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE", "PTT_LOCK");
                    dynamic dataContract = xgvw.GetRowValues(visibleindex, "SCONTRACTNO");
                    if (Session["ss_data"] != null)
                    {
                        #region File GPS
                        if (Session["ss_filedata"] != null)
                        {
                            DataTable dt_filedata = (DataTable)Session["ss_filedata"];
                            if (dt_filedata.Rows.Count > 0)
                            {
                                foreach (string doc_type in ArrayGPSType)
                                {
                                    DataRow[] drfileHasRows = dt_filedata.Select("SCONTRACTID='" + dy_data[0] + "' AND DDATE='" + dy_data[1] + "' AND DOC_TYPE='" + doc_type + "'");
                                    if (drfileHasRows.Length > 0)
                                    {
                                        ((ASPxButton)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("btnDelGPSSpic")).ClientVisible = ((ASPxButton)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("btnViewGPSpic")).ClientVisible = (drfileHasRows[0]["SystemFileName"] != "" && doc_type == "PIC") ? true : false;

                                        ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtSystem" + ((doc_type == "XLS") ? "File" : "PIC") + "Name")).Text = "" + drfileHasRows[0]["SystemFileName"];
                                        ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtOriginal" + ((doc_type == "XLS") ? "File" : "PIC") + "Name")).Text = "" + drfileHasRows[0]["OriginalFileName"];
                                        ((ASPxTextBox)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtGPS" + ((doc_type == "XLS") ? "" : "PIC") + "Mode")).Text = "" + drfileHasRows[0]["IMP_ID"];
                                    }
                                }
                                ((ASPxMemo)xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("txtremark")).Text = "" + dt_filedata.Rows[0]["SREMARK"];
                            }
                        }
                        #endregion
                        DataTable dt_data = (DataTable)Session["ss_data"];
                        DataRow[] drHasRows = dt_data.Select("SCONTRACTID='" + dy_data[0] + "' AND DDATE='" + dy_data[1].ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' ");
                        if (drHasRows.Length > 0)
                        {//ถ้า selectเจอเท่ากับเคยมีการเพิ่มหรือแก้ไขให้เอามาใช้เรย
                            DataTable dt_DataGPSExcel = CommonFunction.ArrayDataRowToDataTable(dt_data, drHasRows);
                            BindTruckWithDataFromExcel(xgvwCar, dt_DataGPSExcel, "STARTEDIT");
                        }
                        else
                        {
                            BindTruckWithDataInGridEditing(xgvwCar, "STARTEDIT");
                        }
                    }
                    else
                    {
                        BindTruckWithDataInGridEditing(xgvwCar, "STARTEDIT");
                    }
                    if (Session["DataTcontractTruck"] != null)
                    {
                        DataTable dt = (DataTable)Session["DataTcontractTruck"];
                        if (dt.Select("IVMS_GPS_STAT = '1' AND IVMS_MDVR_STAT = '1'").Any())
                        {
                            ASPxButton btnsubmit = (ASPxButton)xgvw.FindEditFormTemplateControl("btnsubmit");
                            ASPxButton btnIVMSCheck = (ASPxButton)xgvw.FindEditFormTemplateControl("btnIVMSCheck");
                            if (btnsubmit != null && btnIVMSCheck != null)
                            {
                                if (dy_data[2] + string.Empty == "1" || Convert.ToDateTime(dy_data[1]) < DateTime.Today.AddDays(-1))
                                {
                                    btnsubmit.Enabled = false;
                                    btnIVMSCheck.Enabled = false;
                                    lblError.Text = "ไม่สามารถแก้ไขข้อมูลการยืนยันรถในสัญญากรุณาติดต่อพนักงาน ปตท.";
                                    if (Convert.ToDateTime(dy_data[1]) < DateTime.Today.AddDays(-1))
                                    {
                                        lblError.Text = "หมดเวลายืนยันรถแล้ว !!!";
                                    }

                                }
                                else
                                {
                                    btnIVMSCheck.Enabled = true;
                                    btnsubmit.Enabled = true;
                                }

                            }
                            if (string.Equals(dataContract, "PTTOR/ขปธ./พ.1/6/2562") || string.Equals(dataContract, "PTTOR/ขปธ./พ.1/3/2562") || string.Equals(dataContract, "PTTOR/ขปธ./พ.1/4/2562") || string.Equals(dataContract, "PTTOR/ขปธ./พ.1/5/2562") || string.Equals(dataContract, "PTTOR/ขปธ./พ.1/2/2562"))
                            {
                                btnsubmit.Enabled = true;
                            }
                        }
                    }

                    SetHeaderColumnxgvwCar(xgvwCar, null, null, null, null, null, null, null);
                    break;
                case "CUSTOMCALLBACK":
                    #region CUSTOMCALLBACK
                    //ถ้าไม่มีกริดรถ
                    if (xgvwCar == null) break;
                    #region GetHerderControl
                    CheckBoxList cblDeliveryNumHeader = (CheckBoxList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["DELIVERY_NUM"], "cblDeliveryNumHeader");
                    DropDownList ddlCarTypeHeader = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["CARTYPE"], "ddlCarTypeHeader");
                    CheckBox cbSelectHeader = (CheckBox)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["IS_SELECT"], "cbSelectHeader");
                    //DropDownList ddlGpsStatusHeader = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["GPS_STATUS"], "ddlGpsStatusHeader");
                    //DropDownList ddlCctvStatusHeader = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["CCTV_STATUS"], "ddlCctvStatusHeader");
                    #endregion

                    if (CallbackName.ToUpper() == "REFRESH")
                    {
                        //btnsubmit.ClientVisible = btnsubmitVisible;
                        BindTruckWithDataFromExcel(xgvwCar, (DataTable)Session["ss_DataGPSExcel"], "STARTEDIT");
                        SetHeaderColumnxgvwCar(xgvwCar, ddlCarTypeHeader, cblDeliveryNumHeader, null, null, null, null, cbSelectHeader);
                        break;
                    }
                    if (CallbackName.ToUpper() == "IVMSCHECK")
                    {


                        CheckIvms(xgvwCar);
                        SetHeaderColumnxgvwCar(xgvwCar, ddlCarTypeHeader, cblDeliveryNumHeader, null, null, null, null, cbSelectHeader);
                        dynamic PTT_LOCK = xgvw.GetRowValues(visibleindex, "PTT_LOCK");
                        ASPxButton btnsubmit = (ASPxButton)xgvw.FindEditFormTemplateControl("btnsubmit");
                        //if (btnsubmit != null)
                        //{
                        //    if (PTT_LOCK + string.Empty == "1")
                        //    {
                        //        btnsubmit.Enabled = false;

                        //        lblError.Text = "LOCK การยืนยันรถโดย PTT ไม่สามารถยืนยันรถใหม่ได้ กณุณาติดต่อ PTT";
                        //    }
                        //    else
                        //    {
                        //        btnsubmit.Enabled = true;
                        //    }
                        //} 
                        btnsubmit.Enabled = true;
                        break;
                    }

                    visibleindex = int.Parse(e.Args.GetValue(0).ToString());

                    //xgvw.FindVisibleIndexByKeyValue(e.Args.GetValue(0));

                    BackUpData(visibleindex, xgvwCar);
                    string strError = string.Empty;
                    bool isRes = Confirm(visibleindex, xgvwCar, ref strError);
                    if (isRes)
                    {
                        xgvw.CancelEdit();
                        SearchData("");
                    }
                    else
                    {
                        lblError.Text = strError;
                    }
                    SetHeaderColumnxgvwCar(xgvwCar, ddlCarTypeHeader, cblDeliveryNumHeader, null, null, null, null, cbSelectHeader);
                    #endregion
                    break;
                case "Refresh":

                    break;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region GetHeaderColumnxgvwCar
    private void GetHeaderColumnxgvwCar(ASPxGridView xgvwCar, CheckBoxList cblDeliveryNumHeader)
    {

    }
    #endregion

    #region SetHeaderColumnxgvwCar
    private void SetHeaderColumnxgvwCar(ASPxGridView xgvwCar, DropDownList ddlCarTypeHeader, CheckBoxList cblDeliveryNumHeader, DropDownList ddlCarConfirmStatusHeader, DropDownList ddlWareHouseHeader, DropDownList ddlGpsStatusHeader, DropDownList ddlCctvStatusHeader, CheckBox cbSelectHeader)
    {
        #region GetHerderControl Old
        CheckBoxList cblDeliveryNumHeaderOld = (CheckBoxList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["DELIVERY_NUM"], "cblDeliveryNumHeader");
        DropDownList ddlCarTypeHeaderOld = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["CARTYPE"], "ddlCarTypeHeader");
        DropDownList ddlCarConfirmStatusHeaderOld = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["CAR_STATUS_ID"], "ddlCarConfirmStatusHeader");
        DropDownList ddlWareHouseHeaderOld = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["STERMINALID"], "ddlWareHouseHeader");
        CheckBox cbSelectHeaderOld = (CheckBox)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["IS_SELECT"], "cbSelectHeader");
        //DropDownList ddlGpsStatusHeaderOld = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["GPS_STATUS"], "ddlGpsStatusHeader");
        //DropDownList ddlCctvStatusHeaderOld = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["CCTV_STATUS"], "ddlCctvStatusHeader");
        #endregion

        #region ddlCarTypeHeader
        if (ddlCarTypeHeader != null)
        {
            ddlCarTypeHeaderOld.Items.Clear();
            foreach (ListItem item in ddlCarTypeHeader.Items)
            {
                ddlCarTypeHeaderOld.Items.Add(item);
            }
        }
        #endregion

        #region ddlCarConfirmStatusHeader
        if (ddlCarConfirmStatusHeader != null)
        {
            ddlCarConfirmStatusHeaderOld.Items.Clear();
            foreach (ListItem item in ddlCarConfirmStatusHeader.Items)
            {
                ddlCarConfirmStatusHeaderOld.Items.Add(item);
            }
        }
        else
        {
            ddlCarConfirmStatusHeaderOld.Items.Clear();
            ddlCarConfirmStatusHeaderOld.Items.Add(new ListItem()
            {
                Text = "--เลือก--",
            });
            ddlCarConfirmStatusHeaderOld.Items.Add(new ListItem()
            {
                Text = "ไม่เลือก",
                Value = "-1"
            });
            foreach (DataRow item in ((DataTable)Session["DataMcarConfirmStatus"]).Rows)
            {
                ddlCarConfirmStatusHeaderOld.Items.Add(new ListItem()
                {
                    Text = item["CAR_STATUS_NAME"] + string.Empty,
                    Value = item["CAR_STATUS_ID"] + string.Empty
                });
            }
        }
        #endregion

        #region cblDeliveryNumHeader

        if (cblDeliveryNumHeader != null)
        {
            cblDeliveryNumHeaderOld.Items.Clear();
            foreach (ListItem item in cblDeliveryNumHeader.Items)
            {
                cblDeliveryNumHeaderOld.Items.Add(item);
            }
        }
        foreach (ListItem item in cblDeliveryNumHeaderOld.Items)
        {
            item.Attributes.Add("onchange", "ddlOnChange($(this).val(),-1,\"DELIVERY_NUMHEADER\");");
        }
        #endregion

        if (cbSelectHeaderOld != null)
        {
            cbSelectHeaderOld.InputAttributes.Add("onchange", "ddlOnChange($(this).is(':checked'),-1,\"IS_SELECTHEADER\");");
        }
        if (cbSelectHeader != null)
        {
            cbSelectHeaderOld.Checked = cbSelectHeader.Checked;
        }

        #region ddlWareHouseHeader
        if (ddlWareHouseHeader != null)
        {
            ddlWareHouseHeaderOld.Items.Clear();
            foreach (ListItem item in ddlWareHouseHeader.Items)
            {
                ddlWareHouseHeaderOld.Items.Add(item);
            }
        }
        else
        {
            ddlWareHouseHeaderOld.Items.Clear();
            ddlWareHouseHeaderOld.Items.Add(new ListItem()
            {
                Text = "--เลือก--",
            });
            ddlWareHouseHeaderOld.Items.Add(new ListItem()
            {
                Text = "ไม่เลือก",
                Value = "-1"
            });
            foreach (DataRow item in ((DataTable)Session["DataTTERMINAL"]).Rows)
            {
                ddlWareHouseHeaderOld.Items.Add(new ListItem()
                {
                    Text = item["SABBREVIATION"] + string.Empty,
                    Value = item["STERMINALID"] + string.Empty
                });
            }

        }
        #endregion

        //#region ddlGpsStatusHeader
        //if (ddlGpsStatusHeader != null)
        //{
        //    ddlGpsStatusHeaderOld.Items.Clear();
        //    foreach (ListItem item in ddlGpsStatusHeader.Items)
        //    {
        //        ddlGpsStatusHeaderOld.Items.Add(item);
        //    }
        //}
        //#endregion

        //#region ddlCctvStatusHeader
        //if (ddlCctvStatusHeader != null)
        //{
        //    ddlCctvStatusHeaderOld.Items.Clear();
        //    foreach (ListItem item in ddlCctvStatusHeader.Items)
        //    {
        //        ddlCctvStatusHeaderOld.Items.Add(item);
        //    }
        //}
        //#endregion
    }
    #endregion


    protected void xgvw_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            switch (e.RowType.ToString().ToLower())
            {
                case "data":

                    if ((("" + e.GetValue("CCONFIRM")).Equals("0") || string.IsNullOrEmpty("" + e.GetValue("CCONFIRM"))) && (Convert.ToDateTime("" + e.GetValue("DEXPIRE")).Date < DateTime.Now.Date))
                    {
                        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
                    }
                    else if (("" + e.GetValue("CCONFIRM")).Equals("1"))
                    { //CEECF5
                        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#CEECF5");
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    protected void xgvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            if (e.DataColumn.Caption == "พร้อมใช้งาน")
            {
                ASPxTextBox txtnTruckConfirm = xgvw.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)xgvw.Columns["พร้อมใช้งาน"], "txtnTruckConfirm") as ASPxTextBox;
                int NCONFIRM = 0;
                if (Session["ss_data"] != null)
                {
                    NCONFIRM = ((DataTable)Session["ss_data"]).Select("CSEND='1'  AND DDATE='" + ((DateTime)e.GetValue("DDATE")).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND SCONTRACTID='" + e.GetValue("SCONTRACTID") + "' ").Length;
                    txtnTruckConfirm.Value = (NCONFIRM <= 0) ? "" + e.GetValue("NCONFIRM") : "" + NCONFIRM;
                    txtnTruckConfirm.Attributes.Add("title", NCONFIRM + "/" + e.GetValue("NONHAND"));
                }
                else
                {
                    txtnTruckConfirm.Value = "" + e.GetValue("NCONFIRM");
                }
            }
            else if (e.DataColumn.Caption == "รวมยืนยัน")
            {

            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    //GridViewParent
    void xgvwCar_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "") e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void xgvwCar_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            ASPxGridView xgvwCar = sender as ASPxGridView;
            bool IsExpire = (DateTime.Now.Date > Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DEXPIRE").ToString()).Date) ? true : false;
            bool IsNotEdit = (DateTime.Now.Date > Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DEXPIRE").ToString()).Date/*.AddDays(-1)*/) ? true : false;
            /*
            ASPxButton btnGPSconfirm = xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("btnGPSconfirm") as ASPxButton;
            ASPxButton btnGPScancel = xgvw.FindEditFormTemplateControl("xcpnGPS").FindControl("btnGPScancel") as ASPxButton;
            btnGPSconfirm.ClientVisible = btnGPScancel.ClientVisible = false;
            */
            if (e.DataColumn.Name == "NO")
            {
                e.Cell.Text = string.Format("{0}.", e.VisibleIndex + 1);
            }
            else if (e.DataColumn.Name == "CCONFIRM")
            {
                #region Set&event column #

                //int VisibleIndex = e.VisibleIndex;
                //ASPxButton imbconfirm = xgvwCar.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "imbconfirm") as ASPxButton;
                //ASPxButton imbcancel = xgvwCar.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "imbcancel") as ASPxButton;
                //ASPxTextBox txtconfirm = xgvwCar.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtconfirm") as ASPxTextBox;
                //if (IsExpire || IsNotEdit)
                //{//ถ้ายืนยันรถ+เกินเวลาที่กำหนด แล้ว
                //    #region display mode
                //    imbconfirm.ClientEnabled = false;
                //    imbcancel.ClientEnabled = false;
                //    //ASPxButton btnsubmit = xgvw.FindEditFormTemplateControl("btnsubmit") as ASPxButton;
                //    //btnsubmit.ClientVisible = false;
                //    //btnsubmitVisible = false;
                //    #endregion
                //}
                //else
                //{
                //    #region comfirm mode
                //    if (e.GetValue("CMA").ToString().Equals("2"))
                //    {//ถ้าห้ามวิ่ง
                //        imbconfirm.ClientEnabled = false;
                //        imbcancel.ClientEnabled = false;
                //        txtconfirm.Value = "0";
                //    }
                //    else if ((e.GetValue("CMA").ToString().Equals("1") && !e.GetValue("DFINAL_MA").ToString().Equals("")))
                //    {//ถ้าต้องแก้ไขภายใน nวัน
                //        DateTime DFINAL_MA = Convert.ToDateTime(e.GetValue("DFINAL_MA").ToString());
                //        if (DFINAL_MA < DateTime.Now)
                //        {//ถ้าวันสิ้นสุดการแก้ไขเลยระยะเวลาๆปแล้ว
                //            imbconfirm.ClientEnabled = false;
                //            imbcancel.ClientEnabled = false;
                //            txtconfirm.Value = "0";
                //        }
                //        else
                //        {
                //            imbconfirm.ClientEnabled = !true;
                //            imbcancel.ClientEnabled = true;
                //            txtconfirm.Value = "1";
                //            #region AddEvent
                //            {
                //                ASPxTextBox txtnTruckConfirm = xgvw.FindRowCellTemplateControl(xgvw.EditingRowVisibleIndex, (GridViewDataColumn)xgvw.Columns["ยืนยันแล้ว"], "txtnTruckConfirm") as ASPxTextBox;

                //                string txtnTruckConfirmID = txtnTruckConfirm.ClientID + "_I";
                //                string sPlus = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) -1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "',''); ";
                //                string sDiff = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) +1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "','update');";
                //                //config ClientInstanceName
                //                txtconfirm.ClientInstanceName = txtconfirm.ClientInstanceName + "_" + VisibleIndex;
                //                imbconfirm.ClientInstanceName = imbconfirm.ClientInstanceName + "_" + VisibleIndex;
                //                imbcancel.ClientInstanceName = imbcancel.ClientInstanceName + "_" + VisibleIndex;

                //                //Add Event
                //                imbcancel.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('0');  s.SetEnabled(false); " + imbconfirm.ClientInstanceName + ".SetEnabled(true); " + sPlus + " }";
                //                imbconfirm.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true); " + sDiff + " }";

                //                bool IsEnabled = ("" + txtconfirm.Value == "1") ? true : false;
                //                //BindingData
                //                if (Session["ss_data"] != null)
                //                {
                //                    DataTable dtss_data = ((DataTable)Session["ss_data"]);
                //                    DataRow[] drSect = dtss_data.Select("SCONTRACTID='" + e.GetValue("SCONTRACTID").ToString() + "' AND DDATE='" + e.GetValue("DDATE").ToString() + "' AND STRUCKID='" + e.GetValue("STRUCKID").ToString() + "'");
                //                    if (drSect.Length > 0)
                //                    {
                //                        txtconfirm.Value = ("" + drSect[0]["CSEND"] == "0") ? "0" : "1";
                //                        IsEnabled = ("" + txtconfirm.Value.ToString() == "1") ? true : false;
                //                        //imbconfirm.ClientEnabled = !IsEnabled;
                //                        //imbcancel.ClientEnabled = IsEnabled;
                //                    }

                //                }
                //                imbconfirm.ClientEnabled = !IsEnabled;
                //                imbcancel.ClientEnabled = IsEnabled;


                //            }
                //            #endregion
                //        }
                //    }
                //    else
                //    {
                //        ASPxLabel txtnTruckConfirm = xgvw.FindRowCellTemplateControl(xgvw.EditingRowVisibleIndex, (GridViewDataColumn)xgvw.Columns["ยืนยันรถ<br/>(รวม)"], "txtnTruckConfirmSum") as ASPxLabel;

                //        string txtnTruckConfirmID = txtnTruckConfirm.ClientID + "_I";
                //        string sPlus = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) -1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "',''); ";
                //        string sDiff = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) +1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "','update');";
                //        //config ClientInstanceName
                //        txtconfirm.ClientInstanceName = txtconfirm.ClientInstanceName + "_" + VisibleIndex;
                //        imbconfirm.ClientInstanceName = imbconfirm.ClientInstanceName + "_" + VisibleIndex;
                //        imbcancel.ClientInstanceName = imbcancel.ClientInstanceName + "_" + VisibleIndex;

                //        //Add Event
                //        imbcancel.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('0');  s.SetEnabled(false); " + imbconfirm.ClientInstanceName + ".SetEnabled(true); " + sPlus + " }";
                //        imbconfirm.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true); " + sDiff + " }";

                //        bool IsEnabled = ("" + txtconfirm.Value == "1") ? true : false;
                //        //BindingData
                //        if (Session["ss_data"] != null)
                //        {
                //            DataTable dtss_data = ((DataTable)Session["ss_data"]);
                //            DataRow[] drSect = dtss_data.Select("SCONTRACTID='" + e.GetValue("SCONTRACTID").ToString() + "' AND DDATE='" + e.GetValue("DDATE").ToString() + "' AND STRUCKID='" + e.GetValue("STRUCKID").ToString() + "'");
                //            if (drSect.Length > 0)
                //            {
                //                txtconfirm.Value = ("" + drSect[0]["CSEND"] == "0") ? "0" : "1";
                //                IsEnabled = ("" + txtconfirm.Value.ToString() == "1") ? true : false;
                //                //imbconfirm.ClientEnabled = !IsEnabled;
                //                //imbcancel.ClientEnabled = IsEnabled;
                //            }

                //        }
                //        imbconfirm.ClientEnabled = !IsEnabled;
                //        imbcancel.ClientEnabled = IsEnabled;


                //    }
                //    #endregion
                //}
                #endregion
            }

            else if (e.DataColumn.FieldName == "CSTANBY")
            {
                DropDownList ddlCarType = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "ddlCarType") as DropDownList;
                if (ddlCarType != null && string.IsNullOrEmpty(ddlCarType.SelectedValue + string.Empty))
                {
                    ddlCarType.SelectedValue = e.CellValue + string.Empty;
                }
            }
            else if (e.DataColumn.FieldName == "DELIVERY_PART")
            {
                DropDownList ddlDeliveryPart = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "ddlDeliveryPart") as DropDownList;

                DropDownList ddlCarType = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, null, "ddlCarType") as DropDownList;
                if (ddlCarType != null)
                {
                    ddlDeliveryPart.Items.Clear();
                    if (ddlCarType.SelectedValue == "Y")
                    {
                        ddlDeliveryPart.Items.Add(new ListItem()
                        {
                            Text = "--เลือก--",
                        });
                        ddlDeliveryPart.Items.Add(new ListItem()
                        {
                            Text = "ไม่ระบุ",
                            Value = "ไม่ระบุ"
                        });
                    }
                    else if (ddlCarType.SelectedValue == "N")
                    {
                        ddlDeliveryPart.Items.Add(new ListItem()
                        {
                            Text = "--เลือก--",
                        });
                        ddlDeliveryPart.Items.Add(new ListItem()
                        {
                            Text = "กลางวัน",
                            Value = "กลางวัน"
                        });
                        ddlDeliveryPart.Items.Add(new ListItem()
                        {
                            Text = "กลางคืน",
                            Value = "กลางคืน"
                        });
                    }
                    else
                    {
                        ddlDeliveryPart.Items.Add(new ListItem()
                        {
                            Text = "--เลือก--",
                        });
                    }
                }
                if (ddlDeliveryPart != null)
                {
                    ddlDeliveryPart.SelectedValue = e.CellValue + string.Empty;
                }
            }
            else if (e.DataColumn.FieldName == "CAR_STATUS_ID")
            {
                DropDownList ddlCarConfirmStatus = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "ddlCarConfirmStatus") as DropDownList;
                if (ddlCarConfirmStatus != null)
                {
                    var CMA = xgvwCar.GetRowValues(e.VisibleIndex, "CMA");
                    string sCMA = CMA + string.Empty;
                    if (sCMA != "4")//เฉพาะรถที่ไม่ใช้งาน
                    {
                        ddlCarConfirmStatus.Items.Clear();
                        ddlCarConfirmStatus.Items.Insert(0, new ListItem()
                        {
                            Text = "--เลือก--",
                        });
                        foreach (DataRow item in ((DataTable)Session["DataMcarConfirmStatus"]).Select("CAR_STATUS_ID <> '1' AND CAR_STATUS_ID <> '2' AND CAR_STATUS_ID <> '3'"))
                        {
                            ddlCarConfirmStatus.Items.Add(new ListItem()
                            {
                                Text = item[1] + string.Empty,
                                Value = item[0] + string.Empty
                            });
                        }
                        ddlCarConfirmStatus.Enabled = false;
                        switch (sCMA)
                        {
                            case "1": ddlCarConfirmStatus.SelectedValue = "12"; break;
                            case "2": ddlCarConfirmStatus.SelectedValue = "12"; break;
                            case "3": ddlCarConfirmStatus.SelectedValue = "12"; break;
                            case "5": ddlCarConfirmStatus.SelectedValue = "12"; break;
                            default:
                                ddlCarConfirmStatus.Enabled = true;
                                break;
                        }
                    }
                    else
                    {
                        ddlCarConfirmStatus.DataValueField = "CAR_STATUS_ID";
                        ddlCarConfirmStatus.DataTextField = "CAR_STATUS_NAME";
                        ddlCarConfirmStatus.DataSource = Session["DataMcarConfirmStatus"];
                        ddlCarConfirmStatus.DataBind();
                        ddlCarConfirmStatus.Items.Insert(0, new ListItem()
                        {
                            Text = "--เลือก--",
                        });
                    }

                    string value = e.CellValue + string.Empty;

                    ddlCarConfirmStatus.SelectedValue = value;

                    //DropDownList ddlGpsStatus = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, null, "ddlGpsStatus") as DropDownList;
                    //ddlGpsStatus.Items.Clear();
                    //ddlGpsStatus.Items.Add(new ListItem()
                    //{
                    //    Text = "--เลือก--",
                    //});
                    //if (value == "1" || value == "2" || value == "3")
                    //{
                    //    ddlGpsStatus.Items.Add(new ListItem()
                    //    {
                    //        Text = "Normal",
                    //        Value = "Normal"
                    //    });
                    //}
                    //else
                    //{
                    //    ddlGpsStatus.Items.Add(new ListItem()
                    //    {
                    //        Text = "Normal",
                    //        Value = "Normal"
                    //    });
                    //    ddlGpsStatus.Items.Add(new ListItem()
                    //    {
                    //        Text = "Offline",
                    //        Value = "Offline"
                    //    });
                    //}

                    //DropDownList ddlCctvStatus = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, null, "ddlCctvStatus") as DropDownList;
                    //ddlCctvStatus.Items.Clear();
                    //ddlCctvStatus.Items.Add(new ListItem()
                    //{
                    //    Text = "--เลือก--",
                    //});
                    //if (value == "1" || value == "2" || value == "3")
                    //{
                    //    ddlCctvStatus.Items.Add(new ListItem()
                    //    {
                    //        Text = "Normal",
                    //        Value = "Normal"
                    //    });
                    //}
                    //else
                    //{
                    //    ddlCctvStatus.Items.Add(new ListItem()
                    //    {
                    //        Text = "Normal",
                    //        Value = "Normal"
                    //    });
                    //    ddlCctvStatus.Items.Add(new ListItem()
                    //    {
                    //        Text = "Offline",
                    //        Value = "Offline"
                    //    });
                    //}

                }
            }
            else if (e.DataColumn.FieldName == "STERMINALID")
            {
                DropDownList ddlWareHouse = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "ddlWareHouse") as DropDownList;
                if (ddlWareHouse != null)
                {
                    ddlWareHouse.DataValueField = "STERMINALID";
                    ddlWareHouse.DataTextField = "SABBREVIATION";
                    ddlWareHouse.DataSource = Session["DataTTERMINAL"];
                    ddlWareHouse.DataBind();
                    ddlWareHouse.Items.Insert(0, new ListItem()
                    {
                        Text = "--เลือก--",
                    });
                    ddlWareHouse.SelectedValue = e.CellValue + string.Empty;
                }
            }
            else if (e.DataColumn.FieldName == "DELIVERY_NUM")
            {
                CheckBoxList cblDeliveryNum = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "cblDeliveryNum") as CheckBoxList;
                if (cblDeliveryNum != null)
                {
                    foreach (ListItem item in cblDeliveryNum.Items)
                    {
                        item.Attributes.Add("onchange", "ddlOnChange($(this).val()," + e.VisibleIndex + ",\"DELIVERY_NUM\");");
                    }
                    if (!string.IsNullOrEmpty(e.CellValue + string.Empty))
                    {
                        foreach (var item in (e.CellValue + string.Empty).Split(','))
                        {
                            cblDeliveryNum.Items[int.Parse(item) - 1].Selected = true;
                        }
                    }

                }
            }
            else if (e.DataColumn.FieldName == "IS_SELECT")
            {
                CheckBox cbSelect = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "cbSelect") as CheckBox;
                if (cbSelect != null)
                {
                    cbSelect.InputAttributes.Add("onchange", "ddlOnChange($(this).is(':checked')," + e.VisibleIndex + ",\"IS_SELECT\");");
                    //!string.IsNullOrEmpty(item["CAR_STATUS_ID"] + string.Empty) && item["STERMINALID"] + string.Empty != "-1";
                    dynamic datas = xgvwCar.GetRowValues(e.VisibleIndex, "CAR_STATUS_ID", "STERMINALID");
                    if (string.IsNullOrEmpty(datas[0] + string.Empty) || (datas[1] + string.Empty == "--เลือก--" || string.IsNullOrEmpty(datas[1] + string.Empty)))
                    {
                        cbSelect.Enabled = false;
                    }
                    cbSelect.Checked = e.CellValue + string.Empty == "1";
                }
            }
            //else if (e.DataColumn.FieldName == "GPS_STATUS")
            //{
            //    DropDownList ddlGpsStatus = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "ddlGpsStatus") as DropDownList;
            //    if (ddlGpsStatus != null)
            //    {
            //        ddlGpsStatus.SelectedValue = e.CellValue + string.Empty;
            //    }
            //}
            //else if (e.DataColumn.FieldName == "CCTV_STATUS")
            //{
            //    DropDownList ddlCctvStatus = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "ddlCctvStatus") as DropDownList;
            //    if (ddlCctvStatus != null)
            //    {
            //        ddlCctvStatus.SelectedValue = e.CellValue + string.Empty;
            //    }
            //}
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    #region xgvwCar_AfterPerformCallback
    protected void xgvwCar_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        try
        {
            btnsubmitVisible = true;
            string CallbackName = "CUSTOMCALLBACK";
            ASPxGridView xgvwCar = ((ASPxGridView)sender);
            //BackUpData(xgvw.EditingRowVisibleIndex, ((ASPxGridView)sender));
            //ASPxButton btnsubmit = xgvw.FindEditFormTemplateControl("btnsubmit") as ASPxButton;

            int idxMaster = xgvw.EditingRowVisibleIndex;
            //dynamic RowValueContract = xgvw.GetRowValues(idxMaster, "SCONTRACTNO");
            //string ContractNoValue = RowValueContract[0];
            int idxParent = 0;
            string[] param = e.Args[0].ToString().Split('$');
            e.Args[0] = param[0];
            if (param.Count() > 1)
            {
                CallbackName = param[1];
            }
            else
            {
                CallbackName = param[0];
            }
            if (e.Args.Length > 0) idxParent = (int.TryParse("" + e.Args[0], out defInt)) ? int.Parse("" + e.Args[0]) : 0;

            #region GetHerderControl
            CheckBoxList cblDeliveryNumHeader = (CheckBoxList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["DELIVERY_NUM"], "cblDeliveryNumHeader");
            DropDownList ddlCarTypeHeader = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["CARTYPE"], "ddlCarTypeHeader");
            CheckBox cbSelectHeader = (CheckBox)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["IS_SELECT"], "cbSelectHeader");
            //DropDownList ddlGpsStatusHeader = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["GPS_STATUS"], "ddlGpsStatusHeader");
            //DropDownList ddlCctvStatusHeader = (DropDownList)xgvwCar.FindHeaderTemplateControl(xgvwCar.Columns["CCTV_STATUS"], "ddlCctvStatusHeader");
            #endregion
            string CAR_STATUS_ID = string.Empty;
            object val = new object();
            DataTable dt;
            int idxMaster2 = xgvw.EditingRowVisibleIndex;
            dynamic RowValueContract = xgvw.GetRowValues(idxMaster2, "SCONTRACTNO");
            //string ContractNoValue = RowValueContract[0];
            switch (CallbackName.ToUpper())
            {
                case "HIDEALLDETAIL":
                case "HIDEDETAILROW":
                case "SHOWDETAILROW":
                case "CUSTOMCALLBACK":
                    break;
                case "ISSUE": //SCONTRACTID,STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,DDELIVERY,NPLANID,CGROUPCHECK,SPROCESS 
                    dynamic RowValue = xgvwCar.GetRowValues(idxParent, "SCONTRACTID", "STRUCKID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO");

                    string sEncrypt = RowValue[0] + "&" + RowValue[1] + "&" + RowValue[2] + "&" + RowValue[3] + "&&&4&011";

                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(sEncrypt);
                    sEncrypt = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    xgvwCar.JSProperties["cpRedirectOpen"] = "checktruck.aspx?str=" + sEncrypt;
                    //CommonFunction.SetPopupOnLoad(xgvwCar, "dxConfirm('ยืนยันการทำงาน','ท่านต้องการทำการ <b>ทำรายการตรวจสภาพรถ</b> ใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide(); OpenNewWindow('" + sEncrypt + "'); } ,function(s,e){ dxPopupConfirm.Hide(); })"); return;
                    break;
                case "BINDDATA":

                    dynamic RowValue2 = xgvw.GetRowValues(idxMaster, "SCONTRACTID");
                    if (Session["ss_GPSExcel"] == null)
                    {
                        CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ระบบไม่พบรายการที่การ Upload กรุณาทำการอัพโหลดใหม่อีกครั้ง! <hr /><font size=""1"" >ข้อแนะนำควรทำการรายการหลังทำการอัพโหลดให้เสร็จสิ้นถายใน 30 นาที</font>')");
                    }
                    else
                    {
                        CheckDataTempExcel((DataTable)Session["ss_GPSExcel"], idxMaster);
                    }

                    if (Session["ss_DataGPSExcel"] != null)
                    {
                        DataTable dt_CHK = (DataTable)Session["ss_DataGPSExcel"];

                        var dv_CHK = dt_CHK.Select("SCONTRACTID = '" + RowValue2 + "'");
                        if (dv_CHK.Length != 0)
                        {
                            dt = (DataTable)Session["DataTcontractTruck"];
                            BindTruckWithDataFromExcel(xgvwCar, dt);
                        }
                        else
                        {
                            ((ASPxButton)xgvw.FindEditFormTemplateControl("btnsubmit")).ClientVisible = btnsubmitVisible = false;
                        }
                    }
                    else
                    {
                        ((ASPxButton)xgvw.FindEditFormTemplateControl("btnsubmit")).ClientVisible = btnsubmitVisible = false;
                    }

                    break;
                case "DDLONCHANGE":
                    string value = param[2];
                    string TYPE = param[3];
                    dt = (DataTable)Session["DataTcontractTruck"];
                    Session.Remove("DataTcontractTruck");
                    DataRow drOld = dt.NewRow();
                    if (idxParent != -1)
                    {
                        drOld = dt.Rows[idxParent];
                    }
                    switch (TYPE.ToUpper())
                    {
                        case "CARTYPE":
                            #region ประเภทรถ
                            if (value == "N")
                            {
                                drOld["CSTANBY"] = value;
                                DataRow dr = dt.NewRow();
                                foreach (DataColumn item in dt.Columns)
                                {
                                    dr[item] = drOld[item];
                                }
                                dr["DELIVERY_PART"] = "กลางคืน";
                                drOld["DELIVERY_PART"] = "กลางวัน";
                                dt.Rows.InsertAt(dr, idxParent + 1);
                            }
                            else
                            {
                                if (drOld["CSTANBY"] + string.Empty == "N")
                                {
                                    DataRow[] drs = dt.Select("SHEADREGISTERNO = '" + drOld["SHEADREGISTERNO"] + "'");
                                    if (drs.Any() && drs.Count() > 1)
                                    {
                                        drs[0]["CSTANBY"] = drs[1]["CSTANBY"] = value;
                                        drs[1].Delete();
                                    }
                                }
                                drOld["CSTANBY"] = value;
                                drOld["DELIVERY_PART"] = "ไม่ระบุ";
                                if (string.IsNullOrEmpty(value))
                                {
                                    drOld["DELIVERY_PART"] = DBNull.Value;
                                }

                            }
                            if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                ClearIVMS(drOld);
                            #endregion
                            break;
                        case "CARTYPEHEADER":
                            #region ประเภทรถ Hader
                            foreach (DataRow item in dt.Rows)
                            {
                                item["CSTANBY"] = value;
                                item["DELIVERY_PART"] = "ไม่ระบุ";
                                if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                {
                                    ClearIVMS(item);
                                    CheckCconfirm(item);
                                }
                            }
                            CheckCconfirmCatType(dt);

                            #endregion
                            break;
                        case "CAR_STATUS_ID":
                            #region สถานะรถขนส่ง
                            val = (value == "--เลือก--" ? (object)DBNull.Value : (object)value);

                            if (drOld["CSTANBY"] + string.Empty == "N")
                            {
                                DataRow[] drs = dt.Select("STRUCKID = '" + drOld["STRUCKID"] + "'");
                                if (drs.Any() && drs.Count() == 2)
                                {
                                    drs[0]["CAR_STATUS_ID"] = drs[1]["CAR_STATUS_ID"] = val;
                                    if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                    {
                                        CheckCconfirm(drs[0]);
                                        CheckCconfirm(drs[1]);
                                    }
                                }
                            }
                            drOld["CAR_STATUS_ID"] = val;
                            if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                ClearIVMS(drOld);
                            #endregion
                            break;
                        case "CAR_STATUS_IDHEADER":
                            #region สถานะรถขนส่ง HEADER
                            string strGPS = string.Empty;
                            val = (value == "-1" ? (object)DBNull.Value : (object)value);
                            if ((value == "1" || value == "2" || value == "3"))
                            {
                                strGPS = "Normal";
                            }
                            int i = 0;
                            foreach (DataRow item in dt.Rows)
                            {
                                if ((item["CMA"] + string.Empty) != "4")
                                {
                                    item["CAR_STATUS_ID"] = (value == "1" || value == "2" || value == "3") ? (object)DBNull.Value : val;
                                }
                                else
                                {
                                    item["CAR_STATUS_ID"] = val;
                                }

                                //item["GPS_STATUS"] = strGPS;
                                //item["CCTV_STATUS"] = strGPS;
                                if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                {
                                    ClearIVMS(item);
                                    CheckCconfirm(item);
                                }
                                CheckBokInGrid(xgvwCar, i, item);
                                i++;
                            }
                            CheckCconfirmCatType(dt);
                            #endregion
                            break;
                        case "STERMINALID":
                            #region คลังต้นทาง
                            val = (value == "-1" ? (object)DBNull.Value : (object)value);
                            drOld["STERMINALID"] = val;
                            if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                ClearIVMS(drOld);
                            #endregion
                            break;

                        case "STERMINALIDHEADER":
                            #region คลังต้นทาง HEADER
                            value = (value == "-1" ? string.Empty : value);
                            int j = 0;
                            foreach (DataRow item in dt.Rows)
                            {
                                item["STERMINALID"] = value;
                                if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                {
                                    ClearIVMS(item);
                                    CheckCconfirm(item);
                                }
                                CheckBokInGrid(xgvwCar, j, item);
                                j++;
                            }
                            CheckCconfirmCatType(dt);

                            #endregion
                            break;
                        case "DELIVERY_NUM":
                            #region เที่ยวที่รับงาน
                            CheckBoxList cbl = (CheckBoxList)xgvwCar.FindRowCellTemplateControl(idxParent, null, "cblDeliveryNum");
                            value = string.Empty;
                            foreach (ListItem item in cbl.Items)
                            {
                                if (item.Selected)
                                {
                                    value += "," + item.Value;
                                }
                            }
                            if (!string.IsNullOrEmpty(value))
                            {
                                value = value.Remove(0, 1);
                            }
                            drOld["DELIVERY_NUM"] = value;
                            if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                ClearIVMS(drOld);
                            #endregion
                            break;
                        case "DELIVERY_NUMHEADER":
                            #region เที่ยวที่รับงาน HEADER

                            value = string.Empty;
                            foreach (ListItem item in cblDeliveryNumHeader.Items)
                            {
                                if (item.Selected)
                                {
                                    value += "," + item.Value;
                                }
                            }
                            if (!string.IsNullOrEmpty(value))
                            {
                                value = value.Remove(0, 1);
                            }
                            foreach (DataRow item in dt.Rows)
                            {
                                item["DELIVERY_NUM"] = value;
                                if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                                {
                                    ClearIVMS(item);
                                }
                            }
                            CheckCconfirmCatType(dt);
                            #endregion
                            break;
                        //case "GPS_STATUS":
                        //    #region สถานะ GPS
                        //    val = (value == "--เลือก--" ? (object)DBNull.Value : (object)value);
                        //    if (drOld["F18"] + string.Empty == "N")
                        //    {
                        //        DataRow[] drs = dt.Select("STRUCKID = '" + drOld["STRUCKID"] + "'");
                        //        if (drs.Any() && drs.Count() == 2)
                        //        {
                        //            drs[0]["GPS_STATUS"] = drs[1]["GPS_STATUS"] = val;
                        //            CheckCconfirm(drs[0]);
                        //            CheckCconfirm(drs[1]);
                        //        }
                        //    }
                        //    drOld["GPS_STATUS"] = val;
                        //    #endregion
                        //    break;
                        //case "CCTV_STATUS":
                        //    #region สถานะ กล้อง
                        //    val = (value == "--เลือก--" ? (object)DBNull.Value : (object)value);
                        //    if (drOld["F18"] + string.Empty == "N")
                        //    {
                        //        DataRow[] drs = dt.Select("STRUCKID = '" + drOld["STRUCKID"] + "'");
                        //        if (drs.Any() && drs.Count() == 2)
                        //        {
                        //            drs[0]["CCTV_STATUS"] = drs[1]["CCTV_STATUS"] = val;
                        //            CheckCconfirm(drs[0]);
                        //            CheckCconfirm(drs[1]);
                        //        }
                        //    }
                        //    drOld["CCTV_STATUS"] = val;

                        //    #endregion
                        //    break;
                        //case "GPS_STATUSHEADER":
                        //    #region สถานะ GPS HEADER
                        //    val = (value == "--เลือก--" ? (object)DBNull.Value : (object)value);
                        //    foreach (DataRow item in dt.Rows)
                        //    {
                        //        item["GPS_STATUS"] = val;
                        //        CAR_STATUS_ID = item["CAR_STATUS_ID"] + string.Empty;
                        //        if ((CAR_STATUS_ID == "1" || CAR_STATUS_ID == "2" || CAR_STATUS_ID == "3") && val + string.Empty == "Offline")
                        //        {
                        //            item["GPS_STATUS"] = DBNull.Value;
                        //        }


                        //        CheckCconfirm(item);
                        //    }
                        //    CheckCconfirmCatType(dt);
                        //    #endregion
                        //    break;
                        //case "CCTV_STATUSHEADER":
                        //    #region สถานะ กล้อง HEADER
                        //    val = (value == "--เลือก--" ? (object)DBNull.Value : (object)value);
                        //    foreach (DataRow item in dt.Rows)
                        //    {
                        //        item["CCTV_STATUS"] = val;
                        //        CAR_STATUS_ID = item["CAR_STATUS_ID"] + string.Empty;
                        //        if ((CAR_STATUS_ID == "1" || CAR_STATUS_ID == "2" || CAR_STATUS_ID == "3") && val + string.Empty == "Offline")
                        //        {
                        //            item["CCTV_STATUS"] = DBNull.Value;
                        //        }
                        //        CheckCconfirm(item);
                        //    }
                        //    CheckCconfirmCatType(dt);
                        //    #endregion
                        //    break;
                        case "REMARK":
                            #region หมายเหตุ
                            drOld["REMARK"] = value;
                            #endregion
                            break;
                        case "IS_SELECT":
                            #region CheckBox
                            val = (value != "true" ? "0" : "1");
                            drOld["IS_SELECT"] = val;
                            #endregion

                            break;
                        case "IS_SELECTHEADER":
                            val = (value != "true" ? "0" : "1");
                            int k = 0;
                            foreach (DataRow item in dt.Rows)
                            {
                                if (!string.IsNullOrEmpty(item["CAR_STATUS_ID"] + string.Empty) && item["STERMINALID"] + string.Empty != "-1")
                                {
                                    item["IS_SELECT"] = val;
                                }

                                k++;
                            }
                            Session["DataTcontractTruck"] = dt;
                            break;
                        default:
                            break;
                    }
                    if (idxParent != -1)
                    {
                        if (!string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/6/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/3/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/4/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/5/2562") && !string.Equals(RowValueContract, "PTTOR/ขปธ./พ.1/2/2562"))
                        {
                            CheckCconfirm(drOld);
                        }
                        dt.AcceptChanges();
                        Session["DataTcontractTruck"] = dt;
                    }

                    xgvwCar.DataSource = Session["DataTcontractTruck"];
                    xgvwCar.DataBind();
                    if (idxParent != -1 && TYPE.ToUpper() != "IS_SELECT")
                    {
                        CheckBokInGrid(xgvwCar, idxParent, drOld);
                    }

                    break;
            }
            //xgvw.da
            SetHeaderColumnxgvwCar(xgvwCar, ddlCarTypeHeader, cblDeliveryNumHeader, null, null, null, null, cbSelectHeader);
        }
        catch (Exception ex)
        {
            Label lblError = (Label)xgvw.FindEditFormTemplateControl("lblError");
            if (lblError != null)
            {
                lblError.Text = ex.Message;
            }

            //alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion


    //Upload
    protected void uclGPSxls_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
            e.CallbackData = UploadFile2Server(e.UploadedFile, "" + MasterData[0] + "_" + MasterData[1].ToString("ddMMyyyy") + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "By" + Session["UserID"], "UploadFile/GPS/Temp_Excel/");
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }
    protected void uclGPSpic_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
            e.CallbackData = UploadFile2Server(e.UploadedFile, "" + MasterData[0] + "_" + MasterData[1].ToString("ddMMyyyy") + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "By" + Session["UserID"], "UploadFile/GPS/Temp_PIC/");

        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }
    private string GetConnectionForFileExcel(FileInfo file)
    {
        // Connection String to Excel Workbook
        // Connection String < v.2007
        string excelConnectionString = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source='" + file.FullName + "';Extended Properties='Excel 8.0;HDR=Yes'";
        if (file.Extension.ToLower().Equals(".xlsx"))
        {
            // Connection String = v.2007
            excelConnectionString = "Provider='Microsoft.ACE.OLEDB.12.0';Data Source='" + file.FullName + "';Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text'";
        }
        return excelConnectionString;
    }
    private string GetExcelSheet(OleDbConnection excelcon)
    {
        try
        {
            // หาชื่อ sheet
            string _exxcelsheet = "";
            DataRowCollection _ar_dr = excelcon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" }).Rows;
            foreach (DataRow _dr in _ar_dr)
            {
                if (!_dr["TABLE_NAME"].ToString().IndexOf("$").Equals(-1) && !string.IsNullOrEmpty("" + _dr["TABLE_NAME"]) && _dr["TABLE_NAME"].ToString().Equals("ConfirmTruck$"))
                {
                    _exxcelsheet = _dr["TABLE_NAME"].ToString();
                    break;
                }
            }
            return _exxcelsheet;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    private bool Confirm(int visibleindex, ASPxGridView xgvwCar, ref string strError)
    {
        bool isRes = false;
        try
        {

            if (CanWrite)
            {
                string Mess = string.Empty;
                if (!string.IsNullOrEmpty(dteEnd.Text))
                {
                    DateTime? dte = ConvertToDateNull(dteEnd.Text);
                    if (dte != null && dte.Value.Date < DateTime.Today)
                    {
                        Mess += "ข้อมูลในวันที่ผ่านมาแล้วไม่สามารถแก้ไขได้";
                        strError = Mess;
                        return isRes;
                    }
                }

                DataTable dtContract = ContractConfirmBLL.Instance.SelectTruckConfirmBLL(Session["UserID"].ToString(), dteEnd.Text + " 00:00:00", dteEnd.Text + " 23:59:59", txtKeyword.Text + "", cmbStatus.SelectedValue + "");
                string SCONTRACTID = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID") + string.Empty;
                DataRow[] drs = dtContract.Select("SCONTRACTID = " + SCONTRACTID);
                if (drs.Any())
                {
                    DataRow dr = drs[0];

                    if (dr["PTT_LOCK"] + string.Empty == "1")
                    {
                        Mess += "ไม่สามารถแก้ไขข้อมูลการยืนยันรถในสัญญากรุณาติดต่อพนักงาน ปตท.";
                        strError = Mess;
                        return isRes;
                    }
                }
                Mess += Validate();
                if (!string.IsNullOrEmpty(Mess))
                {
                    strError = Mess;
                    //CommonFunction.SetPopupOnLoad(xgvw, "dxInfo('ข้อความจากระบบ ','ท่านได้มีรายการยืนยันตามสัญญาไม่ผ่านดังนี้" + Mess + "<br>กรุณาตรวจสอบหรือทำรายการใหม่อีกครั้ง!')"); 
                    return isRes;
                }
                //string dStart = dteStart.Value.ToString();
                string dEnd = dteEnd.Text;
                string sVendorID = "" + Session["SVDID"];

                //TTRUCKCONFIRMLIST conf_list = new TTRUCKCONFIRMLIST(connection);

                //DataTable dt_filedata = (DataTable)Session["ss_filedata"];
                Session.Remove("ss_filedata");
                #region Send
                string msgUnSend = "";
                dynamic RowImport = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE", "NCONFIRMID", "DEXPIRE", "SCONTRACTNO");

                //conf_list.NCONFIRMID = "" + RowImport[2];

                #region FILE&PIC
                //                if (dt_filedata != null)
                //                {
                //                    using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                //                    {
                //                        #region Disable Old DOC
                //                        OracleCommand Disable_DOC_Cmd = new OracleCommand();
                //                        Disable_DOC_Cmd.Connection = OraConnection;
                //                        if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();
                //                        Disable_DOC_Cmd.CommandType = CommandType.Text;
                //                        Disable_DOC_Cmd.CommandText = @"UPDATE TTRUCKCONFIRM_DOC SET CACTIVE='0' WHERE SCONTRACTID='" + RowImport[0] + "' AND DDATE=TO_DATE('" + Convert.ToDateTime(RowImport[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') ";
                //                        Disable_DOC_Cmd.ExecuteNonQuery();
                //                        #endregion
                //                        #region Add New DOC
                //                        OracleCommand INS_DOC_Cmd = new OracleCommand();
                //                        INS_DOC_Cmd.Connection = OraConnection;
                //                        if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();
                //                        INS_DOC_Cmd.CommandType = CommandType.Text;
                //                        foreach (string gps_type in ArrayGPSType)
                //                        {
                //                            //foreach (DataRow drfiledata in dt_filedata.Select("SCONTRACTID='" + RowImport[0] + "' AND DDATE=TO_DATE('" + Convert.ToDateTime(RowImport[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') " + "' AND DOC_TYPE='" + gps_type + "'"))
                //                            foreach (DataRow drfiledata in dt_filedata.Select("SCONTRACTID='" + RowImport[0] + "' AND DOC_TYPE='" + gps_type + "'"))
                //                            {
                //                                INS_DOC_Cmd.CommandText = @"INSERT INTO TTRUCKCONFIRM_DOC (DOC_ID, SCONTRACTID, DDATE, CACTIVE, DCREATE, DUPDATE, DOC_TYPE, DOC_SYSNAME, DOC_ORGNAME, SREMARK) 
                //VALUES (  FC_GENID_TTRUCKCONFIRM_DOC('') ,'" + RowImport[0] + "' , TO_DATE('" + Convert.ToDateTime(RowImport[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') ,'1' ,SYSDATE ,SYSDATE  ,'" + gps_type + "' ,'" + drfiledata["SystemFileName"] + "' ,'" + drfiledata["OriginalFileName"] + "' ,'" + drfiledata["SREMARK"] + "' )";
                //                                INS_DOC_Cmd.ExecuteNonQuery();
                //                            }
                //                        }
                //                        #endregion
                //                    }
                //                }


                #endregion
                #region TRUCK
                //var watch = System.Diagnostics.Stopwatch.StartNew();
                //the code that you want to measure comes here
                DataTable dt = (DataTable)Session["DataTcontractTruck"];
                //int row = dt.Rows.Count;
                //int column = dt.Columns.Count;
                //Session.Remove("DataTcontractTruck");
                isRes = ContractConfirmBLL.Instance.SaveTtruckConfirmList(Session["UserID"] + string.Empty, int.Parse(RowImport[2] + string.Empty), dt);
                //watch.Stop();
                //var elapsedMs = watch.ElapsedMilliseconds;
                //string mess = "จำนวน " + row + " Row " + column + " Column เวลาที่ใช้ในการ Insert " + watch.Elapsed;
                //strError += mess;
                #endregion

                #endregion
                if (isRes)
                {
                    Session.Remove("DataTcontractTruck");

                }
                else
                {
                    strError += "ท่านได้มีรายการยืนยันตามสัญญาไม่ผ่านดังนี้" + msgUnSend + "<br>กรุณาตรวจสอบหรือทำรายการใหม่อีกครั้ง!"; return isRes;
                }
                //SearchData("");
                //ต้องเคลียร์รายการที่เลือกไว้ด้วย
                //LogUser("1", "I", "ยืนยันรถตามสัญญา", "");
            }
            else
            {
                strError += "คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!";
            }
        }
        catch (Exception ex)
        {
            strError += "ท่านได้มีรายการยืนยันตามสัญญาไม่ผ่านดังนี้" + ex.Message + "<br>กรุณาตรวจสอบหรือทำรายการใหม่อีกครั้ง!";
        }
        return isRes;
    }

    #region Validate
    private string Validate()
    {
        string Mess = string.Empty;
        DataTable dt = (DataTable)Session["DataTcontractTruck"];
        Session.Remove("DataTcontractTruck");
        DataRow[] drs;
        DataTable dtDataMcarConfirmStatus = (DataTable)Session["DataMcarConfirmStatus"];
        Session.Remove("DataMcarConfirmStatus");
        foreach (DataRow dr in dt.Rows)
        {

            //if (!string.IsNullOrEmpty(dr["CCONFIRM"] + string.Empty) && ((dr["CCONFIRM"] + string.Empty) == "1"))
            //{
            //    if (dr["CCONFIRM"] + string.Empty == "0")
            //    {

            //        drs = dtDataMcarConfirmStatus.Select("CAR_STATUS_ID = " + dr["CAR_STATUS_ID"]);
            //        if (drs.Any())
            //        {
            //            Mess += "ทะเบียนรถ " + dr["SHEADREGISTERNO"] + " สถานะรถขนส่ง " + drs[0]["CAR_STATUS_NAME"] + " ต้องระบุข้อมูลให้ครบทุกช่อง<br/>";
            //        }

            //    }
            //}
            if (!string.IsNullOrEmpty(dr["CAR_STATUS_ID"] + string.Empty) && (dr["CAR_STATUS_ID"] + string.Empty) == "7" && string.IsNullOrEmpty(dr["REMARK"] + string.Empty))
            {
                drs = dtDataMcarConfirmStatus.Select("CAR_STATUS_ID = " + dr["CAR_STATUS_ID"]);
                Mess += "ทะเบียนรถ " + dr["SHEADREGISTERNO"] + " สถานะรถขนส่ง " + drs[0]["CAR_STATUS_NAME"] + " กรุณาระบุรายละเอียดในช่องหมายเหตุ<br/>";
            }
        }
        Session["DataTcontractTruck"] = dt;
        Session["DataMcarConfirmStatus"] = dtDataMcarConfirmStatus;
        return Mess;
    }
    #endregion

    protected void imbedit_Click(object sender, EventArgs e)
    {
        ASPxButton btn = (ASPxButton)sender;
        string CommandArgument = btn.CommandArgument;
        string[] CommandArguments = CommandArgument.Split('|');
        string I_CONTRACT_ID = CommandArguments[0];
        if (!string.IsNullOrEmpty(I_CONTRACT_ID))
        {
            DataTable dt = ContractConfirmBLL.Instance.GetTcontractTruck(I_CONTRACT_ID);
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Vendor/ConfirmTruckGPSFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["ConfirmTruck"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = CommandArguments[1].Replace("/", "_") + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);

            this.DownloadFile(Path, FileName);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void CheckDataTempExcel(DataTable _dtTemp, int idxMaster)
    {
        try
        {
            ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");
            #region ไม่ได้ทำการอัพโหลดไฟล์
            if (_dtTemp == null)
            {
                CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ระบบไม่พบ WorkSheetชื่อ <b>ConfirmTruck</b> กรุณาตรวจสอบExcelอีกครั้ง! <hr /><font size=""1"" >ข้อแนะนำควรระบุชื่อWorkSheetที่จะทำรายการเป็นConfirmTruckไว้เพียงWorkSheetเดียวเท่านั้น</font>')");
                //CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ระบบไม่พบรายการการ Upload กรุณาทำการอัพโหลดอีกครั้ง! <hr /><font size=""1"" >ข้อแนะนำควรทำการรายการหลังทำการอัพโหลดให้เสร็จสิ้นถายใน 30 นาที</font>')");
                return;
            }
            #endregion
            #region จัดเตีรยมดาต้าเทเบิล
            _dtTemp.Columns.Add("SCONTRACTID");
            _dtTemp.Columns.Add("CPASSED");
            _dtTemp.Columns.Add("CMA");
            _dtTemp.Columns.Add("DFINAL_MA");
            _dtTemp.Columns.Add("STRUCKID");
            _dtTemp.Columns.Add("STRAILERID");
            _dtTemp.Columns.Add("NDAY_MA");
            _dtTemp.Columns.Add("CAR_STATUS_ID");
            _dtTemp.Columns.Add("STERMINALID");
            string[] StringArrayColumnName ={  "CARTYPE"                      //ประเภทรถ
                                           , "DELIVERY_PART"                //รอบการขนส่ง
                                           , "SHEADREGISTERNO"              //ทะเบียนหัว
                                           , "STRAILERREGISTERNO"           //ทะเบียนท้าย
                                           , "CAR_STATUS"                   //สถานะรถขนส่ง
                                           , "WAREHOUSE"                    //คลังต้นทางประสงค์เข้ารับงาน
                                           , "DELIVERY_NUM"                 //เที่ยวที่ประสงค์เข้ารับงาน
                                           //, "GPS_STATUS"                   //สถานะ GPS
                                           //, "CCTV_STATUS"                  //สถานะ CCTV
                                           , "LAT_LONG"                     //ละติจูด/ลองติจูด
                                           , "KM2PLANT"                     //ระยะทางถึงคลัง (กม.)
                                           , "HR2PLANT"                     //เวลาถึงคลัง (ชม.)
                                           , "ADDRESS_CURRENT"              //สถานที่อยู่ปัจจุบัน
                                           , "PERSONAL_CODE"                //รหัสบัตรประชาชน พขร.
                                           , "REMARK"                       //หมายเหตุ
                                           , "SCONTRACTID"                  //เลขที่สัญญา
                                           , "CPASSED"};                     //หมายเหตุ

            for (int i = 0; i < 15; i++)
            {
                _dtTemp.Columns[i].ColumnName = StringArrayColumnName[i];
            }
            #endregion

            dynamic RowImport = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE", "NCONFIRMID", "DEXPIRE", "SCONTRACTNO", "NONHANDSYSTEM", "NSTANDBYSYSTEM");

            string VEN_NO = "", TU_NO = "", mess = string.Empty;
            int nCarInContract = int.Parse(RowImport[5] + "") + int.Parse(RowImport[6] + "");
            DataTable dt = (DataTable)Session["DataTcontractTruck"];
            DataTable DataMcarConfirmStatus = (DataTable)Session["DataMcarConfirmStatus"];
            DataTable DataTTERMINAL = (DataTable)Session["DataTTERMINAL"];
            if (!dt.Columns.Contains("LAT_LONG"))
            {
                dt.Columns.Add("LAT_LONG");
            }
            if (!dt.Columns.Contains("KM2PLANT"))
            {
                dt.Columns.Add("KM2PLANT");
            }
            if (!dt.Columns.Contains("HR2PLANT"))
            {
                dt.Columns.Add("HR2PLANT");
            }
            if (!dt.Columns.Contains("ADDRESS_CURRENT"))
            {
                dt.Columns.Add("ADDRESS_CURRENT");
            }
            if (!dt.Columns.Contains("PERSONAL_CODE"))
            {
                dt.Columns.Add("PERSONAL_CODE");
            }
            if (!dt.Columns.Contains("CPASSED"))
            {
                dt.Columns.Add("CPASSED");
            }
            foreach (DataRow item in dt.Rows)
            {
                DataRow[] drs = _dtTemp.Select("SHEADREGISTERNO = '" + item["SHEADREGISTERNO"] + "'");
                if (drs.Any())
                {
                    if ((drs[0]["CARTYPE"] + string.Empty) == "รถปกติ")
                    {
                        item["CSTANBY"] = "Y";
                        item["DELIVERY_PART"] = "ไม่ระบุ";
                    }
                    else
                    {
                        item["CSTANBY"] = "N";
                    }
                    DataRow[] drMcarConfirmStatuss = DataMcarConfirmStatus.Select("CAR_STATUS_NAME = '" + drs[0]["CAR_STATUS"] + "'");
                    if (drMcarConfirmStatuss.Any())
                    {
                        item["CAR_STATUS_ID"] = drMcarConfirmStatuss[0]["CAR_STATUS_ID"];
                    }

                    DataRow[] drTTERMINAL = DataTTERMINAL.Select("SABBREVIATION = '" + drs[0]["WAREHOUSE"] + "'");
                    if (drTTERMINAL.Any())
                    {
                        item["STERMINALID"] = drTTERMINAL[0]["STERMINALID"];
                    }

                    item["DELIVERY_NUM"] = drs[0]["DELIVERY_NUM"];
                    //item["GPS_STATUS"] = drs[0]["GPS_STATUS"];
                    //item["CCTV_STATUS"] = drs[0]["CCTV_STATUS"];
                    item["REMARK"] = drs[0]["REMARK"];
                }
            }
            CheckCconfirmCatType(dt);
            Session.Remove("ss_GPSExcel");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    #region CheckCconfirm
    private void CheckCconfirm(DataRow dr)
    {
        dr["CCONFIRM"] = "1";
        if ((dr["CMA"] + string.Empty) != "4")
        {
            dr["CCONFIRM"] = "0";
            return;
        }
        if (string.IsNullOrEmpty(dr["CSTANBY"] + string.Empty))
        {
            dr["CCONFIRM"] = "0";
            return;
        }
        if (string.IsNullOrEmpty(dr["CAR_STATUS_ID"] + string.Empty) || !((dr["CAR_STATUS_ID"] + string.Empty) == "1" || (dr["CAR_STATUS_ID"] + string.Empty) == "2" || (dr["CAR_STATUS_ID"] + string.Empty) == "3"))
        {
            dr["CCONFIRM"] = "0";
            return;
        }
        if (string.IsNullOrEmpty(dr["STERMINALID"] + string.Empty))
        {
            dr["CCONFIRM"] = "0";
            return;
        }
        if (string.IsNullOrEmpty(dr["DELIVERY_NUM"] + string.Empty))
        {
            dr["CCONFIRM"] = "0";
            return;
        }
        if (dr["IVMS_GPS_STAT"] + string.Empty != "1")
        {
            dr["CCONFIRM"] = "0";
            return;
        }
        if (dr["IVMS_MDVR_STAT"] + string.Empty != "1")
        {
            dr["CCONFIRM"] = "0";
            return;
        }
        if (string.IsNullOrEmpty(dr["REMARK"] + string.Empty) && string.IsNullOrEmpty(dr["CAR_STATUS_ID"] + string.Empty) && dr["CAR_STATUS_ID"] + string.Empty == "7")
        {
            dr["CCONFIRM"] = "0";
            return;
        }

    }
    #endregion

    #region CheckCconfirmCatType
    private void CheckCconfirmCatType(DataTable dt)
    {
        Session.Remove("DataTcontractTruck");
        List<DataRow> listDataRow = new List<DataRow>();
        List<DataRow> listDataRowDelete = new List<DataRow>();
        foreach (DataRow item in dt.Rows)
        {
            DataRow[] drs = dt.Select("SHEADREGISTERNO = '" + item["SHEADREGISTERNO"] + "'");
            if (drs.Count() == 2 && item["CSTANBY"] + string.Empty == "Y")
            {
                #region มี 2 row แต่เป็นรถปกติ
                //ลบออก 1 row
                //drs[1].AcceptChanges();
                listDataRowDelete.Add(drs[1]);
                drs[0]["DELIVERY_PART"] = "ไม่ระบุ";
                #endregion
            }
            else if (drs.Count() == 1 && item["CSTANBY"] + string.Empty == "N")
            {
                #region มี 1 row แต่เป็นหมุนเวียน
                DataRow dr = dt.NewRow(), drOld = drs[0];
                foreach (DataColumn itemColumn in dt.Columns)
                {
                    dr[itemColumn] = drOld[itemColumn];
                }
                dr["DELIVERY_PART"] = "กลางคืน";
                drOld["DELIVERY_PART"] = "กลางวัน";
                listDataRow.Add(dr);
                #endregion
            }
            CheckCconfirm(item);
        }
        foreach (var item in listDataRow)
        {
            CheckCconfirm(item);
            dt.Rows.Add(item);
        }
        foreach (var item in listDataRowDelete)
        {
            item.Delete();
        }

        dt.AcceptChanges();
        dt.DefaultView.Sort = "SHEADREGISTERNO";
        DataView dv = dt.DefaultView;
        dv.Sort = "SHEADREGISTERNO ASC";
        DataTable sortedDT = dv.ToTable();
        Session["DataTcontractTruck"] = sortedDT;
    }
    #endregion

    protected void xgvwCar_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":
                if (("" + e.GetValue("CCONFIRM")).Equals("1") && ("" + e.GetValue("CSTANBY")).Equals("N"))
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6ffaf");
                }
                else if (("" + e.GetValue("CCONFIRM")).Equals("1") && ("" + e.GetValue("CSTANBY")).Equals("Y"))
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#9bea69");
                }
                else if (("" + e.GetValue("CCONFIRM")).Equals("0") && ("" + e.GetValue("CSTANBY")).Equals("N"))
                {
                    //rgba(47, 255, 121, 0.63)AAAAA

                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#fdf498");
                }
                else if (("" + e.GetValue("CCONFIRM")).Equals("0") && ("" + e.GetValue("CSTANBY")).Equals("Y"))
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f6e53b");
                }

                break;

            case "hader":

                break;
            default:
                break;
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");
        string strError = string.Empty;
        Confirm(0, xgvwCar, ref strError);
    }

    private void CheckIvms(ASPxGridView xgvwCar)
    {
        DataTable dt;
        DataRow dr;
        dt = (DataTable)Session["DataTcontractTruck"];
        Session.Remove("DataTcontractTruck");
        //for (int i = 0; i < xgvwCar.VisibleRowCount; i++)
        //{
        //    if (xgvwCar.Selection.IsRowSelected(i))
        //    {
        //        dr = dt.Rows[i];
        //        dr["IS_SELECT"] = "1";
        //    }
        //}
        DataTable dtTerminal = DataTerminal(dt);
        string I_LICENSE = string.Empty;
        DataSet dsIVMS = new DataSet();
        foreach (DataRow item in dtTerminal.Rows)
        {
            I_LICENSE = string.Empty;
            I_LICENSE = string.Empty;
            foreach (var itemLICENSE in dt.Select("STERMINALID = '" + item["STERMINALID"] + "' AND IS_SELECT = 1"))
            {
                dtIVMSCheckService = ServiceVehicleStatus(itemLICENSE["SHEADREGISTERNO"] + string.Empty, item["STERMINALID"] + string.Empty, dt.Rows[0]["SVENDORID"] + string.Empty, dt.Rows[0]["SCONTRACTNO"] + string.Empty);
                if (dtIVMSCheckService.Rows.Count > 0)
                {
                    foreach (DataRow itemIVMS in dtIVMSCheckService.Rows)
                    {
                        DataRow[] drs = dt.Select(" SHEADREGISTERNO = '" + itemLICENSE["SHEADREGISTERNO"] + "' AND STERMINALID = '" + item["STERMINALID"] + "'");
                        if (drs.Any())
                        {
                            dr = drs[0];
                            dr["IVMS_GPS_STAT"] = itemIVMS["GPS_STAT"];
                            dr["IVMS_MDVR_STAT"] = itemIVMS["MDVR_STATUS"];
                            dr["IVMS_STATUS"] = itemIVMS["STATUS"];
                            dr["IVMS_RADIUS_KM"] = itemIVMS["RADIUS_KM"];
                            dr["IVMS_CHECKDATE"] = itemIVMS["DateTime"];
                        }
                    }
                }
                //I_LICENSE += ",'" + itemLICENSE["SHEADREGISTERNO"] + "'";
            }

            //foreach (var itemLICENSE in dt.Select("STERMINALID = '" + item["STERMINALID"] + "' AND IS_SELECT = 1"))
            //{
            //    I_LICENSE += ",'" + itemLICENSE["SHEADREGISTERNO"] + "'";
            //}
            //if (!string.IsNullOrEmpty(I_LICENSE))
            //{
            //    I_LICENSE = I_LICENSE.Remove(0, 1);
            //    dtIVMSCheckService = ServiceVehicleStatus(I_LICENSE.Replace("\'",""), item["STERMINALID"] + string.Empty, dt.Rows[0]["SVENDORID"] + string.Empty, dt.Rows[0]["SCONTRACTNO"] + string.Empty);
            //    dsIVMS = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(item["STERMINALID"] + string.Empty, I_LICENSE);
            //    if (dsIVMS != null && dsIVMS.Tables.Count > 0 && dsIVMS.Tables[0].Rows.Count > 0)
            //    {
            //        foreach (DataRow itemIVMS in dsIVMS.Tables[0].Rows)
            //        {
            //            DataRow[] drs = dt.Select(" SHEADREGISTERNO = '" + itemIVMS["VEHICLE_NUMBER"] + "' AND STERMINALID = '" + itemIVMS["PLANTCODE"] + "'");
            //            if (drs.Any())
            //            {
            //                dr = drs[0];
            //                dr["IVMS_GPS_STAT"] = itemIVMS["GPS_STAT"];
            //                dr["IVMS_MDVR_STAT"] = itemIVMS["MDVR_STAT"];
            //                dr["IVMS_STATUS"] = itemIVMS["STATUS"];
            //                dr["IVMS_RADIUS_KM"] = itemIVMS["Radius_Km"];
            //                dr["IVMS_CHECKDATE"] = itemIVMS["DateTime"];
            //            }
            //        }
            //    }
            //}

        }
        foreach (DataRow item in dt.Rows)
        {
            CheckCconfirm(item);
        }
        Session["DataTcontractTruck"] = dt;
        xgvwCar.DataSource = Session["DataTcontractTruck"];
        xgvwCar.DataBind();
    }

    private DataTable ServiceVehicleStatus(string I_LICENSE, string I_PLANTCODE, string I_VENDORCODE, string I_CONTRACTNUM)
    {
        DataTable dtcheckivms = new DataTable();
        var YourModel = new { License = I_LICENSE, splantcode = I_PLANTCODE, cariercode = I_VENDORCODE, contractnum = I_CONTRACTNUM };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_vehiclestatus/VehicleStatus", stringContent)
                               .Result
                               ;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                model.EnsureSuccessStatusCode();
                var contents = model.Content.ReadAsStringAsync();
                if (!string.Equals(contents.Result.ToString(), string.Empty))
                    dtcheckivms = (DataTable)JsonConvert.DeserializeObject(contents.Result.ToString(), (typeof(DataTable)));
            }
            return dtcheckivms;
        }
    }

    private DataTable DataTerminal(DataTable dt)
    {
        DataTable dtTerminal = new DataTable();
        dtTerminal.Columns.Add("STERMINALID", typeof(string));
        dtTerminal.Columns.Add("SABBREVIATION", typeof(string));
        if (dt != null && dt.Rows.Count > 0)
        {
            var query = from row in dt.AsEnumerable()
                        group row by new
                        {
                            SABBREVIATION = row["SABBREVIATION"] + string.Empty
                            ,
                            STERMINALID = row["STERMINALID"] + string.Empty
                        } into groupdata
                        orderby groupdata.Key.STERMINALID
                        select new
                        {
                            SABBREVIATION = groupdata.Key.SABBREVIATION,
                            STERMINALID = groupdata.Key.STERMINALID,
                        };
            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.STERMINALID))
                {
                    dtTerminal.Rows.Add(item.STERMINALID, item.SABBREVIATION);
                }
            }
        }
        return dtTerminal;
    }

    private void CheckBokInGrid(ASPxGridView xgvwCar, int index, DataRow dr)
    {
        if (dr["CAR_STATUS_ID"] + string.Empty == "1" && !(string.IsNullOrEmpty(dr["STERMINALID"] + string.Empty) || dr["STERMINALID"] + string.Empty == "--เลือก--"))
        {
            dr["IS_SELECT"] = "1";

        }
        else
        {
            dr["IS_SELECT"] = "0";
        }
    }

    private void ClearIVMS(DataRow dr)
    {
        dr["IVMS_GPS_STAT"] = string.Empty;
        dr["IVMS_MDVR_STAT"] = string.Empty;
        dr["IVMS_STATUS"] = string.Empty;
        dr["IVMS_RADIUS_KM"] = string.Empty;
        dr["IVMS_CHECKDATE"] = string.Empty;
    }

}//End Class