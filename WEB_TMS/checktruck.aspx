﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="checktruck.aspx.cs" 
    Inherits="checktruck" EnableViewState="false" StylesheetTheme="Aqua"%>
<%--<%@ Register Namespace="CuteWebUI" Assembly="CuteWebUI.AjaxUploader" TagPrefix="CuteWebUI" %><%@ Register Namespace="CuteWebUI" Assembly="CuteWebUI.AjaxUploader" TagPrefix="CuteWebUI" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .grvHeaderFlag {
            text-align: center !important;
        }
    </style>

    <style type="text/css">
        /*.datetimepick {
            background: url(../Images/ic_cst_calendar.gif) no-repeat;
            background-position-y:center;
            background-position-x:right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color:white;
        }*/
        .search {
            background: url(../Images/ic_search.gif) no-repeat;
            background-position-y:center;
            background-position-x:right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color:white;
        }
        .time {
            background: url(../Images/ico_time.png) no-repeat;
            background-position-y:center;
            background-position-x:right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color:white;
        }
        .inputfile {
            background: url(../Images/uploadfile.png) no-repeat;
            background-position-y:center;
            background-position-x:right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color:white;
        }
        .auto-style1 {
            height: 21px;
        }
        #file-input {
          cursor: pointer;
          outline: none;
          position: absolute;
          top: 0;
          left: 0;
          width: 0;
          height: 0;
          overflow: hidden;
          filter: alpha(opacity=0); /* IE < 9 */
          opacity: 0;
        }
        .input-label {
          cursor: pointer;
          position: relative;
          display: inline-block;
        }
    </style>
    <%--<link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <script type="text/javascript" src="js/file-upload.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function() {
            //$('.file-upload').file_upload();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" Runat="Server">
    <script type="text/javascript">
        function closeModal() {
            $('#myModal').modal('hide');
        }
        function closeModal2() {
            $('#myModal2').modal('hide');
        }
        function closeModal3() {
            $('#myModal3').modal('hide');
        }
    </script>
    <div class="panel-heading" style="width:98%;padding:10px;margin:10px;">
        <table>
            <tr>
                <td colspan="7">
                    <h3><asp:Label  CssClass="control-label"  runat="server" ID="lblHeader" Text="ตรวจสภาพรถระหว่างทาง(SeupriseCheck)"></asp:Label></h3>
                </td>
            </tr>
            <tr>
                <td style="width:12%">
                    <b>
                        <asp:Label CssClass="control-label" runat="server" ID="lblLabelHeaderNumber" Text="ทะเบียนหัว : "></asp:Label>
                    </b>
                </td>
                <td style="width:12%">
                    <asp:Label CssClass="control-label" runat="server" ID="lblHeaderNumber" Text=""></asp:Label>
                </td>
                <td style="width:12%">
                    <b>
                        <asp:Label CssClass="control-label" runat="server" ID="lblLabelTailerNumber" Text="ทะเบียนท้าย : "></asp:Label>
                    </b>
                </td>
                <td style="width:12%">
                    <asp:Label CssClass="control-label" runat="server" ID="lblTailerNumber" Text=""></asp:Label>
                </td>
                <td style="width:12%">
                    <b>
                        <asp:Label CssClass="control-label" runat="server" ID="lblLabelContactNumber" Text="เลขที่สัญญา : "></asp:Label>
                    </b>
                </td>
                <td style="width:12%">
                    <asp:Label CssClass="control-label" runat="server" ID="lblContactNumber" Text=""></asp:Label>
                </td>
                <td style="text-align:right;">
                    <asp:Label CssClass="control-label" runat="server" ID="lblCheckerDate" Text="" Font-Bold="true"></asp:Label>

                    <asp:HiddenField runat="server" ID="hidMCONTRACTID" />
                    <asp:HiddenField runat="server" ID="hidNSURPRISECHECKID" />
                    <asp:HiddenField runat="server" ID="hidDocID" />
                    <asp:HiddenField runat="server" ID="hidSVENDORID" />
                    <asp:HiddenField runat="server" ID="hidSVENDORNAME" />
                    <asp:HiddenField runat="server" ID="hidSTRUCKID" />
                </td>
            </tr>
        </table>        
    </div>

    <div class="panel panel-default" style="width: 98%; padding: 10px; margin: 10px">
            <div id="Tabs" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" id="Tab1" runat="server"><a href="#TransportIssue" data-toggle="tab"
                        aria-expanded="true" runat="server" id="GeneralTab">ปัญหาการขนส่ง</a></li>
                    <li class="" id="Tab2" runat="server"><a href="#IssueObject" data-toggle="tab" aria-expanded="false"
                        runat="server" id="ProcessTab">หลักฐานการกระทำผิดและปัญหาอื่นๆ</a></li>
                    <li class="" id="Tab3" runat="server" visible="false"><a href="#CheckHistory" data-toggle="tab" aria-expanded="false"
                        runat="server" id="FinalTab">ประวัติการตรวจ</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="padding-top: 20px">
                    <div role="tabpanel" class="tab-pane fade active in" id="TransportIssue">
                        <div class="table-responsive" style="align-content:center;">   
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table id="MainTable" runat="server">                   
                                    </table>  
                                    <table width="100%;">
                                        <tr>
                                            <td style="width:40%;align-items:center;">
                                                <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td style="width:10%;align-items:center;">
                                                <asp:Button runat="server" Width ="100" CssClass="btn btn-info" id="btnSave" text="บันทึก" OnClick="btnSave_Onclick"></asp:Button>    
                                            </td>
                                            <td style="width:10%;align-items:center;">                                        
                                                <asp:Button ID="btnClose" Width ="100" runat="server" Text="ปิด" CssClass="btn btn-info" OnClick="btnClose_Click"/>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr> 
                                    </table> 
                                    <asp:HiddenField ID="hidRowIndex" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                </Triggers>
                          </asp:UpdatePanel>
                        </div>   
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="IssueObject">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
                                <ContentTemplate>
                                  <table id="MainTable2" runat="server" width="100%"> 
                                        <tr>
                                            <td>
                                                <h4><asp:Label ID="Label1" runat="server" Text="ปัญหาอื่นๆ" ForeColor="Red"></asp:Label></h4>                                                
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtNMA" runat="server" Visible="false" Width="25px" Text="0" CssClass="dxeLineBreakFix"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:50%">
                                                <asp:CheckBox ID="cbxOtherIssue" runat="server" Text="ระบุปัญหา" OnCheckedChanged="CheckBox_OtherIssue_CheckedChanged " AutoPostBack="true" />
                                            </td>
                                            <td style="width:50%">
                                                <asp:Label ID="Label2" runat="server" Text="รายละเอียดของปัญหา"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtOtherIssue" runat="server" TextMode="MultiLine" Width="90%" Height="200px" Enabled="False"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDetailIssue" runat="server" TextMode="MultiLine"  Width="90%" Height="200px" Enabled="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <h4><asp:Label ID="Label5" runat="server" Text="หลักฐานการกระทำผิด" ForeColor="Red"></asp:Label></h4>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table border="0" Width="100%">
                                                  <tr>
                                                      <td align="center">
                                                          <h5><asp:Label ID="Label3" runat="server" Text="ภาพถ่ายบริเวณที่เกิดปัญหา"></asp:Label></h5>
                                                      </td>
                                                      <td align="center">
                                                          <h5><asp:Label ID="Label4" runat="server" Text="คำอธิบายเพิ่มเติม"></asp:Label></h5>
                                                      </td>
                                                      <td>

                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>
                                                          <asp:FileUpload ID="fileUpload1" runat="server" Width="100%"/> 
                                                          <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="fileUpload" ErrorMessage="file type.jpg,.jpeg,.jpe,.gif are allowed"  
                                                                ValidationExpression="(.*\.([Jj][Pp][Gg])|.*\.([Jj][Pp][Ee][Gg])|.*\.([Jj][Pp][Ee])|.*\.([Gg][Ii][Ff])$)">  
                                                        </asp:RegularExpressionValidator>  
                                                      </td>
                                                      <td style="vertical-align:central;">
                                                          <div>
                                                              <div>
                                                                    <asp:TextBox runat="server" ID="TextBox1" Width="100%" Height="30">
                                                                    </asp:TextBox>
                                                              </div>
                                                          </div>                                                                        
                                                      </td>
                                                      <td>
                                                          <asp:Button  runat="server" id="Button1" Text="เพิ่ม" OnClick="btnAdd22_Onclick" CssClass="btn btn-info"/>
                                                      </td>
                                                  </tr>
                                              </table>
                                              <asp:GridView BorderWidth="0" ID="gvw_AddCheckList_tab2" runat="server" AutoGenerateColumns ="false" CssClass="table table-primary" 
                                                  ShowFooter="false" ShowHeader="false" OnRowCommand="gvw_AddCheckList_tab2_RowCommand" OnRowDataBound="gvw_AddCheckList_tab2_RowDataBound">
                                                  <Columns>
                                                       <asp:ImageField DataImageUrlField="imagename" ControlStyle-Height="100" ControlStyle-Width="100" />
                                                       <asp:BoundField DataField="filename"/>
                                                       <asp:BoundField DataField="memo"/>   
                                                       <asp:ButtonField Text="ลบ" CommandName="del"/>
                                                  </Columns>
                                              </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width:40%;">
                                                             <asp:Label ID="Label8" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td style="width:10%;">
                                                            <asp:Button ID="btnSaveOtherIssue" Width="100" runat="server" Text="บันทึก"  CssClass="btn btn-info" OnClick="btnSaveOtherIssue_Click"/>
                                                        </td>
                                                        <td style="width:10%;">
                                                            <asp:Button ID="btnCancelOtherIssue" Width="100"  runat="server" Text="ปิด"  CssClass="btn btn-info" OnClick="btnCloseCancelApprove_Click"/>
                                                        </td>
                                                        <td style="width:40%;">
                                                             <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>                                  
                                            
                                        </tr>
                                    </table>     
                                </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="Button1" />
                            </Triggers>
                        </asp:UpdatePanel>    
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="CheckHistory">
                        <div class="table-responsive">   
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvwHistory" runat="server" CssClass="table table-primary"  
                                    AutoGenerateColumns="False" Width="100%" AllowPaging="true" 
                                    ShowFooter="false"
                                    CellPadding="4" ForeColor="Black" GridLines="Both"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                    OnRowDataBound="gvwHistory_RowDataBound" 
                                    OnPageIndexChanging="gvwHistory_PageIndexChanging" OnRowCreated="gvwHistory_RowCreated">
                                        <Columns>
                                            <asp:BoundField HeaderText="วันที่ตรวจ" DataField="DCheck" HeaderStyle-BackColor="#c6defb"/>
                                            <asp:BoundField HeaderText="ประเภทรถ" DataField="CARType" HeaderStyle-BackColor="#c6defb"/>
                                            <asp:BoundField HeaderText="ผู้ตรวจ" DataField="SSURPRISECHECKBY" HeaderStyle-BackColor="#c6defb"/>
                                            <asp:BoundField HeaderText="สถานะการแก้ไข" DataField="Checker" HeaderStyle-BackColor="#c6defb"/>
                                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-BackColor="#c6defb">
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>                             
                        </div>   
                    </div>
                </div>
                
            </div>
      </div>

     <!-- Modal-->
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updMyModal" runat="server" >
                <ContentTemplate>              
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4><asp:Label runat="server" ID="lblCheckerItems" Text="บันทึก"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                        <asp:Label ID="lblMainHeader" runat="server" Text="Step 1:(ทั้งหมด) แจ้งตรวจพบปัญหา" ForeColor="Red"></asp:Label>
                          <table border="0" Width="100%">
                              <tr>
                                  <td align="center">
                                      <h5><asp:Label ID="lblHeader1" runat="server" Text="ภาพถ่ายบริเวณที่เกิดปัญหา"></asp:Label></h5>
                                  </td>
                                  <td align="center">
                                      <h5><asp:Label ID="lblHeader2" runat="server" Text="คำอธิบายเพิ่มเติม"></asp:Label></h5>
                                  </td>
                                  <td>

                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <%-- <div id="UploadFileDIV" class="form-group" style="width:100%">
                                          <div id="colDIV" class="col-sm-offset-2 col-sm-10">
                                              <label class="file-upload btn btn-primary" id="input-label">
                                                  เลือกไฟล์ <input type="file" accept=".jpg,.jpeg,.jpe,.gif" onclick="UploadFile_OnClick" id="fileUpload" runat="server"/>
                                              </label>
                                          </div>
                                      </div>--%>
                                     <asp:FileUpload ID="fileUpload" runat="server" Width="100%"/>  <%--accept=".jpg,.jpeg,.jpe,.gif"--%>
                                      <asp:RegularExpressionValidator ID="regexValidator" runat="server" ControlToValidate="fileUpload" ErrorMessage="file type.jpg,.jpeg,.jpe,.gif are allowed" ForeColor="Red" 
                                            ValidationExpression="(.*\.([Jj][Pp][Gg])|.*\.([Jj][Pp][Ee][Gg])|.*\.([Jj][Pp][Ee])|.*\.([Gg][Ii][Ff])$)">  
                                    </asp:RegularExpressionValidator>  
                                  </td>
                                  <td style="vertical-align:central;">
                                      <div>
                                          <div>
                                                <asp:TextBox runat="server" ID="txtMemo" Width="100%" Height="30">
                                                </asp:TextBox>
                                          </div>
                                      </div>                                                                        
                                  </td>
                                  <td>
                                      <asp:Button  runat="server" id="btnAdd" Text="เพิ่ม" OnClick="btnAdd_Onclick" CssClass="btn btn-info"/>
                                  </td>
                              </tr>
                          </table>

                                 <asp:GridView BorderWidth="0" ID="gvw_AddCheckList" runat="server" AutoGenerateColumns ="false" CssClass="table table-primary" 
                              ShowFooter="false" ShowHeader="false" OnRowCommand="gvw_AddCheckList_RowCommand" OnRowDataBound="gvw_AddCheckList_RowDataBound">
                              <Columns>
                                  <%--<asp:TemplateField>
                                      <ItemTemplate>
                                          <asp:Image runat="server" Height="100" Width="100" ImageUrl="<%=imagename %>"></asp:Image>
                                          <asp:Label ID="lblDescript" runat="server" Text="<%=IssueDescript %>" ForeColor="Red"></asp:Label>
                                      </ItemTemplate>                                     
                                  </asp:TemplateField>--%> 
                                   <asp:ImageField DataImageUrlField="imagename" ControlStyle-Height="100" ControlStyle-Width="100" />
                                   <asp:BoundField DataField="filename"/>
                                   <asp:BoundField DataField="memo"/>   
                                   <asp:ButtonField Text="ลบ" CommandName="del"/>
                              </Columns>
                          </asp:GridView>

                        </div>
                        <div class="modal-footer">
                          <%--<asp:Button runat="server" CssClass="btn btn-info" id="btnSave" text="บันทึก" OnClick="btnSave_Onclick" ></asp:Button>  --%>            
                          <asp:Button runat="server" CssClass="btn btn-info" id="btnConfirmCancel" text="ปิด" OnClientClick="closeModal();"></asp:Button>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnAdd" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="myModal2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updMyModal2" runat="server">
                <ContentTemplate>              
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4><asp:Label runat="server" ID="lblCheckerItems2" Text="บันทึก"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                        <asp:Label ID="lblMainHeader2" runat="server" Text="Step 2: (ผู้ขนส่ง) แจ้งผลการแก้ไข" ForeColor="Red"></asp:Label>
                          <asp:GridView BorderWidth="0" ID="gvw_AddCheckList2_Main" runat="server" AutoGenerateColumns ="false" CssClass="table table-primary" 
                              ShowFooter="false" ShowHeader="false" OnRowCommand="gvw_AddCheckList2_Main_RowCommand" OnRowDataBound="gvw_AddCheckList2_Main_RowDataBound">
                              <Columns>
                                   <asp:ImageField DataImageUrlField="imagename" ControlStyle-Height="100" ControlStyle-Width="100" />
                                   <asp:BoundField DataField="memo"/>   
                                   <asp:ButtonField Text="เลือก" CommandName="select" />
                              </Columns>
                          </asp:GridView>
                          <table border="0" Width="100%">
                              <tr>
                                  <td colspan="2">
                                      <h4><asp:Label ID="Label10" runat="server" Text="ภาพที่เลือก" ForeColor="Red"></asp:Label></h4>
                                  </td>
                              </tr>
                              <tr>
                                  <td align="center">
                                      <h5><asp:Label ID="lblHeader1_2" runat="server" Text="ภาพถ่ายบริเวณที่เกิดปัญหา"></asp:Label></h5>
                                  </td>
                                  <td align="center">
                                      <h5><asp:Label ID="lblHeader2_2" runat="server" Text="คำอธิบายเพิ่มเติม"></asp:Label></h5>
                                  </td>
                                  <td>

                                  </td>
                              </tr>
                              <tr>
                                  <td style="width:300px;align-content:center;">
                                      <asp:TextBox ID="txtdtID" runat="server" Visible="false"></asp:TextBox>
                                      <asp:Image ID="Image_show" runat="server" Height="100" Width="100"/>
                                  </td>
                                  <td colspan="2">
                                      <asp:TextBox ID="Memo_show" runat="server" TextMode="MultiLine" ReadOnly="true" Height="100" Width="100%"></asp:TextBox>
                                  </td>
                              </tr>
                              <tr>
                                  <td align="center">
                                      <h5><asp:Label ID="Label11" runat="server" Text="ภาพถ่ายการแก้ไขบริเวณที่เกิดปัญหา"></asp:Label></h5>
                                  </td>
                                  <td align="center">
                                      <h5><asp:Label ID="Label12" runat="server" Text="คำอธิบายเพิ่มเติม"></asp:Label></h5>
                                  </td>
                                  <td>

                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                     <asp:FileUpload ID="fileUpload2" runat="server" Width="100%" Enabled="false"/> 
                                      <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="fileUpload2" ErrorMessage="file type.jpg,.jpeg,.jpe,.gif are allowed" ForeColor="Red"  
                                            ValidationExpression="(.*\.([Jj][Pp][Gg])|.*\.([Jj][Pp][Ee][Gg])|.*\.([Jj][Pp][Ee])|.*\.([Gg][Ii][Ff])$)">  
                                    </asp:RegularExpressionValidator>  
                                  </td>
                                  <td style="vertical-align:central;">
                                      <div>
                                          <div>
                                                <asp:TextBox runat="server" ID="txtMemo2" Width="100%" Height="30" Enabled="false">
                                                </asp:TextBox>
                                          </div>
                                      </div>                                                                        
                                  </td>
                                  <td>
                                      <asp:Button  runat="server" id="btnAdd2" Text="เพิ่ม" OnClick="btnAdd2_Onclick" CssClass="btn btn-info" Enabled="false"/>
                                  </td>
                              </tr>
                          </table>
                          <asp:GridView BorderWidth="0" ID="gvw_AddCheckList2" runat="server" AutoGenerateColumns ="false" CssClass="table table-primary" 
                              ShowFooter="false" ShowHeader="false" OnRowCommand="gvw_AddCheckList2_RowCommand" OnRowDataBound="gvw_AddCheckList2_RowDataBound">
                              <Columns>
                                   <asp:BoundField DataField="dtID" Visible="false"/>
                                   <asp:ImageField DataImageUrlField="imagename" ControlStyle-Height="100" ControlStyle-Width="100" />
                                   <asp:BoundField DataField="filename"/>
                                   <asp:BoundField DataField="memo"/>   
                                   <asp:ButtonField Text="ลบ" CommandName="del"/>
                              </Columns>
                          </asp:GridView>
                        </div>
                        <div class="modal-footer">
                          <%--<asp:Button runat="server" CssClass="btn btn-info" id="btnSave2" text="บันทึก" OnClick="btnSave2_Onclick" ></asp:Button>              --%>
                          <asp:Button runat="server" CssClass="btn btn-info" id="btnConfirmCancel2" text="ปิด" OnClientClick="closeModal2();"></asp:Button>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnAdd2" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="myModal3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updMyModal3" runat="server" >
                <ContentTemplate>              
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4><asp:Label runat="server" ID="lblCheckerItems3" Text="บันทึก"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                        <asp:Label ID="lblMainHeader3" runat="server" Text="(ปง. และคลัง) ตรวจสอบผลแก้ไข" ForeColor="Red" ></asp:Label>
                          <table border="0" Width="100%">
                              <tr>
                                  <td width='50%' align='center' bgcolor='#CCCCCC'>
                                         ภาพถ่ายบริเวณที่เกิดปัญหา (ก่อนการแก้ไข)
                                  </td>
                                  <td width='50%' align='center' bgcolor='#CCCCCC'>
                                         ภาพถ่ายบริเวณที่เกิดปัญหา (หลังการแก้ไข)
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                  </td>
                              </tr>
                              <asp:Literal ID="lblPicture" runat="server"></asp:Literal>
                              <tr>
                                  <td>
                                  </td>
                              </tr>
                              <tr>
                                  <td class="active">
                                       บันทึกผลการตรวจสอบการแก้ไข
                                  </td>
                                  <td align="right">
                                  </td>
                              </tr>
                              <tr>
                                  <td colspan="2">
                                      <div style="float: left; width: 20%">
                                           ตรวจสอบแล้วมีผลดังนี้</div>
                                           <div style="float: left; width: 30%">
                                               <asp:RadioButtonList ID="rblStatus" runat="server">
                                                   <Items>
                                                       <asp:ListItem Text="อนุมัติผลการแก้ไข" Value="1" Selected="true" />
                                                       <asp:ListItem Text="ไม่อนุมัติ" Value="0" />
                                                   </Items>
                                               </asp:RadioButtonList>
                                           </div>
                                           <div style="float: left; width: 50%">
                                               <asp:TextBox ID="txtRemark" runat="server" Width=""></asp:TextBox>
                                           </div>
                                       </td>
                                  </tr>
                          </table>
                        </div>
                        <div class="modal-footer">
                          <asp:Button runat="server" CssClass="btn btn-info" id="btnSave3" text="บันทึก" OnClick="btnSave3_Onclick" ></asp:Button>              
                          <asp:Button runat="server" CssClass="btn btn-info" id="btnClose3" text="ปิด" OnClientClick="closeModal3();"></asp:Button>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnAdd" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>


