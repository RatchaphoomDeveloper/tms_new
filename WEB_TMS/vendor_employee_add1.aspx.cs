﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using DevExpress.Web.ASPxUploadControl;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Configuration;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using System.Text;
using System.Web.UI.HtmlControls;
using EmailHelper;
using TMS_BLL.Transaction.Report;
using System.Web.Security;
using System.Drawing;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

public partial class vendor_employee_add1 : PageBase
{
    #region Member
    private string SEMPLOYEEID
    {
        get
        {
            if ((string)ViewState["SEMPLOYEEID"] != null)
                return (string)ViewState["SEMPLOYEEID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SEMPLOYEEID"] = value;
        }
    }
    private string REQ_ID
    {
        get
        {
            if ((string)ViewState["REQ_ID"] != null)
                return (string)ViewState["REQ_ID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["REQ_ID"] = value;
        }
    }
    string mode;
    private string EMPSAPID
    {
        get
        {
            if ((string)ViewState["EMPSAPID"] != null)
                return (string)ViewState["EMPSAPID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["EMPSAPID"] = value;
        }
    }
    private string BackUrl
    {
        get
        {
            if ((string)ViewState["BackUrl"] != null)
                return (string)ViewState["BackUrl"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["BackUrl"] = value;
        }
    }
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory5 = "UploadFile/EmpployeeDoc_TEMP/Temp/{0}/{2}/{1}/";
    //อับโลหดไฟล์ไปที่Save
    const string SaverDirectory5 = "UploadFile/EmpployeeDoc_TEMP/PictureEMP/{0}/{2}/{1}/";
    //อับโลหดไฟล์History
    const string HistoryDirectory5 = "UploadFile/EmpployeeDoc_TEMP/History/PictureEMP/{0}/{2}/{1}/";

    private List<ListGridDoc> listGriddoc { get { return SystemFunction.ConvertObject<ListGridDoc>(Session["listGriddoc"]); } set { Session["listGriddoc"] = value; } }

    string SMENUID = "47";

    string MessSAPSuccess = "ข้อมูลพนักงานใน SAP สำเร็จ<br/ >";
    string MessSAPFail = "<span style=\"color:Red;\">{0}ข้อมูลพนักงานใน SAP ไม่สำเร็จ !!!{1}</span><br/ >";
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtUpload2
    {
        get
        {
            if ((DataTable)ViewState["dtUpload2"] != null)
                return (DataTable)ViewState["dtUpload2"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload2"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtUploadType2
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType2"] != null)
                return (DataTable)ViewState["dtUploadType2"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType2"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }
    private DataTable dtRequestFile2
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile2"] != null)
                return (DataTable)ViewState["dtRequestFile2"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile2"] = value;
        }
    }
    [Serializable]
    class ListGridDoc
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }

    }

    bool P4
    {
        get
        {
            if (ViewState["P4"] != null)
                return (bool)ViewState["P4"];
            else
                return false;
        }
        set
        {
            ViewState["P4"] = value;
        }
    }
    bool PK
    {
        get
        {
            if ((bool)ViewState["PK"] != null)
                return (bool)ViewState["PK"];
            else
                return false;
        }
        set
        {
            ViewState["PK"] = value;
        }
    }
    bool SaveDraft
    {
        get
        {
            if (ViewState["SaveDraft"] != null)
                return (bool)ViewState["SaveDraft"];
            else
                return false;
        }
        set
        {
            ViewState["SaveDraft"] = value;
        }
    }

    bool SaveEdit
    {
        get
        {
            if (ViewState["SaveEdit"] != null)
                return (bool)ViewState["SaveEdit"];
            else
                return false;
        }
        set
        {
            ViewState["SaveEdit"] = value;
        }
    }

    bool editStatus
    {
        get
        {
            if ((bool)ViewState["editStatus"] != null)
                return (bool)ViewState["editStatus"];
            else
                return false;
        }
        set
        {
            ViewState["editStatus"] = value;
        }
    }
    bool allowEditStatus
    {
        get
        {
            if ((bool)ViewState["allowEditStatus"] != null)
                return (bool)ViewState["allowEditStatus"];
            else
                return false;
        }
        set
        {
            ViewState["allowEditStatus"] = value;
        }
    }
    string RemarkStatus
    {
        get
        {
            if ((string)ViewState["RemarkStatus"] != null)
                return (string)ViewState["RemarkStatus"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["RemarkStatus"] = value;
        }
    }
    #endregion

    #region Page_Load

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            #region เช็ค Permission

            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            #endregion

            InitialUpload();
            InitialUpload2();
            SetDrowDownList();
            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");

            // setdata();
            imgEmp.ImageUrl = "images/Avatar.png";

            rblStatus.SelectedIndex = 0;
            trWait.Style.Add("display", "none");
            trstatusWait.Style.Add("display", "none");
            trCancel.Style.Add("display", "none");
            trsCancelstatus.Style.Add("display", "none");
            //chkNotfill.Visible = false;
            string str = string.Empty;
            string strQuery;
            string VenId = "";
            mode = Request.QueryString["mode"];

            string getPageBack = Request.QueryString["page"];
            BackUrl = "~/vendor_employee.aspx";
            if (getPageBack == "expire")
            {
                BackUrl = "~/EmployeeExpire.aspx";
            }

            P4 = PK = editStatus = false;
            allowEditStatus = false;

            if (Request.QueryString["p4"] != null)
            {
                str = Request.QueryString["p4"];
                P4 = true;
                BackUrl = "~/Vendor_Detail.aspx";
                if (Session["SVDID"] + string.Empty == ConfigValue.DeliveryDepartmentCode)//"80000176"//ConfigValue.DeliveryDepartmentCode
                {
                    P4 = false;
                    PK = true;
                    BackUrl = "~/approve_pk.aspx";
                }
            }
            else if (Request.QueryString["pk"] != null)
            {
                str = Request.QueryString["pk"];
                PK = true;
                BackUrl = "~/approve_pk.aspx";
                if (Session["SVDID"] + string.Empty != ConfigValue.DeliveryDepartmentCode)
                {
                    P4 = true;
                    PK = false;
                    BackUrl = "~/Vendor_Detail.aspx";
                }
            }
            else if (Request.QueryString["str"] != null)
            {
                str = Request.QueryString["str"];
                if (!string.IsNullOrEmpty(str))
                {
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    strQuery = Encoding.UTF8.GetString(decryptedBytes);
                    SEMPLOYEEID = strQuery;
                    VenId = strQuery;
                }
                tdSaveDraft.Visible = false;
                tdNoApprove.Visible = false;
                tdEdit.Visible = false;
                tdApprove.Visible = false;
                divDescription.Style.Add("display", "none");
                divEmty.Attributes.Remove("class");
                divEmty.Attributes.Add("class", "col-md-4");
                if (Session["CGROUP"] + string.Empty == "0")
                {
                    tbStatus.Style.Add("display", "none");
                }
                if (Session["CGROUP"] + string.Empty == "0")
                {
                    if (!CanWrite)
                    {
                        mode = "view";
                        plStatus.Visible = false;
                    }
                }
            }
            else if (Request.QueryString["editStatus"] != null)
            {
                str = Request.QueryString["editStatus"];
                editStatus = true;
                if (!string.IsNullOrEmpty(str))
                {
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    strQuery = Encoding.UTF8.GetString(decryptedBytes);
                    SEMPLOYEEID = strQuery;
                    VenId = strQuery;
                }
                tdSaveDraft.Visible = false;
                tdNoApprove.Visible = false;
                tdEdit.Visible = false;
                tdApprove.Visible = false;
                divEmty.Attributes.Add("class", "col-md-4");

                mode = "view";
            }
            else
            {
                editStatus = false;
                PK = false;
                P4 = false;
                tdSaveDraft.Visible = false;
                tdNoApprove.Visible = false;
                tdEdit.Visible = false;
                tdApprove.Visible = false;
                divEmty.Attributes.Add("class", "col-md-4");
                BackUrl = "~/vendor_employee.aspx";
                //plStatus.Enabled = false;
            }

            ////////////////////////
            if (P4)
            {
                if (Request.QueryString["strID"] != null)
                {
                    BackUrl = "~/vendor_request.aspx";
                }
                if (!string.IsNullOrEmpty(str))
                {
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    strQuery = Encoding.UTF8.GetString(decryptedBytes);
                    if (strQuery != "NEW")
                    {
                        SEMPLOYEEID = strQuery;
                    }
                    //BackUrl = strQuery[1];
                }
                tdSaveDraft.Visible = false;
                tdNoApprove.Visible = false;
                tdEdit.Visible = false;
                tdApprove.Visible = false;
                divEmty.Attributes.Remove("class");
                divEmty.Attributes.Add("class", "col-md-4");
                txtDescription.Enabled = false;
            }
            else if (PK)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    strQuery = Encoding.UTF8.GetString(decryptedBytes);

                    SEMPLOYEEID = strQuery;
                    VenId = strQuery;
                }
                tdSave.Visible = false;

                divEmty.Attributes.Remove("class");
                divEmty.Attributes.Add("class", "col-md-1");
            }
            ////////////////////
            if (Request.QueryString["allowEditStatus"] != null)
            {
                string getAllow = Request.QueryString["allowEditStatus"];
                var decryptedBytes = MachineKey.Decode(getAllow, MachineKeyProtection.All);
                strQuery = Encoding.UTF8.GetString(decryptedBytes);
                if (strQuery == "true")
                {
                    allowEditStatus = true;
                }
            }
            ///////////////////////

            if (!string.IsNullOrEmpty(SEMPLOYEEID))
            {
                txtPercode.Enabled = false;
                //txtdrivelicence.Enabled = false;
                //txtNumAccident.Enabled = false;
                SetData();

                //chkNotfill.Visible = true;
                if (!P4)
                {
                    divDescription.Style.Add("display", "none");
                    trHistoryEmp.Style.Remove("display");
                    if (PK)
                    {
                        divDescription.Style.Remove("display");
                        txtDescription.Enabled = true;
                        if (Session["CGROUP"] + string.Empty == "0")
                        {
                            tbStatus.Style.Add("display", "none");
                        }
                        trHistoryEmp.Style.Add("display", "none");
                    }
                    txtdrivelicence.Enabled = false;
                    txtNumAccident.Enabled = false;
                    trUpdateUser.Style.Remove("display");
                }
                else
                {
                    if (Session["CGROUP"] + string.Empty == "0")
                    {
                        tbStatus.Style.Add("display", "none");
                    }
                    //trUpdateUser.Style.Add("display", "none");
                }
                if (allowEditStatus)
                {
                    if (Session["CGROUP"] + string.Empty == "0")
                    {
                        tbStatus.Style.Remove("display");
                    }
                }
            }
            else
            {
                divDescription.Style.Add("display", "none");
                trUpdateUser.Style.Add("display", "none");
                if (Session["CGROUP"] + string.Empty == "0")
                {
                    tbStatus.Style.Add("display", "none");
                }
                if (Session["CGROUP"] + string.Empty == "0")
                {
                    cboVendor.Value = Session["SVDID"];
                    cboVendor_SelectedIndexChanged(null, null);
                    cboVendor.Enabled = false;
                    if (!CanWrite)
                    {
                        plStatus.Visible = false;
                    }
                    //mode = "view";
                }
            }

            /////////////
            InitialRequireField();
            /////////////

            string status = string.Empty;
            if (Request.QueryString["status"] != null)
            {
                status = Request.QueryString["status"];
                var decryptedBytes = MachineKey.Decode(status, MachineKeyProtection.All);
                strQuery = Encoding.UTF8.GetString(decryptedBytes);
                if (!strQuery.Equals("0") && !strQuery.Equals("3"))
                {
                    mode = "view";
                }
                //BackUrl = "~/vendor_request.aspx";
            }

            if (!string.IsNullOrEmpty(mode) && mode == "view")
            {
                plInformation.Enabled = false;
                plDocument.Enabled = false;
                plStatus.Enabled = false;
                plImage.Enabled = false;
                fileUpload.Enabled = false;
                tdSave.Visible = false;
                tdSaveDraft.Visible = false;
                tdNoApprove.Visible = false;
                tdEdit.Visible = false;
                tdApprove.Visible = false;

                dgvUploadFile.Columns[5].Visible = false;
                dgvUploadFile2.Columns[8].Visible = false;

                divEmty.Attributes.Remove("class");
                divEmty.Attributes.Add("class", "col-md-5");
                if (editStatus)
                {
                    divEmty.Attributes.Remove("class");
                    divEmty.Attributes.Add("class", "col-md-4");
                    tdSave.Visible = true;

                    if (Session["CGROUP"] + string.Empty != "0")
                    {
                        tbStatus.Style.Remove("display");
                        plStatus.Enabled = true;
                    }

                }
                //if (P4)
                //{
                //    BackUrl = "~/vendor_request.aspx";
                //}
            }
            if (Request.QueryString["str"] != null)
            {
                plStatus.Enabled = false;
                //tbStatus.Style.Add("display", "none");
            }
            txtSettleTo.Enabled = false;
            if (Session["SVDID"] + string.Empty == ConfigValue.DeliveryDepartmentCode)
            {
                txtSettleTo.Enabled = true;
            }

            if (Request.QueryString["bl"] != null)
            {
                BackUrl = "~/vendor_employee.aspx?bl=" + Request.QueryString["bl"];
            }

            this.AssignAuthen();
        }
        else
        {
            cboVendor.DataSource = ViewState["DataVendor"];
            cboVendor.DataBind();

            cboMainPlant.DataSource = ViewState["DataTTERMINAL"];
            cboMainPlant.DataBind();
            if (!string.IsNullOrEmpty(txtEMPSysfilename.Text.Trim()))
            {
                imgEmp.ImageUrl = txtEMPTruePath.Text.Trim() + txtEMPSysfilename.Text.Trim();
            }

        }

    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnUpload.Enabled = false;
                btnEdit.Disabled = true;
                btnApprove.Disabled = true;
                btnNoApprove.Disabled = true;
                btnSaveDraft.Disabled = true;
                btnSave.Disabled = true;

                //btnSave.Disabled = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {

        ddlPosition.DataTextField = "PERSON_TYPE_DESC";
        ddlPosition.DataValueField = "PERSON_TYPE";
        ViewState["DataPosition"] = VendorBLL.Instance.PositionSelect();
        ddlPosition.DataSource = ViewState["DataPosition"];
        ddlPosition.DataBind();
        ddlPosition.Items.Insert(0, new ListItem("-เลือกตำแหน่ง-", "0"));
        ddlPosition.SelectedIndex = 0;

        ddlTitle.DataTextField = "TITLE_NAME";
        ddlTitle.DataValueField = "TITLE_ID";
        ddlTitle.DataSource = VendorBLL.Instance.TitleSelect();
        ddlTitle.DataBind();
        ddlTitle.Items.Insert(0, new ListItem("-เลือกคำนำหน้าชื่อ-", null));
        ddlTitle.SelectedIndex = 0;

        ddlStatus.DataTextField = "STATUS_TYPE";
        ddlStatus.DataValueField = "STATUS_ID";
        ddlStatus.DataSource = VendorBLL.Instance.StatusSelect();
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem("-เลือกประเภทการระงับ-", null));
        ddlStatus.SelectedIndex = 0;

        ddlStatus2.DataTextField = "STATUS_TYPECANCEL";
        ddlStatus2.DataValueField = "STATUS_IDCANCEL";
        ddlStatus2.DataSource = VendorBLL.Instance.StatusCancelSelect();
        ddlStatus2.DataBind();
        ddlStatus2.Items.Insert(0, new ListItem("-เลือกประเภทการยกเลิกใช้งาน-", null));
        ddlStatus2.SelectedIndex = 0;

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        cboVendor.DataSource = ViewState["DataVendor"];
        cboVendor.DataBind();

        ViewState["DataTTERMINAL"] = ContractBLL.Instance.TTERMINALSelect();
        cboMainPlant.DataSource = ViewState["DataTTERMINAL"];
        cboMainPlant.DataBind();
    }
    #endregion

    //    #region อัพโหลดเอกสารอื่นๆ
    //    #region แนบเอกสารทะเบียนบ้าน
    //    #region แนบเอกสารใบขับขี่ประเภท4
    //#region แนบเอกสารสำคัญอื่นๆ
    //    #region แนบเอกสารสำเนาบัตรประชาชน

    #region รูปประจำตัว

    protected void uploadEMP_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFileEmp(e.UploadedFile, genName, string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;
                //  ShowEmpImage(data2, genName + "." + _Filename[count]);
                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                listGriddoc = new List<ListGridDoc>();
                listGriddoc.Add(new ListGridDoc
                {
                    SFILENAME = e.UploadedFile.FileName + "",
                    SSYSFILENAME = genName + "." + _Filename[count] + "",
                    SDESCRIPTION = "รูปประจำตัว",
                    SPATH = data2
                });
                txtEMPFileName.Text = e.UploadedFile.FileName;
                txtEMPTruePath.Text = data2;
                txtEMPSysfilename.Text = genName + "." + _Filename[count] + "";
            }
        }
        else
        {

            return;

        }

    }

    private bool UploadFileEmp(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    #endregion

    #region UpdateEmpSAPID
    private void UpdateEmpSAPID(string EMPID, string PerCode, string EmpSAPID)
    {
        VendorBLL.Instance.EMPSAPIDUpdate(EMPID, PerCode, EmpSAPID);
    }
    #endregion

    #region ddlPosition
    protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitialRequireField();
        //if (ddlPosition.SelectedValue == "11")
        //{
        //    div11.Style.Remove("display");
        //}
        //else
        //{
        //    div11.Style.Add("display", "none");
        //}
    }

    private void ClearRequire()
    {
        txtName.Enabled = true;
        txtSurName.Enabled = true;

        lblReqtxtName.Visible = false;
        lblReqtxtSurName.Visible = false;

    }

    private void InitialRequireField()
    {
        try
        {
            this.ClearRequire();

            DataTable dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(ddlPosition.SelectedValue.ToString()), "VENDOR");

            object tmp;
            Label tmpReq;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                tmp = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                if (tmp != null)
                {
                    tmpReq = (Label)this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID_REQ"].ToString());

                    if (tmpReq != null)
                    {
                        if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                        {//Require Field
                            tmpReq.Visible = true;
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "2"))
                        {//Optional Field
                            tmpReq.Visible = false;
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "3"))
                        {//Disable Field
                            Type = tmp.GetType().ToString();
                            if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                            {
                                DropDownList ddl = (DropDownList)tmp;
                                ddl.SelectedIndex = 0;
                                ddl.Enabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                            {
                                TextBox txt = (TextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Enabled = false;
                            }
                        }
                    }
                }
            }

            tmp = string.Empty;

            dtRequestFile = VendorBLL.Instance.VendorRequestFile_Doc(ddlPosition.SelectedValue, "0");
            if (dtRequestFile.Rows.Count > 0)
            {
                tmp = dtRequestFile.Rows[0]["PERSON_TYPE_DESC"].ToString();

                for (int i = 1; i < dtRequestFile.Rows.Count; i++)
                {
                    if (string.Equals(dtRequestFile.Rows[i]["PERSON_TYPE_DESC"].ToString(), tmp))
                        dtRequestFile.Rows[i]["PERSON_TYPE_DESC"] = string.Empty;
                    else
                        tmp = dtRequestFile.Rows[i]["PERSON_TYPE_DESC"].ToString();
                }

            }
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            //------------------------

            tmp = string.Empty;
            dtRequestFile2 = VendorBLL.Instance.VendorRequestFile_Doc(ddlPosition.SelectedValue, "1");
            if (dtRequestFile2.Rows.Count > 0)
            {
                tmp = dtRequestFile2.Rows[0]["PERSON_TYPE_DESC"].ToString();

                for (int i = 1; i < dtRequestFile2.Rows.Count; i++)
                {
                    if (string.Equals(dtRequestFile2.Rows[i]["PERSON_TYPE_DESC"].ToString(), tmp))
                        dtRequestFile2.Rows[i]["PERSON_TYPE_DESC"] = string.Empty;
                    else
                        tmp = dtRequestFile2.Rows[i]["PERSON_TYPE_DESC"].ToString();
                }

            }
            GridViewHelper.BindGridView(ref dgvRequestFile2, dtRequestFile2);

            this.CheckRequestFile();

        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    #endregion

    #region Validate
    private string Validate()
    {
        string mess = string.Empty;
        try
        {
            if (ddlPosition.SelectedIndex == 0)
            {
                mess += "กรุณาเลือก ตำแหน่ง !!!<br/>";
                return mess;
            }
            if (radSpot.SelectedIndex < 0)
            {
                mess += "กรุณาเลือก ประเภทการจ้างงาน (สัญญา, จ้างพิเศษ) !!!<br/>";
                return mess;
            }
            if (ddlContractSpot.Enabled && ddlContractSpot.SelectedIndex < 1)
            {
                mess += "กรุณาเลือก เลขที่สัญญาจ้างพิเศษ !!!<br/>";
                return mess;
            }
            if (!string.IsNullOrEmpty(txtMail.Text) && !IsValidEmail(txtMail.Text))
            {
                mess += "อีเมล์หลัก รูปแบบไม่ถูกต้อง !!!<br/>";
            }
            if (!string.IsNullOrEmpty(txtMail2.Text) && !IsValidEmail(txtMail2.Text))
            {
                mess += "อีเมล์สำรอง รูปแบบไม่ถูกต้อง !!!<br/>";
            }
            DateTime dedtbirthday;
            DateTime date;
            if (DateTime.TryParseExact(dedtBirthDay.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
            {
                dedtbirthday = date;
                DataRow[] drs = ((DataTable)ViewState["DataPosition"]).Select("PERSON_TYPE = '" + ddlPosition.SelectedValue + "'");
                if (drs.Any())
                {
                    DataRow dr = drs[0];
                    int AgeMin, AgeMax, Age;
                    if (dedtbirthday > DateTime.Now)
                    {
                        mess += "วันเกิดต้องน้อยกว่าหรือเท่ากับวันปัจจุบัน !!!";
                    }
                    else if (!dr.IsNull("AGEMIN") && int.TryParse(dr["AGEMIN"] + string.Empty, out AgeMin) && !dr.IsNull("AGEMAX") && int.TryParse(dr["AGEMAX"] + string.Empty, out AgeMax))
                    {
                        DateTime zeroTime = new DateTime(1, 1, 1);
                        TimeSpan span = (DateTime.Now - dedtbirthday);
                        Age = (zeroTime + span).Year - 1;
                        if ((AgeMin > Age || Age > AgeMax))
                        {
                            mess += "พนักงานอายุ " + Age + "ปี ซึ่งไม่อยู่ในเงื่อนไขของอายุการทำงานในตำแหน่ง " + ddlPosition.SelectedItem.Text + "<br/>";
                            mess += "ตำแหน่ง " + ddlPosition.SelectedItem.Text + " มีอายุการทำงานอยู่ระหว่าง " + AgeMin + " ถึง " + AgeMax + " ปี<br/>";
                        }
                    }

                }
            }
            if (string.IsNullOrEmpty(txtEMPFileName.Text))
                mess += "กรุณาเลือก รูปประจำตัว !!!<br/>";
            string Percode = txtPercode.Text.Trim().Replace(" ", "").Replace("-", "");

            DataTable dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(ddlPosition.SelectedValue.ToString()), "VENDOR");

            StringBuilder sb = new StringBuilder();
            object c;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                {
                    c = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                    Type = c.GetType().ToString();

                    if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxComboBox"))
                    {
                        ASPxComboBox cbo = (ASPxComboBox)c;
                        if (cbo.Value == null)
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!! <br/>");
                    }
                    else if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxTextBox"))
                    {
                        ASPxTextBox txt = (ASPxTextBox)c;
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                    {
                        TextBox txt = (TextBox)c;
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                    {
                        DropDownList cbo = (DropDownList)c;
                        if (cbo.SelectedIndex < 1)
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
                    {
                        HtmlInputText txt = (HtmlInputText)c;
                        if (string.Equals(txt.Value.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                }
            }
            if (!string.Equals(sb.ToString(), string.Empty))
                mess += sb.ToString();
            return mess;
        }
        catch (Exception ex)
        {
            return RemoveSpecialCharacters(ex.Message);
            //throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string ValidateSaveUpLoadFile()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        string getUploadID = dtRequestFile.Rows[i]["UPLOAD_ID"].ToString();
                        if (string.Equals(getUploadID, dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                        {
                            break;
                        }
                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }


                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                return sb.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string ValidateSaveUpLoadFile2()
    {
        try
        {
            //dtRequestFile2, dtUpload2
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload2.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile2.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile2.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile2.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                int getj = 0;
                int getk = 0;

                for (int i = 0; i < dtRequestFile2.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload2.Rows.Count; j++)
                    {
                        string getUploadID2 = dtRequestFile2.Rows[i]["UPLOAD_ID"].ToString();
                        if (string.Equals(getUploadID2, dtUpload2.Rows[j]["UPLOAD_ID"].ToString()))
                        {
                            break;
                        }
                        if (j == dtUpload2.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile2.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile2.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }


                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                return sb.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private Boolean checkUpload2(string uploadID)
    {
        Boolean hasfile = false;
        for (int j = 0; (j < dtUpload2.Rows.Count); j++)
        {
            if (string.Equals(uploadID, dtUpload2.Rows[j]["UPLOAD_ID"].ToString()))
            {
                hasfile = true;
            }
            else
            {
                hasfile = false;
            }

            if (hasfile) break;
        }

        return hasfile;
    }

    #endregion

    #region mpConfirmBack_ClickOK
    protected void mpConfirmBack_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect(BackUrl);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "back", "backurl();", true);
    }

    #endregion

    #region Upload
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
        this.CheckRequestFile();
    }
    protected void btnUpload2_Click(object sender, EventArgs e)
    {

        this.StartUpload2();
        this.CheckRequestFile();
    }
    private void StartUpload()
    {
        try
        {
            string message = "";
            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength, FileNameUser, ref message);
                if (message != "")
                {
                    alertFail(message);
                    return;
                }
                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    if (string.Equals(dtUpload.Rows[i]["UPLOAD_ID"].ToString(), ddlUploadType.SelectedValue))
                    {
                        dtUpload.Rows.RemoveAt(i);
                    }
                }

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            else
            {
                if (!fileUpload.HasFile)
                {
                    message = "- กรุณาอัพโหลดเอกสาร" + "<br/>";
                    alertFail(message);
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void StartUpload2()
    {
        try
        {
            string message = "";
            if (fileUpload2.HasFile)
            {
                string FileNameUser = fileUpload2.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile2(System.IO.Path.GetExtension(FileNameUser), fileUpload2.PostedFile.ContentLength, FileNameUser, ref message);
                if (message != "")
                {
                    alertFail(message);
                    return;
                }

                string Path = this.CheckPath();
                fileUpload2.SaveAs(Path + "\\" + FileNameSystem);

                //DateTime date = new DateTime();
                //DateTime? txtpermissionstartdate = null;
                //DateTime? txtpermissionenddate = null;
                //if (DateTime.TryParseExact(txtWorkPermissionStartDate.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                //{
                //    txtpermissionstartdate = date;
                //}
                //if (DateTime.TryParseExact(txtWorkPermissionEndDate.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                //{
                //    txtpermissionenddate = date;
                //}

                for (int i = 0; i < dtUpload2.Rows.Count; i++)
                {
                    if (string.Equals(dtUpload2.Rows[i]["UPLOAD_ID"].ToString(), ddlUploadType2.SelectedValue))
                    {
                        dtUpload2.Rows.RemoveAt(i);
                    }
                }
                string getDocNum = txtDocNumber.Text.Trim();
                dtUpload2.Rows.Add(ddlUploadType2.SelectedValue, ddlUploadType2.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem, txtWorkPermissionStartDate.Text, txtWorkPermissionEndDate.Text, getDocNum);
                GridViewHelper.BindGridView(ref dgvUploadFile2, dtUpload2);
            }
            else
            {
                if (!fileUpload2.HasFile)
                {
                    message = "- กรุณาอัพโหลดเอกสาร" + "<br/>";
                    alertFail(message);
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Vendor" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize, string FileNameUser, ref string message)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");

            FileNameUser = System.IO.Path.GetFileNameWithoutExtension(FileNameUser);
            //FileNameUser = FileNameUser.Substring(0, FileNameUser.Length - 4);
            if (FileNameUser != txtName.Text + "_" + txtSurName.Text + "_" + ddlUploadType.SelectedItem)
                message += "ชื่อไฟล์ที่อัพโหลด ไม่ตรงกับประเภทเอกสาร '" + ddlUploadType.SelectedItem + "'";

            //if file not match name's condition
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile2(string ExtentionUser, int FileSize, string FileNameUser, ref string message)
    {
        try
        {
            string Extention = dtUploadType2.Rows[ddlUploadType2.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType2.Rows[ddlUploadType2.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte
            //string test = txtName.Text + "_" + txtSurName.Text + "_" + ddlUploadType2.SelectedItem;            
            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType2.Rows[ddlUploadType2.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");
            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
            if (string.IsNullOrEmpty(txtWorkPermissionStartDate.Text))
                message += "- กรุณากรอกวันเริ่มต้น" + "<br/>";
            if (string.IsNullOrEmpty(txtWorkPermissionEndDate.Text))
                message += "- กรุณากรอกวันหมดอายุ" + "<br/>";
            if (string.IsNullOrEmpty(txtDocNumber.Text))
                message += "- กรุณากรอกเลขที่เอกสาร" + "<br/>";

            FileNameUser = System.IO.Path.GetFileNameWithoutExtension(FileNameUser);
            //FileNameUser = FileNameUser.Substring(0, FileNameUser.Length - 4);
            if (FileNameUser != txtName.Text + "_" + txtSurName.Text + "_" + ddlUploadType2.SelectedItem)
                message += "- ชื่อไฟล์อัพโหลดกำหนดรูปแบบ ดังนี้ ชื่อ_นามสกุล_ประเภทของไฟล์" + "<br/>";
            //if file not match name's condition

            if (ddlUploadType2.SelectedValue == "31")
            {
                txtDocNumber.Text = txtDocNumber.Text.Replace("-", "").Replace(" ", ""); ;
                if (string.IsNullOrEmpty(txtDocNumber.Text.ToString()) || txtDocNumber.Text.Length != 13)
                {
                    message += "- กรุณาป้อน หมายเลขบัตรประชาขน ให้ครบ 13 หลัก !!!<br/>";
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            this.CheckRequestFile();
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload2.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile2, dtUpload2);
            this.CheckRequestFile();
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string systemFileName = dgvUploadFile.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), systemFileName);
    }

    protected void dgvUploadFile2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile2.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string systemFileName = dgvUploadFile2.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), systemFileName);
    }
    private void DownloadFile(string Path, string FileName, string systemFileName)
    {
        try
        {
            //if (File.Exists(Path + "\\" + FileName))
            //{
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + systemFileName);
            Response.TransmitFile(Path + "\\" + FileName);
            Response.End();
            //}
            //else
            //{
            //    alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
            //}

        }
        catch (Exception ex)
        {
            alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
        }

    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");

                //if (!CanWrite)
                if ("" + Session["CheckPermission"] == "1")
                {
                    imgDelete.Visible = false;
                }

                ImageButton lnkbtnDetail = (ImageButton)e.Row.FindControl("imgView");
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkbtnDetail);
            }

        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    protected void dgvUploadFile2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");

                if (!CanWrite)
                {
                    imgDelete.Visible = false;
                }

                ImageButton lnkbtnDetail = (ImageButton)e.Row.FindControl("imgView");
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkbtnDetail);
            }
        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL2("VENDOR", "0");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");

        GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
    }

    private void InitialUpload2()
    {
        dtUploadType2 = UploadTypeBLL.Instance.UploadTypeSelectBLL2("VENDOR", "1");
        DropDownListHelper.BindDropDownList(ref ddlUploadType2, dtUploadType2, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload2 = new DataTable();
        dtUpload2.Columns.Add("UPLOAD_ID");
        dtUpload2.Columns.Add("UPLOAD_NAME");
        dtUpload2.Columns.Add("FILENAME_SYSTEM");
        dtUpload2.Columns.Add("FILENAME_USER");
        dtUpload2.Columns.Add("FULLPATH");
        dtUpload2.Columns.Add("START_DATE");
        dtUpload2.Columns.Add("STOP_DATE");
        dtUpload2.Columns.Add("DOC_NUMBER");

        GridViewHelper.BindGridView(ref dgvUploadFile2, dtUpload2);
    }

    private void CheckRequestFile()
    {
        try
        {
            string UploadID = string.Empty;
            CheckBox chk = new CheckBox();

            for (int j = 0; j < dtRequestFile.Rows.Count; j++)
            {
                chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                if (chk != null)
                    chk.Checked = false;
            }

            for (int i = 0; i < dtUpload.Rows.Count; i++)
            {
                UploadID = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (string.Equals(UploadID, dtRequestFile.Rows[j]["UPLOAD_ID"].ToString()))
                    {
                        chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                        if (chk != null)
                            chk.Checked = true;
                        break;
                    }
                }
            }


            for (int j = 0; j < dtRequestFile2.Rows.Count; j++)
            {
                chk = (CheckBox)dgvRequestFile2.Rows[j].FindControl("chkUploaded2");
                if (chk != null)
                    chk.Checked = false;
            }

            for (int i = 0; i < dtUpload2.Rows.Count; i++)
            {
                UploadID = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
                for (int j = 0; j < dtRequestFile2.Rows.Count; j++)
                {
                    if (string.Equals(UploadID, dtRequestFile2.Rows[j]["UPLOAD_ID"].ToString()))
                    {
                        chk = (CheckBox)dgvRequestFile2.Rows[j].FindControl("chkUploaded2");
                        if (chk != null)
                            chk.Checked = true;
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region Edit
    #region SetData
    private void SetData()
    {
        DateTime? DBIRTHDATE = null;
        DateTime? WORKDATE = null;
        DateTime? PERSONEL_BEGIN = null;
        DateTime? PERSONEL_EXPIRE = null;
        DateTime? DDRIVEBEGIN = null;
        DateTime? DDRIVEEXPIRE = null;
        DateTime? DUPDATE = null;
        DateTime? DCREATE = null;
        DataTable dt = new DataTable();
        if (P4 || PK)
        {
            string strReq = Request.QueryString["strID"];
            if (!string.IsNullOrEmpty(strReq))
            {
                var decryptedBytes = MachineKey.Decode(strReq, MachineKeyProtection.All);
                string strQueryReq = Encoding.UTF8.GetString(decryptedBytes);
                REQ_ID = strQueryReq;
            }
            dt = VendorBLL.Instance.VendorP4Select(SEMPLOYEEID, REQ_ID);

        }
        else
        {
            dt = VendorBLL.Instance.VendorSelect(SEMPLOYEEID);
        }

        if (dt.Rows.Count > 0)
        {
            //SVENDOR_ID = dt.Rows[0]["STRANS_ID"] + "";
            txtEmpID.Text = dt.Rows[0]["SEMPLOYEEID"] + "";
            if (dt.Columns.Contains("EMPSAPID"))
            {
                EMPSAPID = dt.Rows[0]["EMPSAPID"] + "";
            }

            if (!string.IsNullOrEmpty(dt.Rows[0]["SEMPTPYE"] + ""))
            {
                ddlPosition.SelectedValue = dt.Rows[0]["SEMPTPYE"] + "";
            }

            radSpot.SelectedValue = dt.Rows[0]["SPOT_STATUS"].ToString();
            radSpot_SelectedIndexChanged(null, null);
            ddlTitle.SelectedValue = dt.Rows[0]["TNAME"] + "";

            lgetStatus.Text = dt.Rows[0]["EMPSTATUS"] + "";
            if (lgetStatus.Text == "อนุญาต") { lgetStatus.ForeColor = Color.Green; }
            if (lgetStatus.Text == "ระงับการใช้งาน") { lgetStatus.ForeColor = Color.Red; }
            if (lgetStatus.Text == "Black List") { lgetStatus.ForeColor = Color.Maroon; }

            txtName.Text = dt.Rows[0]["FNAME"] + "";
            txtSurName.Text = dt.Rows[0]["LNAME"] + "";
            txtTel.Text = dt.Rows[0]["STEL"] + "";
            txtTel2.Text = dt.Rows[0]["STEL2"] + "";
            txtMail.Text = dt.Rows[0]["SMAIL"] + "";
            txtMail2.Text = dt.Rows[0]["SMAIL2"] + "";
            txtdrivelicence.Text = dt.Rows[0]["SDRIVERNO"] + "";
            if (string.IsNullOrEmpty(txtdrivelicence.Text))
            {
                txtdrivelicence.Enabled = true;
            }
            txtNumAccident.Text = dt.Rows[0]["NUMACCIDENT"] + "";
            if (string.IsNullOrEmpty(txtNumAccident.Text))
            {
                txtNumAccident.Enabled = true;
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["SVENDORID"] + ""))
            {
                cboVendor.Value = dt.Rows[0]["SVENDORID"];
                cboVendor_SelectedIndexChanged(null, null);
                ddlContractSpot.SelectedValue = dt.Rows[0]["CONTRACT_ID"].ToString();
                hidVendor.Value = cboVendor.Value + string.Empty;
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["STERMINALID"] + ""))
            {
                cboMainPlant.Value = dt.Rows[0]["STERMINALID"];
            }
            txtPercode.Text = dt.Rows[0]["PERS_CODE"] + "";

            if (!string.IsNullOrEmpty(txtPercode.Text) && txtPercode.Text.Length == 13)
            {
                txtPercode.Text = txtPercode.Text.Insert(12, "-").Insert(10, "-").Insert(5, "-").Insert(1, "-");
            }
            else
            {
                txtPercode.Enabled = true;
            }

            if (!string.IsNullOrEmpty(dt.Rows[0]["DBIRTHDATE"] + ""))
            {
                DBIRTHDATE = DateTime.Parse(dt.Rows[0]["DBIRTHDATE"] + "");
                dedtBirthDay.Text = DBIRTHDATE.Value.ToString("dd/MM/yyyy");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["WORKDATE"] + ""))
            {
                WORKDATE = DateTime.Parse(dt.Rows[0]["WORKDATE"] + "");
                dedStartWorkDate.Text = WORKDATE.Value.ToString("dd/MM/yyyy");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["PERSONEL_BEGIN"] + ""))
            {
                PERSONEL_BEGIN = DateTime.Parse(dt.Rows[0]["PERSONEL_BEGIN"] + "");
                dedtstartPercode.Text = PERSONEL_BEGIN.Value.ToString("dd/MM/yyyy");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["PERSONEL_EXPIRE"] + ""))
            {
                PERSONEL_EXPIRE = DateTime.Parse(dt.Rows[0]["PERSONEL_EXPIRE"] + "");
                dedtendPercode.Text = PERSONEL_EXPIRE.Value.ToString("dd/MM/yyyy");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DDRIVEBEGIN"] + ""))
            {
                DDRIVEBEGIN = DateTime.Parse(dt.Rows[0]["DDRIVEBEGIN"] + "");
                dedtstartlicence.Text = DDRIVEBEGIN.Value.ToString("dd/MM/yyyy");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DDRIVEEXPIRE"] + ""))
            {
                DDRIVEEXPIRE = DateTime.Parse(dt.Rows[0]["DDRIVEEXPIRE"] + "");
                dedtEndlicence.Text = DDRIVEEXPIRE.Value.ToString("dd/MM/yyyy");
            }
            DateTime? ACCIDENTSTARTDATE = null;
            if (!string.IsNullOrEmpty(dt.Rows[0]["ACCIDENTSTARTDATE"] + ""))
            {
                ACCIDENTSTARTDATE = DateTime.Parse(dt.Rows[0]["ACCIDENTSTARTDATE"] + "");
                txtAccidentStartDate.Text = ACCIDENTSTARTDATE.Value.ToString("dd/MM/yyyy");
            }
            DateTime? ACCIDENTENDDATE = null;
            if (!string.IsNullOrEmpty(dt.Rows[0]["ACCIDENTENDDATE"] + ""))
            {
                ACCIDENTENDDATE = DateTime.Parse(dt.Rows[0]["ACCIDENTENDDATE"] + "");
                txtAccidentEndDate.Text = ACCIDENTENDDATE.Value.ToString("dd/MM/yyyy");
            }

            if (!string.IsNullOrEmpty(dt.Rows[0]["DCREATE"] + ""))
            {
                DCREATE = DateTime.Parse(dt.Rows[0]["DCREATE"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DUPDATE"] + ""))
            {
                DUPDATE = DateTime.Parse(dt.Rows[0]["DUPDATE"] + "");
            }

            // dedtDEXPIRE13BIS.Value = DEXPIRE13BIS;
            if (!string.IsNullOrEmpty(dt.Rows[0]["CACTIVE"] + ""))
            {
                if ((Session["CGROUP"] + string.Empty != "0"))
                {
                    tbStatus.Style.Remove("display");
                }

                switch (int.Parse(dt.Rows[0]["CACTIVE"] + ""))
                {
                    //กรณีระงับการใช้งาน
                    case 0: rblStatus.SelectedIndex = 1;
                        Typedefault.Style.Add("display", "none");
                        trWait.Style.Remove("display");
                        trstatusWait.Style.Remove("display");
                        trCancel.Style.Add("display", "none");
                        trsCancelstatus.Style.Add("display", "none");
                        if (!string.IsNullOrEmpty(dt.Rows[0]["BANSTATUS"] + ""))
                        {
                            string sBANSTATUS = dt.Rows[0]["BANSTATUS"] + "";
                            ddlStatus.SelectedValue = sBANSTATUS;
                        }

                        txtComment.Text = dt.Rows[0]["CAUSESAP"] + "";
                        DateTime? SETTLETO = null;
                        if (!string.IsNullOrEmpty(dt.Rows[0]["SETTLETO"] + ""))
                        {
                            SETTLETO = DateTime.Parse(dt.Rows[0]["SETTLETO"] + "");
                            txtSettleTo.Text = SETTLETO.Value.ToString("dd/MM/yyyy");
                        }
                        cboVendor.ClientEnabled = true;
                        break;
                    case 1:
                        Typedefault.Style.Remove("display");
                        trWait.Style.Add("display", "none");
                        trstatusWait.Style.Add("display", "none");
                        trCancel.Style.Add("display", "none");
                        trsCancelstatus.Style.Add("display", "none");
                        rblStatus.SelectedIndex = 0;
                        txtConfirm.Text = dt.Rows[0]["CAUSESAPCOMMIT"] + "";
                        cboVendor.ClientEnabled = false;
                        break;
                    case 2:
                        rblStatus.SelectedIndex = 2;
                        Typedefault.Style.Add("display", "none");
                        trWait.Style.Add("display", "none");
                        trstatusWait.Style.Add("display", "none");
                        trCancel.Style.Remove("display");
                        trsCancelstatus.Style.Remove("display");
                        if (!string.IsNullOrEmpty(dt.Rows[0]["CANCELSTATUS"] + ""))
                        {
                            string sCANCELSTATUS = dt.Rows[0]["CANCELSTATUS"] + "";
                            ddlStatus2.SelectedValue = sCANCELSTATUS;
                        }
                        txtstatus2.Text = dt.Rows[0]["CAUSESAPCANCEL"] + "";
                        cboVendor.ClientEnabled = false;
                        break;
                }

            }

            txtComment2.Text = dt.Rows[0]["CAUSEOVER"] + "";
            if (!string.IsNullOrEmpty(dt.Rows[0]["SFILENAME"] + "") && !string.IsNullOrEmpty(dt.Rows[0]["SSYSFILENAME"] + "") && !string.IsNullOrEmpty(dt.Rows[0]["SPATH"] + ""))
            {
                imgEmp.ImageUrl = dt.Rows[0]["SPATH"] + "" + dt.Rows[0]["SSYSFILENAME"] + "";
            }
            else
            {
                if (!string.IsNullOrEmpty(txtEMPTruePath.Text) && !string.IsNullOrEmpty(txtEMPSysfilename.Text))
                {
                    imgEmp.ImageUrl = txtEMPTruePath.Text + txtEMPSysfilename.Text;
                }
                else
                {
                    imgEmp.ImageUrl = "images/Avatar.png";
                }
            }

            UpEMP.Value = dt.Rows[0]["SPATH"] + "" + dt.Rows[0]["SSYSFILENAME"] + "";
            txtEMPFilePath.Text = dt.Rows[0]["SPATH"] + "";
            txtEMPFileName.Text = dt.Rows[0]["SFILENAME"] + "";
            txtEMPSysfilename.Text = dt.Rows[0]["SSYSFILENAME"] + "";
            txtEMPTruePath.Text = dt.Rows[0]["SPATH"] + "";

            lblDUPDATE.Text = dt.Rows[0]["DUPDATE"] + "" != "" ? Convert.ToDateTime(dt.Rows[0]["DUPDATE"]).ToString("dd/MM/yyyy HH:mm") + " น." : "-";
            lblSUPDATE.Text = string.IsNullOrEmpty(dt.Rows[0]["SUPDATENAME"] + string.Empty) ? "-" : dt.Rows[0]["SUPDATENAME"] + string.Empty;

            //if (dt.Rows[0]["NOTFILL"] + "" == "1")
            //{
            //    chkNotfill.Checked = true;
            //}
            //else
            //{
            //    chkNotfill.Checked = false;
            //}
            if (!string.IsNullOrEmpty(dt.Rows[0]["DATE_LASTJOB"] + ""))
            {
                lblDayuse.Text = dt.Rows[0]["DATE_LASTJOB"] + "";
                DateTime sDate = DateTime.Parse(dt.Rows[0]["DATE_LASTJOB"] + "");
                string ssDate = sDate.Day + "/" + sDate.Month + "/" + sDate.Year + "";

                string DateRange = "SELECT TRUNC(sysdate) - TRUNC(TO_DATE('" + CommonFunction.ReplaceInjection(ssDate) + "','DD/MM/YYYY')) AS DateLast FROM Dual";
                DataTable dtRange = new DataTable();

                dtRange = CommonFunction.Get_Data(conn, DateRange);
                //DateTime Date = DateTime.Parse(dtRange.Rows[0]["DateLast"] + "");

                if (!string.IsNullOrEmpty(dtRange.Rows[0]["DateLast"] + ""))
                {
                    lbldateuse.Text = dtRange.Rows[0]["DateLast"] + "";
                }
                else
                {
                    lbldateuse.Text = "-";
                }


            }
            else
            {
                lblDayuse.Text = "-";
                lbldateuse.Text = "-";
            }

            if (dt.Columns.Contains("SDESCRIPTION"))
            {
                hidStatusID.Value = dt.Rows[0]["STATUS"] + string.Empty;
                switch (hidStatusID.Value)
                {
                    case "0": lblStatus.Text = "ผขส. ขอเปลี่ยนข้อมูล"; break;
                    case "1": lblStatus.Text = "รข. อนุมัติ"; break;
                    case "2": lblStatus.Text = "รข. ปฏิเสธ"; break;
                    case "3": lblStatus.Text = "รข. ขอข้อมูลเพิ่มเติม"; break;
                    default:
                        break;
                }

                txtDescription.Text = dt.Rows[0]["SDESCRIPTION"] + string.Empty;
            }
            ComplainImportFileBLL complainImportFileBLL = new ComplainImportFileBLL();
            string getSEmId = dt.Rows[0]["SEMPLOYEEID"] + "";
            if (getSEmId != SEMPLOYEEID)
            {
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(getSEmId, "VENDOR", "0");
            }
            if (dtUpload.Rows.Count == 0)
            {
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(SEMPLOYEEID, "VENDOR", "0");
            }
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            /////////////////////////////////
            if (getSEmId != SEMPLOYEEID)
            {
                dtUpload2 = complainImportFileBLL.ImportFileSelectBLL2(getSEmId, "VENDOR", "1");
            }
            if (dtUpload2.Rows.Count == 0)
            {
                dtUpload2 = complainImportFileBLL.ImportFileSelectBLL2(SEMPLOYEEID, "VENDOR", "1");

                for (int i = 0; i < dtUpload2.Columns.Count; i++)
                {
                    string getColumnDocNum = dtUpload2.Columns[i].ColumnName;
                    if (string.Equals(getColumnDocNum, "DOC_NUMBER"))
                    {
                        for (int j = 0; j < dtUpload2.Rows.Count; j++)
                        {
                            string getUploadID = dtUpload2.Rows[j]["UPLOAD_ID"].ToString();
                            if (getUploadID == "31")
                            {
                                string getPercode = dtUpload2.Rows[j]["DOC_NUMBER"].ToString();
                                getPercode = getPercode.Trim().Replace("-", "").Replace(" ", "");
                                if (getPercode.Length == 13)
                                {
                                    getPercode = getPercode.Insert(12, "-").Insert(10, "-").Insert(5, "-").Insert(1, "-");
                                    dtUpload2.Rows[j]["DOC_NUMBER"] = getPercode;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            GridViewHelper.BindGridView(ref dgvUploadFile2, dtUpload2);

            //txtEmpID.Text = EMPSAPID + " / " + getSEmId  + " / " + SEMPLOYEEID;
        }
        if ((P4 || PK) && string.IsNullOrEmpty(REQ_ID))
        {
            SEMPLOYEEID = string.Empty;
        }
    }
    #endregion

    #region btnHistoryEmp_Click
    protected void btnHistoryEmp_Click(object sender, EventArgs e)
    {
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
        string PK = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
        Response.Redirect("~/employee_history.aspx?str=" + PK);
    }
    protected void btnGenerateFileName_Click(object sender, EventArgs e)
    {
        //UpdatePanel3.Update();
        if (fileUpload.HasFile)
        { }
        if (ddlUploadType.SelectedValue == "")
        {
            alertFail("กรุณาเลือกประเภทไฟล์");
        }
        else
        {
            txtGenerateFileName.Text = txtName.Text + "_" + txtSurName.Text + "_" + ddlUploadType.SelectedItem;
        }

    }
    protected void ddlUploadType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtGenerateFileName.Text = txtName.Text + "_" + txtSurName.Text + "_" + ddlUploadType.SelectedItem;

        //GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        //upGenerateFile1.Update();
    }
    protected void ddlUploadType2_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtGenerateFileName2.Text = txtName.Text + "_" + txtSurName.Text + "_" + ddlUploadType2.SelectedItem;
        if (ddlUploadType2.SelectedValue == "31")
        {
            txtDocNumber.MaxLength = 13;
            //txtDocNumber.Attributes.Add("onkeypress", "return IDCard(event,'" + txtDocNumber.ClientID + "')");
            txtDocNumber.CssClass += " IDCard number";
        }
        else
        {
            txtDocNumber.MaxLength = 50;
            txtDocNumber.CssClass = "form-control";
        }
        // GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        //upGenerateFile2.Update();
    }

    protected void btnGenerateFileName2_Click(object sender, EventArgs e)
    {
        if (ddlUploadType2.SelectedValue == "")
        {
            alertFail("กรุณาเลือกประเภทไฟล์");
        }
        else
        {
            txtGenerateFileName2.Text = txtName.Text + "_" + txtSurName.Text + "_" + ddlUploadType2.SelectedItem;
        }
        //upGenerateFile2.Update();
    }
    #endregion
    #endregion

    #region GetDataByPersCode
    private DataTable GetDataByPersCode(string Percode)
    {
        return VendorBLL.Instance.VendorGetByPersCode(Percode);
    }
    #endregion

    #region Save
    #region btnSave_Click
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string mess = Validate();
        mess += ValidateSaveUpLoadFile();
        mess += ValidateSaveUpLoadFile2();

        if (!string.IsNullOrEmpty(mess))
        {
            alertFail(RemoveSpecialCharacters(mess));
        }
        else
        {
            try
            {
                #region เตรียมข้อมูล
                mess = string.Empty;
                string CAUSESAPCOMMIT = "";
                string BANSTATUS = "";
                string CAUSESAP = "";
                string CANCELSTATUS = "";
                string CAUSESAPCANCEL = "";
                DateTime? SETTLETO = null;
                DateTime date = new DateTime();
                switch (rblStatus.SelectedIndex)
                {   //อนุญาติ
                    case 0:
                        RemarkStatus = CAUSESAPCOMMIT = txtConfirm.Text;
                        break;
                    //ระงับ
                    case 1:
                        if (ddlStatus.SelectedIndex != 0)
                        {
                            BANSTATUS = ddlStatus.SelectedItem.Value + "";
                        }
                        RemarkStatus = CAUSESAP = txtComment.Text;

                        if (DateTime.TryParse(txtSettleTo.Text, out date))
                        {
                            SETTLETO = date;
                        }
                        break;
                    //ยกเลิก
                    case 2:
                        if (ddlStatus2.SelectedIndex != 0)
                        {
                            CANCELSTATUS = ddlStatus2.SelectedItem.Value + "";
                        }
                        else
                        {
                        }
                        RemarkStatus = CAUSESAPCANCEL = txtstatus2.Text;
                        break;
                }
                DataTable dt = new DataTable();
                string cactive = "1";
                string drivelicence = txtdrivelicence.Text.Trim().Replace(" ", "");
                string NumAccident = txtNumAccident.Text.Trim().Replace(" ", "");
                string tel = txtTel.Text.Trim().Replace(" ", "").Replace("-", "");
                string tel2 = txtTel2.Text.Trim().Replace(" ", "").Replace("-", "");
                string Percode = txtPercode.Text.Trim().Replace(" ", "").Replace("-", "");
                if (getDataFromUpload2("perCode") != "")
                {
                    Percode = getDataFromUpload2("perCode");
                }
                if (getDataFromUpload2("LicenseNo") != "")
                {
                    drivelicence = getDataFromUpload2("LicenseNo");
                }
                if (drivelicence == "")
                {
                    drivelicence = "#!";
                }
                string notfill = "";//chkNotfill.Checked == true ? "1" : "0";

                DateTime? dedtbirthday = null;
                if (DateTime.TryParseExact(dedtBirthDay.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    dedtbirthday = date;
                }
                DateTime? dedtstartpercode = null;
                if (DateTime.TryParseExact(dedtstartPercode.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    dedtstartpercode = date;
                }
                DateTime? dedtendpercode = null;
                if (DateTime.TryParseExact(dedtendPercode.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    dedtendpercode = date;
                }
                DateTime? dedtstartlicences = null;
                if (getDataFromUpload2("LicenseFrom") != "")
                {
                    dedtstartlicence.Text = getDataFromUpload2("LicenseFrom");
                }
                if (DateTime.TryParseExact(dedtstartlicence.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    dedtstartlicences = date;
                }

                DateTime? dedtendlicence = null;
                if (getDataFromUpload2("LicenseTo") != "")
                {
                    dedtEndlicence.Text = getDataFromUpload2("LicenseTo");
                }
                if (DateTime.TryParseExact(dedtEndlicence.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    dedtendlicence = date;
                }
                DateTime? dedstartworkdate = null;
                if (DateTime.TryParseExact(dedStartWorkDate.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    dedstartworkdate = date;
                }
                DateTime? txtaccidentstartdate = null;
                if (DateTime.TryParseExact(txtAccidentStartDate.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    txtaccidentstartdate = date;
                }
                DateTime? txtaccidentenddate = null;
                if (DateTime.TryParseExact(txtAccidentEndDate.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    txtaccidentenddate = date;
                }
                DateTime? txtsettleto = null;
                int day = 0;
                if (DateTime.TryParseExact(txtSettleTo.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
                {
                    txtsettleto = date;
                    day = (txtsettleto.Value - DateTime.Now).Days;
                }

                if (rblStatus.SelectedIndex >= 0)
                {
                    cactive = rblStatus.Value.ToString();
                }
                int? Position = int.Parse(ddlPosition.SelectedValue);
                string vendor = string.Empty;
                if (cboVendor.Value != null)
                {
                    vendor = cboVendor.Value.ToString();
                }
                Button btn = (Button)sender;
                string Detail = string.Empty;
                #endregion

                #region SAP
                string Check = "N", DRV = " ";
                if (!P4 && !SaveDraft)
                {
                    string sMsg = "";

                    #region SAP_INS

                    switch (rblStatus.Value + "")
                    {
                        case "0": DRV = "1";
                            break;
                        case "1": DRV = " ";
                            break;
                        case "2": DRV = "2";
                            break;
                        default: DRV = " ";
                            break;
                    }

                    //string QUERY = "SELECT * FROM SAP_DRIVER_MASTER_DATA WHERE PERS_CODE = '" + CommonFunction.ReplaceInjection(Percode) + "'";
                    string QUERY = "SELECT * FROM SAPECP1000087.SAP_DRIVER_MASTER_DATA@MASTER WHERE PERS_CODE = '" + CommonFunction.ReplaceInjection(Percode) + "'";
                    DataTable dtSap = CommonFunction.Get_Data(conn, QUERY);

                    SAP_Create_Driver veh_driver = new SAP_Create_Driver();
                    veh_driver.DRIVER_CODE = dtSap.Rows.Count > 0 ? dtSap.Rows[0]["DRIVER_CODE"] + "" : EMPSAPID;

                    veh_driver.PERS_CODE = Percode;
                    veh_driver.CARRIER = vendor;
                    veh_driver.FIRST_NAME = txtName.Text.Trim();
                    veh_driver.LAST_NAME = txtSurName.Text.Trim();
                    veh_driver.DRV_STATUS = DRV;
                    veh_driver.LICENSE_NO = drivelicence;
                    veh_driver.Phone_1 = tel;
                    veh_driver.Phone_2 = tel2;
                    veh_driver.LICENSENOE_FROM = dedtstartlicence.Text;
                    veh_driver.LICENSENO_TO = dedtEndlicence.Text;

                    if (dtSap.Rows.Count > 0 || !string.IsNullOrEmpty(EMPSAPID))
                    {
                        //กรณีที่มีการอัพเดทข้อมูล พขร.
                        sMsg = veh_driver.UDP_Driver_SYNC_OUT_SI();
                        Check = sMsg.Substring(0, 1);
                        if (Check == "Y")
                        {
                            string[] data = sMsg.Split(':');
                            if (data.Length > 1)
                            {
                                EMPSAPID = data[1];
                                //UpdateEmpSAPID(EMPID, Percode, EMPSAPID);

                                mess += "แก้ไข" + MessSAPSuccess;
                            }
                        }
                        else
                        {
                            mess += MessSAPFail.Replace("{0}", "แก้ไข").Replace("{1}", "<br/>" + sMsg);
                        }
                    }
                    else
                    {
                        //กรณีที่มีการสร้างข้อมูล พขร.
                        sMsg = veh_driver.CRT_Driver_SYNC_OUT_SI();
                        Check = sMsg.Substring(0, 1);
                        if (Check == "Y")
                        {
                            string[] data = sMsg.Split(':');
                            if (data.Length > 1)
                            {
                                EMPSAPID = data[1];
                                //UpdateEmpSAPID(EMPID, Percode, EMPSAPID);
                                mess += "เพิ่ม" + MessSAPSuccess;
                            }

                        }
                        else
                        {
                            mess += MessSAPFail.Replace("{0}", "เพิ่ม").Replace("{1}", "<br/>" + sMsg);

                        }
                    }


                    #endregion
                }
                #endregion
                if (string.IsNullOrEmpty(SEMPLOYEEID))
                {
                    #region Insert
                    bool isInsert = false;
                    if (btn.CommandArgument == "ConfirmBeforeSaveCheckVendor")
                    {
                        #region กรณีคลิก comfirm CheckVendor
                        if (P4)
                        {
                            isInsert = true;
                            Detail += "ขอเพิ่มข้อมูลพนักงาน<br/>";
                            Detail += "บริษัท : " + cboVendor.Text + "<br/>";
                            Detail += "บริษัทเดิม : XXX<br/>";
                            Detail += "สถานะคำขอ : ขอเปลี่ยนข้อมูล<br/>";
                            VendorBLL.Instance.LogInsert(Detail.Replace("XXX", Session["SABBREVIATIONOld"] + string.Empty), Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);
                            Session.Remove("SABBREVIATIONOld");
                            // Send Email ขอเพิ่มพนักงาน
                            mess += "ขอเพิ่มข้อมูลพนักงาน";
                        }
                        else
                        {

                            Detail += "เพิ่มข้อมูลพนักงาน<br/>";
                            Detail += "บริษัท : " + cboVendor.Text + "<br/>";
                            Detail += "บริษัทเดิม : XXX<br/>";
                            Detail += "สถานะการทำงาน : อนุญาติ<br/>";
                            VendorBLL.Instance.LogInsert(Detail, Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);
                            // Send Email เพิ่มพนักงาน
                            mess += "เพิ่มข้อมูลพนักงาน";
                        }
                        #endregion
                    }
                    else
                    {

                        //USP_T_VENDOR_CHECKBEFORESAVE
                        dt = VendorBLL.Instance.VendorCheckBeforeSave(Percode);
                        //ตรวจสอบก่อน Save

                        if (dt.Rows[0]["MESSAGE"] == DBNull.Value)//Save ได้เลย
                        {
                            #region Save ได้เลย
                            isInsert = true;
                            Detail += "เพิ่มข้อมูลพนักงาน<br/>";
                            Detail += "บริษัท : " + cboVendor.Text + "<br/>";
                            if (P4)
                            {
                                Detail = Detail.Insert(0, "ขอ");
                                Detail += "สถานะคำขอ : ผขส. ขอเปลี่ยนข้อมูล<br/>";
                                mess += "ขอ";
                            }
                            else
                            {
                                Detail += "สถานะการทำงาน : อนุญาติ<br/>";
                            }
                            mess += "เพิ่มข้อมูลพนักงาน";
                            #endregion

                        }
                        else if (dt.Rows[0]["MESSAGE"] + string.Empty == "1")//มีข้อมูลอยู่ในระบบแล้ว
                        {
                            if (dt.Rows[0]["INUSE"] + string.Empty == "0" && dt.Rows[0]["CACTIVE"] + string.Empty != "2")
                            {
                                #region บริษัทเดิมเลิกจ้างงานแล้วและไม่ถูก BlackList
                                mess += "ชื่อตามข้อมูลเดิม : " + dt.Rows[0]["EMPNAME"] + "<br/>";
                                mess += "หมายเลขบัตรประชาชน : " + txtPercode.Text.Trim() + "<br/>";
                                mess += "สถานะการทำงาน : <span style=\"color:Red;\">เคยทำงานอยู่กับบริษัท XXX</span><br/>";
                                mess += "ยืนยันการเพิ่มพนักงานในบริษัทของท่าน<br/>";
                                Session["SABBREVIATIONOld"] = dt.Rows[0]["SABBREVIATION"] + string.Empty;
                                mpConfirmSaveCheckVendor.TextDetail = mess;
                                Session["VendorOld"] = dt.Rows[0]["SABBREVIATION"];
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "confirm", "confirmChangevendor();", true);
                                return;
                                #endregion
                            }
                            else if (dt.Rows[0]["CACTIVE"] + string.Empty == "2")
                            {
                                #region พนักงานถูก BlackList ไม่สามารถเพิ่มพนักงานได้
                                mess += "ชื่อตามข้อมูลเดิม : " + dt.Rows[0]["EMPNAME"] + "<br/>";
                                mess += "หมายเลขบัตรประชาชน : " + txtPercode.Text.Trim() + "<br/>";
                                mess += "สถานะการทำงาน : <span style=\"color:Red;\">BlackList !!!</span><br/>";
                                mess += "<span style=\"color:Red;\">ไม่สามารถ เพิ่ม/แก้ไข พนักงานได้ !!!</span><br/>";
                                alertFail(mess);
                                return;
                                #endregion
                            }
                            else if (dt.Rows[0]["STRANS_ID"] + string.Empty == cboVendor.Value + string.Empty)
                            {
                                #region แก้ไขข้อมูลได้
                                isInsert = true;
                                Detail += "ขอแก้ไขข้อมูลพนักงาน<br/>";
                                Detail += "บริษัท : " + cboVendor.Text + "<br/>";
                                Detail += "สถานะคำขอ : ผขส. ขอเปลี่ยนข้อมูล<br/>";
                                Detail += "ชื่อตามข้อมูลเดิม : " + dt.Rows[0]["EMPNAME"] + "<br/>";
                                Detail += "หมายเลขบัตรประชาชน : " + txtPercode.Text.Trim() + "<br/>";
                                Detail += "สถานะการทำงาน : <span style=\"color:Red;\">" + dt.Rows[0]["STATUS"] + " !!!</span><br/>";
                                //Detail += "เหตุผล : " + dt.Rows[0]["REMARK"] + "<br/>";
                                mess += "ขอแก้ไขข้อมูลพนักงาน";
                                #endregion
                            }
                            else if (dt.Rows[0]["STRANS_ID"] + string.Empty != cboVendor.Value + string.Empty)
                            {
                                #region ทำงานอยู่บริษัทอื่น เพิ่มไมไ่ด้
                                mess += "ชื่อตามข้อมูลเดิม : " + dt.Rows[0]["EMPNAME"] + "<br/>";
                                mess += "หมายเลขบัตรประชาชน : " + txtPercode.Text.Trim() + "<br/>";
                                mess += "เนื่องจากพนักงานยังทำงานอยู่กับบริษัท : XXX<br/>";
                                //mess += "สถานะการทำงาน : <span style=\"color:Red;\">" + dt.Rows[0]["STATUS"] + " !!!</span><br/>";
                                mess += "<span style=\"color:Red;\">จึงไม่สามารถ เพิ่ม/แก้ไข ข้อมูลพนักงานได้ !!!</span><br/>";
                                alertFail(mess);
                                return;
                                #endregion
                            }
                        }
                        else if (dt.Rows[0]["MESSAGE"] + string.Empty == "2" && (dt.Rows[0]["CACTIVE"] + string.Empty != "1" && dt.Rows[0]["CACTIVE"] + string.Empty != "2"))//ยื่นคำขอแล้ว
                        {
                            #region ส่งคำขอแล้ว
                            mess += "ชื่อตามข้อมูลเดิม : " + dt.Rows[0]["EMPNAME"] + "<br/>";
                            mess += "คำขอ : ได้ทำการส่งทำข้อให้ รข. เรียบร้อยแล้ว<br/>";
                            mess += "สถานะคำขอ : <span style=\"color:Red;\">" + dt.Rows[0]["STATUS"] + " !!!</span><br/>";
                            if (dt.Rows[0]["STRANS_ID"] + string.Empty != cboVendor.Value + string.Empty)
                                mess += "กับบริษัท : XXXX<br/>";
                            mess += "<span style=\"color:Red;\">ไม่สามารถ เพิ่ม/แก้ไข พนักงานได้ !!!</span><br/>";
                            alertFail(mess);
                            return;
                            #endregion
                        }
                        else
                        {
                            #region Save ได้เลย
                            isInsert = true;
                            Detail += "เพิ่มข้อมูลพนักงาน<br/>";
                            Detail += "บริษัท : " + cboVendor.Text + "<br/>";
                            if (P4)
                            {
                                Detail = Detail.Insert(0, "ขอ");
                                Detail += "สถานะคำขอ : ผขส. ขอเปลี่ยนข้อมูล<br/>";
                                mess += "ขอ";
                            }
                            else
                            {
                                Detail += "สถานะการทำงาน : อนุญาติ<br/>";
                            }
                            mess += "เพิ่มข้อมูลพนักงาน";
                            #endregion
                        }
                    }
                    if (isInsert)
                    {
                        string messSuccess = "สำเร็จ<br/>";
                        #region เพิ่มข้อมูลใหม่
                        if (P4)
                        {
                            //USP_T_VENDOR_P4_INSERT

                            //dt = VendorBLL.Instance.VendorP4Insert2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, EMPSAPID, dtUpload, dtUpload2);
                            //SPOT_1 (Vendor Create Request)
                            int ContractID = 0;
                            if (ddlContractSpot.Enabled)
                                ContractID = int.Parse(ddlContractSpot.SelectedValue);
                            dt = VendorBLL.Instance.VendorP4Insert3(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, EMPSAPID, dtUpload, dtUpload2, radSpot.SelectedValue, ContractID);

                            if (dt.Rows.Count > 0)
                            {
                                DataRow dr = dt.Rows[0];
                                if (dr["MESSAGE"] == DBNull.Value)
                                {
                                    SEMPLOYEEID = dr["EMPID"] + string.Empty;
                                    REQ_ID = dr["REQ_ID"] + string.Empty;
                                }
                            }
                            if (SendEmail(ConfigValue.VendorEmployeeAdd, "เพิ่ม", "0"))
                            {
                                Detail += MessEmailSuccess;
                            }
                            else
                            {
                                Detail += MessEmailFail;
                            }
                            messSuccess = "สำเร็จ รอการอนุมัติ<br/>";
                            // Send Email ขอเพิ่มพนักงาน
                        }
                        else if (Check == "Y")
                        {

                            // USP_T_VENDOR_INSERT
                            //dt = VendorBLL.Instance.VendorInsert2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, EMPSAPID, dtUpload, dtUpload2);
                            int ContractID = 0;
                            if (ddlContractSpot.Enabled)
                                ContractID = int.Parse(ddlContractSpot.SelectedValue);
                            dt = VendorBLL.Instance.VendorInsert3(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, EMPSAPID, dtUpload, dtUpload2, radSpot.SelectedValue, ContractID);

                            Detail += "เพิ่มพนักงานใน TMS<br/>";
                            Detail += "บริษัท : " + cboVendor.Text + "<br/>";
                            Detail += "สถานะคำขอ : รข. อนุมัติ<br/>";
                            Detail += "สถานะการทำงาน : อนุญาติ<br/>";
                            if (SendEmail(ConfigValue.VendorEmployeeAddReturn, "อนุมัติ", "1"))
                            {
                                mess += MessEmailSuccess;
                            }
                            else
                            {
                                mess += MessEmailFail;
                            }
                            // Send Email เพิ่มพนักงาน
                        }
                        #endregion
                        VendorBLL.Instance.LogInsert(Detail, Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);
                        mess = Detail + mess + messSuccess;
                    }
                    #endregion
                }
                else
                {
                    #region Update
                    if (P4)
                    {
                        //USP_T_VENDOR_P4_UPDATE

                        //dt = VendorBLL.Instance.VendorP4Update2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, REQ_ID, EMPSAPID, dtUpload, dtUpload2);
                        //SPOT_2 (Vendor Update Request)
                        int ContractID = 0;
                        if (ddlContractSpot.Enabled)
                            ContractID = int.Parse(ddlContractSpot.SelectedValue);
                        dt = VendorBLL.Instance.VendorP4Update3(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, REQ_ID, EMPSAPID, dtUpload, dtUpload2, radSpot.SelectedValue, ContractID);

                        Detail += "แก้ไขข้อมูลพนักงาน<br/>";
                        if (!string.IsNullOrEmpty(txtDescription.Text))
                            Detail += "ตามหมายเหตุ : " + txtDescription.Text + "<br/>";
                        Detail += "สถานะคำขอ : ผขส. ขอเปลี่ยนข้อมูล<br/>";
                        VendorBLL.Instance.LogInsert(Detail, Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);

                        // Send Email แก้ไขข้อมูลพนักงาน ตามหมายเหตุ


                        mess += Detail;
                        if (SendEmail(ConfigValue.VendorEmployeeAdd, "แก้ไข", "0"))
                        {
                            mess += MessEmailSuccess;
                        }
                        else
                        {
                            mess += MessEmailFail;
                        }
                        mess += "แก้ไขข้อมูลพนักงานสำเร็จ รอการอนุมัติ<br/ >";
                    }
                    else if (SaveDraft)
                    {
                        //dt = VendorBLL.Instance.VendorPKSaveDraft2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, REQ_ID, dtUpload, dtUpload2);
                        //SPOT_3 (PTT Save Draft Request)
                        int ContractID = 0;
                        if (ddlContractSpot.Enabled)
                            ContractID = int.Parse(ddlContractSpot.SelectedValue);
                        dt = VendorBLL.Instance.VendorPKSaveDraft3(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, REQ_ID, dtUpload, dtUpload2, radSpot.SelectedValue, ContractID);

                        VendorBLL.Instance.VendorPKEditOrApprove(Session["UserID"].ToString(), txtDescription.Text, REQ_ID, hidStatusID.Value);

                        mess += "บันทึกชั่วคราวสำเร็จ<br/ >";
                        alertSuccess(mess, BackUrl.Replace("~/", ""));

                    }
                    if (Check == "Y")
                    {
                        string log = string.Empty, strData = string.Empty, strValue = string.Empty;
                        DateTime dDate;
                        DataTable dtOld = GetDataByPersCode(Percode);
                        if (dtOld != null && dtOld.Rows.Count > 0)
                        {
                            DataRow drOld = dtOld.Rows[0];
                            if (editStatus)
                            {
                                #region สถานะการทำงาน

                                strData = drOld["CACTIVE"] + string.Empty;
                                ListEditItem listEditItem = rblStatus.Items.FindByValue(strData);
                                if (listEditItem != null)
                                {
                                    strValue = listEditItem.Text;
                                }
                                DivLog(ref log, "สถานะการทำงาน", strValue, rblStatus.SelectedItem.Text);
                                if (strValue != rblStatus.SelectedItem.Text)
                                {
                                    switch (rblStatus.Value + string.Empty)
                                    {
                                        case "1":
                                            DivLog(ref log, "สาเหตุการอนุญาติ", string.Empty, txtConfirm.Text.Trim());
                                            break;
                                        case "0":
                                            DivLog(ref log, "ประเภทการระงับ", string.Empty, ddlStatus.SelectedItem.Text);
                                            DivLog(ref log, "วันที่ระงับถึง", string.Empty, txtSettleTo.Text.Trim());
                                            DivLog(ref log, "สาเหตุที่ระงับ", string.Empty, txtComment.Text.Trim());
                                            break;
                                        case "2":
                                            DivLog(ref log, "ประเภทการ Black List", string.Empty, ddlStatus2.SelectedItem.Text);
                                            DivLog(ref log, "สาเหตุที่ สาเหตุที่ Black List", string.Empty, txtstatus2.Text.Trim());
                                            DataTable dtDetailEMP = ComplainBLL.Instance.PersonalDetailSelectBLL(EMPSAPID);
                                            this.InsertDriverStatus(EMPSAPID, Percode, txtName.Text.Trim(), txtSurName.Text.Trim(), "0", vendor, dtDetailEMP.Rows[0]["PICTUREPATH"].ToString(), DateTime.Now.ToString());
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                #region Log ข้อมูลเดิม
                                #region ตำแหน่ง
                                strData = drOld["SEMPTPYE"] + string.Empty;
                                ListItem listItem = ddlPosition.Items.FindByValue(strData);
                                if (listItem != null)
                                {
                                    strValue = listItem.Text;
                                }
                                DivLog(ref log, "ตำแหน่ง", strValue, (ddlPosition.SelectedItem == null ? string.Empty : ddlPosition.SelectedItem.Text));

                                #endregion

                                #region คำนำหน้าชื่อ

                                strData = drOld["TNAME"] + string.Empty;
                                listItem = ddlTitle.Items.FindByValue(strData);
                                if (listItem != null)
                                {
                                    strValue = listItem.Text;
                                }
                                DivLog(ref log, "คำนำหน้าชื่อ", strValue, (ddlTitle.SelectedItem == null ? string.Empty : ddlTitle.SelectedItem.Text));
                                #endregion

                                #region ชื่อ
                                DivLog(ref log, "ชื่อ", drOld["FNAME"], txtName.Text.Trim());
                                #endregion

                                #region นามสกุล
                                DivLog(ref log, "นามสกุล", drOld["LNAME"], txtSurName.Text.Trim());
                                #endregion

                                #region วันเกิด
                                strData = string.Empty;
                                if (!drOld.IsNull("DBIRTHDATE"))
                                {
                                    dDate = Convert.ToDateTime(drOld["DBIRTHDATE"]);
                                    strData = dDate.ToString("dd/MM/yyyy");
                                }
                                DivLog(ref log, "วันเกิด", strData, dedtBirthDay.Text.Trim());
                                #endregion

                                #region หมายเลขโทรศัพท์หลัก
                                DivLog(ref log, "หมายเลขโทรศัพท์หลัก", drOld["STEL"], tel);
                                #endregion

                                #region หมายเลขโทรศัพท์สำรอง
                                DivLog(ref log, "หมายเลขโทรศัพท์สำรอง", drOld["STEL2"], tel2);
                                #endregion

                                #region อีเมล์หลัก
                                DivLog(ref log, "อีเมล์หลัก", drOld["SMAIL"], txtMail.Text.Trim());

                                #endregion

                                #region อีเมล์สำรอง
                                DivLog(ref log, "อีเมล์สำรอง", drOld["SMAIL2"], txtMail2.Text.Trim());
                                #endregion

                                #region บริษัทผู้ขนส่ง
                                strData = drOld["STRANS_ID"] + string.Empty;
                                if (strData != cboVendor.Value)
                                {
                                    DataRow[] drs = ((DataTable)ViewState["DataVendor"]).Select(" SVENDORID = '" + drOld["STRANS_ID"] + "'");
                                    if (drs.Any())
                                    {
                                        strData = drs[0]["SABBREVIATION"] + string.Empty;
                                    }

                                }
                                DivLog(ref log, "บริษัทผู้ขนส่ง", strData, (cboVendor.SelectedItem == null ? string.Empty : cboVendor.SelectedItem.Text));
                                #endregion

                                #region หมายเลขบัตรประชาชน
                                DivLog(ref log, "บริษัทผู้ขนส่ง", drOld["SPERSONELNO"], Percode);
                                #endregion

                                #region ช่วงระยะเวลาอนุญาตบัตรประชาชน
                                strData = string.Empty;
                                if (!drOld.IsNull("PERSONEL_BEGIN"))
                                {
                                    dDate = Convert.ToDateTime(drOld["PERSONEL_BEGIN"]);
                                    strData = dDate.ToString("dd/MM/yyyy");
                                }
                                DivLog(ref log, "บริษัทผู้ช่วงระยะเวลาอนุญาตบัตรประชาชน", strData, dedtstartPercode.Text.Trim());
                                #endregion

                                #region ช่วงระยะเวลาอนุญาตบัตรประชาชน(ถึง)
                                strData = string.Empty;
                                if (!drOld.IsNull("PERSONEL_EXPIRE"))
                                {
                                    dDate = Convert.ToDateTime(drOld["PERSONEL_EXPIRE"]);
                                    strData = dDate.ToString("dd/MM/yyyy");
                                }
                                DivLog(ref log, "บริษัทผู้ช่วงระยะเวลาอนุญาตบัตรประชาชน(ถึง)", strData, dedtendPercode.Text.Trim());
                                #endregion

                                #region หมายเลขใบขับขี่ประเภท 4
                                DivLog(ref log, "บริษัทผู้ช่วงระยะเวลาอนุญาตบัตรประชาชน(ถึง)", drOld["SDRIVERNO"], drivelicence);
                                #endregion

                                #region ช่วงระยะเวลาอนุญาตใบขับขี่ ประเภท 4
                                strData = string.Empty;
                                if (!drOld.IsNull("DDRIVEBEGIN"))
                                {
                                    dDate = Convert.ToDateTime(drOld["DDRIVEBEGIN"]);
                                    strData = dDate.ToString("dd/MM/yyyy");
                                }
                                DivLog(ref log, "ช่วงระยะเวลาอนุญาตใบขับขี่ ประเภท 4", strData, dedtstartlicence.Text.Trim());
                                #endregion

                                #region ช่วงระยะเวลาอนุญาตใบขับขี่ ประเภท 4(ถึง)

                                strData = string.Empty;
                                if (!drOld.IsNull("DDRIVEEXPIRE"))
                                {
                                    dDate = Convert.ToDateTime(drOld["DDRIVEEXPIRE"]);
                                    strData = dDate.ToString("dd/MM/yyyy");
                                }
                                DivLog(ref log, "ช่วงระยะเวลาอนุญาตใบขับขี่ ประเภท 4(ถึง)", strData, dedtEndlicence.Text.Trim());
                                #endregion

                                #region คลังต้นทาง
                                strData = drOld["STERMINALID"] + string.Empty;
                                if (strData != cboMainPlant.Value + string.Empty)
                                {
                                    DataRow[] drs = ((DataTable)ViewState["DataTTERMINAL"]).Select(" STERMINALID = '" + drOld["STERMINALID"] + "'");
                                    if (drs.Any())
                                    {
                                        strData = drs[0]["STERMINALID"] + " - " + drs[0]["STERMINALNAME"];
                                    }
                                }
                                DivLog(ref log, "คลังต้นทาง", strData, (cboMainPlant.SelectedItem == null ? string.Empty : cboMainPlant.SelectedItem.Text));

                                #endregion

                                #region หมายเลขขับขี่เชิงป้องกันอุบัติเหตุ
                                DivLog(ref log, "หมายเลขขับขี่เชิงป้องกันอุบัติเหตุ", drOld["NUMACCIDENT"], NumAccident);
                                #endregion

                                #region ช่วงระยะเวลาอนุญาติหมายเลขขับขี่เชิงป้องกันอุบัติเหตุ
                                strData = string.Empty;
                                if (!drOld.IsNull("ACCIDENTSTARTDATE"))
                                {
                                    dDate = Convert.ToDateTime(drOld["ACCIDENTSTARTDATE"]);
                                    strData = dDate.ToString("dd/MM/yyyy");
                                }
                                DivLog(ref log, "ช่วงระยะเวลาอนุญาติหมายเลขขับขี่เชิงป้องกันอุบัติเหตุ", strData, txtAccidentStartDate.Text.Trim());

                                #endregion

                                #region ช่วงระยะเวลาอนุญาติหมายเลขขับขี่เชิงป้องกันอุบัติเหตุ(ถึง)
                                strData = string.Empty;
                                if (!drOld.IsNull("ACCIDENTENDDATE"))
                                {
                                    dDate = Convert.ToDateTime(drOld["ACCIDENTENDDATE"]);
                                    strData = dDate.ToString("dd/MM/yyyy");
                                }
                                DivLog(ref log, "ช่วงระยะเวลาอนุญาติหมายเลขขับขี่เชิงป้องกันอุบัติเหตุ(ถึง)", strData, txtAccidentEndDate.Text.Trim());

                                #endregion

                                #endregion
                            }
                            log = log.Insert(0, "<div class=\"form-horizontal\">");
                            log += "</div>";
                        }
                        if (PK)
                        {
                            //if (SaveEdit == true)
                            //   dt = VendorBLL.Instance.VendorPKSaveApprove2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, string.Empty, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, REQ_ID, EMPSAPID, dtUpload, dtUpload2);
                            //else
                            //dt = VendorBLL.Instance.VendorPKSaveApprove2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, REQ_ID, EMPSAPID, dtUpload, dtUpload2);
                            //SPOT_4
                            int ContractID = 0;
                            if (ddlContractSpot.Enabled)
                                ContractID = int.Parse(ddlContractSpot.SelectedValue);
                            dt = VendorBLL.Instance.VendorPKSaveApprove3(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, REQ_ID, EMPSAPID, dtUpload, dtUpload2, radSpot.SelectedValue, ContractID);

                            string strStutus = "เพิ่ม";
                            if (dt.Rows[0]["STATUS"] + string.Empty == "INSERT")
                            {
                                Detail += "เพิ่มข้อมูลพนักงานใน TMS<br/>";
                            }
                            else
                            {
                                Detail += "แก้ไขข้อมูลพนักงานใน TMS<br/>";
                                strStutus = "แก้ไข";
                            }


                            Detail += log + "สถานะคำขอ : รข. อนุมัติ<br/>";
                            Detail += "สถานะการทำงาน : อนุญาติ<br/>";
                            VendorBLL.Instance.LogInsert(Detail, Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);
                            if (SendEmail(ConfigValue.VendorEmployeeAddReturn, strStutus, string.Empty))
                            {
                                Detail += MessEmailSuccess;
                            }
                            else
                            {
                                Detail += MessEmailFail;
                            }
                            mess += Detail + "บันทึกและอนุมัติสำเร็จ<br/ >";
                        }
                        else
                        {
                            //USP_T_VENDOR_UPDATE
                            //if (SaveEdit == true)
                            //    dt = VendorBLL.Instance.VendorUpdate2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, string.Empty, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, EMPSAPID, dtUpload, dtUpload2);
                            //else
                            dt = VendorBLL.Instance.VendorUpdate2(SEMPLOYEEID, txtName.Text, txtSurName.Text, DateTime.Now, Session["UserID"].ToString(), Session["UserID"].ToString(), Percode, vendor, drivelicence, DRV, vendor, Position, dedtbirthday, tel, tel2, txtMail.Text, txtMail2.Text, dedtstartpercode, dedtendpercode, cactive, txtComment2.Text, txtEMPFileName.Text, txtEMPSysfilename.Text, txtEMPTruePath.Text, drivelicence, dedtstartlicences, dedtendlicence, Percode, BANSTATUS, CAUSESAP, CANCELSTATUS, CAUSESAPCANCEL, CAUSESAPCOMMIT, dedstartworkdate, notfill, ddlTitle.SelectedValue, NumAccident, txtaccidentstartdate, txtaccidentenddate, txtsettleto, cboMainPlant.Value + string.Empty, EMPSAPID, dtUpload, dtUpload2);

                            Detail += "แก้ไขข้อมูลพนักงานใน TMS<br/>";

                            Detail += log + "<br/>";
                            VendorBLL.Instance.LogInsert(Detail, Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);

                            if (editStatus)
                            {
                                if (SendEmail(ConfigValue.VendorEmployeeStatus, "แก้ไข", string.Empty))
                                {
                                    mess += MessEmailSuccess;
                                }
                                else
                                {
                                    mess += MessEmailFail;
                                }
                            }
                            mess += log + "แก้ไขข้อมูลพนักงานใน TMS สำเร็จ<br/ >";
                        }
                    }

                    #endregion
                }
                mess = mess.Replace("\r\n", "<br/>");
                mess = mess.Replace("\n", "<br/>");
                if (Check == "Y" || P4)
                {
                    alertSuccess(mess, BackUrl.Replace("~/", ""));
                }
                else
                {
                    alertFail(mess);
                }
            }
            catch (Exception ex)
            {
                mess = mess.Replace("\r\n", "<br/>");
                mess = mess.Replace("\n", "<br/>");
                alertFail("บันทึกไม่สำเร็จ : " + RemoveSpecialCharacters(ex.Message) + "<br />" + mess);
            }

        }
    }

    private string getDataFromUpload2(string type)
    {
        string getValue = "";
        for (int i = 0; i < dtUpload2.Rows.Count; i++)
        {
            //getValue = dtUpload2.Rows[i]["UPLOAD_ID"].ToString();
            switch (type)
            {
                case "perCode":
                    if (string.Equals(dtUpload2.Rows[i]["UPLOAD_ID"].ToString(), "31"))
                    {
                        getValue = dtUpload2.Rows[i]["DOC_NUMBER"].ToString().Trim().Replace(" ", "").Replace("-", "");
                        return getValue;
                    }
                    break;

                case "LicenseNo":
                    if (string.Equals(dtUpload2.Rows[i]["UPLOAD_ID"].ToString(), "33"))
                    {
                        getValue = dtUpload2.Rows[i]["DOC_NUMBER"].ToString().Trim().Replace(" ", "");
                        return getValue;
                    }
                    break;

                case "LicenseFrom":
                    if (string.Equals(dtUpload2.Rows[i]["UPLOAD_ID"].ToString(), "33"))
                    {
                        getValue = dtUpload2.Rows[i]["START_DATE"].ToString();
                        if (!string.Equals(dtUpload2.Rows[i]["START_DATE"].ToString().Trim(), ""))
                            getValue = DateTime.ParseExact(dtUpload2.Rows[i]["START_DATE"].ToString(), "dd/MM/yyyy", new CultureInfo("en-US")).ToString("dd/MM/yyyy");
                        return getValue;
                    }
                    break;

                case "LicenseTo":
                    if (string.Equals(dtUpload2.Rows[i]["UPLOAD_ID"].ToString(), "33"))
                    {
                        getValue = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        if (!string.Equals(dtUpload2.Rows[i]["STOP_DATE"].ToString().Trim(), ""))
                            getValue = DateTime.ParseExact(dtUpload2.Rows[i]["STOP_DATE"].ToString(), "dd/MM/yyyy", new CultureInfo("en-US")).ToString("dd/MM/yyyy");

                        //getValue = dtUpload2.Rows[i]["STOP_DATE"].ToString();
                        return getValue;
                    }
                    break;

                default:
                    break;
            }
        }
        return getValue;
    }
    #endregion

    #region mpConfirmSaveDraft_ClickOK
    protected void mpConfirmSaveDraft_ClickOK(object sender, EventArgs e)
    {
        SaveDraft = true;
        btnSave_Click(null, null);
    }
    #endregion

    #region mpConfirmEdit_ClickOK
    protected void mpConfirmEdit_ClickOK(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
        {
            string Percode = txtPercode.Text.Trim().Replace(" ", "").Replace("-", "");
            DateTime? txtsettleto = null;
            DateTime date = new DateTime();
            int day = 0;
            if (DateTime.TryParseExact(txtSettleTo.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
            {
                txtsettleto = date;
                day = (txtsettleto.Value - DateTime.Now).Days;
            }
            string vendor = string.Empty;
            if (cboVendor.Value != null)
            {
                vendor = cboVendor.Value.ToString();
            }
            string Detail = "ขอข้อมูลเพิ่มเติม<br/>";
            Detail += "สถานะคำขอ : รข. ขอข้อมูลเพิ่มเติม<br/>";
            Detail += "รายละเอียด : " + txtDescription.Text + "<br/>";
            VendorBLL.Instance.LogInsert(Detail, Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);
            EditOrNoApprove("3", Detail + "{0}ส่งขอข้อมูลเพิ่มเติมสำเร็จ<br/>", "ขอข้อมูลเพิ่มเติม");
        }
        else
        {
            alertFail("กรุณาป้อน หมายเหตุ !!!");
        }
    }
    #endregion

    #region mpConfirmApprove_ClickOK
    protected void mpConfirmApprove_ClickOK(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
        {
            SaveDraft = false;
            SaveEdit = true;
            btnSave_Click(null, null);
        }
        else
        {
            alertFail("กรุณาป้อน หมายเหตุ !!!");
        }
    }
    #endregion

    #region mpConfirmNoApprove_ClickOK
    protected void mpConfirmNoApprove_ClickOK(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtDescription.Text.Trim()))
        {
            string Percode = txtPercode.Text.Trim().Replace(" ", "").Replace("-", "");
            DateTime? txtsettleto = null;
            DateTime date = new DateTime();
            int day = 0;
            if (DateTime.TryParseExact(txtSettleTo.Text, "dd/MM/yyyy", null, DateTimeStyles.None, out date))
            {
                txtsettleto = date;
                day = (txtsettleto.Value - DateTime.Now).Days;
            }
            string vendor = string.Empty;
            if (cboVendor.Value != null)
            {
                vendor = cboVendor.Value.ToString();
            }
            string Detail = "ปฎิเสธคำขอ เพิ่ม/แก้ไข ข้อมูลพนักงาน<br/>";
            Detail += "สถานะคำขอ : รข. ปฏิเสธ<br/>";
            Detail += "รายละเอียด : " + txtDescription.Text + "<br/>";
            VendorBLL.Instance.LogInsert(Detail, Percode, txtsettleto, "0", "0", int.Parse(Session["UserID"].ToString()), vendor, day);
            EditOrNoApprove("2", Detail + "{0}ปฎิเสธคำขอ เพิ่ม/แก้ไข ข้อมูลพนักงานสำเร็จ<br/>", "ปฎิเสธ");
        }
        else
        {
            alertFail("กรุณาป้อน หมายเหตุ !!!");
        }
    }
    #endregion

    #region EditOrNoApprove
    private void EditOrNoApprove(string Status, string mess, string Event)
    {
        try
        {
            DataTable dt = VendorBLL.Instance.VendorPKEditOrApprove(Session["UserID"].ToString(), txtDescription.Text, REQ_ID, Status);
            if (dt.Rows[0]["Message"] == DBNull.Value)
            {
                if (SendEmail(ConfigValue.VendorEmployeeAddReturn, Event, Status))
                {
                    mess = mess.Replace("{0}", MessEmailSuccess);
                }
                else
                {
                    mess = mess.Replace("{0}", MessEmailFail);
                }
                alertSuccess(mess, BackUrl.Replace("~/", ""));
            }
        }
        catch (Exception ex)
        {

            alertFail("บันทึกไม่สำเร็จ : " + RemoveSpecialCharacters(ex.Message) + "<br />");
        }
    }
    #endregion
    #endregion

    #region IfIsNullOrEmtpy
    private string IfIsNullOrEmtpy(string strIf)
    {
        return string.IsNullOrEmpty(strIf) ? "ไม่ระบุ" : strIf;
    }
    private string IfIsNullOrEmtpy(string strIf, string strValue)
    {
        return string.IsNullOrEmpty(strIf) ? "ไม่ระบุ" : strValue;
    }
    #endregion

    #region DivLog
    private void DivLog(ref string log, string title, object drOld, string strNew)
    {
        string strData = drOld + string.Empty;
        if (log.Length <= 3900 && strData != strNew)
        {
            log += "<div class=\"row\">";
            log += "<label class=\"col-md-6  text-right\">" + title + " :</label>";
            log += "<div class=\"col-md-6\">" + IfIsNullOrEmtpy(strData) + "</div>";
            log += "</div>";
            log += "<div class=\"row \">";
            log += "<label class=\"col-md-6  text-right\">>></label>";
            log += "<div class=\"col-md-6\">" + IfIsNullOrEmtpy(strNew) + "</div>";
            log += "</div>";
        }
    }
    #endregion

    #region SendEmail
    private bool SendEmail(int TemplateID, string Event, string Status)
    {
        try
        {


            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                string EmpName = (ddlTitle.SelectedIndex == 0 ? "คุณ" : ddlTitle.SelectedItem.Text) + " " + txtName.Text + " " + txtSurName.Text;
                string EmailList = string.Empty, Vendor = cboVendor.SelectedItem.Text;
                if (TemplateID == ConfigValue.VendorEmployeeAdd)
                {
                    #region VendorEmployeeAdd
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), cboVendor.Value + string.Empty, true, false);
                    //EmailList = "nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + EmailList;
                    if (!string.IsNullOrEmpty(txtMail.Text))
                    {
                        EmailList += "," + txtMail.Text;
                    }
                    if (!string.IsNullOrEmpty(txtMail2.Text))
                    {
                        EmailList += "," + txtMail2.Text;
                    }
                    //EmailList = "zsuntipab.k@pttict.com,zsomchart.j@pttict.com,zmaliwan.p@pttict.com";
                    Subject = Subject.Replace("{Event}", Event);
                    Subject = Subject.Replace("{EmpName}", EmpName);
                    Body = Body.Replace("{EmpName}", EmpName);
                    Body = Body.Replace("{Event}", Event);
                    Body = Body.Replace("{Vendor}", Vendor);
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
                    string PK = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    plaintextBytes = Encoding.UTF8.GetBytes(REQ_ID);
                    string strID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    plaintextBytes = Encoding.UTF8.GetBytes(Status);
                    string status = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "vendor_employee_add1.aspx?PK=" + PK + "&strID=" + strID + "&status=" + status;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "VendorEmployeeAdd", ColumnEmailName);
                    #endregion

                }
                else if (TemplateID == ConfigValue.VendorEmployeeAddReturn)
                {
                    #region VendorEmployeeAddReturn
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), cboVendor.Value + string.Empty, true, false);
                    //EmailList = "nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + EmailList;
                    if (!string.IsNullOrEmpty(txtMail.Text))
                    {
                        EmailList += "," + txtMail.Text;
                    }
                    if (!string.IsNullOrEmpty(txtMail2.Text))
                    {
                        EmailList += "," + txtMail2.Text;
                    }
                    //EmailList = "zsuntipab.k@pttict.com,zsomchart.j@pttict.com,zmaliwan.p@pttict.com";
                    Subject = Subject.Replace("{Event}", Event).Replace("{EmpName}", EmpName);
                    Body = Body.Replace("{EmpName}", EmpName);
                    Body = Body.Replace("{Event}", Event);
                    Body = Body.Replace("{Vendor}", Vendor);
                    Body = Body.Replace("{Remark}", txtDescription.Text.Trim());
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
                    string P4 = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    plaintextBytes = Encoding.UTF8.GetBytes(REQ_ID);
                    string strID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    plaintextBytes = Encoding.UTF8.GetBytes(Status);
                    string status = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "vendor_employee_add1.aspx?p4=" + P4 + "&strID=" + strID + "&status=" + status;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "VendorEmployeeAdd", ColumnEmailName);
                    #endregion

                }
                else if (TemplateID == ConfigValue.VendorEmployeeStatus)
                {
                    #region VendorEmployeeStatus
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), cboVendor.Value + string.Empty, true, false);
                    //EmailList = "chansak.t@pttor.com,praewpailin.p@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + EmailList;
                    if (!string.IsNullOrEmpty(txtMail.Text))
                    {
                        EmailList += "," + txtMail.Text;
                    }
                    if (!string.IsNullOrEmpty(txtMail2.Text))
                    {
                        EmailList += "," + txtMail2.Text;
                    }
                    //EmailList = "zsuntipab.k@pttict.com,zsomchart.j@pttict.com,zmaliwan.p@pttict.com";
                    Subject = Subject.Replace("{EmpName}", EmpName).Replace("{Position}", ddlPosition.SelectedItem.Text);

                    Body = Body.Replace("{EmpName}", EmpName);
                    Body = Body.Replace("{Vendor}", Vendor);
                    Body = Body.Replace("{Status}", rblStatus.SelectedItem.Text);
                    Body = Body.Replace("{Position}", ddlPosition.SelectedItem.Text);
                    Body = Body.Replace("{Remark}", string.IsNullOrEmpty(RemarkStatus) ? "-" : RemarkStatus);
                    // Session["vendoraccountname"] หน่วยงาน {VendorAccountName}
                    Body = Body.Replace("{VendorAccountName}", Session["vendoraccountname"] + string.Empty);
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
                    string P4 = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    plaintextBytes = Encoding.UTF8.GetBytes(REQ_ID);
                    string strID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                    MailService.SendMail(EmailList, Subject, Body, "", "VendorEmployeeStatus", ColumnEmailName);
                    #endregion

                }
                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion


    protected void radSpot_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(radSpot.SelectedValue, "1"))
            {
                ddlContractSpot.Items.Clear();
                ddlContractSpot.ClearSelection();
                ddlContractSpot.Enabled = false;
            }
            else
            {
                ddlContractSpot.Enabled = true;
                this.GetContract();
            }
            
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void GetContract()
    {
        try
        {
            if (cboVendor.Value != null)
            {
                DataTable dt = ComplainBLL.Instance.ContractSelectBLL2(" AND CSPACIALCONTRAC = 'Y' AND TCONTRACT.SVENDORID = '" + cboVendor.Value + "'");
                DropDownListHelper.BindDropDownList(ref ddlContractSpot, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                ddlContractSpot.Items.Clear();
                ddlContractSpot.ClearSelection();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cboVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.GetContract();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void InsertDriverStatus(string I_DRIVERID, string I_PERSONALID, string I_FIRSTNAME, string I_LASTNAME, string I_DRIVERSTATUS, string I_CARIERCODE, string I_PICTUREPATH, string I_UPDATEDATETIME)
    {
        var YourModel = new { ID = I_DRIVERID, PERSONAL_ID = I_PERSONALID, FIRST_NAME = I_FIRSTNAME, LAST_NAME = I_LASTNAME, DRIVERSTATUS = I_DRIVERSTATUS, CARIER_CODE = I_CARIERCODE, PICTUREPATH = I_PICTUREPATH, UPDATEDATETIME = I_UPDATEDATETIME };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_driver/InsertDriver", stringContent)
                               .Result;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                //ผ่าน
            }

        }
    }
}