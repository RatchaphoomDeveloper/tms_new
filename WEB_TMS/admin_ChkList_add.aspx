﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_ChkList_add.aspx.cs" Inherits="admin_ChkList_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style15
        {
            height: 23px;
        }
        .style30
        {
            color: #CCCCCC;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function confirmDelete() {
            return confirm('คุณต้องการที่ลบรายการนี้ใช่หรือไม่ ?');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;lblFormName2.SetValue(txtExam1.GetValue());lblFormName3.SetValue(txtExam1.GetValue());}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF">
                            <dx:ASPxPageControl ID="PagControl" runat="server" ActiveTabIndex="0" ClientInstanceName="PagControl"
                                Width="100%">
                                <TabPages>
                                    <dx:TabPage Name="t1" Text="ชื่อแบบฟอร์ม">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            ประเภทแบบฟอร์ม<font color="#ff0000">*</font>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblTypeName" runat="server" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Text="ตรวจสภาพรถ" Value="1" Selected="true" />
                                                                    <dx:ListEditItem Text="ตรวจผลิตภัณฑ์" Value="2" />
                                                                    <dx:ListEditItem Text="ปฏิเสธการส่งรถ" Value="3" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                            <dx:ASPxTextBox ID="CheckUpdate" runat="server" Width="10px" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" style="width: 20%">
                                                            User Role <font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style15">
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag1n1" runat="server" CheckState="Unchecked" Text="ปง."
                                                                    CssClass="checkbox1" ClientInstanceName="chkPag1n1">
                                                                    <ClientSideEvents CheckedChanged="function (s, e){chkPag2n1.SetChecked(s.GetChecked());chkPag3n1.SetChecked(s.GetChecked());}" />
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag1n2" runat="server" CheckState="Unchecked" Text="คลังต้นทาง"
                                                                    ClientInstanceName="chkPag1n2">
                                                                    <ClientSideEvents CheckedChanged="function (s, e){chkPag2n2.SetChecked(s.GetChecked());chkPag3n2.SetChecked(s.GetChecked());}" />
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag1n3" runat="server" CheckState="Unchecked" Text="ระหว่างทาง"
                                                                    ClientInstanceName="chkPag1n3">
                                                                    <ClientSideEvents CheckedChanged="function (s, e){chkPag2n3.SetChecked(s.GetChecked());chkPag3n3.SetChecked(s.GetChecked());}" />
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag1n4" runat="server" CheckState="Unchecked" Text="คลังปลายทาง"
                                                                    ClientInstanceName="chkPag1n4">
                                                                    <ClientSideEvents CheckedChanged="function (s, e){chkPag2n4.SetChecked(s.GetChecked());chkPag3n4.SetChecked(s.GetChecked());}" />
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag1n5" runat="server" CheckState="Unchecked" Text="ผู้ประกอบการขนส่ง"
                                                                    ClientInstanceName="chkPag1n5">
                                                                    <ClientSideEvents CheckedChanged="function (s, e){chkPag2n5.SetChecked(s.GetChecked());chkPag3n5.SetChecked(s.GetChecked());}" />
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF">
                                                            ชื่อแบบฟอร์ม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style21">
                                                            <dx:ASPxTextBox ID="txtExam1" runat="server" MaxLength="200" Width="270px" ClientInstanceName="txtExam1">
                                                                <ClientSideEvents TextChanged="function (s, e){lblFormName2.SetValue(s.GetValue());lblFormName3.SetValue(s.GetValue())}" />
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            วันที่เริ่มใช้งาน <font color="#ff0000">*</font>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteDateStart1" runat="server" SkinID="xdte" ClientInstanceName="dteDateStart1">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" class="style25" valign="top">
                                                            สถานะ<font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" style="width: 80%">
                                                            <dx:ASPxRadioButtonList ID="rblStatus1" runat="server" ClientInstanceName="rblStatus1"
                                                                EnableDefaultAppearance="False" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                                                SelectedIndex="0" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                                                                    <dx:ListEditItem Text="InActive" Value="0" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="t2" Text="หมวดปัญหา">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" width="20%">
                                                            User Role <font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style15" colspan="2">
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag2n1" runat="server" CheckState="Unchecked" Text="ปง."
                                                                    ClientInstanceName="chkPag2n1" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag2n2" runat="server" CheckState="Unchecked" Text="คลังต้นทาง"
                                                                    ClientInstanceName="chkPag2n2" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag2n3" runat="server" CheckState="Unchecked" Text="ระหว่างทาง"
                                                                    ClientInstanceName="chkPag2n3" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag2n4" runat="server" CheckState="Unchecked" Text="คลังปลายทาง"
                                                                    ClientInstanceName="chkPag2n4" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag2n5" runat="server" CheckState="Unchecked" Text="ผู้ประกอบการขนส่ง"
                                                                    ClientInstanceName="chkPag2n5" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF">
                                                            ชื่อแบบฟอร์ม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style21" colspan="2">
                                                            <dx:ASPxLabel ID="lblFormName2" runat="server" Text="" ClientInstanceName="lblFormName2">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">
                                                            หมวดปัญหา <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2" class="style32">
                                                            <dx:ASPxTextBox ID="txtProb2" runat="server" Width="170px">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtIndex" runat="server" ClientVisible="False" Width="20px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ใช้กับผลิตภัณฑ์ <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2">
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkProductP2n1" runat="server" CheckState="Unchecked" Text="น้ำมัน"
                                                                    ClientInstanceName="chkProductP2n1">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkProductP2n2" runat="server" CheckState="Unchecked" Text="ก๊าซ"
                                                                    ClientInstanceName="chkProductP2n2">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" class="style25" valign="top">
                                                            สถานะ<font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" width="30%">
                                                            <dx:ASPxRadioButtonList ID="rblStatus2" runat="server" ClientInstanceName="rblStatus2"
                                                                SelectedIndex="0" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                                                                    <dx:ListEditItem Text="InActive" Value="0" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td align="center">
                                                            <dx:ASPxButton ID="btnAdd2" runat="server" Text="เพิ่ม" SkinID="_add">
                                                                <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('AddData1');}" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="left">
                                                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                                                KeyFieldName="dtnGroupID" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                                                SkinID="_gvw" Style="margin-top: 0px" Width="100%" OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared" >
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="1"
                                                                        Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="dtsID" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="2">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="หมวดปัญหา" FieldName="dtsGroupName" ShowInCustomizationForm="True"
                                                                        VisibleIndex="3" Width="33%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ใช้กับผลิตภัณฑ์" FieldName="ddd" ShowInCustomizationForm="True"
                                                                        VisibleIndex="11" Width="15%">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxLabel ID="lbloil" runat="server" Text='<%# Eval("dtcOil").ToString() + Eval("dtcGas").ToString() == "10"?"น้ำมัน":""%>'>
                                                                            </dx:ASPxLabel>
                                                                            <dx:ASPxLabel ID="lbloil2" runat="server" Text='<%# Eval("dtcOil").ToString() + Eval("dtcGas").ToString() == "01"?"ก๊าซ":""%>'>
                                                                            </dx:ASPxLabel>
                                                                            <dx:ASPxLabel ID="lbloil3" runat="server" Text='<%# Eval("dtcOil").ToString() + Eval("dtcGas").ToString() == "11"?"น้ำมันและก๊าซ":""%>'>
                                                                            </dx:ASPxLabel>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataCheckColumn Caption="สถานะ" ShowInCustomizationForm="True" VisibleIndex="13"
                                                                        Width="5%">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxCheckBox ID="ASPxCheckBox2" runat="server" Checked='<%# (Boolean.Parse(Eval("dtcActive").ToString()== "1"?"true":"false"))%>'
                                                                                ReadOnly="true">
                                                                            </dx:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataCheckColumn>
                                                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="15" Width="8%">
                                                                        <DataItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" SkinID="_edit">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('editdt1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbDel" runat="server" CausesValidation="false" SkinID="_delete">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('deletedt1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </DataItemTemplate>
                                                                        <CellStyle Cursor="hand">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsBehavior AllowSort="False" />
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                            </dx:ASPxGridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="t3" Text="หัวข้อคำถาม">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" style="width: 20%">
                                                            User Role <font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style15" colspan="2">
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag3n1" runat="server" CheckState="Unchecked" Text="ปง."
                                                                    CssClass="checkbox1" ClientInstanceName="chkPag3n1" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag3n2" runat="server" CheckState="Unchecked" Text="คลังต้นทาง"
                                                                    CssClass="checkbox1" ClientInstanceName="chkPag3n2" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag3n3" runat="server" CheckState="Unchecked" Text="ระหว่างทาง"
                                                                    CssClass="checkbox1" ClientInstanceName="chkPag3n3" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag3n4" runat="server" CheckState="Unchecked" Text="คลังปลายทาง"
                                                                    CssClass="checkbox1" ClientInstanceName="chkPag3n4" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkPag3n5" runat="server" CheckState="Unchecked" Text="ผู้ประกอบการขนส่ง"
                                                                    CssClass="checkbox1" ClientInstanceName="chkPag3n5" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF">
                                                            ชื่อแบบฟอร์ม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style21" colspan="2">
                                                            <dx:ASPxLabel ID="lblFormName3" runat="server" Text="" ClientInstanceName="lblFormName3">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            หมวดปัญหา <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2">
                                                            <dx:ASPxComboBox ID="cboProb3" runat="server" TextField="dtsGroupName" ValueField="dtsID"
                                                                ValueType="System.Int32">
                                                                <ClientSideEvents SelectedIndexChanged="function(s,e){xcpn.PerformCallback('cboProbSelectChanged')}" />
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาเลือกข้อมูล" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ใช้กับผลิตภัณฑ์ <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2">
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkProductP3n1" runat="server" CheckState="Unchecked" ClientInstanceName="chkProductP3n1"
                                                                    Text="น้ำมัน" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                            <div style="float: left">
                                                                <dx:ASPxCheckBox ID="chkProductP3n2" runat="server" CheckState="Unchecked" ClientInstanceName="chkProductP3n2"
                                                                    Text="ก๊าซ" ClientEnabled="false">
                                                                </dx:ASPxCheckBox>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            หัวข้อตัดคะแนนประเมินผล <font color="#ff0000">*</font>
                                                        </td>
                                                        <td class="style15" colspan="2">
                                                            <dx:ASPxComboBox ID="cboTopic3" runat="server" CallbackPageSize="30" ClientInstanceName="cboTopic3"
                                                                EnableCallbackMode="True" OnItemRequestedByValue="cboTopic3_OnItemRequestedByValueSQL"
                                                                OnItemsRequestedByFilterCondition="cboTopic3_OnItemsRequestedByFilterConditionSQL"
                                                                SkinID="xcbbATC" TextFormatString="{1}" ValueField="STOPICID_SVERSION" Width="150px">
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัสหัวข้อตัดคะแนน" FieldName="STOPICID" Width="100px" />
                                                                    <dx:ListBoxColumn Caption="หัวข้อตัดคะแนน" FieldName="STOPICNAME" Width="320px" />
                                                                    <dx:ListBoxColumn Caption="สถานะ" Width="70px" FieldName="SACTIVE" />
                                                                </Columns>
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาเลือกข้อมูล" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsTopic" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            หัวข้อคำถาม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td class="style15" colspan="2">
                                                            <dx:ASPxTextBox ID="txtTopicQ3" runat="server" Width="600px">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtIndexQ3" runat="server" Width="20px" ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox runat="server" Width="20px" ClientVisible="False" ID="txtIDChkList">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            กลุ่มหัวข้อคำถาม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2">
                                                            <dx:ASPxComboBox ID="cboCheckListGroup" runat="server" TextField="dtsCheckListGroupName" ValueField="dtsCheckListGroupID"
                                                                ValueType="System.Int32">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาเลือกข้อมูล" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ตัดคะแนน
                                                        </td>
                                                        <td colspan="2">
                                                            <dx:ASPxCheckBox ID="chkCut3" runat="server" CheckState="Unchecked" Text="ทันที">
                                                            </dx:ASPxCheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ห้ามวิ่ง
                                                        </td>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxRadioButtonList ID="rblBan" runat="server" SkinID="rblStatus" CssClass="dxeLineBreakFix">
                                                                            <Items>
                                                                                <dx:ListEditItem Selected="true" Text="ทันที" Value="1" />
                                                                                <dx:ListEditItem Text="แก้ไขภายใน" Value="2" />
                                                                            </Items>
                                                                        </dx:ASPxRadioButtonList>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtEditDay" runat="server" CssClass="dxeLineBreakFix" Width="40px">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp วัน
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            เวอร์ชั่น
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtVersion3" runat="server" Width="40px" ClientEnabled="false"
                                                                NullText="N/A">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <span class="style30">*การแก้ไขข้อมูล ระบบจะทำการปรับเวอร์ชั่นให้อัตโนมัติ</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" class="style25" valign="top">
                                                            สถานะ<font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" style="width: 20%">
                                                            <dx:ASPxRadioButtonList ID="rblStatus3" runat="server" ClientInstanceName="rblStatus3"
                                                                SelectedIndex="0" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                                                                    <dx:ListEditItem Text="InActive" Value="0" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td align="center">
                                                            <dx:ASPxButton ID="btnAdd3" runat="server" Text="เพิ่ม" SkinID="_add">
                                                                <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('AddData2');}" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <dx:ASPxTextBox ID="txtDelIndex" runat="server" ClientInstanceName="txtDelIndex" Width="10px" ClientVisible="false"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <dx:ASPxGridView ID="gvwTopicQ" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwTopicQ"
                                                                KeyFieldName="dtnTopicQID" SkinID="_gvw" Style="margin-top: 0px" Width="100%" OnHtmlDataCellPrepared="gvwTopicQ_HtmlDataCellPrepared" >
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="ข้อที่" FieldName="dtnTopicQID" ShowInCustomizationForm="True"
                                                                        Width="5%" VisibleIndex="1">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัสTopic" FieldName="dtnTopicID" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="3">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="TopicVersion" FieldName="dtnTopicVersion" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="4">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัสปัญหา" FieldName="dtnProbID" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="5">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="version" FieldName="dtnVersion" ShowInCustomizationForm="True"
                                                                        Width="5%" VisibleIndex="6">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ห้ามวิ่ง" FieldName="dtcBan" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="7">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="หัวข้อคำถาม" FieldName="dtnTopicQName" ShowInCustomizationForm="True"
                                                                        VisibleIndex="8" Width="33%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="left">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="หัวข้อตัดคะแนนประเมินผล" FieldName="dtsTopicName"
                                                                        ShowInCustomizationForm="True" VisibleIndex="9" Width="25%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="left">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="กลุ่มหัวข้อคำถาม" FieldName="dtnTopicGroupName" ShowInCustomizationForm="True"
                                                                         VisibleIndex="10" Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="left">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ตัดคะแนน" FieldName="ff" ShowInCustomizationForm="True"
                                                                        VisibleIndex="11" Width="5%">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxLabel ID="ddd" runat="server" Text='<%# Eval("dtcCut").ToString() == "1"?"ทันที":"-"%>'>
                                                                            </dx:ASPxLabel>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataCheckColumn Caption="สถานะ" ShowInCustomizationForm="True" VisibleIndex="12"
                                                                        Width="5%">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxCheckBox ID="ASPxCheckBox3" runat="server" Checked='<%# (Boolean.Parse(Eval("dtcActive").ToString()== "1"?"true":"false"))%>'
                                                                                ReadOnly="true">
                                                                            </dx:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataCheckColumn>
                                                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="13" Width="8%">
                                                                        <DataItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbedit0" runat="server" CausesValidation="False" SkinID="_edit">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('editDT2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" SkinID="_delete">
                                                                                          <ClientSideEvents Click="function (s, e) {txtDelIndex.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); checkBeforeDelete(function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('deleteDT2'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                                                                             </ClientSideEvents>
                                                                                           <%-- <ClientSideEvents Click="function (s, e) {if(confirmDelete()){xcpn.PerformCallback('deleteDT2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }}" />--%>
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </DataItemTemplate>
                                                                        <CellStyle Cursor="hand">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataTextColumn Caption="จำนวนวัน" FieldName="dtnDAY_MA" Visible="false">
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                                <SettingsBehavior AllowSort="False" />
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                            </dx:ASPxGridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnSave" runat="server" SkinID="_submit">
                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Save');}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                                                            <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_ChkList_lst.aspx'; }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                </TabPages>
                            </dx:ASPxPageControl>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" align="left">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img height="1px" src="images/spacer.GIF" width="250px"></img></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
