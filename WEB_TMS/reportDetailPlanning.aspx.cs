﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.IO;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;

public partial class reportDetailPlanning : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(STCrypt.encryptMD5("p@ssw0rd"));
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ExportFile("pdf");
    }
    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        ExportFile("xls");
    }
    public void ExportFile(string fileType)
    {
        int _COLUMN0 = 0;
        string _comm = "";
        _COLUMN0 = adeEnd.Date.DayOfYear - adeStart.Date.DayOfYear + 1;
        if (!string.IsNullOrEmpty(cbxTerminalStart.Value + ""))
        {
            _comm += " AND SNAME = '" + cbxTerminalStart.Text + "'";
        }
        if (!string.IsNullOrEmpty(cbxTerminal.Value + ""))
        {
            _comm += " AND SABBREVIATION = '" + cbxTerminal.Text + "'";
        }
        #region
        //        string sqlquery = @"SELECT TerminalSTART,TerminalEND," + (adeEnd.Date.DayOfYear - adeStart.Date.DayOfYear) + @" COLUMN0
        //        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        //        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        //        ,NVL(SUM(COLUMN1),0) COLUMN1,NVL(SUM(COLUMN5),0) AS COLUMN5
        //        ,NVL(SUM(COLUMN6),0) AS COLUMN6,NVL(SUM(COLUMN10),0) AS COLUMN10,NVL(SUM(COLUMN11),0) AS COLUMN11
        //        ,NVL(SUM(COLUMN13),0) AS COLUMN13,NVL(SUM(COLUMN14),0) AS COLUMN14
        //        FROM (SELECT TB4.SNAME AS TerminalSTART,TB2.sTerminalName AS TerminalEND
        //        ,CASE WHEN dPlan BETWEEN TO_DATE('" + adeStart.Date.ToS   tring("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
        //        AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy') THEN
        //        COUNT(dPlan) END AS COLUMN1,SUM(COLUMN13) AS COLUMN13,SUM(COLUMN5) AS COLUMN5
        //        ,SUM(COLUMN6) AS COLUMN6,SUM(COLUMN10) AS COLUMN10,SUM(COLUMN11) AS COLUMN11,SUM(COLUMN14) AS COLUMN14
        //        FROM TPlanSchedule TB1 INNER JOIN TTerminal_SAP TB2 ON TB1.sTerminalID = TB2.sTerminalID
        //        INNER JOIN TUSER TB3 ON TB3.SUID = TB1.SCREATE
        //        INNER JOIN (SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
        //        UNION
        //        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal) TB4 ON TB4.SCODE = TB3.SVENDORID
        //        LEFT JOIN (SELECT NPLANID,CASE WHEN CUSTOMER_NO IS NULL THEN 0 ELSE  COUNT(SHIP_TO) END COLUMN10
        //        ,CASE WHEN CUSTOMER_NO IS NULL AND  NVL(CCONFIRM,'0') <> '0' THEN 0 ELSE  COUNT(SHIP_TO) END COLUMN11        
        //        ,CASE WHEN CUSTOMER_NO IS NOT NULL THEN 0 ELSE  COUNT(*) END COLUMN13
        //        ,CASE WHEN CUSTOMER_NO IS NOT NULL AND NVL(CCONFIRM,'0') <> '0' THEN 0 ELSE  COUNT(SHIP_TO) END COLUMN14
        //        ,NVL(COUNT(SHIP_TO),0) AS COLUMN5
        //        ,CASE WHEN CCONFIRM ='1' THEN COUNT(NPLANID) END AS COLUMN6
        //        FROM TPLANSCHEDULELIST INNER JOIN TSHIPMENT ON SSHIPTO = SHIP_TO
        //        INNER JOIN TPlanSchedule ON TPLANSCHEDULELIST.NPLANID = TPlanSchedule.NPLANID
        //        WHERE NPLANID IN (SELECT NPLANID FROM TPlanSchedule 
        //        WHERE dPlan BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
        //        AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy'))
        //        GROUP BY NPLANID,CUSTOMER_NO,CCONFIRM ) TB5 ON TB5.NPLANID = TB1.NPLANID
        //
        //        GROUP BY TB4.SNAME,TB2.sTerminalName,dPlan,CCONFIRM 
        //        order by TB4.SNAME,TB2.sTerminalName,dPlan )TB
        // 
        //        GROUP BY TerminalSTART,TerminalEND ";
        #endregion
        string sqlquery1 = @"SELECT SNAME TerminalSTART,TS.STERMINALNAME TerminalEND," + _COLUMN0 + @" COLUMN0 
        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        ,TRUNC(P.SPLANDATE),P.STERMINALID
        ,CASE WHEN SDELIVERYNO LIKE '8%' OR SDELIVERYNO LIKE '008%' THEN 1 ELSE 0 END COLUMN13
        FROM (((TPlanSchedule p LEFT JOIN TPlanScheduleList pl ON P.NPLANID = PL.NPLANID) 
        LEFT JOIN TTERMINAL t ON P.STERMINALID = T.STERMINALID) 
        LEFT JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID) 
        LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT GROUP BY SHIPMENT_NO,DELIVERY_NO) o ON PL.SDELIVERYNO = o.DELIVERY_NO
        INNER JOIN TUSER tu ON tu.SUID = p.SCREATE
        INNER JOIN (SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
        UNION
        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal) TB4 ON TB4.SCODE = tu.SVENDORID
        WHERE 1=1 AND  p.CACTIVE = '1' AND pl.CACTIVE = '1'
        AND TRUNC(DPLAN) BETWEEN TO_DATE('" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy') AND 
        TO_DATE('" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy')";

        string sqlquery2 = @"SELECT TerminalSTART,TerminalEND,dStart,dEnd,SUM(COLUMN10) COLUMN10 ,SUM(COLUMN13) COLUMN13,DPLAN
        FROM 
        (SELECT distinct SNAME TerminalSTART,SABBREVIATION TerminalEND," + _COLUMN0 + @" COLUMN0 
        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        ,TRUNC(DPLAN) DPLAN ,NVL(CCONFIRM,'0') CCONFIRM,TB2.SUPPLY_PLANT,0 COLUMN5
        ,CASE WHEN CUSTOMER_NO IS NOT NULL THEN 1 ELSE 0 END COLUMN10
        ,CASE WHEN SUPPLY_PLANT IS NOT NULL AND PLANT IS NOT NULL THEN 1 ELSE 0 END COLUMN13
        ,TB5.NPLANID,CUSTOMER_NO 
        FROM TDELIVERY TB1 
        INNER JOIN (SELECT DELIVERY_NO,SUPPLY_PLANT,PLANT FROM TDELIVERY_SAP
        GROUP BY DELIVERY_NO,SUPPLY_PLANT,PLANT )TB2 
        ON TB1.DELIVERY_NO = TB2.DELIVERY_NO
        LEFT OUTER JOIN (SELECT SHIP_TO,CUSTOMER_NO FROM  TSHIPMENT GROUP BY SHIP_TO,CUSTOMER_NO)TB3 ON TB1.SHIP_TO = TB3.SHIP_TO
        LEFT JOIN TPLANSCHEDULELIST TB4 ON TB4.SSHIPTO = TB3.SHIP_TO
        LEFT JOIN TPLANSCHEDULE TB5 ON TB5.NPLANID = TB4.NPLANID
        INNER JOIN TTERMINAL TB6 ON TB6.STERMINALID  = TB5.STERMINALID 
        INNER JOIN TUSER TB7 ON TB7.SUID = TB5.SCREATE
        INNER JOIN (SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
        UNION
        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal) TB8 ON TB8.SCODE = TB7.SVENDORID
        WHERE TRUNC(DPLAN) BETWEEN TO_DATE('" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy') AND 
        TO_DATE('" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/MM/yyyy')) tb
        GROUP BY TerminalSTART,TerminalEND,dStart,dEnd,DPLAN";

        _comm = "";
        if (!string.IsNullOrEmpty(cbxTerminalStart.Value + ""))
        {
            _comm += " AND TerminalSTART = '" + cbxTerminalStart.Text + "'";
        }
        if (!string.IsNullOrEmpty(cbxTerminal.Value + ""))
        {
            _comm += " AND TerminalEND = '" + cbxTerminal.Text + "'";
        }

        string sqlqueryGroup = @"SELECT distinct TerminalSTART,TerminalEND,COUNT(COLUMN1) COLUMN1," + _COLUMN0 + @" COLUMN0 
        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        ,0 COLUMN5,0 COLUMN6,0 COLUMN10,0 COLUMN11,0 COLUMN13 ,0 COLUMN14,'' COLUMN3
        FROM ( SELECT SNAME TerminalSTART,SABBREVIATION TerminalEND
       ,TRUNC(DPLAN) COLUMN1
        FROM TPLANSCHEDULE TB5 
        LEFT JOIN TPLANSCHEDULELIST PLNLST ON TB5.NPLANID=PLNLST.NPLANID 
        INNER JOIN TTERMINAL TB6 ON TB6.STERMINALID = TB5.STERMINALID 
        INNER JOIN TUSER TB7 ON TB7.SUID = TB5.SCREATE 
        INNER JOIN (SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
        UNION 
        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal) TB8 ON TB8.SCODE = TB7.SVENDORID 
        WHERE 1=1 AND  TB5.CACTIVE = '1' AND TB5.CACTIVE = '1' AND 
        TRUNC(DPLAN) BETWEEN TO_DATE('" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy') AND 
        TO_DATE('" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy')
        GROUP BY SNAME,SABBREVIATION,TRUNC(DPLAN))
        WHERE 1=1 " + _comm + @"
        GROUP BY TerminalSTART,TerminalEND ORDER BY TerminalSTART,TerminalEND ";

        sqlqueryGroup = @"SELECT distinct TerminalSTART,TerminalEND,COUNT(COLUMN1) COLUMN1," + _COLUMN0 + @" COLUMN0 
        ,'" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        ,0 COLUMN5,0 COLUMN6,0 COLUMN10,0 COLUMN11,0 COLUMN13 ,0 COLUMN14,'' COLUMN3
        FROM (
                    SELECT SNAME TerminalSTART,SABBREVIATION TerminalEND
                   ,TRUNC(DLV.DELIVERY_DATE) COLUMN1
                    FROM TBL_ORDERPLAN TB5 
                    LEFT JOIN TDELIVERY DLV On TB5.SDELIVERYNO =DLV.DELIVERY_NO
--                  LEFT JOIN TPLANSCHEDULELIST PLNLST ON TB5.NPLANID=PLNLST.NPLANID 
                    INNER JOIN TTERMINAL TB6 ON TB6.STERMINALID = TB5.STERMINALID 
                    INNER JOIN TUSER TB7 ON TB7.SUID = TB5.SCREATE 
                    INNER JOIN (
                        SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                        UNION 
                        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
                    ) TB8 ON TB8.SCODE = TB7.SVENDORID 
                    WHERE 1=1 AND  NVL(TB5.CACTIVE,'Y') != 'N'  
                    AND TRUNC(DLV.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy') AND TO_DATE('" + adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy')
                    GROUP BY SNAME,SABBREVIATION ,TRUNC(DLV.DELIVERY_DATE)
        )
        WHERE 1=1 " + _comm + @"
        GROUP BY TerminalSTART,TerminalEND ORDER BY TerminalSTART,TerminalEND";

        string Qry_GroupNextDate = @"SELECT distinct TerminalSTART,TerminalEND,COUNT(COLUMN1) COLUMN1,1 COLUMN0 
        ,'" + adeEnd.Date.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        ,0 COLUMN5,0 COLUMN6,0 COLUMN10,0 COLUMN11,0 COLUMN13 ,0 COLUMN14,'' COLUMN3,0 COLUMN2 
        FROM ( SELECT SNAME TerminalSTART,SABBREVIATION TerminalEND
       ,TRUNC(DPLAN) COLUMN1
        FROM TPLANSCHEDULE TB5 
        LEFT JOIN TPLANSCHEDULELIST PLNLST ON TB5.NPLANID=PLNLST.NPLANID 
        INNER JOIN TTERMINAL TB6 ON TB6.STERMINALID = TB5.STERMINALID 
        INNER JOIN TUSER TB7 ON TB7.SUID = TB5.SCREATE 
        INNER JOIN (SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
        UNION 
        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal) TB8 ON TB8.SCODE = TB7.SVENDORID 
        WHERE 1=1 AND  TB5.CACTIVE = '1' AND TB5.CACTIVE = '1' AND 
        TRUNC(DPLAN) BETWEEN TO_DATE('" + adeEnd.Date.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy') AND 
        TO_DATE('" + adeEnd.Date.AddDays(1).Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy')
        GROUP BY SNAME,SABBREVIATION,TRUNC(DPLAN))
        WHERE 1=1 " + _comm + @"
        GROUP BY TerminalSTART,TerminalEND ORDER BY TerminalSTART,TerminalEND ";


        Qry_GroupNextDate = @"SELECT distinct TerminalSTART,TerminalEND,COUNT(COLUMN1) COLUMN1,1 COLUMN0 
        ,'" + adeStart.Date.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dStart
        ,'" + adeEnd.Date.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("th-th")) + @"' dEnd
        ,0 COLUMN5,0 COLUMN6,0 COLUMN10,0 COLUMN11,0 COLUMN13 ,0 COLUMN14,'' COLUMN3
        FROM (
                    SELECT SNAME TerminalSTART,SABBREVIATION TerminalEND
                   ,TRUNC(DLV.DELIVERY_DATE) COLUMN1
                    FROM TBL_ORDERPLAN TB5 
                    LEFT JOIN TDELIVERY DLV On TB5.SDELIVERYNO =DLV.DELIVERY_NO
--                  LEFT JOIN TPLANSCHEDULELIST PLNLST ON TB5.NPLANID=PLNLST.NPLANID 
                    INNER JOIN TTERMINAL TB6 ON TB6.STERMINALID = TB5.STERMINALID 
                    INNER JOIN TUSER TB7 ON TB7.SUID = TB5.SCREATE 
                    INNER JOIN (
                        SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                        UNION 
                        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
                    ) TB8 ON TB8.SCODE = TB7.SVENDORID 
                    WHERE 1=1 AND  NVL(TB5.CACTIVE,'Y') != 'N'  
                    AND TRUNC(DLV.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy') AND TO_DATE('" + adeEnd.Date.AddDays(1).ToString("dd/MM/yyyy", new CultureInfo("en-us")) + @"','dd/mm/yyyy')
                    GROUP BY SNAME,SABBREVIATION,TRUNC(DLV.DELIVERY_DATE)
        )
        WHERE 1=1 " + _comm + @"
        GROUP BY TerminalSTART,TerminalEND ORDER BY TerminalSTART,TerminalEND";

        #region Qry All DO
        string Qry_ALLDO = @" 
SELECT SHPT_DLVR_GRP.SHIPMENT_NO ,LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0') DELIVERY_NO,NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT) SUPPLY_PLANT,
TRUNC(SHPT_VEH_GRP.DELIVERY_DATE) DPLAN , TBL_PLANS.SNAME  TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
,(CASE WHEN TBL_PLANS.SDELIVERYNO is not null THEN 1 ELSE 0 END) IS_PLANED 
FROM SAPECP1000087.SHIPMENT_DELIVERY_GROUP@MASTER SHPT_DLVR_GRP
LEFT JOIN SAPECP1000087.SHIPMENT_VEHICLE_GROUP@MASTER SHPT_VEH_GRP ON SHPT_DLVR_GRP.SHIPMENT_NO=SHPT_VEH_GRP.SHIPMENT_NO
LEFT JOIN (
    SELECT DLVR.DELIVERY_NO ,DLVRSAP.SUPPLY_PLANT  
    FROM TDELIVERY DLVR 
    LEFT JOIN TDELIVERY_SAP DLVRSAP ON DLVR.DELIVERY_NO=DLVRSAP.DELIVERY_NO 
    WHERE TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') 
    GROUP BY  DLVR.DELIVERY_NO ,DLVRSAP.SUPPLY_PLANT
)TBL_DLVR ON LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0')=LPAD(TBL_DLVR.DELIVERY_NO,10,'0')
LEFT JOIN (
    SELECT  PLN. STERMINALID,PLN.SVENDORID  ,LPAD(PLNLST.SDELIVERYNO,10,'0') SDELIVERYNO ,SNAME
    FROM  TPLANSCHEDULE  PLN
    LEFT JOIN TPLANSCHEDULELIST PLNLST ON PLN.NPLANID=PLNLST.NPLANID
    LEFT JOIN TUSER USR ON USR.SUID = PLN.SCREATE 
    LEFT JOIN (
                SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                UNION 
                SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
    ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
    WHERE TRUNC(PLN.DPLAN) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  
    GROUP BY PLN. STERMINALID,PLN.SVENDORID  ,LPAD(PLNLST.SDELIVERYNO,10,'0') ,SNAME
)TBL_PLANS ON LPAD(TBL_PLANS.SDELIVERYNO,10,'0') = LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0') 
LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT),'xxxx')
WHERE SHPT_VEH_GRP.CARRIER !='PHTDPRT' AND NVL(NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT),'NONE_PLAN') !='NONE_PLAN'
AND TRUNC(SHPT_VEH_GRP.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  

GROUP BY SHPT_DLVR_GRP.SHIPMENT_NO  ,LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0'),NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT) ,
TRUNC(SHPT_VEH_GRP.DELIVERY_DATE) 
,TBL_PLANS.SDELIVERYNO,PLANT.SABBREVIATION, TBL_PLANS.SNAME
 ";
        /*
                Qry_ALLDO = @"SELECT '' SHIPMENT_NO,
            DLVR.DELIVERY_NO ,DLVR.SPLNT_CODE SUPPLY_PLANT  ,DLVR.DELIVERY_DATE DPLAN ,TBL_USER.SNAME TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
            --,NVL(ODP.CACTIVE,'X') CACTIVE,NVL(ODP.ORDERID,'xxxxxx-xxxx') ORDERID 
            ,(CASE WHEN NVL(ODP.CACTIVE,'X') ='Y' THEN 1 ELSE 0 END) IS_PLANED
            FROM TDELIVERY DLVR 
            LEFT JOIN TBL_ORDERPLAN ODP ON  CAST(DLVR.DELIVERY_NO AS number)=CAST(ODP.SDELIVERYNO AS number) 
            LEFT JOIN TUSER USR ON USR.SUID = ODP.SCREATE 
            LEFT JOIN (
                        SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                        UNION 
                        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
            ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
            LEFT JOIN TTERMINAL PLANTA ON  NVL(PLANTA.STERMINALID,'xxxx')  = NVL(ODP.STERMINALID ,'xxxx')
            LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(DLVR.SPLNT_CODE ,'xxxx')
            WHERE TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') 
            AND NVL(ODP.CACTIVE,'X') !='N' 
        ";*/
        /*
                Qry_ALLDO = @"SELECT SHMT.SHIPMENT_NO
        ,CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='0000096')  then '70'||SUBSTR(DLVR.DELIVERY_NO,3,8) ELSE DLVR.DELIVERY_NO END  DELIVERY_NO--, DLVR.DELIVERY_NO 
        ,DLVR.SPLNT_CODE SUPPLY_PLANT  ,DLVR.DELIVERY_DATE DPLAN ,TBL_USER.SNAME TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
        ,SUBSTR(SHMT.SHIP_TO,0,7) SHIPTO
        ,(CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='0000096')  then 'COD' ELSE (CASE WHEN DLVR.DELIVERY_NO like '008%' AND SUBSTR(SHMT.SHIP_TO,0,7) in ('0000095','0000098')  THEN 'TOC' ELSE '' END) END)  SHIP_MODE
        ,(CASE WHEN NVL(ODP.CACTIVE,'X') ='Y' THEN 1 ELSE 0 END) IS_PLANED
            FROM TDELIVERY DLVR 
            LEFT JOIN TBL_ORDERPLAN ODP ON  CAST(DLVR.DELIVERY_NO AS number)=CAST(ODP.SDELIVERYNO AS number) 
            LEFT JOIN (
                SELECT SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
                FROM TSHIPMENT
                WHERE  1=1 --AND SHIPPING_POINT is not null  
                GROUP BY SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
            ) SHMT ON DLVR.DELIVERY_NO = SHMT.DELIVERY_NO 
            LEFT JOIN TUSER USR ON USR.SUID = ODP.SCREATE 
            LEFT JOIN (
                        SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                        UNION 
                        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
            ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
            LEFT JOIN TTERMINAL PLANTA ON  NVL(PLANTA.STERMINALID,'xxxx')  = NVL(ODP.STERMINALID ,'xxxx')
            LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(DLVR.SPLNT_CODE ,'xxxx')
            WHERE NVL(ODP.CACTIVE,'X') !='N' AND SHMT.MAT_FRT_GRP IN('CLEAN','DIRTY','AVI')  AND SUBSTR(NVL(SHMT.SHIPPING_POINT,'A000'),0,1) IN('D','E','A','')
            AND TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  
        ";
        */

        Qry_ALLDO = @"SELECT SHMT.SHIPMENT_NO
,CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='000009I')  then '70'||SUBSTR(DLVR.DELIVERY_NO,3,8) ELSE DLVR.DELIVERY_NO END  DELIVERY_NO--, DLVR.DELIVERY_NO 
,DLVR.SPLNT_CODE SUPPLY_PLANT  ,DLVR.DELIVERY_DATE DPLAN ,TBL_USER.SNAME TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
,SUBSTR(SHMT.SHIP_TO,0,7) SHIPTO
,(CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='000009I')  then 'COD' ELSE (CASE WHEN DLVR.DELIVERY_NO like '008%' AND SUBSTR(SHMT.SHIP_TO,0,7) in ('000009H','000009K')  THEN 'TOC' ELSE '' END) END)  SHIP_MODE
,(CASE WHEN NVL(ODP.CACTIVE,'X') ='Y' THEN 1 ELSE 0 END) IS_PLANED
    FROM TDELIVERY DLVR 
    LEFT JOIN TBL_ORDERPLAN ODP ON  CAST(DLVR.DELIVERY_NO AS number)=CAST(ODP.SDELIVERYNO AS number) 
    LEFT JOIN (
        SELECT SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
        FROM TSHIPMENT
        WHERE  1=1 --AND SHIPPING_POINT is not null  
        GROUP BY SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
    ) SHMT ON DLVR.DELIVERY_NO = SHMT.DELIVERY_NO 
    LEFT JOIN TUSER USR ON USR.SUID = ODP.SCREATE 
    LEFT JOIN (
                SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                UNION 
                SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
    ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
    LEFT JOIN TTERMINAL PLANTA ON  NVL(PLANTA.STERMINALID,'xxxx')  = NVL(ODP.STERMINALID ,'xxxx')
    LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(DLVR.SPLNT_CODE ,'xxxx')
    WHERE NVL(ODP.CACTIVE,'X') !='N' AND SHMT.MAT_FRT_GRP IN('CLEAN','DIRTY','AVI')  AND SUBSTR(NVL(SHMT.SHIPPING_POINT,'A000'),0,1) IN('D','E','A','')
    AND TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  
";

        #endregion
        #region Qry Next Date
        string Qry_NextDO = @" 
SELECT SHPT_DLVR_GRP.SHIPMENT_NO ,LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0') DELIVERY_NO,NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT) SUPPLY_PLANT,
TRUNC(SHPT_VEH_GRP.DELIVERY_DATE) DPLAN , TBL_PLANS.SNAME  TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
,(CASE WHEN TBL_PLANS.SDELIVERYNO is not null THEN 1 ELSE 0 END) IS_PLANED 
FROM SAPECP1000087.SHIPMENT_DELIVERY_GROUP@MASTER SHPT_DLVR_GRP
LEFT JOIN SAPECP1000087.SHIPMENT_VEHICLE_GROUP@MASTER SHPT_VEH_GRP ON SHPT_DLVR_GRP.SHIPMENT_NO=SHPT_VEH_GRP.SHIPMENT_NO
LEFT JOIN (
    SELECT DLVR.DELIVERY_NO ,DLVRSAP.SUPPLY_PLANT  
    FROM TDELIVERY DLVR 
    LEFT JOIN TDELIVERY_SAP DLVRSAP ON DLVR.DELIVERY_NO=DLVRSAP.DELIVERY_NO 
    WHERE TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') 
    GROUP BY  DLVR.DELIVERY_NO ,DLVRSAP.SUPPLY_PLANT
)TBL_DLVR ON LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0')=LPAD(TBL_DLVR.DELIVERY_NO,10,'0')
LEFT JOIN (
    SELECT  PLN. STERMINALID,PLN.SVENDORID  ,LPAD(PLNLST.SDELIVERYNO,10,'0') SDELIVERYNO ,SNAME
    FROM  TPLANSCHEDULE  PLN
    LEFT JOIN TPLANSCHEDULELIST PLNLST ON PLN.NPLANID=PLNLST.NPLANID
    LEFT JOIN TUSER USR ON USR.SUID = PLN.SCREATE 
    LEFT JOIN (
                SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                UNION 
                SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
    ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
    WHERE TRUNC(PLN.DPLAN) BETWEEN TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  
    GROUP BY PLN. STERMINALID,PLN.SVENDORID  ,LPAD(PLNLST.SDELIVERYNO,10,'0') ,SNAME
)TBL_PLANS ON LPAD(TBL_PLANS.SDELIVERYNO,10,'0') = LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0') 
LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT),'xxxx')
WHERE SHPT_VEH_GRP.CARRIER !='PHTDPRT' AND NVL(NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT),'NONE_PLAN') !='NONE_PLAN'
AND TRUNC(SHPT_VEH_GRP.DELIVERY_DATE) BETWEEN TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY')  

GROUP BY SHPT_DLVR_GRP.SHIPMENT_NO  ,LPAD(SHPT_DLVR_GRP.DELIVERY_NO,10,'0'),NVL(SHPT_DLVR_GRP.SUPPLY_PLANT,TBL_DLVR.SUPPLY_PLANT) ,
TRUNC(SHPT_VEH_GRP.DELIVERY_DATE) 
,TBL_PLANS.SDELIVERYNO,PLANT.SABBREVIATION, TBL_PLANS.SNAME
 ";
        /*
                Qry_NextDO = @"SELECT '' SHIPMENT_NO,
            DLVR.DELIVERY_NO ,DLVR.SPLNT_CODE SUPPLY_PLANT  ,DLVR.DELIVERY_DATE DPLAN ,TBL_USER.SNAME TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
            --,NVL(ODP.CACTIVE,'X') CACTIVE,NVL(ODP.ORDERID,'xxxxxx-xxxx') ORDERID 
            ,(CASE WHEN NVL(ODP.CACTIVE,'X') ='Y' THEN 1 ELSE 0 END) IS_PLANED
            FROM TDELIVERY DLVR  
            LEFT JOIN TBL_ORDERPLAN ODP ON  CAST(DLVR.DELIVERY_NO AS number)=CAST(ODP.SDELIVERYNO AS number) 
            LEFT JOIN TUSER USR ON USR.SUID = ODP.SCREATE 
            LEFT JOIN (
                        SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                        UNION 
                        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
            ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
            LEFT JOIN TTERMINAL PLANTA ON  NVL(PLANTA.STERMINALID,'xxxx')  = NVL(ODP.STERMINALID ,'xxxx')
            LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(DLVR.SPLNT_CODE,'xxxx')
            WHERE TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') 
            AND NVL(ODP.CACTIVE,'X') !='N'
        ";
        */

        /*
        Qry_ALLDO = @"SELECT SHMT.SHIPMENT_NO
,CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='0000096')  then '70'||SUBSTR(DLVR.DELIVERY_NO,3,8) ELSE DLVR.DELIVERY_NO END  DELIVERY_NO--, DLVR.DELIVERY_NO 
,DLVR.SPLNT_CODE SUPPLY_PLANT  ,DLVR.DELIVERY_DATE DPLAN ,TBL_USER.SNAME TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
,SUBSTR(SHMT.SHIP_TO,0,7) SHIPTO
,(CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='0000096')  then 'COD' ELSE (CASE WHEN DLVR.DELIVERY_NO like '008%' AND SUBSTR(SHMT.SHIP_TO,0,7) in ('0000095','0000098')  THEN 'TOC' ELSE '' END) END)  SHIP_MODE
,(CASE WHEN NVL(ODP.CACTIVE,'X') ='Y' THEN 1 ELSE 0 END) IS_PLANED
    FROM TDELIVERY DLVR 
    LEFT JOIN TBL_ORDERPLAN ODP ON  CAST(DLVR.DELIVERY_NO AS number)=CAST(ODP.SDELIVERYNO AS number) 
    LEFT JOIN (
        SELECT SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
        FROM TSHIPMENT
        WHERE  1=1 --AND SHIPPING_POINT is not null  
        GROUP BY SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
    ) SHMT ON DLVR.DELIVERY_NO = SHMT.DELIVERY_NO 
    LEFT JOIN TUSER USR ON USR.SUID = ODP.SCREATE 
    LEFT JOIN (
                SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                UNION 
                SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
    ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
    LEFT JOIN TTERMINAL PLANTA ON  NVL(PLANTA.STERMINALID,'xxxx')  = NVL(ODP.STERMINALID ,'xxxx')
    LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(DLVR.SPLNT_CODE ,'xxxx')
    WHERE NVL(ODP.CACTIVE,'X') !='N' AND SHMT.MAT_FRT_GRP IN('CLEAN','DIRTY','AVI')  AND SUBSTR(NVL(SHMT.SHIPPING_POINT,'A000'),0,1) IN('D','E','A','')
    AND TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') 
";
*/

        Qry_ALLDO = @"SELECT SHMT.SHIPMENT_NO
,CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='000009I')  then '70'||SUBSTR(DLVR.DELIVERY_NO,3,8) ELSE DLVR.DELIVERY_NO END  DELIVERY_NO--, DLVR.DELIVERY_NO 
,DLVR.SPLNT_CODE SUPPLY_PLANT  ,DLVR.DELIVERY_DATE DPLAN ,TBL_USER.SNAME TERMINALSTART ,PLANT.SABBREVIATION TERMINALEND
,SUBSTR(SHMT.SHIP_TO,0,7) SHIPTO
,(CASE WHEN DLVR.DELIVERY_NO like '7%' OR (SUBSTR(SHMT.SHIP_TO,0,7)='000009I')  then 'COD' ELSE (CASE WHEN DLVR.DELIVERY_NO like '008%' AND SUBSTR(SHMT.SHIP_TO,0,7) in ('000009H','000009K')  THEN 'TOC' ELSE '' END) END)  SHIP_MODE
,(CASE WHEN NVL(ODP.CACTIVE,'X') ='Y' THEN 1 ELSE 0 END) IS_PLANED
    FROM TDELIVERY DLVR 
    LEFT JOIN TBL_ORDERPLAN ODP ON  CAST(DLVR.DELIVERY_NO AS number)=CAST(ODP.SDELIVERYNO AS number) 
    LEFT JOIN (
        SELECT SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
        FROM TSHIPMENT
        WHERE  1=1 --AND SHIPPING_POINT is not null  
        GROUP BY SHIPMENT_NO ,DELIVERY_NO ,PLANT ,MAT_FRT_GRP ,SHIPPING_POINT ,VAL_TYPE ,SHIP_TO
    ) SHMT ON DLVR.DELIVERY_NO = SHMT.DELIVERY_NO 
    LEFT JOIN TUSER USR ON USR.SUID = ODP.SCREATE 
    LEFT JOIN (
                SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
                UNION 
                SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal
    ) TBL_USER ON TBL_USER.SCODE = USR.SVENDORID 
    LEFT JOIN TTERMINAL PLANTA ON  NVL(PLANTA.STERMINALID,'xxxx')  = NVL(ODP.STERMINALID ,'xxxx')
    LEFT JOIN TTERMINAL PLANT ON  NVL(PLANT.STERMINALID,'xxxx')  = NVL(DLVR.SPLNT_CODE ,'xxxx')
    WHERE NVL(ODP.CACTIVE,'X') !='N' AND SHMT.MAT_FRT_GRP IN('CLEAN','DIRTY','AVI')  AND SUBSTR(NVL(SHMT.SHIPPING_POINT,'A000'),0,1) IN('D','E','A','')
    AND TRUNC(DLVR.DELIVERY_DATE) BETWEEN TO_DATE('" + adeStart.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') AND  TO_DATE('" + adeEnd.Date.AddDays(1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/DD/YYYY') 
";

        #endregion
        //Response.Write(sqlquery2); return;

        DataTable _dtALLDO = CommonFunction.Get_Data(sql, Qry_ALLDO);
        DataTable _dtNextDO = CommonFunction.Get_Data(sql, Qry_NextDO);
        DataTable _dtGroup = CommonFunction.Get_Data(sql, sqlqueryGroup);
        DataTable _dtGroup3 = CommonFunction.Get_Data(sql, Qry_GroupNextDate);
        //DataTable _dt1 = CommonFunction.Get_Data(sql, sqlquery1);
        //DataTable _dt2 = CommonFunction.Get_Data(sql, sqlquery2);
        //DataTable _dtGroup2 = CommonFunction.Get_Data(sql, sqlqueryGroup2);
        //DataTable _dt3 = CommonFunction.Get_Data(sql, sqlquery3);
        //DataTable _dt4 = CommonFunction.Get_Data(sql, sqlquery4);
        //DataTable _dtTmp = _dt.Clone();
        #region Prepaire Data
        foreach (DataRow _dr in _dtGroup.Rows)
        {

            _dr["COLUMN6"] = _dtALLDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND IS_PLANED='1' ").Length;//จำนวนงานที่จัดแผนผ่านระบบ        // int.Parse(_dr["COLUMN11"] + "") + int.Parse(_dr["COLUMN14"] + "");
            _dr["COLUMN5"] = _dtALLDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "'").Length;//งานทั้งหมด(โอนคลัง+COD) ของคลัง        //try { _dr["COLUMN5"] = int.Parse(_dr["COLUMN10"] + "") + int.Parse(_dr["COLUMN13"] + ""); }catch { _dr["COLUMN5"] = 0; }
            _dr["COLUMN13"] = _dtALLDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND DELIVERY_NO like '008%'").Length;//งานของคลัง ที่เป็นโอนคลัง ทั้งหมด       //(_dt2.Rows.Count > 0) ? _dt2.Compute("SUM(COLUMN13)", " TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + "' ") : 0;
            _dr["COLUMN10"] = _dtALLDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND DELIVERY_NO like '7%'").Length;//งานของคลัง ที่เป็นCOD ทั้งหมด      //(_dt2.Rows.Count > 0) ? _dt2.Compute("SUM(COLUMN10)", " TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + "' ") : 0;
            _dr["COLUMN11"] = _dtALLDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND IS_PLANED='1' AND DELIVERY_NO like '7%'").Length;//งานCOD ที่จัดแผนผ่านระบบ     // _dt1.Select(" TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + "' AND COLUMN13 = '1' ").Length;
            _dr["COLUMN14"] = _dtALLDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND IS_PLANED='1' AND DELIVERY_NO like '008%'").Length;//งานโอนคลังที่จัดแผนผ่านระบบ     //_dt1.Select(@" TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + @"' AND COLUMN13 = '0' ").Length;
            int COLUMN1 = 0;
            for (int i = 0; i < adeEnd.Date.DayOfYear - adeStart.Date.DayOfYear + 1; i++)
            {
                if (_dtALLDO.Select(" DPLAN = #" + adeStart.Date.AddDays(i).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + "# AND TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + "'  ").Length == 0)//
                {
                    _dr["COLUMN3"] += (string.IsNullOrEmpty(_dr["COLUMN3"] + "") ? "" : ", ") + adeStart.Date.AddDays(i).ToString("dd/MM/yyyy", new CultureInfo("th-th"));
                }
                else { COLUMN1++; }
            }
            _dr["COLUMN1"] = COLUMN1;
        }
        #endregion
        xrtDetailPlanning report = new xrtDetailPlanning();
        report.DataSource = _dtGroup;
        int col0 = 0; double defDbl = 0.00;
        #region Prepaire Data next Date
        foreach (DataRow _dr in _dtGroup3.Rows)
        {
            _dr["COLUMN6"] = _dtNextDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND IS_PLANED='1' ").Length;//จำนวนงานที่จัดแผนผ่านระบบ        // int.Parse(_dr["COLUMN11"] + "") + int.Parse(_dr["COLUMN14"] + "");
            _dr["COLUMN5"] = _dtNextDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "'").Length;//งานทั้งหมด(โอนคลัง+COD) ของคลัง        //try { _dr["COLUMN5"] = int.Parse(_dr["COLUMN10"] + "") + int.Parse(_dr["COLUMN13"] + ""); }catch { _dr["COLUMN5"] = 0; }
            _dr["COLUMN13"] = _dtNextDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND DELIVERY_NO like '008%'").Length;//งานของคลัง ที่เป็นโอนคลัง ทั้งหมด       //(_dt2.Rows.Count > 0) ? _dt2.Compute("SUM(COLUMN13)", " TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + "' ") : 0;
            _dr["COLUMN10"] = _dtNextDO.Select("TERMINALEND='" + _dr["TerminalEND"] + "' AND DELIVERY_NO like '7%'").Length;//งานของคลัง ที่เป็นCOD ทั้งหมด      //(_dt2.Rows.Count > 0) ? _dt2.Compute("SUM(COLUMN10)", " TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + "' ") : 0;
            double dblCol6 = (double.TryParse("" + _dr["COLUMN6"], out defDbl) ? double.Parse("" + _dr["COLUMN6"]) : 0.00);
            double dblCol5 = (double.TryParse("" + _dr["COLUMN5"], out defDbl) ? double.Parse("" + _dr["COLUMN5"]) : 0.00);

            _dr["COLUMN14"] = dblCol6 / (dblCol5 > 0 ? dblCol5 : 1);

            int COLUMN1 = 0;
            for (int i = 0; i < adeEnd.Date.AddDays(1).DayOfYear - adeEnd.Date.AddDays(1).DayOfYear + 1; i++)
            {
                COLUMN1 += (_dtNextDO.Select(" DPLAN = #" + adeEnd.Date.AddDays(i + 1).ToString("MM/dd/yyyy", new CultureInfo("en-us")) + "# AND TerminalSTART = '" + _dr["TerminalSTART"] + "' AND TerminalEND = '" + _dr["TerminalEND"] + "'  ").Length == 0) ? 0 : 1;
            }
            _dr["COLUMN1"] = COLUMN1;
            _dr["COLUMN11"] = COLUMN1 / (int.TryParse("" + _dr["COLUMN0"], out col0) ? int.Parse("" + _dr["COLUMN0"]) : 1);
        }
        #endregion
        report.DetailReport5.DataSource = _dtGroup3;
        report.DetailReport6.DataSource = _dtGroup3;
        string fileName = "DetailPlaning_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        if (fileType == "xls")
            report.ExportToXls(stream);
        else
            report.ExportToPdf(stream);

        Response.ContentType = "application/" + fileType;
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName + "." + fileType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    protected void TP01RouteOnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsOrganiz.SelectCommand = @"SELECT STERMINALID, STERMINALNAME FROM (SELECT T.STERMINALID, TS.STERMINALNAME, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) 
        AS RN FROM TTERMINAL T  INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE T.STERMINALID || 
        TS.STERMINALNAME LIKE :fillter AND (T.STERMINALID LIKE :plant5 OR T.STERMINALID LIKE :plant8))  WHERE RN BETWEEN :startIndex AND :endIndex";
        sdsOrganiz.SelectParameters.Clear();
        sdsOrganiz.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsOrganiz.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("plant5", PlantHelper.Plant5);
        sdsOrganiz.SelectParameters.Add("plant8", PlantHelper.Plant8);

        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();

    }
    protected void TP01RouteOnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cbxTerminalStart_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cbxTerminalStart_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        SqlDataSource1.SelectCommand = @"SELECT SCODE,SNAME FROM (SELECT SCODE,SNAME FROM 
        (SELECT UNITCODE AS SCODE ,UNITABBR AS SNAME FROM TUNIT 
        UNION
        SELECT sTerminalID AS SCODE ,sAbbreviation AS SNAME FROM TTerminal) TB  
        WHERE  SNAME LIKE : fillter or SCODE LIKE : fillter AND SNAME IS NOT NULL) ";
        SqlDataSource1.SelectParameters.Clear();
        SqlDataSource1.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        //SqlDataSource1.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        //SqlDataSource1.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = SqlDataSource1;
        comboBox.DataBind();
    }
}