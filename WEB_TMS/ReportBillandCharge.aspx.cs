﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;

public partial class ReportBillandCharge : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            ListData();

        }
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "ใบเรียก")
        {
            ASPxTextBox txtUrlDownloadBill = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtUrlDownloadBill") as ASPxTextBox;
            txtUrlDownloadBill.ClientInstanceName = txtUrlDownloadBill.ID + "_" + e.VisibleIndex;
            txtUrlDownloadBill.Text = "download_invoice_request.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(e.CellValue + "")) + "";
            
        }
        if (e.DataColumn.Caption == "ใบเสร็จ")
        {
            
            ASPxTextBox txtCharge = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtCharge") as ASPxTextBox;

            txtCharge.ClientInstanceName = txtCharge.ID + "_" + e.VisibleIndex;
        }

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": ListData();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void ListData()
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Condition = " AND REQ.VEH_NO||REQ.TU_NO||VEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%'";
        }

        if (edtStart.Value != null && edtEnd.Value != null)
        {
            lblsTail.Text = edtStart.Text + " - " + edtEnd.Text;

            DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
            DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());

            Condition += " AND (TRUNC(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE)) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY')) ";
        }
        else
        {
            lblsTail.Text = " - ";
        }



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
, TO_CHAR(add_months(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),6516),'dd/MM/yyyy') as APPOINTMENT_DATE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
,NVL(RDC.FILE_PATH||RDC.FILE_SYSNAME,'xxx') as CHARGE
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN TBL_REQDOC RDC ON RDC.REQUEST_ID = REQ.REQUEST_ID AND RDC.DOC_TYPE = '0001'
WHERE 1=1 " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;

        }
        gvw.DataBind();
    }

//    private void ListReport(string Type)
//    {

//        string Condition = "";
//        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
//        //{
//        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
//        //}
//        //else
//        //{

//        //}



//        string QUERY = @"SELECT ROWNUM||'.' as NO,REQ.REQUEST_ID
//,REQ.VEH_NO
//,REQ.TU_NO
//,TCK.SCAR_NUM,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
//, TO_CHAR(add_months(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),6516),'dd/MM/yyyy') as APPOINTMENT_DATE
//,STU.STATUSREQ_NAME
//FROM TBL_REQUEST REQ
//LEFT JOIN TTRUCK TCK
//ON TCK.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
//LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
//LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
//LEFT JOIN TBL_STATUSREQ STU ON STU.STATUSREQ_ID = REQ.STATUS_FLAG
//" + Condition + "";

//        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
//        //if (dt.Rows.Count > 0)
//        //{
//        //    gvw.DataSource = dt;
//        //    gvw.DataBind();
//        //}


//        ReportOperationMonth report = new ReportOperationMonth();

//        #region function report

//        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
//        //{
//        //    string Date = "1/" + cboMonth.Value + "/" + cboYear.Text + "";
//        //    DateTime DMONTH = DateTime.Parse(Date);
//        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = DMONTH.ToString("MMMM", new CultureInfo("th-TH")) + " " + cboYear.Text;
//        //}
//        //else
//        //{
//        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = " - ";
//        //}

//        report.Name = "รายงานผลการดำเนินงานประจำเดือน";
//        report.DataSource = dt;
//        string fileName = "รายงานผลการดำเนินงานประจำเดือน_" + DateTime.Now.ToString("MMddyyyyHHmmss");

//        MemoryStream stream = new MemoryStream();


//        string sType = "";
//        if (Type == "P")
//        {
//            report.ExportToPdf(stream);
//            Response.ContentType = "application/pdf";
//            sType = ".pdf";
//        }
//        else
//        {
//            report.ExportToXls(stream);
//            Response.ContentType = "application/xls";
//            sType = ".xls";
//        }


//        Response.AddHeader("Accept-Header", stream.Length.ToString());
//        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
//        Response.AddHeader("Content-Length", stream.Length.ToString());
//        Response.ContentEncoding = System.Text.Encoding.ASCII;
//        Response.BinaryWrite(stream.ToArray());
//        Response.End();
//        #endregion
//    }

//    protected void btnPDF_Click(object sender, EventArgs e)
//    {
//        ListReport("P");
//    }

//    protected void btnExcel_Click(object sender, EventArgs e)
//    {
//        ListReport("E");
//    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion
}