﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="truck_info.aspx.cs" Inherits="truck_info" StylesheetTheme="Aqua" Culture="en-US" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            //วันที่
            $("#<%=dedtStart.ClientID %>").on('change', function (e) {
                $("#<%=dedtEnd.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate($(this).val());

            });
            $("#<%=StartDateVehicle.ClientID %>").on('change', function (e) {
                $("#<%=EndDateVehicle.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate($(this).val());

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; inEndRequestHandler();}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>
                        <dx:TabPage Name="Tab1" Text="ข้อมูลTC">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                    <div class="form-horizontal">
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">บริษัทผู้ขนส่ง :</label>
                                            <div class="col-md-4  ">
                                                <asp:DropDownList ID="cboVendor" runat="server" class="form-control" DataTextField="SABBREVIATION"
                                                    DataValueField="SVENDORID" Width="250px">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">สังกัดรถ :</label>
                                            <div class="col-md-4  ">
                                                <%--<dx:ASPxRadioButtonList runat="server" ID="cblstatus" RepeatDirection="Horizontal" ClientInstanceName="cblstatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="ทั้งหมด" Value="" Selected="true" />
                                                        <dx:ListEditItem Text="รถในสังกัด" Value="1" />
                                                        <dx:ListEditItem Text="รถไม่มีสังกัด" Value="0" />
                                                    </Items>

                                                </dx:ASPxRadioButtonList>--%>
                                                <asp:RadioButtonList ID="radStatusTC" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="ทั้งหมด" Value="" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="รถในสังกัด" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="รถไม่มีสังกัด" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">เวลาอัพเดทข้อมูล :</label>
                                            <div class="col-md-4  ">
                                                <asp:TextBox runat="server" ID="dedtStart" CssClass="datepicker" />
                                            </div>
                                            <label class="col-md-2 control-label">ถึง :</label>
                                            <div class="col-md-4  ">
                                                <asp:TextBox runat="server" ID="dedtEnd" CssClass="datepicker" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">การอนุญาตใช้งาน :</label>
                                            <div class="col-md-4  ">
                                                <dx:ASPxCheckBox runat="server" ID="chkAllow" Text="อนุญาต" ValueChecked="true" CssClass="dxeLineBreakFix">
                                                </dx:ASPxCheckBox>
                                                <dx:ASPxCheckBox runat="server" ID="chkDisallow" Text="ระงับรถชั่วคราว" CssClass="dxeLineBreakFix">
                                                </dx:ASPxCheckBox>
                                                <dx:ASPxCheckBox runat="server" ID="chkBacklist" Text="Blacklist" CssClass="dxeLineBreakFix">
                                                </dx:ASPxCheckBox>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                จำนวนรถบรรทุก :
                                            </label>
                                            <label class="col-md-2 control-label">
                                                <%=  gvwTruck.VisibleRowCount%>
                                                        &nbsp;&nbsp; คัน
                                            </label>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-2 control-label">ค้นหาข้อมูลรถ :</label>
                                            <div class="col-md-4  ">
                                                <dx:ASPxTextBox runat="server" ID="txtsHeadRegisterNo" NullText="หมายเลขทะเบียนรถ"
                                                    CssClass="dxeLineBreakFix" Width="300px">
                                                </dx:ASPxTextBox>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <div class="col-md-4">
                                                    <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search">
                                                        <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCH;'); }" />
                                                    </dx:ASPxButton>
                                                </div>
                                                <div class="col-md-4">
                                                    <dx:ASPxButton ID="btnClearSearch" ClientInstanceName="btnClearSearch" runat="server"
                                                        SkinID="_clearsearch">
                                                        <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCH_CANCEL;'); }" />
                                                    </dx:ASPxButton>
                                                </div>
                                                <div class="col-md-4">
                                                    <dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="btnExportTC_Click" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                                
                                                <label class="col-md-2 control-label">ประเภทสัญญาจ้าง :</label>
                                                <div class="col-md-4  ">
                                                   <asp:RadioButtonList ID="radSpotTruck" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                                </div>
                                            </div>
                                    </div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td bgcolor="#0E4999">
                                                <img src="images/spacer.GIF" width="250px" height="1px" alt="" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView ID="gvwTruck" runat="server" ClientInstanceName="gvwTruck" SkinID="_gvw"
                                                    Width="100%" Style="margin-top: 0px" AutoGenerateColumns="False" KeyFieldName="STRUCKID"
                                                    DataSourceID="sdsTruck">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
                                                    <Columns>

                                                        <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="วันที่อัพเดทล่าสุด" Visible="false">
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SABBREVIATION" Caption="ชื่อผู้ขนส่ง" Visible="true">
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา">
                                                            <DataItemTemplate>
                                                                <dx:ASPxHyperLink ID="lbkViewContract" ClientInstanceName="lbkViewContract" runat="server"
                                                                    CausesValidation="False" AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false"
                                                                    EnableTheming="false" CssClass="dxeLineBreakFix" Text='<%# ""+Eval("SCONTRACTNO") %>'>
                                                                    <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('VIEWCONTRACT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxHyperLink>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนรถ">
                                                            <DataItemTemplate>
                                                                <dx:ASPxHyperLink ID="lbkViewHRegNo" ClientInstanceName="lbkViewHRegNo" runat="server"
                                                                    CausesValidation="False" AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false"
                                                                    EnableTheming="false" CssClass="dxeLineBreakFix" Text='<%# ""+Eval("SHEADREGISTERNO") %>'>
                                                                    <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('VIEW;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxHyperLink>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SCARCATEGORY" Caption="ประเภทรถTC">
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NSLOT" Caption="จำนวนช่อง">
                                                            <PropertiesTextEdit DisplayFormatString="N0">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NTOTALCAPACITY" Caption="ปริมาตร">
                                                            <PropertiesTextEdit DisplayFormatString="N0">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataDateColumn FieldName="DUPDATE" Caption="วันที่อัพเดทล่าสุด">
                                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />

                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataDateColumn>
                                                        <dx:GridViewDataTextColumn FieldName="STATUS" Caption="การอนุญาตใช้งาน">
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="สังกัดรถ" FieldName="ISUSE" Visible="true" Width="8%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การจัดการ">
                                                            <HeaderTemplate>
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td align="center">
                                                                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" Width="50px" CausesValidation="False"
                                                                                AutoPostBack="false" Enabled='<%# CanWrite ? true : false %>' >
                                                                                <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('ADD;'); }" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxLabel ID="lblAdd" runat="Server" Text="การจัดการ" ClientVisible="false">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnEdit" runat="server" SkinID="_edit" Width="50px" CausesValidation="False"
                                                                    AutoPostBack="false">
                                                                    <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('EDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Cursor="hand" />
                                                            <CellStyle HorizontalAlign="Center" Cursor="hand" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="RTRUCKID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SCARTYPEID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false" />
                                                        <dx:GridViewDataTextColumn Caption="เอกสาร" FieldName="STATUS" Visible="true" Width="8%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnAge" runat="server" Text="ตรวจสอบ" Width="80px" CausesValidation="False"
                                                                    AutoPostBack="false">
                                                                    <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('AGE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="sdsTruck" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdTruck"></asp:SqlDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                    </div>        
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <%-- คั่นกลางระหว่างแท็บ --%>
                        <dx:TabPage Name="Tab1" Text="ข้อมูลรถ">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                    <div>
                                        <div class="form-horizontal">
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label">บริษัทผู้ขนส่ง :</label>
                                                <div class="col-md-4  ">
                                                    <asp:DropDownList ID="cboVendorVehicle" runat="server" class="form-control" DataTextField="SABBREVIATION"
                                                        DataValueField="SVENDORID" Width="250px">
                                                    </asp:DropDownList>
                                                </div>
                                                <label class="col-md-2 control-label">ประเภทสัญญาจ้าง :</label>
                                                <div class="col-md-4  ">
                                                   <asp:RadioButtonList ID="radSpot" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label">เวลาอัพเดทข้อมูล :</label>
                                                <div class="col-md-4  ">
                                                    <asp:TextBox runat="server" ID="StartDateVehicle" CssClass="datepicker" />
                                                </div>
                                                <label class="col-md-2 control-label">ถึง :</label>
                                                <div class="col-md-4  ">
                                                    <asp:TextBox runat="server" ID="EndDateVehicle" CssClass="datepicker" />
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label">การอนุญาตใช้งาน :</label>
                                                <div class="col-md-4  ">
                                                    <dx:ASPxCheckBox runat="server" ID="checkAllowVehicle" Text="อนุญาต" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxCheckBox runat="server" ID="chkDisallowVehicle" Text="ระงับรถชั่วคราว" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxCheckBox runat="server" ID="chkBacklistVehicle" Text="Blacklist" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxCheckBox>
                                                </div>
                                                <label class="col-md-2 control-label">
                                                    จำนวนรถบรรทุก :
                                                </label>
                                                <label class="col-md-2 control-label">
                                                    <%=  VehicleTruck.VisibleRowCount%>
                                                        &nbsp;&nbsp; คัน
                                                </label>
                                            </div>
                                            <div class="row form-group">
                                                <label class="col-md-2 control-label">ค้นหาข้อมูลรถ :</label>
                                                <div class="col-md-4  ">
                                                    <dx:ASPxTextBox runat="server" ID="txtRegisVehicle" NullText="หมายเลขทะเบียนรถ"
                                                        CssClass="form-control" Width="300px">
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-4">
                                                    <div class="col-md-4">
                                                        <dx:ASPxButton ID="btnSearchVehicle" ClientInstanceName="btnSearch" runat="server" SkinID="_search">
                                                            <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCHVEHICLE;'); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <dx:ASPxButton ID="ASPxButton4" ClientInstanceName="btnClearSearch" runat="server"
                                                            SkinID="_clearsearch">
                                                            <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCH_CANCELVEHICLE;'); }" />
                                                        </dx:ASPxButton>
                                                        <%--<dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxButton>--%>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <dx:ASPxButton ID="btnExportVehicle" runat="server" Text="Export" OnClick="btnExportVehicle_Click" CssClass="dxeLineBreakFix">
                                                        </dx:ASPxButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td bgcolor="#0E4999">
                                                    <img src="images/spacer.GIF" width="250px" height="1px" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <dx:ASPxGridView ID="VehicleTruck" runat="server" ClientInstanceName="VehicleTruck" SkinID="_gvw"
                                                        Width="100%" Style="margin-top: 0px" AutoGenerateColumns="False" KeyFieldName="STRUCKID" DataSourceID="sdsTruckVehicle" OnHtmlRowPrepared="vehiclegrid_HtmlRowPrepared">
                                                        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="วันที่อัพเดทล่าสุด" Visible="false">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="SABBREVIATION" Caption="ชื่อผู้ขนส่ง" Visible="true">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>

                                                            <dx:GridViewDataTextColumn FieldName="SCARTYPE" Caption="รูปแบบ" Visible="true">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxHyperLink ID="lbkViewContract" ClientInstanceName="lbkViewContract" runat="server"
                                                                        CausesValidation="False" AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false"
                                                                        EnableTheming="false" CssClass="dxeLineBreakFix" Text='<%# ""+Eval("SCONTRACTNO") %>'>
                                                                        <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('VIEWCONTRACT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxHyperLink>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนหัว">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนหาง">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="SCARCATEGORY" Caption="ประเภทรถ">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="NSLOT" Caption="จำนวนช่อง">
                                                                <PropertiesTextEdit DisplayFormatString="N0">
                                                                </PropertiesTextEdit>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="NTOTALCAPACITY" Caption="ปริมาตร">
                                                                <PropertiesTextEdit DisplayFormatString="N0">
                                                                </PropertiesTextEdit>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataDateColumn FieldName="DUPDATE" Caption="วันที่อัพเดทล่าสุด">
                                                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" />

                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataDateColumn>
                                                            <dx:GridViewDataTextColumn FieldName="CLASSGRP" Caption="CLASSGRP">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="STATUS" Caption="การอนุญาตใช้งาน">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="สังกัดรถ" FieldName="ISUSE" ShowInCustomizationForm="false"
                                                                Visible="true" VisibleIndex="10" Width="8%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                                <DataItemTemplate>
                                                                    <%# Eval("ISUSE").ToString()== "1"?"รถในสังกัด":"รถไม่มีสังกัด"%>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="การจัดการ">
                                                                <HeaderTemplate>
                                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <dx:ASPxButton ID="btnAddVehicle" runat="server" SkinID="" Width="50px" CausesValidation="true"
                                                                                    AutoPostBack="false" Text="จับคู่TC">
                                                                                    <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('ADDVEHICLE;'); }" />
                                                                                    <%--ADDVEHICLE--%>
                                                                                </dx:ASPxButton>
                                                                                <dx:ASPxLabel ID="lblAdd" runat="Server" Text="การจัดการ" ClientVisible="false">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxButton ID="btnEdit" runat="server" Text="อนุญาต/ระงับ" Width="100px" CausesValidation="False"
                                                                        AutoPostBack="false">
                                                                        <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('EDITVEHICLE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxButton>
                                                                </DataItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" Cursor="hand" />
                                                                <CellStyle HorizontalAlign="Center" Cursor="hand" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="RTRUCKID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="SCARTYPEID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false" />
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="sdsTruckVehicle" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdTruck"></asp:SqlDataSource>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
