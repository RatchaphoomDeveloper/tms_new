﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="contract_add_pk.aspx.cs" Inherits="contract_add_pk" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" HideContentOnCallback="False">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server">
                <div id="ContractAdd">
                    <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="980px"
                        HeaderText="รายละเอียดสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="pctInformation" runat="server">
                                <table width="100%">
                                    <tbody id="tbd_Infomation" runat="server">
                                        <tr style="display:none">
                                            <td colspan="2" style="">
                                                <asp:SqlDataSource ID="sdsContract" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdContract" SelectCommand="SELECT * FROM TCONTRACT WHERE SCONTRACTID = NVL( :CONTRACT_ID , SCONTRACTID ) ">
                                                    <SelectParameters>
                                                        <asp:SessionParameter SessionField="SCONTRACTID" Name="CONTRACT_ID" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                            <td style="width: 15%">
                                                <dx:ASPxButton ID="btnEditMode" runat="server" OnClick="btnEditMode_Click" Text="เปลี่ยนเป็นโหมดแก้ไข">
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnViewMode" runat="server" OnClick="btnViewMode_Click" Text="เปลี่ยนเป็นโหมดเรียกดู">
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr id="trModify_type" runat="Server">
                                            <td style="width: 20%; text-align: right">
                                                ประเภทเปลี่ยนแปลงข้อมูลสัญญา:
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cboModify_type" runat="Server" ClientInstanceName="cboModify_type">
                                                    <ClientSideEvents SelectedIndexChanged="function(s,e){ if(s.GetValue()=='0'){ txtContractNo.SetValue(txtCNO.GetValue()); }else{ txtCNO.SetValue(txtContractNo.GetValue()); txtContractNo.SetValue(''); txtContractNo.Focus(); } }" />
                                                    <Items>
                                                        <dx:ListEditItem Value="0" Text="แก้ไข/เพิ่มเติมสัญญา" Selected="true" />
                                                        <dx:ListEditItem Value="1" Text="ต่ออายุสัญญา" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทการเปลี่ยนแปลงข้อมูล" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td style="width: 15%">
                                                <table style="display: none;">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtRenewFrom" runat="Server" ClientInstanceName="txtRenewFrom">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtCNO" runat="Server" ClientInstanceName="txtCNO">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right">
                                                ประเภทการจัดจ้าง:
                                            </td>
                                            <td>
                                                <dx:ASPxRadioButtonList ID="cblProcurement" runat="server" ClientInstanceName="cblProcurement"
                                                    RepeatColumns="2">
                                                    <Items>
                                                        <dx:ListEditItem Text="จัดจ้างโดยสัญญา" Value="N" Selected="true" />
                                                        <dx:ListEditItem Text="จัดจ้างโดยวิธีพิเศษ" Value="Y" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทการจัดจ้าง" />
                                                    </ValidationSettings>
                                                    <Border BorderWidth="0px" />
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right">
                                                เลขที่สัญญา:
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtContractNo" runat="server" ClientInstanceName="txtContractNo"
                                                    Width="600">
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุเลขที่สัญญา" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right">
                                                ผู้ขนส่ง:
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cmbVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cmbVendor"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID" Width="350px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="360px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ผู้ขนส่ง" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsVendor" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdVendor" SelectCommand="SELECt * FROM TVENDOR WHERE NVL(CACTIVE,'1')='1'">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right">
                                                Mode การขนส่ง:
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cmbTransType" runat="server" CallbackPageSize="30" ClientInstanceName="cmbTransType"
                                                    EnableCallbackMode="True" DataSourceID="sdsTransportType" TextFormatString="{1}"
                                                    ValueField="STRANTYPEID" Width="200px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัส" FieldName="STRANTYPEID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อประเภทการขนส่ง" FieldName="STRANTYPENAME" Width="360px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทการขนส่ง" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTransportType" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdVendor" SelectCommand="SELECt STRANTYPEID ,STRANTYPENAME FROM TTRANSPORTTYPE ">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right">
                                                ประเภทสัญญา:
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cmbContractType" runat="server" CallbackPageSize="30" ClientInstanceName="cmbContractType"
                                                    EnableCallbackMode="True" DataSourceID="sdsContractType" TextFormatString="{1}"
                                                    ValueField="SCONTRACTTYPEID" Width="200px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัส" FieldName="SCONTRACTTYPEID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อประเภทสัญญา" FieldName="SCONTRACTTYPENAME" Width="360px" />
                                                        <dx:ListBoxColumn Caption="กลุ่มสัญญา" FieldName="CGROUP" Width="100px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทสัญญา" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsContractType" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdVendor" SelectCommand="SELECt * FROM TCONTRACTTYPE ">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right">
                                                ระยะเวลาสัญญา:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteStart" runat="server" ClientInstanceName="dteStart" CssClass="dxeLineBreakFix"
                                                                SkinID="xdte" NullText="ระหว่างวันที่">
                                                                <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                                    <RequiredField IsRequired="true" ErrorText="กรุณาระบุวันที่เริ่มต้นสัญญา" />
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td>
                                                            ถึง
                                                        </td>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                                                                SkinID="xdte" NullText="ถึงวันที่">
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right" valign="top">
                                                หมายเหตุ:
                                            </td>
                                            <td>
                                                <dx:ASPxMemo ID="mmoRemark" runat="Server" ClientInstanceName="mmoRemark" Columns="40"
                                                    Rows="3">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: right">
                                                สถานะสัญญา:
                                            </td>
                                            <td>
                                                <dx:ASPxRadioButtonList ID="rblCACTIVE" runat="server" ClientInstanceName="rblCACTIVE"
                                                    RepeatColumns="2">
                                                    <Items>
                                                        <dx:ListEditItem Text="ใช้งาน" Value="Y" Selected="true" />
                                                        <dx:ListEditItem Text="ยกเลิกใช้งาน" Value="N" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ สถานะ" />
                                                    </ValidationSettings>
                                                    <Border BorderWidth="0px" />
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnDoc" ClientInstanceName="rpn" runat="server" Width="980px"
                        HeaderText="เอกสารสำคัญของสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent1" runat="server">
                                <table width="100%">
                                    <tbody id="Tbody2" runat="server">
                                        <tr>
                                            <td colspan="2" style="">
                                                <asp:SqlDataSource ID="sdsDocument" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdsdsDocument" SelectCommand="SELECT SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE,SPATH,NVERSION ,'0' CNEW ,'0' CCHANGE,'0' CDEL FROM TCONTRACT_DOC WHERE SCONTRACTID LIKE  NVL( :CONTRACT_ID , SCONTRACTID) ">
                                                    <SelectParameters>
                                                        <asp:SessionParameter SessionField="SCONTRACTID" Name="CONTRACT_ID" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%; text-align: right;">
                                                แนบเอกสารสัญญา :
                                            </td>
                                            <td style="text-align: left;">
                                                <dx:ASPxUploadControl ID="ulcContractDoc" ClientInstanceName="ulcContractDoc" runat="server"
                                                    CssClass="dxeLineBreakFix" Width="70%" NullText="Click here to browse files..."
                                                    OnFileUploadComplete="ulcContractDoc_FileUploadComplete">
                                                    <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>"
                                                        MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                    </ValidationSettings>
                                                    <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png กรุณาตรวจสอบใหม่อีกที');}
                 else{txtSystemFileName.SetValue(e.callbackData.split('$')[0]); txtOriginalFileName.SetValue(e.callbackData.split('$')[1]); txtGPSMode.SetValue('UPLAODED'); gvwContractDoc.PerformCallback('BIND_CONTRACT_DOC;');} } " />
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxButton ID="btnUploadDoc" ClientInstanceName="btnUploadDoc" runat="server"
                                                    SkinID="_upload" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){ var msg=''; if(ulcContractDoc.GetText()==''){msg+='<br>แนบไฟล์เอกสารสัญญา';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{ulcContractDoc.Upload();}  }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxTextBox runat="server" ID="txtSystemFileName" ClientInstanceName="txtSystemFileName"
                                                    ClientVisible="false" />
                                                <dx:ASPxTextBox runat="server" ID="txtOriginalFileName" ClientInstanceName="txtOriginalFileName"
                                                    ClientVisible="false" />
                                                <dx:ASPxTextBox runat="server" ID="txtGPSMode" ClientInstanceName="txtGPSMode" ClientVisible="false" />
                                            </td>
                                            <td style="width: 15%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <dx:ASPxGridView ID="gvwContractDoc" ClientInstanceName="gvwContractDoc" runat="server"
                                                    SkinID="_gvw">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SFILENAME" Caption="เอกสาร" Width="90%">
                                                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Left" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="6%">
                                                            <DataItemTemplate>
                                                                <table border="0" cellspacing="1" cellpadding="1">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnEditContractDoc" ClientInstanceName="btnEditGuarantee" runat="server"
                                                                                ToolTip="ดูเอกสาร" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                                Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwContractDoc.InCallback()) gvwContractDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnDelContractDoc" ClientInstanceName="btnDelGuarantee" runat="server"
                                                                                ToolTip="ลบรายการ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                                Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwContractDoc.InCallback()) gvwContractDoc.PerformCallback('DEL_CONTRACT_DOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="6%" Visible="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnEditContractDoc" ClientInstanceName="btnEditGuarantee" runat="server"
                                                                    ToolTip="ดูเอกสาร" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                    EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwContractDoc.InCallback()) gvwContractDoc.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDOCID" Caption="SDOCID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDOCVERSION" Caption="SDOCVERSION" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDOCTYPE" Caption="SDOCTYPE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SFILENAME" Caption="SFILENAME" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SSYSFILENAME" Caption="SSYSFILENAME" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDESCRIPTION" Caption="SDESCRIPTION" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NVERSION" Caption="NVERSION" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DEXPIRE" Caption="DEXPIRE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" />
                                                    </Columns>
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%; text-align: right;">
                                                แนบเอกสารอื่นๆ :
                                            </td>
                                            <td style="text-align: left;">
                                                <dx:ASPxUploadControl ID="ulcContractFile" ClientInstanceName="ulcContractFile" runat="server"
                                                    CssClass="dxeLineBreakFix" Width="70%" NullText="Click here to browse files..."
                                                    OnFileUploadComplete="ulcContractFile_FileUploadComplete">
                                                    <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>"
                                                        MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                    </ValidationSettings>
                                                    <ClientSideEvents FileUploadComplete="function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png กรุณาตรวจสอบใหม่อีกที');}
                 else{txtSystemFileName_File.SetValue(e.callbackData.split('$')[0]); txtOriginalFileName_File.SetValue(e.callbackData.split('$')[1]); txtGPSMode_File.SetValue('UPLAODED'); gvwContractFile.PerformCallback('BIND_CONTRACT_FILE;');} } " />
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxButton ID="btnUploadFile" ClientInstanceName="btnUploadFile" runat="server"
                                                    SkinID="_upload" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){ var msg=''; if(ulcContractFile.GetText()==''){msg+='<br>แนบไฟล์เอกสารอื่นๆ';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{ulcContractFile.Upload();}  }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxTextBox runat="server" ID="txtSystemFileName_File" ClientInstanceName="txtSystemFileName_File"
                                                    ClientVisible="false" />
                                                <dx:ASPxTextBox runat="server" ID="txtOriginalFileName_File" ClientInstanceName="txtOriginalFileName_File"
                                                    ClientVisible="false" />
                                                <dx:ASPxTextBox runat="server" ID="txtGPSMode_File" ClientInstanceName="txtGPSMode_File"
                                                    ClientVisible="false" />
                                            </td>
                                            <td style="width: 15%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <dx:ASPxGridView ID="gvwContractFile" ClientInstanceName="gvwContractFile" runat="server"
                                                    SkinID="_gvw">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SFILENAME" Caption="เอกสาร" Width="90%">
                                                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Left" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="6%">
                                                            <DataItemTemplate>
                                                                <table border="0" cellspacing="1" cellpadding="1">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnEditContractFile" ClientInstanceName="btnEditContractFile"
                                                                                runat="server" ToolTip="ดูเอกสาร" CausesValidation="False" AutoPostBack="false"
                                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                                CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwContractFile.InCallback()) gvwContractFile.PerformCallback('VIEWFILE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnDelContractFile" ClientInstanceName="btnDelContractFile" runat="server"
                                                                                ToolTip="ลบรายการ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                                Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwContractFile.InCallback()) gvwContractFile.PerformCallback('DEL_CONTRACT_FILE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="6%" Visible="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnEditContractFile" ClientInstanceName="btnEditContractFile"
                                                                    runat="server" ToolTip="ดูเอกสาร" CausesValidation="False" AutoPostBack="false"
                                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                    CssClass="dxeLineBreakFix" Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwContractFile.InCallback()) gvwContractFile.PerformCallback('VIEWFILE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDOCID" Caption="SDOCID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDOCVERSION" Caption="SDOCVERSION" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDOCTYPE" Caption="SDOCTYPE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SFILENAME" Caption="SFILENAME" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SSYSFILENAME" Caption="SSYSFILENAME" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SDESCRIPTION" Caption="SDESCRIPTION" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SPATH" Caption="SPATH" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NVERSION" Caption="NVERSION" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CACTIVE" Caption="CACTIVE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DCREATE" Caption="DCREATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SCREATE" Caption="SCREATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="DUPDATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="SUPDATE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DEXPIRE" Caption="DEXPIRE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" />
                                                    </Columns>
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnGuarantee" ClientInstanceName="rpn" runat="server" Width="980px"
                        HeaderText="หลักประกันสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="pctGuarantee" runat="server">
                                <table width="100%">
                                    <tbody id="tbdGuarantee" runat="server">
                                        <tr>
                                            <td style="">
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <dx:ASPxGridView ID="gvwGuarantee" runat="server" ClientInstanceName="gvwGuarantee"
                                                    AutoGenerateColumns="False" KeyFieldName="NCGID" SkinID="_gvw">
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SGUARANTEETYPE" Caption="ประเภท" Width="18%"
                                                            VisibleIndex="1">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Left" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SBOOKNO" Caption="เลขที่เอกสาร" ShowInCustomizationForm="false"
                                                            VisibleIndex="2">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <FooterTemplate>
                                                                <dx:ASPxLabel ID="lblTotal" runat="Server" ClientInstanceName="lblTotal" Text="รวมมูลค่าหลักค้ำประกัน(บาท)">
                                                                </dx:ASPxLabel>
                                                            </FooterTemplate>
                                                            <FooterCellStyle HorizontalAlign="Center">
                                                            </FooterCellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NAMOUNT" Caption="มูลค่าหลักค้ำประกัน(บาท)"
                                                            VisibleIndex="3" Width="9%">
                                                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Right" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <table border="0" cellspacing="1" cellpadding="1">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnEditGuarantee" ClientInstanceName="btnEditGuarantee" runat="server"
                                                                                ToolTip="ปรับปรุง/แก้ไขรายการ" CausesValidation="False" AutoPostBack="false"
                                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                                CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('STARTEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/26466_Settings.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnDelGuarantee" ClientInstanceName="btnDelGuarantee" runat="server"
                                                                                ToolTip="ลบรายการ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                                Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('DELGUARANTEE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NCGID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SBOOKNO" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="GUARANTEESTYPE_ID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SGUARANTEETYPE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NAMOUNT" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" ShowInCustomizationForm="false" />
                                                    </Columns>
                                                    <TotalSummary>
                                                        <dx:ASPxSummaryItem ShowInColumn="NAMOUNT" SummaryType="Sum" FieldName="NAMOUNT"
                                                            DisplayFormat="N2" />
                                                    </TotalSummary>
                                                    <Templates>
                                                        <EditForm>
                                                            <div style="text-align: center;">
                                                                <table width="90%">
                                                                    <tr>
                                                                        <td style="width: 15%;" align="right">
                                                                            ประเภท :
                                                                        </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxComboBox ID="cmbSGUARANTEETYPE" ClientInstanceName="cmbSGUARANTEETYPE" runat="server"
                                                                                DataSourceID="sdsGUARANTEETYPE" TextField="GUARANTEESTYPE_NAME" ValueField="GUARANTEESTYPE_ID">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                    ValidationGroup="addGuarantee">
                                                                                    <ErrorFrameStyle ForeColor="Red" />
                                                                                    <RequiredField ErrorText="กรุณาระบุประเภท" IsRequired="True" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxComboBox>
                                                                            <asp:SqlDataSource ID="sdsGUARANTEETYPE" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                                CancelSelectOnNullParameter="False" CacheKeyDependency="ckdGUARANTEETYPE" SelectCommand="SELECT * FROM TGUARANTEESTYPE WHERE NVL( CACTIVE,'1')='1' ">
                                                                            </asp:SqlDataSource>
                                                                            <dx:ASPxTextBox ID="txtSGUARANTEETYPE" runat="server" ClientInstanceName="txtSGUARANTEETYPE"
                                                                                ClientVisible="false">
                                                                            </dx:ASPxTextBox>
                                                                            <dx:ASPxTextBox ID="txtNCGID" runat="server" ClientInstanceName="txtNCGID" ClientVisible="false">
                                                                            </dx:ASPxTextBox>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%;" align="right">
                                                                            เลขที่เอกสาร :
                                                                        </td>
                                                                        <td style="width: 25%" align="left">
                                                                            <dx:ASPxTextBox ID="txtSBOOKNO" runat="server" ClientInstanceName="txtSBOOKNO" Width="250px">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                    ValidationGroup="addGuarantee">
                                                                                    <ErrorFrameStyle ForeColor="Red" />
                                                                                    <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td style="width: 20%; text-align: right;">
                                                                            มูลค่าหลักค้ำประกัน(บาท) :
                                                                        </td>
                                                                        <td style="width: 20%" align="left">
                                                                            <dx:ASPxTextBox ID="txtGuaranteeValue" runat="server" ClientInstanceName="txtGuaranteeValue"
                                                                                Width="150px" DisplayFormatString="#,##0.00" NullText="0.00">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                    ValidationGroup="addGuarantee">
                                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField ErrorText="กรุณาระบุ" IsRequired="true" />
                                                                                    <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; text-align: right; vertical-align: top;">
                                                                            ธนาคาร :
                                                                        </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxComboBox ID="cmbRemark" runat="Server" ClientInstanceName="cmbRemark" DataSourceID="sdsBank"
                                                                                TextField="BANKNAME" ValueField="BANKNAME">
                                                                            </dx:ASPxComboBox>
                                                                            <asp:SqlDataSource ID="sdsBank" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                                CancelSelectOnNullParameter="False" CacheKeyDependency="ckdBank" SelectCommand="SELECT * FROM TBANK WHERE NVL( CACTIVE,'1')='1' ">
                                                                            </asp:SqlDataSource>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnSaveGuarantee" ClientInstanceName="btnSaveGuarantee" runat="server"
                                                                                CausesValidation="true" ValidationGroup="addGuarantee" SkinID="_confirm" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(ASPxClientEdit.ValidateGroup('addGuarantee')){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('SAVEGUARANTEE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }else{ return false;} }" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxButton ID="btnCancelSave" ClientInstanceName="btnCancelSave" runat="server"
                                                                                SkinID="_cancel" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('CANCELEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </EditForm>
                                                    </Templates>
                                                    <Settings ShowFooter="true" ShowStatusBar="Auto" />
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <SettingsLoadingPanel Mode="ShowAsPopup" Text="Please Wait..." />
                                                    <SettingsEditing Mode="EditFormAndDisplayRow" />
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="sdsGUARANTEES" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdGUARANTEES" SelectCommand="SELECT NCGID, NCONTRACTID, SBOOKNO, SGUARANTEETYPE, NAMOUNT, DCREATE, SCREATE, DUPDATE, SUPDATE,SREMARK,GUARANTEESTYPE_ID ,'0' CNEW ,'0' CCHANGE,'0' CDEL FROM TCONTRACT_GUARANTEES WHERE NCONTRACTID = NVL( :CONTRACT_ID , NCONTRACTID)">
                                                    <SelectParameters>
                                                        <asp:SessionParameter SessionField="SCONTRACTID" Name="CONTRACT_ID" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table id="tbladdgrt" runat="Server">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnNewGuarantee" ClientInstanceName="btnNewGuarantee" runat="server"
                                                                ToolTip="เพิ่มหลักประกันอื่นๆ" CausesValidation="False" AutoPostBack="false"
                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('NEWGUARANTEE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle">
                                                            เพิ่มหลักประกันอื่นๆ
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnplant" ClientInstanceName="rpn" runat="server" Width="980px"
                        HeaderText="ขอบเขตการขนส่ง">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent2" runat="server">
                                <table width="100%">
                                    <tbody id="Tbody3" runat="server">
                                        <tr>
                                            <td style="width: 15%;">
                                                คลังต้นทางหลัก :
                                            </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cboMainPlant" ClientInstanceName="cboMainPlant" runat="server"
                                                    CallbackPageSize="30" EnableCallbackMode="True" OnItemRequestedByValue="cboMainPlant_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboMainPlant_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{1}" ValueField="STERMINALID" Width="400px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s,e){ gvwTerminal.PerformCallback('CHECKPLANT;'); }" />
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="STERMINALNAME" Width="350px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <span style="display: none;">
                                                    <dx:ASPxTextBox ID="txtMainPlant" ClientInstanceName="txtMainPlant" runat="server"
                                                        SkinID="txtValidate">
                                                    </dx:ASPxTextBox>
                                                </span>
                                            </td>
                                            <td>
                                                <table style="display: none;">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnMainPlant" ClientInstanceName="btnMainPlant" runat="server"
                                                                ToolTip="เพิ่มคลังต้นทางหลัก" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                Width="25px">
                                                                <ClientSideEvents Click="function(s,e){ if(!gvwTerminal.InCallback()) gvwTerminal.PerformCallback('MAINPLANT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle">
                                                            เพิ่มคลังต้นทางหลัก
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:SqlDataSource ID="sdsMainPlant" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdMainPlant" SelectCommand="SELECT * FROM TTERMINAL WHERE NVL(CACTIVE,'1')='1'">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                คลังต้นทางอื่นๆ :
                                            </td>
                                            <td colspan="1">
                                                <dx:ASPxComboBox ID="cboPLANTOTHER" ClientInstanceName="cboPLANTOTHER" CallbackPageSize="30"
                                                    runat="server" EnableCallbackMode="True" OnItemRequestedByValue="cboPLANTOTHER_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboPLANTOTHER_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{1}" ValueField="STERMINALID" Width="400px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="STERMINALNAME" Width="350px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="plant" ErrorDisplayMode="ImageWithTooltip"
                                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td>
                                                <table id="tbladdplant" runat="Server">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnNewTerminal" ClientInstanceName="btnNewTerminal" runat="server"
                                                                ToolTip="คลังต้นทางอื่นๆ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('plant')) return false; else{ if(!gvwTerminal.InCallback()) gvwTerminal.PerformCallback('NEWCONTRACTPLANT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); } }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle">
                                                            เพิ่มคลังต้นทาง
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:SqlDataSource ID="sdsOtherPlant" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdOtherPlant" SelectCommand="SELECT * FROM TTERMINAL WHERE NVL(CACTIVE,'1')='1'">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="">
                                                <dx:ASPxGridView ID="gvwTerminal" ClientInstanceName="gvwTerminal" runat="server"
                                                    KeyFieldName="KEYID" SkinID="_gvw" EnableTheming="False" Border-BorderStyle="None">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; }" />
                                                    <Columns>
                                                        <dx:GridViewDataComboBoxColumn Width="10%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblSMAIN_PLANT" ClientInstanceName="lblSMAIN_PLANT" runat="server"
                                                                    Text='<%# ""+ Eval("CMAIN_PLANT")=="1"?"คลังต้นทางหลัก:":"คลังต้นทางอื่นๆ:" %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn FieldName="STERMINALNAME" Width="68%">
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnDelContractPlant" ClientInstanceName="btnDelContractPlant"
                                                                    runat="server" ToolTip="ลบรายการ" CausesValidation="False" AutoPostBack="false"
                                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                    CssClass="dxeLineBreakFix" Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwTerminal.InCallback()) gvwTerminal.PerformCallback('DEL_CONTRACT_PLANT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="STERMINALID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CMAIN_PLANT" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CONT_PLNT" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="KEYID" Visible="false" />
                                                    </Columns>
                                                    <Border BorderStyle="None" />
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <SettingsEditing Mode="EditFormAndDisplayRow" />
                                                    <Settings GridLines="None" ShowColumnHeaders="false" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <span style="display: none;">
                                                    <dx:ASPxTextBox ID="txtSCONT_PLNT" ClientInstanceName="txtSCONT_PLNT" runat="server">
                                                    </dx:ASPxTextBox>
                                                </span>
                                                <asp:SqlDataSource ID="sdsPlant" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdPlant" SelectCommand="SELECT ROW_NUMBER()OVER(ORDER BY CONT_PLNT.STERMINALID) AS KEYID 
                                        ,CONT_PLNT.SCONTRACTID NCONTRACTID,CONT_PLNT.STERMINALID
                                        ,CONT_PLNT.STERMINALID||' :  '||PLNT.SABBREVIATION STERMINALNAME,CONT_PLNT.STERMINALID CONT_PLNT ,NVL(CONT_PLNT.CMAIN_PLANT,'0') CMAIN_PLANT
                                        ,CONT_PLNT.CACTIVE ,'0' CNEW ,'0' CCHANGE,'0' CDEL 
                                        FROM TCONTRACT_PLANT CONT_PLNT 
                                        LEFT JOIN TTERMINAL PLNT ON CONT_PLNT.STERMINALID =PLNT.STERMINALID 
                                        WHERE SCONTRACTID LIKE  NVL( :CONTRACT_ID , SCONTRACTID) ">
                                                    <SelectParameters>
                                                        <asp:SessionParameter SessionField="SCONTRACTID" Name="CONTRACT_ID" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnGProduct" ClientInstanceName="rpn" runat="server" Width="980px"
                        HeaderText="กลุ่มผลิตภัณฑ์">
                        <PanelCollection>
                            <dx:PanelContent ID="pctGProduct" runat="server">
                                <table width="100%">
                                    <tbody id="tblGProduct" runat="server">
                                        <tr>
                                            <td style="">
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <dx:ASPxGridView ID="gvwGProduct" runat="server" ClientInstanceName="gvwGProduct"
                                                    AutoGenerateColumns="False" EnableTheming="False" Width="60%" KeyFieldName="GID"
                                                    SkinID="_gvw">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; } " />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="7%" ReadOnly="false" ShowInCustomizationForm="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SPRODUCTTYPEID" Caption="รหัสกลุ่มผลิตภัณฑ์"
                                                            Width="18%" VisibleIndex="1">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SPRODUCTTYPENAME" Caption="ชื่อกลุ่มผลิตภัณฑ์"
                                                            ShowInCustomizationForm="false" VisibleIndex="2">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="10%">
                                                            <DataItemTemplate>
                                                                <table border="0" cellspacing="1" cellpadding="1">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnEditProduct" ClientInstanceName="btnEditProduct" runat="server"
                                                                                ToolTip="ปรับปรุง/แก้ไขรายการ" CausesValidation="False" AutoPostBack="false"
                                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                                CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('STARTEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/26466_Settings.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnDelProduct" ClientInstanceName="btnDelProduct" runat="server"
                                                                                ToolTip="ลบรายการ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                                Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('DELGPRODUCT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="GID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CACTIVE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" ShowInCustomizationForm="false" />
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            <div style="text-align: center;">
                                                                <table width="90%">
                                                                    <tr>
                                                                        <td style="width: 30%;" align="right">
                                                                            เลือกกลุ่มผลิตภัณฑ์ :
                                                                        </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxComboBox ID="cmbProductType" ClientInstanceName="cmbProductType" runat="server"
                                                                                DataSourceID="sdsProductType" TextField="SPRODUCTTYPENAME" ValueField="SPRODUCTTYPEID">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                    ValidationGroup="addProduct">
                                                                                    <ErrorFrameStyle ForeColor="Red" />
                                                                                    <RequiredField ErrorText="กรุณาระบุกลุ่มผลิตภัณฑ์" IsRequired="True" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxComboBox>
                                                                            <asp:SqlDataSource ID="sdsProductType" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                                CancelSelectOnNullParameter="False" CacheKeyDependency="ckdProductType" SelectCommand="SELECT SPRODUCTTYPEID,SPRODUCTTYPENAME FROM TPRODUCTTYPE">
                                                                            </asp:SqlDataSource>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxButton ID="btnSaveProduct" ClientInstanceName="btnSaveProduct" runat="server"
                                                                                CausesValidation="true" ValidationGroup="addProduct" SkinID="_confirm" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(ASPxClientEdit.ValidateGroup('addProduct')){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('SAVEGPRODUCT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }else{ return false; } }" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxButton ID="btnCancelSave" ClientInstanceName="btnCancelSave" runat="server"
                                                                                SkinID="_cancel" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('CANCELEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </EditForm>
                                                    </Templates>
                                                    <Settings ShowStatusBar="Auto" />
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <SettingsLoadingPanel Mode="ShowAsPopup" Text="Please Wait..." />
                                                    <SettingsEditing Mode="EditFormAndDisplayRow" />
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="sdsGPRODUCT" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdGPRODUCT" SelectCommand="SELECT ROWNUM-1 GID,SCONTRACTID AS NCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,CACTIVE,'0' CNEW ,'0' CCHANGE,'0' CDEL FROM TCONTRACT_GPRODUCT WHERE SCONTRACTID=NVL(:CONTRACT_ID,SCONTRACTID)">
                                                    <SelectParameters>
                                                        <asp:SessionParameter SessionField="SCONTRACTID" Name="CONTRACT_ID" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <table id="tbladdGProduct" runat="Server" width="60%">
                                                    <tr>
                                                        <td align="left" width="28px">
                                                            <dx:ASPxButton ID="btnNewGProduct" ClientInstanceName="btnNewGProduct" runat="server"
                                                                ToolTip="เพิ่มกลุ่มผลิตภัณฑ์" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                Width="25px">
                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('NEWGPRODUCT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle" align="left">
                                                            เพิ่มกลุ่มผลิตภัณฑ์อื่นๆ
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnCTruck" ClientInstanceName="rpn" runat="server" Width="980px"
                        HeaderText="รายชื่อรถในสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent3" runat="server">
                                <table width="100%">
                                    <tbody id="Tbody4" runat="server">
                                        <tr>
                                            <td colspan="2">
                                                <span style="display: none;">
                                                    <dx:ASPxTextBox ID="TRUCK_KEYID" ClientInstanceName="TRUCK_KEYID" runat="server"
                                                        Width="50px">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="VEH_TYPE" ClientInstanceName="VEH_TYPE" runat="server" Width="150px">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="TU_TYPE" ClientInstanceName="TU_TYPE" runat="server" Width="150px">
                                                    </dx:ASPxTextBox>
                                                </span>
                                            </td>
                                            <td align="right" style="padding-right: 10px;">
                                                <dx:ASPxButton ID="btnTruckManagment" ClientInstanceName="btnTruckManagment" runat="server"
                                                    Text="จัดการธุรกรรมการใช้งานรถ" AutoPostBack="false" ClientVisible="false">
                                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('TManagement;'); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ทะบียน(หัว):
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cboVEH" ClientInstanceName="cboVEH" CallbackPageSize="30" runat="server"
                                                                EnableCallbackMode="True" OnItemRequestedByValue="cboVEH_OnItemRequestedByValueSQL"
                                                                OnItemsRequestedByFilterCondition="cboVEH_OnItemsRequestedByFilterConditionSQL"
                                                                SkinID="xcbbATC" TextFormatString="{1}" ValueField="STRUCKID" Width="250px">
                                                                <ClientSideEvents SelectedIndexChanged="function(s,e){ if(s.GetSelectedIndex() + '' != '-1'){ VEH_TYPE.SetText(s.GetItem(s.GetSelectedIndex()).GetColumnText('VEH_TYPE')); if(!cboTU.InCallback()) cboTU.PerformCallback(s.GetValue());} }"
                                                                    ValueChanged="function(s,e){ if(!cboTU.InCallback()) cboTU.PerformCallback(s.GetValue()); }" />
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัส" FieldName="STRUCKID" Width="80px" />
                                                                    <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" Width="100px" />
                                                                    <dx:ListBoxColumn Caption="ทะเบียนหาง" FieldName="TU_NO" Width="100px" />
                                                                    <dx:ListBoxColumn Caption="ประเภท" FieldName="VEH_TYPE" Width="70px" />
                                                                </Columns>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            ทะบียน(หาง):
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cboTU" runat="server" ClientInstanceName="cboTU" Width="250px"
                                                                EnableCallbackMode="True" TextField="TU_NO" ValueField="STRAILERID" DataSourceID="sdsTU"
                                                                OnCallback="cboTU_Callback" DropDownButton-Enabled="false" SelectedIndex="0">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxCheckBox ID="ckbIsStandBy" ClientInstanceName="ckbIsStandBy" runat="server"
                                                                Text="รถสำรอง">
                                                            </dx:ASPxCheckBox>
                                                        </td>
                                                        <td>
                                                            <table id="tblctruck" runat="Server">
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton ID="ASPxButton1" ClientInstanceName="btnNewTerminal" runat="server"
                                                                            ToolTip="เพิ่มรถในสัญญา" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                            EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                            Width="25px">
                                                                            <ClientSideEvents Click="function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('ADD_CONTRACT_TRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                            <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td valign="middle">
                                                                        เพิ่มรถในสัญญา
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <dx:ASPxGridView ID="gvwTruck" ClientInstanceName="gvwTruck" runat="server" KeyFieldName="STRUCKID"
                                                    SkinID="_gvw" EnableTheming="False" Width="80%">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; VEH_TYPE.SetText(''); TU_TYPE.SetText(''); TRUCK_KEYID.SetText(''); cboVEH.SetValue(''); cboTU.SetValue(''); } " />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="ทะเบียน(หัว)" FieldName="VEH_NO" Width="10%">
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="ทะเบียน(หาง)" FieldName="TU_NO" Width="10%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblTU_No" runat="Server" Text='<%# ""+Eval("TU_NO") %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Width="6%" Caption="ชนิด" FieldName="CSTANDBY">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblSTANDBY" ClientInstanceName="lblSTANDBY" runat="server" Text='<%# ""+Eval("CSTANDBY")=="Y"?"รถสำรอง":"รถในสัญญา" %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Width="10%" Caption="ประเภท">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblTruck_Type" ClientInstanceName="lblTruck_Type" runat="server"
                                                                    Text='<%# ""+Eval("Truck_Type") %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataTextColumn Caption="การจัดการ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnViewTruck" ClientInstanceName="btnViewTruck" runat="server"
                                                                    ToolTip="ดูข้อมูลรถ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                    EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('VIEWTRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การจัดการ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <%--<dx:ASPxButton ID="btnEditTruck" ClientInstanceName="btnEditTruck" runat="server"
                                                        ToolTip="แก้ไขรายการ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                        EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function (s, e) { gvwTruck.PerformCallback('EDIT_CONTRACT_TRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                        <Image Width="25px" Height="25px" Url="Images/26466_Settings.png" />
                                                    </dx:ASPxButton>--%>
                                                                <dx:ASPxButton ID="btnViewTruck" ClientInstanceName="btnViewTruck" runat="server"
                                                                    ToolTip="ดูข้อมูลรถ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                                    EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                                    Width="25px" ClientVisible="false">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('VIEWTRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                </dx:ASPxButton>
                                                                <dx:ASPxButton ID="btnDelTruck" ClientInstanceName="btnDelTruck" runat="server" ToolTip="ลบรายการ"
                                                                    CausesValidation="False" AutoPostBack="False" EnableTheming="False" EnableDefaultAppearance="False"
                                                                    SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="STRAILERID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CSTANDBY" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CREJECT" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DSTART" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DEND" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="KEYID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="MOVED" Visible="false" />
                                                    </Columns>
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <Settings ShowColumnHeaders="true" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:SqlDataSource ID="sdsVEH" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdVEH" SelectCommand="SELECT VEH.STRUCKID ,VEH.SHEADREGISTERNO VEH_NO ,VEH.STRAILERID , TU.SHEADREGISTERNO TU_NO
,VEH.SCARTYPEID 
,/*CASE WHEN VEH.SCARTYPEID='0' THEN 'สิบล้อ' ELSE 'Semi-Trailer' END*/VEH_TYPE.SCARTYPENAME  VEH_TYPE
FROM TTRUCK VEH  
LEFT JOIN TTRUCK TU ON VEH.STRAILERID = TU.STRUCKID
LEFT JOIN TTRUCKTYPE VEH_TYPE ON VEH.SCARTYPEID =VEH_TYPE.SCARTYPEID

 WHERE VEH.SCARTYPEID in('0','3')   "></asp:SqlDataSource>
                                                <asp:SqlDataSource ID="sdsTU" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdTU" SelectCommand="SELECT STRUCKID As STRAILERID,SHEADREGISTERNO As TU_NO FROM TTRUCK WHERE SCARTYPEID='4' AND SHEADID=:VEH_ID">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="cboVEH" Name=":VEH_ID" PropertyName="Value" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                                <asp:SqlDataSource ID="sdsContractTruck" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdContractTruck" SelectCommand="
SELECT ROW_NUMBER()OVER(ORDER BY CONT_TRCK.STRUCKID) AS KEYID
,CASE WHEN REF_SCONTRACTID IS NULL THEN CONT_TRCK.SCONTRACTID||'' ELSE :CONTRACT_ID END NCONTRACTID,CONT_TRCK.STRUCKID , VEH.SHEADREGISTERNO VEH_NO
,CONT_TRCK.STRAILERID , TU.SHEADREGISTERNO TU_NO
,CONT_TRCK.CSTANDBY,CONT_TRCK.CREJECT,CONT_TRCK.DSTART,CONT_TRCK.DEND
,CONT_TRCK.DCREATE,CONT_TRCK.SCREATE,CONT_TRCK.DUPDATE,CONT_TRCK.SUPDATE ,VEH.SCARTYPEID, NVL(VEH.NTOTALCAPACITY,0) NTOTALCAPACITY
, CASE WHEN  VEH.SCARTYPEID=0 AND NVL(VEH.NTOTALCAPACITY,0) &lt;= 10000 THEN '6Wheel'
ELSE
     CASE WHEN  VEH.SCARTYPEID=3 THEN 'Semi-Trailer' ELSE '10Wheel' END
 END TRUCK_TYPE,CASE WHEN REF_SCONTRACTID IS NULL THEN '0' ELSE '1' END MOVED,'0' CNEW ,'0' CCHANGE,'0' CDEL 
FROM TCONTRACT_TRUCK CONT_TRCK
LEFT JOIN TTRUCK VEH ON CONT_TRCK.STRUCKID = VEH.STRUCKID
LEFT JOIN TTRUCK TU ON CONT_TRCK.STRAILERID = TU.STRUCKID
WHERE (CONT_TRCK.SCONTRACTID = :CONTRACT_ID AND CONT_TRCK.REF_SCONTRACTID IS NULL) OR CONT_TRCK.REF_SCONTRACTID = :CONTRACT_ID ORDER BY TRUCK_TYPE ">
                                                    <SelectParameters>
                                                        <asp:SessionParameter SessionField="SCONTRACTID" Name="CONTRACT_ID" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                       <tr>
                                                <td align="right" >
                                                    หมายเหตุ :
                                                </td>
                                                <td colspan="2">
                                                    <dx:ASPxMemo runat="server" ID="txtDescription" Width="350px" Height="100px" CssClass="dxeLineBreakFix"
                                                        ClientInstanceName="txtDescription">
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="noapp" ErrorDisplayMode="ImageWithTooltip"
                                                            SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาระบุสาเหตุที่ปฏิเสธ" IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <dx:ASPxButton ID="btnSubmit" ClientInstanceName="btnSubmit" runat="server" Text="บันทึกชั่วคราว"
                                                    CssClass="dxeLineBreakFix" CausesValidation="true" AutoPostBack="false">
                                                    <ClientSideEvents Click="function(s,e){ txtDescription.SetIsValid(true); if(ASPxClientEdit.ValidateGroup('search') && ASPxClientEdit.ValidateGroup('addGuarantee')){ var msg=''; if(window['cboModify_type'] != undefined){ if(cboModify_type.GetValue()=='0') msg='ท่านต้องการบันทึกเพื่อแก้ไข/เพิ่มเติมสัญญานี้ ใช่หรือไม่ ?'; else msg='ท่านต้องการบันทึกเพื่อต่ออายุสัญญานี้ ใช่หรือไม่ ?'; }else{ msg='ท่านต้องการบันทึกเพื่อเพิ่มสัญญานี้ ใช่หรือไม่ ?'; } dxConfirm('แจ้งเตือน', msg ,function(s,e){ dxPopupConfirm.Hide(); if(!xcpn.InCallback()) xcpn.PerformCallback('SAVE;'); } ,function(s,e){ dxPopupConfirm.Hide(); }); } else{ return false; } }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnEdit" runat="server" Text="ขอข้อมูลเพิ่มเติม" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) { if(ASPxClientEdit.ValidateGroup('noapp')){ dxConfirm('แจ้งเตือน', 'คุณต้องการปฏิเสธการเปลี่ยนแปลงข้อมูลใช่หรือไม่?' ,function(s,e){ dxPopupConfirm.Hide(); if(!xcpn.InCallback()) xcpn.PerformCallback('Edit;'); } ,function(s,e){ dxPopupConfirm.Hide(); });}else{  dxError('พบข้อผิดพลาด','ระบุหมายเหตุ');   }  }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                                 <dx:ASPxButton ID="btnApprove" runat="server" Text="บันทึก และอนุมัติ" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false">
                                            <ClientSideEvents Click="function(s,e){txtDescription.SetIsValid(true); if(ASPxClientEdit.ValidateGroup('add')){
dxConfirm('แจ้งเตือน','ท่านต้องการบันทึกเพื่อเปลี่ยนแปลงข้อมูลนี้ ใช่หรือไม่ ?',function(s,e){ dxPopupConfirm.Hide(); if(!xcpn.InCallback())   xcpn.PerformCallback('approve'); } ,function(s,e){ dxPopupConfirm.Hide();});
}  }" />
                                        </dx:ASPxButton>

                                        <dx:ASPxButton ID="btnNoApprove" runat="server" Text="ปฏิเสธ" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) { if(ASPxClientEdit.ValidateGroup('noapp')){ dxConfirm('แจ้งเตือน', 'คุณต้องการปฏิเสธการเปลี่ยนแปลงข้อมูลใช่หรือไม่?' ,function(s,e){ dxPopupConfirm.Hide(); if(!xcpn.InCallback()) xcpn.PerformCallback('noapprove;'); } ,function(s,e){ dxPopupConfirm.Hide(); });}else{  dxError('พบข้อผิดพลาด','ระบุหมายเหตุ');   }  }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnCancel" ClientInstanceName="btnCancel" runat="server" SkinID="_back"
                                                    CssClass="dxeLineBreakFix" AutoPostBack="false">
                                                     <ClientSideEvents Click="function(s,e){ window.location = 'approve_pk.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <div>
        <table>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
