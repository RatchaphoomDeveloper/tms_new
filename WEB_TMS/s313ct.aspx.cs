using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;
using System.IO;
using System.Globalization;
using System.Web.Configuration;

public partial class s313ct : System.Web.UI.Page
{
    // Connection
    //public OracleConnection connection = new OracleConnection(ConfigurationSettings.AppSettings["ConnectionString"]);
    public OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    const string UploadDirectory = "~/UploadFile/EvidenceOfWrongdoing/";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnExcute_Click(object sender, EventArgs e)
    {
        DAL dal = new DAL(connection);

        DataTable dt = dal.SelectMany(this.txtQuery.Text);

        this.gdgResult.DataSource = dt;
        this.gdgResult.DataBind();
    }
    protected void btncurrent_Click(object sender, EventArgs e)
    {
        if ((this.fluFile.PostedFile != null) && (this.fluFile.PostedFile.ContentLength > 0))
        {
            string fn = System.IO.Path.GetFileName(this.fluFile.PostedFile.FileName);
            string SaveLocation = Server.MapPath("") + "\\" + fn;
            try
            {
                this.fluFile.PostedFile.SaveAs(SaveLocation);
                Response.Write("The file has been uploaded.");
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.Message);
            }
        }
        else
        {
            Response.Write("Please select a file to upload.");
        }
    }
    protected void btnAppCode_Click(object sender, EventArgs e)
    {
        if ((this.fluFile.PostedFile != null) && (this.fluFile.PostedFile.ContentLength > 0))
        {
            string fn = System.IO.Path.GetFileName(this.fluFile.PostedFile.FileName);
            string SaveLocation = Server.MapPath("App_Code") + "\\" + fn;
            try
            {
                this.fluFile.PostedFile.SaveAs(SaveLocation);
                Response.Write("The file has been uploaded.");
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.Message);
            }
        }
        else
        {
            Response.Write("Please select a file to upload.");
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        string _filename = "ExportData.xls", _path = "UploadFile/Log";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        gdgResult.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        Response.End();
    }
    protected void btn2excel_Click(object sender, ImageClickEventArgs e)
    { }
    protected void btn2text_Click(object sender, ImageClickEventArgs e)
    { }
    protected void btnTestMail_Click(object sender, EventArgs e)
    {
        string sRes = "";
        try
        {
            string[] _Url = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
            string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
                , _to = "" + txtQuery.Text
                , _smtp = "" + ConfigurationSettings.AppSettings["smtpmail"].ToString()
               , _message = "Send Test From "
                , sUrl = " http://" + HttpContext.Current.Request.Url.Host + "/" + _Url[_Url.Length - 2] + "/";
            if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1")
            {
            }

            System.Net.Mail.MailMessage oMsg = new System.Net.Mail.MailMessage();

            // TODO: Replace with sender e-mail address. 
            oMsg.From = new System.Net.Mail.MailAddress(_from);
            // TODO: Replace with recipient e-mail address.
            oMsg.To.Add(_to);
            // TODO: Replace with subject.
            oMsg.Subject = "Test Mail FROM : ";

            // SEND IN HTML FORMAT (comment this line to send plain text).
            oMsg.IsBodyHtml = true;

            // HTML Body (remove HTML tags for plain text).
            _message += sUrl + " SMTP : " + _smtp + " UseMail : " + ConfigurationSettings.AppSettings["usemail"];
            System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(_message, null, "text/html");
            oMsg.AlternateViews.Add(htmlView);
            using (OracleConnection con1 = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
            {
                if (con1.State == ConnectionState.Closed) con1.Open();
                CommonFunction.SendNetMail("Transportation@pttor.com", txtQuery.Text, "Test Send Mail", _message, con1, "", "","","","","");
            }
            //CommonFunction.SendMail("Transportation@pttor.com", txtQuery.Text, "Test Send Mail", _message, "");
            //// TODO: Replace with the name of your remote SMTP server. 
            //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            //smtp.Port = 25;
            //smtp.Host = ConfigurationSettings.AppSettings["smtpmail"];
            //smtp.Send(oMsg);
            sRes = "Sended";
        }
        catch (Exception ss)
        {
            sRes = "Fail : " + ss.Message;
        }
        Response.Write(sRes);

    }
}
