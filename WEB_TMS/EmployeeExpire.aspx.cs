﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;
using System.Web.UI.HtmlControls;
using GemBox.Spreadsheet;
using System.IO;
using System.Collections.Generic;
public partial class EmployeeExpire : PageBase
{
    DataSet ds;
    //Boolean checkIsnotEmtry = true;
    #region + View State +
    private DataTable dtDataTab1
    {
        get
        {
            if ((DataTable)ViewState["dtDataTab1"] != null)
                return (DataTable)ViewState["dtDataTab1"];
            else
                return null;
        }
        set
        {
            ViewState["dtDataTab1"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }

    private DataTable dtCGroup
    {
        get
        {
            if ((DataTable)ViewState["dtCGroup"] != null)
                return (DataTable)ViewState["dtCGroup"];
            else
                return null;
        }
        set
        {
            ViewState["dtCGroup"] = value;
        }
    }

    private string SEMPLOYEEID
    {
        get
        {
            if ((string)ViewState["SEMPLOYEEID"] != null)
                return (string)ViewState["SEMPLOYEEID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SEMPLOYEEID"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            this.AssignAuthen();

            string str = string.Empty;
            string strQuery;
            str = Request.QueryString["str"];
            if (!string.IsNullOrEmpty(str))
            {
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SEMPLOYEEID = strQuery;
                if (txtSearch.Text.Trim() == "") { txtSearch.Text = SEMPLOYEEID; }
                this.Search(dgvData, dtDataTab1, this.GetConditionSearchTab1());
            }
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdSearchTab1.Enabled = false;
                cmdRefresh.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdAdd.Enabled = false;
                //dgvData.Columns[10].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadStatus();
            this.LoadDocType();
            this.LoadVendor();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadStatus()
    {
        try
        {
            DataTable dtStatus = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'USER_STATUS'");
            // DropDownListHelper.BindDropDownList(ref ddlStatusTab1, dtStatus, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadDocType()
    {
        try
        {
            DataTable dtDocType = DocTypeBLL.Instance.DocTypeSelectBLL("  AND UPLOAD_TYPE = 'VENDOR' AND UPLOAD_TYPE_IND = '1'");
            DropDownListHelper.BindDropDownList(ref ddlDocType, dtDocType, "UPLOAD_ID", "UPLOAD_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadVendor()
    {
        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        cboVendor.DataSource = ViewState["DataVendor"];
        cboVendor.DataBind();
        if (Session["CGROUP"] + string.Empty == "0")
        {
            cboVendor.Enabled = false;
            cboVendor.Value = Session["SVDID"] + string.Empty;
        }
    }

    protected void cmdSearchTab1_Click(object sender, EventArgs e)
    {
        if (this.checkConfitionBeforeSearch())
        {
            this.Search(dgvData, dtDataTab1, this.GetConditionSearchTab1());
            this.LoadVendor();
            if (Session["CGROUP"] + string.Empty == "0")
            {
                dgvData.Columns[2].Visible = false;
            }
        }
    }

    private void Search(GridView dgv, DataTable dtData, string Condition)
    {
        try
        {
            dtData = UserBLL.Instance.UserSelectBLLWithDocDate(Condition, this.GetField(""), this.GetJoin(""), this.GetConditionSearchRownum());
            //GridViewHelper.BindGridView(ref dgv, dtData, true);               

            DataTable dtOnlyDocdate = UserBLL.Instance.UserSelectBLLOnlyDocDate();
            for (int j = 0; j < dtOnlyDocdate.Rows.Count; j++)
            {
                string getSettingDocID = dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString();
                string getSettingDocName = dtOnlyDocdate.Rows[j]["UPLOAD_NAME"].ToString();
                for (int i = 0; i < dtData.Columns.Count; i++)
                {
                    string getColumnName = dtData.Columns[i].ColumnName; // Rows[0][i].ToString();
                    getColumnName = getColumnName.Replace("DOCNUM", "");

                    if (getColumnName == getSettingDocID)
                    {

                        getSettingDocName = getSettingDocName.Replace("เอกสารสำเนาบัตรประชาชน", "สำเนาบัตรประชาชน");
                        getSettingDocName = getSettingDocName.Replace("DDC  (Defensive Driving)", "");
                        getSettingDocName = getSettingDocName.Replace("ใบผ่านการอบรม SHE (6 hr.)", "ใบอบรม SHE");
                        getSettingDocName = getSettingDocName.Replace("ใบอนุญาตผู้ปฏิบัติงาน ถังขนส่งน้ำมันประเภทรถขนส่งน้ำมัน (ธพ.พ.๒ผ)", "ใบอนุญาต (ธพ.พ.๒ผ)");

                        // getSettingDocName = getSettingDocName.Replace("ใบผ่านการอบรม SHE", "ใบอบรม SHE");
                        // getSettingDocName = getSettingDocName.Replace("ใบอนุญาตผู้ปฏิบัติงาน", "ใบอนุญาต (ธพ.พ.๒ผ)");

                        dtData.Columns[i].ColumnName = getSettingDocName;
                    }

                    if (getColumnName.Contains("จำนวนวันที่คงเหลือ"))
                    {
                        //dtData.Columns[i].ColumnName = "";
                    }
                }
            }

            dtData.Columns["SEMPLOYEEID"].ColumnName = "รหัส";
            dtData.Columns["FULLNAME"].ColumnName = "ชื่อ - นามสกุล";
            dtData.Columns["VENDOR_FULLNAME"].ColumnName = "บริษัทผู้ขนส่ง";
            //dtData.Columns["SEMPTPYE"].ColumnName = "ตำแหน่ง";
            dtData.Columns["DBIRTHDATE"].ColumnName = "อายุ พขร.";
            dtData.Columns["CACTIVE"].ColumnName = "สถานะ";
            dtData.Columns["INUSENAME"].ColumnName = "การจ้างงาน";

            dtData.Columns.Remove("STRANS_ID");
            dtData.Columns.Remove("LASTJOB");
            dtData.Columns.Remove("ROW_COLOR");
            dtData.Columns.Remove("SEMPTPYE");

            if (dtData.Rows.Count == 0)
            {
                //checkIsnotEmtry = false;
            }

            dgvData.DataSource = dtData;
            dgvData.DataBind();


        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private Boolean checkConfitionBeforeSearch()
    {
        if ((!string.Equals(txtDateStart.Text, "") || !string.Equals(txtDateEnd.Text, "")) && !string.Equals(txtDocExpireInDay.Text, ""))
        {   // throw new Exception("");
            alertFail("กรุณาเลือก ช่วงเวลาเอกสารหมดอายุ หรือ หมดอายุในอีก(วัน) อย่างใดอย่างหนึ่ง");
            return false;
        }
        return true;
    }
    private string GetConditionSearchTab1()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (!string.Equals(txtSearch.Text.Trim(), string.Empty))
            {
                sb.Append(" AND (EMPSAP.SEMPLOYEEID ||' '|| EMPSAP.FNAME ||' '|| EMPSAP.LNAME || ' ' || EMPSAP.PERS_CODE LIKE '%" + txtSearch.Text.Trim() + "%' )");
            }

            if (ddlStatusTab1.SelectedIndex > 0)
            {
                sb.Append(" AND EMP.CACTIVE = '" + ddlStatusTab1.SelectedValue + "'");
            }

            if (ddlDocType.SelectedIndex > 0)
            {
                sb.Append(" AND FU" + ddlDocType.SelectedValue + ".UPLOAD_ID = '" + ddlDocType.SelectedValue + "'");
            }
            else
            {
                if (!string.Equals(txtDateStart.Text.Trim(), string.Empty) || (!string.Equals(txtDateEnd.Text.Trim(), string.Empty)))
                    sb.Append(" AND ( ");
            }

            DataTable dtOnlyDocdate = UserBLL.Instance.UserSelectBLLOnlyDocDate();

            for (int j = 0; j < dtOnlyDocdate.Rows.Count; j++)
            {
                if (ddlDocType.SelectedIndex == 0)
                {
                    string getDocID = dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString();
                    if (!string.Equals(txtDateStart.Text.Trim(), string.Empty) && (!string.Equals(txtDateEnd.Text.Trim(), string.Empty)))
                    {
                        if (sb.ToString().Contains(".STOP_DATE BETWEEN TO_DATE( '") == false)
                            sb.Append(" (FU" + getDocID + ".STOP_DATE BETWEEN TO_DATE( '" + txtDateStart.Text + "','DD/MM/YYYY') AND TO_DATE( '" + txtDateEnd.Text + "','DD/MM/YYYY'))");
                        else
                            sb.Append(" OR (FU" + getDocID + ".STOP_DATE BETWEEN TO_DATE( '" + txtDateStart.Text + "','DD/MM/YYYY') AND TO_DATE( '" + txtDateEnd.Text + "','DD/MM/YYYY'))");
                    }
                    else
                    {
                        if (!string.Equals(txtDateStart.Text.Trim(), string.Empty))
                        {
                            // sb.Append(" AND CACTIVE = '" + ddlDocType.SelectedValue + "'"); 
                            if (sb.ToString().Contains(".STOP_DATE >= TO_DATE( '") == false)
                                sb.Append(" FU" + getDocID + ".STOP_DATE >= TO_DATE( '" + txtDateStart.Text + "','DD/MM/YYYY')");
                            else
                                sb.Append(" OR FU" + getDocID + ".STOP_DATE >= TO_DATE( '" + txtDateStart.Text + "','DD/MM/YYYY')");
                        }
                        if (!string.Equals(txtDateEnd.Text.Trim(), string.Empty))
                        {
                            if (sb.ToString().Contains(".STOP_DATE <= TO_DATE( '") == false)
                                sb.Append(" FU" + getDocID + ".STOP_DATE <= TO_DATE( '" + txtDateEnd.Text + "','DD/MM/YYYY') ");
                            else
                                sb.Append(" OR FU" + getDocID + ".STOP_DATE <= TO_DATE( '" + txtDateEnd.Text + "','DD/MM/YYYY') ");
                        }
                    }
                }
                else if (string.Equals(dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString(), ddlDocType.SelectedValue))
                {
                    string getDocID = dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString();
                    if (!string.Equals(txtDateStart.Text.Trim(), string.Empty) && (!string.Equals(txtDateEnd.Text.Trim(), string.Empty)))
                    {
                        sb.Append(" AND (FU" + getDocID + ".STOP_DATE BETWEEN TO_DATE( '" + txtDateStart.Text + "','DD/MM/YYYY') AND TO_DATE( '" + txtDateEnd.Text + "','DD/MM/YYYY'))");
                    }
                    else
                    {
                        if (!string.Equals(txtDateStart.Text.Trim(), string.Empty))
                        {
                            // sb.Append(" AND CACTIVE = '" + ddlDocType.SelectedValue + "'"); 
                            sb.Append(" AND FU" + getDocID + ".STOP_DATE >= TO_DATE( '" + txtDateStart.Text + "','DD/MM/YYYY')");
                        }
                        if (!string.Equals(txtDateEnd.Text.Trim(), string.Empty))
                        {
                            sb.Append(" AND FU" + getDocID + ".STOP_DATE <= TO_DATE( '" + txtDateEnd.Text + "','DD/MM/YYYY') ");
                        }
                    }
                }
            }

            if (ddlDocType.SelectedIndex == 0)
            {
                if (!string.Equals(txtDateStart.Text.Trim(), string.Empty) || (!string.Equals(txtDateEnd.Text.Trim(), string.Empty)))
                    sb.Append(" ) ");
            }

            if (!string.IsNullOrEmpty(cboVendor.Text))
            {
                sb.Append(" AND EMP.STRANS_ID = '" + cboVendor.Value + "'");
            }

            if (!string.Equals(txtDocExpireInDay.Text.Trim(), string.Empty))
                sb.Append(" AND ( ");

            for (int j = 0; j < dtOnlyDocdate.Rows.Count; j++)
            {
                string getDocID = dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString();
                if (txtDocExpireInDay.Text.Trim() != "")
                {
                    if (ddlDocType.SelectedIndex == 0)
                    {
                        if (txtDocExpireInDay.Text.Trim() == "0") //ถ้า = 0 ให้ถือว่าเอาที่หมดอายุแล้วทั้งหมดมา
                        {
                            sb.Append(" AND TO_DATE(FU" + getDocID + ".STOP_DATE, 'DD/MM/YYYY') <= TO_DATE(SYSDATE,'DD/MM/YYYY') ");
                        }
                        else
                        {
                            if (sb.ToString().Contains("(TRUNC ((SYSDATE)") == false)
                                sb.Append(" (TRUNC ((SYSDATE) - TO_DATE (FU" + getDocID + ".STOP_DATE, 'DD/MM/YYYY')) BETWEEN -1 * " + txtDocExpireInDay.Text.Trim() + " AND 0 )");
                            else
                                sb.Append(" OR (TRUNC ((SYSDATE) - TO_DATE (FU" + getDocID + ".STOP_DATE, 'DD/MM/YYYY')) BETWEEN -1 * " + txtDocExpireInDay.Text.Trim() + " AND 0 )");
                        }
                    }
                    else if (string.Equals(dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString(), ddlDocType.SelectedValue))
                    {
                        if (txtDocExpireInDay.Text.Trim() == "0") //ถ้า = 0 ให้ถือว่าเอาที่หมดอายุแล้วทั้งหมดมา
                        {
                            sb.Append(" AND TO_DATE(FU" + getDocID + ".STOP_DATE, 'DD/MM/YYYY') <= TO_DATE(SYSDATE,'DD/MM/YYYY') ");
                        }
                        else
                        {
                            if (sb.ToString().Contains("(TRUNC ((SYSDATE)") == false)
                                sb.Append(" (TRUNC ((SYSDATE) - TO_DATE (FU" + getDocID + ".STOP_DATE, 'DD/MM/YYYY')) BETWEEN -1 * " + txtDocExpireInDay.Text.Trim() + " AND 0 )");
                            else
                                sb.Append(" OR (TRUNC ((SYSDATE) - TO_DATE (FU" + getDocID + ".STOP_DATE, 'DD/MM/YYYY')) BETWEEN -1 * " + txtDocExpireInDay.Text.Trim() + " AND 0 )");
                        }
                    }
                }

            }

            if (!string.Equals(txtDocExpireInDay.Text.Trim(), string.Empty))
                sb.Append(" ) ");

            if (ddlInUse.SelectedValue != "")
            {
                sb.Append(" AND EMP.INUSE = '" + ddlInUse.SelectedValue + "'");
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetConditionSearchRownum()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlMaxNumber.SelectedValue != "")
            {
                sb.Append(" AND ROWNUM < '" + ddlMaxNumber.SelectedValue + "'");
            }

            sb.Append(" ORDER BY ROWNUM ASC");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetField(string type)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            DataTable dtOnlyDocdate = UserBLL.Instance.UserSelectBLLOnlyDocDate();

            for (int j = 0; j < dtOnlyDocdate.Rows.Count; j++)
            {
                string getDocID = dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString();
                string getDateFormat = "TO_CHAR(FU" + getDocID + ".STOP_DATE, 'DD-MM-YYYY')";
                if (type == "export")
                {
                    getDateFormat = " TO_DATE(FU" + getDocID + ".STOP_DATE,'DD-MM-YYYY')  ";
                }
                sb.Append("  , TO_CHAR (FU" + getDocID + ".STOP_DATE, 'DD-MM-YYYY') DocNum" + getDocID + ",CASE WHEN FU" + getDocID + ".STOP_DATE IS NULL THEN '' ELSE (CASE WHEN (" +
            "(" +
            "	TRUNC (" +
            "		(" +
            "			MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) - TRUNC (" +
            "				MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE)" +
            "			)" +
            "		) * 30" +
            "	)" +
            ") >= 0" +
            "AND (" +
            "	TRUNC (" +
            "		MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) - (" +
            "			TRUNC (" +
            "				MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) / 12" +
            "			) * 12" +
            "		)" +
            "	)" +
            ") >= 0" +
            "AND (" +
            "	TRUNC (" +
            "		MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) / 12" +
            "	)" +
            ") >= 0" +
            ") THEN 'หมดอายุไปแล้ว ' ELSE '' END) || (" +
            "	- 1 * TRUNC (" +
            "		MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) / 12" +
            "	) || ' ' || 'ปี' || ' ' || ABS(- 1 * TRUNC (" +
            "		MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) - (" +
            "			TRUNC (" +
            "				MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) / 12" +
            "			) * 12" +
            "		)" +
            "	)) || ' ' || 'เดือน' || ' ' || ABS(- 1 * TRUNC (" +
            "		(" +
            "			MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE) - TRUNC (" +
            "				MONTHS_BETWEEN (SYSDATE, FU" + getDocID + ".STOP_DATE)" +
            "			)" +
            "		) * 30" +
            "	)) || ' ' || 'วัน'" +
            ") END AS จำนวนวันที่คงเหลือ" + j.ToString());
            }
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetJoin(string type)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            DataTable dtOnlyDocdate = UserBLL.Instance.UserSelectBLLOnlyDocDate();

            for (int j = 0; j < dtOnlyDocdate.Rows.Count; j++)
            {
                string getDocID = dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString();
                sb.Append(" LEFT JOIN F_UPLOAD FU" + getDocID + " ON FU" + getDocID + ".REF_STR = EMP.SEMPLOYEEID AND FU" + getDocID + ".STOP_DATE IS NOT NULL AND FU" + getDocID + ".UPLOAD_ID = " + getDocID + " ");
            }
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //private string GetDocTypeConditionSearch(string type)
    //{
    //    try
    //    {
    //        StringBuilder sb = new StringBuilder();
    //        DataTable dtOnlyDocdate = UserBLL.Instance.UserSelectBLLOnlyDocDate();
    //        string getDateFormat = "TO_CHAR(FU.STOP_DATE, 'DD-MM-YYYY')";
    //        if (type == "export")
    //        {
    //            getDateFormat = " TO_DATE(FU.STOP_DATE,'DD-MM-YYYY')  ";
    //        }
    //        for (int j = 0; j < dtOnlyDocdate.Rows.Count; j++)
    //        {
    //            string getDocID = dtOnlyDocdate.Rows[j]["UPLOAD_ID"].ToString();
    //            sb.Append("  , ( SELECT DISTINCT " + getDateFormat + "  FROM F_UPLOAD FU JOIN M_UPLOAD_TYPE MUT ON MUT.UPLOAD_ID = FU.UPLOAD_ID  AND UPLOAD_TYPE_IND = 1 " +
    //                      "     WHERE MUT.UPLOAD_ID = '" + getDocID + "' AND FU.ISACTIVE = 1 AND MUT.ISACTIVE = 1 AND FU.REF_STR = EMP.SEMPLOYEEID   " +
    //                      "     AND FU.STOP_DATE = ( SELECT MAX(FF.STOP_DATE) FROM F_UPLOAD FF  WHERE  " +
    //                      "                           FF.UPLOAD_ID = '" + getDocID + "' AND FF.REF_STR = EMP.SEMPLOYEEID ) " +
    //                      "	   ) AS DocNum" + getDocID +
    //                      "    ,  (	SELECT DISTINCT CASE WHEN  " +
    //                      "          (        (TRUNC((months_between(SYSDATE,STOP_DATE) - TRUNC(months_between(SYSDATE,STOP_DATE)))*30)) >= 0  " +
    //                      "               AND (TRUNC(months_between(SYSDATE,STOP_DATE)  - (TRUNC(months_between(SYSDATE,STOP_DATE)/12)*12))) >= 0 " +
    //                      "               AND (TRUNC(months_between(SYSDATE,STOP_DATE)/12)) >= 0 " +
    //                      "          ) THEN 'หมดอายุไปแล้ว' ||' '||   " +
    //                      "                     (TRUNC(months_between(SYSDATE,STOP_DATE)/12)) ||' '||'ปี' ||' '||  " +
    //                      "                     (TRUNC(months_between(SYSDATE,STOP_DATE)  -    (TRUNC(months_between(SYSDATE,STOP_DATE)/12)*12))) ||' '||'เดือน'  ||' '||  " +
    //                      "                     (TRUNC((months_between(SYSDATE,STOP_DATE) - TRUNC(months_between(SYSDATE,STOP_DATE)))*30))||' '||'วัน'   " +
    //                      "                ELSE (  -1 *  TRUNC(months_between(SYSDATE,STOP_DATE)/12)||' '||'ปี' ||' '||  " +
    //                      "                        -1 *  TRUNC(months_between(SYSDATE,STOP_DATE)  - (TRUNC(months_between(SYSDATE,STOP_DATE)/12)*12))||' '||'เดือน'  ||' '||  " +
    //                      "                        -1 *  TRUNC((months_between(SYSDATE,STOP_DATE) - TRUNC(months_between(SYSDATE,STOP_DATE)))*30)||' '||'วัน' )  END  AS GETDATE_OVER1 " +
    //                      "         FROM F_UPLOAD FU 	" +
    //                      "         JOIN M_UPLOAD_TYPE MUT ON MUT.UPLOAD_ID = FU.UPLOAD_ID  AND UPLOAD_TYPE_IND = 1 AND STOP_DATE IS NOT NULL " +
    //                      "         WHERE MUT.UPLOAD_ID = '" + getDocID + "' AND FU.ISACTIVE = 1 AND MUT.ISACTIVE = 1 AND FU.REF_STR = EMP.SEMPLOYEEID   " +
    //                      "               AND FU.STOP_DATE = ( SELECT MAX(FF.STOP_DATE) FROM F_UPLOAD FF  WHERE  " +
    //                      "                                     FF.UPLOAD_ID  = '" + getDocID + "'  AND FF.REF_STR = EMP.SEMPLOYEEID ) " +
    //                      "	 ) AS จำนวนวันที่คงเหลือ" + j);
    //        }
    //        return sb.ToString();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }

    //}

    protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvData.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvData, dtDataTab1, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvData_RowCreated(object sender, GridViewRowEventArgs e)
    {
        GridViewRow row = e.Row;
        // Intitialize TableCell list
        List<TableCell> columns = new List<TableCell>();
        int noColumn = 1;
        foreach (DataControlField column in dgvData.Columns)
        {
            if (noColumn == 1)
            {
                noColumn++;
            }
            else
            {
                //Get the first Cell /Column
                if (row.Cells.Count > 1)
                {
                    TableCell cell = row.Cells[1];
                    // Then Remove it after

                    row.Cells.Remove(cell);
                    //And Add it to the List Collections
                    columns.Add(cell);
                }
            }

        }

        // Add cells
        row.Cells.AddRange(columns.ToArray());

    }
    protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell item in e.Row.Cells)
                {
                    if (item.Text.Contains("จำนวนวันที่คงเหลือ"))
                    {
                        item.Text = "ตรวจสอบวันหมดอายุ";
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView dv = (DataRowView)e.Row.DataItem;

                //e.Row.ForeColor = Color.Red;
                int i = 0;
                foreach (TableCell item in e.Row.Cells)
                {
                    string getText = e.Row.Cells[i].Text;

                    item.ForeColor = System.Drawing.Color.FromName("#4F4F4F");

                    if (getText.Contains("ปี") && getText.Contains("เดือน"))
                    {
                        item.ForeColor = System.Drawing.Color.FromName("#008B45");
                    }
                    if (getText.Contains("หมดอายุ"))
                    {
                        item.ForeColor = System.Drawing.Color.FromName("#CD2626"); //Color.Red;
                    }
                    if (getText.Contains("อายุเกินกำหนด"))
                    {
                        item.ForeColor = System.Drawing.Color.FromName("#CD2626"); //Color.Red;
                    }
                    i++;
                    //string getColor = (dv["ROW_COLOR"] + string.Empty).ToUpper();
                    // System.Drawing.Color.FromName(getColor); //Color.Red;
                    //ColorName = dataSource.Rows[i]["ROW_COLOR"].ToString();
                    //dgvSource.Rows[i].ForeColor = System.Drawing.Color.FromName(ColorName);
                }

            }

        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    protected void dgvData_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (string.Equals(e.CommandName, "view") || string.Equals(e.CommandName, "editData"))
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            string encryptedValue;
            string ID = dgvData.DataKeys[Index].Value.ToString();//SEMPLOYEEID
            encryptedValue = MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);

            switch (e.CommandName)
            {
                case "view": Response.Redirect("vendor_employee_add1.aspx?str=" + encryptedValue + "&mode=view&page=expire&id=" + ID); break;
                case "editData": Response.Redirect("vendor_employee_add1.aspx?str=" + encryptedValue + "&page=expire&id=" + ID); break;

                default:
                    break;
            }
        }
    }
    protected void cmdAdd_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("vendor_employee_add1.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Add.ToString()), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes("0"), MachineKeyProtection.All));
    }
    protected void cmdRefresh_ServerClick(object sender, EventArgs e)
    {
        try
        {
            txtSearch.Text = "";
            ddlDocType.SelectedValue = "";
            ddlStatusTab1.SelectedValue = "";
            txtDateEnd.Text = "";
            txtDateStart.Text = "";
            SEMPLOYEEID = "";
            txtDocExpireInDay.Text = "";
            cboVendor.Value = null;
            GridViewHelper.BindGridView(ref dgvData, null, true);
            if (Session["CGROUP"] + string.Empty == "0")
            {
                cboVendor.Value = Session["SVDID"] + string.Empty;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdExport_ServerClick(object sender, EventArgs e)
    {
        try
        {
            DataTable dtFinal = UserBLL.Instance.UserSelectBLLWithDocDate(this.GetConditionSearchTab1(), this.GetField("export"), this.GetJoin("export"), this.GetConditionSearchRownum());
            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            workbook.Worksheets.Add("Export");
            ExcelWorksheet worksheet = workbook.Worksheets["Export"];

            dtFinal.Columns["SEMPLOYEEID"].ColumnName = "รหัสพนักงาน";
            dtFinal.Columns["FULLNAME"].ColumnName = "ชื่อ - นามสกุล";
            ////dtFinal.Columns["PERS_CODE"].ColumnName = "เลขบัตรประชาชน";
            dtFinal.Columns["VENDOR_FULLNAME"].ColumnName = "บริษัทผู้ขนส่ง";
            //dtFinal.Columns["SEMPTPYE"].ColumnName = "ตำแหน่ง";
            dtFinal.Columns["CACTIVE"].ColumnName = "สถานะ";
            //dtFinal.Columns["INUSE"].ColumnName = "การจ้างงาน";

            dtFinal.Columns["INUSENAME"].ColumnName = "การจ้างงาน";

            dtFinal.Columns.Remove("STRANS_ID");
            dtFinal.Columns.Remove("ROW_COLOR");
            //dtFinal.Columns.Remove("SUPDATESAP");
            //dtFinal.Columns.Remove("CHECKIN");
            //dtFinal.Columns.Remove("NOTFILL");
            //dtFinal.Columns.Remove("INUSE");
            //dtFinal.Columns.Remove("INUSENAME");
            dtFinal.Columns.Remove("SEMPTPYE");
            dtFinal.Columns.Remove("LASTJOB");

            // DataRow dr = dtFinal.Rows[1];

            worksheet.InsertDataTable(dtFinal, new InsertDataTableOptions(1, 0) { ColumnHeaders = false });
            for (int j = 0; j < dtFinal.Columns.Count; j++)
            {//Export Detail
                string getColumeName = dtFinal.Columns[j].ColumnName;
                if (getColumeName.Contains("จำนวนวันที่คงเหลือ")) { getColumeName = "ตรวจสอบวันหมดอายุ"; }
                if (getColumeName.Contains("DOCNUM31"))
                {
                    getColumeName = "สำเนาบัตรประชาชน";
                    //dtFinal.num ["สำเนาบัตรประชาชน"].NumberFormat = "d-mmm-yy";
                    //for (int i = 0; i < dtFinal.Rows.Count; i++)
                    //{
                    //worksheet.Cells[i+1, j].Style. = "mmm/dd/yy";
                    //}
                }
                if (getColumeName.Contains("DOCNUM33")) { getColumeName = "ใบขับขี่ประเภท 4"; }
                if (getColumeName.Contains("DOCNUM34")) { getColumeName = "ใบขับขี่เชิงป้องกันอุบัติเหตุ"; }
                if (getColumeName.Contains("DOCNUM302")) { getColumeName = "ใบอบรม SHE"; }
                if (getColumeName.Contains("DOCNUM303")) { getColumeName = "ใบอนุญาต (ธพ.พ.๒ผ)"; }
                this.SetFormatCell(worksheet.Cells[0, j], getColumeName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
                worksheet.Columns[j].AutoFit();
            }

            string Path = this.CheckPath();
            string FileName = "รายงานสรุปอายุเอกสารพนักงาน_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }




}