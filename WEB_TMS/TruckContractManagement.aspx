﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="TruckContractManagement.aspx.cs"
    Inherits="TruckContractManagement" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <dx:ASPxCallbackPanel ID="xcpnMain" runat="server" CausesValidation="False" HideContentOnCallback="False"
        ClientInstanceName="xcpnMain" OnCallback="xcpnMain_Callback">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; } " />
        <PanelCollection>
            <dx:PanelContent>
                <div id="dvTruckContract">
                    <table border="0" cellspacing="2" cellpadding="3" width="50%">
                        <tr>
                            <td align="left">
                                ข้อมูลสัญญา </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding-left: 30px;">
                                <dx:ASPxGridView ID="gvwContract" runat="Server" ClientInstanceName="gvwContract" SkinID="_gvw" AutoGenerateColumns="False"
                                    DataSourceID="sdsContract">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO">
                                            <CellStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DBEGIN" Caption="เริ่มต้นสัญญา" ReadOnly="True">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DEND" Caption="สิ้นสุดสัญญา" ReadOnly="True">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTTYPEID" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <Settings ShowFooter="false" />
                                    <SettingsPager AlwaysShowPager="false" />
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="sdsContract" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                    CacheKeyDependency="ckdContract" SelectCommand="SELECT * FROM TCONTRACT WHERE SCONTRACTID=:SCONTRACTID AND SVENDORID=:SVENDORID">
                                    <SelectParameters>
                                        <asp:SessionParameter SessionField="CCONTRACTID" Name="SCONTRACTID" />
                                        <asp:SessionParameter SessionField="CVENDORID" Name="SVENDORID" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <dx:ASPxRoundPanel ID="rpnContractTruck" ClientInstanceName="rpn" runat="server" Width="980px" HeaderText="การใช้งานจริง">
                        <PanelCollection>
                            <dx:PanelContent ID="pctContractTruck" runat="server">
                                <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                    <tr>
                                        <td>
                                            <table border="0" cellspacing="0" cellpadding="0" width="50%">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxGridView ID="gvwTruck" runat="Server" ClientInstanceName="gvwTruck" SkinID="_gvw" AutoGenerateColumns="False"
                                                            KeyFieldName="TRID" Width="45%" EnableCallBacks="true">
                                                            <Columns>
                                                                <dx:GridViewBandColumn Caption="รถที่ใช้งานจริง" HeaderStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (หัว)" FieldName="SHEADNO">
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (ท้าย)" FieldName="STRAILERNO">
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                                <dx:GridViewDataTextColumn Caption="การจัดการ" FieldName="CHANGED">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblChanged" runat="Server" ClientInstanceName="lblChanged" Text="-" ClientVisible='<%# (Eval("CHANGED").ToString().Trim()!="IN" ? true : false) %>'>
                                                                        </dx:ASPxLabel>
                                                                        <dx:ASPxButton ID="btnMoveBack" runat="Server" ClientInstanceName="btnMoveBack" Text="ย้ายกลับ" AutoPostBack="false"
                                                                            ClientVisible='<%# (Eval("CHANGED").ToString().Trim()=="IN" ? true : false) %>'>
                                                                            <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('MOVEBACK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="TRID" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="REFCONT" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="PREVCONT" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="CHANGED" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="CSTATUS" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <SettingsLoadingPanel Mode="Disabled" />
                                                            <SettingsPager Mode="ShowAllRecords" />
                                                        </dx:ASPxGridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="gvwContractTruck" runat="Server" ClientInstanceName="gvwContractTruck" SkinID="_gvw"
                                                AutoGenerateColumns="False" KeyFieldName="TRID">
                                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; if(s.cpBindGrid != undefined) { s.cpBindGrid=undefined; if(!gvwTruck.InCallback())gvwTruck.PerformCallback('BINDGRID;');  } } " />
                                                <Columns>
                                                    <dx:GridViewBandColumn Caption="รถในเอกสารสัญญา" HeaderStyle-HorizontalAlign="Center">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (หัว)" FieldName="SHEADNO" Width="10%">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (ท้าย)" FieldName="STRAILERNO" Width="11%">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                    <dx:GridViewBandColumn Caption="การย้ายรถในสัญญา" HeaderStyle-HorizontalAlign="Center">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" Width="22%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxComboBox ID="cmbContract" runat="Server" ClientInstanceName="cmbContract" Width="190px" DataSourceID="sdsContracts"
                                                                        ValueField="SCONTRACTID" TextField="SCONTRACTNO">
                                                                        <Columns>
                                                                            <dx:ListBoxColumn Caption="#" FieldName="SCONTRACTID" Width="25px" />
                                                                            <dx:ListBoxColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="165px" />
                                                                        </Columns>
                                                                    </dx:ASPxComboBox>
                                                                     <asp:SqlDataSource ID="sdsContracts" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                CacheKeyDependency="ckdContracts" SelectCommand="SELECT SCONTRACTID,SCONTRACTNO FROM TCONTRACT WHERE SCONTRACTID<>:SCONTRACTID AND SVENDORID=:SVENDORID">
                                                <SelectParameters>
                                                    <asp:SessionParameter SessionField="CCONTRACTID" Name="SCONTRACTID" />
                                                    <asp:SessionParameter SessionField="CVENDORID" Name="SVENDORID" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="วันที่เริ่มต้นใช้งาน" Width="12%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxDateEdit ID="dteDBEGIN" runat="Server" ClientInstanceName="dteDBEGIN" Width="50px" SkinID="xdte">
                                                                    </dx:ASPxDateEdit>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="วันที่ใช้งานสิ้นสุด" Width="12%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxDateEdit ID="dteDEND" runat="Server" ClientInstanceName="dteDEND" Width="50px" SkinID="xdte">
                                                                    </dx:ASPxDateEdit>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="เอกสารแนบ" Width="13%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxUploadControl ID="ulcContractDoc" runat="Server" Width="20px" ClientInstanceName="ulcContractDoc"
                                                                        CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="ulcContractDoc_FileUploadComplete">
                                                                        <ValidationSettings AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                            MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                        </ValidationSettings>
                                                                        <BrowseButton Image-Url="Images/PaperClip-512.png" Image-ToolTip="แนบไฟล์" Image-Width="15px" Image-Height="15px"
                                                                            Text="">
                                                                        </BrowseButton>
                                                                        <ClearFileSelectionImage Width="0px" />
                                                                    </dx:ASPxUploadControl>
                                                                    <dx:ASPxButton ID="btnUpload" ClientInstanceName="btnUpload" runat="server" CausesValidation="False"
                                                                        AutoPostBack="false" CssClass="dxeLineBreakFix" Width="30px">
                                                                        <Image Width="17px" Height="17px" Url="Images/_upload.png" />
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnView" ClientInstanceName="btnView" runat="server" ToolTip="ดูข้อมูลเอกสาร" CausesValidation="False"
                                                                        AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                        CssClass="dxeLineBreakFix" Width="25px" ClientVisible="false">
                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnDel" ClientInstanceName="btnDel" runat="server" ToolTip="ลบรายการ" CausesValidation="False"
                                                                        AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                        CssClass="dxeLineBreakFix" Width="25px" ClientVisible="false">
                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxTextBox ID="txtFlag" runat="Server" ClientInstanceName="txtFlag" ClientVisible="false">
                                                                    </dx:ASPxTextBox>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="หมายเหตุ" Width="12%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxMemo ID="memRemark" runat="Server" ClientInstanceName="memRemark" Width="100px" AutoResizeWithContainer="true"
                                                                        NullText="ระบุหมายเหตุ">
                                                                    </dx:ASPxMemo>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="การจัดการ" Width="8%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxButton ID="btnMoveOut" runat="Server" ClientInstanceName="btnMoveOut" CausesValidation="false"
                                                                        AutoPostBack="false" Text="ย้ายรถ" Width="65px">
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnMoveIn" runat="Server" ClientInstanceName="btnMoveIn" CausesValidation="false"
                                                                        ClientVisible="false" AutoPostBack="false" Text="ย้ายกลับ">
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnUpdate" runat="Server" ClientInstanceName="btnUpdate" CausesValidation="false"
                                                                        ClientVisible="false" AutoPostBack="false" Text="อัพเดท" Width="65px">
                                                                    </dx:ASPxButton>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                    <dx:GridViewDataTextColumn FieldName="TRID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SCONTRACTTYPEID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="STRAILERID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SCARTYPEID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="REFCONT" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="PREVCONT" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NTRUCK" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CSTANDBY" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CHANGED" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CSTATUS" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="TOCONTRACTID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="TOCONTRACTNO" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DSTART" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DEND" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SFILENAME" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SSYSFILENAME" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SREMARK" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior ColumnResizeMode="NextColumn" />
                                                <SettingsPager Mode="ShowAllRecords" />
                                            </dx:ASPxGridView>
                                            <span style="display: none;">
                                                <dx:ASPxTextBox ID="txtVID" runat="Server" ClientInstanceName="txtVID">
                                                </dx:ASPxTextBox>
                                            </span>
                                           
                                            <asp:SqlDataSource ID="sdsContractTRUCK" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                CacheKeyDependency="ckdContractTRUCK" SelectCommand="SELECT ROWNUM TRID,CON.* FROM(
SELECT CT.SCONTRACTID,C.SCONTRACTTYPEID,C.SVENDORID,C.SCONTRACTNO,CT.STRUCKID,TH.SHEADREGISTERNO SHEADNO,CT.STRAILERID,TR.SHEADREGISTERNO STRAILERNO,TH.SCARTYPEID
,CT.CREJECT,CT.CSTANDBY,C.NTRUCK,CT.REF_SCONTRACTID REFCONT,CT.REF_PREVIOUSCONTRACT PREVCONT,CT.SCONTRACTID||'' TOCONTRACTID,C.SCONTRACTNO||'' TOCONTRACTNO,CT.DSTART||'' DSTART,CT.DEND||'' DEND,CT.SFILENAME||'' SFILENAME,CT.SSYSFILENAME||'' SSYSFILENAME,CT.SREMARK||'' SREMARK,'OUT' CHANGED,'0' CSTATUS
FROM TCONTRACT_TRUCK CT 
LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID 
LEFT JOIN TTRUCK TH ON CT.STRUCKID=TH.STRUCKID
LEFT JOIN TTRUCK TR ON CT.STRAILERID=TR.STRUCKID
WHERE CT.REF_SCONTRACTID=:SCONTRACTID

UNION

SELECT CT.SCONTRACTID,C.SCONTRACTTYPEID,C.SVENDORID,C.SCONTRACTNO,CT.STRUCKID,TH.SHEADREGISTERNO SHEADNO,CT.STRAILERID,TR.SHEADREGISTERNO STRAILERNO,TH.SCARTYPEID
,CT.CREJECT,CT.CSTANDBY,C.NTRUCK,CT.REF_SCONTRACTID REFCONT,CT.REF_PREVIOUSCONTRACT PREVCONT,'' TOCONTRACTID,'' TOCONTRACTNO,'' DSTART,'' DEND,'' SFILENAME,'' SSYSFILENAME,'' SREMARK,CASE WHEN CT.REF_SCONTRACTID IS NOT NULL THEN 'IN' ELSE 'NULL' END CHANGED,'0' CSTATUS
FROM TCONTRACT_TRUCK CT 
LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID 
LEFT JOIN TTRUCK TH ON CT.STRUCKID=TH.STRUCKID
LEFT JOIN TTRUCK TR ON CT.STRAILERID=TR.STRUCKID
WHERE CT.SCONTRACTID=:SCONTRACTID AND C.SVENDORID=:SVENDORID
) CON
ORDER BY SCARTYPEID,SCONTRACTNO">
                                                <SelectParameters>
                                                    <asp:SessionParameter SessionField="CCONTRACTID" Name="SCONTRACTID" />
                                                    <asp:SessionParameter SessionField="CVENDORID" Name="SVENDORID" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellspacing="2" cellpadding="3" width="100%">
                                                <tr>
                                                    <td align="right">
                                                        <dx:ASPxButton ID="btnSubmit" runat="Server" ClientInstanceName="btnSubmit" SkinID="_submit" AutoPostBack="false"
                                                            CausesValidation="false">
                                                            <ClientSideEvents Click="function(s,e){ var vis = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); 
                                                            dxConfirm('แจ้งเตือน','ท่านต้องการบันทึกการเปลี่ยนแปลงข้อมูลนี้  ใช่หรือไม่ ?'
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); if(!xcpnMain.InCallback()) xcpnMain.PerformCallback('SAVECHANGE;'); } 
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); }) }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxButton ID="btnCancel" runat="Server" ClientInstanceName="btnCancel" SkinID="_cancel" AutoPostBack="false"
                                                            CausesValidation="false">
                                                            <ClientSideEvents Click="function(s,e){ if(!xcpnMain.InCallback()) xcpnMain.PerformCallback('CANCEL;'); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                    <dx:ASPxRoundPanel ID="rpnSignOff" ClientInstanceName="rpn" runat="server" Width="980px" HeaderText="ประวัติการย้ายรถออก">
                        <PanelCollection>
                            <dx:PanelContent ID="pctSignOff" runat="server">
                                <dx:ASPxGridView ID="gvwSignOff" runat="Server" ClientInstanceName="gvwSignOff" SkinID="_gvw" AutoGenerateColumns="False"
                                    DataSourceID="sdsSignOff">
                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; } " />
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (หัว)" FieldName="SHEADNO" Width="10%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (ท้าย)" FieldName="STRAILERNO" Width="11%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="22%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="ประวัติการย้ายรถออก" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="DSTART" Caption="วันที่เริ่มต้นสัญญา" ReadOnly="True" Width="11%">
                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DEND" Caption="วันที่ใช้งานสิ้นสุด" ReadOnly="True" Width="11%">
                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataTextColumn Caption="เอกสารแนบ" Width="8%">
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="lblChanged" runat="Server" ClientInstanceName="lblChanged" Text="-" ClientVisible='<%# (Eval("SSYSFILENAME").ToString().Trim()=="" ? true : false) %>'>
                                                </dx:ASPxLabel>
                                                <dx:ASPxButton ID="btnView" ClientInstanceName="btnView" runat="server" ToolTip="เอกสารขอโยกย้ายรถในสัญญา"
                                                    CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                    ClientVisible='<%# (Eval("SSYSFILENAME").ToString().Trim()!="" ? true : false) %>' SkinID="NoSkind"
                                                    Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                    <ClientSideEvents Click="function (s, e) { if(!gvwSignOff.InCallback()) gvwSignOff.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="หมายเหตุ" FieldName="SREMARK" Width="19%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUS" Width="8%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SFILENAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SSYSFILENAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FLAG" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ColumnResizeMode="NextColumn" />
                                    <SettingsPager AlwaysShowPager="false" />
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="sdsSignOff" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                    CacheKeyDependency="ckdSignOff" SelectCommand="SELECT * FROM (
SELECT CT.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO,CT.STRUCKID,T.SHEADREGISTERNO SHEADNO,CT.STRAILERID,R.SHEADREGISTERNO STRAILERNO
,CT.DSTART,CT.DEND,CT.SFILENAME,CT.SSYSFILENAME,SREMARK,'ย้ายออก' STATUS,REF_SCONTRACTID FLAG
FROM TCONTRACT_TRUCK CT
LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID
LEFT JOIN TTRUCK T ON CT.STRUCKID=T.STRUCKID
LEFT JOIN TTRUCK R ON CT.STRAILERID=R.STRUCKID
WHERE REF_SCONTRACTID=:SCONTRACTID

UNION

SELECT CT.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO,CT.STRUCKID,T.SHEADREGISTERNO SHEADNO,CT.STRAILERID,R.SHEADREGISTERNO STRAILERNO
,CT.DSTART,CT.DEND,CT.SFILENAME,CT.SSYSFILENAME,SREMARK,'ย้ายออก' STATUS,REF_SCONTRACTID FLAG
FROM TCONTRACT_TRUCK_HISTORY CT
LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID
LEFT JOIN TTRUCK T ON CT.STRUCKID=T.STRUCKID
LEFT JOIN TTRUCK R ON CT.STRAILERID=R.STRUCKID
WHERE REF_SCONTRACTID=:SCONTRACTID AND CT.CREJECT ='1'

UNION

SELECT C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO,CT.STRUCKID,T.SHEADREGISTERNO SHEADNO,CT.STRAILERID,R.SHEADREGISTERNO STRAILERNO
,CT.DSTART,CT.DEND,CT.SFILENAME,CT.SSYSFILENAME,SREMARK,'ย้ายกลับ' STATUS,REF_SCONTRACTID FLAG
FROM TCONTRACT_TRUCK_HISTORY CT
LEFT JOIN TCONTRACT C ON CT.REF_SCONTRACTID=C.SCONTRACTID
LEFT JOIN TTRUCK T ON CT.STRUCKID=T.STRUCKID
LEFT JOIN TTRUCK R ON CT.STRAILERID=R.STRUCKID
WHERE CT.SCONTRACTID=:SCONTRACTID AND CT.CREJECT ='1' AND CT.REF_SCONTRACTID IS NOT NULL
) TBL
ORDER BY DSTART DESC,DEND DESC">
                                    <SelectParameters>
                                        <asp:SessionParameter SessionField="CCONTRACTID" Name="SCONTRACTID" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <br />
                    <dx:ASPxRoundPanel ID="rpnEntrance" ClientInstanceName="rpn" runat="server" Width="980px" HeaderText="ประวัติการย้ายรถเข้า">
                        <PanelCollection>
                            <dx:PanelContent ID="pctEntrance" runat="server">
                                <dx:ASPxGridView ID="gvwEntrance" runat="Server" ClientInstanceName="gvwEntrance" SkinID="_gvw" AutoGenerateColumns="False"
                                    DataSourceID="sdsEntrance">
                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; } " />
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (หัว)" FieldName="SHEADNO" Width="10%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (ท้าย)" FieldName="STRAILERNO" Width="11%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="22%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="ประวัติการย้ายรถเข้า" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="DSTART" Caption="วันที่เริ่มต้นสัญญา" ReadOnly="True" Width="11%">
                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DEND" Caption="วันที่ใช้งานสิ้นสุด" ReadOnly="True" Width="11%">
                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataTextColumn Caption="เอกสารแนบ" Width="8%">
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="lblChanged" runat="Server" ClientInstanceName="lblChanged" Text="-" ClientVisible='<%# (Eval("SSYSFILENAME").ToString().Trim()=="" ? true : false) %>'>
                                                </dx:ASPxLabel>
                                                <dx:ASPxButton ID="btnView" ClientInstanceName="btnView" runat="server" ToolTip="เอกสารขอโยกย้ายรถในสัญญา"
                                                    CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                    ClientVisible='<%# (Eval("SSYSFILENAME").ToString().Trim()!="" ? true : false) %>' SkinID="NoSkind"
                                                    Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                    <ClientSideEvents Click="function (s, e) { if(gvwEntrance.InCallback()) return; else gvwEntrance.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="หมายเหตุ" FieldName="SREMARK" Width="19%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUS" Width="8%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SFILENAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SSYSFILENAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FLAG" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ColumnResizeMode="NextColumn" />
                                    <SettingsPager AlwaysShowPager="false" />
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="sdsEntrance" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                    CacheKeyDependency="ckdEntrance" SelectCommand="SELECT * FROM (
SELECT C.SCONTRACTID,C.SCONTRACTNO
,CT.STRUCKID,T.SHEADREGISTERNO SHEADNO,CT.STRAILERID,R.SHEADREGISTERNO STRAILERNO
,CT.DSTART,CT.DEND,CT.SFILENAME,CT.SSYSFILENAME,SREMARK,'ย้ายกลับ' STATUS,REF_SCONTRACTID FLAG
FROM TCONTRACT_TRUCK_HISTORY CT
LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID
LEFT JOIN TTRUCK T ON CT.STRUCKID=T.STRUCKID
LEFT JOIN TTRUCK R ON CT.STRAILERID=R.STRUCKID
WHERE REF_SCONTRACTID=:SCONTRACTID AND CT.CREJECT ='1'

UNION

SELECT C.SCONTRACTID,C.SCONTRACTNO
,CT.STRUCKID,T.SHEADREGISTERNO SHEADNO,CT.STRAILERID,R.SHEADREGISTERNO STRAILERNO
,CT.DSTART,CT.DEND,CT.SFILENAME,CT.SSYSFILENAME,SREMARK,'ย้ายเข้า' STATUS,REF_SCONTRACTID FLAG
FROM TCONTRACT_TRUCK_HISTORY CT
LEFT JOIN TCONTRACT C ON CT.REF_SCONTRACTID=C.SCONTRACTID
LEFT JOIN TTRUCK T ON CT.STRUCKID=T.STRUCKID
LEFT JOIN TTRUCK R ON CT.STRAILERID=R.STRUCKID
WHERE CT.SCONTRACTID=:SCONTRACTID AND REF_SCONTRACTID IS NOT NULL

UNION

SELECT C.SCONTRACTID,C.SCONTRACTNO
,CT.STRUCKID,T.SHEADREGISTERNO SHEADNO,CT.STRAILERID,R.SHEADREGISTERNO STRAILERNO
,CT.DSTART,CT.DEND,CT.SFILENAME,CT.SSYSFILENAME,SREMARK,'ย้ายเข้า' STATUS,REF_SCONTRACTID FLAG
FROM TCONTRACT_TRUCK CT
LEFT JOIN TCONTRACT C ON CT.REF_SCONTRACTID=C.SCONTRACTID
LEFT JOIN TTRUCK T ON CT.STRUCKID=T.STRUCKID
LEFT JOIN TTRUCK R ON CT.STRAILERID=R.STRUCKID
WHERE CT.SCONTRACTID=:SCONTRACTID AND REF_SCONTRACTID IS NOT NULL
) CT
ORDER BY CT.DSTART DESC,DEND DESC">
                                    <SelectParameters>
                                        <asp:SessionParameter SessionField="CCONTRACTID" Name="SCONTRACTID" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
