﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class admin_user_lst : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            Session["suid"] = null;

            LogUser("30", "R", "เปิดดูข้อมูลหน้า ผู้มีสิทธิใช้งานระบบ", "");
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnDel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

        switch (cbxGroup.SelectedIndex)
        {
            case 0:
                sds.SelectCommand = @"SELECT ROW_NUMBER () OVER (ORDER BY CAST(SUID AS INT)) AS ID1,dd.MAXDCREATE,dd.SDESCRIPTION,u.SUID,(u.SFIRSTNAME || ' ' || u.SLASTNAME) AS FULLNAME,VS.SVENDORNAME AS SVENDORNAME, (CASE u.CGROUP WHEN '0' THEN 'ผู้ประกอบการขนส่ง' WHEN '1' THEN 'เจ้าหน้าที่ปตท.' ELSE 'คลัง' END) AS CGROUP1, u.DCREATE, u.SPOSITION, u.CACTIVE FROM  ((TUSER u INNER JOIN TVENDOR v ON u.SVENDORID = V.SVENDORID ) INNER JOIN TVENDOR_SAP vs ON  V.SVENDORID = VS.SVENDORID )
LEFT JOIN 
(SELECT d1.DCREATE, d1.NID,  d1.SDESCRIPTION, d1.SMENUID, d1.SREFERENTID, 
   d1.STYPE, d1.SUID,D2.MAXDCREATE FROM TUSERTRACE  d1 INNER JOIN (SELECT SUID, MAX(DCREATE) AS MAXDCREATE FROM TUSERTRACE GROUP BY SUID)  
   d2 ON D1.SUID = D2.SUID AND D1.DCREATE = D2.MAXDCREATE) dd ON u.SUID = dd.SUID
where u.CGROUP = '0' and (u.SFIRSTNAME || u.SLASTNAME like '%' || :oSearch || '%' OR VS.SVENDORNAME like '%' || :oSearch || '%')";
                break;
            case 1:
                sds.SelectCommand = @"SELECT ROW_NUMBER () OVER (ORDER BY CAST(SUID AS INT)) AS ID1,dd.MAXDCREATE,dd.SDESCRIPTION,u.SUID,(u.SFIRSTNAME || ' ' || u.SLASTNAME) AS FULLNAME,v.UNITNAME AS SVENDORNAME, (CASE u.CGROUP WHEN '0' THEN 'ผู้ประกอบการขนส่ง' WHEN '1' THEN 'เจ้าหน้าที่ปตท.' ELSE 'คลัง' END) AS CGROUP1, u.DCREATE, u.SPOSITION, u.CACTIVE FROM  (TUSER u INNER JOIN TUNIT v ON u.SVENDORID = v.UNITCODE )
LEFT JOIN 
(SELECT d1.DCREATE, d1.NID,  d1.SDESCRIPTION, d1.SMENUID, d1.SREFERENTID, 
   d1.STYPE, d1.SUID,D2.MAXDCREATE FROM TUSERTRACE  d1 INNER JOIN (SELECT SUID, MAX(DCREATE) AS MAXDCREATE FROM TUSERTRACE GROUP BY SUID)  
   d2 ON D1.SUID = D2.SUID AND D1.DCREATE = D2.MAXDCREATE) dd ON u.SUID = dd.SUID
where u.CGROUP = '1' and (u.SFIRSTNAME || u.SLASTNAME like '%' || :oSearch || '%' OR v.UNITNAME like '%' || :oSearch || '%')";
                break;
            case 2:
                sds.SelectCommand = @"SELECT ROW_NUMBER () OVER (ORDER BY CAST(SUID AS INT)) AS ID1,dd.MAXDCREATE,dd.SDESCRIPTION,u.SUID,(u.SFIRSTNAME || ' ' || u.SLASTNAME) AS FULLNAME,TS.STERMINALNAME AS SVENDORNAME, (CASE u.CGROUP WHEN '0' THEN 'ผู้ประกอบการขนส่ง' WHEN '1' THEN 'เจ้าหน้าที่ปตท.' ELSE 'คลัง' END) AS CGROUP1, u.DCREATE, u.SPOSITION, u.CACTIVE FROM  ((TUSER u INNER JOIN TTERMINAL v ON u.SVENDORID = V.STERMINALID ) INNER JOIN TTERMINAL_SAP ts ON V.STERMINALID = TS.STERMINALID )
LEFT JOIN 
(SELECT d1.DCREATE, d1.NID,  d1.SDESCRIPTION, d1.SMENUID, d1.SREFERENTID, 
   d1.STYPE, d1.SUID,D2.MAXDCREATE FROM TUSERTRACE  d1 INNER JOIN (SELECT SUID, MAX(DCREATE) AS MAXDCREATE FROM TUSERTRACE GROUP BY SUID)  
   d2 ON D1.SUID = D2.SUID AND D1.DCREATE = D2.MAXDCREATE) dd ON u.SUID = dd.SUID
where u.CGROUP = '2' and (u.SFIRSTNAME || u.SLASTNAME like '%' || :oSearch || '%' OR TS.STERMINALNAME like '%' || :oSearch || '%')";
                break;
        }
        sds.DataBind();
        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();

                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("ID1", "SUID", "FULLNAME", "SVENDORNAME", "CGROUP1", "DCREATE", "SPOSITION")
                    .Cast<object[]>()
                    .Select(s => new { ID1 = s[0].ToString(), SUID = s[1].ToString(), FULLNAME = s[2].ToString(), SVENDORNAME = s[3].ToString(), CGROUP1 = s[4].ToString(), DCREATE = s[5].ToString(), SPOSITION = s[6].ToString() });

                string delid = "";
                foreach (var l in ld)
                {
                    Session["ouid"] = l.SUID;
                    sds.Delete();

                    delid += l.SUID + ",";
                }

                LogUser("30", "D", "ลบข้อมูลหน้า ผู้มีสิทธิใช้งานระบบ รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SUID");
                string UID = Convert.ToString(data);

                Session["suid"] = UID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_user_add1.aspx";

                break;
            case "UnLock":
                dynamic data_UnLock = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SUID");
                string SUID4UnLock = Convert.ToString(data_UnLock);
                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();
                    string sCMD = "Update TUSER SET CACTIVE='1' Where SUID = '" + SUID4UnLock + "'";
                    OracleCommand com = new OracleCommand(sCMD, con);
                    com.ExecuteNonQuery();
                }

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                gvw.DataBind();
                break;
            case "selectgroup":

                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;

        if (!CanWrite)
        {
            imbedit.Enabled = false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_user_add1.aspx");

    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}
