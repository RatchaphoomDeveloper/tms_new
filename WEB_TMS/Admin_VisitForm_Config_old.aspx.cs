﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_Entity;
using Utility;
public partial class Admin_VisitForm_Config : PageBase
{

    #region " Prop"
    protected int? ConfigId
    {
        get { return int.Parse(ViewState[this.ToString() + "ConfigId"].ToString()); }
        set { ViewState[this.ToString() + "ConfigId"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected DataTable DTMain
    {
        get { return (DataTable)ViewState[this.ToString() + "DTMain"]; }
        set { ViewState[this.ToString() + "DTMain"] = value; }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            Init();
        }
    }

    public void Init()
    {
        ConfigId = 0;
        LoadYear();
        loadData();

    }
    public void LoadYear()
    {
        int StartYear = 2015;
        int cYear = DateTime.Today.Year;
        int endYear = cYear + 4;
        int index = 0;
        for (int i = StartYear; i <= endYear; i++)
        {
            cmbYear.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
        }

        ListItem sel = cmbYear.Items.FindByValue(cYear.ToString());
        if (sel != null) sel.Selected = true;
        else cmbYear.SelectedIndex = 0;
    }

    public void loadData()
    {
        string _err = string.Empty;
        DTMain = new QuestionnaireBLL().GetConfig(ref _err);
        grvMain.DataSource = DTMain;
        grvMain.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            ConfigId = 0;
            BindDDLName();
            cmbYear.ClearSelection();
            int cYear = DateTime.Today.Year;
            ListItem sel = cmbYear.Items.FindByValue(cYear.ToString());
            if (sel != null) sel.Selected = true;
            else cmbYear.SelectedIndex = 0;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogAdd", "$('#dialogAdd').modal();", true);
            uplAdd.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void BindDDLName()
    {
        string _err = string.Empty;
        DataTable dt = new QuestionnaireBLL().GetFormName(ref _err);
        DataRow dr = dt.NewRow();
        dr["STYPEVISITFORMNAME"] = "- เลือกข้อมูล -";
        dt.Rows.InsertAt(dr, 0);
        ddlName.DataSource = dt;
        ddlName.DataBind();
        ddlName.SelectedIndex = 0;
    }

    public void Edit(TTYPEOFVISITFORM_YEARS item)
    {
        string _err = string.Empty;
        new QuestionnaireBLL().AddEditConfig(ref _err, item, user_profile);
        if (!string.IsNullOrEmpty(_err))
        {
            alertFail(_err);
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogAdd", "$('#dialogAdd').modal('hide');", true);
            loadData();
            uplMain.Update();
            alertSuccess("บันทึกข้อมูลเรียบร้อย");
        }

    }

    public void Delete(Decimal Id)
    {
        string _err = string.Empty;
        new QuestionnaireBLL().DeleteConfig(ref _err, user_profile, int.Parse(Id.ToString()));
        if (!string.IsNullOrEmpty(_err))
        {
            alertFail(_err);
            return;
        }
        else
        {
            loadData();
            uplMain.Update();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (cmbYear.SelectedIndex == 0)
            {
                alertFail("กรุณาระบุปีที่ต้องการ");
                return;
            }

            if (ddlName.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกชื่อฟอร์มที่ต้องการ");
                return;
            }

            TTYPEOFVISITFORM_YEARS item = new TTYPEOFVISITFORM_YEARS() { YEAR = int.Parse(cmbYear.SelectedItem.Value.Trim()), NTYPEVISITFORMID = int.Parse(ddlName.SelectedItem.Value.ToString()), ID = int.Parse(ConfigId.ToString()) };
            string _err = string.Empty;
            if (ConfigId > 0) // Edit
            {
                if (new QuestionnaireBLL().IsInUse(ref _err, item))
                {
                    Util.OnMessageBox(page: this.Page,
                        Message: " แบบฟอร์มนี้มีการบันทึกข้อมูลแล้ว ต้องกรอกคะแนนใหม่หากทำการแก้ไข!",
                        headerText: "แจ้งเตือน",
                        messagetype: MessageType.Confirm,
                        messageFlag: MessageFlag.Warning,
                        onMethodConfirm: "Edit",
                        parameters: new Object[] { item });
                }
                else
                {
                    new QuestionnaireBLL().AddEditConfig(ref _err, item, user_profile);
                    if (!string.IsNullOrEmpty(_err))
                    {
                        alertFail(_err);
                        return;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogAdd", "$('#dialogAdd').modal('hide');", true);
                        loadData();
                        uplMain.Update();
                        alertSuccess("บันทึกข้อมูลเรียบร้อย");
                    }
                }
            }
            else
            {
                new QuestionnaireBLL().AddEditConfig(ref _err, item, user_profile);
                if (!string.IsNullOrEmpty(_err))
                {
                    alertFail(_err);
                    return;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogAdd", "$('#dialogAdd').modal('hide');", true);
                    loadData();
                    uplMain.Update();
                    alertSuccess("เพิ่มข้อมูลเรียบร้อยแล้ว");
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }
    protected void grvMain_RowCommand(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewRowCommandEventArgs e)
    {
        try
        {
            if (e.CommandArgs.CommandName == "Delete")
            {
                var Id = grvMain.GetRowValuesByKeyValue(e.KeyValue, "ID");
                if (Id != null)
                {
                    Util.OnMessageBox(page: this.Page,
                           Message: " ลบข้อมูลที่เลือก ?",
                           headerText: "แจ้งเตือน",
                           messagetype: MessageType.Confirm,
                           messageFlag: MessageFlag.Warning,
                           onMethodConfirm: "Delete",
                           parameters: new Object[] { Id });
                }
            }
            else if (e.CommandArgs.CommandName == "Select")
            {
                var Id = grvMain.GetRowValuesByKeyValue(e.KeyValue, "ID");
                if (Id != null)
                {
                    ConfigId = int.Parse(Id.ToString());
                    BindDDLName();
                    string _err = string.Empty;
                    DataTable dt = new QuestionnaireBLL().GetConfig(ref _err, ConfigId);
                    string _strYear = dt.Rows[0]["YEAR"].ToString();
                    ListItem sel = cmbYear.Items.FindByValue(_strYear);
                    if (sel != null)
                    {
                        cmbYear.ClearSelection();
                        sel.Selected = true;
                    }
                    else
                    {
                        cmbYear.Items.Add(new ListItem() { Selected = true, Text = _strYear, Value = _strYear });
                    }
                    string FormId = dt.Rows[0]["NTYPEVISITFORMID"].ToString();

                    ListItem selForm = ddlName.Items.FindByValue(FormId);
                    if (selForm != null)
                    {
                        ddlName.ClearSelection();
                        selForm.Selected = true;
                    }
                    else
                    {
                        ddlName.SelectedIndex = 0;
                    }

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogAdd", "$('#dialogAdd').modal();", true);
                    uplAdd.Update();
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogAdd", "$('#dialogAdd').modal('hide');", true);
        uplAdd.Update();
    }
}