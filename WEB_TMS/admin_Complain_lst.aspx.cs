﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.IO;
using GemBox.Spreadsheet;
using System.Drawing;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Master;
using System.Web.Security;
using System.Configuration;

public partial class admin_Complain_lst : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    #region + ViewState +
    private DataTable dtExport
    {
        get
        {
            if ((DataTable)ViewState["dtExport"] != null)
                return (DataTable)ViewState["dtExport"];
            else
                return null;
        }
        set
        {
            ViewState["dtExport"] = value;
        }
    }

    private DataTable dtComplainList
    {
        get
        {
            if ((DataTable)ViewState["dtComplainList"] != null)
                return (DataTable)ViewState["dtComplainList"];
            else
                return null;
        }
        set
        {
            ViewState["dtComplainList"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        #region EventHandler
        //gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {            
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            //dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy");
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            Session["oSSERVICEID"] = null;

            LogUser("21", "R", "เปิดดูข้อมูลหน้า รับเรื่องร้องเรียน", "");

            this.InitialForm();
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    BindFirstData();
        //}
        //else
        //{
        //BindData();
        //}
        //lblCarCount.Text = ((DataView)sds.Select(DataSourceSelectArguments.Empty)).ToTable().Rows.Count + "";
        this.LoadDocStatus();
        this.LoadComplainType();
    }

    private void LoadDocStatus()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = DocStatusBLL.Instance.DocStatusSelectBLL();
            ComboBoxHelper.BindComboBox(ref cboStatus, dt, true, "ทั้งหมด", "DOC_STATUS_NAME");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    //กด แสดงเลข Record
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            //case "Search":

            //    Cache.Remove(sds.CacheKeyDependency);
            //    Cache[sds.CacheKeyDependency] = new object();
            //    sds.Select(new System.Web.UI.DataSourceSelectArguments());
            //    sds.DataBind();

            //    break;

            case "delete":
                //var ld = gvw.GetSelectedFieldValues("ID1", "SSERVICEID")
                //    .Cast<object[]>()
                //    .Select(s => new { ID1 = s[0].ToString(), SSERVICEID = s[1].ToString() });

                //foreach (var l in ld)
                //{
                //    Session["delSSERVICEID"] = l.SSERVICEID;
                //    sds.Delete();

                //}
                string delid = "";
                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();
                    //foreach (var l in ld)
                    //{
                    //    Session["delSSERVICEID"] = l.SSERVICEID;
                    //    sds.Delete();

                    //    delid += l.SSERVICEID + ",";

                    //    string strsql = "SELECT * FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = " + l.SSERVICEID;
                    //    DataTable dt = new DataTable();
                    //    dt = CommonFunction.Get_Data(con, strsql);

                    //    if (dt.Rows.Count > 0)
                    //    {

                    //        string strdel = "DELETE FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = " + l.SSERVICEID;
                    //        using (OracleCommand com = new OracleCommand(strdel, con))
                    //        {
                    //            com.ExecuteNonQuery();
                    //        }

                    //        foreach (DataRow dr in dt.Rows)
                    //        {

                    //            string FilePath = dr["SFILEPATH"] + "";

                    //            if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                    //            {
                    //                File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                    //            }
                    //        }
                    //    }
                    //}
                }

                LogUser("21", "D", "ลบข้อมูลหน้า รับเรื่องร้องเรียน รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                //gvw.Selection.UnselectAll();

                break;
            case "edit":


                //dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SSERVICEID");
                //dynamic DocID = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "DOC_ID");
                //string stmID = Convert.ToString(data);

                //Session["oSSERVICEID"] = stmID;
                //Session["DocID"] = DocID;

                // Ping Start 20/08/2015 Ref. No 11
                #region + Old Code +
                //xcpn.JSProperties["cpRedirectTo"] = "admin_Complain_add.aspx";
                #endregion

                #region + New Code +
                xcpn.JSProperties["cpRedirectOpen"] = "admin_Complain_add.aspx";
                #endregion
                // Ping End

                break;

        }

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //if (Session["DocID"] != null)
        //    Session["DocID"] = null;
        //Response.Redirect("admin_Complain_add.aspx");
        MsgAlert.OpenForm("admin_Complain_add.aspx", Page);
    }

    private void BindData()
    {
        string condition = "";

        if ("" + cboStatus.Value == "1")
        {
            condition = "AND ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - c.DCREATE)) > 0";
        }
        else if ("" + cboStatus.Value == "2")
        {
            condition = "AND (ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5')";
        }
        else if ("" + cboStatus.Value == "3")
        {
            condition = "AND ((ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - c.DCREATE)) <= 0) OR ap.CSTATUS = '6' OR ap.CSTATUS = '3')";
        }
        condition = ("" + cboStatus.Value != "") ? " AND (CASE WHEN ap.SAPPEALID IS NULL THEN  nvl(c.CSTATUS,'1')  ELSE '2' END)='" + CommonFunction.ReplaceInjection("" + cboStatus.Value) + "'" : "";
        string sql = @"SELECT ROW_NUMBER () OVER (ORDER BY CAST(c.SSERVICEID AS INT) DESC) AS ID1,c.SSERVICEID,c.DDATECOMPLAIN,V.SVENDORNAME,c.SHEADREGISTERNO,c.STRAILERREGISTERNO,T.STOPICNAME,
CASE WHEN c.CCUT = '1' THEN 'ตัดคะแนนแล้ว' ELSE 'ไม่ตัดคะแนน' END AS CUTSCORE,CASE WHEN CAST((c.DCLOSE - SYSDATE) AS INT) <= 0 THEN 0 ELSE CAST((c.DCLOSE - SYSDATE) AS INT) END AS DCLOSE, 

CASE WHEN ap.SAPPEALID IS NULL THEN 
    --(CASE WHEN TRUNC(7 - (SYSDATE - c.DCREATE)) > 0 THEN 
    (
    CASE nvl(c.CSTATUS,'1') 
    WHEN '1' THEN 'ดำเนินการ'
    WHEN '2' THEN 'อุทธรณ์'
    WHEN '3' THEN 'ปิดเรื่อง' 
    END)
     --ELSE 'กำลังดำเนินการ*' END)  
ELSE
    (CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN 'กำลังอุทธรณ์' 
    ELSE
        (CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN 'รอปิดเรื่อง'  END) 
    END) 
END  As STATUS, TCONTRACT.SCONTRACTNO
--,THEADCOMPLAIN.SDETAIL SHEADCOMLIAINNAME
,comp.SCOMPLAINTYPENAME SHEADCOMLIAINNAME, c.DOC_ID, C.SCREATE
 FROM ((TCOMPLAIN c LEFT JOIN TVENDOR_SAP v ON c.SVENDORID = V.SVENDORID)
 LEFT JOIN (SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'C_1_1' ) THEADCOMPLAIN ON NVL(c.SHEADCOMLIAIN,-1)=THEADCOMPLAIN.NID
 LEFT JOIN TTOPIC t ON c.STOPICID = T.STOPICID AND C.SVERSION = T.SVERSION )
 LEFT JOIN TAPPEAL ap ON C.SSERVICEID = AP.SREFERENCEID AND AP.SAPPEALTYPE='080' 
LEFT JOIN TCONTRACT ON c.SCONTRACTID = TCONTRACT.SCONTRACTID
LEFT JOIN TCOMPLAINTYPE comp ON comp.SCOMPLAINTYPEID =c.SHEADCOMLIAIN
LEFT JOIN TUSER ON c.SCREATE = TUSER.SUID
WHERE (c.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR c.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR TCONTRACT.SCONTRACTNO LIKE '%' || :oSearch || '%' OR c.DOC_ID LIKE '%' || :oSearch || '%' OR TUSER.SUSERNAME LIKE '%' || :oSearch || '%' OR 
c.SEMPLOYEENAME LIKE '%' || :oSearch || '%' OR V.SVENDORNAME LIKE '%' || :oSearch || '%' ) AND to_char(c.DDATECOMPLAIN,'yyyyMMdd') >= :dStart AND to_char(c.DDATECOMPLAIN,'yyyyMMdd') <= :dEnd  " + condition;

        sds.SelectCommand = sql;
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("oSearch", txtSearch.Text);
        //sds.SelectParameters.Add("dStart", dteStart.Date.ToString("yyyyMMdd", new CultureInfo("en-US")));
        //sds.SelectParameters.Add("dEnd", dteEnd.Date.ToString("yyyyMMdd", new CultureInfo("en-US")));
        sds.SelectParameters.Add("dStart", DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd"));
        sds.SelectParameters.Add("dEnd", DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd"));


        sds.DataBind();
        //gvw.DataBind();

        DataView dv = (DataView)sds.Select(DataSourceSelectArguments.Empty);
        dtExport = dv.ToTable();
        //SELECT ROW_NUMBER () OVER (ORDER BY CAST(c.SSERVICEID AS INT) DESC) AS ID1,c.SSERVICEID,c.DDATECOMPLAIN,V.SVENDORNAME,c.SHEADREGISTERNO,c.STRAILERREGISTERNO,T.STOPICNAME, M_COMPLAIN_POINT.COMPLAIN_POINT_NAME AS CUTSCORE,CASE WHEN CAST((c.DCLOSE - SYSDATE) AS INT) <= 0 THEN 0 ELSE CAST((c.DCLOSE - SYSDATE) AS INT) END AS DCLOSE, DOC_STATUS_NAME AS STATUS, TCONTRACT.SCONTRACTNO, comp.SCOMPLAINTYPENAME SHEADCOMLIAINNAME, c.DOC_ID, C.SCREATE, TUSER.SFIRSTNAME || ' ' || TUSER.SLASTNAME AS CREATENAME, TO_CHAR(DDELIVERY, 'DD/MM/YYYY') AS DDELIVERY, TO_CHAR(DCREATE, 'DD/MM/YYYY') AS DCREATE FROM ((TCOMPLAIN c LEFT JOIN TVENDOR_SAP v ON c.SVENDORID = V.SVENDORID) LEFT JOIN (SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'C_1_1' ) THEADCOMPLAIN ON NVL(c.SHEADCOMLIAIN,-1) = THEADCOMPLAIN.NID LEFT JOIN TTOPIC t ON c.STOPICID = T.STOPICID AND C.SVERSION = T.SVERSION ) LEFT JOIN TAPPEAL ap ON C.SSERVICEID = AP.SREFERENCEID AND AP.SAPPEALTYPE = '080' LEFT JOIN TCONTRACT ON c.SCONTRACTID = TCONTRACT.SCONTRACTID LEFT JOIN TCOMPLAINTYPE comp ON comp.SCOMPLAINTYPEID = c.SHEADCOMLIAIN LEFT JOIN TUSER ON c.SCREATE = TUSER.SUID LEFT JOIN M_DOC_STATUS ON c.DOC_STATUS_ID = M_DOC_STATUS.DOC_STATUS_ID LEFT JOIN M_COMPLAIN_POINT ON c.CCUT = M_COMPLAIN_POINT.ID WHERE 1=1

        this.CanEdit(dtExport);                 //ต้องเป็นผู้สร้างเอกสารนั้นเท่านั้น  จึงจะทำการแก้ไขเอกสารได้
    }




    //    private void BindFirstData()
    //    {
    //        string condition = "";

    //        condition = "AND ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - c.DCREATE)) > 0 OR (ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5')";

    //        string sql = @"SELECT ROW_NUMBER () OVER (ORDER BY CAST(c.SSERVICEID AS INT) DESC) AS ID1,c.SSERVICEID,c.DDATECOMPLAIN,V.SVENDORNAME,c.SHEADREGISTERNO,c.STRAILERREGISTERNO,T.STOPICNAME,
    //CASE WHEN c.CCUT = '1' THEN 'ตัดคะแนนแล้ว' ELSE 'ไม่ตัดคะแนน' END AS CUTSCORE,CASE WHEN CAST((c.DCLOSE - SYSDATE) AS INT) <= 0 THEN 0 ELSE CAST((c.DCLOSE - SYSDATE) AS INT) END AS DCLOSE, 
    //
    //CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (SYSDATE - c.DCREATE)) > 0 THEN 'ดำเนินการ' ELSE 'ปิดเรื่อง' END  ELSE
    //CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN 'อุทธรณ์' ELSE
    //CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN 'ปิดเรื่อง'  END END END  As STATUS
    //
    // FROM ((TCOMPLAIN c LEFT JOIN TVENDOR_SAP v ON c.SVENDORID = V.SVENDORID)
    // LEFT JOIN TTOPIC t ON c.STOPICID = T.STOPICID AND C.SVERSION = T.SVERSION )
    // LEFT JOIN TAPPEAL ap ON C.SSERVICEID = AP.SREFERENCEID AND AP.SAPPEALTYPE='080' WHERE (c.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR c.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR 
    //c.SEMPLOYEENAME LIKE '%' || :oSearch || '%' OR V.SVENDORNAME LIKE '%' || :oSearch || '%' ) " + condition;

    //        sds.SelectCommand = sql;
    //        sds.SelectParameters.Clear();
    //        sds.SelectParameters.Add("oSearch", txtSearch.Text);

    //        sds.DataBind();
    //        gvw.DataBind();

    //    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            workbook.Worksheets.Add("Export");
            ExcelWorksheet worksheet = workbook.Worksheets["Export"];

            DataTable dtFinal = new DataTable();
            dtFinal = dtExport.Clone();

            //Parameter List ของ SSERVICEID วนจาก dtExport
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dtExport.Rows.Count; i++)
            {
                if (i == 0)
                    sb.Append("'" + dtExport.Rows[i]["DOC_ID"].ToString() + "'");
                else
                {
                    sb.Append(",");
                    sb.Append("'" + dtExport.Rows[i]["DOC_ID"].ToString() + "'");
                }

            }
            //dtFinal = ComplainBLL.Instance.ComplainExportSelectBLL(" AND CO.SSERVICEID IN (" + sb.ToString() + ")");
            dtFinal = ComplainBLL.Instance.ComplainExportFinalSelectBLL(" AND TCOMPLAIN.DOC_ID IN (" + sb.ToString() + ")");

            #region " ไม่ใช้แล้ว "
            /*DataTable dtImportFile;
            for (int i = 0; i < dtFinal.Rows.Count; i++)
            {//Get import file
                //dtImportFile = ComplainBLL.Instance.ComplainImportFileBLL(dtFinal.Rows[i]["DOC_ID"].ToString());
                dtImportFile = ComplainBLL.Instance.ComplainImportSelectBLL(dtFinal.Rows[i]["DOC_ID"].ToString());

                for (int j = 0; j < dtImportFile.Rows.Count; j++)
                {
                    if (j == 0)
                        dtFinal.Rows[i]["SDOCUMENT"] = (j + 1).ToString() + ". " + dtImportFile.Rows[j]["UPLOAD_NAME"].ToString();
                    else
                        dtFinal.Rows[i]["SDOCUMENT"] += '\n' + (j + 1).ToString() + ". " + dtImportFile.Rows[j]["UPLOAD_NAME"].ToString();
                }
            }*/
            #endregion " ไม่ใช้แล้ว "

            DataTable dtCusScore;
            for (int i = 0; i < dtFinal.Rows.Count; i++)
            {
                dtCusScore = ComplainBLL.Instance.ExportScoreSelectBLL(dtFinal.Rows[i]["DOC_ID"].ToString());

                bool IS_CORRUPT = false;
                for (int j = 0; j < dtCusScore.Rows.Count; j++)
                {
                    if (j == 0)
                        dtFinal.Rows[i]["STOPICNAME"] = dtCusScore.Rows[j]["STOPICNAME"].ToString();
                    else
                        dtFinal.Rows[i]["STOPICNAME"] += '\n' + dtCusScore.Rows[j]["STOPICNAME"].ToString();

                    if (string.Equals(dtCusScore.Rows[j]["IS_CORRUPT"].ToString(), "1"))
                        IS_CORRUPT = true;
                }

                dtFinal.Rows[i]["IS_CORRUPT"] = (IS_CORRUPT) ? "ทุจริต" : string.Empty;
            }
            dtFinal.Columns["DOC_STATUS_NAME"].SetOrdinal(0);
            dtFinal.Columns["DOC_STATUS_NAME"].ColumnName = "สถานะเอกสาร";

            dtFinal.Columns["GROUPNAME"].SetOrdinal(1);
            dtFinal.Columns["GROUPNAME"].ColumnName = "กลุ่มงานที่";

            dtFinal.Columns["DOC_ID"].SetOrdinal(2);
            dtFinal.Columns["DOC_ID"].ColumnName = "หมายเลขเอกสาร"; //"เลขที่เอกสาร";//

            dtFinal.Columns["TOPIC_NAME"].SetOrdinal(3);
            dtFinal.Columns["TOPIC_NAME"].ColumnName = "ประเภทร้องเรียน";

            dtFinal.Columns["SUBJECT"].SetOrdinal(4);
            dtFinal.Columns["SUBJECT"].ColumnName = "หัวข้อร้องเรียน";

            dtFinal.Columns["SABBREVIATION"].SetOrdinal(5);
            dtFinal.Columns["SABBREVIATION"].ColumnName = "บริษัทผู้ขนส่ง";

            dtFinal.Columns["SCONTRACTNO"].SetOrdinal(6);
            dtFinal.Columns["SCONTRACTNO"].ColumnName = "เลขที่สัญญา";

            dtFinal.Columns["SHEADREGISTERNO"].SetOrdinal(7);
            dtFinal.Columns["SHEADREGISTERNO"].ColumnName = "ทะเบียนรถ (หัว)";

            dtFinal.Columns["STRAILERREGISTERNO"].SetOrdinal(8);
            dtFinal.Columns["STRAILERREGISTERNO"].ColumnName = "ทะเบียนรถ (ท้าย)";

            dtFinal.Columns["TOTAL_CAR"].SetOrdinal(9);
            dtFinal.Columns["TOTAL_CAR"].ColumnName = "จำนวนรถ (คัน)";

            dtFinal.Columns["FULLNAME"].SetOrdinal(10);
            dtFinal.Columns["FULLNAME"].ColumnName = "ชื่อพขร. ที่ถูกร้องเรียน";

            dtFinal.Columns["SPERSONALNO"].SetOrdinal(11);
            dtFinal.Columns["SPERSONALNO"].ColumnName = "ID No. พขร.";

            dtFinal.Columns["SDELIVERYNO"].SetOrdinal(12);
            dtFinal.Columns["SDELIVERYNO"].ColumnName = "Delivery No.";

            dtFinal.Columns["DDELIVERY"].SetOrdinal(13);
            dtFinal.Columns["DDELIVERY"].ColumnName = "วันที่เกิดเหตุ";

            dtFinal.Columns["SCOMPLAINADDRESS"].SetOrdinal(14);
            dtFinal.Columns["SCOMPLAINADDRESS"].ColumnName = "สถานที่เกิดเหตุ";

            dtFinal.Columns["WAREHOUSE"].SetOrdinal(15);
            dtFinal.Columns["WAREHOUSE"].ColumnName = "คลังต้นทาง";

            dtFinal.Columns["WAREHOUSE_TO"].SetOrdinal(16);
            dtFinal.Columns["WAREHOUSE_TO"].ColumnName = "ปลายทาง";

            dtFinal.Columns["SDETAIL"].SetOrdinal(17);
            dtFinal.Columns["SDETAIL"].ColumnName = "สาเหตุ / รายละเอียด";

            dtFinal.Columns["SSOLUTION"].SetOrdinal(18);
            dtFinal.Columns["SSOLUTION"].ColumnName = "การดำเนินการ";

            dtFinal.Columns["SCOMPLAINFNAME"].SetOrdinal(19);
            dtFinal.Columns["SCOMPLAINFNAME"].ColumnName = "ชื่อผู้ร้องเรียน";

            dtFinal.Columns["SCOMPLAINDIVISION"].SetOrdinal(20);
            dtFinal.Columns["SCOMPLAINDIVISION"].ColumnName = "หน่วยงานผู้ร้องเรียน";

            dtFinal.Columns["DDATECOMPLAIN"].SetOrdinal(21);
            dtFinal.Columns["DDATECOMPLAIN"].ColumnName = "วันที่วันที่รับร้องเรียน";

            dtFinal.Columns["CREATENAME"].SetOrdinal(22);
            dtFinal.Columns["CREATENAME"].ColumnName = "ผู้บันทึก";

            dtFinal.Columns["CREATE_POSITION"].SetOrdinal(23);
            dtFinal.Columns["CREATE_POSITION"].ColumnName = "ตำแหน่งผู้บันทึก";

            dtFinal.Columns["SUNITNAME"].SetOrdinal(24);
            dtFinal.Columns["SUNITNAME"].ColumnName = "หน่วยงานผู้บันทึก";

            dtFinal.Columns["DCREATE"].SetOrdinal(25);
            dtFinal.Columns["DCREATE"].ColumnName = "เวลาบันทึก";

            dtFinal.Columns["DUPDATE"].SetOrdinal(26);
            dtFinal.Columns["DUPDATE"].ColumnName = "เวลาแก้ไขล่าสุด";

            dtFinal.Columns["UPDATENAME"].SetOrdinal(27);
            dtFinal.Columns["UPDATENAME"].ColumnName = "ผู้ตัดสิน";

            dtFinal.Columns["UPDATE_POSITION"].SetOrdinal(28);
            dtFinal.Columns["UPDATE_POSITION"].ColumnName = "ตำแหน่งผู้ตัดสิน";

            dtFinal.Columns["COMPLAIN_DATE"].SetOrdinal(29);
            dtFinal.Columns["COMPLAIN_DATE"].ColumnName = "วันที่ตัดสิน";

            dtFinal.Columns["STOPICNAME"].SetOrdinal(30);
            dtFinal.Columns["STOPICNAME"].ColumnName = "หัวข้อสัญญาหักคะแนน";

            dtFinal.Columns["CCUT"].SetOrdinal(31);
            dtFinal.Columns["CCUT"].ColumnName = "คะแนนที่ตัด";

            dtFinal.Columns["FINECOST"].SetOrdinal(32);
            dtFinal.Columns["FINECOST"].ColumnName = "ค่าปรับ";

            dtFinal.Columns["COST_OTHER"].SetOrdinal(33);
            dtFinal.Columns["COST_OTHER"].ColumnName = "ค่าเสียหายหรือค่าใช้จ่าย";

            dtFinal.Columns["OIL_LOSE"].SetOrdinal(34);
            dtFinal.Columns["OIL_LOSE"].ColumnName = "ปริมาณเสียหาย/สูญหาย";

            dtFinal.Columns["TOTAL_DISABLE_DRIVER"].SetOrdinal(35);
            dtFinal.Columns["TOTAL_DISABLE_DRIVER"].ColumnName = "วันระงับ พขร.";

            dtFinal.Columns["BLACKLIST"].SetOrdinal(36);
            dtFinal.Columns["BLACKLIST"].ColumnName = "Blacklist หรือไม่";

            dtFinal.Columns["IS_CORRUPT"].SetOrdinal(37);
            dtFinal.Columns["IS_CORRUPT"].ColumnName = "ทุจริตหรือไม่";

            dtFinal.Columns["TOTAL_COST"].SetOrdinal(38);
            dtFinal.Columns["TOTAL_COST"].ColumnName = "จำนวนเงินทั้งหมด";

            dtFinal.Columns["TOTALPAID"].SetOrdinal(39);
            dtFinal.Columns["TOTALPAID"].ColumnName = "จำนวนเงินที่ชำระแล้ว";

            dtFinal.Columns["COST_CHECK"].SetOrdinal(40);
            dtFinal.Columns["COST_CHECK"].ColumnName = "สถานะการชำระเงิน";

            dtFinal.Columns["COST_CHECK_DATE"].SetOrdinal(41);
            dtFinal.Columns["COST_CHECK_DATE"].ColumnName = "วันที่ชำระเงินครบถ้วน";

            try
            {
                dtFinal.Columns.Remove("SDOCUMENT");
                dtFinal.Columns.Remove("DATEIMPORT");
                dtFinal.Columns.Remove("COMPLAIN_TYPE_NAME");
            }
            catch (Exception)
            {
            }

            for (int i = 1; i <= dtFinal.Rows.Count; i++)
            {
                for (int j = 0; j < dtFinal.Columns.Count; j++)
                {//Export Detail
                    if (i == 1)
                    {//Export Header
                        this.SetFormatCell(worksheet.Cells[0, j], dtFinal.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                        worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
                        worksheet.Columns[j].AutoFit();
                    }

                    if (string.Equals(dtFinal.Columns[j].ColumnName, "เอกสารหลักฐานที่มี/การดำเนินการ") || string.Equals(dtFinal.Columns[j].ColumnName, "หัวข้อสัญญาหักคะแนน"))
                        this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 1][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, true);
                    else
                        this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 1][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false);

                    worksheet.Columns[j].AutoFit();
                }
            }

            string Path = this.CheckPath();
            string FileName = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CanEdit(DataTable dtExport)
    {
        try
        {
            string UserID = Session["UserID"].ToString();

            for (int i = 0; i < dtExport.Rows.Count; i++)
            {
                if (!string.Equals(UserID, dtExport.Rows[i]["SCREATE"].ToString()))
                {
                    //Disable Edit Button
                    //gvw.GetRowValuesByKeyValue(dtExport.Rows[i]["SSERVICEID"].ToString(), "SSERVICEID");

                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void gvw_DataBound(object sender, EventArgs e)
    {

    }

    protected void gvw_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        //ASPxGridView gv = (ASPxGridView)sender;
        //if (e.Column.FieldName == "edit")
        //{
        //    ASPxLabel lbl = (ASPxLabel)gv.FindEditRowCellTemplateControl((GridViewDataColumn)gv.Columns["SSERVICEID"], "teste_001");
        //    lbl.Text = e.Value.ToString();
        //}
    }
    protected void gvw_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {


    }
    protected void gvw_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {

    }

    private void InitialForm()
    {
        try
        {
            this.LoadDocStatus();
            this.LoadComplainType();

            cboStatus.SelectedIndex = 0;

            DataTable dt = new DataTable();
            dt.Columns.Add("ValueField");
            dt.Columns.Add("TextField");
            dt.Rows.Add("", "- เงื่อนไขค้นหา -");
            dt.Rows.Add("DOC_ID", "หมายเลขเอกสาร");
            dt.Rows.Add("SCONTRACTNO", "เลขที่สัญญา");
            dt.Rows.Add("SHEADREGISTERNO", "ทะเบียนรถ");
            dt.Rows.Add("M_GROUPS.NAME", "กลุ่มที่");
            dt.Rows.Add("DRIVER", "รหัส,ชื่อ-นามสกุล พขร.");

            if (Session["CGROUP"] + "" == ConfigValue.UserGroup1.ToString())
            {//เป็นผู้ขนส่ง
                btnAdd.Visible = false;
                btnExport.Visible = false;

                cboStatus.Enabled = false;
                txtCreateBy.Enabled = false;

                chkIsCorrupt.Visible = false;
                lblUrgent.Visible = false;
                chkIsUrgent.Visible = false;
            }
            else
            {
                dt.Rows.Add("SCOMPLAINFNAME", "ชื่อผู้ร้องเรียน");
                dt.Rows.Add("SVENDORNAME", "ชื่อผู้ประกอบการ");
            }

            cboSearchType.DataSource = dt;
            cboSearchType.ValueField = "ValueField";
            cboSearchType.TextField = "TextField";
            cboSearchType.DataBind();
            cboSearchType.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadComplainType()
    {
        try
        {
            DataTable dtComplainType = ComplainTypeBLL.Instance.ComplainTypeSelect(string.Empty);
            ComboBoxHelper.BindComboBox(ref cboComplainType, dtComplainType, true, "ทั้งหมด", "COMPLAIN_TYPE_NAME");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadComplainList()
    {
        try
        {
            dtComplainList = ComplainBLL.Instance.ComplainListSelectBLL(this.GetConditionSearch());
            GridViewHelper.BindGridView(ref dgvComplain, dtComplainList);

            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                dgvComplain.Columns[6].Visible = false;

            for (int i = 0; i < dtComplainList.Rows.Count; i++)
            {
                if (string.Equals(dtComplainList.Rows[i]["IS_URGENT"].ToString(), "1"))
                {
                    for (int j = 0; j < dgvComplain.Columns.Count; j++)
                        dgvComplain.Rows[i].Cells[j].ForeColor = Color.Red;
                }
            }

            dtExport = dtComplainList;

            lblCarCount.Text = dtComplainList.Rows.Count.ToString();
            //gvw.DataSource = dtComplainList;
            //gvw.DataBind();

            dtExport = dtComplainList;

            //this.CanEdit(dtExport);                 //ต้องเป็นผู้สร้างเอกสารนั้นเท่านั้น  จึงจะทำการแก้ไขเอกสารได้
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.LoadComplainList();
    }

    private string GetConditionSearch()
    {
        try
        {
            lblContractID.Text = "ผลการค้นหา เลขที่สัญญา {0}";
            lblContractID.Visible = false;

            StringBuilder sb = new StringBuilder();
            if (cboStatus.Value != null)
                sb.Append(" AND c.DOC_STATUS_ID = " + cboStatus.Value.ToString());

            if (cboSearchType.Value != null)
            {
                switch (cboSearchType.Value.ToString())
                {
                    case "DOC_ID": sb.Append(" AND c.DOC_ID LIKE '%" + txtSearch.Text.Trim() + "%'"); break;
                    case "SCONTRACTNO": sb.Append(" AND TCONTRACT.SCONTRACTNO LIKE '%" + txtSearch.Text.Trim() + "%'");
                        lblContractID.Visible = true;
                        lblContractID.Text = string.Format(lblContractID.Text, txtSearch.Text.Trim());
                        break;
                    case "SHEADREGISTERNO": sb.Append(" AND c.SHEADREGISTERNO LIKE '%" + txtSearch.Text.Trim() + "%' OR c.STRAILERREGISTERNO LIKE '%" + txtSearch.Text.Trim() + "%'"); break;
                    case "SEMPLOYEENAME": sb.Append(" AND c.SEMPLOYEENAME LIKE '%" + txtSearch.Text.Trim() + "%'"); break;
                    case "SFIRSTNAME": sb.Append(" AND TUSER.SFIRSTNAME LIKE '%" + txtSearch.Text.Trim() + "%'"); break;
                    case "SCOMPLAINFNAME": sb.Append(" AND c.SCOMPLAINFNAME LIKE '%" + txtSearch.Text.Trim() + "%'"); break;
                    case "SVENDORNAME": sb.Append(" AND V.SVENDORNAME LIKE '%" + txtSearch.Text.Trim() + "%'"); break;
                    case "M_GROUPS.NAME": sb.Append(" AND M_GROUPS.NAME LIKE '%" + txtSearch.Text.Trim() + "%'"); break;
                    case "DRIVER": sb.Append(" AND (TEMPLOYEE_SAP.FNAME LIKE '%" + txtSearch.Text.Trim() + "%' OR TEMPLOYEE_SAP.LNAME LIKE '%" + txtSearch.Text.Trim() + "%' OR TEMPLOYEE_SAP.SEMPLOYEEID LIKE '%" + txtSearch.Text.Trim() + "%')"); break;
                    default:
                        break;
                }
            }

            if (cboComplainType.Value != null)
                sb.Append(" AND c.SHEADCOMLIAIN = " + cboComplainType.Value.ToString());

            if (!string.Equals(txtCreateBy.Text.Trim(), string.Empty))
                sb.Append(" AND TUSER.SFIRSTNAME LIKE '%" + txtCreateBy.Text.Trim() + "%'");

            if (chkIsCorrupt.Checked)
                sb.Append(" AND (SELECT COUNT(*) FROM TCOMPLAIN_SCORE_DETAIL INNER JOIN TTOPIC ON TCOMPLAIN_SCORE_DETAIL.STOPICID = TTOPIC.STOPICID WHERE DOC_ID = c.DOC_ID AND TCOMPLAIN_SCORE_DETAIL.ISACTIVE = 1 AND TTOPIC.CACTIVE = 1 AND TTOPIC.IS_CORRUPT = 1) > 0 ");

            sb.Append(" AND to_char(c.DDELIVERY,'yyyyMMdd') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND to_char(c.DDELIVERY,'yyyyMMdd') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'");
            sb.Append(" AND to_char(c.DCREATE,'yyyyMMdd') >= '" + ConfigurationSettings.AppSettings["CUT_OFF_DATE"].ToString() + "'");

            if (Session["CGROUP"] + "" == ConfigValue.UserGroup1.ToString())
            {
                sb.Append(" AND c.SVENDORID = '" + Session["SVDID"].ToString() + "'");
                sb.Append(" AND c.DOC_STATUS_ID IN (" + ConfigValue.DocStatus5 + "," + ConfigValue.DocStatus6 + "," + ConfigValue.DocStatus7 + "," + ConfigValue.DocStatus10 + "," + ConfigValue.DocStatus11 + ")");
            }
            else
            {
                if (chkIsUrgent.Checked)
                    sb.Append(" AND c.IS_URGENT = 1");
                else
                    sb.Append(" AND NVL(c.IS_URGENT, 0) <> 1");
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvComplain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvComplain.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvComplain, dtComplainList);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvComplain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvComplain.DataKeys[e.RowIndex].Value.ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            if (Session["CGROUP"] + "" == ConfigValue.UserGroup1.ToString())
                MsgAlert.OpenForm("admin_Complain_add.aspx?DocID=" + encryptedValue, Page);
            else
                MsgAlert.OpenForm("admin_Complain_add.aspx?DocID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}