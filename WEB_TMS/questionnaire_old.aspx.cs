﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;
using System.IO;
using DevExpress.XtraPrinting;
using TMS_BLL.Transaction.Complain;

public partial class questionnaire : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            Session["sNTYPEVISITFORMID"] = null;
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Session["oSSVENDORID"] = null;
            Session["oNQUESTIONID"] = null;

            DataTable dt = CommonFunction.Get_Data(sql, "SELECT TO_CHAR(DCHECK,'YYYY') AS SYEAR FROM TQUESTIONNAIRE GROUP BY TO_CHAR(DCHECK,'YYYY') ORDER BY TO_CHAR(DCHECK,'YYYY') DESC");

            if (dt.Rows.Count > 0)
            {
                bool checkYear = false;
                string cYear = DateTime.Today.Year.ToString();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int Year = Convert.ToInt32(dt.Rows[i]["SYEAR"]);
                    cmbYear.Items.Add(Year.ToString(), Year);
                    if ("" + dt.Rows[i]["SYEAR"] == cYear)
                    {
                        checkYear = true;
                    }
                }
                if (checkYear == false)
                {
                    cmbYear.Items.Insert(0, new ListEditItem(cYear, cYear));
                }
                cmbYear.SelectedIndex = 0;
            }
            else
            {
                int cYear = DateTime.Today.Year-4;
                for (int i = 0; i <= 8; i++)
                {
                    cmbYear.Items.Insert(i, new ListEditItem((cYear + i).ToString(), (cYear + i))); 
                }
                cmbYear.SelectedIndex = 4;
            }

            int Year1 = Convert.ToInt32(cmbYear.Value);
            string MaxDate1 = DateTime.DaysInMonth(Year1, 12) + "";
            dateStart.Value = "1/1/" + (Year1);
            dateEnd.Value = MaxDate1 + "/12/" + Year1;

            listTrimas();

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();
            gvw.DataBind();

            DataTable dtVendor = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = dtVendor.NewRow();
            dr["SABBREVIATION"] = "";
            dtVendor.Rows.InsertAt(dr, 0);
            cmbVendor.DataSource = dtVendor;
            cmbVendor.DataBind();
            LogUser("28", "R", "เปิดดูข้อมูลหน้า ประเมินสถานประกอบการ", "");
        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

        //lblRecord.Text = gvw.VisibleRowCount + "";
        //lblRecord.Text = ((DataView)sds.Select(DataSourceSelectArguments.Empty)).ToTable().Rows.Count + "";
    }

    //กด แสดงเลข Record
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        //string[] paras = e.Parameter.Split(';');

        //switch (paras[0])
        //{
        //    case "Search":

        //        string MaxDate = "";
        //        int Year = Convert.ToInt32(cmbYear.Value);

        //        if ("" + rblCheck.Value == "1")
        //        {
        //            if ("" + cmbMonth.Value == "t1")
        //            {
        //                MaxDate = DateTime.DaysInMonth(Year, 2) + "";
        //                dateStart.Value = "1/12/" + (Year - 1);
        //                dateEnd.Value = MaxDate + "/2/" + Year;
        //            }
        //            else if ("" + cmbMonth.Value == "t2")
        //            {
        //                MaxDate = DateTime.DaysInMonth(Year, 5) + "";
        //                dateStart.Value = "1/3/" + Year;
        //                dateEnd.Value = MaxDate + "/5/" + Year;
        //            }
        //            else if ("" + cmbMonth.Value == "t3")
        //            {
        //                MaxDate = DateTime.DaysInMonth(Year, 8) + "";
        //                dateStart.Value = "1/6/" + Year;
        //                dateEnd.Value = MaxDate + "/8/" + Year;
        //            }
        //            else if ("" + cmbMonth.Value == "t4")
        //            {
        //                MaxDate = DateTime.DaysInMonth(Year, 11) + "";
        //                dateStart.Value = "1/9/" + Year;
        //                dateEnd.Value = MaxDate + "/11/" + Year;
        //            }
        //            else
        //            {
        //                MaxDate = DateTime.DaysInMonth(Year, 11) + "";
        //                dateStart.Value = "1/12/" + (Year - 1);
        //                dateEnd.Value = MaxDate + "/11/" + Year;
        //            }
        //        }
        //        else
        //        {
        //            switch ("" + cmbMonth.Value)
        //            {
        //                case "1":
        //                    MaxDate = DateTime.DaysInMonth(Year, 1) + "";
        //                    dateStart.Value = "1/1/" + Year;
        //                    dateEnd.Value = MaxDate + "/1/" + Year;

        //                    break;
        //                case "2":
        //                    MaxDate = DateTime.DaysInMonth(Year, 2) + "";
        //                    dateStart.Value = "1/2/" + Year;
        //                    dateEnd.Value = MaxDate + "/2/" + Year;
        //                    break;
        //                case "3":
        //                    MaxDate = DateTime.DaysInMonth(Year, 3) + "";
        //                    dateStart.Value = "1/3/" + Year;
        //                    dateEnd.Value = MaxDate + "/3/" + Year;
        //                    break;
        //                case "4":
        //                    MaxDate = DateTime.DaysInMonth(Year, 4) + "";
        //                    dateStart.Value = "1/4/" + Year;
        //                    dateEnd.Value = MaxDate + "/4/" + Year;
        //                    break;
        //                case "5":
        //                    MaxDate = DateTime.DaysInMonth(Year, 5) + "";
        //                    dateStart.Value = "1/5/" + Year;
        //                    dateEnd.Value = MaxDate + "/5/" + Year;
        //                    break;
        //                case "6":
        //                    MaxDate = DateTime.DaysInMonth(Year, 6) + "";
        //                    dateStart.Value = "1/6/" + Year;
        //                    dateEnd.Value = MaxDate + "/6/" + Year;
        //                    break;
        //                case "7":
        //                    MaxDate = DateTime.DaysInMonth(Year, 7) + "";
        //                    dateStart.Value = "1/7/" + Year;
        //                    dateEnd.Value = MaxDate + "/7/" + Year;
        //                    break;
        //                case "8":
        //                    MaxDate = DateTime.DaysInMonth(Year, 8) + "";
        //                    dateStart.Value = "1/8/" + Year;
        //                    dateEnd.Value = MaxDate + "/8/" + Year;
        //                    break;
        //                case "9":
        //                    MaxDate = DateTime.DaysInMonth(Year, 9) + "";
        //                    dateStart.Value = "1/9/" + Year;
        //                    dateEnd.Value = MaxDate + "/9/" + Year;
        //                    break;
        //                case "10":
        //                    MaxDate = DateTime.DaysInMonth(Year, 10) + "";
        //                    dateStart.Value = "1/10/" + Year;
        //                    dateEnd.Value = MaxDate + "/10/" + Year;
        //                    break;
        //                case "11":
        //                    MaxDate = DateTime.DaysInMonth(Year, 11) + "";
        //                    dateStart.Value = "1/11/" + Year;
        //                    dateEnd.Value = MaxDate + "/11/" + Year;
        //                    break;
        //                case "12":
        //                    MaxDate = DateTime.DaysInMonth(Year, 12) + "";
        //                    dateStart.Value = "1/12/" + Year;
        //                    dateEnd.Value = MaxDate + "/12/" + Year;
        //                    break;
        //                default:
        //                    string MaxDate1 = DateTime.DaysInMonth(Year, 12) + "";
        //                    dateStart.Value = "1/1/" + (Year);
        //                    dateEnd.Value = MaxDate1 + "/12/" + Year;
        //                    break;
        //            }
        //        }

        //        Cache.Remove(sds.CacheKeyDependency);
        //        Cache[sds.CacheKeyDependency] = new object();
        //        sds.Select(new System.Web.UI.DataSourceSelectArguments());
        //        sds.DataBind();

        //        break;

        //    case "edit":

        //        dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SVENDORID", "DCHECK1");

        //        Session["oSSVENDORID"] = data[0] + ";" + data[1];
        //        xcpn.JSProperties["cpRedirectTo"] = "questionnaire_edit.aspx";

        //        break;
        //    case "ViewDetail":
        //        string NNO = e.Parameter.Split(';')[2] + "";

        //        dynamic data1 = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "NQUESTIONNAIREID" + NNO);
        //        string questionID = data1 + "";

        //        if (questionID != "")
        //        {

        //            Session["oNQUESTIONID"] = questionID;
        //            xcpn.JSProperties["cpRedirectTo"] = "questionnaire_detail.aspx";
        //        }
        //        break;

        //}

    }



    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        int VisibleIndex = e.VisibleIndex;
        ASPxButton imbedit = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbedit");
        ASPxButton imbView1 = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbView1");
        //ASPxButton imbView2 = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbView2");


        string NQUESTIONNAIREID1 = "" + e.GetValue("NQUESTIONNAIREID1");
        string NQUESTIONNAIREID2 = "" + e.GetValue("NQUESTIONNAIREID2");
        //if (NQUESTIONNAIREID1 == "" && NQUESTIONNAIREID2 == "")
        //{
        //    imbedit.Visible = false;
        //}

        //if (NQUESTIONNAIREID1 == "") imbView1.ClientVisible = false;

        //if (NQUESTIONNAIREID2 == "") imbView2.ClientVisible = false;
    }

    protected void btnXlsExport_Click(object sender, EventArgs e)
    {
        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;
        //gridExport.ExportToXls("File.xls", options);
        gridExport.WriteXlsToResponse("สรุปผลการประเมินสถานประกอบการ", options);
        //gridExport.WriteXlsToResponse();
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void cmbMonth_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if ("" + rblCheck.Value == "1")
        {
            listTrimas();
        }
        else
        {
            cmbMonth.Items.Clear();
            cmbMonth.Items.Add(new ListEditItem(" -ทั้งหมด- "));
            cmbMonth.SelectedIndex = 0;
            for (int i = 1; i <= 12; i++)
            {
                string Month = DateTimeFormatInfo.GetInstance(new CultureInfo("th-TH")).GetMonthName(i);
                cmbMonth.Items.Add(new ListEditItem("เดือน " + Month, i));
            }
        }
    }

    protected void listTrimas()
    {
        cmbMonth.Items.Clear();
        cmbMonth.Items.Add(new ListEditItem(" -ทั้งหมด- "));
        cmbMonth.SelectedIndex = 0;
        int Year1 = Convert.ToInt32(cmbYear.Value);

        string MaxDate = "";
        string DisplayT = "";
        string ValueT = "";

        int tYear = Year1 + 543;

        for (int i = 1; i <= 4; i++)
        {
            if (i == 1)
            {
                MaxDate = DateTime.DaysInMonth(Year1, 3) + "";
                DisplayT = "ไตรมาสที่ 1 " + "1/1/" + tYear + " ถึง " + MaxDate + "/3/" + tYear;
                ValueT = "t1";
            }
            else if (i == 2)
            {
                MaxDate = DateTime.DaysInMonth(Year1, 6) + "";
                DisplayT = "ไตรมาสที่ 2 " + "1/4/" + tYear + " ถึง " + MaxDate + "/6/" + tYear;
                ValueT = "t2";
            }
            else if (i == 3)
            {
                MaxDate = DateTime.DaysInMonth(Year1, 9) + "";
                DisplayT = "ไตรมาสที่ 3 " + "1/7/" + tYear + " ถึง " + MaxDate + "/9/" + tYear;
                ValueT = "t3";
            }
            else if (i == 4)
            {
                MaxDate = DateTime.DaysInMonth(Year1, 12) + "";
                DisplayT = "ไตรมาสที่ 4 " + "1/10/" + tYear + " ถึง " + MaxDate + "/12/" + tYear;
                ValueT = "t4";
            }
            cmbMonth.Items.Add(new ListEditItem(DisplayT, ValueT));

        }
    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string[] Para = e.Args[0].Split(';');
                switch (Para[0])
                {
                    case "Search":

                        string MaxDate = "";
                        int Year = Convert.ToInt32(cmbYear.Value);

                        if ("" + rblCheck.Value == "1")
                        {
                            if ("" + cmbMonth.Value == "t1")
                            {
                                MaxDate = DateTime.DaysInMonth(Year, 3) + "";
                                dateStart.Value = "1/1/" + (Year);
                                dateEnd.Value = MaxDate + "/3/" + Year;
                                Session["dateStart"] = "1/1/" + Year;
                                Session["dateEnd"] = MaxDate + "/3/" + Year;
                            }
                            else if ("" + cmbMonth.Value == "t2")
                            {
                                MaxDate = DateTime.DaysInMonth(Year, 6) + "";
                                dateStart.Value = "1/4/" + Year;
                                dateEnd.Value = MaxDate + "/6/" + Year;
                                Session["dateStart"] = "1/4/" + Year;
                                Session["dateEnd"] = MaxDate + "/6/" + Year;
                            }
                            else if ("" + cmbMonth.Value == "t3")
                            {
                                MaxDate = DateTime.DaysInMonth(Year, 9) + "";
                                dateStart.Value = "1/7/" + Year;
                                dateEnd.Value = MaxDate + "/9/" + Year;
                                Session["dateStart"] = "1/7/" + Year;
                                Session["dateEnd"] = MaxDate + "/9/" + Year;
                            }
                            else if ("" + cmbMonth.Value == "t4")
                            {
                                MaxDate = DateTime.DaysInMonth(Year, 12) + "";
                                dateStart.Value = "1/10/" + Year;
                                dateEnd.Value = MaxDate + "/12/" + Year;
                                Session["dateStart"] = "1/10/" + Year;
                                Session["dateEnd"] = MaxDate + "/12/" + Year;
                            }
                            else
                            {
                                MaxDate = DateTime.DaysInMonth(Year, 11) + "";
                                dateStart.Value = "1/1/" + Year;
                                dateEnd.Value = MaxDate + "/12/" + Year;
                                Session["dateStart"] = "1/1/" + Year;
                                Session["dateEnd"] = MaxDate + "/12/" + Year;
                            }
                        }
                        else
                        {
                            switch ("" + cmbMonth.Value)
                            {
                                case "1":
                                    MaxDate = DateTime.DaysInMonth(Year, 1) + "";
                                    dateStart.Value = "1/1/" + Year;
                                    dateEnd.Value = MaxDate + "/1/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;

                                    break;
                                case "2":
                                    MaxDate = DateTime.DaysInMonth(Year, 2) + "";
                                    dateStart.Value = "1/2/" + Year;
                                    dateEnd.Value = MaxDate + "/2/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "3":
                                    MaxDate = DateTime.DaysInMonth(Year, 3) + "";
                                    dateStart.Value = "1/3/" + Year;
                                    dateEnd.Value = MaxDate + "/3/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "4":
                                    MaxDate = DateTime.DaysInMonth(Year, 4) + "";
                                    dateStart.Value = "1/4/" + Year;
                                    dateEnd.Value = MaxDate + "/4/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "5":
                                    MaxDate = DateTime.DaysInMonth(Year, 5) + "";
                                    dateStart.Value = "1/5/" + Year;
                                    dateEnd.Value = MaxDate + "/5/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "6":
                                    MaxDate = DateTime.DaysInMonth(Year, 6) + "";
                                    dateStart.Value = "1/6/" + Year;
                                    dateEnd.Value = MaxDate + "/6/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "7":
                                    MaxDate = DateTime.DaysInMonth(Year, 7) + "";
                                    dateStart.Value = "1/7/" + Year;
                                    dateEnd.Value = MaxDate + "/7/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "8":
                                    MaxDate = DateTime.DaysInMonth(Year, 8) + "";
                                    dateStart.Value = "1/8/" + Year;
                                    dateEnd.Value = MaxDate + "/8/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "9":
                                    MaxDate = DateTime.DaysInMonth(Year, 9) + "";
                                    dateStart.Value = "1/9/" + Year;
                                    dateEnd.Value = MaxDate + "/9/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "10":
                                    MaxDate = DateTime.DaysInMonth(Year, 10) + "";
                                    dateStart.Value = "1/10/" + Year;
                                    dateEnd.Value = MaxDate + "/10/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "11":
                                    MaxDate = DateTime.DaysInMonth(Year, 11) + "";
                                    dateStart.Value = "1/11/" + Year;
                                    dateEnd.Value = MaxDate + "/11/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                case "12":
                                    MaxDate = DateTime.DaysInMonth(Year, 12) + "";
                                    dateStart.Value = "1/12/" + Year;
                                    dateEnd.Value = MaxDate + "/12/" + Year;
                                    Session["dateStart"] = "1/" + cmbMonth.Value + "/" + Year;
                                    Session["dateEnd"] = MaxDate + "/" + cmbMonth.Value + "/" + Year;
                                    break;
                                default:
                                    string MaxDate1 = DateTime.DaysInMonth(Year, 12) + "";
                                    dateStart.Value = "1/1/" + (Year);
                                    dateEnd.Value = MaxDate1 + "/12/" + Year;
                                    Session["dateStart"] = "1/1/" + Year;
                                    Session["dateEnd"] = MaxDate + "/12/" + Year;
                                    break;
                            }
                        }

                        Cache.Remove(sds.CacheKeyDependency);
                        Cache[sds.CacheKeyDependency] = new object();
                        sds.Select(new System.Web.UI.DataSourceSelectArguments());
                        sds.DataBind();
                        gvw.DataBind();

                        break;

                    case "edit":

                        dynamic data = gvw.GetRowValues(int.Parse(Para[1]), "SVENDORID", "DCHECK1", "NNOMAX");
                        Session["NQUESTIONNAIREID"] = gvw.GetRowValues(int.Parse(Para[1]), "NQUESTIONNAIREID1").ToString();
                        Session["oSSVENDORID"] = data[0] + ";" + data[1] + ";" + data[2];
                        gvw.JSProperties["cpRedirectTo"] = "questionnaire_form.aspx";

                        break;
                    case "ViewDetail":
                        string NNO = Para[2] + "";

                        dynamic data1 = gvw.GetRowValues(int.Parse(Para[1]), "NQUESTIONNAIREID" + NNO);
                        string questionID = data1 + "";

                        if (questionID != "")
                        {

                            Session["oNQUESTIONID"] = questionID;
                            gvw.JSProperties["cpRedirectTo"] = "questionnaire_detail.aspx";
                        }
                        break;
                }
                break;
        }

    }
}
