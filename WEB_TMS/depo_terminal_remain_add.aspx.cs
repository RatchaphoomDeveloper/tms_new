﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;

public partial class depo_terminal_remain_add : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblDate.Text = DateTime.Now.ToString("dd/MM/yyyy เวลา HH:mm น."); ;

        if (!IsPostBack)
        {
            bool chkurl = false;
            Session["CheckPermission"] = null;
            string AddEdit = "";

            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "16")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }


            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            Session["CheckPermission"] = AddEdit;

            txtTerminal.Text = CommonFunction.Get_Value(sql, "SELECT STERMINALNAME FROM TTERMINAL_SAP WHERE STERMINALID = '" + Session["SVDID"] + "" + "'");
            dteBan.Value = DateTime.Now;
            dteUnBan.Value = DateTime.Now;

            if (Session["NCARBANID"] != null)
            {
                sdsCarBanCause.DataBind();
                chkBanCause.DataBind();
                listData();
            }
            else
            {
                SetDetailFromQuaryString();
            }

        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {
        Session["NCARBANID"] = null;
        txtTrailerRegist.Text = "";
        cboVendor.Value = "";
        cboHeadRegist.Value = "";
        dteBan.Value = DateTime.Now;
        dteUnBan.Value = DateTime.Now;
        txtRemark.Text = "";
        txtTerminal.Text = "";
    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {


            case "Save":
                #region save
                if ("" + Session["CheckPermission"] == "1")
                {
                    if (Session["NCARBANID"] == null)
                    {
                        string GENID;
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            GENID = CommonFunction.Gen_ID(con, "SELECT NCARBANID FROM (SELECT NCARBANID FROM TCARBAN ORDER BY NCARBANID DESC)  WHERE ROWNUM <= 1");
                            string strsql = "INSERT INTO TCARBAN(SOTHERCAUSE,NCARBANID,SVENDORID,SOUTBOUNDNO,DBAN,SHEADREGISTERNO,STRAILERREGISTERNO,SREMARK,CUNBAN,DUNBAN,STIMEUNBAN,SCREATE,DCREATE,SUPDATE,DUPDATE,STERMINALID,SEMPLOYEEID) VALUES (:SOTHERCAUSE,:NCARBANID,:SVENDORID,:SOUTBOUNDNO,:DBAN,:SHEADREGISTERNO,:STRAILERREGISTERNO,:SREMARK,:CUNBAN,:DUNBAN,:STIMEUNBAN,:SCREATE,SYSDATE,:SUPDATE,SYSDATE,:STERMINALID,:SEMPLOYEEID)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NCARBANID", OracleType.Number).Value = Convert.ToInt32(GENID);
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                                com.Parameters.Add(":SOUTBOUNDNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                com.Parameters.Add(":DBAN", OracleType.DateTime).Value = dteBan.Value;
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value;
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerRegist.Text;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text;
                                com.Parameters.Add(":CUNBAN", OracleType.Char).Value = (chkUnBan.Checked == true) ? '1' : '0';
                                com.Parameters.Add(":DUNBAN", OracleType.DateTime).Value = dteUnBan.Value;
                                com.Parameters.Add(":STIMEUNBAN", OracleType.VarChar).Value = teUnBan.Text;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = Session["SVDID"] + "";
                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value + "";
                                com.Parameters.Add(":SOTHERCAUSE", OracleType.VarChar).Value = txtOtherCause.Text;
                                com.ExecuteNonQuery();
                            }

                            string strsql1 = "INSERT INTO TCARBANLIST(NCARBANLISTID,NCARBANID,NCARBANCAUSEID) VALUES(:NCARBANLISTID,:NCARBANID,:NCARBANCAUSEID)";

                            foreach (ListEditItem n in chkBanCause.Items)
                            {
                                if (n.Selected)
                                {
                                    string GENLISTID = CommonFunction.Gen_ID(con, "SELECT NCARBANLISTID FROM (SELECT NCARBANLISTID FROM TCARBANLIST ORDER BY NCARBANLISTID DESC)  WHERE ROWNUM <= 1");
                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NCARBANLISTID", OracleType.Number).Value = Convert.ToInt32(GENLISTID);
                                        com1.Parameters.Add(":NCARBANID", OracleType.Number).Value = Convert.ToInt32(GENID);
                                        com1.Parameters.Add(":NCARBANCAUSEID", OracleType.Number).Value = n.Value;
                                        com1.ExecuteNonQuery();
                                    }
                                }
                            }

                            string qry_upd = @"UPDATE TTRUCK SET CHOLD='" + (chkUnBan.Checked ? "0" : "2") + @"' 
WHERE NVL(CACTIVE,'N')='Y' 
AND REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SHEADREGISTERNO ,'(','') ,')','') ,'.','') ,'-','')  ,',','')= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('" + cboHeadRegist.Value + @"','(','') ,')','') ,'.','') ,'-','')  ,',','') AND nvl(CHOLD,'0') != '1'";


                            // อัพเดทสถานะห้ามวิ่งในTTRUCK
                            using (OracleCommand cmd_upd = new OracleCommand(qry_upd, con))
                            {
                                cmd_upd.ExecuteNonQuery();

                                if (!chkUnBan.Checked)
                                {
                                    AlertTruckOnHold onHold = new AlertTruckOnHold(Page, con);
                                    onHold.VehNo = cboHeadRegist.Value + "";
                                    onHold.TUNo = txtTrailerRegist.Text;
                                    onHold.Subject = "การตรวจสภาพรถ/ละเลยการแก้สภาพรถเกินตามที่กำหนดหรือโดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                                    onHold.SendTo();
                                }

                            }
                        }

                        LogUser("16", "I", "บันทึกข้อมูลหน้า รถตกค้างคลังปลายทาง", GENID + "");

                    }
                    else
                    {

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = "UPDATE TCARBAN SET SOTHERCAUSE = :SOTHERCAUSE, SVENDORID = :SVENDORID,SOUTBOUNDNO = :SOUTBOUNDNO,DBAN = :DBAN,SHEADREGISTERNO = :SHEADREGISTERNO,STRAILERREGISTERNO = :STRAILERREGISTERNO,SREMARK = :SREMARK,CUNBAN = :CUNBAN,DUNBAN = :DUNBAN,STIMEUNBAN = :STIMEUNBAN,SUPDATE = :SUPDATE,DUPDATE = SYSDATE,STERMINALID = :STERMINALID, SEMPLOYEEID = :SEMPLOYEEID WHERE NCARBANID = :NCARBANID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NCARBANID", OracleType.Number).Value = Session["NCARBANID"] + "";
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                                com.Parameters.Add(":SOUTBOUNDNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                com.Parameters.Add(":DBAN", OracleType.DateTime).Value = dteBan.Value;
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value;
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerRegist.Text;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text;
                                com.Parameters.Add(":CUNBAN", OracleType.Char).Value = (chkUnBan.Checked == true) ? '1' : '0';
                                com.Parameters.Add(":DUNBAN", OracleType.DateTime).Value = dteUnBan.Value;
                                com.Parameters.Add(":STIMEUNBAN", OracleType.VarChar).Value = teUnBan.Text;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = Session["SVDID"] + "";
                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value + "";
                                com.Parameters.Add(":SOTHERCAUSE", OracleType.VarChar).Value = txtOtherCause.Text;
                                com.ExecuteNonQuery();
                            }


                            using (OracleCommand com1 = new OracleCommand("DELETE FROM TCARBANLIST WHERE NCARBANID = :NCARBANID", con))
                            {
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":NCARBANID", OracleType.Number).Value = Session["NCARBANID"] + "";
                                com1.ExecuteNonQuery();
                            }

                            string strsql1 = "INSERT INTO TCARBANLIST(NCARBANLISTID,NCARBANID,NCARBANCAUSEID) VALUES(:NCARBANLISTID,:NCARBANID,:NCARBANCAUSEID)";

                            foreach (ListEditItem n in chkBanCause.Items)
                            {
                                if (n.Selected)
                                {
                                    string GENLISTID = CommonFunction.Gen_ID(con, "SELECT NCARBANLISTID FROM (SELECT NCARBANLISTID FROM TCARBANLIST ORDER BY NCARBANLISTID DESC)  WHERE ROWNUM <= 1");
                                    using (OracleCommand com2 = new OracleCommand(strsql1, con))
                                    {
                                        com2.Parameters.Clear();
                                        com2.Parameters.Add(":NCARBANLISTID", OracleType.Number).Value = Convert.ToInt32(GENLISTID);
                                        com2.Parameters.Add(":NCARBANID", OracleType.Number).Value = Session["NCARBANID"] + "";
                                        com2.Parameters.Add(":NCARBANCAUSEID", OracleType.Number).Value = n.Value;
                                        com2.ExecuteNonQuery();
                                    }
                                }
                            }
                            string qry_upd = @"UPDATE TTRUCK SET CHOLD='" + (chkUnBan.Checked ? "0" : "2") + @"'  WHERE NVL(CACTIVE,'N')='Y' AND SHEADREGISTERNO='" + cboHeadRegist.Value + @"' AND nvl(CHOLD,'0') != '1'";
                            //qry_upd += (txtTrailerRegist.Text != "") ? " AND REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(NVL(STRAILERREGISTERNO,'xx.nn-nnnn') ,'(','') ,')','') ,'.','') ,'-','')  ,',','')=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE( '" + ((txtTrailerRegist.Text != "") ? txtTrailerRegist.Text : "xx.nn-nnnn") + "' ,'(','') ,')','') ,'.','') ,'-','')  ,',','') " : "";
                            //อัพเดทสถานะห้ามวิ่งในTTRUCK
                            using (OracleCommand cmd_upd = new OracleCommand(qry_upd, con))
                            {
                                //cmd_upd.CommandType = CommandType.Text;
                                //cmd_upd.CommandText = qry_upd;
                                cmd_upd.ExecuteNonQuery();

                                if (!chkUnBan.Checked)
                                {
                                    AlertTruckOnHold onHold = new AlertTruckOnHold(Page, con);
                                    onHold.VehNo = cboHeadRegist.Value + "";
                                    onHold.TUNo = txtTrailerRegist.Text;
                                    onHold.Subject = "การตรวจสภาพรถ/ละเลยการแก้สภาพรถเกินตามที่กำหนดหรือโดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                                    onHold.SendTo();
                                }
                            }
                        }

                        LogUser("16", "E", "แก้ไขข้อมูลหน้า", Session["NCARBANID"] + "");
                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='depo_terminal_remain_lst.aspx';});");

                    ClearControl();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                #endregion
                break;
            case "SelectChange":
                if (cboHeadRegist.Value != "") cboVendor.SelectedIndex = 0;
                break;

        }

    }


    void listData()
    {

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT SOTHERCAUSE, SOUTBOUNDNO,DBAN,SHEADREGISTERNO,STRAILERREGISTERNO,SREMARK,CUNBAN,DUNBAN,STIMEUNBAN,DCREATE,STERMINALID,SVENDORID,SEMPLOYEEID from TCARBAN WHERE NCARBANID = '" + Session["NCARBANID"] + "" + "'");
        if (dt.Rows.Count > 0)
        {
            cboDelivery.Value = dt.Rows[0]["SOUTBOUNDNO"] + "";
            dteBan.Date = Convert.ToDateTime(dt.Rows[0]["DBAN"] + "");
            cboHeadRegist.Value = dt.Rows[0]["SHEADREGISTERNO"] + "";
            txtTrailerRegist.Text = dt.Rows[0]["STRAILERREGISTERNO"] + "";
            txtRemark.Text = dt.Rows[0]["SREMARK"] + "";
            chkUnBan.Checked = ("" + dt.Rows[0]["CUNBAN"] == "1") ? true : false;
            dteUnBan.Date = Convert.ToDateTime(dt.Rows[0]["DUNBAN"] + "");
            teUnBan.Text = dt.Rows[0]["STIMEUNBAN"] + "";
            lblDate.Text = Convert.ToDateTime(dt.Rows[0]["DCREATE"] + "").ToString("dd/MM/yyyy เวลา HH:mm น.");
            cboVendor.Value = dt.Rows[0]["SVENDORID"] + "";
            cmbPersonalNo.Value = dt.Rows[0]["SEMPLOYEEID"] + "";
            txtOtherCause.Text = dt.Rows[0]["SOTHERCAUSE"] + "";

            DataTable dt1 = new DataTable();
            dt1 = CommonFunction.Get_Data(sql, "SELECT NCARBANCAUSEID from TCARBANLIST WHERE NCARBANID = '" + Session["NCARBANID"] + "" + "'");

            if (dt1.Rows.Count > 0)
            {

                foreach (DataRow o in dt1.Rows)
                {
                    int value = Convert.ToInt32(o["NCARBANCAUSEID"] + "");
                    ListEditItem item = chkBanCause.Items.FindByValue(value);
                    if (item != null)
                        item.Selected = true;

                }
            }
        }

    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID,SCONTRACTID FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID FROM (TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID AND nvl(CT.STRAILERID,'1') = nvl(T.STRAILERID,'1') 
WHERE t.SHEADREGISTERNO LIKE :fillter AND c.SVENDORID LIKE :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }
    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM ((((TVENDOR_SAP v LEFT JOIN TCONTRACT c ON V.SVENDORID = C.SVENDORID )LEFT JOIN TCONTRACTTYPE ct ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID) 
LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID)LEFT JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID)
WHERE v.SVENDORID || V.SVENDORNAME  like :fillter AND T.SHEADREGISTERNO  LIKE :fillter2
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", cboHeadRegist.Value + ""));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID, FULLNAME FROM (SELECT E.SEMPLOYEEID,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND E.SPERSONELNO LIKE :fillter AND e.STRANS_ID = :SVENDORID) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsPersonal.SelectParameters.Add("SVENDORID", TypeCode.String, cboVendor.Value + "");
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SDELIVERYNO, SHEADREGISTERNO,SVENDORID,STRAILERREGISTERNO,SVENDORNAME FROM(SELECT pl.SDELIVERYNO , P.SHEADREGISTERNO,P.SVENDORID,p.STRAILERREGISTERNO,vs.SVENDORNAME , ROW_NUMBER()OVER(ORDER BY p.DDELIVERY DESC) AS RN 
FROM (((TPlanScheduleList pl  INNER JOIN TPLANSCHEDULE p ON PL.NPLANID = P.NPLANID) LEFT JOIN TVENDOR_SAP vs ON p.SVENDORID = vs.SVENDORID)
LEFT JOIN TDELIVERY d ON PL.SDELIVERYNO = D.DELIVERY_NO) 
LEFT JOIN TCUSTOMER ct ON D.SHIP_TO = CT.SHIP_TO  
WHERE 
pl.CACTIVE = '1' AND p.CACTIVE = '1' AND
PL.SDELIVERYNO LIKE :fillter 
AND P.SVENDORID LIKE :fillter1 
AND P.SHEADREGISTERNO LIKE :fillter2 
AND NVL(ct.DEPO_ID,:fillter3 ) LIKE  NVL( CASE WHEN Length(:fillter3)>7 THEN NULL ELSE :fillter3 END,'%%')
) 
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsTruck.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", cboHeadRegist.Value + ""));
        sdsTruck.SelectParameters.Add("fillter3", TypeCode.String, String.Format("%{0}%", Session["SVDID"] + ""));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

        if (comboBox.Items.Count <= 0)
        {
            comboBox.Value = "";
        }

    }
    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    private void SetDetailFromQuaryString()
    {
        string str = Request.QueryString["str"];
        string[] strQuery;
        if (!string.IsNullOrEmpty(str))
        {
            DateTime date;
            strQuery = STCrypt.DecryptURL(str);
            cboDelivery.Value = strQuery[1] + "";
            cboVendor.Value = strQuery[10] + "";
            cmbPersonalNo.Value = strQuery[8] + "";
            cboHeadRegist.Value = strQuery[3] + "";
            txtTrailerRegist.Text = strQuery[4] + "";

        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}