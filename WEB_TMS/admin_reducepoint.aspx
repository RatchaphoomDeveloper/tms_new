﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="admin_reducepoint.aspx.cs" Inherits="admin_reducepoint" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
    <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
    <div id="dv_searchbar">
        <table width="100%">
            <tr>
                <td>
                </td>
                <td align="right">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <dx:ASPxTextBox ID="txtKeyWord" runat="server" ClientInstanceName="txtKeyWord" Width="250px"
                                    NullText="เลขที่สัญญา,รหัส-ชื่อผู้ขนส่ง,ทะเบียนหัว,ทะเบียนหาง">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dteStart" runat="server" ClientInstanceName="dteStart" CssClass="dxeLineBreakFix"
                                    SkinID="xdte" NullText="ระหว่างวันที่">
                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                        <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                    </ValidationSettings>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dteEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                                    SkinID="xdte" NullText="ถึงวันที่">
                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                        <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                    </ValidationSettings>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxComboBox ID="cbbProcessType" runat="server" ClientInstanceName="cbbProcessType">
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <dx:ASPxButton ID="btnSearch" runat="server" ClientInstanceName="btnSearch" Text=" ค้นหารายการ "
                        OnClick="btnSearch_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </div>
    <div id="dv_Result">
        <dx:ASPxGridView ID="gvw" ClientInstanceName="gvw" runat="server" Width="99%" SkinID="_gvw"
            DataSourceID="sdsReducePoint" KeyFieldName="NREDUCEID" AutoGenerateColumns="False"> 
            <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                    <HeaderTemplate>
                        <dx:ASPxCheckBox ID="cbxSelectAll" runat="server" ToolTip="Select/Unselect all rows on the page"
                            ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                        </dx:ASPxCheckBox>
                    </HeaderTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="ลำดับที่" Width="1%">
                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="ประเภทความผิด" FieldName="SPROCESSID" />
                <dx:GridViewDataTextColumn Caption="วันที่กระทำความผิด" FieldName="DREDUCE" />
            </Columns>
            <SettingsPager PageSize="100">
            </SettingsPager>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="sdsReducePoint" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
            CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
            SelectCommand="SELECT RDP.NREDUCEID ,RDP.DREDUCE ,RDP.SREDUCETYPE ,RDP.SPROCESSID ,RDP.SCHECKLISTID ,RDP.STOPICID 
,RDP.SVERSIONLIST ,RDP.SREDUCENAME ,RDP.NPOINT ,RDP.SREDUCEBY ,RDP.CACTIVE ,RDP.SREFERENCEID
,RDP.SCONTRACTID ,RDP.SHEADID ,RDP.SHEADREGISTERNO ,RDP.STRAILERREGISTERNO ,RDP.SDRIVERNO ,RDP.STRAILERID ,RDP.STYPECHECKLISTID
FROM TREDUCEPOINT RDP
LEFT JOIN TCONTRACT CONT ON RDP.SCONTRACTID=CONT.SCONTRACTID
LEFT JOIN TEMPLOYEE EPYE ON NVL(RDP.SDRIVERNO,'NULLTEXT') = NVL(EPYE.SEMPLOYEEID,'NULLTEXT')
LEFT JOIN TEMPLOYEE_SAP EPYE_SAP ON NVL(EPYE.SEMPLOYEEID,'NULLTEXT') = NVL(EPYE_SAP.SEMPLOYEEID,'NULLTEXT')
WHERE TO_DATE(DREDUCE,'DD/MM/YYYY') &gt;=TO_DATE( :D_START,'DD/MM/YYYY') AND TO_DATE( DREDUCE,'DD/MM/YYYY') &lt;=TO_DATE( :D_END,'DD/MM/YYYY')  
AND CONT.SCONTRACTNO||' '||RDP.SHEADREGISTERNO ||' '||RDP.STRAILERREGISTERNO||' '||RDP.SDRIVERNO ||' '||EPYE.SDRIVERNO||' '||EPYE.SDRIVERNO||' '||EPYE_SAP.FNAME||' '|| EPYE_SAP.LNAME ||' '||EPYE.SPERSONELNO LIKE '%'|| :S_KEYWORD ||'%'
order by DREDUCE desc">
            <SelectParameters>
                <asp:ControlParameter Name=":S_KEYWORD" ControlID="txtKeyWord" PropertyName="Text"
                    DbType="String" />
                <asp:ControlParameter Name=":D_START" ControlID="dteStart" PropertyName="Value" DbType="DateTime" />
                <asp:ControlParameter Name=":D_END" ControlID="dteEnd" PropertyName="Value" DbType="DateTime" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
