﻿using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.IO;
using GemBox.Spreadsheet;
using System.Drawing;
using System.Text;
using System.Web.Security;

public partial class Quartery_Report : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            initalForm();
            //GetDataGridView();
            this.AssignAuthen();
        }
    }

    #region Loaddata
    private void initalForm()
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref ddlYear, SetYear(), "value", "text", false);
            DataTable dt_vendor = VendorBLL.Instance.SelectName(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);

            DataTable dt = QuarterReportBLL.Instance.GetSPROCESSID();
            chkaccident.DataSource = dt;
            chkaccident.DataTextField = "SPROCESSNAME";
            chkaccident.DataValueField = "SPROCESSID";

            chkaccident.DataBind();
            foreach (ListItem item in chkaccident.Items)
            {
                item.Selected = true;
            }
            GetDataGridView();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private DataTable GetData()
    {
        try
        {
            if (!string.Equals(ddlQuerter.SelectedValue, string.Empty))
            {
                switch (ddlQuerter.SelectedValue)
                {
                    case "1":
                        gvw.Columns["COL_QUARTER1"].Visible = true;
                        gvw.Columns["COL_QUARTER2"].Visible = false;
                        gvw.Columns["COL_QUARTER3"].Visible = false;
                        gvw.Columns["COL_QUARTER4"].Visible = false;
                        break;
                    case "2":
                        gvw.Columns["COL_QUARTER2"].Visible = true;
                        gvw.Columns["COL_QUARTER1"].Visible = false;
                        gvw.Columns["COL_QUARTER3"].Visible = false;
                        gvw.Columns["COL_QUARTER4"].Visible = false;
                        break;
                    case "3":
                        gvw.Columns["COL_QUARTER3"].Visible = true;
                        gvw.Columns["COL_QUARTER1"].Visible = false;
                        gvw.Columns["COL_QUARTER2"].Visible = false;
                        gvw.Columns["COL_QUARTER4"].Visible = false;
                        break;
                    case "4":
                        gvw.Columns["COL_QUARTER4"].Visible = true;
                        gvw.Columns["COL_QUARTER1"].Visible = false;
                        gvw.Columns["COL_QUARTER2"].Visible = false;
                        gvw.Columns["COL_QUARTER3"].Visible = false;
                        break;
                    default:
                        gvw.Columns["COL_QUARTER1"].Visible = true;
                        gvw.Columns["COL_QUARTER2"].Visible = true;
                        gvw.Columns["COL_QUARTER3"].Visible = true;
                        gvw.Columns["COL_QUARTER4"].Visible = true;
                        break;
                }
            }
            //checkมีเลือก checkboxไหม

            string selectedValue = "";
            foreach (ListItem item in chkaccident.Items)
            {
                if (item.Selected)
                {
                    selectedValue += ",'" + item.Value + "'";
                }
            }
            if (!string.IsNullOrEmpty(selectedValue))
                selectedValue = selectedValue.Substring(1);
            else
            {
                selectedValue = "''";
            }
            string I_SPROCESSID = selectedValue;
            DataTable dt = QuarterReportBLL.Instance.GetData(ddlYear.SelectedValue, ddlVendor.SelectedValue, I_SPROCESSID, 0);
            foreach (DataRow item in dt.Rows)
            {
                decimal total1 = 0, total2 = 0, total3 = 0, total4 = 0;
                decimal value1 = 0, value2 = 0, value3 = 0, value4 = 0, value5 = 0, value6 = 0, value7 = 0, value8 = 0, value9 = 0, value10 = 0, value11 = 0, value12 = 0;

                if (decimal.Parse(item["JAN"].ToString()) > -1)
                    total1 = 1 + total1;
                if (decimal.Parse(item["FEB"].ToString()) > -1)
                    total1 = 1 + total1;
                if (decimal.Parse(item["MAR"].ToString()) > -1)
                    total1 = 1 + total1;
                if (decimal.Parse(item["JAN"].ToString()) == -1)
                    value1 = 0;
                else
                    value1 = decimal.Parse(item["JAN"].ToString());
                if (decimal.Parse(item["FEB"].ToString()) == -1)
                    value2 = 0;
                else
                    value2 = decimal.Parse(item["FEB"].ToString());
                if (decimal.Parse(item["MAR"].ToString()) == -1)
                    value3 = 0;
                else
                    value3 = decimal.Parse(item["MAR"].ToString());
                if (total1 > 0)
                    item["QUARTER1"] = Math.Round(((decimal)((value1 + value2 + value3) / total1)), 0, MidpointRounding.AwayFromZero);
                else
                    item["QUARTER1"] = Math.Round(((decimal)((value1 + value2 + value3))), 0, MidpointRounding.AwayFromZero);
                if (decimal.Parse(item["APR"].ToString()) > -1)
                    total2 = 1 + total2;
                if (decimal.Parse(item["MAY"].ToString()) > -1)
                    total2 = 1 + total2;
                if (decimal.Parse(item["JUN"].ToString()) > -1)
                    total2 = 1 + total2;
                if (decimal.Parse(item["APR"].ToString()) == -1)
                    value4 = 0;
                else
                    value4 = decimal.Parse(item["APR"].ToString());
                if (decimal.Parse(item["MAY"].ToString()) == -1)
                    value5 = 0;
                else
                    value5 = decimal.Parse(item["MAY"].ToString());
                if (decimal.Parse(item["JUN"].ToString()) == -1)
                    value6 = 0;
                else
                    value6 = decimal.Parse(item["JUN"].ToString());
                if (total2 > 0)
                    item["QUARTER2"] = Math.Round(((decimal)((value4 + value5 + value6) / total2)), 0, MidpointRounding.AwayFromZero);
                else
                    item["QUARTER2"] = Math.Round(((decimal)((value4 + value5 + value6))), 0, MidpointRounding.AwayFromZero);
                if (decimal.Parse(item["JUL"].ToString()) > -1)
                    total3 = 1 + total3;
                if (decimal.Parse(item["AUG"].ToString()) > -1)
                    total3 = 1 + total3;
                if (decimal.Parse(item["SEP"].ToString()) > -1)
                    total3 = 1 + total3;
                if (decimal.Parse(item["JUL"].ToString()) == -1)
                    value7 = 0;
                else
                    value7 = decimal.Parse(item["JUL"].ToString());
                if (decimal.Parse(item["AUG"].ToString()) == -1)
                    value8 = 0;
                else
                    value8 = decimal.Parse(item["AUG"].ToString());
                if (decimal.Parse(item["SEP"].ToString()) == -1)
                    value9 = 0;
                else
                    value9 = decimal.Parse(item["SEP"].ToString());
                if (total3 > 0)
                    item["QUARTER3"] = Math.Round(((decimal)((value7 + value8 + value9) / total3)), 0, MidpointRounding.AwayFromZero);
                else
                    item["QUARTER3"] = Math.Round(((decimal)((value7 + value8 + value9))), 0, MidpointRounding.AwayFromZero);
                if (decimal.Parse(item["OCT"].ToString()) > -1)
                    total4 = 1 + total4;
                if (decimal.Parse(item["NOV"].ToString()) > -1)
                    total4 = 1 + total4;
                if (decimal.Parse(item["DEC"].ToString()) > -1)
                    total4 = 1 + total4;
                if (decimal.Parse(item["OCT"].ToString()) == -1)
                    value10 = 0;
                else
                    value10 = decimal.Parse(item["OCT"].ToString());
                if (decimal.Parse(item["NOV"].ToString()) == -1)
                    value11 = 0;
                else
                    value11 = decimal.Parse(item["NOV"].ToString());
                if (decimal.Parse(item["DEC"].ToString()) == -1)
                    value12 = 0;
                else
                    value12 = decimal.Parse(item["DEC"].ToString());
                if (total4 > 0)
                    item["QUARTER4"] = Math.Round(((decimal)((value10 + value11 + value12) / total4)), 0, MidpointRounding.AwayFromZero);
                else
                    item["QUARTER4"] = Math.Round(((decimal)((value10 + value11 + value12))), 0, MidpointRounding.AwayFromZero);

                if (decimal.Parse(item["JAN"].ToString()) == -1 && decimal.Parse(item["FEB"].ToString()) == -1 && decimal.Parse(item["MAR"].ToString()) == -1)
                    item["QUARTER1"] = DBNull.Value;
                if (decimal.Parse(item["APR"].ToString()) == -1 && decimal.Parse(item["MAY"].ToString()) == -1 && decimal.Parse(item["JUN"].ToString()) == -1)
                    item["QUARTER2"] = DBNull.Value;
                if (decimal.Parse(item["JUL"].ToString()) == -1 && decimal.Parse(item["AUG"].ToString()) == -1 && decimal.Parse(item["SEP"].ToString()) == -1)
                    item["QUARTER3"] = DBNull.Value;
                if (decimal.Parse(item["OCT"].ToString()) == -1 && decimal.Parse(item["NOV"].ToString()) == -1 && decimal.Parse(item["DEC"].ToString()) == -1)
                    item["QUARTER4"] = DBNull.Value;

                if (decimal.Parse(item["JAN"].ToString()) < 0)
                    item["JAN"] = DBNull.Value;
                if (decimal.Parse(item["FEB"].ToString()) < 0)
                    item["FEB"] = DBNull.Value;
                if (decimal.Parse(item["MAR"].ToString()) < 0)
                    item["MAR"] = DBNull.Value;

                if (decimal.Parse(item["APR"].ToString()) < 0)
                    item["APR"] = DBNull.Value;
                if (decimal.Parse(item["MAY"].ToString()) < 0)
                    item["MAY"] = DBNull.Value;
                if (decimal.Parse(item["JUN"].ToString()) < 0)
                    item["JUN"] = DBNull.Value;

                if (decimal.Parse(item["JUL"].ToString()) < 0)
                    item["JUL"] = DBNull.Value;
                if (decimal.Parse(item["AUG"].ToString()) < 0)
                    item["AUG"] = DBNull.Value;
                if (decimal.Parse(item["SEP"].ToString()) < 0)
                    item["SEP"] = DBNull.Value;

                if (decimal.Parse(item["OCT"].ToString()) < 0)
                    item["OCT"] = DBNull.Value;
                if (decimal.Parse(item["NOV"].ToString()) < 0)
                    item["NOV"] = DBNull.Value;
                if (decimal.Parse(item["DEC"].ToString()) < 0)
                    item["DEC"] = DBNull.Value;
            }

            return dt;

            //Grid.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(grid_HtmlRowCreated);
            //Grid.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void GetDataGridView()
    {
        try
        {
            gvw.DataSource = this.GetData();
            gvw.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private DataTable SetYear()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("value");
            dt.Columns.Add("text");
            DateTime date = new DateTime(2015, 1, 1);
            for (int i = 2015; i <= DateTime.Today.Year; i++)
            {
                date = new DateTime(i, 1, 1);
                dt.Rows.Add(date.Year, date.Year);
            }
            dt.DefaultView.Sort = "value DESC";
            return dt;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExportExcel.Enabled = false;
                btnExportPDF.Enabled = false;
                aQuarterResult.Disabled = true;
                aQuarterResult.HRef = "javascript:void(0);";
            }
            if (!CanWrite)
            {
                aConfigQuarterReport.Disabled = true;
                aConfigQuarterReport.HRef = "javascript:void(0);";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {

            GetDataGridView();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region btn_ExportPDF_Click
    protected void btn_ExportPDF_Click(object sender, EventArgs e)
    {

        DataTable dt = this.GetData();

        rpt_Quarter_report report = new rpt_Quarter_report();
        report.Name = "Quartery_report";
        report.DataSource = dt;
        string fileName = "Quartery_report_" + DateTime.Now.ToString("MMddyyyyHHmmss");
        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);
        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.OutputStream.Close();
        Response.End();

    }
    #endregion

    #region btnExportExcel_Click
    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = GetData();
            dt.Columns.Remove("SVENDORID");
            dt.Columns.Remove("SCONTRACTID");
            dt.Columns.Remove("ID1");
            dt.Columns.Remove("ID2");
            dt.Columns.Remove("ID3");
            dt.Columns.Remove("ID4");
            dt.Columns.Remove("DOCNO1");
            dt.Columns.Remove("DOCNO2");
            dt.Columns.Remove("DOCNO3");
            dt.Columns.Remove("DOCNO4");
            dt.Columns.Remove("DATESTR1");
            dt.Columns.Remove("DATESTR2");
            dt.Columns.Remove("DATESTR3");
            dt.Columns.Remove("DATESTR4");
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/QuarteryReportFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["ConfirmTruck"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = "Quartery_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GridView_Event


    ASPxGridView grid;
    Dictionary<GridViewDataColumn, TableCell> mergedCells = new Dictionary<GridViewDataColumn, TableCell>();
    Dictionary<TableCell, int> cellRowSpans = new Dictionary<TableCell, int>();
    protected void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        //add the attribute that will be used to find which column the cell belongs to
        e.Cell.Attributes.Add("ci", e.DataColumn.VisibleIndex.ToString());

        if (cellRowSpans.ContainsKey(e.Cell))
        {
            e.Cell.RowSpan = cellRowSpans[e.Cell];
        }
        dynamic data = gvw.GetRowValues(e.VisibleIndex, "ID1", "ID2", "ID3", "ID4");
        if (!string.IsNullOrEmpty(data[0] + string.Empty) && e.DataColumn.Name == "COL_QUARTER1")
        {
            e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
        }
        else if (!string.IsNullOrEmpty(data[1] + string.Empty) && e.DataColumn.Name == "COL_QUARTER2")
        {
            e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
        }
        else if (!string.IsNullOrEmpty(data[2] + string.Empty) && e.DataColumn.Name == "COL_QUARTER3")
        {
            e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
        }
        else if (!string.IsNullOrEmpty(data[3] + string.Empty) && e.DataColumn.Name == "COL_QUARTER4")
        {
            e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
        }

        Button btnView1 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView1");
        Button btnView2 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView2");
        Button btnView3 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView3");
        Button btnView4 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView4");
        if (!CanWrite)
        {
            btnView1.Enabled = false;
            btnView2.Enabled = false;
            btnView3.Enabled = false;
            btnView4.Enabled = false;
        }
    }
    protected void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType == GridViewRowType.Data)
        {
            Button btnView1 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView1");
            Button btnView2 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView2");
            Button btnView3 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView3");
            Button btnView4 = (Button)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView4");
            //Label TotalQ1 = (Label)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "TotalQ1");
            //Label TotalQ2 = (Label)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "TotalQ2");
            //Label TotalQ3 = (Label)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "TotalQ3");
            //Label TotalQ4 = (Label)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "TotalQ4");
            //dynamic data = gvw.GetRowValues(e.VisibleIndex,"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
            //decimal total = (decimal.Parse(data[0] + string.Empty) + decimal.Parse(data[1] + string.Empty) + decimal.Parse(data[2] + string.Empty)) / 3;
            //if (TotalQ1 != null)
            //{
            //    TotalQ1.Text = total.ToString("N");
            //}
            //total = (decimal.Parse(data[3] + string.Empty) + decimal.Parse(data[4] + string.Empty) + decimal.Parse(data[5] + string.Empty)) / 3;
            //if (TotalQ2 != null)
            //{
            //    TotalQ2.Text = total.ToString("N");
            //}
            //total = (decimal.Parse(data[6] + string.Empty) + decimal.Parse(data[7] + string.Empty) + decimal.Parse(data[8] + string.Empty)) / 3;
            //if (TotalQ3 != null)
            //{
            //    TotalQ3.Text = total.ToString("N");
            //}
            //total = (decimal.Parse(data[9] + string.Empty) + decimal.Parse(data[10] + string.Empty) + decimal.Parse(data[11] + string.Empty)) / 3;
            //if (TotalQ4 != null)
            //{
            //    TotalQ4.Text = total.ToString("N");
            //}
            for (int i = e.Row.Cells.Count - 1; i >= 0; i--)
            {

                DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell dataCell = e.Row.Cells[i] as DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell;
                if (dataCell != null && (i <= 1))
                {
                    MergeCells(dataCell.DataColumn, e.VisibleIndex, dataCell);
                }
            }
        }

    }

    protected void MergeCells(GridViewDataColumn column, int visibleIndex, TableCell cell)
    {
        bool isNextTheSame = IsNextRowHasSameData(column, visibleIndex);
        if (isNextTheSame)
        {
            if (!mergedCells.ContainsKey(column))
            {
                mergedCells[column] = cell;
            }
        }
        if (IsPrevRowHasSameData(column, visibleIndex))
        {
            ((TableRow)cell.Parent).Cells.Remove(cell);
            if (mergedCells.ContainsKey(column))
            {
                TableCell mergedCell = mergedCells[column];
                if (!cellRowSpans.ContainsKey(mergedCell))
                {
                    cellRowSpans[mergedCell] = 1;
                }
                cellRowSpans[mergedCell] = cellRowSpans[mergedCell] + 1;
            }
        }
        if (!isNextTheSame)
        {
            mergedCells.Remove(column);
        }
    }
    bool IsNextRowHasSameData(GridViewDataColumn column, int visibleIndex)
    {
        //is it the last visible row
        if (visibleIndex >= gvw.VisibleRowCount - 1)
            return false;

        return IsSameData(column.FieldName, visibleIndex, visibleIndex + 1);
    }
    bool IsPrevRowHasSameData(GridViewDataColumn column, int visibleIndex)
    {
        ASPxGridView grid = column.Grid;
        //is it the first visible row
        if (visibleIndex <= gvw.VisibleStartIndex)
            return false;

        return IsSameData(column.FieldName, visibleIndex, visibleIndex - 1);
    }
    bool IsSameData(string fieldName, int visibleIndex1, int visibleIndex2)
    {
        // is it a group row?
        if (gvw.GetRowLevel(visibleIndex2) != gvw.GroupCount)
            return false;

        return object.Equals(gvw.GetRowValues(visibleIndex1, fieldName), gvw.GetRowValues(visibleIndex2, fieldName));
    }
    #endregion

    #region btnView_Click
    protected void btnView_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if (btn != null)
        {
            string command = btn.CommandArgument;
            string[] liststr = command.Split(';');
            int index = int.Parse(liststr[1]);
            string Quarter = liststr[0];
            dynamic data = gvw.GetRowValues(index, "SVENDORID", "SABBREVIATION", "ID1", "ID2", "ID3", "ID4");
            string ID = string.Empty;
            switch (Quarter)
            {
                case "1":
                    ID = data[2] + string.Empty;
                    break;
                case "2":
                    ID = data[3] + string.Empty;
                    break;
                case "3":
                    ID = data[4] + string.Empty;
                    break;
                case "4":
                    ID = data[5] + string.Empty;
                    break;
                default:
                    break;
            }
            //Quarter&SVENDORID&SABBREVIATION&YEAR&ID
            string strData = Quarter + "&" + data[0] + "&" + data[1] + "&" + ddlYear.SelectedValue + "&" + ID;
            byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
            string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
            Page.ClientScript.RegisterStartupScript(
   this.GetType(), "OpenWindow", "window.open('Quarter_AlertVendor.aspx?str=" + encryptedValue + "','_blank');", true);
        }
    }
    #endregion




}