﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class WorkGroup : PageBase
{
    DataTable dt = new DataTable();
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //dt = WorkGroupBLL.Instance.VEHICLE_STATUS();
            AddDataTable();
        }
    }
    #endregion

    #region gvWorkGroup_RowDataBound
    protected void gvWorkGroup_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            TextBox txt = (TextBox)e.Row.FindControl("txtName");
            if (txt != null)
            {
                txt.Text = dr["NAME"] + string.Empty;
            }
            RadioButtonList rbl = (RadioButtonList)e.Row.FindControl("rblCactive");
            if (rbl != null)
            {
                rbl.SelectedValue = dr["CACTIVE"] + string.Empty;
            }
            HiddenField hid = (HiddenField)e.Row.FindControl("hidID");
            if (hid != null)
            {
                hid.Value = dr["ID"] + string.Empty;
            }

            HtmlAnchor hl = (HtmlAnchor)e.Row.FindControl("hlGroup");
            if (hl != null)
            {
                var plaintextBytes = Encoding.UTF8.GetBytes(dr["ID"] + string.Empty);
                var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                hl.HRef = "~/Group.aspx?id=" + encryptedValue;
            }
        }
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        dt = WorkGroupBLL.Instance.WorkGroupSelect(txtWorkGroup.Text, rblCactive.SelectedValue);
        DataRow dr = dt.NewRow();
        dr["ID"] = 0;
        dr["NAME"] = "";
        dr["CACTIVE"] = "1";
        dt.Rows.InsertAt(dr, 0);

        gvWorkGroup.DataSource = dt;
        gvWorkGroup.DataBind();
    }
    #endregion

    #region AddDataTable
    private void AddDataTable()
    {

        dt.Columns.Add("ID", typeof(int));
        dt.Columns.Add("NAME", typeof(string));
        dt.Columns.Add("CACTIVE", typeof(string));
        dt.Rows.Add(0, "", "1");

        gvWorkGroup.DataSource = dt;
        gvWorkGroup.DataBind();
    }
    #endregion

    #region mpConfirmSave_ClickOK
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int index = int.Parse(btn.CommandArgument);
        GridViewRow gvr = gvWorkGroup.Rows[index];
        int ID = 0;
        string NAME = string.Empty, CACTIVE = "1";
        TextBox txt = (TextBox)gvr.FindControl("txtName");
        if (txt != null)
        {
            NAME = txt.Text.Trim();
        }
        RadioButtonList rbl = (RadioButtonList)gvr.FindControl("rblCactive");
        if (rbl != null)
        {
            CACTIVE = rbl.SelectedValue;
        }
        HiddenField hid = (HiddenField)gvr.FindControl("hidID");
        if (hid != null)
        {
            ID = int.Parse(hid.Value);
        }
        bool isRes = false;
        try
        {
            if (WorkGroupBLL.Instance.WorkGroupCheckValidate(ID, NAME))
            {
                alertFail("กลุ่มงาน " + NAME + " มีอยู่แล้วในระบบ");

            }
            else
            {
                if (ID == 0)
                {
                    isRes = WorkGroupBLL.Instance.WorkGroupInsert(NAME, CACTIVE);
                }
                else
                {
                    isRes = WorkGroupBLL.Instance.WorkGroupUpdate(ID, NAME, CACTIVE);
                }
                if (isRes)
                {
                    alertSuccess("บันทึกข้อมูลสำเร็จ");
                    btnSearch_Click(null, null);
                }
                else
                {
                    alertFail("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>");
                }
            }

        }
        catch (Exception ex)
        {
            alertFail("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>" + ex.Message);
        }

    }
    #endregion
}