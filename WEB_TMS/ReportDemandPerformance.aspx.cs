﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
//using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Web.Configuration;

public partial class ReportDemandPerformance : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    { 
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        DeleteAllFromFolder("~/UploadFile/Report/");
        string Qry_Performance = "", Qry_PerformanceSO = "", Qry_PerformanceSTO = "", Qry_Demand = "", Qry_DemandSTO = "";
        string filed_DemandSO = "", filed_per4manceSO = "";
        string sFromDate = "" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "/01/" + CommonFunction.ReplaceInjection(cboYear.Value + "")
            , sToDate = "" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "/" + DateTime.DaysInMonth(int.Parse(CommonFunction.ReplaceInjection(cboYear.Value + "")), int.Parse(CommonFunction.ReplaceInjection(cboMonth.Value + ""))) + "/" + CommonFunction.ReplaceInjection(cboYear.Value + "");
        for (int nDtae = 1; nDtae <= 31; nDtae++)
        {
            string sDate = nDtae.ToString().PadLeft(2, '0');
            filed_DemandSO += @"
,SUM(CASE WHEN SUBSTR(DLVR.DELIVERY_NO,1,3)='008' AND EXTRACT(day from DLVR.DELIVERY_DATE ) =" + sDate + " THEN 1 ELSE 0 END) STO_" + sDate + @"
,SUM(CASE WHEN SUBSTR(DLVR.DELIVERY_NO,1,3)<>'008' AND EXTRACT(day from DLVR.DELIVERY_DATE ) =" + sDate + " THEN 1 ELSE 0 END) SO_" + sDate + "";
            filed_per4manceSO += @"
,SUM(CASE WHEN NVL(DMAND.SCONTRACTTYPEID,'-')='1' AND EXTRACT(day from DMAND.DATA_DATE ) =" + sDate + " THEN DMAND.TRUCK_AMOUNT ELSE 0 END) STO_" + sDate + @"
,SUM(CASE WHEN NVL(DMAND.SCONTRACTTYPEID,'-')='2' AND EXTRACT(day from DMAND.DATA_DATE ) =" + sDate + " THEN DMAND.TRUCK_AMOUNT ELSE 0 END) SO_" + sDate + "";
        }

        Qry_Demand = string.Format(@"SELECT DLVR_SAP.SUPPLY_PLANT 
{0}
FROM TDELIVERY DLVR 
LEFT JOIN (
    SELECT DELIVERY_NO,SUPPLY_PLANT  FROM TDELIVERY_SAP 
    WHERE 1=1 and DELIVERY_DATE BETWEEN TO_DATE('{2}','MM/DD/YYYY') AND TO_DATE('{3}','MM/DD/YYYY') 
    GROUP BY DELIVERY_NO,SUPPLY_PLANT 
) DLVR_SAP ON DLVR.DELIVERY_NO = DLVR_SAP.DELIVERY_NO
LEFT JOIN TDELIVERY_PRODTYPE DLVR_PRODTYPE ON DLVR.DELIVERY_NO=DLVR_PRODTYPE.DELIVERY_NO
WHERE 1=1 
AND DLVR_SAP.SUPPLY_PLANT ='{1}' 
and DLVR.DELIVERY_DATE BETWEEN TO_DATE('{2}','MM/DD/YYYY') AND TO_DATE('{3}','MM/DD/YYYY') 
{4} 
GROUP BY DLVR_SAP.SUPPLY_PLANT ", filed_DemandSO, CommonFunction.ReplaceInjection("" + cboTerminal.Value), sFromDate, sToDate
                                , (cboProdGrp.Value + "" != "" ? "AND DLVR_PRODTYPE.SPRODUCTTYPEID IN ('" + CommonFunction.ReplaceInjection(cboProdGrp.Value.ToString()).Split(':')[1].Replace(",", "','") + "')" : ""));

//        Qry_Demand = string.Format(@"SELECT DLVR_SAP.SUPPLY_PLANT 
//{0}
//FROM TDELIVERY DLVR 
//LEFT JOIN (
//    SELECT DELIVERY_NO,SUPPLY_PLANT,SPRODUCTTYPEID  FROM TDELIVERY_SAP DLVRSAP
//    INNER JOIN TPRODUCT DLVR_PRODTYPE ON DLVRSAP.MATERIAL_NO  = DLVR_PRODTYPE.PROD_ID
//    WHERE 1=1 and DELIVERY_DATE BETWEEN TO_DATE('{2}','MM/DD/YYYY') AND TO_DATE('{3}','MM/DD/YYYY') 
//    GROUP BY DELIVERY_NO,SUPPLY_PLANT,SPRODUCTTYPEID
//) DLVR_SAP ON DLVR.DELIVERY_NO = DLVR_SAP.DELIVERY_NO
//--LEFT JOIN TDELIVERY_PRODTYPE DLVR_PRODTYPE ON DLVR.DELIVERY_NO=DLVR_PRODTYPE.DELIVERY_NO
//WHERE 1=1 
//AND DLVR_SAP.SUPPLY_PLANT ='{1}' 
//and DLVR.DELIVERY_DATE BETWEEN TO_DATE('{2}','MM/DD/YYYY') AND TO_DATE('{3}','MM/DD/YYYY') 
//{4} 
//GROUP BY DLVR_SAP.SUPPLY_PLANT ", filed_DemandSO, CommonFunction.ReplaceInjection("" + cboTerminal.Value), sFromDate, sToDate
//                              , (cboProdGrp.Value + "" != "" ? "AND DLVR_SAP.SPRODUCTTYPEID IN ('" + CommonFunction.ReplaceInjection(cboProdGrp.Value.ToString()).Split(':')[1].Replace(",", "','") + "')" : ""));

        Qry_Performance = string.Format(@"SELECT DMAND.TERMINAL_ID SUPPLY_PLANT 
{0}
FROM TBL_demandper4mance DMAND
where 1=1 AND DMAND.TERMINAL_ID is not null
AND DMAND.TERMINAL_ID ='{1}' 
and DMAND.DATA_DATE BETWEEN TO_DATE('{2}','MM/DD/YYYY') AND TO_DATE('{3}','MM/DD/YYYY') 
{4}
GROUP BY  DMAND.TERMINAL_ID", filed_per4manceSO, CommonFunction.ReplaceInjection("" + cboTerminal.Value), sFromDate, sToDate
                            , (cboProdGrp.Value + "" != "" ? "AND DMAND.PRODUCTTYPE_ID IN ('" + CommonFunction.ReplaceInjection(cboProdGrp.Value.ToString()).Split(':')[1].Replace(",", "','") + "')" : ""));

        //Response.Write(Qry_Demand + "<br />" + Qry_Performance); return;
        DataTable dt_Demand = CommonFunction.Get_Data(conn, Qry_Demand);
        DataTable dt_Performance = CommonFunction.Get_Data(conn, Qry_Performance);


        //To open a work book
        var m_TemplateFileName = Server.MapPath("~/UploadFile/Report/Template/") + "DemandReport_Template.xlsx";
        var m_InputWorkSheetName = "Demand-Performance";
        var application = new Microsoft.Office.Interop.Excel.ApplicationClass();
        var workBook = application.Workbooks.Open(m_TemplateFileName);
        //After opening the work book, you will need to get access to the excel sheet in the work book
        var inputWorkSheet = workBook.Worksheets.Cast<Microsoft.Office.Interop.Excel.Worksheet>().Where(w => w.Name == m_InputWorkSheetName).FirstOrDefault();
        //Worksheet sheet = (Worksheet)workBook.Worksheets[1];
        //Range range = sheet.get_Range("A1", System.Reflection.Missing.Value);
        //to set cell values
        string[] arr_column = { "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"
                                  , "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U"
                                  , "V", "W", "X", "Y", "Z" , "AA", "AB", "AC", "AD", "AE"
                                  , "AF"};
        string[] arr_date = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10"
                                  , "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"
                                  , "21", "22", "23", "24", "25" , "26", "27", "28", "29", "30"
                                  , "31"};
        string[] arr_rows = { "6", "7", "8", "9" };
        Object[] args_title = new Object[1];
        var cellTitle = inputWorkSheet.get_Range("A2", "A2");
        var cellTitle2 = inputWorkSheet.get_Range("A3", "A3");
        args_title[0] = "Demand - Performance (" + cboTerminal.Text + " " + cboMonth.Text + " " + cboYear.Text + ")";
        cellTitle.GetType().InvokeMember("Value", System.Reflection.BindingFlags.SetProperty, null, cellTitle, args_title);
        args_title[0] = "กลุ่มผลิตภัณฑ์ที่ " + cboProdGrp.Value.ToString().Split(':')[0] + "(" + cboProdGrp.Text + ")";
        cellTitle.GetType().InvokeMember("Value", System.Reflection.BindingFlags.SetProperty, null, cellTitle2, args_title);

        DataTable dt_data = new DataTable();
        foreach (string str_row in arr_rows)
        {
            string PFixCol = "";
            switch (str_row)
            {
                case "6":
                    //Performance SO COD
                    dt_data = dt_Performance;
                    PFixCol = "SO_";
                    break;
                case "8":
                    //Performance STO โอนคลัง
                    dt_data = dt_Performance;
                    PFixCol = "STO_";
                    break;
                case "7":
                    //Demand SO COD
                    dt_data = dt_Demand;
                    PFixCol = "SO_";
                    break;
                case "9":
                    //Demand STO โอนคลัง
                    dt_data = dt_Demand;
                    PFixCol = "STO_";
                    break;
            }
            int idx_arrcolumn = 0;
            bool HasRows = (dt_data.Rows.Count > 0) ? true : false;
            foreach (string str_cols in arr_column)
            {
                idx_arrcolumn++;
                var cell = inputWorkSheet.get_Range(str_cols + str_row, str_cols + str_row);
                Object[] args1 = new Object[1];
                Object[] SO = new Object[1];
                Object[] STO = new Object[1];

                args1[0] = (HasRows) ? dt_data.Rows[0][PFixCol + idx_arrcolumn.ToString().PadLeft(2, '0')] : "";
                cell.GetType().InvokeMember("Value", System.Reflection.BindingFlags.SetProperty, null, cell, args1);
            }
        }
        string FileName = DateTime.Now.ToString("yyyyMMddhhmmss") + "_DemandReport_Template.xls";
        string sUrlFile = "" + Server.MapPath("~/UploadFile/Report/") + FileName;
        //Finally I needed to save the workbook to another file
        workBook.SaveAs(sUrlFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        // finally close workbook
        workBook.Close();
        if (File.Exists(sUrlFile))
        {
            Response.Redirect("UploadFile/Report/" + FileName);
        }
        else
        {
            ltr.Text = "ไม่สามารถดึงไฟล์ ดังกล่าวได้ กรุณาลองใหม่อีกครั้ง";

        }

    }
    protected DataTable QryData(string _year, string _month, string _plant, string _progrp)
    {
        DataTable _dt = new DataTable();

        return _dt;
    }
    protected void cboTerminal_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboTerminal_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTERMINAL.SelectCommand = @"SELECT STERMINALID,STERMINALNAME 
FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.STERMINALID) AS RN , v.STERMINALID, v.SABBREVIATION STERMINALNAME FROM TTERMINAL v WHERE v.STERMINALID||''||NVL(v.SABBREVIATION,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex  ";

        sdsTERMINAL.SelectParameters.Clear();
        sdsTERMINAL.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTERMINAL.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTERMINAL.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTERMINAL;
        comboBox.DataBind();

    }
    protected void DeleteAllFromFolder(string sFileName)
    {
        DirectoryInfo d = new DirectoryInfo(Server.MapPath(sFileName));
        foreach (FileInfo f in d.GetFiles("*.xls"))
        {
            if (f.CreationTime.AddHours(24) < DateTime.Now)
                File.Delete(f.FullName);
        }
         
    }
}