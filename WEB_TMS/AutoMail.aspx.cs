﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Configuration;
using System.Data;
using System.IO;

public partial class AutoMail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string SuccessClearLogOnHold = SendMailClearLogTruckOnHold();
            string SuccessOnHold = SendMailTruckOnHold();

            if (SuccessOnHold == "" || SuccessClearLogOnHold == "")
            {
                string script = "window.opener = 'Self';window.open('','_parent',''); window.close();";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseWindow", script, true);
            }
            else
            {
                ///Paths File
                string strPathName = "./UploadFile/ErrorLog/";

                #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
                if (!Directory.Exists(Server.MapPath(strPathName)))
                {
                    Directory.CreateDirectory(Server.MapPath(strPathName));
                }
                #endregion
                ///Create a text file containing error details
                string strFileName = DateTime.Now.ToString("ddMMyyyy", new System.Globalization.CultureInfo("th-TH")) + ".txt";
                string errorText = "\r\nError Message: " + SuccessOnHold.Replace("^", "\r\n") + "\r\n";
                string logMessage = String.Format(@"[{0}] :: {1} ", DateTime.Now.ToString("HH:mm:ss", new System.Globalization.CultureInfo("th-TH")), errorText);
                FileInfo file = new FileInfo(Request.MapPath(strPathName + strFileName));
                StreamWriter _sw = file.AppendText();
                _sw.Write(logMessage);
                _sw.Write(_sw.NewLine);
                _sw.WriteLine(("*").PadLeft(logMessage.Length, '*'));
                _sw.Write(_sw.NewLine);
                _sw.Close();


                bool IsSended = CommonFunction.SendMail("" + ConfigurationSettings.AppSettings["SystemMail"], "" + ConfigurationSettings.AppSettings["SupportMail"]
                    , "TMS:Systems Error [Alert Vehicle on Hold]", errorText.Replace("\r\n", "<br>"), "");

                string script = "window.opener = 'Self';window.open('','_parent',''); window.close();";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseWindow", script, true);
            }
        }
    }
    protected string SendMailTruckOnHold()
    {
        string Qry = @"SELECT DISTINCT chk.STRUCKID ,chklst.DEND_LIST ,VEH.STRUCKID VEHID,chk.SHEADERREGISTERNO  ,TU.STRUCKID TUID ,chk.STRAILERREGISTERNO
        FROM TCheckTruckItem  chklst
        LEFT JOIN TCHECKTRUCK chk ON chklst.SCHECKID =chk.SCHECKID 
        LEFT JOIN TTRUCK VEH ON chk.SHEADERREGISTERNO=VEH.SHEADREGISTERNO 
        LEFT JOIN TTRUCK TU ON chk.STRAILERREGISTERNO=TU.SHEADREGISTERNO 
        WHERE NVL(chklst.CEDITED,'0') !='1' AND chklst.CMA IN('2','1')
         AND chklst.DEND_LIST < SYSDATE AND NVL(VEH.CHOLD,'0')='0' 
         AND VEH.STRUCKID is not null
         AND TRUNC(chklst.DEND_LIST) > TRUNC(SYSDATE,'yyyy') ";

        string OP_Msg = "", result = "";
        using (OracleConnection Connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable _dtTRCK = CommonFunction.Get_Data(Connection, Qry);
            foreach (DataRow _drTRCK in _dtTRCK.Rows)
            {
                try
                {
                    OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1' WHERE STRUCKID=:S_TRUCKID", Connection);
                    ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = _drTRCK["STRUCKID"] + "";
                    //ora_cmd.ExecuteNonQuery();

                    AlertTruckOnHold onHold = new AlertTruckOnHold(Page, Connection);
                    onHold.VehNo = _drTRCK["SHEADERREGISTERNO"] + "";
                    onHold.TUNo = _drTRCK["STRAILERREGISTERNO"] + "";
                    onHold.Subject = "ละเลยการแก้สภาพรถเกินตามที่กำหนด โดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                    onHold.VehID = _drTRCK["VEHID"] + "";
                    onHold.TUID = _drTRCK["TUID"] + "";
                    result = onHold.SendTo();
                }
                catch (Exception ect)
                {
                    OP_Msg += "^" + _drTRCK["SHEADERREGISTERNO"] + "-" + _drTRCK["STRAILERREGISTERNO"] + " Error:" + ect.Message;
                }
            }
        }

        return OP_Msg;
    }

    protected string SendMailClearLogTruckOnHold()
    {
        string Qry = @"SELECT STRUCKID,DATE_ADD,DEND_LIST,VEHID,SHEADERREGISTERNO,TUID,STRAILERREGISTERNO,SEND_STATUS FROM TBL_TRUCK_ONHOLD WHERE 1=1";

        string OP_Msg = "", result = "";
        using (OracleConnection Connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable _dtTRCK = CommonFunction.Get_Data(Connection, Qry);
            foreach (DataRow _drTRCK in _dtTRCK.Rows)
            {
                try
                {
                    OracleCommand ora_del_cmd = new OracleCommand("DELETE TBL_TRUCK_ONHOLD WHERE SHEADERREGISTERNO=:S_HEADERREGISTERNO AND STRAILERREGISTERNO=:S_TRAILERREGISTERNO", Connection);
                    ora_del_cmd.Parameters.Add(":S_HEADERREGISTERNO", OracleType.VarChar).Value = _drTRCK["SHEADERREGISTERNO"] + "";
                    ora_del_cmd.Parameters.Add(":S_TRAILERREGISTERNO", OracleType.VarChar).Value = _drTRCK["STRAILERREGISTERNO"] + "";
                    ora_del_cmd.ExecuteNonQuery();


                    OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1' WHERE STRUCKID=:S_TRUCKID", Connection);
                    ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = _drTRCK["STRUCKID"] + "";
                    //ora_cmd.ExecuteNonQuery();

                    AlertTruckOnHold onHold = new AlertTruckOnHold(Page, Connection);
                    onHold.VehNo = _drTRCK["SHEADERREGISTERNO"] + "";
                    onHold.TUNo = _drTRCK["STRAILERREGISTERNO"] + "";
                    onHold.Subject = "ละเลยการแก้สภาพรถเกินตามที่กำหนด โดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                    onHold.VehID = _drTRCK["VEHID"] + "";
                    onHold.TUID = _drTRCK["TUID"] + "";
                    result = onHold.SendTo();
                }
                catch (Exception ect)
                {
                    OP_Msg += "^" + _drTRCK["SHEADERREGISTERNO"] + "-" + _drTRCK["STRAILERREGISTERNO"] + " Error:" + ect.Message;
                }
            }
        }

        return OP_Msg;
    }
}