﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;

public partial class AccidentList : PageBase
{
    DataTable dt;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            if (Session["CGROUP"] + string.Empty == "0")
            {
                ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
                ddlVendor.Enabled = false;
                ddlVendor_SelectedIndexChanged(null, null);
                aAdd.Visible = false;
                //mode = "view";
            }
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
                btnSearch.CssClass = "btn btn-md bth-hover btn-info";
                btnExport.CssClass = "btn btn-md bth-hover btn-info";
            }
            if (!CanWrite)
            {
                aAdd.Disabled = true;
                aAdd.HRef = "javascript:void(0);";
                aConclude.Disabled = true;
                aConclude.HRef = "javascript:void(0);";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.DataTextField = "NAME";
        ddlWorkGroup.DataValueField = "ID";
        ddlWorkGroup.DataSource = ContractTiedBLL.Instance.WorkGroupSelect();
        ddlWorkGroup.DataBind();
        ddlWorkGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.SelectedIndex = 0;
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlGroup.SelectedIndex = 0;
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;

        ddlStatus.DataTextField = "NAME";
        ddlStatus.DataValueField = "CACTIVE";
        ddlStatus.DataSource = AccidentBLL.Instance.AccidentStatusSelect();
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
    }
    #region ddlWorkGroup_SelectedIndexChanged
    protected void ddlWorkGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroupsDataBind(ddlWorkGroup.SelectedValue + string.Empty);
    }
    #endregion

    #region ddlGroupsDataBind
    private void ddlGroupsDataBind(string WordGroupID)
    {
        ddlGroup.DataTextField = "NAME";
        ddlGroup.DataValueField = "ID";
        ddlGroup.DataSource = ContractTiedBLL.Instance.GroupSelect(WordGroupID);
        ddlGroup.DataBind();
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });
    }
    #endregion

    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID",typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlContract_SelectedIndexChanged
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
            {
                dt = AccidentBLL.Instance.GroupSelect(ddlContract.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                    ddlWorkGroup_SelectedIndexChanged(null, null);
                    ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            gvAccident.DataSource = dt;
            gvAccident.DataBind();
            lblItem.Text = dt.Rows.Count + string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region GetData
    private void GetData()
    {
        dt = AccidentBLL.Instance.AccidentSelect(txtSearch.Text.Trim(), txtDate.Text.Trim(), ddlWorkGroup.SelectedValue, ddlGroup.SelectedValue, ddlVendor.SelectedValue, ddlContract.SelectedValue, rblType.SelectedValue, ddlStatus.SelectedValue, Session["CGROUP"] + string.Empty, txtDateTo.Text.Trim());
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDate.Text = string.Empty;
        rblType.ClearSelection();
        ddlWorkGroup.SelectedIndex = 0;
        ddlWorkGroup_SelectedIndexChanged(null, null);
        
        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
        txtSearch.Text = string.Empty;
        ddlStatus.SelectedIndex = 0;
        gvAccident.DataSource = null;
        gvAccident.DataBind();
        lblItem.Text = "0";
    }
    #endregion

    #region btnExport_Click
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            dt.Columns.Remove("SYS_TIME");
            dt.Columns.Remove("REPORT_PTT");
            dt.Columns.Remove("REPORTER");
            dt.Columns.Remove("VENDOR_REPORT_TIME");
            dt.Columns.Remove("SEND_CONSIDER");
            dt.Columns.Remove("PTT_APPROVE");
            dt.Columns.Remove("INFORMER_NAME");
            dt.Columns.Remove("CACTIVE");
            dt.Columns.Remove("CREATE_DATE");
            dt.Columns.Remove("CREATE_BY");
            dt.Columns.Remove("UPDATE_DATE");
            dt.Columns.Remove("UPDATE_BY");
            dt.Columns.Remove("ACCIDENTTYPE_ID");
            dt.Columns.Remove("SHIPMENT_NO");
            dt.Columns.Remove("STRUCKID");
            dt.Columns.Remove("SVENDORID");
            dt.Columns.Remove("GROUPID");
            dt.Columns.Remove("SCONTRACTID");
            dt.Columns.Remove("SEMPLOYEEID");
            dt.Columns.Remove("SOURCE");
            dt.Columns.Remove("LOCATIONS");
            dt.Columns.Remove("GPSL");
            dt.Columns.Remove("GPSR");
            dt.Columns.Remove("PRODUCT_ID");
            dt.Columns.Remove("WORKGROUPNAME");
            dt.Columns.Remove("SERIOUS");
            dt.Columns.Remove("EVALUATION");
            //dt.Columns.Remove("TERMINALNAME");
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/AccidentFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["Accident"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = "Accident_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #endregion

    #region gvAccident_RowUpdating
    protected void gvAccident_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hidCACTIVE = (HiddenField)gvAccident.Rows[e.RowIndex].FindControl("hidCACTIVE");
        string strData = gvAccident.DataKeys[e.RowIndex]["ACCIDENT_ID"] + string.Empty, page = string.Empty;
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
        string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
        
        if (hidCACTIVE.Value == "1" || (hidCACTIVE.Value == "2" && Session["CGROUP"] + string.Empty != "0"))
        {
            page = "AccidentTab1";
            
        }
        else if (hidCACTIVE.Value == "2" && Session["CGROUP"] + string.Empty == "0")
        {
            page = "AccidentTab2";
        }
        else if ((hidCACTIVE.Value == "4" || hidCACTIVE.Value == "6" || hidCACTIVE.Value == "7") && Session["CGROUP"] + string.Empty == "0")
        {
            page = "AccidentTab3_Vendor";
        }
        else if ((hidCACTIVE.Value == "4" || hidCACTIVE.Value == "6" || hidCACTIVE.Value == "7") && Session["CGROUP"] + string.Empty == "1")
        {
            page = "AccidentTab3";
        }
        else if ((int.Parse(hidCACTIVE.Value) >= 8 &&int.Parse(hidCACTIVE.Value) <= 15))
        {
            page = "AccidentTab4";
        }
        Page.ClientScript.RegisterStartupScript(
   this.GetType(), "OpenWindow", "window.open('" + page + ".aspx?str=" + encryptedValue + "','_blank');", true);
    }
    #endregion

    #region gvAccident_PageIndexChanging
    protected void gvAccident_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAccident.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);
    }
    #endregion
    
}