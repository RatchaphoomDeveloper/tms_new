﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Import.aspx.cs" Inherits="KPI_Import" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <script type="text/javascript">
                $(document).ready(function () {
            
                    // here clone our gridview first
                    var tab = $("#<%=dgvKPI.ClientID%>").clone(true);
                    // clone again for freeze
                    var tabFreeze = $("#<%=dgvKPI.ClientID%>").clone(true);
 
                    // set width (for scroll)
                    var totalWidth = $("#<%=dgvKPI.ClientID%>").outerWidth();
                    var firstColWidth = $("#<%=dgvKPI.ClientID%> th:first-child").outerWidth();
                    tabFreeze.width(firstColWidth);
                    tab.width(totalWidth - firstColWidth);
 
                    // here make 2 table 1 for freeze column 2 for all remain column

                    tabFreeze.find("th:gt(0)").remove();
                    tabFreeze.find("td:not(:first-child)").remove();
 
                    tab.find("th:first-child").remove();
                    tab.find("td:first-child").remove();
 
                    // create a container for these 2 table and make 2nd table scrollable
            
                    var container = $('<table border="0" cellpadding="0" cellspacing="0"><tr><td valign="top"><div id="FCol"></div></td><td valign="top"><div id="Col" style="width:320px; overflow:auto"></div></td></tr></table)');
                    $("#FCol", container).html($(tabFreeze));
                    $("#Col", container).html($(tab));
 
                    // clear all html
                    $("#gridContainer").html('');
                    $("#gridContainer").append(container);
                });
            </script>

    <div class="form-horizontal">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>Batch Input
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ปี</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="GetKPIForm" >
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เดือน</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlMonth" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="GetKPIForm">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ชุดประเมิน KPI</label>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtKPIForm" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </div>
                            <label class="col-md-2 control-label">วันที่บันทึก</label>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtSaveDate" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ข้อมูล IVMS ล่าสุด</label>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtIVMS" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </div>
                            <label class="col-md-2 control-label">ข้อมูล Shipment Cost ล่าสุด</label>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtShipmentCost" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">
                        <asp:Button Text="Preview" runat="server" ID="cmdExport" class="btn btn-md bth-hover btn-info" OnClick="cmdExport_Click" />
                        <asp:Button Text="บันทึก" runat="server" ID="cmdSave" class="btn btn-md bth-hover btn-info" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                        <asp:Button Text="ยกเลิก" runat="server" ID="cmdCancel" class="btn btn-md bth-hover btn-info" OnClick="cmdCancel_Click" />
                    </div>
                </div>
                        </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>Batch Input
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                    <ContentTemplate>
                        <asp:GridView runat="server" ID="dgvKPI" ShowHeader="false"
                            HeaderStyle-HorizontalAlign="Center" Width="100%"
                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                            DataKeyNames="SCONTRACTID,SVENDORID">
                            <Columns>
                                <%--<asp:BoundField HeaderText="SVENDORID" DataField="" Visible="false" />
                                <asp:BoundField HeaderText="SCONTRACTID" DataField="" Visible="false" />
                                <asp:BoundField HeaderText="FORMULA_ID" DataField="" Visible="false" />
                                <asp:BoundField HeaderText="ลำดับ" DataField="NO" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" />
                                <asp:BoundField HeaderText="ผู้ขนส่ง" DataField="SABBREVIATION" />--%>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="50" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                        <br />
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">
                        
                    </div>
                </div>
                        </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
                        <asp:Button Text="Export" runat="server" ID="cmdExportExcel" class="btn btn-md bth-hover btn-info" OnClick="cmdExportExcel_Click" />

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>