﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_multiplier_add.aspx.cs" Inherits="admin_multiplier_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            width: 50%;
            height: 31px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" style="width: 20%">
                            ช่วงคะแนน <font color="#ff0000">*</font>
                        </td>
                        <td style="width: 30%">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtStartPoint" runat="server" CssClass="dxeLineBreakFix" 
                                            Width="100px">
                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" 
                                                SetFocusOnError="True" ValidationGroup="add">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_DecimalNumber %>" 
                                                    ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                <RequiredField ErrorText="กรุณากรอกช่วงคะแนน" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp;-&nbsp;
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtEndPoint" runat="server" CssClass="dxeLineBreakFix" 
                                            Width="100px">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                            
                        </td>
                        <td>
                        &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style28">
                            คะแนน <font color="#ff0000">*</font>
                        </td>
                        <td align="left" class="style27">
                            <dx:ASPxTextBox ID="txtMultiple" runat="server" CssClass="dxeLineBreakFix" 
                                Width="100px">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" 
                                    SetFocusOnError="True" ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_DecimalNumber %>" 
                                        ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                    <RequiredField ErrorText="กรุณากรอกช่วงคะแนน" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                            ผู้บันทึก
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxLabel ID="lblUser" runat="server" Text="" ClientInstanceName="lblUser">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2" class="style13">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_multiplier_lst.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
