﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;

public partial class vendor_depo_desc : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["oSternalID"] != null)
            {
                BindData();
            }
        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
     
    }

    void BindData()
    {
        string sterminalID = Session["oSternalID"] + "";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT T.STERMINALID,TS.STERMINALNAME,T.SADDRESS,T.SEVIDENCE,T.SFILEPATH,T.SEVIDENCE1,T.SFILEPATH1,T.STEL,t.SFAX,R.REGION_DESC  FROM (TTERMINAL t LEFT JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID) LEFT JOIN TREGION r ON T.SREGION = R.REGION WHERE T.STERMINALID = '" + sterminalID + "'");
        if (dt.Rows.Count > 0)
        {
            lblTerminal.Text = dt.Rows[0]["STERMINALNAME"] + "";
            lblAddress.Text = dt.Rows[0]["SADDRESS"] + "";
            lblTel.Text = dt.Rows[0]["STEL"] + "";
            lblFax.Text = dt.Rows[0]["SFAX"] + "";
            lblRegion.Text = dt.Rows[0]["REGION_DESC"] + "";
            lblEvidence.Text = dt.Rows[0]["SEVIDENCE"] + "";
            lblEvidence1.Text = dt.Rows[0]["SEVIDENCE1"] + "";
            txtFilePath.Text = dt.Rows[0]["SFILEPATH"] + "";
            txtFilePath1.Text = dt.Rows[0]["SFILEPATH1"] + "";

            string FirstSterminal = sterminalID.Substring(0, 1);
            string ProductType = "";

            if (FirstSterminal == "H")
            {
                string TypeSterminal = sterminalID.Substring(2, 1);
                if (TypeSterminal == "0") 
                {
                    ProductType = "น้ำมัน";
                }
                else if (TypeSterminal == "4")
                {
                    ProductType = "ก๊าซ";
                }
                else if (TypeSterminal == "2") 
                {
                    ProductType = "อากาศยาน";
                }
            }
            else if (FirstSterminal == "I")
            {
                ProductType = "COCO";
            }
            else if (FirstSterminal == "J")
            {
                ProductType = "โรงบรรจุของเอกชน";
            }
            else if (FirstSterminal == "K")
            {
                ProductType = "คลังเช่าที่บริษัท ปตท.";
            }

            lblProductType.Text = ProductType;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Response.Redirect("vendor_depo_view.aspx");
    }
}