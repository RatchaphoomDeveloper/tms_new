﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="menustandard.aspx.cs" Inherits="menustandard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="15%"></td>
            <td style="vertical-align: top;" width="100%">
                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="350px" HeaderText="ข้อมูลพื้นฐานทั่วไป"
                    CssClass="dxeLineBreakFix">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                            <table width="100%">
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href="vendor_list.aspx">ข้อมูลผู้ขนส่ง</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='vendor_employee.aspx'>ข้อมูลพนักงาน</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='product.aspx'>ข้อมูลผลิตภัณฑ์</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='Information_standard.aspx'>ข้อมูล Average Standard Trip</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='contract_info.aspx'>ข้อมูลสัญญา</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='truck_info.aspx'>ข้อมูลรถ</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='Truck_Moving.aspx'>ผูกข้อมูลรถ</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>

                                        <%--function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('EDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }--%>
                                        <li><a id="vehicle" href="Vehicle.aspx">ผขส.ขอผูกTU</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <%--<tr>
                                    <td></td>

                                    <td>
                                        <li><a href='ContractTied_Lst.aspx'>ผูกสัญญา</a></li>
                                    </td>
                                    <td></td>
                                </tr>--%>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='CheckOldVersion.aspx'>ข้อมูลระบบเดิม</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <%--<tr>
                                    <td></td>

                                    <td>
                                        <li><a href='Kpi_lst.aspx'>KPI_DEV</a></li>
                                    </td>
                                    <td></td>
                                </tr>--%>

                                <%--<tr>
                                    <td></td>
                                    <td><li>
                                        <a href='KPI_Index.aspx'>ดัชนีชี้วัดประสิทธิภาพการทำงาน (KPI)
 	
                                        </a>

                                        </li> </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><li>
                                        <a href="KPI_SUMMARY.aspx">รายละเอียด ดัชนีชี้วัดประสิทธิภาพการทำงาน (KPI)
 	
                                        </a>

                                        </li> </td>
                                    <td></td>
                                </tr>--%>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='AutoChangeStatusLog.aspx'>Auto Change Status (Driver, Truck)</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='final-score_detail.aspx'>ข้อมูลการตัดคะแนน (Old) </a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='final-grade_old.aspx'>ผลประเมินประจำปี (Old) </a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='Department.aspx'>Department</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='Division.aspx'>Division</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='ProductTagHead.aspx'>ProductTagHead</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='ProductTagItem.aspx'>ProductTagItem</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='KnowledgeType.aspx'>KnowledgeType</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='SpotDelivery.aspx'>สรุปแผนการขนส่ง รถจ้างพิเศษ</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp; </td>
                                    <td></td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
                <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="350px" HeaderText="ข้อมูลพื้นฐานทั่วไปของวัดน้ำ"
                    CssClass="dxeLineBreakFix">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                            <table width="100%">
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='service_cost.aspx'>อัตราค่าบริการ</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='order_car.aspx'>จำนวนการให้บริการตรวจวัดน้ำ</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href="doc_set.aspx">เพิ่มเอกสารพื้นฐาน</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%--<li></li>--%>&nbsp;
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%--<li>&nbsp;</li>--%>&nbsp;
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%--<li>&nbsp;</li>--%>&nbsp;
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp;
                                        <%--<li>&nbsp;</li>--%>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp;</td>
                                    <%--<td>&nbsp; </td>--%>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp; </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp; </td>
                                    <td></td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>

                <dx:ASPxRoundPanel ID="qwefw" runat="server" Width="350px" HeaderText="Maintain Function"
                    CssClass="dxeLineBreakFix">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                            <table width="100%">
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='EmailTemplate.aspx'>รูปแบบการส่งอีเมล์</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='EmailSchedule.aspx'>ตารางการส่งอีเมล์</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li><a href='ComplaintTopic.aspx'>ข้อมูล Topic</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='RequireField.aspx'>Require</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='Position_Lst.aspx'>ข้อมูลตำแหน่ง</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='WorkGroup.aspx'>กลุ่มงาน</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='UploadFile.aspx'>ไฟล์อัพโหลด</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <li><a href='MaintenanceTruck.aspx'>Require</a></li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li>
                                            <a href='Quarter_Template.aspx'>Template แจ้งผลประเมินการทำงาน 	
                                            </a>

                                        </li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li>
                                            <a href='AccidentType.aspx'>ขั้นตอนการขนส่งขณะเกิดเหตุ
 	
                                            </a>

                                        </li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li>
                                            <a href='AnnualReportEmailFormat.aspx'>Template แจ้งผลประเมินรายปี 	
                                            </a>
                                        </li>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <li>
                                            <a href='Maintain.aspx'>Maintain
                                            </a>
                                        </li>
                                    </td>
                                    <td></td>
                                </tr>
                                <%--<tr>
                                    <td></td>
                                    <td><li>
                                        <a href='KPI_Formula.aspx'>จัดการสูตร KPI
 	
                                        </a>

                                        </li> </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><li>
                                        <a href='KPI_Evaluation_Kit.aspx'>จัดการชุดคำถาม KPI
 	
                                        </a>

                                        </li> </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><li>
                                        <a href='KPI_Manual.aspx'>บันทึกผลการประเมิน KPI
 	
                                        </a>

                                        </li> </td>
                                    <td></td>
                                </tr>--%>
                                <tr>
                                    <td></td>
                                    <td>&nbsp; </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>&nbsp; </td>
                                    <td></td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </td>
            <td width="15%"></td>
        </tr>
    </table>
    <!--
   <table width="100%" cellpadding="1" cellspacing="3">
     <tr>
       <td></td>
       <td style=" font-size:18px">&nbsp;</td>
     </tr>
     <tr>
       <td width="10%"></td>
       <td width="40%"><li><a href='ReportGrade.aspx' >หนังสือแจ้งบริษัท</a></li></td>
       <td width="10%"></td>
       <td width="40%"><li><a href='ReportReducePoint.aspx' >สรุปข้อมูลการผิดสัญญา</a></li></td>
     </tr>
      <tr>
       <td></td>
       <td><li><a href='ReportSuppliant.aspx' >สรุปข้อมูลการขออุทธรณ์</a></li></td>
       <td></td>
       <td><li><a href="reportsDetailConfirmContact.aspx">รายงานยืนยันรถตามสัญญา</a></li></td>
     </tr>
       
      <tr>
       <td>&nbsp;</td>
       <td>
           <li><a href="reportDetailPlanning.aspx">รายงานการจัดแผนงาน</a></li>
          </td>
       <td>&nbsp;</td>
       <td>
           <li><a href="reportDetailConfirmPlan.aspx">รายงานยืนยันตามแผนงาน</a></li>
          </td>
     </tr>
     <tr>
        <td></td>
       <td><li><a href='ReportExpireEmployee.aspx' >รายงานค้นหา พขร. ตามอายุ</a></li></td>
       <td></td>
       <td><li><a href='ReportExpireResourceMonitoring.aspx' >รายงานรถที่ใกล้หมดสภาพการใช้งาน</a></li></td>
     </tr>
     <tr>
        <td></td>
        <td><li><a href='ReportExpireResource.aspx' >รายงานค้นหารถตามอายุ</a></li></td>
        <td></td>
        <td><li><a href='DetailDefectMonitoring.aspx' >รายงานสำหรับติดตามปัญหาการขนส่ง</a></li>  </td>
     </tr>
     <tr>
       <td></td><td><li><a href='ReportDemandPerformance.aspx' >รายงาน DemandPerformance</a></li></td><td></td>
       <td><li><a href='Reportsolvepurchase.aspx' >รายงาน SolvePurchase</a></li></td>
     </tr>
     <tr>
       <td></td><td><li><a href='ReportDefectPercentage.aspx' >รายงาน Defect Percentage</a></li></td> 
         <td></td>
        <td><li><a href='ReportQuarterLetter.aspx' >รายงานแจ้งผลการประเมินประจำไตรมาส</a></li>  </td>
     </tr>
      <tr>
       <td></td><td><li><a href='Defect_Point_Quarter.aspx' >รายงานประเมินผลการทำงาน</a></li></td> 
         <td></td>
        <td> </td>
     </tr>
   </table>
   -->
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>--%>
