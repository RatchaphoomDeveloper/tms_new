﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using TMS_BLL.Master;
using System.Web.Security;
using System.Text;


public partial class approve_pk : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string SMENUID = "61";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }

        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return; 
            LogUser(SMENUID, "R", "เปิดดูข้อมูลหน้า พิจารณาคำขอแก้ไขข้อมูล", "");
            gvw.SettingsPager.PageSize = 50;
            ListData();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                ASPxButton1.Enabled = false;
            }
            if (!CanWrite)
            {
                            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.ToString() == "จัดการ")
        {

            ASPxButton btnSaveother = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnSaveother") as ASPxButton;
            dynamic data = gvw.GetRowValues(e.VisibleIndex, "STATUS");
            if (data == "0" || data == "4")
            {
                btnSaveother.Text = "รอพิจารณา";
            }
            else if (data == "2" || data == "4" || data == "5")
            {
                btnSaveother.Visible = false;
            }
            else
            {
                btnSaveother.Text = "ดูรายละเอียด";
            }

            if (!CanWrite)
            {
                btnSaveother.Enabled = false;
            }
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData();
        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        cboSelect.DataSource = ViewState["DataVendor"];
        cboSelect.DataBind();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string[] param = e.Parameter.Split(';');
            int Idx = 0;
            if (param.Length > 1)
            {
                Idx = int.Parse(param[1] + "");
            }

            switch (param[0])
            {
                case "approve":
                    dynamic data = gvw.GetRowValues(Idx, "TYPE", "REF_ID", "REQ_ID", "STATUS");
                    string TYPE = data[0] + "";
                    switch (TYPE)
                    {
                        //ผุ้ขนส่ง
                        case "V":
                            //Session["Backpage"] = VendorID;
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_edit_pk.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data[1])) + "&strID=" + Server.UrlEncode(STCrypt.Encrypt("" + data[2])) + "";
                            break;
                        //พขร.
                        case "E":
                            byte[] plaintextBytes = Encoding.UTF8.GetBytes(data[1] + string.Empty);
                            string PK = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                            plaintextBytes = Encoding.UTF8.GetBytes(data[2] + string.Empty);
                            string strID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                            plaintextBytes = Encoding.UTF8.GetBytes(data[3] + string.Empty);
                            string status = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                            plaintextBytes = Encoding.UTF8.GetBytes("true" + string.Empty);
                            string sAllowEdit = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                            xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx?PK=" + PK + "&strID=" + strID + "&status=" + status + "&allowEditStatus=" + sAllowEdit;
                            break;
                        //สัญญา
                        case "C":

                            //int inxT2 = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                            dynamic dataT2 = gvw.GetRowValues(Idx, "REF_ID", "SVENDORID");
                            string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("EDIT&" + dataT2[0] + "&" + dataT2[1])), vsUrl = "contract_add_pk.aspx?str=";

                            string SCONTRACTID = dataT2 + "";
                            xcpn.JSProperties["cpRedirectTo"] = vsUrl + vsEncrypt + "&strID=" + Server.UrlEncode(STCrypt.Encrypt("" + data[2])) + "";
                            break;
                        //รถ
                        case "T":
                            dynamic dy_data = gvw.GetRowValues(Idx, "REF_ID", "SVENDORID");
                            DataTable dt = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + dy_data[0] + "' AND STRANSPORTID LIKE '%" + dy_data[1] + "%'");
                            string STRUCKID = "";
                            string STRAILERID= "";
                            string SCARTYPEID = "";

                            if (dt.Rows.Count > 0)
                            {
                                STRUCKID = dt.Rows[0]["STRUCKID"] + "";
                                STRAILERID = dt.Rows[0]["STRAILERID"] + "";
                                SCARTYPEID = dt.Rows[0]["SCARTYPEID"] + "";
                            }
                            else
                            {
                                DataTable dt_TEMP = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK_TEMP WHERE STRUCKID = '" + dy_data[0] + "' AND STRANSPORTID LIKE '%" + dy_data[1] + "%'");
                                if (dt_TEMP.Rows.Count > 0)
                                {
                                    STRUCKID = dt_TEMP.Rows[0]["STRUCKID"] + "";
                                    STRAILERID = dt_TEMP.Rows[0]["STRAILERID"] + "";
                                    SCARTYPEID = dt_TEMP.Rows[0]["SCARTYPEID"] + "";
                                }
                            }
                            if (string.Equals(data[3], "1") || string.Equals(data[3], "2"))
                            {
                                string sEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID) + "&STRAILERID=" + ConfigValue.GetEncodeText(STRAILERID) + "&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID)
                                    , sUrl = "Truck_History_View.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = sUrl + sEncrypt + "&strID=" + ConfigValue.GetEncodeText(data[2]);
                            }
                            else
                            {
                                string sEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID) + "&STRAILERID=" + ConfigValue.GetEncodeText(STRAILERID) + "&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID)
                                    , sUrl = "truck_edit.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = sUrl + sEncrypt + "&strID=" + ConfigValue.GetEncodeText(data[2]);
                            }
                            break;
                        //การผูกรถ
                        case "H":
                            dynamic dy_data_H = gvw.GetRowValues(Idx, "REF_ID", "SVENDORID");
                            DataTable dt_H = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + dy_data_H[0] + "' AND STRANSPORTID LIKE '%" + dy_data_H[1] + "%'");
                            string STRUCKID_H = "";
                            string STRAILERID_H = "";
                            string SCARTYPEID_H = "";

                            if (dt_H.Rows.Count > 0)
                            {
                                STRUCKID_H = dt_H.Rows[0]["STRUCKID"] + "";
                                STRAILERID_H = dt_H.Rows[0]["STRAILERID"] + "";
                                SCARTYPEID_H = dt_H.Rows[0]["SCARTYPEID"] + "";
                            }
                            else
                            {
                                DataTable dt_TEMP = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK_TEMP WHERE STRUCKID = '" + dy_data_H[0] + "' AND STRANSPORTID LIKE '%" + dy_data_H[1] + "%'");
                                if (dt_TEMP.Rows.Count > 0)
                                {
                                    STRUCKID_H = dt_TEMP.Rows[0]["STRUCKID"] + "";
                                    STRAILERID_H = dt_TEMP.Rows[0]["STRAILERID"] + "";
                                    SCARTYPEID_H = dt_TEMP.Rows[0]["SCARTYPEID"] + "";
                                }
                            }
                            if (string.Equals(data[3], "1") || string.Equals(data[3], "2"))
                            {
                                string sEncrypt_H = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID_H) + "&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID_H)
                                    , HUrl = "Vehicle_detail.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = HUrl + sEncrypt_H + "&strID=" +  ConfigValue.GetEncodeText(data[2]);
                            }
                            else
                            {
                                string sEncrypt_H = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID_H) + "&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID_H)
                                    , HUrl = "Vehicle_approve.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = HUrl + sEncrypt_H + "&strID=" + ConfigValue.GetEncodeText(data[2]);
                            }
                            break;
                    }

                    break;


                case "request": xcpn.JSProperties["cpRedirectTo"] = "Vendor_Detail.aspx";
                    break;

                case "search":
                    LogUser(SMENUID, "S", "ค้นหาข้อมูล พิจารณาคำขอแก้ไขข้อมูล", "");
                    ListData();
                    break;
            }

        }
    }

    void ListData()
    {
        string Condition = "";

        if (!string.IsNullOrEmpty(cboSelect.Value + ""))
        {
            Condition += " AND VEN.SVENDORID = '" + cboSelect.Value + "'";
        }

        if (!string.IsNullOrEmpty(rblStatus.Value + ""))
        {
            Condition += " AND TDC.STATUS = '" + rblStatus.Value + "'";
        }

        if (!string.IsNullOrEmpty(rblDatastatus.Value + ""))
        {
            Condition += " AND TDC.TYPE = '" + rblDatastatus.Value + "'";
        }
        if (!string.IsNullOrEmpty(txtSearch.Text.ToString().Trim()))
        {
            Condition += " AND TDC.DESCRIPTION LIKE '%" + txtSearch.Text.ToString().Trim() + "%'";
        }

        string QUERY = @"SELECT TDC.REQ_ID, TDC.REF_ID
                        ,CASE 
                        WHEN TDC.TYPE = 'V' THEN 'ข้อมูลผู้ขนส่ง' 
                        WHEN TDC.TYPE = 'E' THEN 'ข้อมูล พขร.' 
                        WHEN TDC.TYPE = 'C' THEN 'ข้อมูลสัญญา' 
                        WHEN TDC.TYPE = 'T' THEN 'Truck Component(TC)'
                        WHEN TDC.TYPE = 'H' THEN 'ข้อมูลรถ'
                        ELSE '' END as STYPE
                        ,TDC.DESCRIPTION, TDC.STATUS, TDC.DCREATE, TDC.SCREATE, TDC.DUPDATE, TDC.SUPDATE
                        ,VEN.SABBREVIATION,TDC.TYPE,TDC.SVENDORID,
                        CASE 
                        WHEN TDC.STATUS = '0' THEN 'ผขส.ขอเปลี่ยนข้อมูล' 
                        WHEN TDC.STATUS = '1' THEN 'รข. อนุมัติ'
                        WHEN TDC.STATUS = '2' THEN 'รข. ปฏิเสธ' 
                        WHEN TDC.STATUS = '3' THEN 'รข.ขอข้อมูลเพิ่มเติม' 
                        ELSE '' END as STATUSNAME
                        FROM TREQ_DATACHANGE TDC
                        LEFT JOIN TVENDOR VEN
                        ON TDC.SVENDORID = VEN.SVENDORID
                        WHERE 1=1 " + Condition + @"
                        ORDER BY DCREATE DESC";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        gvw.DataSource = dt;
        gvw.DataBind();
    }

   

    protected void cboSelect_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}