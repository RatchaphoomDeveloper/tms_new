﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.IO;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using System.Configuration;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using EmailHelper;
using TMS_BLL.Transaction.Appeal;
using TMS_BLL.Transaction.Accident;
using System.Web.Security;
using GemBox.Spreadsheet;
using System.Drawing;

public partial class admin_suppliant_lst : PageBase
{
    #region + View State +

    private int IndexEdit
    {
        get
        {
            if ((int)ViewState["IndexEdit"] != null)
                return (int)ViewState["IndexEdit"];
            else
                return -1;
        }
        set
        {
            ViewState["IndexEdit"] = value;
        }
    }

    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }

    private string DocIDEdit
    {
        get
        {
            if ((string)ViewState["DocIDEdit"] != null)
                return (string)ViewState["DocIDEdit"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocIDEdit"] = value;
        }
    }

    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }

    private DataTable dtExportData
    {
        get
        {
            if ((DataTable)ViewState["dtExportData"] != null)
                return (DataTable)ViewState["dtExportData"];
            else
                return null;
        }
        set
        {
            ViewState["dtExportData"] = value;
        }
    }

    private DataTable dtHeader
    {
        get
        {
            if ((DataTable)ViewState["dtHeader"] != null)
                return (DataTable)ViewState["dtHeader"];
            else
                return null;
        }
        set
        {
            ViewState["dtHeader"] = value;
        }
    }

    private DataTable dtScore
    {
        get
        {
            if ((DataTable)ViewState["dtScore"] != null)
                return (DataTable)ViewState["dtScore"];
            else
                return null;
        }
        set
        {
            ViewState["dtScore"] = value;
        }
    }

    private DataTable dtScoreList
    {
        get
        {
            if ((DataTable)ViewState["dtScoreList"] != null)
                return (DataTable)ViewState["dtScoreList"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreList"] = value;
        }
    }

    private DataTable dtScoreListCar
    {
        get
        {
            if ((DataTable)ViewState["dtScoreListCar"] != null)
                return (DataTable)ViewState["dtScoreListCar"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreListCar"] = value;
        }
    }

    private DataTable dtDup
    {
        get
        {
            if ((DataTable)ViewState["dtDup"] != null)
                return (DataTable)ViewState["dtDup"];
            else
                return null;
        }
        set
        {
            ViewState["dtDup"] = value;
        }
    }

    private DataTable dtCusScoreType
    {
        get
        {
            if ((DataTable)ViewState["dtCusScoreType"] != null)
                return (DataTable)ViewState["dtCusScoreType"];
            else
                return null;
        }
        set
        {
            ViewState["dtCusScoreType"] = value;
        }
    }

    private DataTable dtCarDistinct
    {
        get
        {
            if ((DataTable)ViewState["dtCarDistinct"] != null)
                return (DataTable)ViewState["dtCarDistinct"];
            else
                return null;
        }
        set
        {
            ViewState["dtCarDistinct"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    DataTable dt;
    DataSet ds;
    DataRow dr;
    #endregion

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/Suppliant/{0}/{2}/{1}/";
    const int ThumbnailSize = 100;
    int def_Int = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        this.CheckPostBack();

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            #region Check Permission
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            #endregion
            string gtest = Session["CGROUP"].ToString();
            if (Session["CGROUP"] + string.Empty == ConfigValue.UserGroup1.ToString())
            {
                Response.Redirect("~/vendor_appeal_lst.aspx"); //หน้านี้สำหรับ Cgroup ทีเป็น 0
                return;
            }

            Session["SAPPEALID"] = null;
            //dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            //dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy");
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            string str = Request.QueryString["str"];
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                hideAppealID.Text = "" + strQuery[0];
                txtSearch.Text = "" + strQuery[1];
                dteStart.Text = dteEnd.Text = Convert.ToDateTime("" + strQuery[3]).ToString("dd/MM/yyyy");

            }

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            LogUser("24", "R", "เปิดดูข้อมูลหน้า ข้อมูลยื่นอุทธรณ์", "");

            DataTable dtScore = ComplainBLL.Instance.ScoreSelectBLL();
            DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);

            //this.AssignAuthen();
        }
        if (rblStatus4 != null && rblStatus4.Value != null && Session["rblStatus"] == null)
        {
            Session["rblStatus"] = rblStatus4.Value;
        }
        if (rblCSENTENCE4 != null && rblCSENTENCE4.Value != null && Session["rblCSENTENCE4"] == null)
        {
            Session["rblCSENTENCE4"] = rblCSENTENCE4.Value;
        }

        if ((Session["dtUpload"] != null) && (lblAttachTotal != null))
        {
            DataTable dtUpload = (DataTable)Session["dtUpload"];
            lblAttachTotal.Text = string.Format(lblAttachTotal.Text, dtUpload.Rows.Count.ToString());
        }
        else if (lblAttachTotal != null)
            lblAttachTotal.Text = string.Format(lblAttachTotal.Text, "0");
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {
        if (Session["CGROUP"] + string.Empty != "0")
        {
            BindData();
        }


    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    private void BlackListDriver(DataTable dtDriverDetail, string Status)
    {
        try
        {
            SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
            {
                DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                DRV_STATUS = Status
            };

            string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
            string resultCheck = result.Substring(0, 1);

            if (string.Equals(resultCheck, "N"))
                throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        try
        {
            string[] paras = e.Parameter.Split(';');
            int VisibleIndex = (paras.Length > 1) ? (int.TryParse(paras[1] + "", out def_Int) ? int.Parse(paras[1] + "") : 0) : 0;
            switch (paras[0])
            {
                case "Search":
                    Cache.Remove(sds.CacheKeyDependency);
                    Cache[sds.CacheKeyDependency] = new object();
                    sds.Select(new System.Web.UI.DataSourceSelectArguments());
                    sds.DataBind();
                    break;

                case "edit":
                    Session["dtScoreList"] = null;

                    int Index = int.Parse(e.Parameter.Split(';')[1]);
                    gvw.StartEdit(Index);

                    Session["IndexEdit"] = Index;

                    string cStatus = gvw.GetRowValues(Index, "") + "";
                    string id = gvw.GetRowValues(Index, "SAPPEALID") + "";
                    string cTYPE = gvw.GetRowValues(Index, "CHKSTATUS") + "";
                    Session["SAPPEALID"] = id;
                    ListData(id, cTYPE);
                    //hideCheckFinal.Text = "0";
                    VisibleTabControl();

                    DataTable dtUpload = AppealBLL.Instance.ImportFileSelectBLL(id, "COMPLAIN_APPEAL");
                    Session["dtUpload"] = dtUpload;
                    GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
                    lblAttachTotal.Text = string.Format(lblAttachTotal.Text, dtUpload.Rows.Count.ToString());

                    break;
                case "Save":
                    #region < TabPage Name="t4" Text="รข.รับเรื่อง/พิจารณา" >
                    //if ("" + Session["CheckPermission"] == "1")
                    if (CanWrite)
                    {
                        this.ValidateAppealSave();

                        ASPxPageControl PageControl = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                        ASPxRadioButtonList rblCSENTENCE4 = (ASPxRadioButtonList)PageControl.FindControl("rblCSENTENCE4");
                        ASPxRadioButtonList rblStatus4 = (ASPxRadioButtonList)PageControl.FindControl("rblStatus4");
                        ASPxMemo txtData4 = (ASPxMemo)PageControl.FindControl("txtData4");
                        ASPxMemo txtWaitDocument = (ASPxMemo)PageControl.FindControl("txtWaitDocument");
                        ASPxComboBox cmbSENTENCERNAME1 = (ASPxComboBox)PageControl.FindControl("cmbSENTENCERNAME");
                        ASPxLabel lblNo = (ASPxLabel)PageControl.FindControl("lblNo");
                        dynamic GridData = gvw.GetRowValues(VisibleIndex, "SAPPEALID", "DINCIDENT", "SVENDORID", "SVENDORNAME", "SPROCESSNAME", "STOPICNAME", "SHEADREGISTERNO", "STRAILREGISTERNO", "NPOINT", "STATUS", "SAPPEALNO", "SPROCESSID", "BLACKLIST");
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strsql = "UPDATE TAPPEAL SET SSENTENCERCODE = :SSENTENCERCODE, DSENTENCE = SYSDATE,SWAITDOCUMENT = :SWAITDOCUMENT,CSTATUS = :CSTATUS,CSENTENCE = :CSENTENCE,SSENTENCETEXT = :SSENTENCETEXT,SSENTENCER = :SSENTENCER  WHERE  SAPPEALID = :SAPPEALID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();

                                com.Parameters.Add(":CSENTENCE", OracleType.Char).Value = rblCSENTENCE4.Value + "";

                                /*
                                 รับเรื่องอุทธรณ์ (รข. พิจารณาเอง)" Value="1" 
                                 รับเรื่องอุทธรณ์ (ส่งให้คณะกรรมการพิจารณา)" Value="2"
                                 ไม่รับการอุทธรณ์" Value="3"
                                 ขอเอกสารเพิ่มเติม" Value="4"
                                 */
                                string sSubject = "", sStep = "", sStatus = "", cStat = "", sHeaderLink = "", sDetail = "", sMode = "";
                                if (rblCSENTENCE4.Value == "1")
                                {//รับเรื่องอุทธรณ์ (รข. พิจารณาเอง)
                                    if (rblStatus4.Value == "1" /*|| rblStatus4.Value == "2"*/)
                                    {//ถูก
                                        com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "3";
                                        com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = rblStatus4.Value + "";
                                        com.Parameters.Add(":SWAITDOCUMENT", OracleType.VarChar).Value = "";
                                        sSubject = "[TMS]:แจ้งผลพิจารณาการขอยื่นอุทธรณ์ เลขที่ " + GridData[10];
                                        sStep = "";//ผลการขออุทธรณ์
                                        sStatus = "พิจารณาตัดสิน(ถูก)";
                                        sHeaderLink = "";
                                        sDetail = "";
                                        sMode = "RK2VD4REPORT_TRUE";
                                        cStat = "3";

                                        if (rblStatus4.Value == "1")
                                        {
                                            string AppealID = GridData[0] + "";
                                            string DeliveryDate = GridData[1] + "";

                                            string strsql2 = @"UPDATE TREDUCEPOINT
                                                               SET NPOINT = 0, COST = 0, DISABLE_DRIVER = 0, BLACKLIST = 0
                                                               WHERE SREFERENCEID = (SELECT SREFERENCEID FROM TAPPEAL WHERE SAPPEALID =:I_SAPPEALID)
                                                               AND TO_CHAR(DREDUCE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') =:I_DINCIDENT";
                                            using (OracleCommand com2 = new OracleCommand(strsql2, con))
                                            {
                                                com2.Parameters.Clear();
                                                com2.Parameters.Add(":I_SAPPEALID", OracleType.Char).Value = AppealID;
                                                com2.Parameters.Add(":I_DINCIDENT", OracleType.Char).Value = DeliveryDate;
                                                com2.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                    else
                                    {//ผิด
                                        com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "3";
                                        com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = rblStatus4.Value + "";
                                        com.Parameters.Add(":SWAITDOCUMENT", OracleType.VarChar).Value = "";
                                        sSubject = "[TMS]:แจ้งผลพิจารณาการขอยื่นอุทธรณ์ เลขที่ " + GridData[10];
                                        sStep = "";//ผลการขออุทธรณ์
                                        sStatus = "พิจารณาตัดสิน(ผิด)";
                                        sHeaderLink = ""; sDetail = "";
                                        sMode = "RK2VD4REPORT_FALSE";
                                        cStat = "3";
                                    }

                                    //AppealBLL.Instance.UpdateStatusComplainBLL(GridData[5] + "", ConfigValue.DocStatus7);
                                }
                                else if (rblCSENTENCE4.Value == "2")
                                {//รับเรื่องอุทธรณ์ (ส่งให้คณะกรรมการพิจารณา)
                                    com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "5";
                                    com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = "";
                                    com.Parameters.Add(":SWAITDOCUMENT", OracleType.VarChar).Value = "";
                                    sSubject = "[TMS]:แจ้งนัดพิจารณาการขอยื่นอุทธรณ์ เลขที่ " + GridData[10];
                                    sStep = "";
                                    sStatus = "รอคณะกรรมการพิจารณา";
                                    sHeaderLink = ""; sDetail = "โดย " + cmbSENTENCERNAME1.Text;
                                    sMode = "RK2VD4FROWORDING";
                                    cStat = "5";
                                }
                                else if (rblCSENTENCE4.Value == "3")
                                {//ไม่รับการอุทธรณ์ waiting
                                    com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "6";
                                    com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = "";
                                    com.Parameters.Add(":SWAITDOCUMENT", OracleType.VarChar).Value = "";
                                    sSubject = "[TMS]:แจ้งผลการขอยื่นอุทธรณ์ เลขที่ " + GridData[10];
                                    sStep = "";//ผลการขออุทธรณ์
                                    sStatus = "ไม่รับการอุทธรณ์ ตัดสิน(ผิด)";
                                    sMode = "RK2VD4REJECT";
                                    sHeaderLink = ""; sDetail = "";
                                    cStat = "6";
                                }
                                else if (rblCSENTENCE4.Value == "4")
                                {
                                    #region //ขอเอกสารเพิ่มเติม success!
                                    com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "2";
                                    com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = "";
                                    com.Parameters.Add(":SWAITDOCUMENT", OracleType.VarChar).Value = txtWaitDocument.Text;

                                    sSubject = "[TMS]:แจ้งขอเอกสารเพิ่มเติมประกอบการขอยื่นอุทธรณ์ เลขที่ " + GridData[10];
                                    sStep = "รายละเอียดเอกสารเพิ่มเติม";
                                    sStatus = "ขอเอกสารเพิ่มเติม";
                                    sMode = "NEEDDOC2VD4ATTHDOC";
                                    sHeaderLink = "คลิีกเพื่อเข้าสู่ระบบและส่งเอกสารเพิ่มเติม คลิ๊กที่นี้";
                                    cStat = "2"; sDetail = txtWaitDocument.Text.Replace("\r\n", "<br />") + "";
                                    #endregion
                                }

                                com.Parameters.Add("SSENTENCERCODE", OracleType.VarChar).Value = "" + rblCSENTENCE4.Value == "2" ? cmbSENTENCERNAME1.Value + "" : "";
                                com.Parameters.Add(":SSENTENCETEXT", OracleType.VarChar).Value = txtData4.Text;
                                com.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = Session["SAPPEALID"] + "";

                                com.ExecuteNonQuery();

                                #region ข้อร้องเรียน
                                if ((rblCSENTENCE4.Value == "1") && string.Equals(GridData[11] + "", "080") && rblStatus4.Value == "3")
                                {//รับเรื่องอุทธรณ์ (รข. พิจารณาเอง)
                                    if (dtScoreList.Rows.Count == 0)
                                    {
                                        alertFail("กรุณาเลือกหัวข้อตัดคะแนน");
                                        return;
                                    }
                                    string AppealID = Session["SAPPEALID"].ToString();
                                    DataTable dtAppeal = AppealBLL.Instance.AppealSelectBLL(AppealID);
                                    string ComplainDocID = dtAppeal.Rows[0]["SREFERENCEID"].ToString();

                                    int Blacklist = (chkBlacklist.Checked) ? 1 : 0;
                                    decimal CostFinal = 0, CostOther = 0, FinalCost = 0;
                                    CostFinal = (!string.Equals(txtCostFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostFinal.Text.Trim()) : 0;
                                    CostOther = (!string.Equals(txtCostOther.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostOther.Text.Trim()) : 0;
                                    if (CostFinal == 0 && CostOther == 0)
                                        FinalCost = -1;
                                    else
                                        FinalCost = CostFinal + CostOther;

                                    ComplainImportFileBLL.Instance.ComplainTab3AppealSaveBLL(ComplainDocID, int.Parse(Session["UserID"].ToString()), (radCusScoreType.SelectedIndex > -1) ? int.Parse(radCusScoreType.SelectedValue) : -1, (!string.Equals(txtPointFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtPointFinal.Text.Trim()) : -1, FinalCost, (!string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? int.Parse(txtDisableFinal.Text.Trim()) : -1, dtScoreList, dtScoreListCar, decimal.Parse(txtCostOther.Text.Trim()), 0, txtRemarkTab3.Text.Trim(), decimal.Parse(txtOilLose.Text.Trim()), AppealID, Blacklist);

                                    AppealBLL.Instance.ComplainCheckStatusBLL();
                                    this.SendEmail(ConfigValue.EmailComplainAppealConfirm, GridData[5]);
                                }
                                else if ((rblCSENTENCE4.Value == "1") && string.Equals(GridData[11] + "", "080"))
                                {
                                    AppealBLL.Instance.ComplainCheckStatusBLL();
                                    this.SendEmail(ConfigValue.EmailComplainAppealConfirm, GridData[5]);
                                }

                                if (string.Equals(GridData[11] + "", "080"))
                                {//ถ้าเป็นข้อร้องเรียน ให้เซฟ Attach File
                                    string AppealID = Session["SAPPEALID"].ToString();
                                    AppealBLL.Instance.AppealSaveImportFileBLL((DataTable)Session["dtUpload"], AppealID, "COMPLAIN_APPEAL", int.Parse(Session["UserID"].ToString()));
                                }

                                //Blacklist
                                if ((rblCSENTENCE4.Value == "1") && string.Equals(GridData[11] + "", "080") && (rblStatus4.Value == "1" || rblStatus4.Value == "3"))
                                {
                                    string DocID = GridData[5] + "";
                                    DataTable dtHeader = ComplainBLL.Instance.ComplainSelectHeaderBLL(" AND TCOMPLAIN.DOC_ID = '" + DocID + "'");
                                    bool BlackListOld = (string.Equals(GridData[12] + "", "Y") ? true : false);
                                    DataTable dtDriverDetail;

                                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                                    {
                                        if (!string.Equals(dtHeader.Rows[i]["PERSONALID"].ToString(), string.Empty))
                                        {
                                            //dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dtHeader.Rows[i]["PERSONALID"].ToString());

                                            //if ((rblStatus4.Value == "1") && BlackListOld)
                                            //{//ตัดสินคืนคะแนน ให้ปลดล็อค
                                            //    ComplainBLL.Instance.UpdateDriverStatusBLL(dtHeader.Rows[i]["PERSONALID"].ToString(), "1", " ");
                                            //    this.BlackListDriver(dtDriverDetail, ConfigValue.DriverEnable);
                                            //}
                                            //else if ((rblStatus4.Value == "3") && BlackListOld && !chkBlacklist.Checked)
                                            //{//เปลี่ยนคำตัดสิน ให้ปลดล็อค
                                            //    ComplainBLL.Instance.UpdateDriverStatusBLL(dtHeader.Rows[i]["PERSONALID"].ToString(), "1", " ");
                                            //    this.BlackListDriver(dtDriverDetail, ConfigValue.DriverEnable);
                                            //}
                                            //else if ((rblStatus4.Value == "3") && !BlackListOld && chkBlacklist.Checked)
                                            //{//เปลี่ยนคำตัดสิน ให้ล็อค
                                            //    ComplainBLL.Instance.UpdateDriverStatusBLL(dtHeader.Rows[i]["PERSONALID"].ToString(), "2", "2");
                                            //    this.BlackListDriver(dtDriverDetail, ConfigValue.DriverOut);
                                            //}
                                        }
                                    }
                                }
                                #endregion

                                #region อุบัติเหตุ
                                if ((rblCSENTENCE4.Value == "1") && string.Equals(GridData[11] + "", "090") && rblStatus4.Value == "3")
                                {//รับเรื่องอุทธรณ์ (รข. พิจารณาเอง)
                                    if (dtScoreList.Rows.Count == 0)
                                    {
                                        alertFail("กรุณาเลือกหัวข้อตัดคะแนน");
                                        return;
                                    }
                                    string AppealID = Session["SAPPEALID"].ToString();
                                    DataTable dtAppeal = AppealBLL.Instance.AppealSelectBLL(AppealID);
                                    string ACCIDENT_ID = dtAppeal.Rows[0]["SREFERENCEID"].ToString();

                                    int Blacklist = (chkBlacklist.Checked) ? 1 : 0;
                                    DataSet dsScoreList = new DataSet("DS");
                                    dtScoreList.TableName = "DT";
                                    dsScoreList.Tables.Add(dtScoreList.Copy());
                                    DataSet dsScoreListCar = new DataSet("DS");
                                    dtScoreListCar.TableName = "DT";
                                    dsScoreListCar.Tables.Add(dtScoreListCar.Copy());
                                    //CACTIVE = -2 ใน sto คำนวน
                                    decimal CostFinal = 0, CostOther = 0, FinalCost = 0;
                                    CostFinal = (!string.Equals(txtCostFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostFinal.Text.Trim()) : 0;
                                    CostOther = (!string.Equals(txtCostOther.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostOther.Text.Trim()) : 0;
                                    if (CostFinal == 0 && CostOther == 0)
                                        FinalCost = -1;
                                    else
                                        FinalCost = CostFinal + CostOther;
                                    AccidentBLL.Instance.AccidentAppealSave(ACCIDENT_ID, -2, Session["UserID"] + string.Empty, (radCusScoreType.SelectedIndex > -1) ? int.Parse(radCusScoreType.SelectedValue) : -1, (!string.Equals(txtPointFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtPointFinal.Text.Trim()) : -1, FinalCost, (!string.Equals(txtDisableFinal.Text.Trim(), string.Empty))
                ? (string.Equals(txtDisableFinal.Text.Trim(), "Hold") ? -2 : int.Parse(txtDisableFinal.Text.Trim())) : -1, dsScoreList, dsScoreListCar, decimal.Parse(txtCostOther.Text.Trim()), 0, txtRemarkTab3.Text.Trim(), decimal.Parse(txtOilLose.Text.Trim()), AppealID, Blacklist);

                                    DataTable dts = AccidentBLL.Instance.AccidentTab1Select(ACCIDENT_ID);
                                    if (dts != null && dts.Rows.Count > 0)
                                    {
                                        DataRow dr = dts.Rows[0];
                                        string Detail = string.Empty;
                                        Detail += "รถขนส่งเกิดอุบัติเหตุ หมายเลขเอกสาร " + ACCIDENT_ID;
                                        string cactive = "1", drvstatus = ConfigValue.DriverEnable;

                                        if (string.Equals(txtDisableFinal.Text.Trim(), "Hold") || !string.Equals(txtDisableFinal.Text.Trim(), "0"))
                                        {
                                            cactive = "0";
                                            drvstatus = ConfigValue.DriverDisable;
                                            Detail += "<br/>สถานะการทำงาน : ระงับการทำงาน";
                                        }
                                        else
                                        {
                                            Detail += "<br/>สถานะการทำงาน : อนุญาต";
                                        }

                                        Detail += "<br/>หมายเหตุ : ตรวจสอบรถขนส่งเกิดอุบัติเหตุเรียบร้อย(อุทธรณ์)";
                                        VendorBLL.Instance.LogInsert(Detail, dr["PERS_CODE"] + string.Empty, null, "0", "0", int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, 0);

                                        DataTable dtDriverDetail;

                                        if (!string.Equals(dr["SEMPLOYEEID"] + string.Empty, string.Empty))
                                        {
                                            dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dr["SEMPLOYEEID"] + string.Empty);

                                            ComplainBLL.Instance.UpdateDriverStatusBLL(dr["SEMPLOYEEID"] + string.Empty, cactive, drvstatus);

                                            SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                                            {
                                                DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                                                PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                                                LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                                                LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
                                                LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
                                                Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                                                Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                                                FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                                                LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                                                CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                                                DRV_STATUS = ConfigValue.DriverOut
                                            };

                                            string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                                            string resultCheck = result.Substring(0, 1);

                                            if (string.Equals(resultCheck, "N"))
                                                throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                                        }


                                    }

                                    //this.SendEmail(ConfigValue.EmailComplainAppealConfirm, GridData[5]);
                                }
                                else if ((rblCSENTENCE4.Value == "1") && string.Equals(GridData[11] + "", "090"))
                                {
                                    int cactive = -2;
                                    string AppealID = Session["SAPPEALID"].ToString();
                                    DataTable dtAppeal = AppealBLL.Instance.AppealSelectBLL(AppealID);
                                    string ACCIDENT_ID = dtAppeal.Rows[0]["SREFERENCEID"].ToString();
                                    AccidentBLL.Instance.UpdateStatus(ACCIDENT_ID, cactive, Session["UserID"] + string.Empty);
                                    //AppealBLL.Instance.ComplainCheckStatusBLL();
                                    //this.SendEmail(ConfigValue.EmailComplainAppealConfirm, GridData[5]);
                                }
                                this.SendEmail(ConfigValue.EmailAccident8, GridData[5]);
                                if (string.Equals(GridData[11] + "", "090"))
                                {//ถ้าเป็นข้อร้องเรียน ให้เซฟ Attach File
                                    string AppealID = Session["SAPPEALID"].ToString();
                                    AppealBLL.Instance.AppealSaveImportFileBLL((DataTable)Session["dtUpload"], AppealID, "COMPLAIN_APPEAL", int.Parse(Session["UserID"].ToString()));
                                }

                                //Blacklist
                                //if ((rblCSENTENCE4.Value == "1") && string.Equals(GridData[11] + "", "090") && (rblStatus4.Value == "1" || rblStatus4.Value == "3"))
                                //{
                                //    string DocID = GridData[5] + "";
                                //}
                                #endregion

                                #region//Send Mail
                                /*
                             * 0    "SAPPEALID"
                             * 1    "DINCIDENT"
                             * 2    "SVENDORID"
                             * 3    "SVENDORNAME"
                             * 4    "SPROCESSNAME"
                             * 5    "STOPICNAME"
                             * 6    "SHEADREGISTERNO"
                             * 7    "STRAILREGISTERNO"
                             * 8    "NPOINT"
                             * 9    "STATUS"
                             * 10   "SAPPEALNO"
                             * 11    "SPROCESSID"
                             */
                                string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
                                    , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
                                    , _toid = "55,40", _refid = "" + GridData[0], _status = "" + GridData[9], _usr_acc = "anucha.p,sysadmin";
                                string[] _Url = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
                                if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1")
                                {
                                    DataTable dt_usr = CommonFunction.Get_Data(con, "SELECT wm_concat(TUSR.SUID) usr_recv ,wm_concat(TUSR.SUSERNAME ) usr_acc,wm_concat(TUSR.SEMAIL) email_recv FROM TPERMISSION PRMS  LEFT JOIN TMENU MENU ON PRMS.SMENUID = MENU.SMENUID  LEFT JOIN TUSER TUSR ON TUSR.SUID=PRMS.SUID WHERE 1=1 AND MENU.SMENUID='4' AND PRMS.MAIL2ME ='1' AND SVENDORID='" + "" + GridData[2] + "'");
                                    if (dt_usr.Rows.Count > 0)
                                    {
                                        _to = "" + dt_usr.Rows[0]["email_recv"];
                                        _toid = "" + dt_usr.Rows[0]["usr_recv"];
                                        _usr_acc = "" + dt_usr.Rows[0]["usr_acc"];
                                    }
                                }
                                /*
                                 * {1}:work's mode
                                 * {2}:work's id
                                 * {3}:url for forword
                                 * {4}:params for forword format:{[],[],[],[][],[],[],[]}
                                 * {5}:user recieve
                                 */
                                string sEncrypt = "mode$" + sMode + "&RefID$" + _refid + "&URL$vendor_appeal_lst&SAPPEALID$" + _refid + "&SHEADREGISTERNO$" + GridData[6] + "&STRAILREGISTERNO$" + GridData[7] + "&DINCIDENT$" + GridData[1] + "&CSTATUS$" + cStat + "&CAPPEAL$" + rblCSENTENCE4.Value + "&SPROCESSID$" + GridData[11] + "&usr_recv$" + _toid + "&usr_acc$" + _usr_acc + ""
                                    , link = ((sHeaderLink == "") ? "" : "" + sHeaderLink + " :<a target=_blank href='http://" + HttpContext.Current.Request.Url.Host + "/" + _Url[_Url.Length - 2] + "/" + "bypassmail2system.aspx?proc=" +
                                    Server.UrlEncode(STCrypt.Encrypt(sEncrypt)) + "' >ตกลง</a>");

                                #region massage
                                string messageMail = @"<table>
<tr>
            <td style='width:10%; white-space:nowrap;background-color:#F1F1F1;'>เลขที่การอุทธรณ์ : </td>
            <td style=' width:30%;'>{10}</td>
            <td style=' width:10%; '></td>
            <td style=' width:30%'></td>
        </tr>
        <tr>
            <td style='width:10%; white-space:nowrap;background-color:#F1F1F1;'>ประเภทการอุทธรณ์ : </td>
            <td style=' width:30%;'>{0}</td>
            <td style=' width:10%; white-space:nowrap;background-color:#F1F1F1;'>วันที่เกิดเหตุ : </td>
            <td style=' width:30%'>{1}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>หัวข้อปัญหา : </td>
            <td>{2}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>คะแนนที่ถูกตัด : </td>
            <td>{3}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>ทะเบียน(หัว) : </td>
            <td>{4}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>ทะเบียน(ท้าย) : </td>
            <td>{5}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>สถานะการอุทธรณ์ : </td>
            <td>{6}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>สถานะหลังการอุทธรณ์ : </td>
            <td>{7}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;' colspan='2'>{11} </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan='4'>{8}</td>
        </tr>
        <tr>
            <td colspan='4'>{9}</td>
        </tr>
    </table>";
                                #endregion
                                /**/
                                CommonFunction.SendNetMail(_from, _to, sSubject
                                    , string.Format(messageMail, GridData[4] + "", GridData[1] + "", GridData[5] + "", GridData[8] + "", GridData[6] + "", GridData[7] + ""
                                    , sStatus, "-", sDetail, "" + link, "" + GridData[10], "" + sStep)
                                    , con, ""
                                    , "", "", "" + _toid, "" + _refid, "0");
                                #endregion
                            }
                        }
                        //return;

                        if ("" + rblCSENTENCE4.Value == "1")
                        {
                            if ("" + rblStatus4.Value == "1")
                            {
                                SetActiveReducePoint(false);
                            }
                            else if ("" + rblStatus4.Value == "2")
                            {
                                SetActiveReducePoint(true);
                            }
                            else if ("" + rblStatus4.Value == "3")
                            {
                                SetActiveReducePoint(true);
                            }
                        }

                        EndSave(); /**/
                    }
                    else if (Session["CGROUP"] != "2")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssCheckPermissionExpire", "window.location='default.aspx';", true); return;
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    #endregion
                    break;
                case "Save1":
                    #region//< TabPage Name="t5" Text="คณะกรรมการพิจารณาฯ" >
                    if ("" + Session["CheckPermission"] == "1")
                    //if (CanWrite)
                    {
                        int Index2 = int.Parse(e.Parameter.Split(';')[1]);

                        ASPxPageControl PageControl1 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                        ASPxCheckBoxList chkSentencer5 = (ASPxCheckBoxList)PageControl1.FindControl("chkSentencer5");
                        ASPxRadioButtonList rblStatus5 = (ASPxRadioButtonList)PageControl1.FindControl("rblStatus5");
                        ASPxMemo txtDetail5 = (ASPxMemo)PageControl1.FindControl("txtDetail5");
                        dynamic GridData = gvw.GetRowValues(VisibleIndex, "SAPPEALID", "DINCIDENT", "SVENDORID", "SVENDORNAME", "SPROCESSNAME", "STOPICNAME", "SHEADREGISTERNO", "STRAILREGISTERNO", "NPOINT", "STATUS", "SAPPEALNO", "SPROCESSID");
                        string sSubject = "", sStep = "", sStatus = "", cStat = "", sHeaderLink = "", sDetail = "", sMode = "";
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strsql = "UPDATE TAPPEAL SET DSENTENCER = SYSDATE,CSTATUS = '3',SSENTENCERTEXT = :SSENTENCERTEXT,SSENTENCER = :SSENTENCER  WHERE  SAPPEALID = :SAPPEALID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = rblStatus5.Value + "";
                                com.Parameters.Add(":SSENTENCERTEXT", OracleType.VarChar).Value = txtDetail5.Text;
                                com.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = Session["SAPPEALID"] + "";
                                com.ExecuteNonQuery();
                                #region //Send Mail
                                /*
                             * 0    "SAPPEALID"
                             * 1    "DINCIDENT"
                             * 2    "SVENDORID"
                             * 3    "SVENDORNAME"
                             * 4    "SPROCESSNAME"
                             * 5    "STOPICNAME"
                             * 6    "SHEADREGISTERNO"
                             * 7    "STRAILREGISTERNO"
                             * 8    "NPOINT"
                             * 9    "STATUS"
                             * 10   "SAPPEALNO"
                             * 11    "SPROCESSID"
                             */

                                sSubject = "[TMS]:แจ้งผลการขอยื่นอุทธรณ์ เลขที่ " + GridData[10];
                                sStep = "";
                                sStatus = "คณะกรรมการพิจารณาตัดสิน(" + (rblStatus5.Value == "1" ? "ถูก" : "ผิด") + ")";
                                sMode = "RK2VD4REJECT";
                                sHeaderLink = ""; sDetail = "";
                                cStat = "3";

                                string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
                                    , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
                                    , _toid = "55,40", _refid = "" + GridData[0], _status = "" + GridData[9], _usr_acc = "anucha.p,sysadmin";
                                string[] _Url = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
                                if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1")
                                {
                                    DataTable dt_usr = CommonFunction.Get_Data(con, "SELECT wm_concat(TUSR.SUID) usr_recv ,wm_concat(TUSR.SUSERNAME ) usr_acc,wm_concat(TUSR.SEMAIL) email_recv FROM TPERMISSION PRMS  LEFT JOIN TMENU MENU ON PRMS.SMENUID = MENU.SMENUID  LEFT JOIN TUSER TUSR ON TUSR.SUID=PRMS.SUID WHERE 1=1 AND MENU.SMENUID='4' AND PRMS.MAIL2ME ='1' AND SVENDORID='" + "" + GridData[2] + "'");
                                    if (dt_usr.Rows.Count > 0)
                                    {
                                        _to = "" + dt_usr.Rows[0]["email_recv"];
                                        _toid = "" + dt_usr.Rows[0]["usr_recv"];
                                        _usr_acc = "" + dt_usr.Rows[0]["usr_acc"];
                                    }
                                }
                                string sEncrypt = "mode$SENCETENTAPPROVE2VD4REPORT&RefID$" + _refid + "&URL$vendor_appeal_lst&SAPPEALID$" + _refid + "&SHEADREGISTERNO$" + GridData[6] + "&STRAILREGISTERNO$" + GridData[7] + "&DINCIDENT$" + GridData[1] + "&CSTATUS$" + cStat + "&CAPPEAL$x&SPROCESSID$" + GridData[11] + "&usr_recv$" + _toid + "&usr_acc$" + _usr_acc + ""
                                    , link = ((sHeaderLink == "") ? "" : "" + sHeaderLink + " :<a target=_blank href='http://" + HttpContext.Current.Request.Url.Host + "/" + _Url[_Url.Length - 2] + "/" + "bypassmail2system.aspx?proc=" +
                                    Server.UrlEncode(STCrypt.Encrypt(sEncrypt)) + "' >ตกลง</a>");

                                #region massage
                                string messageMail = @"<table>
<tr>
            <td style='width:10%; white-space:nowrap;background-color:#F1F1F1;'>เลขที่การอุทธรณ์ : </td>
            <td style=' width:30%;'>{10}</td>
            <td style=' width:10%; '></td>
            <td style=' width:30%'></td>
        </tr>
        <tr>
            <td style='width:10%; white-space:nowrap;background-color:#F1F1F1;'>ประเภทการอุทธรณ์ : </td>
            <td style=' width:30%;'>{0}</td>
            <td style=' width:10%; white-space:nowrap;background-color:#F1F1F1;'>วันที่เกิดเหตุ : </td>
            <td style=' width:30%'>{1}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>หัวข้อปัญหา : </td>
            <td>{2}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>คะแนนที่ถูกตัด : </td>
            <td>{3}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>ทะเบียน(หัว) : </td>
            <td>{4}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>ทะเบียน(ท้าย) : </td>
            <td>{5}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>สถานะการอุทธรณ์ : </td>
            <td>{6}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>สถานะหลังการอุทธรณ์ : </td>
            <td>{7}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;' colspan='2'>{11} </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan='4'>{8}</td>
        </tr>
        <tr>
            <td colspan='4'>{9}</td>
        </tr>
    </table>";
                                #endregion

                                CommonFunction.SendNetMail(_from, _to, sSubject
                                    , string.Format(messageMail, GridData[4] + "", GridData[1] + "", GridData[5] + "", GridData[8] + "", GridData[6] + "", GridData[7] + ""
                                    , sStatus, "-", sDetail, "" + link, "" + GridData[10], "" + sStep)
                                    , con, ""
                                    , "", "", "" + _toid, "" + _refid, "0");
                                #endregion
                            }

                            string strSentence = "INSERT INTO TAPPEAL_SENTENCER (SAPPEALID, NSENTENCE, SSENTENCERID,  SSENTENCERCODE) VALUES (:SAPPEALID,:NSENTENCE,:SSENTENCERID,:SSENTENCERCODE)";
                            int i = 1;
                            foreach (ListEditItem items in chkSentencer5.Items)
                            {
                                if (items.Selected == true)
                                {
                                    using (OracleCommand com1 = new OracleCommand(strSentence, con))
                                    {
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = Session["SAPPEALID"] + "";
                                        com1.Parameters.Add(":NSENTENCE", OracleType.Number).Value = i;
                                        com1.Parameters.Add(":SSENTENCERID", OracleType.VarChar).Value = items.Value;
                                        com1.Parameters.Add(":SSENTENCERCODE", OracleType.VarChar).Value = "";
                                        com1.ExecuteNonQuery();
                                        i++;
                                    }
                                }
                            }
                        }

                        if ("" + rblStatus5.Value == "1")
                        {//ไม่มีความผิด
                            SetActiveReducePoint(false);
                        }
                        else if ("" + rblStatus5.Value == "2")
                        {
                            SetActiveReducePoint(true);
                        }
                        EndSave();
                        this.SendEmail(ConfigValue.EmailComplainAppealConfirm, GridData[5]);
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    #endregion
                    break;

                case "Save2":
                    if ("" + Session["CheckPermission"] == "1")
                    //if (CanWrite)
                    {
                        ASPxPageControl PageControl2 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                        ASPxCheckBox chkChange6 = (ASPxCheckBox)PageControl2.FindControl("chkChange6");
                        ASPxMemo txtDetail6 = (ASPxMemo)PageControl2.FindControl("txtDetail6");
                        ASPxRadioButtonList rblStatus6 = (ASPxRadioButtonList)PageControl2.FindControl("rblStatus6");
                        ASPxTextBox txtEvidence = (ASPxTextBox)PageControl2.FindControl("txtEvidence");
                        ASPxTextBox txtFileName = (ASPxTextBox)PageControl2.FindControl("txtFileName");
                        ASPxTextBox txtFilePath = (ASPxTextBox)PageControl2.FindControl("txtFilePath");
                        ASPxButton btnView = (ASPxButton)PageControl2.FindControl("btnView");
                        ASPxButton btnDelFile = (ASPxButton)PageControl2.FindControl("btnDelFile");
                        ASPxTextBox txtName6 = (ASPxTextBox)PageControl2.FindControl("txtName6");
                        ASPxTextBox txtOldStatus = (ASPxTextBox)PageControl2.FindControl("txtOldStatus");

                        if (chkChange6.Checked)
                        {
                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TAPPEAL SET CSTATUS = '3', SSENTENCER = :SSENTENCER WHERE  SAPPEALID = :SAPPEALID";
                                string strHistory = @"INSERT INTO TAPPEALRESULT (SAPPEALID, NCHANGE, SBECUASE,SSENTENCE, SAPPROVEDOC, DCHANGE, SAPPROVEBY, SFILENAME, SFILEPATH) VALUES (:SAPPEALID,
                                    :NCHANGE,:SBECUASE,:SSENTENCE,:SAPPROVEDOC,SYSDATE,:SAPPROVEBY,:SFILENAME,:SFILEPATH)";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SSENTENCER", OracleType.Char).Value = rblStatus6.Value + "";
                                    com.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = Session["SAPPEALID"] + "";
                                    com.ExecuteNonQuery();
                                }

                                string GenNO = CommonFunction.Gen_ID(con, "SELECT NCHANGE FROM (SELECT NCHANGE FROM TAPPEALRESULT WHERE SAPPEALID = '" + Session["SAPPEALID"] + "' ORDER BY NCHANGE DESC) WHERE ROWNUM <= 1");

                                using (OracleCommand com1 = new OracleCommand(strHistory, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = Session["SAPPEALID"] + "";
                                    com1.Parameters.Add(":NCHANGE", OracleType.Number).Value = GenNO;
                                    com1.Parameters.Add(":SBECUASE", OracleType.VarChar).Value = txtDetail6.Text;
                                    com1.Parameters.Add(":SSENTENCE", OracleType.VarChar).Value = txtOldStatus.Text;
                                    com1.Parameters.Add(":SAPPROVEDOC", OracleType.VarChar).Value = txtEvidence.Text;
                                    com1.Parameters.Add(":SAPPROVEBY", OracleType.VarChar).Value = txtName6.Text;
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                    com1.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath.Text;
                                    com1.ExecuteNonQuery();
                                }
                            }

                            if ("" + rblStatus6.Value == "1")
                            {
                                SetActiveReducePoint(false);
                            }
                            else if ("" + rblStatus6.Value == "2")
                            {
                                SetActiveReducePoint(true);
                            }

                            EndSave();
                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;

                case "imbClick":

                    ASPxPageControl PageControl3 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                    ASPxComboBox cmbSENTENCERNAME = (ASPxComboBox)PageControl3.FindControl("cmbSENTENCERNAME");
                    ASPxButton imbView = (ASPxButton)PageControl3.FindControl("imbView");

                    string sqlsentencer = "SELECT FNAME,SJOB FROM TSENTENCER WHERE CACTIVE = '1' AND SSENTENCERCODE = '" + cmbSENTENCERNAME.Value + "'";

                    DataTable dt = new DataTable();
                    dt = CommonFunction.Get_Data(sql, sqlsentencer);

                    if (dt.Rows.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();

                        sb.Append("<table width='100%' border='0' cellspacing='2' cellpadding='5'>");

                        string formatPopup = @" <tr><td align='left'> ลำดับที่ {0}  ชื่อ คุณ {1}  ตำแหน่ง {2} </td></tr>";
                        int i = 1;
                        foreach (DataRow dr in dt.Rows)
                        {

                            sb.Append(string.Format(formatPopup, i, dr["FNAME"] + "", dr["SJOB"] + ""));
                            i++;
                        }
                        sb.Append("</table>");

                        ((Literal)popupControl.FindControl("ltrContent")).Text = sb.ToString();
                        popupControl.ShowOnPageLoad = true;
                    }
                    cmbSENTENCERNAME.ClientVisible = true;
                    imbView.ClientVisible = true;
                    VisibleTabControl();
                    break;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    void EndSave()
    {
        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");
        gvw.CancelEdit();
        Cache.Remove(sds.CacheKeyDependency);
        Cache[sds.CacheKeyDependency] = new object();
        sds.Select(new System.Web.UI.DataSourceSelectArguments());
        sds.DataBind();
        gvw.DataBind();
    }
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    void BindData()
    {
        if (Session["CGROUP"] + string.Empty != "0" && !string.IsNullOrEmpty(dteStart.Text.Trim()) && !string.IsNullOrEmpty(dteEnd.Text.Trim()))
        {
            string strsql = "";
            string strstatus = "";
            string Status = cboStatus.Value + "";

            if (Status == "1")
            {
                strstatus = "AND (nvl(ap.CSTATUS,'0') = '1' or nvl(ap.CSTATUS,'0') = '2')";
            }
            else if (Status == "2")
            {
                strstatus = "AND (nvl(ap.CSTATUS,'0') = '3' or nvl(ap.CSTATUS,'0')='6')";
            }
            else if (Status == "3")
            {
                strstatus = "AND nvl(ap.CSTATUS,'0') = '0'";
            }
            else
            {
                strstatus = "AND nvl(ap.CSTATUS,'0') != '3'";
            }
            if (hideAppealID.Text != "")
            {
                strstatus = " AND AP.SAPPEALID = '" + CommonFunction.ReplaceInjection(hideAppealID.Text) + "'";
            }

            //ROW_NUMBER() OVER(ORDER BY AP.DAPPEAL DESC) AS ID1
            strsql = @"SELECT ROW_NUMBER() OVER(ORDER BY SAPPEALID DESC) AS ID1,SAPPEALID,NREDUCEID,DINCIDENT,SVENDORNAME,SAPPEALTYPE,SPROCESSNAME,STOPICNAME,SHEADREGISTERNO,STRAILREGISTERNO,NPOINT,COST,
DISABLE_DRIVER,BLACKLIST,DINCIDENT1,STATUS,CHKSTATUS,SVENDORID,SAPPEALNO,SPROCESSID,TOTAL_POINT,TOTAL_COST,TOTAL_DISABLE_DRIVER,BLACKLIST_A
FROM (
SELECT DISTINCT AP.SAPPEALID,AP.NREDUCEID, TO_CHAR(AP.DINCIDENT, 'DD/MM/YYYY') AS DINCIDENT,VS.SVENDORNAME,ap.sappealtype,p.SPROCESSNAME,CASE WHEN AP.SAPPEALTYPE = '090' THEN AP.SREFERENCEID  ELSE CASE WHEN AP.STYPECHECKLISTID != '0' THEN (SELECT cc.SCHECKLISTNAME FROM TCHECKLIST cc WHERE CC.SCHECKLISTID = AP.SCHECKLISTID AND CC.SVERSIONLIST = AP.SVERSIONLIST AND CC.STYPECHECKLISTID = AP.STYPECHECKLISTID )  
 WHEN AP.SAPPEALTYPE = '080' THEN AP.SREFERENCEID ELSE T.STOPICNAME END END AS STOPICNAME,AP.SHEADREGISTERNO,AP.STRAILREGISTERNO,- AP.NPOINT AS NPOINT, NVL(AP.COST, 0) AS COST

,CASE WHEN AP.SAPPEALTYPE = '090' THEN 'H' ELSE NVL(AP.DISABLE_DRIVER, 0)  || '' END AS DISABLE_DRIVER
, CASE WHEN AP.BLACKLIST = 1 THEN 'Y' ELSE '' END AS BLACKLIST,
TRUNC( SYSDATE - AP.DINCIDENT) || ' วัน' AS DINCIDENT1,

CASE WHEN ap.CSTATUS = '0' THEN CASE WHEN TRUNC(15 - (SYSDATE - AP.DAPPEAL )) > 0 THEN 'เหลือเวลาพิจารณา ' || TRUNC(15 - (SYSDATE - AP.DAPPEAL )) || ' วัน' ELSE 'หมดเวลาพิจารณา' END  
ELSE
CASE WHEN ap.CSTATUS = '1'  THEN CASE WHEN TRUNC(15 - (SYSDATE - AP.DAPPEAL )) > 0 THEN 'กำลังพิจารณา ' || 'เหลือเวลาพิจารณา ' || TRUNC(15 - (SYSDATE - AP.DAPPEAL )) || ' วัน' ELSE 'หมดเวลาพิจารณา' END
 ELSE
CASE WHEN ap.CSTATUS = '2'  THEN CASE WHEN TRUNC(15 - (SYSDATE - AP.DAPPEAL )) > 0 THEN 'รอเอกสารเพิ่มเติม ' || 'เหลือเวลาพิจารณา ' || TRUNC(15 - (SYSDATE - AP.DAPPEAL )) || ' วัน' ELSE 'หมดเวลาพิจารณา' END 
ELSE
CASE WHEN ap.CSTATUS = '4' THEN CASE WHEN TRUNC(15 - (SYSDATE - AP.DAPPEAL )) > 0 THEN 'ผู้ขนส่ง ส่งเอกสารแล้ว ' || 'เหลือเวลาพิจารณา ' || TRUNC(15 - (SYSDATE - AP.DAPPEAL )) || ' วัน' ELSE 'หมดเวลาพิจารณา' END 
ELSE
CASE WHEN ap.CSTATUS = '5' THEN 
    CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('1/1/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
        CASE WHEN    AP.DSENTENCE <= TO_DATE('20/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'รอคณะกรรมการพิจารณา '|| 'เหลือเวลา '|| (TO_DATE('20/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') - TRUNC(SYSDATE  )) || ' วัน' ELSE 'หมดเวลาพิจารณา('||TO_DATE('20/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy')||')' END
         ELSE
            CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                CASE WHEN    AP.DSENTENCE <= TO_DATE('20/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'รอคณะกรรมการพิจารณา '|| 'เหลือเวลา '|| (TO_DATE('20/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') - TRUNC(SYSDATE  )) || ' วัน'  ELSE 'หมดเวลาพิจารณา('||TO_DATE('20/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy')||')'  END
                    ELSE
                        CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                            CASE WHEN    AP.DSENTENCE <= TO_DATE('20/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'รอคณะกรรมการพิจารณา '|| 'เหลือเวลา '|| (TO_DATE('20/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') - TRUNC(SYSDATE  )) || ' วัน' ELSE 'หมดเวลาพิจารณา('||TO_DATE('20/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy')||')' END 
                                ELSE
                                    CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                                        CASE WHEN    AP.DSENTENCE <= TO_DATE('20/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'รอคณะกรรมการพิจารณา '|| 'เหลือเวลา '|| (TO_DATE('20/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') - TRUNC(SYSDATE  )) || ' วัน' ELSE 'หมดเวลาพิจารณา('||TO_DATE('20/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy')||')' END 
                                            ELSE
                                                CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('30/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                                                    CASE WHEN    AP.DSENTENCE <= TO_DATE('20/3/' || TO_CHAR(sysdate + interval '1' year,'yyyy'),'dd/MM/yyyy') THEN 'รอคณะกรรมการพิจารณา '|| 'เหลือเวลา '|| (TO_DATE('20/1/' || TO_CHAR(sysdate + interval '1' year,'yyyy'),'dd/MM/yyyy') - TRUNC(SYSDATE  )) || ' วัน' 
                                                        ELSE 'หมดเวลาพิจารณา' 
                                                    END 
                                                END 
                                    END 
                        END
             END 
    END 
ELSE
    CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
        CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน (เปลี่ยน)' END  END  END END  END END END END END  As STATUS,

CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' THEN CASE WHEN TRUNC(15 - (SYSDATE - AP.DAPPEAL )) > 0 THEN 'W' ELSE 'E' END 
ELSE
    CASE WHEN ap.CSTATUS = '5' THEN 
        CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('1/1/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
            CASE WHEN    AP.DSENTENCE <= TO_DATE('20/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'S' ELSE 'E' END 
            ELSE
                CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/3/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                    CASE WHEN    AP.DSENTENCE <= TO_DATE('20/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'S' ELSE 'E' END 
                    ELSE
                        CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/6/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                            CASE WHEN    AP.DSENTENCE <= TO_DATE('20/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'S' ELSE 'E' END 
                            ELSE
                                CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/9/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('20/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                                    CASE WHEN    AP.DSENTENCE <= TO_DATE('20/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'S' ELSE 'E' END 
                                    ELSE
                                        CASE WHEN TO_DATE(AP.DSENTENCE,'dd/MM/yyyy') BETWEEN TO_DATE('21/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') AND TO_DATE('30/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN
                                            CASE WHEN    AP.DSENTENCE <= TO_DATE('30/12/' || TO_CHAR(sysdate,'yyyy'),'dd/MM/yyyy') THEN 'S' ELSE 'E' END 
                                        END 
                                END 
                        END 
                END 
        END 
    ELSE
        CASE WHEN ap.CSTATUS = '6' THEN 'E'  ELSE
            CASE WHEN ap.CSTATUS = '3' THEN  'F' END 
        END 
    END 
END  As CHKSTATUS
,AP.SVENDORID,AP.SAPPEALNO,p.SPROCESSID
, CASE WHEN AP.SSENTENCER = 1 THEN 0 WHEN AP.SSENTENCER = 2 THEN - AP.NPOINT 
  ELSE  - (CASE WHEN AP.SAPPEALTYPE = '080' THEN NVL(TCOMPLAIN_SCORE_A.TOTAL_POINT, 0)
  WHEN AP.SAPPEALTYPE = '090' THEN NVL(ACC_SCORE_A.TOTAL_POINT, 0)
  ELSE 0 END)  END AS TOTAL_POINT
, CASE WHEN AP.SSENTENCER = 1 THEN 0 WHEN AP.SSENTENCER = 2 THEN COST 
  ELSE (CASE WHEN AP.SAPPEALTYPE = '080' THEN NVL(TCOMPLAIN_SCORE_A.TOTAL_COST, 0)
  WHEN AP.SAPPEALTYPE = '090' THEN NVL(ACC_SCORE_A.TOTAL_COST, 0)
  ELSE 0 END)  END AS TOTAL_COST
   ,CASE WHEN AP.SAPPEALTYPE = '090' AND acc_score_a.total_disable_driver IS NULL THEN '' 
  WHEN AP.SAPPEALTYPE = '090' AND acc_score_a.total_disable_driver = -2 THEN 'H' 
  WHEN AP.SAPPEALTYPE = '090' AND acc_score_a.total_disable_driver <> -2 THEN to_char(acc_score_a.total_disable_driver)
  ELSE
 (CASE WHEN AP.SSENTENCER = 1 THEN '0' WHEN AP.SSENTENCER = 2 THEN  to_char(disable_driver) 
  ELSE to_char(CASE WHEN AP.SAPPEALTYPE = '080' THEN to_char(NVL(TCOMPLAIN_SCORE_A.TOTAL_DISABLE_DRIVER, ''))
  ELSE '' END)
  END) END AS TOTAL_DISABLE_DRIVER
, CASE WHEN AP.SSENTENCER = 1 THEN '' WHEN AP.SSENTENCER = 2 THEN CASE WHEN TO_CHAR(AP.BLACKLIST) = '1' THEN 'Y' ELSE '' END ELSE CASE WHEN TO_CHAR(TCOMPLAIN_SCORE_A.BLACKLIST) = '1' THEN 'Y' ELSE '' END END AS BLACKLIST_A


FROM (((TAPPEAL ap LEFT JOIN TVENDOR_SAP vs ON AP.SVENDORID = VS.SVENDORID) LEFT JOIN TTOPIC t ON AP.STOPICAPPEAL = T.STOPICID AND T.CACTIVE = '1')
LEFT JOIN TCONTRACT ct ON AP.SCONTRACTID = CT.SCONTRACTID)
LEFT JOIN TCOMPLAIN_SCORE_A ON AP.SREFERENCEID = TCOMPLAIN_SCORE_A.DOC_ID AND AP.DINCIDENT = TCOMPLAIN_SCORE_A.DELIVERY_DATE AND TCOMPLAIN_SCORE_A.SAPPEALID = ap.SAPPEALID
 LEFT JOIN ACC_SCORE_A ON AP.SREFERENCEID = ACC_SCORE_A.ACCIDENT_ID AND TO_DATE(AP.DINCIDENT) = TO_DATE(ACC_SCORE_A.DELIVERY_DATE) AND ACC_SCORE_A.SAPPEALID = AP.SAPPEALID
LEFT JOIN TREDUCEPOINT ON AP.NREDUCEID = TREDUCEPOINT.NREDUCEID
LEFT JOIN TCOMPLAIN ON TCOMPLAIN.DOC_ID = TREDUCEPOINT.SREDUCENAME
LEFT JOIN ACC_ACCIDENT ON ACC_ACCIDENT.ACCIDENT_ID = TREDUCEPOINT.SREDUCENAME
LEFT JOIN TPROCESS p ON AP.SAPPEALTYPE = P.SPROCESSID WHERE 1=1 {DATE}
AND (ct.SCONTRACTNO LIKE '%' || :oSearch || '%' OR ap.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR ap.STRAILREGISTERNO LIKE '%' || :oSearch || '%' OR AP.SREFERENCEID LIKE '%' || :oSearch || '%') ";

            sds.SelectParameters.Clear();

            if (Session["AppealDoc"] != null)
            {
                DocID = Session["AppealDoc"].ToString();
                Session.Remove("AppealDoc");

                txtSearch.Visible = false;
                dteStart.Visible = false;
                dteEnd.Visible = false;
                cboStatus.Visible = false;
                btnSearch.Visible = false;

                txtSearch.Text = DocID;
            }

            if (Session["AppealAccID"] != null)
            {
                DocID = Session["AppealAccID"].ToString();
                Session.Remove("AppealAccID");

                txtSearch.Visible = false;
                dteStart.Visible = false;
                dteEnd.Visible = false;
                cboStatus.Visible = false;
                btnSearch.Visible = false;

                txtSearch.Text = DocID;
            }

            if (!string.Equals(DocID, string.Empty))
            {
                strsql = strsql.Replace("{DATE}", string.Empty);
                strsql += " ) ORDER BY DINCIDENT DESC";
            }
            else
            {
                //strsql = strsql.Replace("{DATE}", " AND AP.DINCIDENT BETWEEN TO_DATE(:dStart,'DD/MM/YYYY  HH24:MI:SS') AND TO_DATE(:dEnd,'DD/MM/YYYY  HH24:MI:SS')");
                strsql = strsql.Replace("{DATE}", " AND (CASE  AP.SAPPEALTYPE WHEN '080'  THEN TCOMPLAIN.complain_date WHEN '090'  THEN to_date(acc_accident.accident_date) ELSE AP.DINCIDENT END) BETWEEN TO_DATE(:dStart,'DD/MM/YYYY  HH24:MI:SS') AND TO_DATE(:dEnd,'DD/MM/YYYY  HH24:MI:SS')");
                strsql += strstatus;
                strsql += " ) ORDER BY DINCIDENT DESC";
                //sds.SelectParameters.Add(":dStart", dteStart.Date.ToString("dd/MM/yyyy 00:00:00", new CultureInfo("en-US")));
                //sds.SelectParameters.Add(":dEnd", CommonFunction.ReplaceInjection(dteEnd.Date.ToString("dd/MM/yyyy 23:59:59", new CultureInfo("en-US"))));

                sds.SelectParameters.Add(":dStart", dteStart.Text + " 00:00:00");
                sds.SelectParameters.Add(":dEnd", dteEnd.Text + " 23:59:59");
            }

            sds.SelectCommand = strsql;
            sds.SelectParameters.Add(":oSearch", txtSearch.Text.Trim());

            sds.DataBind();
            gvw.DataBind();
        }
    }
    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Contains(" "))
        {
            int VisibleIndex = e.VisibleIndex;



            ASPxButton imbIssue = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbedit");


            if (("" + e.GetValue("CHKSTATUS")).Equals("S"))
            {
                imbIssue.Text = "คณะกรรมการฯ";
            }
            else if (("" + e.GetValue("CHKSTATUS")).Equals("F"))
            {
                imbIssue.Text = "ดูรายละเอียด";
            }
            else if (("" + e.GetValue("CHKSTATUS")).Equals("E"))
            {
                imbIssue.Text = "หมดเวลาพิจารณา";
                imbIssue.ClientEnabled = false;
            }
            if ("" + Session["CheckPermission"] == "1")
            //if (!CanWrite)
            {
                imbIssue.Enabled = false;
            }
        }
    }
    void ListData(string id, string ctype)
    {

        ASPxPageControl PageControl = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
        ASPxLabel lblNo = (ASPxLabel)PageControl.FindControl("lblNo");
        ASPxLabel lblOutbound = (ASPxLabel)PageControl.FindControl("lblOutbound");
        ASPxLabel lblDate = (ASPxLabel)PageControl.FindControl("lblDate");
        ASPxLabel lblCustomer = (ASPxLabel)PageControl.FindControl("lblCustomer");
        ASPxLabel lblAddress = (ASPxLabel)PageControl.FindControl("lblAddress");
        ASPxLabel lblContractNo = (ASPxLabel)PageControl.FindControl("lblContractNo");
        ASPxLabel lblDStart = (ASPxLabel)PageControl.FindControl("lblDStart");
        ASPxLabel lblDEnd = (ASPxLabel)PageControl.FindControl("lblDEnd");
        ASPxLabel lblNum = (ASPxLabel)PageControl.FindControl("lblNum");
        ASPxLabel lblScore = (ASPxLabel)PageControl.FindControl("lblScore");

        ASPxMemo txtDetail2 = (ASPxMemo)PageControl.FindControl("txtDetail2");
        Literal ltrPicture2 = (Literal)PageControl.FindControl("ltrPicture2");
        Literal ltrDocument2 = (Literal)PageControl.FindControl("ltrDocument2");
        ASPxTextBox txtNameCreate2 = (ASPxTextBox)PageControl.FindControl("txtNameCreate2");

        ASPxLabel lblDate3 = (ASPxLabel)PageControl.FindControl("lblDate3");
        ASPxLabel lblScore3 = (ASPxLabel)PageControl.FindControl("lblScore3");
        ASPxMemo txtAppealDetail3 = (ASPxMemo)PageControl.FindControl("txtAppealDetail3");
        Literal ltrPicture3 = (Literal)PageControl.FindControl("ltrPicture3");
        Literal ltrDocument3 = (Literal)PageControl.FindControl("ltrDocument3");
        ASPxTextBox txtNameCreate3 = (ASPxTextBox)PageControl.FindControl("txtNameCreate3");

        ASPxLabel lblDate4 = (ASPxLabel)PageControl.FindControl("lblDate4");
        ASPxRadioButtonList rblCSENTENCE4 = (ASPxRadioButtonList)PageControl.FindControl("rblCSENTENCE4");
        ASPxRadioButtonList rblStatus4 = (ASPxRadioButtonList)PageControl.FindControl("rblStatus4");
        ASPxMemo txtData4 = (ASPxMemo)PageControl.FindControl("txtData4");
        ASPxButton btnConfirm = (ASPxButton)PageControl.FindControl("btnConfirm");
        ASPxMemo txtWaitDocument = (ASPxMemo)PageControl.FindControl("txtWaitDocument");

        ASPxLabel lblDate5 = (ASPxLabel)PageControl.FindControl("lblDate5");
        ASPxCheckBoxList chkSentencer5 = (ASPxCheckBoxList)PageControl.FindControl("chkSentencer5");
        ASPxRadioButtonList rblStatus5 = (ASPxRadioButtonList)PageControl.FindControl("rblStatus5");
        ASPxMemo txtDetail5 = (ASPxMemo)PageControl.FindControl("txtDetail5");
        ASPxButton btnConfirm1 = (ASPxButton)PageControl.FindControl("btnConfirm1");

        ASPxLabel lblDate6 = (ASPxLabel)PageControl.FindControl("lblDate6");
        ASPxCheckBox chkChange6 = (ASPxCheckBox)PageControl.FindControl("chkChange6");
        ASPxMemo txtDetail6 = (ASPxMemo)PageControl.FindControl("txtDetail6");
        ASPxRadioButtonList rblStatus6 = (ASPxRadioButtonList)PageControl.FindControl("rblStatus6");
        ASPxTextBox txtEvidence = (ASPxTextBox)PageControl.FindControl("txtEvidence");
        ASPxTextBox txtFileName = (ASPxTextBox)PageControl.FindControl("txtFileName");
        ASPxTextBox txtFilePath = (ASPxTextBox)PageControl.FindControl("txtFilePath");
        ASPxButton btnView = (ASPxButton)PageControl.FindControl("btnView");
        ASPxButton btnDelFile = (ASPxButton)PageControl.FindControl("btnDelFile");
        ASPxTextBox txtName6 = (ASPxTextBox)PageControl.FindControl("txtName6");
        ASPxTextBox txtOldStatus = (ASPxTextBox)PageControl.FindControl("txtOldStatus");

        DataTable dtAppeal = AppealBLL.Instance.AppealSelectBLL(Session["SAPPEALID"].ToString());
        if (!(string.Equals(dtAppeal.Rows[0]["SAPPEALTYPE"].ToString(), "080") || string.Equals(dtAppeal.Rows[0]["SAPPEALTYPE"].ToString(), "090")))
        {
            rblStatus4.Items.RemoveAt(2);
            cmdAppeal.Visible = false;
            cmdAttach.Visible = false;
            lblAttachTotal.Visible = false;
        }

        Session["IsAppeal"] = 0;

        if (ctype == "S")
        {
            Session["IsAppeal"] = "1";
            btnConfirm.Visible = false;
            cmdSaveTab3.Visible = false;
            rblCSENTENCE4.ClientEnabled = false;
            rblStatus4.ClientEnabled = false;
            txtData4.ClientEnabled = false;
        }

        if (ctype == "F")
        {
            Session["IsAppeal"] = "1";
            btnConfirm.Visible = false;
            cmdSaveTab3.Visible = false;
            btnConfirm1.Visible = false;
            rblCSENTENCE4.ClientEnabled = false;
            rblStatus4.ClientEnabled = false;
            txtData4.ClientEnabled = false;

            chkSentencer5.ClientEnabled = false;
            rblStatus5.ClientEnabled = false;
            txtDetail5.ClientEnabled = false;
        }

        if (ctype == "W")
        {
            btnConfirm1.Visible = false;
            chkSentencer5.ClientEnabled = false;
            rblStatus5.ClientEnabled = false;
            txtDetail5.ClientEnabled = false;

            chkChange6.ClientEnabled = false;
            txtDetail6.ClientEnabled = false;
            ASPxUploadControl uplExcel = (ASPxUploadControl)PageControl.FindControl("uplExcel");
            ASPxButton btnConfirm2 = (ASPxButton)PageControl.FindControl("btnConfirm2");
            uplExcel.ClientVisible = false;
            rblStatus6.ClientEnabled = false;
            txtEvidence.ClientEnabled = false;
            btnView.ClientEnabled = false;
            btnDelFile.ClientEnabled = false;
            txtName6.ClientEnabled = false;
            btnConfirm2.Visible = false;


        }

        if (ctype == "E")
        {
            Session["IsAppeal"] = "1";
            btnConfirm.Visible = false;
            cmdSaveTab3.Visible = false;
            btnConfirm1.Visible = false;

            chkChange6.ClientEnabled = false;
            txtDetail6.ClientEnabled = false;
            ASPxUploadControl uplExcel = (ASPxUploadControl)PageControl.FindControl("uplExcel");
            ASPxButton btnConfirm2 = (ASPxButton)PageControl.FindControl("btnConfirm2");
            uplExcel.ClientVisible = false;
            rblStatus6.ClientEnabled = false;
            txtEvidence.ClientEnabled = false;
            btnView.ClientEnabled = false;
            btnDelFile.ClientEnabled = false;
            txtName6.ClientEnabled = false;
            btnConfirm2.Visible = false;
        }

        //tab1
        string strsql = @"SELECT ap.SAPPEALNO,ap.SWAITDOCUMENT,ap.NREDUCEID, ap.DAPPEAL, ap.SAPPEALTYPE,ap.SSENTENCERCODE, ap.STOPICAPPEAL,AP.SDELIVERYNO, ap.SVENDORID, ap.SDRIVERNO,  ap.NPOINT, ap.NDIFFPOINT, ap.DEXPIRE, ap.CSTATUS, ap.SSENTENCER, ap.SSENTENCERTEXT, ap.DINCIDENT,  ap.SISSUETEXT, ap.SAPPEALTEXT, ap.DSENTENCE, ap.CSENTENCE, ap.SSENTENCETEXT, ap.DSENTENCER, ap.SCREATE, 
   ap.SCUSTOMERID, ap.SAREAACCIDENT,ap.DUPDATE, ap.SREFERENCEID,AP.SHEADREGISTERNO,AP.STRAILREGISTERNO,AP.STOPICAPPEAL,AP.SCONTRACTID, C.SCONTRACTNO,C.DBEGIN,C.DEND,CT.CUST_NAME FROM (TAPPEAL ap LEFT JOIN TCONTRACT c ON AP.SCONTRACTID = C.SCONTRACTID) LEFT JOIN TCUSTOMER_SAP ct ON AP.SCUSTOMERID = CT.SHIP_TO WHERE ap.SAPPEALID = '" + id + "'";
        DataTable dt = CommonFunction.Get_Data(sql, strsql);
        DateTime date;

        if (dt.Rows.Count > 0)
        {
            hideReduceID.Text = dt.Rows[0]["NREDUCEID"] + "";
            lblNo.Text = dt.Rows[0]["SAPPEALNO"] + "";
            lblOutbound.Text = dt.Rows[0]["SDELIVERYNO"] + "";
            lblDate.Text = DateTime.TryParse(dt.Rows[0]["DINCIDENT"] + "", out date) ? date.ToShortDateString() : "";
            lblCustomer.Text = dt.Rows[0]["CUST_NAME"] + "";
            lblAddress.Text = dt.Rows[0]["SAREAACCIDENT"] + "";
            lblContractNo.Text = dt.Rows[0]["SCONTRACTNO"] + "";
            lblDStart.Text = DateTime.TryParse(dt.Rows[0]["DBEGIN"] + "", out date) ? date.ToShortDateString() : "";
            lblDEnd.Text = DateTime.TryParse("" + dt.Rows[0]["DEND"], out date) ? date.ToShortDateString() : "";

            hideCheckFinal.Text = dt.Rows[0]["CSTATUS"] + "";
            hideSentence.Text = dt.Rows[0]["CSENTENCE"] + "";
            //tab2


            string strImportt2 = "";
            string GetData = "";
            string CountData = "";
            if ("" + dt.Rows[0]["SAPPEALTYPE"] == "090")
            {

                CountData = @"SELECT COUNT(*) AS NCOUNT, SUM(TP.NPOINT) AS NPOINT FROM acc_accident 
  LEFT JOIN acc_detail ON acc_accident.accident_id = acc_detail.accident_id
  LEFT JOIN acc_score_detail ON acc_accident.accident_id = acc_score_detail.accident_id
 LEFT JOIN  TTOPIC  tp ON acc_score_detail.STOPICID = TP.STOPICID 
LEFT JOIN TTRUCK  ON acc_detail.struckid = TTRUCK.struckid 
LEFT JOIN TCONTRACT_TRUCK ON nvl(TTRUCK.STRUCKID,1) = nvl(TCONTRACT_TRUCK.STRUCKID,1) 
  WHERE acc_accident.CCUT = '1' AND TTRUCK.SHEADREGISTERNO =  '" + dt.Rows[0]["SHEADREGISTERNO"] + @"'  
  AND nvl(TTRUCK.STRAILERREGISTERNO,1)  LIKE '%" + dt.Rows[0]["STRAILREGISTERNO"] + @"%' 
  AND (acc_detail.SCONTRACTID = '" + dt.Rows[0]["SCONTRACTID"] + "' )";

                GetData = @"SELECT TUSER.SFIRSTNAME || ' ' || TUSER.SLASTNAME FULLNAME,ACC_DETAIL.LOCATIONS FROM ACC_ACCIDENT 
  LEFT JOIN ACC_DETAIL ON ACC_DETAIL.ACCIDENT_ID = ACC_ACCIDENT.ACCIDENT_ID
LEFT JOIN TUSER ON SUID = ACC_ACCIDENT.CREATE_BY
  WHERE acc_accident.accident_id =  '" + dt.Rows[0]["SREFERENCEID"] + "'";

                strImportt2 = @"SELECT UPLOAD_NAME AS SDOCUMENT, FILENAME_USER AS SFILENAME, FULLPATH AS SFILEPATH FROM F_UPLOAD LEFT JOIN M_UPLOAD_TYPE ON F_UPLOAD.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID WHERE REF_STR = '" + dt.Rows[0]["SREFERENCEID"].ToString() + "'";
            }
            else if ("" + dt.Rows[0]["SAPPEALTYPE"] == "011")
            {
                CountData = "SELECT COUNT(*) AS NCOUNT,SUM(CT.NPOINT) AS NPOINT FROM (TCHECKTRUCK c LEFT JOIN TCHECKTRUCKITEM ct  ON C.SCHECKID = ct.SCHECKID) LEFT JOIN TCHECKLIST  cl ON CT.SCHECKLISTID = CL.SCHECKLISTID AND CT.SVERSIONLIST = CL.SVERSIONLIST AND ct.STYPECHECKLISTID  = CL.STYPECHECKLISTID  WHERE  ct.SCHECKLISTID != '0' AND C.SHEADERREGISTERNO =  '" + dt.Rows[0]["SHEADREGISTERNO"] + "'  AND nvl(C.STRAILERREGISTERNO,1)  LIKE '%" + dt.Rows[0]["STRAILREGISTERNO"] + "%' AND CL.STOPICID = '" + dt.Rows[0]["STOPICAPPEAL"] + "' AND C.SCONTRACTID = '" + dt.Rows[0]["SCONTRACTID"] + "' ";

                GetData = "SELECT U.SFIRSTNAME || ' ' || U.SLASTNAME AS FULLNAME ,CL.SCHECKLISTNAME FROM (TCHECKTRUCKITEM ct LEFT JOIN TCHECKLIST cl ON CT.SCHECKLISTID = CL.SCHECKLISTID AND CT.STYPECHECKLISTID = CL.SCHECKLISTID AND CT.SVERSIONLIST = CL.SVERSIONLIST) LEFT JOIN TUSER u ON CT.SCREATE = U.SUID WHERE ct.SCHECKID = '" + dt.Rows[0]["SREFERENCEID"] + "'";

                strImportt2 = @"SELECT 'เอกสาร' AS SDOCUMENT, CA.SFILE AS SFILENAME ,CA.SPATH || CA.SSYSFILE AS SFILEPATH  FROM TCHECKTRUCKITEM c INNER JOIN TCHECKTRUCKATTACHMENT ca ON C.SCHECKID = CA.SCHECKID  WHERE C.SCHECKID = '" + dt.Rows[0]["SREFERENCEID"] + "'";
            }
            else if ("" + dt.Rows[0]["SAPPEALTYPE"] == "010")
            {
                CountData = "SELECT COUNT(*) AS NCOUNT,SUM(tp.NPOINT) AS NPOINT FROM (TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM c ON TC.NCONFIRMID = C.NCONFIRMID)LEFT JOIN (SELECT NPOINT FROM (SELECT NPOINT FROM TTOPIC WHERE CACTIVE = '1' AND CPROCESS = '1' ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp ON 1 = 1 WHERE Tc.CCONFIRM = '0' AND TC.SHEADREGISTERNO =  '" + dt.Rows[0]["SHEADREGISTERNO"] + "'  AND nvl(TC.STRAILERREGISTERNO,1)  LIKE '%" + dt.Rows[0]["STRAILREGISTERNO"] + "%' AND C.SCONTRACTID = '" + dt.Rows[0]["SCONTRACTID"] + "' ";
                GetData = "SELECT 'เจ้าหน้าที่ ปตท.' AS FULLNAME , STOPICNAME FROM (SELECT STOPICID, STOPICNAME FROM TTOPIC WHERE CACTIVE = '1' ORDER BY SVERSION DESC ) WHERE STOPICID = '" + dt.Rows[0]["STOPICAPPEAL"] + "' AND ROWNUM <= 1";
            }
            else if ("" + dt.Rows[0]["SAPPEALTYPE"] == "020")
            {
                CountData = @"SELECT COUNT(*) AS NCOUNT,SUM(tp.NPOINT) AS NPOINT FROM
(((TPLANSCHEDULE p LEFT JOIN TPlanScheduleList pl ON p.NPLANID = PL.NPLANID) LEFT JOIN (SELECT NPOINT FROM (SELECT NPOINT FROM TTOPIC WHERE CACTIVE = '1' AND CPROCESS = '2' ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp ON 1 = 1 )
LEFT JOIN TTRUCK t ON REPLACE(REPLACE(p.SHEADREGISTERNO,'-',''),' ','') = REPLACE(REPLACE(T.SHEADREGISTERNO,'-',''),' ','') )
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1) WHERE (P.CCONFIRM = '0' OR P.CCONFIRM IS NULL ) AND p.CACTIVE = '1' AND PL.CACTIVE = '1' AND PL.CREDUCE = '1' AND P.SHEADREGISTERNO =  '" + dt.Rows[0]["SHEADREGISTERNO"] + "'  AND nvl(P.STRAILERREGISTERNO,1)  LIKE '%" + dt.Rows[0]["STRAILREGISTERNO"] + "%' AND (T.SCONTRACTID = '" + dt.Rows[0]["SCONTRACTID"] + "' OR  CT2.SCONTRACTID = '" + dt.Rows[0]["SCONTRACTID"] + "')";

                GetData = "SELECT 'เจ้าหน้าที่ ปตท.' AS FULLNAME , STOPICNAME FROM (SELECT STOPICID, STOPICNAME FROM TTOPIC WHERE CACTIVE = '1' ORDER BY SVERSION DESC ) WHERE STOPICID = '" + dt.Rows[0]["STOPICAPPEAL"] + "' AND ROWNUM <= 1";
            }
            else if ("" + dt.Rows[0]["SAPPEALTYPE"] == "080")
            {
                CountData = @"SELECT COUNT(*) AS NCOUNT, SUM(TP.NPOINT) AS NPOINT FROM 
((TCOMPLAIN cp LEFT JOIN  TTOPIC  tp ON cp.STOPICID = TP.STOPICID AND CP.SVERSION = TP.SVERSION )
LEFT JOIN TTRUCK t ON cp.SHEADREGISTERNO = T.SHEADREGISTERNO )
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1) WHERE cp.CCUT = '1' AND CP.SHEADREGISTERNO =  '" + dt.Rows[0]["SHEADREGISTERNO"] + "'  AND nvl(CP.STRAILERREGISTERNO,1)  LIKE '%" + dt.Rows[0]["STRAILREGISTERNO"] + "%' AND (T.SCONTRACTID = '" + dt.Rows[0]["SCONTRACTID"] + "' OR  CT2.SCONTRACTID = '" + dt.Rows[0]["SCONTRACTID"] + "')";

                GetData = "SELECT CP.SCOMPLAINFNAME || ' ' || CP.SCOMPLAINLNAME AS FULLNAME, CP.SDETAIL FROM TCOMPLAIN cp WHERE cp.SSERVICEID = '" + dt.Rows[0]["SREFERENCEID"] + "'";
                //strImportt2 = @"SELECT SDOCUMENT, SFILENAME, SFILEPATH FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = '" + dt.Rows[0]["SREFERENCEID"] + "'";
                strImportt2 = @"SELECT UPLOAD_NAME AS SDOCUMENT, FILENAME_USER AS SFILENAME, FULLPATH AS SFILEPATH FROM F_UPLOAD LEFT JOIN M_UPLOAD_TYPE ON F_UPLOAD.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID WHERE REF_STR = '" + dt.Rows[0]["SREFERENCEID"].ToString() + "'";
            }
            else if ("" + dt.Rows[0]["SAPPEALTYPE"] == "100")
            {
                CountData = "";

                GetData = "SELECT TUSER.SFIRSTNAME || ' ' || TUSER.SLASTNAME AS FULLNAME, TMONTHLY_HEADER.DESCRIPTION FROM TMONTHLY_HEADER INNER JOIN TUSER ON TMONTHLY_HEADER.FINAL_BY = TUSER.SUID WHERE TMONTHLY_HEADER.TRANSACTION_ID = '" + dt.Rows[0]["SREFERENCEID"] + "'";
                //strImportt2 = @"SELECT SDOCUMENT, SFILENAME, SFILEPATH FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = '" + dt.Rows[0]["SREFERENCEID"] + "'";
                strImportt2 = @"SELECT UPLOAD_NAME AS SDOCUMENT, FILENAME_USER AS SFILENAME, FULLPATH AS SFILEPATH FROM F_UPLOAD LEFT JOIN M_UPLOAD_TYPE ON F_UPLOAD.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID WHERE REF_STR = '" + dt.Rows[0]["SREFERENCEID"].ToString() + "'";
            }

            if (CountData != "")
            {
                DataTable dttTab2 = CommonFunction.Get_Data(sql, CountData);
                if (dttTab2.Rows.Count > 0)
                {

                    lblScore.Text = "-" + dttTab2.Rows[0][1] + " คะแนน";
                    lblNum.Text = dttTab2.Rows[0][0] + "";

                }
            }

            if (GetData != "")
            {
                DataTable dtTab2 = CommonFunction.Get_Data(sql, GetData);
                if (dtTab2.Rows.Count > 0)
                {
                    txtDetail2.Text = dtTab2.Rows[0][1] + "";
                    txtNameCreate2.Text = dtTab2.Rows[0][0] + "";
                }
            }

            if (strImportt2 != "")
            {
                DataTable dtImportFilet2 = CommonFunction.Get_Data(sql, strImportt2);

                if (dtImportFilet2.Rows.Count > 0)
                {
                    StringBuilder sbt2 = new StringBuilder();
                    StringBuilder sb1t2 = new StringBuilder();

                    string format1t2 = "<div style='float:left;'><a href=openFile.aspx?str={0} target=_new><img src='{0}' alt='{1}' width='250' height='200' border='1'/></a></div>";
                    string format2t2 = "<div style='float:left;'><a href=openFile.aspx?str={0} target=_new><img src='{0}' alt='{1}' width='250' height='200' border='1'/></a></div>";

                    string format3t2 = "<div>{0}{1}<a href=openFile.aspx?str={2} target=_new><img src='images/search.png' alt='คลิกเพื่อดูรายละเอียดไฟล์' width='16' height='16' border='0'/></a></div>";
                    int i = 1;
                    int c = 1;
                    string[] fileExpandt2;
                    foreach (DataRow dr in dtImportFilet2.Rows)
                    {
                        fileExpandt2 = ("" + dr["SFILENAME"]).Split('.');
                        int count = fileExpandt2.Count() - 1;
                        if (fileExpandt2.Length > 1)
                        {
                            string filename2 = fileExpandt2[count];
                            if (filename2.Equals("jpg") || filename2.Equals("jpeg") || filename2.Equals("gif") || filename2.Equals("png") || filename2.Equals("jpe"))
                            {

                                //if (i == 2)
                                //{
                                sbt2.Append(string.Format(format1t2, dr["SFILEPATH"] + "", dr["SDOCUMENT"] + ""));
                                //}
                                //else
                                //{
                                //    sbt2.Append(string.Format(format2t2, dr["SFILEPATH"] + "",dr["SDOCUMENT"] + ""));
                                //}

                                //if (i == 2) i = 1;
                                //i++;
                            }
                            else
                            {
                                sb1t2.Append(string.Format(format3t2, c + ". ", (dr["SDOCUMENT"] + "") == "" ? dr["SFILENAME"] + "" : dr["SDOCUMENT"] + "", dr["SFILEPATH"] + ""));
                                c++;
                            }
                        }

                    }
                    ltrPicture2.Text = sbt2.ToString();
                    ltrDocument2.Text = sb1t2.ToString();
                }


            }


            //tab3
            lblDate3.Text = DateTime.TryParse(dt.Rows[0]["DUPDATE"] + "", out date) ? date.ToShortDateString() : "";
            lblScore3.Text = dt.Rows[0]["NPOINT"] + "";
            lblScore.Text = dt.Rows[0]["NPOINT"] + "";
            txtAppealDetail3.Text = dt.Rows[0]["SAPPEALTEXT"] + "";
            txtNameCreate3.Text = CommonFunction.Get_Value(sql, "SELECT SFIRSTNAME || ' ' || SLASTNAME AS FULLNAME FROM TUSER WHERE SUID ='" + dt.Rows[0]["SCREATE"] + "'");

            string strImport = @"SELECT  SAPPEALID, NITEM, SDOCFROM, SDOCTYPE, SDOCUMENT, SPATH,  SFILE, SSYSFILE, DUPLOAD, DEDIT, SCREATE, SUPDATE
                        FROM TAPPEALATTACHMENT WHERE SAPPEALID = '" + id + "'";

            DataTable dtImportFile = CommonFunction.Get_Data(sql, strImport);

            if (dtImportFile.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder sb1 = new StringBuilder();

                string format1 = "<div style='float:left;'><a href=openFile.aspx?str={0} target=_new><img src='{0}' alt='{1}' width='250' height='200' border='1'/></a></div>";
                string format2 = "<div><a href=openFile.aspx?str={0} target=_new><img src='{0}' alt='{1}' width='250' height='200' border='1'/></a></div>";

                string format3 = "<div>{0}{1}<a href=openFile.aspx?str={2} target=_new><img src='images/search.png' alt='คลิกเพื่อดูรายละเอียดไฟล์' width='16' height='16' border='0'/></a></div>";
                int i = 1;
                int c = 1;
                string[] fileExpand;
                foreach (DataRow dr in dtImportFile.Rows)
                {
                    fileExpand = ("" + dr["SFILE"]).Split('.');
                    int count = fileExpand.Count() - 1;
                    if (fileExpand.Length > 1)
                    {
                        string filetype = fileExpand[count];
                        if (filetype.Equals("jpg") || filetype.Equals("jpeg") || filetype.Equals("gif") || filetype.Equals("png") || filetype.Equals("jpe"))
                        {

                            //if (i == 2)
                            //{
                            sb.Append(string.Format(format1, dr["SPATH"] + "", dr["SDOCUMENT"] + ""));
                            //}
                            //else
                            //{
                            //    sb.Append(string.Format(format2, dr["SPATH"] + "", dr["SDOCUMENT"] + ""));
                            //}
                            //if (i == 2) i = 1;
                            //i++;
                        }
                        else
                        {
                            sb1.Append(string.Format(format3, c + ". ", (dr["SDOCUMENT"] + "") == "" ? dr["SFILE"] + "" : dr["SDOCUMENT"] + "", dr["SPATH"] + ""));
                            c++;
                        }
                    }

                }

                ltrPicture3.Text = sb.ToString();
                ltrDocument3.Text = sb1.ToString();
            }



            //tab4

            if (dt.Rows[0]["CSTATUS"] + "" == "2" || dt.Rows[0]["CSTATUS"] + "" == "4")
            {
                txtWaitDocument.ClientVisible = true;
                txtWaitDocument.Text = dt.Rows[0]["SWAITDOCUMENT"] + "";
            }

            lblDate4.Text = DateTime.TryParse(dt.Rows[0]["DSENTENCE"] + "", out date) ? date.ToShortDateString() : "";
            rblCSENTENCE4.Value = dt.Rows[0]["CSENTENCE"] + "";
            rblStatus4.Value = dt.Rows[0]["SSENTENCER"] + "";
            txtData4.Text = dt.Rows[0]["SSENTENCETEXT"] + "";


            //tab5
            DataTable dtSentencer = CommonFunction.Get_Data(sql, "SELECT SAPPEALID, NSENTENCE, SSENTENCERID, SSENTENCERCODE FROM TAPPEAL_SENTENCER WHERE SAPPEALID = '" + id + "'");
            lblDate5.Text = DateTime.TryParse(dt.Rows[0]["DSENTENCER"] + "", out date) ? date.ToShortDateString() : "";
            rblStatus5.Value = dt.Rows[0]["SSENTENCER"] + "";
            txtDetail5.Text = dt.Rows[0]["SSENTENCERTEXT"] + "";

            Session["ssSSENTENCERCODE"] = dt.Rows[0]["SSENTENCERCODE"] + "";
            chkSentencer5.DataBind();

            foreach (DataRow dr in dtSentencer.Rows)
            {
                ListEditItem chkSentence = chkSentencer5.Items.FindByValue(dr["SSENTENCERID"] + "");
                if (chkSentence != null)
                {
                    chkSentence.Selected = true;
                }
            }

            //tab6
            DataTable dtRESULT = CommonFunction.Get_Data(sql, "SELECT  SBECUASE, SSENTENCE, SAPPROVEDOC, DCHANGE, SAPPROVEBY,SFILENAME,SFILEPATH FROM TAPPEALRESULT WHERE SAPPEALID = '" + id + "' ORDER BY NCHANGE DESC");
            if (dtRESULT.Rows.Count > 0)
            {
                lblDate6.Text = DateTime.TryParse(dtRESULT.Rows[0]["DCHANGE"] + "", out date) ? date.ToShortDateString() : "";
                chkChange6.Checked = true;
                txtDetail6.Text = dtRESULT.Rows[0]["SBECUASE"] + "";
                rblStatus6.Value = dt.Rows[0]["SSENTENCER"] + "";
                txtOldStatus.Text = dt.Rows[0]["SSENTENCER"] + "";
                txtEvidence.Text = dtRESULT.Rows[0]["SAPPROVEDOC"] + "";
                txtFileName.Text = dtRESULT.Rows[0]["SFILENAME"] + "";
                txtFilePath.Text = dtRESULT.Rows[0]["SFILEPATH"] + "";
                txtName6.Text = dtRESULT.Rows[0]["SAPPROVEBY"] + "";
                VisibleControlUpload();
            }
        }
    }
    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;
            int count = _Filename.Count() - 1;

            string genName = "suppliant" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                if (upl.ID == "uplExcel")
                {
                    e.CallbackData = data + "|" + e.UploadedFile.FileName;
                }
            }
        }
        else
        {

            return;

        }
    }
    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    private void VisibleControlUpload()
    {
        ASPxPageControl PageControl = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
        ASPxUploadControl uplExcel = (ASPxUploadControl)PageControl.FindControl("uplExcel");
        ASPxTextBox txtFileName = (ASPxTextBox)PageControl.FindControl("txtFileName");
        ASPxTextBox txtFilePath = (ASPxTextBox)PageControl.FindControl("txtFilePath");
        ASPxButton btnView = (ASPxButton)PageControl.FindControl("btnView");
        ASPxButton btnDelFile = (ASPxButton)PageControl.FindControl("btnDelFile");

        if (!string.IsNullOrEmpty(txtFilePath.Text))
        {
            uplExcel.ClientVisible = false;
            txtFileName.ClientVisible = true;
            btnView.ClientEnabled = true;
            btnDelFile.ClientEnabled = true;

        }
        else
        {
            uplExcel.ClientVisible = true;
            txtFileName.ClientVisible = false;
            btnView.ClientEnabled = false;
            btnDelFile.ClientEnabled = false;
        }
    }
    private void VisibleTabControl()
    {
        ASPxPageControl PageControl = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;

        if (hideCheckFinal.Text == "3")
        {
            PageControl.TabPages.FindByName("t6").ClientEnabled = true;
        }
        else
        {
            PageControl.TabPages.FindByName("t6").ClientEnabled = false;
        }

        if (hideSentence.Text == "2")
        {
            PageControl.TabPages.FindByName("t5").ClientEnabled = true;
        }
        else
        {
            PageControl.TabPages.FindByName("t5").ClientEnabled = false;
        }

    }
    protected void SetActiveReducePoint(bool active)
    {
        string NREDUCEID = hideReduceID.Text;
        if (active)
        {
            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();


                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '1' WHERE  NREDUCEID = '" + NREDUCEID + "'", con))
                {
                    comUpdate.ExecuteNonQuery();
                }

            }
        }
        else
        {
            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();


                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE  NREDUCEID = '" + NREDUCEID + "'", con))
                {
                    comUpdate.ExecuteNonQuery();
                }
                //fix by j for TREDUCEPOINT don't Update
                using (OracleCommand ComdUpdateAllTREDUCEPOINT = new OracleCommand(@"UPDATE TREDUCEPOINT SET CACTIVE='0'
WHERE NREDUCEID IN ( 

SELECT  DISTINCT
(SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE REPLACE(RP.SREFERENCEID,'-','') = TC.NCONFIRMID /*AND RP.SPROCESSID = '010' */AND rownum <=1 AND rp.SHEADREGISTERNO=NVL(RDP.SHEADREGISTERNO,TL.SHEADREGISTERNO))  as NREDUCEID
-- ,RDP.CACTIVE
FROM TREDUCEPOINT RDP
LEFT JOIN TTRUCKCONFIRM tc ON REPLACE(RDP.SREFERENCEID,'-','') = TC.NCONFIRMID
LEFT JOIN TTRUCKCONFIRMLIST tl ON TL.NCONFIRMID = TC.NCONFIRMID AND NVL(RDP.SHEADREGISTERNO,'TRxxxxxx')=NVL(TL.SHEADREGISTERNO,'TRxxxxxx')
LEFT JOIN (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM TTOPIC WHERE CACTIVE = '1' /*AND CPROCESS LIKE '%,010%'*/ ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp ON 1 = 1 
LEFT JOIN TAPPEAL ap ON NVL(tl.NLISTNO,TC.NCONFIRMID) =  REPLACE(AP.SREFERENCEID,'-','') AND NVL(AP.SHEADREGISTERNO,'TRxxxxxx')=NVL(RDP.SHEADREGISTERNO,'TRxxxxxx') AND NVL(AP.STRAILREGISTERNO,'TRxxxxxx')=NVL(RDP.STRAILERREGISTERNO,'TRxxxxxx') 
/*AND AP.SAPPEALTYPE='010'*/
--LEFT JOIN TAPPEAL ap ON tl.NLISTNO = AP.SREFERENCEID AND AP.SAPPEALTYPE='010'
LEFT JOIN TCONTRACT cc ON tc.SCONTRACTID = CC.SCONTRACTID 
WHERE 1=1 
AND NVL(TL.CCONFIRM,'0') = '0' 
--AND RDP.SPROCESSID='010'
--AND CC.SVENDORID = '0010001322'
 AND TO_DATE(tc.DDATE+1,'DD/MM/YYYY') BETWEEN TO_DATE('01/01/2013','DD/MM/YYYY') AND TO_DATE(TRUNC(SYSDATE),'DD/MM/YYYY') 
 AND  ap.CSTATUS = '3'  AND ap.SSENTENCER = '1' AND RDP.CACTIVE !='0'
 
)", con))
                {
                    ComdUpdateAllTREDUCEPOINT.ExecuteNonQuery();
                }

            }
        }

    }
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    private void SendEmail(int TemplateID, string DocID)
    {
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailComplainAppealConfirm)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                    Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                    Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                    Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                    Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());
                    Body = Body.Replace("{WAREHOUSE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "vendor_appeal_lst.aspx";
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;
                    string CreateMail = string.Empty;

                    MailService.SendMail("YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CreateMail, Subject, Body);
                }
                else if (TemplateID == ConfigValue.EmailAccident8)
                {
                    #region EmailAccident8
                    string accID = DocID;
                    DataTable dt = AccidentBLL.Instance.AccidentTab1Select(accID);
                    DataRow dr = dt.Rows[0];
                    string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, false, true);
                    if (!string.IsNullOrEmpty(EmailList))
                    {
                        EmailList += ",";
                    }

                    EmailList += "bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com";
                    
                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                    Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                    Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                    Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                    Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                    Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                    Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                    //Body = Body.Replace("{REPORT_CHECK}", IsApprove);
                    Body = Body.Replace("{REMARK}", "");
                    Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                    Body = Body.Replace("{USER_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);

                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "vendor_appeal_lst.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body);
                    #endregion
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }

    protected void cmdAppeal_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["dtScoreList"] == null)
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);

                string AppealID = Session["SAPPEALID"].ToString();
                DataTable dtAppeal = AppealBLL.Instance.AppealSelectBLL(AppealID);
                string ComplainDocID = dtAppeal.Rows[0]["SREFERENCEID"].ToString();
                IndexEdit = -1;

                DataTable dtAppealData = AppealBLL.Instance.AppealSelectAllBLL(AppealID);

                if (ComplainDocID.IndexOf("ACC") > -1)//กรณึอุบัติเหตุ
                {
                    #region อุบัติเหตุ
                    ds = AccidentBLL.Instance.AccidentTab4Select(ComplainDocID);
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables["ACCIDENT"];
                        if (dt != null && dt.Rows.Count > 0)
                        {

                            DataRow dr = dt.Rows[0];

                            txtRemarkTab3.Text = dr["REMARK_TAB4"] + string.Empty;
                            lblCusScoreHeader.Text = string.Format(lblCusScoreHeader.Text, dr["SCONTRACTNO"] + string.Empty);
                        }
                    }
                    dtScore = ComplainBLL.Instance.ScoreSelectBLL();

                    DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
                    this.SelectDuplicateCarAcc();

                    dtScoreList = new DataTable();
                    dtScoreList.Columns.Add("STOPICID");
                    dtScoreList.Columns.Add("STOPICNAME");
                    dtScoreList.Columns.Add("Cost");
                    dtScoreList.Columns.Add("CostDisplay");
                    dtScoreList.Columns.Add("DisableDriver");
                    dtScoreList.Columns.Add("DisableDriverDisplay");
                    dtScoreList.Columns.Add("Score1");
                    dtScoreList.Columns.Add("Score2");
                    dtScoreList.Columns.Add("Score3");
                    dtScoreList.Columns.Add("Score4");
                    dtScoreList.Columns.Add("Score5");
                    dtScoreList.Columns.Add("Score6");
                    dtScoreList.Columns.Add("Score6Display");
                    dtScoreList.Columns.Add("TotalCar");

                    dtScoreListCar = new DataTable();
                    dtScoreListCar.Columns.Add("STOPICID");
                    dtScoreListCar.Columns.Add("TRUCKID");
                    dtScoreListCar.Columns.Add("CARHEAD");
                    dtScoreListCar.Columns.Add("CARDETAIL");
                    dtScoreListCar.Columns.Add("DELIVERY_DATE");
                    dtScoreListCar.Columns.Add("TOTAL_CAR");

                    int sum = 0;
                    //for (int i = 0; i < dtHeader.Rows.Count; i++)
                    //{
                    //    if (!string.Equals(dtHeader.Rows[i]["TotalCar"].ToString(), string.Empty) && int.Parse(dtHeader.Rows[i]["TotalCar"].ToString()) > 0)
                    //        sum += int.Parse(dtHeader.Rows[i]["TotalCar"].ToString());
                    //}

                    if (sum > 0)
                    {
                        //SumCar = sum;
                        //rowTotalCarTab3.Visible = true;
                        //txtTotalCarTab3.Text = SumCar.ToString();
                        rowListTotalCarTab3.Visible = true;
                        rowListCarTab3.Visible = false;

                        dtDup = AccidentBLL.Instance.AccidentDuplicateTotal(ComplainDocID);
                        GridViewHelper.BindGridView(ref dgvScoreTotalCar, dtDup);
                    }
                    else
                    {
                        rowListTotalCarTab3.Visible = false;
                        rowListCarTab3.Visible = true;
                    }

                    dtCusScoreType = ComplainBLL.Instance.CusScoreSelectBLL();
                    DropDownListHelper.BindRadioButton(ref radCusScoreType, dtCusScoreType, "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME");

                    if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dtScoreList = AccidentBLL.Instance.AccidentScoreDetailASelect(ComplainDocID);
                        dtScoreListCar = AccidentBLL.Instance.AccidentScoreCarASelect(ComplainDocID);

                        if ((dtScoreList.Rows.Count == 0) || (dtScoreListCar.Rows.Count == 0))
                        {
                            dtScoreList = AccidentBLL.Instance.AccidentScoreDetailPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                            dtScoreListCar = AccidentBLL.Instance.AccidentScoreCarPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                        }
                    }
                    else
                    {
                        dtScoreList = AccidentBLL.Instance.AccidentScoreDetailPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                        dtScoreListCar = AccidentBLL.Instance.AccidentScoreCarPSelect(ComplainDocID, dtAppealData.Rows[0]["DREDUCE"].ToString(), dtAppealData.Rows[0]["SHEADID"].ToString());
                    }

                    dtScoreList.Columns["COST"].ColumnName = "Cost";
                    dtScoreList.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
                    dtScoreList.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
                    dtScoreList.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
                    dtScoreList.Columns["SCORE1"].ColumnName = "Score1";
                    dtScoreList.Columns["SCORE2"].ColumnName = "Score2";
                    dtScoreList.Columns["SCORE3"].ColumnName = "Score3";
                    dtScoreList.Columns["SCORE4"].ColumnName = "Score4";
                    dtScoreList.Columns["SCORE5"].ColumnName = "Score5";
                    dtScoreList.Columns["SCORE6"].ColumnName = "Score6";
                    dtScoreList.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
                    dtScoreList.Columns["TOTALCAR"].ColumnName = "TotalCar";

                    GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);

                    dt = new DataTable();
                    if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dt = AccidentBLL.Instance.AccidentScoreASelect(ComplainDocID);

                        if (dt.Rows.Count == 0)
                            dt = AccidentBLL.Instance.AccidentScoreSelect(ComplainDocID);
                    }
                    else
                    {
                        dt = AccidentBLL.Instance.AccidentScoreSelect(ComplainDocID);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        if (!string.Equals(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString(), "-1"))
                        {
                            radCusScoreType.SelectedValue = dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString();

                            txtPointFinal.Enabled = false;
                            txtCostFinal.Enabled = false;
                            txtDisableFinal.Enabled = false;

                            lblShowSumPoint.Text = "(ผลรวม : {0})";
                            lblShowSumCost.Text = "(ผลรวม : {0})";
                            lblShowSumDisable.Text = "(ผลรวม : {0})";

                            lblShowSumPoint.Visible = false;
                            lblShowSumCost.Visible = false;
                            lblShowSumDisable.Visible = false;

                            int row = this.GetSelectRow(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString());

                            this.RefreshCusScore();
                            //string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                            //switch (CusType)
                            //{
                            //    //case "MAX": this.FindMax(); break;
                            //    //case "SUM": this.FindSum("Normal"); break;
                            //    case "FREETEXT":
                            //        this.FindSum(string.Empty);

                            //        txtPointFinal.Enabled = true;
                            //        txtCostFinal.Enabled = true;
                            //        txtDisableFinal.Enabled = true;

                            //        lblShowSumPoint.Visible = true;
                            //        lblShowSumCost.Visible = true;
                            //        lblShowSumDisable.Visible = true; break;
                            //    //this.FreeText(); break;
                            //    default:
                            //        break;
                            //}

                            txtCostOther.Text = dt.Rows[0]["COST_OTHER"].ToString();
                            txtOilLose.Text = dt.Rows[0]["OIL_LOSE"].ToString();
                            this.CheckBlacklist();
                            if (string.Equals(dt.Rows[0]["BLACKLIST"].ToString(), "1"))
                            {
                                chkBlacklist.Checked = true;
                                chkBlacklist.Enabled = true;
                            }
                        }

                        //if (!string.Equals(dt.Rows[0]["TOTAL_POINT"].ToString(), "-1"))
                        //    txtPointFinal.Text = dt.Rows[0]["TOTAL_POINT"].ToString();

                        //if (!string.Equals(dt.Rows[0]["TOTAL_COST"].ToString(), "-1"))
                        //    txtCostFinal.Text = dt.Rows[0]["TOTAL_COST"].ToString();

                        //if (!string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-1"))
                        //    txtDisableFinal.Text = dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString();

                        Session["dtScoreList"] = dtScoreList;
                    }
                    #endregion
                }
                else//ข้อร้องเรียน
                {
                    #region ข้อร้องเรียน
                    StringBuilder ConditionData = new StringBuilder(); ;
                    ConditionData.Append(" AND TO_CHAR(c.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') = '" + dtAppealData.Rows[0]["DREDUCE"].ToString() + "'");
                    if (!string.Equals(dtAppealData.Rows[0]["SHEADID"].ToString(), string.Empty))
                        ConditionData.Append(" AND c.STRUCKID = '" + dtAppealData.Rows[0]["SHEADID"].ToString() + "'");

                    StringBuilder ConditionHeader = new StringBuilder();
                    ConditionHeader.Append(" AND TO_CHAR(TCOMPLAIN.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') = '" + dtAppealData.Rows[0]["DREDUCE"].ToString() + "'");
                    if (!string.Equals(dtAppealData.Rows[0]["SHEADID"].ToString(), string.Empty))
                        ConditionHeader.Append(" AND TCOMPLAIN.STRUCKID = '" + dtAppealData.Rows[0]["SHEADID"].ToString() + "'");

                    if (cmdSaveTab3.Visible)
                    {
                        dtData = ComplainBLL.Instance.ComplainSelectBLL(" AND c.DOC_ID = '" + ComplainDocID + "'" + ConditionData.ToString());
                        dtHeader = ComplainBLL.Instance.ComplainSelectHeaderBLL(" AND TCOMPLAIN.DOC_ID = '" + ComplainDocID + "'" + ConditionHeader.ToString());
                    }
                    else
                    {
                        dtData = ComplainBLL.Instance.ComplainSelectBLL(" AND c.DOC_ID = '" + ComplainDocID + "'" + ConditionData.ToString());
                        dtHeader = ComplainBLL.Instance.ComplainSelectHeaderBLL(" AND TCOMPLAIN.DOC_ID = '" + ComplainDocID + "'" + ConditionHeader.ToString());
                    }

                    txtRemarkTab3.Text = dtData.Rows[0]["REMARK_TAB3"].ToString();
                    lblCusScoreHeader.Text = string.Format(lblCusScoreHeader.Text, dtHeader.Rows[0]["CONTRACTNAME"].ToString());

                    dtScore = ComplainBLL.Instance.ScoreSelectBLL();

                    DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
                    this.SelectDuplicateCar();

                    dtScoreList = new DataTable();
                    dtScoreList.Columns.Add("STOPICID");
                    dtScoreList.Columns.Add("STOPICNAME");
                    dtScoreList.Columns.Add("Cost");
                    dtScoreList.Columns.Add("CostDisplay");
                    dtScoreList.Columns.Add("DisableDriver");
                    dtScoreList.Columns.Add("DisableDriverDisplay");
                    dtScoreList.Columns.Add("Score1");
                    dtScoreList.Columns.Add("Score2");
                    dtScoreList.Columns.Add("Score3");
                    dtScoreList.Columns.Add("Score4");
                    dtScoreList.Columns.Add("Score5");
                    dtScoreList.Columns.Add("Score6");
                    dtScoreList.Columns.Add("Score6Display");
                    dtScoreList.Columns.Add("TotalCar");

                    dtScoreListCar = new DataTable();
                    dtScoreListCar.Columns.Add("STOPICID");
                    dtScoreListCar.Columns.Add("TRUCKID");
                    dtScoreListCar.Columns.Add("CARHEAD");
                    dtScoreListCar.Columns.Add("CARDETAIL");
                    dtScoreListCar.Columns.Add("DELIVERY_DATE");
                    dtScoreListCar.Columns.Add("TOTAL_CAR");

                    int sum = 0;
                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                    {
                        if (!string.Equals(dtHeader.Rows[i]["TotalCar"].ToString(), string.Empty) && int.Parse(dtHeader.Rows[i]["TotalCar"].ToString()) > 0)
                            sum += int.Parse(dtHeader.Rows[i]["TotalCar"].ToString());
                    }

                    if (sum > 0)
                    {
                        //SumCar = sum;
                        //rowTotalCarTab3.Visible = true;
                        //txtTotalCarTab3.Text = SumCar.ToString();
                        rowListTotalCarTab3.Visible = true;
                        rowListCarTab3.Visible = false;

                        dtDup = ComplainBLL.Instance.SelectDuplicateTotalCarPartialBLL(ComplainDocID, " AND TO_CHAR(TCOMPLAIN.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') = '" + dtAppealData.Rows[0]["DREDUCE"].ToString() + "'");
                        GridViewHelper.BindGridView(ref dgvScoreTotalCar, dtDup);
                    }
                    else
                    {
                        rowListTotalCarTab3.Visible = false;
                        rowListCarTab3.Visible = true;
                    }

                    dtCusScoreType = ComplainBLL.Instance.CusScoreSelectBLL();
                    DropDownListHelper.BindRadioButton(ref radCusScoreType, dtCusScoreType, "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME");

                    if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailAppealBLL(ComplainDocID);
                        dtScoreListCar = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreCarAppealBLL(ComplainDocID);

                        if ((dtScoreList.Rows.Count == 0) || (dtScoreListCar.Rows.Count == 0))
                        {
                            dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                            dtScoreListCar = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreCarPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                        }
                    }
                    else
                    {
                        dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                        dtScoreListCar = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreCarPartialBLL(ComplainDocID, dtAppealData.Rows[0]["SHEADID"].ToString(), dtAppealData.Rows[0]["DREDUCE"].ToString());
                    }

                    dtScoreList.Columns["COST"].ColumnName = "Cost";
                    dtScoreList.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
                    dtScoreList.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
                    dtScoreList.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
                    dtScoreList.Columns["SCORE1"].ColumnName = "Score1";
                    dtScoreList.Columns["SCORE2"].ColumnName = "Score2";
                    dtScoreList.Columns["SCORE3"].ColumnName = "Score3";
                    dtScoreList.Columns["SCORE4"].ColumnName = "Score4";
                    dtScoreList.Columns["SCORE5"].ColumnName = "Score5";
                    dtScoreList.Columns["SCORE6"].ColumnName = "Score6";
                    dtScoreList.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
                    dtScoreList.Columns["TOTALCAR"].ColumnName = "TotalCar";

                    GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);

                    DataTable dt = new DataTable();
                    if (Session["IsAppeal"].ToString() == "1")
                    {//มีข้อมูลยื่นอุทธรณ์แล้ว
                        dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreAppealBLL(ComplainDocID);

                        if (dt.Rows.Count == 0)
                            dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreBLL(ComplainDocID);
                    }
                    else
                    {
                        dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreBLL(ComplainDocID);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        if (!string.Equals(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString(), "-1"))
                        {
                            radCusScoreType.SelectedValue = dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString();

                            txtPointFinal.Enabled = false;
                            txtCostFinal.Enabled = false;
                            txtDisableFinal.Enabled = false;

                            lblShowSumPoint.Text = "(ผลรวม : {0})";
                            lblShowSumCost.Text = "(ผลรวม : {0})";
                            lblShowSumDisable.Text = "(ผลรวม : {0})";

                            lblShowSumPoint.Visible = false;
                            lblShowSumCost.Visible = false;
                            lblShowSumDisable.Visible = false;

                            int row = this.GetSelectRow(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString());

                            this.RefreshCusScore();
                            //string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                            //switch (CusType)
                            //{
                            //    //case "MAX": this.FindMax(); break;
                            //    //case "SUM": this.FindSum("Normal"); break;
                            //    case "FREETEXT":
                            //        this.FindSum(string.Empty);

                            //        txtPointFinal.Enabled = true;
                            //        txtCostFinal.Enabled = true;
                            //        txtDisableFinal.Enabled = true;

                            //        lblShowSumPoint.Visible = true;
                            //        lblShowSumCost.Visible = true;
                            //        lblShowSumDisable.Visible = true; break;
                            //    //this.FreeText(); break;
                            //    default:
                            //        break;
                            //}

                            txtCostOther.Text = dt.Rows[0]["COST_OTHER"].ToString();
                            txtOilLose.Text = dt.Rows[0]["OIL_LOSE"].ToString();
                            this.CheckBlacklist();
                            if (string.Equals(dt.Rows[0]["BLACKLIST"].ToString(), "1"))
                            {
                                chkBlacklist.Checked = true;
                                chkBlacklist.Enabled = true;
                            }
                        }
                        if (string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-2"))
                        {
                            txtDisableFinal.Text = "Hold";
                            txtDisableFinal.ReadOnly = true;
                        }
                        //if (!string.Equals(dt.Rows[0]["TOTAL_POINT"].ToString(), "-1"))
                        //    txtPointFinal.Text = dt.Rows[0]["TOTAL_POINT"].ToString();

                        //if (!string.Equals(dt.Rows[0]["TOTAL_COST"].ToString(), "-1"))
                        //    txtCostFinal.Text = dt.Rows[0]["TOTAL_COST"].ToString();

                        //if (!string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-1"))
                        //    txtDisableFinal.Text = dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString();

                        Session["dtScoreList"] = dtScoreList;
                    }
                    #endregion
                }

            }

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowScore", "$('#ShowScore').modal();", true);
            updShowScore.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckBlacklist()
    {
        try
        {
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (this.CheckTopicBlacklist(dtScoreList.Rows[i]["STOPICID"].ToString()))
                {
                    chkBlacklist.Enabled = true;
                    return;
                }
            }

            chkBlacklist.Checked = false;
            chkBlacklist.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private bool CheckTopicBlacklist(string STOPICID)
    {
        try
        {
            for (int i = 0; i < dtScore.Rows.Count; i++)
            {
                if (string.Equals(dtScore.Rows[i]["STOPICID"].ToString(), STOPICID) && string.Equals(dtScore.Rows[i]["BLACKLIST"].ToString(), "1"))
                    return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FindSum(string ShowType)
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                PointMax += decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtPointFinal.Text = PointMax.ToString();
            else
                lblShowSumPoint.Text = string.Format(lblShowSumPoint.Text, PointMax.ToString());

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                CostMax += decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtCostFinal.Text = CostMax.ToString();
            else
                lblShowSumCost.Text = string.Format(lblShowSumCost.Text, CostMax.ToString());

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                DisableDriverMax += int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            if (DisableDriverMax == -2)
            {
                txtDisableFinal.Text = "Hold";
                lblShowSumDisable.Text = "Hold";
            }
            else
            {
                if (string.Equals(ShowType, "Normal"))
                    txtDisableFinal.Text = DisableDriverMax.ToString();
                else
                    lblShowSumDisable.Text = string.Format(lblShowSumDisable.Text, DisableDriverMax.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int GetSelectRow(string Value)
    {
        try
        {
            for (int i = 0; i < dtCusScoreType.Rows.Count; i++)
            {
                if (string.Equals(dtCusScoreType.Rows[i]["CUSSCORE_TYPE_ID"].ToString(), Value))
                    return i;
            }

            return -1;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SelectDuplicateCarAcc()
    {
        try
        {
            dtCarDistinct = new DataTable();
            dtCarDistinct.Columns.Add("DELIVERY_DATE");
            dtCarDistinct.Columns.Add("TRUCKID");
            dtCarDistinct.Columns.Add("CARHEAD");
            dtCarDistinct.Columns.Add("CARDETAIL");
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    dr = ds.Tables[0].Rows[i];
                    if (string.Equals(dr["STRUCKID"].ToString(), string.Empty))
                        continue;

                    if (dtCarDistinct.Rows.Count == 0)
                    {
                        dtCarDistinct.Rows.Add(DateTime.Parse(dr["ACCIDENT_DATE"].ToString()).ToString("dd/MM/yyyy HH:mm"), dr["STRUCKID"].ToString(), dr["SHEADREGISTERNO"].ToString(), dr["STRAILERREGISTERNO"].ToString());
                    }
                    else
                    {
                        int j;
                        for (j = 0; j < dtCarDistinct.Rows.Count; j++)
                        {
                            if (string.Equals(dtCarDistinct.Rows[j]["STRUCKID"].ToString(), dr["STRUCKID"].ToString()) && string.Equals(dtCarDistinct.Rows[j]["DELIVERY_DATE"].ToString(), dr["ACCIDENT_DATE"].ToString()))
                                break;
                        }
                        if ((j == dtCarDistinct.Rows.Count) && ((!string.Equals(dtCarDistinct.Rows[j - 1]["STRUCKID"].ToString(), dr["STRUCKID"].ToString())) || (!string.Equals(dtCarDistinct.Rows[j - 1]["DELIVERY_DATE"].ToString(), dr["ACCIDENT_DATE"].ToString()))))
                            dtCarDistinct.Rows.Add(dr["ACCIDENT_DATE"].ToString(), dr["STRUCKID"].ToString(), dr["SHEADREGISTERNO"].ToString(), dr["STRAILERREGISTERNO"].ToString());
                    }
                }
            }


            GridViewHelper.BindGridView(ref dgvScore, dtCarDistinct);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SelectDuplicateCar()
    {
        try
        {
            dtCarDistinct = new DataTable();
            dtCarDistinct.Columns.Add("DELIVERY_DATE");
            dtCarDistinct.Columns.Add("TRUCKID");
            dtCarDistinct.Columns.Add("CARHEAD");
            dtCarDistinct.Columns.Add("CARDETAIL");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (string.Equals(dtHeader.Rows[i]["TRUCKID"].ToString(), string.Empty))
                    continue;

                if (dtCarDistinct.Rows.Count == 0)
                {
                    dtCarDistinct.Rows.Add(dtHeader.Rows[i]["DELIVERYDATE"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
                else
                {
                    int j;
                    for (j = 0; j < dtCarDistinct.Rows.Count; j++)
                    {
                        if (string.Equals(dtCarDistinct.Rows[j]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString()) && string.Equals(dtCarDistinct.Rows[j]["DELIVERY_DATE"].ToString(), dtHeader.Rows[i]["DELIVERYDATE"].ToString()))
                            break;
                    }
                    if ((j == dtCarDistinct.Rows.Count) && ((!string.Equals(dtCarDistinct.Rows[j - 1]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString())) || (!string.Equals(dtCarDistinct.Rows[j - 1]["DELIVERY_DATE"].ToString(), dtHeader.Rows[i]["DELIVERYDATE"].ToString()))))
                        dtCarDistinct.Rows.Add(dtHeader.Rows[i]["DELIVERYDATE"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
            }

            GridViewHelper.BindGridView(ref dgvScore, dtCarDistinct);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cboScore_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.EnableScore(false);

            if (cboScore.SelectedIndex == 0)
            {
                this.ClearScreenTab3();
            }
            else
            {
                txtCost.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString();
                if (cboScore.SelectedValue == "115")
                {
                    txtDisableDriver.Text = "Hold";
                    txtDisableDriver.Enabled = false;
                }
                else
                {
                    txtDisableDriver.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString();
                }
                txtScore1.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL02"].ToString();
                txtScore2.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL03"].ToString();
                txtScore3.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL04"].ToString();
                txtScore4.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL06"].ToString();
                txtScore5.Text = dtScore.Rows[cboScore.SelectedIndex]["NMULTIPLEIMPACT"].ToString();
                txtScore6.Text = dtScore.Rows[cboScore.SelectedIndex]["NPOINT"].ToString();
                lblShowOption.Visible = (dtScore.Rows[cboScore.SelectedIndex]["IS_CORRUPT"].ToString() == "0") ? false : true;

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("น้ำมัน"))
                {
                    txtOilPrice.Text = "0";
                    txtOilQty.Text = "0";
                    rowOil.Visible = true;
                }
                else
                {
                    txtOilPrice.Text = string.Empty;
                    txtOilQty.Text = string.Empty;
                    rowOil.Visible = false;
                }

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("ซีล"))
                {
                    txtZeal.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString();
                    txtZealQty.Text = "0";
                    rowZeal.Visible = true;
                }
                else
                {
                    txtZeal.Text = string.Empty;
                    txtZealQty.Text = string.Empty;
                    rowZeal.Visible = false;
                }

                if ((!lblShowOption.Visible) && (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["CAN_EDIT"].ToString(), "1")))
                {//สามารถแก้ไขคะแนนได้
                    this.EnableScore(true);
                }

                if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["BLACKLIST"].ToString(), "1"))
                {
                    chkBlacklist.Checked = true;
                    chkBlacklist.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ClearScreenTab3()
    {
        try
        {
            cboScore.SelectedIndex = 0;
            txtCost.Text = string.Empty;
            txtDisableDriver.Text = string.Empty;
            txtScore1.Text = string.Empty;
            txtScore2.Text = string.Empty;
            txtScore3.Text = string.Empty;
            txtScore4.Text = string.Empty;
            txtScore5.Text = string.Empty;
            txtScore6.Text = string.Empty;
            lblShowOption.Visible = false;

            txtOilPrice.Text = string.Empty;
            txtOilQty.Text = string.Empty;
            txtZeal.Text = string.Empty;
            txtZealQty.Text = string.Empty;

            rowOil.Visible = false;
            rowZeal.Visible = false;

            this.EnableScore(false);

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = true;
            }
            ((CheckBox)this.FindControlRecursive(dgvScore, "chkChooseScoreAll")).Checked = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void EnableScore(bool Enable)
    {
        txtScore1.Enabled = Enable;
        txtScore2.Enabled = Enable;
        txtScore3.Enabled = Enable;
        txtScore4.Enabled = Enable;
    }

    protected void cmdAddScore_Click(object sender, EventArgs e)
    {
        try
        {
            //IsChange = true;

            int CountCar = this.ValidateAddScore();

            string CostDisplay = string.Empty;
            string Score6Display = string.Empty;
            string DisableDriverDisplay = string.Empty;

            CostDisplay = txtCost.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString();
            Score6Display = txtScore6.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString();
            if (txtDisableDriver.Text.Trim() == "Hold")
            {
                DisableDriverDisplay = "Hold";
            }
            else
            {
                DisableDriverDisplay = txtDisableDriver.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString();
            }

            dtScoreList.Rows.Add(cboScore.SelectedValue, cboScore.SelectedItem, (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString(), CostDisplay, txtDisableDriver.Text.Trim() == "Hold" ? "-2" : (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString(), DisableDriverDisplay, txtScore1.Text.Trim(), txtScore2.Text.Trim(), txtScore3.Text.Trim(), txtScore4.Text.Trim(), txtScore5.Text.Trim(), (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString(), Score6Display, CountCar);

            CheckBox Chk;
            if (rowListTotalCarTab3.Visible)
            {//ป้อนจำนวนคัน
                TextBox Txt;

                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                    {
                        Txt = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, string.Empty, string.Empty, string.Empty, dtDup.Rows[i]["DELIVERY_DATE"].ToString(), Txt.Text.Trim());
                    }
                }
            }
            else
            {//เลือกทะเบียนรถ
                for (int i = 0; i < dgvScore.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                    {
                        DateTime dtt = new DateTime();
                        if (DateTime.TryParseExact(dtCarDistinct.Rows[i]["DELIVERY_DATE"] + string.Empty, DateTimeFormat, new CultureInfo("en-US"), DateTimeStyles.None, out dtt))
                        {

                        }
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtCarDistinct.Rows[i]["CARHEAD"].ToString(), dtCarDistinct.Rows[i]["CARDETAIL"].ToString(), dtt.ToString(DateFormat), 0);
                    }

                }
            }

            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);
            this.ClearScreenTab3();
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void RefreshCusScore()
    {
        try
        {
            if (!string.Equals(radCusScoreType.SelectedValue, string.Empty))
            {
                int row = this.GetSelectRow(radCusScoreType.SelectedValue);

                txtPointFinal.Enabled = false;
                txtCostFinal.Enabled = false;
                txtDisableFinal.Enabled = false;

                lblShowSumPoint.Text = "(Sum : {0})";
                lblShowSumCost.Text = "(Sum : {0})";
                lblShowSumDisable.Text = "(Sum : {0})";

                lblShowSumPoint.Visible = false;
                lblShowSumCost.Visible = false;
                lblShowSumDisable.Visible = false;

                string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                switch (CusType)
                {
                    case "MAX": this.FindMax(); break;
                    case "SUM": this.FindSum("Normal"); break;
                    case "FREETEXT":
                        lblShowSumPoint.Visible = true;
                        lblShowSumCost.Visible = true;
                        lblShowSumDisable.Visible = true;
                        this.FreeText(); break;
                    default:
                        break;
                }
            }
            else
            {
                //txtPointFinal.Text = "0";
                //txtCostFinal.Text = "0";
                //txtDisableFinal.Text = "0";
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void FreeText()
    {
        try
        {
            txtPointFinal.Text = string.Empty;
            txtCostFinal.Text = string.Empty;
            txtDisableFinal.Text = string.Empty;

            this.FindSum("ForFreeText");

            txtPointFinal.Enabled = true;
            txtCostFinal.Enabled = true;
            txtDisableFinal.Enabled = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void FindMax()
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString()) > PointMax)
                    PointMax = decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            }
            txtPointFinal.Text = PointMax.ToString();

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString()) > CostMax)
                    CostMax = decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            }
            txtCostFinal.Text = CostMax.ToString();

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (dtScoreList.Rows[i]["DisableDriver"] + string.Empty == "-2")
                {
                    DisableDriverMax = -2;
                }
                else if (decimal.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString()) > DisableDriverMax)
                    DisableDriverMax = int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            }
            if (DisableDriverMax == -2)
            {
                txtDisableFinal.Text = "Hold";
                txtDisableFinal.ReadOnly = true;
            }
            else
            {
                txtDisableFinal.Text = DisableDriverMax.ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int ValidateAddScore()
    {
        try
        {
            if (cboScore.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก หัวข้อตัดคะแนน");

            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (string.Equals(dtScoreList.Rows[i]["STOPICID"].ToString(), cboScore.SelectedValue))
                    throw new Exception("เลือกหัวข้อ ตัดคะแนน ซ้ำกัน");
            }

            if (string.Equals(txtScore1.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ภาพลักษณ์องค์กร");

            if (string.Equals(txtScore2.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ความพึงพอใจลูกค้า");

            if (string.Equals(txtScore3.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน กฎ/ระเบียบฯ");

            if (string.Equals(txtScore4.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน แผนงานขนส่ง");

            if (rowOil.Visible)
            {
                if (string.Equals(txtOilQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนน้ำมัน");

                if (string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน ค่าน้ำมัน");
            }

            if (rowZeal.Visible)
            {
                if (string.Equals(txtZealQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนซิล");
            }

            if (rowListTotalCarTab3.Visible)
            {
                CheckBox Chk;
                TextBox Txt1;
                int MaxNumber = 0;
                int tmp;
                int TotalCar = 0;
                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");

                    if (Chk.Checked)
                    {
                        MaxNumber = int.Parse(dtDup.Rows[i]["TOTAL_CAR"].ToString());

                        Txt1 = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        if (string.Equals(Txt1.Text.Trim(), string.Empty))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน)");
                        if (!int.TryParse(Txt1.Text.Trim(), out tmp))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน) เป็นตัวเลข");
                        if (MaxNumber < int.Parse(Txt1.Text.Trim()))
                            throw new Exception("จำนวนรถที่ป้อน มีค่ามากกว่า จำนวนรถที่ถูกร้องเรียน");

                        TotalCar += int.Parse(Txt1.Text.Trim());
                    }
                }
                return TotalCar;
            }
            else
                return this.CountCar();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int CountCar()
    {
        try
        {
            int CountCar = 0;

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                if (Chk.Checked)
                {
                    CountCar += 1;
                }
            }

            if (CountCar == 0)
                throw new Exception("กรุณาเลือก ทะเบียนรถ ที่ต้องการตัดคะแนน");

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void radCusScoreType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void chkChooseScoreAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvScore_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dtTmp = dtScoreListCar.Copy();
            dtTmp.Rows.Clear();

            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                //if (!string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[e.RowIndex]["STOPICID"].ToString()))
                //    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
                if (i != e.RowIndex)
                    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
            }
            dtScoreListCar = dtTmp;

            dtScoreList.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvScoreList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            IndexEdit = Index;

            if (string.Equals(e.CommandName, "Select"))
            {
                DataTable dtTmp = dtScoreList.Copy();
                //string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ภาพลักษณ์องค์กร", "ความพึงพอใจลูกค้า", "กฎ/ระเบียบฯ", "แผนงานขนส่ง", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };

                string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };
                dtTmp.Columns["STOPICNAME"].ColumnName = "หัวข้อตัดคะแนน";
                dtTmp.Columns["Cost"].ColumnName = "ค่าปรับ";
                dtTmp.Columns["DisableDriver"].ColumnName = "ระงับ พขร. (วัน)";
                //dtTmp.Columns["Score1"].ColumnName = "ภาพลักษณ์องค์กร";
                //dtTmp.Columns["Score2"].ColumnName = "ความพึงพอใจลูกค้า";
                //dtTmp.Columns["Score3"].ColumnName = "กฎ/ระเบียบฯ";
                //dtTmp.Columns["Score4"].ColumnName = "แผนงานขนส่ง";
                dtTmp.Columns["Score5"].ColumnName = "ผลคูณความรุนแรง";
                dtTmp.Columns["Score6"].ColumnName = "หักคะแนนต่อครั้ง";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                for (int i = 0; i < dtTmp.Columns.Count; i++)
                {
                    if (display.Contains(dtTmp.Columns[i].ColumnName))
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dtTmp.Columns[i].ColumnName + " : " + dtTmp.Rows[Index][i].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    if (i == dtTmp.Columns.Count - 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        //sb.Append("ทะเบียนรถ : " + this.GetCarList(dtTmp.Rows[Index]["STOPICID"].ToString()));
                        if (rowListTotalCarTab3.Visible)
                        {
                            //sb.Append("จำนวนรถ : " + dtTmp.Rows[Index]["TotalCar"].ToString() + " คัน");
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        else
                        {
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private int GetCarListCount(string STOPICID)
    {
        try
        {
            int CountCar = 0;
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (rowListTotalCarTab3.Visible)
                        CountCar += int.Parse(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                    else
                        CountCar += 1;
                }
            }

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetCarListString(string STOPICID)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (sb.Length > 0)
                        sb.Append(", ");

                    if (rowListTotalCarTab3.Visible)
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                        sb.Append(" คัน");
                    }
                    else
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["CARHEAD"].ToString());
                        if (!string.Equals(dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), string.Empty))
                        {
                            sb.Append("/");
                            sb.Append(dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
                        }
                    }
                }
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkChooseScoreListCarAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdCloseTab3_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
        if (Session["dtUpload"] != null)
        {
            lblAttachTotal.Text = string.Format(lblAttachTotal.Text, ((DataTable)Session["dtUpload"]).Rows.Count.ToString());

        }
        else
        {
            lblAttachTotal.Text = string.Format(lblAttachTotal.Text, "0");
        }
        rblStatus4.Value = Session["rblStatus"];
        rblCSENTENCE4.Value = Session["rblCSENTENCE4"];
    }

    private void CheckPostBack()
    {
        try
        {
            var ctrlName = Request.Params[Page.postEventSourceID];
            var args = Request.Params[Page.postEventArgumentID];

            if (ctrlName == txtScore1.UniqueID && args == "txtScore1_OnKeyPress")
                txtScore1_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore2.UniqueID && args == "txtScore2_OnKeyPress")
                txtScore2_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore3.UniqueID && args == "txtScore3_OnKeyPress")
                txtScore3_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore4.UniqueID && args == "txtScore4_OnKeyPress")
                txtScore4_OnKeyPress(ctrlName, args);

            if (ctrlName == txtZealQty.UniqueID && args == "txtZealQty_OnKeyPress")
                txtZealQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilQty.UniqueID && args == "txtOilQty_OnKeyPress")
                txtOilQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilPrice.UniqueID && args == "txtOilPrice_OnKeyPress")
                txtOilPrice_OnKeyPress(ctrlName, args);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void txtScore1_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore2_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore3_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtScore4_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CalulateTotal()
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtScore1.Text.Trim(), out tmp) || (decimal.Parse(txtScore1.Text.Trim()) < 0))
                txtScore1.Text = "0";

            if (!decimal.TryParse(txtScore2.Text.Trim(), out tmp) || (decimal.Parse(txtScore2.Text.Trim()) < 0))
                txtScore2.Text = "0";

            if (!decimal.TryParse(txtScore3.Text.Trim(), out tmp) || (decimal.Parse(txtScore3.Text.Trim()) < 0))
                txtScore3.Text = "0";

            if (!decimal.TryParse(txtScore4.Text.Trim(), out tmp) || (decimal.Parse(txtScore4.Text.Trim()) < 0))
                txtScore4.Text = "0";

            int sum = 1;
            sum *= (txtScore1.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore1.Text.Trim());
            sum *= (txtScore2.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore2.Text.Trim());
            sum *= (txtScore3.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore3.Text.Trim());
            sum *= (txtScore4.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore4.Text.Trim());
            txtScore5.Text = sum.ToString();

            DataTable dt = ComplainBLL.Instance.SelectScorePointBLL(txtScore5.Text.Trim());
            if (dt.Rows.Count > 0)
                txtScore6.Text = dt.Rows[0]["NSCORE"].ToString();
            else
                txtScore6.Text = "0";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void txtZealQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            int tmp;
            if (!int.TryParse(txtZealQty.Text.Trim(), out tmp) || decimal.Parse(txtZealQty.Text.Trim()) < 0)
            {
                txtZealQty.Text = "0";
                txtCost.Text = "0";
                return;
            }

            txtZealQty.Text = int.Parse(txtZealQty.Text.Trim()).ToString();
            txtCost.Text = (int.Parse(txtZealQty.Text.Trim()) * int.Parse(txtZeal.Text.Trim())).ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void txtOilQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtOilQty.Text.Trim(), out tmp) || decimal.Parse(txtOilQty.Text.Trim()) < 0)
                txtOilQty.Text = "0";

            if (decimal.Parse(txtOilQty.Text.Trim()) > 0)
            {
                if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString(), "0") && decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
                else if (!string.Equals(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString(), "0") && (decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()) || decimal.Parse(txtOilQty.Text.Trim()) > decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString())))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
            }

            txtOilQty.Text = decimal.Parse(txtOilQty.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void txtOilPrice_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            txtOilPrice.ReadOnly = true;
            decimal tmp;
            if (!decimal.TryParse(txtOilPrice.Text.Trim(), out tmp) || decimal.Parse(txtOilPrice.Text.Trim()) < 0)
                txtOilPrice.Text = "0";

            txtOilPrice.Text = decimal.Parse(txtOilPrice.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
        finally
        {
            txtOilPrice.ReadOnly = false;
        }
    }

    private void CalculateOil()
    {
        try
        {
            if (!string.Equals(txtOilQty.Text.Trim(), string.Empty) && !string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                txtCost.Text = (int.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString()) * decimal.Parse(txtOilPrice.Text.Trim()) * decimal.Parse(txtOilQty.Text.Trim())).ToString();
            else
                txtCost.Text = "0";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        //ComplainImportFileBLL.Instance.ComplainTab3AppealSaveBLL(DocID, int.Parse(Session["UserID"].ToString()), (radCusScoreType.SelectedIndex > -1) ? int.Parse(radCusScoreType.SelectedValue) : -1, (!string.Equals(txtPointFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtPointFinal.Text.Trim()) : -1, (!string.Equals(txtCostFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostFinal.Text.Trim()) : -1, (!string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? int.Parse(txtDisableFinal.Text.Trim()) : -1, dtScoreList, dtScoreListCar, decimal.Parse(txtCostOther.Text.Trim()), 0, txtRemarkTab3.Text.Trim(), decimal.Parse(txtOilLose.Text.Trim()));
    }

    protected void cmdSaveTab3_Click(object sender, EventArgs e)
    {
        if (dtScoreList.Rows.Count == 0)
        {
            alertFail("กรุณาเลือกหัวข้อตัดคะแนน");
            return;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
        lblAttachTotal.Text = string.Format(lblAttachTotal.Text, ((DataTable)Session["dtUpload"]).Rows.Count.ToString());
        rblStatus4.Value = Session["rblStatus"];
        rblCSENTENCE4.Value = Session["rblCSENTENCE4"];
    }

    private void ValidateAppealSave()
    {
        try
        {
            ASPxPageControl PageControl = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
            ASPxRadioButtonList rblStatus4 = (ASPxRadioButtonList)PageControl.FindControl("rblStatus4");

            if (string.Equals(rblStatus4.Value, "3") && (Session["dtScoreList"] == null || ((DataTable)Session["dtScoreList"]).Rows.Count == 0))
            {//เปลี่ยนผลการพิจารณา
                throw new Exception("กรุณา กดปุ่ม ผลการพิจารณาใหม่ เพื่อเลือกหัวข้อตัดคะแนน");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dtUpload = (DataTable)Session["dtUpload"];
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            Session["dtUpload"] = dtUpload;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();

        Session["Appeal_Download"] = FullPath;
        FormHelper.OpenForm("DownloadFile.aspx", Page);
        //this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    //private void DownloadFile(string Path, string FileName)
    //{
    //    Response.ContentType = "APPLICATION/OCTET-STREAM";
    //    Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
    //    Response.TransmitFile(Path + "\\" + FileName);
    //    Response.End();
    //}

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        object rblStatus4dd = rblStatus4.Value;
        this.StartUpload();
    }

    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN_APPEAL");
        DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        if (Session["dtUpload"] == null)
        {
            DataTable dtUpload = new DataTable();
            dtUpload.Columns.Add("UPLOAD_ID");
            dtUpload.Columns.Add("UPLOAD_NAME");
            dtUpload.Columns.Add("FILENAME_SYSTEM");
            dtUpload.Columns.Add("FILENAME_USER");
            dtUpload.Columns.Add("FULLPATH");
            Session["dtUpload"] = dtUpload;
        }

        GridViewHelper.BindGridView(ref dgvUploadFile, (DataTable)Session["dtUpload"]);
    }

    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                rblStatus4.Value = Session["rblStatus"];
                rblCSENTENCE4.Value = Session["rblCSENTENCE4"];
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                DataTable dtUpload = (DataTable)Session["dtUpload"];
                dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
                Session["dtUpload"] = dtUpload;

                lblAttachTotal.Text = string.Format(lblAttachTotal.Text, dtUpload.Rows.Count.ToString());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowAttach", "$('#ShowAttach').modal();", true);
                udpAttach.Update();
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAttach_Click(object sender, EventArgs e)
    {
        DocIDEdit = gvw.GetRowValues(int.Parse(Session["IndexEdit"].ToString()), "STOPICNAME") + "";
        this.InitialUpload();

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowAttach", "$('#ShowAttach').modal();", true);
        udpAttach.Update();
    }
    protected void cmdClose_Click(object sender, EventArgs e)
    {
        lblAttachTotal.Text = string.Format(lblAttachTotal.Text, ((DataTable)Session["dtUpload"]).Rows.Count.ToString());
        rblStatus4.Value = Session["rblStatus"];
        rblCSENTENCE4.Value = Session["rblCSENTENCE4"];
        Session.Remove("rblStatus");
        Session.Remove("rblCSENTENCE4");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalAttach();", true);
    }

    private void Export()
    {
        try
        {
            this.GetData();

            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();

            workbook.Worksheets.Add("Detail");
            ExcelWorksheet worksheet = workbook.Worksheets["Detail"];
            DataTable dtFinal = dtExportData.Copy();

            this.SetFormatCell(worksheet.Cells[0, 0], "ที่", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 0, 1, 0).Merged = true;
            this.SetFormatCell(worksheet.Cells[0, 1], "วันที่เกิดเหตุ", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 1, 1, 1).Merged = true;
            this.SetFormatCell(worksheet.Cells[0, 2], "เลขที่สัญญา", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 2, 1, 2).Merged = true;
            this.SetFormatCell(worksheet.Cells[0, 3], "Outbound No.", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 3, 1, 3).Merged = true;
            this.SetFormatCell(worksheet.Cells[0, 4], "ประเภท", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 4, 1, 4).Merged = true;
            this.SetFormatCell(worksheet.Cells[0, 5], "หัวข้อปัญหา", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 5, 1, 5).Merged = true;
            this.SetFormatCell(worksheet.Cells[0, 6], "ทะเบียนรถ", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 6, 0, 7).Merged = true;
            this.SetFormatCell(worksheet.Cells[1, 6], "หัว", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 7], "ท้าย", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[0, 8], "คะแนนที่ตัด", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 8, 0, 9).Merged = true;
            this.SetFormatCell(worksheet.Cells[1, 8], "ก่อน", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 9], "หลัง", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[0, 10], "ค่าปรับ", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 10, 0, 11).Merged = true;
            this.SetFormatCell(worksheet.Cells[1, 10], "ก่อน", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 11], "หลัง", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[0, 12], "ระงับ พขร.", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 12, 0, 13).Merged = true;
            this.SetFormatCell(worksheet.Cells[1, 12], "ก่อน", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 13], "หลัง", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[0, 14], "Blacklist", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 14, 0, 15).Merged = true;
            this.SetFormatCell(worksheet.Cells[1, 14], "ก่อน", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[1, 15], "หลัง", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            this.SetFormatCell(worksheet.Cells[0, 16], "สถานะการอุทธรณ์", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 16, 1, 16).Merged = true;
            this.SetFormatCell(worksheet.Cells[0, 17], "รายละเอียดการขออุทธรณ์", VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells.GetSubrangeAbsolute(0, 17, 1, 17).Merged = true;

            dtFinal.Columns.Remove("CSTATUS");
            dtFinal.Columns.Remove("SREFERENCEID");

            for (int i = 2; i <= dtFinal.Rows.Count + 1; i++)
            {
                for (int j = 0; j < dtFinal.Columns.Count; j++)
                {//Export Detail
                    this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 2][j].ToString(), VerticalAlignmentStyle.Center, HorizontalAlignmentStyle.Center, false);
                }
            }

            int columnCount = worksheet.CalculateMaxUsedColumns();

            for (int i = 0; i < columnCount; i++)
                worksheet.Columns[i].AutoFit(1.3);

            string Path = this.CheckPath();
            string FileName = "Supplain_Report_"+DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetData()
    {
        try
        {
            if (string.Equals(dteStart.Text.Trim(), string.Empty) || string.Equals(dteEnd.Text.Trim(), string.Empty))
                throw new Exception("กรุณาระบุวันที่ค้นหา");
            dtExportData = ComplainBLL.Instance.SupplainSelectBLL(" AND COMPLAIN_DATE BETWEEN TO_DATE('" + dteStart.Text + "','DD/MM/YYYY  HH24:MI:SS') AND TO_DATE('" + dteEnd.Text + "','DD/MM/YYYY  HH24:MI:SS') AND (SCONTRACTNO LIKE '%' || '" + txtSearch.Text + "' || '%' OR SHEADREGISTERNO LIKE '%' || '" + txtSearch.Text + "' || '%' OR STRAILREGISTERNO LIKE '%' || '" + txtSearch.Text + "' || '%' OR SREFERENCEID LIKE '%' || '" + txtSearch.Text + "' || '%') AND nvl(CSTATUS,'0') != '3' ");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
            cell.Style.Font.Size = 12 * 20;
            cell.Style.Font.Name = "Calibri";
            cell.Style.NumberFormat = "_-* #,##0_-;-* #,##0_-;_-* \"\"??_-;_-@_-";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            this.Export();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}
