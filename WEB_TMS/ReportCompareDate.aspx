﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportCompareDate.aspx.cs" Inherits="ReportCompareDate" %>

<%@ Register Assembly="DevExpress.XtraCharts.v11.2.Web, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="70%" align="right">&nbsp;</td>
                                                <td width="9%">
                                                    <dx:ASPxComboBox runat="server" ID="cboMonth">
                                                        <Items>
                                                            <dx:ListEditItem Text="มกราคม" Value="01" />
                                                            <dx:ListEditItem Text="กุมภาพันธ์" Value="02" />
                                                            <dx:ListEditItem Text="มีนาคม" Value="03" />
                                                            <dx:ListEditItem Text="เมษายน" Value="04" />
                                                            <dx:ListEditItem Text="พฤษภาคม" Value="05" />
                                                            <dx:ListEditItem Text="มิถุนายน" Value="06" />
                                                            <dx:ListEditItem Text="กรกฎาคม" Value="07" />
                                                            <dx:ListEditItem Text="สิงหาคม" Value="08" />
                                                            <dx:ListEditItem Text="กันยายน" Value="09" />
                                                            <dx:ListEditItem Text="ตุลาคม" Value="10" />
                                                            <dx:ListEditItem Text="พฤศจิกายน" Value="11" />
                                                            <dx:ListEditItem Text="ธันวาคม" Value="12" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="2%">&nbsp;</td>
                                                <td width="9%">
                                                    <dx:ASPxComboBox runat="server" ID="cboYear">
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="10%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false">
                                                        <ClientSideEvents Click="function(){ xcpn.PerformCallback('search');}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="87%" height="35">
                                        <dx:ASPxLabel runat="server" ID="lblsHead" Text="รายงานเปรียบเทียบระยะเวลายื่นคำขอ "
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblsTail" Text="" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td width="13%">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                            <tr>
                                                <td width="37%"></td>
                                                <td width="37%"></td>
                                                <%--  <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnPDF" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="PDF">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('PDF');}" />
                                                        <Image Url="images/ic_pdf2.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnExcel" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="Excel">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('xls');}" />
                                                        <Image Url="images/ic_ms_excel.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>--%>
                                              <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnPDF" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="PDF" OnClick="btnPDF_Click">
                                                        <%--<ClientSideEvents Click="function(){ xcpn.PerformCallback('ReportPDF');}" />--%>
                                                        <Image Url="images/ic_pdf2.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnExcel" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="Excel" OnClick="btnExcel_Click">
                                                        <%--<ClientSideEvents Click="function(){ xcpn.PerformCallback('ReportExcel');}" />--%>
                                                        <Image Url="images/ic_ms_excel.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <%--  <dx:ReportViewer ID="rvw" runat="server" ClientInstanceName="rvw" Width="100%">
                                        </dx:ReportViewer>--%>
                                        <dx:ASPxTextBox runat="server" ID="txtSeriesName" ClientInstanceName="txtSeriesName"
                                            ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" ClientInstanceName="WebChartControl1"
                                            Height="400px" Width="900px">
                                            <clientsideevents objectselected="function(s, e) {
	 if (e.hitInfo.inSeries) {
       var obj = e.additionalHitObject;
      txtSeriesName.SetText(e.hitInfo.seriesPoint.argument);
        btnReport2.DoClick();
       if (obj != null){
           pcX = e.absoluteX;
           pcY = e.absoluteY;

           //cbp.PerformCallback(obj.argument);
       }
    }
}" />
                                        </dxchartsui:WebChartControl>
                                        <dx:ASPxButton runat="server" ID="btnReport2" ClientInstanceName="btnReport2" AutoPostBack="false"
                                            ClientVisible="false">
                                            <ClientSideEvents Click=" function(){gvw.PerformCallback('ListData;');}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="70%" AutoGenerateColumns="false"
                                            ClientInstanceName="gvw" OnAfterPerformCallback="gvw_AfterPerformCallback">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ชื่อบริษัท" Width="70%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="SABBREVIATION">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="จำนวนรถ" Width="30%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="Q1">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
