﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Web.Configuration;
public partial class vendor_home : System.Web.UI.Page
{
    string _conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }

        if (!IsPostBack)
        {
            //foreach (object objSession in HttpContext.Current.Session)
            //{
            //    Response.Write(objSession + " : " + HttpContext.Current.Session["" + objSession] + "<br>");
            //} 
            string sdate2day = DateTime.Today.ToString("dd MMM yyyy");
            spn_ConfirmContract.InnerHtml += sdate2day; //"สรุปรถยืนยันตามสัญญา" +
            spn_ConfirmPlan.InnerHtml += sdate2day;//"สรุปรถยืนยันตามแผน"+sdate2day
            spn_TruckIssue.InnerHtml += sdate2day;//"สรุปรถเกิดปัญหา"sdate2day
            spn_Plan2Day.InnerHtml += sdate2day;//"แผนงาน"+sdate2day
            ListConfTruck();
            ListConfPlan();
            ListAccident();
            ListPersonalInfo();
            ListApprove();
            //ListPlan2Day();
            ListAlert();
            Session["ssAlert"] = null;

            #region สิทธิการเข้าทำงานสอบเทียบ
            DataTable dtPer = CommonFunction.Get_Data(_conn, "SELECT SMENUID,CPERMISSION,SUID FROM TPERMISSION WHERE SUID = '" + CommonFunction.ReplaceInjection(Session["UserID"] + "") + "'");
            if (dtPer.Rows.Count > 0)
            {
                if (dtPer.Select("SMENUID = '55' AND CPERMISSION <> 0").Count() > 0)
                {

                    trVen.Visible = true;
                }
                else
                {
                    trVen.Visible = false;
                }
            }
            #endregion
        }
    }
    void ListConfTruck()
    {
        string[] ArrayConfTruckList = { "รถตามสัญญา", "ยืนยันแล้ว", "ไม่ยืนยัน" };

        string msg = ""
            , sOutHtml = @"<table border=""0"" width=""100%"">
                                <tr>
                                    <td width=""67%"" title=""จำนวนรถในสัญญาทั้งหมด(ไม่รวมรถสำรอง)"">{0}</td>
                                    <td align=""center"" width=""33%"">{1}</td>
                                </tr>
                                <tr visible=""{7}"" align=""center"">
                                    <td colspan=""2""><font color=""red"">{8}</font></td> 
                                </tr> 
                                <tr visible=""{6}"">
                                    <td  title=""จำนวนรถที่ได้ยืนยันรถตามสัญญา"">{2}</td>
                                    <td align=""center"">{3}</td>
                                </tr>
                                <tr>
                                    <td title=""จำนวนรถที่ไม่ได้ยืนยันรถตามสัญญา"">{4}</td>
                                    <td align=""center"">{5}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align=""center""></td>
                                </tr>
                            </table>"
            ,
        #region MyRegion
 sConfTruck = @"SELECT TCONT.SVENDORID  
,SUM(NVL(TCONT.NTRUCK,0)) NTRUCK 
,SUM(NVL(TCONF.CONFIRMED,0)) CONFIRMED
,SUM(NVL(TCONF.CONFIRMES,0)) CONFIRMES
FROM(
    SELECT DISTINCT CONT.SVENDORID,CONT.SCONTRACTID,COUNT(CONT_TRCK.STRUCKID)  NTRUCK
    FROM TCONTRACT CONT 
    LEFT JOIN TCONTRACT_TRUCK CONT_TRCK  ON CONT.SCONTRACTID=CONT_TRCK.SCONTRACTID
    LEFT JOIN TTRUCK TRCK ON CONT_TRCK.STRUCKID=TRCK.STRUCKID
    WHERE NVL(NTRUCK,0)>0 AND NVL(CACTIVE,'Y')='Y' AND CGROUPCONTRACT =('C') AND NVL(CSTANDBY,'N')='N'
    AND TO_DATE(SYSDATE,'dd/mm/yyyy') BETWEEN TO_DATE(CONT.DBEGIN,'dd/mm/yyyy') AND  TO_DATE(CONT.DEND,'dd/mm/yyyy')
    AND NVL(TRCK.CACTIVE,'N')='Y'
    AND CONT.SVENDORID='{0}' --0010001144     0010001778 
    GROUP BY CONT.SVENDORID,CONT.SCONTRACTID 
)TCONT
LEFT JOIN (
     SELECT SCONTRACTID 
     ,SUM(CASE WHEN NVL(i_trckconf.CCONFIRM,'0')='0' THEN 1 ELSE 0 END) CONFIRMES
    ,SUM(CASE WHEN NVL(i_trckconf.CCONFIRM,'0')='1' THEN 1 ELSE 0 END) CONFIRMED
     FROM TTRUCKCONFIRM t_trckconf
     LEFT JOIN TTRUCKCONFIRMLIST i_trckconf ON T_TRCKCONF.NCONFIRMID =I_TRCKCONF.NCONFIRMID
     WHERE 1=1 AND DDATE = TO_DATE('{1}','dd/MM/yyyy') AND NVL(t_trckconf.CCONFIRM,'0')='1'
     GROUP BY SCONTRACTID 
 ) TCONF ON TCONT.SCONTRACTID=TCONF.SCONTRACTID
 
 GROUP BY TCONT.SVENDORID";
        #endregion

        //Response.Write(string.Format(sConfTruck, "" + Session["SVDID"], DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["SVDID"], DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtConfTruck.Rows.Count > 1)
            {
                msg = "ไม่พบรายการ..";
                ltrvendor_conftruck.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!");
            }
            else if (dtConfTruck.Rows.Count == 1)
            {
                msg = "";
                ltrvendor_conftruck.Text = string.Format(sOutHtml, ArrayConfTruckList[0], " " + dtConfTruck.Rows[0]["NTRUCK"] + " คัน"
                     , ArrayConfTruckList[1], " " + dtConfTruck.Rows[0]["CONFIRMED"] + " คัน"
                     , ArrayConfTruckList[2], " " + dtConfTruck.Rows[0]["CONFIRMES"] + " คัน"
                     , true, !true, "");
            }
            else
            {
                msg = "ไม่พบรายการ";
                ltrvendor_conftruck.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ");
            }
        }
    }
    void ListConfPlan()
    {
        //string[] ArrayConfTruckList = { "รถตามแผน", "ยืนยันแล้ว", "ไม่ยืนยัน" };
        string[] ArrayConfTruckList = { "แผนงาน", "จัดงาน", "ยังไม่ได้จัด" };
        string msg = ""
            , sOutHtml = @"<table border=""0"" width=""100%"">
                                <tr>
                                    <td width=""67%"">{0}</td>
                                    <td align=""center"" width=""33%"">{1}</td>
                                </tr>
                                <tr visible=""{7}"" align=""center"">
                                    <td colspan=""2""><font color=""red"">{8}</font></td> 
                                </tr> 
                                <tr style=""display:{6}"">
                                    <td>{2}</td>
                                    <td align=""center"">{3}</td>
                                </tr>
                                <tr>
                                    <td>{4}</td>
                                    <td align=""center"">{5}</td>
                                </tr> 
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align=""center""></td>
                                </tr>
                            </table>"
            ,
        #region OLDVERSION
            // sConfTruck = @"SELECT SVENDORID ,COUNT(NPLANID) NRECORD ,SUM(CONFIRMED) CONFIRMED,SUM(CONFIRMES) CONFIRMES
            //FROM(
            //    SELECT t_vend.SVENDORID,(t_plnschd.NPLANID) NPLANID 
            //     ,MAX(CASE WHEN NVL(t_plnschd.CCONFIRM,'0')='0' THEN 1 ELSE 0 END) CONFIRMED
            //    ,MAX(CASE WHEN NVL(t_plnschd.CCONFIRM,'0')='1' THEN 1 ELSE 0 END) CONFIRMES 
            //     FROM  TVENDOR t_vend 
            //     LEFT JOIN TPLANSCHEDULE t_plnschd ON t_vend.SVENDORID =t_plnschd.SVENDORID
            //     LEFT JOIN TPLANSCHEDULELIST i_plnschd ON T_PLNSCHD.NPLANID =i_plnschd.NPLANID
            //    WHERE  t_vend.SVENDORID='{0}' --0010001144     0010001778
            //    AND TO_DATE(DDELIVERY,'DD/MM/YYYY') = TO_DATE('{1}','dd/MM/yyyy')
            //    GROUP BY t_vend.SVENDORID,t_plnschd.NPLANID
            //)
            // GROUP BY SVENDORID";
        #endregion


        sConfTruck = @"SELECT 
        COUNT(CASE 
        WHEN ODP.ORDERTYPE = '1' THEN TO_DATE(ODP.DATE_CREATE,'DD/MM/YYYY') 
        WHEN ODP.ORDERTYPE = '2' THEN TO_DATE(ODP.DDELIVERY,'DD/MM/YYYY')
        ELSE SYSDATE END) as NRECORD
         ,SUM(CASE WHEN NVL(PSL.CACTIVE,'XXX') = '0' OR  NVL(PSL.CACTIVE,'XXX') = 'XXX'  THEN 1 ELSE 0 END) CONFIRMED
         ,SUM(CASE WHEN NVL(PSL.CACTIVE,'XXX') = '1' THEN 1 ELSE 0 END) CONFIRMES 
        FROM TBL_ORDERPLAN ODP
        LEFT JOIN TPlanScheduleList PSL ON PSL.SDELIVERYNO = ODP.SDELIVERYNO
        --LEFT JOIN TPLANSCHEDULE t_plnschd ON t_vend.SVENDORID =t_plnschd.SVENDORID
        WHERE 1=1 
        AND ODP.CACTIVE = 'Y' AND NVL(PSL.CACTIVE,'XXX') <> '0' 
        AND ODP.SVENDORID = '{0}'
        AND CASE 
        WHEN ODP.ORDERTYPE = '1' THEN TO_DATE(ODP.DATE_CREATE,'DD/MM/YYYY') 
        WHEN ODP.ORDERTYPE = '2' THEN TO_DATE(ODP.DDELIVERY,'DD/MM/YYYY')
        ELSE SYSDATE END = TO_DATE('{1}','dd/MM/yyyy')";

        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["SVDID"], DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtConfTruck.Rows.Count > 1)
            {
                msg = "ไม่พบรายการ..";
                ltr_confplan.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "false", "true", "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!");
            }
            else if (dtConfTruck.Rows.Count == 1)
            {
                msg = "";
                ltr_confplan.Text = string.Format(sOutHtml, ArrayConfTruckList[0], " " + dtConfTruck.Rows[0]["NRECORD"] + " รายการ"
                     , ArrayConfTruckList[1], " " + dtConfTruck.Rows[0]["CONFIRMES"] + " รายการ"
                     , ArrayConfTruckList[2], " " + dtConfTruck.Rows[0]["CONFIRMED"] + " รายการ"
                     , "true", "false", "");
            }
            else
            {
                msg = "ไม่พบรายการ";
                ltr_confplan.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "none", "true", "ไม่พบรายการ");
            }
        }
    }
    void ListAccident()
    {
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }

        if (Session["ssAlert"] == null) PreAlert();

        string[] ArrayConfTruckList = { "รถห้ามวิ่ง", "รถรอการแก้ไข", "รถตกค้าง", "รถเกิดอุบัติเหตุ" };
        #region msg sOutHtml sConfTruck
        string msg = ""
            , sOutHtml = @"<table border=""0"" width=""100%"">
                                <tr>
                                    <td width=""67%"" title=""รถที่ยังติดสถานะเป็นห้ามวิ่ง(จนถึง ปัจจุบัน)"">{0}</td>
                                    <td align=""center"" width=""33%"">{1}</td>
                                </tr>
                                <tr visible=""{7}"" align=""center"">
                                    <td colspan=""2""><font color=""red"">{8}</font></td> 
                                </tr> 
                                <tr visible=""{6}"">
                                    <td title=""รถที่ผู้ขนส่งยังมีรายการค้างแก้ไข(จนถึง ปัจจุบัน)"" >{2}</td>
                                    <td align=""center"">{3}</td>
                                </tr>
                                <tr>
                                    <td title=""เฉพาะรถที่ถูกระบุว่าตกค้างคลังปลายทาง(จนถึง ปัจจุบัน)"">{4}</td>
                                    <td align=""center"">{5}</td>
                                </tr>
                                <tr>
                                    <td title=""รถที่ระบุว่าเกิดอุบัติเหตุ(วันนี้)เท่านั้น"">{9}</td>
                                    <td align=""center"">{10}</td>
                                </tr>
                            </table>"
            , sConfTruck = @"SELECT t_vend.SVENDORID ,NVL(NRECORD,0) NACCIDENT,NVL(NCARBAN,0) NCARBAN ,NVL(NCHECK,0) NCHECK ,NVL(NHOLD,0) NHOLD ,NVL(NMA,0) NMA
FROM TVENDOR t_vend
LEFT JOIN(
    SELECT SVENDORID ,COUNT(SACCIDENTID) NRECORD    
    FROM TACCIDENT 
    WHERE 1=1 
    AND SVENDORID='{0}'
    AND TO_DATE(DACCIDENT,'dd/mm/yyyy')=TO_DATE('{1}','dd/mm/yyyy')
    GROUP BY SVENDORID
) ACCIDENTS ON t_vend.SVENDORID=ACCIDENTS.SVENDORID
LEFT JOIN(
    SELECT SVENDORID ,COUNT(NCARBANID) NCARBAN     
    FROM TCARBAN
    WHERE 1=1 
     AND SVENDORID='{0}'
     AND TO_DATE(DBAN,'dd/mm/yyyy')=TO_DATE('{1}','dd/mm/yyyy')
    GROUP BY SVENDORID
)  CARBANS ON t_vend.SVENDORID=CARBANS.SVENDORID
LEFT JOIN(
    SELECT t_cont.SVENDORID ,COUNT(i_chktrck.SCHECKLISTID) NCHECK
    ,SUM(CASE WHEN NVL(i_chktrck.CMA,'0')='2' THEN 1 ELSE 0 END ) NHOLD
    ,SUM(CASE WHEN NVL(i_chktrck.CMA,'0')='1' THEN 1 ELSE 0 END ) NMA
    FROM TCHECKTRUCK t_chktrck
    LEFT JOIN TCHECKTRUCKITEM i_chktrck ON T_CHKTRCK.SCHECKID=I_CHKTRCK.SCHECKID
    LEFT JOIN TCONTRACT t_cont ON t_chktrck.SCONTRACTID=t_cont.SCONTRACTID
    WHERE 1=1 AND  NVL(i_chktrck.CEDITED,'0')='0' AND I_CHKTRCK.SCHECKLISTID !=0
    AND SVENDORID='{0}'
    AND TO_DATE(DCHECK,'dd/mm/yyyy')=TO_DATE('{1}','dd/mm/yyyy')
    GROUP BY t_cont.SVENDORID
) CHECKS ON t_vend.SVENDORID=CHECKS.SVENDORID
WHERE t_vend.SVENDORID='{0}'";
        #endregion
        #region SQL QUERY
        string[] SQL = { 
    #region OLDVERSION
//    @" SELECT CONT.SVENDORID ,TRCK.SHEADREGISTERNO SHEADERREGISTERNO,TU_TRCK.SHEADREGISTERNO STRAILERREGISTERNO
//    FROM TCONTRACT CONT 
//    LEFT JOIN TCONTRACT_TRUCK CONT_TRCK  ON CONT.SCONTRACTID=CONT_TRCK.SCONTRACTID
//    LEFT JOIN TTRUCK TRCK ON CONT_TRCK.STRUCKID=TRCK.STRUCKID
//    LEFT JOIN TTRUCK TU_TRCK ON NVL(CONT_TRCK.STRAILERID,'TRxxxxxx') = NVL(TU_TRCK.STRUCKID,'TRxxxxxx')
//    WHERE NVL(NTRUCK,0)>0 AND NVL(CACTIVE,'Y')='Y' --AND CGROUPCONTRACT =('C') AND NVL(CSTANDBY,'N')='N' 
//    AND NVL(TRCK.CACTIVE,'N')='Y' AND NVL(TRCK.CHOLD,0) !='0'  AND NVL(CONT.CACTIVE,'Y') = 'Y' 
//    AND CONT.SVENDORID='{0}' --0010001144     0010001778 GROUP BY CONT.SVENDORID" ,
//@"SELECT distinct t_cont.SVENDORID,T_CHKTRCK.SHEADERREGISTERNO,T_CHKTRCK.STRAILERREGISTERNO 
//    FROM TCHECKTRUCK t_chktrck
//    LEFT JOIN TCHECKTRUCKITEM i_chktrck ON T_CHKTRCK.SCHECKID=I_CHKTRCK.SCHECKID
//    LEFT JOIN TCONTRACT t_cont ON t_chktrck.SCONTRACTID=t_cont.SCONTRACTID
//    WHERE 1=1 AND  NVL(i_chktrck.CEDITED,'0')='0' AND NVL(I_CHKTRCK.SCHECKLISTID,'0') !='0' AND  NVL(i_chktrck.COTHER,'0')!='1' AND NVL(I_CHKTRCK.CCHECKED,'0') !='1' 
//    AND NVL(t_cont.CACTIVE,'Y') = 'Y' AND SVENDORID='{0}' --0010001144     0010001778  
//----AND TO_DATE(DCHECK,'dd/mm/yyyy')=TO_DATE('20/11/2012','dd/mm/yyyy')
//   GROUP BY t_cont.SVENDORID,T_CHKTRCK.SHEADERREGISTERNO,T_CHKTRCK.STRAILERREGISTERNO,T_CONT.SCONTRACTID" ,
	#endregion
@" SELECT TTR.STRUCKID, TTR.SHEADREGISTERNO as SHEADERREGISTERNO, TTR.STRAILERREGISTERNO, TTR.SCHASIS,tc.SCONTRACTNO,v.SVENDORNAME,TCT.SCONTRACTID,cc.CMA,TTR.CHOLD
FROM  (((TTRUCK TTR INNER JOIN TContract_Truck TCT ON TTR.STRUCKID = TCT.STRUCKID) 
INNER JOIN   (
SELECT c.STRUCKID,MAX(ct.CMA) as CMA FROM (TCHECKTRUCK c INNER JOIN    TCHECKTRUCKITEM ct ON c.SCHECKID =  CT.SCHECKID) 
INNER JOIN  TCHECKTRUCKFILE cl ON CT.SCHECKID = CL.SCHECKID   WHERE nvl(CCHECKED,0) != 1 GROUP BY c.STRUCKID
) cc ON TTR.STRUCKID = cc.STRUCKID )
INNER JOIN TContract TC ON TCT.SCONTRACTID = TC.SCONTRACTID) LEFT JOIN TVENDOR_SAP v ON tc.SVENDORID = v.SVENDORID
WHERE NVL(TC.CACTIVE,'Y') = 'Y' AND v.SVENDORID ='{0}'  AND cc.CMA <> 0 AND TTR.CHOLD = '1'" ,
@"SELECT TTR.STRUCKID, TTR.SHEADREGISTERNO as SHEADERREGISTERNO, TTR.STRAILERREGISTERNO, TTR.SCHASIS,tc.SCONTRACTNO,v.SVENDORNAME,TCT.SCONTRACTID,cc.CMA,TTR.CHOLD
FROM  (((TTRUCK TTR INNER JOIN TContract_Truck TCT ON TTR.STRUCKID = TCT.STRUCKID) 
INNER JOIN   (
SELECT c.STRUCKID,MAX(ct.CMA) as CMA FROM (TCHECKTRUCK c INNER JOIN    TCHECKTRUCKITEM ct ON c.SCHECKID =  CT.SCHECKID) 
INNER JOIN  TCHECKTRUCKFILE cl ON CT.SCHECKID = CL.SCHECKID   WHERE nvl(CCHECKED,0) != 1 GROUP BY c.STRUCKID
) cc ON TTR.STRUCKID = cc.STRUCKID )
INNER JOIN TContract TC ON TCT.SCONTRACTID = TC.SCONTRACTID) LEFT JOIN TVENDOR_SAP v ON tc.SVENDORID = v.SVENDORID
WHERE NVL(TC.CACTIVE,'Y') = 'Y' AND v.SVENDORID ='{0}'  AND cc.CMA = '1' AND TTR.CHOLD = '0' " ,
@"SELECT  TCARBAN.SVENDORID ,TCARBAN.SHEADREGISTERNO,TCARBAN.STRAILERREGISTERNO
    FROM TCARBAN
    WHERE 1=1 AND NVL(CUNBAN,'0')='0' AND SVENDORID='{0}'" ,
@"   SELECT SVENDORID ,SHEADREGISTERNO ,STRAILERREGISTERNO  
    FROM TACCIDENT 
    WHERE 1=1 
    AND SVENDORID='{0}'
   AND TO_DATE(DACCIDENT,'dd/mm/yyyy')=TO_DATE('{1}','dd/mm/yyyy')"
                       };
        #endregion
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["SVDID"], DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtConfTruck.Rows.Count > 1)
            {
                msg = "ไม่พบรายการ..";
                ltr_accident.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ[" + dtConfTruck.Rows.Count + "]!", "&nbsp;", "&nbsp;");
            }
            else if (dtConfTruck.Rows.Count == 1)
            {//"รถห้ามวิ่ง", "รถรอการแก้ไข", "รถตกค้าง", "รถเกิดอุบัติเหตุ" 
                msg = "";
                string struckhold = "", strucklist = "", struckban = "", struckaccident = "";
                //"รถห้ามวิ่ง"
                DataTable dtHOLD = CommonFunction.Get_Data(OraConnection, string.Format(SQL[0] + "", "" + Session["SVDID"]));
                if (dtHOLD.Rows.Count > 0)
                {
                    struckhold = "<table>";
                    foreach (DataRow drHOLD in dtHOLD.Rows) struckhold += @"<tr ><td style=""color:#ffffff;"">" + (dtHOLD.Rows.IndexOf(drHOLD) + 1) + ". " + drHOLD["SHEADERREGISTERNO"] + ((drHOLD["STRAILERREGISTERNO"] + "" != "") ? " - " + drHOLD["STRAILERREGISTERNO"] + "</td></tr>" : "");
                    struckhold += "</table>";
                }
                //"รถรอการแก้ไข"
                DataTable dtCHECK = CommonFunction.Get_Data(OraConnection, string.Format(SQL[1] + "", "" + Session["SVDID"]));
                if (dtCHECK.Rows.Count > 0)
                {
                    strucklist = "<table>";
                    foreach (DataRow drCheck in dtCHECK.Rows) strucklist += @"<tr ><td style=""color:#ffffff;"">" + (dtCHECK.Rows.IndexOf(drCheck) + 1) + ". " + drCheck["SHEADERREGISTERNO"] + ((drCheck["STRAILERREGISTERNO"] + "" != "") ? " - " + drCheck["STRAILERREGISTERNO"] + "</td></tr>" : "");
                    strucklist += "</table>";
                }
                //"รถตกค้าง"
                DataTable dtBAN = CommonFunction.Get_Data(OraConnection, string.Format(SQL[2] + "", "" + Session["SVDID"]));
                if (dtBAN.Rows.Count > 0)
                {
                    struckban = "<table>";
                    foreach (DataRow drBAN in dtBAN.Rows) struckban += @"<tr ><td style=""color:#ffffff;"">" + (dtBAN.Rows.IndexOf(drBAN) + 1) + ". " + drBAN["SHEADREGISTERNO"] + ((drBAN["STRAILERREGISTERNO"] + "" != "") ? " - " + drBAN["STRAILERREGISTERNO"] + "</td></tr>" : "");
                    struckban += "</table>";
                }
                //"รถเกิดอุบัติเหตุ" 
                DataTable dtACCIDENT = CommonFunction.Get_Data(OraConnection, string.Format(SQL[3] + "", "" + Session["SVDID"], DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"))));

                if (dtACCIDENT.Rows.Count > 0)
                {
                    struckaccident = "<table>";
                    foreach (DataRow drACCIDENT in dtACCIDENT.Rows) struckaccident += @"<tr ><td style=""color:#ffffff;"">" + (dtACCIDENT.Rows.IndexOf(drACCIDENT) + 1) + ". " + drACCIDENT["SHEADREGISTERNO"] + ((drACCIDENT["STRAILERREGISTERNO"] + "" != "") ? " - " + drACCIDENT["STRAILERREGISTERNO"] + "</td></tr>" : "");
                    struckaccident += "</table>";
                }
                ltr_accident.Text = string.Format(sOutHtml
                    , ArrayConfTruckList[0], "<span " + ((struckhold != "") ? "title='รายชื่อรถห้ามวิ่ง ดังนี้" + struckhold + " ' " : "") + @" >  " + ((dtHOLD.Rows.Count > 0) ? "" + dtHOLD.Rows.Count : "0") + " คัน</span>"
                    , ArrayConfTruckList[1], @"<span " + ((strucklist != "") ? "title='รายชื่อรถที่ต้องแก้ไข ดังนี้" + strucklist + " ' " : "") + @" > " + ((dtCHECK.Rows.Count > 0) ? "" + dtCHECK.Rows.Count : "0") + " คัน</span>"
                     , ArrayConfTruckList[2], @"<span " + ((struckban != "") ? "title='รายชื่อรถที่ต้องตกค้าง ดังนี้" + struckban + " ' " : "") + @" > " + ((dtBAN.Rows.Count > 0) ? "" + dtBAN.Rows.Count : "0") + " คัน</span>"
                     , true, !true, ""
                     , ArrayConfTruckList[3], @"<span " + ((struckaccident != "") ? "title='รายชื่อรถเกิดอุบัติเหตุวันนี้ ดังนี้" + struckaccident + " ' " : "") + @" > " + ((dtACCIDENT.Rows.Count > 0) ? "" + dtACCIDENT.Rows.Count : "0") + " คัน</span>");

                dtHOLD.Dispose(); dtCHECK.Dispose();
            }
            else
            {
                msg = "ไม่พบรายการ";
                ltr_accident.Text = string.Format(sOutHtml, "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", !true, true, "ไม่พบรายการ", "&nbsp;", "&nbsp;");
            }
        }
    }

    void ListApprove()
    {

        lblMonth.Text = "เดือน" + DateTime.Now.ToString("MMMM");
        string Month = CommonFunction.ReplaceInjection((DateTime.Now.Month + "").PadLeft(2, '0'));
        string YEAR = CommonFunction.ReplaceInjection((DateTime.Now.Year + ""));
        string Period = Month + YEAR;
        //string RK_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('09','01','02') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "' ";
        //string RK_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('03') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "'";
        //string MV_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('03','04','07','06') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "'";
        //string MV_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MM') = '" + Month + "'";
        string vendor = Session["SVDID"] + "";
        string RK_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('01','12') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND RK_FLAG <> 'M' AND VENDOR_ID = '" + vendor + "'";
        string RK_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE (STATUS_FLAG in ('03','04','07','06','10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "')  AND RK_FLAG <> 'M' AND VENDOR_ID = '" + vendor + "'";
        string MV_ONPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE (STATUS_FLAG in ('01','03','04','05','08','09','07','06') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 01)";
        string MV_ENDPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 01";
        //string MV_EDITPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('02','12') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 01";
        string MV_CANCELPROCESS = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('11') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 01";
        string MV_ONPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE (STATUS_FLAG in ('01','03','04','05','08','09','07','06') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "') AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 02";
        string MV_ENDPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('10') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 02";
        //string MV_EDITPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('02','12') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 02";
        string MV_CANCELPROCESS2 = @"SELECT  REQUEST_ID,STATUS_FLAG FROM TBL_REQUEST WHERE STATUS_FLAG in ('11') AND TO_CHAR(NVL(UPDATE_DATE,CREATE_DATE),'MMYYYY') = '" + Period + "' AND VENDOR_ID = '" + vendor + "' AND REQTYPE_ID  = 02";
        
        DataTable dtPer = CommonFunction.Get_Data(_conn, "SELECT SMENUID,CPERMISSION,SUID FROM TPERMISSION WHERE SUID = '" + CommonFunction.ReplaceInjection(Session["UserID"] + "") + "'");
        if (dtPer.Rows.Count > 0)
        {
            //if (dtPer.Select("SMENUID = '56' AND CPERMISSION <> 0").Count() > 0)
            //{
            //    DataTable dt = CommonFunction.Get_Data(_conn, RK_ONPROCESS);
            //    lblSumw1.Text = (dt.Rows.Count > 0 ? dt.Rows.Count + "" : "0") + " รายการ";

            //    DataTable dt2 = CommonFunction.Get_Data(_conn, RK_ENDPROCESS);
            //    lblSumw3.Text = (dt2.Rows.Count > 0 ? dt2.Rows.Count + "" : "0") + " รายการ";
            //    trRk.Visible = true;
            //}
            //else
            //{
            //    trRk.Visible = false;
            //}

            if (dtPer.Select("SMENUID = '55' AND CPERMISSION <> 0").Count() > 0)
            {
                DataTable dt3 = CommonFunction.Get_Data(_conn, MV_ONPROCESS);
                lblSumw21.Text = (dt3.Rows.Count > 0 ? dt3.Rows.Count + "" : "0") + " รายการ";

                //DataTable dt4 = CommonFunction.Get_Data(_conn, MV_EDITPROCESS);
                //lblSumw22.Text = (dt4.Rows.Count > 0 ? dt4.Rows.Count + "" : "0") + " รายการ";

                DataTable dt5 = CommonFunction.Get_Data(_conn, MV_ENDPROCESS);
                lblSumw23.Text = (dt5.Rows.Count > 0 ? dt5.Rows.Count + "" : "0") + " รายการ";

                DataTable dt6 = CommonFunction.Get_Data(_conn, MV_CANCELPROCESS);
                lblSumw24.Text = (dt6.Rows.Count > 0 ? dt6.Rows.Count + "" : "0") + " รายการ";
                trMv.Visible = true;

                DataTable dt7 = CommonFunction.Get_Data(_conn, MV_ONPROCESS2);
                lblSumw25.Text = (dt7.Rows.Count > 0 ? dt7.Rows.Count + "" : "0") + " รายการ";

                //DataTable dt8 = CommonFunction.Get_Data(_conn, MV_EDITPROCESS2);
                //lblSumw22.Text = (dt4.Rows.Count > 0 ? dt4.Rows.Count + "" : "0") + " รายการ";

                DataTable dt9 = CommonFunction.Get_Data(_conn, MV_ENDPROCESS2);
                lblSumw27.Text = (dt9.Rows.Count > 0 ? dt9.Rows.Count + "" : "0") + " รายการ";

                DataTable dt10 = CommonFunction.Get_Data(_conn, MV_CANCELPROCESS2);
                lblSumw28.Text = (dt10.Rows.Count > 0 ? dt10.Rows.Count + "" : "0") + " รายการ";
                trMv2.Visible = true;
            }
            else
            {
                trMv.Visible = false;
                trMv2.Visible = false;
            }
        }

    }
    void ListPersonalInfo()
    {/*
        string msg = ""
            , sConfTruck = @"SELECT TUSER.SUID , TUSER.SVENDORID ,TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SFULLNAME
,CASE TUSER.CGROUP 
WHEN '0' THEN TVENDOR.SABBREVIATION 
WHEN '1' THEN TUNIT.UNITNAME 
WHEN '2' THEN TTERMINAL.SABBREVIATION
END  SUNITNAME
FROM TUSER 
LEFT JOIN TVENDOR ON TUSER.SVENDORID=TVENDOR.SVENDORID
LEFT JOIN TTERMINAL ON TUSER.SVENDORID=TTERMINAL.STERMINALID
LEFT JOIN TUNIT ON TUSER.SVENDORID=TUNIT.UNITCODE
WHERE SUID='{0}'";
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["UserID"]));
            if (dtConfTruck.Rows.Count > 0)
            {
                lbFulllName.Text = "คุณ " + dtConfTruck.Rows[0]["SFULLNAME"];
                lblVendorName.Text = "" + dtConfTruck.Rows[0]["SUNITNAME"];
            }

        }*/
    }
    void ListPlan2Day()
    {//mytable  tablesorter
        if (Session["ssAlert"] == null) Session["ssAlert"] = PreAlert();
        string msg = ""
            , sOutHtml = @"<table id=""{1}"" width=""100%"" class='{2}' > <thead>
                                <tr>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""16%"">Outbound No </th>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""15%"">ทะเบียนรถ</th>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""43%"">จุดส่งรถ</th>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""10%"">เที่ยวที่</th>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""12%"">สถานะ</th>
                                </tr></thead>
                                 {0}
                            </table>"
            , sDetailHtml = @"<tr><td >{0}</td>
                               <td align=""center"" >{1}</td>
                               <td align=""left"" >{2}</td>
                               <td align=""center"">{3}</td>
                               <td align=""center"">{4}</td>
                           </tr>"
            ,
        #region OLDVERSION
            //             sPlan2Day = @"SELECT t_pln.SHEADREGISTERNO ,t_pln.STRAILERREGISTERNO
            // ,i_plnlst.SDELIVERYNO,t_pln.STIMEWINDOW,I_PLNLST.SSHIPTO
            // ,T_CUS.CUST_NAME
            //,COUNT(i_plnlst.SDELIVERYNO) NDROP
            //,CASE WHEN NVL(i_plnlst.CACTIVE,'0')='0' THEN 'D' ELSE
            //    CASE WHEN NVL(T_PLN.CCONFIRM,'x')='x' THEN 'W' ELSE 
            //        CASE WHEN NVL(T_PLN.CCONFIRM,'0')='0' THEN 'U' ELSE 'C'  END 
            //    END
            //END CSTATUS
            //FROM TPLANSCHEDULE t_pln
            //LEFT JOIN TPLANSCHEDULELIST i_plnlst ON t_pln.NPLANID= i_plnlst.NPLANID
            //LEFT JOIN TCUSTOMER_SAP t_cus ON i_plnlst.SSHIPTO= t_cus.SHIP_TO
            //WHERE SVENDORID='{0}' AND NVL(i_plnlst.CACTIVE,'0')='1'
            //AND TO_DATE(SPLANDATE,'DD/MM/YYYY') = TO_DATE('{1}','dd/MM/yyyy')
            //GROUP BY i_plnlst.SDELIVERYNO,t_pln.SHEADREGISTERNO ,t_pln.STRAILERREGISTERNO,t_pln.STIMEWINDOW,I_PLNLST.SSHIPTO ,T_CUS.CUST_NAME
            //,CASE WHEN NVL(i_plnlst.CACTIVE,'0')='0' THEN 'D' ELSE
            //    CASE WHEN NVL(T_PLN.CCONFIRM,'x')='x' THEN 'W' ELSE 
            //        CASE WHEN NVL(T_PLN.CCONFIRM,'0')='0' THEN 'U' ELSE 'C' END 
            //    END
            //END";
        #endregion
 sPlan2Day = @"SELECT ODP.SDELIVERYNO ,PS.SHEADREGISTERNO   ,PS.STRAILERREGISTERNO,SHM.SHIP_TO  ,SHM.CUST_ABBR  as CUST_NAME,ODP.NWINDOWTIMEID as STIMEWINDOW
,CASE WHEN NVL(PSL.CACTIVE,'XXX') = '1' THEN 'C' WHEN NVL(PSL.CACTIVE,'XXX') = 'XXX' THEN 'W' WHEN NVL(PSL.CACTIVE,'XXX') = '0' THEN 'D' ELSE '' END as CSTATUS
FROM TBL_ORDERPLAN ODP
LEFT JOIN 
        (
            SELECT d.DELIVERY_NO,d.SPLNT_CODE,d.OTHER,C.CUST_ABBR,
            (CASE 
            WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') 
            WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)  
            ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO
            FROM 
            (
                SELECT LM.* FROM TDELIVERY LM
                INNER JOIN 
                (
                    SELECT DELIVERY_NO,MAX(DATE_CREATED) as DATE_CREATED,MAX(DELIVERY_DATE) as DELIVERY_DATE FROM TDELIVERY 
                    GROUP BY DELIVERY_NO
                )LL
                ON LM.DELIVERY_NO = LL.DELIVERY_NO AND LM.DATE_CREATED = LL.DATE_CREATED  AND LM.DELIVERY_DATE = LL.DELIVERY_DATE
                WHERE  
                LM.DELIVERY_NO IN (SELECT SDELIVERYNO FROM TBL_ORDERPLAN WHERE 1=1  AND CACTIVE = 'Y'  AND SVENDORID = '{0}' AND CASE WHEN ORDERTYPE = '1' THEN TO_DATE(DATE_CREATE,'DD/MM/YYYY') WHEN ORDERTYPE = '2' THEN TO_DATE(DDELIVERY,'DD/MM/YYYY') ELSE SYSDATE END = TO_DATE('{1}','dd/MM/yyyy'))
            )d  
            LEFT JOIN 
            (
                --SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
                SELECT  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE FROM TRANS_ORDER   LM
                INNER JOIN 
                (
                    SELECT  DOC_NO,MAX(DBSYS_DATE) as DBSYS_DATE  FROM TRANS_ORDER  
                    WHERE NVL(VALTYP,'-') !='REBRAND' AND NVL(STATUS,'N') IN ('N','G')
                    GROUP BY DOC_NO
                )LL
                ON LM.DOC_NO = LL.DOC_NO AND LM.DBSYS_DATE = LL.DBSYS_DATE
                WHERE NVL(LM.VALTYP,'-') !='REBRAND' AND NVL(LM.STATUS,'N') IN ('N','G')
                GROUP BY  LM.DOC_NO,LM.SHIP_ID,LM.REV_CODE
            )TRN
            ON TRN.DOC_NO = d.SALES_ORDER
            LEFT JOIN 
            (
                --SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
                    SELECT  LM.DOCNO,LM.RECIEVEPOINT FROM TBORDER LM
                    INNER JOIN
                    (
                        SELECT  DOCNO,MAX(UPDATEDATE) as UPDATEDATE FROM TBORDER 
                        WHERE RECIEVEPOINT IS NOT NULL
                        GROUP BY DOCNO
                    )LL
                    ON LM.DOCNO = LL.DOCNO AND LM.UPDATEDATE = LL.UPDATEDATE
                    GROUP BY LM.DOCNO,LM.RECIEVEPOINT
            )TBO
            ON TBO.DOCNO = d.SALES_ORDER
            LEFT JOIN TCUSTOMER C ON  C.SHIP_TO =  (CASE  WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0')  WHEN TRN.REV_CODE IS NOT NULL OR TBO.RECIEVEPOINT IS NOT NULL  THEN NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) ELSE ''  END)   ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) 
        )SHM
        ON SHM.DELIVERY_NO = ODP.SDELIVERYNO
LEFT JOIN TPlanScheduleList PSL ON PSL.SDELIVERYNO = ODP.SDELIVERYNO
LEFT JOIN TPLANSCHEDULE  PS ON PS.NPLANID =PSL.NPLANID
WHERE 1=1 
AND ODP.CACTIVE = 'Y' AND NVL(PSL.CACTIVE,'XXX') <> '0' 
AND ODP.SVENDORID = '{0}'
AND CASE 
WHEN ODP.ORDERTYPE = '1' THEN TO_DATE(ODP.DATE_CREATE,'DD/MM/YYYY') 
WHEN ODP.ORDERTYPE = '2' THEN TO_DATE(ODP.DDELIVERY,'DD/MM/YYYY')
ELSE SYSDATE END = TO_DATE('{1}','dd/MM/yyyy')";
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dtPlan2Day = CommonFunction.Get_Data(OraConnection, string.Format(sPlan2Day, "" + Session["SVDID"], "" + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
            if (dtPlan2Day.Rows.Count > 0)
            {
                int nCancel = 0;
                DataTable dtAlert = (DataTable)Session["ssAlert"];
                foreach (DataRow drDel in dtAlert.Select("type='plancancel'")) dtAlert.Rows[dtAlert.Rows.IndexOf(drDel)].Delete();

                foreach (DataRow drPlan2Day in dtPlan2Day.Rows)
                {
                    msg += string.Format(sDetailHtml, "" + drPlan2Day["SDELIVERYNO"]
                        , "" + drPlan2Day["SHEADREGISTERNO"] + ("" + drPlan2Day["STRAILERREGISTERNO"] == "" ? "" : " " + drPlan2Day["STRAILERREGISTERNO"]), "" + drPlan2Day["CUST_NAME"]
                        , "" + drPlan2Day["STIMEWINDOW"], ("" + drPlan2Day["CSTATUS"] == "D" ? "ยกเลิก" : ("" + drPlan2Day["CSTATUS"] == "W" ? "รอยืนยัน" : ("" + drPlan2Day["CSTATUS"] == "C" ? "ยืนยัน" : "ไม่ยืนยัน"))));
                    if ("" + drPlan2Day["CSTATUS"] == "U")
                    {
                        nCancel += 1;
                    }
                }

                if (nCancel > 0)
                {
                    DataRow drAlert = dtAlert.NewRow();
                    drAlert["rows"] = "" + (dtAlert.Rows.Count + 1);
                    drAlert["type"] = "plancancel";
                    drAlert["detail"] = "ยกเลิกแผนการขนส่ง";
                    drAlert["amount"] = "" + nCancel;
                    dtAlert.Rows.Add(drAlert);
                    Session["ssAlert"] = dtAlert;
                }
                ltr_2dayplan.Text = string.Format(sOutHtml, msg, "mytable", "tablesorter");
            }
            else
            {
                msg = @"<tr><td colspan=""5"" align=""center""><font color=""red"">ไม่มีข้อมูลแผนงานวันนี้</font></td></tr>";
                ltr_2dayplan.Text = string.Format(sOutHtml, msg, "xmytable", "tablesorter");
            }
        }
    }
    void ListAlert()
    {
        if (Session["ssAlert"] == null) Session["ssAlert"] = PreAlert();
        string msg = ""
             , sOutHtml = @"<table id=""{1}"" width=""100%"" class='{2}' > <thead>
                                <tr>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""10%"">No.</th>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""75%"">รายการแจ้งเตือน</th>
                                    <th class = 'sethead' align='center' bgcolor=""#FFDC51"" width=""15%"">จำนวน</th> 
                                </tr></thead>
                                 {0}
                            </table>"
             , sDetailHtml = @"<tr><td >{0}</td>
                               <td align=""left"" >{1}</td>
                               <td align=""center"" >{2}</td> 
                           </tr>"
             ;
        DataTable dtAlert = (DataTable)Session["ssAlert"];
        if (dtAlert.Rows.Count > 0)
        {
            foreach (DataRow drdtAlert in dtAlert.Rows)
            {
                msg += string.Format(sDetailHtml, "" + drdtAlert["rows"], "" + drdtAlert["detail"], "" + drdtAlert["amount"]);
            }
            ltrAlert.Text = string.Format(sOutHtml, msg, "mytablealert", "tablesorter");
        }
        else
        {
            msg = @"<tr><td colspan=""5"" align=""center""><font color=""red"">ไม่มีข้อมูลแจ้งเตือนวันนี้</font></td></tr>";
            ltrAlert.Text = string.Format(sOutHtml, msg, "xmytablealert", "tablesorter");
        }
    }
    protected DataTable PreAlert()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("rows", typeof(string));
        dt.Columns.Add("type", typeof(string));
        dt.Columns.Add("detail", typeof(string));
        dt.Columns.Add("amount", typeof(string));
        return dt;
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        switch (e.Parameter)
        {
            case "vendor_conftruck":
                ListConfTruck();
                break;
            case "vendor_confplan":
                ListConfPlan();
                break;
            case "vendor_accident":
                ListAccident();
                break;
            case "vendor_plan2day":
                //ListPlan2Day();
                break;

            case "popup":
                //เมื่อกดปุ่มตกลง ในpopup ลืม password
                string msg = "";
                if (txtForgetUsername.Text.Trim() != "" && txtoldpassword.Text.Trim() != "" && txtnewpassword.Text.Trim() != "")
                {
                    using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                    {
                        var EncodePass = STCrypt.encryptMD5(txtoldpassword.Text.Trim());
                        var NewEncodePass = STCrypt.encryptMD5(txtnewpassword.Text.Trim());
                        var NewEncode = STCrypt.encryptMD5(txtnewpassword.Text.Trim());
                        DataTable dt = CommonFunction.Get_Data(OraConnection, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME ,SPASSWORD FROM TUSER WHERE SUID = '" + Session["UserId"] + "' AND CACTIVE = '1'");
                        if (dt.Rows.Count > 0)
                        {
                            msg = "";
                            if (EncodePass != dt.Rows[0]["SPASSWORD"] + "")
                            {
                                msg += "<br>- รหัสผ่านเดิมไม่ถูกต้อง!";
                            }
                            if (NewEncodePass != NewEncode)
                            {
                                msg += "<br>- รหัสผ่านใหม่ไม่ตรงกัน!";
                            }
                            if (msg == "")
                            {
                                if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();

                                string sql = "UPDATE TUSER SET SPASSWORD = '" + NewEncode + "',SOLDPASSWORD = '" + dt.Rows[0]["SPASSWORD"] + "" + "' WHERE SUID='" + dt.Rows[0]["SUID"] + "" + "' AND SUSERNAME = '" + txtForgetUsername.Text.Trim() + "'";
                                using (OracleCommand com = new OracleCommand(sql, OraConnection))
                                {
                                    try
                                    {
                                        com.ExecuteNonQuery();
                                        msg = "ระบบได้ทำการเปลี่ยนรหัสผ่านให้ท่านแล้ว<br>ท่านสามารถเข้าใช้งานด้วยรหัสผ่านใหม่ได้ทันทีในการครั้งต่อไป";
                                    }
                                    catch
                                    {
                                        msg = "ระบบไม่สามารถดำเนินการเปลี่ยนรหัสผ่านให้ท่านได้<br>กรุณาลองใหม่อีกครั้ง";
                                    }
                                    finally
                                    {
                                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('ผลการดำเนินการ','" + msg + "',function(){pcForget.Hide();});");
                                    }
                                }

                            }
                            else
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบข้อมูลดังนี้ " + msg + "',function(){pcForget.Hide();});");
                            }
                        }
                        else
                        {
                            msg = "ไม่พบผู้ใช้งานดังกล่าว";
                            CommonFunction.SetPopupOnLoad(xcpn, "pcForget.Hide(); dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + msg + "',function(){pcForget.Hide();});");
                        }

                    }
                }
                break;
        }
    }
    protected void xcpn_conftruck_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        switch (e.Parameter)
        {
            case "vendor_conftruck":
                ListConfTruck();
                break;
            case "vendor_confplan":
                ListConfPlan();
                break;
            case "vendor_accident":
                ListAccident();
                break;
            case "vendor_plan2day":
                //ListPlan2Day();
                break;
            case "vendor_approve":
                ListApprove();
                break;
        }
    }
}//End Class vendor_home