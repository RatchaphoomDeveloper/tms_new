﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Text.RegularExpressions;
using TMS_BLL.Master;
using TMS_BLL.Authentication;
using System.Text;
using System.Web.Security;
public partial class MP : System.Web.UI.MasterPage
{
    string sqlCon = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl liMaster;
    protected global::System.Web.UI.WebControls.Literal ltMaster;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl liRequest;
    protected global::System.Web.UI.WebControls.Literal ltRequest;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl liReport;
    protected global::System.Web.UI.WebControls.Literal ltReport;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl liAdmin;
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl liSystem;
    protected override void OnLoad(EventArgs e)
    {
        ScriptManager.GetCurrent(this.Page).RegisterAsyncPostBackControl(btnMessageConfirm);
        ScriptManager.GetCurrent(this.Page).RegisterAsyncPostBackControl(btnMessageCancel);
        btnMessageConfirm.Click += ComfirmClick;
        btnMessageCancel.Click += CancelClick;
        if (!this.Page.IsPostBack)
        {
            hdnMessage.Set("MethodConfirmBox", null);
            hdnMessage.Set("MethodCancelBox", null);
            hdnMessage.Set("MessageParameters", null);
        }
        base.OnLoad(e);
    }

    protected void ComfirmClick(object sender, EventArgs e)
    {
        try
        {
            String methods = (String)hdnMessage.Get("MethodConfirmBox");
            if (!String.IsNullOrEmpty(methods))
            {
                Object[] messages = (Object[])hdnMessage.Get("MessageParameters");
                OnCallMethods(methodName: methods, parameters: messages);
            }

        }
        catch (System.Exception ex)
        {
            throw ex;
        }
        finally
        {
            //popupMessage.ShowOnPageLoad = false;
            uplMessage.Update();
            // this.ScriptManager.
        }

    }

    private object OnCallMethods(string methodName, params object[] parameters)
    {
        Type contentType = this.Page.GetType();
        System.Reflection.MethodInfo mi = contentType.GetMethod(methodName);
        if (mi == null) return null;
        return mi.Invoke(this.Page, parameters);
    }

    protected void CancelClick(object sender, EventArgs e)
    {
        try
        {
            String methods = (String)hdnMessage.Get("MethodCancelBox");
            if (!String.IsNullOrEmpty(methods))
            {
                Object[] messages = (Object[])hdnMessage.Get("MessageParameters");
                OnCallMethods(methodName: methods, parameters: messages);
            }
        }
        catch (System.Exception ex)
        {
            throw ex;
        }
        finally
        {
            //popupMessage.ShowOnPageLoad = false;
            uplMessage.Update();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetNoStore();
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            Session["Redirect"] = Request.Url;
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        if (!IsPostBack)
        {
            if (Request.Browser.Browser == "IE")
            {
                if (Request.Browser.MajorVersion < 7)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('เพื่อความสะดวกในการใช้งาน กรุณาใช้InternetExplorer รุ่น 7 หรือใหม่กว่า!')", true);
                }
            }

            ahrefhome.HRef = (Session["CGROUP"] + "" == "1" ? "admin_" : (Session["CGROUP"] + "" == "0" ? "vendor_" : "Page")) + "home.aspx";
            #region Set Account Name
            Session["ss_AccountName"] = "";
            string sConfTruck = @"SELECT DISTINCT TUSER.SUID , TUSER.SVENDORID ,TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SFULLNAME
                                ,CASE TUSER.CGROUP 
                                WHEN '0' THEN TVENDOR.SABBREVIATION 
                                WHEN '1' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '2' THEN TTERMINAL.SABBREVIATION
                                WHEN '3' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '4' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '5' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '6' THEN M_SOLD_TO.SOLD_NAME
                                END  SUNITNAME
                                FROM TUSER 
                                LEFT JOIN TVENDOR ON TUSER.SVENDORID=TVENDOR.SVENDORID
                                LEFT JOIN TTERMINAL ON TUSER.SVENDORID=TTERMINAL.STERMINALID
                                LEFT JOIN TUNIT ON TUSER.SVENDORID=TUNIT.UNITCODE
                                LEFT JOIN M_DEPARTMENT ON TUSER.DEPARTMENT_ID = M_DEPARTMENT.DEPARTMENT_ID 
	                            LEFT JOIN M_DIVISION ON TUSER.DIVISION_ID = M_DIVISION.DIVISION_ID 
								LEFT JOIN M_SOLD_TO ON TUSER.SVENDORID = M_SOLD_TO.SOLD_ID
                                WHERE SUID='{0}'";
            using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
            {
                DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + Session["UserID"]));
                if (dtConfTruck.Rows.Count > 0)
                {
                    Session["vendoraccountname"] = dtConfTruck.Rows[0]["SUNITNAME"];
                    Session["ss_AccountName"] += "ผู้ใช้ : คุณ " + dtConfTruck.Rows[0]["SFULLNAME"] + "<br>หน่วยงาน :" + dtConfTruck.Rows[0]["SUNITNAME"];
                    //lbFulllName.Text = "คุณ " + dtConfTruck.Rows[0]["SFULLNAME"];
                    //lblVendorName.Text = "หน่วยงาน " + dtConfTruck.Rows[0]["SUNITNAME"].ToString().Substring(0,15);
                    //lblVendorName.Attributes.Add("title", "" + dtConfTruck.Rows[0]["SUNITNAME"]);
                    ahrefperson.Attributes.Add("title", @"<table >
                                <tr ><td style=""color:#ffffff"">" + "คุณ " + dtConfTruck.Rows[0]["SFULLNAME"] + @"</td></tr>
                               <tr><td  style=""color:#ffffff"">" + "หน่วยงาน " + dtConfTruck.Rows[0]["SUNITNAME"] + @"</td></tr></table>");

                    ltrShow.Text = @"<table ><tr ><td >" + "คุณ " + dtConfTruck.Rows[0]["SFULLNAME"] + @"</td></tr>
                               <tr><td >" + "หน่วยงาน " + dtConfTruck.Rows[0]["SUNITNAME"] + @"</td></tr></table>";

                }
            }
            #endregion

            #region Authenticate
            if (Session["UserLogin"] != null)
            {
               
                DataTable dtUserLogin = (DataTable)Session["UserLogin"];
                DataTable dtAuthen = AuthenBLL.Instance.AuthenSelectBLL(dtUserLogin.Rows[0]["USERGROUP_ID"].ToString());
                if (dtAuthen.Rows.Count == 0)
                {
                    //Response.Redirect("../../Pages/Other/NotAuthorize.aspx");
                }
                DataRow[] dr = dtAuthen.Select("VISIBLE = 1");
                if (dr.Length == 0)
                {
                   // Response.Redirect("../../Pages/Other/NotAuthorize.aspx");
                }
                
                Session["dtAuthen"] = dtAuthen;
            }
            #endregion

            this.ltrSubmenu.Text = SetMenu();
            if (Session["Redirect"] != null)
            {
                string URL = Session["Redirect"].ToString();
                Session.Remove("Redirect");
                Response.Redirect(URL);
            }  
        }

        if ("" + Session["CHKCHANGEPASSWORD"] == "1")
        {
            lnkForget.ClientSideEvents.Init = "function(s, e){pcForget.Show();txtForgetUsername.SetValue(getSession());}";
        }
    }


    protected string SetMenu()
    {
       int id = int.Parse(Session["UserID"].ToString());
       //DataTable dt = MenuBLL.Instance.MenuSelect(Session["CGROUP"] + string.Empty == "0" ? "0" : "1"); //ของเก่า
       string getcGroup = Session["CGROUP"].ToString();
       string getcUserGroupID = Session["UserGroupID"].ToString();
       string groupMenuAccess = "";

       if (getcGroup == "0")
       {
           groupMenuAccess = "0"; //ผู้ประกอบการขนส่ง
       }
       else if (getcGroup == "1")
       {
           groupMenuAccess = "1"; //เจ้าหน้าที่ ปตท.
       }
       else if (getcGroup == "2")
       {
           groupMenuAccess = "2"; //พนักงานคลัง
       }
       else
       {
           groupMenuAccess = "1"; //ผู้ดูแลระบบ, ผจ.รข.ขปน., BSA >> เห็นเท่า พนักงาน ปตท
       }

       Session["groupMenuAccess"] = groupMenuAccess;

       DataTable dt = MenuBLL.Instance.MenuSelect2(getcUserGroupID); //Authen ของใหม่
       if (groupMenuAccess == "1" || groupMenuAccess == "2")
        {
            string code = @"
            <ul id='navigation' class='nav-main'>
                <li class='menu_int'><a href='" + (Session["CGROUP"] + "" == "1" ? "admin_" : "Page") + @"home.aspx' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s1','','images/bt_tms_d01x.jpg',1)"">
                    <img src='images/bt_tms_d01.jpg'  border='0' id='s1' /></a>
                <ul class='nav-sub'>
                ";

            foreach (var item in dt.Select("SMENUORDER < 100"))
            {
                code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
            }
             code += @"
            </ul>
                </li>
                
                <li class='menu_int list'><a href='#' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s2','','images/bt_tms_d02x.jpg',1)"">
                    <img src='images/bt_tms_d02.jpg' border='0' id='s2' /></a>
                    <ul class='nav-sub'>
                         ";

             foreach (var item in dt.Select("SMENUORDER >= 100 AND SMENUORDER <= 200"))
             {
                 code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
             }
             code += @"
                    </ul>
                </li>


                <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()""
                    onmouseover=""MM_swapImage('s3','','images/bt_tms_d03x.jpg',1)"">
                    <img src='images/bt_tms_d03.jpg'  border='0' id='s3' /></a>
                    <ul class='nav-sub'>
                        ";

             foreach (var item in dt.Select("SMENUORDER >= 200 AND SMENUORDER <= 300"))
             {                
                 code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
             }
             code += @"  
                    </ul>
                </li>
                 <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()""
                    onmouseover=""MM_swapImage('s4','','images/bt_tms_d04x.jpg',1)"">
                    <img src='images/bt_tms_d04.jpg'  border='0' id='s4' /></a>
                    <ul class='nav-sub'>
                        ";

             foreach (var item in dt.Select("SMENUORDER >= 300 AND SMENUORDER <= 400"))
             {
                 code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
             }
             code += @"
                    </ul>
                </li>

                 <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s5','','images/bt_tms_d05x.jpg',1)"">
                        <img src='images/bt_tms_d05.jpg' border='0' id='s5' /></a>
                    <ul class='nav-sub'>
                        ";

             foreach (var item in dt.Select("SMENUORDER >= 400 AND SMENUORDER <= 500"))
             {
                 code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
             }
             code += @"
                    </ul>

                </li>
                        
                 <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()""
                    onmouseover=""MM_swapImage('s6','','images/bt_tms_d06x.jpg',1)"">
                    <img src='images/bt_tms_d06.jpg'  border='0' id='s6' /></a>
                    <ul class='nav-sub'>
                        ";

             foreach (var item in dt.Select("SMENUORDER >= 500 AND SMENUORDER <= 600"))
             {
                 code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
             }
             code += @"
                    </ul>
                </li>
                 <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()""
                    onmouseover=""MM_swapImage('s7','','images/bt_tms_d07x.jpg',1)"">
                    <img src='images/bt_tms_d07.jpg'  border='0' id='s7' /></a>
                    <ul class='nav-sub'>
                        ";

             foreach (var item in dt.Select("SMENUORDER >= 600 AND SMENUORDER <= 700"))
             {
                 code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
             }
             code += @"
                    </ul>
                </li>
                 <li class='menu_int'><a href='Report.aspx' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s8','','images/bt_tms_d08x.jpg',1)"">
                    <img src='images/bt_tms_d08.jpg'  border='0' id='s8' /></a>
               <ul class='nav-sub'>
                                        ";

             foreach (var item in dt.Select("SMENUORDER >= 700 AND SMENUORDER <= 800"))
             {
                 code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
             }
             code += @"
                                    </ul>
                </li>
                            </ul>";
       
            return code;
        }
       else if (groupMenuAccess == "0")
        {
            string code = @"
            <ul id='navigation' class='nav-main'>
                <li class='menu_int'><a href='vendor_home.aspx' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s1','','images/bt_tms_c01x.jpg',1)"">
                    <img src='images/bt_tms_c01.jpg'  border='0' id='s1' /></a>
                     <ul class='nav-sub'>
                        ";

            foreach (var item in dt.Select("SMENUORDER < 100"))
            {
                code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
            }
            code += @"
                    </ul>
                </li>
                <li class='menu_int list'><a href='#' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s2','','images/bt_tms_c02x.jpg',1)"">
                    <img src='images/bt_tms_c02.jpg'  border='0' id='s2' /></a>
                    <ul class='nav-sub'>
                        ";

            foreach (var item in dt.Select("SMENUORDER >= 100 AND SMENUORDER <= 200"))
            {
                code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
            }
            code += @"
                    </ul>
                    </li>
                <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s3','','images/bt_tms_c03x.jpg',1)"">
                    <img src='images/bt_tms_c03.jpg'  border='0' id='s3' /></a>
                     <ul class='nav-sub'>
                      ";

            foreach (var item in dt.Select("SMENUORDER >= 200 AND SMENUORDER <= 300"))
            {
                string URLName = item["MENU_NAME"].ToString();
                string MenuURL = item["MENU_URL"].ToString();
                if (MenuURL.ToLower() == "reportmonthly.aspx")
                {
                    URLName = "ส่งมอบรายงานการขนส่ง";
                }
                code += @"<li><a href='" + item["MENU_URL"] + "'>" + URLName + "</a></li>";
            }
            code += @"
                    </ul>
                    </li>
                 <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()""
                    onmouseover=""MM_swapImage('s4','','images/bt_tms_c04x.jpg',1)"">
                    <img src='images/bt_tms_c04.jpg'  border='0' id='s4' /></a>
                    <ul class='nav-sub'>
                       ";

            foreach (var item in dt.Select("SMENUORDER >= 300 AND SMENUORDER <= 400"))
            {
                code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
            }
            code += @"
                        
                    </ul>
                </li>
                 <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()""
                    onmouseover=""MM_swapImage('s5','','images/bt_tms_c05x.jpg',1)"">
                    <img src='images/bt_tms_c05.jpg'  border='0' id='s5' /></a>
                    <ul class='nav-sub'>
                        ";

            foreach (var item in dt.Select("SMENUORDER >= 400 AND SMENUORDER <= 500"))
            {
                code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
            }
            code += @"
                    </ul>
                </li>
                <li class='menu_int'><a href='#' onmouseout=""MM_swapImgRestore()"" onmouseover=""MM_swapImage('s8','','images/bt_tms_c06x.jpg',1)"">
                    <img src='images/bt_tms_c06.jpg'  border='0' id='s8' /></a>
                        <ul class='nav-sub'>
                        ";

            foreach (var item in dt.Select("SMENUORDER >= 500 AND SMENUORDER <= 600"))
            {
                code += @"<li><a href='" + item["MENU_URL"] + "'>" + item["MENU_NAME"] + "</a></li>";
            }
            code += @"
                    </ul>
                </li>
            </ul>";

            //string[] sLink = new string[19];
            string cPermission = "";
            ////int id = int.Parse(Session["UserID"].ToString());

            //DataTable dt = new DataTable();
            dt = CommonFunction.Get_Data(sqlCon, "SELECT SMENUID,CPERMISSION FROM TPERMISSION WHERE SUID = '" + id + "' ORDER BY SMENUID");


            for (int i = 0; i < dt.Rows.Count; i++)
            {            
                cPermission += dt.Rows[i][0] + ";" + dt.Rows[i][1] + "" + "|";
            }
            if (cPermission != "")
            {
                Session["cPermission"] = cPermission.Remove(cPermission.Length - 1);                
            }

            //////เมนูงานสอบเทียบของ USER
            ////string SNAME = "";
            ////if (dt.Rows.Count > 0)
            ////{

            ////    if (dt.Select("SMENUID = '55' AND CPERMISSION <> 0").Count() > 0)
            ////    {
            ////        SNAME = "<li><a href='vendor_HomeAlert.aspx'>งานสอบเทียบ</a></li>";
            ////    }

            ////}


            return code;
        }
        return "";
    }

    protected void xcpnM_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        switch (e.Parameter)
        {
            case "popup":
                //เมื่อกดปุ่มตกลง ในpopup ลืม password
                string msg = "";

                if (txtForgetUsername.Text.Trim() != "" && txtoldpassword.Text.Trim() != "" && txtnewpassword.Text.Trim() != "")
                {
                    using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                    {
                        var EncodePass = STCrypt.encryptMD5(CommonFunction.ReplaceInjection(txtoldpassword.Text.Trim()));
                        var NewEncodePass = STCrypt.encryptMD5(CommonFunction.ReplaceInjection(txtnewpassword.Text.Trim()));
                        var NewEncode = STCrypt.encryptMD5(CommonFunction.ReplaceInjection(txtnewpassword.Text.Trim()));
                        DataTable dt = CommonFunction.Get_Data(OraConnection, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME ,SPASSWORD FROM TUSER WHERE SUID = '" + Session["UserId"] + "' AND CACTIVE = '1'");
                        if (dt.Rows.Count > 0)
                        {
                            msg = "";
                            if (EncodePass != dt.Rows[0]["SPASSWORD"] + "")
                            {
                                msg += "<br>- รหัสผ่านเดิมไม่ถูกต้อง!";
                            }
                            if (NewEncodePass != NewEncode)
                            {
                                msg += "<br>- รหัสผ่านใหม่ไม่ตรงกัน!";
                            }
                            if (msg == "")
                            {
                                if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();

                                string sql = "UPDATE TUSER SET DCHANGEPASSWORD = SYSDATE, SPASSWORD = '" + NewEncode + "',SOLDPASSWORD = '" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPASSWORD"] + "") + "' WHERE SUID='" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUID"] + "") + "' AND SUSERNAME = '" + CommonFunction.ReplaceInjection(txtForgetUsername.Text.Trim()) + "'";
                                using (OracleCommand com = new OracleCommand(sql, OraConnection))
                                {
                                    try
                                    {
                                        com.ExecuteNonQuery();
                                        msg = "ระบบได้ทำการเปลี่ยนรหัสผ่านให้ท่านแล้ว<br>ท่านสามารถเข้าใช้งานด้วยรหัสผ่านใหม่ได้ทันทีในการครั้งต่อไป";
                                        Session["CHKCHANGEPASSWORD"] = "0";
                                    }
                                    catch
                                    {
                                        msg = "ระบบไม่สามารถดำเนินการเปลี่ยนรหัสผ่านให้ท่านได้<br>กรุณาลองใหม่อีกครั้ง";
                                    }
                                    finally
                                    {
                                        CommonFunction.SetPopupOnLoad(xcpnM, "dxInfoRedirect('ผลการดำเนินการ','" + msg + "',function(){pcForget.Hide();dxPopupInfo.Hide();});");
                                    }
                                }

                            }
                            else
                            {
                                CommonFunction.SetPopupOnLoad(xcpnM, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบข้อมูลดังนี้ " + msg + "',function(){pcForget.Hide();});");
                            }
                        }
                        else
                        {
                            msg = "ไม่พบผู้ใช้งานดังกล่าว";
                            CommonFunction.SetPopupOnLoad(xcpnM, "pcForget.Hide(); dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + msg + "',function(){pcForget.Hide();});");
                        }

                    }
                }
                break;
        }
    }
   
}

//public class AuthenStandard
//{

//     protected global::System.Web.UI.HtmlControls.HtmlGenericControl liMaster;
//     protected global::System.Web.UI.WebControls.Literal ltMaster;
//     protected global::System.Web.UI.HtmlControls.HtmlGenericControl liRequest;
//     protected global::System.Web.UI.WebControls.Literal ltRequest;
//     protected global::System.Web.UI.HtmlControls.HtmlGenericControl liReport;
//     protected global::System.Web.UI.WebControls.Literal ltReport;
//     protected global::System.Web.UI.HtmlControls.HtmlGenericControl liAdmin;
//     protected global::System.Web.UI.HtmlControls.HtmlGenericControl liSystem;
//}


