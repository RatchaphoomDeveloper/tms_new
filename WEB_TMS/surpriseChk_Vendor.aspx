﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="surpriseChk_Vendor.aspx.cs" Inherits="surpriseChk_Vendor" StylesheetTheme="Aqua" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .grvHeaderFlag {
            text-align: center !important;
        }
    </style>

    <style type="text/css">
        .search {
            background: url(../Images/ic_search.gif) no-repeat;
            background-position-y: center;
            background-position-x: right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color: white;
        }

        .time {
            background: url(../Images/ico_time.png) no-repeat;
            background-position-y: center;
            background-position-x: right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color: white;
        }

        .auto-style1 {
            height: 21px;
        }

        #file-input {
            cursor: pointer;
            outline: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 0;
            height: 0;
            overflow: hidden;
            filter: alpha(opacity=0); /* IE < 9 */
            opacity: 0;
        }

        .input-label {
            cursor: pointer;
            position: relative;
            display: inline-block;
        }
    </style>

    <style type="text/css">
        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ข้อมูลทั่วไป&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract">
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="จำนวนรถที่นัดหมาย" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TotalCar" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="ชื่อผู้ประกอบการ" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control" Enabled="false" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="เลขที่สัญญา" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="วันที่นัดหมายตรวจประจำปี" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="AppointDate" runat="server" CssClass="form-control datetimepicker" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="สถานที่ตรวจนัดหมายประจำปี" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="AppointLocate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="ผู้ทำการตรวจ" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlUserCheck" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="รายชื่อทีมผู้ทำการตรวจ" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlTeamCheck" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="หมายเหตุ" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="Remark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right" runat="server" id="divButton">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSaveY" runat="server" Text="ยืนยัน" OnClick="btnSaveY_Click" CssClass="btn btn-info" Enabled="false" />
                            <asp:Button ID="btnCancle" runat="server" Text="ยกเลิก" OnClick="btnCancle_Click" CssClass="btn btn-danger" Enabled="false" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract5">รายชื่อทีมผู้ทำการตรวจ&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract5" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract5">
                    <div class="panel-body">
                        <asp:GridView runat="server" DataKeyNames="SUID" ID="dgvTeamCheck" Width="100%" HeaderStyle-HorizontalAlign="Center"
                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" Enabled="false"
                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvTeamCheck_PageIndexChanging"
                            OnRowUpdating="dgvTeamCheck_RowUpdating" OnRowDeleting="dgvTeamCheck_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="Action" Visible="false">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDel" runat="server" ImageUrl="~/Images/bin1.png" Width="23"
                                            Height="23" Style="cursor: pointer" CommandName="delete" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SUID" HeaderText="SUID" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FULLNAME" HeaderText="ชื่อทีมที่จะทำการตรวจสอบ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                        </asp:GridView>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract2">
                    <div class="panel-body">
                        <asp:GridView runat="server" DataKeyNames="ID" ID="dgvSurpriseCheckYear" Width="100%" HeaderStyle-HorizontalAlign="Center"
                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="true"
                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvSurpriseCheckYear_PageIndexChanging"
                            OnRowUpdating="dgvSurpriseCheckYear_RowUpdating">
                            <Columns>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/64/blue-23.png" Width="23"
                                            Height="23" Style="cursor: pointer" CommandName="update" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SVENDORNAME" HeaderText="ชื่อผู้ประกอบการ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TOTALCAR" HeaderText="จำนวนรถที่นัดหมาย">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="APPOINTDATE" HeaderText="วันที่นัดหมายตรวจประจำปี" DataFormatString="{0:dd/MM/yyyy  HH:mm}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="APPOINTLOCATE" HeaderText="สถานที่ตรวจนัดหมายประจำปี">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FULLNAME" HeaderText="ผู้ทำการตรวจ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TEAMCHECK" HeaderText="ทีมผู้ทำการตรวจ" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="STATUS" HeaderText="สถานะ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                        </asp:GridView>
                    </div>
                </div>
                <br />
                <br />
                <div class="panel-footer" style="text-align: right">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


