﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;

public partial class admin_Holiday_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblUser.Text = Session["UserName"] + "";

        if (!IsPostBack)
        {            
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
           
            dteHoliday.Value = DateTime.Now;
            if (Session["oNHOLIDAYID"] != null)
            {
                listData();
            }
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnSubmit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    void ClearControl()
    {

        Session["oNHOLIDAYID"] = null;
        txtHolidayName.Text = "";
        dteHoliday.Value = DateTime.Now;
        lblUser.Text = "";


    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {

            case "Save":
                if (CanWrite)
                {
                    if (Session["oNHOLIDAYID"] == null)
                    {
                        //ตรวจสอบว่ามีข้อมูลอยู่หรือไม่ถ้ามีให้ทำการอัพเดทข้อมูล
                        if (CommonFunction.Count_Value(sql, "Select * from LSTHOLIDAY WHERE TRUNC(DHOLIDAY) = TO_DATE('" + dteHoliday.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')") > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','วันที่วันหยุดนี้มีอยู่ในระบบแล้ว!');");
                            return;
                        }

                        string GenID;

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();

                            string strsql = @"INSERT INTO LSTHOLIDAY ( NID, DHOLIDAY, SHOLIDAY,  SCREATE, DCREATE, SUPDATE,  DUPDATE) VALUES (:NID, :DHOLIDAY, :SHOLIDAY,  :SCREATE, SYSDATE, :SUPDATE, SYSDATE)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                GenID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM LSTHOLIDAY ORDER BY NID DESC) WHERE ROWNUM <= 1");
                                com.Parameters.Clear();
                                com.Parameters.Add(":NID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":DHOLIDAY", OracleType.DateTime).Value = dteHoliday.Value;
                                com.Parameters.Add(":SHOLIDAY", OracleType.VarChar).Value = txtHolidayName.Text;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.ExecuteNonQuery();
                            }
                        }

                        LogUser("41", "I", "บันทึกข้อมูลหน้า ข้อมูลวันหยุด", GenID);
                    }
                    else
                    {
                        if (CommonFunction.Count_Value(sql, "Select * from LSTHOLIDAY WHERE TRUNC(DHOLIDAY) = TO_DATE('" + dteHoliday.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND NID != " + Session["oNHOLIDAYID"]) > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','วันที่วันหยุดนี้มีอยู่ในระบบแล้ว!');");
                            return;
                        }
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();

                            string strsql = @"UPDATE LSTHOLIDAY SET  DHOLIDAY = :DHOLIDAY,  SHOLIDAY = :SHOLIDAY,  SUPDATE  = :SUPDATE, DUPDATE  = SYSDATE WHERE  NID  = :NID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NID", OracleType.Number).Value = Session["oNHOLIDAYID"];
                                com.Parameters.Add(":DHOLIDAY", OracleType.DateTime).Value = dteHoliday.Value;
                                com.Parameters.Add(":SHOLIDAY", OracleType.VarChar).Value = txtHolidayName.Text;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.ExecuteNonQuery();
                            }
                        }

                        LogUser("41", "E", "แก้ไขข้อมูลหน้า ข้อมูลวันหยุด", Session["oNHOLIDAYID"] + "");
                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_Holiday_lst.aspx';});");

                    ClearControl();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }

                break;

        }

    }


    void listData()
    {

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT DHOLIDAY, SHOLIDAY FROM LSTHOLIDAY WHERE NID =" + Session["oNHOLIDAYID"]);
        if (dt.Rows.Count > 0)
        {
            DateTime date;
            dteHoliday.Value =  DateTime.TryParse(dt.Rows[0]["DHOLIDAY"] + "",out date) ? date : DateTime.Now;
            txtHolidayName.Text = dt.Rows[0]["SHOLIDAY"] + "";
            lblUser.Text = Session["UserName"] + "";
        }
        
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}