﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;

public partial class admin_TimeWindows_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblUser.Text = Session["UserName"] + "";
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnDel.Enabled = false;
                btnSubmit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {

        cbxOrganiz.SelectedIndex = 0;
        rblDayType.SelectedIndex = 0;
        txtDStart.Text = "";
        txtDEnd.Text = "";
        txtNLine.Text = "";
        rblStatus.SelectedIndex = 0;
        lblUser.Text = "";
        txtLateDay.Text = "0";
        txtLateTime.Text = "";

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {
        gvw.DataBind();
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        if (CanWrite)
        {
            switch (paras[0])
            {


                case "Save":

                    if (Session["stmID"] == null)
                    {

                        if (CommonFunction.Count_Value(sql, "Select * from TWINDOWTIME Where STERMINALID = '" + cbxOrganiz.Value + "' AND NLINE = " + txtNLine.Text.Trim() + " AND nvl(CDAYTYPE,'0') = '" + rblDayType.Value + "'") > 0)
                        {
                            sds.Update();
                        }
                        else
                        {

                            sds.Insert();
                            Session["stmID"] = cbxOrganiz.Value;
                        }

                        LogUser("31", "I", "บันทึกข้อมูลหน้า Time Windows", "");

                    }
                    else //เป็นการอัพเดท
                    {
                        if (CommonFunction.Count_Value(sql, "Select * from TWINDOWTIME Where STERMINALID = '" + cbxOrganiz.Value + "' AND NLINE = " + txtNLine.Text.Trim() + " AND nvl(CDAYTYPE,'0') = '" + rblDayType.Value + "'") <= 0)
                        {
                            sds.Insert();
                        }
                        else
                        {
                            sds.Update();
                        }

                        LogUser("31", "E", "แก้ไขข้อมูลหน้า Time Windows", "");
                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");
                    gvw.DataBind();

                    ClearControl();


                    break;

                case "edit":
                    int index = int.Parse(e.Parameter.Split(';')[1]);
                    dynamic data = gvw.GetRowValues(index, "STERMINALID", "NLINE", "TSTART", "TEND", "CACTIVE", "CDAYTYPE", "LATEDAY", "LATETIME");

                    cbxOrganiz.Enabled = false;
                    txtNLine.Enabled = false;
                    cbxOrganiz.Value = data[0] + "";
                    txtNLine.Text = data[1] + "";
                    txtDStart.Text = data[2] + "";
                    txtDEnd.Text = data[3] + "";
                    rblStatus.Value = data[4] + "";
                    lblUser.Text = Session["UserName"] + "";
                    rblDayType.Value = data[5] + "";
                    txtLateDay.Text = data[6] + "";
                    txtLateTime.Text = data[7] + "";
                    break;

                case "delete":
                    if (CanWrite)
                    {
                        var ld = gvw.GetSelectedFieldValues("ID1", "STERMINALID", "NLINE", "CDAYTYPE")
                            .Cast<object[]>()
                            .Select(s => new { ID1 = s[0].ToString(), STERMINALID = s[1].ToString(), NLINE = s[2].ToString(), CDAYTYPE = s[3].ToString() });

                        foreach (var l in ld)
                        {
                            Session["oTerminalID"] = l.STERMINALID;
                            Session["oNLine"] = l.NLINE;
                            Session["oCDAYTYPE"] = l.CDAYTYPE;
                            sds.Delete();
                        }

                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                        gvw.Selection.UnselectAll();
                    }

                    break;

            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }


    protected void TP01RouteOnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsOrganiz.SelectCommand = "SELECT STERMINALID, STERMINALNAME FROM (SELECT T.STERMINALID, TS.STERMINALNAME, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T  INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE TS.STERMINALNAME || T.STERMINALID LIKE '%" + e.Filter + "%' AND (T.STERMINALID LIKE '" + PlantHelper.Plant5 + "' OR T.STERMINALID LIKE '" + PlantHelper.Plant7 + "' OR T.STERMINALID LIKE '" + PlantHelper.Plant8 + "'))  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";

        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();

    }

    protected void TP01RouteOnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

        //long value = 0;
        //if (!Int64.TryParse(e.Value.ToString(), out value)) return;
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}