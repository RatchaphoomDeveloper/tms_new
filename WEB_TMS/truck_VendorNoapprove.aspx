﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="truck_VendorNoapprove.aspx.cs" Inherits="truck_VendorNoapprove" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server"> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; inEndRequestHandler();}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                 <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>
                      <%-- คั่นกลางระหว่างแท็บ --%>
                        <dx:TabPage Name="Tab1" Text="ข้อมูลรถ">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                    <div>
                                        <div class="form-horizontal">
                                            <div id="Div_Vendor" runat="server" class="row form-group">
                                                <label class="col-md-2 control-label">บริษัทผู้ขนส่ง :</label>
                                                <div class="col-md-4  ">
                                                   <dx:ASPxComboBox ID="cboVendor" runat="server" Width="100%" ClientInstanceName="cboVendor" CssClass="form-control" TextFormatString="{1}" ValueField="SVENDORID" IncrementalFilteringMode="Contains" >
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="200px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                </div>
                                                 <label class="col-md-2 control-label">
                                                    จำนวนรถบรรทุก :
                                                </label>
                                                 <label class="col-md-1 control-label">
                                                    <%=  VehicleTruck.VisibleRowCount %>
                                                        &nbsp;&nbsp; คัน
                                                </label>                                                 
                                            </div>
                                        <div class="row form-group">
                                                <label class="col-md-2 control-label">ค้นหาข้อมูลรถ :</label>
                                                <div class="col-md-4">
                                                   <dx:ASPxTextBox runat="server" ID="txtsHeadRegisterNo" NullText="หมายเลขทะเบียนรถ"
                                                        CssClass="form-control" Width="300px">
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div class="col-md-4 control-label">
                                                    <div class="col-md-4">
                                                       <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search">
                                                            <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCH;'); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div class="col-md-6">
                                                   <dx:ASPxButton ID="ASPxButton4" ClientInstanceName="btnClearSearch" runat="server"
                                                        SkinID="_clearsearch">
                                                        <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCH_CANCELVEHICLE;'); }" />
                                                    </dx:ASPxButton>
                                                    <%--<dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxButton>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                      
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td bgcolor="#0E4999">
                                                    <img src="images/spacer.GIF" width="250px" height="1px" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <dx:ASPxGridView ID="VehicleTruck" runat="server" ClientInstanceName="VehicleTruck" SkinID="_gvw"
                                                        Width="100%" Style="margin-top: 0px" AutoGenerateColumns="False" KeyFieldName="STRUCKID" DataSourceID="sdsTruckVehicle">
                                                        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="วันที่อัพเดทล่าสุด" Visible="false">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="SABBREVIATION" Caption="ชื่อผู้ขนส่ง" Visible="true">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            
                                                            <dx:GridViewDataTextColumn FieldName="SCARTYPE" Caption="รูปแบบ" Visible="true">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxHyperLink ID="lbkViewContract" ClientInstanceName="lbkViewContract" runat="server"
                                                                        CausesValidation="False" AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false"
                                                                        EnableTheming="false" CssClass="dxeLineBreakFix" Text='<%# ""+Eval("SCONTRACTNO") %>'>
                                                                        <ClientSideEvents Click="function(s,e){ if(gvwTruck.InCallback()) return; else gvwTruck.PerformCallback('VIEWCONTRACT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    </dx:ASPxHyperLink>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>                                                            
                                                            <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนหัว">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนหาง">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>                                                                                
                                                            <dx:GridViewDataTextColumn FieldName="SCARCATEGORY" Caption="ประเภทรถ">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="NSLOT" Caption="จำนวนช่อง">
                                                                <PropertiesTextEdit DisplayFormatString="N0">
                                                                </PropertiesTextEdit>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn> 
                                                            <dx:GridViewDataTextColumn FieldName="NTOTALCAPACITY" Caption="ปริมาตร">
                                                                <PropertiesTextEdit DisplayFormatString="N0">
                                                                </PropertiesTextEdit>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>                                                            
                                                            <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="วันที่เริ่มระงับ">
                                                                <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                                </PropertiesTextEdit>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="STATUS" Caption="การอนุญาตใช้งาน">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="สังกัดรถ" FieldName="ISUSE" ShowInCustomizationForm="false"
                                                                        Visible="true" VisibleIndex="10" Width="8%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                        <DataItemTemplate>
                                                                            <%# Eval("ISUSE").ToString()== "1"?"รถในสังกัด":"รถไม่มีสังกัด"%>
                                                                        </DataItemTemplate>
                                                                    </dx:GridViewDataTextColumn>                                                            
                                                            <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="RTRUCKID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="SCARTYPEID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false" />
                                                            <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false" />
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="sdsTruckVehicle" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdTruck">                                    
                                                    </asp:SqlDataSource>
                    
                                                </td>
                                            </tr>
                                        </table>
                                    </div>        
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
