﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;

public partial class Change2stat : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw.StartRowEditing += new DevExpress.Web.Data.ASPxStartRowEditingEventHandler(gvw_StartRowEditing);
        gvw.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvw_AfterPerformCallback);
        gvw.RowUpdating += new DevExpress.Web.Data.ASPxDataUpdatingEventHandler(gvw_RowUpdating);
        xcpn.Callback += new DevExpress.Web.ASPxClasses.CallbackEventHandlerBase(xcpn_Callback);
        //gvw.CellEditorInitialize += new ASPxGridViewEditorEventHandler(gvw_CellEditorInitialize);
        //gvw.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvw_HtmlRowPrepared);
        if (!IsPostBack)
        {

            SearchData("");
        }
    }

    void gvw_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView _gvw = (ASPxGridView)sender;
        int idxEditing = _gvw.EditingRowVisibleIndex;
        dynamic keyValue = _gvw.GetRowValues(idxEditing, "STERMINALID", "TERMINAL_NAME");
        string STERMINALID = keyValue[0] + "";
        string STERMINALNAME = keyValue[1] + "";
        string _SQL = @"INSERT INTO TCHANGE2STAT (STERMINALID, CPRODUCT_GROUP, MIN2STAT, MAX2STAT, CACTIVE) VALUES ('{0}', '{1}',  {2}, {3} , '{4}')";

        string sQry = "DELETE TCHANGE2STAT WHERE STERMINALID='" + STERMINALID + "'";

        using (OracleConnection _connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            if (_connection.State == ConnectionState.Closed) _connection.Open();
            using (OracleCommand com = new OracleCommand(sQry, _connection))
            {
                com.ExecuteNonQuery();
            }

            for (int iCol = 0; iCol < 5; iCol++)
            {
                ASPxTextBox txtGRP_MIN2STAT = _gvw.FindEditRowCellTemplateControl(null, "txtGRP" + (iCol + 1) + "_MIN2STAT") as ASPxTextBox;
                ASPxTextBox txtGRP_MAX2STAT = _gvw.FindEditRowCellTemplateControl(null, "txtGRP" + (iCol + 1) + "_MAX2STAT") as ASPxTextBox;
                sQry = string.Format(_SQL, STERMINALID, (iCol + 1) + ""
                    , CommonFunction.ReplaceInjection(txtGRP_MIN2STAT.Text).Equals("") ? "null" : "'" + CommonFunction.ReplaceInjection(txtGRP_MIN2STAT.Text) + "'"
                    , CommonFunction.ReplaceInjection(txtGRP_MAX2STAT.Text).Equals("") ? "null" : "'" + CommonFunction.ReplaceInjection(txtGRP_MAX2STAT.Text) + "'"
                    , "1");

                using (OracleCommand com = new OracleCommand(sQry, _connection))
                {
                    com.ExecuteNonQuery();
                }

            }
        }
        e.Cancel = true;
        _gvw.CancelEdit();
        SearchData("");
    }
    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');
                int idxRow = int.Parse(param[1] + "");
                switch (param[0])
                {
                    case "startedit":
                        gvw.StartEdit(idxRow);
                        break;
                    case "updateedit":
                        gvw.UpdateEdit();

                        gvw.CancelEdit();
                        break;
                    case "cancel":
                        break;
                    case "search":
                        Cache.Remove(sdsCHG2ST.CacheKeyDependency);
                        Cache[sdsCHG2ST.CacheKeyDependency] = new object();
                        sdsCHG2ST.Select(new System.Web.UI.DataSourceSelectArguments());
                        sdsCHG2ST.DataBind();
                        gvw.DataBind();
                        break;
                }
                break;
        }
    }
    protected void gvw_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        string PLANT_ID = gvw.GetRowValuesByKeyValue(e.EditingKeyValue, "STERMINALID") + "";
    }
    protected void gvw_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":
                SearchData("");
                break;
        }
    }
    protected void SearchData(string _mode)
    {

        Cache.Remove(sdsCHG2ST.CacheKeyDependency);
        Cache[sdsCHG2ST.CacheKeyDependency] = new object();
        sdsCHG2ST.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsCHG2ST.DataBind();
        gvw.DataBind();
    }
}