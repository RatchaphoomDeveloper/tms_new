﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="monitor_LogEmail.aspx.cs" Inherits="monitor_LogEmail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .style13
        {
            width: 803px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    
    <a href="SupportReport.aspx">Support Report</a>
    <div id="DivNoneMode" runat="server" visible="true">
        <table width="100%">
            <tr>
                <td class="style13" align="right">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxDateEdit ID="dteStart" runat="server" ClientInstanceName="dteStart" CssClass="dxeLineBreakFix"
                                    SkinID="xdte" NullText="ระหว่างวันที่">
                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                        <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                    </ValidationSettings>
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dteEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                                    SkinID="xdte" NullText="ถึงวันที่">
                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                        <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                    </ValidationSettings>
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="center">
                    <dx:ASPxButton ID="btnSearch" runat="server" ClientInstanceName="btnSearch" Text=" ค้นหารายการ "
                        OnClick="btnSearch_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td class="style13">
                    <div style="float: left">
                    </div>
                </td>
                <td>
                </td>
            </tr>
            <tr id="tr_data" runat="server">
                <td colspan="2">
                    <dx:ASPxGridView ID="gvwLogEmail" ClientInstanceName="gvwLogEmail" runat="server"
                        Width="99%" SkinID="_gvw" DataSourceID="sdsLogEmail" KeyFieldName="DLOG" AutoGenerateColumns="False">
                        <ClientSideEvents RowClick="function (s,e) { gvwLogEmail.StartEditRow(e.visibleIndex); }" />
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="DLOG" FieldName="DLOG" />
                            <dx:GridViewDataTextColumn Caption="SSUBJECT" FieldName="SSUBJECT" />
                            <dx:GridViewDataTextColumn Caption="SEMAILTO" FieldName="SEMAILTO" />
                            <dx:GridViewDataTextColumn Caption="SEMAILFROM" FieldName="SEMAILFROM" Visible="false" />
                            <dx:GridViewDataTextColumn Caption="SREFID" FieldName="SREFID" Visible="false" />
                            <dx:GridViewDataTextColumn Caption="DSEND" FieldName="DSEND" Visible="false" />
                            <dx:GridViewDataTextColumn Caption="SMESSAGE" FieldName="SMESSAGE" Visible="false" />
                        </Columns>
                        <Templates>
                            <EditForm>
                                <asp:Literal ID="ltrDetail" runat="server"></asp:Literal>
                            </EditForm>
                        </Templates>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="sdsLogEmail" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                        CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                        SelectCommand="SELECT * FROM TLOGEMAIL 
WHERE TO_DATE(DSEND,'DD/MM/YYYY') &gt;=TO_DATE( :D_START,'DD/MM/YYYY') AND TO_DATE( DSEND,'DD/MM/YYYY') &lt;=TO_DATE( :D_END,'DD/MM/YYYY')  
order by dlog desc">
                        <SelectParameters>
                            <asp:ControlParameter Name=":D_START" ControlID="dteStart" PropertyName="Value" DbType="DateTime" />
                            <asp:ControlParameter Name=":D_END" ControlID="dteEnd" PropertyName="Value" DbType="DateTime" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
