﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class doc_set : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
       // gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        Listdata();

        if (!IsPostBack)
        {
            this.AssignAuthen();
        }
    }

    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //Response.Redirect("NotAuthorize.aspx");  
                btnSerach.Enabled = false;
            }
            if (!CanWrite)
            {
                // gvw.Columns[0].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {


            string[] param = e.Parameter.Split(';');

            

            switch (param[0])
            {
                case "search":
                    Listdata();
                    break;

                case "add":
                    gvw.AddNewRow();
                    Set_Chk_CarType();
                    break;

                case "cancel":
                    gvw.CancelEdit();
                    break;

                case "save":

                    ASPxTextBox txtDocName = gvw.FindEditFormTemplateControl("txtDocName") as ASPxTextBox;
                    ASPxCheckBox chk1 = gvw.FindEditFormTemplateControl("chk1") as ASPxCheckBox;
                    ASPxCheckBox chk2 = gvw.FindEditFormTemplateControl("chk2") as ASPxCheckBox;
                    ASPxCheckBox chk3 = gvw.FindEditFormTemplateControl("chk3") as ASPxCheckBox;
                    ASPxCheckBox chk4 = gvw.FindEditFormTemplateControl("chk4") as ASPxCheckBox;
                    ASPxRadioButtonList rblActive = gvw.FindEditFormTemplateControl("rblActive") as ASPxRadioButtonList;
                    ASPxTextBox txtDescription = gvw.FindEditFormTemplateControl("txtDescription") as ASPxTextBox;
                    ASPxRadioButtonList rblReq1 = gvw.FindEditFormTemplateControl("rblReq1") as ASPxRadioButtonList;
                    ASPxRadioButtonList rblReq2 = gvw.FindEditFormTemplateControl("rblReq2") as ASPxRadioButtonList;
                    ASPxRadioButtonList rblReq3 = gvw.FindEditFormTemplateControl("rblReq3") as ASPxRadioButtonList;
                    ASPxRadioButtonList rblReq4 = gvw.FindEditFormTemplateControl("rblReq4") as ASPxRadioButtonList;
                    ASPxCheckBox chkdynamic = gvw.FindEditFormTemplateControl("chkdynamic") as ASPxCheckBox;

                    string schk1 = chk1.Checked ? "Y" : "N";
                    string schk2 = chk2.Checked ? "Y" : "N";
                    string schk3 = chk3.Checked ? "Y" : "N";
                    string schk4 = chk4.Checked ? "Y" : "N";
                    string Active = rblActive.SelectedItem != null ? (rblActive.SelectedItem.Value == "Y" ? "Y" : "N") : "";
                    string srblReq1 = chk1.Checked ? (rblReq1.SelectedItem != null ? (rblReq1.SelectedItem.Value == "Y" ? "Y" : "N") : "") : "";
                    string srblReq2 = chk2.Checked ? (rblReq2.SelectedItem != null ? (rblReq2.SelectedItem.Value == "Y" ? "Y" : "N") : "") : "";
                    string srblReq3 = chk3.Checked ? (rblReq3.SelectedItem != null ? (rblReq3.SelectedItem.Value == "Y" ? "Y" : "N") : "") : "";
                    string srblReq4 = chk4.Checked ? (rblReq4.SelectedItem != null ? (rblReq4.SelectedItem.Value == "Y" ? "Y" : "N") : "") : "";
                    //string schkdynamic = chkdynamic.Checked ? "Y" : "N";
                    string schkdynamic = "Y";

                    string CARCATE_ID = "";
                  
                    ASPxCheckBoxList Chk_CarType = gvw.FindEditFormTemplateControl("Chk_CarType") as ASPxCheckBoxList;
                    foreach (ListEditItem item in Chk_CarType.Items)
                    {
                        if (item.Selected == true)
                        {
                            CARCATE_ID += "," + item.Value;
                        }
                    }
                    CARCATE_ID = !string.IsNullOrEmpty(CARCATE_ID) ? CARCATE_ID.Remove(0, 1) : "";
                    if (param[1] == "btnSave")
                    {
                        string Insert = @"INSERT INTO TBL_DOCTYPE (
                                       DOCTYPE_ID, DOC_DESCRIPTION, DOC_01, 
                                       DOC_02, DOC_03, DOC_04, CATTACH_DOC01,CATTACH_DOC02,CATTACH_DOC03, CATTACH_DOC04,CDYNAMIC,
                                       ISACTIVE_FLAG, DESCRIPTION,MASTER_FLAG,CARCATE_ID) 
                                      VALUES ( '" + GenID() + @"',
                                     '" + CommonFunction.ReplaceInjection(txtDocName.Text) + @"',
                                     '" + CommonFunction.ReplaceInjection(schk1) + @"',
                                     '" + CommonFunction.ReplaceInjection(schk2) + @"',
                                     '" + CommonFunction.ReplaceInjection(schk3) + @"',
                                     '" + CommonFunction.ReplaceInjection(schk4) + @"',
                                     '" + CommonFunction.ReplaceInjection(srblReq1) + @"',
                                     '" + CommonFunction.ReplaceInjection(srblReq2) + @"',
                                     '" + CommonFunction.ReplaceInjection(srblReq3) + @"',
                                     '" + CommonFunction.ReplaceInjection(srblReq4) + @"',
                                     '" + CommonFunction.ReplaceInjection(schkdynamic) + @"',
                                     '" + CommonFunction.ReplaceInjection(Active) + @"',
                                     '" + CommonFunction.ReplaceInjection(txtDescription.Text) + @"',
                                     'N'
                                     ,'" + CommonFunction.ReplaceInjection(CARCATE_ID) + "')";
                        AddTODB(Insert);

                    }
                    else
                    {
                        int inx = param[1] + "" != "btnSave" ? int.Parse(param[1] + "") : 0;
                        dynamic ID = gvw.GetRowValues(inx, "DOCTYPE_ID");
                        string Update = @"UPDATE TBL_DOCTYPE
                                      SET      DOC_DESCRIPTION = '" + CommonFunction.ReplaceInjection(txtDocName.Text) + @"',
                                               DOC_01          = '" + CommonFunction.ReplaceInjection(schk1) + @"',
                                               DOC_02          =  '" + CommonFunction.ReplaceInjection(schk2) + @"',
                                               DOC_03          =  '" + CommonFunction.ReplaceInjection(schk3) + @"',
                                               DOC_04          =  '" + CommonFunction.ReplaceInjection(schk4) + @"',
                                               CATTACH_DOC01   = '" + CommonFunction.ReplaceInjection(srblReq1) + @"',
                                               CATTACH_DOC02   =  '" + CommonFunction.ReplaceInjection(srblReq2) + @"',
                                               CATTACH_DOC03   =  '" + CommonFunction.ReplaceInjection(srblReq3) + @"',
                                               CATTACH_DOC04   =  '" + CommonFunction.ReplaceInjection(srblReq4) + @"',
                                               CDYNAMIC   =  '" + CommonFunction.ReplaceInjection((ID != "0002" ? schkdynamic : "N")) + @"',
                                               DESCRIPTION     = '" + CommonFunction.ReplaceInjection(txtDescription.Text) + @"',
                                               ISACTIVE_FLAG   = '" + CommonFunction.ReplaceInjection(Active) + @"',
                                               CARCATE_ID = '" + CommonFunction.ReplaceInjection(CARCATE_ID) + @"'
                                     WHERE  DOCTYPE_ID      = " + CommonFunction.ReplaceInjection(ID) + "";
                        AddTODB(Update);

                    }

                    gvw.CancelEdit();
                    Listdata();
                    break;

                case "edit":
                    int inx2 = param[1] + "" != "" ? int.Parse(param[1] + "") : 0;
                    gvw.StartEdit(inx2);

                    ASPxTextBox etxtDocName = gvw.FindEditFormTemplateControl("txtDocName") as ASPxTextBox;
                    ASPxCheckBox echk1 = gvw.FindEditFormTemplateControl("chk1") as ASPxCheckBox;
                    ASPxCheckBox echk2 = gvw.FindEditFormTemplateControl("chk2") as ASPxCheckBox;
                    ASPxCheckBox echk3 = gvw.FindEditFormTemplateControl("chk3") as ASPxCheckBox;
                    ASPxCheckBox echk4 = gvw.FindEditFormTemplateControl("chk4") as ASPxCheckBox;
                    ASPxRadioButtonList erblActive = gvw.FindEditFormTemplateControl("rblActive") as ASPxRadioButtonList;
                    ASPxTextBox etxtDescription = gvw.FindEditFormTemplateControl("txtDescription") as ASPxTextBox;
                    Set_Chk_CarType();
                    ASPxCheckBoxList Chk_CarType2 = gvw.FindEditFormTemplateControl("Chk_CarType") as ASPxCheckBoxList;


                    dynamic DOCID = gvw.GetRowValues(inx2, "DOCTYPE_ID");

                    string Query = @"SELECT DOCTYPE_ID, DOC_DESCRIPTION, DOC_01, DOC_02, DOC_03, DOC_04, ISACTIVE_FLAG, DESCRIPTION,CARCATE_ID
                          FROM TBL_DOCTYPE WHERE DOCTYPE_ID = '" + CommonFunction.ReplaceInjection(DOCID + "") + "'";

                    DataTable dt = CommonFunction.Get_Data(conn, Query);
                    if (dt.Rows.Count > 0)
                    {
                        etxtDocName.Text = dt.Rows[0]["DOC_DESCRIPTION"] + "";
                        echk1.Checked = dt.Rows[0]["DOC_01"] + "" == "Y" ? true : false;
                        echk2.Checked = dt.Rows[0]["DOC_02"] + "" == "Y" ? true : false;
                        echk3.Checked = dt.Rows[0]["DOC_03"] + "" == "Y" ? true : false;
                        echk4.Checked = dt.Rows[0]["DOC_04"] + "" == "Y" ? true : false;
                        erblActive.SelectedIndex = dt.Rows[0]["ISACTIVE_FLAG"] + "" == "Y" ? 0 : 1;
                        etxtDescription.Text = dt.Rows[0]["DESCRIPTION"] + "";

                        foreach (ListEditItem item in Chk_CarType2.Items)
                        {
                            if ((dt.Rows[0]["CARCATE_ID"] + "").Contains(item.Value + ""))
                            {
                                item.Selected = true;
                            }
                        }

                    }


                    break;

            }

        }
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    private string GenID()
    {
        string Result = "";
        string Query = @"SELECT DOCTYPE_ID FROM (SELECT DOCTYPE_ID FROM TBL_DOCTYPE ORDER BY DOCTYPE_ID DESC) WHERE ROWNUM <= 1";

        DataTable dt = CommonFunction.Get_Data(conn, Query);

        if (dt.Rows.Count > 0)
        {
            Result = !string.IsNullOrEmpty(dt.Rows[0]["DOCTYPE_ID"] + "") ? ((int.Parse(dt.Rows[0]["DOCTYPE_ID"] + "") + 1) + "").PadLeft(4, '0') : "0001";
        }
        else
        {
            Result = "0001";
        }

        return Result;
    }

    void Set_Chk_CarType()
    {
        //string QUERY = "SELECT CARCATE_ID,CARCATE_NAME FROM TBL_CARCATE ORDER BY ORDER_CAR";
        //DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        //ASPxCheckBoxList Chk_CarType = gvw.FindEditFormTemplateControl("Chk_CarType") as ASPxCheckBoxList;
        //Chk_CarType.DataSource = dt;
        //Chk_CarType.TextField = "CARCATE_NAME";
        //Chk_CarType.ValueField = "CARCATE_ID";
        //Chk_CarType.DataBind();


    }

    void Listdata()
    {
        string Conditions = "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Conditions = " AND DOC_DESCRIPTION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "%'";

        }

        string Query = @"SELECT DOCTYPE_ID, DOC_DESCRIPTION, NVL(DOC_01,'N') as DOC_01 , NVL(DOC_02,'N') as DOC_02 , NVL(DOC_03,'N') as DOC_03, NVL(DOC_04,'N') as DOC_04 , NVL(CATTACH_DOC01,'N') as CATTACH_DOC01, NVL(CATTACH_DOC02,'N') as CATTACH_DOC02, NVL(CATTACH_DOC03,'N') as CATTACH_DOC03, NVL(CATTACH_DOC04,'N') as CATTACH_DOC04, NVL(CDYNAMIC,'N') as CDYNAMIC, NVL(ISACTIVE_FLAG,'Y') as ISACTIVE_FLAG, DESCRIPTION,MASTER_FLAG
                          FROM TBL_DOCTYPE WHERE 1=1 AND DOCTYPE_ID <> '0002' " + Conditions + " ORDER BY DOCTYPE_ID ASC ";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;
            gvw.DataBind();
        }
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        //ASPxButton btnAdd = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "btnAdd") as ASPxButton;
        ASPxButton btnEdit = gvw.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)gvw.Columns["data1"], "btnEdit") as ASPxButton;
        //Code Edit and Save not work yet - mark on 12 July
        //ASPxButton btnSave = gvw.FindEditFormTemplateControl("btnSave") as ASPxButton;
        //ASPxButton btnSave = (ASPxButton)gvw.FindEditFormTemplateControl("btnSave");
        if (!CanWrite)
        {            
            btnEdit.Enabled = false;           
        }
    }

}