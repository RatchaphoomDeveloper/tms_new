﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class KPI_Evaluation_Kit : PageBase
{
    #region + View State +
    private DataTable dtKPI
    {
        get
        {
            if ((DataTable)ViewState["dtKPI"] != null)
                return (DataTable)ViewState["dtKPI"];
            else
                return null;
        }
        set
        {
            ViewState["dtKPI"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdFormula.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdAdd.Enabled = false;
                dgvKPI.Columns[6].Visible = false;
               
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialForm()
    {
        try
        {
            this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SearchData(string Condition)
    {
        try
        {
            dtKPI = KPIBLL.Instance.KPIEvaluationSelectBLL(string.Empty);
            GridViewHelper.BindGridView(ref dgvKPI, dtKPI);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvKPI_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvKPI.DataKeys[e.RowIndex]["TOPIC_HEADER_ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            MsgAlert.OpenForm("KPI_Evaluation_Kit_AddEdit.aspx?TOPIC_HEADER_ID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
       
    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        MsgAlert.OpenForm("KPI_Evaluation_Kit_AddEdit.aspx", Page);
    }
    protected void cmdFormula_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Formula.aspx");
    }
    protected void dgvKPI_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvKPI.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvKPI, dtKPI);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}