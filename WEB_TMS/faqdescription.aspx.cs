﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxHtmlEditor;

public partial class faqdescription : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);

                Binddata(strQuery[0] + "");
            }

        }


    }

    void Binddata(string FAQID)
    {
        string strsql = @"SELECT TFAQ.FAQ_ID,  TFAQ.FAQ_QUEST,  TFAQ.FAQ_ANSWER,  TFAQ.USER_CREATED,  TFAQ.DATE_CREATED, TUSER.SFIRSTNAME, TFAQ.FILE_ORIGINALNAME,TFAQ.FILE_SYSTEMNAME FROM TFAQ
LEFT JOIN TUSER ON TFAQ.USER_CREATED = TUSER.SUID WHERE FAQ_ID  = " + FAQID;

        

        DataTable dt = CommonFunction.Get_Data(sql, strsql);

        if (dt.Rows.Count > 0)
        {
            lblQuest.Text = dt.Rows[0]["FAQ_QUEST"] + "";
            lblUsercreated.Text = dt.Rows[0]["SFIRSTNAME"] + "";
            lblDatecreated.Text = dt.Rows[0]["DATE_CREATED"] + "";
            lblAnswer.Text = dt.Rows[0]["FAQ_ANSWER"] + "";
            txtFileName.Text = dt.Rows[0]["FILE_ORIGINALNAME"] + "";
            txtFilePath.Text = dt.Rows[0]["FILE_SYSTEMNAME"] + "";

            if (txtFilePath.Text != "")
            {
                lbnFile.Text = "เอกสารแนบ " + dt.Rows[0]["FILE_ORIGINALNAME"] + " คลิกเพื่อดาวโหลด";
            }
            else
            {
                lbnFile.Text = "";
            }
        }
    }

    protected void lbnFile_Click(object sender, EventArgs e)
    {


        Response.Redirect(txtFilePath.Text);


    }
    
}