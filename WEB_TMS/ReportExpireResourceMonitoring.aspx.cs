﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using DevExpress.Web.ASPxGridView;
using System.Text;
using DevExpress.XtraPrinting;
using System.Net.Mime;
using System.IO;
using DevExpress.XtraPrintingLinks;
using iTextSharp.text;
using System.Drawing.Printing;

public partial class ReportExpireResourceMonitoring : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        #endregion
        visiblecontrol();
        if (!IsPostBack)
        {
            ListYear();
        }
    }


    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

        gvw.CancelEdit();
        BindData();

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {

            case "view":

                dynamic dataHead = "";
                dynamic dataTrailer = "";
                dynamic dataCarType = "";

                dynamic data2 = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRANSPORTTYPE");

                dataHead = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRUCKID");

                dataTrailer = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STRAILERID");

                dataCarType = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SCARTYPE");

                string rowsIndex1 = paras[1] + "";
                gvw.StartEdit(int.Parse(rowsIndex1));

                Literal ltl3 = (Literal)gvw.FindEditFormTemplateControl("ltlTruckTotal");
                Literal ltl4 = (Literal)gvw.FindEditFormTemplateControl("ltlTruckTotal1");

                StringBuilder sb2 = new StringBuilder();
                string st2 = @"
                            <tr>
                              <td width='15%' align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสผู้ขนส่ง</span></td>
                              <td width='19%' align='left'><span class='style24'>{0}</span></td>
                              <td width='15%' align='left' bgcolor='#FFEBD7'><span class='style24'>ชื่อผู้ขนส่ง</span></td>
                              <td width='32%' align='left'><span class='style24'>{1}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ประเภทผู้ขนส่ง</span></td>
                              <td align='left'><span class='style24'>{2}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ชื่อเจ้าของรถ</span></td>
                              <td align='left'><span class='style24'>{3}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ทะเบียนรถ (หัว) </span></td>
                              <td align='left'><span class='style24'>{4}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ทะเบียนรถ(ท้าย)</span></td>
                              <td align='left'><span class='style24'>{5}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสรถ(หัว)</span></td>
                              <td align='left'><span class='style24'>{6}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>รหัสรถ(ท้าย)</span></td> 
                              <td align='left'><span class='style24'>{7}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ประเภทรถ</span></td>
                              <td align='left'><span class='style24'>{8}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'></span></td>
                              <td align='left'><span class='style24'></span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>หมายเลขแชชซีย์</span></td>
                              <td align='left'><span class='style24'>{9}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>หมายเลขเครื่องยนต์</span></td>
                              <td align='left'><span class='style24'>{10}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>จำนวนล้อ</span></td>
                              <td align='left'><span class='style24'>{11}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>วันที่จดทะเบียน</span></td>
                              <td align='left'><span class='style24'>{12}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>กลุ่มผลิตภัณฑ์ที่บรรทุก</span></td>
                              <td align='left'><span class='style24'>{13}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>วันหมดอายุวัดน้ำ</span></td>
                              <td align='left'><span class='style24'>{18}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>น้ำหนักบรรทุก</span></td>
                              <td align='left'><span class='style24'>{15}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>น้ำหนักรถ</span></td>
                              <td align='left'><span class='style24'>{14}</span></td>
                            </tr>
                            <tr>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>จำนวนช่อง</span></td>
                              <td align='left'><span class='style24'>{16}</span></td>
                              <td align='left' bgcolor='#FFEBD7'><span class='style24'>ความจุรวม</span></td>
                              <td align='left'><span class='style24'>{17}</span></td>
                            </tr> ";

                //////////// ส่วนหัว


                string query1 =string.Format(@"

SELECT
T.DWATEREXPIRE,T.STRANSPORTID,V.SABBREVIATION CUST_ABBR,TT.STRANS_TYPE_DESC,T.SOWNER_NAME,T.SHEADREGISTERNO,T.STRAILERREGISTERNO,T.STRUCKID SHEADID,T.STRAILERID,

CASE 
WHEN  t.NTOTALCAPACITY <= 10000 AND t.SCARTYPEID = 0 THEN 'หกล้อ'
WHEN t.NTOTALCAPACITY > 10000 AND t.SCARTYPEID = 0 THEN 'สิบล้อ'
WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN 'Semi-Trailer'
ELSE 'อื่นๆ' END AS SCARTYPENAME
                                                        ,T.SCHASIS,T.SENGINE,T.NWHEELS,T.DREGISTER,T.SPROD_GRP
                                                        ,T.NWEIGHT
                                                        ,T.NLOAD_WEIGHT
,t.NTOTALCAPACITY  
,t.NSLOT 
, t.DWATEREXPIRE
  FROM TTRUCK t
   LEFT JOIN TVENDOR v ON t.STRANSPORTID = v.SVENDORID
   LEFT JOIN (SELECT COUNT(tt.SHEADREGISTERNO) AS COUNTTRUCK , tt.SHEADREGISTERNO FROM TTRUCK tt WHERE tt.SCARTYPEID = '4' GROUP BY tt.SHEADREGISTERNO)  TUCKCHK ON TUCKCHK.SHEADREGISTERNO = t.STRAILERREGISTERNO
  LEFT JOIN TTRANS_TYPE TT ON T.STRANSPORTTYPE = TT.STRANS_TYPE
WHERE  nvl(t.SCARTYPEID,0) != 4
  AND nvl(t.CACTIVE,'Y') != 'N'
  AND nvl(v.CACTIVE,'Y') != 'N'
  AND T.STRUCKID = '{0}'
 ", CommonFunction.ReplaceInjection( dataHead + ""));

  


                DataTable dt3 = new DataTable();

                    dt3 = CommonFunction.Get_Data(sql,query1);

                    DateTime temp;

                    if (dt3.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt3.Rows)
                        {
                            sb2.Append(string.Format(st2, dr["STRANSPORTID"] + "", dr["CUST_ABBR"] + "", dr["STRANS_TYPE_DESC"] + "", dr["SOWNER_NAME"] + "", dr["SHEADREGISTERNO"] + "", dr["STRAILERREGISTERNO"] + "", dr["SHEADID"] + "", dr["STRAILERID"] + "", dr["SCARTYPENAME"] + "", dr["SCHASIS"] + "", dr["SENGINE"] + "", (dr["NWHEELS"] + "") != "" ? dr["NWHEELS"] + " ล้อ" : "", (DateTime.TryParse(dr["DREGISTER"] + "", out temp) ? temp.ToString("dd/MM/yyyy") : ""), dr["SPROD_GRP"] + "", (dr["NWEIGHT"] + "") != "" ? dr["NWEIGHT"] + " กก." : "", (dr["NLOAD_WEIGHT"] + "") != "" ? dr["NLOAD_WEIGHT"] + " กก." : "", (dr["NSLOT"] + "") != "" ? dr["NSLOT"] + " ช่อง" : "", (dr["NTOTALCAPACITY"] + "") != "" ? dr["NTOTALCAPACITY"] + " ลิตร" : "", (DateTime.TryParse(dr["DWATEREXPIRE"] + "", out temp) ? temp.ToString("dd/MM/yyyy") : "")));
                        }
                        ltl3.Text = sb2.ToString();
                        sb2 = null;
                    }
                    else
                    {
                        if (ltl3 != null)
                        {
                            sb2.Append(string.Format(st2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
                            ltl3.Text = sb2.ToString();
                        }
                    }



                    if (("" + dataCarType) != "Semi-Trailer")
                    {

                        DataTable dt4 = new DataTable();
                        dt4 = CommonFunction.Get_Data(sql, "SELECT NCOMPARTNO,NPANLEVEL,NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(dataHead + "") + "' ORDER BY NCOMPARTNO,NPANLEVEL");

                        if (dt4.Rows.Count > 0)
                        {
                            StringBuilder sb1 = new StringBuilder();
                            string st1 = @"<tr>
                                    <td align='center' class='style24'>{0}</td>
                                    <td align='center' class='style24'>{1}</td>
                                    <td align='center' class='style24'>{2}</td>
                                  </tr>";
                            int i = 1;
                            foreach (DataRow dr in dt4.Rows)
                            {
                                sb1.Append(string.Format(st1, dr["NCOMPARTNO"] + "", dr["NPANLEVEL"] + "", dr["NCAPACITY"] + ""));
                                i++;
                            }

                            ltl4.Text = sb1.ToString();
                            sb1 = null;
                        }
                        else
                        {
                            if (ltl4 != null)
                            {
                                ltl4.Text = "<tr><td align='center' colspan='3' style='color:red'>ไม่มีข้อมูล</td></tr>";
                            }
                        }
                    }





                ////////////////////////////////// ส่วนห่าง


                    if (("" + dataTrailer) != "")
                    {

                        Literal ltl3_2 = (Literal)gvw.FindEditFormTemplateControl("ltlTruckTotal2_1");
                        Literal ltl4_2 = (Literal)gvw.FindEditFormTemplateControl("ltlTruckTotal2_2");


                        string query2 = string.Format(@"

SELECT
T.DWATEREXPIRE,T.STRANSPORTID,V.SABBREVIATION CUST_ABBR,TT.STRANS_TYPE_DESC,T.SOWNER_NAME,T.SHEADREGISTERNO,T.STRAILERREGISTERNO,T.STRUCKID SHEADID,T.STRAILERID,

CASE 
WHEN  t.NTOTALCAPACITY <= 10000 AND t.SCARTYPEID = 0 THEN 'หกล้อ'
WHEN t.NTOTALCAPACITY > 10000 AND t.SCARTYPEID = 0 THEN 'สิบล้อ'
WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN 'Semi-Trailer'
ELSE 'อื่นๆ' END AS SCARTYPENAME
                                                        ,T.SCHASIS,T.SENGINE,T.NWHEELS,T.DREGISTER,T.SPROD_GRP
                                                        ,T.NWEIGHT
                                                        ,T.NLOAD_WEIGHT
,t.NTOTALCAPACITY  
,t.NSLOT 
, t.DWATEREXPIRE
  FROM TTRUCK t
   LEFT JOIN TVENDOR v ON t.STRANSPORTID = v.SVENDORID
   LEFT JOIN (SELECT COUNT(tt.SHEADREGISTERNO) AS COUNTTRUCK , tt.SHEADREGISTERNO FROM TTRUCK tt WHERE tt.SCARTYPEID = '4' GROUP BY tt.SHEADREGISTERNO)  TUCKCHK ON TUCKCHK.SHEADREGISTERNO = t.STRAILERREGISTERNO
  LEFT JOIN TTRANS_TYPE TT ON T.STRANSPORTTYPE = TT.STRANS_TYPE
WHERE nvl(t.CACTIVE,'Y') != 'N'
  AND T.STRUCKID = '{0}'
 ", CommonFunction.ReplaceInjection(dataTrailer + ""));

                        StringBuilder sb3 = new StringBuilder();
                        DataTable dt3_2 = new DataTable();

                        dt3_2 = CommonFunction.Get_Data(sql, query2);
                        if (dt3_2.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt3_2.Rows)
                            {
                                sb3.Append(string.Format(st2, dr["STRANSPORTID"] + "", dr["CUST_ABBR"] + "", dr["STRANS_TYPE_DESC"] + "", dr["SOWNER_NAME"] + "", dr["SHEADREGISTERNO"] + "", dr["STRAILERREGISTERNO"] + "", dr["SHEADID"] + "", dr["STRAILERID"] + "", dr["SCARTYPENAME"] + "", dr["SCHASIS"] + "", dr["SENGINE"] + "", (dr["NWHEELS"] + "") != "" ? dr["NWHEELS"] + " ล้อ" : "", (DateTime.TryParse(dr["DREGISTER"] + "", out temp) ? temp.ToString("dd/MM/yyyy") : ""), dr["SPROD_GRP"] + "", (dr["NWEIGHT"] + "") != "" ? dr["NWEIGHT"] + " กก." : "", (dr["NLOAD_WEIGHT"] + "") != "" ? dr["NLOAD_WEIGHT"] + " กก." : "", (dr["NSLOT"] + "") != "" ? dr["NSLOT"] + " ช่อง" : "", (dr["NTOTALCAPACITY"] + "") != "" ? dr["NTOTALCAPACITY"] + " ลิตร" : "", (DateTime.TryParse(dr["DWATEREXPIRE"] + "", out temp) ? temp.ToString("dd/MM/yyyy") : "")));
                            
                            }
                            ltl3_2.Text = sb3.ToString();
                            sb3 = null;
                        }
                        else
                        {
                            if (ltl3_2 != null)
                            {
                                sb3.Append(string.Format(st2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
                                ltl3_2.Text = sb3.ToString();
                            }
                        }


                        DataTable dt4_2 = new DataTable();
                        dt4_2 = CommonFunction.Get_Data(sql, "SELECT NCOMPARTNO,NPANLEVEL,NCAPACITY FROM TTRUCK_COMPART WHERE STRUCKID = '" + dataTrailer + "' ORDER BY NCOMPARTNO,NPANLEVEL");

                        if (dt4_2.Rows.Count > 0)
                        {
                            StringBuilder sb1 = new StringBuilder();
                            string st1 = @"<tr>
                                    <td align='center' class='style24'>{0}</td>
                                    <td align='center' class='style24'>{1}</td>
                                    <td align='center' class='style24'>{2}</td>
                                  </tr>";
                            int i = 1;
                            foreach (DataRow dr in dt4_2.Rows)
                            {
                                sb1.Append(string.Format(st1, dr["NCOMPARTNO"] + "", dr["NPANLEVEL"] + "", dr["NCAPACITY"] + ""));
                                i++;
                            }

                            ltl4_2.Text = sb1.ToString();
                            sb1 = null;
                        }
                        else
                        {
                            if (ltl4_2 != null)
                            {
                                ltl4_2.Text = "<tr><td align='center' colspan='3' style='color:red'>ไม่มีข้อมูล</td></tr>";
                            }
                        }

                    }

                break;


        }
    }


    void BindData()
    {

        sds.SelectCommand = "";
        sds.SelectParameters.Clear();


        string sqlQuery = @"
SELECT ROW_NUMBER () OVER (ORDER BY o.STRUCKID) AS ID1, o.* FROM(

SELECT
T.STRUCKID ,
T.STRAILERID,
T.STRANSPORTTYPE,
T.SHEADREGISTERNO,
T.STRAILERREGISTERNO,
V.SABBREVIATION,
C.SCONTRACTNO
,
CASE 
WHEN  t.NTOTALCAPACITY <= 10000 AND t.SCARTYPEID = 0 THEN 'หกล้อ'
WHEN t.NTOTALCAPACITY > 10000 AND t.SCARTYPEID = 0 THEN 'สิบล้อ'
WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN 'Semi-Trailer'
ELSE 'อื่นๆ' END AS SCARTYPE
, CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0  THEN TRCK_TAIL.NTOTALCAPACITY ELSE  t.NTOTALCAPACITY  END AS NTOTALCAPACITY
, CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0  THEN TRCK_TAIL.DWATEREXPIRE  ELSE  t.DWATEREXPIRE  END AS DWATEREXPIRE
,  CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN   TRCK_TAIL.DSIGNIN  ELSE  t.DSIGNIN  END AS DSIGNIN
,  CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN   TRCK_TAIL.DREGISTER  ELSE  t.DREGISTER  END AS DREGISTER
,  trunc(months_between(sysdate,CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN   TRCK_TAIL.DSIGNIN  ELSE  t.DSIGNIN  END)/12) AS NYEAR,
CASE WHEN (
 CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN   TRCK_TAIL.DSIGNIN  ELSE  t.DSIGNIN  END) IS NOT NULL THEN 
 
trunc(months_between(sysdate,CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN   TRCK_TAIL.DSIGNIN  ELSE  t.DSIGNIN  END)/12) || ' ปี ' || 
trunc(mod(months_between(sysdate,CASE WHEN  t.SCARTYPEID = 3 AND  NVL(TUCKCHK.COUNTTRUCK,0) > 0 THEN   TRCK_TAIL.DSIGNIN  ELSE  t.DSIGNIN  END),12)) || ' เดือน ' ELSE '' END  AS SYEARMONTH
  FROM TTRUCK t
   LEFT JOIN TTRUCK TRCK_TAIL ON T.STRAILERID=TRCK_TAIL.STRUCKID
   LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND nvl(T.STRAILERID,'0') = nvl(CT.STRAILERID,'0') LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID
   LEFT JOIN TVENDOR v ON C.SVENDORID = v.SVENDORID
   LEFT JOIN (SELECT COUNT(tt.SHEADREGISTERNO) AS COUNTTRUCK , tt.SHEADREGISTERNO FROM TTRUCK tt WHERE tt.SCARTYPEID = '4' GROUP BY tt.SHEADREGISTERNO)  TUCKCHK ON TUCKCHK.SHEADREGISTERNO = t.STRAILERREGISTERNO
  WHERE 1=1 
  --and nvl(t.SCARTYPEID,0) != 4
  --and nvl(c.SCONTRACTID,0) != 0
  AND nvl(t.CACTIVE,'Y') != 'N'
  AND nvl(c.CACTIVE,'Y') != 'N'
  AND nvl(v.CACTIVE,'Y') != 'N'
  
  ) o WHERE 1 = 1 

";


        switch (rblSearch.Value.ToString())
        {
            case "1":

                if (dteStart.Value != null && dteEnd.Value != null)
                {

                    sqlQuery += "AND o.DWATEREXPIRE BETWEEN TO_DATE('" + dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND  TO_DATE('" + dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                }


                //sqlQuery += " AND TRUNC(SYSDATE - o.DWATEREXPIRE) > 0";

                break;
            case "2":

                int temp;
                if (dteStart.Value != null && dteEnd.Value != null)
                {

                    int yEnd = (int.TryParse(cbxYear.Value.ToString(), out temp) ? temp : 0) * (-1);
                    //sqlQuery += "AND o.DSIGNIN BETWEEN TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND  TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                    sqlQuery += "AND o.DREGISTER BETWEEN TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND  TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                }


                if (cbxYear.Value.ToString() != "-1")
                {
                    int Year = int.TryParse(cbxYear.Value.ToString(), out temp) ? temp : 0;
                    //sqlQuery += " AND trunc(months_between(sysdate,o.DSIGNIN )/12) = " + Year;
                    sqlQuery += " AND trunc(months_between(sysdate,o.DREGISTER)/12) = " + Year;
                }

                break;
        }




        sds.SelectCommand = sqlQuery;

        sds.DataBind();
        gvw.DataBind();
    }


    protected void ListYear()
    {
        cbxYear.Items.Add(new ListEditItem(" - ทั้งหมด - ", (-1).ToString()));

        for (int i = 5; i <= 30; i++)
        {
            cbxYear.Items.Add(new ListEditItem(i.ToString(), i.ToString()));
        }

        cbxYear.SelectedIndex = 0;
    }

    void visiblecontrol()
    {
        if (rblSearch.Value.ToString() == "1")
        {
            cbxYear.ClientEnabled = false;

        }
        else
        {

            cbxYear.ClientEnabled = true;
        }

    }


    //protected void btnPdfExport2_Click(object sender, EventArgs e)
    //{

    //    BindData();
    //    PdfExportOptions options = new PdfExportOptions();


    //    gridExport.GridViewID = "gvw";
    //    gridExport.PageHeader.Center = "รายงานรถที่ใกล้หมดสภาพการใช้งาน";
    //    gridExport.PageHeader.Font.Bold = true;
    //    gridExport.Landscape = true;
    //    gridExport.MaxColumnWidth = 180;
    //   // gridExport.WritePdfToResponse("ReportExpireResourceMonitoring", options);

    //    using (MemoryStream ms = new MemoryStream())
    //    {
    //        PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
    //        pcl.Component = gridExport;
    //        pcl.Margins.Left = pcl.Margins.Right = 50;
    //        pcl.Landscape = true;
    //        pcl.CreateDocument(false);
    //        pcl.PrintingSystem.Document.AutoFitToPagesWidth = 1;
    //        pcl.ExportToPdf(ms);
    //        WriteResponse(this.Response, ms.ToArray(), System.Net.Mime.DispositionTypeNames.Inline.ToString());
    //    }
    //}

    protected void btnPdfExport2_Click(object sender, EventArgs e)
    {

        BindData();
        PdfExportOptions options = new PdfExportOptions();

        //gvw.VisibleColumns[8].Visible = false;
        gridExport.GridViewID = "gvw";
        gridExport.PageHeader.Center = "รายงานรถที่ใกล้หมดสภาพการใช้งาน";
        gridExport.PageHeader.Font.Bold = true;
        gridExport.Landscape = true;
        //gridExport.MaxColumnWidth = 150;
        
        
        // gridExport.WritePdfToResponse("ReportExpireResourceMonitoring", options);
        PrintingSystem ps = new PrintingSystem();
        //Link header = new Link();
        //header.CreateDetailArea += new CreateAreaEventHandler(header_CreateDetailArea);
        Margins m = new Margins(10, 10, 10, 10); 
        DevExpress.Web.ASPxGridView.Export.Helper.GridViewLink link1 = new DevExpress.Web.ASPxGridView.Export.Helper.GridViewLink(gridExport);

        CompositeLink compositeLink = new CompositeLink(ps);
        compositeLink.Links.AddRange(new object[] { link1 });
        compositeLink.Margins = m;
        compositeLink.CreateDocument();
        using (MemoryStream stream = new MemoryStream())
        {
            compositeLink.PrintingSystem.ExportToPdf(stream);
            WriteToResponse("ReportExpireResourceMonitoring", true, "pdf", stream);
        }
        ps.Dispose();
    }

    #region Report Function
    //private System.Drawing.Image headerImage;
    //void header_CreateDetailArea(object sender, CreateAreaEventArgs e)
    //{
    //    e.Graph.BorderWidth = 0;

    //    //Rectangle r = new Rectangle(0, 0, 600, 200);
    //    //e.Graph.DrawImage(headerImage, r);

    //    //r = new Rectangle(0, 200, 200, 50);
    //    //e.Graph.DrawString("Report Analysis Of Water", r);

    //    TextBrick tb = new TextBrick();
    //    tb.Text = "Report Analysis Of Water";
    //    tb.Rect = new RectangleF(0, 0, 800, 50);
    //    tb.BorderWidth = 0;
    //    tb.BackColor = Color.Transparent;
    //    tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;
    //    e.Graph.DrawBrick(tb);

    //}

    private void WriteToResponse(string fileName, bool saveAsFile, string fileFormat, MemoryStream stream)
    {
        if (Page == null || Page.Response == null)
            return;
        string disposition = saveAsFile ? "attachment" : "inline";
        Page.Response.Clear();
        Page.Response.Buffer = false;
        Page.Response.AppendHeader("Content-Type", string.Format("application/{0}", fileFormat));
        Page.Response.AppendHeader("Content-Transfer-Encoding", "binary");
        Page.Response.AppendHeader("Content-Disposition",
            string.Format("{0}; filename={1}.{2}", disposition, fileName, fileFormat));
        Page.Response.Charset = System.Text.Encoding.UTF8.WebName;
        Page.Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Page.Response.ContentEncoding = System.Text.Encoding.UTF8;
        Page.Response.BinaryWrite(stream.GetBuffer());
        Page.Response.End();
    }

  
    #endregion
    protected void btnXlsExport2_Click(object sender, EventArgs e)
    {
        BindData();
        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;


        gridExport.GridViewID = "gvw";
        gridExport.PageHeader.Center = "รายงานรถที่ใกล้หมดสภาพการใช้งาน";
        gridExport.PageHeader.Font.Bold = true;
        //gridExport.ExportToXls("File.xls", options);
        gridExport.WriteXlsToResponse("ReportExpireResourceMonitoring", options);
        //gridExport.WriteXlsToResponse();
    }



    public static void WriteResponse(HttpResponse response, byte[] filearray, string type)
    {
        response.ClearContent();
        response.Buffer = true;
        response.Cache.SetCacheability(HttpCacheability.Private);
        response.ContentType = "application/pdf";
        ContentDisposition contentDisposition = new ContentDisposition();
        contentDisposition.FileName = "test.pdf";
        contentDisposition.DispositionType = type;
        response.AddHeader("Content-Disposition", contentDisposition.ToString());
        response.BinaryWrite(filearray);
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        try
        {
            response.End();
        }
        catch (System.Threading.ThreadAbortException)
        {
        }

    }

}