﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.IO;
using System.Globalization;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using EmailHelper;
using Utility;
using TMS_BLL.ObjectDataSource;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Threading;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

public partial class admin_Complain_add_New : PageBase
{
    #region + ViewState +
    private bool IsSpecialDoc
    {
        get
        {
            if ((bool)ViewState["IsSpecialDoc"] != null)
                return (bool)ViewState["IsSpecialDoc"];
            else
                return false;
        }
        set
        {
            ViewState["IsSpecialDoc"] = value;
        }
    }

    private int SumCar
    {
        get
        {
            if ((int)ViewState["SumCar"] != null)
                return (int)ViewState["SumCar"];
            else
                return 0;
        }
        set
        {
            ViewState["SumCar"] = value;
        }
    }

    private bool IsCheckTab3Click
    {
        get
        {
            if ((bool)ViewState["IsCheckTab3Click"] != null)
                return (bool)ViewState["IsCheckTab3Click"];
            else
                return false;
        }
        set
        {
            ViewState["IsCheckTab3Click"] = value;
        }
    }

    private bool IsSendToVendor
    {
        get
        {
            if ((bool)ViewState["IsSendToVendor"] != null)
                return (bool)ViewState["IsSendToVendor"];
            else
                return false;
        }
        set
        {
            ViewState["IsSendToVendor"] = value;
        }
    }

    private bool IsTruckOutContract
    {
        get
        {
            if ((bool)ViewState["IsTruckOutContract"] != null)
                return (bool)ViewState["IsTruckOutContract"];
            else
                return false;
        }
        set
        {
            ViewState["IsTruckOutContract"] = value;
        }
    }

    private int IndexTruckOutContract
    {
        get
        {
            if ((int)ViewState["IndexTruckOutContract"] != null)
                return (int)ViewState["IndexTruckOutContract"];
            else
                return 0;
        }
        set
        {
            ViewState["IndexTruckOutContract"] = value;
        }
    }

    private int DocStatusID
    {
        get
        {
            if ((int)ViewState["DocStatusID"] != null)
                return (int)ViewState["DocStatusID"];
            else
                return 0;
        }
        set
        {
            ViewState["DocStatusID"] = value;
        }
    }


    private string Detail
    {
        get
        {
            if ((string)ViewState["Detail"] != null)
                return (string)ViewState["Detail"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["Detail"] = value;
        }
    }
    private string CancelDoc
    {
        get
        {
            if ((string)ViewState["CancelDoc"] != null)
                return (string)ViewState["CancelDoc"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CancelDoc"] = value;
        }
    }

    private string OTP
    {
        get
        {
            if ((string)ViewState["OTP"] != null)
                return (string)ViewState["OTP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["OTP"] = value;
        }
    }

    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }
    private DataTable dtAddDriver
    {
        get
        {
            if ((DataTable)ViewState["dtAddDriver"] != null)
                return (DataTable)ViewState["dtAddDriver"];
            else
                return null;
        }
        set
        {
            ViewState["dtAddDriver"] = value;
        }
    }

    private DataTable dtSuspendDriver
    {
        get
        {
            if ((DataTable)ViewState["dtSuspendDriver"] != null)
                return (DataTable)ViewState["dtSuspendDriver"];
            else
                return null;
        }
        set
        {
            ViewState["dtSuspendDriver"] = value;
        }
    }

    private DataTable dtHistoryDriver
    {
        get
        {
            if ((DataTable)ViewState["dtHistoryDriver"] != null)
                return (DataTable)ViewState["dtHistoryDriver"];
            else
                return null;
        }
        set
        {
            ViewState["dtHistoryDriver"] = value;
        }
    }

    private DataTable dtDup
    {
        get
        {
            if ((DataTable)ViewState["dtDup"] != null)
                return (DataTable)ViewState["dtDup"];
            else
                return null;
        }
        set
        {
            ViewState["dtDup"] = value;
        }
    }

    private DataTable dtEmployee
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee"] != null)
                return (DataTable)ViewState["dtEmployee"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtEmployee"] = value;
        }
    }

    private DataTable dtEmployee2
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee2"] != null)
                return (DataTable)ViewState["dtEmployee2"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtEmployee2"] = value;
        }
    }

    private DataTable dtEmployee3
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee3"] != null)
                return (DataTable)ViewState["dtEmployee3"];
            else
                return null;
        }
        set
        {
            ViewState["dtEmployee3"] = value;
        }
    }

    private DataTable dtEmployee4
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee4"] != null)
                return (DataTable)ViewState["dtEmployee4"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtEmployee4"] = value;
        }
    }

    private DataTable dtDriverSearch
    {
        get
        {
            if ((DataTable)ViewState["dtDriverSearch"] != null)
                return (DataTable)ViewState["dtDriverSearch"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtDriverSearch"] = value;
        }
    }

    private DataTable dtCarSearch
    {
        get
        {
            if ((DataTable)ViewState["dtCarSearch"] != null)
                return (DataTable)ViewState["dtCarSearch"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtCarSearch"] = value;
        }
    }

    private DataTable dtDeliveryNoSearch
    {
        get
        {
            if ((DataTable)ViewState["dtDeliveryNoSearch"] != null)
                return (DataTable)ViewState["dtDeliveryNoSearch"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtDeliveryNoSearch"] = value;
        }
    }

    private DataTable dtDeliveryNo
    {
        get
        {
            if ((DataTable)ViewState["dtDeliveryNo"] != null)
                return (DataTable)ViewState["dtDeliveryNo"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtDeliveryNo"] = value;
        }
    }

    private DataTable dtDeliveryNoAll
    {
        get
        {
            if ((DataTable)ViewState["dtDeliveryNoAll"] != null)
                return (DataTable)ViewState["dtDeliveryNoAll"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtDeliveryNoAll"] = value;
        }
    }

    private DataTable dtCusScoreType
    {
        get
        {
            if ((DataTable)ViewState["dtCusScoreType"] != null)
                return (DataTable)ViewState["dtCusScoreType"];
            else
                return null;
        }
        set
        {
            ViewState["dtCusScoreType"] = value;
        }
    }

    private DataTable dtCarDistinct
    {
        get
        {
            if ((DataTable)ViewState["dtCarDistinct"] != null)
                return (DataTable)ViewState["dtCarDistinct"];
            else
                return null;
        }
        set
        {
            ViewState["dtCarDistinct"] = value;
        }
    }

    private DataTable dtScoreList
    {
        get
        {
            if ((DataTable)ViewState["dtScoreList"] != null)
                return (DataTable)ViewState["dtScoreList"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreList"] = value;
        }
    }

    private DataTable dtScoreListCar
    {
        get
        {
            if ((DataTable)ViewState["dtScoreListCar"] != null)
                return (DataTable)ViewState["dtScoreListCar"];
            else
                return null;
        }
        set
        {
            ViewState["dtScoreListCar"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }
    private DataTable dtUploadType2
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType2"] != null)
                return (DataTable)ViewState["dtUploadType2"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType2"] = value;
        }
    }
    private DataTable dtUploadReqDoc
    {
        get
        {
            if ((DataTable)ViewState["dtUploadReqDoc"] != null)
                return (DataTable)ViewState["dtUploadReqDoc"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadReqDoc"] = value;
        }
    }

    private DataTable dtUploadTypeTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTab3"] != null)
                return (DataTable)ViewState["dtUploadTypeTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTab3"] = value;
        }
    }

    private DataTable dtUploadTypeCost
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeCost"] != null)
                return (DataTable)ViewState["dtUploadTypeCost"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeCost"] = value;
        }
    }

    private DataTable dtUploadCost
    {
        get
        {
            if ((DataTable)ViewState["dtUploadCost"] != null)
                return (DataTable)ViewState["dtUploadCost"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadCost"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtUpload2
    {
        get
        {
            if ((DataTable)ViewState["dtUpload2"] != null)
                return (DataTable)ViewState["dtUpload2"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload2"] = value;
        }
    }

    private DataTable dtUploadFlag
    {
        get
        {
            if ((DataTable)ViewState["dtUploadFlag"] != null)
                return (DataTable)ViewState["dtUploadFlag"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadFlag"] = value;
        }
    }

    private DataTable dtUploadTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTab3"] != null)
                return (DataTable)ViewState["dtUploadTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTab3"] = value;
        }
    }

    private DataTable dtScore
    {
        get
        {
            if ((DataTable)ViewState["dtScore"] != null)
                return (DataTable)ViewState["dtScore"];
            else
                return null;
        }
        set
        {
            ViewState["dtScore"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }

    private DataTable dtRequestFile2
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile2"] != null)
                return (DataTable)ViewState["dtRequestFile2"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile2"] = value;
        }
    }
    private DataTable dtHeader
    {
        get
        {
            if ((DataTable)ViewState["dtHeader"] != null)
                return (DataTable)ViewState["dtHeader"];
            else
                return null;
        }
        set
        {
            ViewState["dtHeader"] = value;
        }
    }

    private DataTable dtTopic
    {
        get
        {
            if ((DataTable)ViewState["dtTopic"] != null)
                return (DataTable)ViewState["dtTopic"];
            else
                return null;
        }
        set
        {
            ViewState["dtTopic"] = value;
        }
    }

    private DataTable dtComplainType
    {
        get
        {
            if ((DataTable)ViewState["dtComplainType"] != null)
                return (DataTable)ViewState["dtComplainType"];
            else
                return null;
        }
        set
        {
            ViewState["dtComplainType"] = value;
        }
    }

    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }

    private DataTable dtRequire
    {
        get
        {
            if ((DataTable)ViewState["dtRequire"] != null)
                return (DataTable)ViewState["dtRequire"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequire"] = value;
        }
    }

    private string ServiceIDEdit
    {
        get
        {
            if ((string)ViewState["ServiceIDEdit"] != null)
                return (string)ViewState["ServiceIDEdit"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ServiceIDEdit"] = value;
        }
    }

    private int IndexEdit
    {
        get
        {
            if ((int)ViewState["IndexEdit"] != null)
                return (int)ViewState["IndexEdit"];
            else
                return -1;
        }
        set
        {
            ViewState["IndexEdit"] = value;
        }
    }

    private DataTable dtTruck
    {
        get
        {
            if ((DataTable)ViewState["dtTruck"] != null)
                return (DataTable)ViewState["dtTruck"];
            else
                return null;
        }
        set
        {
            ViewState["dtTruck"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        this.CheckPostBack();

        if (!IsPostBack)
        {
            this.InitialForm();

            IsSpecialDoc = false;
            this.CheckSpecialDocument();
        }
        this.EnableTab();

        if (dtUpload.Rows.Count > 0 && dtUploadReqDoc.Rows.Count > 0)
        {
            this.ValidateSaveTab2();
            this.ValidateSaveTab2ReqDoc();
            cmdSaveTab2.Enabled = true;
        }

        if (dtData != null)
            if (dtData.Rows.Count > 0)
                for (int i = 0; i < dtData.Rows.Count; i++)
                    if (!string.Equals(dtData.Rows[i]["DOC_STATUS_ID"].ToString(), "1") && !string.Equals(dtData.Rows[i]["DOC_STATUS_ID"].ToString(), "2") && !string.Equals(dtData.Rows[i]["DOC_STATUS_ID"].ToString(), "7"))
                    { cmdSaveTab2.Enabled = false; cmdSaveTab2Draft.Enabled = false; cmdRequestDoc.Enabled = false; }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearchByCarLicense.Enabled = false;
                btnSearchLicenseAll.Enabled = false;
                btnSearchLicenseReset.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdAddHeader.Enabled = false;
                cmdAddHeader.CssClass = "btn btn-md bth-hover btn-info";
                cmdAddScore.Enabled = false;
                cmdSaveTab1.Enabled = false;
                cmdCancelDoc.Enabled = false;

                cmdSaveTab2.Enabled = false;
                cmdSaveTab2Draft.Enabled = false;
                cmdUploadCost.Enabled = false;
                cmdSaveIsCost.Enabled = false;

                cmdSaveTab3.Enabled = false;
                cmdSaveTab3Draft.Enabled = false;
                cmdChangeStatus.Enabled = false;
                cmdUploadTab3.Enabled = false;

                cmdAddHeader.Enabled = false;
                cmdAdminChangeStatus.Enabled = false;
                cmdUpload.Enabled = false;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }
    private void CheckSpecialDocument()
    {
        try
        {
            string sqlCon = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
            DataTable dt = CommonFunction.Get_Data(sqlCon, "SELECT DOC_ID FROM T_DOC_SPECIAL");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (string.Equals(txtDocID.Value.Trim(), dt.Rows[i]["DOC_ID"].ToString()) && !string.Equals(txtDocID.Value.Trim(), "Generate by System"))
                {
                    cmdSaveTab1.Enabled = true;
                    cmdSaveTab2.Enabled = true;
                    IsSpecialDoc = true;
                    break;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void CheckPostBack()
    {
        try
        {
            var ctrlName = Request.Params[Page.postEventSourceID];
            var args = Request.Params[Page.postEventArgumentID];

            if (ctrlName == txtScore1.UniqueID && args == "txtScore1_OnKeyPress")
                txtScore1_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore2.UniqueID && args == "txtScore2_OnKeyPress")
                txtScore2_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore3.UniqueID && args == "txtScore3_OnKeyPress")
                txtScore3_OnKeyPress(ctrlName, args);

            if (ctrlName == txtScore4.UniqueID && args == "txtScore4_OnKeyPress")
                txtScore4_OnKeyPress(ctrlName, args);

            if (ctrlName == txtZealQty.UniqueID && args == "txtZealQty_OnKeyPress")
                txtZealQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilQty.UniqueID && args == "txtOilQty_OnKeyPress")
                txtOilQty_OnKeyPress(ctrlName, args);

            if (ctrlName == txtOilPrice.UniqueID && args == "txtDays_OnKeyPress")
                txtOilPrice_OnKeyPress(ctrlName, args);

            foreach (GridViewRow item in dgvSuspendDriver.Rows)
            {
                TextBox txtDays = (TextBox)item.FindControl("txtDays");
                if (ctrlName == txtDays.UniqueID && args == "txtDays_OnKeyPress")
                    txtDays_OnKeyPress(txtDays, args, item.RowIndex);
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void InitialCreateDoc()
    {
        try
        {
            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            txtFName.Text = dtLogin.Rows[0]["SFULLNAME"].ToString();
            txtFNameDivision.Text = dtLogin.Rows[0]["SUNITNAME"].ToString();

            txtComplainBy.Value = dtLogin.Rows[0]["SFULLNAME"].ToString();
            txtComplainDivisionBy.Value = dtLogin.Rows[0]["SUNITNAME"].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LoadWarehouse()
    {
        try
        {
            DataTable dtWarehouse = new DataTable();
            dtWarehouse = WarehouseBLL.Instance.WarehouseSelectBLL();
            DropDownListHelper.BindDropDownList(ref cbxOrganiz, dtWarehouse, "STERMINALID", "STERMINALNAME", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LoadContract(string Condition)
    {
        try
        {
            DataTable dtContract = new DataTable();
            dtContract = ComplainBLL.Instance.ContractSelectBLL(Condition);
            DropDownListHelper.BindDropDownList(ref cboContract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LoadVendor()
    {
        try
        {
            DataTable dtVendor = new DataTable();
            if (!String.IsNullOrEmpty(strHeadRegistNo))
            {
                DropDownListHelper.BindDropDownList(ref cboVendor, ComplainBLL.Instance.VendorSelectByHeadRegisterNoBLL(strHeadRegistNo), "SVENDORID", "SABBREVIATION", true);
                if (cboVendor.Items.Count < 1)
                {
                    throw new Exception("ไม่พบข้อมูล บริษัทผู้ประกอบการขนส่งจากเลขทะเบียนรถที่เลือก");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogHEADREGISTERNOSearch", "$('#dialogHEADREGISTERNOSearch').modal('hide');", true);
                }
            }
            else
            {
                dtVendor = ComplainBLL.Instance.VendorSelectBLL();
                DropDownListHelper.BindDropDownList(ref cboVendor, dtVendor, "SVENDORID", "SABBREVIATION", true);
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LoadEmployee()
    {
        try
        {
            dtEmployee = ComplainBLL.Instance.EmployeeSelectBLL();
            dtEmployee2 = ComplainBLL.Instance.EmployeeSelectBLL();
            dtEmployee3 = ComplainBLL.Instance.EmployeeSelectBLL();
            dtEmployee4 = ComplainBLL.Instance.EmployeeSelectBLL();
            DropDownListHelper.BindDropDownList(ref cmbPersonalNo, dtEmployee, "SPERSONELNO", "FULLNAME", true);
            DropDownListHelper.BindDropDownList(ref cmbPersonalNo2, dtEmployee2, "SPERSONELNO", "FULLNAME", true);
            DropDownListHelper.BindDropDownList(ref cmbPersonalNo3, dtEmployee3, "SPERSONELNO", "FULLNAME", true);
            DropDownListHelper.BindDropDownList(ref cmbPersonalNo4, dtEmployee4, "SPERSONELNO", "FULLNAME", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void InitialForm()
    {
        try
        {
            ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = false;
            ((HtmlAnchor)this.FindControlRecursive(Page, "ChangeStatusTab")).Visible = false;

            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["DocID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                DocID = decryptedValue;
            }

            dtRequestFile = new DataTable();
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            dtRequestFile2 = new DataTable();
            GridViewHelper.BindGridView(ref grvRequestReqDoc, dtRequestFile2);

            txtDateComplain.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
            txtCreateDate.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", DateTimeFormatInfo.CurrentInfo);

            this.InitialCreateDoc();
            this.LoadTopic();
            this.LoadComplainType();
            this.InitialUpload();
            this.InitialHeader();
            this.LoadVendor();
            this.LoadWarehouse();
            this.LoadData();

            IndexEdit = -1;

            if (dtHeader.Rows.Count == 0)
                txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
            else
                txtDateTrans.Text = (dtHeader.Rows[0]["DELIVERYDATE"] != null) ? dtHeader.Rows[0]["DELIVERYDATE"].ToString() : string.Empty;

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void EnableTab()
    {
        if ((DocID != null) && (!string.Equals(DocID, string.Empty)))
        {
            ((HtmlAnchor)this.FindControlRecursive(Page, "GeneralTab")).Visible = true;

            this.CheckCanEdit();
        }
        else
        {//New Document
            cmdSaveTab1.Enabled = true;
            //cmdSaveTab2.Enabled = true;
            //cmdSaveTab2Draft.Enabled = true;

            ((HtmlAnchor)this.FindControlRecursive(Page, "GeneralTab")).Visible = true;
            ((HtmlAnchor)this.FindControlRecursive(this, "GeneralTab")).Visible = true;
        }

        if (string.Equals(CancelDoc, "Y"))
            this.DisplayCancelDoc();
    }

    private void EnableSaveTab3(bool IsSave)
    {
        cmdSaveTab3.Enabled = IsSave;
        cmdSaveTab3Draft.Enabled = IsSave;
        cmdChangeStatus.Enabled = IsSave;
        cmdSaveIsCost.Enabled = IsSave;
    }

    private void EnableTab3()
    {
        try
        {
            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            //ถ้าเป็น รข หรือเจ้าหน้าที่ ปตท ให้เปิด Tab3
            if ((string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode) || string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup2.ToString()) || string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString())) && (DocStatusID == ConfigValue.DocStatus3 || DocStatusID == ConfigValue.DocStatus4 || DocStatusID == ConfigValue.DocStatus5 || DocStatusID == ConfigValue.DocStatus6 || DocStatusID == ConfigValue.DocStatus7 || DocStatusID == ConfigValue.DocStatus10 || DocStatusID == ConfigValue.DocStatus11))
            {
                txtRemarkTab3.Text = dtData.Rows[0]["REMARK_TAB3"].ToString();
                lblCusScoreHeader.Text = string.Format(lblCusScoreHeader.Text, dtHeader.Rows[0]["CONTRACTNAME"].ToString());

                ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = true;
                if (string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode))
                    this.EnableSaveTab3(true);
                else
                    this.EnableSaveTab3(false);

                DataTable dtSentencer = ComplainBLL.Instance.SentencerSelectBLL(1);
                DropDownListHelper.BindDropDownList(ref ddlSentencer, dtSentencer, "SSENTENCERCODE", "SSENTENCERNAME", true);
                if (!string.Equals(dtHeader.Rows[0]["SSENTENCERCODE"].ToString(), string.Empty))
                {
                    if (string.Equals(dtHeader.Rows[0]["SSENTENCERCODE"].ToString(), "0"))
                    {
                        radSentencerType.SelectedValue = "0";
                        ddlSentencer.SelectedIndex = 0;
                        ddlSentencer.Enabled = false;
                    }
                    else
                    {
                        radSentencerType.SelectedValue = "1";
                        ddlSentencer.Enabled = true;
                        ddlSentencer.SelectedValue = dtHeader.Rows[0]["SSENTENCERCODE"].ToString();
                    }
                }

                dtScore = ComplainBLL.Instance.ScoreSelectBLL();
                DropDownListHelper.BindDropDownList(ref cboScore, dtScore, "STOPICID", "STOPICNAME", true);
                this.SelectDuplicateCar();

                dtScoreList = new DataTable();
                dtScoreList.Columns.Add("STOPICID");
                dtScoreList.Columns.Add("STOPICNAME");
                dtScoreList.Columns.Add("Cost");
                dtScoreList.Columns.Add("CostDisplay");
                dtScoreList.Columns.Add("SuspendDriver");
                dtScoreList.Columns.Add("DisableDriver");
                dtScoreList.Columns.Add("DisableDriverDisplay");
                dtScoreList.Columns.Add("Score1");
                dtScoreList.Columns.Add("Score2");
                dtScoreList.Columns.Add("Score3");
                dtScoreList.Columns.Add("Score4");
                dtScoreList.Columns.Add("Score5");
                dtScoreList.Columns.Add("Score6");
                dtScoreList.Columns.Add("Score6Display");
                dtScoreList.Columns.Add("TotalCar");
                dtScoreList.Columns.Add("Blacklist");

                dtScoreListCar = new DataTable();
                dtScoreListCar.Columns.Add("STOPICID");
                dtScoreListCar.Columns.Add("TRUCKID");
                dtScoreListCar.Columns.Add("CARHEAD");
                dtScoreListCar.Columns.Add("CARDETAIL");
                dtScoreListCar.Columns.Add("DELIVERY_DATE");
                dtScoreListCar.Columns.Add("TOTAL_CAR");

                dtUploadTab3 = new DataTable();
                dtUploadTab3.Columns.Add("UPLOAD_ID");
                dtUploadTab3.Columns.Add("UPLOAD_NAME");
                dtUploadTab3.Columns.Add("FILENAME_SYSTEM");
                dtUploadTab3.Columns.Add("FILENAME_USER");
                dtUploadTab3.Columns.Add("FULLPATH");

                dtUploadTypeCost = new DataTable();
                dtUploadTypeCost.Columns.Add("UPLOAD_ID");
                dtUploadTypeCost.Columns.Add("UPLOAD_NAME");
                dtUploadTypeCost.Columns.Add("FILENAME_SYSTEM");
                dtUploadTypeCost.Columns.Add("FILENAME_USER");
                dtUploadTypeCost.Columns.Add("FULLPATH");

                DropDownListHelper.BindDropDownList(ref ddlScore1, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore2, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore3, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);
                DropDownListHelper.BindDropDownList(ref ddlScore4, ComplainBLL.Instance.CusScoreSelectBLL(), "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME", true);

                dtUploadTypeTab3 = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN_SCORE");
                DropDownListHelper.BindDropDownList(ref cboUploadTypeTab3, dtUploadTypeTab3, "UPLOAD_ID", "UPLOAD_NAME", true);

                dtUploadTypeCost = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN_COST");
                DropDownListHelper.BindDropDownList(ref cboUploadCost, dtUploadTypeCost, "UPLOAD_ID", "UPLOAD_NAME", true);

                dtUploadCost = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_COST", string.Empty);
                GridViewHelper.BindGridView(ref dgvUploadCost, dtUploadCost);

                int sum = 0;
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    if (!string.Equals(dtHeader.Rows[i]["TotalCar"].ToString(), string.Empty) && int.Parse(dtHeader.Rows[i]["TotalCar"].ToString()) > 0)
                        sum += int.Parse(dtHeader.Rows[i]["TotalCar"].ToString());
                }

                if (sum > 0)
                {
                    //SumCar = sum;
                    //rowTotalCarTab3.Visible = true;
                    //txtTotalCarTab3.Text = SumCar.ToString();
                    rowListTotalCarTab3.Visible = true;

                    dtDup = ComplainBLL.Instance.SelectDuplicateTotalCarBLL(DocID);
                    GridViewHelper.BindGridView(ref dgvScoreTotalCar, dtDup);
                }
                else
                {
                    rowListCarTab3.Visible = true;
                }

                dtCusScoreType = ComplainBLL.Instance.CusScoreSelectBLL();
                DropDownListHelper.BindRadioButton(ref radCusScoreType, dtCusScoreType, "CUSSCORE_TYPE_ID", "CUSSCORE_TYPE_NAME");

                if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                    dtUploadTab3 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_SCORE", " AND IS_VENDOR_DOWNLOAD = '1'");
                else
                    dtUploadTab3 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_SCORE", string.Empty);

                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);

                dtScoreList = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreDetailBLL(DocID);
                dtScoreListCar = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreCarBLL(DocID);

                dtScoreList.Columns["COST"].ColumnName = "Cost";
                dtScoreList.Columns["COSTDISPLAY"].ColumnName = "CostDisplay";
                dtScoreList.Columns["SUSPENDDRIVER"].ColumnName = "SuspendDriver";
                dtScoreList.Columns["DISABLEDRIVER"].ColumnName = "DisableDriver";
                dtScoreList.Columns["DISABLEDRIVERDISPLAY"].ColumnName = "DisableDriverDisplay";
                dtScoreList.Columns["SCORE1"].ColumnName = "Score1";
                dtScoreList.Columns["SCORE2"].ColumnName = "Score2";
                dtScoreList.Columns["SCORE3"].ColumnName = "Score3";
                dtScoreList.Columns["SCORE4"].ColumnName = "Score4";
                dtScoreList.Columns["SCORE5"].ColumnName = "Score5";
                dtScoreList.Columns["SCORE6"].ColumnName = "Score6";
                dtScoreList.Columns["SCORE6DISPLAY"].ColumnName = "Score6Display";
                dtScoreList.Columns["TOTALCAR"].ColumnName = "TotalCar";
                dtScoreList.Columns["BLACKLIST"].ColumnName = "Blacklist";

                GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);

                DataTable dt = new DataTable();
                dt = ComplainImportFileBLL.Instance.ComplainSelectTab3ScoreBLL(DocID);
                if (dt.Rows.Count > 0)
                {
                    if (!string.Equals(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString(), "-1"))
                    {
                        radCusScoreType.SelectedValue = dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString();

                        txtPointFinal.Enabled = false;
                        txtCostFinal.Enabled = false;
                        txtDisableFinal.Enabled = false;

                        lblShowSumPoint.Text = "(Sum : {0})";
                        lblShowSumCost.Text = "(Sum : {0})";
                        lblShowSumDisable.Text = "(Sum : {0})";

                        lblShowSumPoint.Visible = false;
                        lblShowSumCost.Visible = false;
                        lblShowSumDisable.Visible = false;

                        int row = this.GetSelectRow(dt.Rows[0]["CUSSCORE_TYPE_ID"].ToString());

                        string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                        switch (CusType)
                        {
                            //case "MAX": this.FindMax(); break;
                            //case "SUM": this.FindSum("Normal"); break;
                            case "FREETEXT":
                                this.FindSum(string.Empty);

                                txtPointFinal.Enabled = true;
                                txtCostFinal.Enabled = true;
                                txtDisableFinal.Enabled = true;

                                lblShowSumPoint.Visible = true;
                                lblShowSumCost.Visible = true;
                                lblShowSumDisable.Visible = true; break;
                            //this.FreeText(); break;
                            default:
                                break;
                        }

                        radCostCheck.SelectedValue = dt.Rows[0]["COST_CHECK"].ToString();
                        txtCostCheckDate.Text = (dt.Rows[0]["COST_CHECK_DATE"] != null) ? dt.Rows[0]["COST_CHECK_DATE"].ToString() : string.Empty;
                        txtCostOther.Text = dt.Rows[0]["COST_OTHER"].ToString();
                        txtOilLose.Text = dt.Rows[0]["OIL_LOSE"].ToString();

                        this.CheckBlacklist();
                        if (string.Equals(dt.Rows[0]["BLACKLIST"].ToString(), "1"))
                        {
                            chkBlacklist.Checked = true;
                            chkBlacklist.Enabled = true;
                        }
                    }

                    if (!string.Equals(dt.Rows[0]["TOTAL_POINT"].ToString(), "-1"))
                        txtPointFinal.Text = dt.Rows[0]["TOTAL_POINT"].ToString();

                    if (!string.Equals(dt.Rows[0]["TOTAL_COST"].ToString(), "-1"))
                        txtCostFinal.Text = dt.Rows[0]["TOTAL_COST"].ToString();

                    if (!string.Equals(dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString(), "-1"))
                        txtDisableFinal.Text = dt.Rows[0]["TOTAL_DISABLE_DRIVER"].ToString();

                    if (string.Equals(DocStatusID, ConfigValue.DocStatus5) || string.Equals(DocStatusID, ConfigValue.DocStatus6) || string.Equals(DocStatusID, ConfigValue.DocStatus7) || string.Equals(DocStatusID, ConfigValue.DocStatus8) || string.Equals(DocStatusID, ConfigValue.DocStatus9))
                        this.NotSaveTab3();
                }

                if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup2) && !string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode))
                {//เจ้าหน้าที่ ปตท ไม่ใช่ รข.
                    this.NotSaveTab3();
                    cmdSaveIsCost.Visible = false;
                }

                if (ConfigValue.ComplainAdminUser.Contains(dtLogin.Rows[0]["SUSERNAME"].ToString()))
                {//Super Admin
                    ((HtmlAnchor)this.FindControlRecursive(Page, "ChangeStatusTab")).Visible = true;
                    cmdAdminChangeStatus.Enabled = true;

                    txtDocIDTab4.Value = txtDocID.Value;
                    this.LoadDocStatus();
                }

                this.CheckBlacklist();
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void NotSaveTab3()
    {
        cmdSaveTab3.Enabled = false;
        cmdSaveTab3Draft.Enabled = false;
        cmdChangeStatus.Enabled = false;
        cmdAdminChangeStatus.Enabled = false;
    }

    private void LoadData()
    {
        try
        {
            if ((DocID != null) && (!string.Equals(DocID, string.Empty)))
            {// Edit Complain
                dtData = ComplainBLL.Instance.ComplainSelectBLL(" AND c.DOC_ID = '" + DocID + "'");
                dtHeader = ComplainBLL.Instance.ComplainSelectHeaderBLL(" AND TCOMPLAIN.DOC_ID = '" + DocID + "'");
                dtAddDriver = ComplainBLL.Instance.ComplainSelectAddDriverBLL(" AND DOC_ID = '" + DocID + "'");

                OTP = dtData.Rows[0]["OTP_CODE"].ToString();

                int Index = 0;
                cmbTopic.SelectedValue = dtHeader.Rows[Index]["TOPICID"].ToString();
                cmbTopic_SelectedIndexChanged(null, null);

                cboComplainType.SelectedValue = dtHeader.Rows[Index]["COMPLAINID"].ToString();
                cboComplainType_SelectedIndexChanged(null, null);
                cbxOrganiz.SelectedValue = dtHeader.Rows[Index]["ORGANIZID"].ToString();
                chkLock.Checked = (string.Equals(dtHeader.Rows[Index]["LOCKDRIVERID"].ToString(), "0") ? false : true);
                rblTypeProduct.Value = dtHeader.Rows[Index]["TYPEPRODUCTID"].ToString();
                txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

                cboVendor.SelectedValue = dtHeader.Rows[Index]["VENDORID"].ToString();
                cboVendor_SelectedIndexChanged(null, null);

                if (!string.Equals(dtHeader.Rows[Index]["CONTRACTID"].ToString(), string.Empty))
                {
                    cboContract.SelectedValue = dtHeader.Rows[Index]["CONTRACTID"].ToString();
                    cboContract_SelectedIndexChanged(null, null);
                }
                if (!string.Equals(dtHeader.Rows[Index]["TRUCKID"].ToString(), string.Empty))
                {
                    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                    if (string.Equals(dtHeader.Rows[Index]["CARDETAIL"].ToString(), string.Empty))
                        cboHeadRegist_SelectedIndexChanged(null, null);
                }

                try
                {
                    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                    if (!string.Equals(dtHeader.Rows[Index]["TRUCKID"].ToString(), string.Empty) && (string.Equals(cboHeadRegist.SelectedValue, string.Empty)))
                    {
                        chkTruckOutContract.Checked = true;
                        this.TruckSelect();
                        cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                    }
                }
                catch
                {
                    chkTruckOutContract.Checked = true;
                    this.TruckSelect();
                    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                }
                finally
                {
                    if (string.Equals(dtHeader.Rows[Index]["CARDETAIL"].ToString(), string.Empty))
                        cboHeadRegist_SelectedIndexChanged(null, null);
                }

                if (!string.Equals(dtHeader.Rows[Index]["DELIVERYID"].ToString(), string.Empty))
                    cboDelivery.SelectedValue = dtHeader.Rows[Index]["DELIVERYID"].ToString();

                cmbPersonalNo.SelectedValue = dtHeader.Rows[Index]["PERSONALID"].ToString();

                if (dtAddDriver.Rows.Count > 0)
                {
                    if (dtAddDriver.Rows.Count > 0)
                        cmbPersonalNo2.SelectedValue = dtAddDriver.Rows[0]["SDRIVERNO"].ToString();
                    if (dtAddDriver.Rows.Count > 1)
                        cmbPersonalNo3.SelectedValue = dtAddDriver.Rows[1]["SDRIVERNO"].ToString();
                    if (dtAddDriver.Rows.Count > 2)
                        cmbPersonalNo4.SelectedValue = dtAddDriver.Rows[2]["SDRIVERNO"].ToString();
                }

                txtDateTrans.Text = (dtHeader.Rows[Index]["DELIVERYDATE"] != null) ? dtHeader.Rows[Index]["DELIVERYDATE"].ToString() : string.Empty;
                //teTimeTrans.Text = dtHeader.Rows[Index]["DELIVERYTIME"].ToString();
                txtComplainAddress.Value = dtHeader.Rows[Index]["COMPLAINADDRESS"].ToString();
                txtTotalCar.Text = dtHeader.Rows[Index]["TOTALCAR"].ToString();
                txtShipTo.Text = dtHeader.Rows[Index]["WAREHOUSE_TO"].ToString();
                txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

                this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));

                if (dtData.Rows.Count > 0)
                {
                    DocStatusID = int.Parse(dtData.Rows[0]["DOC_STATUS_ID"].ToString());

                    txtDocID.Value = dtData.Rows[0]["DOC_ID"].ToString();
                    txtComplainBy.Value = dtData.Rows[0]["FULLNAME"].ToString();
                    txtComplainDivisionBy.Value = dtData.Rows[0]["SUNITNAME"].ToString();
                    txtDateComplain.Text = (dtData.Rows[0]["DDATECOMPLAIN"] != null) ? dtData.Rows[0]["DDATECOMPLAIN"].ToString() : string.Empty;
                    txtDetail.Value = dtData.Rows[0]["SDETAIL"] + "";

                    txtOperate.Text = dtData.Rows[0]["SSOLUTION"].ToString();
                    txtProtect.Text = dtData.Rows[0]["SPROTECT"].ToString();
                    txtsEmployee.Text = dtData.Rows[0]["SRESPONSIBLE"].ToString();

                    txtCCEmail.Text = dtData.Rows[0]["CC_EMAIL"].ToString();

                    txtCreateDate.Text = dtData.Rows[0]["CREATE_DATE"].ToString();

                    txtFName.Text = dtData.Rows[0]["SCOMPLAINFNAME"].ToString();
                    txtFNameDivision.Text = dtData.Rows[0]["SCOMPLAINDIVISION"].ToString();

                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                        dtHeader.Rows[i]["DELIVERYDATE"] = (dtHeader.Rows[i]["DELIVERYDATE"] != null) ? dtHeader.Rows[i]["DELIVERYDATE"].ToString() : string.Empty;

                    GridViewHelper.BindGridView(ref dgvHeader, dtHeader);

                    int getCoumn = 0;
                    getCoumn = findGridColumnByCSS(ref dgvHeader, "xxSearch");

                    dtUpload = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN", string.Empty);
                    GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
                    dtUploadFlag = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN", string.Empty);
                    GridViewHelper.BindGridView(ref dgvUploadFile_flag, dtUploadFlag, false);
                    dtUploadReqDoc = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_REQ", string.Empty);
                    GridViewHelper.BindGridView(ref dgvUploadReqDoc, dtUploadReqDoc, false);
                    if (dtUploadReqDoc.Rows.Count > 0)
                        for (int i = 0; i < dtUploadReqDoc.Rows.Count; i++)
                            dtUpload2.Rows.Add(dtUploadReqDoc.Rows[i]["UPLOAD_ID"], dtUploadReqDoc.Rows[i]["UPLOAD_NAME"], dtUploadReqDoc.Rows[i]["FILENAME_SYSTEM"], dtUploadReqDoc.Rows[i]["FILENAME_USER"], dtUploadReqDoc.Rows[i]["FULLPATH"]);
                    dtSuspendDriver = ComplainBLL.Instance.ComplainSelectDriverBLL(" AND DOC_ID = '" + DocID + "'");
                    GridViewHelper.BindGridView(ref dgvSuspendDriver, dtSuspendDriver);
                    string AllName = string.Empty;
                    for (int i = 0; i < dtSuspendDriver.Rows.Count; i++)
                    {
                        if (AllName == string.Empty)
                            AllName = "'" + dtSuspendDriver.Rows[i]["FULLNAME"].ToString() + "'";
                        else
                            AllName = AllName + ",'" + dtSuspendDriver.Rows[i]["FULLNAME"].ToString() + "'";
                    }
                    if (string.Equals(AllName, string.Empty))
                        dtHistoryDriver = null;
                    else
                        dtHistoryDriver = ComplainBLL.Instance.ComplainSelectHistoryBLL(" AND TEMPLOYEE_SAP.FNAME||' '||TEMPLOYEE_SAP.LNAME IN (" + AllName + ") AND TCOMPLAIN.DOC_ID <> '" + DocID + "' AND TCOMPLAIN.DOC_STATUS_ID IN (1,2,3,7,9)");
                    GridViewHelper.BindGridView(ref dgvHistoryComplain, dtHistoryDriver);
                    DropDownListHelper.BindCheckBoxList(ref chkSuspainDriver, dtSuspendDriver, "SDRIVERNO", "FULLNAME");


                    this.InitialRequestFile();
                    this.CheckCanEdit();

                    CancelDoc = dtData.Rows[0]["DOC_CANCEL"].ToString();
                    this.EnableTab3();

                    if (DocStatusID == ConfigValue.DocStatus4)
                        chkSaveTab3Draft.Checked = true;

                    txtComplainDate.Text = dtData.Rows[0]["COMPLAIN_DATE"].ToString();
                    txtSecondDate.Text = dtData.Rows[0]["SECOUND_DATE"].ToString();
                }
            }
            else
            {//New Document
                cmdSaveTab1.Enabled = true;
                //cmdSaveTab2.Enabled = true;
                //cmdSaveTab2Draft.Enabled = true;
                ((HtmlAnchor)this.FindControlRecursive(Page, "GeneralTab")).Visible = false;
                ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ScreenForVendor()
    {
        try
        {
            //Tab1
            if (!string.Equals(DocStatusID + "", ConfigValue.DocStatus1 + "") && !string.Equals(DocStatusID + "", ConfigValue.DocStatus2 + "") && !string.Equals(DocStatusID + "", ConfigValue.DocStatus3 + "") && !string.Equals(DocStatusID + "", ConfigValue.DocStatus4 + ""))
                ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = true;
            else
                ((HtmlAnchor)this.FindControlRecursive(Page, "FinalTab")).Visible = false;
            lblHeaderTab1.Text = "ข้อมูลทั่วไป (หมายเลขเอกสาร " + txtDocID.Value + ")";
            txtFName.Text = "***********";
            txtComplainBy.Value = "***********";
            cmdAddHeader.Visible = false;
            cmdSaveTab1.Visible = false;
            cmdCancelDoc.Visible = false;
            lblShowSumPoint.Visible = false;
            lblShowSumCost.Visible = false;
            lblShowSumDisable.Visible = false;
            tblAddHeader.Visible = true;
            ImageButton imgButtonTab1;
            for (int i = 0; i < dgvHeader.Rows.Count; i++)
            {
                imgButtonTab1 = (ImageButton)dgvHeader.Rows[i].FindControl("imgEdit");
                if (imgButtonTab1 != null)
                    imgButtonTab1.Visible = false;

                imgButtonTab1 = (ImageButton)dgvHeader.Rows[i].FindControl("imgDeleteComplain");
                if (imgButtonTab1 != null)
                    imgButtonTab1.Visible = false;
            }

            //Tab2
            pnlPTT.Visible = false;
            pnl.Visible = false;
            txtsEmployee.Visible = false;
            txtCCEmail.Visible = false;
            lblMailDesc.Visible = false;
            lblEmployeeTab2.Visible = false;
            lblCCEmail.Visible = false;

            //Tab3
            Row1.Visible = false;
            Row2.Visible = false;
            Row3.Visible = false;
            Row4.Visible = false;
            Row5.Visible = false;
            Row6.Visible = false;
            Row7.Visible = false;
            Row8.Visible = false;
            Row9.Visible = false;
            Row10.Visible = false;
            lblCusScoreType.Visible = false;
            radCusScoreType.Visible = false;
            cmdAddScore.Visible = false;
            cmdSaveTab3.Visible = false;
            cmdSaveTab3Draft.Visible = false;
            cmdChangeStatus.Visible = false;
            divSecond.Visible = true;
            lblRemarkTab3.Visible = false;
            txtRemarkTab3.Visible = false;
            lblUploadTypeTab3.Visible = false;
            //lblCostOther.Visible = false;
            //txtCostOther.Visible = false;
            //lblOilLose.Visible = false;
            //txtOilLose.Visible = false;
            lblTotalCostAll.Visible = false;
            radCostCheck.Visible = false;
            lblCostCheckDate.Visible = false;
            txtCostCheckDate.Visible = false;
            cmdSaveIsCost.Visible = false;
            cmdShowAppeal.Text = "ยื่นอุทธรณ์";

            dgvScore.Visible = false;
            dgvScoreTotalCar.Visible = false;

            dgvScoreList.Columns[6].Visible = false;
            //ImageButton imgViewButtonTab3;
            //ImageButton imgDeleteButtonTab3;
            //for (int i = 0; i < dgvScoreList.Rows.Count; i++)
            //{
            //    imgViewButtonTab3 = (ImageButton)dgvScoreList.Rows[i].FindControl("imgViewScore");
            //    imgDeleteButtonTab3 = (ImageButton)dgvScoreList.Rows[i].FindControl("imgDeleteScore");

            //    if (imgViewButtonTab3 != null)
            //        imgViewButtonTab3.Visible = false;

            //    if (imgDeleteButtonTab3 != null)
            //        imgDeleteButtonTab3.Visible = false;
            //}

            divCost.Visible = false;

            ImageButton imgButtonUploadTab3;
            for (int i = 0; i < dgvUploadFileTab3.Rows.Count; i++)
            {
                imgButtonUploadTab3 = (ImageButton)dgvUploadFileTab3.Rows[i].FindControl("imgDelete");
                if (imgButtonUploadTab3 != null)
                    imgButtonUploadTab3.Visible = false;
            }

            lblUploadTypeTab3.Visible = false;
            cboUploadTypeTab3.Visible = false;
            fileUploadTab3.Visible = false;
            cmdUploadTab3.Visible = false;

            dtUploadFlag = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN", " AND IS_VENDOR_VIEW='1'");
            GridViewHelper.BindGridView(ref dgvUploadFile_flag, dtUploadFlag);

            //dtUploadTab3 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN_SCORE", " AND IS_VENDOR_DOWNLOAD = '1'");
            //GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void CheckCanEdit()
    {
        try
        {//ต้องแก้ไขเอกสารภายในวันที่ 15 ของเดือนถัดไป
            //dtData = ComplainBLL.Instance.ComplainSelectBLL(" AND c.DOC_ID = '" + DocID + "'");
            if ((DocID != null) && (!string.Equals(DocID, string.Empty)) && (dtData != null))
            {//  ต้องเป็นผู้สร้างเอกสารเท่านั้น  จึงจะมีสิทธิ์แก้ไขข้อมูลใน Tab1, Tab2
                DataTable dt = ComplainBLL.Instance.ComplainCanEditBLL(DocID);
                lblEditDate.Visible = true;
                lblEditDate.Text = string.Format(lblEditDate.Text, dt.Rows[0]["DATE_EDIT"].ToString());

                DataTable dtLogin = new DataTable();
                dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

                //if (!string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode))
                if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                {
                    this.ScreenForVendor();
                }

                if (!string.Equals(txtPointFinal.Text.Trim(), string.Empty))
                {
                    DataTable dtAppeal = ComplainBLL.Instance.AppealSelectBLL(DocID);
                    if (dtAppeal.Rows.Count == 0)
                    {
                        chkBlacklistFinal.Checked = chkBlacklist.Checked;
                        txtAppealPoint.Text = txtPointFinal.Text;
                        txtAppealCost.Text = txtCostFinal.Text;
                        txtAppealCostOther.Text = txtCostOther.Text;
                        txtAppealDisable.Text = txtDisableFinal.Text;
                        txtAppealOilLose.Text = txtOilLose.Text;
                    }
                    else
                    {
                        //txtAppealPoint.Text = (decimal.Parse(txtPointFinal.Text.Trim()) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_POINT"].ToString())).ToString();
                        //txtAppealCost.Text = (decimal.Parse(txtCostFinal.Text.Trim()) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_COST"].ToString())).ToString();
                        //txtAppealDisable.Text = (decimal.Parse(txtDisableFinal.Text.Trim()) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_DISABLE"].ToString())).ToString();

                        chkBlacklistFinal.Checked = (string.Equals(dtAppeal.Rows[0]["OIL_LOSE"].ToString(), "0") ? false : true);
                        txtAppealPoint.Text = (decimal.Parse(txtPointFinal.Text) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_POINT"].ToString())).ToString();
                        txtAppealCost.Text = (decimal.Parse(txtCostFinal.Text) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_COST"].ToString())).ToString();
                        txtAppealCostOther.Text = dtAppeal.Rows[0]["COST_OTHER"].ToString();
                        txtAppealDisable.Text = (decimal.Parse(txtDisableFinal.Text) - decimal.Parse(dtAppeal.Rows[0]["TOTAL_DISABLE"].ToString())).ToString();
                        txtAppealOilLose.Text = dtAppeal.Rows[0]["OIL_LOSE"].ToString();
                    }
                }

                if (DocStatusID == ConfigValue.DocStatus7 || DocStatusID == ConfigValue.DocStatus10 || DocStatusID == ConfigValue.DocStatus11)
                {//ปิดเรื่อง ทุกกรณี
                    cmdSaveTab1.Enabled = false;
                    cmdSaveTab2.Enabled = false;
                    cmdSaveTab2Draft.Enabled = false;
                    cmdSaveTab3.Enabled = false;
                    cmdSaveTab3Draft.Enabled = false;
                    cmdChangeStatus.Enabled = false;

                    cmdShowAppeal.Visible = true;
                    chkBlacklistFinal.Visible = true;
                    lblAppealPoint.Visible = true;
                    txtAppealPoint.Visible = true;
                    lblAppealCost.Visible = true;
                    txtAppealCost.Visible = true;
                    lblAppealCostOther.Visible = true;
                    txtAppealCostOther.Visible = true;
                    //lblAppealDisable.Visible = true;
                    //txtAppealDisable.Visible = true;
                    lblAppealOilLose.Visible = true;
                    txtAppealOilLose.Visible = true;
                }
                else if (string.Equals(dt.Rows[0]["CAN_EDIT"].ToString(), "1") && (DocStatusID == ConfigValue.DocStatus1 || DocStatusID == ConfigValue.DocStatus2 || DocStatusID == ConfigValue.DocStatus3 || DocStatusID == ConfigValue.DocStatus7))
                {
                    if (string.Equals(Session["UserID"].ToString(), dtData.Rows[0]["SCREATE"].ToString()))
                    {
                        cmdSaveTab1.Enabled = true;
                        cmdRequestDoc.Enabled = true;
                        txtRequestDoc.Enabled = true;
                        if (dtData.Rows[0]["IS_URGENT"].ToString() == "1")
                        {
                            cmdSaveTab2.Enabled = true;
                        }
                        else
                        {
                            if ((DateTime.ParseExact(dtData.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) >= DateTime.ParseExact(dtData.Rows[0]["COMPLAIN_VENDOR_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (DateTime.ParseExact(dtData.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) <= DateTime.ParseExact(dtData.Rows[0]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)))
                                cmdSaveTab2.Enabled = true;
                            else
                                cmdSaveTab2.Enabled = false;
                        }
                        cmdSaveTab2Draft.Enabled = true;
                        cmdRequestDoc.Enabled = true;

                        if (DocStatusID == ConfigValue.DocStatus1 || DocStatusID == ConfigValue.DocStatus2)
                            cmdCancelDoc.Enabled = true;
                        else
                            cmdCancelDoc.Enabled = false;
                    }
                    else
                    {
                        cmdSaveTab1.Enabled = false;
                        cmdSaveTab2.Enabled = false;
                        cmdSaveTab2Draft.Enabled = false;
                        cmdCancelDoc.Enabled = false;
                    }
                }
                else
                {
                    if (DocStatusID == ConfigValue.DocStatus4)
                    {
                        lblEditDate.Text = "อยู่ระหว่างการพิจารณาของ รข.";

                        cmdSaveTab1.Enabled = false;
                        cmdCancelDoc.Enabled = false;

                        cmdSaveTab2.Enabled = false;
                        cmdSaveTab2Draft.Enabled = false;
                    }
                    else if ((DocStatusID == ConfigValue.DocStatus8) && string.Equals(Session["UserID"].ToString(), dtData.Rows[0]["SCREATE"].ToString()))
                    {
                        //if (dtData.Rows[0]["IS_URGENT"].ToString() == "1")
                        //{
                        cmdSaveTab2.Enabled = true;
                        //}
                        //else
                        //{
                        //    if ((DateTime.ParseExact(dtData.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) >= DateTime.ParseExact(dtData.Rows[0]["COMPLAIN_VENDOR_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (DateTime.ParseExact(dtData.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) <= DateTime.ParseExact(dtData.Rows[0]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)))
                        //        cmdSaveTab2.Enabled = true;
                        //}
                    }
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LoadTopic()
    {
        try
        {
            dtTopic = TopicBLL.Instance.TopicSelect(" AND TYPE = 'COMPLAIN'");
            DropDownListHelper.BindDropDownList(ref cmbTopic, dtTopic, "TOPIC_ID", "TOPIC_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void InitialRequestFile()
    {
        try
        {
            dtRequestFile = new DataTable();
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            if (dtHeader.Rows.Count > 0)
            {
                string tmp = string.Empty;

                dtRequestFile = ComplainBLL.Instance.ComplainRequestFileBLL(this.GetConditionRequestFile());
                if (dtRequestFile.Rows.Count > 0)
                {
                    tmp = dtRequestFile.Rows[0]["COMPLAIN_TYPE_NAME"].ToString();

                    for (int i = 1; i < dtRequestFile.Rows.Count; i++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"].ToString(), tmp))
                            dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"] = string.Empty;
                        else
                            tmp = dtRequestFile.Rows[i]["COMPLAIN_TYPE_NAME"].ToString();
                    }
                    GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);
                }
            }

            if (dtHeader.Rows.Count > 0)
            {
                string tmp = string.Empty;

                dtRequestFile2 = ComplainBLL.Instance.ComplainRequestFileBLL(this.GetConditionReqFile());
                if (dtRequestFile2.Rows.Count > 0)
                {
                    tmp = dtRequestFile2.Rows[0]["COMPLAIN_TYPE_NAME"].ToString();

                    for (int i = 1; i < dtRequestFile2.Rows.Count; i++)
                    {
                        if (string.Equals(dtRequestFile2.Rows[i]["COMPLAIN_TYPE_NAME"].ToString(), tmp))
                            dtRequestFile2.Rows[i]["COMPLAIN_TYPE_NAME"] = string.Empty;
                        else
                            tmp = dtRequestFile2.Rows[i]["COMPLAIN_TYPE_NAME"].ToString();
                    }
                    GridViewHelper.BindGridView(ref grvRequestReqDoc, dtRequestFile2, false);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionRequestFile()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID IN (");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                sb.Append("'");
                sb.Append(dtHeader.Rows[i]["COMPLAINID"].ToString());
                sb.Append("',");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append(")");

            sb.Append(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'COMPLAIN'");
            sb.Append(" ORDER BY M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionReqFile()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID IN (");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                sb.Append("'");
                sb.Append(dtHeader.Rows[i]["COMPLAINID"].ToString());
                sb.Append("',");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append(")");

            sb.Append(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'COMPLAIN_REQ'");
            sb.Append(" ORDER BY M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmbTopic_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.LoadComplainType();
            chkLock.Checked = false;
            if (dtHeader.Rows.Count == 0)
                this.ClearRequire();
            else
                this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ClearRequire()
    {
        cmbTopic.Enabled = true;
        cboComplainType.Enabled = true;
        cboVendor.Enabled = true;
        cboContract.Enabled = true;
        cboHeadRegist.Enabled = true;
        btnSearchByCarLicense.Enabled = true;
        btnSearchLicenseReset.Enabled = true;
        cboDelivery.Enabled = true;
        cmbPersonalNo.Enabled = true;
        txtDateTrans.Enabled = true;
        cbxOrganiz.Enabled = true;

        //txtTotalCar.Enabled = false;

        lblReqcmbTopic.Visible = false;
        lblReqtxtComplainAddress.Visible = false;
        lblReqcboComplainType.Visible = false;
        lblReqcboDelivery.Visible = false;
        lblReqcboHeadRegist.Visible = false;
        lblReqcboVendor.Visible = false;
        lblReqcmbPersonalNo.Visible = false;
        lblReqtxtDateTrans.Visible = false;
        lblReqcbxOrganiz.Visible = false;
        lblReqcboContract.Visible = false;
        lblReqtxtTotalCar.Visible = false;

        if (dtHeader.Rows.Count == 0)
            txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
        else
            txtDateTrans.Text = (dtHeader.Rows[0]["DELIVERYDATE"] != null) ? dtHeader.Rows[0]["DELIVERYDATE"].ToString() : string.Empty;
    }

    private void LoadComplainType()
    {
        try
        {
            cboComplainType.Items.Clear();
            if (cmbTopic.SelectedIndex > 0)
            {
                dtComplainType = ComplainTypeBLL.Instance.ComplainTypeSelect(this.GetConditionComplain());
                DropDownListHelper.BindDropDownList(ref cboComplainType, dtComplainType, "COMPLAIN_TYPE_ID", "COMPLAIN_TYPE_NAME", true);
            }
            this.ComplainTypeSelectedIndex();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionComplain()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_TOPIC.TOPIC_ID = '" + cmbTopic.SelectedValue + "'");
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN");
        DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");

        dtUploadType2 = UploadTypeBLL.Instance.UploadTypeSelectBLL("COMPLAIN_REQ");
        DropDownListHelper.BindDropDownList(ref cboUploadReqType, dtUploadType2, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload2 = new DataTable();
        dtUpload2.Columns.Add("UPLOAD_ID");
        dtUpload2.Columns.Add("UPLOAD_NAME");
        dtUpload2.Columns.Add("FILENAME_SYSTEM");
        dtUpload2.Columns.Add("FILENAME_USER");
        dtUpload2.Columns.Add("FULLPATH");
    }

    private void InitialUploadReqDoc()
    {
        //dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL(SystemID, "COMPLAIN_REQ");
        //DropDownListHelper.BindDropDown(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true, Properties.Resources.DropDownChooseText);

        dtUploadReqDoc = new DataTable();
        dtUploadReqDoc.Columns.Add("UPLOAD_ID");
        dtUploadReqDoc.Columns.Add("UPLOAD_NAME");
        dtUploadReqDoc.Columns.Add("FILENAME_SYSTEM");
        dtUploadReqDoc.Columns.Add("FILENAME_USER");
        dtUploadReqDoc.Columns.Add("FULLPATH");
    }

    private void InitialHeader()
    {
        try
        {
            dtHeader = new DataTable();
            dtHeader.Columns.Add("TOPICID");
            dtHeader.Columns.Add("TOPICNAME");
            dtHeader.Columns.Add("ORGANIZID");
            dtHeader.Columns.Add("ORGANIZNAME");
            dtHeader.Columns.Add("COMPLAINID");
            dtHeader.Columns.Add("COMPLAINNAME");
            dtHeader.Columns.Add("LOCKDRIVERID");
            dtHeader.Columns.Add("LOCKDRIVERNAME");
            dtHeader.Columns.Add("DELIVERYID");
            dtHeader.Columns.Add("DELIVERYNAME");
            dtHeader.Columns.Add("TYPEPRODUCTID");
            dtHeader.Columns.Add("TYPEPRODUCTNAME");
            dtHeader.Columns.Add("TRUCKID");
            dtHeader.Columns.Add("CARHEAD");
            dtHeader.Columns.Add("CARDETAIL");
            dtHeader.Columns.Add("VENDORID");
            dtHeader.Columns.Add("VENDORNAME");
            dtHeader.Columns.Add("CONTRACTID");
            dtHeader.Columns.Add("CONTRACTNAME");
            dtHeader.Columns.Add("PERSONALID");
            dtHeader.Columns.Add("EMPLOYEENAME");
            dtHeader.Columns.Add("DELIVERYDATE");
            dtHeader.Columns.Add("COMPLAINADDRESS");
            dtHeader.Columns.Add("TOTALCAR");
            dtHeader.Columns.Add("SSERVICEID");
            dtHeader.Columns.Add("ISNEW");
            dtHeader.Columns.Add("WAREHOUSE_TO");

            dtAddDriver = new DataTable();
            dtAddDriver.Columns.Add("SDRIVERNO");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Complain" + "\\" + DocID;
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
        if (dtUpload.Rows.Count > 0 && dtUploadReqDoc.Rows.Count > 0)
        {
            this.ValidateSaveTab2();
            this.ValidateSaveTab2ReqDoc();
            cmdSaveTab2.Enabled = true;
        }
    }

    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload.HasFile)
            {
                DataTable dtLogin = new DataTable();
                dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

                DataTable dtDocument = ComplainBLL.Instance.ComplainCanEditSelectBLL((DocID == null) ? string.Empty : DocID, dtLogin.Rows[0]["SUID"].ToString(), OTP, 1);

                if (string.Equals(dtDocument.Rows[0][0].ToString(), "URGENT"))
                {
                    //เอกสารแจ้งล่าช้า
                    Label2.Text = "แจ้งเตือนจากระบบการแจ้งเรื่องร้องเรียน";
                    StringBuilder sbMessage = new StringBuilder();
                    //sb.Append("<br />");
                    sbMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตามมติคณะกรรมการพิจารณาโทษ ผู้ร้องเรียนสามารถยื่นข้อร้องเรียนและหลักฐานประกอบของเดือน M ได้ภายใน 1st Half (ถึงวันที่ 15) ของเดือน M+1");
                    sbMessage.Append(" ท่านได้แนบเอกสารล่าช้ากว่าที่กำหนด กรุณาติดต่อผู้บังคับบัญชาของท่านเพื่อขออนุญาตเปิดเรื่องร้องเรียนไปยังผู้จัดการส่วนระบบและประมวลผลการขนส่ง (ผจ.รข.)");
                    sbMessage.Append("<br />");
                    sbMessage.Append("<br />");
                    Label19.Text = sbMessage.ToString();

                    Label20.Text = "หรือกรณีได้รับอนุญาตแล้ว กรุณาใส่รหัสผ่าน OTP เพื่อบันทึกเปิดเรื่องร้องเรียน";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowOTPDoc", "$('#ShowOTPDoc').modal();", true);
                    updOTP.Update();
                    return;
                }

                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void StartUpload2()
    {
        try
        {
            if (cboUploadReqType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload1.HasFile)
            {
                DataTable dtLogin = new DataTable();
                dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

                DataTable dtDocument = ComplainBLL.Instance.ComplainCanEditSelectBLL((DocID == null) ? string.Empty : DocID, dtLogin.Rows[0]["SUID"].ToString(), OTP, 1);

                if (string.Equals(dtDocument.Rows[0][0].ToString(), "URGENT"))
                {
                    //เอกสารแจ้งล่าช้า
                    Label2.Text = "แจ้งเตือนจากระบบการแจ้งเรื่องร้องเรียน";
                    StringBuilder sbMessage = new StringBuilder();
                    //sb.Append("<br />");
                    sbMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตามมติคณะกรรมการพิจารณาโทษ ผู้ร้องเรียนสามารถยื่นข้อร้องเรียนและหลักฐานประกอบของเดือน M ได้ภายใน 1st Half (ถึงวันที่ 15) ของเดือน M+1");
                    sbMessage.Append(" ท่านได้แนบเอกสารล่าช้ากว่าที่กำหนด กรุณาติดต่อผู้บังคับบัญชาของท่านเพื่อขออนุญาตเปิดเรื่องร้องเรียนไปยังผู้จัดการส่วนระบบและประมวลผลการขนส่ง (ผจ.รข.)");
                    sbMessage.Append("<br />");
                    sbMessage.Append("<br />");
                    Label19.Text = sbMessage.ToString();

                    Label20.Text = "หรือกรณีได้รับอนุญาตแล้ว กรุณาใส่รหัสผ่าน OTP เพื่อบันทึกเปิดเรื่องร้องเรียน";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowOTPDoc", "$('#ShowOTPDoc').modal();", true);
                    updOTP.Update();
                    return;
                }

                string FileNameUser = fileUpload1.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile2(System.IO.Path.GetExtension(FileNameUser), fileUpload1.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload1.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload2.Rows.Add(cboUploadReqType.SelectedValue, cboUploadReqType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadReqDoc, dtUpload2);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateUploadFile2(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType2.Rows[cboUploadReqType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType2.Rows[cboUploadReqType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType2.Rows[cboUploadReqType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSaveUpload()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            for (int i = 0; i < dtUpload.Rows.Count; i++)
                data.Add(decimal.Parse(dtUpload.Rows[i]["UPLOAD_ID"].ToString()));

            DataTable dt = this.GetFilteredData(dtRequestFile, data);
            if (dt.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("เอกสารที่ต้องทำการอัพโหลด : ");
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("\t- ");
                    sb.Append(dt.Rows[i]["UPLOAD_NAME"].ToString());
                    sb.Append("\r\n");
                }
                sb.Append("จึงจะทำการบันทึกข้อมูลได้");
                sb.Append("\r\n");
                sb.Append("ถ้าเอกสารยังไม่ครบ กรุณากดปุ่ม บันทึก(ร่าง) เพื่อบันทึกข้อมูลไว้ก่อน");

                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSaveUpload2()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            for (int i = 0; i < dtUpload2.Rows.Count; i++)
                data.Add(decimal.Parse(dtUpload2.Rows[i]["UPLOAD_ID"].ToString()));

            DataTable dt = this.GetFilteredData(dtRequestFile2, data);
            if (dt.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("เอกสารที่ต้องทำการอัพโหลด : ");
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("\t- ");
                    sb.Append(dt.Rows[i]["UPLOAD_NAME"].ToString());
                    sb.Append("\r\n");
                }
                sb.Append("จึงจะทำการบันทึกข้อมูลได้");
                sb.Append("\r\n");
                sb.Append("ถ้าเอกสารยังไม่ครบ กรุณากดปุ่ม บันทึก(ร่าง) เพื่อบันทึกข้อมูลไว้ก่อน");

                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSaveReqUpload()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            for (int i = 0; i < dtUpload.Rows.Count; i++)
                data.Add(decimal.Parse(dtUpload.Rows[i]["UPLOAD_ID"].ToString()));

            DataTable dt = this.GetFilteredData(dtRequestFile2, data);
            if (dt.Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("เอกสารที่ต้องทำการอัพโหลด : ");
                sb.Append("\r\n");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("\t- ");
                    sb.Append(dt.Rows[i]["UPLOAD_NAME"].ToString());
                    sb.Append("\r\n");
                }
                sb.Append("จึงจะทำการบันทึกข้อมูลได้");
                sb.Append("\r\n");
                sb.Append("ถ้าเอกสารยังไม่ครบ กรุณากดปุ่ม บันทึก(ร่าง) เพื่อบันทึกข้อมูลไว้ก่อน");

                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private DataTable GetFilteredData(DataTable table, List<decimal> filterValues)
    {
        DataView dv = new DataView(table);
        var filter = string.Join(",", filterValues);
        dv.RowFilter = "UPLOAD_ID NOT IN (" + filter + ")";
        return dv.ToTable();
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void GrvLoad(string Path, string FileName)
    {
        Response.Redirect(Path + "\\" + FileName);
    }
    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                if ("" + Session["CheckPermission"] == "1")
                //if (!CanWrite)
                {
                    imgDelete.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdAddHeader_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateAddHeader();

            DataTable dtTmp = new DataTable();
            dtTmp = ComplainBLL.Instance.OutboundSelectBLL(this.GetConditionHeader());

            string LockDriverID = "0", LockDriverName = "ไม่";
            if (chkLock.Checked)
            {
                LockDriverID = "1";
                LockDriverName = "ใช่";
            }
            string STRAILERREGISTERNO = string.Empty;
            if (dtTmp.Rows.Count > 0)
                STRAILERREGISTERNO = dtTmp.Rows[0]["STRAILERREGISTERNO"].ToString();

            if (IndexEdit > -1)
            {
                dtHeader.Rows.RemoveAt(IndexEdit);
                dtHeader.Rows.Add(cmbTopic.SelectedValue + ""
                                    , cmbTopic.SelectedItem
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedValue + "" : null
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedItem.ToString() : string.Empty
                                    , cboComplainType.SelectedValue + ""
                                    , cboComplainType.SelectedItem
                                    , LockDriverID
                                    , LockDriverName
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedValue + "" : null
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedItem.ToString() : string.Empty
                                    , rblTypeProduct.Value + ""
                                    , rblTypeProduct.SelectedItem
                                    , (cboHeadRegist.SelectedIndex > 0) ? cboHeadRegist.SelectedValue + "" : null
                                    , (cboHeadRegist.SelectedIndex > 0) ? cboHeadRegist.SelectedItem.ToString() : string.Empty
                                    , txtTrailerRegist.Value
                                    , (cboVendor.SelectedIndex > 0) ? cboVendor.SelectedValue + "" : null
                                    , (cboVendor.SelectedIndex > 0) ? cboVendor.SelectedItem.ToString() : string.Empty
                                    , (cboContract.SelectedIndex > 0) ? cboContract.SelectedValue + "" : null
                                    , (cboContract.SelectedIndex > 0) ? cboContract.SelectedItem.ToString() : string.Empty
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedValue + "" : null
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedItem.ToString() : string.Empty
                                    , txtDateTrans.Text
                    //, txtDateTrans.Text.Remove(11, 5) + "00:00"
                                    , txtComplainAddress.Value.Trim()
                                    , txtTotalCar.Text.Trim()
                                    , ServiceIDEdit
                                    , "N"
                                    , txtShipTo.Text.Trim());

                dtAddDriver.Rows.RemoveAt(IndexEdit);
                dtAddDriver.Rows.Add((cmbPersonalNo2.SelectedIndex > 0) ? cmbPersonalNo2.SelectedValue + "" : null);
                dtAddDriver.Rows.Add((cmbPersonalNo3.SelectedIndex > 0) ? cmbPersonalNo3.SelectedValue + "" : null);
                dtAddDriver.Rows.Add((cmbPersonalNo4.SelectedIndex > 0) ? cmbPersonalNo4.SelectedValue + "" : null);
                IndexEdit = -1;
            }
            else
            {
                dtHeader.Rows.Add(cmbTopic.SelectedValue + ""
                                    , cmbTopic.SelectedItem
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedValue + "" : null
                                    , (cbxOrganiz.SelectedIndex > 0) ? cbxOrganiz.SelectedItem.ToString() : string.Empty
                                    , cboComplainType.SelectedValue + ""
                                    , cboComplainType.SelectedItem
                                    , LockDriverID
                                    , LockDriverName
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedValue + "" : null
                                    , (cboDelivery.SelectedIndex > 0) ? cboDelivery.SelectedItem.ToString() : string.Empty
                                    , rblTypeProduct.Value + ""
                                    , rblTypeProduct.SelectedItem
                                    , (cboHeadRegist.SelectedIndex > 0) ? cboHeadRegist.SelectedValue + "" : null
                                    , (cboHeadRegist.SelectedIndex > 0) ? cboHeadRegist.SelectedItem.ToString() : string.Empty
                                    , txtTrailerRegist.Value
                                    , (cboVendor.SelectedIndex > 0) ? cboVendor.SelectedValue + "" : null
                                    , (cboVendor.SelectedIndex > 0) ? cboVendor.SelectedItem.ToString() : string.Empty
                                    , (cboContract.SelectedIndex > 0) ? cboContract.SelectedValue + "" : null
                                    , (cboContract.SelectedIndex > 0) ? cboContract.SelectedItem.ToString() : string.Empty
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedValue + "" : null
                                    , (cmbPersonalNo.SelectedIndex > 0) ? cmbPersonalNo.SelectedItem.ToString() : string.Empty
                                    , txtDateTrans.Text
                    //, txtDateTrans.Text.Remove(11, 5) + "00:00"
                                    , txtComplainAddress.Value.Trim()
                                    , txtTotalCar.Text.Trim()
                                    , "-1"
                                    , "Y"
                                    , txtShipTo.Text.Trim());

                dtAddDriver.Rows.Add((cmbPersonalNo2.SelectedIndex > 0) ? cmbPersonalNo2.SelectedValue + "" : null);
                dtAddDriver.Rows.Add((cmbPersonalNo3.SelectedIndex > 0) ? cmbPersonalNo3.SelectedValue + "" : null);
                dtAddDriver.Rows.Add((cmbPersonalNo4.SelectedIndex > 0) ? cmbPersonalNo4.SelectedValue + "" : null);
            }

            ServiceIDEdit = string.Empty;
            cmdAddHeader.Text = "เพิ่มรายการ";
            cmdCancel.Visible = false;

            GridViewHelper.BindGridView(ref dgvHeader, dtHeader);

            if (dtHeader.Rows.Count == 1)
                this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));
            else if (dtHeader.Rows.Count == 0)
                this.ClearScreenForHeader();

            this.InitialRequestFile();

            if (chkTruckOutContract.Checked)
                IsTruckOutContract = true;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionHeader()
    {
        StringBuilder sb = new StringBuilder();

        sb.Append(" AND TPLANSCHEDULELIST.SDELIVERYNO = '" + cboDelivery.SelectedValue + "'");
        sb.Append(" AND TCONTRACT_TRUCK.SCONTRACTID = '" + cboContract.SelectedValue + "'");
        sb.Append(" AND TPLANSCHEDULE.SHEADREGISTERNO = '" + cboHeadRegist.SelectedValue + "'");

        return sb.ToString();
    }

    private void ValidateAddHeader()
    {
        try
        {
            if (cmbTopic.SelectedIndex < 1)
                throw new Exception("กรุณาป้อน ประเภทร้องเรียน !!!");

            if (cboComplainType.SelectedIndex < 1)
                throw new Exception("กรุณาป้อน หัวข้อร้องเรียน !!!");

            if ((chkLock.Checked) && (cmbPersonalNo.SelectedIndex < 1))
                throw new Exception("กรุณาป้อน ชื่อ พขร. ที่ถูกร้องเรียน !!!");

            dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(cboComplainType.SelectedValue.ToString()), "COMPLAIN");

            StringBuilder sb = new StringBuilder();
            object c;
            string Type;

            sb.Append("<table>");
            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                {
                    c = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                    Type = c.GetType().ToString();
                    if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxComboBox"))
                    {
                        ASPxComboBox cbo = (ASPxComboBox)c;
                        if (cbo.Value == null)
                        {
                            sb.Append("<tr><td>");
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!! <br />");
                            sb.Append("</td></tr>");
                        }
                    }
                    else if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxTextBox"))
                    {
                        ASPxTextBox txt = (ASPxTextBox)c;
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                        {
                            sb.Append("<tr><td>");
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!! <br />");
                            sb.Append("</td></tr>");
                        }
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                    {
                        TextBox txt = (TextBox)c;
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                        {
                            sb.Append("<tr><td>");
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!! <br />");
                            sb.Append("</td></tr>");
                        }
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                    {
                        DropDownList cbo = (DropDownList)c;
                        if (cbo.SelectedIndex < 1)
                        {
                            sb.Append("<tr><td>");
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!! <br />");
                            sb.Append("</td></tr>");
                        }
                    }
                    else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
                    {
                        HtmlInputText txt = (HtmlInputText)c;
                        if (string.Equals(txt.Value.Trim(), string.Empty))
                        {
                            sb.Append("<tr><td>");
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!! <br />");
                            sb.Append("</td></tr>");
                        }
                    }
                }
            }
            sb.Append("</table>");
            if (!string.Equals(sb.ToString(), "<table></table>"))
                throw new Exception(sb.ToString());

            //เชคซ้ำ
            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (i == IndexEdit)
                    continue;

                if (string.Equals(dtHeader.Rows[i]["TopicID"].ToString(), cmbTopic.SelectedValue + "")
                    && string.Equals(dtHeader.Rows[i]["ComplainID"].ToString(), cboComplainType.SelectedValue + "")
                    && string.Equals(dtHeader.Rows[i]["TruckID"].ToString(), cboHeadRegist.SelectedValue)
                    && string.Equals(dtHeader.Rows[i]["ContractID"].ToString(), cboContract.SelectedValue + "")
                    && string.Equals(dtHeader.Rows[i]["PERSONALID"].ToString(), cmbPersonalNo.SelectedValue)
                    && string.Equals(dtHeader.Rows[i]["DeliveryDate"].ToString(), txtDateTrans.Text)
                    )
                    throw new Exception("ข้อมูลดังกล่าว ได้ถูกเพิ่มรายการเข้ามาแล้ว");
            }

            //1 เอกสาร สามารถเลือกได้เพียง 1 เลขที่สัญญา
            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (i == IndexEdit)
                    continue;

                if (!string.Equals(dtHeader.Rows[i]["CONTRACTID"].ToString(), cboContract.SelectedValue + ""))
                    throw new Exception("1 เอกสาร สามารถเลือกได้เพียง 1 เลขที่สัญญา");
            }

            //1 เอกสาร สามารถเลือกได้เพียง 1 พขร
            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (i == IndexEdit)
                    continue;

                if (!string.Equals(dtHeader.Rows[i]["PERSONALID"].ToString(), cmbPersonalNo.SelectedValue + ""))
                    throw new Exception("1 เอกสาร สามารถเลือกได้เพียง 1 พขร.");
            }

            //for (int i = 0; i < dtHeader.Rows.Count; i++)
            //{
            //    if (i == IndexEdit)
            //        continue;

            //    if (string.Equals(this.CheckIsAddMultipleHeader(dtHeader.Rows[i]["COMPLAINID"].ToString()), "0"))
            //    {
            //        if (!string.Equals(dtHeader.Rows[i]["TruckID"].ToString(), cboHeadRegist.SelectedValue + ""))
            //            throw new Exception("ข้อร้องเรียน " + dtHeader.Rows[i]["COMPLAINNAME"].ToString() + "\r\n" + "สามารถเลือกได้เพียง 1 ทะเบียนรถ");

            //        if (!string.Equals(dtHeader.Rows[i]["DELIVERYDATE"].ToString().Substring(0, 10), txtDateTrans.Text.Substring(0, 10)))
            //            throw new Exception("ข้อร้องเรียน " + dtHeader.Rows[i]["COMPLAINNAME"].ToString() + "\r\n" + "สามารถเลือกวันที่เกิดเหตุ ได้เพียงวันเดียว");
            //    }
            //    else
            //    {
            //        if (i == IndexEdit)
            //            continue;

            //        if (!string.Equals(dtHeader.Rows[i]["COMPLAINID"].ToString(), cboComplainType.SelectedValue + ""))
            //            throw new Exception("ข้อร้องเรียน " + dtHeader.Rows[i]["COMPLAINNAME"].ToString() + "\r\n" + "ไม่สามารถเลือกได้หลายประเภทเรื่องร้องเรียน");
            //    }
            //}

            //1 เอกสาร สามารถเลือกเหตุการณ์ ได้เพียง 1 เหตุการณ์
            //for (int i = 0; i < dtHeader.Rows.Count; i++)
            //{
            //    if (i == IndexEdit)
            //        continue;

            //    if (!string.Equals(dtHeader.Rows[i]["TOPICID"].ToString(), cmbTopic.SelectedValue + ""))
            //        throw new Exception("1 เอกสาร สามารถเลือกได้เพียง 1 เหตุการณ์");
            //}

            int tmp;

            if ((lblReqtxtTotalCar.Visible) && (!int.TryParse(txtTotalCar.Text.Trim(), out tmp)))
                throw new Exception("จำนวนรถ ต้องเป็นตัวเลขเท่านั้น !!!");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void CheckAddHeader(string IsAddMultipleCar)
    {
        try
        {
            this.ClearRequire();
            this.InitialRequireField();

            if (string.Equals(IsAddMultipleCar, "0") || string.Equals(IsAddMultipleCar, string.Empty))
                this.EnableMultipleTopic(true);
            else
                this.EnableMultipleTopic(false);
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void EnableMultipleTopic(bool IsEnable)
    {//ถ้าเปิดอยู่ให้ปิด
        //Header
        cmbTopic.Enabled = IsEnable;
        cboComplainType.Enabled = IsEnable;

        //Detail
        if (IsEnable)
        {
            cboVendor.Enabled = !IsEnable;
            cboContract.Enabled = !IsEnable;
            cboHeadRegist.Enabled = !IsEnable;
            btnSearchByCarLicense.Enabled = !IsEnable;
            btnSearchLicenseReset.Enabled = !IsEnable;
            cmbPersonalNo.Enabled = !IsEnable;
            cmbPersonalNo2.Enabled = !IsEnable;
            cmbPersonalNo3.Enabled = !IsEnable;
            cmbPersonalNo4.Enabled = !IsEnable;
            txtDateTrans.Enabled = !IsEnable;
            cbxOrganiz.Enabled = !IsEnable;
            rblTypeProduct.Enabled = !IsEnable;
            cboDelivery.Enabled = !IsEnable;
            txtComplainAddress.Disabled = IsEnable;
            txtTotalCar.Enabled = IsEnable;
        }
    }

    private void ClearScreenForHeader()
    {
        //Header
        cmbTopic.Enabled = true;
        cboComplainType.Enabled = true;

        //DetailcmdDriverSearch_Click
        cboVendor.Enabled = true;
        cboContract.Enabled = true;
        cboHeadRegist.Enabled = true;
        btnSearchByCarLicense.Enabled = true;
        btnSearchLicenseReset.Enabled = true;
        cmbPersonalNo.Enabled = true;
        cmbPersonalNo2.Enabled = true;
        cmbPersonalNo3.Enabled = true;
        cmbPersonalNo4.Enabled = true;
        txtDateTrans.Enabled = true;
        cbxOrganiz.Enabled = true;
        rblTypeProduct.Enabled = true;
        cboDelivery.Enabled = true;
        txtComplainAddress.Disabled = false;
        txtTotalCar.Enabled = false;

        this.ClearRequire();
        this.InitialRequireField();
    }

    private string CheckComplainTypeName(string ComplainTypeID)
    {
        try
        {
            for (int i = 0; i < dtComplainType.Rows.Count; i++)
            {
                if (string.Equals(dtComplainType.Rows[i]["COMPLAIN_TYPE_ID"].ToString(), "ComplainTypeID"))
                    return dtComplainType.Rows[i]["COMPLAIN_TYPE_NAME"].ToString();
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string CheckIsAddMultipleHeader(string ComplainTypeID)
    {
        try
        {
            for (int i = 0; i < dtComplainType.Rows.Count; i++)
            {
                if (string.Equals(dtComplainType.Rows[i]["COMPLAIN_TYPE_ID"].ToString(), ComplainTypeID))
                    return dtComplainType.Rows[i]["IS_ADD_MULTIPLE_CAR"].ToString();
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (dtHeader.Rows.Count == 0)
                throw new Exception("กรุณา เพิ่มรายการ ก่อนบันทึก");

            if (string.Equals(txtFName.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน หน่วยงานผู้ร้องเรียน");

            if (string.Equals(txtFNameDivision.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ชื่อผู้ร้องเรียน");

            if (string.Equals(txtDetail.Value.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน สาเหตุ / รายละเอียด");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvHeader_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtHeader.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvHeader, dtHeader);

            if (dtHeader.Rows.Count == 0)
                this.ClearScreenForHeader();

            IndexEdit = -1;

            if (dtHeader.Rows.Count == 0)
                IsTruckOutContract = false;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvHeader_RowDeleting2(object sender, EventArgs e)
    {
        try
        {
            //dtHeader.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvHeader, dtHeader);

            IndexEdit = -1;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cboComplainType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.ComplainTypeSelectedIndex();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ComplainTypeSelectedIndex()
    {
        if (cmbTopic.SelectedIndex > 0)
        {
            if (dgvHeader.Rows.Count == 0)
            {
                this.ClearValue();
                this.ClearRequire();

                if (cboComplainType.SelectedIndex > 0)
                {
                    if (string.Equals(dtComplainType.Rows[cboComplainType.SelectedIndex]["LOCK_DRIVER"].ToString(), "0"))
                    {
                        chkLock.Enabled = false;
                        chkLock.Checked = false;
                    }
                    else
                    {
                        chkLock.Enabled = true;
                        chkLock.Checked = true;
                    }

                    this.InitialRequireField();
                }
                else
                {
                    chkLock.Enabled = false;
                    chkLock.Checked = false;
                }
            }
            else
            {
                this.ClearRequire();
                this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));
            }
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null)
                return controlToReturn;
        }
        return null;
    }

    private void InitialRequireField()
    {
        try
        {
            this.ClearRequire();

            if (cboComplainType.SelectedIndex < 1)
                return;

            dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(cboComplainType.SelectedValue.ToString()), "COMPLAIN");

            object tmp;
            Label tmpReq;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                tmp = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                if (tmp != null)
                {
                    tmpReq = (Label)this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID_REQ"].ToString());

                    if (tmpReq != null)
                    {
                        if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                        {//Require Field
                            tmpReq.Visible = true;
                            if (string.Equals(dtRequire.Rows[i]["CONTROL_ID"].ToString(), "txtDateTrans"))
                            {
                                if (dtHeader.Rows.Count == 0)
                                    txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
                                else
                                    txtDateTrans.Text = (dtHeader.Rows[0]["DELIVERYDATE"] != null) ? dtHeader.Rows[0]["DELIVERYDATE"].ToString() : string.Empty;
                            }
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "2"))
                        {//Optional Field
                            tmpReq.Visible = false;
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "3"))
                        {//Disable Field
                            Type = tmp.GetType().ToString();
                            if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxComboBox"))
                            {
                                ASPxComboBox cbo = (ASPxComboBox)tmp;
                                if (cbo.SelectedIndex > 0)
                                    cbo.SelectedIndex = 0;
                                cbo.Value = string.Empty;
                                cbo.ClientEnabled = false;
                            }
                            else if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxTextBox"))
                            {
                                ASPxTextBox txt = (ASPxTextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Value = string.Empty;
                                txt.ClientEnabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                            {
                                TextBox txt = (TextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Enabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                            {
                                DropDownList cbo = (DropDownList)tmp;
                                if (cbo.SelectedIndex > 0)
                                    cbo.SelectedIndex = 0;
                                cbo.Enabled = false;
                                cbo.CssClass = "form-control";

                                if (dtRequire.Rows[i]["CONTROL_ID"].ToString() == "cboHeadRegist")
                                {
                                    btnSearchByCarLicense.Enabled = false;
                                    btnSearchLicenseReset.Enabled = false;
                                }
                            }
                            else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
                            {
                                HtmlInputText txt = (HtmlInputText)tmp;
                                txt.Value = string.Empty;
                                txt.Disabled = true;
                            }
                        }
                    }
                }
            }

            if (!btnSearchByCarLicense.Enabled)
            {
                btnSearchLicenseReset_Click(null, null);
            }

        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDeleteComplain");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลนี้\\nใช่หรือไม่ ?')==false){return false;}");
                if ("" + Session["CheckPermission"] == "1")
                //if (!CanWrite)
                {
                    imgDelete.Enabled = false;
                    imgDelete.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvHeader_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            IndexEdit = Index;

            if (string.Equals(e.CommandName, "Select"))
            {
                DataTable dtTmp = dtHeader.Copy();
                string[] display = { "ประเภทเรื่องร้องเรียน", "คลังต้นทาง", "หัวข้อร้องเรียน", "หยุดพักการขนส่งชั่วคราว (พขร.)", "Delivery No", "ประเภทผลิตภัณฑ์", "ทะเบียนรถ (หัว)", "ทะเบียนรถ (หาง)", "บริษัทผู้ประกอบการขนส่ง", "เลขที่สัญญา", "รหัสพนักงาน", "พขร. ที่ถูกร้องเรียน", "วันที่เกิดเหตุ", "สถานที่เกิดเหตุ", "จำนวนรถ (คัน)", "ปลายทาง" };

                dtTmp.Columns["TopicName"].ColumnName = "ประเภทเรื่องร้องเรียน";
                dtTmp.Columns["OrganizName"].ColumnName = "คลังต้นทาง";
                dtTmp.Columns["ComplainName"].ColumnName = "หัวข้อร้องเรียน";
                dtTmp.Columns["LockDriverName"].ColumnName = "หยุดพักการขนส่งชั่วคราว (พขร.)";
                dtTmp.Columns["DeliveryName"].ColumnName = "Delivery No";
                dtTmp.Columns["TypeProductName"].ColumnName = "ประเภทผลิตภัณฑ์";
                dtTmp.Columns["CarHead"].ColumnName = "ทะเบียนรถ (หัว)";
                dtTmp.Columns["CarDetail"].ColumnName = "ทะเบียนรถ (หาง)";
                dtTmp.Columns["VendorName"].ColumnName = "บริษัทผู้ประกอบการขนส่ง";
                dtTmp.Columns["ContractName"].ColumnName = "เลขที่สัญญา";
                dtTmp.Columns["PersonalID"].ColumnName = "รหัสพนักงาน";
                dtTmp.Columns["EmployeeName"].ColumnName = "พขร. ที่ถูกร้องเรียน";
                dtTmp.Columns["DeliveryDate"].ColumnName = "วันที่เกิดเหตุ";
                dtTmp.Columns["ComplainAddress"].ColumnName = "สถานที่เกิดเหตุ";
                dtTmp.Columns["TotalCar"].ColumnName = "จำนวนรถ (คัน)";
                dtTmp.Columns["WAREHOUSE_TO"].ColumnName = "ปลายทาง";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                for (int i = 0; i < dtTmp.Columns.Count; i++)
                {
                    if (display.Contains(dtTmp.Columns[i].ColumnName))
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dtTmp.Columns[i].ColumnName + " : " + dtTmp.Rows[Index][i].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
            else if (string.Equals(e.CommandName, "EditHeader"))
            {
                ServiceIDEdit = dtHeader.Rows[Index]["SSERVICEID"].ToString();
                cmdAddHeader.Text = "บันทึก";
                cmdCancel.Visible = true;

                cmbTopic.SelectedValue = dtHeader.Rows[Index]["TOPICID"].ToString();
                cmbTopic_SelectedIndexChanged(null, null);

                cboComplainType.SelectedValue = dtHeader.Rows[Index]["COMPLAINID"].ToString();
                cboComplainType_SelectedIndexChanged(null, null);
                cbxOrganiz.SelectedValue = dtHeader.Rows[Index]["ORGANIZID"].ToString();
                chkLock.Checked = (string.Equals(dtHeader.Rows[Index]["LOCKDRIVERID"].ToString(), "0") ? false : true);
                rblTypeProduct.Value = dtHeader.Rows[Index]["TYPEPRODUCTID"].ToString();
                txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

                cboVendor.SelectedValue = dtHeader.Rows[Index]["VENDORID"].ToString();
                cboVendor_SelectedIndexChanged(null, null);

                cboContract.SelectedValue = dtHeader.Rows[Index]["CONTRACTID"].ToString();
                cboContract_SelectedIndexChanged(null, null);

                try
                {
                    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                    if (!string.Equals(dtHeader.Rows[Index]["TRUCKID"].ToString(), string.Empty) && (string.Equals(cboHeadRegist.SelectedValue, string.Empty)))
                    {
                        chkTruckOutContract.Checked = true;
                        this.TruckSelect();
                        cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                    }
                }
                catch
                {
                    chkTruckOutContract.Checked = true;
                    this.TruckSelect();
                    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
                }
                finally
                {
                    if (string.Equals(dtHeader.Rows[Index]["CARDETAIL"].ToString(), string.Empty))
                        cboHeadRegist_SelectedIndexChanged(null, null);
                }

                if (!string.Equals(dtHeader.Rows[Index]["DELIVERYID"].ToString(), string.Empty))
                    cboDelivery.SelectedValue = dtHeader.Rows[Index]["DELIVERYID"].ToString();

                cmbPersonalNo.SelectedValue = dtHeader.Rows[Index]["PERSONALID"].ToString();

                if (dtAddDriver.Rows.Count > 0)
                {
                    if (dtAddDriver.Rows.Count > 0)
                        cmbPersonalNo2.SelectedValue = dtAddDriver.Rows[0]["SDRIVERNO"].ToString();
                    if (dtAddDriver.Rows.Count > 1)
                        cmbPersonalNo3.SelectedValue = dtAddDriver.Rows[1]["SDRIVERNO"].ToString();
                    if (dtAddDriver.Rows.Count > 2)
                        cmbPersonalNo4.SelectedValue = dtAddDriver.Rows[2]["SDRIVERNO"].ToString();
                }

                txtDateTrans.Text = (dtHeader.Rows[Index]["DELIVERYDATE"] != null) ? dtHeader.Rows[Index]["DELIVERYDATE"].ToString() : string.Empty;
                //teTimeTrans.Text = dtHeader.Rows[Index]["DELIVERYTIME"].ToString();
                txtComplainAddress.Value = dtHeader.Rows[Index]["COMPLAINADDRESS"].ToString();
                txtTotalCar.Text = dtHeader.Rows[Index]["TOTALCAR"].ToString();
                txtShipTo.Text = dtHeader.Rows[Index]["WAREHOUSE_TO"].ToString();

                this.CheckAddHeader(this.CheckIsAddMultipleHeader(dtHeader.Rows[0]["COMPLAINID"].ToString()));
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            ServiceIDEdit = string.Empty;
            IndexEdit = -1;
            cmdAddHeader.Text = "เพิ่มรายการ";
            cmdCancel.Visible = false;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ClearScreen()
    {
        try
        {
            this.InitialForm();

            this.ClearValue();

            this.ClearRequire();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ClearValue()
    {
        try
        {
            cboDelivery.Items.Clear();
            cboHeadRegist.Items.Clear();
            cboContract.Items.Clear();
            cmbPersonalNo.Items.Clear();

            cboVendor.Items.Clear();
            strHeadRegistNo = string.Empty;
            this.LoadVendor();

            chkLock.Checked = false;
            txtComplainAddress.Value = string.Empty;
            txtTotalCar.Text = string.Empty;
            txtTrailerRegist.Value = string.Empty;

            if (dtHeader.Rows.Count > 0)
                txtDateTrans.Text = (dtHeader.Rows[0]["DELIVERYDATE"] != null) ? dtHeader.Rows[0]["DELIVERYDATE"].ToString() : string.Empty;
            else
                txtDateTrans.Text = DateTime.Now.ToString("dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdSaveTab1_Click(object sender, EventArgs e)
    {
        IsCheckTab3Click = false;
        this.SaveTab1();
    }

    private void SaveTab1()
    {
        try
        {
            this.ValidateSave();

            DataTable dtDocument = ComplainBLL.Instance.ComplainInsertBLL((DocID == null) ? string.Empty : DocID,
                                                                    dtHeader,
                                                                    txtDetail.Value,
                                                                    txtFName.Text,
                                                                    string.Empty,
                                                                    txtFNameDivision.Text,
                                                                    txtDateComplain.Text,
                                                                    "00:00",
                                                                    "1",
                                                                    Session["UserID"].ToString(),
                                                                    Session["UserID"].ToString(),
                                                                    Session["UserID"].ToString(),
                                                                    OTP,
                                                                    1);

            if (string.Equals(dtDocument.Rows[0][0].ToString(), "URGENT"))
            {
                //เอกสารแจ้งล่าช้า
                lblOTP.Text = "แจ้งเตือนจากระบบการแจ้งเรื่องร้องเรียน";
                StringBuilder sbMessage = new StringBuilder();
                //sb.Append("<br />");
                sbMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตามมติคณะกรรมการพิจารณาโทษ ผู้ร้องเรียนสามารถยื่นข้อร้องเรียนและหลักฐานประกอบของเดือน M ได้ภายใน 1st Half (ถึงวันที่ 15) ของเดือน M+1");
                sbMessage.Append(" ท่านได้แจ้งเรื่องล่าช้ากว่าที่กำหนด กรุณาติดต่อผู้บังคับบัญชาของท่านเพื่อขออนุญาตเปิดเรื่องร้องเรียนไปยังผู้จัดการส่วนระบบและประมวลผลการขนส่ง (ผจ.รข.)");
                sbMessage.Append("<br />");
                sbMessage.Append("<br />");
                lblComplainUrgent.Text = sbMessage.ToString();

                lblComplainUrgent2.Text = "หรือกรณีได้รับอนุญาตแล้ว กรุณาใส่รหัสผ่าน OTP เพื่อบันทึกเปิดเรื่องร้องเรียน";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowOTP", "$('#ShowOTP').modal();", true);
                updOTP.Update();
                return;
            }

            if (!string.Equals(OTP, string.Empty))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalOTP();", true);

            txtDocID.Value = dtDocument.Rows[0]["Doc_ID"].ToString();
            DocID = dtDocument.Rows[0]["Doc_ID"].ToString();

            ComplainBLL.Instance.ComplainDelDriverBLL(dtDocument.Rows[0]["Doc_ID"].ToString());

            if (cmbPersonalNo2.SelectedIndex > 0)
            {
                ComplainBLL.Instance.ComplainInsertLockDriverNewBLL(dtDocument.Rows[0]["Doc_ID"].ToString(), cmbPersonalNo2.SelectedValue, Session["UserID"].ToString(), dtHeader.Rows[0]["VENDORID"].ToString());
                ComplainBLL.Instance.ComplainAddDriverBLL(dtDocument.Rows[0]["Doc_ID"].ToString(), cmbPersonalNo2.SelectedValue);
            }
            if (cmbPersonalNo3.SelectedIndex > 0)
            {
                ComplainBLL.Instance.ComplainInsertLockDriverNewBLL(dtDocument.Rows[0]["Doc_ID"].ToString(), cmbPersonalNo3.SelectedValue, Session["UserID"].ToString(), dtHeader.Rows[0]["VENDORID"].ToString());
                ComplainBLL.Instance.ComplainAddDriverBLL(dtDocument.Rows[0]["Doc_ID"].ToString(), cmbPersonalNo3.SelectedValue);
            }
            if (cmbPersonalNo4.SelectedIndex > 0)
            {
                ComplainBLL.Instance.ComplainInsertLockDriverNewBLL(dtDocument.Rows[0]["Doc_ID"].ToString(), cmbPersonalNo4.SelectedValue, Session["UserID"].ToString(), dtHeader.Rows[0]["VENDORID"].ToString());
                ComplainBLL.Instance.ComplainAddDriverBLL(dtDocument.Rows[0]["Doc_ID"].ToString(), cmbPersonalNo4.SelectedValue);
            }
            //รถงับ พขร.

            bool IsLockDriverSAP = false;
            string ErrorSAP = string.Empty;
            try
            {
                this.LockDriver();
                IsLockDriverSAP = true;
            }
            catch (Exception ex)
            {
                ErrorSAP = ex.Message;
                IsLockDriverSAP = false;
            }

            this.LoadData();
            this.CheckCanEdit();

            if (IsLockDriverSAP)
                alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")");
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");

                sb.Append("<tr>");
                sb.Append("<td>");
                alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.Append("<td>");
                alertSuccess("แต่ไม่สามารถระงับ พขร. ใน SAP ได้ เนื่องจาก " + ErrorSAP);
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }

            this.LockAndUnlockDriver(chkLock.Checked);

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdOTP_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = ComplainBLL.Instance.OTPCheckBLL(txtOTP.Value.Trim(), 1);
            if (dt.Rows.Count > 0)
            {
                OTP = dt.Rows[0]["OTP_CODE"].ToString();
                this.SaveTab1();
                cmdSaveTab2.Enabled = true;
            }
            else
            {
                OTP = string.Empty;
                throw new Exception("รหัส OTP ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdOTPCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_Complain_lst_New.aspx");
    }

    protected void cmdCloseTab1_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_Complain_lst_New.aspx");
    }

    protected void cmdCloseTab2_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_Complain_lst_New.aspx");
    }

    protected void cboVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref cboHeadRegist, null, string.Empty, string.Empty, false);        //ทะเบียนรถ
            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //Delivery No
            DropDownListHelper.BindDropDownList(ref cboContract, null, string.Empty, string.Empty, false);          //เลขที่สัญญา
            DropDownListHelper.BindDropDownList(ref cmbPersonalNo, null, string.Empty, string.Empty, false);        //พขร

            if (cboVendor.SelectedIndex > 0)
            {
                dtEmployee = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());
                dtEmployee2 = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());
                dtEmployee3 = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());
                dtEmployee4 = ComplainBLL.Instance.PersonalSelectBLL(this.GetConditionPersonal());

                DropDownListHelper.BindDropDownList(ref cmbPersonalNo, dtEmployee, "SEMPLOYEEID", "FULLNAME", true);
                DropDownListHelper.BindDropDownList(ref cmbPersonalNo2, dtEmployee2, "SEMPLOYEEID", "FULLNAME", true);
                DropDownListHelper.BindDropDownList(ref cmbPersonalNo3, dtEmployee3, "SEMPLOYEEID", "FULLNAME", true);
                DropDownListHelper.BindDropDownList(ref cmbPersonalNo4, dtEmployee4, "SEMPLOYEEID", "FULLNAME", true);

                this.LoadContract(" AND TCONTRACT.SVENDORID = '" + cboVendor.SelectedValue + "'");
                this.TruckSelect();
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cboContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.TruckSelect();
    }

    protected void cboHeadRegist_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.GetCarDetail(string.Empty);
    }

    private void GetCarDetail(string Type)
    {
        try
        {
            if (!String.IsNullOrEmpty(cboHeadRegist.SelectedValue))
            {
                string Condition = String.Format(@" AND TTRUCK.STRUCKID LIKE '%{0}%'", cboHeadRegist.SelectedValue.Replace("'", "''"));
                DropDownListHelper.BindDropDownList(ref cboContract, ComplainBLL.Instance.ContractSelectBLL(this.GetConditionVendor() + Condition), "SCONTRACTID", "SCONTRACTNO", true);
                cboContract.SelectedIndex = 1;
            }
            else
                DropDownListHelper.BindDropDownList(ref cboContract, ComplainBLL.Instance.ContractSelectBLL(this.GetConditionVendor()), "SCONTRACTID", "SCONTRACTNO", true);

            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //Delivery No
            txtTrailerRegist.Value = string.Empty;

            if (!string.Equals(Type, string.Empty) || (cboHeadRegist.SelectedIndex > 0))
            {
                txtTrailerRegist.Value = dtTruck.Rows[cboHeadRegist.SelectedIndex]["STRAILERREGISTERNO"].ToString();
                dtDeliveryNo = ComplainBLL.Instance.OutboundSelectBLL(this.GetConditionVendor() + this.GetConditionContract() + this.GetConditionTruck() + " AND TPLANSCHEDULELIST.SDELIVERYNO IS NOT NULL");
                DropDownListHelper.BindDropDownList(ref cboDelivery, dtDeliveryNo, "SDELIVERYNO", "SDELIVERYNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionPersonal()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboVendor.SelectedIndex > 0)
                sb.Append(" AND TEMPLOYEE.STRANS_ID = '" + cboVendor.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionTruck()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboHeadRegist.SelectedIndex > 0)
                sb.Append(" AND TTRUCK.STRUCKID = '" + cboHeadRegist.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionVendor()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboVendor.SelectedIndex > 0)
                sb.Append(" AND TCONTRACT.SVENDORID = '" + cboVendor.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetConditionContract()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (cboVendor.SelectedIndex > 0)
                sb.Append(" AND TCONTRACT_TRUCK.SCONTRACTID = '" + cboContract.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdSaveTab2_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSaveTab2();
            this.ValidateEmail();
            ComplainImportFileBLL.Instance.ComplainAttachInsertBLL(dtUpload, DocID, int.Parse(Session["UserID"].ToString()), txtOperate.Text.Trim(), txtProtect.Text.Trim(), txtsEmployee.Text.Trim(), ConfigValue.DocStatus3, txtCCEmail.Text.Trim(), "COMPLAIN");

            bool IsLock = false;
            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (string.Equals(dtHeader.Rows[i]["LOCKDRIVERID"].ToString(), "1"))
                {
                    IsLock = true;
                    break;
                }
            }

            if (!IsSpecialDoc)
            {
                if (IsLock)
                    this.SendEmail(ConfigValue.EmailComplainCreateLock);
                else
                    this.SendEmail(ConfigValue.EmailComplainCreateNoLock);
            }

            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst_New.aspx");
            DocStatusID = ConfigValue.DocStatus3;
            this.EnableTab3();

            //cmdSaveTab1.Enabled = false;
            //cmdSaveTab2.Enabled = false;
            //cmdSaveTab2Draft.Enabled = false;
            cmdCancelDoc.Enabled = false;

            dtUploadFlag = ComplainImportFileBLL.Instance.ImportFileSelectBLL(DocID, "COMPLAIN", string.Empty);
            GridViewHelper.BindGridView(ref dgvUploadFile_flag, dtUploadFlag);

            dtAddDriver = ComplainBLL.Instance.ComplainSelectDriverBLL(" AND DOC_ID = '" + DocID + "'");
            GridViewHelper.BindGridView(ref dgvSuspendDriver, dtAddDriver);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSaveTab2()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSaveTab2ReqDoc()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUploadReqDoc.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile2.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile2.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile2.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile2.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUploadReqDoc.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile2.Rows[i]["UPLOAD_ID"].ToString(), dtUploadReqDoc.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile2.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile2.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateEmail()
    {
        if (!string.IsNullOrEmpty(txtCCEmail.Text))
        {
            txtCCEmail.Text = txtCCEmail.Text.Trim();
            var lstEmail = txtCCEmail.Text.Split(',').Select(p => p.Trim()).ToList();
            foreach (var lst in lstEmail)
            {
                if (!IsValidEmail(lst))
                {
                    throw new Exception(string.Format("E-mail ไม่ถูกต้อง \'{0}\'", lst));
                }
            }
        }
    }

    private bool IsValidEmail(string _email)
    {
        return Regex.IsMatch(_email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
    }
    /// <summary>
    /// ใช้สำหรับ ข้อมูลที่มีการบันทึกไปแล้วให้ Remove ออกเพื่อให้ส่ง Email ได้
    /// </summary>
    private void removeInvalidEmailBeforSend()
    {
        if (!string.IsNullOrEmpty(txtCCEmail.Text))
        {
            txtCCEmail.Text = txtCCEmail.Text.Trim();
            var lstEmail = txtCCEmail.Text.Split(',').ToList();
            foreach (var lst in lstEmail)
            {
                if (!IsValidEmail(lst)) lstEmail.Remove(lst);
            }
            txtCCEmail.Text = String.Join(",", lstEmail);
        }
    }

    protected void cmdSaveTab2Draft_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSaveTab2();
            this.ValidateEmail();

            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            if (dtUpload.Rows.Count > 0)
            {
                ComplainImportFileBLL.Instance.ComplainAttachInsertBLL(dtUpload, DocID, int.Parse(dtLogin.Rows[0]["SUID"].ToString()), txtOperate.Text.Trim(), txtProtect.Text.Trim(), txtsEmployee.Text.Trim(), ConfigValue.DocStatus2, txtCCEmail.Text.Trim(), "COMPLAIN");
                ComplainImportFileBLL.Instance.ComplainAttachInsertBLL(dtUpload2, DocID, int.Parse(dtLogin.Rows[0]["SUID"].ToString()), txtOperate.Text.Trim(), txtProtect.Text.Trim(), txtsEmployee.Text.Trim(), ConfigValue.DocStatus2, txtCCEmail.Text.Trim(), "COMPLAIN_REQ");
                DataTable dtReqDoc = new DataTable();
                dtReqDoc.Columns.Add("LINK");
                dtReqDoc.Columns.Add("VENDOR");
                dtReqDoc.Columns.Add("COMPLAIN");
                dtReqDoc.Columns.Add("CONTRACT");
                dtReqDoc.Columns.Add("CAR");
                dtReqDoc.Columns.Add("SOURCE");
                dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
                dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
                string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                dtReqDoc.Rows.Add(Link, cboVendor.SelectedItem, DocID, cboContract.SelectedItem, cboHeadRegist.SelectedItem, txtShipTo.Text.Trim(), txtFNameDivision.Text.Trim(),txtComplainDivisionBy.Value.Trim());
                this.SendEmail(11);
                //MailService.SendMail(11, int.Parse(dtUserLogin.Rows[0]["SYSTEM_ID"].ToString()), DocID, dtUserLogin.Rows[0]["USER_ID"].ToString(), dtReqDoc);
                alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value.Trim() + ")", "admin_Complain_lst_New.aspx");
            }
            else
            {
                throw new Exception("กรุณาแนบเอกสารก่อนทำการบันทึก");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdCancelDoc_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtDriver = ComplainBLL.Instance.ComplainCancelDocBLL(DocID, int.Parse(Session["UserID"].ToString()));

            if (dtDriver.Rows.Count > 0)
            {
                DataTable dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dtDriver.Rows[0]["SEMPLOYEEID"].ToString());

                SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                {
                    DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                    PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                    LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                    LICENSENOE_FROM = string.Equals(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString()).ToString("dd/MM/yyyy"),
                    LICENSENO_TO = string.Equals(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString()).ToString("dd/MM/yyyy"),
                    Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                    Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                    FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                    LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                    CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                    DRV_STATUS = ConfigValue.DriverEnable
                };

                try
                {
                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    //ต้องเอา ID มา ComplainBLL.Instance.DriverUpdateLogBLL(DocID, dtDriver.Rows[0]["ID"].ToString());
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                }
            }
            this.SendEmail(129);
            alertSuccess("ยกเลิกเอกสาร เรียบร้อย", "admin_Complain_lst_New.aspx");
            this.DisplayCancelDoc();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void DisplayCancelDoc()
    {
        try
        {
            cmdSaveTab1.Enabled = false;
            cmdCancelDoc.Enabled = false;
            cmdSaveTab2.Enabled = false;
            cmdSaveTab2Draft.Enabled = false;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void SelectDuplicateCar()
    {
        try
        {
            dtCarDistinct = new DataTable();
            dtCarDistinct.Columns.Add("DELIVERY_DATE");
            dtCarDistinct.Columns.Add("TRUCKID");
            dtCarDistinct.Columns.Add("CARHEAD");
            dtCarDistinct.Columns.Add("CARDETAIL");

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (string.Equals(dtHeader.Rows[i]["TRUCKID"].ToString(), string.Empty))
                    continue;

                if (dtCarDistinct.Rows.Count == 0)
                {
                    dtCarDistinct.Rows.Add(dtHeader.Rows[i]["DELIVERYDATE"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
                else
                {
                    int j;
                    for (j = 0; j < dtCarDistinct.Rows.Count; j++)
                    {
                        if (string.Equals(dtCarDistinct.Rows[j]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString()) && string.Equals(dtCarDistinct.Rows[j]["DELIVERY_DATE"].ToString(), dtHeader.Rows[i]["DELIVERYDATE"].ToString()))
                            break;
                    }
                    if ((j == dtCarDistinct.Rows.Count) && ((!string.Equals(dtCarDistinct.Rows[j - 1]["TRUCKID"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString())) || (!string.Equals(dtCarDistinct.Rows[j - 1]["DELIVERY_DATE"].ToString(), dtHeader.Rows[i]["DELIVERYDATE"].ToString()))))
                        dtCarDistinct.Rows.Add(dtHeader.Rows[i]["DELIVERYDATE"].ToString(), dtHeader.Rows[i]["TRUCKID"].ToString(), dtHeader.Rows[i]["CARHEAD"].ToString(), dtHeader.Rows[i]["CARDETAIL"].ToString());
                }
            }

            GridViewHelper.BindGridView(ref dgvScore, dtCarDistinct);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cboScore_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.EnableScore(false);

            if (cboScore.SelectedIndex == 0)
            {
                this.ClearScreenTab3();
            }
            else
            {
                txtCost.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_COST"].ToString();
                txtDisableDriver.Text = (dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString() == string.Empty) ? "0" : dtScore.Rows[cboScore.SelectedIndex]["TOTAL_BREAK"].ToString();
                txtScore1.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL02"].ToString();
                txtScore2.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL03"].ToString();
                txtScore3.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL04"].ToString();
                txtScore4.Text = dtScore.Rows[cboScore.SelectedIndex]["SCOL06"].ToString();
                txtScore5.Text = dtScore.Rows[cboScore.SelectedIndex]["NMULTIPLEIMPACT"].ToString();
                txtScore6.Text = dtScore.Rows[cboScore.SelectedIndex]["NPOINT"].ToString();
                chkBlacklistTopic.Checked = dtScore.Rows[cboScore.SelectedIndex]["BLACKLIST"].ToString() == "1" ? true : false;
                lblShowOption.Visible = (dtScore.Rows[cboScore.SelectedIndex]["IS_CORRUPT"].ToString() == "0") ? false : true;

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("น้ำมัน"))
                {
                    txtOilPrice.Text = "0";
                    txtOilQty.Text = "0";
                    rowOil.Visible = true;
                }
                else
                {
                    txtOilPrice.Text = string.Empty;
                    txtOilQty.Text = string.Empty;
                    rowOil.Visible = false;
                }

                if (dtScore.Rows[cboScore.SelectedIndex]["TTOPIC_TYPE_NAME"].ToString().Contains("ซีล"))
                {
                    txtZeal.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString();
                    txtZealQty.Text = "0";
                    rowZeal.Visible = true;
                }
                else
                {
                    txtZeal.Text = string.Empty;
                    txtZealQty.Text = string.Empty;
                    rowZeal.Visible = false;
                }

                if ((!lblShowOption.Visible) && (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["CAN_EDIT"].ToString(), "1")))
                {//สามารถแก้ไขคะแนนได้
                    this.EnableScore(true);
                }

                if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["BLACKLIST"].ToString(), "1"))
                {
                    chkBlacklist.Checked = true;
                    chkBlacklist.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void EnableScore(bool Enable)
    {
        txtScore1.Enabled = Enable;
        txtScore2.Enabled = Enable;
        txtScore3.Enabled = Enable;
        txtScore4.Enabled = Enable;
    }
    protected void chkChooseScoreAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void chkChooseScoreListCarAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ChkAll = (CheckBox)sender;
            bool Checked = ChkAll.Checked;
            CheckBox Chk;

            for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = Checked;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdAddScore_Click(object sender, EventArgs e)
    {
        try
        {
            int CountCar = this.ValidateAddScore();

            string Driver = string.Empty;
            string CostDisplay = string.Empty;
            string Score6Display = string.Empty;
            string DisableDriverDisplay = string.Empty;
            string Blacklist = string.Empty;

            CostDisplay = txtCost.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString();
            Score6Display = txtScore6.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString();
            DisableDriverDisplay = txtDisableDriver.Text.Trim() + " x " + CountCar.ToString() + " = " + (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString();
            Blacklist = chkBlacklistTopic.Checked == true ? "Yes" : "";

            for (int i = 0; i < chkSuspainDriver.Items.Count; i++)
            {
                if (chkSuspainDriver.Items[i].Selected)
                {
                    if (Driver == string.Empty)
                        Driver = chkSuspainDriver.Items[i].ToString();
                    else
                        Driver = Driver + "," + chkSuspainDriver.Items[i].ToString();
                }
            }

            dtScoreList.Rows.Add(cboScore.SelectedValue, cboScore.SelectedItem, (decimal.Parse(txtCost.Text.Trim()) * CountCar).ToString(), CostDisplay, Driver, (decimal.Parse(txtDisableDriver.Text.Trim()) * CountCar).ToString(), DisableDriverDisplay, txtScore1.Text.Trim(), txtScore2.Text.Trim(), txtScore3.Text.Trim(), txtScore4.Text.Trim(), txtScore5.Text.Trim(), (decimal.Parse(txtScore6.Text.Trim()) * CountCar).ToString(), Score6Display, CountCar, Blacklist);

            CheckBox Chk;
            if (rowListTotalCarTab3.Visible)
            {//ป้อนจำนวนคัน
                TextBox Txt;

                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                    {
                        Txt = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, string.Empty, string.Empty, string.Empty, dtDup.Rows[i]["DELIVERY_DATE"].ToString(), Txt.Text.Trim());
                    }
                }
            }
            else
            {//เลือกทะเบียนรถ
                for (int i = 0; i < dgvScore.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                    if (Chk.Checked)
                        dtScoreListCar.Rows.Add(cboScore.SelectedValue, dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtCarDistinct.Rows[i]["CARHEAD"].ToString(), dtCarDistinct.Rows[i]["CARDETAIL"].ToString(), dtCarDistinct.Rows[i]["DELIVERY_DATE"].ToString(), 0);
                }
            }

            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);

            DataTable dtt = new DataTable();

            if (dgvScoreList.Rows.Count > 0)
                dtt = (DataTable)dgvScoreList.DataSource;

            for (int i = 0; i < dtSuspendDriver.Rows.Count; i++)
            {
                string sum = dtt.Compute("SUM(DisableDriver)", "SuspendDriver Like '%" + dtSuspendDriver.Rows[i]["FULLNAME"].ToString() + "%'").ToString();
                dtSuspendDriver.Rows[i]["DAYS"] = (sum == string.Empty ? "0" : sum);
            }

            for (int i = 0; i < dtSuspendDriver.Rows.Count; i++)
            {
                string sum = dtt.Select("BLACKLIST = 'Yes' AND SuspendDriver Like '%" + dtSuspendDriver.Rows[i]["FULLNAME"].ToString() + "%'").Length.ToString();
                dtSuspendDriver.Rows[i]["Blacklist"] = sum;
            }

            GridViewHelper.BindGridView(ref dgvSuspendDriver, dtSuspendDriver, false);
            this.ClearScreenTab3();
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ClearScreenTab3()
    {
        try
        {
            cboScore.SelectedIndex = 0;
            txtCost.Text = string.Empty;
            txtDisableDriver.Text = string.Empty;
            chkSuspainDriver.ClearSelection();
            txtScore1.Text = string.Empty;
            txtScore2.Text = string.Empty;
            txtScore3.Text = string.Empty;
            txtScore4.Text = string.Empty;
            txtScore5.Text = string.Empty;
            txtScore6.Text = string.Empty;
            lblShowOption.Visible = false;

            txtOilPrice.Text = string.Empty;
            txtOilQty.Text = string.Empty;
            txtZeal.Text = string.Empty;
            txtZealQty.Text = string.Empty;

            rowOil.Visible = false;
            rowZeal.Visible = false;

            this.EnableScore(false);

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                Chk.Checked = true;
            }
            ((CheckBox)this.FindControlRecursive(dgvScore, "chkChooseScoreAll")).Checked = true;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private int ValidateAddScore()
    {
        try
        {
            if (cboScore.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก หัวข้อตัดคะแนน");

            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (string.Equals(dtScoreList.Rows[i]["STOPICID"].ToString(), cboScore.SelectedValue))
                    throw new Exception("เลือกหัวข้อ ตัดคะแนน ซ้ำกัน");
            }

            if (string.Equals(txtScore1.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ภาพลักษณ์องค์กร");

            if (string.Equals(txtScore2.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ความพึงพอใจลูกค้า");

            if (string.Equals(txtScore3.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน กฎ/ระเบียบฯ");

            if (string.Equals(txtScore4.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน แผนงานขนส่ง");

            if (rowOil.Visible)
            {
                if (string.Equals(txtOilQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนน้ำมัน");

                if (string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน ค่าน้ำมัน");
            }

            if (rowZeal.Visible)
            {
                if (string.Equals(txtZealQty.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน จำนวนซิล");
            }

            if (rowListTotalCarTab3.Visible)
            {
                //if (string.Equals(txtTotalCarTab3.Text.Trim(), string.Empty))
                //    throw new Exception("กรุณาป้อน จำนวนรถ (คัน)");

                //int tmp;
                //if (!int.TryParse(txtTotalCarTab3.Text.Trim(), out tmp))
                //    throw new Exception("กรุณาป้อน จำนวนรถ (คัน) เป็นตัวเลข");

                //if (SumCar < int.Parse(txtTotalCarTab3.Text.Trim()))
                //    throw new Exception("จำนวนรถที่ป้อน มีค่ามากกว่า จำนวนรถที่ถูกร้องเรียน");

                CheckBox Chk;
                TextBox Txt1;
                int MaxNumber = 0;
                int tmp;
                int TotalCar = 0;
                for (int i = 0; i < dgvScoreTotalCar.Rows.Count; i++)
                {
                    Chk = (CheckBox)dgvScoreTotalCar.Rows[i].FindControl("chkChooseScore");

                    if (Chk.Checked)
                    {
                        MaxNumber = int.Parse(dtDup.Rows[i]["TOTAL_CAR"].ToString());

                        Txt1 = (TextBox)dgvScoreTotalCar.Rows[i].FindControl("txtChooseTotalCar");
                        if (string.Equals(Txt1.Text.Trim(), string.Empty))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน)");
                        if (!int.TryParse(Txt1.Text.Trim(), out tmp))
                            throw new Exception("กรุณาป้อน จำนวนรถ (หักคะแนน) เป็นตัวเลข");
                        if (MaxNumber < int.Parse(Txt1.Text.Trim()))
                            throw new Exception("จำนวนรถที่ป้อน มีค่ามากกว่า จำนวนรถที่ถูกร้องเรียน");

                        TotalCar += int.Parse(Txt1.Text.Trim());
                    }
                }
                return TotalCar;
            }
            else
                return this.CountCar();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private int CountCar()
    {
        try
        {
            int CountCar = 0;

            CheckBox Chk;
            for (int i = 0; i < dgvScore.Rows.Count; i++)
            {
                Chk = (CheckBox)dgvScore.Rows[i].FindControl("chkChooseScore");
                if (Chk.Checked)
                {
                    CountCar += 1;
                }
            }

            if (CountCar == 0)
                throw new Exception("กรุณาเลือก ทะเบียนรถ ที่ต้องการตัดคะแนน");

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvScoreList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            IndexEdit = Index;

            if (string.Equals(e.CommandName, "Select"))
            {
                DataTable dtTmp = dtScoreList.Copy();
                //string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ภาพลักษณ์องค์กร", "ความพึงพอใจลูกค้า", "กฎ/ระเบียบฯ", "แผนงานขนส่ง", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };

                string[] display = { "หัวข้อตัดคะแนน", "ค่าปรับ", "ระงับ พขร. (วัน)", "ผลคูณความรุนแรง", "หักคะแนนต่อครั้ง" };
                dtTmp.Columns["STOPICNAME"].ColumnName = "หัวข้อตัดคะแนน";
                dtTmp.Columns["Cost"].ColumnName = "ค่าปรับ";
                dtTmp.Columns["DisableDriver"].ColumnName = "ระงับ พขร. (วัน)";
                //dtTmp.Columns["Score1"].ColumnName = "ภาพลักษณ์องค์กร";
                //dtTmp.Columns["Score2"].ColumnName = "ความพึงพอใจลูกค้า";
                //dtTmp.Columns["Score3"].ColumnName = "กฎ/ระเบียบฯ";
                //dtTmp.Columns["Score4"].ColumnName = "แผนงานขนส่ง";
                dtTmp.Columns["Score5"].ColumnName = "ผลคูณความรุนแรง";
                dtTmp.Columns["Score6"].ColumnName = "หักคะแนนต่อครั้ง";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table>");
                for (int i = 0; i < dtTmp.Columns.Count; i++)
                {
                    if (display.Contains(dtTmp.Columns[i].ColumnName))
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dtTmp.Columns[i].ColumnName + " : " + dtTmp.Rows[Index][i].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    if (i == dtTmp.Columns.Count - 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>");
                        //sb.Append("ทะเบียนรถ : " + this.GetCarList(dtTmp.Rows[Index]["STOPICID"].ToString()));
                        if (rowListTotalCarTab3.Visible)
                        {
                            //sb.Append("จำนวนรถ : " + dtTmp.Rows[Index]["TotalCar"].ToString() + " คัน");
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        else
                        {
                            sb.Append("จำนวนรถ : " + this.GetCarListCount(dtTmp.Rows[Index]["STOPICID"].ToString()) + " คัน");
                            sb.Append(" (" + this.GetCarListString(dtTmp.Rows[Index]["STOPICID"].ToString()) + ")");
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
            //else if (string.Equals(e.CommandName, "EditHeader"))
            //{
            //    ServiceIDEdit = dtHeader.Rows[Index]["SSERVICEID"].ToString();
            //    cmdAddHeader.Text = "บันทึก";
            //    cmdCancel.Visible = true;

            //    cmbTopic.SelectedValue = dtHeader.Rows[Index]["TOPICID"].ToString();
            //    cmbTopic_SelectedIndexChanged(null, null);

            //    cboComplainType.SelectedValue = dtHeader.Rows[Index]["COMPLAINID"].ToString();
            //    cbxOrganiz.SelectedValue = dtHeader.Rows[Index]["ORGANIZID"].ToString();
            //    chkLock.Checked = (string.Equals(dtHeader.Rows[Index]["LOCKDRIVERID"].ToString(), "0") ? false : true);
            //    rblTypeProduct.Value = dtHeader.Rows[Index]["TYPEPRODUCTID"].ToString();
            //    txtTrailerRegist.Value = dtHeader.Rows[Index]["CARDETAIL"].ToString();

            //    cboVendor.SelectedValue = dtHeader.Rows[Index]["VENDORID"].ToString();
            //    cboVendor_SelectedIndexChanged(null, null);

            //    cboContract.SelectedValue = dtHeader.Rows[Index]["CONTRACTID"].ToString();
            //    cboContract_SelectedIndexChanged(null, null);

            //    cboHeadRegist.SelectedValue = dtHeader.Rows[Index]["TRUCKID"].ToString();
            //    cboHeadRegist_SelectedIndexChanged(null, null);

            //    if (!string.Equals(dtHeader.Rows[Index]["DELIVERYID"].ToString(), string.Empty))
            //        cboDelivery.SelectedValue = dtHeader.Rows[Index]["DELIVERYID"].ToString();

            //    cmbPersonalNo.SelectedValue = dtHeader.Rows[Index]["PERSONALID"].ToString();
            //    txtDateTrans.Text = (dtHeader.Rows[Index]["DELIVERYDATE"] != null) ? dtHeader.Rows[Index]["DELIVERYDATE"].ToString() : string.Empty;
            //    //teTimeTrans.Text = dtHeader.Rows[Index]["DELIVERYTIME"].ToString();
            //    txtComplainAddress.Value = dtHeader.Rows[Index]["COMPLAINADDRESS"].ToString();
            //    txtTotalCar.Value = dtHeader.Rows[Index]["TOTALCAR"].ToString();
            //}
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private int GetCarListCount(string STOPICID)
    {
        try
        {
            int CountCar = 0;
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (rowListTotalCarTab3.Visible)
                        CountCar += int.Parse(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                    else
                        CountCar += 1;
                }
            }

            return CountCar;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetCarListString(string STOPICID)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
                {
                    if (sb.Length > 0)
                        sb.Append(", ");

                    if (rowListTotalCarTab3.Visible)
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
                        sb.Append(" คัน");
                    }
                    else
                    {
                        sb.Append(dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString());
                        sb.Append("/");
                        sb.Append(dtScoreListCar.Rows[i]["CARHEAD"].ToString());
                        if (!string.Equals(dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), string.Empty))
                        {
                            sb.Append("/");
                            sb.Append(dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
                        }
                    }
                }
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    //private string GetCarList(string STOPICID)
    //{
    //    try
    //    {
    //        StringBuilder sb = new StringBuilder();
    //        for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
    //        {
    //            if (string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), STOPICID))
    //            {
    //                sb.Append(",");
    //                sb.Append(dtScoreListCar.Rows[i]["CARHEAD"].ToString());

    //                if (!string.Equals(dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), string.Empty))
    //                {
    //                    sb.Append(" ");
    //                    sb.Append(dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
    //                }
    //            }
    //        }
    //        if (sb.Length > 0)
    //            sb = sb.Remove(0, 1);

    //        return sb.ToString();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(RemoveSpecialCharacters(ex.Message));
    //    }
    //}

    protected void dgvScore_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dtTmp = dtScoreListCar.Copy();
            dtTmp.Rows.Clear();

            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                //if (!string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[e.RowIndex]["STOPICID"].ToString()))
                //    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString());
                if (i != e.RowIndex)
                    dtTmp.Rows.Add(dtScoreListCar.Rows[i]["STOPICID"].ToString(), dtScoreListCar.Rows[i]["TRUCKID"].ToString(), dtScoreListCar.Rows[i]["CARHEAD"].ToString(), dtScoreListCar.Rows[i]["CARDETAIL"].ToString(), dtScoreListCar.Rows[i]["DELIVERY_DATE"].ToString(), dtScoreListCar.Rows[i]["TOTAL_CAR"].ToString());
            }
            dtScoreListCar = dtTmp;

            dtScoreList.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvScoreList, dtScoreList);


            DataTable dtt = new DataTable();

            if (dgvScoreList.Rows.Count > 0)
                dtt = (DataTable)dgvScoreList.DataSource;
            else
            {
                for (int i = 0; i < dtSuspendDriver.Rows.Count; i++)
                {
                    dtSuspendDriver.Rows[i]["DAYS"] = 0;
                    dtSuspendDriver.Rows[i]["Blacklist"] = 0;
                }
            }

            if (dtt != null && dtt.Rows.Count > 0)
            {
                for (int i = 0; i < dtSuspendDriver.Rows.Count; i++)
                {
                    string sum = dtt.Compute("SUM(DisableDriver)", "SuspendDriver Like '%" + dtSuspendDriver.Rows[i]["FULLNAME"].ToString() + "%'").ToString();
                    dtSuspendDriver.Rows[i]["DAYS"] = (sum == string.Empty ? "0" : sum);
                }

                for (int i = 0; i < dtSuspendDriver.Rows.Count; i++)
                {
                    string sum = dtt.Select("BLACKLIST = 'Yes' AND SuspendDriver Like '%" + dtSuspendDriver.Rows[i]["FULLNAME"].ToString() + "%'").Length.ToString();
                    dtSuspendDriver.Rows[i]["Blacklist"] = sum;
                }
            }

            GridViewHelper.BindGridView(ref dgvSuspendDriver, dtSuspendDriver, false);
            this.CheckBlacklist();
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void CheckBlacklist()
    {
        try
        {
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (this.CheckTopicBlacklist(dtScoreList.Rows[i]["STOPICID"].ToString()))
                {
                    chkBlacklist.Enabled = true;
                    return;
                }
            }

            chkBlacklist.Checked = false;
            chkBlacklist.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private bool CheckTopicBlacklist(string STOPICID)
    {
        try
        {
            for (int i = 0; i < dtScore.Rows.Count; i++)
            {
                if (string.Equals(dtScore.Rows[i]["STOPICID"].ToString(), STOPICID) && string.Equals(dtScore.Rows[i]["BLACKLIST"].ToString(), "1"))
                    return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void ddlScore1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore1", "txtScore1Final", "Score1");
    }

    protected void ddlScore2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore2", "txtScore2Final", "Score2");
    }

    protected void ddlScore3_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore3", "txtScore3Final", "Score3");
    }

    protected void ddlScore4_SelectedIndexChanged(object sender, EventArgs e)
    {
        //this.CalculateScore("ddlScore4", "txtScore4Final", "Score4");
    }

    private void CalculateScore(string ddlScoreName, string txtScoreName, string ColumnName)
    {
        try
        {
            //DropDownList ddlScore = (DropDownList)this.FindControlRecursive(divScore, ddlScoreName);
            //TextBox txtScore = (TextBox)this.FindControlRecursive(divScore, txtScoreName);

            //txtScore.ReadOnly = true;

            //if (ddlScore.SelectedIndex == 0)
            //{
            //    txtScore.Text = string.Empty;
            //}
            //else
            //{
            //    switch (ddlScore.SelectedItem.ToString())
            //    {
            //        case "MAX": txtScore.Text = this.FindMaxScore(ColumnName).ToString(); break;
            //        case "SUM": txtScore.Text = this.FindSumScore(ColumnName).ToString(); break;
            //        case "CUSTOM": txtScore.ReadOnly = false;
            //                       txtScore.Text = string.Empty;
            //                       MsgAlert.ajaxFocus(txtScore.ClientID, Page); break;
            //        default:
            //            break;
            //    }
            //}
            //this.CalulateTotal();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private int FindMaxScore(string ColumnName)
    {
        try
        {
            int max = 0;
            int tmp = 0;

            for (int i = 0; i < dtCarDistinct.Rows.Count; i++)
            {
                for (int j = 0; j < dtScoreList.Rows.Count; j++)
                {
                    if (this.Check(dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtScoreList.Rows[j]["STOPICID"].ToString()) && (int.Parse(dtScoreList.Rows[j][ColumnName].ToString()) > tmp))
                    {
                        tmp = int.Parse(dtScoreList.Rows[j][ColumnName].ToString());
                    }
                }
                max += tmp;
                tmp = 0;
            }

            return max;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private bool Check(string TruckID, string TopicID)
    {
        try
        {
            for (int i = 0; i < dtScoreListCar.Rows.Count; i++)
            {
                if (string.Equals(dtScoreListCar.Rows[i]["TRUCKID"].ToString(), TruckID) && string.Equals(dtScoreListCar.Rows[i]["STOPICID"].ToString(), TopicID))
                    return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private int FindSumScore(string ColumnName)
    {
        try
        {
            int sum = 0;
            int tmp = 0;

            for (int i = 0; i < dtCarDistinct.Rows.Count; i++)
            {
                for (int j = 0; j < dtScoreList.Rows.Count; j++)
                {
                    if (this.Check(dtCarDistinct.Rows[i]["TRUCKID"].ToString(), dtScoreList.Rows[j]["STOPICID"].ToString()))
                    {
                        tmp += int.Parse(dtScoreList.Rows[j][ColumnName].ToString());
                    }
                }
                sum += tmp;
                tmp = 0;
            }

            return sum;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void CalulateTotal()
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtScore1.Text.Trim(), out tmp) || (decimal.Parse(txtScore1.Text.Trim()) < 0))
                txtScore1.Text = "0";

            if (!decimal.TryParse(txtScore2.Text.Trim(), out tmp) || (decimal.Parse(txtScore2.Text.Trim()) < 0))
                txtScore2.Text = "0";

            if (!decimal.TryParse(txtScore3.Text.Trim(), out tmp) || (decimal.Parse(txtScore3.Text.Trim()) < 0))
                txtScore3.Text = "0";

            if (!decimal.TryParse(txtScore4.Text.Trim(), out tmp) || (decimal.Parse(txtScore4.Text.Trim()) < 0))
                txtScore4.Text = "0";

            int sum = 1;
            sum *= (txtScore1.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore1.Text.Trim());
            sum *= (txtScore2.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore2.Text.Trim());
            sum *= (txtScore3.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore3.Text.Trim());
            sum *= (txtScore4.Text.Trim() == string.Empty) ? 0 : int.Parse(txtScore4.Text.Trim());
            txtScore5.Text = sum.ToString();

            DataTable dt = ComplainBLL.Instance.SelectScorePointBLL(txtScore5.Text.Trim());
            if (dt.Rows.Count > 0)
                txtScore6.Text = dt.Rows[0]["NSCORE"].ToString();
            else
                txtScore6.Text = "0";
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void RefreshCusScore()
    {
        try
        {
            if (!string.Equals(radCusScoreType.SelectedValue, string.Empty))
            {
                int row = this.GetSelectRow(radCusScoreType.SelectedValue);

                txtPointFinal.Enabled = false;
                txtCostFinal.Enabled = false;
                txtDisableFinal.Enabled = false;

                lblShowSumPoint.Text = "(ผลรวม : {0})";
                lblShowSumCost.Text = "(ผลรวม : {0})";
                lblShowSumDisable.Text = "(ผลรวม : {0})";

                lblShowSumPoint.Visible = false;
                lblShowSumCost.Visible = false;
                lblShowSumDisable.Visible = false;

                string CusType = dtCusScoreType.Rows[row]["VALUE_TYPE"].ToString();
                switch (CusType)
                {
                    case "MAX": this.FindMax(); break;
                    case "SUM": this.FindSum("Normal"); break;
                    case "FREETEXT":
                        lblShowSumPoint.Visible = true;
                        lblShowSumCost.Visible = true;
                        lblShowSumDisable.Visible = true;
                        this.FreeText(); break;
                    default:
                        break;
                }
            }
            else
            {
                //txtPointFinal.Text = "0";
                //txtCostFinal.Text = "0";
                //txtDisableFinal.Text = "0";
            }

            //this.CalculateScore("ddlScore1", "txtScore1Final", "Score1");
            //this.CalculateScore("ddlScore2", "txtScore2Final", "Score2");
            //this.CalculateScore("ddlScore3", "txtScore3Final", "Score3");
            //this.CalculateScore("ddlScore4", "txtScore4Final", "Score4");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void txtScore1_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void txtScore2_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void txtScore3_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void txtScore4_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            this.CalulateTotal();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdSaveTab3Draft_Click(object senderf, EventArgs e)
    {
        try
        {
            this.SaveTab3(false, (chkSaveTab3Draft.Checked) ? ConfigValue.DocStatus4 : ConfigValue.DocStatus3);
            TextBox txtA = new TextBox();
            TextBox txtB = new TextBox();
            CheckBox chk = new CheckBox();
            CheckBox chkbl = new CheckBox();

            for (int i = 0; i < dgvSuspendDriver.Rows.Count; i++)
            {
                chk = (CheckBox)dgvSuspendDriver.Rows[i].FindControl("chkSuspend");
                txtA = (TextBox)dgvSuspendDriver.Rows[i].FindControl("txtDays");
                txtB = (TextBox)dgvSuspendDriver.Rows[i].FindControl("txtRemark");
                chkbl = (CheckBox)dgvSuspendDriver.Rows[i].FindControl("chkBlacklistDriver");

                int active = chk.Checked == true ? 1 : 0;
                int blacklist = chkbl.Checked == true ? 1 : 0;
                string days = txtA.Text;
                string remark = txtB.Text;

                if (dgvSuspendDriver.DataKeys[i]["ID"].ToString() == "1")
                    ComplainBLL.Instance.ComplainUpdateOldDriverBLL(txtDocID.Value, dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), active, days, remark, blacklist);
                else
                    ComplainBLL.Instance.ComplainUpdateDriverBLL(txtDocID.Value, dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), active, days, remark, blacklist);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdUploadTab3_Click(object sender, EventArgs e)
    {
        this.StartUploadTab3();
    }

    private void StartUploadTab3()
    {
        try
        {
            if (cboUploadTypeTab3.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUploadTab3.HasFile)
            {
                string FileNameUser = fileUploadTab3.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFileTab3(System.IO.Path.GetExtension(FileNameUser), fileUploadTab3.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadTab3.SaveAs(Path + "\\" + FileNameSystem);

                dtUploadTab3.Rows.Add(int.Parse(cboUploadTypeTab3.SelectedValue.ToString()), cboUploadTypeTab3.SelectedItem.Text, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateUploadFileTab3(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeTab3.Rows[cboUploadTypeTab3.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvUploadFileTab3_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadTab3.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvUploadFileTab3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFileTab3.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    protected void dgvUploadFileTab3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                if ("" + Session["CheckPermission"] == "1")
                //if (!CanWrite)
                {
                    imgDelete.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void txtZealQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            int tmp;
            if (!int.TryParse(txtZealQty.Text.Trim(), out tmp) || decimal.Parse(txtZealQty.Text.Trim()) < 0)
            {
                txtZealQty.Text = "0";
                txtCost.Text = "0";
                return;
            }

            txtZealQty.Text = int.Parse(txtZealQty.Text.Trim()).ToString();
            txtCost.Text = (int.Parse(txtZealQty.Text.Trim()) * int.Parse(txtZeal.Text.Trim())).ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void txtOilQty_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            decimal tmp;
            if (!decimal.TryParse(txtOilQty.Text.Trim(), out tmp) || decimal.Parse(txtOilQty.Text.Trim()) < 0)
                txtOilQty.Text = "0";

            if (decimal.Parse(txtOilQty.Text.Trim()) > 0)
            {
                if (string.Equals(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString(), "0") && decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
                else if (!string.Equals(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString(), "0") && (decimal.Parse(txtOilQty.Text.Trim()) < decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString()) || decimal.Parse(txtOilQty.Text.Trim()) > decimal.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE4"].ToString())))
                    txtOilQty.Text = dtScore.Rows[cboScore.SelectedIndex]["VALUE3"].ToString();
            }

            txtOilQty.Text = decimal.Parse(txtOilQty.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void txtOilPrice_OnKeyPress(string ctrlName, string args)
    {
        try
        {
            txtOilPrice.ReadOnly = true;
            decimal tmp;
            if (!decimal.TryParse(txtOilPrice.Text.Trim(), out tmp) || decimal.Parse(txtOilPrice.Text.Trim()) < 0)
                txtOilPrice.Text = "0";

            txtOilPrice.Text = decimal.Parse(txtOilPrice.Text.Trim()).ToString();
            this.CalculateOil();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
        finally
        {
            txtOilPrice.ReadOnly = false;
        }
    }

    private void CalculateOil()
    {
        try
        {
            if (!string.Equals(txtOilQty.Text.Trim(), string.Empty) && !string.Equals(txtOilPrice.Text.Trim(), string.Empty))
                txtCost.Text = (int.Parse(dtScore.Rows[cboScore.SelectedIndex]["VALUE2"].ToString()) * decimal.Parse(txtOilPrice.Text.Trim()) * decimal.Parse(txtOilQty.Text.Trim())).ToString();
            else
                txtCost.Text = "0";
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void GenerateTable(int Rows, int Cols)
    {
        //Panel pnl;
        //for (int i = 0; i < Rows; i++)
        //{
        //    pnl = new Panel();
        //    pnl.EnableViewState = true;
        //    for (int j = 0; j < Cols; j++)
        //    {
        //        if (j != 0)
        //        {
        //            Label lbl = new Label();
        //            lbl.EnableViewState = true;
        //            lbl.Width = 20;
        //            lbl.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //            pnl.Controls.Add(lbl);
        //        }

        //        TextBox txt = new TextBox();
        //        txt.Width = 150;
        //        txt.EnableViewState = true;
        //        //txt.Text = "TxT" + (i + 1).ToString() + "_" + (j + 1).ToString();
        //        txt.ID = "TxT" + (i + 1).ToString() + "_" + (j + 1).ToString();
        //        pnl.Controls.Add(txt);
        //    }
        //    pnlMain.Controls.Add(pnl);
        //}

        //this.FindControlRecursive(Page, "TxT1_1");
        //this.FindControlRecursive(Page, "TxT1_2");
    }

    protected void cmdTest_Click(object sender, EventArgs e)
    {
        //string data = ((TextBox)pnlMain.Controls[0].FindControl("TxT1_1")).Text;
        //alertSuccess(data);
    }
    protected void radCusScoreType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.RefreshCusScore();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void FindMax()
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString()) > PointMax)
                    PointMax = decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            }
            txtPointFinal.Text = PointMax.ToString();

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString()) > CostMax)
                    CostMax = decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            }
            txtCostFinal.Text = CostMax.ToString();

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
            {
                if (decimal.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString()) > DisableDriverMax)
                    DisableDriverMax = int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            }

            string num = "0";
            TextBox txtPersonalLock = new TextBox();
            for (int i = 0; i < dgvSuspendDriver.Rows.Count; i++)
            {
                txtPersonalLock = (TextBox)dgvSuspendDriver.Rows[i].FindControl("txtDays");
                num = ((string.Equals(txtPersonalLock.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtPersonalLock.Text.Trim())) > int.Parse(num) ? ((string.Equals(txtPersonalLock.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtPersonalLock.Text.Trim())).ToString() : num;
            }

            txtDisableFinal.Text = num;//DisableDriverMax.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void FindSum(string ShowType)
    {
        try
        {
            decimal PointMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                PointMax += decimal.Parse(dtScoreList.Rows[i]["Score6"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtPointFinal.Text = PointMax.ToString();
            else
                lblShowSumPoint.Text = string.Format(lblShowSumPoint.Text, PointMax.ToString());

            decimal CostMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                CostMax += decimal.Parse(dtScoreList.Rows[i]["Cost"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtCostFinal.Text = CostMax.ToString();
            else
                lblShowSumCost.Text = string.Format(lblShowSumCost.Text, CostMax.ToString());

            int DisableDriverMax = 0;
            for (int i = 0; i < dtScoreList.Rows.Count; i++)
                DisableDriverMax += int.Parse(dtScoreList.Rows[i]["DisableDriver"].ToString());
            if (string.Equals(ShowType, "Normal"))
                txtDisableFinal.Text = DisableDriverMax.ToString();
            else
            {
                string num = "0";
                TextBox txtPersonalLock = new TextBox();
                for (int i = 0; i < dgvSuspendDriver.Rows.Count; i++)
                {
                    txtPersonalLock = (TextBox)dgvSuspendDriver.Rows[i].FindControl("txtDays");
                    num = ((string.Equals(txtPersonalLock.Text.Trim(), string.Empty)) ? 0 + int.Parse(num) : int.Parse(txtPersonalLock.Text.Trim()) + int.Parse(num)).ToString();
                }
                lblShowSumDisable.Text = string.Format(lblShowSumDisable.Text, num);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void FreeText()
    {
        try
        {
            txtPointFinal.Text = string.Empty;
            txtCostFinal.Text = string.Empty;
            txtDisableFinal.Text = string.Empty;

            this.FindSum("ForFreeText");

            txtPointFinal.Enabled = true;
            txtCostFinal.Enabled = true;
            txtDisableFinal.Enabled = true;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private int GetSelectRow(string Value)
    {
        try
        {
            for (int i = 0; i < dtCusScoreType.Rows.Count; i++)
            {
                if (string.Equals(dtCusScoreType.Rows[i]["CUSSCORE_TYPE_ID"].ToString(), Value))
                    return i;
            }

            return -1;
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdSaveTab3_Click(object sender, EventArgs e)
    {
        try
        {
            IsCheckTab3Click = true;
            this.SaveTab3(true, ConfigValue.DocStatus5);

            TextBox txtA = new TextBox();
            TextBox txtB = new TextBox();
            CheckBox chk = new CheckBox();
            CheckBox chkbl = new CheckBox();

            for (int i = 0; i < dgvSuspendDriver.Rows.Count; i++)
            {
                chk = (CheckBox)dgvSuspendDriver.Rows[i].FindControl("chkSuspend");
                txtA = (TextBox)dgvSuspendDriver.Rows[i].FindControl("txtDays");
                txtB = (TextBox)dgvSuspendDriver.Rows[i].FindControl("txtRemark");
                chkbl = (CheckBox)dgvSuspendDriver.Rows[i].FindControl("chkBlacklistDriver");

                int active = chk.Checked == true ? 1 : 0;
                int blacklist = chkbl.Checked == true ? 1 : 0;
                string days = txtA.Text;
                string remark = txtB.Text;

                if (dgvSuspendDriver.DataKeys[i]["ID"].ToString() == "1")
                    ComplainBLL.Instance.ComplainUpdateOldDriverBLL(txtDocID.Value, dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), active, days, remark, blacklist);
                else
                    ComplainBLL.Instance.ComplainUpdateDriverBLL(txtDocID.Value, dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), active, days, remark, blacklist);
            }

            this.LockAndUnlockDriver(false);

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void SaveTab3(bool isValiDate, int DocStatusID)
    {
        try
        {
            this.ValidateEmail();

            int isSaveCusScore = 0;
            if (isValiDate)
            {
                this.ValidateSaveTab3();
                isSaveCusScore = 1;
            }

            decimal tmp2;
            if (!string.Equals(txtCostOther.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtCostOther.Text.Trim(), out tmp2))
                    throw new Exception("กรุณาป้อน ค่าปรับ เป็นตัวเลข");
            }
            else
            {
                txtCostOther.Text = "0";
            }

            if (!string.Equals(txtOilLose.Text.Trim(), string.Empty))
            {
                if (!decimal.TryParse(txtOilLose.Text.Trim(), out tmp2))
                    throw new Exception("กรุณาป้อน ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร) เป็นตัวเลข");
            }
            else
            {
                txtOilLose.Text = "0";
            }

            DateTime? CostCheckDate = null;
            if (!string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
                CostCheckDate = DateTime.ParseExact(txtCostCheckDate.Text.Trim(), "dd/MM/yyyy", null);

            int Blacklist = (chkBlacklist.Checked) ? 1 : 0;

            string Sentencer = string.Empty;
            if (radSentencerType.SelectedIndex >= 0)
                Sentencer = (ddlSentencer.SelectedIndex == 0) ? radSentencerType.SelectedValue : ddlSentencer.SelectedValue;


            var _chk = GetIsVendorDownload();

            ComplainImportFileBLL.Instance.ComplainTab3SaveBLL(dtUploadTab3, DocID, int.Parse(Session["UserID"].ToString()), "COMPLAIN_SCORE", (radCusScoreType.SelectedIndex > -1) ? int.Parse(radCusScoreType.SelectedValue) : -1, (!string.Equals(txtPointFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtPointFinal.Text.Trim()) : -1, (!string.Equals(txtCostFinal.Text.Trim(), string.Empty)) ? Convert.ToDecimal(txtCostFinal.Text.Trim()) : -1, (!string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? int.Parse(txtDisableFinal.Text.Trim()) : -1, dtScoreList, DocStatusID, int.Parse(radCostCheck.SelectedValue), CostCheckDate, dtScoreListCar, isValiDate, decimal.Parse(txtCostOther.Text.Trim()), isSaveCusScore, txtRemarkTab3.Text.Trim(), decimal.Parse(txtOilLose.Text.Trim()), Blacklist, Sentencer, _chk);
            //this.SendEmail(ConfigValue.EmailCreateComplain);
            if (isValiDate)
            {
                if (Convert.ToDecimal(txtPointFinal.Text.Trim()) > 0 || Convert.ToDecimal(txtCostFinal.Text.Trim()) > 0 || int.Parse(txtDisableFinal.Text.Trim()) > 0 || decimal.Parse(txtCostOther.Text.Trim()) > 0)
                    IsSendToVendor = true;
                else
                    IsSendToVendor = false;

                this.SendEmail(ConfigValue.EmailComplainCusScore);
            }
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst_New.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSaveTab3()
    {
        try
        {
            if (dtScoreList.Rows.Count == 0)
                throw new Exception("กรุณาเลือก หัวข้อการตัดคะแนน");

            if (radCusScoreType.SelectedIndex < 0)
                throw new Exception("กรุณาเลือก เงื่อนไขการตัดคะแนน");

            if (string.Equals(txtPointFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน คะแนน");

            if (string.Equals(txtCostFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ค่าปรับ");

            if (string.Equals(txtDisableFinal.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ระงับ พขร. (วัน)");

            int tmp1;
            decimal tmp2;

            if (!int.TryParse(txtDisableFinal.Text.Trim(), out tmp1))
                throw new Exception("กรุณาป้อน ระงับ พขร. (วัน) เป็นตัวเลข");

            if (!decimal.TryParse(txtPointFinal.Text.Trim(), out tmp2))
                throw new Exception("กรุณาป้อน คะแนน (วัน) เป็นตัวเลข");

            if (!decimal.TryParse(txtCostFinal.Text.Trim(), out tmp2))
                throw new Exception("กรุณาป้อน ค่าปรับ เป็นตัวเลข");

            //ในกรณีมีวันที่เกิดเหตุหลายวัน ต้องเลือกเป็น Sum เท่านั้น
            if (dgvScore.Visible)
                this.CheckDeliveryDate(dtCarDistinct);
            else if (dgvScoreTotalCar.Visible)
                this.CheckDeliveryDate(dtDup);

            if ((ddlSentencer.Enabled) && (ddlSentencer.SelectedIndex < 1))
                throw new Exception("กรุณาเลือก คณะกรรมการพิจารณาโทษ");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void CheckDeliveryDate(DataTable dt)
    {
        try
        {
            string DeliveryDate = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i == 0)
                    DeliveryDate = dt.Rows[i]["DELIVERY_DATE"].ToString();
                if ((!string.Equals(DeliveryDate, dt.Rows[i]["DELIVERY_DATE"].ToString())) && (!string.Equals(radCusScoreType.SelectedValue, "2")))
                    throw new Exception("ในกรณีที่มีหลายวันที่เกิดเหตุ ต้องเลือกตัดคะแนน เป็นผลรวมเท่านั้น");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LockDriver()
    {
        try
        {
            //PersonalID = "4700000232";
            DataTable dtDriver = ComplainBLL.Instance.DriverUpdateSelectBLL(DocID);
            DataTable dtDriverDetail;

            ComplainBLL.Instance.UpdateDriverStatusBLL(dtDriver);

            for (int i = 0; i < dtDriver.Rows.Count; i++)
            {
                dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dtDriver.Rows[i]["SEMPLOYEEID"].ToString());

                SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                {
                    DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                    PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                    LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                    LICENSENOE_FROM = string.Equals(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString()).ToString("dd/MM/yyyy"),
                    LICENSENO_TO = string.Equals(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString()).ToString("dd/MM/yyyy"),
                    Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                    Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                    FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                    LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                    CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                    DRV_STATUS = (dtDriver.Rows[i]["CHANGE_TO"].ToString() == "0") ? ConfigValue.DriverEnable : ConfigValue.DriverDisable
                };

                try
                {
                    string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                    string resultCheck = result.Substring(0, 1);

                    ComplainBLL.Instance.DriverUpdateLogBLL(DocID, dtDriver.Rows[i]["ID"].ToString());
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdChangeStatus_Click(object sender, EventArgs e)
    {
        try
        {
            lblModalTitle3.Text = "กรุณาป้อน รายการเอกสาร ที่ขอเพิ่มเติม จากผู้ขนส่ง";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowWithAutoPostBack", "$('#ShowWithAutoPostBack').modal();", true);
            upModal3.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdChangeStatusConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            ComplainBLL.Instance.ComplainChangeStatusBLL(DocID, ConfigValue.DocStatus8, int.Parse(Session["UserID"].ToString()));
            this.SendEmail(ConfigValue.EmailComplainRequest);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowWithAutoPostBack", "$('#ShowWithAutoPostBack').modal('hide');", true);
            upModal3.Update();
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst_New.aspx");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LockAndUnlockDriver(bool IsLockTab1)
    {
        try
        {
            DataTable dtDriverDetail;
            CheckBox chk = new CheckBox();
            CheckBox chkbl = new CheckBox();
            TextBox txtPersonalLock = new TextBox();
            TextBox txtRemarksus = new TextBox();
            for (int i = 0; i < dgvSuspendDriver.Rows.Count; i++)
            {
                //Thread.Sleep(6000);
                chk = (CheckBox)dgvSuspendDriver.Rows[i].FindControl("chkSuspend");
                chkbl = (CheckBox)dgvSuspendDriver.Rows[i].FindControl("chkBlacklistDriver");
                txtPersonalLock = (TextBox)dgvSuspendDriver.Rows[i].FindControl("txtDays");
                if (chkBlacklist.Checked)
                {
                    if (chkbl.Checked)
                    {
                        if (!string.Equals(dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), string.Empty))
                        {
                            string vendor = string.Empty;
                            if (cboVendor.SelectedValue != null)
                            {
                                vendor = cboVendor.SelectedValue.ToString();
                            }

                            int TotalLock = (string.Equals(txtPersonalLock.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtPersonalLock.Text.Trim());
                            if (chkLock.Checked == true)
                            {
                                Detail = string.Empty;
                                Detail += "ผลพิจารณา <br/>";
                                Detail += "<br/>";
                                Detail += "ข้อร้องเรียนหมายเลขเอกสาร :" + DocID + "<br/>";
                                Detail += "<br/>";
                                Detail += "สถานะการอนุญาตใช้งาน : ระงับการใช้งาน >> Blacklist <br/>";
                                Detail += "<br/>";
                                Detail += "สาเหตุ/รายละเอียด :<br/>";
                                Detail += txtDetail.Value.Trim();
                                if (IsCheckTab3Click == true)
                                {
                                    Detail += "<br/>";
                                    Detail += "หัวข้อตัดคะแนน : <br/>";
                                    DataRow[] result1 = dtScoreList.Select("SuspendDriver LIKE '%" + dgvSuspendDriver.DataKeys[i]["FULLNAME"].ToString() + "%'");
                                    foreach (DataRow row in result1)
                                    {
                                        Detail += row["STOPICNAME"].ToString() + " <br/>";
                                    }
                                    Detail += "หมายเหตุ : " + txtRemarkTab3.Text.Trim() + "<br/>";
                                }
                                ComplainBLL.Instance.ComplainLockDriverBLL(DocID, TotalLock, int.Parse(Session["UserID"].ToString()), Detail, "1", dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString());
                            }
                            else
                            {
                                Detail = string.Empty;
                                Detail += "ผลพิจารณา <br/>";
                                Detail += "<br/>";
                                Detail += "ข้อร้องเรียนหมายเลขเอกสาร :" + DocID + "<br/>";
                                Detail += "<br/>";
                                Detail += "สถานะการอนุญาตใช้งาน : อนุญาต >> Blacklist <br/>";
                                Detail += "<br/>";
                                Detail += "สาเหตุ/รายละเอียด :<br/>";
                                Detail += txtDetail.Value.Trim();
                                if (IsCheckTab3Click == true)
                                {
                                    Detail += "<br/>";
                                    Detail += "หัวข้อตัดคะแนน : <br/>";
                                    DataRow[] result1 = dtScoreList.Select("SuspendDriver LIKE '%" + dgvSuspendDriver.DataKeys[i]["FULLNAME"].ToString() + "%'");
                                    foreach (DataRow row in result1)
                                    {
                                        Detail += row["STOPICNAME"].ToString() + " <br/>";
                                    }
                                    Detail += "หมายเหตุ : " + txtRemarkTab3.Text.Trim() + "<br/>";
                                }
                                ComplainBLL.Instance.ComplainLockDriverBLL(DocID, TotalLock, int.Parse(Session["UserID"].ToString()), Detail, "1", dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString());
                            }

                            dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString());

                            SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                            {
                                DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                                PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                                LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                                LICENSENOE_FROM = string.Equals(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString()).ToString("dd/MM/yyyy"),
                                LICENSENO_TO = string.Equals(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString()).ToString("dd/MM/yyyy"),
                                Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                                Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                                FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                                LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                                CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                                DRV_STATUS = ConfigValue.DriverOut
                            };

                            string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                            string resultCheck = result.Substring(0, 1);

                            if (string.Equals(resultCheck, "N"))
                                throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                            else
                                ComplainBLL.Instance.DriverUpdateTMSBLL(dtDriverDetail.Rows[i]["SEMPLOYEEID"].ToString(), "2", "2", "0", txtRemarkTab3.Text.Trim(), "1");

                            //ComplainBLL.Instance.UpdateDriverStatusBLL(dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), "2", "2");
                            //this.InsertDriverStatus(dtDriverDetail.Rows[0]["EMPSAPID"].ToString(), dtDriverDetail.Rows[0]["PERS_CODE"].ToString(), dtDriverDetail.Rows[0]["FNAME"].ToString(), dtDriverDetail.Rows[0]["LNAME"].ToString(), "0", dtDriverDetail.Rows[0]["STRANS_ID"].ToString(), dtDriverDetail.Rows[0]["PICTUREPATH"].ToString(), DateTime.Now.ToString());
                        }
                    }
                    else
                    {
                        int TotalLock = (string.Equals(txtPersonalLock.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtPersonalLock.Text.Trim());
                        if ((IsLockTab1 || TotalLock > 0) || (IsCheckTab3Click == true && chk.Checked == true))
                        {
                            Detail = string.Empty;
                            Detail += "ผลพิจารณา <br/>";
                            Detail += "<br/>";
                            Detail += "ข้อร้องเรียนหมายเลขเอกสาร :" + DocID + "<br/>";
                            Detail += "<br/>";
                            if (IsCheckTab3Click == true && chkbl.Checked == true && chk.Checked == true)
                                Detail += "สถานะการอนุญาตใช้งาน : ระงับการใช้งาน >> Blacklist <br/>";
                            else if (IsCheckTab3Click == true && chkbl.Checked == true)
                                Detail += "สถานะการอนุญาตใช้งาน : อนุญาต >> Blacklist <br/>";
                            else
                                Detail += "สถานะการอนุญาตใช้งาน : อนุญาต >> ระงับการใช้งาน <br/>";
                            Detail += "<br/>";
                            Detail += "สาเหตุ/รายละเอียด :<br/>";
                            Detail += txtDetail.Value.Trim();
                            if (IsCheckTab3Click == true)
                            {
                                Detail += "<br/>";
                                Detail += "หัวข้อตัดคะแนน : <br/>";
                                DataRow[] result1 = dtScoreList.Select("SuspendDriver LIKE '%" + dgvSuspendDriver.DataKeys[i]["FULLNAME"].ToString() + "%'");
                                foreach (DataRow row in result1)
                                {
                                    Detail += row["STOPICNAME"].ToString() + " <br/>";
                                }
                                Detail += "หมายเหตุ : " + txtRemarkTab3.Text.Trim() + "<br/>";
                            }
                            ComplainBLL.Instance.ComplainLockDriverBLL(DocID, TotalLock, int.Parse(Session["UserID"].ToString()), Detail, "0", dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString());
                            //DataTable dtDriver = ComplainBLL.Instance.DriverUpdateSelect2BLL(DocID);

                            dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString());

                            SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                            {
                                DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                                PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                                LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                                LICENSENOE_FROM = string.Equals(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString()).ToString("dd/MM/yyyy"),
                                LICENSENO_TO = string.Equals(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString()).ToString("dd/MM/yyyy"),
                                Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                                Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                                FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                                LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                                CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                                DRV_STATUS = ConfigValue.DriverDisable
                            };

                            string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                            string resultCheck = result.Substring(0, 1);

                            if (string.Equals(resultCheck, "N"))
                                throw new Exception("ไม่สามารถระงับ พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                            else
                                ComplainBLL.Instance.DriverUpdateTMSBLL(dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), "0", "1", "2", txtDetail.Value.Trim(), "0");

                            //this.InsertDriverStatus(dtDriverDetail.Rows[0]["EMPSAPID"].ToString(), dtDriverDetail.Rows[0]["PERS_CODE"].ToString(), dtDriverDetail.Rows[0]["FNAME"].ToString(), dtDriverDetail.Rows[0]["LNAME"].ToString(), "0", dtDriverDetail.Rows[0]["STRANS_ID"].ToString(), dtDriverDetail.Rows[0]["PICTUREPATH"].ToString(), DateTime.Now.ToString());
                        }
                    }
                }
                else
                {
                    int TotalLock = (string.Equals(txtPersonalLock.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtPersonalLock.Text.Trim());
                    if ((IsLockTab1 || TotalLock > 0) || (IsCheckTab3Click == true && chk.Checked == true))
                    {
                        Detail = string.Empty;
                        if (IsCheckTab3Click == true)
                            Detail += "ผลพิจารณา <br/>";
                        else
                            Detail += "ข้อมูลทั่วไป <br/>";
                        Detail += "<br/>";
                        Detail += "ข้อร้องเรียนหมายเลขเอกสาร :" + DocID + "<br/>";
                        Detail += "<br/>";
                        if (IsCheckTab3Click == true)
                            Detail += "สถานะการอนุญาตใช้งาน : ระงับการใช้งาน <br/>";
                        else
                            Detail += "สถานะการอนุญาตใช้งาน : อนุญาต >> ระงับการใช้งาน <br/>";
                        Detail += "<br/>";
                        Detail += "สาเหตุ/รายละเอียด :<br/>";
                        Detail += txtDetail.Value.Trim();
                        if (IsCheckTab3Click == true)
                        {
                            Detail += "<br/>";
                            Detail += "หัวข้อตัดคะแนน : <br/>";
                            DataRow[] result1 = dtScoreList.Select("SuspendDriver LIKE '%" + dgvSuspendDriver.DataKeys[i]["FULLNAME"].ToString() + "%'");
                            foreach (DataRow row in result1)
                            {
                                Detail += row["STOPICNAME"].ToString() + " <br/>";
                            }
                            Detail += "หมายเหตุ : " + txtRemarkTab3.Text.Trim() + "<br/>";
                        }
                        ComplainBLL.Instance.ComplainLockDriverBLL(DocID, TotalLock, int.Parse(Session["UserID"].ToString()), Detail, "0", dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString());
                        //DataTable dtDriver = ComplainBLL.Instance.DriverUpdateSelect2BLL(DocID);

                        dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString());

                        SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
                        {
                            DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
                            PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
                            LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
                            LICENSENOE_FROM = string.Equals(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString()).ToString("dd/MM/yyyy"),
                            LICENSENO_TO = string.Equals(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(), string.Empty) ? string.Empty : Convert.ToDateTime(dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString()).ToString("dd/MM/yyyy"),
                            Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
                            Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
                            FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
                            LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
                            CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
                            DRV_STATUS = ConfigValue.DriverDisable
                        };

                        string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
                        string resultCheck = result.Substring(0, 1);

                        if (string.Equals(resultCheck, "N"))
                            throw new Exception("ไม่สามารถระงับ พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
                        else
                            ComplainBLL.Instance.DriverUpdateTMSBLL(dgvSuspendDriver.DataKeys[i]["SDRIVERNO"].ToString(), "0", "1", "2", txtDetail.Value.Trim(), "0");

                    }
                }
            }
            //if (chkBlacklist.Checked)
            //{
            //    for (int i = 0; i < dtHeader.Rows.Count; i++)
            //    {
            //        if (!string.Equals(dtHeader.Rows[i]["PERSONALID"].ToString(), string.Empty))
            //        {
            //            dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dtHeader.Rows[i]["PERSONALID"].ToString());

            //            ComplainBLL.Instance.UpdateDriverStatusBLL(dtHeader.Rows[i]["PERSONALID"].ToString(), "2", "2");

            //            SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
            //            {
            //                DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
            //                PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
            //                LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
            //                LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
            //                LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
            //                Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
            //                Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
            //                FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
            //                LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
            //                CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
            //                DRV_STATUS = ConfigValue.DriverOut
            //            };

            //            string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
            //            string resultCheck = result.Substring(0, 1);

            //            if (string.Equals(resultCheck, "N"))
            //                throw new Exception("ไม่สามารถ Blacklist พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
            //            else
            //                ComplainBLL.Instance.DriverUpdateTMSBLL(dtDriverDetail.Rows[i]["SEMPLOYEEID"].ToString(), "2", "2", "0", txtRemarkTab3.Text.Trim(), "1");

            //            string vendor = string.Empty;
            //            if (cboVendor.SelectedValue != null)
            //            {
            //                vendor = cboVendor.SelectedValue.ToString();
            //            }

            //            int TotalLock = (string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtDisableFinal.Text.Trim());
            //            if (chkLock.Checked == true)
            //            {
            //                Detail = string.Empty;
            //                Detail += "ผลพิจารณา <br/>";
            //                Detail += "<br/>";
            //                Detail += "ข้อร้องเรียนหมายเลขเอกสาร :" + DocID + "<br/>";
            //                Detail += "<br/>";
            //                Detail += "สถานะการอนุญาตใช้งาน : ระงับการใช้งาน >> Blacklist <br/>";
            //                Detail += "<br/>";
            //                Detail += "หมายเหตุ :<br/>";
            //                Detail += txtRemarkTab3.Text.Trim();
            //                ComplainBLL.Instance.ComplainLockDriverBLL(DocID, TotalLock, int.Parse(Session["UserID"].ToString()), Detail, "1");
            //            }
            //            else
            //            {
            //                Detail = string.Empty;
            //                Detail += "ผลพิจารณา <br/>";
            //                Detail += "<br/>";
            //                Detail += "ข้อร้องเรียนหมายเลขเอกสาร :" + DocID + "<br/>";
            //                Detail += "<br/>";
            //                Detail += "สถานะการอนุญาตใช้งาน : อนุญาต >> Blacklist <br/>";
            //                Detail += "<br/>";
            //                Detail += "หมายเหตุ :<br/>";
            //                Detail += txtRemarkTab3.Text.Trim();
            //                ComplainBLL.Instance.ComplainLockDriverBLL(DocID, TotalLock, int.Parse(Session["UserID"].ToString()), Detail, "1");
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    int TotalLock = (string.Equals(txtDisableFinal.Text.Trim(), string.Empty)) ? 0 : int.Parse(txtDisableFinal.Text.Trim());
            //    if (IsLockTab1 || TotalLock > 0)
            //    {
            //        Detail = string.Empty;
            //        Detail += "ข้อมูลทั่วไป <br/>";
            //        Detail += "<br/>";
            //        Detail += "ข้อร้องเรียนหมายเลขเอกสาร :" + DocID + "<br/>";
            //        Detail += "<br/>";
            //        Detail += "สถานะการอนุญาตใช้งาน : อนุญาต >> ระงับการใช้งาน <br/>";
            //        Detail += "<br/>";
            //        Detail += "สาเหตุ/รายละเอียด :<br/>";
            //        Detail += txtRemarkTab3.Text.Trim() == "" ? txtDetail.Value.Trim() : txtRemarkTab3.Text.Trim();
            //        ComplainBLL.Instance.ComplainLockDriverBLL(DocID, TotalLock, int.Parse(Session["UserID"].ToString()), Detail, "0");
            //        DataTable dtDriver = ComplainBLL.Instance.DriverUpdateSelect2BLL(DocID);

            //        dtDriverDetail = ComplainBLL.Instance.PersonalDetailSelectBLL(dtDriver.Rows[0]["SEMPLOYEEID"].ToString());

            //        SAP_Create_Driver UpdateDriver = new SAP_Create_Driver()
            //        {
            //            DRIVER_CODE = dtDriverDetail.Rows[0]["EMPSAPID"].ToString(),
            //            PERS_CODE = dtDriverDetail.Rows[0]["PERS_CODE"].ToString(),
            //            LICENSE_NO = dtDriverDetail.Rows[0]["SDRIVERNO"].ToString(),
            //            LICENSENOE_FROM = dtDriverDetail.Rows[0]["DDRIVEBEGIN"].ToString(),
            //            LICENSENO_TO = dtDriverDetail.Rows[0]["DDRIVEEXPIRE"].ToString(),
            //            Phone_1 = dtDriverDetail.Rows[0]["STEL"].ToString(),
            //            Phone_2 = dtDriverDetail.Rows[0]["STEL2"].ToString(),
            //            FIRST_NAME = dtDriverDetail.Rows[0]["FNAME"].ToString(),
            //            LAST_NAME = dtDriverDetail.Rows[0]["LNAME"].ToString(),
            //            CARRIER = dtDriverDetail.Rows[0]["STRANS_ID"].ToString(),
            //            DRV_STATUS = ConfigValue.DriverDisable
            //        };

            //        string result = UpdateDriver.UDP_Driver_SYNC_OUT_SI();
            //        string resultCheck = result.Substring(0, 1);

            //        if (string.Equals(resultCheck, "N"))
            //            throw new Exception("ไม่สามารถระงับ พขร. ได้ เนื่องจาก " + result.Replace("N:,", string.Empty).Replace("N:", string.Empty));
            //        else
            //            ComplainBLL.Instance.DriverUpdateTMSBLL(dtDriver.Rows[0]["SEMPLOYEEID"].ToString(), "0", "1", "2", txtDetail.Value.Trim(), "0");

            //    }
            //}
            string a = Session["LoginID"].ToString();
        }
        catch (Exception ex)
        {
            //alertFail(RemoveSpecialCharacters(ex.Message));
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdAdminChangeStatus_Click(object senderf, EventArgs e)
    {
        try
        {
            //=ACOS(SIN(lat1)*SIN(lat2)+COS(lat1)*COS(lat2)*COS(lon2-lon1))*6371
            //double lat1 = 80.45416, lat2 = 100.45416, long1 = 7.04027, long2 = 7.04027;
            //var c = System.Math.Acos(System.Math.Sin(lat1) * System.Math.Sin(lat2) + System.Math.Cos(lat1) * System.Math.Cos(long2 - long1)) * 6371;

            ComplainBLL.Instance.ChangeStatusBLL(DocID, int.Parse(ddlDocIDStatusNew.SelectedValue), int.Parse(Session["UserID"].ToString()));
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst_New.aspx");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void LoadDocStatus()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = DocStatusBLL.Instance.DocStatusSelectBLL();
            DropDownListHelper.BindDropDownList(ref ddlDocIDStatusNew, dt, "DOC_STATUS_ID", "DOC_STATUS_NAME", true);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (string.Equals(dt.Rows[i]["DOC_STATUS_ID"].ToString(), dtData.Rows[0]["DOC_STATUS_ID"].ToString()))
                    txtDocIDStatusOld.Value = dt.Rows[i]["DOC_STATUS_NAME"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void SendEmail(int TemplateID)
    {
        //return; // ไม่ต้องส่ง E-mail

        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                this.removeInvalidEmailBeforSend();
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailComplainCreateNoLock)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                    Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                    Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                    Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                    Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                    Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_add_New.aspx?DocID=" + ConfigValue.GetEncodeText(DocID);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    string CCEmail = (!string.Equals(txtCCEmail.Text.Trim(), string.Empty)) ? "," + txtCCEmail.Text.Trim() : string.Empty;
                    string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                    //MailService.SendMail("raviwan.t@pttor.com,YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, true, true) + CCEmail + CreateMail, Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true) + CCEmail + CreateMail, Subject, Body, "", "EmailComplainCreateNoLock", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailComplainCreateLock)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                    Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                    Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                    Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                    Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                    Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                    Body = Body.Replace("{LOCK_DATE}", dtComplainEmail.Rows[0]["LOCK_DATE"].ToString());
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_add_New.aspx?DocID=" + ConfigValue.GetEncodeText(DocID);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    string CCEmail = (!string.Equals(txtCCEmail.Text.Trim(), string.Empty)) ? "," + txtCCEmail.Text.Trim() : string.Empty;
                    string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                    //MailService.SendMail("raviwan.t@pttor.com,YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), true, true) + CCEmail + CreateMail, Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body, "", "EmailComplainCreateLock", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailComplainCusScore)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                    Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                    Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                    Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                    Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                    Body = Body.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_add_New.aspx?DocID=" + ConfigValue.GetEncodeText(DocID);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    string CCEmail = (!string.Equals(txtCCEmail.Text.Trim(), string.Empty)) ? "," + txtCCEmail.Text.Trim() : string.Empty;
                    string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                    if (IsSendToVendor)
                    {
                        //MailService.SendMail("raviwan.t@pttor.com,YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body);
                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body, "", "EmailComplainCusScore", ColumnEmailName);
                    }
                    else
                    {
                        //MailService.SendMail("raviwan.t@pttor.com,YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true) + CCEmail + CreateMail, Subject, Body);
                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true) + CCEmail + CreateMail, Subject, Body, "", "EmailComplainCusScore", ColumnEmailName);
                    }
                }
                else if (TemplateID == ConfigValue.EmailComplainRequest)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                    Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                    Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                    Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                    Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{COMPLAIN_DEPARTMENT}", dtComplainEmail.Rows[0]["COMPLAIN_DEPARTMENT"].ToString());
                    Body = Body.Replace("{CREATE_DEPARTMENT}", dtComplainEmail.Rows[0]["CREATE_DEPARTMENT"].ToString());
                    Body = Body.Replace("{ATTACH}", txtEmailTab3.Text.Trim().Replace("\n", "<br />"));

                    string CCEmail = (!string.Equals(txtCCEmail.Text.Trim(), string.Empty)) ? "," + txtCCEmail.Text.Trim() : string.Empty;
                    string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                    //MailService.SendMail("raviwan.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true) + CCEmail + CreateMail, Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true) + CCEmail + CreateMail, Subject, Body, "", "EmailComplainRequest", ColumnEmailName);
                }
                else if (TemplateID == 11)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", DocID);
                    Body = Body.Replace("{DOCID}", DocID);
                    Body = Body.Replace("{DRIVER}", dtData.Rows[0]["SEMPLOYEENAME"].ToString());
                    Body = Body.Replace("{VENDOR}", dtData.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{CONTRACT}", cboContract.SelectedItem.ToString());
                    Body = Body.Replace("{CAR}", cboHeadRegist.SelectedItem.ToString());
                    Body = Body.Replace("{SOURCE}", cbxOrganiz.SelectedItem.ToString());
                    Body = Body.Replace("{COMPLAIN_DEPARTMENT}", txtFNameDivision.Text);
                    Body = Body.Replace("{CREATE_DEPARTMENT}", txtComplainDivisionBy.Value);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    string CCEmail = (!string.Equals(txtCCEmail.Text.Trim(), string.Empty)) ? "," + txtCCEmail.Text.Trim() : string.Empty;
                    string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, true) + CCEmail + CreateMail, Subject, Body, "", "EmailComplainRequestDoc", ColumnEmailName);
                }
                else if (TemplateID == 129)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    string drivername = string.Empty;
                    if (cmbPersonalNo.SelectedIndex > 0)
                        drivername = cmbPersonalNo.SelectedItem.ToString();
                    if (cmbPersonalNo2.SelectedIndex > 0)
                        drivername = drivername + "," + cmbPersonalNo2.SelectedItem.ToString();
                    if (cmbPersonalNo3.SelectedIndex > 0)
                        drivername = drivername + "," + cmbPersonalNo3.SelectedItem.ToString();
                    if (cmbPersonalNo4.SelectedIndex > 0)
                        drivername = drivername + "," + cmbPersonalNo4.SelectedItem.ToString();

                    Subject = Subject.Replace("{DOCID}", DocID);
                    Body = Body.Replace("{VENDOR}", dtData.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{COMPLAIN}", txtDocID.Value);
                    Body = Body.Replace("{CONTRACT}", cboContract.SelectedItem.ToString());
                    Body = Body.Replace("{CAR}", cboHeadRegist.SelectedItem.ToString());
                    Body = Body.Replace("{SOURCE}", cbxOrganiz.SelectedItem.ToString());
                    Body = Body.Replace("{DRIVER}", drivername);
                    Body = Body.Replace("{COMPLAIN_DEPARTMENT}", txtFName.Text);
                    Body = Body.Replace("{CREATE_DEPARTMENT}", txtComplainBy.Value);

                    string CCEmail = (!string.Equals(txtCCEmail.Text.Trim(), string.Empty)) ? "," + txtCCEmail.Text.Trim() : string.Empty;
                    string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;

                    //MailService.SendMail("raviwan.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,CHUTAPHA.C@PTTOR.COM,thrathorn.v@pttor.com,patrapol.n@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true) + CCEmail + CreateMail, Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true) + CCEmail + CreateMail, Subject, Body, "", "EmailComplainCancel", ColumnEmailName);
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetTopicList()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (!string.Equals(sb.ToString(), string.Empty))
                    sb.Append(",");

                sb.Append(dtHeader.Rows[i]["COMPLAINNAME"].ToString());
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string GetDriverList()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                if (string.Equals(dtHeader.Rows[i]["LOCKDRIVERID"].ToString(), "1"))
                {
                    if (!string.Equals(sb.ToString(), string.Empty))
                        sb.Append(",");

                    sb.Append(dtHeader.Rows[i]["EMPLOYEENAME"].ToString());
                }
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdSaveIsCost_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSaveIsCost();

            DateTime? CostCheckDate = null;
            if (!string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
                CostCheckDate = DateTime.ParseExact(txtCostCheckDate.Text.Trim(), "dd/MM/yyyy", null);

            int DocStatusID = (string.Equals(radCostCheck.SelectedValue, "1")) ? ConfigValue.DocStatus10 : ConfigValue.DocStatus11;

            ComplainImportFileBLL.Instance.ComplainUpdateCostBLL(DocID, int.Parse(radCostCheck.SelectedValue), CostCheckDate, DocStatusID, dtUploadCost, int.Parse(Session["UserID"].ToString()), "COMPLAIN_COST");
            alertSuccess("บันทึกข้อมูล เรียบร้อย (หมายเลขเอกสาร " + txtDocID.Value + ")", "admin_Complain_lst_New.aspx");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateSaveIsCost()
    {
        try
        {
            //decimal CostFinal = string.Equals(txtCostFinal.Text.Trim(), string.Empty) ? 0 : decimal.Parse(txtCostFinal.Text.Trim());
            //decimal CostOther = string.Equals(txtCostOther.Text.Trim(), string.Empty) ? 0 : decimal.Parse(txtCostOther.Text.Trim());

            if (string.Equals(radCostCheck.SelectedValue, "0") && !string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
                throw new Exception("กรณีชำระค่าปรับยังไม่ครบถ้วน ห้ามระบุ วันที่ชำระครบถ้วน");

            if (string.Equals(radCostCheck.SelectedValue, "1") && string.Equals(txtCostCheckDate.Text.Trim(), string.Empty))
                throw new Exception("กรณีชำระค่าปรับครบถ้วน ให้ระบุ วันที่ชำระครบถ้วน");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdDriverSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblDriverSearchTitle.Text = "ค้นหา พขร.";

            if ((dtEmployee == null) || (dtEmployee.Rows.Count == 0))
            {
                alertSuccess("ไม่มีข้อมูล พขร.");
                return;
            }

            dtDriverSearch = dtEmployee.Copy();
            dtDriverSearch.Rows.RemoveAt(0);
            GridViewHelper.BindGridView(ref dgvDriverSearch, dtDriverSearch);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDriverSearch", "$('#ShowDriverSearch').modal();", true);
            updDriverSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvDriverSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                GridViewHelper.BindGridView(ref dgvDriverSearch, dtEmployee);
                txtSearchDriver.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            dgvDriverSearch.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDriverSearch, dtTmp);
            updDriverSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdStartSearchDriver_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver.Text.Trim() + "%'" + " OR SEMPLOYEEID LIKE '%" + txtSearchDriver.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                //GridViewHelper.BindGridView(ref dgvDriverSearch, dtEmployee);
                txtSearchDriver.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            GridViewHelper.BindGridView(ref dgvDriverSearch, dtTmp);

            updDriverSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvDriverSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            if (cmbPersonalNo.Enabled)
                cmbPersonalNo.SelectedValue = dgvDriverSearch.DataKeys[e.RowIndex].Value.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdDeliveryNoSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblDeliveryNoSearchTitle.Text = "ค้นหา Delivery No";

            if ((dtDeliveryNo == null) || (dtDeliveryNo.Rows.Count == 0))
            {
                alertSuccess("ไม่มีข้อมูล Delivery No");
                return;
            }

            dtDeliveryNoSearch = dtDeliveryNo.Copy();
            dtDeliveryNoSearch.Rows.RemoveAt(0);
            GridViewHelper.BindGridView(ref dgvDeliveryNoSearch, dtDeliveryNoSearch);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDeliveryNoSearch", "$('#ShowDeliveryNoSearch').modal();", true);
            updDeliveryNoSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvDeliveryNoSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DataRow[] dr = dtDeliveryNoSearch.Select("SDELIVERYNO LIKE '%" + txtSearchDeliveryNo.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                GridViewHelper.BindGridView(ref dgvDeliveryNoSearch, dtDeliveryNo);
                txtSearchDeliveryNo.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            dgvDeliveryNoSearch.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDeliveryNoSearch, dtTmp);
            updDeliveryNoSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdStartSearchDeliveryNo_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow[] dr = dtDeliveryNoSearch.Select("SDELIVERYNO LIKE '%" + txtSearchDeliveryNo.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                //GridViewHelper.BindGridView(ref dgvDeliveryNoSearch, dtDeliveryNo);
                txtSearchDeliveryNo.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            GridViewHelper.BindGridView(ref dgvDeliveryNoSearch, dtTmp);

            updDeliveryNoSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvDeliveryNoSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            if (cboDelivery.Enabled)
                cboDelivery.SelectedValue = dgvDeliveryNoSearch.DataKeys[e.RowIndex].Value.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalDeliveryNo();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdCarSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblCarSearchTitle.Text = "ค้นหา ทะเบียนรถ";

            if ((dtTruck == null) || (dtTruck.Rows.Count == 0))
            {
                alertSuccess("ไม่มีข้อมูล ทะเบียนรถ (หัว)");
                return;
            }

            dtCarSearch = dtTruck.Copy();
            dtCarSearch.Rows.RemoveAt(0);
            GridViewHelper.BindGridView(ref dgvCarSearch, dtCarSearch);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowCarSearch", "$('#ShowCarSearch').modal();", true);
            updCarSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvCarSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DataRow[] dr = dtCarSearch.Select("SHEADREGISTERNO LIKE '%" + txtSearchCar.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                GridViewHelper.BindGridView(ref dgvCarSearch, dtTruck);
                txtSearchCar.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            dgvCarSearch.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvCarSearch, dtTmp);
            updCarSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdStartSearchCar_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow[] dr = dtCarSearch.Select("SHEADREGISTERNO LIKE '%" + txtSearchCar.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                //GridViewHelper.BindGridView(ref dgvDriverSearch, dtEmployee);
                txtSearchCar.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            GridViewHelper.BindGridView(ref dgvCarSearch, dtTmp);

            updCarSearch.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvCarSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            if (cboHeadRegist.Enabled)
            {
                cboHeadRegist.SelectedValue = dgvCarSearch.DataKeys[e.RowIndex].Value.ToString();
                this.GetCarDetail("Dialog");
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalCar();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdShowAppeal_Click(object senderf, EventArgs e)
    {
        try
        {
            //Session["AppealDoc"] = DocID;
            //Response.Redirect("admin_suppliant_lst.aspx");
            Session["AppealDoc"] = DocID;
            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                Response.Redirect("admin_suppliant_lst_New.aspx");
            else
                Response.Redirect("admin_suppliant_lst_New.aspx");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdUploadCost_Click(object sender, EventArgs e)
    {
        this.StartUploadCost();
    }

    private void StartUploadCost()
    {
        try
        {
            if (cboUploadCost.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUploadCost.HasFile)
            {
                string FileNameUser = fileUploadCost.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFileCost(System.IO.Path.GetExtension(FileNameUser), fileUploadCost.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadCost.SaveAs(Path + "\\" + FileNameSystem);

                dtUploadCost.Rows.Add(cboUploadCost.SelectedValue, cboUploadCost.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadCost, dtUploadCost);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateUploadFileCost(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeCost.Rows[cboUploadCost.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeCost.Rows[cboUploadCost.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeCost.Rows[cboUploadCost.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvUploadCost_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadCost.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadCost, dtUploadCost);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvUploadCost_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadCost.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    protected void dgvUploadCost_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdDOSearchAll_Click(object sender, EventArgs e)
    {
        try
        {
            if (cboComplainType.SelectedIndex < 1)
                throw new Exception("กรุณาเลือก หัวข้อร้องเรียน ก่อนทำการค้นหา");

            lblDeliveryNoSearchAllTitle.Text = "ค้นหาข้อมูลจาก Delivery No";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDeliveryNoSearchAll", "$('#ShowDeliveryNoSearchAll').modal();", true);
            updDeliveryNoSearchAll.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvDeliveryNoSearchAll_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvDeliveryNoSearchAll.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDeliveryNoSearchAll, dtDeliveryNoAll);
            updDeliveryNoSearchAll.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void dgvDeliveryNoSearchAll_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            //alertSuccess(dgvDeliveryNoSearchAll.DataKeys[e.RowIndex].Value.ToString());

            string DO = dgvDeliveryNoSearchAll.DataKeys[e.RowIndex].Value.ToString();
            DataTable dt = ComplainBLL.Instance.SearchByDOBLL(" AND TPLANSCHEDULELIST.SDELIVERYNO = '" + DO + "'");
            if (dt.Rows.Count > 0)
            {
                if (!string.Equals(dt.Rows[0]["SVENDORID"].ToString(), string.Empty))
                {
                    cboVendor.SelectedValue = dt.Rows[0]["SVENDORID"].ToString();
                    cboVendor_SelectedIndexChanged(null, null);
                }

                if (!string.Equals(dt.Rows[0]["SCONTRACTID"].ToString(), string.Empty))
                {
                    cboContract.SelectedValue = dt.Rows[0]["SCONTRACTID"].ToString();
                    cboContract_SelectedIndexChanged(null, null);
                }

                if (!string.Equals(dt.Rows[0]["STRUCKID"].ToString(), string.Empty))
                {
                    cboHeadRegist.SelectedValue = dt.Rows[0]["STRUCKID"].ToString();
                    if (string.Equals(dt.Rows[0]["STRAILERREGISTERNO"].ToString(), string.Empty))
                        cboHeadRegist_SelectedIndexChanged(null, null);
                    else
                        txtTrailerRegist.Value = dt.Rows[0]["STRAILERREGISTERNO"].ToString();
                }

                if (!string.Equals(dt.Rows[0]["EMPLOYEEID"].ToString(), string.Empty))
                {
                    cmbPersonalNo.SelectedValue = dt.Rows[0]["EMPLOYEEID"].ToString();
                }

                if (!string.Equals(dt.Rows[0]["DDELIVERY"].ToString(), string.Empty))
                {
                    txtDateTrans.Text = dt.Rows[0]["DDELIVERY"].ToString();
                }

                if (!string.Equals(dt.Rows[0]["WAREHOUSE_ID_FROM"].ToString(), string.Empty))
                {
                    cbxOrganiz.SelectedValue = dt.Rows[0]["WAREHOUSE_ID_FROM"].ToString();
                }

                cboDelivery.SelectedValue = DO;

                txtShipTo.Text = dt.Rows[0]["WAREHOUSE_TO"].ToString();
            }
            else
                alertFail("ไม่พบข้อมูลของหมายเลข DO นี้");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalDeliveryNoAll();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    protected void cmdStartSearchDeliveryNoAll_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.Equals(txtDeliveryNoAll.Text.Trim(), string.Empty))
                throw new Exception("กรุณาใส่หมายเลข Delivery No ที่ต้องการค้นหา");

            if (txtDeliveryNoAll.Text.Trim().Length < 5)
                throw new Exception("กรุณาใส่หมายเลข Delivery No อย่างน้อง 5 ตัวอักษร");

            dtDeliveryNoAll = ComplainBLL.Instance.OutboundSelectBLL(" AND TPLANSCHEDULELIST.SDELIVERYNO IS NOT NULL AND TPLANSCHEDULELIST.SDELIVERYNO LIKE '%" + txtDeliveryNoAll.Text.Trim() + "%'");
            GridViewHelper.BindGridView(ref dgvDeliveryNoSearchAll, dtDeliveryNoAll);

            updDeliveryNoSearchAll.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void lblSentencerDetail_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlSentencer.SelectedIndex == 0)
                throw new Exception("กรุณา เลือก คณะกรรมการพิจารณาโทษ");

            DataTable dtDetail = ComplainBLL.Instance.SentencerSelectDetailBLL(ddlSentencer.SelectedValue);
            if (dtDetail.Rows.Count > 0)
            {
                dtDetail.Columns["NLIST"].ColumnName = "ลำดับ";
                dtDetail.Columns["CODE"].ColumnName = "รหัสพนักงาน";
                dtDetail.Columns["FNAME"].ColumnName = "ชื่อ - นามสกุล";
                dtDetail.Columns["SPOSITION"].ColumnName = "ตำแหน่ง";
                dtDetail.Columns["SJOB"].ColumnName = "ตำแหน่งในคณะทำงาน";
                dtDetail.Columns["CACTIVE"].ColumnName = "สถานะ";

                StringBuilder sb = new StringBuilder();
                sb.Append("<table style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                sb.Append("<tr style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                for (int i = 0; i < dtDetail.Columns.Count; i++)
                {
                    sb.Append("<td style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                    sb.Append("&nbsp;&nbsp;" + dtDetail.Columns[i].ColumnName + "&nbsp;&nbsp;");
                    sb.Append("</td>");
                }
                sb.Append("</tr>");

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    sb.Append("<tr style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                    for (int j = 0; j < dtDetail.Columns.Count; j++)
                    {

                        if (string.Equals(dtDetail.Columns[j].ColumnName, "สถานะ"))
                        {
                            sb.Append("<td style=\"border-color:cadetblue; border-style:solid; border-width:1px; text-align:center\">");
                            if (string.Equals(dtDetail.Rows[i][j].ToString(), "1"))                             //Active
                                sb.Append("&nbsp;&nbsp;" + "&#10003;" + "&nbsp;&nbsp;");
                            else
                                sb.Append("&nbsp;&nbsp;" + "&#x2718;" + "&nbsp;&nbsp;");    //Not Active
                        }
                        else
                        {
                            sb.Append("<td style=\"border-color:cadetblue; border-style:solid; border-width:1px\">");
                            sb.Append("&nbsp;&nbsp;" + dtDetail.Rows[i][j].ToString() + "&nbsp;&nbsp;");
                        }
                        sb.Append("</td>");
                    }

                    sb.Append("</tr>");
                }
                sb.Append("</table>");

                alertSuccess(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void radSentencerType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (radSentencerType.SelectedIndex == 0)
            {
                ddlSentencer.SelectedIndex = 0;
                ddlSentencer.Enabled = false;
            }
            else
            {
                ddlSentencer.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    #region " ค้นหาข้อมูล เลขที่สัญญา จากทะเบียนรถ "



    private DataTable dtCarSHEADREGISTERNO
    {
        get
        {
            if ((DataTable)ViewState["dtCarSHEADREGISTERNO"] != null)
                return (DataTable)ViewState["dtCarSHEADREGISTERNO"];
            else return new DataTable();
        }
        set { ViewState["dtCarSHEADREGISTERNO"] = value; }
    }

    public string strHeadRegistNo
    {
        get
        {
            if (ViewState["HeadRegistNo"] != null)
                return (ViewState["HeadRegistNo"].ToString().Trim());
            else return string.Empty;
        }
        set { ViewState["HeadRegistNo"] = value; }
    }

    /// <summary>
    /// ค้นหาข้อมูล เลขที่สัญญา จากทะเบียนรถ
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <remarks>
    /// Add By ORN.K
    /// </remarks>
    protected void btnSearchByCarLicense_Click(object sender, EventArgs e)
    {
        try
        {
            if (cmbTopic.SelectedIndex < 1)
                throw new Exception("กรุณาเลือกประเภทร้องเรียน");

            if (cboComplainType.SelectedIndex < 1)
                throw new Exception("กรุณาเลือกหัวข้อร้องเรียน");

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dialogHEADREGISTERNOSearch", "$('#dialogHEADREGISTERNOSearch').modal();", true);
            grvSearchHEADREGISTERNO.Visible = false;
            uplSearchLicense.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    protected void btnSearchLicenseAll_Click(object sender, EventArgs e)
    {
        try
        {
            string _no = txtSearchLicense.Text.Trim();
            if (string.Equals(_no, string.Empty))
                throw new Exception("กรุณาใส่หมายเลขทะเบียนรถที่ต้องการค้นหา");

            if (_no.Length < 2)
                throw new Exception("กรุณาใส่หมายเลขทะเบียนรถอย่างน้อย 2 ตัวอักษร");

            grvSearchHEADREGISTERNO.Visible = true;
            SetHeadRegistNo();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    public void SetHeadRegistNo()
    {
        odsHeadRegisterNo.SelectParameters["HeadRegisterNo"].DefaultValue = txtSearchLicense.Text.Trim();
        BindGrid();
    }
    private void BindGrid()
    {
        odsHeadRegisterNo.Select();
        odsHeadRegisterNo.DataBind();
        grvSearchHEADREGISTERNO.DataBind();
    }

    protected void odsHeadRegisterNo_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = new HeadRegisterNo();
    }

    protected void odsHeadRegisterNo_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        var count = e.ReturnValue as int?;
        if (count.HasValue)
        {
            hdTotalRecords.Value = Helper.StringHelper.ConvertToString(count);
        }
    }
    protected void odsHeadRegisterNo_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (!IsPostBack)
        {
            e.Cancel = true;
        }
    }
    protected void grvSearchHEADREGISTERNO_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvSearchHEADREGISTERNO.PageIndex = e.NewPageIndex;
        grvSearchHEADREGISTERNO.DataBind();
        uplSearchLicense.Update();
    }

    protected void grvSearchHEADREGISTERNO_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void grvSearchHEADREGISTERNO_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            int? rowIndex = ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            strHeadRegistNo = grvSearchHEADREGISTERNO.DataKeys[rowIndex.Value]["SHEADREGISTERNO"].ToString();
            this.LoadVendor();
            cboVendor.SelectedIndex = 1;
            cboVendor_SelectedIndexChanged(null, null);
            cboContract.SelectedIndex = 1;
            cboContract_SelectedIndexChanged(null, null);
            string STRAILERREGISTERNO = grvSearchHEADREGISTERNO.DataKeys[rowIndex.Value]["STRAILERREGISTERNO"].ToString().Replace("'", "\'").Replace(Environment.NewLine, "").Replace("\n", "");
            txtTrailerRegist.Value = STRAILERREGISTERNO;
        }
    }
    #endregion

    protected void btnSearchLicenseReset_Click(object sender, EventArgs e)
    {
        strHeadRegistNo = string.Empty;
        this.LoadVendor();
        cboVendor_SelectedIndexChanged(null, null);
    }
    protected void dgvUploadFile_flag_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            GridView gv = (GridView)sender;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int rowIndex = e.Row.RowIndex;
                if (rowIndex >= 0)
                {
                    CheckBox chkShow = e.Row.Cells[7].FindControl("chkShowToVendor") as CheckBox;
                    if (chkShow != null)
                    {
                        string str = gv.DataKeys[rowIndex].Values["IS_VENDOR_VIEW"].ToString();
                        if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                        { // For Vendor
                            e.Row.Visible = str == "1" ? true : false;
                        }
                        chkShow.Checked = str == "1" ? true : false;

                        if (DocStatusID == ConfigValue.DocStatus7 || DocStatusID == ConfigValue.DocStatus10 || DocStatusID == ConfigValue.DocStatus11)
                            chkShow.Enabled = false;
                    }

                    if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                    {
                        LinkButton lnkDownload = e.Row.FindControl("lnkDownload") as LinkButton;
                        if (lnkDownload != null) lnkDownload.Visible = true;

                        ImageButton imgView = e.Row.FindControl("imgView") as ImageButton;
                        if (imgView != null) imgView.Visible = false;
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            {
                Label lblEmptyData = e.Row.FindControl("lblEmptyData") as Label;
                lblEmptyData.Visible = !IsPostBack ? false : true;
            }
        }
        catch (Exception)
        {


        }

    }

    private Dictionary<int, string> GetIsVendorDownload()
    {
        Dictionary<int, string> _result = new Dictionary<int, string>();
        try
        {
            foreach (GridViewRow row in dgvUploadFile_flag.Rows)
            {
                CheckBox chkShow = row.Cells[7].FindControl("chkShowToVendor") as CheckBox;
                int uploadID = int.Parse(dgvUploadFile_flag.DataKeys[row.RowIndex].Values["F_ID"].ToString());
                string val = chkShow.Checked ? "1" : "0";
                _result.Add(uploadID, val);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
        return _result;
    }
    protected void dgvUploadFile_flag_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            {
                e.Row.Cells[7].Visible = false;
            }
        }
        catch (Exception)
        { // Session["CGROUP"] = null

        }
    }
    protected void dgvUploadFile_flag_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView gv = (GridView)sender;
        string FullPath = gv.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string path = Path.GetDirectoryName(FullPath);
        string FileName = Path.GetFileName(FullPath);

        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(path + "\\" + FileName);
        Response.End();
    }
    protected void dgvUploadFile_flag_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Download")
        {
            GridView gv = (GridView)sender;
            int? rowIndex = ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            string FullPath = gv.DataKeys[rowIndex.Value]["FULLPATH"].ToString();
            string path = Path.GetDirectoryName(FullPath);
            string FileName = Path.GetFileName(FullPath);

            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(path + "\\" + FileName);
            Response.End();
        }
    }
    protected void chkTruckOutContract_CheckedChanged(object sender, EventArgs e)
    {
        if (chkTruckOutContract.Checked)
        {
            btnSearchByCarLicense.Enabled = false;
            btnSearchLicenseReset.Enabled = false;
        }
        else
        {
            btnSearchByCarLicense.Enabled = true;
            btnSearchLicenseReset.Enabled = true;
        }
        this.TruckSelect();
    }

    private void TruckSelect()
    {
        try
        {
            txtTrailerRegist.Value = string.Empty;

            DropDownListHelper.BindDropDownList(ref cboHeadRegist, null, string.Empty, string.Empty, false);        //ทะเบียนรถ
            DropDownListHelper.BindDropDownList(ref cboDelivery, null, string.Empty, string.Empty, false);          //Delivery No

            if (chkTruckOutContract.Checked)
                dtTruck = ComplainBLL.Instance.TruckOutContractSelectBLL(this.GetConditionVendor());
            else
                dtTruck = ComplainBLL.Instance.TruckSelectBLL(this.GetConditionVendor());
            DropDownListHelper.BindDropDownList(ref cboHeadRegist, dtTruck, "STRUCKID", "SHEADREGISTERNO", true);
            if (!string.IsNullOrEmpty(strHeadRegistNo)) // Filter By ทะเบียนรถ
            {
                ListItem selectedListItem = cboHeadRegist.Items.FindByText(strHeadRegistNo);
                if (selectedListItem != null) selectedListItem.Selected = true;
            }
        }

        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvUploadReqDoc_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void dgvUploadReqDoc_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadReqDoc.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    protected void dgvUploadReqDoc_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void cmdRequestDoc_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateDocument();
            DataTable dtReqDoc = new DataTable();
            dtReqDoc.Columns.Add("LINK");
            dtReqDoc.Columns.Add("VENDOR");
            dtReqDoc.Columns.Add("COMPLAIN");
            dtReqDoc.Columns.Add("CONTRACT");
            dtReqDoc.Columns.Add("CAR");
            dtReqDoc.Columns.Add("SOURCE");
            dtReqDoc.Columns.Add("COMPLAIN_DEPARTMENT");
            dtReqDoc.Columns.Add("CREATE_DEPARTMENT");
            string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_Complain_ReqDoc.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DocID), MachineKeyProtection.All);
            dtReqDoc.Rows.Add(Link, cboVendor.SelectedItem, cboComplainType.SelectedItem, cboContract.SelectedItem, cboHeadRegist.SelectedItem, txtShipTo.Text.Trim(), txtFNameDivision.Text.Trim(), txtComplainDivisionBy.Value.Trim());
            this.SendEmail(11);
            alertSuccess("ขอเอกสารเพิ่มเติม เรียบร้อย");
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void ValidateDocument()
    {
        try
        {
            if (string.Equals(txtRequestDoc.Text.Trim(), string.Empty))
                throw new Exception("กรุณากรอกข้อมูลก่อนขอข้อมูลเพิ่มเติม");

            if (cboVendor.SelectedIndex == 0)
                throw new Exception("กรุณาเลือกผู้ขนส่ง");

            if (string.Equals(DocID, string.Empty))
                throw new Exception("ไม่มีเอกสารข้อร้องเรียน");
        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdDriverSearch2_Click(object sender, EventArgs e)
    {
        try
        {
            lblDriverSearchTitle2.Text = "ค้นหา พขร.";

            if ((dtEmployee == null) || (dtEmployee.Rows.Count == 0))
            {
                alertSuccess("ไม่มีข้อมูล พขร.");
                return;
            }

            dtDriverSearch = dtEmployee.Copy();
            dtDriverSearch.Rows.RemoveAt(0);
            GridViewHelper.BindGridView(ref dgvDriverSearch2, dtDriverSearch);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDriverSearch2", "$('#ShowDriverSearch2').modal();", true);
            updDriverSearch2.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdStartSearchDriver2_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver2.Text.Trim() + "%'" + " OR SEMPLOYEEID LIKE '%" + txtSearchDriver2.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                //GridViewHelper.BindGridView(ref dgvDriverSearch, dtEmployee);
                txtSearchDriver2.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            GridViewHelper.BindGridView(ref dgvDriverSearch2, dtTmp);

            updDriverSearch2.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvDriverSearch2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver2.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                GridViewHelper.BindGridView(ref dgvDriverSearch2, dtEmployee);
                txtSearchDriver2.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            dgvDriverSearch2.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDriverSearch2, dtTmp);
            updDriverSearch2.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvDriverSearch2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            if (cmbPersonalNo2.Enabled)
                cmbPersonalNo2.SelectedValue = dgvDriverSearch2.DataKeys[e.RowIndex].Value.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdDriverSearch3_Click(object sender, EventArgs e)
    {
        try
        {
            lblDriverSearchTitle3.Text = "ค้นหา พขร.";

            if ((dtEmployee == null) || (dtEmployee.Rows.Count == 0))
            {
                alertSuccess("ไม่มีข้อมูล พขร.");
                return;
            }

            dtDriverSearch = dtEmployee.Copy();
            dtDriverSearch.Rows.RemoveAt(0);
            GridViewHelper.BindGridView(ref dgvDriverSearch3, dtDriverSearch);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDriverSearch3", "$('#ShowDriverSearch3').modal();", true);
            updDriverSearch3.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdStartSearchDriver3_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver3.Text.Trim() + "%'" + " OR SEMPLOYEEID LIKE '%" + txtSearchDriver3.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                //GridViewHelper.BindGridView(ref dgvDriverSearch, dtEmployee);
                txtSearchDriver3.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            GridViewHelper.BindGridView(ref dgvDriverSearch3, dtTmp);

            updDriverSearch3.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvDriverSearch3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver3.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                GridViewHelper.BindGridView(ref dgvDriverSearch3, dtEmployee);
                txtSearchDriver3.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            dgvDriverSearch3.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDriverSearch3, dtTmp);
            updDriverSearch3.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvDriverSearch3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            if (cmbPersonalNo3.Enabled)
                cmbPersonalNo3.SelectedValue = dgvDriverSearch3.DataKeys[e.RowIndex].Value.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdDriverSearch4_Click(object sender, EventArgs e)
    {
        try
        {
            lblDriverSearchTitle4.Text = "ค้นหา พขร.";

            if ((dtEmployee == null) || (dtEmployee.Rows.Count == 0))
            {
                alertSuccess("ไม่มีข้อมูล พขร.");
                return;
            }

            dtDriverSearch = dtEmployee.Copy();
            dtDriverSearch.Rows.RemoveAt(0);
            GridViewHelper.BindGridView(ref dgvDriverSearch4, dtDriverSearch);

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowDriverSearch4", "$('#ShowDriverSearch4').modal();", true);
            updDriverSearch4.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdStartSearchDriver4_Click(object sender, EventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver4.Text.Trim() + "%'" + " OR SEMPLOYEEID LIKE '%" + txtSearchDriver4.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                //GridViewHelper.BindGridView(ref dgvDriverSearch, dtEmployee);
                txtSearchDriver4.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            GridViewHelper.BindGridView(ref dgvDriverSearch4, dtTmp);

            updDriverSearch4.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvDriverSearch4_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DataRow[] dr = dtDriverSearch.Select("FULLNAME LIKE '%" + txtSearchDriver4.Text.Trim() + "%'");
            if (dr.Length == 0)
            {
                GridViewHelper.BindGridView(ref dgvDriverSearch4, dtEmployee);
                txtSearchDriver4.Text = string.Empty;
                alertSuccess("ไม่พบข้อมูลตามเงื่อนไข");
                return;
            }
            DataTable dtTmp = dr.CopyToDataTable();

            dgvDriverSearch4.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDriverSearch4, dtTmp);
            updDriverSearch4.Update();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvDriverSearch4_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            if (cmbPersonalNo4.Enabled)
                cmbPersonalNo4.SelectedValue = dgvDriverSearch4.DataKeys[e.RowIndex].Value.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModal();", true);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void dgvSuspendDriver_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void dgvSuspendDriver_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cb = (CheckBox)e.Row.FindControl("chkSuspend");
            CheckBox bl = (CheckBox)e.Row.FindControl("chkBlacklistDriver");
            TextBox t = (TextBox)e.Row.FindControl("txtDays");
            TextBox tshd = (TextBox)e.Row.FindControl("txtStartHoldDate");
            TextBox tehd = (TextBox)e.Row.FindControl("txtEndHoldDate");
            TextBox tbl = (TextBox)e.Row.FindControl("txtBlacklist");
            DataTable dt = ComplainBLL.Instance.SearchDateEndLockDriverBLL(" AND DOC_ID = '" + DocID + "' AND SEMPLOYEEID = '" + dgvSuspendDriver.DataKeys[e.Row.RowIndex]["SDRIVERNO"].ToString() + "'");
            if (!string.IsNullOrEmpty(t.Text.Trim()) && !string.Equals(t.Text.Trim(), "0"))
            {
                if (dt.Rows.Count > 0)
                {
                    tshd.Text = Convert.ToDateTime(dt.Rows[0]["DATE_START"].ToString()).ToString("dd/MM/yyyy");//Convert.ToDateTime(dt.Rows[0]["DATE_START"].ToString()).ToString("dd/MM/yyyy");
                    tehd.Text = dt.Rows[0]["DATE_END"].ToString() == "" ? Convert.ToDateTime(dt.Rows[0]["DATE_START"].ToString()).AddDays(int.Parse(t.Text.Trim())).ToString("dd/MM/yyyy") : (Convert.ToDateTime(dt.Rows[0]["DATE_END"].ToString()).ToString("dd/MM/yyyy") == Convert.ToDateTime(dt.Rows[0]["DATE_START"].ToString()).AddDays(int.Parse(t.Text.Trim())).ToString("dd/MM/yyyy") ? Convert.ToDateTime(dt.Rows[0]["DATE_END"].ToString()).ToString("dd/MM/yyyy") : Convert.ToDateTime(dt.Rows[0]["DATE_START"].ToString()).AddDays(int.Parse(t.Text.Trim())).ToString("dd/MM/yyyy"));
                }
                else
                {
                    tshd.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    tehd.Text = DateTime.Now.AddDays(int.Parse(t.Text.Trim())).ToString("dd/MM/yyyy");
                }
                cb.Checked = true;
            }
            else
            {
                t.Enabled = false;
                tshd.Text = string.Empty;
                tehd.Text = string.Empty;
                cb.Checked = false;
            }

            if (!string.IsNullOrEmpty(tbl.Text.Trim()) && !string.Equals(tbl.Text.Trim(), "0"))
            {
                bl.Checked = true;
            }
            else
            {
                bl.Checked = false;
            }
        }
    }
    protected void dgvSuspendDriver_RowCreated(object sender, GridViewRowEventArgs e)
    {

    }
    protected void dgvSuspendDriver_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void dgvSuspendDriver_DataBound(object sender, EventArgs e)
    {

    }
    protected void cmdOTPDoc_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = ComplainBLL.Instance.OTPCheckBLL(Text1.Value.Trim(), 1);
            if (dt.Rows.Count > 0)
            {
                OTP = dt.Rows[0]["OTP_CODE"].ToString();
                if (dtData.Rows[0]["IS_URGENT"].ToString() == "1")
                {
                    cmdSaveTab2.Enabled = true;
                }
                else
                {
                    if ((DateTime.ParseExact(dtData.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) >= DateTime.ParseExact(dtData.Rows[0]["COMPLAIN_VENDOR_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)) && (DateTime.ParseExact(dtData.Rows[0]["CHECK_DATE"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo) <= DateTime.ParseExact(dtData.Rows[0]["COMPLAIN_PTT_NORMAL"].ToString(), "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo)))
                        cmdSaveTab2.Enabled = true;
                }
                //this.StartUpload();
                if (!string.Equals(OTP, string.Empty))
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalOTPDoc();", true);
            }
            else
            {
                OTP = string.Empty;
                throw new Exception("รหัส OTP ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdCancelOTPDoc_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_Complain_lst_New.aspx");
    }
    protected void cmbPersonalNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (cmbPersonalNo.SelectedIndex > 0)
            {
                if (cmbPersonalNo.SelectedValue == cmbPersonalNo2.SelectedValue || cmbPersonalNo.SelectedValue == cmbPersonalNo3.SelectedValue || cmbPersonalNo.SelectedValue == cmbPersonalNo4.SelectedValue)
                {
                    alertSuccess("ข้อมูล พขร. ซ้ำ");
                    cmbPersonalNo.ClearSelection();
                    cmbPersonalNo2.Enabled = false;
                    cmdDriverSearch2.Enabled = false;
                    return;
                }
                cmbPersonalNo2.Enabled = true;
                cmdDriverSearch2.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmbPersonalNo2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (cmbPersonalNo2.SelectedIndex > 0)
            {
                if (cmbPersonalNo2.SelectedValue == cmbPersonalNo.SelectedValue || cmbPersonalNo2.SelectedValue == cmbPersonalNo3.SelectedValue || cmbPersonalNo2.SelectedValue == cmbPersonalNo4.SelectedValue)
                {
                    alertSuccess("ข้อมูล พขร. ซ้ำ");
                    cmbPersonalNo2.ClearSelection();
                    cmbPersonalNo3.Enabled = false;
                    cmdDriverSearch3.Enabled = false;
                    return;
                }
                cmbPersonalNo3.Enabled = true;
                cmdDriverSearch3.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmbPersonalNo3_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (cmbPersonalNo3.SelectedIndex > 0)
            {
                if (cmbPersonalNo3.SelectedValue == cmbPersonalNo2.SelectedValue || cmbPersonalNo3.SelectedValue == cmbPersonalNo.SelectedValue || cmbPersonalNo3.SelectedValue == cmbPersonalNo4.SelectedValue)
                {
                    alertSuccess("ข้อมูล พขร. ซ้ำ");
                    cmbPersonalNo3.ClearSelection();
                    cmbPersonalNo4.Enabled = false;
                    cmdDriverSearch4.Enabled = false;
                    return;
                }
                cmbPersonalNo4.Enabled = true;
                cmdDriverSearch4.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmbPersonalNo4_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (cmbPersonalNo4.SelectedIndex > 0)
            {
                if (cmbPersonalNo4.SelectedValue == cmbPersonalNo2.SelectedValue || cmbPersonalNo4.SelectedValue == cmbPersonalNo3.SelectedValue || cmbPersonalNo4.SelectedValue == cmbPersonalNo.SelectedValue)
                {
                    alertSuccess("ข้อมูล พขร. ซ้ำ");
                    cmbPersonalNo4.ClearSelection();
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    protected void cmdUploadReqDoc_Click(object sender, EventArgs e)
    {
        this.StartUpload2();
        if (dtUpload.Rows.Count > 0 && dtUploadReqDoc.Rows.Count > 0)
        {
            this.ValidateSaveTab2();
            this.ValidateSaveTab2ReqDoc();
            cmdSaveTab2.Enabled = true;
        }
    }
    protected void dgvHistoryComplain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void dgvHistoryComplain_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void dgvHistoryComplain_RowCreated(object sender, GridViewRowEventArgs e)
    {

    }
    protected void dgvHistoryComplain_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    private void txtDays_OnKeyPress(TextBox txtDays, string args, int RowIndex)
    {
        try
        {
            TextBox t = (TextBox)dgvSuspendDriver.Rows[RowIndex].FindControl("txtDays");
            TextBox txtStartHoldDate = (TextBox)dgvSuspendDriver.Rows[RowIndex].FindControl("txtStartHoldDate");//หา control เริ่มระงับวันที่
            TextBox txtEndHoldDate = (TextBox)dgvSuspendDriver.Rows[RowIndex].FindControl("txtEndHoldDate");
            if (!string.Equals(t.Text.Trim(), string.Empty))
            {
                txtEndHoldDate.Text = DateTime.ParseExact(txtStartHoldDate.Text, "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo).AddDays(int.Parse(t.Text.Trim())).ToString("dd/MM/yyyy");
            }
            else
            {
                t.Text = "0";
                txtEndHoldDate.Text = DateTime.ParseExact(txtStartHoldDate.Text, "dd/MM/yyyy", DateTimeFormatInfo.CurrentInfo).AddDays(int.Parse(t.Text.Trim())).ToString("dd/MM/yyyy");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    private void InsertDriverStatus(string I_DRIVERID, string I_PERSONALID, string I_FIRSTNAME, string I_LASTNAME, string I_DRIVERSTATUS, string I_CARIERCODE, string I_PICTUREPATH, string I_UPDATEDATETIME)
    {
        var YourModel = new { ID = I_DRIVERID, PERSONAL_ID = I_PERSONALID, FIRST_NAME = I_FIRSTNAME, LAST_NAME = I_LASTNAME, DRIVERSTATUS = I_DRIVERSTATUS, CARIER_CODE = I_CARIERCODE, PICTUREPATH = I_PICTUREPATH, UPDATEDATETIME = I_UPDATEDATETIME };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_driver/InsertDriver", stringContent)
                               .Result;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                //ผ่าน
            }

        }
    }
}