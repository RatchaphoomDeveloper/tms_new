﻿<%@ Page Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ApproveRequest.aspx.cs"
    Inherits="ApproveRequest" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
    <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
    <table width="100%" border="0" cellspacing="1" cellpadding="3">
        <tr>
            <td align="left">
                <img src="images/arr.gif" width="7" height="7" alt="" /> ประเภทคำขอ</td>
            <td align="left">
                <img src="images/arr.gif" width="7" height="7" alt="" /> คำค้นหา</td>
            <td width="268" rowspan="5" align="left" bgcolor="#E3F7F8">
                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td align="left">
                            <input type="submit" name="button19" id="button30" value="ตารางคิวงานที่ตอบรับ" onclick="javascript:location.href='booking_calendar.htm'" />
                        </td>
                        <td align="left">
                            <input type="submit" name="button19" id="button31" value="เพิ่มคำขอรับบริการ" onclick="javascript:location.href='service_add0.htm'" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" bgcolor="#FFFF66">
                            <dx:ASPxButton ID="btnPlanPrinting" runat="Server" ClientInstanceName="btnPlanPrinting" AutoPostBack="true"
                                OnClick="btnPlanPrinting_Click" Text="พิมพ์แผนงานประจำวัน">
                                <%-- <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('PlanPrint;'); }" />--%>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" bgcolor="#FFFF66">
                            <table border="0" cellspacing="1" cellpadding="1">
                                <tr>
                                    <td>
                                        <dx:ASPxDateEdit ID="dteStart" runat="Server" ClientInstanceName="dteStart" SkinID="xdte" Width="80px">
                                        </dx:ASPxDateEdit>
                                    </td>
                                    <td>
                                        <dx:ASPxDateEdit ID="dteEnd" runat="Server" ClientInstanceName="dteEnd" SkinID="xdte" Width="80px">
                                        </dx:ASPxDateEdit>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="335" align="left">
                <input type="radio" name="radio" id="radio3" value="radio" />
                ตรวจวัดน้ำ</td>
            <td width="355" align="left">
                <input name="textfield6" type="text" id="textfield5" value="บริษัทขนส่ง,ทะเบียนรถ" size="35" /></td>
        </tr>
        <tr>
            <td align="left">
                <input type="radio" name="radio" id="radio4" value="radio" />
                ตรวจสอบรับรองความถูกต้อง และตีซีลใหม่ของรถน้ำมัน</td>
            <td align="left">
                <img src="images/arr.gif" width="7" height="7" alt="" /> ตัวกรองสถานะ</td>
        </tr>
        <tr>
            <td align="left">
                <input type="radio" name="radio" id="radio5" value="radio" />
                พ่นสาระสำคัญ</td>
            <td align="left" bgcolor="#F8F8F8">
                <input type="checkbox" name="checkbox" id="checkbox4" />
                <label for="checkbox4">
                    ยังไม่ได้ยื่นคำขอ
                    <input type="checkbox" name="checkbox" id="checkbox7" />
                    รอพิจารณา
                    <input type="checkbox" name="checkbox" id="checkbox8" />
                    ปรับแก้คำขอ
                </label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <img src="images/arr.gif" width="7" height="7" alt="" /> ช่วงเวลา
                <input name="textfield3" type="text" value="เริ่มวันที่" size="8" />
                <img src="images/icbcaleno.gif" alt="" width="16" height="16" /> -
                <input name="textfield3" type="text" value="ถึงวันที่" size="8" />
                <img src="images/icbcaleno.gif" alt="" width="16" height="16" /></td>
            <td align="left" bgcolor="#F8F8F8">
                <label for="checkbox4">
                    <input type="checkbox" name="checkbox4" id="checkbox9" />
                    รอยืนยันคิวนัดหมาย
                    <input type="checkbox" name="checkbox4" id="checkbox10" />
                    รอเข้ารับบริการ
                    <input type="checkbox" name="checkbox2" id="checkbox" />
                    รอบันทึกผล</label></td>
        </tr>
        <tr>
            <td align="left">
                <input type="radio" name="radio" id="radio" value="radio" />
                วันที่ยื่นคำขอ
                <input type="radio" name="radio" id="radio2" value="radio" />
                วันที่หมดอายุวัดน้ำ</td>
            <td align="left" bgcolor="#F8F8F8">
                <input type="checkbox" name="checkbox3" id="checkbox2" />
                รอนำส่งเอกสารปิดงาน
                <input type="checkbox" name="checkbox5" id="checkbox3" />
                รถไม่ได้มาตรฐาน
                <input type="checkbox" name="checkbox6" id="checkbox5" />
                ปิดงาน</td>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                <img src="images/button_bigl_searchx.gif" width="79" height="28"></td>
            <td align="left">
                &nbsp;</td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td bgcolor="#0E4999">
                <img src="images/spacer.GIF" width="250" height="1"></td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="2" cellspacing="1">
        <tr>
            <td width="7%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ทะเบียนหัว</td>
            <td width="8%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ทะเบียนท้าย</td>
            <td width="8%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ยื่นคำขอ<img src="images/ic_sort21.gif" border="0"></td>
            <td width="9%" align="center" bgcolor="#B1E8ED">
                <span style="text-align: center">หมดอายุวัดน้ำ</span></td>
            <td width="13%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                สาเหตุ</td>
            <td width="15%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                บริษัทผู้ขนส่ง</td>
            <td width="13%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                สถานะ</td>
            <td width="7%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                จัดคิว</td>
            <td width="6%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ใบงาน</td>
            <td width="5%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                บันทึก</td>
        </tr>
        <tr>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                กพ-9987</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                พด-8874</td>
            <td align="center" bgcolor="#E3F7F8">
                01/01/2557</td>
            <td align="center" bgcolor="#E3F7F8">
                01/01/2557</td>
            <td align="left" bgcolor="#E3F7F8">
                ค่าวัดน้ำปกติ หรือครบวาระ</td>
            <td align="left" bgcolor="#E3F7F8">
                บ.ที เอส เค โลจิสติกส์</td>
            <td width="13%" align="center" bgcolor="#E3F7F8">
                รอยืนยันคิวนัดหมาย</td>
            <td width="7%" align="center" bgcolor="#E3F7F8">
                <span style="text-align: center">
                    <input type="submit" name="button2" id="button4" value="จัดคิว" onclick="javascript:location.href='approve_add1.htm'" />
                </span></td>
            <td width="6%" align="center" bgcolor="#E3F7F8">
                <dx:ASPxButton ID="btnPrintJob" runat="Server" Text="พิมพ์" OnClick="btnPrintJob_Click" CommandArgument="1406/02-0002">
                </dx:ASPxButton>
            </td>
            <td width="5%" align="center" bgcolor="#E3F7F8">
                <input type="submit" name="button14" id="Submit2" value="บันทึก" onclick="javascript:location.href='result-add.htm'" />
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                กผ-9632</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                อท-9965</td>
            <td align="center" bgcolor="#B9EAEF">
                01/01/2557</td>
            <td align="center" bgcolor="#B9EAEF">
                01/01/2557</td>
            <td align="left" bgcolor="#B9EAEF">
                รถประสบอุบัติเหตุ</td>
            <td align="left" bgcolor="#B9EAEF">
                บ.สมพงษ์ ทรานสปอร์ต </td>
            <td width="13%" align="center" bgcolor="#B9EAEF">
                รอยืนยันคิวนัดหมาย</td>
            <td width="7%" align="center" bgcolor="#B9EAEF">
                <span style="text-align: center">
                    <input type="submit" name="button3" id="button5" value="จัดคิว" onclick="javascript:location.href='approve_add1.htm'" />
                </span></td>
            <td width="5%" align="center" bgcolor="#B9EAEF">
                <span style="text-align: center">
                    <input type="submit" name="button8" id="button8" value="พิมพ์" onclick="javascript:location.href='print_job.htm'" />
                </span></td>
            <td width="6%" align="center" bgcolor="#B9EAEF">
                <input type="submit" name="button14" id="Submit1" value="บันทึก" onclick="javascript:location.href='result-add.htm'" />
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                กก-9875</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                สว-9874</td>
            <td align="center" bgcolor="#E3F7F8">
                01/01/2557</td>
            <td align="center" bgcolor="#E3F7F8">
                01/01/2557</td>
            <td align="left" bgcolor="#E3F7F8">
                ซีล M/H ขาดชำรุดจากการใช้งาน โดนเท้าเหยียบ ฝาครอบกันงัดกระแทกซีลขาด</td>
            <td align="left" bgcolor="#E3F7F8">
                บ.ศิริเสาวภาขนส่ง</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                รอเข้ารับบริการ</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                <input name="button5" type="submit" class="style7" id="button18" onclick="javascript:location.href='booking_calendar.htm'"
                    value="จัดคิว" /></td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                <input type="submit" name="button9" id="button9" value="พิมพ์" onclick="javascript:location.href='print_job.htm'" />
            </td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                <input type="submit" name="button14" id="button14" value="บันทึก" onclick="javascript:location.href='result-add.htm'" />
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                กด-9855</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                วง-9641</td>
            <td align="center" bgcolor="#B9EAEF">
                01/01/2557</td>
            <td align="center" bgcolor="#B9EAEF">
                01/01/2557</td>
            <td align="left" bgcolor="#B9EAEF">
                พ่นสาระสำคัญ</td>
            <td align="left" bgcolor="#B9EAEF">
                บ.เจ๊ใหญ่ขนส่ง</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                รอบันทึกผล</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                <input name="button25" type="submit" class="style7" id="button25" onclick="javascript:location.href='booking_calendar.htm'"
                    value="จัดคิว" /></td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                <input type="submit" name="button20" id="button20" value="พิมพ์" onclick="javascript:location.href='print_job.htm'" />
            </td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                <input type="submit" name="button15" id="button15" value="บันทึก" onclick="javascript:location.href='result-add.htm'" />
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                กอ-7485</td>
            <td align="left" bgcolor="#E3F7F8" style="text-align: center">
                สด-9544</td>
            <td bgcolor="#E3F7F8" align="center">
                01/01/2557</td>
            <td bgcolor="#E3F7F8" align="center">
                01/01/2557</td>
            <td align="left" bgcolor="#E3F7F8">
                ระยะแป้นคลาดเคลื่อน</td>
            <td bgcolor="#E3F7F8" align="left">
                บ.รุ่งพิทักษ์การปิโตรเลียม</td>
            <td width="13%" align="center" bgcolor="#E3F7F8">
                รอนำส่งเอกสารปิดงาน</td>
            <td width="7%" align="center" bgcolor="#E3F7F8">
                <input name="button26" type="submit" class="style7" id="button26" onclick="javascript:location.href='booking_calendar.htm'"
                    value="จัดคิว" /></td>
            <td width="5%" align="center" bgcolor="#E3F7F8">
                <input type="submit" name="button21" id="button21" value="พิมพ์" onclick="javascript:location.href='print_job.htm'" />
            </td>
            <td width="6%" align="center" bgcolor="#E3F7F8">
                <input type="submit" name="button16" id="button16" value="บันทึก" onclick="javascript:location.href='result-add5-2.htm'" />
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                กป-9831</td>
            <td align="left" bgcolor="#B9EAEF" style="text-align: center">
                รน-9874</td>
            <td bgcolor="#B9EAEF" align="center">
                01/01/2557</td>
            <td bgcolor="#B9EAEF" align="center">
                01/01/2557</td>
            <td align="left" bgcolor="#B9EAEF">
                รถประสบอุบัติเหตุ</td>
            <td bgcolor="#B9EAEF" align="left">
                xxxxxxxxxxxxxxxxxxxxxxx</td>
            <td align="center" bgcolor="#B9EAEF">
                รถไม่ได้มาตรฐาน</td>
            <td align="center" bgcolor="#B9EAEF">
                <span style="text-align: center">
                    <input type="submit" name="button4" id="button" value="จัดคิว" onclick="javascript:location.href='approve_add1.htm'" />
                </span></td>
            <td align="center" bgcolor="#B9EAEF">
                <input type="submit" name="button" id="button3" value="พิมพ์" onclick="javascript:location.href='print_job.htm'" />
            </td>
            <td align="center" bgcolor="#B9EAEF">
                <span style="text-align: center">
                    <input name="button23" type="submit" class="style7" id="button2" onclick="javascript:location.href='result-add.htm'"
                        value="บันทึก" />
                </span></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                กป-9831</td>
            <td align="left" bgcolor="#E3F7F8" style="text-align: center">
                รน-9874</td>
            <td bgcolor="#E3F7F8" align="center">
                01/01/2557</td>
            <td bgcolor="#E3F7F8" align="center">
                01/01/2557</td>
            <td align="left" bgcolor="#E3F7F8">
                รถประสบอุบัติเหตุ</td>
            <td bgcolor="#E3F7F8" align="left">
                xxxxxxxxxxxxxxxxxxxxxxx</td>
            <td width="13%" align="center" bgcolor="#E3F7F8">
                ปิดงาน</td>
            <td width="7%" align="center" bgcolor="#E3F7F8">
                <input name="button27" type="submit" class="style7" id="button27" onclick="javascript:location.href='booking_calendar.htm'"
                    value="จัดคิว" /></td>
            <td width="5%" align="center" bgcolor="#E3F7F8">
                <input type="submit" name="button22" id="button22" value="พิมพ์" onclick="javascript:location.href='print_job.htm'" />
            </td>
            <td width="6%" align="center" bgcolor="#E3F7F8">
                <input type="submit" name="button17" id="button17" value="บันทึก" onclick="javascript:location.href='result-add.htm'" />
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
            <td colspan="2" align="left">
                &nbsp;</td>
            <td align="center" style="text-align: center">
                &nbsp;</td>
            <td colspan="6" align="right">
                หน้าที่
                <input name="textfield" type="text" id="textfield" value="1/12" size="5" />
                แสดงหน้าละ
                <input name="textfield2" type="text" id="textfield2" value="10" size="5" /></td>
        </tr>
    </table>
    <br />
    <table width="100%" border="0" cellpadding="2" cellspacing="1">
        <tr>
            <td height="35" colspan="7" align="left">
                ขอสำเนาเอกสารการตรวจสอบปริมาตรถังน้ำมัน<br>
            </td>
        </tr>
        <tr>
            <td width="9%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ทะเบียนหัว</td>
            <td width="8%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ทะเบียนท้าย</td>
            <td width="9%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ยื่นคำขอ<img src="images/ic_sort21.gif" border="0"></td>
            <td width="21%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                บริษัทผู้ขนส่ง</td>
            <td width="31%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                ขอรับบริการ</td>
            <td width="17%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                สถานะ</td>
            <td width="5%" align="center" bgcolor="#B1E8ED" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                กพ-9987</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                พด-8874</td>
            <td align="center" bgcolor="#E3F7F8">
                01/01/2557</td>
            <td align="left" bgcolor="#E3F7F8">
                บ.ที เอส เค โลจิสติกส์</td>
            <td width="31%" align="center" bgcolor="#E3F7F8">
                ขอสำเนาเอกสารการตรวจสอบปริมาตรถังน้ำมัน</td>
            <td width="17%" align="center" bgcolor="#E3F7F8">
                <span style="text-align: center">รอบันทึกผล</span></td>
            <td width="5%" align="center" bgcolor="#E3F7F8">
                <dx:ASPxButton ID="btnSave" runat="Server" Text="บันทึก" OnClick="btnSave_Click" CommandArgument="1406/01-0003">
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                กผ-9632</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                อท-9965</td>
            <td align="center" bgcolor="#B9EAEF">
                01/01/2557</td>
            <td align="left" bgcolor="#B9EAEF">
                บ.สมพงษ์ ทรานสปอร์ต </td>
            <td width="31%" align="center" bgcolor="#B9EAEF">
                ขอสำเนาเอกสารการตรวจสอบปริมาตรถังน้ำมัน</td>
            <td width="17%" align="center" bgcolor="#B9EAEF">
                <span style="text-align: center">รอบันทึกผล</span></td>
            <td width="5%" align="center" bgcolor="#B9EAEF">
                <span style="text-align: center">
                    <input type="submit" name="button12" id="button11" value="บันทึก" onclick="javascript:location.href='result-add.htm'">
                </span></td>
        </tr>
        <tr>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                กก-9875</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                สว-9874</td>
            <td align="center" bgcolor="#E3F7F8">
                01/01/2557</td>
            <td align="left" bgcolor="#E3F7F8">
                บ.ศิริเสาวภาขนส่ง</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                ขอสำเนาเอกสารการตรวจสอบปริมาตรถังน้ำมัน</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                ปิดงาน</td>
            <td align="center" bgcolor="#E3F7F8" style="text-align: center">
                <input type="submit" name="button18" id="button12" value="บันทึก" onclick="javascript:location.href='result-add.htm'">
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                กด-9855</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                วง-9641</td>
            <td align="center" bgcolor="#B9EAEF">
                01/01/2557</td>
            <td align="left" bgcolor="#B9EAEF">
                บ.เจ๊ใหญ่ขนส่ง</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                ขอสำเนาเอกสารการตรวจสอบปริมาตรถังน้ำมัน</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                ปิดงาน</td>
            <td align="center" bgcolor="#B9EAEF" style="text-align: center">
                <input type="submit" name="button10" id="button33" value="บันทึก" onclick="javascript:location.href='result-add.htm'">
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
            <td colspan="2" align="left">
                &nbsp;</td>
            <td colspan="4" align="right">
                หน้าที่
                <input name="textfield4" type="text" id="textfield3" value="1/12" size="5">
                แสดงหน้าละ
                <input name="textfield4" type="text" id="textfield4" value="10" size="5"></td>
        </tr>
    </table>
</asp:Content>
