﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AccidentList.aspx.cs" Culture="en-US" UICulture="en-US" Inherits="AccidentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="form-horizontal">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>ค้นหา
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">วันที่เกิดเหตุ</label>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDate" />
                            </div>
                            <label class="col-md-2 control-label">ถึง</label>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDateTo" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ประเภทอุบัติเหตุ</label>
                            <div class="col-md-3">
                                <asp:RadioButtonList runat="server" ID="rblType" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="&nbsp;ร้ายแรง&nbsp;" Value="0" />
                                    <asp:ListItem Text="&nbsp;ไม่ร้ายแรง&nbsp;" Value="1" />
                                </asp:RadioButtonList>
                            </div>
                            <label class="col-md-2 control-label">ประเภทอุบัติเหตุ</label>
                            <div class="col-md-3">
                                
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">กลุ่มงาน</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlWorkGroup" OnSelectedIndexChanged="ddlWorkGroup_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>

                            </div>
                            <label class="col-md-2 control-label">กลุ่มที่</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGroup">
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เลขที่สัญญา</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ค้นหา</label>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" placeholder ="Accident ID,ทะเบียนรถ,พขร." />
                            </div>
                            <label class="col-md-2 control-label">สถานะเอกสาร</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlStatus" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-8">

                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                        <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                        <asp:Button Text="Export" runat="server" ID="btnExport" class="btn btn-md bth-hover btn-info" OnClick="btnExport_Click" />
                        <a href="AccidentTab1.aspx" target="_blank" runat="server" id="aAdd" class="btn btn-md bth-hover btn-info" style="color:white;">
                            เพิ่ม
                        </a>
                        <a href="AccidentConclude.aspx" target="_blank" runat="server" id="aConclude" class="btn btn-md bth-hover btn-info" style="color:white;">
                            สรุปข้อมูลรถขนส่งเกิดอุบัติเหตุ
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>ผลการค้นหา จำนวน <asp:Label Text="0" runat="server" ID="lblItem" /> รายการ
            </div>
            <div class="panel-body">
                <asp:GridView runat="server" ID="gvAccident"
                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                    HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                    AllowPaging="True" OnRowUpdating="gvAccident_RowUpdating" DataKeyNames="ACCIDENT_ID" OnPageIndexChanging="gvAccident_PageIndexChanging">
                    <Columns>
                        <asp:BoundField HeaderText="Accident ID" DataField="ACCIDENT_ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="วันที่-เวลาเกิดเหตุ" DataField="ACCIDENT_DATE" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="วันที่-เวลาที่แจ้งเรื่องในระบบ" DataField="SYS_TIME" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-HorizontalAlign="Center" Visible="false" />
                        <asp:BoundField HeaderText="ผขส. แจ้งเหตุให้ ปตท." DataField="REPORT_PTT" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-HorizontalAlign="Center" Visible="false" />
                        <asp:BoundField HeaderText="ชื่อผู้แจ้ง" DataField="USERNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                        <asp:BoundField HeaderText="กลุ่มงาน" DataField="GROUPSNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="ชื่อผู้ขนส่ง" DataField="VENDORNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="ทะเบียน(หัว)" DataField="SHEADREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="ทะเบียน(ห่าง)" DataField="STRAILERREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="พขร." DataField="EMPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="เกิดเหตุ" DataField="ACCIDENTTYPENAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="ความเสียหาย" DataField="SERIOUSNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="ฝ่ายถูก/ผิด" DataField="EVALUATIONNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="สถานะ" DataField="STATUSNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hidCACTIVE" Value='<%# Eval("CACTIVE") %>' />
                                <div class="col-sm-1">
                                    <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="View" CommandName="update"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

