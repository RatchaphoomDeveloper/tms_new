﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class admin_reducepoint : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            Response.Write(DateTime.Today.AddDays(-(((double)DateTime.Today.Day)-1)).ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            dteStart.Value = DateTime.Today.AddDays(-(((double)DateTime.Today.Day) - 1)).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
           dteEnd.Value = DateTime.Today.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
}