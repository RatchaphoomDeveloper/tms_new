﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_Entity;
using Utility;

public partial class Admin_VisitForm_Config : PageBase
{
    #region " Prop "
    protected USER_PERMISSION Page_Status
    {
        get { return (USER_PERMISSION)ViewState[this.ToString() + "Page_Status"]; }
        set { ViewState[this.ToString() + "Page_Status"] = value; }
    }

    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    #endregion " Prop "

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            InitForm();
            LoadMain();
            this.AssignAuthen();
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_VisitForm_config_add.aspx");
    }
    public void LoadMain()
    {
        try
        {
            string _err = string.Empty;
            SearchFormConfig _search = new SearchFormConfig();
            _search.STYPEVISITFORMNAME = txtSearchFormName.Text.Trim();
            _search.YEAR = ddlYearSearch.SelectedValue;
            _search.Y_CACTIVE = rdoStatus.SelectedValue;
            SearhData = new QuestionnaireBLL().GetConfig(ref _err, _search);
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }


    }
    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }
    public void InitForm()
    {
        txtSearchFormName.Text = string.Empty;

        int StartYear = 2015;
        int cYear = DateTime.Today.Year;
        int endYear = cYear + 4;
        int index = 0;
        for (int i = StartYear; i <= endYear; i++)
        {
            ddlYearSearch.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
        }

        ddlYearSearch.Items.Insert(index, new ListItem("-- เลือกทั้งหมด --", ""));

        // เลือกปีปัจจุบัน
        // ListItem sel = ddlYearSearch.Items.FindByValue(cYear.ToString());
        // if (sel != null) sel.Selected = true;
        // else ddlYearSearch.SelectedIndex = 0;

        // Default เลือกทั้งหมด
        ddlYearSearch.SelectedIndex = 0;

    }

    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }
        else if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            GotoDefault();
            return;
        }
    }

    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RowSelect")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            int _keyNTYPEVISITFORMID = int.Parse(grvMain.DataKeys[rowIndex.Value]["ID"].ToString());
            if (_keyNTYPEVISITFORMID != null)
            {
                Session["EditYEARID"] = _keyNTYPEVISITFORMID;
                Response.Redirect("admin_VisitForm_config_add.aspx");
            }
        }
        else if (e.CommandName == "View")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            int _keyNTYPEVISITFORMID = int.Parse(grvMain.DataKeys[rowIndex.Value]["ID"].ToString());
            if (_keyNTYPEVISITFORMID != null)
            {
                Session["ViewYEARID"] = _keyNTYPEVISITFORMID;
                Response.Redirect("admin_VisitForm_config_add.aspx");
            }
        }
    }
    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk = e.Row.Cells[1].FindControl("chkActive") as CheckBox;
            if (chk != null)
            {
                string CACTIVE = DataBinder.Eval(e.Row.DataItem, "Y_CACTIVE").ToString();
                chk.Checked = CACTIVE == "1" ? true : false;
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtSearchFormName.Text = string.Empty;
        int cYear = DateTime.Today.Year;
        ListItem sel = ddlYearSearch.Items.FindByValue(cYear.ToString());
        if (sel != null) sel.Selected = true;
        else ddlYearSearch.SelectedIndex = 0;
        rdoStatus.ClearSelection();
        rdoStatus.SelectedIndex = 0;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadMain();
    }
}