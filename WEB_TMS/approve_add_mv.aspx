﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="approve_add_mv.aspx.cs" Inherits="approve_add_mv" %>

<%@ Register TagPrefix="uc" TagName="ucCalendar" Src="~/ucCalendarDetail.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script type="text/javascript">

        //เช็คMemo
        function maxlength() {
            var Text = txtDescription.GetText();

            if (Text.length > 250) {
                txtDescription.SetText(Text.substring(0, 250));
            }
            else {

            }

        }

        //เช็คMemo
        function maxlength2() {
            var Text = MemoChageDate.GetText();

            if (Text.length > 250) {
                MemoChageDate.SetText(Text.substring(0, 250));
            }
            else {

            }

        }

        //เช็ควนที่ว่าเปลี่ยนวันไหม
        function CheckDateChage() {

            //วันที่เลือก
            var getd = PadLeft(lblCalendarDate.GetText());
            //วันที่เคยเลือก
            var gets = PadLeft(txtDateOld.GetText());


            if (getd === gets) {

                $("#trChagedate").hide();
                $("#trChagedateCause").hide();
                rblChangedate.SetEnabled(false);
                MemoChageDate.SetEnabled(false);
            }
            else {
                if (txtRkFlag.GetText() === "M") {

                }
                else {
                    $("#trChagedate").show();
                    $("#trChagedateCause").show();

                    rblChangedate.SetEnabled(true);
                    MemoChageDate.SetEnabled(true);
                }
            }
        }

        function PadLeft(Value) {
            var cdate = Value;
            var sv = "";

            if (Value === "-") {

            }
            else {
                var CV = cdate.toString();
                var _item = CV.split('/');

                var sss = _item[1].toString();
                if (sss.length > 1) {
                    sv = _item[1] + '-' + _item[0] + '-' + (parseInt(_item[2]) - 543);

                }
                else {
                    sv = '0' + sss + '-' + _item[0] + '-' + (parseInt(_item[2]) - 543);

                }


            }
            return sv;
        }


        function openFile(str) {

            window.open("" + str + "");
        }

        function OnStartUpload(e, s) {

            var filename = e.GetText();
            if (filename != "") {
                var arrfilename = filename.split('.');
                var indx = e.name.substring(e.name.lastIndexOf('_') + 1, e.name.length);

                if (arrfilename[arrfilename.length - 1].toLowerCase() == 'pdf' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpeg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'bmp' || arrfilename[arrfilename.length - 1].toLowerCase() == 'gif' || arrfilename[arrfilename.length - 1].toLowerCase() == 'png' || arrfilename[arrfilename.length - 1].toLowerCase() == 'doc' || arrfilename[arrfilename.length - 1].toLowerCase() == 'docx' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xls' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xlsx') {
                    e.Upload(indx);
                }
                else {
                    e.ClearText();
                    dxWarning('แจ้งเตือน', 'เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น');
                }
            }
        }

        function UploadFile1(e, s) {

            var filename = e.GetText();
            if (filename != "") {
                var arrfilename = filename.split('.');

                var indx = e.name.substring(e.name.lastIndexOf('_') + 1, e.name.length);


                if (arrfilename[arrfilename.length - 1].toLowerCase() == 'pdf' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpeg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'bmp' || arrfilename[arrfilename.length - 1].toLowerCase() == 'gif' || arrfilename[arrfilename.length - 1].toLowerCase() == 'png' || arrfilename[arrfilename.length - 1].toLowerCase() == 'doc' || arrfilename[arrfilename.length - 1].toLowerCase() == 'docx' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xls' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xlsx') {
                    gvwBill.PerformCallback('UPLOAD;' + indx);
                }
                else {
                    e.ClearText();
                    dxWarning('แจ้งเตือน', 'เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น');
                }
            }
        }
    </script>
    <style type="text/css">
        .cHideControl
        {
            display: none;
        }
        
        .cShowControl
        {
            display: block;
        }
        
        .cInline
        {
            display: inline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td width="50%" colspan="2">
                            <%--<dx:ASPxButton runat="server" ID="btnRequest" AutoPostBack="false" Text="แก้ไขใบเรียกเก็บ/คำนวณค่าธรรมเนียมใหม่">
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText('editData'); xcpn.PerformCallback();}" />
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText(&#39;editData&#39;); xcpn.PerformCallback();}">
                                </ClientSideEvents>
                            </dx:ASPxButton>--%>
                        </td>
                        <td colspan="2" align="right" width="50%">
                            <%--<input type="submit" name="button3" id="button3" value="ดูประวัติรถ" onclick="javascript:location.href='home_vendor.htm'">--%>
                            <%--<input type="submit" name="button7" id="button7" value="ประวัติการรับบริการ" onclick="javascript:location.href='home_vendor.htm'">--%>
                            <dx:ASPxTextBox runat="server" ID="txtSTATUSREQ_NAME" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtCallbackType" ClientVisible="false" ClientInstanceName="txtCallbackType">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtDateOld" ClientVisible="false" ClientInstanceName="txtDateOld">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtRkFlag" ClientVisible="false" ClientInstanceName="txtRkFlag">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" bgcolor="#B9EAEF">
                            รหัสวัดน้ำ
                        </td>
                        <td width="30%">
                            <dx:ASPxLabel runat="server" ID="lblSCARNUM">
                            </dx:ASPxLabel>
                        </td>
                        <td width="20%">
                            
                        </td>
                        <td width="30%">
                         
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" bgcolor="#B9EAEF">
                            วันที่ยืนคำขอ
                        </td>
                        <td width="30%">
                            <dx:ASPxLabel runat="server" ID="lbldatereq">
                            </dx:ASPxLabel>
                        </td>
                        <td width="20%" bgcolor="#B9EAEF">
                            วันที่หมดอายุวัดน้ำ
                        </td>
                        <td width="30%">
                            <dx:ASPxLabel runat="server" ID="lblDateexp">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">
                            วันที่นัดหมาย
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxLabel runat="server" ID="lblCalendarDate" ClientInstanceName="lblCalendarDate">
                                        </dx:ASPxLabel>
                                        <dx:ASPxTextBox runat="server" ID="txtUserSelectDate" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnShowCalendar" runat="server" AutoPostBack="false" SkinID="_changeappointdate">
                                        </dx:ASPxButton>
                                        <dx:ASPxTextBox ID="txtCalendarDate" runat="server" ClientVisible="false" ClientInstanceName="txtCalendarDate">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td bgcolor="#B9EAEF">
                            วันที่ รข. อนุมัติคำขอ
                        </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblRk">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr class="displayNone" id="trVisible">
                        <td colspan="4">
                            <uc:ucCalendar ID="ucCalendar1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">
                            ประเภทคำขอ
                        </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblReq">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">
                            สาเหตุ
                        </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblCause">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">
                            ทะเบียนรถ
                        </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblRegis">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">
                            ประเภทรถ
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxLabel runat="server" ID="lblCar">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td>
                                        &nbsp;-&nbsp;
                                    </td>
                                    <td>
                                        <dx:ASPxLabel runat="server" ID="lblCap">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">
                            บริษัทขนส่ง
                        </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblVendorname">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <img src="images/btnQuest1.gif" width="23" height="23" alt="" />
                            รายละเอียดความจุ
                        </td>
                        <td colspan="2" align="right">
                            <%--<span class="active">* รายการเพิ่มแป้น</span>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <dx:ASPxGridView runat="server" ID="gvw" Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn Caption=" " FieldName="SNAME" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 1" FieldName="SLOT1" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 2" FieldName="SLOT2" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 3" FieldName="SLOT3" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 4" FieldName="SLOT4" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 5" FieldName="SLOT5" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 6" FieldName="SLOT6" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 7" FieldName="SLOT7" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 8" FieldName="SLOT8" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 9" FieldName="SLOT9" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 10" FieldName="SLOT10" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsBehavior AllowDragDrop="False" AllowSort="False"></SettingsBehavior>
                                <SettingsPager AlwaysShowPager="false" Visible="false">
                                </SettingsPager>
                                <SettingsBehavior AllowDragDrop="false" AllowSort="false" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <span class="corp">จำนวนช่อง :
                                <dx:ASPxLabel ID="lblCompartTotal" runat="server" Text="" Font-Bold="True">
                                </dx:ASPxLabel>
                                ช่อง ความจุรวม :
                                <dx:ASPxLabel ID="lblCapacityTotal" runat="server" Text="" Font-Bold="True">
                                </dx:ASPxLabel>
                                ลิตร</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <img src="images/credit.png" width="16" height="16" alt="" />รายละเอียดค่าธรรมเนียม
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#6AC6DF">
                            <dx:ASPxGridView runat="server" ID="gvwService" Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn Width="30%" CellStyle-BackColor="#EAEAEA">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblServiceName" Text='<%# DataBinder.Eval(Container.DataItem,"SERVICE_NAME") %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblncount" Text='<%# Eval("NITEM") != null ? Eval("NITEM").ToString() : "" %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text='<%# Eval("UNIT") != null ? Eval("UNIT").ToString() : "" %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <CellStyle BackColor="#EAEAEA">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Width="70%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblServiceprice" Text='<%# DataBinder.Eval(Container.DataItem,"NPRICE") %>'>
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblUnit" Text='<%# DataBinder.Eval(Container.DataItem,"UNITSERVICE") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="False"></Settings>
                                <SettingsPager AlwaysShowPager="false" Visible="false">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <img src="images/cv.png" width="16" height="16" alt="" />
                            แนบไฟล์เอกสารสำคัญ
                        </td>
                    </tr>
                    <tr id="trshowdoc1" runat="server">
                        <td colspan="4">
                            <dx:ASPxGridView ID="gvwdoc" runat="server" AutoGenerateColumns="false" Width="100%"
                                Border-BorderWidth="0px" Border-BorderStyle="None" Border-BorderColor="White">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ประเภท" CellStyle-BackColor="#b9eaef" CellStyle-Border-BorderColor="White"
                                        Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblDes" Text='<%# Eval("DOC_DESCRIPTION") %>'>
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblstar" Text="*" ForeColor="Red" Visible='<%# Eval("CDYNAMIC").ToString() == "Y" ? true : false %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <CellStyle BackColor="#B9EAEF">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="คอนโทรนดูเอกสาร" CellStyle-Border-BorderColor="White"
                                        Width="50%">
                                        <DataItemTemplate>
                                            <table width="96%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left" width="4%">
                                                        <dx:ASPxButton runat="server" ID="btnpdf" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                            EnableDefaultAppearance="false" EnableTheming="false" Cursor="pointer">
                                                            <%--<Image Url="Images/ic_pdf2.gif" Height="17px" Width="17px">
                                                            </Image>--%>
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td align="left">
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <dx:ASPxLabel runat="server" ID="lblFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_NAME") %>'>
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                                <td align="left">
                                                                    <dx:ASPxButton ID="btnView" runat="server" AutoPostBack="False" CssClass="dxeLineBreakFix"
                                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                        Width="25px">
                                                                        <Image Height="20px" Url="Images/view1.png" Width="20px">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxTextBox runat="server" ID="txtFilePath" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_PATH") %>'
                                                                        ClientVisible="false">
                                                                    </dx:ASPxTextBox>
                                                                    <dx:ASPxTextBox runat="server" ID="txtFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_SYSNAME") %>'
                                                                        ClientVisible="false">
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle>
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ListRdl" CellStyle-Border-BorderColor="White" CellStyle-HorizontalAlign="Right"
                                        Width="30%">
                                        <DataItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxImage runat="server" ID="img4" ImageUrl='<%# Eval("CONSIDER").ToString() == "Y" ? "images/action_check.png": "images/action_delete.png" %>'>
                                                        </dx:ASPxImage>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxLabel ID="lblstacheck" runat="server" Text='<%# Eval("CONSIDER").ToString() == "Y" ? "ผ่าน": "ไม่ผ่าน" %>'>
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Left">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOC_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CONSIDER" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="false" />
                                <Settings ShowColumnHeaders="False"></Settings>
                                <Border BorderStyle="None" BorderColor="White" BorderWidth="0px"></Border>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr id="trdoc2" runat="server">
                        <td colspan="4" style="padding: 0 0 0 0;">
                            <dx:ASPxGridView ID="gvwBill" runat="server" AutoGenerateColumns="false" ClientInstanceName="gvwBill"
                                SkinID="_gvw" Width="100%" OnAfterPerformCallback="gvwBill_AfterPerformCallback"
                                Border-BorderWidth="0px" Border-BorderStyle="None" Border-BorderColor="White">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="รายการเอกสาร" Width="20%" CellStyle-BackColor="#B9EAEF">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text='<%#Eval("DOC_DESCRIPTION")%>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text='*' ForeColor="Red" ClientVisible='<%# (Eval("CDYNAMIC")).ToString() == "Y" ? true : false %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Width="30%">
                                        <DataItemTemplate>
                                            <dx:ASPxUploadControl ID="uclBill" runat="server" Width="70%" ClientInstanceName="uclBill"
                                                OnFileUploadComplete="uclBill_FileUploadComplete">
                                                <ClientSideEvents TextChanged="function(e,s){OnStartUpload(e,s)}" FileUploadComplete="
                                                             function(s,e){
                                                                 if(e.callbackData =='')
                                                                 {
                                                                    if(gvwBill.InCallback()) return false; else UploadFile1(s,e);
                                                                 }
                                                                 else
                                                                 {
                                                                    dxWarning('แจ้งเตือน', (e.callbackData+'').split('|')[0]);
                                                                 } 
                                                             }" />
                                            </dx:ASPxUploadControl>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Width="50%" Caption="sFileName">
                                        <DataItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <%--<img src="images/ic_pdf2.gif" width="16" height="16" border="0" class='<%# (Eval("sOpenFile")).ToString() == "N" ? "cHideControl" : "cShowControl cInline" %>' />--%>
                                                        <dx:ASPxLabel ID="lblsNameShow" runat="server" Text='<%#Bind("FILE_NAME") %>' Style="display: inline;"
                                                            CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td>
                                                        <img title="ดูเอกสาร" src="images/view1.png" width="25px" height="25px" border="0"
                                                            class='<%# (Eval("SVISIBLE")).ToString() == "N" ? "cHideControl" : "cShowControl" %>'
                                                            onclick=<%# "javascript:openFile('" + Eval("OPENFILE") + "')" %> style="cursor: pointer;">
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="btnDelOtherDoc" ClientInstanceName="btnDelOtherDoc" runat="server"
                                                            ToolTip="ลบรายการ" ClientVisible='<%# Eval("SVISIBLE").ToString() == "N" ? false : true %>'
                                                            CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                            SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                            <ClientSideEvents Click="function (s, e) { txtIndxDelPublicFile.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {  dxPopupConfirm.Hide();  gvwBill.PerformCallback('DEL;'+ txtIndxDelPublicFile.GetValue() + ';' + s.name); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                            <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOCTYPE_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager Mode="ShowAllRecords" />
                                <Settings ShowColumnHeaders="False"></Settings>
                                <Border BorderStyle="None" BorderColor="White" BorderWidth="0px"></Border>
                                <Styles Cell-Border-BorderColor="White">
                                    <Cell>
                                        <Border BorderColor="White"></Border>
                                    </Cell>
                                </Styles>
                            </dx:ASPxGridView>
                            <dx:ASPxTextBox ID="txtIndxDelPublicFile" runat="server" ClientInstanceName="txtIndxDelPublicFile"
                                ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr id="trdoc3" runat="server">
                        <td colspan="4" style="padding: 0 0 0 0; border: 0px;">
                            <table id="tbOtherFile" runat="server" style="border-width: 0px; width: 100%; padding: 0 0 0 0;
                                border: 0px;" cellpadding="4" cellspacing="0">
                                <tr>
                                    <td colspan="1" style="padding: 0 0 0 5; border-right: solid 1px white" bgcolor="#B9EAEF"
                                        width="20%">
                                        เอกสารอื่นๆ
                                    </td>
                                    <td width="25%" valign="top" style="padding-top: 5px">
                                        <dx:ASPxUploadControl ID="ulcOtherDoc" ClientInstanceName="ulcOtherDoc" runat="server"
                                            CssClass="dxeLineBreakFix" Width="82%" NullText="Click here to browse files..."
                                            OnFileUploadComplete="ulcOtherDoc_FileUploadComplete">
                                            <ValidationSettings MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                            </ValidationSettings>
                                            <ClientSideEvents TextChanged="function(e,s){OnStartUpload(e,s)}" FileUploadComplete="
                                                             function(s,e){
                                                                 if(e.callbackData =='')
                                                                 {
                                                                    ulcOtherDoc.Upload(); xcpnGvwotherDoc.PerformCallback('UPLOAD;');
                                                                 }
                                                                 else
                                                                 {
                                                                    dxWarning('แจ้งเตือน', (e.callbackData+'').split('|')[0]);
                                                                 } 
                                                             }" />
                                        </dx:ASPxUploadControl>
                                    </td>
                                    <td width="5%">
                                        <dx:ASPxButton ID="btnAddOtherDoc" runat="server" ClientInstanceName="btnAddOtherDoc"
                                            Text="เพิ่ม" Width="20%" CssClass="dxeLineBreakFix" AutoPostBack="false" ClientVisible="false">
                                            <ClientSideEvents Click=" function(e,s){  var msg='';   if(ulcOtherDoc.GetText()==''){ msg+='<br>แนบไฟล์เอกสารอื่นๆ'; } if(msg != ''){dxWarning('แจ้งเตือน','กรุณา' + msg); return false;} else{ var filename = ulcOtherDoc.GetText(); var arrfilename =  filename.split('.'); if(arrfilename[arrfilename.length-1] == 'pdf' || arrfilename[arrfilename.length-1] == 'PDF') { ulcOtherDoc.Upload(); xcpnGvwotherDoc.PerformCallback('BIND;') } else { dxWarning('แจ้งเตือน','เฉพาะไฟล์ .pdf เท่านั้น');}   } }    " />
                                        </dx:ASPxButton>
                                    </td>
                                    <td width="60%" style="padding: 0 0 0 0;">
                                        <dx:ASPxCallbackPanel ID="xcpnGvwotherDoc" runat="server" ClientInstanceName="xcpnGvwotherDoc"
                                            OnCallback="xcpnGvwotherDoc_Callback">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="gvwOtherDoc" ClientInstanceName="gvwOtherDoc" runat="server"
                                                        SkinID="_gvw" Width="100%">
                                                        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                                                        <Columns>
                                                            <dx:GridViewDataColumn Width="75%" Caption="sFileName">
                                                                <DataItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <img src="images/ic_pdf2.gif" width="16" height="16" border="0" style="display: none;" />
                                                                                <dx:ASPxLabel ID="lblsNameShow" runat="server" Text='<%#Bind("FILE_NAME") %>'>
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                            <td>
                                                                                <img title="ดูเอกสาร" src="images/view1.png" width="25px" height="25px" border="0"
                                                                                    class='<%# (Eval("SVISIBLE")).ToString() == "N" ? "cHideControl" : "cShowControl" %>'
                                                                                    onclick=<%# "javascript:openFile('" + Eval("OPENFILE") + "')" %> style="cursor: pointer;">
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxButton ID="btnDelOtherDoc" ClientInstanceName="btnDelOtherDoc" runat="server"
                                                                                    ToolTip="ลบรายการ" ClientVisible='<%# (Eval("CONSIDER")).ToString() == "Y" ? false : true %>'
                                                                                    CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                    SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                    <ClientSideEvents Click="function (s, e) { txtIndxDelOterhFile.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {  dxPopupConfirm.Hide(); if(xcpnGvwotherDoc.InCallback()) return; else xcpnGvwotherDoc.PerformCallback('DEL;'+ txtIndxDelOterhFile.GetValue() + ';' + s.name); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                                </dx:ASPxButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Left">
                                                                </CellStyle>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn FieldName="DOC_ID" Visible="false">
                                                            </dx:GridViewDataColumn>
                                                        </Columns>
                                                        <SettingsPager Mode="ShowAllRecords" />
                                                        <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                                        <Styles>
                                                            <Table>
                                                                <Border BorderStyle="None" />
                                                            </Table>
                                                            <Cell>
                                                                <Border BorderWidth="0px" />
                                                            </Cell>
                                                        </Styles>
                                                        <Border BorderWidth="0px" />
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxTextBox ID="txtIndxDelOterhFile" runat="server" ClientInstanceName="txtIndxDelOterhFile"
                                                        ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxCallbackPanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <img src="images/i_newtopic.gif" width="16" height="16" alt="" />
                            บันทึกข้อมูลการดำเนินงาน
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" bgcolor="#B9EAEF">
                            <dx:ASPxCheckBox runat="server" ID="chkServiceDelivery">
                                <ClientSideEvents CheckedChanged="function(s,e){xcpnService.PerformCallback('service');}" />
                                <ClientSideEvents CheckedChanged="function(s,e){xcpnService.PerformCallback(&#39;service&#39;);}">
                                </ClientSideEvents>
                            </dx:ASPxCheckBox>
                            การบริการเร่งด่วน
                        </td>
                        <td colspan="3">
                            <dx:ASPxCallbackPanel ID="xcpnService" runat="server" HideContentOnCallback="False"
                                OnCallback="xcpnService_Callback" ClientInstanceName="xcpnService" CausesValidation="False">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <table width="100%">
                                            <tr>
                                                <td width="8%">
                                                    <dx:ASPxTextBox runat="server" ID="txtDeliveryCharge" CssClass="dxeLineBreakFix"
                                                        ClientEnabled="false" DisplayFormatString="#,#;-#,#;0">
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel runat="server" ID="lblUnit" Text="บาท" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
                        </td>
                    </tr>
                    <tr id="trChagedate" style="display: none;">
                        <td bgcolor="#B9EAEF">
                            ผู้ขอเลื่อนนัดหมาย<font color="red">*</font>
                        </td>
                        <td colspan="3">
                            <dx:ASPxRadioButtonList runat="server" ID="rblChangedate" RepeatDirection="Horizontal"
                                SkinID="rblStatus" ClientInstanceName="rblChangedate">
                                <Items>
                                    <dx:ListEditItem Text="ผู้ยื่นคำขอ" Value="N" />
                                    <dx:ListEditItem Text="เจ้าหน้าที่ มว." Value="M" />
                                </Items>
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic" RequiredField-IsRequired="true"
                                    RequiredField-ErrorText="เลือกผู้ขอเลื่อนนัดหมาย" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="เลือกผู้ขอเลื่อนนัดหมาย"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxRadioButtonList>
                        </td>
                    </tr>
                    <tr id="trChagedateCause" style="display: none;">
                        <td bgcolor="#B9EAEF">
                            สาเหตุที่เลื่อนวันนัดหมาย<font color="red">*</font>
                        </td>
                        <td colspan="3">
                            <dx:ASPxMemo runat="server" ID="MemoChageDate" Columns="80" Rows="6" AutoResizeWithContainer="false"
                                ClientInstanceName="MemoChageDate" MinValue="0" MaxValue="255">
                                <ClientSideEvents KeyUp="function(s, e) {return maxlength2(); }"></ClientSideEvents>
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic" RequiredField-IsRequired="true"
                                    RequiredField-ErrorText="ระบุสาเหตุที่เลื่อนวันนัดหมาย" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="ระบุสาเหตุที่เลื่อนวันนัดหมาย"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">
                            ชื่อผู้ยื่นคำขอ<font color="red">*</font>
                        </td>
                        <td colspan="3">
                            <dx:ASPxLabel runat="server" ID="txtName" Width="40%">
                                <%--  <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุชื่อผู้ยื่นคำขอ"
                                                RequiredField-IsRequired="true">
                                                <RequiredField IsRequired="True" ErrorText="ระบุชื่อผู้ยื่นคำขอ"></RequiredField>
                                            </ValidationSettings>--%>
                            </dx:ASPxLabel>
                            <dx:ASPxTextBox runat="server" ID="txtMVNAME" Width="40%">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุชื่อผู้ยื่นคำขอ"
                                    RequiredField-IsRequired="true">
                                    <RequiredField IsRequired="True" ErrorText="ระบุชื่อผู้ยื่นคำขอ"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">
                            เบอร์โทรศัพท์<font color="red">*</font>
                        </td>
                        <td colspan="3">
                            <dx:ASPxLabel runat="server" ID="txtPhone" Width="40%">
                                <%-- <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุเบอร์โทรศัพท์"
                                                RequiredField-IsRequired="true">
                                                <RequiredField IsRequired="True" ErrorText="ระบุเบอร์โทรศัพท์"></RequiredField>
                                            </ValidationSettings>--%>
                            </dx:ASPxLabel>
                            <dx:ASPxTextBox runat="server" ID="txtMVPHONE" Width="40%">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุเบอร์โทรศัพท์"
                                    RequiredField-IsRequired="true">
                                    <RequiredField IsRequired="True" ErrorText="ระบุเบอร์โทรศัพท์"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" bgcolor="#B9EAEF">
                            &nbsp;
                        </td>
                        <td colspan="3">
                            <dx:ASPxButton runat="server" ID="btnApprove" Text="บันทึก" CssClass="dxeLineBreakFix"
                                AutoPostBack="false" ValidationGroup="add">
                                <ClientSideEvents Click="function (s, e) 
                                            { 
                                                txtCallbackType.SetText('Approve');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false;  dxConfirm('คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่','คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่', function (s, e) { xcpn.PerformCallback(); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnEdit" Text="แจ้งปรับแก้คำขอ" CssClass="dxeLineBreakFix"
                                AutoPostBack="false">
                                <ClientSideEvents Click="function (s, e) 
                                            {
                                             txtCallbackType.SetText('Edit');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false; dxConfirm('คุณต้องการยืนยันการแจ้งปรับแก้คำขอใช่หรือไม่','คุณต้องการยืนยันการแจ้งปรับแก้คำขอใช่หรือไม่', function (s, e) { xcpn.PerformCallback('Edit'); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )    
                                            }"></ClientSideEvents>
                            </dx:ASPxButton>
                            <%--<dx:ASPxButton runat="server" ID="btnCancel" Text="ยกเลิกคำขอ" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) 
                                            {
                                             txtCallbackType.SetText('Cancel');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false; dxConfirm('คุณต้องการยืนยันการยกเลิกคำขอใช่หรือไม่','คุณต้องการยืนยันการยกเลิกคำขอใช่หรือไม่', function (s, e) { xcpn.PerformCallback('Cancel'); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>
                                        </dx:ASPxButton>--%>
                            <dx:ASPxButton runat="server" ID="btnBack" Text="ย้อนกลับ" CssClass="dxeLineBreakFix"
                                AutoPostBack="false">
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText('Back'); xcpn.PerformCallback();}" />
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText(&#39;Back&#39;); xcpn.PerformCallback();}">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" bgcolor="#B9EAEF">
                            หมายเหตุ แจ้งปรับแก้คำขอ
                        </td>
                        <td colspan="3">
                            <dx:ASPxMemo runat="server" ID="txtDescription" Columns="80" Rows="6" AutoResizeWithContainer="false"
                                ClientInstanceName="txtDescription" MinValue="0" MaxValue="255">
                                <ClientSideEvents KeyUp="function(s, e) {return maxlength(); }" />
                                <ClientSideEvents KeyUp="function(s, e) {return maxlength(); }"></ClientSideEvents>
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <img src="images/comment.png" width="16" height="16" alt="" />
                            หมายเหตุ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <dx:ASPxGridView runat="server" ID="gvwdescription" Width="100%" AutoGenerateColumns="false"
                                EnableViewState="false">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="วันที่-เวลา" FieldName="REMARK_DATE" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ผู้บันทึก" FieldName="SNAME" Width="29%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ขั้นตอน" FieldName="SDESCRIPTION" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="หมายเหตุ" FieldName="REMARKS" Width="39%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
