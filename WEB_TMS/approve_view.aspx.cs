﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;
using System.Globalization;
using System.Configuration;

public partial class approve_view : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        gvwdoc.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwdoc_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            string SUID = Session["UserID"] + "";
            string str = Request.QueryString["str"];
            string type = Request.QueryString["type"];

            string[] strType;
            if (!string.IsNullOrEmpty(type))
            {
                strType = STCrypt.DecryptURL(type);
                if (strType[0] == "O")
                {
                    btnCancel.Enabled = true;
                }
                else
                {
                    btnCancel.Enabled = false;
                }
            }

            string[] strQuery;
            string ReqID = "";
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                ReqID = strQuery[0];
                Session["ReqID"] = strQuery[0];
            }
            else
            {

            }

            Listdata(ReqID);
            //ListDescriptionData(ReqID);
        }
    }

    //แอด Javascript เพื่อใช้เปิดดูไฟล์
    void gvwdoc_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("คอนโทรนดูเอกสาร"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePath = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePath") as ASPxTextBox;
            ASPxTextBox txtFileName = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileName") as ASPxTextBox;
            ASPxButton btnView = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnView") as ASPxButton;

            txtFilePath.ClientInstanceName = txtFilePath.ID + "_" + VisibleIndex;
            txtFileName.ClientInstanceName = txtFileName.ID + "_" + VisibleIndex;
            btnView.ClientInstanceName = btnView.ID + "_" + VisibleIndex;

            //Add Event
            btnView.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePath.ClientInstanceName + ".GetValue() +" + txtFileName.ClientInstanceName + ".GetValue());}";
        }

        if (e.DataColumn.Caption.Equals("ListRdl"))
        {
            int VisibleIndex = e.VisibleIndex;


            ASPxRadioButtonList rblStatus = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "rblStatus") as ASPxRadioButtonList;
            string rblValue = !string.IsNullOrEmpty(gvwdoc.GetRowValues(VisibleIndex, "CONSIDER") + "") ? gvwdoc.GetRowValues(VisibleIndex, "CONSIDER") + "" : "";

            if (!string.IsNullOrEmpty(rblValue))
            {
                if (rblValue == "Y")
                {
                    rblStatus.SelectedIndex = 0;
                }
                else
                {
                    rblStatus.SelectedIndex = 1;
                }


            }

        }
    }

    //แอดสีFont
    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        //if (e.DataColumn.Caption == "ช่อง 1")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 2")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 3")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 4")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 5")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 6")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 7")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 8")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 9")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 10")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        string Req_ID = Session["ReqID"] + "";
        Listdata(Req_ID);
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string SUID = CommonFunction.ReplaceInjection(Session["UserID"] + "");
            string Req_ID = CommonFunction.ReplaceInjection(Session["ReqID"] + "");
            string Description = CommonFunction.ReplaceInjection(txtDescription.Text);
            string REMARK_STEP = CommonFunction.ReplaceInjection(txtSTATUSREQ_NAME.Text);

            string[] param = e.Parameter.Split(';');

            string UpdateRequest = @"UPDATE TBL_REQUEST
                                    SET    STATUS_FLAG = '{0}',
                                           APPROVE_DATE = sysdate
                                    WHERE  REQUEST_ID   = '{1}'";

            string InsertRemark = @"INSERT INTO TBL_REQREMARK (
                                       REQUEST_ID, REPORT2VEND, REMARKS, 
                                       REMARK_STEP, REMARK_STATUS, REMARK_ID, 
                                       REMARK_DATE, REMARK_BY) 
                                    VALUES ( '{0}',
                                     '{1}',
                                     '{2}',
                                     '{3}',
                                     '{4}',
                                     {5},
                                     sysdate,
                                    '{6}' )";

            AddDataCheckDocument(Req_ID);
            switch (param[0])
            {
                case "Approve":
                    AddTODB(string.Format(UpdateRequest, "03", Req_ID));
                    AddTODB(string.Format(InsertRemark, Req_ID, "N", Description, REMARK_STEP, "A", Gen_ID(), SUID));

                    break;
                case "Edit":

                    string strMail = @"SELECT SEMAIL FROM TUSER WHERE SUID = '" + SUID + "' ";
                    DataTable dtmail = CommonFunction.Get_Data(conn, strMail);
                    string Email = dtmail.Rows[0]["SEMAIL"] + "";

                    AddTODB(string.Format(UpdateRequest, "02", Req_ID));
                    AddTODB(string.Format(InsertRemark, Req_ID, "Y", Description, REMARK_STEP, "W", Gen_ID(), SUID));
                    //CommonFunction.SendMail();
                    break;
                case "Cancel":
                    AddTODB(string.Format(UpdateRequest, "11", Req_ID));
                    AddTODB(string.Format(InsertRemark, Req_ID, "N", "ยกเลิกคำขอ", REMARK_STEP, "C", Gen_ID(), SUID));
                    xcpn.JSProperties["cpRedirectTo"] = "approve.aspx";
                    break;
                case "Back":
                    xcpn.JSProperties["cpRedirectTo"] = "approve.aspx";
                    break;
            }
            Listdata(Req_ID);
            // ListDescriptionData(Req_ID);
        }
    }
    private void ListDoc(string Req_ID)
    {
        #region DATA#3 เอกสาร
        string CheckTypeDoc = "";
        //if (dt.Rows.Count > 0)
        //{
        //    //switch (dt.Rows[0]["REQTYPE_ID"] + "")
        //    //{
        //    //    case "01": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC01 = 'Y'";
        //    //        break;
        //    //    case "02": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC02 = 'Y'";
        //    //        break;
        //    //    case "03": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC03 = 'Y'";
        //    //        break;
        //    //    case "04": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC04 = 'Y'";
        //    //        break;
        //    //}
        //}


        string QueryDoc = @"SELECT TBL_REQDOC.REQUEST_ID, TBL_REQDOC.DOC_ID, TBL_REQDOC.DOC_TYPE, TBL_REQDOC.DOC_ITEM, TBL_REQDOC.FILE_NAME, TBL_REQDOC.FILE_SYSNAME, 
TBL_REQDOC.FILE_PATH, TBL_REQDOC.CONSIDER,TBL_DOCTYPE.DOC_DESCRIPTION,TBL_DOCTYPE.CATTACH_DOC01,TBL_DOCTYPE.CATTACH_DOC02,TBL_DOCTYPE.CATTACH_DOC03,TBL_DOCTYPE.CATTACH_DOC04,TBL_DOCTYPE.CDYNAMIC
FROM TBL_REQDOC
LEFT JOIN TBL_DOCTYPE
ON TBL_REQDOC.DOC_TYPE = TBL_DOCTYPE.DOCTYPE_ID
WHERE NVL(TBL_REQDOC.FILE_NAME,'xxx') <> 'xxx' AND NVL(TBL_REQDOC.FILE_SYSNAME,'xxx') <> 'xxx' AND 
NVL(TBL_REQDOC.FILE_PATH,'xxx') <> 'xxx' AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'  " + CheckTypeDoc + @"
ORDER BY  TBL_DOCTYPE.CDYNAMIC DESC,TBL_REQDOC.DOC_TYPE ASC ,TBL_REQDOC.DOC_ID ASC";

        DataTable dtDoc = CommonFunction.Get_Data(conn, QueryDoc);
        if (dtDoc.Rows.Count > 0)
        {
            gvwdoc.ClientVisible = true;
            gvwdoc.DataSource = dtDoc;
            gvwdoc.DataBind();

            txtDYNAMIC.Text = dtDoc.Select("CDYNAMIC = 'Y'").Count() + "";
            txtDYNAMICALL.Text = dtDoc.Select("CDYNAMIC = 'Y' AND CONSIDER = 'Y'").Count() + "";

            if (txtDYNAMIC.Text == txtDYNAMICALL.Text)
            {
                btnApprove.ClientEnabled = true;
            }
            else
            {
                btnApprove.ClientEnabled = false;
            }
        }
        else
        {
            gvwdoc.ClientVisible = false;
        }

        #endregion
    }
    private void Listdata(string Req_ID)
    {
        #region DATA#1 ข้อมูลพิ้นฐาน และ รายละเอียดค่าธรรมเนียม

        string QueryReq = @"SELECT   TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
                        ,TREQ.Status_Flag,TRUNC(TREQ.WATER_EXPIRE_DATE) as DWATEREXPIRE ,TRUNC(TREQ.APPOINTMENT_DATE) as APPOINTMENT_DATE,TVEN.SABBREVIATION
                        ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_NAME  ,TCAR.CARCATE_NAME
                        ,serPRICE.NPRICE   as SERVCHAGE_PRICE
                        ,addPRICE.NPRICE  as ADDITIONAL_PRICE
                        ,TREQ.TOTLE_SERVCHAGE,TSTATUS.STATUSREQ_ID,TREQCAUSE.CAUSE_NAME,TREQTYPE.REQTYPE_ID,TREQ.CONTACTNAME,TREQ.CONTACTPHONE,TREQ.CARCATE_ID,TREQ.RK_FLAG
                        FROM TBL_Request TREQ
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR 
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_CAUSE TREQCAUSE
                       ON TREQCAUSE.CAUSE_ID = TREQ.CAUSE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        LEFT JOIN TBL_CARCATE TCAR
                        ON TCAR.CARCATE_ID = TREQ.CARCATE_ID
                       LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID = '00002') addPRICE
                       ON addPRICE.REQUEST_ID = TREQ.REQUEST_ID
                          LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID <> '00002') serPRICE
                        ON serPRICE.REQUEST_ID = TREQ.REQUEST_ID
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,
                            CASE  SCARTYPEID
                            WHEN 0 THEN SHEADREGISTERNO
                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
                            ELSE ''
                            END REGISTERNO
                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                            FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID
                        WHERE 1=1 AND TREQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'";
        DataTable dt = CommonFunction.Get_Data(conn, QueryReq);
        if (dt.Rows.Count > 0)
        {
            //sREQTYPE_ID = dt.Rows[0]["REQTYPE_ID"] + "";
            //sStatusID = dt.Rows[0]["STATUS_FLAG"] + "";
            //CARCATE_ID = dt.Rows[0]["CARCATE_ID"] + "";
            //RK_FLAG = dt.Rows[0]["RK_FLAG"] + "";
            //VENDOR_ID = dt.Rows[0]["VENDOR_ID"] + "";
            //if (RK_FLAG == "Y")
            //{
            //    ListDoc(sReq_ID, "Y");
            //    ListDocOther(sReq_ID, "Y");
            //    trdocshow2.Visible = false;
            //    trdoc2.Visible = true;
            //    trdoc3.Visible = true;
            //    txtRk_flag.Text = RK_FLAG;
            //    btnEdit.ClientVisible = false;
            //}
            //else
            //{
            //    trdocshow2.Visible = true;
            //    trdoc2.Visible = false;
            //    trdoc3.Visible = false;

            //    btnEdit.ClientVisible = true;
            //}


            lbldatereq.Text = ChecknullDate(dt.Rows[0]["REQUEST_DATE"] + "");
            lblDateexp.Text = ChecknullDate(dt.Rows[0]["DWATEREXPIRE"] + "");
            lblCause.Text = Checknull(dt.Rows[0]["CAUSE_NAME"] + "");
            lblReq.Text = Checknull(dt.Rows[0]["REQTYPE_NAME"] + "");
            lblCar.Text = Checknull(dt.Rows[0]["CARCATE_NAME"] + "");
            lblRegis.Text = dt.Rows[0]["VEH_NO"] + "" + (!string.IsNullOrEmpty(dt.Rows[0]["TU_NO"] + "") ? " / " + dt.Rows[0]["TU_NO"] + "" : "");
            lblVendorname.Text = Checknull(dt.Rows[0]["SABBREVIATION"] + "");
            txtName.Text = Checknull(dt.Rows[0]["CONTACTNAME"] + "");
            txtPhone.Text = Checknull(dt.Rows[0]["CONTACTPHONE"] + "");
            //lblService.Text = CheckNum(dt.Rows[0]["SERVCHAGE_PRICE"] + "");
            //lblAddition.Text = CheckNum(dt.Rows[0]["ADDITIONAL_PRICE"] + "");
            //lblTotal.Text = CheckNum(dt.Rows[0]["TOTLE_SERVCHAGE"] + "");

            lblDuedate.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            txtCalendarDate.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");

            txtUserSelectDate.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            //เซ็ทเพื่อนำไปใช้เก็บค่าลง TBL_REMARk
            txtSTATUSREQ_NAME.Text = (dt.Rows[0]["STATUSREQ_ID"] + "");
        }
        #endregion

        #region DATA#2 ข้อมูลความจุรายการเพิ่มแป้น

        DataTable _dtCompacity = new DataTable();

        _dtCompacity = SystemFunction.LISTCAPACITY(Req_ID);
        if (_dtCompacity.Rows.Count > 0)
        {
            gvw.DataSource = _dtCompacity;
            gvw.DataBind();

            #region เช็คว่ามีแป้นเท่าไหร่
            string _chkPan3 = @"SELECT MAX(R.LEVEL_NO)
FROM TBL_REQSLOT r  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"' AND NVL(STATUS_PAN3,'xxx') <> '0'";
            DataTable _dtChkPan3 = new DataTable();

            _dtChkPan3 = CommonFunction.Get_Data(conn, _chkPan3);
            if (_dtChkPan3.Rows.Count > 0)
            {
                string _nPan = _dtChkPan3.Rows[0][0].ToString();
                if (_nPan != "3")
                {
                    //ถ้าไม่มีแป้นสามให้แสดง 2 แป้น
                    gvw.SettingsPager.PageSize = 2;
                }
            }
            #endregion

        }




        #endregion

        #region DATA#3 เอกสาร
        //        string CheckTypeDoc = "";
        //        if (dt.Rows.Count > 0)
        //        {
        //            //switch (dt.Rows[0]["REQTYPE_ID"] + "")
        //            //{
        //            //    case "01": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC01 = 'Y'";
        //            //        break;
        //            //    case "02": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC02 = 'Y'";
        //            //        break;
        //            //    case "03": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC03 = 'Y'";
        //            //        break;
        //            //    case "04": CheckTypeDoc = " AND TBL_DOCTYPE.CATTACH_DOC04 = 'Y'";
        //            //        break;
        //            //}
        //        }


        //        string QueryDoc = @"SELECT TBL_REQDOC.REQUEST_ID, TBL_REQDOC.DOC_ID, TBL_REQDOC.DOC_TYPE, TBL_REQDOC.DOC_ITEM, TBL_REQDOC.FILE_NAME, TBL_REQDOC.FILE_SYSNAME, 
        //TBL_REQDOC.FILE_PATH, TBL_REQDOC.CONSIDER,TBL_DOCTYPE.DOC_DESCRIPTION,TBL_DOCTYPE.CATTACH_DOC01,TBL_DOCTYPE.CATTACH_DOC02,TBL_DOCTYPE.CATTACH_DOC03,TBL_DOCTYPE.CATTACH_DOC04,TBL_DOCTYPE.CDYNAMIC
        //FROM TBL_REQDOC
        //LEFT JOIN TBL_DOCTYPE
        //ON TBL_REQDOC.DOC_TYPE = TBL_DOCTYPE.DOCTYPE_ID
        //WHERE NVL(TBL_REQDOC.FILE_NAME,'xxx') <> 'xxx' AND NVL(TBL_REQDOC.FILE_SYSNAME,'xxx') <> 'xxx' AND 
        //NVL(TBL_REQDOC.FILE_PATH,'xxx') <> 'xxx' AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'  " + CheckTypeDoc + @"
        //ORDER BY  TBL_DOCTYPE.CDYNAMIC DESC,TBL_REQDOC.DOC_TYPE ASC ,TBL_REQDOC.DOC_ID ASC";

        //        DataTable dtDoc = CommonFunction.Get_Data(conn, QueryDoc);
        //        if (dtDoc.Rows.Count > 0)
        //        {
        //            gvwdoc.ClientVisible = true;
        //            gvwdoc.DataSource = dtDoc;
        //            gvwdoc.DataBind();
        //        }
        //        else
        //        {
        //            gvwdoc.ClientVisible = false;
        //        }

        ListDoc(Req_ID);

        #endregion

        #region DATA#4 หมายเหตุ

        ListDescriptionData(Req_ID, Session["UserID"] + "");
        #endregion

        #region DATA#5 ค่าบริการ

        DataTable dtService = SystemFunction.List_SERVICE(Req_ID, "");
        if (dtService.Rows.Count > 0)
        {
            gvwService.DataSource = dtService;
        }
        gvwService.DataBind();

        #endregion
    }

    private void ListDescriptionData(string Req_ID, string USERID)
    {
        //        string sql = @"SELECT TBL_REQREMARK.REMARK_ID, TBL_REQREMARK.REQUEST_ID, TBL_REQREMARK.REMARK_DATE, 
        //                       TBL_REQREMARK.REMARK_STEP, TBL_REQREMARK.REMARKS, TBL_REQREMARK.REMARK_STATUS, 
        //                       TBL_REQREMARK.REMARK_BY, TBL_REQREMARK.REPORT2VEND,TBL_STATUSREQ.STATUSREQ_NAME,TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME as sName
        //                    FROM TBL_REQREMARK
        //                    LEFT JOIN TBL_STATUSREQ
        //                    ON TBL_STATUSREQ.STATUSREQ_ID = TBL_REQREMARK.REMARK_STEP
        //                    LEFT JOIN TUSER
        //                    ON TUSER.SUID = TBL_REQREMARK.REMARK_BY
        //                    WHERE TBL_REQREMARK.REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"'
        //                    ORDER BY TBL_REQREMARK.REMARK_DATE DESC";

        DataTable dt = SystemFunction.List_TBL_REQREMARK(Req_ID, "");
        if (dt.Rows.Count > 0)
        {
            //gvwdescription.Visible = true;
            gvwdescription.DataSource = dt;
            gvwdescription.DataBind();
            txtDescription.Text = dt.Rows[0]["REMARKORIGINAL"] + "";
            txtUserDescription.Text = dt.Rows[0]["USERDESCRIPTION"] + "";
        }
        else
        {
            //gvwdescription.Visible = false;
        }

    }

    private string Checknull(string Data)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Data))
        {
            Result = Data;
        }
        else
        {
            Result = " - ";
        }


        return Result;
    }

    private string ChecknullDate(string Data)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Data))
        {
            Result = DateTime.Parse(Data).ToString(ConfigurationSettings.AppSettings["FormatDate"]);
        }
        else
        {
            Result = " - ";
        }


        return Result;
    }

    private string CheckNum(string Data)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(Data))
        {
            if (Data != "0")
            {
                decimal num = decimal.Parse(Data);
                Result = num.ToString("#,###,###,###,###,###") + " บาท";
            }
            else
            {
                Result = "0 บาท";
            }
        }
        else
        {
            Result = " - ";
        }

        return Result;
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    private string Gen_ID()
    {
        string Result = "";

        string strsql = @"SELECT REMARK_ID, REQUEST_ID, REMARK_DATE, 
   REMARK_STEP, REMARKS, REMARK_STATUS, 
   REMARK_BY, REPORT2VEND
FROM TBL_REQREMARK ORDER BY REMARK_ID DESC";

        DataTable dt = CommonFunction.Get_Data(conn, strsql);


        if (dt.Rows.Count > 0)
        {

            string sID = dt.Rows[0]["REMARK_ID"] + "";
            int nID = int.Parse(sID) + 1;

            string NewID = nID.ToString();


            Result = NewID;
        }
        else
        {
            Result = "1";
        }

        return Result;
    }

    private void AddDataCheckDocument(string Req_ID)
    {
//        int gvwcount = gvwdoc.VisibleRowCount;

//        string UpdateCheckFile = @"UPDATE TBL_REQDOC
//                                SET    
//                                       CONSIDER     = '{2}'
//                                WHERE  REQUEST_ID   = '{0}'
//                                AND    DOC_ID       = {1}";
//        for (int i = 0; i < gvwcount; i++)
//        {
//            string DOC_ID = CommonFunction.ReplaceInjection(gvwdoc.GetRowValues(i, "DOC_ID") + "");
//            ASPxRadioButtonList rblStatus = gvwdoc.FindRowCellTemplateControl(i, null, "rblStatus") as ASPxRadioButtonList;
//            string rblValue = rblStatus.Value + "";

//            AddTODB(string.Format(UpdateCheckFile, CommonFunction.ReplaceInjection(Req_ID), DOC_ID, rblValue));
//        }
    }
}