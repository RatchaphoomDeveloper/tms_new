﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ReportFeedUtilization.aspx.cs" Inherits="ReportFeedUtilization" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
    <script type="text/javascript">
        window.__OriginalDXUpdateRowCellsHandler = ASPxClientTableFixedColumnsHelper.prototype.ChangeCellsVisibility;
        ASPxClientTableFixedColumnsHelper.prototype.ChangeCellsVisibility = function (row, startIndex, endIndex, display) {
            if ((row.cells.length == 0) || (row.cells[0].getAttribute("ci") == null))
                window.__OriginalDXUpdateRowCellsHandler(row, startIndex, endIndex - 1, display); // base call
            else {
                //custom processing
                for (var i = startIndex; i <= endIndex; i++) {
                    var cell = FindCellWithColumnIndex(row, i);
                    if (cell != null)
                        cell.style.display = display;
                }
            }
        };

        function FindCellWithColumnIndex(row, colIndex) {
            for (var i = 0; i < row.cells.length; i++) {
                if (row.cells[i].getAttribute("ci") == colIndex)
                    return row.cells[i];
            }
            return null;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                 <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                <label  class="col-md-1 control-label">กลุ่มที่</label>
                <div class="col-md-5">
                    <asp:DropDownList runat="server" ID="ddlGroup" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                    </asp:DropDownList>
                </div>
                <label  class="col-md-1 control-label">กลุ่มงาน</label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="txtWorkGroup" ReadOnly="true" CssClass="form-control" />
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-1 control-label">วันที่</label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="txtDateStart" CssClass="datepicker" />
                </div>
                <label  class="col-md-1 control-label">ถึง</label>
                <div class="col-md-5 ">
                    <asp:TextBox runat="server" ID="txtDateEnd" CssClass="datepicker" />
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    <center>
                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" /></center>
                </div>
                
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    <dx:ASPxGridView ID="gvReport" runat="server" AutoGenerateColumns="false" Width="100%"  SettingsPager-Mode="ShowAllRecords" OnAfterPerformCallback="gvReport_AfterPerformCallback">
                        <Columns>
                            
                            <dx:GridViewDataTextColumn FieldName="DDELIVERY" Caption="วันที่" ReadOnly="True" GroupIndex="1"  >
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                <Settings AllowGroup="True" GroupInterval="Value" SortMode="Value"/>
                                        </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataTextColumn  FieldName="NAME" Caption="กลุ่มที่" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               <Settings />
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="WORKGROUPNAME"  Caption="กลุ่มงาน" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataTextColumn  FieldName="SCONTRACTNO"  Caption="เลขที่สัญญา" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="SABBREVIATION"  Caption="ผู้ขนส่ง" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <%--<dx:GridViewBandColumn Caption="จำนวนรถ" HeaderStyle-HorizontalAlign="Center"  >
                                <Columns>
                                    
                                    <dx:GridViewDataTextColumn  FieldName="NTRUCKRESERVE"  Caption="สำรอง" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn  FieldName="NTRUCKSUM"  Caption="รวม" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>--%>
                            <dx:GridViewDataTextColumn  FieldName="NTRUCKSUM"  Caption="ในสัญญา" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="NTRANSIT"  Caption="อยู่ระหว่าง<br/>การขนส่ง" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="NAVAILABLE"  Caption="รถพร้อม<br/>ปฏิบัติงาน" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="COUNTDO"  Caption="รถเข้ารับ<br/>งานขนส่ง" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="PERCENTNTRUCKSUMCONFIRM"  Caption="<center>% การใช้รถ<br/>(เทียบสัญญา)</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="PERCENTNTRUCKSUM"  Caption="<center>% การใช้รถ<br/>(เทียบรถพร้อม)</center>" ReadOnly="True"  PropertiesTextEdit-EncodeHtml="false">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS1"  Caption="<center>รับงาน<br/>1 เที่ยว</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS2"  Caption="<center>รับงาน<br/>2 เที่ยว</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS3"  Caption="<center>รับงาน<br/>3 เที่ยว</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS4"  Caption="<center>รับงาน<br/>4 เที่ยว</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="UNNUMFLIGHTS"  Caption="<center>รถที่ไม่ได้<br/>รับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="รวมตามกลุ่มงาน">
                                <Columns>
                            
                            
                            <dx:GridViewDataTextColumn  FieldName="SUMGROUPNTRUCKSUM"  Caption="<center>ในสัญญา</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn  FieldName="SUMGROUPNTRANSIT"  Caption="<center>อยู่ระหว่าง<br/>ขนส่ง</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="SUMGROUPNTRUCKSUMCONFIRM"  Caption="<center>รถพร้อม<br/>ปฏิบัติงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="SUMGROUPCOUNTDO"  Caption="<center>รถเข้ารับ<br/>งานขนส่ง</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn  FieldName="PERCENT_SUMCONFIRM"  Caption="<center>% การใช้รถ<br/>(ในสัญญา)</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="PERCENT_SUMGROUPCOUNTDO"  Caption="<center>% การใช้รถ<br/>(ปฏิบัติงาน)</center>" ReadOnly="True"  PropertiesTextEdit-EncodeHtml="false">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                        </Columns>
                        <Paddings />
                    </dx:ASPxGridView>
                    <%--<asp:GridView runat="server" ID="gvReport" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader" DataKeyNames="SVENDORID">
                        <Columns>
                            <asp:BoundField DataField="SVENDORID" Visible="false" />
                            <asp:BoundField DataField="NAME" HeaderText="กลุ่มที่"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="WORKGROUPNAME" HeaderText="กลุ่มงาน"  HeaderStyle-HorizontalAlign="Center"/>
                            <asp:BoundField DataField="DDELIVERY" HeaderText="วันที่" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="SABBREVIATION" HeaderText="<center>ผู้ขนส่ง</center>" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HtmlEncode="false" />
                            <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                           
                        </Columns>
                    </asp:GridView>--%>
                </div>
            </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $(document).ready(function () {

            updatedate();

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            updatedate();
        }
        function updatedate() {

            $("#<%=txtDateStart.ClientID %>").on('change', function (e) {
                $("#<%=txtDateEnd.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate($(this).val());
                var date = new Date($(this).val().split('/')[2], $(this).val().split('/')[1] - 1, $(this).val().split('/')[0]);
                date.setDate(date.getDate() + 6);
                //alert(date);
                $("#<%=txtDateEnd.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate(date);
            });
            $("#<%=txtDateEnd.ClientID %>").on('change', function (e) {
                $("#<%=txtDateStart.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate($(this).val());
                var date = new Date($(this).val().split('/')[2], $(this).val().split('/')[1] - 1, $(this).val().split('/')[0]);
                date.setDate(date.getDate() - 6);
                //alert(date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear());
                //alert(date);
                $("#<%=txtDateStart.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate(date);
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

