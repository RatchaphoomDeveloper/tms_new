﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity.CarEntity;
using TMS_BLL.Transaction.Report;
using EmailHelper;
using System.Web.Security;
using System.Web.Configuration;

public partial class truck_edit : PageBase
{
    const string UploadDirectory = "UploadFile/Truck/";
    SapTU SapTU = new SapTU();
    //เก็บหมายเลขทะเบียนส่งTques

    #region + ViewState +
    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    //เก็บหมายเลขทะเบียนส่งTques
    private string registerno
    {
        get
        {
            if ((string)ViewState["registerno"] != null)
                return (string)ViewState["registerno"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["registerno"] = value;
        }
    }
    private string mode
    {
        get
        {
            if ((string)ViewState["mode"] != null)
                return (string)ViewState["mode"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["mode"] = value;
        }
    }
    //เก็บทะเบียนรถอย่างเดียว
    private string regisText
    {
        get
        {
            if ((string)ViewState["regisText"] != null)
                return (string)ViewState["regisText"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["regisText"] = value;
        }
    }
    private string SVDID
    {
        get
        {
            if ((string)ViewState["SVDID"] != null)
                return (string)ViewState["SVDID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SVDID"] = value;
        }
    }
    private string NamveVendor
    {
        get
        {
            if ((string)ViewState["NamveVendor"] != null)
                return (string)ViewState["NamveVendor"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["NamveVendor"] = value;
        }
    }
    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtUploadTypeTPN
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTPN"] != null)
                return (DataTable)ViewState["dtUploadTypeTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTPN"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtUploadTPN
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTPN"] != null)
                return (DataTable)ViewState["dtUploadTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTPN"] = value;
        }
    }

    private DataTable dtCheckCar
    {
        get
        {
            if ((DataTable)ViewState["dtCheckCar"] != null)
                return (DataTable)ViewState["dtCheckCar"];
            else
                return null;
        }
        set
        {
            ViewState["dtCheckCar"] = value;
        }
    }

    private string V_CheckCar
    {
        get
        {
            if ((string)ViewState["V_CheckCar"] != null)
                return (string)ViewState["V_CheckCar"];
            else
                return null;
        }
        set
        {
            ViewState["V_CheckCar"] = value;
        }
    }

    private DataTable dtRequestFileTPN
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFileTPN"] != null)
                return (DataTable)ViewState["dtRequestFileTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFileTPN"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }
    private DataTable Valuecapacity
    {
        get
        {
            if ((DataTable)ViewState["Valuecapacity"] != null)
                return (DataTable)ViewState["Valuecapacity"];
            else
                return null;
        }
        set
        {
            ViewState["Valuecapacity"] = value;
        }
    }
    private string V_TRUCKID
    {
        get
        {
            if ((string)ViewState["V_TRUCKID"] != null)
                return (string)ViewState["V_TRUCKID"];
            else
                return null;
        }
        set
        {
            ViewState["V_TRUCKID"] = value;
        }
    }
    private string VendorID
    {
        get
        {
            if ((string)ViewState["VendorID"] != null)
                return (string)ViewState["VendorID"];
            else
                return null;
        }
        set
        {
            ViewState["VendorID"] = value;
        }

    }
    private string LOGDATA
    {
        get
        {
            if ((string)ViewState["LOGDATA"] != null)
                return (string)ViewState["LOGDATA"];
            else
                return null;
        }
        set
        {
            ViewState["LOGDATA"] = value;
        }
    }
    public bool isUse = true;
    //private string cActive = "Y";

    private DataTable NewDataCapacity
    {
        get
        {
            if ((DataTable)ViewState["NewDataCapacity"] != null)
                return (DataTable)ViewState["NewDataCapacity"];
            else
                return null;
        }
        set
        {
            ViewState["NewDataCapacity"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTRANConnectionString"].ConnectionString;
        this.Culture = "en-US";
        this.UICulture = "en-US";
        CultureInfo ThaiCulture = new CultureInfo("th-TH");
        Response.Cache.SetNoStore();
        //Get User expire
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
        }
        else
        {
            CGROUP = Session["CGROUP"].ToString();
            SVDID = Session["SVDID"].ToString();
        }
        cmbCarcate.Enabled = false;
        if (!IsPostBack)
        {
            #region Not Postback
            DataTable dtWarning = MonthlyReportBLL.Instance.TruckSelectSpecialValueBLL();
            lblWarning.Text = "หมายเหตุ ในกรณีที่ยังไม่ได้รับ ธพ.น.2 สามารถแนบ ธพ.น.3 (แบบคําขอต่ออายุใบอนุญาตประกอบกิจการควบคุมประเภทที่ ๓) ทดแทนก่อนได้ และขอความอนุเคราะห์แนบ เอกสาร ธพ.น.2 ปี " + DateTime.Now.ToString("yyyy", ThaiCulture) + " ก่อนวันที " + dtWarning.Rows[0]["SPECIALDATE"];
            if (Request.UrlReferrer != null)
            {
                ViewState["GoBackTo"] = Request.UrlReferrer.AbsoluteUri;
            }
            else
            {
                ViewState["GoBackTo"] = "~/truck_edit.aspx";
            }
            string str = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                str = decryptedValue;

                decryptedBytes = MachineKey.Decode(Request.QueryString["STTRUCKID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                V_TRUCKID = decryptedValue;

                decryptedBytes = MachineKey.Decode(Request.QueryString["CarTypeID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                Session["SCARTYPEID"] = decryptedValue;

                mode = Request.QueryString["mode"];

                DataTable TableDataHeader = CarBLL.Instance.LoadDataHeader(V_TRUCKID);
                DataTable TableDataInsurance = CarBLL.Instance.LoadDataInsurance(V_TRUCKID);
                DataTable TableDataCompart = CarBLL.Instance.LoadDataCompart(V_TRUCKID);

                //Get Doc MV
                DataTable dtMVDoc = CarBLL.Instance.GetDocMVBLL(V_TRUCKID);
                GridViewHelper.BindGridView(ref dgvMV, dtMVDoc);

                #region Set Gaobla Varaiable
                LOGDATA = TableDataHeader.Rows[0]["LOGDATA"].ToString();
                cActive.Value = TableDataHeader.Rows[0].Field<string>("CACTIVE");
                isUse = (Convert.ToInt32(TableDataHeader.Rows[0]["ISUSE"]) == 1);
                #endregion
                Valuecapacity = TableDataCompart;
                this.InitialForm(CGROUP, TableDataHeader);
                InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");
                this.bindDataForm(V_TRUCKID, TableDataHeader, TableDataInsurance, TableDataCompart);
                //ข้อมูล Max Colomns
                if (TableDataHeader.Rows[0]["SCARTYPEID"].ToString() != "3")
                {
                    if (!string.Equals(TableDataCompart.Rows.Count.ToString(), "0"))
                    {
                        DataTable MaxValue = TableDataCompart.Select("NPANLEVEL > 0", "NPANLEVEL DESC").CopyToDataTable();
                        int MaxColumn = int.Parse(MaxValue.Rows[0]["NPANLEVEL"].ToString());
                        capacityCell.SelectedValue = MaxColumn.ToString();
                        table_capacity.Rows.Clear();
                        CreateHeadCapacity(int.Parse(capacityCell.SelectedValue));
                        GenRowCapacity(int.Parse(ddl_addition.SelectedValue.ToString()), int.Parse(capacityCell.SelectedValue));
                        GetDataCapacity(Valuecapacity);
                    }
                    else
                    {
                        table_capacity.Rows.Clear();
                        CreateHeadCapacity(1);
                        this.GenRowCapacity(1, 1);
                    }
                }
            }

            dtRequestFile = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'TRUCK' AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 0");
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            if (cmbCarcate.SelectedValue == "0")
            {
                dtRequestFileTPN = MonthlyReportBLL.Instance.TruckTPNRequestFileBLL();
                this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);
            }
            else if (cmbCarcate.SelectedValue == "3")
            {
                dtRequestFileTPN = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'TRUCK' AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 1 AND M_UPLOAD_TYPE.UPLOAD_ID <> 344");
                this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);
            }
            else if (cmbCarcate.SelectedValue == "4")
            {
                dtRequestFileTPN = MonthlyReportBLL.Instance.TruckTPNRequestFileBLL();
                this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);
            }
            #endregion

            this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
            this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);

            if (!string.IsNullOrEmpty(mode) && mode == "view")
            {
                pnlcartype.Enabled = false;
                pnltruck.Enabled = false;
                pnlsemitruck.Enabled = false;
                pnlowncar.Enabled = false;
                pnldocument.Enabled = false;
                pnldocumentTPN.Enabled = false;
                pnlinsurance.Enabled = false;
                pnlcarrier.Enabled = false;
                pnlscalewater.Enabled = false;
                pnlcapacity.Enabled = false;
                pnlbutton.Enabled = false;
            }
            //this.AssignAuthen();

            if (Session["AutoApprove"] != null)
            {
                if ((bool)Session["AutoApprove"])
                {
                    approve_Click(null, null);

                    Session["AutoApprove"] = false;
                }
            }
        }
        else
        {
            table_capacity.Rows.Clear();
            CreateHeadCapacity(int.Parse(capacityCell.SelectedValue));
            this.GenRowCapacity(int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));
        }

        /*******************/
        //Check รถอนุมัติ
        if (cActive.Value == "Y")
        {
            //Check รถในสัญญา
            if (ContractBLL.Instance.CheckTruckInContract(V_TRUCKID))
            {
                SetPermissionControl(true, true);
            }
            else
            {
                SetPermissionControl(true, false);
            }
        }
        else
        {
            SetPermissionControl(false, false);
        }

        V_CheckCar = @"select * from TCONTRACT_TRUCK INNER JOIN TCONTRACT ON TCONTRACT_TRUCK.SCONTRACTID = TCONTRACT.SCONTRACTID where (STRUCKID = '{0}' or STRAILERID = '{1}') AND TCONTRACT.CACTIVE = 'Y'";
        dtCheckCar = CommonFunction.Get_Data(strConn, string.Format(V_CheckCar, CommonFunction.ReplaceInjection(V_TRUCKID), CommonFunction.ReplaceInjection(V_TRUCKID)));
        if (dtCheckCar.Rows.Count > 0)
        {
            txtRnShaftDriven.Enabled = false;
            cboHLoadMethod.Enabled = false;
            ddl_addition.Enabled = false;
            capacityCell.Enabled = false;
            table_capacity.Enabled = false;
        }
        else
        {
            txtRnShaftDriven.Enabled = true;
            cboHLoadMethod.Enabled = true;
            ddl_addition.Enabled = true;
            capacityCell.Enabled = true;
            table_capacity.Enabled = true;
        }

    }

    private void CheckUploadFileMV(int TruckType, DataTable dtUpload)
    {
        try
        {
            lblPlaceMV.Visible = true;
            radPlaceMV.Visible = true;

            if (radSpot.SelectedIndex == 0)
            {
                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    if (string.Equals(dtUpload.Rows[i]["UPLOAD_ID"].ToString(), "41"))
                    {
                        dtUpload.Rows.RemoveAt(i);

                        radPlaceMV.ClearSelection();
                        lblPlaceMV.Visible = false;
                        radPlaceMV.Visible = false;

                        break;
                    }
                }
            }
            else if (radSpot.SelectedIndex == 1)
            {
                if (TruckType == 2)                     //หางลาก
                {
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        if (string.Equals(dtUpload.Rows[i]["UPLOAD_ID"].ToString(), "41"))
                        {
                            dtUpload.Rows.RemoveAt(i);

                            radPlaceMV.ClearSelection();
                            lblPlaceMV.Visible = false;
                            radPlaceMV.Visible = false;

                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                xbnLastHis.Enabled = false;
            }
            if (!CanWrite)
            {
                btnUpload.Enabled = false;
                btnCreateRow.Enabled = false;
                btnSave.Enabled = false;
                btncomment.Enabled = false;
                btnapprove.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region bindDataForm
    private void bindDataForm(string TTRUCKID, DataTable TableDataHeader, DataTable TableDataInsurance, DataTable TableDataCompart)
    {
        string TypeId = TableDataHeader.Rows[0]["SCARTYPEID"].ToString();
        //ส่งทะเบียนรถ
        registerno = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString();
        regisText = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString();
        //Add ชนิดรถส่งไป TREAQ DATABASE
        registerno += LangeTypeCar(int.Parse(TypeId));
        ComplainImportFileBLL complainImportFileBLL = new ComplainImportFileBLL();
        DataTable Blacklist_ds;
        int tmp;
        //ข้อมูลวัดน้ำ
        placewater.SelectedValue = TableDataHeader.Rows[0]["PLACE_WATER_MEASURE"].ToString();

        radPlaceMV.SelectedValue = TableDataHeader.Rows[0]["PLACE_WATER_MEASURE"].ToString();

        txtcodewater.Text = string.Equals(TableDataHeader.Rows[0]["SCAR_NUM"].ToString(), "") ? TableDataHeader.Rows[0]["SLAST_REQ_ID"].ToString() : TableDataHeader.Rows[0]["SCAR_NUM"].ToString();
        //if (!string.Equals(txtcodewater.Text, ""))
        //{
        scale_start.Text = string.Equals(TableDataHeader.Rows[0]["DPREV_SERV"].ToString(), string.Empty) ? "" : Convert.ToDateTime(TableDataHeader.Rows[0]["DPREV_SERV"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        scale_end.Text = string.Equals(TableDataHeader.Rows[0]["DWATEREXPIRE"].ToString(), string.Empty) ? "" : Convert.ToDateTime(TableDataHeader.Rows[0]["DWATEREXPIRE"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        //}
        txtcomment.Text = TableDataHeader.Rows[0]["COMMENTDATA"].ToString();
        switch (TypeId)
        {
            case "0":
                Truck.Visible = true;
                insurance.Visible = true;
                carrier.Visible = true;
                capacity.Visible = true;
                semitruck.Visible = false;
                document.Visible = true;
                companyTank.Visible = true;
                documentTPN.Visible = true;
                //bindValue
                cmbCarcate.SelectedValue = TypeId;
                txtHRegNo.Text = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString().Trim();
                first_regis.Text = string.Equals(TableDataHeader.Rows[0]["DREGISTER"].ToString(), "") ? "" : Convert.ToDateTime(TableDataHeader.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                cmbHsHolder.Text = TableDataHeader.Rows[0]["SOWNER_NAME"].ToString();
                cmbHRoute.SelectedValue = TableDataHeader.Rows[0]["ROUTE"].ToString();
                cmbHTru_Cate.SelectedValue = TableDataHeader.Rows[0]["TRUCK_CATEGORY"].ToString();
                txtHsEngine.Text = TableDataHeader.Rows[0]["SENGINE"].ToString();
                txtHsModel.SelectedValue = TableDataHeader.Rows[0]["SBRAND"].ToString().Trim() == string.Empty ? (TableDataHeader.Rows[0]["SMODEL"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SMODEL"].ToString()) : (TableDataHeader.Rows[0]["SBRAND"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SBRAND"].ToString());
                txtHsChasis.Text = TableDataHeader.Rows[0]["SCHASIS"].ToString();
                cboHsVibration.SelectedValue = TableDataHeader.Rows[0]["SVIBRATION"].ToString();
                wheel.SelectedValue = TableDataHeader.Rows[0]["NWHEELS"].ToString();
                cboPressure.SelectedValue = TableDataHeader.Rows[0]["MATERIALOFPRESSURE"].ToString();
                txtRnShaftDriven.Text = TableDataHeader.Rows[0]["NSHAFTDRIVEN"].ToString();
                txtRwightDriven.SelectedValue = TableDataHeader.Rows[0]["RWIGHTDRIVEN"].ToString();
                txtHPowermover.Text = TableDataHeader.Rows[0]["POWERMOVER"].ToString();
                txtRnWeight.Text = TableDataHeader.Rows[0]["NWEIGHT"].ToString();
                cboHPumpPower_type.SelectedValue = TableDataHeader.Rows[0]["PUMPPOWER_TYPE"].ToString();
                txtVEH_Volume.SelectedValue = TableDataHeader.Rows[0]["VOL_UOM"].ToString();
                cboHGPSProvider.SelectedValue = TableDataHeader.Rows[0]["GPS_SERVICE_PROVIDER"].ToString();
                TruckTank.Text = TableDataHeader.Rows[0]["STANK_MAKER"].ToString();
                materialTank.SelectedValue = TableDataHeader.Rows[0]["STANK_MATERAIL"].ToString();
                if (int.TryParse(TableDataHeader.Rows[0]["VALVETYPE"].ToString(), out tmp))
                {
                    rblRValveType.SelectedValue = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
                }
                else
                {
                    rblRValveType.SelectedValue = "3";
                    txtRValveType.Text = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
                }
                if (int.TryParse(TableDataHeader.Rows[0]["FUELTYPE"].ToString(), out tmp))
                {

                    rblRFuelType.SelectedValue = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
                }
                else
                {
                    rblRFuelType.SelectedValue = "3";
                    txtRFuelType.Text = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
                }

                cboHLoadMethod.SelectedValue = TableDataHeader.Rows[0]["SLOADING_METHOD"].ToString();
                ddl_addition.SelectedValue = TableDataHeader.Rows[0]["NSLOT"].ToString();
                type_vendor.SelectedValue = TableDataHeader.Rows[0]["ISUSE"].ToString();
                //เอกสารอัพโหลด
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(TTRUCKID, "TRUCK", "0");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                //เอกสารอัพโหลด
                dtUploadTPN = complainImportFileBLL.ImportFileSelectBLL3(TTRUCKID, "TRUCK", "1");
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                radSpot.SelectedValue = TableDataHeader.Rows[0]["SPOT_STATUS"].ToString();

                //บริษัทประกัน
                if (TableDataInsurance.Rows.Count > 0)
                {
                    company_name.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SCOMPANY"].ToString();
                    detailinsurance.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SDETAIL"].ToString();
                    start_duration.Text = string.Equals(TableDataInsurance.Rows[0]["START_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    end_duration.Text = string.Equals(TableDataInsurance.Rows[0]["END_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    txtbudget.Text = TableDataInsurance.Rows[0]["INSURE_BUDGET"].ToString();
                    txtninsure.Text = TableDataInsurance.Rows[0]["NINSURE"].ToString();
                    ddlTypeInsurance.SelectedValue = TableDataInsurance.Rows[0]["INSURE_TYPE"].ToString();
                    string[] sHolding = TableDataInsurance.Rows[0]["HOLDING"].ToString().Split(',');
                    if (!string.IsNullOrEmpty(sHolding[0]))
                    {
                        foreach (ListItem items in cbHolding.Items)
                        {
                            for (int i = 0; i < sHolding.Length; i++)
                            {
                                if (items.Value == sHolding[i])
                                    items.Selected = true;
                            }
                        }
                    }
                }
                //blacklist
                Blacklist_ds = CarBLL.Instance.initalBlacklist(TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString());
                //GridViewHelper.BindGridView(ref Data_cencal, Blacklist_ds);
                break;
            case "3":
                Truck.Visible = true;
                insurance.Visible = true;
                carrier.Visible = true;
                capacity.Visible = false;
                semitruck.Visible = false;
                document.Visible = true;
                companyTank.Visible = false;
                documentTPN.Visible = true;
                //สถานะวัดน้ำไม่แสดง
                scalewater.Visible = false;
                //bindValue
                cmbCarcate.SelectedValue = TypeId;
                first_regis.Text = string.Equals(TableDataHeader.Rows[0]["DREGISTER"].ToString(), "") ? "" : Convert.ToDateTime(TableDataHeader.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                txtHRegNo.Text = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString().Trim();
                cmbHsHolder.Text = TableDataHeader.Rows[0]["SOWNER_NAME"].ToString();
                cmbHRoute.SelectedValue = TableDataHeader.Rows[0]["ROUTE"].ToString();
                txtHsEngine.Text = TableDataHeader.Rows[0]["SENGINE"].ToString();
                cmbHTru_Cate.SelectedValue = TableDataHeader.Rows[0]["TRUCK_CATEGORY"].ToString();
                txtHsModel.Text = TableDataHeader.Rows[0]["SBRAND"].ToString().Trim() == string.Empty ? (TableDataHeader.Rows[0]["SMODEL"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SMODEL"].ToString()) : (TableDataHeader.Rows[0]["SBRAND"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SBRAND"].ToString());
                txtHsChasis.Text = TableDataHeader.Rows[0]["SCHASIS"].ToString();
                cboHsVibration.SelectedValue = TableDataHeader.Rows[0]["SVIBRATION"].ToString();
                wheel.SelectedValue = TableDataHeader.Rows[0]["NWHEELS"].ToString();
                cboPressure.SelectedValue = TableDataHeader.Rows[0]["MATERIALOFPRESSURE"].ToString();
                txtRnShaftDriven.Text = TableDataHeader.Rows[0]["NSHAFTDRIVEN"].ToString();
                txtRwightDriven.SelectedValue = TableDataHeader.Rows[0]["RWIGHTDRIVEN"].ToString();
                txtHPowermover.Text = TableDataHeader.Rows[0]["POWERMOVER"].ToString();
                txtRnWeight.Text = TableDataHeader.Rows[0]["NWEIGHT"].ToString();
                cboHPumpPower_type.SelectedValue = TableDataHeader.Rows[0]["PUMPPOWER_TYPE"].ToString();
                txtVEH_Volume.SelectedValue = TableDataHeader.Rows[0]["VOL_UOM"].ToString();
                cboHGPSProvider.SelectedValue = TableDataHeader.Rows[0]["GPS_SERVICE_PROVIDER"].ToString();
                if (int.TryParse(TableDataHeader.Rows[0]["VALVETYPE"].ToString(), out tmp))
                {
                    rblRValveType.SelectedValue = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
                }
                else
                {
                    rblRValveType.SelectedValue = "3";
                    txtRValveType.Text = TableDataHeader.Rows[0]["VALVETYPE"].ToString();
                }
                if (int.TryParse(TableDataHeader.Rows[0]["FUELTYPE"].ToString(), out tmp))
                {

                    rblRFuelType.SelectedValue = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
                }
                else
                {
                    rblRFuelType.SelectedValue = "3";
                    txtRFuelType.Text = TableDataHeader.Rows[0]["FUELTYPE"].ToString();
                }
                //เอกสารอัพโหลด
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(TTRUCKID, "TRUCK", "0");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                //เอกสารอัพโหลด
                dtUploadTPN = complainImportFileBLL.ImportFileSelectBLL3(TTRUCKID, "TRUCK", "1");
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                radSpot.SelectedValue = TableDataHeader.Rows[0]["SPOT_STATUS"].ToString();

                //สถานะรถลอย
                type_vendor.SelectedValue = TableDataHeader.Rows[0]["ISUSE"].ToString();
                //บริษัทประกัน
                if (TableDataInsurance.Rows.Count > 0)
                {
                    company_name.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SCOMPANY"].ToString();
                    detailinsurance.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SDETAIL"].ToString();
                    start_duration.Text = string.Equals(TableDataInsurance.Rows[0]["START_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    end_duration.Text = string.Equals(TableDataInsurance.Rows[0]["END_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    txtbudget.Text = TableDataInsurance.Rows[0]["INSURE_BUDGET"].ToString();
                    txtninsure.Text = TableDataInsurance.Rows[0]["NINSURE"].ToString();
                    ddlTypeInsurance.SelectedValue = TableDataInsurance.Rows[0]["INSURE_TYPE"].ToString();
                    string[] sHolding = TableDataInsurance.Rows[0]["HOLDING"].ToString().Split(',');
                    if (!string.IsNullOrEmpty(sHolding[0]))
                    {
                        foreach (ListItem items in cbHolding.Items)
                        {
                            for (int i = 0; i < sHolding.Length; i++)
                            {
                                if (items.Value == sHolding[i])
                                    items.Selected = true;
                            }
                        }
                    }
                }

                break;
            case "4":
                semitruck.Visible = true;
                insurance.Visible = true;
                carrier.Visible = true;
                capacity.Visible = true;
                Truck.Visible = false;
                document.Visible = true;
                documentTPN.Visible = true;
                cmbCarcate.SelectedValue = TypeId;
                semifirst_regis.Text = string.Equals(TableDataHeader.Rows[0]["DREGISTER"].ToString(), "") ? "" : Convert.ToDateTime(TableDataHeader.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                txtSemiNo.Text = TableDataHeader.Rows[0]["SHEADREGISTERNO"].ToString().Trim();
                cmbSemiHolder.Text = TableDataHeader.Rows[0]["SOWNER_NAME"].ToString();
                cmbCarSemiSap.SelectedValue = TableDataHeader.Rows[0]["TRUCK_CATEGORY"].ToString();
                txtSemiChasis.Text = TableDataHeader.Rows[0]["SCHASIS"].ToString();
                txtsemimodal.SelectedValue = TableDataHeader.Rows[0]["SBRAND"].ToString().Trim() == string.Empty ? (TableDataHeader.Rows[0]["SMODEL"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SMODEL"].ToString()) : (TableDataHeader.Rows[0]["SBRAND"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SBRAND"].ToString());
                SemiWheel.SelectedValue = TableDataHeader.Rows[0]["NWHEELS"].ToString();
                txtSemiShaftDriven.Text = TableDataHeader.Rows[0]["NSHAFTDRIVEN"].ToString();
                rblSemiPumpPower.SelectedValue = TableDataHeader.Rows[0]["PUMPPOWER"].ToString();
                txtSemiWeight.Text = TableDataHeader.Rows[0]["NWEIGHT"].ToString();
                txtownTank.Text = TableDataHeader.Rows[0]["STANK_MAKER"].ToString();
                cboRTankMaterial.SelectedValue = TableDataHeader.Rows[0]["STANK_MATERAIL"].ToString();
                cboHLoadMethod.SelectedValue = TableDataHeader.Rows[0]["SLOADING_METHOD"].ToString();
                ddl_addition.SelectedValue = TableDataHeader.Rows[0]["NSLOT"].ToString();
                txtHsModel.SelectedValue = TableDataHeader.Rows[0]["SBRAND"].ToString().Trim() == string.Empty ? (TableDataHeader.Rows[0]["SMODEL"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SMODEL"].ToString()) : (TableDataHeader.Rows[0]["SBRAND"].ToString().Length > 2 ? TableDataHeader.Rows[0]["BRANDID"].ToString() : TableDataHeader.Rows[0]["SBRAND"].ToString());
                //เอกสารอัพโหลด
                dtUpload = complainImportFileBLL.ImportFileSelectBLL2(TTRUCKID, "TRUCK", "0");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                //เอกสารอัพโหลด
                dtUploadTPN = complainImportFileBLL.ImportFileSelectBLL3(TTRUCKID, "TRUCK", "1");
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                radSpot.SelectedValue = TableDataHeader.Rows[0]["SPOT_STATUS"].ToString();

                //สถานะรถลอย
                type_vendor.SelectedValue = TableDataHeader.Rows[0]["ISUSE"].ToString();
                //บริษัทประกัน
                if (TableDataInsurance.Rows.Count > 0)
                {
                    company_name.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SCOMPANY"].ToString();
                    detailinsurance.Text = string.Equals(TableDataInsurance.Rows.Count.ToString(), "0") ? "" : TableDataInsurance.Rows[0]["SDETAIL"].ToString();
                    start_duration.Text = string.Equals(TableDataInsurance.Rows[0]["START_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    end_duration.Text = string.Equals(TableDataInsurance.Rows[0]["END_DURATION"].ToString(), "") ? "" : Convert.ToDateTime(TableDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    txtbudget.Text = TableDataInsurance.Rows[0]["INSURE_BUDGET"].ToString();
                    txtninsure.Text = TableDataInsurance.Rows[0]["NINSURE"].ToString();
                    ddlTypeInsurance.SelectedValue = TableDataInsurance.Rows[0]["INSURE_TYPE"].ToString();
                    string[] sHolding = TableDataInsurance.Rows[0]["HOLDING"].ToString().Split(',');
                    if (!string.IsNullOrEmpty(sHolding[0]))
                    {
                        foreach (ListItem items in cbHolding.Items)
                        {
                            for (int i = 0; i < sHolding.Length; i++)
                            {
                                if (items.Value == sHolding[i])
                                    items.Selected = true;
                            }
                        }
                    }
                }



                break;
            default:
                semitruck.Visible = false;
                insurance.Visible = false;
                carrier.Visible = false;
                capacity.Visible = false;
                Truck.Visible = false;
                document.Visible = false;
                break;


        }
    }
    #endregion
    #region InitialForm
    private void InitialForm(string CGROUP, DataTable TableDataHeader)
    {

        try
        {
            this.InitialData();
            this.InitialUpload();
            #region Setcondition เอกสารอนุมัติรถ
            TruckDoccument TDOC = new TruckDoccument();
            TDOC.Doc_TRUCKID = V_TRUCKID;
            TDOC.CheckDocComplate();
            #endregion
            switch (CGROUP)
            {

                case "0":
                    DataTable dt_vendor = VendorBLL.Instance.SelectName(SVDID);
                    if (dt_vendor.Rows.Count > 0)
                    {
                        NameVendor.Text = dt_vendor.Rows[0]["SABBREVIATION"].ToString();
                        NamveVendor = dt_vendor.Rows[0]["SABBREVIATION"].ToString();
                    }
                    else
                    {
                        NameVendor.Text = string.Empty;
                        NamveVendor = string.Empty;
                    }
                    STRANSPORTID.Visible = false;
                    owncar.Visible = true;
                    VendorID = SVDID;//ดูค่าว่าSVDID ใช่ค่าเดียวกันไหม

                    divcomment.Visible = TDOC.CheckDocComplate();
                    commenttext.InnerText = "รข.ขอรายละเอียดเพิ่มเติม ดังนี้";
                    txtcomment.Enabled = false;
                    first_regis.Enabled = false;
                    xbnLastHis.Visible = true;
                    break;
                case "2":
                    STRANSPORTID.Visible = true;
                    NameVendor.Visible = false;
                    first_regis.Enabled = true;
                    xbnLastHis.Visible = true;
                    break;
                case "1":

                    DataTable test = VendorBLL.Instance.SelectName("");
                    STRANSPORTID.Visible = true;
                    owncar.Visible = false;
                    divcomment.Visible = TDOC.CheckDocComplate();
                    btnapprove.Visible = TDOC.CheckDocComplate();
                    btncomment.Visible = TDOC.CheckDocComplate();
                    btncancel.Visible = TDOC.CheckDocComplate();
                    btnSave.Text = "บันทึกและส่งข้อมูล";
                    xbnLastHis.Visible = true;
                    //เขียนโค็ดเช็คblacklist
                    try
                    {
                        VendorID = string.Equals(TableDataHeader.Rows.Count.ToString(), "0") ? "" : TableDataHeader.Rows[0]["STRANSPORTID"].ToString();
                        STRANSPORTID.SelectedValue = VendorID;
                        STRANSPORTID.Enabled = false;
                        NamveVendor = STRANSPORTID.SelectedItem.Text;

                    }
                    catch (Exception)
                    {

                        STRANSPORTID.SelectedValue = "";
                    }

                    NameVendor.Visible = false;
                    first_regis.Enabled = true;
                    break;
                case "3":

                    DataTable test2 = VendorBLL.Instance.SelectName("");
                    STRANSPORTID.Visible = true;
                    owncar.Visible = false;
                    divcomment.Visible = TDOC.CheckDocComplate();
                    btnapprove.Visible = TDOC.CheckDocComplate();
                    btncomment.Visible = TDOC.CheckDocComplate();
                    btncancel.Visible = TDOC.CheckDocComplate();
                    btnSave.Text = "บันทึกและส่งข้อมูล";
                    xbnLastHis.Visible = true;
                    //เขียนโค็ดเช็คblacklist
                    try
                    {
                        VendorID = string.Equals(TableDataHeader.Rows.Count.ToString(), "0") ? "" : TableDataHeader.Rows[0]["STRANSPORTID"].ToString();
                        STRANSPORTID.SelectedValue = VendorID;
                        STRANSPORTID.Enabled = false;
                        NamveVendor = STRANSPORTID.SelectedItem.Text;

                    }
                    catch (Exception)
                    {

                        STRANSPORTID.SelectedValue = "";
                    }

                    NameVendor.Visible = false;
                    first_regis.Enabled = true;
                    break;
                default:
                    break;
            }
            //this.InitialData();
            //this.InitialUpload();

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #region InitialData
    protected void InitialData()
    {
        try
        {
            DataTable dt = CarBLL.Instance.LoadDataRoute();
            DataTable dt_CartypeSap = CarBLL.Instance.LoadDataCarSap();
            DataTable dt_typecar = CarBLL.Instance.LoadDataCartype();
            DataTable dt_brand = CarBLL.Instance.LoadBrand();
            DataTable dt_gps = CarBLL.Instance.LoadGps();
            // รอรับการเลือกพนักงานว่าสังกัดพนักงานอะไร
            string condition = string.Empty;
            DataTable dt_vendor = VendorBLL.Instance.SelectName(condition);
            DropDownListHelper.BindDropDownList(ref cmbCarcate, dt_typecar, "config_value", "config_name", true);
            DropDownListHelper.BindDropDownList(ref cmbHRoute, dt, "ROUTE", "DESCRIPTION", false);
            DropDownListHelper.BindDropDownList(ref cmbHTru_Cate, dt_CartypeSap, "TYPE_ID", "TYPE_NAME", false);
            DropDownListHelper.BindDropDownList(ref cmbCarSemiSap, dt_CartypeSap, "TYPE_ID", "TYPE_NAME", false);
            DropDownListHelper.BindDropDownList(ref txtHsModel, dt_brand, "BRANDID", "BRANDNAME", true);
            DropDownListHelper.BindDropDownList(ref txtsemimodal, dt_brand, "BRANDID", "BRANDNAME", false);
            DropDownListHelper.BindDropDownList(ref cboHGPSProvider, dt_gps, "GPSID", "GPSNAME", true);
            DropDownListHelper.BindDropDownList(ref STRANSPORTID, dt_vendor, "SVENDORID", "SABBREVIATION", true);
            this.GetCapacityNum(ref ddl_addition, 20);
            this.CalculateWheel(wheel, 26);
            this.CalculateWheel(SemiWheel, 26);

            //if (cmbCarcate.SelectedValue == "0" || cmbCarcate.SelectedValue == "")
            //    this.SetShaftDriven(txtRnShaftDriven, 5);
            //else
            //    this.SetShaftDrivenSemi(txtRnShaftDriven, 5);

            if (cmbCarcate.SelectedValue == "0" || cmbCarcate.SelectedValue == "")
                this.SetShaftDriven(txtRnShaftDriven, 5);
            else
                this.SetShaftDriven(txtRnShaftDriven, 5);

            this.SetShaftDriven(txtRwightDriven, 10);
            this.SetShaftDriven(txtSemiShaftDriven, 10);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #region Upload
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                if (!string.Equals(txtSuggestFileName.Text.Trim(), System.IO.Path.GetFileNameWithoutExtension(FileNameUser)))
                    throw new Exception("ชื่อไฟล์ไม่ถูกต้อง ตามที่กำหนด");

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
            }
        }
        catch (Exception ex)
        {
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Truck" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

            this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string FileName = dgvUploadFile.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName);
    }

    private void DownloadFile(string Path, string FileName, string FileNameDownload)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileNameDownload);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                if (!CanWrite)
                {
                    imgDelete.Enabled = false;
                    imgDelete.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL2("TRUCK", "0");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUploadTypeTPN = UploadTypeBLL.Instance.UploadTypeSelectBLL3("TRUCK", "1");
        DropDownListHelper.BindDropDownList(ref ddlUploadTypeTPN, dtUploadTypeTPN, "UPLOAD_ID", "UPLOAD_NAME", true);

        if (dtUpload == null || dtUpload.Rows.Count == 0)
        {
            dtUpload = new DataTable();
            dtUpload.Columns.Add("UPLOAD_ID");
            dtUpload.Columns.Add("UPLOAD_NAME");
            dtUpload.Columns.Add("FILENAME_SYSTEM");
            dtUpload.Columns.Add("FILENAME_USER");
            dtUpload.Columns.Add("FULLPATH");
        }

        if (dtUploadTPN == null || dtUploadTPN.Rows.Count == 0)
        {
            dtUploadTPN = new DataTable();
            dtUploadTPN.Columns.Add("UPLOAD_ID");
            dtUploadTPN.Columns.Add("UPLOAD_NAME");
            dtUploadTPN.Columns.Add("FILENAME_SYSTEM");
            dtUploadTPN.Columns.Add("FILENAME_USER");
            dtUploadTPN.Columns.Add("FULLPATH");
            dtUploadTPN.Columns.Add("START_DATE");
            dtUploadTPN.Columns.Add("STOP_DATE");
            dtUploadTPN.Columns.Add("DOC_NUMBER");
            dtUploadTPN.Columns.Add("YEAR");
        }
    }
    #region Validate Upload
    private void ValidateUploadFile()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }

            StringBuilder sb2 = new StringBuilder();
            sb2.Append("<table>");
            if (dtUploadTPN.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFileTPN.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb2.Append("<tr>");
                        sb2.Append("<td>โปรดแนบเอกสาร</td>");
                        sb2.Append("<td>");
                        sb2.Append(">" + dtRequestFileTPN.Rows[j]["UPLOAD_NAME"].ToString());
                        sb2.Append("</td>");
                        sb2.Append("</tr>");
                    }
                    else
                    {
                        sb2.Append("<tr>");
                        sb2.Append("<td></td>");
                        sb2.Append("<td>");
                        sb2.Append(">" + dtRequestFileTPN.Rows[j]["UPLOAD_NAME"].ToString());
                        sb2.Append("</td>");
                        sb2.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFileTPN.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUploadTPN.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFileTPN.Rows[i]["UPLOAD_ID"].ToString(), dtUploadTPN.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUploadTPN.Rows.Count - 1)
                        {
                            string UploadIDTPN2 = string.Empty;
                            string UploadIDTPN3 = string.Empty;
                            bool IsFoundTPN3 = false;

                            for (int k = 0; k < dtUploadTypeTPN.Rows.Count; k++)
                            {//ถ้าไม่ได้แนบ ธพ.น.2 ให้แนบ ธพ.น.3 แทนได้
                                if (string.Equals(dtUploadTypeTPN.Rows[k]["SPECIAL_FLAG"].ToString(), "1"))
                                    UploadIDTPN2 = dtUploadTypeTPN.Rows[k]["UPLOAD_ID"].ToString();

                                if (string.Equals(dtUploadTypeTPN.Rows[k]["SPECIAL_FLAG"].ToString(), "2"))
                                    UploadIDTPN3 = dtUploadTypeTPN.Rows[k]["UPLOAD_ID"].ToString();
                            }

                            if (string.Equals(dtRequestFileTPN.Rows[i]["UPLOAD_ID"].ToString(), UploadIDTPN2))
                            {
                                for (int m = 0; m < dtUploadTPN.Rows.Count; m++)
                                {
                                    if (string.Equals(UploadIDTPN3, dtUploadTPN.Rows[m]["UPLOAD_ID"].ToString()))
                                    {
                                        IsFoundTPN3 = true;
                                        break;
                                    }
                                }
                            }

                            if (!IsFoundTPN3)
                            {
                                if (string.Equals(sb2.ToString(), "<table>"))
                                {
                                    sb2.Append("<tr>");
                                    sb2.Append("<td>โปรดแนบเอกสาร</td>");
                                    sb2.Append("<td>");
                                    sb2.Append(">" + dtRequestFileTPN.Rows[i]["UPLOAD_NAME"].ToString());
                                    sb2.Append("</td>");
                                    sb2.Append("</tr>");
                                }
                                else
                                {
                                    sb2.Append("<tr>");
                                    sb2.Append("<td></td>");
                                    sb2.Append("<td>");
                                    sb2.Append(">" + dtRequestFileTPN.Rows[i]["UPLOAD_NAME"].ToString());
                                    sb2.Append("</td>");
                                    sb2.Append("</tr>");
                                }
                            }
                        }
                    }
                }
            }
            sb2.Append("</table>");

            if (!string.Equals(sb2.ToString(), "<table></table>"))
            {
                throw new Exception(sb2.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #endregion
    #region Event DevExpress

    protected void xcpnMain_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        string USER = Session["UserID"] + "";
        switch (paras[0])
        {
            case "FullData":
                string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("TRUCK&" + txtHRegNo.Text.Trim() + "&" + V_TRUCKID))
                        , sUrl = "Truck_History_Info.aspx?str=";
                xcpnMain.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                break;
        }
    }
    #endregion
    #region Even Button
    #region Approve
    protected void approve_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateCapacity(int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));
            this.ValidateUploadFile();
            string TRUCKID = V_TRUCKID;
            NewDataCapacity = this.DataCapacity(int.Parse(capacityCell.SelectedValue));
            this.SetLogToInsert(regisText);
            DataTable retrunValue = CarBLL.Instance.UpdateTruck(TRUCKID, cmbCarcate.SelectedValue.ToString(), DataToDatatable(), dtUpload, dtUploadTPN, DataInsurance(), NewDataCapacity, radSpot.SelectedValue);

            this.ApproveFunction();
            this.SendEmail(ConfigValue.EmailTruckApprove, V_TRUCKID);

            alertSuccess("แก้ไขข้อมูลรถใน SAP สำเร็จ <br/ >แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "approve_pk.aspx");
        }
        catch (Exception ex)
        {

            alertFail("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ <br/ >แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ" + ex.Message);
        }

    }
    #region conditionForApprove
    //ถ้ามีการแก้ไขหลังจากอนุมัติไป 1 รอบ เพื่อไม่ให้เปลี่ยนสถานะ รถ
    private void CheckApproveDocNoTruck()
    {
        if (string.Equals(LOGDATA, "0"))
        {
            CarBLL.Instance.UpdateApprove(V_TRUCKID);
        }
    }
    #endregion
    #endregion
    #region Cencal Click
    protected void cancel_Click(object sender, EventArgs e)
    {
        try
        {
            CarBLL.Instance.TruckCancelBLL(V_TRUCKID);
            #region บันทึก Log
            LOG.Instance.SaveLog("รข.ปฏิเสธ ข้อมูลรถทะเบียน", regisText, Session["UserID"].ToString(), "<span class=\"red\">" + registerno + "</span>", "");

            #endregion
            SystemFunction.AddToTREQ_DATACHANGE(V_TRUCKID, "T", Session["UserID"] + "", "2", registerno, VendorID, "Y", txtcomment.Text.ToString());

            this.SendEmail(ConfigValue.EmailTruckNoApprove, V_TRUCKID);
            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "approve_pk.aspx");
        }
        catch (Exception ex)
        {

            alertFail("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ <br/ >แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ" + ex.Message);
        }

    }
    #endregion
    #region SaveData
    protected void cmdSaveTab1_Click(object sender, EventArgs e)
    {
        try
        {
            if (radSpot.SelectedIndex < 0)
                throw new Exception("กรุณาเลือก ประเภทการจ้างงาน (สัญญา, จ้างพิเศษ) !!!");

            this.ValidateCapacity(int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));
            this.ValidateUploadFile();
            string TRUCKID = V_TRUCKID;
            //เก็บค่าช่องเติม
            NewDataCapacity = this.DataCapacity(int.Parse(capacityCell.SelectedValue));
            this.SetLogToInsert(regisText);
            DataTable retrunValue = CarBLL.Instance.UpdateTruck(TRUCKID, cmbCarcate.SelectedValue.ToString(), DataToDatatable(), dtUpload, dtUploadTPN, DataInsurance(), NewDataCapacity, radSpot.SelectedValue);
            if (string.Equals(CGROUP, "1"))
            {
                if (string.Equals(retrunValue.Rows[0][0].ToString(), "01"))
                {
                    try
                    {
                        this.ApproveFunction();
                        alertSuccess("แก้ไขข้อมูลรถใน SAP สำเร็จ <br/ >แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "approve_pk.aspx");
                        //บันทึกข้อมูลสำเร็จ
                    }
                    catch (Exception)
                    {

                        alertSuccess("แก้ไขข้อมูลรถใน SAP สำเร็จ <br/ >แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ", "approve_pk.aspx");
                    }

                }
                else
                {
                    alertFail("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ <br/ >แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ" + retrunValue.Rows[0][0].ToString());
                }
            }
            else
            {
                if (string.Equals(retrunValue.Rows[0][0].ToString(), "01"))
                {
                    try
                    {
                        AddRequestLog(type_vendor.SelectedValue.ToString(), retrunValue.Rows[0][1].ToString());

                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ <br /> รอการอนุมัติ", "vendor_request.aspx");
                    }
                    catch (Exception ex)
                    {

                        alertFail("แก้ไขข้อมูลรถใน SAP สำเร็จ <br/ >แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ" + retrunValue.Rows[0][0].ToString() + ex.Message);
                    }

                }
                else
                {
                    alertFail(retrunValue.Rows[0][0].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }


    #endregion
    #region SapModify
    private string SetmodifirySap(string regisNumber)
    {
        string msg;
        SapTU.regisnumber = regisNumber;
        msg = SapTU.SapUpdateTU();
        if (msg.Contains(",Entry " + regisNumber.ToUpper() + " does not exist"))
        {
            msg = SapTU.SapCreateTU();
        }
        return msg;

    }
    #endregion
    #region Comment ส่งผู้ขนส่ง
    protected void cmdComment_Click(object sender, EventArgs e)
    {
        try
        {
            #region บันทึก Log
            LOG.Instance.SaveLog("รข.ขอข้อมูลเพิ่มเติม ข้อมูลรถทะเบียน", regisText, Session["UserID"].ToString(), "<span class=\"orange\">" + txtcomment.Text.Trim() + "</span>", "");

            #endregion
            CarBLL.Instance.SaveComment(V_TRUCKID, txtcomment.Text.Trim().ToString());
            SystemFunction.AddToTREQ_DATACHANGE(V_TRUCKID, "T", Session["UserID"] + "", "3", registerno, VendorID, "Y", txtcomment.Text.ToString());
            this.SendEmail(ConfigValue.EmailTruckComment, V_TRUCKID);
            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "approve_pk.aspx");
        }
        catch (Exception)
        {

            alertFail("แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ");
        }

    }
    #endregion
    #endregion
    #region RowAndDataCapacity สร้าง ตาราง
    #region InitalTable
    public void CreateHeadCapacity(int Columns)
    {
        if (!int.Equals(Columns, 0))
        {
            TableRow tRow;
            // Total number of rows.
            int cellCtr;
            // Current cell counter.
            int cellCnt;
            cellCnt = Columns;
            // Create a new row and add it to the table.
            tRow = new TableRow();

            TableCell tCell = new TableCell();
            Label label = new Label();
            label.Text = "CompartmentNo.";

            tCell.Controls.Add(label);
            tCell.HorizontalAlign = HorizontalAlign.Center;
            tCell.Width = Unit.Pixel(150);
            tRow.Cells.Add(tCell);
            //กำหนดาช่องเติม
            string stringText = "";

            for (cellCtr = 1; cellCtr <= cellCnt; cellCtr++)
            {

                tCell = new TableCell();
                switch (cellCtr)
                {
                    case 1:
                        stringText = " (High) ";
                        break;
                    case 2:
                        stringText = " (Low) ";
                        break;
                    case 3:
                        stringText = " (Middle) ";
                        break;
                    default:
                        break;
                }
                label = new Label();
                // tCell = new TableCell();
                label.Text = "ระดับแป้นที่" + cellCtr.ToString() + stringText.ToString();
                tCell.Controls.Add(label);
                tCell.HorizontalAlign = HorizontalAlign.Center;
                tRow.Cells.Add(tCell);
            }
            tRow.Font.Bold = true;
            table_capacity.Rows.Add(tRow);

        }
    }
    public void GenRowCapacity(int RowsData, int ColumnData)
    {
        // Total number of rows.
        int rowCnt;
        // Current row count.
        int rowCtr;
        // Total number of cells per row (columns).
        int cellCtr;
        // Current cell counter.
        int cellCnt;
        rowCnt = RowsData;
        cellCnt = ColumnData;
        for (rowCtr = 0; rowCtr < rowCnt; rowCtr++)
        {
            // Create a new row and add it to the table.
            //No.
            TableRow tRow = new TableRow();
            TableCell tCell = new TableCell();
            Label label = new Label();
            label.Text = "ช่องที่ " + int.Parse((rowCtr + 1).ToString());
            tCell.Controls.Add(label);
            tCell.HorizontalAlign = HorizontalAlign.Center;
            tCell.Width = Unit.Pixel(150);
            tRow.Cells.Add(tCell);
            table_capacity.Rows.Add(tRow);
            for (cellCtr = 1; cellCtr < cellCnt + 1; cellCtr++)
            {

                // Create a new cell and add it to the row.
                tCell = new TableCell();
                tCell.HorizontalAlign = HorizontalAlign.Center;
                tRow.Cells.Add(tCell);
                // Mock up a product ID.

                TextBox txt = new TextBox();
                txt.Width = 150;
                txt.EnableViewState = true;
                txt.CssClass = "form-control number placehoderKg required";
                txt.MaxLength = 5;
                tCell.HorizontalAlign = HorizontalAlign.Center;
                //txt.Text = "TxT" + (i + 1).ToString() + "_" + (j + 1).ToString();
                txt.ID = "TxT" + (rowCtr + 1).ToString() + "_" + (cellCtr).ToString();
                // pnl.Controls.Add(txt);
                tCell.Controls.Add(txt);
            }
        }
    }
    #region GETDATACAPACITY
    public void GetDataCapacity(DataTable Valuecapacity)
    {
        try
        {
            int rowCtr;
            for (rowCtr = 0; int.Parse(Valuecapacity.Rows.Count.ToString()) > rowCtr; rowCtr++)
            {
                ((TextBox)table_capacity.FindControl("TxT" + int.Parse(Valuecapacity.Rows[rowCtr]["NCOMPARTNO"].ToString()) + "_" + int.Parse(Valuecapacity.Rows[rowCtr]["NPANLEVEL"].ToString()))).Text = Valuecapacity.Rows[rowCtr]["NCAPACITY"].ToString();
            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }

    }
    #endregion
    #endregion
    #region SetCapacityToDatatable
    public DataTable DataCapacity(int Columncapacity)
    {
        DataTable dt = new DataTable();
        DataTable Data_dt = new DataTable();
        // Get columns from first row
        if (table_capacity.Rows.Count > 0)
        {
            for (int i = 1; i < Columncapacity + 1; i++)
            {
                dt.Columns.Add("Column_" + i);
            }
            int rowIndex = 1;
            var table_capacity_row = table_capacity.Rows[0];
            table_capacity.Rows.RemoveAt(0);
            foreach (TableRow row in table_capacity.Rows)
            {
                DataRow dr = dt.NewRow();
                for (int i = 1; i < Columncapacity + 1; i++)
                {
                    dr["Column_" + i] = ((TextBox)table_capacity.FindControl("TxT" + rowIndex + "_" + i)).Text;

                }
                dt.Rows.Add(dr);
                rowIndex++;
            }
            table_capacity.Rows.AddAt(0, table_capacity_row);

        }
        int rowsindex = 0;
        Data_dt.Columns.Add("NCOMPARTNO");
        Data_dt.Columns.Add("NPANLEVEL");
        Data_dt.Columns.Add("NCAPACITY");
        for (int row = 0; row < dt.Rows.Count; row++)
        {
            for (int column = 1; column < dt.Columns.Count + 1; column++)
            {
                string value_capacity = dt.Rows[row]["Column_" + column].ToString();
                Data_dt.Rows.Add(rowsindex + 1, int.Parse(column.ToString()), value_capacity);
            }
            rowsindex++;
        }

        return Data_dt;
    }
    #endregion
    #endregion
    #region Miscellaneous ฟังชันเปลี่ยนค่า
    protected string LangeTypeCar(int type_num)
    {
        switch (type_num)
        {
            case 0:
                return " (รถเดี่ยว)";

            case 3:
                return " (หัวลาก)";
            case 4:
                return " (หางลาก) ";

            default:
                return " (รถเดี่ยว)";

        }
    }
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    private string RadioToTextBox(string p, string Returnstring)
    {
        if (string.Equals(p, "3"))
        {
            return Returnstring;
        }
        else
        {
            return p;
        }
    }

    private string nullvalue(string p)
    {
        if (string.Equals(p, ""))
        {
            return "0";
        }
        else
        {
            return p;
        }

    }
    //ฟังชันล้อ
    private void CalculateWheel(DropDownList dropdown, Int32 max_wheel)
    {
        dropdown.Items.Clear();
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 6; i <= max_wheel; i += 2)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    //สร้างแถว
    public void GetCapacityNum(ref DropDownList Name, int Num)
    {
        int count = 20;
        for (int i = 1; i < count + 1; i++)
        {

            Name.Items.Add(i.ToString());

        }

    }
    private void SetShaftDriven(DropDownList dropdown, int Num)
    {
        dropdown.Items.Clear();
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 1; i < Num; i++)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    private void SetShaftDrivenSemi(DropDownList dropdown, int Num)
    {
        dropdown.Items.Clear();
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 2; i < Num; i++)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    private void ValidateCapacity(int RowNo, int columnNo)
    {
        StringBuilder sb = new StringBuilder();
        if (columnNo > 1)
        {
            if (columnNo == 2)
            {

                for (int i = 1; i < RowNo + 1; i++)
                {
                    int capacity1 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_1")).Text.ToString());
                    int capacity2 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_2")).Text.ToString());

                    //if (capacity1 < capacity2)
                    //{
                    //    sb.Clear();
                    //    sb.Append("ปริมาตรช่องเติมมากกว่าช่องสูงสุด");
                    //}
                }
            }
            else
            {
                for (int i = 1; i < RowNo + 1; i++)
                {
                    int capacity1 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_1")).Text.ToString());
                    int capacity2 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_2")).Text.ToString());
                    int capacity3 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_3")).Text.ToString());

                    //if (capacity1 < capacity2 || capacity2 > capacity3 || capacity1 < capacity3)
                    //{
                    //    sb.Clear();
                    //    sb.Append("ปริมาตรช่องเติมมากกว่าช่องสูงสุด");
                    //}
                }
            }
            if (!string.IsNullOrEmpty(sb.ToString()))
                throw new Exception(sb.ToString());

        }
    }
    private int NullToInt(string value)
    {
        if (string.Equals(value.ToString(), string.Empty))
        {
            return 0;
        }
        else
        {
            return int.Parse(value.ToString());
        }
    }
    #endregion
    #region Alert การผิดพลาด
    protected void CreateRowsCapacity(object sender, EventArgs e)
    {
        if (string.Equals(ddl_addition.SelectedValue, "0"))
        {
            alertFail("กรุณาเลือกช่องเติมก่อน");

        }

    }
    #endregion
    #region Ajax
    [System.Web.Services.WebMethod]
    public static List<string> VendorJson(string Vendor)
    {
        DataTable dtVendor = VendorBLL.Instance.LoadNameVendor(Vendor);
        List<string> DataVendor = new List<string>();
        for (int i = 0; i < dtVendor.Rows.Count; i++)
        {

            DataVendor.Add(dtVendor.Rows[i][0].ToString());
        }

        return DataVendor;

    }
    #endregion
    #region DataToDatatable
    #region บันทึกข้อมูลTRUCK
    protected DataTable DataToDatatable()
    {
        DataTable dt;

        //string MVDoc = string.Empty;
        //string MVDateStart = string.Empty;
        //string MVDateEnd = string.Empty;
        //if (Session["AutoApprove"] != null && Session["dtMVUpload"] != null)
        //{
        //    DataTable dtMVUpload = (DataTable)Session["dtMVUpload"];
        //    MVDoc = dtMVUpload.Rows[0]["DOC_ID"].ToString();
        //    MVDateStart = dtMVUpload.Rows[0]["START_DATE"].ToString();
        //    MVDateEnd = dtMVUpload.Rows[0]["END_DATE"].ToString();

        //    Session.Remove("dtMVUpload");
        //}
        //else
        //{
        //    MVDoc = txtcodewater.Text.Trim();
        //    MVDateStart = scale_start.Text.Trim();
        //    MVDateEnd = scale_end.Text.Trim();
        //}

        switch (cmbCarcate.SelectedValue.ToString())
        {
            case "4":
                dt = new DataTable();
                dt.Columns.Add("TypeCar");
                dt.Columns.Add("semifirst_regis");
                dt.Columns.Add("txtHRegNo");
                dt.Columns.Add("cmbHsHolder");
                dt.Columns.Add("STRANSPORTID");
                dt.Columns.Add("cmbHTru_Cate");
                dt.Columns.Add("txtHsModel");
                dt.Columns.Add("cboHLoadMethod");
                dt.Columns.Add("txtRnShaftDriven");
                dt.Columns.Add("txtHsChasis");
                dt.Columns.Add("wheel");
                dt.Columns.Add("txtRnWeight");
                dt.Columns.Add("txtownTank");
                dt.Columns.Add("cboRTankMaterial");
                dt.Columns.Add("cboHPumpPower_type");
                dt.Columns.Add("nslot");
                dt.Columns.Add("sessionId");
                dt.Columns.Add("type_vendor");
                //ข้อมูลวัดน้ำ
                dt.Columns.Add("PLACE_WATER_MEASURE");
                dt.Columns.Add("SLAST_REQ_ID");
                dt.Columns.Add("DPREV_SERV");
                dt.Columns.Add("DWATEREXPIRE");

                dt.Rows.Add(
                    int.Parse(cmbCarcate.SelectedValue.ToString()),
                    DateTime.ParseExact(semifirst_regis.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    txtSemiNo.Text.Trim(),
                    cmbSemiHolder.Text.ToString(),
                    string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID,
                    cmbCarSemiSap.SelectedValue.ToString(),
                    txtsemimodal.SelectedValue.ToString(),
                    cboHLoadMethod.SelectedValue.ToString(),
                    txtSemiShaftDriven.SelectedValue.ToString(),
                    txtSemiChasis.Text,
                    SemiWheel.SelectedValue,
                    txtSemiWeight.Text,
                    txtownTank.Text.Trim().ToString(),
                    cboRTankMaterial.SelectedValue,
                    rblSemiPumpPower.SelectedValue.ToString(),
                    ddl_addition.SelectedValue.ToString(),
                    Session["UserID"].ToString(),
                    type_vendor.SelectedValue.ToString(),

                    (radPlaceMV.Visible) ? radPlaceMV.SelectedValue : placewater.SelectedValue,

                    //MVDoc,
                    //string.Equals(MVDateStart, string.Empty) ? "" : DateTime.ParseExact(MVDateStart, "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    //string.Equals(MVDateEnd, string.Empty) ? "" : DateTime.ParseExact(MVDateEnd, "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)

                    txtcodewater.Text.Trim(),
                    string.Equals(scale_start.Text, string.Empty) ? "" : DateTime.ParseExact(scale_start.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    string.Equals(scale_end.Text, string.Empty) ? "" : DateTime.ParseExact(scale_end.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)

                    );
                break;
            default:
                dt = new DataTable();
                dt.Columns.Add("TypeCar");
                dt.Columns.Add("first_regis");
                dt.Columns.Add("txtHRegNo");
                dt.Columns.Add("STRANSPORTID");
                dt.Columns.Add("cmbHsHolder");
                dt.Columns.Add("cmbHRoute");
                dt.Columns.Add("txtHsEngine");
                dt.Columns.Add("cmbHTru_Cate");
                dt.Columns.Add("cboHLoadMethod");
                dt.Columns.Add("txtHsModel");
                dt.Columns.Add("txtHsChasis");
                dt.Columns.Add("cboHsVibration");
                dt.Columns.Add("wheel");
                dt.Columns.Add("cboRMaterialOfPressure");
                dt.Columns.Add("txtRnShaftDriven");
                dt.Columns.Add("txtRwightDriven");
                dt.Columns.Add("txtHPowermover");
                dt.Columns.Add("txtRnWeight");
                dt.Columns.Add("cboHPumpPower_type");
                dt.Columns.Add("txtVEH_Volume");
                dt.Columns.Add("cboHGPSProvider");
                dt.Columns.Add("txtownTank");
                dt.Columns.Add("cboRTankMaterial");
                dt.Columns.Add("rblRValveType");
                dt.Columns.Add("rblRFuelType");
                dt.Columns.Add("nslot");
                dt.Columns.Add("sessionId");
                dt.Columns.Add("type_vendor");
                //ข้อมูลวัดน้ำ
                dt.Columns.Add("PLACE_WATER_MEASURE");
                dt.Columns.Add("SLAST_REQ_ID");
                dt.Columns.Add("DPREV_SERV");
                dt.Columns.Add("DWATEREXPIRE");
                dt.Rows.Add(
                    int.Parse(cmbCarcate.SelectedValue.ToString()),
                    DateTime.ParseExact(first_regis.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    txtHRegNo.Text.Trim(),
                    string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID,
                    cmbHsHolder.Text.ToString(),
                    cmbHRoute.SelectedValue.ToString(),
                    txtHsEngine.Text,
                    cmbHTru_Cate.SelectedValue.ToString(),
                    cboHLoadMethod.SelectedValue.ToString(),
                    txtHsModel.SelectedValue,
                    txtHsChasis.Text,
                    cboHsVibration.SelectedValue,
                    wheel.SelectedValue,
                    cboPressure.SelectedValue.ToString(),
                    txtRnShaftDriven.SelectedValue,
                    txtRwightDriven.SelectedValue,
                    txtHPowermover.Text.Trim().ToString(),
                    int.Parse(nullvalue(txtRnWeight.Text)),
                    cboHPumpPower_type.SelectedValue.ToString(),
                    txtVEH_Volume.SelectedValue.ToString(),
                    cboHGPSProvider.SelectedValue.ToString(),
                    TruckTank.Text.Trim().ToString(),
                    materialTank.SelectedValue.ToString(),
                    RadioToTextBox(rblRValveType.Text, txtRValveType.Text),
                    RadioToTextBox(rblRFuelType.Text.ToString(), txtRFuelType.Text),
                    ddl_addition.SelectedValue.ToString(),
                    Session["UserID"].ToString(),
                    type_vendor.SelectedValue.ToString(),

                    (radPlaceMV.Visible) ? radPlaceMV.SelectedValue : placewater.SelectedValue,

                    //MVDoc,
                    //string.Equals(MVDateStart, string.Empty) ? "" : DateTime.ParseExact(MVDateStart, "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    //string.Equals(MVDateEnd, string.Empty) ? "" : DateTime.ParseExact(MVDateEnd, "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)

                    txtcodewater.Text.Trim(),
                    string.Equals(scale_start.Text, string.Empty) ? "" : DateTime.ParseExact(scale_start.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    string.Equals(scale_end.Text, string.Empty) ? "" : DateTime.ParseExact(scale_end.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)

                    );

                break;
        }
        return dt;

    }
    #endregion
    #region Insurance
    protected DataTable DataInsurance()
    {
        string selectedValue = "";
        foreach (ListItem item in cbHolding.Items)
        {
            if (item.Selected)
            {
                selectedValue += "," + item.Value;
            }
        }
        if (selectedValue.Trim() != "")
            selectedValue = selectedValue.Substring(1);
        DataTable dt = new DataTable();
        dt.Columns.Add("SCOMPANY");
        dt.Columns.Add("SDETAIL");
        dt.Columns.Add("INSURE_BUDGET");
        dt.Columns.Add("INSURE_TYPE");
        dt.Columns.Add("HOLDING");
        dt.Columns.Add("NINSURE");
        dt.Columns.Add("START_DURATION");
        dt.Columns.Add("END_DURATION");
        dt.Rows.Add(
            company_name.Text.Trim(), detailinsurance.Text.Trim(), txtbudget.Text.Trim(), ddlTypeInsurance.SelectedValue
            , selectedValue, txtninsure.Text.Trim(),
            string.Equals(start_duration.Text, string.Empty) ? "" : DateTime.ParseExact(start_duration.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
            string.Equals(end_duration.Text, string.Empty) ? "" : DateTime.ParseExact(end_duration.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)
       );
        return dt;
    }
    #endregion
    #endregion
    #region ResponseMenu
    protected void ResponseMenu(object sender, EventArgs e)
    {
        if (string.Equals(CGROUP.ToString(), "1"))
        {
            Response.Redirect(string.IsNullOrEmpty(ViewState["GoBackTo"].ToString()) ? "approve_pk.aspx" : ViewState["GoBackTo"].ToString());
        }
        else
        {
            Response.Redirect("vendor_request.aspx");
        }
    }
    #endregion
    #region AddRequestLog
    private void AddRequestLog(string Car_flaot, string STTRUCKID)
    {
        switch (Car_flaot)
        {
            case "1":
                SystemFunction.AddToTREQ_DATACHANGE(V_TRUCKID, "T", Session["UserID"] + "", "0", registerno, VendorID, "Y", "");
                this.SendEmail(ConfigValue.EmailTruckEdit, STTRUCKID);
                break;
            case "0":
                this.SendEmail(ConfigValue.EmailStopCar, STTRUCKID);
                break;

        }
    }
    #endregion
    #region อีเมลล์
    private void SendEmail(int TemplateID, string STTRUCKID)
    {
        try
        {
            DataSet dsSendEmail = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("EMAIL");
            string EmailList = string.Empty;

            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                Subject = Subject.Replace("{regisNo}", registerno);
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailTruckEdit)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "truck_edit.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(cmbCarcate.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", Session["vendoraccountname"].ToString());
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //Body = Body.Replace("{Remark}", txtReport1Remark.Value.Trim());
                    dt.Rows.Add("");
                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body, "", "EmailTruckEdit", ColumnEmailName);

                }
                else if (TemplateID == ConfigValue.EmailTruckComment)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "truck_edit.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(cmbCarcate.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", NamveVendor);
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{Comment}", txtcomment.Text.ToString());
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body, "", "EmailTruckComment", ColumnEmailName);

                }
                else if (TemplateID == ConfigValue.EmailTruckNoApprove)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Truck_History_View.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(cmbCarcate.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", NamveVendor);
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{Comment}", txtcomment.Text.ToString());
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body, "", "EmailTruckNoApprove", ColumnEmailName);

                }
                else if (TemplateID == ConfigValue.EmailTruckApprove)
                {
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Truck_History_View.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(cmbCarcate.SelectedValue.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", NamveVendor);
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{Comment}", txtcomment.Text.ToString());
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //MailService.SendMail("raviwan.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body, "", "EmailTruckApprove", ColumnEmailName);
                }
                //ไม่มีอีเมลล์แจ้งผู้ขนส่งถอดรถ
                //else if (TemplateID == ConfigValue.EmailStopCar)
                //{
                //    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Truck_History_View.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(cmbCarcate.SelectedValue.ToString());
                //    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID);
                //    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                //    Body = Body.Replace("{contract}", NamveVendor);
                //    Body = Body.Replace("{regisNo}", registerno);
                //    Body = Body.Replace("{Comment}", txtcomment.Text.ToString());
                //    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                //    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, ConfigValue.DeliveryDepartmentCode, int.Parse(Session["UserID"].ToString()), VendorID, true), Subject, Body);
                //}

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region บันทึกLog
    private void SetLogToInsert(string regisText)
    {
        try
        {
            string newdata = string.Empty;
            string olddata = string.Empty;
            string headder = string.Empty;
            DataTable OldData = LOG.Instance.GetOldDataTruck(regisText);
            if (OldData.Rows.Count < 1)
                OldData.Rows.Add();
            if (string.Equals(cmbCarcate.SelectedValue.ToString(), "4"))
            {
                #region Log สำหรับ เซมิเทรลเลอร์
                if (cmbSemiHolder.Text != OldData.Rows[0]["SOWNER_NAME"].ToString())
                {
                    newdata += "ผู้ถือกรรมสิทธิ์::" + cmbSemiHolder.Text;
                    olddata += "ผู้ถือกรรมสิทธิ์::" + OldData.Rows[0]["SOWNER_NAME"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (cmbCarSemiSap.SelectedValue != OldData.Rows[0]["TRUCK_CATEGORY"].ToString())
                {
                    string newcmbCarSemiSap = cmbCarSemiSap.SelectedValue.ToString();
                    newdata += "ประเภทรถ(SAP)::" + cmbCarSemiSap.SelectedItem.Text;
                    try
                    {
                        cmbCarSemiSap.SelectedValue = OldData.Rows[0]["TRUCK_CATEGORY"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "ประเภทรถ(SAP)::" + cmbCarSemiSap.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cmbCarSemiSap.SelectedValue = newcmbCarSemiSap;
                }
                if (txtSemiChasis.Text != OldData.Rows[0]["SCHASIS"].ToString())
                {
                    newdata += "หมายเลขแซสซีย์::" + txtSemiChasis.Text;
                    olddata += "หมายเลขแซสซีย์::" + OldData.Rows[0]["SCHASIS"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (txtsemimodal.SelectedValue != OldData.Rows[0]["SBRAND"].ToString())
                {
                    string newtxtsemimodal = txtsemimodal.SelectedValue.ToString();
                    newdata += "รุ่น ::" + txtsemimodal.SelectedItem.Text;
                    try
                    {
                        txtsemimodal.SelectedValue = OldData.Rows[0]["SBRAND"].ToString();
                    }
                    catch
                    {


                    }

                    olddata += "รุ่น ::" + txtsemimodal.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    txtsemimodal.SelectedValue = newtxtsemimodal;
                }
                if (SemiWheel.SelectedValue != OldData.Rows[0]["NWHEELS"].ToString())
                {
                    string newsemiwheel = SemiWheel.SelectedValue.ToString();
                    newdata += "จำนวนล้อ::" + SemiWheel.SelectedItem.Text;
                    try
                    {
                        SemiWheel.SelectedValue = OldData.Rows[0]["NWHEELS"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "จำนวนล้อ::" + SemiWheel.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    SemiWheel.SelectedValue = newsemiwheel;

                }
                if (txtSemiWeight.Text != OldData.Rows[0]["NWEIGHT"].ToString())
                {
                    newdata += "น้ำหนักรถ::" + txtSemiWeight.Text;
                    olddata += "น้ำหนักรถ::" + OldData.Rows[0]["NWEIGHT"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (txtSemiShaftDriven.SelectedValue != OldData.Rows[0]["NSHAFTDRIVEN"].ToString())
                {
                    string newtxtSemiShaftDriven = txtSemiShaftDriven.SelectedValue.ToString();
                    newdata += "จำนวนเพลารับน้ำหนัก ::" + txtSemiShaftDriven.SelectedItem.Text;
                    try
                    {
                        txtSemiShaftDriven.SelectedValue = OldData.Rows[0]["NSHAFTDRIVEN"].ToString();
                    }
                    catch
                    {


                    }

                    olddata += "จำนวนเพลารับน้ำหนัก ::" + txtSemiShaftDriven.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    txtSemiShaftDriven.SelectedValue = newtxtSemiShaftDriven;
                }
                if (txtownTank.Text != OldData.Rows[0]["STANK_MAKER"].ToString())
                {

                    newdata += "น้ำหนักรถ::" + txtownTank.Text;
                    olddata += "น้ำหนักรถ::" + OldData.Rows[0]["STANK_MAKER"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (cboRTankMaterial.SelectedValue != OldData.Rows[0]["STANK_MATERAIL"].ToString())
                {
                    string newcboRTankMaterial = cboRTankMaterial.SelectedValue.ToString();
                    newdata += "จำนวนเพลารับน้ำหนัก ::" + cboRTankMaterial.SelectedItem.Text;
                    try
                    {
                        cboRTankMaterial.SelectedValue = OldData.Rows[0]["STANK_MATERAIL"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "จำนวนเพลารับน้ำหนัก ::" + cboRTankMaterial.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cboRTankMaterial.SelectedValue = newcboRTankMaterial;
                }
                if (semifirst_regis.Text != (string.IsNullOrEmpty(OldData.Rows[0]["DREGISTER"].ToString()) ? "" : Convert.ToDateTime(OldData.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
                {
                    newdata += "วันที่จดทะเบียนครั้งแรก:: " + semifirst_regis.Text;
                    olddata += "วันที่จดทะเบียนครั้งแรก:: " + (string.IsNullOrEmpty(OldData.Rows[0]["DREGISTER"].ToString()) ? "" : Convert.ToDateTime(OldData.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (rblSemiPumpPower.SelectedValue != OldData.Rows[0]["PUMPPOWER"].ToString())
                {
                    string newrblSemiPumpPower = rblSemiPumpPower.SelectedValue.ToString();
                    newdata += "จำนวนเพลารับน้ำหนัก ::" + rblSemiPumpPower.SelectedItem.Text;
                    try
                    {
                        rblSemiPumpPower.SelectedValue = OldData.Rows[0]["PUMPPOWER"].ToString();
                    }
                    catch
                    {


                    }

                    olddata += "จำนวนเพลารับน้ำหนัก ::" + rblSemiPumpPower.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    rblSemiPumpPower.SelectedValue = newrblSemiPumpPower;
                }
                #endregion
            }
            else
            {
                #region Log สำหรับรถเดี่ยวและหัวลาก


                if (cmbHsHolder.Text != OldData.Rows[0]["SOWNER_NAME"].ToString())
                {
                    newdata += "ผู้ถือกรรมสิทธิ์::" + cmbHsHolder.Text;
                    olddata += "ผู้ถือกรรมสิทธิ์::" + OldData.Rows[0]["SOWNER_NAME"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (cmbHRoute.SelectedValue != OldData.Rows[0]["ROUTE"].ToString())
                {
                    string NewRoute = cmbHRoute.SelectedValue.ToString();
                    newdata += "Route::" + cmbHRoute.SelectedItem.Text;
                    try
                    {
                        cmbHRoute.SelectedValue = OldData.Rows[0]["ROUTE"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "Route::" + cmbHRoute.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cmbHRoute.SelectedValue = NewRoute;
                }
                if (txtHsEngine.Text != OldData.Rows[0]["SENGINE"].ToString())
                {

                    newdata += "หมายเลขเครื่องยนต์::" + txtHsEngine.Text;
                    olddata += "หมายเลขเครื่องยนต์::" + txtHsEngine.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (cmbHTru_Cate.SelectedValue != OldData.Rows[0]["TRUCK_CATEGORY"].ToString())
                {
                    string newSap = cmbHTru_Cate.SelectedValue.ToString();
                    newdata += "ประเภทรถ(SAP)::" + cmbHTru_Cate.SelectedItem.Text;
                    try
                    {
                        cmbHTru_Cate.SelectedValue = OldData.Rows[0]["TRUCK_CATEGORY"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "ประเภทรถ(SAP)::" + cmbHTru_Cate.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cmbHTru_Cate.SelectedValue = newSap;
                }
                if (txtHsModel.SelectedValue != OldData.Rows[0]["SBRAND"].ToString())
                {
                    string newmodel = txtHsModel.SelectedValue.ToString();
                    newdata += "รุ่น ::" + txtHsModel.SelectedItem.Text;
                    try
                    {
                        txtHsModel.SelectedValue = OldData.Rows[0]["SBRAND"] + string.Empty;
                    }
                    catch
                    {

                    }
                    olddata += "รุ่น ::" + txtHsModel.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    txtHsModel.SelectedValue = newmodel;
                }
                if (txtHsChasis.Text != OldData.Rows[0]["SCHASIS"].ToString())
                {
                    newdata += "หมายเลขแซสซีย์::" + txtHsChasis.Text;
                    olddata += "หมายเลขแซสซีย์::" + OldData.Rows[0]["SCHASIS"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (cboHsVibration.SelectedValue != OldData.Rows[0]["SVIBRATION"].ToString())
                {
                    string newcbohsvibration = cboHsVibration.SelectedValue.ToString();
                    newdata += "ระบบกันสะเทือน ::" + cboHsVibration.SelectedItem.Text;
                    try
                    {
                        cboHsVibration.SelectedValue = OldData.Rows[0]["SVIBRATION"].ToString();
                    }
                    catch
                    {

                        throw;
                    }
                    olddata += "ระบบกันสะเทือน ::" + cboHsVibration.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cboHsVibration.SelectedValue = newcbohsvibration;
                }
                if (wheel.SelectedValue != OldData.Rows[0]["NWHEELS"].ToString())
                {
                    string newwheel = wheel.SelectedValue.ToString();
                    newdata += "จำนวนล้อ::" + wheel.SelectedItem.Text;
                    try
                    {
                        wheel.SelectedValue = OldData.Rows[0]["NWHEELS"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "จำนวนล้อ::" + wheel.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    wheel.SelectedValue = newwheel;
                }
                if (cboPressure.SelectedValue != OldData.Rows[0]["MATERIALOFPRESSURE"].ToString())
                {
                    string newcbopressure = cboPressure.SelectedValue.ToString();
                    newdata += "Pressure ของปั้ม Power ::" + cboPressure.SelectedItem.Text;
                    try
                    {
                        cboPressure.SelectedValue = OldData.Rows[0]["MATERIALOFPRESSURE"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "Pressure ของปั้ม Power ::" + cboPressure.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cboPressure.SelectedValue = newcbopressure;


                }
                if (txtRnShaftDriven.SelectedValue != OldData.Rows[0]["NSHAFTDRIVEN"].ToString())
                {
                    string newtxtRnShaftDriven = txtRnShaftDriven.SelectedValue.ToString();
                    newdata += "จำนวนเพลาขับเคลื่อน::" + txtRnShaftDriven.SelectedItem.Text;
                    try
                    {
                        txtRnShaftDriven.SelectedValue = OldData.Rows[0]["NSHAFTDRIVEN"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "จำนวนเพลาขับเคลื่อน::" + txtRnShaftDriven.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    txtRnShaftDriven.SelectedValue = newtxtRnShaftDriven;
                }
                if (txtRnWeight.Text != OldData.Rows[0]["NWEIGHT"].ToString())
                {
                    newdata += "น้ำหนักรถ::" + txtRnWeight.Text;
                    olddata += "น้ำหนักรถ::" + OldData.Rows[0]["NWEIGHT"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (txtRwightDriven.SelectedValue != OldData.Rows[0]["RWIGHTDRIVEN"].ToString())
                {
                    string newtxtRwightDriven = txtRwightDriven.SelectedValue.ToString();
                    newdata += "จำนวนเพลารับน้ำหนัก::" + txtRwightDriven.SelectedItem.Text;
                    try
                    {
                        txtRwightDriven.SelectedValue = OldData.Rows[0]["RWIGHTDRIVEN"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "จำนวนเพลารับน้ำหนัก::" + txtRwightDriven.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    txtRwightDriven.SelectedValue = newtxtRwightDriven;

                }
                if (cboHPumpPower_type.SelectedValue != OldData.Rows[0]["PUMPPOWER_TYPE"].ToString())
                {
                    string newcbohpumppower_type = cboHPumpPower_type.SelectedValue.ToString();
                    newdata += "ชนิดของปั้ม Power::" + cboHPumpPower_type.SelectedItem.Text;
                    try
                    {
                        cboHPumpPower_type.SelectedValue = OldData.Rows[0]["PUMPPOWER_TYPE"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "ชนิดของปั้ม Power::" + cboHPumpPower_type.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cboHPumpPower_type.SelectedValue = newcbohpumppower_type;
                }
                if (txtVEH_Volume.SelectedValue != OldData.Rows[0]["VOL_UOM"].ToString())
                {
                    string newVEH_Volume = txtVEH_Volume.SelectedValue.ToString();
                    newdata += "หน่วยความจุเชื้อเพลิง::" + txtVEH_Volume.SelectedItem.Text;
                    try
                    {
                        txtVEH_Volume.SelectedValue = OldData.Rows[0]["VOL_UOM"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "หน่วยความจุเชื้อเพลิง::" + txtVEH_Volume.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    txtVEH_Volume.SelectedValue = newVEH_Volume;
                }
                if (cboHGPSProvider.SelectedValue != OldData.Rows[0]["GPS_SERVICE_PROVIDER"].ToString())
                {
                    string newcbohgpsprovider = cboHGPSProvider.SelectedValue.ToString();
                    newdata += "บริษัททีให้บริการ GPS::" + cboHGPSProvider.SelectedItem.Text;
                    try
                    {
                        cboHGPSProvider.SelectedValue = OldData.Rows[0]["GPS_SERVICE_PROVIDER"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "บริษัททีให้บริการ GPS::" + cboHGPSProvider.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    cboHGPSProvider.SelectedValue = newcbohgpsprovider;

                }
                if (TruckTank.Text != OldData.Rows[0]["STANK_MAKER"].ToString())
                {
                    newdata += "บริษัทผู้ผลิตถัง ::" + TruckTank.Text;
                    olddata += "บริษัทผู้ผลิตถัง ::" + OldData.Rows[0]["STANK_MAKER"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (materialTank.SelectedValue != OldData.Rows[0]["STANK_MATERAIL"].ToString())
                {
                    string newmaterialTank = materialTank.SelectedValue.ToString();
                    newdata += "วัสดุที่ผลิตถัง ::" + materialTank.SelectedItem.Text;
                    try
                    {
                        materialTank.SelectedValue = OldData.Rows[0]["STANK_MATERAIL"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "วัสดุที่ผลิตถัง ::" + materialTank.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    materialTank.SelectedValue = newmaterialTank;
                }
                if (first_regis.Text != (string.IsNullOrEmpty(OldData.Rows[0]["DREGISTER"].ToString()) ? "" : Convert.ToDateTime(OldData.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
                {
                    newdata += "วันที่จดทะเบียนครั้งแรก:: " + first_regis.Text;
                    olddata += "วันที่จดทะเบียนครั้งแรก:: " + (string.IsNullOrEmpty(OldData.Rows[0]["DREGISTER"].ToString()) ? "" : Convert.ToDateTime(OldData.Rows[0]["DREGISTER"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                if (rblRValveType.Text != OldData.Rows[0]["VALVETYPE"].ToString())
                {
                    newdata += "ชนิดของวาล์วใต้ท้อง::" + rblRValveType.Text;
                    olddata += "ชนิดของวาล์วใต้ท้อง::" + OldData.Rows[0]["VALVETYPE"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }

                if (rblRFuelType.SelectedValue != OldData.Rows[0]["FUELTYPE"].ToString())
                {
                    string newrblrfueltype = rblRFuelType.SelectedValue.ToString();
                    newdata += "ประเภทของเชื้อเพลิง::" + rblRFuelType.SelectedItem.Text;
                    try
                    {
                        rblRFuelType.SelectedValue = OldData.Rows[0]["FUELTYPE"].ToString();
                    }
                    catch
                    {


                    }
                    olddata += "ประเภทของเชื้อเพลิง::" + rblRFuelType.SelectedItem.Text;
                    newdata += "<br/>";
                    olddata += "<br/>";
                    rblRFuelType.SelectedValue = newrblrfueltype;
                }
                if (txtHPowermover.Text != OldData.Rows[0]["POWERMOVER"].ToString())
                {
                    newdata += "กำลังเครื่องยนต์ ::" + txtHPowermover.Text;
                    olddata += "กำลังเครื่องยนต์ ::" + OldData.Rows[0]["POWERMOVER"].ToString();
                    newdata += "<br/>";
                    olddata += "<br/>";
                }
                #endregion
            }

            if (type_vendor.Text != OldData.Rows[0]["ISUSE"].ToString())
            {
                newdata += "สถานะรถภายในระบบ::" + (string.Equals(type_vendor.Text.ToString(), "1") ? "รถในสังกัด" : "ปลดรถในสังกัด(ขายรถ)");
                olddata += "สถานะรถภายในระบบ::" + (string.Equals(OldData.Rows[0]["ISUSE"].ToString(), "1") ? "รถในสังกัด" : "ปลดรถในสังกัด(ขายรถ)");
                newdata += "<br/>";
                olddata += "<br/>";
            }
            #region Log ประกัน
            DataTable OldDataInsurance = LOG.Instance.GetOldDataTruckInsurance(V_TRUCKID);
            if (OldDataInsurance.Rows.Count < 1)
                OldDataInsurance.Rows.Add();
            if (company_name.Text != OldDataInsurance.Rows[0]["SCOMPANY"].ToString())
            {
                newdata += "บริษัทประกันภัย::" + company_name.Text;
                olddata += "บริษัทประกันภัย::" + OldDataInsurance.Rows[0]["SCOMPANY"].ToString();
                newdata += "<br/>";
                olddata += "<br/>";
            }
            if (start_duration.Text != (string.IsNullOrEmpty(OldDataInsurance.Rows[0]["START_DURATION"].ToString()) ? "" : Convert.ToDateTime(OldDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
            {
                newdata += "ระยะเวลาอายุ พรบ. ::" + start_duration.Text;
                olddata += "ระยะเวลาอายุ พรบ. ::" + (string.IsNullOrEmpty(OldDataInsurance.Rows[0]["START_DURATION"].ToString()) ? "" : Convert.ToDateTime(OldDataInsurance.Rows[0]["START_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                newdata += "<br/>";
                olddata += "<br/>";
            }
            if (end_duration.Text != (string.IsNullOrEmpty(OldDataInsurance.Rows[0]["END_DURATION"].ToString()) ? "" : Convert.ToDateTime(OldDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
            {
                newdata += "ถึงวันที่::" + end_duration.Text;
                olddata += "ถึงวันที่::" + (string.IsNullOrEmpty(OldDataInsurance.Rows[0]["END_DURATION"].ToString()) ? "" : Convert.ToDateTime(OldDataInsurance.Rows[0]["END_DURATION"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                newdata += "<br/>";
                olddata += "<br/>";
            }
            if (txtbudget.Text != OldDataInsurance.Rows[0]["INSURE_BUDGET"].ToString())
            {
                newdata += "ทุนประกัน.::" + txtbudget.Text;
                olddata += "ทุนประกัน.::" + OldDataInsurance.Rows[0]["INSURE_BUDGET"].ToString();
                newdata += "<br/>";
                olddata += "<br/>";
            }
            if (txtninsure.Text != OldDataInsurance.Rows[0]["NINSURE"].ToString())
            {
                newdata += "เบี้ยประกันต่อปี.::" + txtninsure.Text;
                olddata += "เบี้ยประกันต่อปี.::" + OldDataInsurance.Rows[0]["NINSURE"].ToString();
                newdata += "<br/>";
                olddata += "<br/>";
            }
            #endregion
            if (!string.Equals(cmbCarcate.SelectedValue.ToString(), "3"))
            {
                DataTable OldCapacity = LOG.Instance.GetOldDataCapacity(V_TRUCKID);
                DataTable newDatacapacity = NewDataCapacity;
                var dataOld = OldCapacity.AsEnumerable().Select(it => new { NCOMPARTNO = it["NCOMPARTNO"] + string.Empty, NPANLEVEL = it["NPANLEVEL"] + string.Empty, NCAPACITY = it["NCAPACITY"] + string.Empty }).ToList();
                var datNew = newDatacapacity.AsEnumerable().Select(it => new { NCOMPARTNO = it["NCOMPARTNO"] + string.Empty, NPANLEVEL = it["NPANLEVEL"] + string.Empty, NCAPACITY = it["NCAPACITY"] + string.Empty }).ToList();

                var rows = (from n in datNew
                            where !(dataOld.Contains(n))
                            select n);

                if (rows.Any())
                {
                    //diff = rows.CopyToDataTable();
                    foreach (var item in rows)
                    {
                        newdata += "CompartmentNo::" + item.NCOMPARTNO.ToString() + "ระดับแป้นที่" + item.NPANLEVEL.ToString() + "::" + item.NCAPACITY.ToString();
                        newdata += "<br/>";
                        for (int i = 0; i < OldCapacity.Rows.Count; i++)
                        {
                            if (string.Equals(OldCapacity.Rows[i]["NCOMPARTNO"].ToString(), item.NCOMPARTNO.ToString()) && string.Equals(OldCapacity.Rows[i]["NPANLEVEL"].ToString(), item.NPANLEVEL.ToString()))
                            {
                                olddata += "CompartmentNo::" + item.NCOMPARTNO.ToString() + "ระดับแป้นที่" + OldCapacity.Rows[i]["NPANLEVEL"].ToString() + "::" + OldCapacity.Rows[i]["NCAPACITY"].ToString();
                                olddata += "<br/>";
                            }
                        }
                    }
                }
            }

            if (!string.Equals(newdata.ToString(), olddata.ToString()) && !string.IsNullOrEmpty(newdata))
            {
                headder = "ได้มีการเปลี่ยนแปลงข้อมูลดังนี้";
                #region บันทึก Log
                LOG.Instance.SaveLog(headder, regisText, Session["UserID"].ToString(), newdata, olddata);
                #endregion
            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }

    }
    #endregion
    #region รข. Approve
    private void ApproveFunction()
    {
        try
        {
            #region บันทึก Log
            LOG.Instance.SaveLog("รข.อนุมัติ ทะเบียน", regisText, Session["UserID"].ToString(), "<span class=\"green\">" + registerno + "</span>", "");
            #endregion
            //เพิ่มรถใน SAP ถ้ามีให้ UPDATE ไม่มีให้ CREATE
            this.SetmodifirySap(regisText);
            this.CheckApproveDocNoTruck();
            if (!cmbCarcate.SelectedValue.ToString().Equals("3"))
            {
                this.UpdateNTotal();
            }
            SystemFunction.AddToTREQ_DATACHANGE(V_TRUCKID, "T", Session["UserID"] + "", "1", registerno, VendorID, "Y", txtcomment.Text.ToString());
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void UpdateNTotal()
    {
        /****When pttict Approve then Change NTOTAL (CLASS GROUP) ****************/
        this.ValidateCapacity(int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));
        string TRUCKID = V_TRUCKID;
        //เก็บค่าช่องเติม
        NewDataCapacity = this.DataCapacity(int.Parse(capacityCell.SelectedValue));
        int nTotalCapacity = CalNTotal(NewDataCapacity);
        /***************************************************/
        CarBLL.Instance.UpdateNTotal(TRUCKID, nTotalCapacity);
    }

    private int CalNTotal(DataTable NewDataCapacity)
    {
        try
        {
            int nTotal = 0;
            if (NewDataCapacity != null && NewDataCapacity.Rows != null && NewDataCapacity.Rows.Count > 0)
            {
                var capacityRows = NewDataCapacity.AsEnumerable().Where(it => Convert.ToInt32(it["NPANLEVEL"]) == 1);
                if (capacityRows != null && capacityRows.Any())
                {
                    foreach (DataRow row in capacityRows)
                    {
                        nTotal += Convert.ToInt32(row["NCAPACITY"]);
                    }
                }
            }
            return nTotal;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region SetPermissionControl
    public void SetPermissionControl(bool isActive, bool isContract)
    {
        if (isActive)
        {
            if (CGROUP == "0")
            {
                #region ผู้ขนส่ง
                if (isContract)
                {
                    #region อนุมัติแล้ว อยู่ในสัญญา
                    //หัวลาก
                    txtHRegNo.ReadOnly = true;
                    txtHsEngine.ReadOnly = true;
                    txtHsChasis.ReadOnly = true;
                    txtRnShaftDriven.Enabled = false;
                    txtRwightDriven.Enabled = false;
                    first_regis.Enabled = false;
                    first_regis.ReadOnly = true;

                    //หางลาก
                    txtSemiNo.ReadOnly = true;
                    txtSemiChasis.ReadOnly = true;
                    txtSemiShaftDriven.Enabled = false;
                    semifirst_regis.ReadOnly = true;
                    semifirst_regis.Enabled = false;

                    //ข้อมูลวัดน้ำ
                    placewater.Enabled = false;
                    txtcodewater.Enabled = false;
                    scale_start.Enabled = false;
                    scale_end.Enabled = false;

                    //ข้อมูลช่องเติม
                    Table6.Enabled = false;

                    //ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
                    txtcomment.ReadOnly = true;
                    #endregion
                }
                else
                {
                    #region อนุมัติแล้ว ไม่อยู่ในสัญญา
                    //หัวลาก
                    txtHRegNo.ReadOnly = true;
                    txtHsEngine.ReadOnly = true;
                    txtHsChasis.ReadOnly = true;
                    txtRnShaftDriven.Enabled = true;
                    txtRwightDriven.Enabled = true;
                    first_regis.Enabled = false;
                    first_regis.ReadOnly = true;

                    //หางลาก
                    txtSemiNo.ReadOnly = true;
                    txtSemiChasis.ReadOnly = true;
                    txtSemiShaftDriven.Enabled = true;
                    semifirst_regis.ReadOnly = true;
                    semifirst_regis.Enabled = false;

                    //ข้อมูลวัดน้ำ
                    placewater.Enabled = false;
                    txtcodewater.Enabled = false;
                    scale_start.Enabled = false;
                    scale_end.Enabled = false;

                    //ข้อมูลช่องเติม
                    Table6.Enabled = true;

                    //ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
                    txtcomment.ReadOnly = true;
                    #endregion
                }
                #endregion
            }
            else
            {
                #region ปตท.
                if (isContract)
                {
                    #region อนุมัติแล้ว อยู่ในสัญญา
                    //หัวลาก
                    txtHRegNo.ReadOnly = true;
                    txtHsEngine.ReadOnly = true;
                    txtHsChasis.ReadOnly = true;
                    txtRnShaftDriven.Enabled = true;
                    txtRwightDriven.Enabled = true;
                    first_regis.Enabled = false;
                    first_regis.ReadOnly = true;

                    //หางลาก
                    txtSemiNo.ReadOnly = true;
                    txtSemiChasis.ReadOnly = true;
                    txtSemiShaftDriven.Enabled = true;
                    semifirst_regis.ReadOnly = true;
                    semifirst_regis.Enabled = false;

                    //ข้อมูลวัดน้ำ
                    placewater.Enabled = false;
                    txtcodewater.Enabled = false;
                    scale_start.Enabled = false;
                    scale_end.Enabled = false;

                    //ข้อมูลช่องเติม
                    Table6.Enabled = false;

                    //ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
                    //txtcomment.ReadOnly = true;
                    #endregion
                }
                else
                {
                    #region อนุมัติแล้ว ไม่อยู่ในสัญญา
                    //หัวลาก
                    txtHRegNo.ReadOnly = true;
                    txtHsEngine.ReadOnly = true;
                    txtHsChasis.ReadOnly = true;
                    txtRnShaftDriven.Enabled = true;
                    txtRwightDriven.Enabled = true;
                    first_regis.Enabled = false;
                    first_regis.ReadOnly = true;

                    //หางลาก
                    txtSemiNo.ReadOnly = true;
                    txtSemiChasis.ReadOnly = true;
                    txtSemiShaftDriven.Enabled = true;
                    semifirst_regis.ReadOnly = true;
                    semifirst_regis.Enabled = false;

                    //ข้อมูลวัดน้ำ
                    placewater.Enabled = true;
                    txtcodewater.Enabled = true;
                    scale_start.Enabled = true;
                    scale_end.Enabled = true;

                    //ข้อมูลช่องเติม
                    Table6.Enabled = true;

                    //ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
                    //txtcomment.ReadOnly = true;
                    #endregion
                }
                #endregion
            }
        }
        else
        {
            if (CGROUP == "0")
            {
                #region ผู้ขนส่ง  สถานะไม่อนุมัติ
                //หัวลาก
                txtHRegNo.ReadOnly = false;
                txtHsEngine.ReadOnly = false;
                txtHsChasis.ReadOnly = false;
                txtRnShaftDriven.Enabled = true;
                txtRwightDriven.Enabled = true;
                first_regis.Enabled = true;
                first_regis.ReadOnly = false;

                //หางลาก
                txtSemiNo.ReadOnly = false;
                txtSemiChasis.ReadOnly = false;
                txtSemiShaftDriven.Enabled = true;
                semifirst_regis.Enabled = true;
                semifirst_regis.ReadOnly = false;

                //ข้อมูลวัดน้ำ
                placewater.Enabled = false;
                txtcodewater.Enabled = false;
                scale_start.Enabled = false;
                scale_end.Enabled = false;

                //ข้อมูลช่องเติม
                Table6.Enabled = true;

                //ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
                txtcomment.ReadOnly = true;
                #endregion
            }
            else
            {
                #region ปตท สถานะไม่อนุมัติ
                //หัวลาก
                txtHRegNo.ReadOnly = false;
                txtHsEngine.ReadOnly = false;
                txtHsChasis.ReadOnly = false;
                txtRnShaftDriven.Enabled = true;
                txtRwightDriven.Enabled = true;
                first_regis.Enabled = true;
                first_regis.ReadOnly = false;

                //หางลาก
                txtSemiNo.ReadOnly = false;
                txtSemiChasis.ReadOnly = false;
                txtSemiShaftDriven.Enabled = true;
                semifirst_regis.Enabled = true;
                semifirst_regis.ReadOnly = false;

                //ข้อมูลวัดน้ำ
                placewater.Enabled = true;
                txtcodewater.Enabled = true;
                scale_start.Enabled = true;
                scale_end.Enabled = true;

                //ข้อมูลช่องเติม
                Table6.Enabled = true;

                //ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม
                txtcomment.ReadOnly = false;
                #endregion
            }
        }


    }
    #endregion
    protected void btnUploadTPN_Click(object sender, EventArgs e)
    {
        this.StartUploadTPN();
    }

    private void StartUploadTPN()
    {
        try
        {
            if (fileUploadTPN.HasFile)
            {

                if (string.Equals(txtDocUploadTPN.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน หมายเลขเอกสาร");

                string FileNameUser = fileUploadTPN.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                if (!string.Equals(txtSuggestFileNameTPN.Text.Trim(), System.IO.Path.GetFileNameWithoutExtension(FileNameUser)))
                    throw new Exception("ชื่อไฟล์ไม่ถูกต้อง ตามที่กำหนด");

                if (txtdateStartTPN.Enabled)
                {
                    if (string.Equals(txtdateStartTPN.Text.Trim(), string.Empty))
                        throw new Exception("กรุณาป้อน วันที่เอกสาร เริ่มต้น");
                }

                if (txtdateEndTPN.Enabled)
                {
                    if (string.Equals(txtdateEndTPN.Text.Trim(), string.Empty))
                        throw new Exception("กรุณาป้อน วันที่เอกสาร สิ้นสุด");
                }

                this.ValidateUploadFileTPN(System.IO.Path.GetExtension(FileNameUser), fileUploadTPN.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadTPN.SaveAs(Path + "\\" + FileNameSystem);

                string Year = string.Empty;
                if (ddlYearTPN.Items.Count > 0)
                    Year = ddlYearTPN.SelectedValue;

                string DateStart = string.Empty;
                if (!string.Equals(txtdateStartTPN.Text.Trim(), string.Empty))
                    DateStart = txtdateStartTPN.Text.Trim();

                string DateStop = string.Empty;
                if (!string.Equals(txtdateEndTPN.Text.Trim(), string.Empty))
                    DateStop = txtdateEndTPN.Text.Trim();

                dtUploadTPN.Rows.Add(ddlUploadTypeTPN.SelectedValue, ddlUploadTypeTPN.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem, DateStart, DateStop, txtDocUploadTPN.Text.Trim(), Year);
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);
            }

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFileTPN(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFileTPN_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string FullPath = dgvUploadFileTPN.DataKeys[e.RowIndex]["FULLPATH"].ToString();
            string FileName = dgvUploadFileTPN.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
            this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvUploadFileTPN_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadTPN.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

            this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvUploadFileTPN_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlUploadType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(cmbCarcate.SelectedValue, "4"))
            {
                if (string.Equals(txtHRegNo.Text.Trim(), string.Empty))
                {
                    ddlUploadType.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }

                if (ddlUploadType.SelectedIndex < 1)
                    txtSuggestFileName.Text = string.Empty;
                else
                    txtSuggestFileName.Text = txtHRegNo.Text.Trim() + "_" + ddlUploadType.SelectedItem.ToString();
            }
            else
            {
                if (string.Equals(txtSemiNo.Text.Trim(), string.Empty))
                {
                    ddlUploadType.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }

                if (ddlUploadType.SelectedIndex < 1)
                    txtSuggestFileName.Text = string.Empty;
                else
                    txtSuggestFileName.Text = txtSemiNo.Text.Trim() + "_" + ddlUploadType.SelectedItem.ToString();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckRequestFile(GridView dgvRequestFile, DataTable dtRequestFile, DataTable dtUpload)
    {
        try
        {
            string UploadID = string.Empty;
            CheckBox chk = new CheckBox();

            for (int j = 0; j < dtRequestFile.Rows.Count; j++)
            {
                chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                if (chk != null)
                    chk.Checked = false;
            }

            for (int i = 0; i < dtUpload.Rows.Count; i++)
            {
                UploadID = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (string.Equals(UploadID, dtRequestFile.Rows[j]["UPLOAD_ID"].ToString()))
                    {
                        chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                        if (chk != null)
                            chk.Checked = true;
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlUploadTypeTPN_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(cmbCarcate.SelectedValue, "4"))
            {
                if (string.Equals(txtHRegNo.Text.Trim(), string.Empty))
                {
                    ddlUploadTypeTPN.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }
            }
            else
            {
                if (string.Equals(txtSemiNo.Text.Trim(), string.Empty))
                {
                    ddlUploadTypeTPN.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }
            }

            this.ClearUploadTPN();

            if (ddlUploadTypeTPN.SelectedIndex > 0)
            {
                if (!string.Equals(cmbCarcate.SelectedValue, "4"))
                    txtSuggestFileNameTPN.Text = txtHRegNo.Text.Trim() + "_" + ddlUploadTypeTPN.SelectedItem.ToString();
                else
                    txtSuggestFileNameTPN.Text = txtSemiNo.Text.Trim() + "_" + ddlUploadTypeTPN.SelectedItem.ToString();

                DataTable dtYear = new DataTable();
                dtYear.Columns.Add("YEAR");
                dtYear.Columns.Add("YEAR_DISPLAY");

                if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "1"))
                {//ธพ.น.2
                    dtYear.Rows.Add(DateTime.Now.ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.ToString("yyyy", new CultureInfo("en-US")));
                    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                    ddlYearTPN.SelectedIndex = 0;
                    ddlYearTPN.Enabled = true;

                    txtdateStartTPN.Text = "01/01/" + ddlYearTPN.SelectedValue;
                    txtdateEndTPN.Text = "31/12/" + ddlYearTPN.SelectedValue;
                }
                else if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "2"))
                {//ธพ.น.3
                    dtYear.Rows.Add(DateTime.Now.ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.ToString("yyyy", new CultureInfo("en-US")));
                    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                    ddlYearTPN.SelectedIndex = 0;
                    ddlYearTPN.Enabled = true;
                }
                else
                {
                    txtdateStartTPN.Enabled = true;
                    txtdateEndTPN.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ClearUploadTPN()
    {
        try
        {
            txtSuggestFileNameTPN.Text = string.Empty;
            txtDocUploadTPN.Text = string.Empty;

            ddlYearTPN.Items.Clear();
            ddlYearTPN.Enabled = false;

            txtdateStartTPN.Text = string.Empty;
            txtdateStartTPN.Enabled = false;

            txtdateEndTPN.Text = string.Empty;
            txtdateEndTPN.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlYearTPN_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            txtdateStartTPN.Text = "01/01/" + ddlYearTPN.SelectedValue;
            txtdateEndTPN.Text = "31/12/" + ddlYearTPN.SelectedValue;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvMV_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvMV.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string FileName = dgvMV.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName + Path.GetExtension(FullPath));
    }
}

class SapTU
{
    public string regisnumber { get; set; }


    public string SapUpdateTU()
    {
        SAP_UPDATE_TU Sap_update_tu = new SAP_UPDATE_TU();
        Sap_update_tu.VEH_NO = this.regisnumber;
        return Sap_update_tu.UDT_TU_SYNC_OUT_SI();
    }
    public string SapCreateTU()
    {
        SAP_CREATE_TU Sap_Create_tu = new SAP_CREATE_TU();
        Sap_Create_tu.sVehicle = this.regisnumber;
        return Sap_Create_tu.CRT_TU_SYNC_OUT_SI();
    }

}
class TruckDoccument
{
    public string Doc_TRUCKID { get; set; }
    public bool CheckDocComplate()
    {
        DataTable Dt = CarBLL.Instance.CheckDocTruck(this.Doc_TRUCKID);
        if (Dt.Rows.Count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}