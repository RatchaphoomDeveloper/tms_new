﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using DevExpress.Web.ASPxUploadControl;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Configuration;


public partial class vendor_employee_add_p4 : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    //static List<DeleteItem> DeleteGrid = new List<DeleteItem>();
    //อับโลหดไฟล์ไปที่Te,p
    const string TempDirectory = "UploadFile/EmpployeeDoc_TEMP/Temp/{0}/{2}/{1}/";
    const string TempDirectory2 = "UploadFile/EmpployeeDoc_TEMP/Temp/{0}/{2}/{1}/";
    const string TempDirectory3 = "UploadFile/EmpployeeDoc_TEMP/Temp/{0}/{2}/{1}/";
    const string TempDirectory4 = "UploadFile/EmpployeeDoc_TEMP/Temp/{0}/{2}/{1}/";
    const string TempDirectory5 = "UploadFile/EmpployeeDoc_TEMP/Temp/{0}/{2}/{1}/";
    //อับโลหดไฟล์ไปที่Saver
    const string SaverDirectory = "UploadFile/EmpployeeDoc_TEMP/IDcard/{0}/{2}/{1}/";
    const string SaverDirectory2 = "UploadFile/EmpployeeDoc_TEMP/Registration/{0}/{2}/{1}/";
    const string SaverDirectory3 = "UploadFile/EmpployeeDoc_TEMP/Driverlicense/{0}/{2}/{1}/";
    const string SaverDirectory4 = "UploadFile/EmpployeeDoc_TEMP/Other/{0}/{2}/{1}/";
    const string SaverDirectory5 = "UploadFile/EmpployeeDoc_TEMP/PictureEMP/{0}/{2}/{1}/";
    //อับโลหดไฟล์History
    const string HistoryDirectory = "UploadFile/EmpployeeDoc_TEMP/History/IDcard/{0}/{2}/{1}/";
    const string HistoryDirectory2 = "UploadFile/EmpployeeDoc_TEMP/History/Registration/{0}/{2}/{1}/";
    const string HistoryDirectory3 = "UploadFile/EmpployeeDoc_TEMP/History/Driverlicense/{0}/{2}/{1}/";
    const string HistoryDirectory4 = "UploadFile/EmpployeeDoc_TEMP/History/Other/{0}/{2}/{1}/";
    const string HistoryDirectory5 = "UploadFile/EmpployeeDoc_TEMP/History/PictureEMP/{0}/{2}/{1}/";

    //static List<ListGridDoc> listGriddoc = new List<ListGridDoc>();
    //static List<sEmployee> listEmp = new List<sEmployee>();
    //static List<historyEmployee> historyEmp = new List<historyEmployee>();
    //static List<ListGridother> listGridother = new List<ListGridother>();
    //static List<DeleteItem> DeleteGrid = new List<DeleteItem>();
    //private static string REQ_ID = "";
    //private static string VENDORID = "";

    private List<ListGridDoc> listGriddoc { get { return SystemFunction.ConvertObject<ListGridDoc>(Session["listGriddoc"]); } set { Session["listGriddoc"] = value; } }
    private List<sEmployee> listEmp { get { return SystemFunction.ConvertObject<sEmployee>(Session["listEmp"]); } set { Session["listEmp"] = value; } }
    private List<historyEmployee> historyEmp { get { return SystemFunction.ConvertObject<historyEmployee>(Session["historyEmp"]); } set { Session["historyEmp"] = value; } }
    private List<ListGridother> listGridother { get { return SystemFunction.ConvertObject<ListGridother>(Session["listGridother"]); } set { Session["listGridother"] = value; } }
    private List<DeleteItem> DeleteGrid { get { return SystemFunction.ConvertObject<DeleteItem>(Session["DeleteGrid"]); } set { Session["DeleteGrid"] = value; } }

    private string REQ_ID { get { return Session["REQ_ID"] + ""; } set { Session["REQ_ID"] = value; } }
    private string VENDORID { get { return Session["VENDORID"] + ""; } set { Session["VENDORID"] = value; } }
    private string SVENDOR_ID { get { return Session["SVENDOR_ID"] + ""; } set { Session["SVENDOR_ID"] = value; } }
    string SMENUID = "60";

    private void ClearSessionStatic()
    {
        Session["listGriddoc"] = "";
        Session["listEmp"] = "";
        Session["historyEmp"] = "";
        Session["listGridother"] = "";
        Session["DeleteGrid"] = "";
        Session["REQ_ID"] = "";
        Session["SVENDOR_ID"] = "";
    }

    private void NewLstStatic()
    {
        listGriddoc = new List<ListGridDoc>();
        listEmp = new List<sEmployee>();
        historyEmp = new List<historyEmployee>();
        listGridother = new List<ListGridother>();
        DeleteGrid = new List<DeleteItem>();
        REQ_ID = "";
        SVENDOR_ID = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        if (!IsPostBack)
        {
            ClearSessionStatic();
            NewLstStatic();

            #region เช็ค Permission
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            #endregion


            cboPosition.DataSource = sds;
            cboPosition.DataBind();
            cboPosition.Items.Insert(0, new ListEditItem("-เลือกตำแหน่ง-", null));
            cboPosition.SelectedIndex = 0;


            // setdata();
            imgEmp.ImageUrl = "images/Avatar.png";
            VisibleControlUpload();
           
           
        }

        Listgvwother();

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            setDatacallback();
            string[] param = e.Parameter.Split(';');
            //DataTable dtEMP = CommonFunction.Get_Data(conn, "SELECT FC_GENID_EMPLOYEEID() as EMPID FROM DUAL");
            string EMPID = Gen_IDEMP();
            string USER = Session["UserID"] + "";
            //string Line_no = "";
            string SDOCID = Session["SDOCID"] + "";
            string SDOCTYPE = Session["SDOCTYPE"] + "";
            string SDOCID2 = Session["SDOCID2"] + "";
            string SDOCTYPE2 = Session["SDOCTYPE2"] + "";
            string SDOCID3 = Session["SDOCID3"] + "";
            string SDOCTYPE3 = Session["SDOCTYPE3"] + "";
            string SDOCID4 = Session["SDOCID4"] + "";
            string SDOCTYPE4 = Session["SDOCTYPE4"] + "";

            VENDORID = cboVendor.Value + "";
            switch (param[0])
            {


                case "Save":
                    if (CanWrite)
                    {
                        using (OracleConnection con = new OracleConnection(conn))
                        {
                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }

                            string chkFILL = "";
                            DateTime? BirthDay = null;
                            DateTime? startPercode = null;
                            DateTime? endPercode = null;
                            if (!string.IsNullOrEmpty(dedtBirthDay.Text))
                            {
                                BirthDay = DateTime.Parse(dedtBirthDay.Text);

                            }
                            if (!string.IsNullOrEmpty(dedtstartPercode.Text))
                            {
                                startPercode = DateTime.Parse(dedtstartPercode.Text);
                            }
                            if (!string.IsNullOrEmpty(dedtendPercode.Text))
                            {
                                endPercode = DateTime.Parse(dedtendPercode.Text);
                            }
                            if (chkNotfill.Checked == true)
                            {
                                chkFILL = "1";
                            }
                            else
                            {
                                chkFILL = "0";
                            }
                            string CAUSESAPCOMMIT = "";
                            string BANSTATUS = "";
                            string CAUSESAP = "";
                            string CANCELSTATUS = "";
                            string CAUSESAPCANCEL = "";
                            switch (rblStatus.SelectedIndex)
                            {   //อนุญาติ
                                case 0:
                                    CAUSESAPCOMMIT = txtConfirm.Text;

                                    break;
                                //ระงับ
                                case 1:
                                    if (cboStatus.SelectedItem != null)
                                    {
                                        BANSTATUS = cboStatus.SelectedItem.Value + "";
                                    }
                                    else
                                    {

                                    }
                                    CAUSESAP = txtComment.Text;
                                    break;
                                //ยกเลิก
                                case 2:
                                    if (cboStatus2.SelectedItem != null)
                                    {
                                        CANCELSTATUS = cboStatus2.SelectedItem.Value + "";
                                    }
                                    else
                                    {
                                    }
                                    CAUSESAPCANCEL = txtstatus2.Text;
                                    break;
                            }




                            string FNAME = txtName.Text;
                            string LNAME = txtSurName.Text;
                            string PERS_CODE = txtPercode.Text;
                            string DESCROPTION = txtName.Text + " " + txtSurName.Text;

                            DataTable dtPersonal = CommonFunction.Get_Data(con, "SELECT SPERSONELNO  FROM TEMPLOYEE WHERE SPERSONELNO = '" + PERS_CODE + "' AND SEMPLOYEEID <> '" + EMPID + "'");
                            if (dtPersonal.Rows.Count > 0)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','มีเลขบัตรประชาชนนนี้ในระบบแล้ว');");
                                break;
                            }

                            string SREQ_ID = "";
                            if (!string.IsNullOrEmpty(REQ_ID))
                            {
                                SREQ_ID = REQ_ID;
                                //SystemFunction.AddToTREQ_DATACHANGE(EMPID, "E", USER, "0", DESCROPTION, VENDORID, "Y");
                            }
                            else
                            {
                                SREQ_ID = SystemFunction.AddToTREQ_DATACHANGE(EMPID, "E", USER, "0", DESCROPTION, VENDORID, "Y");
                            }



                            string INS_EMP_SAP = @"INSERT INTO TEMPLOYEE_SAP_TEMP (SEMPLOYEEID, FNAME, LNAME, DCREATE, SCREATE, PERS_CODE,CARRIER,REQ_ID,LICENSE_NO) 
VALUES ( '" + EMPID + "','" + FNAME + "','" + LNAME + "',SYSDATE, '" + USER + "','" + PERS_CODE + "','" + cboVendor.Value + "','" + SREQ_ID + "','" + txtdrivelicence.Text + "')";
                            AddTODB(INS_EMP_SAP);
                            string INS_EMP = @"INSERT INTO TEMPLOYEE_TEMP (SEMPLOYEEID, STRANS_ID,SEMPTPYE,DBIRTHDATE,STEL,STEL2,SMAIL,SMAIL2,PERSONEL_BEGIN,PERSONEL_EXPIRE,SCREATE,DCREATE,CACTIVE,CAUSEOVER,SFILENAME
,SSYSFILENAME,SPATH,SDRIVERNO,DDRIVEBEGIN,DDRIVEEXPIRE,REQ_ID,SPERSONELNO) 
VALUES ( '" + EMPID + @"', :STRANS_ID,:SEMPTPYE,:DBIRTHDATE,:STEL,:STEL2,:SMAIL,:SMAIL2,:PERSONEL_BEGIN,:PERSONEL_EXPIRE,:SCREATE,SYSDATE,:CACTIVE,:CAUSEOVER,:SFILENAME
,:SSYSFILENAME,:SPATH,:SDRIVERNO,:DDRIVEBEGIN,:DDRIVEEXPIRE,'" + SREQ_ID + "',:SPERSONELNO)";
                            using (OracleCommand com1 = new OracleCommand(INS_EMP, con))
                            {
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":STRANS_ID", OracleType.VarChar).Value = cboVendor.Value != null ? cboVendor.Value : DBNull.Value;
                                com1.Parameters.Add(":SEMPTPYE", OracleType.Number).Value = cboPosition.Value != null ? cboPosition.Value : DBNull.Value;
                                com1.Parameters.Add(":DBIRTHDATE", OracleType.DateTime).Value = dedtBirthDay.Value != null ? dedtBirthDay.Value : DBNull.Value;
                                com1.Parameters.Add(":STEL", OracleType.VarChar).Value = txtTel.Text;
                                com1.Parameters.Add(":STEL2", OracleType.VarChar).Value = txtTel2.Text;
                                com1.Parameters.Add(":SMAIL", OracleType.VarChar).Value = txtMail.Text;
                                com1.Parameters.Add(":SMAIL2", OracleType.VarChar).Value = txtMail2.Text;
                                com1.Parameters.Add(":PERSONEL_BEGIN", OracleType.DateTime).Value = dedtstartPercode.Value != null ? dedtstartPercode.Value : DBNull.Value;
                                com1.Parameters.Add(":PERSONEL_EXPIRE", OracleType.DateTime).Value = dedtendPercode.Value != null ? dedtendPercode.Value : DBNull.Value;
                                com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "1";
                                com1.Parameters.Add(":CAUSEOVER", OracleType.VarChar).Value = txtComment2.Text;
                                com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtEMPFileName.Text;
                                com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtEMPSysfilename.Text;
                                com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtEMPTruePath.Text;
                                // com1.Parameters.Add(":NOTFILL", OracleType.VarChar).Value = chkNotfill.Checked == true ? "1" : "0";
                                com1.Parameters.Add(":SDRIVERNO", OracleType.VarChar).Value = txtdrivelicence.Text;
                                com1.Parameters.Add(":DDRIVEBEGIN", OracleType.DateTime).Value = dedtstartlicence.Value != null ? dedtstartlicence.Value : DBNull.Value;
                                com1.Parameters.Add(":DDRIVEEXPIRE", OracleType.DateTime).Value = dedtEndlicence.Value != null ? dedtEndlicence.Value : DBNull.Value;
                                com1.Parameters.Add(":SPERSONELNO", OracleType.VarChar).Value = PERS_CODE;
                                ////ระงับการใช้งาน
                                //com1.Parameters.Add(":BANSTATUS", OracleType.VarChar).Value = BANSTATUS;
                                //com1.Parameters.Add(":CAUSESAP", OracleType.VarChar).Value = CAUSESAP;

                                ////กรณียกเลิกการใช้งาน
                                //com1.Parameters.Add(":CANCELSTATUS", OracleType.VarChar).Value = CANCELSTATUS;
                                //com1.Parameters.Add(":CAUSESAPCANCEL", OracleType.VarChar).Value = CAUSESAPCANCEL;

                                ////กรณีอนุญาต
                                //com1.Parameters.Add(":CAUSESAPCOMMIT", OracleType.VarChar).Value = CAUSESAPCOMMIT;
                                com1.ExecuteNonQuery();

                            }

                            LogUser(SMENUID, "I", "บันทึกคำขอเพิ่มข้อมูลพนักงาน", EMPID);

                            #region EMP


                            //เช็คว่ามีข้อมูลในList หรือไม่ถ้ามีแสดงว่ามีการอัพรูปประจำตัว
                            if (listGriddoc.Count > 0)
                            {
                                //เช็คว่ามีรูปประจำตัวอยู่แล้วหรือไม่
                                if (!string.IsNullOrEmpty(UpEMP.Text))
                                {
                                    //สร้างญPath
                                    string HistoryPath = string.Format(HistoryDirectory5, Session["SVDID"] + "", "uploadEMP", Session["UserID"] + "");
                                    //splitเอาชื่อไฟล์เก่า
                                    string[] OldFile = UpEMP.Text.Split('/');
                                    int ncol = OldFile.Length - 1;
                                    //UploadFile2History(UpEMP.Text, HistoryPath, OldFile[ncol]);
                                    AddhistoryEmployeeAvatarDoc(EMPID, USER, SREQ_ID);
                                    LogUser(SMENUID, "E", "แก้ไขข้อมูลพนักงาน เปลี่ยนรูปประจำตัว", EMPID);
                                }
                            }


                            //                            AddToTemp(EMPID, SREQ_ID);

                            //                            string QUERY_SAP_EMP = @"UPDATE TEMPLOYEE_SAP_TEMP
                            //                                                        SET   
                            //                                                               FNAME       = '" + CommonFunction.ReplaceInjection(txtName.Text) + @"',
                            //                                                               LNAME       = '" + CommonFunction.ReplaceInjection(txtSurName.Text) + @"',
                            //                                                               DUPDATE     = SYSDATE,
                            //                                                               SUPDATE     = '" + CommonFunction.ReplaceInjection(USER) + @"',
                            //                                                               PERS_CODE   = '" + CommonFunction.ReplaceInjection(txtPercode.Text) + @"'
                            //                                                        WHERE  SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(EMPID) + "'";

                            //                            AddTODB(QUERY_SAP_EMP);

                            //                            string UpdateEmployee = @"UPDATE TEMPLOYEE_TEMP
                            //                                         SET SEMPTPYE=:SEMPTPYE
                            //                                        ,DBIRTHDATE=:DBIRTHDATE
                            //                                        ,STEL=:STEL
                            //                                        ,STEL2=:STEL2
                            //                                        ,SMAIL=:SMAIL
                            //                                        ,SMAIL2=:SMAIL2
                            //                                        ,PERSONEL_BEGIN=:PERSONEL_BEGIN
                            //                                        ,PERSONEL_EXPIRE=:PERSONEL_EXPIRE
                            //                                        ,DUPDATE=sysdate
                            //                                        ,SUPDATE=:SUPDATE
                            //                                        ,CACTIVE=:CACTIVE
                            //                                        ,CAUSESAP=:CAUSESAP
                            //                                        ,CAUSEOVER=:CAUSEOVER
                            //                                        ,SFILENAME=:SFILENAME
                            //                                        ,SSYSFILENAME=:SSYSFILENAME
                            //                                        ,SPATH=:SPATH
                            //                                        ,NOTFILL=:NOTFILL
                            //                                        ,BANSTATUS=:BANSTATUS
                            //                                        ,CAUSESAPCANCEL=:CAUSESAPCANCEL
                            //                                        ,CANCELSTATUS=:CANCELSTATUS
                            //                                        ,CAUSESAPCOMMIT=:CAUSESAPCOMMIT
                            //                                        ,SDRIVERNO=:SDRIVERNO   
                            //                                        ,DDRIVEBEGIN=:DDRIVEBEGIN   
                            //                                        ,DDRIVEEXPIRE=:DDRIVEEXPIRE                
                            //                                         WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(EMPID) + "' AND REQ_ID = '" + SREQ_ID + "'";



                            //}
                            #endregion



                            #region saveUpload


                            string InsUpload = @"INSERT INTO TEMPLOYEE_DOC_TEMP(SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE ,SFILENAME
                                                           , SDESCRIPTION, CACTIVE ,DCREATE ,SCREATE , DEXPIRE,SPATH ,SSYSFILENAME,REQ_ID) 
                                                           VALUES (:SEMPLOYEEID,FC_GENID_EMPLOYEE_DOC(), :SDOCVERSION , :SDOCTYPE ,:SFILENAME
                                                           ,  :SDESCRIPTION, :CACTIVE, sysdate,:SCREATE ,:DEXPIRE,:SPATH,:SSYSFILENAME,:REQ_ID)";

                            string UpdateUpload = @"UPDATE TEMPLOYEE_DOC_TEMP
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                               ,CACTIVE=:CACTIVE
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "' AND REQ_ID = '" + SREQ_ID + "'";

                            #region ลบไฟล์ในUpload ทั้งหมดที่มีการลบ

                            if (DeleteGrid.Count > 0)
                            {
                                foreach (var item in DeleteGrid)
                                {
                                    AddhistoryEmployeeDoc(item.SVENDORID, item.SDOCID, item.SDOCTYPE);

                                    string UpdateUpload4 = @"UPDATE TEMPLOYEE_DOC_TEMP

                                                                SET CACTIVE=:CACTIVE
                                                                WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(item.SVENDORID) + @"' 
                                                                AND SDOCID ='" + CommonFunction.ReplaceInjection(item.SDOCID) + @"'
                                                                AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(item.SDOCTYPE) + "' AND REQ_ID = '" + SREQ_ID + "'";

                                    using (OracleCommand com1 = new OracleCommand(UpdateUpload4, con))
                                    {

                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "0";
                                        com1.ExecuteNonQuery();
                                    }

                                    switch (item.SDOCTYPE)
                                    {
                                        case "1":
                                            LogUser(SMENUID, "D", "แก้ไขข้อมูลพนักงาน   เอกสารสำเนาบัตรประชาชน", item.SVENDORID);
                                            string HistoryPath = string.Format(HistoryDirectory, EMPID, "uploader1", USER);
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile = item.SPATHALL.Split('/');
                                            int ncol = OldFile.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath, OldFile[ncol]);
                                            break;
                                        case "2":
                                            //สร้างญPath
                                            LogUser(SMENUID, "D", "แก้ไขข้อมูลพนักงาน  เอกสารทะเบียนบ้าน", item.SVENDORID);
                                            string HistoryPath2 = string.Format(HistoryDirectory2, EMPID, "uploader2", USER);
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile2 = item.SPATHALL.Split('/');
                                            int ncol2 = OldFile2.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath2, OldFile2[ncol2]);
                                            break;
                                        case "3":
                                            //สร้างญPath
                                            LogUser(SMENUID, "D", "แก้ไขข้อมูลพนักงาน  เอกสารใบขับขี่ประเภท4", item.SVENDORID);
                                            string HistoryPath3 = string.Format(HistoryDirectory3, EMPID, "uploader3", USER);
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile3 = item.SPATHALL.Split('/');
                                            int ncol3 = OldFile3.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath3, OldFile3[ncol3]);
                                            break;
                                        case "4":
                                            //สร้างญPath
                                            LogUser(SMENUID, "D", "แก้ไขข้อมูลพนักงาน  เอกสารสำคัญอื่นๆ", item.SVENDORID);
                                            string HistoryPath4 = string.Format(HistoryDirectory4, EMPID, "uploaderother", USER);
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile4 = item.SPATHALL.Split('/');
                                            int ncol4 = OldFile4.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath4, OldFile4[ncol4]);
                                            break;
                                        //case "5":
                                        //    //สร้างญPath
                                        //    LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง  ลบเอกสารอื่นๆ", item.SVENDORID);
                                        //    string HistoryPath6 = string.Format(HistoryDirectory6, Session["SVDID"] + "", "uploaderother", Session["UserID"] + "");
                                        //    //splitเอาชื่อไฟล์เก่า
                                        //    string[] OldFile6 = item.SPATHALL.Split('/');
                                        //    int ncol6 = OldFile6.Length - 1;
                                        //    UploadFile2History(item.SPATHALL, HistoryPath6, OldFile6[ncol6]);
                                        //    break;
                                    }


                                }

                            }
                            #endregion

                            #region Upload1
                            string CheckUpload1 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TEMPLOYEE_DOC_TEMP 
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename.Text) + "' AND REQ_ID = '" + SREQ_ID + "'";


                            if (!string.IsNullOrEmpty(txtFileName.Text))
                            {

                                DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload1);

                                if (dtUpload.Rows.Count > 0)
                                {


                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(txtchkUpdate.Text))
                                    {

                                        //AddhistoryVendorDoc(VendorID, SDOCID, SDOCTYPE);
                                        LogUser(SMENUID, "E", "แก้ไขข้อมูลพนักงาน แนบเอกสารสำเนาบัตรประชาชน", EMPID);
                                        AddhistoryEmployeeDoc(EMPID, SDOCID, SDOCTYPE);
                                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;
                                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "1";
                                            com1.ExecuteNonQuery();

                                        }

                                        //สร้างญPath
                                        string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                                        //splitเอาชื่อไฟล์เก่า
                                        string[] OldFile = Up1.Text.Split('/');
                                        int ncol = OldFile.Length - 1;
                                        UploadFile2History(Up1.Text, HistoryPath, OldFile[ncol]);
                                    }
                                    else
                                    {
                                        //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                        LogUser(SMENUID, "I", "แก้ไขข้อมูลพนักงาน แนบเอกสารสำเนาบัตรประชาชน", EMPID);
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "1";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารสำเนาบัตรประชาชน";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;
                                            com1.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //LogUser("47", "D", "แก้ไขข้อมูลพนักงาน ลบเอกสารสำเนาบัตรประชาชน", EMPID);
                                //AddhistoryEmployeeDoc(EMPID, SDOCID, SDOCTYPE);
                                ////สร้างญPath
                                //string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                                ////splitเอาชื่อไฟล์เก่า
                                //string[] OldFile = Up1.Text.Split('/');
                                //int ncol = OldFile.Length - 1;
                                //UploadFile2History(Up1.Text, HistoryPath, OldFile[ncol]);
                                //using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                                //{
                                //    com1.Parameters.Clear();
                                //    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                //    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                //    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                //    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                //    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;

                                //    com1.ExecuteNonQuery();

                                //}
                            }

                            #endregion

                            #region Upload2

                            string UpdateUpload2 = @"UPDATE TEMPLOYEE_DOC_TEMP
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE2) + "' AND REQ_ID = '" + SREQ_ID + "'";

                            string CheckUpload2 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TEMPLOYEE_DOC_TEMP 
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE2) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName2.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath2.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename2.Text) + "' AND REQ_ID = '" + SREQ_ID + "'";


                            if (!string.IsNullOrEmpty(txtFileName2.Text))
                            {

                                DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload2);

                                if (dtUpload.Rows.Count > 0)
                                {


                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(txtchkUpdate2.Text))
                                    {

                                        LogUser(SMENUID, "E", "แก้ไขข้อมูลพนักงาน แนบเอกสารทะเบียนบ้าน", EMPID);
                                        AddhistoryEmployeeDoc(EMPID, SDOCID2, SDOCTYPE2);
                                        using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;
                                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "1";
                                            com1.ExecuteNonQuery();

                                        }
                                        //สร้างญPath
                                        string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                                        //splitเอาชื่อไฟล์เก่า
                                        string[] OldFile = Up2.Text.Split('/');
                                        int ncol = OldFile.Length - 1;
                                        UploadFile2History(Up2.Text, HistoryPath, OldFile[ncol]);
                                    }
                                    else
                                    {
                                        LogUser(SMENUID, "I", "แก้ไขข้อมูลพนักงาน แนบเอกสารทะเบียนบ้าน", EMPID);
                                        //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "2";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารทะเบียนบ้าน";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;
                                            com1.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //AddhistoryEmployeeDoc(EMPID, SDOCID2, SDOCTYPE2);
                                //LogUser("47", "D", "แก้ไขข้อมูลพนักงาน ลบเอกสารทะเบียนบ้าน", EMPID);
                                //string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                                ////splitเอาชื่อไฟล์เก่า
                                //string[] OldFile = Up2.Text.Split('/');
                                //int ncol = OldFile.Length - 1;
                                //UploadFile2History(Up2.Text, HistoryPath, OldFile[ncol]);
                                //using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                                //{
                                //    com1.Parameters.Clear();
                                //    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                //    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                //    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                //    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                //    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                                //    com1.ExecuteNonQuery();

                                //}
                            }
                            #endregion

                            #region Upload3
                            string UpdateUpload3 = @"UPDATE TEMPLOYEE_DOC_TEMP
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE3) + "' AND REQ_ID = '" + SREQ_ID + "'";

                            string CheckUpload3 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TEMPLOYEE_DOC_TEMP 
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE3) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName3.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath3.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename3.Text) + "' AND REQ_ID = '" + SREQ_ID + "'";
                            if (!string.IsNullOrEmpty(txtFileName3.Text))
                            {

                                DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload3);

                                if (dtUpload.Rows.Count > 0)
                                {


                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(txtchkUpdate3.Text))
                                    {
                                        AddhistoryEmployeeDoc(EMPID, SDOCID3, SDOCTYPE3);
                                        LogUser(SMENUID, "E", "แก้ไขข้อมูลพนักงาน แนบเอกสารใบขับขี่ประเภท4", EMPID);
                                        using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "1";
                                            com1.ExecuteNonQuery();

                                        }
                                        //สร้างญPath
                                        string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                                        //splitเอาชื่อไฟล์เก่า
                                        string[] OldFile = Up3.Text.Split('/');
                                        int ncol = OldFile.Length - 1;
                                        UploadFile2History(Up3.Text, HistoryPath, OldFile[ncol]);
                                    }
                                    else
                                    {
                                        //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                        LogUser(SMENUID, "I", "แก้ไขข้อมูลพนักงาน แนบเอกสารใบขับขี่ประเภท4", EMPID);
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "3";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารใบขับขี่ประเภท4";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                            com1.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //AddhistoryEmployeeDoc(EMPID, SDOCID3, SDOCTYPE3);
                                //LogUser("47", "D", "แก้ไขข้อมูลพนักงาน ลบเอกสารใบขับขี่ประเภท4", EMPID);
                                //string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                                ////splitเอาชื่อไฟล์เก่า
                                //string[] OldFile = Up3.Text.Split('/');
                                //int ncol = OldFile.Length - 1;
                                //UploadFile2History(Up3.Text, HistoryPath, OldFile[ncol]);
                                //using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                                //{
                                //    com1.Parameters.Clear();
                                //    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                //    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                //    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                //    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                //    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;

                                //    com1.ExecuteNonQuery();

                                //}
                            }
                            #endregion

                            #region Upload4
                            //                            string UpdateUpload4 = @"UPDATE TEMPLOYEE_DOC
                            //                                            SET SFILENAME=:SFILENAME
                            //                                               ,SPATH=:SPATH
                            //                                               ,DUPDATE=sysdate
                            //                                               ,SUPDATE=:SUPDATE
                            //                                               ,DEXPIRE=:DEXPIRE
                            //                                               ,SSYSFILENAME=:SSYSFILENAME
                            //                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                            //                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID4) + @"'
                            //                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE4) + @"'";

                            //                            string CheckUpload4 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                            //                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                            //                                            FROM TEMPLOYEE_DOC 
                            //                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                            //                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID4) + @"'
                            //                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE4) + @"'
                            //                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName4.Text) + @"'
                            //                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath4.Text) + @"'
                            //                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename4.Text) + "'";

                            //                            if (!string.IsNullOrEmpty(txtFileName4.Text))
                            //                            {

                            //                                DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload4);

                            //                                if (dtUpload.Rows.Count > 0)
                            //                                {


                            //                                }
                            //                                else
                            //                                {
                            //                                    if (!string.IsNullOrEmpty(txtchkUpdate4.Text))
                            //                                    {
                            //                                        AddhistoryEmployeeDoc(EMPID, SDOCID4, SDOCTYPE4);
                            //                                        LogUser("47", "E", "แก้ไขข้อมูลพนักงาน แนบเอกสารสำคัญอื่นๆ", EMPID);
                            //                                        using (OracleCommand com1 = new OracleCommand(UpdateUpload4, con))
                            //                                        {
                            //                                            com1.Parameters.Clear();
                            //                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName4.Text;
                            //                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath4.Text;
                            //                                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            //                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            //                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename4.Text;
                            //                                            com1.ExecuteNonQuery();

                            //                                        }
                            //                                        //สร้างญPath
                            //                                        string HistoryPath = string.Format(HistoryDirectory4, Session["SVDID"] + "", "uploader4", Session["UserID"] + "");
                            //                                        //splitเอาชื่อไฟล์เก่า
                            //                                        string[] OldFile = Up4.Text.Split('/');
                            //                                        int ncol = OldFile.Length - 1;
                            //                                        UploadFile2History(Up4.Text, HistoryPath, OldFile[ncol]);
                            //                                    }
                            //                                    else
                            //                                    {
                            //                                        //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                            //                                        LogUser("47", "I", "แก้ไขข้อมูลพนักงาน แนบเอกสารสำคัญอื่นๆ", EMPID);
                            //                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                            //                                        {
                            //                                            decimal num = 0;
                            //                                            com1.Parameters.Clear();
                            //                                            com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                            //                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                            //                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "4";
                            //                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName4.Text;
                            //                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารสำคัญอื่นๆ";
                            //                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                            //                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                            //                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            //                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath4.Text;
                            //                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename4.Text;
                            //                                            com1.ExecuteNonQuery();
                            //                                        }
                            //                                    }
                            //                                }

                            //                                //ListgvwDoc();
                            //                                //gvwDoc.DataBind();

                            //                            }
                            //                            else
                            //                            {
                            //                                AddhistoryEmployeeDoc(EMPID, SDOCID4, SDOCTYPE4);
                            //                                LogUser("47", "D", "แก้ไขข้อมูลพนักงาน ลบเอกสารสำคัญอื่นๆ", EMPID);
                            //                                string HistoryPath = string.Format(HistoryDirectory4, Session["SVDID"] + "", "uploader4", Session["UserID"] + "");
                            //                                //splitเอาชื่อไฟล์เก่า
                            //                                string[] OldFile = Up4.Text.Split('/');
                            //                                int ncol = OldFile.Length - 1;
                            //                                UploadFile2History(Up4.Text, HistoryPath, OldFile[ncol]);
                            //                                using (OracleCommand com1 = new OracleCommand(UpdateUpload4, con))
                            //                                {

                            //                                    com1.Parameters.Clear();
                            //                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName4.Text;
                            //                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath4.Text;
                            //                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            //                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            //                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename4.Text;

                            //                                    com1.ExecuteNonQuery();

                            //                                }
                            //                            }

                            #region Uploadother

                            var chknewDocother = listGridother.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
                            if (chknewDocother.Count() > 0)
                            {

                                foreach (var item in chknewDocother)
                                {
                                    LogUser(SMENUID, "I", "แก้ไขข้อมูลผู้ขนส่ง  แนบเอกสารอื่นๆ", item.SVENDORID);
                                    //int index = int.Parse(item.INDEX + "");

                                    //ASPxTextBox txtSysfilenameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilenameother") as ASPxTextBox;
                                    //ASPxTextBox txtFileNameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtFileNameother") as ASPxTextBox;
                                    //ASPxTextBox txtTruePathother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtTruePathother") as ASPxTextBox;


                                    using (OracleConnection con2 = new OracleConnection(conn))
                                    {

                                        if (con2.State == ConnectionState.Closed)
                                        {
                                            con2.Open();
                                        }
                                        else
                                        {

                                        }
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con2))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "4";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = item.SFILENAME;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "เอกสารอื่นๆ";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = item.STRUEPATH;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = item.SSYSFILENAME;
                                            com1.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;

                                            com1.ExecuteNonQuery();
                                        }
                                        con2.Close();
                                    }


                                }
                                //เมื่อทำการเซฟข้อมูลแล้ว1ครั้งจะมีใส่ค่า S
                                //FileToServer();
                                //txtUploadchk4.Text = "S";
                                //listGrid.RemoveAll(o => 1 == 1);
                                //Setdata();
                                //Listgvwdoc4();
                            }
                            else
                            {
                                Listgvwother();
                            }
                            #endregion
                            #endregion


                            #endregion

                            txtUploadother.Text = "S";

                            FileToServer();
                            con.Close();
                            VisibleControlUpload();

                            con.Close();

                            Session.Remove("SDOCID");
                            Session.Remove("SDOCTYPE");
                            Session.Remove("SDOCID2");
                            Session.Remove("SDOCTYPE2");
                            Session.Remove("SDOCID3");
                            Session.Remove("SDOCTYPE3");
                            Session.Remove("SDOCID4");
                            Session.Remove("SDOCTYPE4");
                            Session.Remove("SESSIONEMPID");
                            Session.Remove("CheckPermission");

                            // CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='Vendor_Detail.aspx';});");
                            //// xcpn.JSProperties["cpRedirectTo"] = "Vendor_Detail.aspx";
                            if (SendMailToVendorRk())
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){ window.location='Vendor_Detail.aspx'; });");
                            }
                            else
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + " <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้',function(){window.location='Vendor_Detail.aspx';});");
                            }
                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;
                #region ลบไฟล์ที่อับโหลด
                //case "deleteFile":

                //    string FilePath = param[1];

                //    if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                //    {
                //        File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                //    }

                //    string cNo = param[2];
                //    if (cNo == "1")
                //    {
                //        txtFileName.Text = "";
                //        txtFilePath.Text = "";
                //        txtTruePath.Text = "";
                //        txtSysfilename.Text = "";
                //    }
                //    txtValidate.Text = "";

                //    VisibleControlUpload();
                //    break;

                //case "deleteFile2":

                //    string FilePath2 = param[1];

                //    if (File.Exists(Server.MapPath("./") + FilePath2.Replace("/", "\\")))
                //    {
                //        File.Delete(Server.MapPath("./") + FilePath2.Replace("/", "\\"));
                //    }

                //    string cNo2 = param[2];
                //    if (cNo2 == "1")
                //    {
                //        txtFileName2.Text = "";
                //        txtFilePath2.Text = "";
                //        txtTruePath2.Text = "";
                //        txtSysfilename2.Text = "";
                //        //dedtUpload2.Value = "";
                //    }

                //    txtValidate2.Text = "";
                //    VisibleControlUpload();
                //    break;

                //case "deleteFile3":

                //    string FilePath3 = param[1];

                //    if (File.Exists(Server.MapPath("./") + FilePath3.Replace("/", "\\")))
                //    {
                //        File.Delete(Server.MapPath("./") + FilePath3.Replace("/", "\\"));
                //    }

                //    string cNo3 = param[2];
                //    if (cNo3 == "1")
                //    {
                //        txtFileName3.Text = "";
                //        txtFilePath3.Text = "";
                //        txtTruePath3.Text = "";
                //        txtSysfilename3.Text = "";
                //        //dedtUpload3.Value = "";
                //    }

                //    VisibleControlUpload();

                //    break;

                case "deleteFile":
                    //เก็บข้อมูลลง List ลบ
                    DeleteGrid.Add(new DeleteItem
                    {
                        SVENDORID = EMPID,
                        SDOCID = SDOCID,
                        SDOCTYPE = SDOCTYPE,
                        SPATHALL = txtTruePath.Text + txtSysfilename.Text
                    });
                    if (CanWrite)
                    {
                        string cNo = param[2];
                        if (cNo == "1")
                        {
                            txtFileName.Text = "";
                            txtFilePath.Text = "";
                            txtTruePath.Text = "";
                            txtSysfilename.Text = "";
                        }
                        txtValidate.Text = "";

                        VisibleControlUpload();
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;

                case "deleteFile2":
                    if (CanWrite)
                    {
                        //เก็บข้อมูลลง List ลบ
                        //DeleteGrid.Add(new DeleteItem
                        //{
                        //    SVENDORID = EMPID,
                        //    SDOCID = SDOCID2,
                        //    SDOCTYPE = SDOCTYPE2,
                        //    SPATHALL = txtTruePath2.Text + txtSysfilename2.Text
                        //});

                        string cNo2 = param[2];
                        if (cNo2 == "1")
                        {
                            txtFileName2.Text = "";
                            txtFilePath2.Text = "";
                            txtTruePath2.Text = "";
                            txtSysfilename2.Text = "";

                        }
                        txtValidate2.Text = "";

                        VisibleControlUpload();
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;

                case "deleteFile3":
                    //เก็บข้อมูลลง List ลบ
                    //DeleteGrid.Add(new DeleteItem
                    //{
                    //    SVENDORID = EMPID,
                    //    SDOCID = SDOCID3,
                    //    SDOCTYPE = SDOCTYPE3,
                    //    SPATHALL = txtTruePath3.Text + txtSysfilename3.Text
                    //});
                    if (CanWrite)
                    {
                        string cNo3 = param[2];
                        if (cNo3 == "1")
                        {
                            txtFileName3.Text = "";
                            txtFilePath3.Text = "";
                            txtTruePath3.Text = "";
                            txtSysfilename3.Text = "";

                        }

                        VisibleControlUpload();
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }

                    break;

                case "deleteFile4":
                    //if (CanWrite)
                    //{
                    //    //เก็บข้อมูลลง List ลบ
                    //    //DeleteGrid.Add(new DeleteItem
                    //    //{
                    //    //    SVENDORID = EMPID,
                    //    //    SDOCID = SDOCID4,
                    //    //    SDOCTYPE = SDOCTYPE4,
                    //    //    SPATHALL = txtTruePath4.Text + txtSysfilename4.Text
                    //    //});

                    //    string cNo4 = param[2];
                    //    if (cNo4 == "1")
                    //    {
                    //        txtFileName4.Text = "";
                    //        txtFilePath4.Text = "";
                    //        txtTruePath4.Text = "";
                    //        txtSysfilename4.Text = "";

                    //    }

                    //    VisibleControlUpload();
                    //}
                    //else
                    //{
                    //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    //}
                    break;



                #endregion

                #region chagevalidate

                case "chagevalidate":
                    if (CanWrite)
                    {
                        string Cbovalue = "";
                        if (!string.IsNullOrEmpty(cboPosition.Value + ""))
                        {
                            Cbovalue = cboPosition.Value + "";
                        }

                        switch (Cbovalue)
                        {
                            //พขร. ประจำรถ
                            case "11":
                                //บอร์โทรศัพท์หลัก
                                txtTel.ValidationSettings.RequiredField.IsRequired = false;
                                //txtTel.ValidationSettings.Display = Display.Dynamic;
                                //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtTel.ValidationSettings.SetFocusOnError = true;
                                //txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                                txtTel.ValidationSettings.ValidationGroup = "add";
                                //E-Mail หลัก
                                txtMail.ValidationSettings.RequiredField.IsRequired = false;
                                //txtMail.ValidationSettings.Display = Display.Dynamic;
                                //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtMail.ValidationSettings.SetFocusOnError = true;
                                //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                                txtMail.ValidationSettings.ValidationGroup = "add";
                                //หมายเลขใบขับขี่ประเภท 4
                                txtdrivelicence.ValidationSettings.RequiredField.IsRequired = true;
                                //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                                //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                                //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                                txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                                dedtstartlicence.ValidationSettings.RequiredField.IsRequired = true;
                                //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                                dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                                //dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                                dedtEndlicence.ValidationSettings.RequiredField.IsRequired = true;
                                //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                                dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                                //เอกสารใบขับขี่ประเภท 4
                                txtValidate3.ValidationSettings.RequiredField.IsRequired = true;
                                //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                                //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtValidate3.ValidationSettings.SetFocusOnError = true;
                                //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                                txtValidate3.ValidationSettings.ValidationGroup = "add";
                                break;
                            //พขร. ขับรถสำรอง
                            case "4":
                                //บอร์โทรศัพท์หลัก
                                txtTel.ValidationSettings.RequiredField.IsRequired = false;
                                //txtTel.ValidationSettings.Display = Display.Dynamic;
                                //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtTel.ValidationSettings.SetFocusOnError = true;
                                //txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                                txtTel.ValidationSettings.ValidationGroup = "add";
                                //E-Mail หลัก
                                txtMail.ValidationSettings.RequiredField.IsRequired = false;
                                //txtMail.ValidationSettings.Display = Display.Dynamic;
                                //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtMail.ValidationSettings.SetFocusOnError = true;
                                //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                                txtMail.ValidationSettings.ValidationGroup = "add";
                                //หมายเลขใบขับขี่ประเภท 4
                                txtdrivelicence.ValidationSettings.RequiredField.IsRequired = true;
                                //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                                //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                                //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                                txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                                dedtstartlicence.ValidationSettings.RequiredField.IsRequired = true;
                                //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                                dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                                //dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                                dedtEndlicence.ValidationSettings.RequiredField.IsRequired = true;
                                //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                                dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                                //เอกสารใบขับขี่ประเภท 4
                                txtValidate3.ValidationSettings.RequiredField.IsRequired = true;
                                //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                                //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtValidate3.ValidationSettings.SetFocusOnError = true;
                                //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                                txtValidate3.ValidationSettings.ValidationGroup = "add";
                                break;
                            //พนักงานติดรถ
                            case "12":
                                //บอร์โทรศัพท์หลัก
                                txtTel.ValidationSettings.RequiredField.IsRequired = false;
                                //txtTel.ValidationSettings.Display = Display.Dynamic;
                                //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtTel.ValidationSettings.SetFocusOnError = true;
                                // txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                                txtTel.ValidationSettings.ValidationGroup = "add";
                                //E-Mail หลัก
                                txtMail.ValidationSettings.RequiredField.IsRequired = false;
                                //txtMail.ValidationSettings.Display = Display.Dynamic;
                                //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtMail.ValidationSettings.SetFocusOnError = true;
                                //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                                txtMail.ValidationSettings.ValidationGroup = "add";
                                //หมายเลขใบขับขี่ประเภท 4
                                txtdrivelicence.ValidationSettings.RequiredField.IsRequired = false;
                                //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                                //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                                //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                                txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                                dedtstartlicence.ValidationSettings.RequiredField.IsRequired = false;
                                //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                                dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                                dedtEndlicence.ValidationSettings.RequiredField.IsRequired = false;
                                //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                                dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                                //เอกสารใบขับขี่ประเภท 4
                                txtValidate3.ValidationSettings.RequiredField.IsRequired = false;
                                //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                                //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtValidate3.ValidationSettings.SetFocusOnError = true;
                                //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                                txtValidate3.ValidationSettings.ValidationGroup = "add";
                                break;
                            //ผู้ประสานงาน
                            case "10":
                                //บอร์โทรศัพท์หลัก
                                //txtTel.ValidationSettings.RequiredField.IsRequired = true;
                                //txtTel.ValidationSettings.Display = Display.Dynamic;
                                //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                txtTel.ValidationSettings.SetFocusOnError = true;
                                //txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                                txtTel.ValidationSettings.ValidationGroup = "add";
                                //E-Mail หลัก
                                txtMail.ValidationSettings.RequiredField.IsRequired = true;
                                //txtMail.ValidationSettings.Display = Display.Dynamic;
                                //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtMail.ValidationSettings.SetFocusOnError = true;
                                //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                                //txtMail.ValidationSettings.ValidationGroup = "add";
                                //หมายเลขใบขับขี่ประเภท 4
                                txtdrivelicence.ValidationSettings.RequiredField.IsRequired = false;
                                //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                                //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                                //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                                txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                                dedtstartlicence.ValidationSettings.RequiredField.IsRequired = false;
                                //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                                dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                                dedtEndlicence.ValidationSettings.RequiredField.IsRequired = false;
                                //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                                dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                                //เอกสารใบขับขี่ประเภท 4
                                txtValidate3.ValidationSettings.RequiredField.IsRequired = false;
                                //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                                //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtValidate3.ValidationSettings.SetFocusOnError = true;
                                //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                                txtValidate3.ValidationSettings.ValidationGroup = "add";
                                break;
                            //ตัวแทนผู้ขนส่ง
                            case "13":
                                //บอร์โทรศัพท์หลัก
                                txtTel.ValidationSettings.RequiredField.IsRequired = true;
                                //txtTel.ValidationSettings.Display = Display.Dynamic;
                                //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtTel.ValidationSettings.SetFocusOnError = true;
                                //txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                                txtTel.ValidationSettings.ValidationGroup = "add";
                                //E-Mail หลัก
                                txtMail.ValidationSettings.RequiredField.IsRequired = true;
                                //txtMail.ValidationSettings.Display = Display.Dynamic;
                                //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtMail.ValidationSettings.SetFocusOnError = true;
                                //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                                txtMail.ValidationSettings.ValidationGroup = "add";
                                //หมายเลขใบขับขี่ประเภท 4
                                txtdrivelicence.ValidationSettings.RequiredField.IsRequired = false;
                                //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                                //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                                //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                                txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                                dedtstartlicence.ValidationSettings.RequiredField.IsRequired = false;
                                //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                                dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                                //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                                dedtEndlicence.ValidationSettings.RequiredField.IsRequired = false;
                                //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                                //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                                //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                                dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                                //เอกสารใบขับขี่ประเภท 4
                                txtValidate3.ValidationSettings.RequiredField.IsRequired = false;
                                //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                                //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                                //txtValidate3.ValidationSettings.SetFocusOnError = true;
                                //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                                txtValidate3.ValidationSettings.ValidationGroup = "add";
                                break;
                            default:
                                //drivelicence.Visible = true;
                                //datedrivelicence.Visible = true;
                                //txtdrivelicence.ValidationSettings.RequiredField.IsRequired = true;
                                //dedtstartlicence.ValidationSettings.RequiredField.IsRequired = true;
                                //dedtEndlicence.ValidationSettings.RequiredField.IsRequired = true;
                                break;

                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;
                #endregion
                case "viewHistoryemp":
                    Session["Backpage"] = EMPID;
                    xcpn.JSProperties["cpRedirectTo"] = "employee_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + EMPID));
                    break;

                case "viewHistory":
                    Session["Backpage"] = EMPID;
                    xcpn.JSProperties["cpRedirectTo"] = "document_employee_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + EMPID));
                    break;

                case "back":

                    string str = Request.QueryString["strType"];
                    if (!string.IsNullOrEmpty(str))
                    {

                        xcpn.JSProperties["cpRedirectTo"] = "vendor_request.aspx";
                    }
                    else
                    {

                        xcpn.JSProperties["cpRedirectTo"] = "Vendor_Detail.aspx";
                    }
                    break;
            }
        }
    }

    private string Gen_IDEMP()
    {
        string SYEAR = DateTime.Now.ToString("yy", new CultureInfo("th-TH"));
        DataTable dt = CommonFunction.Get_Data(conn, @"SELECT * FROM
(
SELECT SEMPLOYEEID FROM TEMPLOYEE_SAP 
UNION
SELECT SEMPLOYEEID FROM TEMPLOYEE_SAP_TEMP
)
WHERE SUBSTR( SEMPLOYEEID, 0 ,2 ) LIKE'%" + SYEAR + "%' ORDER BY SEMPLOYEEID DESC");

        if (dt.Rows.Count > 0)
        {
            string ID = (dt.Rows[0]["SEMPLOYEEID"] + "").Substring(2, 8);
            string NUM = (int.Parse(ID) + 1) + "";
            string NewID = SYEAR + NUM.PadLeft(8, '0');
            return NewID;
        }
        else
        {

            return SYEAR + "00000001";
        }
    }

    private void AddToTemp(string EMP_ID, string REQ_ID)
    {
        string QUERY_EMP = @"INSERT INTO TEMPLOYEE_TEMP (
   SEMPLOYEEID, STRANS_ID, STRANTYPE, 
   SEMPTPYE, SDRIVERNO, SREGIDTERNO, 
   STRUCKID, INAME, SNATIONNAL, 
   SORIGIN, SRELIGION, NHEIGHT, 
   NWEIGHT, SSCAR, DBIRTHDATE, 
   SADDRESS, TUMBON, AMPHUR, 
   SPROVINCEID, SZIPCODE, STEL, 
   SFAX, SPERSONELNO, PERSONEL_BEGIN, 
   PERSONEL_EXPIRE, DDRIVEBEGIN, DDRIVEEXPIRE, 
   NENTRYYEAR, NEDUCATIONID, SREMARK, 
   CACTIVE, SSTATUSREMARK, SPICNAME, 
   SBLACKLISTTYPE, DBEGINBLACKLIST, DENDBLACKLIST, 
   CBLACKLIST, DCREATE, SCREATE, 
   DUPDATE, SUPDATE, SPATH, 
   SFILENAME, SSYSFILENAME, CAUSESAP, 
   CAUSEOVER, STEL2, SMAIL, 
   SMAIL2, CHECKIN, NOTFILL, 
   BANSTATUS, CAUSESAPCANCEL, CANCELSTATUS, 
   CAUSESAPCOMMIT, DATE_LASTJOB, LAST_SHIPMENT, 
   INUSE,REQ_ID) 
SELECT SEMPLOYEEID ,
  STRANS_ID ,
  STRANTYPE ,
  SEMPTPYE ,
  SDRIVERNO ,
  SREGIDTERNO ,
  STRUCKID ,
  INAME ,
  SNATIONNAL ,
  SORIGIN ,
  SRELIGION ,
  NHEIGHT ,
  NWEIGHT ,
  SSCAR ,
  DBIRTHDATE ,
  SADDRESS ,
  TUMBON ,
  AMPHUR ,
  SPROVINCEID ,
  SZIPCODE ,
  STEL ,
  SFAX ,
  SPERSONELNO ,
  PERSONEL_BEGIN ,
  PERSONEL_EXPIRE ,
  DDRIVEBEGIN ,
  DDRIVEEXPIRE ,
  NENTRYYEAR ,
  NEDUCATIONID ,
  SREMARK ,
  CACTIVE ,
  SSTATUSREMARK ,
  SPICNAME ,
  SBLACKLISTTYPE ,
  DBEGINBLACKLIST ,
  DENDBLACKLIST ,
  CBLACKLIST ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  SPATH ,
  SFILENAME ,
  SSYSFILENAME ,
  CAUSESAP ,
  CAUSEOVER ,
  STEL2 ,
  SMAIL ,
  SMAIL2 ,
  CHECKIN ,
  NOTFILL ,
  BANSTATUS ,
  CAUSESAPCANCEL ,
  CANCELSTATUS ,
  CAUSESAPCOMMIT ,
  DATE_LASTJOB ,
  LAST_SHIPMENT ,
  INUSE,  '" + REQ_ID + @"' as  REQ_ID  FROM TEMPLOYEE WHERE SEMPLOYEEID  = '" + CommonFunction.ReplaceInjection(EMP_ID) + @"'
  AND NOT EXISTS(SELECT * FROM TEMPLOYEE_TEMP WHERE SEMPLOYEEID  = '" + CommonFunction.ReplaceInjection(EMP_ID) + @"' AND REQ_ID = '" + REQ_ID + @"')";
        AddTODB(QUERY_EMP);

        string QUERY_EMP_SAP = @"INSERT INTO TEMPLOYEE_SAP_TEMP (
   SEMPLOYEEID, FNAME, LNAME, 
   DCREATE, SCREATE, DUPDATE, 
   SUPDATE, PERS_CODE, CARRIER, 
   LICENSE_NO, DRVSTATUS,REQ_ID) 
SELECT SEMPLOYEEID ,
  FNAME ,
  LNAME ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  PERS_CODE ,
  CARRIER ,
  LICENSE_NO ,
  DRVSTATUS,  '" + REQ_ID + @"' as  REQ_ID  FROM TEMPLOYEE_SAP WHERE SEMPLOYEEID  = '" + CommonFunction.ReplaceInjection(EMP_ID) + @"'
  AND NOT EXISTS(SELECT * FROM TEMPLOYEE_SAP_TEMP WHERE SEMPLOYEEID  = '" + CommonFunction.ReplaceInjection(EMP_ID) + @"'  AND REQ_ID = '" + REQ_ID + @"')";
        AddTODB(QUERY_EMP_SAP);

        string QUERY_EMP_DOC = @"INSERT INTO TEMPLOYEE_DOC_TEMP (
   SEMPLOYEEID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH, NVERSION,REQ_ID) 
SELECT SEMPLOYEEID ,
  SDOCID ,
  SDOCVERSION ,
  SDOCTYPE ,
  SFILENAME ,
  SSYSFILENAME ,
  SDESCRIPTION ,
  CACTIVE ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  DEXPIRE ,
  SPATH ,
  NVERSION,
  '" + REQ_ID + @"' as  REQ_ID
  FROM TEMPLOYEE_DOC WHERE SEMPLOYEEID  = '" + CommonFunction.ReplaceInjection(EMP_ID) + @"'
  AND NOT EXISTS(SELECT * FROM TEMPLOYEE_DOC_TEMP WHERE SEMPLOYEEID  = '" + CommonFunction.ReplaceInjection(EMP_ID) + @"' AND SDOCID = TEMPLOYEE_DOC.SDOCID  AND REQ_ID = '" + REQ_ID + @"')";
        AddTODB(QUERY_EMP_DOC);
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    #region แนบเอกสารสำเนาบัตรประชาชน

    protected void upload_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region แนบเอกสารทะเบียนบ้าน

    protected void upload2_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory2, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory2, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory2, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region แนบเอกสารใบขับขี่ประเภท4

    protected void upload3_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Emp" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory3, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory3, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory3, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region อัพโหลดเอกสารอื่นๆ
    protected void gvwother_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePathother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathother") as ASPxTextBox;
            ASPxTextBox txtFileNameother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNameother") as ASPxTextBox;
            ASPxTextBox txtTruePathother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtTruePathother") as ASPxTextBox;
            ASPxTextBox txtSysfilenameother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtSysfilenameother") as ASPxTextBox;
            ASPxLabel lblData2 = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwother") as ASPxButton;
            ASPxButton btnDelFilegvwother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwother") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePathother.Text))
            {
                btnViewgvwother.ClientVisible = true;
                btnDelFilegvwother.ClientVisible = true;
                btnViewgvwother.ClientEnabled = true;
                btnDelFilegvwother.ClientEnabled = true;

            }
            else
            {
                btnViewgvwother.ClientVisible = true;
                btnDelFilegvwother.ClientVisible = true;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }
            txtFileNameother.ClientInstanceName = txtFileNameother.ClientInstanceName + "_" + VisibleIndex;
            txtFilePathother.ClientInstanceName = txtFilePathother.ClientInstanceName + "_" + VisibleIndex;
            txtTruePathother.ClientInstanceName = txtTruePathother.ClientInstanceName + "_" + VisibleIndex;
            txtSysfilenameother.ClientInstanceName = txtSysfilenameother.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwother.ClientInstanceName = btnViewgvwother.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwother.ClientInstanceName = btnDelFilegvwother.ClientInstanceName + "_" + VisibleIndex;

            // Add Event
            btnViewgvwother.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathother.ClientInstanceName + ".GetValue()+" + txtSysfilenameother.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwother.ClientSideEvents.Click = "function (s, e) { gvwother.PerformCallback(\"deleteUploadother; " + txtFilePathother.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + ";" + txtFileNameother.ClientInstanceName.ToString() + ";" + txtFilePathother.ClientInstanceName.ToString() + " ;" + btnViewgvwother.ClientInstanceName.ToString() + " ; " + btnDelFilegvwother.ClientInstanceName.ToString() + "\");}";
            //btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    protected void uploaderother_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string VendorID = Session["SESSIONEMPID"] + "";
        string USER = Session["UserID"] + "";
        string[] _Filename = e.UploadedFile.FileName.Split('.');
        int ncol = _Filename.Length - 1;

        //if (_Filename[ncol] == "xlsx" || _Filename[ncol] == "xls" || _Filename[ncol] == "doc" || _Filename[ncol] == "docx" || _Filename[ncol] == "pdf" || _Filename[ncol] == "jpg" || _Filename[ncol] == "jpeg")
        //{

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Emp" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory4, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory4, Session["SESSIONEMPID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                listGridother.Add(new ListGridother
                {
                    SFILENAME = e.UploadedFile.FileName,
                    SSYSFILENAME = genName + "." + _Filename[count],
                    SPATH = data,
                    STRUEPATH = data2,
                    SVENDORID = VendorID,
                    NEWPICTRUEFLAG = "1",
                    SDOCTYPE = "4",
                    INDEX = listGridother.Count
                });
            }
        }
        else
        {

            return;

        }
        //}
        //else
        //{
        //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถอับเอกสารประเภทนี้ได้');");
        //}
    }

    protected void gvwother_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxUploadControl Upload = gvwother.FindEditRowCellTemplateControl(null, "uploaderother") as ASPxUploadControl;
        Upload.ClientVisible = true;
        e.Cancel = true;
        //Listgvwdoc4();
        gvwother.CancelEdit();

    }

    protected void gvwother_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string VendorID = Session["SESSIONEMPID"] + "";
            string USER = Session["UserID"] + "";


            switch (e.CallbackName)
            {

                case "CUSTOMCALLBACK":


                    string[] param = e.Args[0].Split(';');
                    if (CanWrite)
                    {
                        switch (param[0])
                        {

                            case "Newgvwother": gvwother.AddNewRow();
                                gvwother.Visible = true;


                                //VisibleControlUpload();
                                break;
                            case "deleteUploadother":


                                dynamic delete = gvwother.GetRowValues(int.Parse(param[3] + ""), "SVENDORID", "SDOCID", "SDOCTYPE", "SPATH", "SSYSFILENAME");

                                try
                                {
                                    DeleteGrid.Add(new DeleteItem
                                    {
                                        SVENDORID = delete[0],
                                        SDOCID = delete[1],
                                        SDOCTYPE = delete[2],
                                        SPATHALL = delete[3] + delete[4]
                                    });

                                    int index = int.Parse(param[3]);
                                    listGridother.RemoveAt(index);
                                    Session["nosavedeleteother"] = "deletenotsave";
                                    Listgvwother();
                                }
                                catch
                                {

                                }

                                break;
                        }

                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(gvwother, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;





            }
        }
    }

    void Listgvwother()
    {
        string SVENDORID = Session["SESSIONEMPID"] + "";

        //เช็คว่าค่าในList กับในดาต้าเบสเท่ากันไหม ถ้าเมท่ากันคือSaveแล้วให้Setdata ใหม่ แต่ถ้าไม่เท่ากันสแดงว่ายังไม่ได้เซฟให้เอาลิสมาริสข้อมูลใหม่
        if (txtUploadother.Text != "S")
        {
            string chkDocInDB = @"SELECT SEMPLOYEEID as SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                  SCREATE, DUPDATE, SUPDATE,  DEXPIRE, SPATH FROM TEMPLOYEE_DOC_TEMP 
                                  WHERE  SEMPLOYEEID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE = '1' AND SDOCTYPE = '4'";
            DataTable chkData = new DataTable();
            chkData = CommonFunction.Get_Data(conn, chkDocInDB);
            var dblist = listGridother.Where(w => w.SDOCTYPE == "4").ToList();
            if (listGridother.Count > 0)
            {//เช็คว่าข้อมูลมีในเบสไหม




                if (dblist.Count > 0)
                {//เช็คว่าในลิสมีข้อมูลไหม ถ้าในเบสมีข้อมูลแต่ถ้าใน List ไม่มี คือบายข้อมูลผิด

                    gvwother.DataSource = dblist;
                    gvwother.DataBind();

                }
                else
                {
                    gvwother.DataBind();
                    gvwother.Visible = true;
                    gvwother.AddNewRow();

                }

            }
            else
            {
                //if (ViewState["nosavedeleteother"] + "" == "deletenotsave")//เช็คว่าลบแล้วแต่ยังไม่ได้เซฟ
                //{
                var dblist2 = listGridother.ToList();
                if (dblist2.Count > 0)
                {//เช็คว่าข้อมูลมีในเบสไหม
                    gvwother.DataSource = dblist2;
                    gvwother.DataBind();




                }
                else
                {
                    gvwother.DataSource = dblist2;
                    gvwother.DataBind();
                    gvwother.Visible = true;
                    gvwother.AddNewRow();

                }

                //}
                //else
                //{
                //    if (chkData.Rows.Count > 0)//เช็คจากข้อมูลในเบสจริงว่ามีจริงถ้ามีจริงให้ลิสแต่ถ้า
                //    {
                //        Setdata();
                //    }
                //    else
                //    {
                //        gvwother.DataSource = dblist;
                //        gvwother.DataBind();
                //        gvwother.Visible = true;
                //        gvwother.AddNewRow();
                //    }
                //}

            }
        }

    }
    #endregion

    //#region แนบเอกสารสำคัญอื่นๆ

    //protected void upload4_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    //{
    //    string[] _Filename = e.UploadedFile.FileName.Split('.');

    //    if (_Filename[0] != "")
    //    {
    //        int count = _Filename.Count() - 1;
    //        ASPxUploadControl upl = (ASPxUploadControl)sender;

    //        string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
    //        if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
    //        {
    //            string data = string.Format(TempDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
    //            string data2 = string.Format(SaverDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
    //            //e.CallbackData = data;

    //            e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];

    //        }
    //    }
    //    else
    //    {

    //        return;

    //    }
    //}

    //#endregion

    #region รูปประจำตัว

    protected void uploadEMP_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        if (CanWrite)
        {
            string[] _Filename = e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                int count = _Filename.Count() - 1;
                ASPxUploadControl upl = (ASPxUploadControl)sender;

                string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFileEmp(e.UploadedFile, genName, string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
                {
                    string data = string.Format(TempDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                    string data2 = string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                    //e.CallbackData = data;
                    //  ShowEmpImage(data2, genName + "." + _Filename[count]);
                    e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                    listGriddoc = new List<ListGridDoc>();
                    listGriddoc.Add(new ListGridDoc
                    {
                        SFILENAME = e.UploadedFile.FileName + "",
                        SSYSFILENAME = genName + "." + _Filename[count] + "",
                        SDESCRIPTION = "รูปประจำตัว",
                        SPATH = data2
                    });
                }
            }
            else
            {

                return;

            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }

    #endregion

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void VisibleControlUpload()
    {
        if (!(string.IsNullOrEmpty(txtFilePath.Text)))
        {
            bool visible = string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = visible;
            txtFileName.ClientVisible = !visible;
            btnView.ClientEnabled = !visible;
            btnDelFile.ClientEnabled = !visible;


            chkUpload1.Value = "1";
        }
        else
        {
            bool visible = !string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = !visible;
            txtFileName.ClientVisible = visible;
            btnView.ClientEnabled = visible;
            btnDelFile.ClientEnabled = visible;
            chkUpload1.Value = "";
        }

        if (!(string.IsNullOrEmpty(txtFilePath2.Text)))
        {
            bool visible2 = string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = visible2;
            txtFileName2.ClientVisible = !visible2;
            btnView2.ClientEnabled = !visible2;
            btnDelFile2.ClientEnabled = !visible2;


            chkUpload2.Value = "1";
        }
        else
        {
            bool visible2 = !string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = !visible2;
            txtFileName2.ClientVisible = visible2;
            btnView2.ClientEnabled = visible2;
            btnDelFile2.ClientEnabled = visible2;
            chkUpload2.Value = "";
        }


        if (!(string.IsNullOrEmpty(txtFilePath3.Text)))
        {
            bool visible3 = string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = visible3;
            txtFileName3.ClientVisible = !visible3;
            btnView3.ClientEnabled = !visible3;
            btnDelFile3.ClientEnabled = !visible3;


            chkUpload3.Value = "1";
        }
        else
        {
            bool visible3 = !string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = !visible3;
            txtFileName3.ClientVisible = visible3;
            btnView3.ClientEnabled = visible3;
            btnDelFile3.ClientEnabled = visible3;
            chkUpload3.Value = "";
        }


        //if (!(string.IsNullOrEmpty(txtFilePath4.Text)))
        //{

        //    bool visible4 = string.IsNullOrEmpty(txtFilePath4.Text);
        //    uploader4.ClientVisible = visible4;
        //    txtFileName4.ClientVisible = !visible4;
        //    btnView4.ClientEnabled = !visible4;
        //    btnDelFile4.ClientEnabled = !visible4;


        //    chkUpload4.Value = "1";
        //}
        //else
        //{
        //    bool visible4 = !string.IsNullOrEmpty(txtFilePath4.Text);
        //    uploader4.ClientVisible = !visible4;
        //    txtFileName4.ClientVisible = visible4;
        //    btnView4.ClientEnabled = visible4;
        //    btnDelFile4.ClientEnabled = visible4;
        //    chkUpload4.Value = "";
        //}

    }

    private void AddhistoryEMP(string EMPID)
    {
        //        var SendData = listEmp.Where(w => w.SEMPLOYEEID == EMPID).FirstOrDefault();

        //        //แปลงค่า SEMPTPYE
        //        string SEMPTPYE = "";
        //        if (!string.IsNullOrEmpty(SendData + ""))
        //        {
        //            int num = 0;
        //            int EMPTPYE = int.TryParse(SendData.SEMPTPYE, out num) ? num : 0;

        //            if (EMPTPYE == 0)
        //            {
        //                SEMPTPYE = "null";
        //            }
        //            else
        //            {
        //                SEMPTPYE = EMPTPYE + "";
        //            }


        //        }
        //        using (OracleConnection con = new OracleConnection(conn))
        //        {

        //            if (con.State == ConnectionState.Closed)
        //            {
        //                con.Open();
        //            }
        //            else
        //            {

        //            }

        //            //เช็คว่ามีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
        //            if (SendData != null)
        //            {
        //                var chkHistory = historyEmp.Where(w => w.SEMPLOYEEID == EMPID).OrderByDescending(o => o.NVERSION).FirstOrDefault();



        //                //เช็คว่า HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
        //                if (chkHistory != null)
        //                {
        //                    //ถ้าเคยมีข้อมูลแล้ว

        //                    int NVERSION = int.Parse(chkHistory.NVERSION + "") + 1;


        //                    string strQuery = @"INSERT INTO TEMPLOYEE_HISTORY(SEMPLOYEEID,SEMPTPYE,FNAME,LNAME,DBIRTHDATE,STEL,STEL2
        //                                      ,SMAIL,SMAIL2,SABBREVIATION,PERS_CODE,PERSONEL_BEGIN,PERSONEL_EXPIRE,SDRIVERNO,DDRIVEBEGIN,DDRIVEEXPIRE
        //                                      ,DCREATE,SCREATE,DUPDATE,SUPDATE,NVERSION,SFILENAME,SSYSFILENAME,SPATH,CAUSESAP,CAUSEOVER,CACTIVE,NOTFILL,BANSTATUS,CANCELSTATUS,CAUSESAPCANCEL,CAUSESAPCOMMIT) 
        //                                 VALUES('" + CommonFunction.ReplaceInjection(SendData.SEMPLOYEEID) + @"'
        //                                  ," + CommonFunction.ReplaceInjection(SEMPTPYE) + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.FNAME) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.LNAME) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DBIRTHDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL2) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL2) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SABBREVIATION) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.PERS_CODE) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_BEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_EXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SDRIVERNO) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEBEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEEXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DCREATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SCREATE) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DUPDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SUPDATE) + @"'
        //                                  ," + CommonFunction.ReplaceInjection(NVERSION + "") + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SFILENAME) + @"' 
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SSYSFILENAME) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SPATH) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAP) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSEOVER) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CACTIVE) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.NOTFILL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.BANSTATUS) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CANCELSTATUS) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCANCEL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCOMMIT) + @"')";
        //                    using (OracleCommand com = new OracleCommand(strQuery, con))
        //                    {
        //                        com.ExecuteNonQuery();
        //                    }
        //                }
        //                else
        //                {
        //                    //ถ้าไม่่เคยข้อมูล

        //                    //DateTime? DSTARTPTT = null;
        //                    //DateTime? DBEGINTRANSPORT = null;
        //                    //DateTime? DEXPIRETRANSPORT = null;
        //                    //DateTime? DBEGIN13BIS = null;
        //                    //DateTime? DEXPIRE13BIS = null;


        //                    string strQuery = @"INSERT INTO TEMPLOYEE_HISTORY(SEMPLOYEEID,SEMPTPYE,FNAME,LNAME,DBIRTHDATE,STEL,STEL2
        //                                      ,SMAIL,SMAIL2,SABBREVIATION,PERS_CODE,PERSONEL_BEGIN,PERSONEL_EXPIRE,SDRIVERNO,DDRIVEBEGIN,DDRIVEEXPIRE
        //                                      ,DCREATE,SCREATE,DUPDATE,SUPDATE,NVERSION,SFILENAME,SSYSFILENAME,SPATH,CAUSESAP,CAUSEOVER,CACTIVE,NOTFILL,BANSTATUS,CANCELSTATUS,CAUSESAPCANCEL,CAUSESAPCOMMIT) 
        //                                  VALUES('" + CommonFunction.ReplaceInjection(SendData.SEMPLOYEEID) + @"'
        //                                  ," + CommonFunction.ReplaceInjection(SEMPTPYE) + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.FNAME) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.LNAME) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DBIRTHDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL2) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL2) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SABBREVIATION) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.PERS_CODE) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_BEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_EXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SDRIVERNO) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEBEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEEXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DCREATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SCREATE) + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DUPDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SUPDATE) + @"'
        //                                  ," + CommonFunction.ReplaceInjection("1") + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SFILENAME) + @"' 
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SSYSFILENAME) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.SPATH) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAP) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSEOVER) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CACTIVE) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.NOTFILL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.BANSTATUS) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CANCELSTATUS) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCANCEL) + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCOMMIT) + @"')";
        //                    using (OracleCommand com = new OracleCommand(strQuery, con))
        //                    {
        //                        com.ExecuteNonQuery();
        //                    }
        //                }
        //            }
        //            else
        //            {

        //            }




        //        }
    }

    private void UploadFile2History(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath);
            }
        }
    }

    private void UploadFileToServer(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath + filename))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old + filename, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath + filename.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath + filename);
            }
        }
    }

    void FileToServer()
    {
        #region เก็บPathลงviewstate
        if (txtFilePath.Text != "")
        {

            UploadFileToServer(txtFilePath.Text, txtTruePath.Text, txtSysfilename.Text);
        }
        if (txtFilePath2.Text != "")
        {


            UploadFileToServer(txtFilePath2.Text, txtTruePath2.Text, txtSysfilename2.Text);
        }
        if (txtFilePath3.Text != "")
        {


            UploadFileToServer(txtFilePath3.Text, txtTruePath3.Text, txtSysfilename3.Text);
        }
        //if (txtFilePath4.Text != "")
        //{

        //    UploadFileToServer(txtFilePath4.Text, txtTruePath4.Text, txtSysfilename4.Text);
        //}
        var chknewDocother = listGridother.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
        if (chknewDocother.Count() > 0)
        {

            foreach (var item in chknewDocother)
            {
                ASPxTextBox txtSysfilenameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilenameother") as ASPxTextBox;
                ASPxTextBox txtFileNameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtFileNameother") as ASPxTextBox;
                ASPxTextBox txtTruePathother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtTruePathother") as ASPxTextBox;
                ASPxTextBox txtFilePathother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtFilePathother") as ASPxTextBox;
                try
                {
                    if (!string.IsNullOrEmpty(txtFilePathother.Text))
                    {

                        UploadFileToServer(txtFilePathother.Text, txtTruePathother.Text, txtSysfilenameother.Text);
                    }
                }
                catch
                {

                }
            }
        }
        #endregion
    }

    private bool UploadFileEmp(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void AddhistoryEmployeeDoc(string SEMPLOYEEID, string SDOCID, string SDOCTYPE)
    {
        //        string senddatatohistory = @"SELECT  SEMPLOYEEID, SDOCID, SDOCVERSION, 
        //   SDOCTYPE, SFILENAME, SSYSFILENAME, 
        //   SDESCRIPTION, CACTIVE, DCREATE, 
        //   SCREATE, DUPDATE, SUPDATE, 
        //   DEXPIRE, SPATH
        //                                      FROM TEMPLOYEE_DOC WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'  
        //                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
        //                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "' ";

        //        using (OracleConnection con = new OracleConnection(conn))
        //        {

        //            if (con.State == ConnectionState.Closed)
        //            {
        //                con.Open();
        //            }
        //            else
        //            {

        //            }
        //            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

        //            //เช็คว่า TVendor_DOC มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
        //            if (dt.Rows.Count > 0)
        //            {


        //                string datatohistory = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
        //                                      SCREATE, DUPDATE, SUPDATE,DEXPIRE, NVERSION,DATERECEIVE
        //                                      FROM TEMPLOYEE_DOC_HISTORY WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'  
        //                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
        //                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"' 
        //                                      ORDER BY NVERSION DESC";

        //                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
        //                //เช็คว่า TVendor_DOC_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
        //                DateTime? DCREATE = null;
        //                DateTime? DUPDATE = null;
        //                if (!string.IsNullOrEmpty(dt.Rows[0]["DCREATE"] + ""))
        //                {
        //                    DCREATE = DateTime.Parse(dt.Rows[0]["DCREATE"] + "");
        //                }
        //                if (!string.IsNullOrEmpty(dt.Rows[0]["DUPDATE"] + ""))
        //                {
        //                    DUPDATE = DateTime.Parse(dt.Rows[0]["DUPDATE"] + "");
        //                }

        //                if (dt2.Rows.Count > 0)
        //                {
        //                    //ถ้าเคยมีข้อมูลแล้ว
        //                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";

        //                    string strQuery = @"INSERT INTO TEMPLOYEE_DOC_HISTORY(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
        //                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE,SPATH,NVERSION,DATERECEIVE) 
        //                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SEMPLOYEEID"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
        //                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
        //                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";
        //                    using (OracleCommand com = new OracleCommand(strQuery, con))
        //                    {
        //                        com.ExecuteNonQuery();
        //                    }
        //                }
        //                else
        //                {
        //                    //ถ้าไม่่เคยข้อมูล
        //                    string nversion = "1";
        //                    string strQuery = @"INSERT INTO TEMPLOYEE_DOC_HISTORY(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
        //                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE ,SPATH,NVERSION,DATERECEIVE) 
        //                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SEMPLOYEEID"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
        //                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
        //                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
        //                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";
        //                    using (OracleCommand com = new OracleCommand(strQuery, con))
        //                    {
        //                        com.ExecuteNonQuery();
        //                    }
        //                }
        //            }
        //            else
        //            {

        //            }




        //        }
    }

    private void AddhistoryEmployeeAvatarDoc(string SEMPLOYEEID, string USER, string REQ_ID)
    {
        //        string senddatatohistory = @"SELECT 
        //           ROWID, SEMPLOYEEID, SDOCID, SDOCVERSION, 
        //           SDOCTYPE, SFILENAME, SSYSFILENAME, 
        //           SDESCRIPTION, CACTIVE, DCREATE, 
        //           SCREATE, DUPDATE, SUPDATE, 
        //           DEXPIRE, SPATH, NVERSION
        //        FROM TEMPLOYEE_AVATAR_DOC_TEMP
        //        WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"' AND REQ_ID = '" + REQ_ID + "' ORDER BY NVERSION DESC ";

        //        using (OracleConnection con = new OracleConnection(conn))
        //        {

        //            if (con.State == ConnectionState.Closed)
        //            {
        //                con.Open();
        //            }
        //            else
        //            {

        //            }
        //            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

        //            //เช็คว่า TVendor_DOC มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
        //            //var item = listGriddoc.FirstOrDefault();
        //            // var SendData = listEmp.Where(w => w.SEMPLOYEEID == SEMPLOYEEID).FirstOrDefault();
        //            string query = @"SELECT 
        //           SEMPLOYEEID,SFILENAME,SSYSFILENAME,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH
        //        FROM TEMPLOYEE where SEMPLOYEEID='" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + "'";

        //            DataTable dt2 = CommonFunction.Get_Data(con, query);

        //            DateTime? DCREATE = null;
        //            DateTime? DUPDATE = null;
        //            if (!string.IsNullOrEmpty(dt2.Rows[0]["DCREATE"] + ""))
        //            {
        //                DCREATE = DateTime.Parse(dt2.Rows[0]["DCREATE"] + "");
        //            }
        //            if (!string.IsNullOrEmpty(dt2.Rows[0]["DUPDATE"] + ""))
        //            {
        //                DUPDATE = DateTime.Parse(dt2.Rows[0]["DUPDATE"] + "");
        //            }

        //            if (dt.Rows.Count > 0)
        //            {
        //                //เช็คว่า TVendor_DOC_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ

        //                //ถ้าเคยมีข้อมูลแล้ว
        //                string nversion = (int.Parse(dt.Rows[0]["NVERSION"] + "") + 1) + "";
        //                string SDOCID = (int.Parse(dt.Rows[0]["SDOCID"] + "") + 1) + "";
        //                string SDOCVERSION = (int.Parse(dt.Rows[0]["SDOCVERSION"] + "") + 1) + "";

        //                string strQuery = @"INSERT INTO TEMPLOYEE_AVATAR_DOC_TEMP(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
        //                                                ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE ,SPATH,NVERSION,DATERECEIVE,REQ_ID) 
        //                                          VALUES('" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(SDOCID) + @"'
        //                                          ," + CommonFunction.ReplaceInjection(SDOCVERSION) + @"
        //                                          ,'" + CommonFunction.ReplaceInjection("5") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SFILENAME"] + "") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SSYSFILENAME"] + "") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection("รูปประจำตัว") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["CACTIVE"] + "") + @"'
        //                                          ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SCREATE"] + "") + @"'
        //                                          ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SUPDATE"] + "") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SPATH"] + "") + @"'
        //                                          ," + CommonFunction.ReplaceInjection(nversion) + @"
        //                                          ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 ) 
        //                                          ,'" + REQ_ID + "') ";

        //                using (OracleCommand com = new OracleCommand(strQuery, con))
        //                {
        //                    com.ExecuteNonQuery();
        //                }

        //            }
        //            else
        //            {
        //                //ถ้าไม่่เคยข้อมูล
        //                string strQuery = @"INSERT INTO TEMPLOYEE_AVATAR_DOC_TEMP(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
        //                                                ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE ,SPATH,NVERSION,DATERECEIVE,REQ_ID) 
        //                                          VALUES('" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'
        //                                          ," + CommonFunction.ReplaceInjection("1") + @"
        //                                          ," + CommonFunction.ReplaceInjection("1") + @"
        //                                          ,'" + CommonFunction.ReplaceInjection("5") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SFILENAME"] + "") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SSYSFILENAME"] + "") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection("รูปประจำตัว") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["CACTIVE"] + "") + @"'
        //                                         ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SCREATE"] + "") + @"'
        //                                         ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SUPDATE"] + "") + @"'
        //                                          ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SPATH"] + "") + @"'
        //                                          ," + CommonFunction.ReplaceInjection("1") + @"
        //                                          ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
        //                                          ,'" + REQ_ID + "') ";

        //                using (OracleCommand com = new OracleCommand(strQuery, con))
        //                {
        //                    com.ExecuteNonQuery();
        //                }
        //            }




        //        }
    }

    //void DeleteDocEmp()
    //{
    //        foreach (var item in DeleteGrid.ToList())
    //        {

    //            //string steDelete = @"UPDATE TVENDOR_SIGNER SET IS_ACTIVE='0' WHERE SVENDORID='" + item.SVENDORID + "' AND LINE_NO = '" + item.LINE_NO + "' ";
    //            //using (OracleCommand com = new OracleCommand(steDelete, con))
    //            //{
    //            //    com.ExecuteNonQuery();
    //            //}
    //        }

    //}

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
          FROM TVENDOR
         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

        //if (!IsPostBack)
        //{
        //    comboBox.Value = SystemFunction.GET_VENDORID(Session["UserID"] + "");
        //}
    }

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    private void setDatacallback()
    {
        if (!string.IsNullOrEmpty(txtEMPTruePath.Text) && !string.IsNullOrEmpty(txtEMPSysfilename.Text))
        {
            imgEmp.ImageUrl = txtEMPTruePath.Text + txtEMPSysfilename.Text;
        }
        else
        {
            imgEmp.ImageUrl = "images/Avatar.png";
        }

        VisibleControlUpload();
    }

    private bool SendMailToVendorRk()
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "ขอเพิ่มข้อมูล พขร. " + txtName.Text + " " + txtSurName.Text;
        string VENDOR_NAME = "";
        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {

            DataTable dt_MAIL = CommonFunction.Get_Data(conn, @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('61')");
            for (int i = 0; i < dt_MAIL.Rows.Count; i++)
            {
                _to += ";" + dt_MAIL.Rows[i]["SEMAIL"] + "";
            }

            _to = _to.Remove(0, 1);
        }
        string SVENDOR_ID = SystemFunction.GET_VENDORID(Session["UserID"] + "");
        //หาชื่อบริษัท
        DataTable dt_NAME = CommonFunction.Get_Data(conn, "SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = '" + SVENDOR_ID + "' ");
        if (dt_NAME.Rows.Count > 0)
        {
            VENDOR_NAME = dt_NAME.Rows[0]["SABBREVIATION"] + "";
        }
        #region html

        sHTML = @" <table width='600px' cellpadding='3' cellspacing='1' border='0' >
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง ขอเพิ่มข้อมูล พขร.
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน รข.</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   ทางบริษัท " + VENDOR_NAME + @" ได้ขอเพิ่มข้อมูล พขร.  """ + txtName.Text + " " + txtSurName.Text + @""" เพื่อให้ข้อมูลเป็นปัจจุบันและถูกต้องยิ่งขึ้น</td>
            </tr>
              <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>จึงเรียนมาเพื่อโปรดทราบ
                    และดำเนินการต่อไป</td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>" + VENDOR_NAME + @"
                </td>
            </tr>
            </table>";

        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(conn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }
    [Serializable]
    class sEmployee
    {
        public string SEMPLOYEEID { get; set; }
        public string SEMPTPYE { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public string STEL { get; set; }
        public string STEL2 { get; set; }
        public string SMAIL { get; set; }
        public string SMAIL2 { get; set; }
        public DateTime? DBIRTHDATE { get; set; }
        public string SABBREVIATION { get; set; }
        public string PERS_CODE { get; set; }
        public DateTime? PERSONEL_BEGIN { get; set; }
        public DateTime? PERSONEL_EXPIRE { get; set; }
        public string SDRIVERNO { get; set; }
        public DateTime? DDRIVEBEGIN { get; set; }
        public DateTime? DDRIVEEXPIRE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string CAUSESAP { get; set; }
        public string CAUSEOVER { get; set; }
        public string CACTIVE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SPATH { get; set; }
        public string NOTFILL { get; set; }
        public string BANSTATUS { get; set; }
        public string CAUSESAPCANCEL { get; set; }
        public string CANCELSTATUS { get; set; }
        public string CAUSESAPCOMMIT { get; set; }

    }
    [Serializable]
    class historyEmployee
    {
        public string SEMPLOYEEID { get; set; }
        public string SEMPTPYE { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public string STEL { get; set; }
        public string STEL2 { get; set; }
        public string SMAIL { get; set; }
        public string SMAIL2 { get; set; }
        public DateTime? DBIRTHDATE { get; set; }
        public string SABBREVIATION { get; set; }
        public string PERS_CODE { get; set; }
        public DateTime? PERSONEL_BEGIN { get; set; }
        public DateTime? PERSONEL_EXPIRE { get; set; }
        public string SDRIVERNO { get; set; }
        public DateTime? DDRIVEBEGIN { get; set; }
        public DateTime? DDRIVEEXPIRE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public Decimal NVERSION { get; set; }
        public string CAUSESAP { get; set; }
        public string CAUSEOVER { get; set; }
        public string CACTIVE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SPATH { get; set; }
        public string NOTFILL { get; set; }
        public string BANSTATUS { get; set; }
        public string CAUSESAPCANCEL { get; set; }
        public string CANCELSTATUS { get; set; }
        public string CAUSESAPCOMMIT { get; set; }
    }
    [Serializable]
    class ListGridDoc
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }

    }
    [Serializable]
    class ListGridother
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }
        public string NEWPICTRUEFLAG { get; set; }
        public int INDEX { get; set; }
    }
    [Serializable]
    class DeleteItem
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public string SDOCTYPE { get; set; }
        public string SPATHALL { get; set; }

    }
}