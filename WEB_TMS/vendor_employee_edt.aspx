﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_employee_edt.aspx.cs" Inherits="vendor_employee_edt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        #cph_Main_xcpn_rpnInformation_uploadEMP_TextBox0
        {
            padding: 0px;
            border-bottom-color: White !important;
            border-left-color: White !important;
            border-right-color: White !important;
            border-top-color: White !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            border-right-width: 0px !important;
            border-top-width: 0px !important;
            border-bottom-style: none !important;
            border-left-style: none !important;
            border-right-style: none !important;
            border-top-style: none !important;
        }
        #cph_Main_xcpn_rpnInformation_uploadEMP_ClearBox0
        {
            padding: 0px;
            border-bottom-color: White !important;
            border-left-color: White !important;
            border-right-color: White !important;
            border-top-color: White !important;
            border-bottom-width: 0px !important;
            border-left-width: 0px !important;
            border-right-width: 0px !important;
            border-top-width: 0px !important;
            border-bottom-style: none !important;
            border-left-style: none !important;
            border-right-style: none !important;
            border-top-style: none !important;
        }
    </style>
    <script type="text/javascript">



        function VisibleControlEMP() {
            if (!(txtEMPFilePath.GetValue() == "" || txtEMPFilePath.GetValue() == null)) {
                chkEMPUpload1.SetValue('1');

            } else {
                chkEMPUpload1.SetValue('');
            }
        }

        function VisibleControl() {
            var bool = txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null;
            uploader1.SetClientVisible(bool);
            txtFileName.SetClientVisible(!bool);
            btnView.SetEnabled(!bool);
            btnDelFile.SetEnabled(!bool);
            if (!(txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null)) {
                chkUpload1.SetValue('1');
            } else {
                chkUpload1.SetValue('');
            }

        }

        function VisibleControl2() {


            var bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
            uploader2.SetClientVisible(bool);
            txtFileName2.SetClientVisible(!bool);
            btnView2.SetEnabled(!bool);
            btnDelFile2.SetEnabled(!bool);
            if (!(txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null)) {
                chkUpload2.SetValue('1');
            } else {
                chkUpload2.SetValue('');
            }

        }

        function VisibleControl3() {


            var bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
            uploader3.SetClientVisible(bool);
            txtFileName3.SetClientVisible(!bool);
            btnView3.SetEnabled(!bool);
            btnDelFile3.SetEnabled(!bool);
            if (!(txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null)) {
                chkUpload3.SetValue('1');
            } else {
                chkUpload3.SetValue('');
            }

        }

        function VisibleControl4() {

            var bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
            uploader4.SetClientVisible(bool);
            txtFileName4.SetClientVisible(!bool);
            btnView4.SetEnabled(!bool);
            btnDelFile4.SetEnabled(!bool);
            if (!(txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null)) {
                chkUpload4.SetValue('1');
            } else {
                chkUpload4.SetValue('');
            }
        }

        function OnBirthdayValidation(s, e) {


            //            var Enddate = dedtendPercode.GetDate();
            //            var Startdate = dedtstartPercode.GetDate();

            //            if (Enddate == null) {
            //                e.isValid = false;
            //                e.errorText = "กรุณาระบุเเวลาอนุญาตบัตรประชาชน";
            //            }
            //            else {
            //                if (Enddate < Startdate) {
            //                    e.isValid = false;
            //                    e.errorText = "วันที่สิ้นสุดต้องไม่น้อยกว่าวันที่เริ่มต้น";
            //                }
            //            }
        }

        //        function OnValueChanged(s, e) {
        //            Page_ClientValidate(""); // undocumented
        //        }

        //        function validateDates(s, e) {
        //            var date1 = e1.GetDate();
        //            var date2 = e2.GetDate();
        //            e.IsValid = date1 == null || date2 == null || date1 < date2;
        //        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="รายละเอียดพนักงาน ">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="arp">
                            <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                            <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                            <table id="Table1" width="100%" runat="server">
                                <tr>
                                    <td>
                                    </td>
                                    <td align="right">
                                        <dx:ASPxButton runat="server" ID="btnHistoryEmp" Text="ประวัติพนักงาน" AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('viewHistoryemp'); }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%" valign="top">
                                        <table id="Table2" runat="server" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <dx:ASPxImage runat="server" ID="imgEmp" Width="175px" Height="200px" ClientInstanceName="imgEmp">
                                                    </dx:ASPxImage>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="32px">
                                                            </td>
                                                            <td>
                                                                <dx:ASPxUploadControl ID="uploadEMP" runat="server" ClientInstanceName="uploadEMP"
                                                                    NullText="Click here to browse files..." OnFileUploadComplete="uploadEMP_FileUploadComplete"
                                                                    Width="150px" CssClass="dxucTextBox_Aqua2">
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) {if(e.callbackData ==&#39;&#39;)
                 {dxWarning(&#39;แจ้งเตือน&#39;,&#39;ระบบสามารถ import file ได้เฉพาะ นามสกุล .jpg,.jpeg หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที&#39;);}
                 else{txtEMPFilePath.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[0]);txtEMPFileName.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[1]);txtEMPTruePath.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[2]);txtEMPSysfilename.SetValue((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[3]);imgEmp.SetImageUrl((e.callbackData+&#39;&#39;).split(&#39;|&#39;)[2]+(e.callbackData+&#39;&#39;).split(&#39;|&#39;)[3]);VisibleControlEMP();} }"
                                                                        TextChanged="function(s,e){uploadEMP.Upload();}"></ClientSideEvents>
                                                                    <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                        MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents TextChanged="function(s,e){uploadEMP.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .jpg,.jpeg หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtEMPFilePath.SetValue((e.callbackData+'').split('|')[0]);txtEMPFileName.SetValue((e.callbackData+'').split('|')[1]);txtEMPTruePath.SetValue((e.callbackData+'').split('|')[2]);txtEMPSysfilename.SetValue((e.callbackData+'').split('|')[3]);imgEmp.SetImageUrl((e.callbackData+'').split('|')[2]+(e.callbackData+'').split('|')[3]);VisibleControlEMP();} }" />
                                                                    <BrowseButton Text="เลือกรูปประจำตัว">
                                                                    </BrowseButton>
                                                                </dx:ASPxUploadControl>
                                                                <dx:ASPxTextBox ID="txtEMPFilePath" runat="server" ClientInstanceName="txtEMPFilePath"
                                                                    ClientVisible="False" Width="200px">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtEMPFileName" runat="server" ClientEnabled="False" ClientInstanceName="txtEMPFileName"
                                                                    ClientVisible="False" Width="200px">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtEMPTruePath" runat="server" ClientEnabled="False" ClientInstanceName="txtEMPTruePath"
                                                                    ClientVisible="False" Width="200px">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtEMPSysfilename" runat="server" ClientEnabled="False" ClientInstanceName="txtEMPSysfilename"
                                                                    ClientVisible="False" Width="200px">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="chkEMPUpload1" runat="server" ClientInstanceName="chkEMPUpload1"
                                                                    ForeColor="White" Width="1px">
                                                                    <Border BorderStyle="None" />
                                                                    <Border BorderStyle="None"></Border>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="70%">
                                        <table id="Table3" runat="server" width="100%">
                                            <tr>
                                                <td width="34%" align="right">
                                                    รหัสพนักงาน:
                                                </td>
                                                <td width="66%">
                                                    <table id="Table5" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtEmpID" Width="200px" SkinID="_txtRead" CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                        <RequiredField ErrorText="กรุณาระบุรหัสพนักงาน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox runat="server" ID="txtEMPSAPID" Width="200px" CssClass="dxeLineBreakFix"
                                                                    ClientVisible="false">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="กรุณาระบุรหัสพนักงาน"
                                                            class="dxeLineBreakFix" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    ตำแหน่ง:
                                                </td>
                                                <td>
                                                    <table id="Table7" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox runat="server" ID="cboPosition" TextField="PERSON_TYPE_DESC" ValueField="PERSON_TYPE"
                                                                    CssClass="dxeLineBreakFix" Width="200px">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                        <RequiredField ErrorText="กรุณาระบุตำแหน่งพนักงาน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents SelectedIndexChanged="function (s, e) { xcpn.PerformCallback('chagevalidate');  }" />
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                    SelectCommand="SELECT PERSON_TYPE, PERSON_TYPE_DESC FROM TEMPLOYEETYPES WHERE CACTIVE ='1'">
                                                                </asp:SqlDataSource>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="กรุณาระบุตำแหน่งพนักงาน"
                                                            class="dxeLineBreakFix" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    ชื่อ :
                                                </td>
                                                <td>
                                                    <table id="Table8" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtName" Width="200px" CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุชื่อพนักงาน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="กรุณาระบุชื่อพนักงาน"
                                                            class="dxeLineBreakFix" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    นามสกุล :
                                                </td>
                                                <td>
                                                    <table id="Table9" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtSurName" Width="200px" CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุนามสกุลพนักงาน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="กรุณาระบุชื่อพนักงาน"
                                                            class="dxeLineBreakFix" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    วันเกิด :
                                                </td>
                                                <td>
                                                    <table id="Table18" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxDateEdit runat="server" ID="dedtBirthDay" Width="120px" SkinID="xdte">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุวันเกิด" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    หมายเลขโทรศัพท์หลัก :
                                                </td>
                                                <td>
                                                    <table id="Table10" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtTel" Width="200px" CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุเบอร์โทรศัพท์หลัก" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    หมายเลขโทรศัพท์สำรอง :
                                                </td>
                                                <td>
                                                    <table id="Table11" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtTel2" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    อีเมล์หลัก :
                                                </td>
                                                <td>
                                                    <table runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtMail" Width="200px">
                                                                    <ValidationSettings EnableCustomValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true" ValidationGroup="add">
                                                                        <RequiredField ErrorText="กรุณาระบุ E-Mail หลัก" IsRequired="true"></RequiredField>
                                                                        <RegularExpression ErrorText="ใส่แบบฟอร์มอีเมลล์ไม่ถูกต้อง" ValidationExpression="<%$ Resources:ValidationResource, Valid_Email %>">
                                                                        </RegularExpression>
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    อีเมล์สำรอง :
                                                </td>
                                                <td>
                                                    <table id="Table17" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtMail2" Width="200px">
                                                                    <ValidationSettings EnableCustomValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true" ValidationGroup="add">
                                                                        <RegularExpression ErrorText="ใส่แบบฟอร์มอีเมลล์ไม่ถูกต้อง" ValidationExpression="<%$ Resources:ValidationResource, Valid_Email %>" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    บริษัทผู้ขนส่ง :
                                                </td>
                                                <td>
                                                    <table id="Table12" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox runat="server" ID="cboCompany" ValueField="SVENDORID" Width="200px"
                                                                    TextField="SABBREVIATION" CssClass="dxeLineBreakFix" DataSourceID="sdsCompany" Enabled="false" SkinID="_cboRead">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true" CausesValidation="True">
                                                                        <RequiredField ErrorText="กรุณาระบุบริษัทผู้ขนส่ง" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sdsCompany" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                    SelectCommand="SELECT SVENDORID, SABBREVIATION FROM TVENDOR"></asp:SqlDataSource>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="กรุณาระบุบริษัทผู้ขนส่ง"
                                                            class="dxeLineBreakFix" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    หมายเลขบัตรประชาชน :
                                                </td>
                                                <td>
                                                    <table id="Table13" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtPercode" Width="200px" CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="กรุณาระบุหมายเลขบัตรประชาชน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="กรุณาระบุหมายเลขบัตรประชาชน"
                                                            class="dxeLineBreakFix" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    ช่วงระยะเวลาอนุญาตบัตรประชาชน :
                                                </td>
                                                <td>
                                                    <table id="Table14" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxDateEdit runat="server" ID="dedtstartPercode" Width="120px" CssClass="dxeLineBreakFix"
                                                                    SkinID="xdte" ClientInstanceName="dedtstartPercode">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="กรุณาระบุเเวลาอนุญาตบัตรประชาชน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                    <%--<ClientSideEvents ValueChanged="OnValueChanged"/>--%>
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="ช่วงระยะเวลาอนุญาตบัตรประชาชน"
                                                            class="dxeLineBreakFix" />--%>
                                                                <dx:ASPxDateEdit runat="server" ID="dedtendPercode" Width="120px" CssClass="dxeLineBreakFix"
                                                                    SkinID="xdte" ClientInstanceName="dedtendPercode">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="กรุณาระบุเเวลาอนุญาตบัตรประชาชน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents Validation="OnBirthdayValidation" />
                                                                    <%--<ClientSideEvents ValueChanged="function(s,e){if(dedtstartPercode.GetDate() > s.GetDate()){OnBirthdayValidation(s,e);}else{alert('มากกว่า')} }" />--%>
                                                                </dx:ASPxDateEdit>
                                                                <%--<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="validateDates" ErrorMessage="CustomValidator" ></asp:CustomValidator>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    วันที่บริษัทจ้างงาน :
                                                </td>
                                                <td>
                                                    <table id="Table25" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxDateEdit runat="server" ID="dedStartWorkDay" Width="120px" SkinID="xdte">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                                        SetFocusOnError="true">
                                                                        <RequiredField ErrorText="กรุณาระบุวันที่บริษัทจ้างงาน" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    หมายเลขใบขับขี่ประเภท 4 :
                                                </td>
                                                <td>
                                                    <table id="Table16" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox runat="server" ID="txtdrivelicence" Width="200px" CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="กรุณาระบุหมายเลขใบขับขี่ประเภท 4" IsRequired="true"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td runat="server" id="drivelicence">
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px"
                                                            title="กรุณาระบุเหมายเลขใบขับขี่ประเภท 4" class="dxeLineBreakFix" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4 :
                                                </td>
                                                <td>
                                                    <table id="Table15" runat="server">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxDateEdit runat="server" ID="dedtstartlicence" Width="120px" SkinID="xdte"
                                                                    ClientInstanceName="dedtstartlicence" CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4" IsRequired="true">
                                                                        </RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                            <td>
                                                                <%--<img src="Images/alert.jpg" width="16px" height="16px" title="ช่วงระยะเวลาอนุญาตบัตรประชาชน"
                                                            class="dxeLineBreakFix" />--%>
                                                                <dx:ASPxDateEdit runat="server" ID="dedtEndlicence" Width="120px" SkinID="xdte" ClientInstanceName="dedtEndlicence"
                                                                    CssClass="dxeLineBreakFix">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="กรุณาระบุเวลาหมดอายุอนุญาตใบขับขี่ประเภท 4" IsRequired="true">
                                                                        </RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                                <%--<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="validateDates" ErrorMessage="CustomValidator" ></asp:CustomValidator>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%------------------------------------------------------------------เอกสารสำคัญของบริษัท---------------------------------------------------------------%>
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel1" ClientInstanceName="rpn" runat="server" Width="900px"
                                HeaderText="เอกสารสำคัญของพนักงาน">
                                <PanelCollection>
                                    <dx:PanelContent runat="server" ID="PanelContent1">
                                        <table id="Table4" width="100%" runat="server">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                    <dx:ASPxTextBox runat="server" ID="Up1" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="Up2" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="Up3" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="Up4" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="UpEMP" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td align="right">
                                                    <dx:ASPxTextBox runat="server" ID="txtchkUpdate" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="txtchkUpdate2" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="txtchkUpdate3" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox runat="server" ID="txtchkUpdate4" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxButton runat="server" ID="btnHistoryDoc" AutoPostBack="false" Text="เอกสารเก่า">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('viewHistory'); }">
                                                        </ClientSideEvents>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <font color="black">คำแนะนำระเบียบการใช้งาน<br>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สามารถอัพโหลดไฟล์ .jpg,.jpeg,.jpe,.gif,.png,.doc,.docx,.xlsx,.xls,.txt,.pdf,.ppt,.pptx,.mm,.zip,.rar
                                                        เท่านั้น และขนาดไม่เกิน 1 MB</font></td>
                                            </tr>
                                            <tr>
                                                <td width="35%" align="right">
                                                    แนบเอกสารสำเนาบัตรประชาชน:
                                                </td>
                                                <td width="65%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="middle" width="30%">
                                                                <dx:ASPxUploadControl ID="uploader1" runat="server" ClientInstanceName="uploader1"
                                                                    CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="upload_FileUploadComplete"
                                                                    Width="200px">
                                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                        MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents TextChanged="function(s,e){uploader1.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath.SetValue((e.callbackData+'').split('|')[0]);txtFileName.SetValue((e.callbackData+'').split('|')[1]);txtTruePath.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename.SetValue((e.callbackData+'').split('|')[3]);txtValidate.SetValue('.');VisibleControl();} }" />
                                                                    <BrowseButton Text="แนบไฟล์">
                                                                    </BrowseButton>
                                                                </dx:ASPxUploadControl>
                                                                <dx:ASPxTextBox ID="txtFilePath" runat="server" ClientInstanceName="txtFilePath"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtFileName" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtTruePath" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtSysfilename" runat="server" ClientEnabled="false" ClientInstanceName="txtSysfilename"
                                                                    ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="chkUpload1" runat="server" ClientInstanceName="chkUpload1" ForeColor="White"
                                                                    CssClass="dxeLineBreakFix" Width="1px" ClientVisible="false">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td width="16%" valign="middle" align="left">
                                                                <dx:ASPxButton ID="btnView" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnView" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath.GetValue()+ txtSysfilename.GetValue());}" />
                                                                    <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <dx:ASPxButton ID="btnDelFile" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnDelFile" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath.GetValue() +';1');}" />
                                                                    <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <%--<dx:ASPxButton ID="btnUpload" runat="server" AutoPostBack="false" Text="Upload" CssClass="dxeLineBreakFix">
                                                                    <ClientSideEvents Click="function(s,e){uploader1.Upload();}" />
<ClientSideEvents Click="function(s,e){uploader1.Upload();}"></ClientSideEvents>
                                                                </dx:ASPxButton>--%>
                                                            </td>
                                                            <td width="55%">
                                                                <dx:ASPxTextBox runat="server" ID="txtValidate" ForeColor="White" Width="1px" ClientInstanceName="txtValidate">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                        <RequiredField ErrorText="กรุณาแนบเอกสารสำเนาบัตรประชาชน" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%" align="right">
                                                    แนบเอกสารทะเบียนบ้าน:
                                                </td>
                                                <td width="65%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="middle" width="30%">
                                                                <dx:ASPxUploadControl ID="uploader2" runat="server" ClientInstanceName="uploader2"
                                                                    CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="upload2_FileUploadComplete"
                                                                    Width="200px">
                                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                        MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents TextChanged="function(s,e){uploader2.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath2.SetValue((e.callbackData+'').split('|')[0]);txtFileName2.SetValue((e.callbackData+'').split('|')[1]);txtTruePath2.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename2.SetValue((e.callbackData+'').split('|')[3]);txtValidate2.SetValue('.');VisibleControl2();} }" />
                                                                    <BrowseButton Text="แนบไฟล์">
                                                                    </BrowseButton>
                                                                </dx:ASPxUploadControl>
                                                                <dx:ASPxTextBox ID="txtFilePath2" runat="server" ClientInstanceName="txtFilePath2"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtFileName2" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName2"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtTruePath2" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath2"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtSysfilename2" runat="server" ClientEnabled="false" ClientInstanceName="txtSysfilename2"
                                                                    ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="chkUpload2" runat="server" ClientInstanceName="chkUpload2" CssClass="dxeLineBreakFix"
                                                                    ForeColor="White" Width="1px" ClientVisible="false">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td align="left" valign="middle" width="16%">
                                                                <dx:ASPxButton ID="btnView2" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnView2" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath2.GetValue()+ txtSysfilename2.GetValue());}" />
                                                                    <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <dx:ASPxButton ID="btnDelFile2" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnDelFile2" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile2;'+ txtFilePath2.GetValue() +';1');}" />
                                                                    <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <%--  <dx:ASPxDateEdit runat="server" ID="dedtUpload2" Width="220px" SkinID="xdte" CssClass="dxeLineBreakFix">
                                        </dx:ASPxDateEdit>--%>
                                                                <%--    <dx:ASPxButton ID="btnUpload2" runat="server" AutoPostBack="false" Text="Upload"
                                                                    CssClass="dxeLineBreakFix">
                                                                    <ClientSideEvents Click="function(s,e){ uploader2.Upload();}" />
<ClientSideEvents Click="function(s,e){ uploader2.Upload();}"></ClientSideEvents>
                                                                </dx:ASPxButton>--%>
                                                            </td>
                                                            <td width="55%">
                                                                <dx:ASPxTextBox runat="server" ID="txtValidate2" ForeColor="White" Width="1px" ClientInstanceName="txtValidate2">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                        <RequiredField ErrorText="กรุณาแนบเอกสารทะเบียนบ้าน" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%" align="right">
                                                    แนบเอกสารใบขับขี่ประเภท4:
                                                </td>
                                                <td width="65%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="middle" width="30%">
                                                                <dx:ASPxUploadControl ID="uploader3" runat="server" ClientInstanceName="uploader3"
                                                                    CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="upload3_FileUploadComplete"
                                                                    Width="200px">
                                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                        MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents TextChanged="function(s,e){uploader3.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath3.SetValue((e.callbackData+'').split('|')[0]);txtFileName3.SetValue((e.callbackData+'').split('|')[1]);txtTruePath3.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename3.SetValue((e.callbackData+'').split('|')[3]);txtValidate3.SetValue('.');VisibleControl3();} }" />
                                                                    <BrowseButton Text="แนบไฟล์">
                                                                    </BrowseButton>
                                                                </dx:ASPxUploadControl>
                                                                <dx:ASPxTextBox ID="txtFilePath3" runat="server" ClientInstanceName="txtFilePath3"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtFileName3" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName3"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtTruePath3" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath3"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtSysfilename3" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilename3"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="chkUpload3" runat="server" ClientInstanceName="chkUpload3" CssClass="dxeLineBreakFix"
                                                                    ForeColor="White" Width="1px" ClientVisible="false">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td width="16%" valign="middle" align="left">
                                                                <dx:ASPxButton ID="btnView3" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnView3" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath3.GetValue()+ txtSysfilename3.GetValue());}" />
                                                                    <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <dx:ASPxButton ID="btnDelFile3" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnDelFile3" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile3;'+ txtFilePath3.GetValue() +';1');}" />
                                                                    <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <%--<dx:ASPxDateEdit runat="server" ID="dedtUpload3" Width="220px" SkinID="xdte" CssClass="dxeLineBreakFix">
                                        </dx:ASPxDateEdit>--%>
                                                                <%--  <dx:ASPxButton ID="btnUpload3" runat="server" AutoPostBack="false" Text="Upload"
                                                                    CssClass="dxeLineBreakFix">
                                                                    <ClientSideEvents Click="function(s,e){ uploader3.Upload();}" />
<ClientSideEvents Click="function(s,e){ uploader3.Upload();}"></ClientSideEvents>
                                                                </dx:ASPxButton>--%>
                                                            </td>
                                                            <td width="55%">
                                                                <dx:ASPxTextBox runat="server" ID="txtValidate3" ForeColor="White" Width="1px" ClientInstanceName="txtValidate3">
                                                                    <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                        <RequiredField ErrorText="กรุณาแนบเอกสารใบขับขี่ประเภท 4" IsRequired="false" />
                                                                    </ValidationSettings>
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="35%" align="right" valign="top" style="padding-top: 10px;">
                                                    แนบเอกสารสำคัญอื่นๆ:
                                                </td>
                                                <td width="65%">
                                                    <dx:ASPxTextBox runat="server" ID="txtUploadother" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxGridView runat="server" ID="gvwother" AutoGenerateColumns="false" Width="100%"
                                                        ClientInstanceName="gvwother" OnHtmlDataCellPrepared="gvwother_OnHtmlDataCellPrepared"
                                                        KeyFieldName="SVENDORID" EnableRowsCache="False" OnRowInserting="gvwother_RowInserting"
                                                        OnAfterPerformCallback="gvwother_AfterPerformCallback" Border-BorderWidth="0px"
                                                        Border-BorderStyle="None" SkinID="sds" SettingsPager-AlwaysShowPager="true">
                                                        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                                        </ClientSideEvents>
                                                        <Columns>
                                                            <dx:GridViewDataColumn Caption="" FieldName="SVENDORID" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center" Visible="false" CellStyle-Border-BorderWidth="0px">
                                                                <%--<DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="lblPROD_ID" Width="100px" Text='<%# DataBinder.Eval(Container.DataItem,"SVENDORID") %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>--%>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="" FieldName="SFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-Border-BorderWidth="0px" Width="25%">
                                                                <EditItemTemplate>
                                                                    <dx:ASPxUploadControl ID="uploaderother" runat="server" ClientInstanceName="uploaderother"
                                                                        CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="uploaderother_FileUploadComplete"
                                                                        Width="200px">
                                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                            AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>" MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                        </ValidationSettings>
                                                                        <%--<ClientSideEvents FileUploadComplete="function(s, e) { gvwdoc4.CancelEdit();} " />--%>
                                                                        <ClientSideEvents TextChanged="function(s,e){uploaderother.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{gvwother.CancelEdit();} }" />
                                                                        <BrowseButton Text="แนบไฟล์">
                                                                        </BrowseButton>
                                                                    </dx:ASPxUploadControl>
                                                                </EditItemTemplate>
                                                                <DataItemTemplate>
                                                                    <dx:ASPxTextBox ID="txbDesc" runat="server" Enabled="false" Width="200px" Text='<%# DataBinder.Eval(Container.DataItem,"SFILENAME") %>'>
                                                                    </dx:ASPxTextBox>
                                                                </DataItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle>
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Width="18%" CellStyle-Cursor="hand" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center" Caption="การจัดการ" CellStyle-Border-BorderWidth="0px">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblData2" ClientVisible="false">
                                                                    </dx:ASPxLabel>
                                                                    <dx:ASPxTextBox ID="txtFilePathother" runat="server" ClientInstanceName="txtFilePathother"
                                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SPATH") %>'>
                                                                    </dx:ASPxTextBox>
                                                                    <dx:ASPxTextBox ID="txtFileNameother" runat="server" ClientEnabled="False" ClientInstanceName="txtFileNameother"
                                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SFILENAME") %>'>
                                                                    </dx:ASPxTextBox>
                                                                    <dx:ASPxTextBox ID="txtTruePathother" runat="server" ClientInstanceName="txtTruePathother"
                                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("STRUEPATH") %>'>
                                                                    </dx:ASPxTextBox>
                                                                    <dx:ASPxTextBox ID="txtSysfilenameother" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilenameother"
                                                                        ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SSYSFILENAME") %>'>
                                                                    </dx:ASPxTextBox>
                                                                    <dx:ASPxTextBox ID="chkUploadgvwdocother" runat="server" ClientInstanceName="chkUploadgvwdocother"
                                                                        ClientVisible="false" CssClass="dxeLineBreakFix" ForeColor="White" Width="1px">
                                                                        <Border BorderStyle="None" />
                                                                    </dx:ASPxTextBox>
                                                                    <dx:ASPxButton ID="btnViewgvwother" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                        ClientInstanceName="btnViewgvwother" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                                        EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd" Width="25px">
                                                                        <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnDelFilegvwother" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                        ClientInstanceName="btnDelFilegvwother" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                                        EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd" Width="25px">
                                                                        <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </DataItemTemplate>
                                                                <EditItemTemplate>
                                                                    <dx:ASPxButton ID="btnViewgvwothershow" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                        ClientInstanceName="btnViewgvwothershow" CssClass="dxeLineBreakFix" Cursor="pointer"
                                                                        Enabled="false" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                        Width="25px">
                                                                        <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnDelFilegvwothershow" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                        ClientInstanceName="btnDelFilegvwothershow" Enabled="false" CssClass="dxeLineBreakFix"
                                                                        Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                        Width="25px">
                                                                        <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </EditItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle Cursor="hand">
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn CellStyle-Border-BorderWidth="0px" Width="47%">
                                                                <DataItemTemplate>
                                                                    <%--<dx:ASPxButton ID="btnSaveImagedata" runat="server" AutoPostBack="false" SkinID="_Upload">
                                                        </dx:ASPxButton>--%>
                                                                </DataItemTemplate>
                                                                <EditItemTemplate>
                                                                    <%-- <dx:ASPxButton ID="btnSaveImage" runat="server" AutoPostBack="false" SkinID="_Upload">
                                                            <ClientSideEvents Click="function (s, e) {uploaderDoc4.Upload();}" />
                                                        </dx:ASPxButton>--%>
                                                                </EditItemTemplate>
                                                                <CellStyle>
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="" FieldName="SDOCID" HeaderStyle-HorizontalAlign="Center"
                                                                Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle>
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="" FieldName="SDOCTYPE" HeaderStyle-HorizontalAlign="Center"
                                                                Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle>
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="" FieldName="SPATH" HeaderStyle-HorizontalAlign="Center"
                                                                Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle>
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="" FieldName="SSYSFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                                Visible="false" CellStyle-Border-BorderWidth="0px" Width="25%">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle>
                                                                    <Border BorderWidth="0px"></Border>
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                        </Columns>
                                                        <SettingsPager Mode="ShowAllRecords">
                                                        </SettingsPager>
                                                        <SettingsEditing Mode="Inline" NewItemRowPosition="Bottom" />
                                                        <Settings ShowColumnHeaders="False" GridLines="None" />
                                                        <Styles>
                                                            <CommandColumn>
                                                                <BorderBottom BorderWidth="0px" />
                                                            </CommandColumn>
                                                        </Styles>
                                                        <Border BorderWidth="0px"></Border>
                                                    </dx:ASPxGridView>
                                                    <%--<table width="100%">
                                                        <tr>
                                                            <td align="left" valign="middle" width="30%">
                                                                <dx:ASPxUploadControl ID="uploader4" runat="server" ClientInstanceName="uploader4"
                                                                    CssClass="dxeLineBreakFix" NullText="Click here to browse files..." OnFileUploadComplete="upload4_FileUploadComplete"
                                                                    Width="200px">
                                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                        MultiSelectionErrorText="Attention! 

The following {0} files are invalid because they exceed the allowed file size ({1}) or their extensions are not allowed. These files have been removed from selection, so they will not be uploaded. 

{2}">
                                                                    </ValidationSettings>
                                                                    <ClientSideEvents TextChanged="function(s,e){uploader4.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}
                 else{txtFilePath4.SetValue((e.callbackData+'').split('|')[0]);txtFileName4.SetValue((e.callbackData+'').split('|')[1]);txtTruePath4.SetValue((e.callbackData+'').split('|')[2]);txtSysfilename4.SetValue((e.callbackData+'').split('|')[3]);VisibleControl4();} }" />
                                                                    <BrowseButton Text="แนบไฟล์">
                                                                    </BrowseButton>
                                                                </dx:ASPxUploadControl>
                                                                <dx:ASPxTextBox ID="txtFilePath4" runat="server" ClientInstanceName="txtFilePath4"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtFileName4" runat="server" ClientEnabled="False" ClientInstanceName="txtFileName4"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtTruePath4" runat="server" ClientEnabled="False" ClientInstanceName="txtTruePath4"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="txtSysfilename4" runat="server" ClientEnabled="False" ClientInstanceName="txtSysfilename4"
                                                                    ClientVisible="False" Width="200px" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxTextBox>
                                                                <dx:ASPxTextBox ID="chkUpload4" runat="server" ClientInstanceName="chkUpload4" CssClass="dxeLineBreakFix"
                                                                    ForeColor="White" Width="1px" ClientVisible="false">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td width="70%" valign="middle" align="left">
                                                                <dx:ASPxButton ID="btnView4" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnView4" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath4.GetValue()+ txtSysfilename4.GetValue());}" />
                                                                    <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                                <dx:ASPxButton ID="btnDelFile4" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    ClientEnabled="False" ClientInstanceName="btnDelFile4" CssClass="dxeLineBreakFix"
                                                                    Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                                    Width="25px">
                                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile4;'+ txtFilePath4.GetValue() +';1');}" />
                                                                    <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                               <dx:ASPxButton ID="btnUpload4" runat="server" AutoPostBack="false" Text="Upload"
                                                                    CssClass="dxeLineBreakFix">
                                                                    <ClientSideEvents Click="function(s,e){ uploader4.Upload();}" />
<ClientSideEvents Click="function(s,e){ uploader4.Upload();}"></ClientSideEvents>
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="13%">
                                                            </td>
                                                            <td width="11%" align="right">
                                                                <dx:ASPxButton runat="server" ID="ASPxButton2" AutoPostBack="false" ToolTip="เพิ่มเอกสารอื่นๆ"
                                                                    CausesValidation="False" EnableTheming="False" EnableDefaultAppearance="False"
                                                                    SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){gvwother.PerformCallback('Newgvwother')}" />
                                                                    <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td valign="middle" width="76%">
                                                                เพิ่มเอกสารอื่นๆ
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                            <table runat="server" width="100%">
                                <tr>
                                    <td>
                                        <table width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    สถานะการใช้งาน :
                                                </td>
                                                <td width="28%">
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal"
                                                        Enabled="False">
                                                        <Items>
                                                            <dx:ListEditItem Text="อนุญาต" Value="1" />
                                                            <dx:ListEditItem Text="ระงับ" Value="0" />
                                                            <dx:ListEditItem Text="ยกเลิกใช้งาน" Value="2" />
                                                        </Items>
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                            SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาเลือกสถานะการใช้งาน" IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                                <td width="50%">
                                                    <dx:ASPxCheckBox runat="server" ID="chkNotfill" Text="ไม่เติมเต็มข้อมูล" Visible="false">
                                                    </dx:ASPxCheckBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="Typedefault">
                                    <td>
                                        <table id="Table24" width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    สาเหตุการอนุญาติ :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxMemo runat="server" ID="txtConfirm" Width="350px" Height="100px" CssClass="dxeLineBreakFix">
                                                        <%--  <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาระบุสาเหตุการอนุญาต" IsRequired="true" />
                                                        </ValidationSettings>--%>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="Type">
                                    <td>
                                        <table id="Table20" width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    ประเภทการระงับ :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxComboBox runat="server" ID="cboStatus" DataSourceID="sdsStatus" ValueField="STATUS_ID"
                                                        TextField="STATUS_TYPE" Width="240px">
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                            SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาเลือกสาเหตุที่ระงับ" IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sdsStatus" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                        SelectCommand="SELECT &quot;STATUS_ID&quot;, &quot;STATUS_TYPE&quot; FROM &quot;TEMPLOYEESTATUS&quot;">
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="status">
                                    <td>
                                        <table width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    สาเหตุที่ระงับจากระบบ SAP :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxMemo runat="server" ID="txtComment" Width="350px" Height="100px" CssClass="dxeLineBreakFix">
                                                        <%--  <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาระบุสาเหตุที่ระงับ" IsRequired="true" />
                                                        </ValidationSettings>--%>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="Type2">
                                    <td>
                                        <table id="Table22" width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    ประเภทการยกเลิกใช้งาน :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxComboBox runat="server" ID="cboStatus2" DataSourceID="sdsStatusCancel" TextField="STATUS_TYPECANCEL"
                                                        ValueField="STATUS_IDCANCEL" Width="200px">
                                                        <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip"
                                                            SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาเลือกประเภทการยกเลิกใช้งาน" IsRequired="true" />
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sdsStatusCancel" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                        SelectCommand="SELECT &quot;STATUS_IDCANCEL&quot;, &quot;STATUS_TYPECANCEL&quot; FROM &quot;TEMPLOYEESTATUSCANCEL&quot;">
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="status2">
                                    <td>
                                        <table id="Table23" width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    สาเหตุที่ยกเลิกใช้งานจากระบบ SAP :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxMemo runat="server" ID="txtstatus2" Width="350px" Height="100px" CssClass="dxeLineBreakFix">
                                                        <%-- <ValidationSettings Display="Dynamic" ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true">
                                                            <RequiredField ErrorText="กรุณาระบุสาเหตุที่ยกเลิกใช้งาน" IsRequired="true" />
                                                        </ValidationSettings>--%>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="Table21" width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    ใช้งานล่าสุด :
                                                </td>
                                                <td width="15%">
                                                    <dx:ASPxLabel runat="server" ID="lblDayuse">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="53%">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="22%" align="right">
                                                    วันที่ใช้งานล่าสุดเมื่อ :
                                                </td>
                                                <td width="15%">
                                                    <dx:ASPxLabel runat="server" ID="lbldateuse">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="53%" align="left">
                                                    วัน
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    สาเหตุการไม่ได้งานเกินกำหนด :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxMemo runat="server" ID="txtComment2" Width="350px" Height="100px" CssClass="dxeLineBreakFix"
                                                        SkinID="_memoRead">
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="Table19" width="100%" runat="server">
                                            <tr>
                                                <td width="22%" align="right">
                                                    วันที่อัพเดทข้อมูลล่าสุด :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxLabel runat="server" ID="lblDUPDATE">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="22%" align="right">
                                                    ผู้อัพเดทข้อมูลล่าสุด :
                                                </td>
                                                <td width="78%">
                                                    <dx:ASPxLabel runat="server" ID="lblSUPDATE">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table id="Table6" width="100%" runat="server">
                                <tr>
                                    <td bgcolor="#FFFFFF" style="width: 50%" align="right" colspan="2">
                                        <dx:ASPxButton ID="btnSubmitgvw" runat="server" SkinID="_submit" AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) { if(txtEMPSysfilename.GetText() == ''){ dxError('พบข้อผิดพลาด','แนบไฟล์รูปประจำตัว');  } else {  if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save');} }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                    <td style="width: 50%">
                                        <dx:ASPxButton ID="btnCancelgvw" runat="server" SkinID="_back">
                                            <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'vendor_employee.aspx'; }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
