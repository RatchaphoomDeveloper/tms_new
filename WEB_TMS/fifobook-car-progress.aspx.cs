﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Web.Configuration;
using System.Data.OracleClient;
using System.Data;
using System.Globalization;
using System.Data.OleDb;
using System.Text;

public partial class fifobook_car_importData : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/bookcar/{0}/{2}/{1}/";
    double def_Double;

    public static string progress = "1;0";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            popupControl.ShowOnPageLoad = true;
            //Set progress bar 1 and 0 percent
            progress = "1;0.0";
        }
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        DateTime outdate;

        string sMessageiterminal = "";

        switch (paras[0])
        {
            case "InsertExceltoDatabase":


                // ที่อยู่ไฟล์ และ Type ที่เลือกจากหน้า book-car
                string sDataUpload = Session["fifosDataUpload"] + "";
                string sImporttype = Request.QueryString["v"] + "";

                iTerminal itm = new iTerminal();
                string tempEmployeeID = "";

                #region InsertExceltoDatabase
                #region ImportExcel มาเก็บไว้ใน DataTable

                string _fileimport = Server.MapPath(sDataUpload);
                // Connection String < v.2007
                string _version = "<2007";
                string connectionString = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source='" + _fileimport + "';Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'";
                try
                {
                    int dataindex = 1;
                    if (Path.GetExtension(_fileimport).ToLower().Equals(".xlsx"))
                    {
                        // Connection String = v.2007
                        _version = "=2007";
                        connectionString = "Provider='Microsoft.ACE.OLEDB.12.0';Data Source='" + _fileimport + "';Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text'";
                        //dataindex = 2;
                    }
                    OleDbConnection excelcon = new OleDbConnection(connectionString);
                    excelcon.Open();
                    // datatable รวม
                    //DataTable alldt = new DataTable();
                    // หาชื่อ sheet

                    DataRowCollection _ar_dr = excelcon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows;

                    //var qq = _ar_dr[1].Table.Rows[0].ToString();
                    //_ar_dr[1] ตำแหน่ง sheet ของข้อมูลทีต้องการ
                    string _exxcelsheet = _ar_dr[0]["TABLE_NAME"].ToString();
                    string sqlstr = "SELECT [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL] FROM [" + _exxcelsheet + "] GROUP BY [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL] ";
                    string sqlstr1 = "SELECT * FROM [" + _exxcelsheet + "] ";
                    DataTable dt = new DataTable();
                    DataTable dt1 = new DataTable();
                    new OleDbDataAdapter(sqlstr, excelcon).Fill(dt);//ข้อมูลมูลในExcel GROUP BY [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL]
                    new OleDbDataAdapter(sqlstr1, excelcon).Fill(dt1);
                    #region เช็คTemplate Excel
                    string[] arrayColumn = { "Deliv date", "TimeWindow", "Supply Plnt", "Vehicle-Head", "Vehicle-Tail", "Drop No", "Delivery", "SHIP-TO" };
                    string sMsgLossColumn = "";
                    if (arrayColumn.Length != dt1.Columns.Count)
                    {
                        foreach (string scolname in arrayColumn)
                        {
                            if (dt1.Columns.IndexOf(scolname) == -1)
                            {
                                sMsgLossColumn += "," + scolname;
                            }
                        }
                        if (sMsgLossColumn != "")
                        {
                            xcpn.JSProperties["cpError"] = "กรุณาตรวจสอบTemplate Excel <br>เนื่องจากขาดคอลัมป์ <b>" + sMsgLossColumn.Remove(0, 1) + "</b>!";
                            return;
                        }

                    } //return;




                    #endregion

                    excelcon.Close();

                #endregion
                    //DataRow[] sdtchk = dt1.Select("'' + [DELIV DATE] = '' AND ([VEHICLE-HEAD] is not null or [VEHICLE-HEAD] <> '')");
                    DataRow[] sdtchk = dt1.Select("'' + isnull('' + [DELIV DATE],'') = '' AND (([VEHICLE-HEAD] is not null or [VEHICLE-HEAD] <> '') OR ([VEHICLE-TAIL] is not null or [VEHICLE-TAIL] <> '')) ");
                    if (sdtchk.Count() > 0)
                    {
                        xcpn.JSProperties["cpError"] = "กรุณากรอกวันที่จัดแผน!";
                        return;
                    }

                    DataRow[] sdtchkt = dt1.Select("'' + isnull('' + [VEHICLE-HEAD],'') = '' AND ([VEHICLE-TAIL] is not null or [VEHICLE-TAIL] <> '')");
                    if (sdtchkt.Count() > 0)
                    {
                        xcpn.JSProperties["cpError"] = "กรุณาระบุทะเบียนหัว!";
                        return;
                    }

                    int OldRecord = 0;
                    int newRecord = 0;


                    //Progress 
                    double nprogress = 0.0;
                    double ncount = Convert.ToDouble(dt.Rows.Count);



                    //เช็คเงื่อนไข
                    int nCheckError = 0;
                    StringBuilder DetailAlertList = new StringBuilder("<table class='tbDetailAlert' width='100%' border='1' align='center' cellpadding='0' cellspacing='0'");

                    DetailAlertList.Append("<tr><th>Deliv date</th><th>TimeWindow</th><th>Supply Plnt</th><th>Vehicle-Head</th><th>Vehicle-Tail</th><th>Drop No</th><th>Delivery</th><th>Ship-to</th><th>Remark</th></tr>");

                    string FormateError = "<tr><td>&nbsp;{0}&nbsp;</td><td>&nbsp;{1}&nbsp;</td><td>&nbsp;{2}&nbsp;</td><td>&nbsp;{3}&nbsp;</td><td>&nbsp;{4}&nbsp;</td><td>&nbsp;{5}&nbsp;</td><td>&nbsp;{6}&nbsp;</td><td>&nbsp;{7}&nbsp;</td><td>&nbsp;{8}&nbsp;</td></tr>";

                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        if (con.State == ConnectionState.Closed) con.Open();

                        string NPLANPLUS = "";

                        if (dt.Rows.Count > 0)
                        {

                            DateTime datee;



                            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            {
                                tempEmployeeID = "";
                                #region  for excel rows

                                if (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", out datee))
                                {
                                    #region ถ้าเช็คFormat วันที่ขนส่ง ผ่าน
                                    string[] dDelivery = ("" + dt.Rows[i][0]).Split('/');
                                    string formateDelivery = "";
                                    formateDelivery = (dDelivery[2].StartsWith("20")) ? "en-US" : "th-TH";
                                    DateTime datetime;
                                    DateTime DDELIVERY = (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", new CultureInfo(formateDelivery), DateTimeStyles.None, out datetime)) ? datetime : DateTime.Now;
                                    string tmpTerminal = dt.Rows[i]["SUPPLY PLNT"] + "";
                                    #region ///รถคันดังกล่าวถูกยืนยัน มา ณ คลังที่ระบุหรือไม่
                                    string sqlCheckTruck = @"SELECT f.SHEADREGISTERNO,f.SEMPLOYEEID
                FROM  TFIFO f
                WHERE  NVL(f.CACTIVE,'1') = '1' AND f.SHEADREGISTERNO LIKE '%" + dt.Rows[i]["VEHICLE-HEAD"] + "%' AND TO_CHAR(f.DDATE,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND f.STERMINAL LIKE '" + tmpTerminal + "'  GROUP BY  f.SHEADREGISTERNO,f.SEMPLOYEEID";

                                    //                                    string sqlCheckTruck = @"SELECT Tc.SHEADREGISTERNO
                                    //                FROM  (TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
                                    //                LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  
                                    //                WHERE  TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE '%" + dt.Rows[i]["VEHICLE-HEAD"] + "%' AND TF.DDATE = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') -1  /* AND f.STERMINAL LIKE AND nvl(ct.STERMINALID,'" + tmpTerminal + "') LIKE '" + tmpTerminal + "' */  GROUP BY  Tc.SHEADREGISTERNO";
                                    DataTable numTruckConfirm = CommonFunction.Get_Data(con, sqlCheckTruck);
                                    if (numTruckConfirm.Rows.Count < 1)
                                    {

                                        DetailAlertList.Append(string.Format(FormateError, dt.Rows[i]["DELIV DATE"] + "", dt.Rows[i]["TIMEWINDOW"] + "", dt.Rows[i]["SUPPLY PLNT"] + "", dt.Rows[i]["VEHICLE-HEAD"] + "", dt.Rows[i]["VEHICLE-TAIL"] + "", "", "", "", "ยังไม่ได้ถูกจัดคิว"));
                                        nCheckError++;
                                    }
                                    else
                                    {
                                        tempEmployeeID = numTruckConfirm.Rows[0]["SEMPLOYEEID"] + "";
                                    }
                                    #endregion

                                    string chkNPLAN = CommonFunction.Get_Value(con, "SELECT nPlanID FROM TPLANSCHEDULE WHERE CACTIVE = '1' AND To_Char(DDELIVERY,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND STIMEWINDOW = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND STERMINALID = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND SHEADREGISTERNO = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND nvl(STRAILERREGISTERNO,'1') = nvl('" + dt.Rows[i]["VEHICLE-TAIL"] + "','1')");
                                    //ข็อมูล
                                    int num1 = 0;
                                    int num2 = 0;

                                    DataRow[] sdt = dt1.Select("isnull([DELIV DATE],'') = '" + dt.Rows[i]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND isnull([VEHICLE-TAIL],'') = '" + dt.Rows[i]["VEHICLE-TAIL"] + "' ");



                                    if (sdt.Count() > 0)
                                    {

                                        int num = 0;
                                        string OutboundPlus = "";
                                        for (int o = 0; o < sdt.Count(); o++)
                                        {
                                            #region ///วนข้อมูล ตาม [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL]

                                            if (!string.IsNullOrEmpty(sdt[o]["DELIVERY"] + ""))
                                            {
                                                #region เช็ค เลขDO DELIVERY ว่าใส่มาหรือไม่
                                                DataRow[] drr = dt1.Select("DELIVERY = '" + sdt[o]["DELIVERY"] + "'");//ดึงข้อมูลDELIVERY ใน Excel 
                                                #region เช็ค เลขDO DELIVERY ซ้ำใน Excel
                                                if (drr.Count() <= 1)
                                                {
                                                    #region จัดแผนซ้ำใน Excel (วันที่ขนส่ง+รถ+คลัง+TimeWindow+Drop)
                                                    DataRow[] chkdrr = dt1.Select("isnull([DELIV DATE],'') = '" + sdt[o]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + sdt[o]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + sdt[o]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + sdt[o]["VEHICLE-HEAD"] + "' AND isnull([DROP NO],'') = '" + sdt[o]["DROP NO"] + "'");

                                                    if (chkdrr.Count() > 1)
                                                    {
                                                        DetailAlertList.Append(string.Format(FormateError, sdt[o]["DELIV DATE"] + "", sdt[o]["TIMEWINDOW"] + "", sdt[o]["SUPPLY PLNT"] + "", sdt[o]["VEHICLE-HEAD"] + "", sdt[o]["VEHICLE-TAIL"] + "", sdt[o]["DROP NO"] + "", sdt[o]["DELIVERY"] + "", sdt[o]["SHIP-TO"] + "", "อยู่ในรายการนำเข้าข้อมูล ซ้ำกัน"));
                                                        nCheckError++;
                                                    }
                                                    #endregion
                                                    OutboundPlus += ",'" + sdt[o]["DELIVERY"] + "'";
                                                    if (chkNPLAN == "")
                                                    {//ถ้า วันที่ขนส่ง+ทามวินโดว+คลัง+หัว+หาง ที่ระบุมาไม่มีอยู่ในระบบ
                                                        #region//ถ้า DDELIVERY,STIMEWINDOW,STERMINALID,SHEADREGISTERNO,STRAILERREGISTERNO ไม่เคยมีจัดแผน
                                                        if (("" + sImporttype).Contains("1")) //ทับรายการเก่าทุกรายการ
                                                        {
                                                            OracleCommand comcount = new OracleCommand("SELECT COUNT(*) FROM TPLANSCHEDULELIST  WHERE CACTIVE = '1' AND SDELIVERYNO = '" + sdt[o]["DELIVERY"] + "'", con);
                                                            int countDeliveryno = int.Parse("" + comcount.ExecuteScalar());
                                                            if (countDeliveryno > 0)
                                                            {
                                                                DetailAlertList.Append(string.Format(FormateError, sdt[o]["DELIV DATE"] + "", sdt[o]["TIMEWINDOW"] + "", sdt[o]["SUPPLY PLNT"] + "", sdt[o]["VEHICLE-HEAD"] + "", sdt[o]["VEHICLE-TAIL"] + "", sdt[o]["DROP NO"] + "", sdt[o]["DELIVERY"] + "", sdt[o]["SHIP-TO"] + "", "มีอยู่ในระบบแล้ว"));
                                                                nCheckError++;
                                                            }
                                                        }

                                                        int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULELIST pl ON P.NPLANID = PL.NPLANID WHERE  p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '"
                                                            + sdt[o]["VEHICLE-HEAD"] + "' AND P.STIMEWINDOW = '" + sdt[o]["TIMEWINDOW"]
                                                            + "' AND To_Char(DDELIVERY,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US"))
                                                            + "' AND p.STERMINALID ='" + sdt[o]["TIMEWINDOW"] + "' AND pl.NDROP = '" + sdt[o]["DROP NO"] + "'");
                                                        if (CheckCar > 0)
                                                        {
                                                            DetailAlertList.Append(string.Format(FormateError, sdt[o]["DELIV DATE"] + "", sdt[o]["TIMEWINDOW"] + "", sdt[o]["SUPPLY PLNT"] + "", sdt[o]["VEHICLE-HEAD"] + "", sdt[o]["VEHICLE-TAIL"] + "", sdt[o]["DROP NO"] + "", sdt[o]["DELIVERY"] + "", sdt[o]["SHIP-TO"] + "", "มีอยู่ในระบบแล้ว"));
                                                            nCheckError++;
                                                        }
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        #region//ถ้า DDELIVERY,STIMEWINDOW,STERMINALID,SHEADREGISTERNO,STRAILERREGISTERNO เคยมีจัดแผน
                                                        if (("" + sImporttype).Contains("1")) //ทับรายการเก่า
                                                        {
                                                            OracleCommand comcount = new OracleCommand("SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND SDELIVERYNO = '" + sdt[o]["DELIVERY"] + "' AND NPLANID != '" + chkNPLAN + "'", con);
                                                            int countDeliveryno = int.Parse("" + comcount.ExecuteScalar());
                                                            if (countDeliveryno > 0)
                                                            {

                                                                DetailAlertList.Append(string.Format(FormateError, sdt[o]["DELIV DATE"] + "", sdt[o]["TIMEWINDOW"] + "", sdt[o]["SUPPLY PLNT"] + "", sdt[o]["VEHICLE-HEAD"] + "", sdt[o]["VEHICLE-TAIL"] + "", sdt[o]["DROP NO"] + "", sdt[o]["DELIVERY"] + "", sdt[o]["SHIP-TO"] + "", "มีอยู่ในระบบแล้ว"));
                                                                nCheckError++;
                                                            }
                                                        }
                                                        #endregion
                                                    }

                                                }
                                                else if (drr.Count() > 1)
                                                {
                                                    DetailAlertList.Append(string.Format(FormateError, sdt[o]["DELIV DATE"] + "", sdt[o]["TIMEWINDOW"] + "", sdt[o]["SUPPLY PLNT"] + "", sdt[o]["VEHICLE-HEAD"] + "", sdt[o]["VEHICLE-TAIL"] + "", sdt[o]["DROP NO"] + "", sdt[o]["DELIVERY"] + "", sdt[o]["SHIP-TO"] + "", "อยู่ในรายการนำเข้าข้อมูล ซ้ำกัน"));
                                                    nCheckError++;
                                                }

                                                #endregion

                                                #endregion
                                            }
                                            #endregion
                                        }

                                        #region เช็คปริมานเกินความจุรถต่อเที่ยว
                                        DataTable dtGRP_DETWTRCK = CommonFunction.GroupDataTable(dt1, "DELIV DATE,TIMEWINDOW,VEHICLE-HEAD,VEHICLE-TAIL");
                                        foreach (DataRow drGRP_DETWTRCK in dtGRP_DETWTRCK.Rows)
                                        {
                                            string sDO_NO = "";
                                            if (!string.IsNullOrEmpty("" + drGRP_DETWTRCK["DELIV DATE"]) && !string.IsNullOrEmpty("" + drGRP_DETWTRCK["TIMEWINDOW"]) && !string.IsNullOrEmpty("" + drGRP_DETWTRCK["VEHICLE-HEAD"]))
                                            {
                                                foreach (DataRow drDO in dt1.Select("[DELIV DATE]='" + drGRP_DETWTRCK["DELIV DATE"] + "' AND [TIMEWINDOW]='" + drGRP_DETWTRCK["TIMEWINDOW"] + "' AND [VEHICLE-HEAD]='" + drGRP_DETWTRCK["VEHICLE-HEAD"] + "' "))
                                                {
                                                    sDO_NO += "," + drDO["DELIVERY"];
                                                }
                                            }
                                            sDO_NO = (sDO_NO.Length > 0) ? sDO_NO.Remove(0, 1) : "";
                                            string DO_TOTLE_VOLUMN = CommonFunction.Get_Value(con, "SELECT  SUM(nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0)) AS NVALUE FROM TDELIVERY WHERE DELIVERY_NO in ( '" + sDO_NO.Replace(",", "','") + "' )");

                                            string sTruck;

                                            if (!string.IsNullOrEmpty("" + drGRP_DETWTRCK["VEHICLE-TAIL"].ToString().Trim()))
                                            {
                                                sTruck = drGRP_DETWTRCK["VEHICLE-TAIL"] + "";
                                            }
                                            else
                                            {
                                                sTruck = drGRP_DETWTRCK["VEHICLE-HEAD"] + "";
                                            }

                                            string STOTALCAP = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + sTruck.Trim() + "' GROUP BY  T.NTOTALCAPACITY");
                                            double TRCK_TOTALCAP = double.TryParse(STOTALCAP, out def_Double) ? double.Parse(STOTALCAP) + 100 : 0.00;
                                            double DO_VOLUMN = double.TryParse(DO_TOTLE_VOLUMN, out def_Double) ? def_Double : 0.00;
                                            if (TRCK_TOTALCAP < DO_VOLUMN)
                                            {
                                                DetailAlertList.Append(string.Format(FormateError, "", drGRP_DETWTRCK["TIMEWINDOW"] + "", "", drGRP_DETWTRCK["VEHICLE-HEAD"] + "", "", "", sDO_NO.Replace("'", " "), "", "เกินปริมาณความจุของรถ ความจุของรถ : </b>" + ((TRCK_TOTALCAP > 0) ? (TRCK_TOTALCAP - 100) : 0) + "<b> ปริมาณที่จัดแผน : </b>" + DO_VOLUMN));
                                                nCheckError++;
                                            }
                                        }
                                        #endregion

                                    }
                                    #endregion
                                }
                                #endregion

                                //Progress bar 1
                                nprogress = Convert.ToDouble(i) / ncount * 100;
                                progress = "1;" + nprogress;
                            }
                        }



                        if (nCheckError > 0)
                        {
                            DetailAlertList.Append("</table>");
                            xcpn.JSProperties["cpError"] = DetailAlertList.ToString();
                            return;
                        }





                        #region หาขอบเขตของข้อมูลที่ต้องการนำเข้าและบันทึกลงดาต้าเบส


                        //Progress
                        nprogress = 0.0;

                        if (dt.Rows.Count > 0)
                        {//หาขอบเขตของข้อมูลที่ต้องการนำเข้า
                            string strsql = "INSERT INTO TPLANSCHEDULE(SCONTRACTNO,SCONTRACTID,SEMPLOYEEID,CCONFIRM,SVENDORID,nPlanID,nNo,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate,CFIFO) VALUES (:SCONTRACTNO,:SCONTRACTID,:SEMPLOYEEID,'1',:SVENDORID,:nPlanID,1,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate,'1')";
                            string strsql1 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,NVALUE) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:NVALUE)";
                            DateTime datee;
                            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            {
                                if (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", out datee))
                                {

                                    string[] dDelivery = ("" + dt.Rows[i][0]).Split('/');
                                    string formateDelivery = "";
                                    formateDelivery = (dDelivery[2].StartsWith("20")) ? "en-US" : "th-TH";

                                    DateTime datetime;
                                    DateTime DDELIVERY = (DateTime.TryParse(dt.Rows[i]["DELIV DATE"] + "", new CultureInfo(formateDelivery), DateTimeStyles.None, out datetime)) ? datetime : DateTime.Now;

                                    DateTime Now = DateTime.Now;
                                    DateTime DDELIVERYCONVERT = DDELIVERY.Date.Add(new TimeSpan(Now.Hour, Now.Minute, Now.Second));

                                    string chkNPLAN = CommonFunction.Get_Value(con, "SELECT nPlanID FROM TPLANSCHEDULE WHERE CACTIVE = '1' AND To_Char(DDELIVERY,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND STIMEWINDOW = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND STERMINALID = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND SHEADREGISTERNO = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND nvl(STRAILERREGISTERNO,'1') = nvl('" + dt.Rows[i]["VEHICLE-TAIL"] + "','1')");
                                    //ข็อมูล
                                    int num1 = 0;
                                    int num2 = 0;

                                    if (chkNPLAN == "") //ถ้าไม่มีรายการเดิม
                                    {
                                        string genid = CommonFunction.Gen_ID(con, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                                        using (OracleCommand com = new OracleCommand(strsql, con))
                                        {
                                            string sVendorID = "";
                                            string sContractID = "";
                                            string sContractNO = "";
                                            DataTable DataTemp = CommonFunction.Get_Data(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID,c.SCONTRACTID, C.SCONTRACTNO FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE REPLACE(REPLACE(T.SHEADREGISTERNO,'-',''),'.','') = REPLACE(REPLACE('" + dt.Rows[i]["VEHICLE-HEAD"] + "','-',''),'.','') ORDER BY C.SCONTRACTNO ");
                                            if (DataTemp.Rows.Count > 0)
                                            {
                                                sVendorID = DataTemp.Rows[0]["SVENDORID"] + "";
                                                sContractID = DataTemp.Rows[0]["SCONTRACTID"] + "";
                                                sContractNO = DataTemp.Rows[0]["SCONTRACTNO"] + "";
                                            }


                                            com.Parameters.Clear();
                                            com.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = sContractNO;
                                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = sContractID;
                                            com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = sVendorID;
                                            com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = tempEmployeeID;
                                            com.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
                                            com.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = dt.Rows[i]["SUPPLY PLNT"] + "";
                                            com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now;
                                            com.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = DDELIVERYCONVERT;
                                            com.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = "";
                                            com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = DDELIVERYCONVERT;
                                            com.Parameters.Add(":sTimeWindow", OracleType.Number).Value = int.TryParse(dt.Rows[i]["TIMEWINDOW"] + "", out num1) ? num1 : 0;
                                            com.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = dt.Rows[i]["VEHICLE-HEAD"] + "";
                                            com.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = dt.Rows[i]["VEHICLE-TAIL"] + "";
                                            com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com.ExecuteNonQuery();
                                        }

                                        //update fifo
                                        string strsql2222 = "UPDATE TFIFO SET CPLAN = '1' WHERE NVL(CACTIVE,'1') = '1' AND SHEADREGISTERNO LIKE '%" + dt.Rows[i]["VEHICLE-HEAD"] + "%' AND TO_Char(DDATE,'dd/MM/yyyy') = '" + DDELIVERYCONVERT.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND STERMINAL LIKE '" + dt.Rows[i]["SUPPLY PLNT"] + "'";
                                        using (OracleCommand com2 = new OracleCommand(strsql2222, con))
                                        {
                                            com2.ExecuteNonQuery();
                                        }



                                        DataRow[] sdt = dt1.Select("isnull([DELIV DATE],'') = '" + dt.Rows[i]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND isnull([VEHICLE-TAIL],'') = '" + dt.Rows[i]["VEHICLE-TAIL"] + "' ");

                                        if (sdt.Count() > 0)
                                        {
                                            int num = 0;

                                            string ValueTruck;
                                            if (!string.IsNullOrEmpty(dt.Rows[i]["VEHICLE-TAIL"] + ""))
                                            {
                                                ValueTruck = dt.Rows[i]["VEHICLE-TAIL"] + "";
                                            }
                                            else
                                            {
                                                ValueTruck = dt.Rows[i]["VEHICLE-HEAD"] + "";
                                            }

                                            string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num + 100 : 0;

                                            string _DL = "";

                                            for (int o = 0; o <= sdt.Count() - 1; o++)
                                            {
                                                newRecord += 1;
                                                string genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                                int NDROP = int.TryParse(sdt[o]["DROP NO"] + "", out num2) ? num2 : 0;

                                                DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE LPAD(DELIVERY_NO,10,'0') = LPAD('" + sdt[o]["DELIVERY"] + "',10,'0')");

                                                int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;

                                                string sqlValue = "";
                                                sqlValue = strsql1.Replace(":sPlanListID", genid1 + "").Replace(":nPlanID", genid + "").Replace(":nDrop", NDROP + "").Replace(":sDeliveryNo", "'" + sdt[o]["DELIVERY"] + "'").Replace(":sShipTo", (dtData.Rows.Count > 0) ? "'" + dtData.Rows[0][0] + "'" : "''").Replace(":NVALUE", NNVALUE + "");

                                                using (OracleCommand com1 = new OracleCommand(sqlValue, con))
                                                {

                                                    //com1.Parameters.Clear();
                                                    //com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genid1;
                                                    //com1.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
                                                    //com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
                                                    //com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = sdt[o]["DELIVERY"] + "";
                                                    //com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = (dtData.Rows.Count > 0) ? dtData.Rows[0][0] + "" : "";
                                                    //com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
                                                    com1.ExecuteNonQuery();

                                                    _DL += ";" + sdt[o]["DELIVERY"] + "";
                                                }

                                            }

                                            #region Webservices iterminal
                                            sMessageiterminal = itm.UpdateMAP(((_DL.Length > 0) ? _DL.Remove(0, 1) : ""), dt.Rows[i]["TIMEWINDOW"] + "", dt.Rows[i]["VEHICLE-HEAD"] + "", dt.Rows[i]["VEHICLE-TAIL"] + "", dt.Rows[i]["SUPPLY PLNT"] + "", tempEmployeeID, genid + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DDELIVERYCONVERT);
                                            #endregion
                                        }
                                    }
                                    else // มีรายการเดิม
                                    {
                                        if (("" + sImporttype).Contains("1")) //ทับรายการเก่า
                                        {
                                            string sValueDelivery = CommonFunction.Get_Value(con, "SELECT wm_concat(SDELIVERYNO) AS SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + chkNPLAN + "'");

                                            using (OracleCommand delcom = new OracleCommand("DELETE FROM TPLANSCHEDULELIST WHERE NPLANID = '" + chkNPLAN + "'", con))
                                            {
                                                delcom.ExecuteNonQuery();
                                            }


                                            itm.DeleteMAP(sValueDelivery, dt.Rows[i]["TIMEWINDOW"] + "", dt.Rows[i]["VEHICLE-HEAD"] + "", dt.Rows[i]["VEHICLE-TAIL"] + "", dt.Rows[i]["SUPPLY PLNT"] + "", "", chkNPLAN + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DDELIVERYCONVERT);

                                            DataRow[] sdt = dt1.Select("isnull([DELIV DATE],'') = '" + dt.Rows[i]["DELIV DATE"] + "' AND isnull([TIMEWINDOW],'') = '" + dt.Rows[i]["TIMEWINDOW"] + "' AND isnull([SUPPLY PLNT],'') = '" + dt.Rows[i]["SUPPLY PLNT"] + "' AND isnull([VEHICLE-HEAD],'') = '" + dt.Rows[i]["VEHICLE-HEAD"] + "' AND isnull([VEHICLE-TAIL],'') = '" + dt.Rows[i]["VEHICLE-TAIL"] + "' ");


                                            if (sdt.Count() > 0)
                                            {
                                                int num = 0;

                                                string ValueTruck;
                                                if (!string.IsNullOrEmpty(dt.Rows[i]["VEHICLE-TAIL"] + ""))
                                                {
                                                    ValueTruck = dt.Rows[i]["VEHICLE-TAIL"] + "";
                                                }
                                                else
                                                {
                                                    ValueTruck = dt.Rows[i]["VEHICLE-HEAD"] + "";
                                                }

                                                string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                                                int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num + 100 : 0;

                                                string _DL = "";

                                                for (int o = 0; o <= sdt.Count() - 1; o++)
                                                {
                                                    OldRecord += 1;
                                                    int NDROP = int.TryParse(sdt[o]["DROP NO"] + "", out num2) ? num2 : 0;

                                                    DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE lpad(DELIVERY_NO,10,'0') =  lpad('" + sdt[o]["DELIVERY"] + "',10,'0') ");

                                                    int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;

                                                    string genid1 = "";
                                                    string sqlValue = "";
                                                    genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                                    sqlValue = strsql1.Replace(":sPlanListID", genid1 + "").Replace(":nPlanID", chkNPLAN + "").Replace(":nDrop", NDROP + "").Replace(":sDeliveryNo", "'" + sdt[o]["DELIVERY"] + "'").Replace(":sShipTo", (dtData.Rows.Count > 0) ? "'" + dtData.Rows[0][0] + "'" : "''").Replace(":NVALUE", NNVALUE + "");

                                                    using (OracleCommand com1 = new OracleCommand(sqlValue, con))
                                                    {


                                                        //com1.Parameters.Clear();
                                                        //com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genid1);
                                                        //com1.Parameters.Add(":nPlanID", OracleType.Number).Value = chkNPLAN;
                                                        //com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
                                                        //com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = sdt[o]["DELIVERY"].ToString();
                                                        //com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = (dtData.Rows.Count > 0) ? dtData.Rows[0][0] + "" : "";
                                                        //com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
                                                        com1.ExecuteNonQuery();

                                                        _DL += ";" + sdt[o]["DELIVERY"] + "";
                                                    }

                                                }

                                                #region Webservices iterminal
                                                sMessageiterminal = itm.UpdateMAP(((_DL.Length > 0) ? _DL.Remove(0, 1) : ""), dt.Rows[i]["TIMEWINDOW"] + "", dt.Rows[i]["VEHICLE-HEAD"] + "", dt.Rows[i]["VEHICLE-TAIL"] + "", dt.Rows[i]["SUPPLY PLNT"] + "", tempEmployeeID, chkNPLAN + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DDELIVERYCONVERT);
                                                #endregion
                                            }

                                        }
                                    }
                                }


                                //Progress bar 2
                                nprogress = Convert.ToDouble(i) / ncount * 100;
                                progress = "2;" + nprogress;
                            }

                        }
                        #endregion
                    }

                    string iMessage = "";
                    if (sMessageiterminal.StartsWith("E"))
                    {
                        iMessage = " iTerminal Error : " + sMessageiterminal.Split(';')[1];
                    }


                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','บันทึกข้อมูลเรียบร้อยแล้ว มีรายการใหม่ " + newRecord + " รายการ ปรับปรุงรายการเก่า " + OldRecord + " รายการ  " + iMessage + "',function(){window.location='book-car.aspx?s=1'});");
                    SaveImportDetail(sDataUpload, sDataUpload);
                }
                catch (Exception ex)
                {
                    try
                    {
                        xcpn.JSProperties["cpError"] = "ไม่สามารถอ่านไฟล์ข้อมูลที่นำเข้าได้ กรุณาตรวจสอบไฟล์ข้อมูลที่นำเข้าใหม่อีกครั้ง<br > เนื่องจาก Error Massege: " + ex.Message;

                    }
                    catch
                    {
                        xcpn.JSProperties["cpError"] = "ไม่สามารถอ่านไฟล์ข้อมูลที่นำเข้าได้ กรุณาตรวจสอบไฟล์ข้อมูลเอกสารที่นำเข้าโดยถี่ถ้วนใหม่อีกครั้ง";
                    }
                    return;
                }



                #endregion

                break;
        }

    }

    protected void timerCallback_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        e.Result = progress;

    }


    void SaveImportDetail(string filename, string genname)
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            string strsql = "INSERT INTO TIMPORTFILE(NIMPORTID,SFILENAME,SGENFILENAME,SUSERNAME,SCREATE,DCREATE,CFIFO) VALUES (:NIMPORTID,:SFILENAME,:SGENFILENAME,:SUSERNAME,:SCREATE,SYSDATE,'1')";
            using (OracleCommand com = new OracleCommand(strsql, con))
            {
                string genImportID = CommonFunction.Gen_ID(con, "SELECT NIMPORTID FROM (SELECT NIMPORTID FROM TIMPORTFILE ORDER BY NIMPORTID DESC) WHERE ROWNUM <= 1");
                com.Parameters.Clear();
                com.Parameters.Add(":NIMPORTID", OracleType.Number).Value = Convert.ToInt32(genImportID);
                com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = filename;
                com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = genname;
                com.Parameters.Add(":SUSERNAME", OracleType.VarChar).Value = Session["UserName"] + "";
                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                com.ExecuteNonQuery();
            }
        }
    }

}