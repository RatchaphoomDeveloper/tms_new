﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="document_employee_history.aspx.cs" Inherits="document_employee_history"
    StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="ประวัติเอกสารสำคัญของพนักงาน">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="PanelContent1">
                            <table id="Table1" width="100%" runat="server">
                                <%-- <tr>
                        <td colspan="2">จัดการข้อมูลผู้ขนส่ง</td>
                    </tr>--%>
                                <tr>
                                    <td width="20%">ค้นหาเอกสารสำคัญของพนักงาน :</td>
                                    <td width="80%">
                                        <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="ประเภทเอกสาร,ชื่อเอกสาร"
                                            CssClass="dxeLineBreakFix" Width="220px">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnCancelSearch" runat="server" SkinID="_cancelsearch" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('CancelSearch'); }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvwDoc" AutoGenerateColumns="false" Width="100%"
                                            OnHtmlDataCellPrepared="gvwDoc_OnHtmlDataCellPrepared" KeyFieldName="SDOCVERSION">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ID" FieldName="SDOCID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="version" FieldName="SDOCVERSION" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataDateColumn Caption="วันที่เปลี่ยนแปลง" FieldName="DATERECEIVE" Width="10%"
                                                    HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                    <PropertiesDateEdit DisplayFormatString="g"></PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทเอกสาร" FieldName="SDESCRIPTION" HeaderStyle-HorizontalAlign="Center"
                                                    Width="40%" CellStyle-HorizontalAlign="Left">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ชื่อเอกสาร" FieldName="SFILENAME" HeaderStyle-HorizontalAlign="Center"
                                                    Width="30%" CellStyle-HorizontalAlign="Left">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันหมดอายุ" FieldName="DEXPIRE" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Width="20%">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="10%" CellStyle-Cursor="hand" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="การจัดการ">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblData2" ClientVisible="false">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxTextBox ID="txtFilePathgvwDoc" runat="server" ClientInstanceName="txtFilePathgvwDoc"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SPATH") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtFileNamegvwDoc" runat="server" ClientEnabled="False" ClientInstanceName="txtFileNamegvwDoc"
                                                            ClientVisible="false" Width="200px" CssClass="dxeLineBreakFix" Text='<%# Eval("SSYSFILENAME") %>'>
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="chkUploadgvwDoc" runat="server" ClientInstanceName="chkUploadgvwDoc"
                                                            CssClass="dxeLineBreakFix" ForeColor="White" Width="1px">
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxButton ID="btnViewgvwDoc" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientEnabled="False" ClientInstanceName="btnViewgvwDoc" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePathgvwDoc.GetValue()+ txtFileNamegvwDoc.GetValue());}" />
                                                            <Image Height="25px" Url="Images/view1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnDelFilegvwDoc" runat="server" AutoPostBack="False" CausesValidation="False"
                                                            ClientEnabled="False" ClientInstanceName="btnDelFilegvwDoc" CssClass="dxeLineBreakFix"
                                                            Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd"
                                                            Width="25px">
                                                            <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFilegvwDoc;'+ txtFilePathgvwDoc.GetValue() +';1');}" />
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
