﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="listloguser.aspx.cs" Inherits="listloguser" %>

<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dxwschs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" oncallback="xcpn_Callback" 
            onload="xcpn_Load" ClientInstanceName="xcpn">
        <PanelCollection>
            <dx:PanelContent>
            <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
            <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0">
                    <tr>
                        <td width="50%">
                            &nbsp;
                        </td>
                        <td> <%--<ClientSideEvents SelectedIndexChanged ="function (s, e) {cboTrailerRegist.PerformCallback(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANSPORTID'));txtVenderID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANSPORTID'));txtVenderName.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORNAME'));cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')) }" />--%>
                            <dx:ASPxComboBox ID="cboSearchNameAndOrgUser" runat="server" CallbackPageSize="10" SkinID="xcbbATC" Width="300px" 
                                ClientInstanceName="cboSearchNameAndOrgUser" EnableCallbackMode="True" ValueField="USER_NAME"
                                OnItemsRequestedByFilterCondition="cboSearchNameAndOrgUser_ItemsRequestedByFilterCondition" TextFormatString="{0},{1}">
                                <Columns>
                                    <dx:ListBoxColumn Caption="ชื่อผู้ใช้" FieldName="USER_NAME" Width="250px" />
                                    <dx:ListBoxColumn Caption="หน่วยงาน" FieldName="ORG" Width="200px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="listUserName" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dateEditStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dateEditEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboUserGroup" runat="server" Width="100px">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="- กลุ่มผู้ใช้งาน -" />
                                    <dx:ListEditItem Text="ผู้ขนส่ง" Value="1" />
                                    <dx:ListEditItem Text="Admin" Value="2" />
                                    <dx:ListEditItem Text="คลัง" Value="3" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboSearchMenu" runat="server" CallbackPageSize="30" Width="230px" SkinID="xcbbATC" 
                                ValueField="Value"
                                OnItemsRequestedByFilterCondition="cboMenu_ItemsRequestedByFilterCondition" TextFormatString="{0}">
                                <Columns>
                                    <dx:ListBoxColumn Caption="ชื่อเมนู" FieldName="Text" Width="250px" />
                                </Columns>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" Text="ค้นหา" Width="50px" AutoPostBack="false">
                            <%--ClientSideEvents ทำให้ไม่มีการ Refresh หน้าจอ--%>
                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}" />  
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvwLogUser" runat="server" SkinID="_gvw">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" FieldName="ARRANGE" ShowInCustomizationForm="True" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center"/>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เมนู" FieldName="MENU" ShowInCustomizationForm="True" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="วันที่" FieldName="DATES" ShowInCustomizationForm="True" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="กลุ่มผู้ใช้งาน" FieldName="USER_GROUP" ShowInCustomizationForm="True" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ใช้งาน" FieldName="USER_ACTIVE" ShowInCustomizationForm="True" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หน่วยงาน" FieldName="ORG" ShowInCustomizationForm="True" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="การทำงาน" FieldName="WORKING" ShowInCustomizationForm="True" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    </div>
    </form>
</body>
</html>
