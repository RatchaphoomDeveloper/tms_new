﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Data;
using System.Collections;
using System.Text;
using System.Web.Security;
using TMS_BLL.Transaction.SurpriseCheckYear;

public partial class ConfirmChecktruck : PageBase
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            LogUser("42", "R", "เปิดดูข้อมูลหน้า ตรวจสอบการตรวจสภาพรถ", "");
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {
        gvw.DataBind();
    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;

            case "view1":
                if (CanWrite)
                {
                    int IndexSend = int.Parse(e.Parameter.Split(';')[1]);

                    //dynamic NSURPRISECHECKIDSend = gvw.GetRowValues(IndexSend, "SCONTRACTID", "STRUCKID", "SHEADREGISTERNO", "STRAILERREGISTERNO", "SCONTRACTNO", "SVENDORNAME", "SVENDORID");
                    //byte[] plaintextBytes = Encoding.UTF8.GetBytes(NSURPRISECHECKIDSend[0] + "&" + NSURPRISECHECKIDSend[1] + "&" + NSURPRISECHECKIDSend[2] + "&" + NSURPRISECHECKIDSend[3] + "&" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "&" + "&" + NSURPRISECHECKIDSend[4] + "&" + NSURPRISECHECKIDSend[5] + "&" + NSURPRISECHECKIDSend[6]);
                    //string str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    //xcpn.JSProperties["cpRedirectOpen"] = "SurpriseCheckAddEdit.aspx?str=" + str;

                    dynamic NSURPRISECHECKIDSend = gvw.GetRowValues(IndexSend, "SCONTRACTID", "STRUCKID", "SHEADREGISTERNO", "STRAILERREGISTERNO", "SCONTRACTNO", "SVENDORNAME", "SVENDORID", "IS_YEAR");
                    if (string.Equals(NSURPRISECHECKIDSend[6] + string.Empty, string.Empty))
                    {
                        byte[] plaintextBytes = Encoding.UTF8.GetBytes(NSURPRISECHECKIDSend[0] + "&" + NSURPRISECHECKIDSend[1] + "&" + NSURPRISECHECKIDSend[2] + "&" + NSURPRISECHECKIDSend[3] + "&" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "&" + "&" + NSURPRISECHECKIDSend[4] + "&" + NSURPRISECHECKIDSend[5] + "&" + NSURPRISECHECKIDSend[6]);
                        string str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                        xcpn.JSProperties["cpRedirectOpen"] = "SurpriseCheckAddEdit.aspx?str=" + str;
                    }
                    else
                    {
                        DataTable dt = SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSelectBLL(" AND SHEADREGISTERNO = '" + NSURPRISECHECKIDSend[2] + string.Empty + "' AND IS_ACTIVE IN (0,2) AND SVENDORID = '" + NSURPRISECHECKIDSend[6] + string.Empty + "' AND (USERCHECK = " + Session["UserID"].ToString() + " OR SUID IN (" + Session["UserID"].ToString() + "))");
                        if (dt.Rows.Count > 0)
                        {
                            byte[] plaintextBytes = Encoding.UTF8.GetBytes(NSURPRISECHECKIDSend[0] + "&" + NSURPRISECHECKIDSend[1] + "&" + NSURPRISECHECKIDSend[2] + "&" + NSURPRISECHECKIDSend[3] + "&" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "&" + "&" + NSURPRISECHECKIDSend[4] + "&" + NSURPRISECHECKIDSend[5] + "&" + NSURPRISECHECKIDSend[6] + "&" + NSURPRISECHECKIDSend[7] + "&" + dt.Rows[0]["ID"] + string.Empty);
                            string str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                            xcpn.JSProperties["cpRedirectOpen"] = "SurpriseCheckAddEdit.aspx?str=" + str;
                        }
                        else
                        {
                            byte[] plaintextBytes = Encoding.UTF8.GetBytes(NSURPRISECHECKIDSend[0] + "&" + NSURPRISECHECKIDSend[1] + "&" + NSURPRISECHECKIDSend[2] + "&" + NSURPRISECHECKIDSend[3] + "&" + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "&" + "&" + NSURPRISECHECKIDSend[4] + "&" + NSURPRISECHECKIDSend[5] + "&" + NSURPRISECHECKIDSend[6]);
                            string str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                            xcpn.JSProperties["cpRedirectOpen"] = "SurpriseCheckAddEdit.aspx?str=" + str;
                        }


                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_topic_add.aspx");

    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }


    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
