﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Admin_VisitForm_Config_Add.aspx.cs" Inherits="Admin_VisitForm_Config_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="uplAdd" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>แบบฟอร์มการทำงาน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div class="form-group form-horizontal row">
                                <label for="ddlYear" class="col-md-2 control-label">ปีใช้งาน</label>
                                <div class="col-md-3">
                                    <asp:DropDownList ID="ddlYear" runat="server" Width="150px" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="ddlName" class="col-md-2 control-label">ชื่อแบบฟอร์ม</label>
                                <div class="col-md-3">
                                    <asp:DropDownList ID="ddlName" runat="server" CssClass="form-control marginTp" Width="400" DataValueField="NTYPEVISITFORMID" DataTextField="STYPEVISITFORMNAME">
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="form-group form-horizontal row">
                                <label for="rdoOptionStatus" class="col-md-2 control-label">สถานะการใช้งาน</label>
                                <div class="col-md-3 radio">
                                    <asp:RadioButtonList ID="rdoOptionStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                        <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                        <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="<%=txtPercent.ClientID %>" class="col-md-2 control-label">เปอร์เซ็นต์คำนวณรายปี</label>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtPercent" MaxLength="6" CssClass="form-control" runat="server" autocomplete="off" Width="150"></asp:TextBox>
                                </div>
                                <div class="col-md-7" style="text-align: right;">
                                    <asp:Button ID="btnAdd" runat="server" Text="บันทึก" CssClass="btn btn-hover btn-info" OnClick="btnAdd_Click" />
                                    <asp:Button ID="btnClose" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 100px" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" style="z-index: 1060" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" id="alertCopy">
                <div class="modal-dialog">
                    <div class="modal-content" style="text-align: left;">
                        <div class="modal-header">
                            <h4 class="modal-title">แจ้งเตือน</h4>
                        </div>
                        <div class="modal-body" style="word-wrap: break-word;">
                            <table style="width: 100%">
                                <tr>
                                    <td valign="top" align="left" style="font-size: 16px; vertical-align: central;" class="TextDetail">
                                        <div style="height: 40px; text-align: center; display: table-cell; vertical-align: middle;">
                                            <asp:Label ID="lblComfirmText" runat="server" Text="แบบฟอร์มนี้มีการประเมินแล้ว หากแก้ไขต้องดำเนินการกรอกข้อมูลประเมินใหม่"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Button runat="server" ID="btnSave" Text="แก้ไข" CssClass="btn btn-md bth-hover btn-info" OnClientClick="clickSave()" />
                            <asp:Button runat="server" ID="btnNotSave" Text="ยกเลิก" CssClass="btn btn-md bth-hover btn-warning" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <input type="text" name="hdnNum" id="hdnNum" runat="server" value="0">
        <asp:Button runat="server" ID="btnSaveAction" Text="คัดลอกฟอร์ม" CssClass="btn btn-md bth-hover btn-info" OnClick="btnSaveAction_Click" />
    </div>

    <script type="text/javascript">
        function clickSave() {
            $("#<%=btnSaveAction.ClientID%>").click();
        }
        function AlertCopy() {
            $('#alertCopy').find('input').attr('data-dismiss', 'modal');
            var $dialog = $('#alertCopy').find(".modal-dialog");
            var offset = (($(window).height() - $dialog.height()) / 2 - $dialog.height() / 2);
            if ($dialog.height() < 0) {
                offset = $(window).height() / 2 - 200;
            }

            offset = $(window).height() / 2 - 200;
            $('#alertCopy').find('.modal-dialog').css('margin-top', offset);
            $('#alertCopy').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

