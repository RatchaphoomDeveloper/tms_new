﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.XtraCharts;

public partial class ReportStatusInMonths : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            SetCboYear();

        }
        CreateReport();
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": CreateReport();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    void CreateReport()
    {
        string Condition = "";
        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            Condition = " AND to_char(NVL(REQUEST_DATE,CREATE_DATE),'mm') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "' AND to_char(NVL(REQUEST_DATE,CREATE_DATE),'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }


        string Query = @"SELECT  MS.STATUSREQ_ID,MS.STATUSREQ_NAME,LDS.CSTATUS_FLAG,LDS.SMONTH,LDS.SYEAR
FROM TBL_STATUSREQ MS
LEFT JOIN 
(
SELECT STATUS_FLAG,COUNT(STATUS_FLAG) as CSTATUS_FLAG,to_char(NVL(REQUEST_DATE,CREATE_DATE),'mm') as SMONTH ,to_char(NVL(REQUEST_DATE,CREATE_DATE),'yyyy')  as SYEAR
FROM TBL_REQUEST
WHERE 1=1 " + Condition + @" 
GROUP BY STATUS_FLAG,to_char(NVL(REQUEST_DATE,CREATE_DATE),'mm'),to_char(NVL(REQUEST_DATE,CREATE_DATE),'yyyy')
)LDS
ON MS.STATUSREQ_ID = LDS.STATUS_FLAG
WHERE MS.ISACTIVE_FLAG = 'Y'  ORDER BY MS.NORDER";

        DataTable dt = CommonFunction.Get_Data(conn, Query);

        string Query1 = @"SELECT '1' as NO FROM DUAL";

        DataTable dsFill = CommonFunction.Get_Data(conn, Query1);

        //gvw.DataSource = dt;
        //gvw.DataBind();SELECT  MS.STATUSREQ_ID,MS.STATUSREQ_NAME,LDS.CSTATUS_FLAG,LDS.SMONTH,LDS.SYEAR

        if (dt.Rows.Count > 0)
        {
            //1
            ReportStatusInmothChart report = new ReportStatusInmothChart();

            XRChart xrChart2 = report.FindControl("xrChart1", true) as XRChart;
            xrChart2.DataSource = dt;

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                xrChart2.Series[0].ArgumentScaleType = ScaleType.Qualitative;
                xrChart2.Series[0].ArgumentDataMember = "STATUSREQ_NAME";
                xrChart2.Series[0].ValueDataMembers[0] = "CSTATUS_FLAG";

            }
            report.Name = "ReportDailyStatus";
            ((XRLabel)report.FindControl("XRLabel1", true)).Text = "วันที่จัดทำรายงาน " + DateTime.Now.ToString("dd MMMM yyyy");
            if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
            {
                ((XRLabel)report.FindControl("XRLabel3", true)).Text = "รายงาน Daily Status ช่วงเดือน " + cboMonth.Text + " " + cboYear.Text + "";
            }
            else
            {

            }

            report.DataSource = dsFill;
            rvw.Report = report;
            rvw.DataBind();
            //report.CreateDocument();
        }

    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion
}