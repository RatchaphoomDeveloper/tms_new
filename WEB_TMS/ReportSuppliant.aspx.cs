﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Web.Configuration;
using DevExpress.XtraReports.UI;
using DevExpress.XtraCharts;

public partial class ReportSuppliant : System.Web.UI.Page
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            checkPermitionReport();

            for (int i = 1; i <= 12; i++)
            {
                string Month = DateTimeFormatInfo.GetInstance(new CultureInfo("th-TH")).GetMonthName(i);
                cmbStartMonth.Items.Add(new ListEditItem("เดือน " + Month, i));
                cmbEndMonth.Items.Add(new ListEditItem("เดือน " + Month, i));
            }

            cmbStartMonth.SelectedIndex = 0;
            cmbEndMonth.SelectedIndex = 11;

            string cYear = DateTime.Today.Year.ToString();
            int nYear = Convert.ToInt16(cYear);
            for (int i = 0; i <= 20; i++)
            {
                cmbYear.Items.Add(nYear + 543 + "", nYear);
                nYear -= 1;

            }
            cmbYear.SelectedIndex = 0;

        }
        else
        {
            CreateReport();
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }



    protected void BuildChart(XRChart chart)
    {

        string sqlquery = string.Format(@"SELECT VS.SVENDORNAME,COUNT(AP.SAPPEALID) AS NAPPEAL,ROUND((COUNT(AP.SAPPEALID) / (SELECT COUNT(SAPPEALID) FROM TAPPEAL WHERE TO_DATE('1/' || TO_CHAR(DINCIDENT,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY'))) * 100,2) AS NPERSENT  
FROM TAPPEAL AP LEFT JOIN TVENDOR_SAP VS ON AP.SVENDORID = VS.SVENDORID WHERE TO_DATE('1/' || TO_CHAR(ap.DINCIDENT,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY')  GROUP BY VS.SVENDORNAME ORDER BY VS.SVENDORNAME", "1/" + cmbStartMonth.Value + "/" + cmbYear.Value, "1/" + cmbEndMonth.Value + "/" + cmbYear.Value);

        DataTable dt1 = CommonFunction.Get_Data(sql, sqlquery);

        chart.DataSource = dt1;
        chart.Series[0].ArgumentScaleType = ScaleType.Qualitative;
        chart.Series[0].ArgumentDataMember = "SVENDORNAME";
        chart.Series[0].ValueDataMembers[0] = "NAPPEAL";

        chart.Series[1].ArgumentScaleType = ScaleType.Qualitative;
        chart.Series[1].ArgumentDataMember = "SVENDORNAME";
        chart.Series[1].ValueDataMembers[0] = "NPERSENT";

    }

    protected void checkPermitionReport()
    {
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;

    }

    protected void CreateReport()
    {
//        string sqlquery = string.Format(@"SELECT  ROW_NUMBER () OVER (ORDER BY TO_CHAR(DAPPEAL,'MM')) AS ID, TO_CHAR(DAPPEAL,'MONTH') AS SMONTH,SUM(nvl(NAPPEAL,0)) AS NAPPEAL,SUM(nvl(NFINAL,0)) AS NFINAL
//--,SUM(nvl(NWAIT,0)) AS NWAIT 
//,SUM(nvl(NAPPEAL,0)) - SUM(nvl(NFINAL,0))  AS NWAIT 
//FROM(
//SELECT  ap.DAPPEAL,
//CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '3' THEN 1 ELSE 0 END AS NAPPEAL,
//CASE WHEN ap.CSTATUS = '3' THEN 1 ELSE 0 END AS NFINAL
//--CASE WHEN ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' OR ap.CSTATUS = '6' THEN 1 ELSE 0  END AS NWAIT
//FROM TAPPEAL ap  WHERE TO_DATE('1/' || TO_CHAR(ap.DAPPEAL,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY')
//) GROUP BY TO_CHAR(DAPPEAL,'MONTH'),TO_CHAR(DAPPEAL,'MM')", "1/" + cmbStartMonth.Value + "/" + cmbYear.Value, "1/" + cmbEndMonth.Value + "/" + cmbYear.Value);

        string sqlquery = string.Format(@"SELECT  ROW_NUMBER () OVER (ORDER BY TO_CHAR(DINCIDENT,'MM')) AS ID, TO_CHAR(DINCIDENT,'MONTH') AS SMONTH,SUM(nvl(NAPPEAL,0)) AS NAPPEAL,SUM(nvl(NFINAL,0)) AS NFINAL
--,SUM(nvl(NWAIT,0)) AS NWAIT 
,SUM(nvl(NAPPEAL,0)) - SUM(nvl(NFINAL,0))  AS NWAIT 
FROM(
SELECT  ap.DINCIDENT,
CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '3' THEN 1 ELSE 0 END AS NAPPEAL,
CASE WHEN ap.CSTATUS = '3' THEN 1 ELSE 0 END AS NFINAL
--CASE WHEN ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' OR ap.CSTATUS = '6' THEN 1 ELSE 0  END AS NWAIT
FROM TAPPEAL ap  WHERE TO_DATE('1/' || TO_CHAR(ap.DINCIDENT,'MM/YYYY'),'DD/MM/YYYY')  BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY')
) GROUP BY TO_CHAR(DINCIDENT,'MONTH'),TO_CHAR(DINCIDENT,'MM')", "1/" + cmbStartMonth.Value + "/" + cmbYear.Value, "1/" + cmbEndMonth.Value + "/" + cmbYear.Value);

        DataTable dt = CommonFunction.Get_Data(sql, sqlquery);

        SuppliantReport report = new SuppliantReport();

        report.DataSource = dt;
        report.Parameters["prYEAR"].Value = "ระหว่าง " + cmbStartMonth.Text + " ถึง " + cmbEndMonth.Text + " ปี " + cmbYear.Text;


        XRChart xrChart = report.FindControl("xrChart", true) as XRChart;
        BuildChart(xrChart);

        report.Name = "สรุปข้อมูลการขออุทธรณ์";

        this.ReportViewer1.Report = report;
    }
}