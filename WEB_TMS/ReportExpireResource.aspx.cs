﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;

public partial class ExpireResource : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {

            string sqlquery = @"SELECT
V.SABBREVIATION,
C.SCONTRACTNO
, SUM(CASE WHEN t.NTOTALCAPACITY <= 10000 AND t.SCARTYPEID = 0 THEN 1 ELSE 0 END) as NTRUCKSIX
,SUM(CASE WHEN t.NTOTALCAPACITY > 10000 AND t.SCARTYPEID = 0 THEN 1 ELSE 0 END) as NTRUCKTEN
,SUM(CASE WHEN  t.SCARTYPEID = 3 AND nvl((SELECT COUNT(tt.SHEADREGISTERNO) FROM TTRUCK tt WHERE tt.SCARTYPEID = '4' AND tt.SHEADREGISTERNO =  t.STRAILERREGISTERNO ),0) > 0 THEN 1 ELSE 0 END )as NTRUCKSEMI
,(SELECT wm_concat(SCP.STERMINALID || ' - ' || STS.STERMINALNAME) AS STERMINALNAME FROM TCONTRACT_PLANT SCP LEFT JOIN TTERMINAL STM ON SCP.STERMINALID = STM.STERMINALID LEFT JOIN  TTERMINAL_SAP STS ON STM.STERMINALID = STS.STERMINALID
WHERE NVL(SCP.CACTIVE,'1') != '0' AND NVL(STM.CACTIVE,'1') != '0'
AND SCP.SCONTRACTID = C.SCONTRACTID
 ) as SCONT_PLNT

  FROM TTRUCK t
   LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND nvl(T.STRAILERID,'0') = nvl(CT.STRAILERID,'0') LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID
   LEFT JOIN TVENDOR v ON C.SVENDORID = v.SVENDORID
  WHERE 1=1 
--and nvl(t.SCARTYPEID,0) != 4
--and nvl(c.SCONTRACTID,0) != 0
  AND nvl(t.CACTIVE,'Y') != 'N'
  AND nvl(c.CACTIVE,'Y') != 'N'
  AND nvl(v.CACTIVE,'Y') != 'N' 
  AND nvl(C.SCONTRACTNO,'N') != 'N' 

";

            int temp;
            int yEnd = (int.TryParse(cbxYearEnd.SelectedItem != null ? cbxYearEnd.Value.ToString() : "0", out temp) ? temp : 0) * (-1);
            if (dteStart.Value != null && dteEnd.Value != null)
            {
                //   sqlquery += " AND t.DSIGNIN BETWEEN TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND  TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";

                #region BackUpCode
                   //case "<":
                    //    sqlquery += " AND TRUNC(t.DSIGNIN) > TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                    //    break;
                    //case ">":
                    //    sqlquery += " AND TRUNC(t.DSIGNIN) < TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                    //    break;

                    //case "=":
                    //default:
                    //    sqlquery += " AND TRUNC(t.DSIGNIN) BETWEEN TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND  TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                    //    break;
                #endregion


                switch (cbxYear.Value.ToString())
                {   
                   case "<":
                        sqlquery += " AND TRUNC(t.DREGISTER) > TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                        break;
                    case ">":
                        sqlquery += " AND TRUNC(t.DREGISTER) < TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                        break;

                    case "=":
                    default:
                        sqlquery += " AND TRUNC(t.DREGISTER) BETWEEN TO_DATE('" + dteStart.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND  TO_DATE('" + dteEnd.Date.AddYears(yEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY')";
                        break;
                }
            }

            //if (cbxYear.Value.ToString() != "-1" )
            //{
            //    sqlquery += "AND trunc(months_between(sysdate,DSIGNIN)/12) = " + CommonFunction.ReplaceInjection(cbxYear.Value.ToString()); ;
            //}

            if (cbxYear.Value.ToString() != "-1")
            {
                int YearEnd = int.TryParse(cbxYearEnd.Value.ToString(), out temp) ? temp : 0;
                switch (cbxYear.Value.ToString())
                {
                    case "=":
                        sqlquery += " AND trunc(months_between(sysdate,DSIGNIN)/12) = " + YearEnd;

                        break;
                    case "<":
                        sqlquery += " AND trunc(months_between(sysdate,DSIGNIN)/12) < " + YearEnd;

                        break;
                    case ">":
                        sqlquery += " AND trunc(months_between(sysdate,DSIGNIN)/12) > " + YearEnd;

                        break;
                    default:
                        int YearStart = int.TryParse(cbxYear.Value.ToString(), out temp) ? temp : 0;
                        sqlquery += " AND trunc(months_between(sysdate,DSIGNIN)/12) Between " + YearStart + " AND " + YearEnd;
                        break;

                }

            }


            sqlquery += " GROUP BY V.SABBREVIATION,C.SCONTRACTNO,C.SCONTRACTID";

            //Response.Write(sqlquery);
            dsReport dsRep = new dsReport();



            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open();
                }



                using (OracleDataAdapter adapter = new OracleDataAdapter(sqlquery, con))
                {
                    OracleCommand com = new OracleCommand(sqlquery, con);

                    adapter.InsertCommand = com;
                    adapter.Fill(dsRep.dsExpireReport);
                }


            }

            DataTable dt = new DataTable();
            dt = CommonFunction.Get_Data(sql, sqlquery);


            //dsRep.dsExpireReport.Merge(dt);


            xrtExpireResource report = new xrtExpireResource();
            report.DataSource = dsRep.dsExpireReport;


            if (dteStart.Value != null && dteEnd.Value != null)
            {
                report.Parameters["pStart"].Value = dteStart.Date.ToString("dd/MM/yyyy");
                report.Parameters["pEnd"].Value = dteEnd.Date.ToString("dd/MM/yyyy");
            }
            report.Name = "รายงานค้นหารถตามอายุ";
            this.ReportViewer1.Report = report;
        }
        else
        {
            ListYear();
            ListYear2();

        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

    protected void ListYear()
    {
        cbxYear.Items.Add(new ListEditItem(" - ทั้งหมด - ", (-1).ToString()));
        cbxYear.Items.Add(new ListEditItem(" น้อยกว่า ", "<"));
        cbxYear.Items.Add(new ListEditItem(" มากกว่า ", ">"));
        cbxYear.Items.Add(new ListEditItem(" เท่ากับ ", "="));

        //for (int i = 15; i <= 100; i++)
        //{
        //    cbxYear.Items.Add(new ListEditItem(i.ToString(), i.ToString()));
        //}

        cbxYear.SelectedIndex = 0;
    }

    protected void ListYear2()
    {
        for (int i = 1; i <= 100; i++)
        {
            cbxYearEnd.Items.Add(new ListEditItem(i.ToString(), i.ToString()));
        }
    }
}