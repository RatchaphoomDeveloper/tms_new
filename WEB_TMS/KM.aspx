﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KM.aspx.cs" Inherits="KM" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div style="font-family: 'Century Gothic'">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>
                        <asp:Label ID="lblHeaderTab1" runat="server" Text="Knowledge Management (KM) Organizational Learning Center"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:Table runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblTopicID" runat="server" Text="Topic ID :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtTopicID" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblAuthorName" runat="server" Text="Author Name :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtAuthorName" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblKnowledgeType" runat="server" Text="Knowledge Type :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:DropDownList ID="ddlKnowledgeType" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px">
                                            </asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblDepartment" runat="server" Text="Department :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblKeyword" runat="server" Text="Keywords (tags) :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Width="45%">
                                                        <asp:DropDownList ID="ddlProductTagHead" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px" AutoPostBack="true" OnSelectedIndexChanged="ddlProductTagHead_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="10%" Style="text-align: center">
                                                    
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="45%">
                                                        <asp:DropDownList ID="ddlProductTagItem" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px" Enabled="false">
                                                        </asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblDivisioin" runat="server" Text="Division :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:DropDownList ID="ddlDivision" runat="server" CssClass="form-control" Enabled="false" Style="font-family: 'Century Gothic'" Font-Size="16px">
                                            </asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblOtherKeyword" runat="server" Text="Other keywords :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtOtherKeyword" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblCreateDateStart" runat="server" Text="Created Date :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Width="45%">
                                                        <asp:TextBox ID="txtCreateDateStart" runat="server" CssClass="datepicker" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="10%" Style="text-align: center">
                                                        <asp:Label ID="Label1" runat="server" Text="to &nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="45%">
                                                        <asp:TextBox ID="txtCreateDateEnd" runat="server" CssClass="datepicker" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>


                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblYear" runat="server" Text="Year :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtYear" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px" placeholder="กรอกข้อมูล ค.ศ."></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                        
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                        
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                                <br />

                                <div class="row">
                                    <div style="text-align: left" class="col-sm-6">
                                        <asp:ImageButton ID="cmdSearchBook" runat="server" ImageUrl="~/Images/ImageForKM/show bookmark.png" Width="15%" Height="15%" Style="cursor: pointer" OnClick="cmdSearchBook_Click" />
                                        <asp:ImageButton ID="cmdAddNewTopic" runat="server" ImageUrl="~/Images/ImageForKM/add new topic.png" Width="15%" Height="15%" Style="cursor: pointer" OnClick="cmdAddNewTopic_Click" />
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/ImageForKM/manual download.png" Width="15%" Height="15%" Style="cursor: pointer" />
                                    </div>
                                    <div style="text-align: right" class="col-sm-6">
                                        <asp:Button ID="cmdSearch" runat="server" Text="SEARCH" CssClass="btn btn-success" Width="80px" OnClick="cmdSearch_Click" />
                                        &nbsp;
                                    <asp:Button ID="cmdClear" runat="server" Text="CLEAR" CssClass="btn btn-success" Width="80px" OnClick="cmdClear_Click" />
                                    </div>
                                </div>

                                <br />

                                <asp:Table runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell Width="33%">
                                            <asp:Label ID="lblTotal" runat="server" ForeColor="Red" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="33%" Style="text-align: center">
                                        <%--<asp:LinkButton ID="lblUserManual" runat="server" Text="USER MANUAL" ForeColor="Red"></asp:LinkButton>--%>
                                        </asp:TableCell>
                                        <asp:TableCell Width="33%" Style="text-align: right">
                                            <%--Text="Created by {User Login} : 50 Topics"--%>
                                            <asp:Label ID="lblCreate" runat="server" ForeColor="Red" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                                <div>
                                    <div style="text-align: left">
                                    </div>
                                    <div style="text-align: center; display: inline-block">
                                    </div>
                                    <div style="text-align: right">
                                    </div>
                                </div>

                                <br />
                                <asp:GridView ID="dgvKM" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" DataKeyNames="TOPIC_ID,KM_ID,STATUS_ID" Font-Size="16px"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" HeaderStyle-ForeColor="Info" 
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnRowDataBound="dgvKM_RowDataBound"
                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowCommand="dgvKM_RowCommand"
                                    AllowPaging="true" OnPageIndexChanging="dgvKM_PageIndexChanging" PageSize="10"
                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                    <Columns>
                                        <asp:BoundField DataField="TOPIC_ID" HeaderText="Topic ID">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Topic name">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkTopicName" runat="server" Text='<%# Eval("TOPIC_NAME")%>' CommandName="View" Font-Size="16px"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TAGS" HeaderText="Tags">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="KNOWLEDGE_TYPE_NAME" HeaderText="Knowledge type">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DIVISION_NAME" HeaderText="Division">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DEPARTMENT_NAME" HeaderText="Department">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FULLNAME" HeaderText="Author name">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CREATE_DATE" HeaderText="Created date">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UPDATE_DATE" HeaderText="Updated date">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Bookmark">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemTemplate>
                                                <div class="col-sm-12">
                                                    <asp:ImageButton ID="imgBook" runat="server" CommandName="Book" Width="24" Height="24" Font-Names="Century Gothic" Font-Size="16px"/>
                                                    <%--<asp:ImageButton ID="ImaBookNo" runat="server" CommandName="No" ImageUrl="~/Images/ImageForKM/bookmark_icon3_inactive.png" Width="24" Height="24" Visible="false"/>--%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    <HeaderStyle CssClass="gvHeader-white"></HeaderStyle>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                </asp:GridView>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right">
                        &nbsp;
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
