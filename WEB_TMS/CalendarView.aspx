﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="CalendarView.aspx.cs" Inherits="CalendarView" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
  <link href="fullcalendar/lib/cupertino/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link href='fullcalendar/fullcalendarDetail.css' rel='stylesheet' />
<link href='fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="fullcalendar/styleButton.css" rel="stylesheet" type="text/css" />
<script src='fullcalendar/lib/moment.min.js'></script>
<script src="fullcalendar/lang/th.js" type="text/javascript"></script>
<script src='fullcalendar/fullcalendar.min.js'></script>
<script src='fullcalendar/lib/jquery-ui.custom.min.js'></script>
<style type="text/css">
    .buttonMonth
    {
        border-width: 1px;
        width: 50px;
        text-align: center;
    }
    .tab
    {
        width: 100%;
        border-color: #600;
        border-width: 0 0 1px 1px;
        border-style: solid;
    }
    .cell
    {
        border-color: #600;
        border-width: 1px 1px 0 0;
        border-style: solid;
        margin: 0;
        padding: 4px;
        background-color: #eee6a3;
    }
    
    .td_x
    {
        padding-bottom: 10px;
    }
    #loading
    {
        text-align: right;
        display: none;
        top: 10px;
        right: 10px;
    }
    
    .displayNone
    {
        display: none;
    }
    
    
    .displayTR
    {
        display: table-row;
    }
</style>
<asp:Literal ID="ltrjQueryScript" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
      <table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="#CCCCCC">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="5" cellpadding="0" bgcolor="#FFFFFF">
                <tr>
                    <td>
                    <%--    <dx:ASPxTextBox ID="txtCalendarDateHide" runat="server" ClientVisible="false" ClientInstanceName="txtCalendarDateHide">
                        </dx:ASPxTextBox>
                        <dx:ASPxTextBox ID="txtValueCalendar" runat="server" ClientVisible="false" ClientInstanceName="txtValueCalendar">
                        </dx:ASPxTextBox>--%>
                  <%--      <asp:Button ID="btnAddDataCalendar" runat="server" Style="display: none" OnClick="btnAddDataCalendar_Click" />--%>
                        <div style="width: 100%;">
                            <div style="width: 100%; text-align: center;">
                                <ul id="months-list" style="margin-bottom: -22px">
                                </ul>
                            </div>
                            <div id='calendar'>
                            </div>
                            <%--<asp:ImageButton ID="Call" ImageUrl=""  />--%>
                            <asp:Literal ID="ltrmonthcalendar" runat="server"></asp:Literal>
                            <asp:Literal ID="ltrday" runat="server"></asp:Literal>
                        </div>
                    </td>
                </tr>
                <tr>
                <td align="center">
                    <dx:ASPxButton runat="server" ID="BACK" SkinID="_back">
                    <ClientSideEvents Click="function(s,e){ window.location='approve_mv.aspx';}" />
                    </dx:ASPxButton>
                </td>
                    
                </tr>
            </table>
             <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
            </dx:ASPxLoadingPanel>
        </td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

