﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="RequireField.aspx.cs" Inherits="RequireField" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="Require Field, Require Document"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="tblAddHeader" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: right; width: 40%">
                                                <asp:Label ID="lblScreen" runat="server" Text="หน้าจอ :"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell Style="width: 60%">
                                                <asp:DropDownList ID="ddlScreen" runat="server" CssClass="form-control" Width="350px" AutoPostBack="true" OnSelectedIndexChanged="ddlScreen_SelectedIndexChanged">
                                                    <asp:ListItem Value="COMPLAIN" Text="Complain"></asp:ListItem>
                                                    <asp:ListItem Value="COMPLAIN_REQ" Text="Complain Req"></asp:ListItem>
                                                    <asp:ListItem Value="VENDOR" Text="Driver"></asp:ListItem>
                                                    <asp:ListItem Value="TRUCK" Text="Truck (TC)"></asp:ListItem>
                                                    <asp:ListItem Value="REPORT_MONTHLY_1" Text="รายงานประจำเดือน"></asp:ListItem>
                                                    <asp:ListItem Value="REPORT_MONTHLY_3" Text="รายงานประจำ 3 เดือน"></asp:ListItem>
                                                    <asp:ListItem Value="REPORT_MONTHLY_6" Text="รายงานประจำ 6 เดือน"></asp:ListItem>
                                                    <asp:ListItem Value="ACCIDENTTAB1" Text="Accident Tab1"></asp:ListItem>
                                                    <asp:ListItem Value="ACCIDENTTAB2" Text="Accident Tab2"></asp:ListItem>
                                                    <asp:ListItem Value="ACCIDENTTAB3" Text="Accident Tab3"></asp:ListItem>
                                                    <asp:ListItem Value="ACCIDENTTAB3VENDOR" Text="Accident Tab3 Vendor"></asp:ListItem>
                                                    <asp:ListItem Value="ACCIDENTTAB4" Text="Accident Tab4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: right">
                                                <asp:Label ID="lblSubject1" runat="server" Text="ประเภทร้องเรียน :"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlSubject1" runat="server" CssClass="form-control" Width="350px" AutoPostBack="true" OnSelectedIndexChanged="ddlSubject1_SelectedIndexChanged"></asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: right">
                                                <asp:Label ID="lblSubject2" runat="server" Text="หัวข้อร้องเรียน :"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlSubject2" runat="server" CssClass="form-control" Width="350px"></asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="3" Style="text-align: center">
                                                <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" Width="100px" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" OnClick="cmdSearch_Click" />
                                                &nbsp;<asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" Width="100px" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" OnClick="cmdCancel_Click" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>

                                <br />

                                <ul class="nav nav-tabs" runat="server" id="tabtest">
                                    <li class="active" id="Tab1" runat="server"><a href="#TabField" data-toggle="tab"
                                        aria-expanded="true" runat="server" id="FieldTab">Require Field</a></li>
                                    <li class="" id="Tab2" runat="server"><a href="#TabFile" data-toggle="tab" aria-expanded="false"
                                        runat="server" id="FileTab">Require File</a></li>
                                    <li class="" id="Tab3" runat="server"><a href="#TabOther" data-toggle="tab" aria-expanded="false"
                                        runat="server" id="OtherTab">Other</a></li>
                                </ul>
                                <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
                                    <div class="tab-pane fade active in" id="TabField">
                                        <br />
                                        <br />
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="dgvField" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="CONTROL_ID"
                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                    <Columns>
                                                        <asp:BoundField DataField="CONTROL_ID" Visible="false"></asp:BoundField>
                                                        <asp:BoundField DataField="CONTROL_ID_REQ" Visible="false"></asp:BoundField>
                                                        <asp:BoundField DataField="DESCRIPTION" HeaderText="ฟิล์ด"></asp:BoundField>
                                                        <asp:TemplateField HeaderText="Require">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlRequire" runat="server" Width="300px" CssClass="form-control">
                                                                    <asp:ListItem Text="REQUIRE" Value="1" />
                                                                    <asp:ListItem Text="OPTIONAL" Value="2" />
                                                                    <asp:ListItem Text="DISABLE" Value="3" />
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="tab-pane fade" id="TabFile">
                                        <br />
                                        <br />
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID"
                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                    <Columns>
                                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false"></asp:BoundField>
                                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์อัพโหลด"></asp:BoundField>
                                                        <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลที่รองรับ"></asp:BoundField>
                                                        <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์สูงสุด (MB)"></asp:BoundField>
                                                        <asp:TemplateField HeaderText="Require">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <%--<div class="col-sm-1">--%>
                                                                <asp:RadioButtonList ID="radRequire" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="1" Text="Yes&nbsp;"></asp:ListItem>
                                                                    <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                <%--</div>--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="tab-pane fade active in" id="TabOther">
                                        <br />
                                        <br />
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:Table ID="tblMonthlyReport" runat="server" Width="100%" Visible="false">
                                                    <asp:TableRow>
                                                        <asp:TableCell Style="text-align: right">
                                                            <asp:Label ID="lblStartUploadDay" runat="server" Text="วันที่เริ่มส่งรายงาน : &nbsp;"></asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtStartUploadDay" runat="server" CssClass="form-control" Width="250px" Style="text-align: center"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Style="text-align: right">
                                                            <asp:Label ID="lblEndUploadDay" runat="server" Text="วันที่สิ้นสุดส่งรายงาน : &nbsp;"></asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtEndUploadDay" runat="server" CssClass="form-control" Width="250px" Style="text-align: center"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>

                                                <asp:Table ID="tblComplain" runat="server" Width="100%" Visible="false">
                                                    <asp:TableRow>
                                                        <asp:TableCell Style="text-align: right">
                                                            <asp:Label ID="lblDisableDriver" runat="server" Text="ระงับ พขร. ชั่วคราว : &nbsp;"></asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:RadioButtonList ID="radDisableDriver" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="1" Text="Yes&nbsp;&nbsp;"></asp:ListItem>
                                                                <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </asp:TableCell>
                                                        <asp:TableCell Style="text-align: right">
                                                            <asp:Label ID="lblMultipleCar" runat="server" Text="เพิ่มได้หลายวันที่เกิดเหตุ : &nbsp;"></asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:RadioButtonList ID="radMultiplecar" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="1" Text="Yes&nbsp;&nbsp;"></asp:ListItem>
                                                                <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
