﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Data.OracleClient;
using System.IO;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraReports.UI;
using System.Drawing.Text;
using System.Drawing;
public partial class History_Car : System.Web.UI.Page
{
    string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string sREQUESTID = "";
    private static string sDocOther = "0002";
    private static string sFomateDate = ConfigurationManager.AppSettings["FormatDate"] + "";
    private static string sCheckWaterID = "01";
    private static string sThisStatus = "06"; //รอนำส่งเอกสารปิดงาน
    private static string NVERSION = ""; //รอนำส่งเอกสารปิดงาน
    //ID Group
    private const string sG1 = "00001", sG2 = "00002", sG3 = "00003", sG4 = "00004", sG5 = "00005", sG6 = "00006", sG7 = "00007", sG8 = "00008", sG9 = "00009", sG10 = "00010", sG11 = "00011"; // Note 8,9 เพิ่มใน table อื่น

    private static DataTable dtMainData = new DataTable();
    private static List<TData_CHECKLIST> lstMasterCheckList = new List<TData_CHECKLIST>();
    private static List<TData_EmpWorker> lstEmpWork = new List<TData_EmpWorker>();
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (string.IsNullOrEmpty(Session["UserID"] + ""))
        //{
        if (false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        else
        {
            if (!IsPostBack)
            {
               // Session["UserID"] = "sdsds";
                ListData_Detail2("");
                string strREQID = Request.QueryString["strRQID"];
                if (!string.IsNullOrEmpty(strREQID + ""))
                {
                    dtMainData = new DataTable();
                    lstMasterCheckList = new List<TData_CHECKLIST>();

                    string[] arrREQID = STCrypt.DecryptURL(strREQID);
                    sREQUESTID = arrREQID[0];

                    cmbG7.DataBind();
                    cmbG7.SelectedIndex = 0;
                    gvwEmpWorker.ClientVisible = false;

                    ListDataToPage(arrREQID[0]);

                    GetMasterCheckList();
                }
            }
        }
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] spara = e.Parameter.Split(';');
        switch (spara[0])
        {
            case "addempwork": AddRowToGvwEmpWorker(); break;
            case "deleteEmp":
                AddRowToGvwEmpWorker(); break;
            case "SAVE": AddData(); break;
            case "RedirectTHome": xcpn.JSProperties["cpRedirectTo"] = "result-add.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT2": xcpn.JSProperties["cpRedirectTo"] = "History_Car.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT3": xcpn.JSProperties["cpRedirectTo"] = "History_Car2.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
           // case "RedirectT4": xcpn.JSProperties["cpRedirectTo"] = "result-add4.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            //case "RedirectT5": xcpn.JSProperties["cpRedirectTo"] = "result-add5.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
        };
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListDataToGridEmployeeWorker(sREQUESTID, NVERSION);
    }

    private void ListDataToPage(string sREQID)
    {
//        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,
//                        TRK.DWATEREXPIRE,TRK.SCARTYPEID,
//                        TCAT.CARCATE_NAME,
//                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
//                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,
//                        TVD.SABBREVIATION,
//                        TUS.SFIRSTNAME,TUS.SLASTNAME
//                        FROM TBL_REQUEST TRQ 
//                        LEFT JOIN TTRUCK TRK ON TRQ.STRUCKID = TRK.STRUCKID
//                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
//                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
//                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
//                        INNER JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
//                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
//                        WHERE TRQ.REQUEST_ID = '{0}'";
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
                        TRQ.WATER_EXPIRE_DATE  as DWATEREXPIRE,TRK.SCARTYPEID,TRQ.RESULT_CHECKING_DATE,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,TRQ.CHECKWATER_NVERSION,
                        TVD.SVENDORID,TVD.SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME,TRK.NWHEELS,TRQ.TOTLE_SLOT,TRK.DPREV_SERV,TRK.DLAST_SERV,TRKN.SCAR_NUM,INC.DATE_CREATED
                        FROM TBL_REQUEST TRQ 
                        LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = TRQ.STRUCKID
                        LEFT JOIN TTRUCK TRKN ON TRKN.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        LEFT JOIN (SELECT REQ_ID, MAX(NVL( DATE_UPDATED, DATE_CREATED)) as DATE_CREATED FROM TBL_INNER_CHECKINGS GROUP BY REQ_ID)INC
                        ON TRQ.REQUEST_ID = INC.REQ_ID
                        WHERE TRQ.REQUEST_ID = '{0}'";

        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        SetDataToPage();
    }

    private void SetDataToPage()
    {
        if (dtMainData.Rows.Count > 0)
        {
            DataRow dr = null;
            dr = dtMainData.Rows[0];
            decimal nTemp = 0;

            lblREQUEST_DATE.Text = !string.IsNullOrEmpty(dr["REQUEST_DATE"] + "") ? Convert.ToDateTime(dr["REQUEST_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblDWATEREXPIRE.Text = !string.IsNullOrEmpty(dr["DWATEREXPIRE"] + "") ? Convert.ToDateTime(dr["DWATEREXPIRE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblSERVICE_DATE.Text = !string.IsNullOrEmpty(dr["SERVICE_DATE"] + "") ? Convert.ToDateTime(dr["SERVICE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblAPPOINTMENT_BY_DATE.Text = (!string.IsNullOrEmpty(dr["APPROVE_DATE"] + "") ? Convert.ToDateTime(dr["APPROVE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "") +
                                          (!string.IsNullOrEmpty(dr["SFIRSTNAME"] + "") && !string.IsNullOrEmpty(dr["SLASTNAME"] + "") ? " - คุณ" + dr["SFIRSTNAME"] + " " + dr["SLASTNAME"] : "-");


            lblREQTYPE_NAME.Text = dr["REQTYPE_NAME"] + "";
            lblCAUSE_NAME.Text = dr["CAUSE_NAME"] + "" + (!string.IsNullOrEmpty(dr["REMARK_CAUSE"] + "") ? (" (" + dr["REMARK_CAUSE"] + ")") : "");
            lblVendorName.Text = dr["SABBREVIATION"] + "";
            lblTypeCar.Text = dr["CARCATE_NAME"] + "";
            lblTotalCap.Text = (decimal.TryParse(dr["TOTLE_CAP"] + "", out nTemp) ? nTemp : 0).ToString(SystemFunction.CheckFormatNuberic(0));
            lblREGISTERNO.Text = dr["VEH_NO"] + "" + (!string.IsNullOrEmpty(dr["TU_NO"] + "") ? "/" + dr["TU_NO"] + "" : "");

            if (dr["REQTYPE_ID"] + "" == sCheckWaterID)
            {
                // set data to grid
                string sTruckID = "";
                switch (dr["SCARTYPEID"] + "")
                {
                    case "0":
                        sTruckID = dr["VEH_ID"] + "";

                        break; // 10 ล้อ
                    case "3":

                        sTruckID = dr["TU_ID"] + "";
                        break; // หัวลาก
                }
            }


            // Set data
            ListData_Detail(sREQUESTID);

        }
    }

    // Note
    /*
     Falge {
     * Y = ผ่าน    
     * N = ไม่ผ่าน
     * W = ไม่มี //without
     }
     */

    /*ส่วนข้างล่าง*/

    private void ListData_Detail(string sReqID)
    {

        // 1  TBL_SCRUTINEERING
        string sql1 = @"select * from TBL_SCRUTINEERING where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
        string sql2 = @"select * from TBL_SCRUTINEERINGLIST where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' and NVERSION = {0} and ISACTIVE_FLAG = 'Y' order by checklist_item";
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();

        dt1 = CommonFunction.Get_Data(strConn, sql1);


        if (dt1.Rows.Count == 1)
        {
            DataRow dr1 = null;
            dr1 = dt1.Rows[0];
            dt2 = CommonFunction.Get_Data(strConn, string.Format(sql2, dr1["NVERSION"] + ""));

            txtOpinion.Text = dr1["CHECKED_OPINION"] + "";

            //สรุปผลการตรวจสอบ
            rblG9.Value = dr1["CHECKING_TRY"] + "";

            //ตรวจอุปกรณ์จรวดหรือไม่
            rblG2_Head.Value = dr1["ROCKETS_FLAG"] + "";
            if (dr1["ROCKETS_FLAG"] + "" == "N")
            {
                rblG2_7.ClientEnabled = false;
                rblG2_8.ClientEnabled = false;
                rblG2_9.ClientEnabled = false;
                txtG2_10_1.ClientEnabled = false;
                txtG2_10_2.ClientEnabled = false;
                rblG2_10.ClientEnabled = false;
                txtG2_11_1.ClientEnabled = false;
                txtG2_11_2.ClientEnabled = false;
                rblG2_11.ClientEnabled = false;
                rblG2_12.ClientEnabled = false;
            }
            //ตรวจยางรถตัวเทเลอร์ 
            rblG4_Head.Value = dr1["TUTIRE_FLAG"] + "";

            //ชนิดวาล์ว
            cmbG7.Value = dr1["VALVE_TYPE"] + "";
          

            foreach (DataRow dr in dt2.Rows)
            {
                if (dr["CHECK_GROUPID"] + "" == "00001")
                {
                    #region G1
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "1": rblG1_1.Value = dr["ITEM1_VAL"] + ""; break;
                        case "2": rblG1_2.Value = dr["ITEM1_VAL"] + ""; break;
                        case "3": rblG1_3.Value = dr["ITEM1_VAL"] + ""; break;
                        case "4": rblG1_4.Value = dr["ITEM1_VAL"] + ""; break;
                        case "5": rblG1_5.Value = dr["ITEM1_VAL"] + ""; break;
                        case "6": rblG1_6.Value = dr["ITEM1_VAL"] + ""; break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00002")
                {
                    #region G2
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "7": rblG2_7.Value = dr["ITEM1_VAL"] + ""; break;
                        case "8": rblG2_8.Value = dr["ITEM1_VAL"] + ""; break;
                        case "9": rblG2_9.Value = dr["ITEM1_VAL"] + ""; break;
                        case "10":
                            txtG2_10_1.Text = dr["ITEM1_VAL"] + "";
                            txtG2_10_2.Text = dr["ITEM2_VAL"] + "";
                            rblG2_10.Value = dr["ITEM3_VAL"] + "";
                            break;
                        case "11":
                            txtG2_11_1.Text = dr["ITEM1_VAL"] + "";
                            txtG2_11_2.Text = dr["ITEM2_VAL"] + "";
                            rblG2_11.Value = dr["ITEM3_VAL"] + "";
                            break;
                        case "12": rblG2_12.Value = dr["ITEM1_VAL"] + ""; break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00003")
                {
                    #region G3
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "13":
                            txtG3_13_1.Text = dr["ITEM1_VAL"] + "";
                            txtG3_13_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                        case "14":
                            txtG3_14_1.Text = dr["ITEM1_VAL"] + "";
                            txtG3_14_2.Text = dr["ITEM2_VAL"] + "";
                            ckbG3_14.Checked = (dr["ITEM3_VAL"] + "" == "Y");
                            break;
                        case "1414":
                            txtG3_1414_1.Text = dr["ITEM1_VAL"] + "";
                            txtG3_1414_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                        case "15":
                            txtG3_15_1.Text = dr["ITEM1_VAL"] + "";
                            txtG3_15_2.Text = dr["ITEM2_VAL"] + "";
                            ckbG3_15.Checked = (dr["ITEM3_VAL"] + "" == "Y");
                            break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00004")
                {
                    if (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0")
                    {
                        rblG1_4.ClientEnabled = false;
                        txtG4_16_1.ClientEnabled = false;
                        txtG4_16_2.ClientEnabled = false;
                        txtG4_17_1.ClientEnabled = false;
                        txtG4_17_2.ClientEnabled = false;
                        ckbG4_17.ClientEnabled = false;
                        txtG4_18_1.ClientEnabled = false;
                        txtG4_18_2.ClientEnabled = false;
                        ckbG4_18.ClientEnabled = false;
                        txtG4_19_1.ClientEnabled = false;
                        txtG4_19_2.ClientEnabled = false;
                        ckbG4_19.ClientEnabled = false;
                    }
                    else
                    {
                        rblG1_4.ClientEnabled = true;
                        txtG4_16_1.ClientEnabled = true;
                        txtG4_16_2.ClientEnabled = true;
                        txtG4_17_1.ClientEnabled = true;
                        txtG4_17_2.ClientEnabled = true;
                        ckbG4_17.ClientEnabled = true;
                        txtG4_18_1.ClientEnabled = true;
                        txtG4_18_2.ClientEnabled = true;
                        ckbG4_18.ClientEnabled = true;
                        txtG4_19_1.ClientEnabled = true;
                        txtG4_19_2.ClientEnabled = true;
                        ckbG4_19.ClientEnabled = true;
                    }

                    #region G4
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "16":
                            txtG4_16_1.Text = dr["ITEM1_VAL"] + "";
                            txtG4_16_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                        case "17":
                            txtG4_17_1.Text = dr["ITEM1_VAL"] + "";
                            txtG4_17_2.Text = dr["ITEM2_VAL"] + "";
                            ckbG4_17.Checked = (dr["ITEM3_VAL"] + "" == "Y");
                            break;
                        case "18":
                            txtG4_18_1.Text = dr["ITEM1_VAL"] + "";
                            txtG4_18_2.Text = dr["ITEM2_VAL"] + "";
                            ckbG4_18.Checked = (dr["ITEM3_VAL"] + "" == "Y");
                            break;
                        case "19":
                            txtG4_19_1.Text = dr["ITEM1_VAL"] + "";
                            txtG4_19_2.Text = dr["ITEM2_VAL"] + "";
                            ckbG4_19.Checked = (dr["ITEM3_VAL"] + "" == "Y");
                            break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00005")
                {
                    #region G5
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "20":
                            txtG5_20_1.Text = dr["ITEM1_VAL"] + "";
                            txtG5_20_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                        case "21":
                            txtG5_21_1.Text = dr["ITEM1_VAL"] + "";
                            txtG5_21_2.Text = dr["ITEM2_VAL"] + "";
                            ckbG5_21_1.Checked = (dr["ITEM3_VAL"] + "" == "Y");
                            break;
                        case "22":
                            txtG5_22_1.Text = dr["ITEM1_VAL"] + "";
                            txtG5_22_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                        case "23":
                            txtG5_23_1.Text = dr["ITEM1_VAL"] + "";
                            txtG5_23_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00006")
                {
                    #region G6
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "52":
                            txtG6_52_1.Text = dr["ITEM1_VAL"] + "";
                            rblG6_52.Value = dr["ITEM2_VAL"] + "";
                            break;
                        case "53":
                            txtG6_53_1.Text = dr["ITEM1_VAL"] + "";
                            rblG6_53.Value = dr["ITEM2_VAL"] + "";
                            break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00007")
                {
                    #region G7
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "24":
                            txtG7_24_1.Text = dr["ITEM1_VAL"] + "";
                            txtG7_24_2.Text = dr["ITEM2_VAL"] + "";
                            rblG7_24.Value = dr["ITEM3_VAL"] + "";
                            break;
                        case "25":
                            txtG7_25_1.Text = dr["ITEM1_VAL"] + "";
                            txtG7_25_2.Text = dr["ITEM2_VAL"] + "";
                            rblG7_25.Value = dr["ITEM3_VAL"] + "";
                            break;
                        case "26":
                            txtG7_26_1.Text = dr["ITEM1_VAL"] + "";
                            txtG7_26_2.Text = dr["ITEM2_VAL"] + "";
                            rblG7_26.Value = dr["ITEM3_VAL"] + "";
                            break;
                        case "27": rblG7_27.Value = dr["ITEM1_VAL"] + ""; break;
                        case "28": rblG7_28.Value = dr["ITEM1_VAL"] + ""; break;
                        case "29": rblG7_29.Value = dr["ITEM1_VAL"] + ""; break;
                        case "30": rblG7_30.Value = dr["ITEM1_VAL"] + ""; break;
                        case "31": rblG7_31.Value = dr["ITEM1_VAL"] + ""; break;
                        case "32": rblG7_32.Value = dr["ITEM1_VAL"] + ""; break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00010")
                {
                    #region G10
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "33": rblG10_33.Value = dr["ITEM1_VAL"] + ""; break;
                        case "34": rblG10_34.Value = dr["ITEM1_VAL"] + ""; break;
                        case "35": rblG10_35.Value = dr["ITEM1_VAL"] + ""; break;
                        case "36": rblG10_36.Value = dr["ITEM1_VAL"] + ""; break;
                        case "37": rblG10_37.Value = dr["ITEM1_VAL"] + ""; break;
                        case "38": rblG10_38.Value = dr["ITEM1_VAL"] + ""; break;
                        case "39": rblG10_39.Value = dr["ITEM1_VAL"] + ""; break;
                        case "40": rblG10_40.Value = dr["ITEM1_VAL"] + ""; break;
                        case "41": rblG10_41.Value = dr["ITEM1_VAL"] + ""; break;
                        case "42":
                            txtG10_42_1.Text = dr["ITEM1_VAL"] + "";
                            rblG10_42.Value = dr["ITEM2_VAL"] + "";
                            break;
                        case "43": rblG10_43.Value = dr["ITEM1_VAL"] + ""; break;
                        case "44": rblG10_44.Value = dr["ITEM1_VAL"] + ""; break;
                        case "45": rblG10_45.Value = dr["ITEM1_VAL"] + ""; break;
                        case "46": rblG10_46.Value = dr["ITEM1_VAL"] + ""; break;
                        case "47": rblG10_47.Value = dr["ITEM1_VAL"] + ""; break;
                        case "48": txtG10_48.Text = dr["ITEM1_VAL"] + ""; break;
                        case "50": rblG10_50.Value = dr["ITEM1_VAL"] + ""; break;
                        case "51": rblG10_51.Value = dr["ITEM1_VAL"] + ""; break;
                    }
                    #endregion
                }
                else if (dr["CHECK_GROUPID"] + "" == "00011")
                {
                    #region //G11
                    switch (dr["CHECKLIST_ID"] + "")
                    {
                        case "55": txtG11_55_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "56": txtG11_56_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "57": txtG11_57_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "58": txtG11_58_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "59": txtG11_59_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "60": txtG11_60_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "61": txtG11_61_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "62": txtG11_62_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "63": txtG11_63_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "64": txtG11_64_1.Text = dr["ITEM1_VAL"] + ""; break;
                        case "65":
                            txtG11_65_1.Text = dr["ITEM1_VAL"] + "";
                            txtG11_65_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                        case "66":
                            txtG11_66_1.Text = dr["ITEM1_VAL"] + "";
                            txtG11_66_2.Text = dr["ITEM2_VAL"] + "";
                            break;
                    }
                    #endregion
                }

                // ผู้ปฏิบัติงาน
                NVERSION = dr1["NVERSION"] + "";
                ListDataToGridEmployeeWorker(sREQUESTID, dr1["NVERSION"] + "");
                btnPrintT2.ClientVisible = true;
            }
        }
        else
        {
            btnPrintT2.ClientVisible = false;
        }
    }

    private void ListData_Detail2(string sReqID)
    {
        string DATA = "12112118    11-    2-      - -      - -      1212                  900/20   900/20   3         3         3    3    0-    -    -    11111111111-                                   11110";

        string sss = DATA.Replace(" ","");

        for (int i = 0; i < sss.Length; i++)
        {
            string chk = sss.Substring(i,1);
        }
        //string[] ar = DATA.Replace(" ","&").Split('&&&&');
        

        //// 1  TBL_SCRUTINEERING
        //string sql1 = @"select * from TBL_SCRUTINEERING where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
        //string sql2 = @"select * from TBL_SCRUTINEERINGLIST where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' and NVERSION = {0} and ISACTIVE_FLAG = 'Y' order by checklist_item";
        //DataTable dt1 = new DataTable();
        //DataTable dt2 = new DataTable();

        //dt1 = CommonFunction.Get_Data(strConn, sql1);


        //if (dt1.Rows.Count == 1)
        //{
        //    DataRow dr1 = null;
        //    dr1 = dt1.Rows[0];
        //    dt2 = CommonFunction.Get_Data(strConn, string.Format(sql2, dr1["NVERSION"] + ""));

        //    txtOpinion.Text = dr1["CHECKED_OPINION"] + "";

        //    //สรุปผลการตรวจสอบ
        //    rblG9.Value = dr1["CHECKING_TRY"] + "";

        //    //ตรวจอุปกรณ์จรวดหรือไม่
        //    rblG2_Head.Value = dr1["ROCKETS_FLAG"] + "";
        //    if (dr1["ROCKETS_FLAG"] + "" == "N")
        //    {
        //        rblG2_7.ClientEnabled = false;
        //        rblG2_8.ClientEnabled = false;
        //        rblG2_9.ClientEnabled = false;
        //        txtG2_10_1.ClientEnabled = false;
        //        txtG2_10_2.ClientEnabled = false;
        //        rblG2_10.ClientEnabled = false;
        //        txtG2_11_1.ClientEnabled = false;
        //        txtG2_11_2.ClientEnabled = false;
        //        rblG2_11.ClientEnabled = false;
        //        rblG2_12.ClientEnabled = false;
        //    }
        //    //ตรวจยางรถตัวเทเลอร์ 
        //    rblG4_Head.Value = dr1["TUTIRE_FLAG"] + "";

        //    //ชนิดวาล์ว
        //    cmbG7.Value = dr1["VALVE_TYPE"] + "";


        //    foreach (DataRow dr in dt2.Rows)
        //    {
        //        if (dr["CHECK_GROUPID"] + "" == "00001")
        //        {
        //            #region G1
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "1": rblG1_1.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "2": rblG1_2.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "3": rblG1_3.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "4": rblG1_4.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "5": rblG1_5.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "6": rblG1_6.Value = dr["ITEM1_VAL"] + ""; break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00002")
        //        {
        //            #region G2
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "7": rblG2_7.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "8": rblG2_8.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "9": rblG2_9.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "10":
        //                    txtG2_10_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG2_10_2.Text = dr["ITEM2_VAL"] + "";
        //                    rblG2_10.Value = dr["ITEM3_VAL"] + "";
        //                    break;
        //                case "11":
        //                    txtG2_11_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG2_11_2.Text = dr["ITEM2_VAL"] + "";
        //                    rblG2_11.Value = dr["ITEM3_VAL"] + "";
        //                    break;
        //                case "12": rblG2_12.Value = dr["ITEM1_VAL"] + ""; break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00003")
        //        {
        //            #region G3
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "13":
        //                    txtG3_13_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG3_13_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "14":
        //                    txtG3_14_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG3_14_2.Text = dr["ITEM2_VAL"] + "";
        //                    ckbG3_14.Checked = (dr["ITEM3_VAL"] + "" == "Y");
        //                    break;
        //                case "1414":
        //                    txtG3_1414_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG3_1414_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "15":
        //                    txtG3_15_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG3_15_2.Text = dr["ITEM2_VAL"] + "";
        //                    ckbG3_15.Checked = (dr["ITEM3_VAL"] + "" == "Y");
        //                    break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00004")
        //        {
        //            if (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0")
        //            {
        //                rblG1_4.ClientEnabled = false;
        //                txtG4_16_1.ClientEnabled = false;
        //                txtG4_16_2.ClientEnabled = false;
        //                txtG4_17_1.ClientEnabled = false;
        //                txtG4_17_2.ClientEnabled = false;
        //                ckbG4_17.ClientEnabled = false;
        //                txtG4_18_1.ClientEnabled = false;
        //                txtG4_18_2.ClientEnabled = false;
        //                ckbG4_18.ClientEnabled = false;
        //                txtG4_19_1.ClientEnabled = false;
        //                txtG4_19_2.ClientEnabled = false;
        //                ckbG4_19.ClientEnabled = false;
        //            }
        //            else
        //            {
        //                rblG1_4.ClientEnabled = true;
        //                txtG4_16_1.ClientEnabled = true;
        //                txtG4_16_2.ClientEnabled = true;
        //                txtG4_17_1.ClientEnabled = true;
        //                txtG4_17_2.ClientEnabled = true;
        //                ckbG4_17.ClientEnabled = true;
        //                txtG4_18_1.ClientEnabled = true;
        //                txtG4_18_2.ClientEnabled = true;
        //                ckbG4_18.ClientEnabled = true;
        //                txtG4_19_1.ClientEnabled = true;
        //                txtG4_19_2.ClientEnabled = true;
        //                ckbG4_19.ClientEnabled = true;
        //            }

        //            #region G4
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "16":
        //                    txtG4_16_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG4_16_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "17":
        //                    txtG4_17_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG4_17_2.Text = dr["ITEM2_VAL"] + "";
        //                    ckbG4_17.Checked = (dr["ITEM3_VAL"] + "" == "Y");
        //                    break;
        //                case "18":
        //                    txtG4_18_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG4_18_2.Text = dr["ITEM2_VAL"] + "";
        //                    ckbG4_18.Checked = (dr["ITEM3_VAL"] + "" == "Y");
        //                    break;
        //                case "19":
        //                    txtG4_19_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG4_19_2.Text = dr["ITEM2_VAL"] + "";
        //                    ckbG4_19.Checked = (dr["ITEM3_VAL"] + "" == "Y");
        //                    break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00005")
        //        {
        //            #region G5
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "20":
        //                    txtG5_20_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG5_20_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "21":
        //                    txtG5_21_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG5_21_2.Text = dr["ITEM2_VAL"] + "";
        //                    ckbG5_21_1.Checked = (dr["ITEM3_VAL"] + "" == "Y");
        //                    break;
        //                case "22":
        //                    txtG5_22_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG5_22_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "23":
        //                    txtG5_23_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG5_23_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00006")
        //        {
        //            #region G6
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "52":
        //                    txtG6_52_1.Text = dr["ITEM1_VAL"] + "";
        //                    rblG6_52.Value = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "53":
        //                    txtG6_53_1.Text = dr["ITEM1_VAL"] + "";
        //                    rblG6_53.Value = dr["ITEM2_VAL"] + "";
        //                    break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00007")
        //        {
        //            #region G7
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "24":
        //                    txtG7_24_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG7_24_2.Text = dr["ITEM2_VAL"] + "";
        //                    rblG7_24.Value = dr["ITEM3_VAL"] + "";
        //                    break;
        //                case "25":
        //                    txtG7_25_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG7_25_2.Text = dr["ITEM2_VAL"] + "";
        //                    rblG7_25.Value = dr["ITEM3_VAL"] + "";
        //                    break;
        //                case "26":
        //                    txtG7_26_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG7_26_2.Text = dr["ITEM2_VAL"] + "";
        //                    rblG7_26.Value = dr["ITEM3_VAL"] + "";
        //                    break;
        //                case "27": rblG7_27.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "28": rblG7_28.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "29": rblG7_29.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "30": rblG7_30.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "31": rblG7_31.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "32": rblG7_32.Value = dr["ITEM1_VAL"] + ""; break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00010")
        //        {
        //            #region G10
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "33": rblG10_33.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "34": rblG10_34.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "35": rblG10_35.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "36": rblG10_36.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "37": rblG10_37.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "38": rblG10_38.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "39": rblG10_39.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "40": rblG10_40.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "41": rblG10_41.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "42":
        //                    txtG10_42_1.Text = dr["ITEM1_VAL"] + "";
        //                    rblG10_42.Value = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "43": rblG10_43.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "44": rblG10_44.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "45": rblG10_45.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "46": rblG10_46.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "47": rblG10_47.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "48": txtG10_48.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "50": rblG10_50.Value = dr["ITEM1_VAL"] + ""; break;
        //                case "51": rblG10_51.Value = dr["ITEM1_VAL"] + ""; break;
        //            }
        //            #endregion
        //        }
        //        else if (dr["CHECK_GROUPID"] + "" == "00011")
        //        {
        //            #region //G11
        //            switch (dr["CHECKLIST_ID"] + "")
        //            {
        //                case "55": txtG11_55_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "56": txtG11_56_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "57": txtG11_57_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "58": txtG11_58_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "59": txtG11_59_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "60": txtG11_60_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "61": txtG11_61_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "62": txtG11_62_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "63": txtG11_63_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "64": txtG11_64_1.Text = dr["ITEM1_VAL"] + ""; break;
        //                case "65":
        //                    txtG11_65_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG11_65_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //                case "66":
        //                    txtG11_66_1.Text = dr["ITEM1_VAL"] + "";
        //                    txtG11_66_2.Text = dr["ITEM2_VAL"] + "";
        //                    break;
        //            }
        //            #endregion
        //        }

        //        // ผู้ปฏิบัติงาน
        //        NVERSION = dr1["NVERSION"] + "";
        //        ListDataToGridEmployeeWorker(sREQUESTID, dr1["NVERSION"] + "");
        //        btnPrintT2.ClientVisible = true;
        //    }
        //}
        //else
        //{
        //    btnPrintT2.ClientVisible = false;
        //}
    }

    private void ListDataToGridEmployeeWorker(string sReqID, string sVersion)
    {
        lstEmpWork.Clear();
        string sql = @"SELECT TEW.*  ,TUS.SFIRSTNAME,TUS.SLASTNAME
                        FROM TBL_EMP_WORKER TEW 
                        INNER JOIN TUSER TUS ON TEW.SUID = TUS.SUID
                        WHERE REQUEST_ID = '{0}' AND NVERSION = " + CommonFunction.ReplaceInjection(sVersion) + "";

        //List<TData_EmpWorker> lstEmpWork = new List<TData_EmpWorker>();
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sReqID)));
        if (dt.Rows.Count == 0)
        {

            lstEmpWork.Add(new TData_EmpWorker { sEmpID = "0", sFName = "", sLName = "", sFullName = "", nOrder = 0 });
        }
        else
        {
            lstEmpWork = dt.AsEnumerable().Select(s => new TData_EmpWorker
                {
                    sEmpID = s.Field<string>("SUID"),
                    sFName = s.Field<string>("SFIRSTNAME"),
                    sLName = s.Field<string>("SLASTNAME"),
                    sFullName = s.Field<string>("SFIRSTNAME") + " " + s.Field<string>("SLASTNAME"),
                    nOrder = s.Field<decimal>("NORDER")
                }).OrderBy(o => o.nOrder).ToList();
        }

        gvwEmpWorker.DataSource = lstEmpWork;
        gvwEmpWorker.DataBind();

        gvwEmpWorker.ClientVisible = (lstEmpWork.Count > 0);

        SetDataCallBackToGridEmpWork(lstEmpWork);
    }

    protected void cmbUser_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cmbUser_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        //List<TData_EmpWorker> lstEmpWork = new List<TData_EmpWorker>(GetDataInGirdEmpWorker());

        string EmpChose = "";
        foreach (var item in lstEmpWork)
        {
            EmpChose += ",'" + item.sEmpID + "'";
        }
        string Codition = "";
        if (lstEmpWork.Where(w => w.sEmpID != null || w.sEmpID == "").Count() > 0)
        {
            Codition = " AND SUID NOT IN (" + (!string.IsNullOrEmpty(EmpChose) ? EmpChose.Remove(0, 1) : "") + ")";
        }

        sdsEmpWorker.SelectCommand = @"SELECT SUID,SFIRSTNAME,SLASTNAME, SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,RN
                                        FROM
                                        (
                                        SELECT ROW_NUMBER() OVER(ORDER BY TUS.SUID) AS RN ,TUS.*  FROM TUSER TUS 
                                        WHERE TUS.CACTIVE = '1' AND CGROUP <> '0' " + Codition + @"  AND  (TUS.SFIRSTNAME LIKE :fillter OR TUS.SLASTNAME LIKE :fillter)
                                        ) 
                                        WHERE RN BETWEEN :startIndex AND :endIndex ORDER BY SFIRSTNAME";

        sdsEmpWorker.SelectParameters.Clear();
        sdsEmpWorker.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsEmpWorker.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsEmpWorker.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsEmpWorker;
        comboBox.DataBind();
    }

    private void AddRowToGvwEmpWorker()
    {

        decimal nIndx = lstEmpWork.Count() > 0 ? lstEmpWork.Max(x => x.nOrder) : 0;



        if (!string.IsNullOrEmpty(cmbUser.Value + "") && cmbUser.Value + "" != "")
        {
            lstEmpWork.Add(new TData_EmpWorker { sEmpID = cmbUser.Value + "", sFName = "", sLName = "", sFullName = cmbUser.Text, nOrder = nIndx });
            cmbUser.Value = "";
            nIndx++;
        }

        int Indx = 0;
        if (!string.IsNullOrEmpty(txtIndexgvwService.Text))
        {
            Indx = int.Parse(txtIndexgvwService.Text);
            dynamic SID = gvwEmpWorker.GetRowValues(Indx, "sEmpID");
            string EMPID = SID;
            lstEmpWork.RemoveAll(w => w.sEmpID == EMPID);
            txtIndexgvwService.Text = "";
        }


        gvwEmpWorker.DataSource = lstEmpWork;
        gvwEmpWorker.DataBind();

        gvwEmpWorker.ClientVisible = true;

        //SetDataCallBackToGridEmpWork(lstEmpWork);

    }

    private List<TData_EmpWorker> GetDataInGirdEmpWorker()
    {
        //List<TData_EmpWorker> lstEmpWork = new List<TData_EmpWorker>();
        //string sEmpID = "", sFullName = "";
        //int nIndx = 0;
        //for (int i = 0; i < gvwEmpWorker.VisibleRowCount; i++)
        //{
        //    //GridViewDataColumn col1 = gvwEmpWorker.Columns[1] as GridViewDataColumn;
        //    ASPxComboBox cmbEmp = (ASPxComboBox)gvwEmpWorker.FindRowCellTemplateControl(i, null, "cmbUser");// as ASPxComboBox;
        //    sEmpID = cmbEmp.Value + "";
        //    sFullName = cmbEmp.Text;
        //    if (!string.IsNullOrEmpty(sEmpID) && sEmpID != "0")
        //    {
        //        lstEmpWork.Add(new TData_EmpWorker { sEmpID = sEmpID, sFName = "", sLName = "", sFullName = sFullName, nOrder = nIndx });
        //        nIndx++;
        //    }
        //}

        return lstEmpWork;
    }

    private void SetDataCallBackToGridEmpWork(List<TData_EmpWorker> lst)
    {
        //for (int i = 0; i < gvwEmpWorker.VisibleRowCount; i++)
        //{
        //    ASPxComboBox cmbEmp = (ASPxComboBox)gvwEmpWorker.FindRowCellTemplateControl(i, null, "cmbUser");
        //    if (lst[i].sEmpID != "0")
        //    {
        //        cmbEmp.Value = lst[i].sEmpID;
        //    }
        //}
    }

    private void AddData()
    {
        int StatusLog = rblG9.SelectedIndex == 0 ? 5 : 6;
        SystemFunction.Add_To_TBL_REQREMARK(sREQUESTID, "N", SystemFunction.GetRemark_WorkFlowRequest(StatusLog), "06", "A", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(11), CommonFunction.ReplaceInjection(txtOpinion.Text), "M");
        if (CheckDataInPageBeforAddData())
        {
            if (!CheckHasData(sREQUESTID)) //add data
            {
                AddDataToTBL_SCRUTINEERING();
                UpdateStatus_TBL_REQUEST(sREQUESTID, sThisStatus);

                if (ckbCheckSendMailToVerdor.Checked)
                {
                    SendMailToVerdor();
                }

                SetDataToPage();
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
            }
            else // update
            {
                UpdateDataToTBL_SCRUTINEERING(sREQUESTID);
                UpdateStatus_TBL_REQUEST(sREQUESTID, sThisStatus);

                if (ckbCheckSendMailToVerdor.Checked)
                {
                    SendMailToVerdor();
                }

                SetDataToPage();
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
            }
        }
        else
        {

        }
    }

    private void AddDataToTBL_SCRUTINEERING()
    {
        if (dtMainData.Rows.Count > 0)
        {
            string _sReqTypeID = "", _sVEH_ID = "", _sTU_ID = "", _sVEH_NO = "", _sTU_NO = "", _sVEH_CHASSIS = "", _sTU_CHASSIS = "", _sCHECKING_STATUS = "", _sCHECKING_REMARK = "", _sREPORT2VEND = "",
                   _sCHECKED_OPINION = "", _sCHECKING_TRY = "", _sROCKETS_FLAG = "", _sTUTIRE_FLAG = "", _sVALVE_TYPE = "";

            string sqlInsert = @"INSERT INTO TBL_SCRUTINEERING(REQUEST_ID,REQTYPE_ID,VEH_ID,VEH_NO,VEH_CHASSIS,TU_ID,TU_NO,TU_CHASSIS,CHECKING_STATUS,CHECKING_REMARK,REPORT2VEND,CHECKED_OPINION,CHECKING_TRY,ROCKETS_FLAG,TUTIRE_FLAG,VALVE_TYPE,DCREATE,SCREATE_ID,DUPDATE,SUPDATE_ID,NVERSION)
                                                        VALUES(:REQUEST_ID,:REQTYPE_ID,:VEH_ID,:VEH_NO,:VEH_CHASSIS,:TU_ID,:TU_NO,:TU_CHASSIS,:CHECKING_STATUS,:CHECKING_REMARK,:REPORT2VEND,:CHECKED_OPINION,:CHECKING_TRY,:ROCKETS_FLAG,:TUTIRE_FLAG,:VALVE_TYPE,SYSDATE,:SCREATE_ID,SYSDATE,:SUPDATE_ID,1)";

            DataRow dr = null;
            dr = dtMainData.Rows[0];
            _sReqTypeID = dr["REQTYPE_ID"] + "";
            _sVEH_ID = dr["VEH_ID"] + "";
            _sTU_ID = dr["TU_ID"] + "";
            _sVEH_NO = dr["VEH_NO"] + "";
            _sTU_NO = dr["TU_NO"] + "";
            _sVEH_CHASSIS = dr["VEH_CHASSIS"] + "";
            _sTU_CHASSIS = dr["TU_CHASSIS"] + "";

            // ข้อมูลจาก Page
            _sCHECKING_STATUS = null;
            _sCHECKING_REMARK = null;
            _sREPORT2VEND = ckbCheckSendMailToVerdor.Checked == true ? "Y" : "N";
            _sCHECKED_OPINION = !string.IsNullOrEmpty(txtOpinion.Text) ? txtOpinion.Text : "";

            //สถานการการตรวจสภาพ
            _sCHECKING_TRY = rblG9.SelectedItem.Value + "";

            //ตรวจอุปกรณ์จรวดหรือไม่
            _sROCKETS_FLAG = rblG2_Head.SelectedItem.Value + "";

            //ตรวจยางรถตัวเทเลอร์ 
            _sTUTIRE_FLAG = rblG4_Head.SelectedItem.Value + "";

            //ชนิดวาล์ว
            _sVALVE_TYPE = cmbG7.SelectedItem.Value + "";

            using (OracleConnection con = new OracleConnection(strConn))
            {
                con.Open();
                using (OracleCommand com = new OracleCommand(sqlInsert, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQUESTID;
                    com.Parameters.Add(":REQTYPE_ID", OracleType.VarChar).Value = _sReqTypeID;
                    com.Parameters.Add(":VEH_ID", OracleType.VarChar).Value = _sVEH_ID;
                    com.Parameters.Add(":VEH_NO", OracleType.VarChar).Value = _sVEH_NO;
                    com.Parameters.Add(":VEH_CHASSIS", OracleType.VarChar).Value = _sVEH_CHASSIS;
                    com.Parameters.Add(":TU_ID", OracleType.VarChar).Value = _sTU_ID;
                    com.Parameters.Add(":TU_NO", OracleType.VarChar).Value = _sTU_NO;
                    com.Parameters.Add(":TU_CHASSIS", OracleType.VarChar).Value = _sTU_CHASSIS;
                    com.Parameters.Add(":CHECKING_STATUS", OracleType.VarChar).Value = DBNull.Value;//_sCHECKING_STATUS;
                    com.Parameters.Add(":CHECKING_REMARK", OracleType.VarChar).Value = DBNull.Value; //_sCHECKING_REMARK;
                    com.Parameters.Add(":REPORT2VEND", OracleType.VarChar).Value = _sREPORT2VEND;
                    com.Parameters.Add(":CHECKED_OPINION", OracleType.VarChar).Value = _sCHECKED_OPINION;
                    com.Parameters.Add(":CHECKING_TRY", OracleType.VarChar).Value = _sCHECKING_TRY;
                    com.Parameters.Add(":ROCKETS_FLAG", OracleType.VarChar).Value = _sROCKETS_FLAG;
                    com.Parameters.Add(":TUTIRE_FLAG", OracleType.VarChar).Value = _sTUTIRE_FLAG;
                    com.Parameters.Add(":VALVE_TYPE", OracleType.VarChar).Value = _sVALVE_TYPE;
                    com.Parameters.Add(":SCREATE_ID", OracleType.VarChar).Value = Session["UserID"] + "";
                    com.Parameters.Add(":SUPDATE_ID", OracleType.VarChar).Value = Session["UserID"] + "";
                    com.ExecuteNonQuery();
                }
            }

            // add to table child
            AddDataToTBL_SCRUTINEERINGLIST(sREQUESTID, 1);
            AddDataToTBL_EMP_WORKER(sREQUESTID, 1);
        }
    }

    private void UpdateDataToTBL_SCRUTINEERING(string sReqID)
    {
        string sqlUpdate = @"UPDATE TBL_SCRUTINEERING SET 
                                REPORT2VEND = :REPORT2VEND,
                                CHECKED_OPINION = :CHECKED_OPINION,
                                CHECKING_TRY = :CHECKING_TRY,
                                ROCKETS_FLAG = :ROCKETS_FLAG,
                                TUTIRE_FLAG = :TUTIRE_FLAG,
                                VALVE_TYPE = :VALVE_TYPE,
                                DUPDATE = SYSDATE,
                                SUPDATE_ID = :SUPDATE_ID,
                                NVERSION = :NVERSION
                                WHERE REQUEST_ID = :REQUEST_ID";

        string _sREPORT2VEND = "", _sCHECKED_OPINION = "", _sCHECKING_TRY = "", _sROCKETS_FLAG = "", _sTUTIRE_FLAG = "", _sVALVE_TYPE = "";

        _sREPORT2VEND = ckbCheckSendMailToVerdor.Checked == true ? "Y" : "N";
        _sCHECKED_OPINION = !string.IsNullOrEmpty(txtOpinion.Text) ? txtOpinion.Text : "";

        //สถานการการตรวจสภาพ
        _sCHECKING_TRY = rblG9.SelectedItem.Value + "";

        //ตรวจอุปกรณ์จรวดหรือไม่
        _sROCKETS_FLAG = rblG2_Head.SelectedItem.Value + "";

        //ตรวจยางรถตัวเทเลอร์ 
        _sTUTIRE_FLAG = rblG4_Head.SelectedItem.Value + "";

        //ชนิดวาล์ว
        _sVALVE_TYPE = cmbG7.SelectedItem.Value + "";

        int nVsersion = GetMaxVersionCheckList(sReqID);

        using (OracleConnection con = new OracleConnection(strConn))
        {
            con.Open();
            using (OracleCommand com = new OracleCommand(sqlUpdate, con))
            {
                com.Parameters.Clear();
                com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQUESTID;

                com.Parameters.Add(":REPORT2VEND", OracleType.VarChar).Value = _sREPORT2VEND;
                com.Parameters.Add(":CHECKED_OPINION", OracleType.VarChar).Value = _sCHECKED_OPINION;
                com.Parameters.Add(":CHECKING_TRY", OracleType.VarChar).Value = _sCHECKING_TRY;
                com.Parameters.Add(":ROCKETS_FLAG", OracleType.VarChar).Value = _sROCKETS_FLAG;
                com.Parameters.Add(":TUTIRE_FLAG", OracleType.VarChar).Value = _sTUTIRE_FLAG;
                com.Parameters.Add(":VALVE_TYPE", OracleType.VarChar).Value = _sVALVE_TYPE;
                com.Parameters.Add(":SUPDATE_ID", OracleType.VarChar).Value = Session["UserID"] + "";
                com.Parameters.Add(":NVERSION", OracleType.Number).Value = nVsersion;
                com.ExecuteNonQuery();
            }
        }

        // add to table child
        AddDataToTBL_SCRUTINEERINGLIST(sReqID, nVsersion);
        AddDataToTBL_EMP_WORKER(sReqID, nVsersion);
    }

    private void AddDataToTBL_SCRUTINEERINGLIST(string sREQID, int nVersion) // รายละเอียดการตรวจสอบ
    {
        //Clear Data Befor Add
        //SystemFunction.SQLExecuteNonQuery(strConn, "DELETE FROM TBL_SCRUTINEERINGLIST WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + "'");

        // Update data to history
        SystemFunction.SQLExecuteNonQuery(strConn, "UPDATE TBL_SCRUTINEERINGLIST SET  ISACTIVE_FLAG = 'N' WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + "'");

        #region Group Data
        /*00001	ตรวจอุปกรณ์ความปลอดภัย
         00002	ตรวจอุปกรณ์จรวดหรือไม่
         00003	ตรวจยางรถหัวลากเทเลอร์และแหนบ
         00004	ตรวจยางรถตัวเทเลอร์และแหนบ
         00005	ตรวจท่อจ่ายน้ำมัน
         00006	วัดค่า Gas Detector
         00007	ตรวจวาล์ว
         00008	รายชื่อผู้ปฏิบัติงาน
         00009	สรุปผลการตรวจสอบ*/
        #endregion



        string sqlInsert = @"INSERT INTO TBL_SCRUTINEERINGLIST(REQUEST_ID,CHECK_GROUPID,CHECKLIST_ID,CHECKLIST_ITEM,ITEM1_VAL,ITEM1_UNIT,ITEM2_VAL,ITEM2_UNIT,ITEM3_VAL,ITEM3_UNIT,ISACTIVE_FLAG,NVERSION) 
                             VALUES(:REQUEST_ID, :CHECK_GROUPID, :CHECKLIST_ID, :CHECKLIST_ITEM, :ITEM1_VAL, :ITEM1_UNIT, :ITEM2_VAL, :ITEM2_UNIT, :ITEM3_VAL, :ITEM3_UNIT,'Y'," + CommonFunction.ReplaceInjection(nVersion + "") + ")";

        // เงื่อนไข insert
        bool CheckG2 = rblG2_Head.SelectedItem.Value == "Y" ? true : false;

        using (OracleConnection con = new OracleConnection(strConn))
        {
            con.Open();
            for (int i = 1; i <= 65; i++)
            {
                using (OracleCommand com = new OracleCommand(sqlInsert, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQID;

                    #region Defaul Parameter // Note =>> i + (จำนวนข้อมูลในแต่ละGroup - 1)
                    if (i <= 6)//G1 //** ผ่าน,ไม่ผ่าน
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG1;
                    }
                    else if (i <= 12)//G2
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG2;
                    }
                    else if (i <= 16)//G3
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG3;
                    }
                    else if (i <= 20)//G4
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG4;
                    }
                    else if (i <= 24)//G5
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG5;
                    }
                    else if (i <= 26)//G6
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG6;
                    }
                    else if (i <= 35)//G7
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG7;
                    }
                    else if (i <= 53)//G10
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG10;
                    }
                    else if (i <= 65)//G11
                    {
                        com.Parameters.Add(":CHECK_GROUPID", OracleType.VarChar).Value = sG11;
                    }

                    //com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = i;
                    com.Parameters.Add(":CHECKLIST_ITEM", OracleType.Int32).Value = i;

                    #endregion

                    switch (i)
                    {
                        #region G1
                        case 1:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG1_1.SelectedItem != null ? rblG1_1.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 1;
                            break;
                        case 2:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG1_2.SelectedItem != null ? rblG1_2.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 2;
                            break;
                        case 3:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG1_3.SelectedItem != null ? rblG1_3.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 3;
                            break;
                        case 4:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG1_4.SelectedItem != null ? rblG1_4.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 4;
                            break;
                        case 5:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG1_5.SelectedItem != null ? rblG1_5.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 5;
                            break;
                        case 6:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG1_6.SelectedItem != null ? rblG1_6.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 6;
                            break;
                        #endregion

                        #region G2
                        case 7:
                            if (CheckG2)
                            {
                                com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG2_7.SelectedItem != null ? rblG2_7.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 7;
                            }
                            break;
                        case 8:
                            if (CheckG2)
                            {
                                com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG2_8.SelectedItem != null ? rblG2_8.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 8;
                            }
                            break;
                        case 9:
                            if (CheckG2)
                            {
                                com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG2_9.SelectedItem != null ? rblG2_9.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 9;
                            }
                            break;
                        case 10:
                            if (CheckG2)
                            {
                                com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG2_10_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                                com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG2_10_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ท่อ";
                                com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = rblG2_10.SelectedItem != null ? rblG2_10.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = rblG2_10.SelectedItem != null ? rblG2_10.SelectedItem.Text.Trim() : "";
                                com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 10;
                            }
                            break;
                        case 11:
                            if (CheckG2)
                            {
                                com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG2_11_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                                com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG2_11_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ท่อ";
                                com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = rblG2_11.SelectedItem != null ? rblG2_11.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = rblG2_11.SelectedItem != null ? rblG2_11.SelectedItem.Text.Trim() : "";
                                com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 11;
                            }
                            break;
                        case 12:
                            if (CheckG2)
                            {
                                com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG2_12.SelectedItem != null ? rblG2_12.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                                com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 12;
                            }
                            break;
                        #endregion

                        #region G3
                        case 13:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG3_13_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG3_13_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ปอนด์";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 13;
                            break;

                        case 14:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG3_14_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG3_14_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = (ckbG3_14.Checked ? "Y" : "N"); com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 14;
                            break;

                        case 15: // 14 ซ้ำ
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG3_1414_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG3_1414_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ปอนด์";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 1414;
                            break;

                        case 16:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG3_15_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG3_15_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = (ckbG3_15.Checked ? "Y" : "N"); com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 15;
                            break;
                        #endregion

                        #region G4
                        case 17:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG4_16_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG4_16_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ปอนด์";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 16;
                            break;

                        case 18:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG4_17_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG4_17_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = (ckbG4_17.Checked ? "Y" : "N"); com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 17;
                            break;

                        case 19:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG4_18_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG4_18_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = (ckbG4_18.Checked ? "Y" : "N"); com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 18;
                            break;

                        case 20:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG4_19_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG4_19_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = (ckbG4_19.Checked ? "Y" : "N"); com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 19;
                            break;
                        #endregion

                        #region G5
                        case 21:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG5_20_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG5_20_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ท่อ";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 20;
                            break;
                        case 22:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG5_21_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG5_21_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "เมตร";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = (ckbG5_21_1.Checked ? "Y" : "N"); com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 21;
                            break;
                        case 23:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG5_22_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG5_22_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ท่อ";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 22;
                            break;
                        case 24:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG5_23_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "เมตร";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG5_23_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ลิตร";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 23;
                            break;
                        #endregion

                        #region G6
                        case 25: // 52
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG6_52_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "%LEL";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = rblG6_52.SelectedItem != null ? rblG6_52.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 52;
                            break;

                        case 26: // 53
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG6_53_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "vol%";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = rblG6_53.SelectedItem != null ? rblG6_53.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 53;
                            break;
                        #endregion

                        #region G7

                        case 27: // 24
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG7_24_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG7_24_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ตัว";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = rblG7_24.SelectedItem != null ? rblG7_24.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 24;
                            break;

                        case 28:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG7_25_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG7_25_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ตัว";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = rblG7_25.SelectedItem != null ? rblG7_25.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 25;
                            break;

                        case 29:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG7_26_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "นิ้ว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG7_26_2.Text.Trim(); com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "ตัว";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = rblG7_26.SelectedItem != null ? rblG7_26.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 26;
                            break;

                        case 30:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG7_27.SelectedItem != null ? rblG7_27.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 27;
                            break;

                        case 31:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG7_28.SelectedItem != null ? rblG7_28.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 28;
                            break;

                        case 32:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG7_29.SelectedItem != null ? rblG7_29.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 29;
                            break;

                        case 33:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG7_30.SelectedItem != null ? rblG7_30.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 30;
                            break;

                        case 34:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG7_31.SelectedItem != null ? rblG7_31.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 31;
                            break;

                        case 35:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG7_32.SelectedItem != null ? rblG7_32.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 32;
                            break;

                        #endregion

                        #region G10
                        case 36: // 33
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_33.SelectedItem != null ? rblG10_33.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 33;
                            break;
                        case 37:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_34.SelectedItem != null ? rblG10_34.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 34;
                            break;
                        case 38:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_35.SelectedItem != null ? rblG10_35.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 35;
                            break;
                        case 39:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_36.SelectedItem != null ? rblG10_36.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 36;
                            break;
                        case 40:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_37.SelectedItem != null ? rblG10_37.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 37;
                            break;
                        case 41:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_38.SelectedItem != null ? rblG10_38.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 38;
                            break;
                        case 42:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_39.SelectedItem != null ? rblG10_39.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 39;
                            break;
                        case 43:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_40.SelectedItem != null ? rblG10_40.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 40;
                            break;
                        case 44: //41
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_41.SelectedItem != null ? rblG10_41.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 41;
                            break;
                        case 45:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG10_42_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "ทะลุผ่านถึงกัน";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = rblG10_42.SelectedItem != null ? rblG10_42.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 42;
                            break;
                        case 46:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_43.SelectedItem != null ? rblG10_43.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 43;
                            break;
                        case 47:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_44.SelectedItem != null ? rblG10_44.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 44;
                            break;
                        case 48:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_45.SelectedItem != null ? rblG10_45.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 45;
                            break;
                        case 49:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_46.SelectedItem != null ? rblG10_46.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 46;
                            break;
                        case 50:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_47.SelectedItem != null ? rblG10_47.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 47;
                            break;
                        case 51:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG10_48.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 48;
                            break;
                        case 52:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_50.SelectedItem != null ? rblG10_50.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 50;
                            break;
                        case 53:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = rblG10_51.SelectedItem != null ? rblG10_51.SelectedItem.Value + "" : ""; com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 51;
                            break;
                        #endregion

                        #region G11
                        case 54: //55
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_55_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "อัน";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 55;
                            break;
                        case 55:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_56_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "อัน";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 56;
                            break;
                        case 56:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_57_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "ตัว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 57;
                            break;
                        case 57:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_58_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "ม.";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 58;
                            break;
                        case 58:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_59_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "ม.";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 59;
                            break;
                        case 59:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_60_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "ตัว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 60;
                            break;
                        case 60:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_61_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "ตัว";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 61;
                            break;
                        case 61:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_62_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 62;
                            break;
                        case 62:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_63_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "แผ่น";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 63;
                            break;
                        case 63:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_64_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "อัน";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 64;
                            break;
                        case 64:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_65_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "อัน";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG11_65_2.Text.Trim(); ; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "อัน";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 65;
                            break;
                        case 65:
                            com.Parameters.Add(":ITEM1_VAL", OracleType.VarChar).Value = txtG11_66_1.Text.Trim(); com.Parameters.Add(":ITEM1_UNIT", OracleType.VarChar).Value = "กระป๋อง";
                            com.Parameters.Add(":ITEM2_VAL", OracleType.VarChar).Value = txtG11_66_2.Text.Trim(); ; com.Parameters.Add(":ITEM2_UNIT", OracleType.VarChar).Value = "กระป๋อง";
                            com.Parameters.Add(":ITEM3_VAL", OracleType.VarChar).Value = DBNull.Value; com.Parameters.Add(":ITEM3_UNIT", OracleType.VarChar).Value = DBNull.Value;
                            com.Parameters.Add(":CHECKLIST_ID", OracleType.Int32).Value = 66;
                            break;
                        #endregion

                    }

                    com.ExecuteNonQuery();
                }
            }
        }

    }

    private void AddDataToTBL_EMP_WORKER(string sREQID, int nVersion) // ผู้ปฏิบัติงาน
    {
        // Clear data befor add
        //SystemFunction.SQLExecuteNonQuery(strConn, "DELETE FROM TBL_EMP_WORKER WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + "'");

        string sqlInsert = @"INSERT INTO TBL_EMP_WORKER(REQUEST_ID,SUID,NORDER,NVERSION) VALUES(:REQUEST_ID,:SUID,:NORDER,:NVERSION)";

        List<TData_EmpWorker> lstEmpWork = new List<TData_EmpWorker>(GetDataInGirdEmpWorker());
        int nIndx = 0;
        using (OracleConnection con = new OracleConnection(strConn))
        {
            con.Open();

            foreach (var item in lstEmpWork)
            {
                using (OracleCommand com = new OracleCommand(sqlInsert, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQID;
                    com.Parameters.Add(":SUID", OracleType.VarChar).Value = item.sEmpID;
                    com.Parameters.Add(":NORDER", OracleType.Int32).Value = nIndx;
                    com.Parameters.Add(":NVERSION", OracleType.Number).Value = nVersion;
                    com.ExecuteNonQuery();
                }

                nIndx++;
            }
        }
    }

    private bool CheckHasData(string sREQID)
    {
        bool cCheck = false;

        string sql = @"SELECT * FROM TBL_SCRUTINEERING WHERE request_id = '" + CommonFunction.ReplaceInjection(sREQID) + "' ";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);

        cCheck = (dt.Rows.Count > 0);

        return cCheck;
    }

    private bool CheckDataInPageBeforAddData()
    {
        bool cCheck = true;
        string _Msg = "กรุณาระบุหัวข้อต่อไปนี้ <br/>";

        #region G1
        if (CheckRequiredField(1, sG1) && rblG1_1.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 1. สภาพสายไฟ / คัทเอาท์ดี <br/>";
        }

        if (CheckRequiredField(2, sG1) && rblG1_2.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 2. แบตเตอร์รี่มีฝาฉนวนครอบ <br/>";
        }

        if (CheckRequiredField(3, sG1) && rblG1_3.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 3. ถังดับเพลิงเคมี ขนาด 20 ปอนด์ <br/>";
        }

        if (CheckRequiredField(4, sG1) && rblG1_4.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 4. แชชชีย์ไม่ผุ <br/>";
        }

        if (CheckRequiredField(5, sG1) && rblG1_5.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 5. ท่อไอเสียไม่รั่ว,ไม่ผุ <br/>";
        }

        if (CheckRequiredField(6, sG1) && rblG1_5.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 6. สายดินประจำรถ 2 จุด <br/>";
        }
        #endregion

        #region G2
        if (rblG2_Head.SelectedItem == null)//ตรวจอุปกรณ์จรวดหรือไม่
        {
            cCheck = false;
            _Msg += @" - ตรวจอุปกรณ์จรวดหรือไม่ <br/>";
        }
        else
        {
            if (rblG2_Head.SelectedItem.Value == "Y") //ใช้งาน
            {
                if (CheckRequiredField(7, sG2) && rblG2_7.SelectedItem == null)
                {
                    cCheck = false;
                    _Msg += @" - 7. ฐานจรวดหนักไม่เกิน 1.5 หุน <br/>";
                }

                if (CheckRequiredField(8, sG2) && rblG2_8.SelectedItem == null)
                {
                    cCheck = false;
                    _Msg += @" - 8. ฐานจรวดไม่ปิดข้าง <br/>";
                }

                if (CheckRequiredField(9, sG2) && rblG2_9.SelectedItem == null)
                {
                    cCheck = false;
                    _Msg += @" - 9. มีน๊อตยึดฐานสามารถถอดได้ <br/>";
                }

                if (CheckRequiredField(10, sG2) && rblG2_10.SelectedItem == null)
                {
                    cCheck = false;
                    _Msg += @" - 10. จรวดใส่น้ำมัน <br/>";
                }
                else
                {
                    if (CheckRequiredField(10, sG2) && rblG2_10.SelectedItem.Value + "" != "W" && (string.IsNullOrEmpty(txtG2_10_1.Text) || string.IsNullOrEmpty(txtG2_10_2.Text)))
                    {
                        cCheck = false;
                        _Msg += @" - 10. จรวดใส่น้ำมัน <br/>";
                    }
                }

                if (CheckRequiredField(11, sG2) && rblG2_11.SelectedItem == null)
                {
                    cCheck = false;
                    _Msg += @" - 11. จรวดใส่สายยาง <br/>";
                }
                else
                {
                    if (CheckRequiredField(11, sG2) && rblG2_11.SelectedItem.Value + "" != "W" && (string.IsNullOrEmpty(txtG2_11_1.Text) || string.IsNullOrEmpty(txtG2_11_2.Text)))
                    {
                        cCheck = false;
                        _Msg += @" - 11. จรวดใส่สายยาง <br/>";
                    }
                }

                if (CheckRequiredField(12, sG2) && rblG2_12.SelectedItem == null)
                {
                    cCheck = false;
                    _Msg += @" - 12. จรวดเจาะรู สามารถมองทะลุผ่านได้ <br/>";
                }

            }
        }
        #endregion

        #region G3
        if (CheckRequiredField(13, sG3) && (string.IsNullOrEmpty(txtG3_13_1.Text) || string.IsNullOrEmpty(txtG3_13_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 13. ยางสองล้อหน้า <br/>";
        }

        if (CheckRequiredField(14, sG3) && (string.IsNullOrEmpty(txtG3_14_1.Text) || string.IsNullOrEmpty(txtG3_14_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 14. แหนบหน้าซ้าย <br/>";
        }

        if (CheckRequiredField(1414, sG3) && (string.IsNullOrEmpty(txtG3_1414_1.Text) || string.IsNullOrEmpty(txtG3_1414_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 15. ยางแปดล้อหลัง <br/>";
        }

        if (CheckRequiredField(15, sG3) && (string.IsNullOrEmpty(txtG3_15_1.Text) || string.IsNullOrEmpty(txtG3_15_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 16. แหนบหลังซ้าย <br/>";
        }
        #endregion

        #region G4
        string SCARTYPE = dtMainData.Rows.Count > 0 ? dtMainData.Rows[0]["SCARTYPEID"] + "" : "";
        if (SCARTYPE == "3")
        {
            if (rblG4_Head.SelectedItem == null)
            {
                cCheck = false;
                _Msg += @" - ตรวจยางรถตัวเทเลอร์และแหนบ <br/>";
            }
            else
            {
                if (CheckRequiredField(16, sG4) && (string.IsNullOrEmpty(txtG4_16_1.Text) || string.IsNullOrEmpty(txtG4_16_2.Text)))
                {
                    cCheck = false;
                    _Msg += @" - 17. ขนาดยางล้อหลังเทเลอร์ <br/>";
                }

                if (CheckRequiredField(17, sG4) && (string.IsNullOrEmpty(txtG4_17_1.Text) || string.IsNullOrEmpty(txtG4_17_2.Text)))
                {
                    cCheck = false;
                    _Msg += @" - 18. แหนบหน้าซ้ายหรือแหนบหน้าขวา <br/>";
                }

                if (CheckRequiredField(18, sG4) && (string.IsNullOrEmpty(txtG4_18_1.Text) || string.IsNullOrEmpty(txtG4_18_2.Text)))
                {
                    cCheck = false;
                    _Msg += @" - 19. แหนบกลางซ้ายหรือแหนบกลางขวา <br/>";
                }

                if (CheckRequiredField(19, sG4) && (string.IsNullOrEmpty(txtG4_19_1.Text) || string.IsNullOrEmpty(txtG4_19_2.Text)))
                {
                    cCheck = false;
                    _Msg += @" - 20. แหนบท้ายซ้ายหรือแหนบท้ายขวา <br/>";
                }
            }

        }
        #endregion

        #region G5

        if (CheckRequiredField(20, sG5) && (string.IsNullOrEmpty(txtG5_20_1.Text) || string.IsNullOrEmpty(txtG5_20_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 21. ท่อจ่ายน้ำมัน <br/>";
        }

        if (CheckRequiredField(21, sG5) && (string.IsNullOrEmpty(txtG5_21_1.Text) || string.IsNullOrEmpty(txtG5_21_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 22. ท่อร่วมทางจ่าย <br/>";
        }

        if (CheckRequiredField(22, sG5) && (string.IsNullOrEmpty(txtG5_22_1.Text) || string.IsNullOrEmpty(txtG5_22_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 23. ท่อใต้ท้อง <br/>";
        }

        if (CheckRequiredField(23, sG5) && (string.IsNullOrEmpty(txtG5_23_1.Text) || string.IsNullOrEmpty(txtG5_23_2.Text)))
        {
            cCheck = false;
            _Msg += @" - 24. ยาวรวมทั้งหมด <br/>";
        }

        #endregion

        #region G6

        if (CheckRequiredField(52, sG6) && (string.IsNullOrEmpty(txtG6_52_1.Text) || rblG6_52.SelectedItem == null))
        {
            cCheck = false;
            _Msg += @" - 25. Gas1 <br/>";
        }

        if (CheckRequiredField(53, sG6) && (string.IsNullOrEmpty(txtG6_53_1.Text) || rblG6_53.SelectedItem == null))
        {
            cCheck = false;
            _Msg += @" - 26. Gas2 <br/>";
        }
        #endregion

        #region G7

        if (cmbG7.SelectedItem.Value == "0")
        {
            cCheck = false;
            _Msg += @" - ชนิดวาล์ว <br/>";
        }

        if (CheckRequiredField(24, sG7) && (string.IsNullOrEmpty(txtG7_24_1.Text) || string.IsNullOrEmpty(txtG7_24_2.Text) || rblG7_24.SelectedItem == null))
        {
            cCheck = false;
            _Msg += @" - 27. วาล์วฉุกเฉิน <br/>";
        }

        if (CheckRequiredField(25, sG7) && (string.IsNullOrEmpty(txtG7_25_1.Text) || string.IsNullOrEmpty(txtG7_25_2.Text) || rblG7_25.SelectedItem == null))
        {
            cCheck = false;
            _Msg += @" - 28. วาล์วแยกแต่ละช่อง <br/>";
        }

        if (CheckRequiredField(26, sG7) && rblG7_26.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 29. วาล์วท่อจ่ายเพาเวอร์ <br/>";
        }
        else
        {
            if (rblG7_26.SelectedItem.Value + "" != "W" && CheckRequiredField(26, sG7) && (string.IsNullOrEmpty(txtG7_26_1.Text) || string.IsNullOrEmpty(txtG7_26_2.Text)))
            {
                cCheck = false;
                _Msg += @" - 29. วาล์วท่อจ่ายเพาเวอร์ <br/>";
            }
        }

        if (CheckRequiredField(27, sG7) && (rblG7_27.SelectedItem == null))
        {
            cCheck = false;
            _Msg += @" - 30. มือหมุนวาล์วหรือก้านวาล์วเชื่อมติดแกนทุกตัว <br/>";
        }

        if (CheckRequiredField(28, sG7) && rblG7_28.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 31. เจาะรูแกนวาล์วหรือด้ำมบอลล์วาล์วทุกตัว <br/>";
        }

        if (CheckRequiredField(29, sG7) && rblG7_29.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 32. เชื่อมน๊อตหน้าแปลนวาล์วสกัดตัวที่ 1 ตรงข้าม 2 ตัว <br/>";
        }

        if (CheckRequiredField(30, sG7) && rblG7_30.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 33. เชื่อมน๊อตหน้าแปลนวาล์วสกัดตัวสุดท้าย ตรงข้าม 2 ตัว <br/>";
        }

        if (CheckRequiredField(31, sG7) && rblG7_31.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 34. เชื่อมน๊อตหน้าแปลนตรงข้ามวาล์วสกัดทุกตัว <br/>";
        }

        if (CheckRequiredField(32, sG7) && rblG7_32.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 35. ขารับถังเจาะรู 0.5 นิ้ว มองผ่านได้ <br/>";
        }


        #endregion

        #region G10
        if (CheckRequiredField(33, sG10) && rblG10_33.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 36. ใต้ท้องถังต้องไม่ทำเป็น 2 ชั้น <br/>";
        }

        if (CheckRequiredField(34, sG10) && rblG10_34.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 37. ภายในตองกั้นถังต้องไม่เป็น 2 ชั้น <br/>";
        }

        if (CheckRequiredField(35, sG10) && rblG10_35.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 38. ตองกั้นถังต้องแข็งแรงไม่บิดตัว <br/>";
        }

        if (CheckRequiredField(36, sG10) && rblG10_36.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 39. สลักฝำปิดช่องลงน้ำมันเชื่อมติดแกน <br/>";
        }

        if (CheckRequiredField(37, sG10) && rblG10_37.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 40. มีวาล์วระบำยไอแก๊สที่ฝำปิดช่องลงน้ำมัน <br/>";
        }

        if (CheckRequiredField(38, sG10) && rblG10_38.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 41. สภาพถังไม่บุบเสียรูปทรง <br/>";
        }

        if (CheckRequiredField(39, sG10) && rblG10_39.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 42. ช่องลงน้ำมัน/ตำแหน่งแป้นอยู่กลางระหว่างตองกั้นถัง <br/>";
        }

        if (CheckRequiredField(40, sG10) && rblG10_40.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 43. ขอบฝำถังเจาะรูและลึกไม่เกิน 25 ซม. <br/>";
        }

        if (CheckRequiredField(41, sG10) && rblG10_41.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 44. ภายในถังไม่เจาะทะลุออกเพื่อการใด <br/>";
        }

        if (CheckRequiredField(42, sG10) && rblG10_42.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 45. ภายในถัง <br/>";
        }
        else
        {
            if (CheckRequiredField(42, sG10) && rblG10_42.SelectedItem.Value + "" != "W" && (string.IsNullOrEmpty(txtG10_42_1.Text)))
            {
                cCheck = false;
                _Msg += @" - 45. ภายในถัง <br/>";
            }
        }

        if (CheckRequiredField(43, sG10) && rblG10_43.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 46. ท่ออุ่นน้ำมันภายในถัง <br/>";
        }

        if (CheckRequiredField(44, sG10) && rblG10_44.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 47. ไม่มีแผ่นเหล็กบังสายตาภายใน <br/>";
        }


        if (CheckRequiredField(45, sG10) && rblG10_45.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 48. มีวงแหวนกันงัดที่ฝาแมนโฮลและเต็มวงทุกช่อง <br/>";
        }

        if (CheckRequiredField(46, sG10) && rblG10_46.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 49. ตัดแป้นเก่าออก,แต่งรอยให้เรียบ <br/>";
        }

        if (CheckRequiredField(47, sG10) && rblG10_47.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 50. ภายในถังล้างสะอาดไม่มีคราบน้ำมัน <br/>";
        }

        if (CheckRequiredField(48, sG10) && string.IsNullOrEmpty(txtG10_48.Text))
        {
            cCheck = false;
            _Msg += @" - 51. อู่ต่อถังรถบรรทุกน้ำมัน <br/>";
        }

        if (CheckRequiredField(50, sG10) && rblG10_50.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 52. ชนิดของน๊อตฝา MANHOLD <br/>";
        }

        if (CheckRequiredField(51, sG10) && rblG10_51.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - 53. ช่องว่างระยะติดแป้น ไม่ต่ำกว่า 15 ซม. <br/>";
        }

        #endregion

        #region G11
        if (CheckRequiredField(55, sG11) && string.IsNullOrEmpty(txtG11_55_1.Text))
        {
            cCheck = false;
            _Msg += @" - 54. แป้นทองเหลือง(ไม่มีตัวเลข) <br/>";
        }

        if (CheckRequiredField(56, sG11) && string.IsNullOrEmpty(txtG11_56_1.Text))
        {
            cCheck = false;
            _Msg += @" - 55. เหล็กรองแป้น 2 ม. <br/>";
        }

        if (CheckRequiredField(57, sG11) && string.IsNullOrEmpty(txtG11_57_1.Text))
        {
            cCheck = false;
            _Msg += @" - 56. เหล็กร้อยซีล <br/>";
        }

        if (CheckRequiredField(58, sG11) && string.IsNullOrEmpty(txtG11_58_1.Text))
        {
            cCheck = false;
            _Msg += @" - 57. ลวดสลิง1.5 มม. ขนาดร้อยฝาแมนโฮล <br/>";
        }

        if (CheckRequiredField(59, sG11) && string.IsNullOrEmpty(txtG11_59_1.Text))
        {
            cCheck = false;
            _Msg += @" - 58. ลวดสลิงขนาด 2.5 มม. ร้อยแป้นระดับ <br/>";
        }

        if (CheckRequiredField(60, sG11) && string.IsNullOrEmpty(txtG11_60_1.Text))
        {
            cCheck = false;
            _Msg += @" - 59. ฟาสซีล <br/>";
        }

        if (CheckRequiredField(61, sG11) && string.IsNullOrEmpty(txtG11_61_1.Text))
        {
            cCheck = false;
            _Msg += @" - 60. ซีลตะกั่ว <br/>";
        }

        if (CheckRequiredField(62, sG11) && string.IsNullOrEmpty(txtG11_62_1.Text))
        {
            cCheck = false;
            _Msg += @" - 61. ป้ายบอกระยะสีแดง <br/>";
        }

        if (CheckRequiredField(63, sG11) && string.IsNullOrEmpty(txtG11_63_1.Text))
        {
            cCheck = false;
            _Msg += @" - 62. ป้ายบอกระยะน้ำเงิน <br/>";
        }

        if (CheckRequiredField(64, sG11) && string.IsNullOrEmpty(txtG11_64_1.Text))
        {
            cCheck = false;
            _Msg += @" - 63. ลวดเชื่อม 3.2 มม. <br/>";
        }

        if (CheckRequiredField(65, sG11) && string.IsNullOrEmpty(txtG11_65_1.Text) || string.IsNullOrEmpty(txtG11_65_2.Text))
        {
            cCheck = false;
            _Msg += @" - 64. ดอกรีเวท <br/>";
        }

        if (CheckRequiredField(66, sG11) && string.IsNullOrEmpty(txtG11_66_1.Text) || string.IsNullOrEmpty(txtG11_66_2.Text))
        {
            cCheck = false;
            _Msg += @" - 65. สีสเปรย์ สีขาว <br/>";
        }
        #endregion

        //G8 รายชื่อผู้ปฏิบัติงาน
        List<TData_EmpWorker> lstEmpWork = new List<TData_EmpWorker>(GetDataInGirdEmpWorker());
        if (lstEmpWork.Count == 0)
        {
            cCheck = false;
            _Msg += @" - รายชื่อผู้ปฏิบัติงาน <br/>";
        }
        else
        {
            var query = lstEmpWork.GroupBy(g => new { g.sEmpID }).ToList();
            if (query.Count != lstEmpWork.Count) // user ซ้ำ
            {
                cCheck = false;
                _Msg += @" - รายชื่อผู้ปฏิบัติงานไม่สามารถซ้ำกันได้ <br/>";
            }
        }

        //G9 สรุปผลการตรวจสอบ
        if (rblG9.SelectedItem == null)
        {
            cCheck = false;
            _Msg += @" - สรุปผลการตรวจสอบ <br/>";
        }
        else
        {
            if (rblG9.SelectedItem.Value == "N")
            {
                if (string.IsNullOrEmpty(txtOpinion.Text))
                {
                    cCheck = false;
                    _Msg += @" - ความคิดเห็นผู้ตรวจ <br/>";
                }
            }

        }

        if (cCheck == false) { CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('แจ้งให้ทราบ','" + _Msg + "')"); }

        return cCheck;
    }

    private void UpdateStatus_TBL_REQUEST(string sReqID, string _sStatusID)
    {
        string sql = @"UPDATE TBL_REQUEST SET STATUS_FLAG = '" + CommonFunction.ReplaceInjection(_sStatusID) + "', RESULT_CHECKING_DATE = SYSDATE  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
        SystemFunction.SQLExecuteNonQuery(strConn, sql);
    }

    private bool CheckRequiredField(decimal nCheckListID, string sGroupID)
    {
        var query = lstMasterCheckList.Where(w => w.CHECKLIST_ID == nCheckListID && w.REQUIRE_FLAG == "Y" && w.CHECK_GROUPID == sGroupID).FirstOrDefault();
        if (query != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void GetMasterCheckList()
    {
        string sql = @"SELECT * FROM TBL_CHECKLIST";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);
        lstMasterCheckList = new List<TData_CHECKLIST>();
        lstMasterCheckList = SystemFunction.DataTableConvertToList<TData_CHECKLIST>(dt);
    }

    private int GetMaxVersionCheckList(string sREQID)
    {
        string sql = @"SELECT * FROM TBL_SCRUTINEERING WHERE request_id = '" + CommonFunction.ReplaceInjection(sREQID) + "' ORDER BY NVERSION DESC ";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);
        if (dt.Rows.Count > 0)
        {
            int nTemp = 0;
            nTemp = int.TryParse(dt.Rows[0]["NVERSION"] + "", out nTemp) ? nTemp : 1;
            return nTemp + 1;
        }
        else
        {
            return 1;
        }
    }

    private void SendMailToVerdor()
    {

    }


    protected void btnPrintT2_Click(object sender, EventArgs e)
    {

        if (rblG9.SelectedIndex == 0)
        {
            ListReport(sREQUESTID);
        }
        else
        {
            ListReportNoPass(sREQUESTID);
        }
    }

    private void ListReport(string sReqID)
    {
        rpt_InspectionCar report = new rpt_InspectionCar();

        // 
        OracleDataAdapter adapter = new OracleDataAdapter();
        dsInspectionCar ds = new dsInspectionCar();

        PrivateFontCollection fontColl = new PrivateFontCollection();
        //  fontColl.AddFontFile(Server.MapPath("~/font/THSarabun Bold.ttf"));
        fontColl.AddFontFile(Server.MapPath("~/font/Tahoma.ttf"));


        //SQl ประเภทสัญญา
        DataRow dr1 = dtMainData.Rows.Count > 0 ? dtMainData.Rows[0] : null;
        if (dr1 != null)
        {
            string sTruckSearchID = "";
            if (dr1["SCARTYPEID"] + "" == "0") // 10 ล้อ
            {
                sTruckSearchID = dr1["VEH_ID"] + "";
            }
            else if (dr1["SCARTYPEID"] + "" == "3") // หัวลาก
            {
                sTruckSearchID = dr1["TU_ID"] + "";
            }

            #region Q1
            string sql1 = @"select * from ttruck where struckid = '" + CommonFunction.ReplaceInjection(sTruckSearchID) + "'";
            DataTable dt1 = new DataTable();
            dt1 = CommonFunction.Get_Data(strConn, sql1);
            if (dt1.Rows.Count > 0)
            {
                //วันที่ รายงาน
                ((XRLabel)report.FindControl("lblDate", true)).Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                // วันที่วัดน้ำเดิม
                report.Parameters["sOldDateCheckWater"].Value = dt1.Rows[0]["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dt1.Rows[0]["DLAST_SERV"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "";

                //ยี่ห้อรถ (ใช้หัวหา)
                DataTable dtCheckBand = new DataTable();
                dtCheckBand = CommonFunction.Get_Data(strConn, "select * from ttruck where struckid = '" + CommonFunction.ReplaceInjection(dr1["VEH_ID"] + "") + "'");
                if (dtCheckBand.Rows.Count > 0)
                {
                    report.Parameters["sBandCar"].Value = dtCheckBand.Rows[0]["SBRAND"] + "";
                }
                else
                {
                    report.Parameters["sBandCar"].Value = "";
                }



                if (dt1.Rows[0]["CARCATE_ID"] + "" == "01") //รถลูกค้า
                {
                    ((XRCheckBox)report.FindControl("ckbContractType3", true)).Checked = true;
                }
                else if (dt1.Rows[0]["CARCATE_ID"] + "" == "02")// รถในสัญญา หรือ รถรับจ้าง
                {
                    // เป็นรถในสัญญาประเภทไหน
                    //                    string sql1_1 = @"SELECT  TRT.*
                    //                                    FROM TCONTRACT_TRUCK TCT 
                    //                                    INNER JOIN TCONTRACT TCR ON TCT.SCONTRACTID = TCR.SCONTRACTID
                    //                                    INNER JOIN TCONTRACTTYPE TRT ON TRT.SCONTRACTTYPEID = TCR.SCONTRACTTYPEID
                    //INNER JOIN TCONTRACTTYPE TRT ON TRT.SCONTRACTTYPEID = TCR.SCONTRACTTYPEID
                    //                                    WHERE  NVL(TCT.STRAILERID,TCT.STRUCKID) = '" + sTruckSearchID + "'";

                    //เป็นรถในสัญญาประเภทไหน
                    string sql1_1 = @"     SELECT  TRT.*,TMN.SABBREVIATION,TCR.STERMINALID,TRP.STERMINALID
                                    FROM TCONTRACT_TRUCK TCT 
                                    INNER JOIN TCONTRACT TCR ON TCT.SCONTRACTID = TCR.SCONTRACTID
                                    INNER JOIN TCONTRACTTYPE TRT ON TRT.SCONTRACTTYPEID = TCR.SCONTRACTTYPEID
                                     INNER JOIN TCONTRACT_PLANT TRP ON TRP.SCONTRACTID = TCT.SCONTRACTID AND TRP.CMAIN_PLANT = '1'
                                    LEFT  JOIN TTERMINAL TMN ON TMN.STERMINALID = TRP.STERMINALID
                                    WHERE  NVL(TCT.STRAILERID,TCT.STRUCKID) = '" + sTruckSearchID + "'";

                    DataTable dt1_1 = new DataTable();
                    dt1_1 = CommonFunction.Get_Data(strConn, sql1_1);
                    if (dt1_1.Rows.Count > 0)
                    {
                        DataRow dr1_1 = dt1_1.Rows[0];
                        switch (dr1_1["SCONTRACTTYPEID"] + "")
                        {
                            case "2": ((XRCheckBox)report.FindControl("ckbContractType1", true)).Checked = true; break;
                        }

                        //คลังที่เข้ารับน้ำมัน
                        report.Parameters["sSendOilStart"].Value = dr1_1["SABBREVIATION"] + "";
                        report.Parameters["sSendOilEnd"].Value = "";
                    }
                }



            }
            #endregion

            #region Q2
            string sql2 = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TRQ.VEH_ID,TRQ.TU_ID,
                            TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
                            TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
                            CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
                            TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
                            TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRK.SCAR_NUM
                             FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
                             LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
                             LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                             WHERE TRQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";

            OracleConnection con = new OracleConnection(strConn);
            con.Open();
            adapter.SelectCommand = new OracleCommand(sql2, con);
            adapter.Fill(ds, "dt1");
            adapter.Dispose();
            #endregion

            //Q3
            // 1  TBL_SCRUTINEERING
            string sql3_1 = @"select * from TBL_SCRUTINEERING where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
            string sql3_2 = @"select * from TBL_SCRUTINEERINGLIST where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' and NVERSION = {0} and ISACTIVE_FLAG = 'Y' order by checklist_item";
            DataTable dt3_1 = new DataTable();
            DataTable dt3_2 = new DataTable();
            dt3_1 = CommonFunction.Get_Data(strConn, sql3_1);
            if (dt1.Rows.Count == 1)
            {
                DataRow dr3_1 = null;
                dr3_1 = dt3_1.Rows[0];
                dt3_2 = CommonFunction.Get_Data(strConn, string.Format(sql3_2, dr3_1["NVERSION"] + ""));

                #region Deflut Seting
                //สรุปผลการตรวจสอบ
                //rblG9.Value = dr1["CHECKING_TRY"] + "";

                //ตรวจอุปกรณ์จรวดหรือไม่ G2
                if (dr3_1["ROCKETS_FLAG"] + "" == "Y")
                {
                    ((XRCheckBox)report.FindControl("ckbG2_1", true)).Checked = true;
                }
                else if (dr3_1["ROCKETS_FLAG"] + "" == "N")
                {
                    ((XRCheckBox)report.FindControl("ckbG2_2", true)).Checked = true;
                }

                //ตรวจยางรถตัวเทเลอร์  G4
                ((XRCheckBox)report.FindControl("ckbG4_1", true)).Checked = true;
                if (dr3_1["TUTIRE_FLAG"] + "" == "1")
                {
                    ((XRCheckBox)report.FindControl("ckbG4_1", true)).Text = "ยาง 8 ล้อหลังเทเลอร์";
                }
                else if (dr3_1["TUTIRE_FLAG"] + "" == "2")
                {
                    ((XRCheckBox)report.FindControl("ckbG4_1", true)).Text = "ยาง 12 ล้อหลังเทเลอร์";
                }

                //ชนิดวาล์ว G7
                ((XRLabel)report.FindControl("lblG7_1", true)).Text = GetVALVEName(dr3_1["VALVE_TYPE"] + "");

                // ความคิดเห็นผู้ตรวจ
                report.Parameters["sOpinion"].Value = dr3_1["CHECKED_OPINION"] + "";

                #endregion

                #region //Set Data
                foreach (DataRow dr in dt3_2.Rows)
                {
                    if (dr["CHECK_GROUPID"] + "" == "00001")
                    {
                        #region G1
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "1": ((XRCheckBox)report.FindControl("ckbG1_1", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "2": ((XRCheckBox)report.FindControl("ckbG1_2", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "3": ((XRCheckBox)report.FindControl("ckbG1_3", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "4": ((XRCheckBox)report.FindControl("ckbG1_4", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "5": ((XRCheckBox)report.FindControl("ckbG1_5", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "6": ((XRCheckBox)report.FindControl("ckbG1_6", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00002")
                    {
                        #region G2
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "7":
                                XRCheckBox ckbG2_7_1 = (XRCheckBox)report.FindControl("ckbG2_7_1", true);
                                ckbG2_7_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_7_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_7_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_7_1.Text = "ไม่มี";
                                }
                                break;
                            case "8":
                                XRCheckBox ckbG2_8_1 = (XRCheckBox)report.FindControl("ckbG2_8_1", true);
                                ckbG2_8_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_8_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_8_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_8_1.Text = "ไม่มี";
                                }
                                break;
                            case "9":
                                XRCheckBox ckbG2_9_1 = (XRCheckBox)report.FindControl("ckbG2_9_1", true);
                                ckbG2_9_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_9_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_9_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_9_1.Text = "ไม่มี";
                                }
                                break;
                            case "10":
                                ((XRLabel)report.FindControl("lblG2_10_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_10_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_10_3", true)).Text = dr["ITEM3_UNIT"] + "";
                                break;
                            case "11":
                                ((XRLabel)report.FindControl("lblG2_11_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_11_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_11_3", true)).Text = dr["ITEM3_UNIT"] + "";
                                break;
                            case "12":
                                XRCheckBox ckbG2_12_1 = (XRCheckBox)report.FindControl("ckbG2_12_1", true);
                                ckbG2_12_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_12_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_12_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_12_1.Text = "ไม่มี";
                                }
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00003")
                    {
                        #region G3
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "13":
                                ((XRLabel)report.FindControl("lblG3_13_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_13_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "14":
                                ((XRLabel)report.FindControl("lblG3_14_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_14_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG3_14_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "1414":
                                ((XRLabel)report.FindControl("lblG3_15_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_15_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "15":
                                ((XRLabel)report.FindControl("lblG3_16_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_16_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG3_16_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00004")
                    {
                        #region G4
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "16":
                                ((XRLabel)report.FindControl("lblG4_17_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_17_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "17":
                                ((XRLabel)report.FindControl("lblG4_18_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_18_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG4_18_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "18":
                                ((XRLabel)report.FindControl("lblG4_19_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_19_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG4_19_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "19":
                                ((XRLabel)report.FindControl("lblG4_20_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_20_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG4_20_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00005")
                    {
                        #region G5
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "20":
                                ((XRLabel)report.FindControl("lblG5_21_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_21_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "21":
                                ((XRLabel)report.FindControl("lblG5_22_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_22_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG5_22_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "22":
                                ((XRLabel)report.FindControl("lblG5_23_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_23_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "23":
                                ((XRLabel)report.FindControl("lblG5_24_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_24_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00006")
                    {
                        #region G6
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "52":
                                ((XRLabel)report.FindControl("lblG6_25_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG6_25_2", true)).Checked = (dr["ITEM2_VAL"] + "" == "Y");
                                ((XRCheckBox)report.FindControl("ckbG6_25_2", true)).Text = dr["ITEM2_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "53":
                                ((XRLabel)report.FindControl("lblG6_26_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG6_26_2", true)).Checked = (dr["ITEM2_VAL"] + "" == "Y");
                                ((XRCheckBox)report.FindControl("ckbG6_26_2", true)).Text = dr["ITEM2_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00007")
                    {
                        #region G7
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "24":
                                ((XRLabel)report.FindControl("lblG7_27_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG7_27_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG7_27_3", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_27_3", true)).Text = dr["ITEM3_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "25":
                                ((XRLabel)report.FindControl("lblG7_28_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG7_28_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG7_28_3", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_28_3", true)).Text = dr["ITEM3_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "26":
                                ((XRLabel)report.FindControl("lblG7_29_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG7_29_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG7_29_3", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_29_3", true)).Text = dr["ITEM3_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM3_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "27":
                                ((XRCheckBox)report.FindControl("ckbG7_30_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_30_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "28":
                                ((XRCheckBox)report.FindControl("ckbG7_31_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_31_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "29":
                                ((XRCheckBox)report.FindControl("ckbG7_32_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_32_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "30":
                                ((XRCheckBox)report.FindControl("ckbG7_33_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_33_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "31":
                                ((XRCheckBox)report.FindControl("ckbG7_34_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_34_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "32":
                                ((XRCheckBox)report.FindControl("ckbG7_35_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_35_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00010")
                    {
                        #region G10
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "33":
                                ((XRCheckBox)report.FindControl("ckbG10_36_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_36_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "34":
                                ((XRCheckBox)report.FindControl("ckbG10_37_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_37_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "35":
                                ((XRCheckBox)report.FindControl("ckbG10_38_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_38_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "36":
                                ((XRCheckBox)report.FindControl("ckbG10_39_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_39_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "37":
                                ((XRCheckBox)report.FindControl("ckbG10_40_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_40_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "38":
                                ((XRCheckBox)report.FindControl("ckbG10_41_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_41_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "39":
                                ((XRCheckBox)report.FindControl("ckbG10_42_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_42_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "40":
                                ((XRCheckBox)report.FindControl("ckbG10_43_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_43_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "41":
                                ((XRCheckBox)report.FindControl("ckbG10_44_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_44_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "42":
                                ((XRLabel)report.FindControl("lblG10_45_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG10_45_2", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_45_2", true)).Text = dr["ITEM2_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM2_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "43":
                                ((XRCheckBox)report.FindControl("ckbG10_46_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_46_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "44":
                                ((XRCheckBox)report.FindControl("ckbG10_47_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_47_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "45":
                                ((XRCheckBox)report.FindControl("ckbG10_48_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_48_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "46":
                                ((XRCheckBox)report.FindControl("ckbG10_49_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_49_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "47":
                                ((XRCheckBox)report.FindControl("ckbG10_50_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_50_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "48": ((XRLabel)report.FindControl("lblG10_51_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "50":
                                ((XRLabel)report.FindControl("lblG10_52_1", true)).Text = dr["ITEM1_VAL"] + "" == "1" ? "น๊อตยึดใน" : dr["ITEM1_VAL"] + "" == "2" ? "น๊อตรอบ" : dr["ITEM1_VAL"] + "" == "3" ? "แค้มรัดรอบฝา" : "";
                                break;
                            case "51":
                                ((XRCheckBox)report.FindControl("ckbG10_53_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_53_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00011")
                    {
                        #region G11
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "55": ((XRLabel)report.FindControl("lblG11_54_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "56": ((XRLabel)report.FindControl("lblG11_55_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "57": ((XRLabel)report.FindControl("lblG11_56_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "58": ((XRLabel)report.FindControl("lblG11_57_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "59": ((XRLabel)report.FindControl("lblG11_58_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "60": ((XRLabel)report.FindControl("lblG11_59_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "61": ((XRLabel)report.FindControl("lblG11_60_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "62": ((XRLabel)report.FindControl("lblG11_61_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "63": ((XRLabel)report.FindControl("lblG11_62_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "64": ((XRLabel)report.FindControl("lblG11_63_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "65":
                                ((XRLabel)report.FindControl("lblG11_64_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG11_64_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "66":
                                ((XRLabel)report.FindControl("lblG11_65_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG11_65_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                        }
                        #endregion
                    }
                }
                #endregion

            }

        }

        #region function report

        report.Name = "รายงานการตรวจสภาพรถบรรทุกน้ำมัน";
        report.DataSource = ds;
        report.DataAdapter = adapter;
        report.DataMember = "Table";

        string fileName = "รายงานการตรวจสภาพรถบรรทุกน้ำมัน_FM-005_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    private string GetVALVEName(string sID)
    {
        string sql = @" SELECT VALVE_TYPEID,VALVE_TYPENAME FROM TBL_VALVETYPE WHERE CACTIVE = 'Y' AND VALVE_TYPEID = '" + CommonFunction.ReplaceInjection(sID) + "'";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);
        if (dt.Rows.Count > 0)
        {
            return dt.Rows[0]["VALVE_TYPENAME"] + "";
        }
        else
        {
            return "";
        }
    }

    private void ListReportNoPass(string sReqID)
    {
        string CarType = "";
        string RegisID = "";
        string CHASSIS = "";
        string NWHEALL = "";
        string TOTLE_SLOT = "";
        string TOTALCAP = "";
        string DPREV_SERV = "";
        string DLAST_SERV = "";
        string SABBREVIATION = "";
        string SCAR_NUM = "";
        string RESULT_CHECKING_DATE = "";
        string DATE_CREATED = "";

        if (dtMainData.Rows.Count > 0)
        {
            //CarType = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? "หัว" : "หาง");
            RegisID = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_NO"] + "" : dtMainData.Rows[0]["TU_NO"] + "");
            CHASSIS = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_CHASSIS"] + "" : dtMainData.Rows[0]["TU_CHASSIS"] + "");
            NWHEALL = dtMainData.Rows[0]["NWHEELS"] + "";
            TOTLE_SLOT = dtMainData.Rows[0]["TOTLE_SLOT"] + "";
            TOTALCAP = dtMainData.Rows[0]["TOTLE_CAP"] + "";
            DPREV_SERV = dtMainData.Rows[0]["DPREV_SERV"] + "";
            DLAST_SERV = dtMainData.Rows[0]["DLAST_SERV"] + "";
            SABBREVIATION = dtMainData.Rows[0]["SABBREVIATION"] + "";
            SCAR_NUM = dtMainData.Rows[0]["SCAR_NUM"] + "";
            RESULT_CHECKING_DATE = dtMainData.Rows[0]["RESULT_CHECKING_DATE"] + "";
            DATE_CREATED = dtMainData.Rows[0]["DATE_CREATED"] + "";
        }

        rpt_nopasswatercheckInOut report = new rpt_nopasswatercheckInOut();

        // set control
        ((XRLabel)report.FindControl("xrLabel3", true)).Text = sReqID;
        ((XRLabel)report.FindControl("xrLabel4", true)).Text = "วันที่ " + DateTime.Now.Day + " เดือน " + DateTime.Now.ToString("MMMM", new CultureInfo("th-TH")) + " พ.ศ. " + DateTime.Now.ToString("yyyy", new CultureInfo("th-TH")) + "";
        ((XRLabel)report.FindControl("xrLabel10", true)).WidthF = 364;
        ((XRLabel)report.FindControl("xrLabel10", true)).Text = SABBREVIATION;
        ((XRLabel)report.FindControl("xrLabel12", true)).Text = SCAR_NUM;
        ((XRLabel)report.FindControl("xrLabel14", true)).Text = RegisID;
        ((XRLabel)report.FindControl("xrLabel16", true)).Text = CHASSIS;
        ((XRLabel)report.FindControl("xrLabel18", true)).Text = NWHEALL;
        ((XRLabel)report.FindControl("xrLabel20", true)).Text = TOTLE_SLOT;
        ((XRLabel)report.FindControl("xrLabel22", true)).Text = TOTALCAP;
        ((XRLabel)report.FindControl("xrLabel24", true)).Text = !string.IsNullOrEmpty(DPREV_SERV) ? DateTime.Parse(DPREV_SERV).Day + " " + DateTime.Parse(DPREV_SERV).ToString("MMMM", new CultureInfo("th-TH")) + " " + DateTime.Parse(DPREV_SERV).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel26", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).Day + " " : "";
        ((XRLabel)report.FindControl("xrLabel28", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).ToString("MMM", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel30", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel33", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).Day + " " : "";
        ((XRLabel)report.FindControl("xrLabel35", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).ToString("MMM", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel37", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        // ((XRLabel)report.FindControl("xrLabel41", true)).Text = txtComment.Text;
        ((XRLabel)report.FindControl("xrLabel41", true)).Text = GetComment(sReqID);

        ((XRLabel)report.FindControl("xrLabel41", true)).Text = "FM-มว.-006 (1/1)";
        ((XRLabel)report.FindControl("xrLabel39", true)).Text = "REV.2 (" + DateTime.Now.Day + "/" + DateTime.Now.ToString("MM", new CultureInfo("th-TH")) + "/" + DateTime.Now.ToString("yy", new CultureInfo("th-TH")) + ")";

        DataTable dtName = CommonFunction.Get_Data(strConn, @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "' AND ISACTIVE_FLAG = 'Y'");
        if (dtName.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel44", true)).Text = dtName.Rows[0]["SNAME"] + "";
        }

//        string SQL_NOPASS = @"SELECT CCP.REQUEST_ID,CCP.COMPART_NO 
//,CASE WHEN CCP.SEAL_ERR = '1' THEN '/'  WHEN CCP.SEAL_ERR = '0' THEN 'X' ELSE '' END  as  SEAL_ERR
//,CASE WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
//WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '0' THEN '/'
//WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
//WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '0' THEN 'X'
//ELSE '' END AS MANHOLD
//,CASE WHEN INC.RESULT = '1' THEN '/' WHEN INC.RESULT = '0' THEN 'X'    ELSE '' END  as  RESULT
//,CASE WHEN INC.PAN_ERR = '1' THEN '/' WHEN INC.PAN_ERR = '0' THEN 'X' ELSE '' END  as  PAN_ERR
//,CASE WHEN INC.PASSSTANDARD = '1' THEN '/' WHEN INC.PASSSTANDARD = '0' THEN 'X' ELSE '' END  as  PASSSTANDARD
//,CCP.MANHOLD_NO1
//,CCP.MANHOLD_NO2
//FROM 
//(
//SELECT REQ.REQUEST_ID ,REQ.SLOT_NO as COMPART_NO,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2,DS.MANHOLD_NO1,DS.MANHOLD_NO2 FROM  TBL_REQSLOT REQ
//LEFT JOIN  TBL_CHECKING_COMPART ds  ON REQ.REQUEST_ID = DS.REQUEST_ID AND REQ.SLOT_NO = DS.COMPART_NO
//WHERE REQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
//GROUP BY  REQ.REQUEST_ID ,REQ.SLOT_NO ,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2,DS.MANHOLD_NO1,DS.MANHOLD_NO2 
//)
//CCP
//LEFT JOIN 
//(
//    SELECT REQ_ID,COMPART_NO
//    ,MAX(PAN_LEVEL) as PAN_LEVEL
//    ,PAN_ERR
//    ,PASSSTANDARD
//    ,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END AS RESULT
//    --,(HEIGHT1 - OLD_HEIGHT1)  AS CAL1
//    --,(HEIGHT2 - OLD_HEIGHT2)  AS CAL2
//    FROM TBL_INNER_CHECKINGS
//    GROUP BY REQ_ID,COMPART_NO,PAN_ERR,PASSSTANDARD,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END
//)INC
// ON INC.REQ_ID = CCP.REQUEST_ID AND INC.COMPART_NO = CCP.COMPART_NO
//WHERE CCP.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
//ORDER BY CCP.COMPART_NO ASC";


        #region ตรวจภายนอกภายใน


        DataTable dtNopass = CommonFunction.Get_Data(strConn, @"SELECT ROWNUM||'.' as NO, MS.CHECKLIST_ID,MS.CHECKLIST_NAME,LAW.ITEM1_VAL,LAW.ITEM2_VAL,LAW.ITEM3_VAL,MS.RD_STATUSCT,REQUEST_ID,
CASE 
WHEN LAW.ITEM1_VAL = 'N' THEN MS.CHECKLIST_NAME 
WHEN LAW.ITEM1_VAL <> 'N' AND  LAW.ITEM2_VAL = 'N'   THEN REPLACE(MS.CHECKLIST_NAME,'{0}',LAW.ITEM1_VAL) 
WHEN LAW.ITEM1_VAL <> 'N' AND LAW.ITEM2_VAL <> 'N' THEN  REPLACE((REPLACE(MS.CHECKLIST_NAME,'{0}',LAW.ITEM1_VAL)),'{1}',LAW.ITEM2_VAL )
ELSE ''
END
AS DETAIL FROM TBL_CHECKLIST MS
LEFT JOIN TBL_SCRUTINEERINGLIST LAW
ON  MS.CHECKLIST_ID = LAW.CHECKLIST_ID
WHERE LAW.ITEM1_VAL = 'N' OR LAW.ITEM2_VAL = 'N'  OR (LAW.ITEM3_VAL = 'N'  AND MS.RD_STATUSCT = 'R' ) AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'");


        #endregion

       // DataTable dtNopass = CommonFunction.Get_Data(strConn, SQL_NOPASS);
        //DataRow dr = null;
        //for (int i = dtNopass.Rows.Count; i <= 9; i++)
        //{
        //    dr = dtNopass.NewRow();
        //    dr[0] = ""; // or you could generate some random string.
        //    dtNopass.Rows.Add(dr);
        //}
        report.Name = "แจ้งสาเหตุการตรวจสอบสภาพรถ/การตวงวัดน้าและตรวจวัดระดับแป้นที่ไม่ผ่าน";
        report.DataSource = dtNopass;
        string fileName = "รายงานวัดน้ำ_FM-มว.-006_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private string GetComment(string sReqID)
    {
        string sResult = "";
        //คอมเม้น/บันทึก
        string sql5 = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dtComment = new DataTable();
        dtComment = CommonFunction.Get_Data(strConn, sql5);

        DataRow dr4 = dtComment.Rows.Count > 0 ? dtComment.Rows[0] : null;

        sResult = dr4 != null ? dr4["SCOMMENT"] + "" : "";

        return sResult;
    }

    //private void Update_Scarnum(string STRUCKID)
    //{
    //    DataTable dtSCARNUM = CommonFunction.Get_Data(strConn, "SELECT STRUCKID,SCAR_NUM FROM TTRUCK WHERE  STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'");

    //    if (dtSCARNUM.Rows.Count > 0)
    //    {
    //        //ถ้ามีแล้วให้อัพเดทเข้าไป
    //        if (!string.IsNullOrEmpty(dtSCARNUM.Rows[0]["SCAR_NUM"] + ""))
    //        {
    //            string QUERY = "UPDATE TTRUCK SET SCAR_NUM = '' WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
    //        }
    //    }



    //}

    #region class
    [Serializable]

    class TData_EmpWorker
    {
        public string sEmpID { get; set; }
        public string sFName { get; set; }
        public string sLName { get; set; }
        public string sFullName { get; set; }
        public decimal nOrder { get; set; }
    }

    class TData_CHECKLIST
    {
        public string CHECK_GROUPID { get; set; }
        public decimal CHECKLIST_ID { get; set; }
        public string CHECKLIST_NAME { get; set; }
        public string REQUIRE_FLAG { get; set; }
    }
    #endregion

}