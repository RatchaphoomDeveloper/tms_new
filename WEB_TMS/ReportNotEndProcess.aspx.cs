﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;

public partial class ReportNotEndProcess : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static int nNextYeatCheckWater = 3;
    private static DataTable dtMainData = new DataTable();
    private static string sTruckID_CheckWater = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        // gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            ListData();
            SetCboYear();
        }
    }

    // void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    //{
    //    if (e.DataColumn.Caption == "ใบรับรอง")
    //    {
    //        ASPxTextBox txtChecking = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtChecking") as ASPxTextBox;
    //        txtChecking.ClientInstanceName = txtChecking.ID + "_" + e.VisibleIndex;
    //        ASPxTextBox txtRequestID = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtRequestID") as ASPxTextBox;
    //        txtRequestID.ClientInstanceName = txtRequestID.ID + "_" + e.VisibleIndex;
    //    }


    //}

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": ListData();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void ListData()
    {

        string Condition = "";

        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            Condition = " AND TO_CHAR(TIC.EXAMDATE,'MM/yyyy')  = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
            lblsTail.Text = cboMonth.Text + " - " + cboYear.Text + "(จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
        }
        else
        {
            lblsTail.Text = "(จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
        }



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
, TO_CHAR(add_months(TIC.EXAMDATE,6516),'dd/MM/yyyy') as APPOINTMENT_DATE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
,NVL(REQ.CCHECKING_WATER,'x') as CCHECKING_WATER
,STQ.STATUSREQ_NAME
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN 
(
    SELECT REQUEST_ID,MAX(EXAMDATE) as  EXAMDATE  FROM  TBL_TIME_INNER_CHECKINGS WHERE ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID
)
TIC ON TIC.REQUEST_ID = REQ.REQUEST_ID
LEFT JOIN TBL_STATUSREQ STQ
ON STQ.STATUSREQ_ID = REQ.STATUS_FLAG
WHERE 1=1 AND STQ.STATUSREQ_ID NOT IN('10','11') " + Condition + "";
        
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;

        }
        gvw.DataBind();
    }

    private void ListReport(string Type)
    {

        string Condition = "";
        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
        //}
        //else
        //{

        //}

        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            Condition = " AND TO_CHAR(TIC.EXAMDATE,'MM/yyyy')  = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
            lblsTail.Text = cboMonth.Text + " - " + cboYear.Text + "(จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
        }
        else
        {
            lblsTail.Text = "(จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
        }



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
, TO_CHAR(add_months(TIC.EXAMDATE,6516),'dd/MM/yyyy') as APPOINTMENT_DATE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
,NVL(REQ.CCHECKING_WATER,'x') as CCHECKING_WATER
,STQ.STATUSREQ_NAME
,REQ.STATUS_FLAG
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN 
(
    SELECT REQUEST_ID,MAX(EXAMDATE) as  EXAMDATE  FROM  TBL_TIME_INNER_CHECKINGS WHERE ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID
)
TIC ON TIC.REQUEST_ID = REQ.REQUEST_ID
LEFT JOIN TBL_STATUSREQ STQ
ON STQ.STATUSREQ_ID = REQ.STATUS_FLAG
WHERE 1=1 AND REQ.STATUS_FLAG NOT IN ('10','11') " + Condition + "";
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
       
        //if (dt.Rows.Count > 0)
        //{
        //    gvw.DataSource = dt;
        //    gvw.DataBind();
        //}


        rpt_NoEndProcess report = new rpt_NoEndProcess();

        #region function report

        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    string Date = "1/" + cboMonth.Value + "/" + cboYear.Text + "";
        //    DateTime DMONTH = DateTime.Parse(Date);
        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = DMONTH.ToString("MMMM", new CultureInfo("th-TH")) + " " + cboYear.Text;
        //}
        //else
        //{
        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = " - ";
        //}
        ((XRLabel)report.FindControl("xrLabel2", true)).Text = lblsHead.Text + " " + lblsTail.Text;
        report.Name = "ReportNotEndProcess";
        report.DataSource = dt;
        string fileName = "รายงานรถที่ยังไม่ปิดงาน(จ่าย/ไม่จ่าย)_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();


        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }


    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ListReport("P");
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ListReport("E");
    }

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion



}