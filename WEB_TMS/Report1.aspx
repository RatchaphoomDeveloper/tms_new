﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report1.aspx.cs" Inherits="Report1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <style type="text/css">
            *
            {
                font: 12px Tahoma;
            }
            li a
            {
                /*padding-top: 15px;*/
                color: #283B56;
                text-decoration: blink;
            }
        </style>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="15%">
                </td>
                <td style="width: 300px; vertical-align: top;">
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="300px" HeaderText="รายงานทั่วไป">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                <table width="100%">
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportGrade.aspx'>หนังสือแจ้งบริษัท</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportSuppliant.aspx'>สรุปข้อมูลการขออุทธรณ์</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportReducePoint.aspx'>สรุปข้อมูลการผิดสัญญา</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportExpireEmployee.aspx'>รายงานค้นหา พขร. ตามอายุ</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportExpireResourceMonitoring.aspx'>รายงานรถที่ใกล้หมดสภาพการใช้งาน</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportExpireResource.aspx'>รายงานค้นหารถตามอายุ</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='DetailDefectMonitoring.aspx'>รายงานสำหรับติดตามปัญหาการขนส่ง</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportQuarterLetter.aspx'>รายงานแจ้งผลการประเมินประจำไตรมาส</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='Defect_Point_Quarter.aspx'>รายงานประเมินผลการทำงาน</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </td>
                <td style="width: 300px; vertical-align: top;">
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="300px" HeaderText="รายงานเชิงวิเคราะห์">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                                <table width="100%">
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportDemandPerformance.aspx'>รายงาน DemandPerformance</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='Reportsolvepurchase.aspx'>รายงาน SolvePurchase</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href='ReportDefectPercentage.aspx'>รายงาน Defect Percentage</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </td>
                <td style="width: 300px; vertical-align: top;">
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" Width="300px" HeaderText="รายการปฎิบัติการ">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                                <table width="100%">
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href="reportsDetailConfirmContact.aspx">รายงานยืนยันรถตามสัญญา</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href="reportDetailPlanning.aspx">รายงานการจัดแผนงาน</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <li><a href="reportDetailConfirmPlan.aspx">รายงานยืนยันตามแผนงาน</a></li>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                </td>
                <td width="15%">
                </td>
            </tr>
        </table><!--
        <hr />
        <!--
        <table>
            <tr>
                <td>
                    
                    <table width="100%">
                        <tr>
                            <td colspan="4">
                                <b>รายงานทั่วไป</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportGrade.aspx'>หนังสือแจ้งบริษัท</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportSuppliant.aspx'>สรุปข้อมูลการขออุทธรณ์</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportReducePoint.aspx'>สรุปข้อมูลการผิดสัญญา</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportExpireEmployee.aspx'>รายงานค้นหา พขร. ตามอายุ</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportExpireResourceMonitoring.aspx'>รายงานรถที่ใกล้หมดสภาพการใช้งาน</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportExpireResource.aspx'>รายงานค้นหารถตามอายุ</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='DetailDefectMonitoring.aspx'>รายงานสำหรับติดตามปัญหาการขนส่ง</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportQuarterLetter.aspx'>รายงานแจ้งผลการประเมินประจำไตรมาส</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='Defect_Point_Quarter.aspx'>รายงานประเมินผลการทำงาน</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table width="100%">
                        <tr>
                            <td colspan="4">
                                <b>รายงานเชิงวิเคราะห์</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> <li><a href='ReportDemandPerformance.aspx'>รายงาน DemandPerformance</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportSuppliant.aspx'>สรุปข้อมูลการขออุทธรณ์</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportReducePoint.aspx'>สรุปข้อมูลการผิดสัญญา</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportExpireEmployee.aspx'>รายงานค้นหา พขร. ตามอายุ</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <li><a href='ReportExpireResourceMonitoring.aspx'>รายงานรถที่ใกล้หมดสภาพการใช้งาน</a></li>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> 
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> 
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> 
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> 
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="1" cellspacing="3">
            <tr>
                <td width="10%">
                </td>
                <td width="40%">
                    &nbsp;
                </td>
                <td width="10%">
                </td>
                <td width="40%">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    </li>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        -->
    </div>
    </form>
</body>
</html>
