﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Diagnostics;
using System.IO;
using DevExpress.XtraPrinting;
using DevExpress.Web.ASPxEditors;

public partial class Default2 : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ExportFile("pdf");
    }
    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        ExportFile("xls");
    }
    public void ExportFile(string fileType)
    {
        int _COLUMN0 = 0;
        _COLUMN0 = adeEnd.Date.DayOfYear - adeStart.Date.DayOfYear + 1;
        string _comm = "";

        if (!string.IsNullOrEmpty(cbxVendor.Value + ""))
        {
            _comm += " AND TB3.SVENDORID = '" + cbxVendor.Value + "'";
        }
        if (!string.IsNullOrEmpty(cboSCONTRACTNO.Value + ""))
        {
            _comm += " AND SCONTRACTNO = '" + cboSCONTRACTNO.Text + "'";
        }
        string sqlquery = string.Format(@"SELECT {3} COLUMN0,sVendorName,sContractNo,COLUMN1,COUNT(COLUMN3) COLUMN3,SUM(COLUMN6) COLUMN6 ,SUM(COLUMN14) COLUMN14
        ,'{0}' dStart
        ,'{1}' dEnd
        ,'' COLUMN5,'' COLUMN8
        FROM(SELECT  TB4.sVendorName,TB3.sContractNo,TB3.nTruck COLUMN1
        ,DDATE COLUMN3
        ,0 COLUMN6
        ,0 COLUMN14
        FROM TTruckConfirm TB1 INNER JOIN TContract TB3 ON TB1.sContractID = TB3.sContractID
        INNER JOIN TVendor_SAP TB4 ON TB4.sVendorID = TB3.sVendorID
        WHERE DDATE + 1  BETWEEN TO_DATE('{0}','dd/MM/yyyy')
        AND TO_DATE('{1}','dd/MM/yyyy') {2} AND NVL(TB3.CACTIVE,'Y') !='N'
        GROUP BY TB4.sVendorName,TB3.sContractNo,TB3.nTruck,DDATE,TB1.cConfirm)
        GROUP BY sVendorName,sContractNo,COLUMN1 "
        , adeStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us"))
        , adeEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-us")), _comm, _COLUMN0);
        //Response.Write(sqlquery); return;

        DataTable _dt = CommonFunction.Get_Data(sql, sqlquery);
        string sqlquery1 = @"SELECT distinct DDATE + 1 DDATE,sVendorName,sContractNo,NVL(TTruckConfirm.cConfirm,'0') cConfirm
FROM TVendor_SAP LEFT JOIN TContract TB3 ON TVendor_SAP.sVendorID = TB3.sVendorID
LEFT JOIN TTruckConfirm  ON TTruckConfirm.sContractID =TB3.sContractID
LEFT JOIN TTruckConfirmList TB5 ON TB5.nConfirmID  = TTruckConfirm.nConfirmID 

WHERE TB3.sContractNo IS NOT NULL 

AND TTruckConfirm.DDATE + 1 BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
" + _comm + @" AND NVL(TB3.CACTIVE,'Y') !='N' 
ORDER BY TVendor_SAP.sVendorName";
        //Response.Write(sqlquery1); return;
        DataTable _dtDate = CommonFunction.Get_Data(sql, sqlquery1);
        foreach (DataRow _dr0 in _dt.Rows)
        {
            _dr0["COLUMN3"] = _dtDate.Select("sVendorName = '" + _dr0["sVendorName"] + "' AND sContractNo = '" + _dr0["sContractNo"] + "' AND cConfirm = '1' ", "DDATE").Length;
            foreach (DataRow _dr1 in _dtDate.Select("sVendorName = '" + _dr0["sVendorName"] + "' AND sContractNo = '" + _dr0["sContractNo"] + "' AND cConfirm = '0' ", "DDATE"))
            {
                _dr0["COLUMN5"] += (string.IsNullOrEmpty(_dr0["COLUMN5"] + "") ? "" : ", ") + Convert.ToDateTime(_dr1["DDATE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-th"));
            }
        }

        string sqlquery2 = @"SELECT DDATE + 1 DDATE,sVendorName,sContractNo, SUM(NVL(TB5.cConfirm,0))  Sub1 ,TB3.nTruck
FROM TVendor_SAP LEFT JOIN TContract TB3 ON TVendor_SAP.sVendorID = TB3.sVendorID
LEFT JOIN TTruckConfirm  ON TTruckConfirm.sContractID =TB3.sContractID
LEFT JOIN TTruckConfirmList TB5 ON TB5.nConfirmID  = TTruckConfirm.nConfirmID 

WHERE TB3.sContractNo IS NOT NULL 
 AND NVL(TB3.CACTIVE,'Y') !='N'
AND TTruckConfirm.DDATE + 1 BETWEEN TO_DATE('" + adeStart.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
AND TO_DATE('" + adeEnd.Date.ToString("MM/dd/yyyy", new CultureInfo("en-us")) + @"','MM/dd/yyyy')
AND NVL(TTruckConfirm.cConfirm,'0') = '1' 
" + _comm + @"
GROUP BY DDATE,TVendor_SAP.sVendorName,TB3.sContractNo,TB3.nTruck
HAVING TB3.nTruck > SUM(NVL(TB5.cConfirm,0))
ORDER BY sVendorName";
        DataTable _dtDate2 = CommonFunction.Get_Data(sql, sqlquery2);
        foreach (DataRow _dr0 in _dt.Rows)
        {
            string StrDate = "";
            int _Total = 0, nDay = 0;
            foreach (DataRow _dr1 in _dtDate2.Select("sVendorName = '" + _dr0["sVendorName"] + "' AND sContractNo = '" + _dr0["sContractNo"] + "' ", "DDATE"))
            {
                StrDate += (string.IsNullOrEmpty(StrDate) ? "" : @", ") + Convert.ToDateTime(_dr1["DDATE"] + "").ToString("dd/MM/yyyy", new CultureInfo("en-us"));
                _Total += int.Parse(_dr1["nTruck"] + "") - int.Parse(_dr1["Sub1"] + "");
                nDay++;
            }
            _dr0["COLUMN6"] = int.Parse(_dr0["COLUMN3"] + "") - nDay;
            _dr0["COLUMN8"] = StrDate;
            _dr0["COLUMN14"] = _Total;
        }
        //Response.Write(sqlquery1); return;
        xrtDetailConfirmContact report = new xrtDetailConfirmContact();
        report.DataSource = _dt;
        string fileName = "reportsDetailConfirmContact_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        if (fileType == "xls")
            report.ExportToXls(stream);
        else
            report.ExportToPdf(stream);

        Response.ContentType = "application/" + fileType;
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName + "." + fileType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORNAME LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboSCONTRACTNO_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        dscboSCONTRACTNO.SelectCommand = "SELECT * FROM TCONTRACT WHERE SCONTRACTNO  LIKE :fillter  ";
        dscboSCONTRACTNO.SelectParameters.Clear();
        dscboSCONTRACTNO.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        //sdsOrganiz.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        //sdsOrganiz.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = dscboSCONTRACTNO;
        comboBox.DataBind();
    }
    protected void cboSCONTRACTNO_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
}