﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="book-car.aspx.cs" Inherits="bookcar" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style23 {
            color: #0066FF;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function SetVisibleControl(value) {
            if (value == 'หมดอายุ') {
                CPanel.SetClientVisible(true);
            } else {
                CPanel.SetClientVisible(false);
            }
        }

        function SetVisibleControl1(value) {
            if (value == '0' || value == '1') {
                btnsAdd.SetClientVisible(false);
                sgvw.SetClientVisible(false);
            } else {
                btnsAdd.SetClientVisible(true);
                sgvw.SetClientVisible(true);
            }
        }


        // <![CDATA[
        var uploadCompleteFlag;
        function FilesUploadComplete(s, e) {
            uploadCompleteFlag = true;
            if (e.errorText != "") {
                ShowMessage(e.errorText);
                return false
            }
        }
        function ShowMessage(message) {
            alert(message);
        }
        function FileUploadStart(s, e) {
            uploadCompleteFlag = false;
        }
        function ShowPopupProgressingPanel() {
            if (!uploadCompleteFlag) {
                pbProgressing1.SetPosition(0);
            }
        }
        function UploadingProgressChanged(s, e) {
            pbProgressing1.SetPosition(e.progress);
            var info = e.currentFileName + "&emsp;[" + GetKBytes(e.uploadedContentLength) + " / " + GetKBytes(e.totalContentLength) + "] KBytes";
        }
        function GetKBytes(bytes) {
            return Math.floor(bytes / 1024);
        }
        // ]]> 

        function OpenPopup() {
            //            var x = screen.width / 2 - 400 / 2;
            //            var y = screen.height / 2 - 210 / 2;
            //            window.open('book-car-progress.aspx?v=' + rblImport.GetValue(), '', "width=400,height=230,scrollbars=1,left=" + x + ",top=" + y);

            window.location = 'book-car-progress.aspx?v=' + rblImport.GetValue();
        };



        var fifouploadCompleteFlag;
        function fifoFilesUploadComplete(s, e) {
            fifouploadCompleteFlag = true;
            if (e.errorText != "") {
                ShowMessage(e.errorText);
                return false
            }
        }

        function fifoFileUploadStart(s, e) {
            fifouploadCompleteFlag = false;
        }
        function fifoShowPopupProgressingPanel() {
            if (!fifouploadCompleteFlag) {
                fifopbProgressing1.SetPosition(0);
            }
        }
        function fifoUploadingProgressChanged(s, e) {
            fifopbProgressing1.SetPosition(e.progress);
            var info = e.currentFileName + "&emsp;[" + GetKBytes(e.uploadedContentLength) + " / " + GetKBytes(e.totalContentLength) + "] KBytes";
        }

        function fifoOpenPopup() {
            //            var x = screen.width / 2 - 400 / 2;
            //            var y = screen.height / 2 - 210 / 2;
            //            window.open('book-car-progress.aspx?v=' + rblImport.GetValue(), '', "width=400,height=230,scrollbars=1,left=" + x + ",top=" + y);

            window.location = 'fifobook-car-progress.aspx?v=' + fiforblImport.GetValue();
        };



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){setdatepicker();inEndRequestHandler();eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px">
                        </td>
                    </tr>
                </table>
                <dx:ASPxButton ID="btnCallback" runat="server" ClientInstanceName="btnCallback" AutoPostBack="false"
                    ClientVisible="false">
                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('InsertExceltoDatabase');}" />
                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback(&#39;InsertExceltoDatabase&#39;);}"></ClientSideEvents>
                </dx:ASPxButton>
                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="1" Width="100%">
                    <%--gvw.CancelEdit();--%>
                    <ClientSideEvents ActiveTabChanged="function (s, e) {gvw1.CancelEdit();gvw2.CancelEdit();gvw3.CancelEdit();}" />
                    <TabPages>
                        <dx:TabPage Name="p1" Text="จัดแผนประจำวัน" Visible="false">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p2" Text="จ่ายงานลงคิว (FIFO)">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                    <table border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px" width="100%">
                                        <tr class="hidden">
                                            <td bgcolor="#FFFFFF" style="width: 25%">
                                                <span style="color: Red">แผนการจัดส่งสินค้า (แบบเร่งด่วน FIFO)</span>
                                            </td>
                                            <td colspan="2" style="width: 60%">
                                                <dx:ASPxLabel ID="lblDate2" runat="server">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 10%;">&nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 40%;">&nbsp;
                                            </td>
                                        </tr>
                                        <tr class="hidden">
                                            <td class="style28" style="width: 25%">
                                                <img height="16" src="Images/ic_ms_excel.gif" width="16" />
                                                <span class="style23">Import Data</span><span class="active">*</span> (เฉพาะไฟล์
                                                Excel)
                                            </td>
                                            <td align="left" class="style27" colspan="4" style="width: 100%">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="fifouplExcel1" runat="server" ClientInstanceName="fifouploader1"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="fifouplExcel_FileUploadComplete"
                                                                UploadMode="Standard">
                                                                <ValidationSettings MaxFileSize="10000000" ShowErrors="false" AllowedFileExtensions=".xlsx,.xls">
                                                                </ValidationSettings>
                                                                <ClientSideEvents UploadingProgressChanged="function(s,e){fifoUploadingProgressChanged(s,e);}"
                                                                    FilesUploadComplete="function(s,e){fifoFilesUploadComplete(s,e);}" FileUploadStart="function(s,e){fifoFileUploadStart(s,e);}"
                                                                    FileUploadComplete="function(s, e) {if(e.callbackData!=''){fifoOpenPopup();}else{dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xls หรือ .xlsx หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');} }"></ClientSideEvents>
                                                                <BrowseButton Text="Browse..">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                        </td>
                                                        <td>&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="fifobtnImport1" runat="server" Text="ยืนยันการนำเข้าข้อมูล" AutoPostBack="false">
                                                                <ClientSideEvents Click="function(s, e) { fifouploader1.Upload(); }"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="fiforblImport" runat="server" ClientInstanceName="fiforblImport"
                                                                SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="true" Text="ทุกรายการ" Value="1" />
                                                                    <dx:ListEditItem Text="เฉพาะรายการใหม่" Value="2" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <div style="float: left; width: 100px;">
                                                                Upload File :
                                                            </div>
                                                            <div style="float: left; width: 300px;">
                                                                <dx:ASPxProgressBar ID="fifopbProgressing1" ClientInstanceName="fifopbProgressing1"
                                                                    runat="server" Width="100%">
                                                                </dx:ASPxProgressBar>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="hidden">
                                            <td class="style28" valign="top">
                                                <span class="style23">Import History </span>
                                            </td>
                                            <td>
                                                <asp:Literal ID="fifoltlHistory1" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="width: 10%">คลังต้นทาง:
                                            </td>
                                            <td style="width: 20%">
                                                <dx:ASPxComboBox ID="cboTerminal2" ClientInstanceName="cboTerminal2" runat="server" TextFormatString="{1}" ValueField="STERMINALID" AutoPostBack="false" CssClass="form-control" Width="100%">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสคลังสินค้า" FieldName="STERMINALID" Width="80px" />
                                                        <dx:ListBoxColumn Caption="ชื่อคลังสินค้า" FieldName="SABBREVIATION" Width="100px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td align="right" style="width: 10%">ผู้ขนส่ง:
                                            </td>
                                            <td style="width: 20%">
                                                <dx:ASPxComboBox ID="cboVendor" runat="server" Width="100%" ClientInstanceName="cboVendor" CssClass="form-control" TextFormatString="{1}" ValueField="SVENDORID" IncrementalFilteringMode="Contains">

                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="200px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td align="right" style="width: 10%">พขร./ID:
                                            </td>
                                            <td ">
                                                <dx:ASPxTextBox ID="txtEmployee" runat="server"  Width="100%" NullText="ชื่อ,หมายเลขบัตรประชาชน" CssClass="form-control" >
                                                                                    </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="right">
                                                <dx:ASPxButton runat="server" ID="btnSearch" AutoPostBack="false" SkinID="_search" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton runat="server" ID="btnClear" AutoPostBack="false" Text="Clear" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Claer');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>

                                            <td colspan="6" align="right">รอจัด
                                                <dx:ASPxLabel ID="lblCarWait" runat="server" Text="0" ForeColor="Red" Font-Bold="true">
                                                </dx:ASPxLabel>
                                                คัน
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" bgcolor="#FFFFFF" class="style14" colspan="6">
                                                <dx:ASPxGridView ID="gvw2" runat="server" AutoGenerateColumns="False"
                                                    Style="margin-top: 0px" ClientInstanceName="gvw2" Width="100%" KeyFieldName="ID1"
                                                    SkinID="_gvw" OnAfterPerformCallback="gvw1_AfterPerformCallback">
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="2%">
                                                            <HeaderTemplate>
                                                                <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                                    ClientSideEvents-CheckedChanged="function(s, e) { gvw2.SelectAllRowsOnPage(s.GetChecked()); }">
                                                                </dx:ASPxCheckBox>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="รหัส" FieldName="NID" Visible="False">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="คลังต้นทาง" FieldName="SABBREVIATION" CellStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="คิวที่" FieldName="NNO"  CellStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="เวลา" HeaderStyle-HorizontalAlign="Center" FieldName="DDATE" CellStyle-HorizontalAlign="Center">
                                      
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lbldd" runat="server" Text='<%# Eval("DDATE", "{0:HH:mm น.}") %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="ผู้ขนส่ง" FieldName="SVENDORNAME">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="ทะเบียนรถ">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="หัว" HeaderStyle-HorizontalAlign="Center" FieldName="SHEADREGISTERNO">
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="ท้าย" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="10%" FieldName="STRAILERREGISTERNO">
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataTextColumn Caption="ความจุ" HeaderStyle-HorizontalAlign="Center" FieldName="NCAPACITY">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="ชื่อ พขร." HeaderStyle-HorizontalAlign="Center" FieldName="SEMPLOYEENAME">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>

                                                        <dx:GridViewDataColumn CellStyle-Cursor="hand" Caption="คิวงาน" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="imbedit" runat="server" Width="60px" Text="จัดแผน"
                                                                    AutoPostBack="False">
                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('EditClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle Cursor="hand">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn Caption="STERMINAL" FieldName="STERMINAL" Visible="false" />
                                                        <dx:GridViewDataTextColumn Caption="STERMINAL" FieldName="STRUCKID" Visible="false" />
                                                        <dx:GridViewDataTextColumn Caption="SVENDORID" FieldName="SVENDORID" Visible="false" />
                                                        <dx:GridViewDataTextColumn Caption="SEMPLOYEEID" FieldName="SEMPLOYEEID" Visible="False"/>
                                                        <dx:GridViewDataTextColumn Caption="SCONTRACTID" FieldName="SCONTRACTID" Visible="False"/>
                                                        <dx:GridViewDataTextColumn Caption="strailerid" FieldName="STRAILERID" Visible="False"/>
                                                        <dx:GridViewDataTextColumn Caption="sheadid" FieldName="SHEADID" Visible="False"/>
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            
                                                            <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                                
                                                                <tr>
                                                                    <td>วันที่</td>
                                                                    <td>
                                                                        <asp:TextBox ID="dtePlan" CssClass="datepicker" runat="server" />
                                                                        </td>
                                                                    <td>เที่ยวที่</td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cbxTimeWindow" runat="server" ClientInstanceName="cbxTimeWindow"
                                                                                        EnableCallbackMode="True" OnItemRequestedByValue="cbxTimeWindow_OnItemRequestedByValueSQL"
                                                                                        OnItemsRequestedByFilterCondition="cbxTimeWindow_OnItemsRequestedByFilterConditionSQL"
                                                                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="NLINE" Width="150px">
                                                                                        <Columns>
                                                                                            <dx:ListBoxColumn Caption="เที่ยวที่" FieldName="WINDOWTIME" Width="200px" />
                                                                                        </Columns>
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                            ValidationGroup="add">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField ErrorText="กรุณาระบุเที่ยว" IsRequired="True" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxComboBox>
                                                                        <dx:ASPxTextBox ID="dteDelivery" ClientVisible="false" runat="server">
                                                                                    </dx:ASPxTextBox>
                                                                        <dx:ASPxTextBox ID="cmbTerminal" ClientVisible="false" runat="server">
                                                                                    </dx:ASPxTextBox>
                                                                        
                                                                    </td>
                                                                    <td>จำนวน DO.</td>
                                                                    <td><dx:ASPxTextBox ID="txtDONum" runat="server" Width="100%">
                                                                        </dx:ASPxTextBox></td>
                                                                    <td><dx:ASPxButton ID="ASPxButton1" runat="server" ClientInstanceName="btnsAdd" AutoPostBack="false" CssClass="dxeLineBreakFix" Text="สร้าง"
                                                                            >
                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('AddDONum'); }" />
                                                                        </dx:ASPxButton>(ใส่ตัวเลขแล้วกดสร้าง)</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="7">
                                                                        <dx:ASPxGridView ID="gvDONum" runat="server" AutoGenerateColumns="False" Style="margin-top: 0px" ClientInstanceName="gvDONum" Width="100%" SkinID="_gvw">
                                                                            <Columns>
                                                                                
                                                                                <dx:GridViewDataColumn FieldName="DELIVERY" Caption="ระบุ Delivery No. (ระบุเลข DO. ให้ครบแล้วกด เพิ่มแผนงาน)" Settings-AllowSort="False">
                                                                                    <DataItemTemplate>
                           <dx:ASPxTextBox ID="txtDelivery" runat="server" ClientInstanceName="txtDelivery"  Width="130px" >
                                                                               
                                                                            </dx:ASPxTextBox>
                                                                                    </DataItemTemplate>
                                                                                </dx:GridViewDataColumn>
                                                                                
                                                                            </Columns>
                                                                        </dx:ASPxGridView>
                                                                    </td>
                                                                    
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td colspan="7">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="50%" align="right">
                                                                                    <dx:ASPxButton ID="btnSave" runat="server" Text="เพิ่มแผนงาน" AutoPostBack="False">
                                                                                        <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                                <td width="50%">
                                                                                    <dx:ASPxButton ID="btnCancel" runat="server" Text="ยกเลิก" AutoPostBack="False">
                                                                                        <%--<ClientSideEvents Click="function (s,e){gvw2.CancelEdit() ;}" />--%>
                                                                                        <ClientSideEvents Click="function (s,e){xcpn.PerformCallback('Cancel');}" />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>
                                                    <SettingsPager AlwaysShowPager="True">
                                                    </SettingsPager>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnDel2" runat="server" SkinID="_delete" Width="50px">
                                                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw2, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <%--"SELECT ROW_NUMBER () OVER (ORDER BY F.NID,F.NNO) AS ID1,F.NID,F.NNO,F.DDATE,F.SHEADREGISTERNO,F.STRAILERREGISTERNO,VP.SVENDORNAME,F.SEMPLOYEENAME,F.SPERSONALNO,F.STEL,F.STERMINAL,VP.SVENDORID,f.SEMPLOYEEID  FROM TFIFO F 
                                        LEFT JOIN (TVENDOR V INNER JOIN TVENDOR_SAP VP ON V.SVENDORID = VP.SVENDORID) ON F.SVENDORID = V.SVENDORID WHERE F.STERMINAL LIKE '%' || :sSTERMINALID || '%' AND (nvl(F.CPLAN,'0') = '0') AND TO_DATE(F.DDATE,'dd/MM/yyyy') = TO_DATE(SYSDATE,'dd/MM/yyyy') "--%>
                                    <asp:SqlDataSource ID="sds2" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                        CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                        EnableCaching="True" CacheKeyDependency="ckdChk" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY F.NID,F.NNO) AS ID1,F.NID,F.NNO,F.DDATE,F.SHEADREGISTERNO,F.STRAILERREGISTERNO,CASE WHEN nvl(VP.SVENDORNAME,'') = '' Then VP.SVENDORID else VP.SVENDORNAME  end AS  SVENDORNAME
  ,CASE WHEN nvl(us.FULLNAME,' ') = ' ' THEN  TEMPLOYEE_SAP.FNAME || ' ' || TEMPLOYEE_SAP.LNAME ELSE nvl(us.FULLNAME,'') END AS SEMPLOYEENAME
  ,us.SPERSONELNO as  SPERSONALNO,us.STEL as STEL,F.STERMINAL,VP.SVENDORID,f.SEMPLOYEEID ,
CASE WHEN F.STRAILERREGISTERNO IS NOT NULL THEN
(SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
GROUP BY Tt.NTOTALCAPACITY) ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY
FROM (TFIFO F 
LEFT JOIN (TVENDOR V INNER JOIN TVENDOR_SAP VP ON V.SVENDORID = VP.SVENDORID) ON F.SVENDORID = V.SVENDORID
LEFT JOIN TTRUCK t ON F.SHEADREGISTERNO = T.SHEADREGISTERNO)
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID
LEFT JOIN (
SELECT E.SEMPLOYEEID, E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,E.SPERSONELNO FROM TEMPLOYEE e 
INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID where  nvl(E.CACTIVE,'1') = '1' GROUP BY E.SEMPLOYEEID, E.INAME , ES.FNAME , ES.LNAME,E.STEL,E.SPERSONELNO) us 
ON us.SEMPLOYEEID = F.SEMPLOYEEID 
LEFT JOIN TEMPLOYEE_SAP ON TEMPLOYEE_SAP.empsapid = F.SEMPLOYEEID
WHERE  NVL(f.CACTIVE,'1') = '1' AND F.STERMINAL LIKE '%' || :SSTERMINALID || '%' AND (nvl(F.CPLAN,'0') = '0') AND TO_DATE(F.DDATE,'dd/MM/yyyy') = TO_DATE(SYSDATE,'dd/MM/yyyy') 
GROUP BY TEMPLOYEE_SAP.FNAME, TEMPLOYEE_SAP.LNAME, F.NID,F.NNO,F.DDATE,F.SHEADREGISTERNO,F.STRAILERREGISTERNO,VP.SVENDORNAME,us.FULLNAME,us.SPERSONELNO,us.STEL,F.STERMINAL,VP.SVENDORID,f.SEMPLOYEEID ,T.NTOTALCAPACITY"
                                        DeleteCommand="DELETE FROM FIFO WHERE NID = :NID" OnDeleted="sds_Deleted" OnDeleting="sds_Deleting">
                                        <DeleteParameters>
                                            <asp:SessionParameter Name="NID" SessionField="delPlanList" Type="Int32" />
                                        </DeleteParameters>
                                        <SelectParameters>
                                            <asp:ControlParameter Name="sSTERMINALID" ControlID="cboTerminal2" PropertyName="Value" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p3" Text="จัดแผนขนส่ง" ClientVisible="false">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                    <table border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px" width="100%">
                                        <tr>
                                            <td class="style28" colspan="2">
                                                <span class="active">แผนการจัดส่งสินค้า (แบบล่วงหน้า)</span> *แสดงเฉพาะรายการรถที่ยืนยันสัญญา
                                                และยังไม่ได้จัดลงแผน
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28" style="width: 25%">
                                                <img height="16" src="Images/ic_ms_excel.gif" width="16" />
                                                <span class="style23">Import Data</span><span class="active">*</span> (เฉพาะไฟล์
                                                Excel)
                                            </td>
                                            <td align="left" class="style27" colspan="2" style="width: 75%">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxUploadControl ID="uplExcel1" runat="server" ClientInstanceName="uploader1"
                                                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete"
                                                                UploadMode="Advanced">
                                                                <ValidationSettings MaxFileSize="10000000" ShowErrors="false" AllowedFileExtensions=".xlsx,.xls">
                                                                </ValidationSettings>
                                                                <ClientSideEvents UploadingProgressChanged="function(s,e){UploadingProgressChanged(s,e);}"
                                                                    FilesUploadComplete="function(s,e){FilesUploadComplete(s,e);}" FileUploadStart="function(s,e){FileUploadStart(s,e);}"
                                                                    FileUploadComplete="function(s, e) {if(e.callbackData!=''){OpenPopup();}else{dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xls หรือ .xlsx หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');} }"></ClientSideEvents>
                                                                <BrowseButton Text="Browse..">
                                                                </BrowseButton>
                                                            </dx:ASPxUploadControl>
                                                        </td>
                                                        <td>&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnImport1" runat="server" Text="ยืนยันการนำเข้าข้อมูล" AutoPostBack="false">
                                                                <ClientSideEvents Click="function(s, e) { uploader1.Upload(); }"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxRadioButtonList ID="rblImport" runat="server" ClientInstanceName="rblImport"
                                                                SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="true" Text="ทุกรายการ" Value="1" />
                                                                    <dx:ListEditItem Text="เฉพาะรายการใหม่" Value="2" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <div style="float: left; width: 100px;">
                                                                Upload File :
                                                            </div>
                                                            <div style="float: left; width: 300px;">
                                                                <dx:ASPxProgressBar ID="pbProgressing1" ClientInstanceName="pbProgressing1" runat="server"
                                                                    Width="100%">
                                                                </dx:ASPxProgressBar>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28" valign="top">
                                                <span class="style23">Import History </span>
                                            </td>
                                            <td>
                                                <asp:Literal ID="ltlHistory1" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF">
                                                <span class="style23">ระบุวันที่รถเข้ารับงาน<span class="active">*</span></span>
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="dtePlan1" runat="server">
                                                    <ClientSideEvents ValueChanged="function(s,e) {xcpn.PerformCallback();}" />
                                                    <ClientSideEvents ValueChanged="function(s,e) {xcpn.PerformCallback();}"></ClientSideEvents>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาเวลา" />
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาเวลา"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                                <%--<dx:ASPxDateEdit ID="dtePlan1" runat="server" SkinID="xdte">
                                                    <ClientSideEvents ValueChanged="function(s,e) {xcpn.PerformCallback();}" />
                                                    <ClientSideEvents ValueChanged="function(s,e) {xcpn.PerformCallback();}"></ClientSideEvents>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาเวลา" />
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาเวลา"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxDateEdit>--%>
                                            </td>
                                            <td width="15%">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                <span class="style23">ระบุคลัง (ต้นทาง)<span class="active">*</span></span>
                                            </td>
                                            <td align="left" class="style27">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cboTerminal1" ClientInstanceName="cboTerminal1" runat="server"
                                                                DataSourceID="sqlTerminal1" TextFormatString="{1}" ValueField="STERMINALID" AutoPostBack="false"
                                                                SelectedIndex="-1">
                                                                <ClientSideEvents SelectedIndexChanged="function (s, e) {xcpn.PerformCallback();}" />
                                                                <ClientSideEvents SelectedIndexChanged="function (s, e) {xcpn.PerformCallback();}"></ClientSideEvents>
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" />
                                                                    <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" />
                                                                </Columns>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sqlTerminal1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT t.STERMINALID, ts.STERMINALNAME FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON t.STERMINALID = ts.STERMINALID WHERE t.STERMINALID LIKE 'H%' OR t.STERMINALID LIKE 'K%' ORDER BY t.STERMINALID"></asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" bgcolor="#FFFFFF" class="style14" colspan="3">
                                                <div style="float: right">
                                                    คัน
                                                </div>
                                                <div style="float: right">
                                                    <dx:ASPxLabel ID="lblShowBookCar1" runat="server" Font-Bold="true" ForeColor="Red"
                                                        Text="0">
                                                    </dx:ASPxLabel>
                                                </div>
                                                <div style="float: right">
                                                    คัน จัดแล้ว
                                                </div>
                                                <div style="float: right">
                                                    <dx:ASPxLabel ID="lblShowConfirm1" runat="server" ForeColor="Red" Font-Bold="true"
                                                        Text="0">
                                                    </dx:ASPxLabel>
                                                </div>
                                                <div style="float: right">
                                                    <asp:LinkButton ID="lkbconfirmtruck" runat="server" OnClick="lkbconfirmtruck_Click">ยืนยันรถตามสัญญา</asp:LinkButton>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" bgcolor="#FFFFFF" class="style14" colspan="3">
                                                <dx:ASPxGridView ID="gvw1" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                                    Style="margin-top: 0px" ClientInstanceName="gvw1" Width="100%" KeyFieldName="ID1"
                                                    SkinID="_gvw" DataSourceID="sds1" OnAfterPerformCallback="gvw1_AfterPerformCallback">
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                                            <HeaderTemplate>
                                                                <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                                    ClientSideEvents-CheckedChanged="function(s, e) { gvw1.SelectAllRowsOnPage(s.GetChecked()); }">
                                                                </dx:ASPxCheckBox>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="3%"
                                                            VisibleIndex="1">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="2" FieldName="NPLANID" Visible="False">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="วัน-ที่จัดแผน" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                                            Width="13%" FieldName="SPLANDATE">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lbldd" runat="server" Text='<%# Eval("SPLANDATE", "{0:dd/MM/yyyy - HH:mm น.}") %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="รับสินค้าที่คลัง" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                            Width="10%" FieldName="STERMINALNAME">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataDateColumn Caption="วันที่จัดส่ง" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                                            Width="10%" FieldName="DDELIVERY">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataDateColumn>
                                                        <dx:GridViewDataTextColumn Caption="เที่ยวที่" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                                            Width="5%" FieldName="STIMEWINDOW">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Drop No." VisibleIndex="7" HeaderStyle-HorizontalAlign="Center"
                                                            Width="5%" FieldName="NDROP">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Outbound No." VisibleIndex="8" HeaderStyle-HorizontalAlign="Center"
                                                            Width="10%" FieldName="SDELIVERYNO">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Ship-to" VisibleIndex="9" HeaderStyle-HorizontalAlign="Center"
                                                            Width="10%" FieldName="SSHIPTO">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="ทะเบียนรถ">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="หัว" VisibleIndex="10" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="7%" FieldName="SHEADREGISTERNO">
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="ท้าย" VisibleIndex="11" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="7%" FieldName="STRAILERREGISTERNO">
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataTextColumn Caption="ปริมาณ" VisibleIndex="12" HeaderStyle-HorizontalAlign="Center"
                                                            Width="7%" FieldName="NVALUE">
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataColumn Width="5%" CellStyle-Cursor="hand" VisibleIndex="13">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False"
                                                                    AutoPostBack="False">
                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('EditClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle Cursor="hand">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn Caption="PlanListID" FieldName="SPLANLISTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn Caption="NNO" FieldName="NNO" Visible="false" />
                                                        <dx:GridViewDataTextColumn Caption="STERMINALID" FieldName="STERMINALID" Visible="false" />
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                                <tr style="background-color: Blue">
                                                                    <td colspan="4" align="center">
                                                                        <dx:ASPxLabel ID="lblShow" runat="server" Text="แก้ไข / Edit" Font-Bold="true" ForeColor="Yellow"
                                                                            Font-Italic="true">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="14%" bgcolor="#CCCCCC">วัน-เวลาจัดแผน
                                                                    </td>
                                                                    <td width="34%">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <dx:ASPxTextBox ID="dtePlan" runat="server" Value='<%# Eval("SPLANDATE") %>' ClientEnabled="false">
                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                            SetFocusOnError="True" Display="Dynamic">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุวันที่" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxTextBox>
                                                                                    <%--<dx:ASPxDateEdit ID="dtePlan" runat="server" SkinID="xdte" Value='<%# Eval("SPLANDATE") %>'
                                                                                        ClientEnabled="false">
                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                            SetFocusOnError="True" Display="Dynamic">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุวันที่" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxDateEdit>--%>
                                                                                </td>
                                                                                <td></td>
                                                                                <td>
                                                                                    <dx:ASPxTextBox ID="txtTime" runat="server" Width="100px" Text='<%# Eval("SPLANTIME") %>'
                                                                                        ClientVisible="false">
                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                            SetFocusOnError="True" Display="Dynamic">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุเวลา" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="16%" bgcolor="#CCCCCC">คลังต้นทาง
                                                                    </td>
                                                                    <td width="36%">
                                                                        <dx:ASPxComboBox ID="cmbTerminal" ClientInstanceName="cmbTerminal1" runat="server"
                                                                            ValueType="System.String" SkinID="xcbbATC" Width="200px" ValueField="STERMINALID"
                                                                            TextFormatString="{0} - {1}" CallbackPageSize="30" EnableCallbackMode="true"
                                                                            OnItemsRequestedByFilterCondition="cmbTerminal_OnItemsRequestedByFilterConditionSQL"
                                                                            OnItemRequestedByValue="cmbTerminal_OnItemRequestedByValueSQL">
                                                                            <ClientSideEvents ValueChanged="function(s,e){cbxTimeWindow.PerformCallback();cboHeadRegist.PerformCallback();cboTrailerRegist.PerformCallback();}"></ClientSideEvents>
                                                                            <Columns>
                                                                                <dx:ListBoxColumn FieldName="STERMINALID" Caption="รหัสคลังสินค้า" />
                                                                                <dx:ListBoxColumn FieldName="STERMINALNAME" Caption="ชื่อคลังสินค้า" />
                                                                            </Columns>
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                SetFocusOnError="True" Display="Dynamic">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RequiredField IsRequired="True" ErrorText="กรุณาระบุคลัง" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#CCCCCC">วัน - เวลาที่จัดส่ง
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <%--<dx:ASPxDateEdit ID="dteDelivery" runat="server" SkinID="xdte" Value='<%# Eval("DDELIVERY") %>'>
                                                                                        <ClientSideEvents ValueChanged="function(s,e){cbxTimeWindow.PerformCallback();cboHeadRegist.PerformCallback();cboTrailerRegist.PerformCallback();}">
                                                                                        </ClientSideEvents>
                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                            SetFocusOnError="True" Display="Dynamic">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField IsRequired="True" ErrorText="กรุณาเวลา" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxDateEdit>--%>
                                                                                    <dx:ASPxTextBox ID="dteDelivery" runat="server">
                                                                                        <ClientSideEvents ValueChanged="function(s,e){cbxTimeWindow.PerformCallback();cboHeadRegist.PerformCallback();cboTrailerRegist.PerformCallback();}"></ClientSideEvents>
                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                            SetFocusOnError="True" Display="Dynamic">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField IsRequired="True" ErrorText="กรุณาเวลา" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td>-
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxComboBox ID="cbxTimeWindow" runat="server" ClientInstanceName="cbxTimeWindow"
                                                                                        EnableCallbackMode="True" OnItemRequestedByValue="cbxTimeWindow_OnItemRequestedByValueSQL"
                                                                                        OnItemsRequestedByFilterCondition="cbxTimeWindow_OnItemsRequestedByFilterConditionSQL"
                                                                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="NLINE" Width="150px">
                                                                                        <Columns>
                                                                                            <dx:ListBoxColumn Caption="เที่ยวที่" FieldName="WINDOWTIME" Width="200px" />
                                                                                        </Columns>
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                            ValidationGroup="add">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField ErrorText="กรุณาระบุเที่ยว" IsRequired="True" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td bgcolor="#CCCCCC">ทะเบียนรถ (หัว-ท้าย)
                                                                    </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <%--SetVisibleControl(s.GetItem(s.GetSelectedIndex()).GetColumnText('DWATEREXPIRE'))--%>
                                                                                    <dx:ASPxComboBox ID="cboHeadRegist" runat="server" ClientInstanceName="cboHeadRegist"
                                                                                        EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL"
                                                                                        OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                                                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="150px">
                                                                                        <ClientSideEvents BeginCallback="function(s,e){LoadingPanel.Show();}" EndCallback="function(s,e){LoadingPanel.Hide();}"
                                                                                            SelectedIndexChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtVendorID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANSPORTID'));cboTrailerRegist.PerformCallback();cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')); txtSumValue.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('NCAPACITY'));}else{s.SetValue('');txtVendorID.SetValue('');cboTrailerRegist.PerformCallback();cboTrailerRegist.SetValue('');txtSumValue.SetValue('');} }" />
                                                                                        <Columns>
                                                                                            <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                                                                            <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                                                            <dx:ListBoxColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORNAME" Width="150px" />
                                                                                            <dx:ListBoxColumn Caption="ความจุ" FieldName="NCAPACITY" Width="80px" />
                                                                                            <dx:ListBoxColumn Caption="รหัสผู้ขนส่ง" FieldName="STRANSPORTID" Width="80px" />
                                                                                        </Columns>
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                            ValidationGroup="add">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxComboBox>
                                                                                </td>
                                                                                <td>-
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxComboBox ID="cboTrailerRegist" runat="server" ClientInstanceName="cboTrailerRegist"
                                                                                        EnableCallbackMode="True" OnItemRequestedByValue="cboTrailerRegist_OnItemRequestedByValueSQL"
                                                                                        OnItemsRequestedByFilterCondition="cboTrailerRegist_OnItemsRequestedByFilterConditionSQL"
                                                                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="STRAILERREGISTERNO" Width="150px">
                                                                                        <Columns>
                                                                                            <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                                                            <dx:ListBoxColumn Caption="ความจุ" FieldName="NCAPACITY" Width="80px" />
                                                                                        </Columns>
                                                                                    </dx:ASPxComboBox>
                                                                                    <dx:ASPxTextBox ID="txtVendorID" ClientInstanceName="txtVendorID" runat="server"
                                                                                        Width="170px" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <dx:ASPxPanel ID="ASPxPanel1" runat="server" ClientInstanceName="CPanel" ClientVisible="false">
                                                                                        <PanelCollection>
                                                                                            <dx:PanelContent>
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <dx:ASPxImage ID="ximg" runat="server" ImageUrl="Images/05.png">
                                                                                                            </dx:ASPxImage>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <dx:ASPxLabel ID="lblAlert" runat="server" ForeColor="Red" Text="มาตรวัดน้ำหมดอายุ">
                                                                                                            </dx:ASPxLabel>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </dx:PanelContent>
                                                                                        </PanelCollection>
                                                                                    </dx:ASPxPanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#CCCCCC">ปริมาณความจุของรถ
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtSumValue" ClientInstanceName="txtSumValue" runat="server"
                                                                            Width="100px" ClientEnabled="false" BackColor="Gainsboro">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                                <tr>
                                                                    <td width="10%" align="center" bgcolor="#CCCCCC">Drop No.
                                                                    </td>
                                                                    <td width="20%" align="center" bgcolor="#CCCCCC">Outbound No.
                                                                    </td>
                                                                    <td width="50%" align="center" bgcolor="#CCCCCC">Ship-to
                                                                    </td>
                                                                    <td width="10%" align="center" bgcolor="#CCCCCC">ปริมาณ
                                                                    </td>
                                                                    <td width="10%" rowspan="2" align="center">
                                                                        <dx:ASPxButton ID="btnsAdd" runat="server" ClientInstanceName="btnsAdd" Text="เพิ่ม"
                                                                            SkinID="_add">
                                                                            <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('AddDataToList'); }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <dx:ASPxTextBox ID="txtDrop" runat="server" Width="100%" Text='<%# Eval("NDROP") %>'>
                                                                            
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="float: left">
                                                                            <dx:ASPxTextBox ID="txtDelivery" runat="server" ClientInstanceName="txtDelivery"
                                                                                Width="130px" AutoPostBack="false">
                                                                                <ClientSideEvents TextChanged="function(s,e){if(((s.GetValue() + '').indexOf('8') == 0 && (s.GetValue() + '').length == '8') || (s.GetValue() + '').length == '10'){ xcpnOutBound.PerformCallback(s.GetValue())}}" />
                                                                            </dx:ASPxTextBox>
                                                                        </div>
                                                                        <div style="float: left">
                                                                            <table border="0" cellspacing="0" cellpadding="0" height="22px" width="15px">
                                                                                <tr valign="middle">
                                                                                    <td style="border-bottom-style: none; background-color: white; border-left-style: none; border-top-style: none; border-right-style: none"
                                                                                        class="dxeButtonEditButton_Aqua">
                                                                                        <table style="border-collapse: separate" class="dxbebt" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td class="dx">
                                                                                                        <img alt="v" src="Images/autocomplete.gif">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <dx:ASPxCallback ID="xcpnOutBound" ClientInstanceName="xcpnOutBound" runat="server"
                                                                                HideContentOnCallback="true" OnCallback="xcpnOutBound_Callback" ClientSideEvents-BeginCallback="function(s,e){LoadingPanel.Show();}"
                                                                                ClientSideEvents-EndCallback="function(s,e){LoadingPanel.Hide();eval(s.cpPopup); s.cpPopup = '';}">
                                                                            </dx:ASPxCallback>
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="float: left">
                                                                            <dx:ASPxTextBox ID="txtShipto" runat="server" ClientInstanceName="txtShipto" MaxLength="10"
                                                                                Width="150px" ReadOnly="true" Text='<%# Eval("SSHIPTO") %>' Border-BorderColor="#cccccc">
                                                                               
                                                                            </dx:ASPxTextBox>
                                                                        </div>
                                                                        <div style="float: left">
                                                                            -
                                                                        </div>
                                                                        <div style="float: left">
                                                                            <dx:ASPxTextBox ID="txtShiptoName" runat="server" ClientInstanceName="txtShiptoName"
                                                                                MaxLength="10" Width="280px" ReadOnly="true" Border-BorderColor="#cccccc">
                                                                            </dx:ASPxTextBox>
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <dx:ASPxTextBox ID="txtValue" runat="server" Width="100%" ClientInstanceName="txtValue"
                                                                            Text='<%# Eval("NVALUE") %>' ClientEnabled="false" Border-BorderColor="#cccccc">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                                                            Settings-ShowColumnHeaders="false" Style="margin-top: 0px" ClientInstanceName="sgvw"
                                                                            Width="100%" KeyFieldName="dtDrop" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                                                            SkinID="_gvw">
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn Caption="Drop" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="10%" FieldName="dtDrop">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="OutBound No." VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="20%" FieldName="dtDeliveryNo">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="Ship-to" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="18%" FieldName="dtShipto">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="Ship-toName" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="32%" FieldName="dtShiptoName">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="10%" FieldName="dtValue">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataColumn Width="10%" CellStyle-Cursor="hand" VisibleIndex="10">
                                                                                    <DataItemTemplate>
                                                                                        <dx:ASPxButton ID="imbedit" runat="server" SkinID="_delete" CausesValidation="False"
                                                                                            AutoPostBack="False">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('DelClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </DataItemTemplate>
                                                                                    <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataColumn>
                                                                            </Columns>
                                                                        </dx:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="50%" align="right">
                                                                                    <dx:ASPxButton ID="btnSave" runat="server" Text="ยืนยัน" AutoPostBack="False">
                                                                                        <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                                <td width="50%">
                                                                                    <dx:ASPxButton ID="btnCancel" runat="server" Text="ปิด" AutoPostBack="False">
                                                                                        <%--  <ClientSideEvents Click="function (s,e){gvw1.CancelEdit() ;}" />--%>
                                                                                        <ClientSideEvents Click="function (s,e){xcpn.PerformCallback('Cancel');}" />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>
                                                    <SettingsPager AlwaysShowPager="True">
                                                    </SettingsPager>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnDel1" runat="server" SkinID="_delete" Width="50px">
                                                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw1, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnAdd1" runat="server" SkinID="_add" AutoPostBack="False" Width="50px">
                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('AddNewRow'); }" />
                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback(&#39;AddNewRow&#39;); }"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:SqlDataSource ID="sds1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                        CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                        EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY PL.SPLANLISTID DESC) AS ID1,P.NPLANID,P.NNO,P.SPLANDATE,P.SPLANTIME,TS.STERMINALNAME,P.STERMINALID,P.DDELIVERY,P.STIMEWINDOW,PL.NDROP,PL.SDELIVERYNO,PL.SSHIPTO,P.SHEADREGISTERNO,P.STRAILERREGISTERNO,PL.NVALUE,PL.SPLANLISTID  FROM ((TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULEList pl ON P.NPLANID = PL.NPLANID) LEFT JOIN TTERMINAL t ON P.STERMINALID = T.STERMINALID) LEFT JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID WHERE P.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.STERMINALID = :STERMINALID AND (nvl(P.CFIFO,'0') = '0' AND TO_DATE(P.SPLANDATE,'dd/MM/yyyy') = TO_DATE(:DDATE,'dd/MM/yyyy')) "
                                        DeleteCommand="DELETE FROM TPLANSCHEDULEList WHERE SPLANLISTID = :SPLANLISTID"
                                        OnDeleted="sds_Deleted" OnDeleting="sds_Deleting">
                                        <DeleteParameters>
                                            <asp:SessionParameter Name="SPLANLISTID" SessionField="delPlanList" Type="Int32" />
                                        </DeleteParameters>
                                        <SelectParameters>
                                            <asp:ControlParameter Name="STERMINALID" ControlID="cboTerminal1" PropertyName="Value" />
                                            <asp:ControlParameter Name="DDATE" ControlID="dtePlan1" PropertyName="Value" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p4" Text="สรุปการจัดแผนประจำวัน">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl4" runat="server" SupportsDisabledAttribute="True">
                                    <table border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px" width="100%">
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxTextBox runat="server" ID="txtSerachPlan" Width="250px" NullText="เลข DO, ทะเบียนรถ" CssClass="dxeLineBreakFix">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxButton ID="btnSearchplan" runat="server" ClientInstanceName="btnSearchplan" Text="ค้นหา" CssClass="dxeLineBreakFix" AutoPostBack="false">
                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('SearchPlan'); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" bgcolor="#FFFFFF" class="style14">สรุปแผนประจำวันที่
                                                <dx:ASPxLabel ID="lblDateNow" runat="server" Font-Bold="true" Width="100px">
                                                </dx:ASPxLabel>
                                                ยืนยันรถตามสัญญา
                                                <dx:ASPxLabel ID="lblShowConfirm2" runat="server" Font-Bold="true" ForeColor="Red"
                                                    Text="0">
                                                </dx:ASPxLabel>
                                                คัน จัดแล้ว
                                                <dx:ASPxLabel ID="lblShowBookCar2" runat="server" Font-Bold="true" ForeColor="Red"
                                                    Text="0">
                                                </dx:ASPxLabel>
                                                คัน
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#FFFFFF" align="left" class="style14">
                                                <dx:ASPxGridView ID="gvw3" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw3"
                                                    DataSourceID="sds3" KeyFieldName="ID1" OnAfterPerformCallback="gvw1_AfterPerformCallback"
                                                    SkinID="_gvw" Style="margin-top: 0px" Width="100%">
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่" VisibleIndex="1" Width="3%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="ประเภทแผน" FieldName="STYPE" VisibleIndex="2"
                                                            Width="5%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="รหัส" FieldName="NPLANID" Visible="False" VisibleIndex="2">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="วัน-ที่จัดแผน" FieldName="SPLANDATE" VisibleIndex="3"
                                                            Width="8%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lbldd" runat="server" Text='<%# Eval("SPLANDATE", "{0:dd/MM/yyyy}") %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="รับสินค้าที่คลัง" FieldName="STERMINALNAME" VisibleIndex="4"
                                                            Width="10%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataDateColumn Caption="วันที่จัดส่ง" FieldName="DDELIVERY" VisibleIndex="5"
                                                            Width="8%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataDateColumn>
                                                        <dx:GridViewDataTextColumn Caption="เที่ยวที่" FieldName="STIMEWINDOW" VisibleIndex="6"
                                                            Width="5%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Drop No." FieldName="NDROP" VisibleIndex="7"
                                                            Width="5%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Outbound No." FieldName="SDELIVERYNO" VisibleIndex="8"
                                                            Width="10%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Ship-to" FieldName="SSHIPTO" VisibleIndex="9"
                                                            Width="10%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewBandColumn Caption="ทะเบียนรถ" VisibleIndex="10">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" VisibleIndex="10"
                                                                    Width="10%">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" VisibleIndex="11"
                                                                    Width="10%">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" VisibleIndex="12"
                                                            Width="5%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="13" Width="5%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="False" CausesValidation="False"
                                                                    Text="RePlan">
                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('EditClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle Cursor="hand">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn Caption="PlanListID" FieldName="SPLANLISTID" Visible="False">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="NNO" FieldName="NNO" Visible="False">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="STERMINALID" FieldName="STERMINALID" Visible="False">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="SEMPLOYEEID" FieldName="SEMPLOYEEID" Visible="False">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="SREMARK" FieldName="SREMARK" Visible="False">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="CFIFO" FieldName="CFIFO" Visible="False">
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                    <SettingsPager AlwaysShowPager="True">
                                                    </SettingsPager>
                                                    <Templates>
                                                        <EditForm>
                                                            <table border="0" cellpadding="3" cellspacing="2" width="100%">
                                                                <tr style="background-color: Blue">
                                                                    <td colspan="4" align="center">
                                                                        <dx:ASPxLabel ID="lblShow" runat="server" Text="แก้ไข / Edit" Font-Bold="true" ForeColor="Yellow"
                                                                            Font-Italic="true">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#CCCCCC" width="14%">ลักษณะการปรับแผน
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <dx:ASPxRadioButtonList ID="rblCheck" runat="server" RepeatDirection="Horizontal"
                                                                            SkinID="rblStatus">
                                                                            <ClientSideEvents ValueChanged="function (s, e) {SetVisibleControl1(s.GetValue()) }" />
                                                                            <Items>
                                                                                <dx:ListEditItem Text="แก้ไขแผน" Value="0" Selected="true" />
                                                                                <dx:ListEditItem Text="ยกเลิกแผน" Value="1" />
                                                                                <dx:ListEditItem Text="ยกเลิก/สร้างแผนใหม่" Value="2" />
                                                                            </Items>
                                                                        </dx:ASPxRadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#CCCCCC" width="14%">ทะเบียนรถ (หัว-ท้าย)
                                                                    </td>
                                                                    <td width="34%">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <%--SetVisibleControl(s.GetItem(s.GetSelectedIndex()).GetColumnText('DWATEREXPIRE'))--%>
                                                                                    <dx:ASPxComboBox ID="cboHeadRegist" runat="server" ClientInstanceName="cboHeadRegist"
                                                                                        EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegisTFIFO_OnItemRequestedByValueSQL"
                                                                                        OnItemsRequestedByFilterCondition="cboHeadRegisTFIFO_OnItemsRequestedByFilterConditionSQL"
                                                                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="150px">
                                                                                        <ClientSideEvents BeginCallback="function(s,e){LoadingPanel.Show();}" EndCallback="function(s,e){LoadingPanel.Hide();}"
                                                                                            SelectedIndexChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtVendorID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANSPORTID'));cboTrailerRegist.PerformCallback();cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'));txtSumValue.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('NCAPACITY'));}else{s.SetValue('');txtVendorID.SetValue('');cboTrailerRegist.PerformCallback();cboTrailerRegist.SetValue('');txtSumValue.SetValue('');} }" />
                                                                                        <Columns>
                                                                                            <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                                                                            <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                                                            <dx:ListBoxColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORNAME" Width="150px" />
                                                                                            <dx:ListBoxColumn Caption="ความจุ" FieldName="NCAPACITY" Width="80px" />
                                                                                            <dx:ListBoxColumn Caption="รหัสผู้ขนส่ง" FieldName="STRANSPORTID" Width="80px" />
                                                                                        </Columns>
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                            ValidationGroup="add">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxComboBox>
                                                                                </td>
                                                                                <td>-
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxComboBox ID="cboTrailerRegist" runat="server" ClientInstanceName="cboTrailerRegist"
                                                                                        EnableCallbackMode="True" OnItemRequestedByValue="cboTrailerRegisTFIFO_OnItemRequestedByValueSQL"
                                                                                        OnItemsRequestedByFilterCondition="cboTrailerRegisTFIFO_OnItemsRequestedByFilterConditionSQL"
                                                                                        SkinID="xcbbATC" TextFormatString="{0}" ValueField="STRAILERREGISTERNO" Width="150px">
                                                                                        <Columns>
                                                                                            <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                                                            <dx:ListBoxColumn Caption="ความจุ" FieldName="NCAPACITY" Width="80px" />
                                                                                        </Columns>
                                                                                    </dx:ASPxComboBox>
                                                                                    <dx:ASPxTextBox ID="txtVendorID" ClientInstanceName="txtVendorID" runat="server"
                                                                                        Width="170px" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <dx:ASPxPanel ID="ASPxPanel1" runat="server" ClientInstanceName="CPanel" ClientVisible="false">
                                                                                        <PanelCollection>
                                                                                            <dx:PanelContent>
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <dx:ASPxImage ID="ximg" runat="server" ImageUrl="Images/05.png">
                                                                                                            </dx:ASPxImage>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <dx:ASPxLabel ID="lblAlert" runat="server" ForeColor="Red" Text="มาตรวัดน้ำหมดอายุ">
                                                                                                            </dx:ASPxLabel>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </dx:PanelContent>
                                                                                        </PanelCollection>
                                                                                    </dx:ASPxPanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td bgcolor="#CCCCCC" width="16%">พนักงานขับรถ
                                                                    </td>
                                                                    <td width="36%">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="10" ClientInstanceName="cmbPersonalNo"
                                                                                        EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                                                                        OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                                                                        SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="SEMPLOYEEID" Width="250px">
                                                                                        <%--<ClientSideEvents ValueChanged="function (s, e) {lblName.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('FULLNAME'))}" />--%>
                                                                                        <Columns>
                                                                                            <dx:ListBoxColumn Caption="รหัสบัตรประชาชน" FieldName="SPERSONELNO" />
                                                                                            <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                                                                        </Columns>
                                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                            ValidationGroup="add">
                                                                                            <ErrorFrameStyle ForeColor="Red">
                                                                                            </ErrorFrameStyle>
                                                                                            <RequiredField ErrorText="เลือกชื่อพนักงาน" IsRequired="True" />
                                                                                        </ValidationSettings>
                                                                                    </dx:ASPxComboBox>
                                                                                </td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#CCCCCC">สาเหตุการปรับแผน <span style="color: Red">*</span>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <dx:ASPxTextBox ID="txtRemark" runat="server" Width="500px" Text='<%# Eval("SREMARK")%>'>
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="add">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RequiredField ErrorText="กรุณาระบุสาเหตุการปรับแผน" IsRequired="True" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#CCCCCC">ปริมาณความจุของรถ
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtSumValue" ClientInstanceName="txtSumValue" runat="server"
                                                                            Width="100px" ClientEnabled="false" BackColor="Gainsboro">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                                <tr>
                                                                    <td width="10%" align="center" bgcolor="#CCCCCC">Drop No.
                                                                    </td>
                                                                    <td width="20%" align="center" bgcolor="#CCCCCC">Outbound No.
                                                                    </td>
                                                                    <td width="50%" align="center" bgcolor="#CCCCCC">Ship-to
                                                                    </td>
                                                                    <td width="10%" align="center" bgcolor="#CCCCCC">ปริมาณ
                                                                    </td>
                                                                    <td width="10%" rowspan="2" align="center">
                                                                        <dx:ASPxButton ID="btnsAdd" runat="server" ClientInstanceName="btnsAdd" Text="เพิ่ม"
                                                                            SkinID="_add">
                                                                            <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('AddDataToList'); }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <dx:ASPxTextBox ID="txtDrop" runat="server" Width="100%" Text='<%# Eval("NDROP") %>'>
                                                                            
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="float: left">
                                                                            <dx:ASPxTextBox ID="txtDelivery" runat="server" ClientInstanceName="txtDelivery"
                                                                                Width="130px" AutoPostBack="false">
                                                                                <ClientSideEvents TextChanged="function(s,e){if(((s.GetValue() + '').indexOf('8') == 0 && (s.GetValue() + '').length == '8') || (s.GetValue() + '').length == '10'){ xcpnOutBound.PerformCallback(s.GetValue())}}" />
                                                                            </dx:ASPxTextBox>
                                                                        </div>
                                                                        <div style="float: left">
                                                                            <table border="0" cellspacing="0" cellpadding="0" height="22px" width="15px">
                                                                                <tr valign="middle">
                                                                                    <td style="border-bottom-style: none; background-color: white; border-left-style: none; border-top-style: none; border-right-style: none"
                                                                                        class="dxeButtonEditButton_Aqua">
                                                                                        <table style="border-collapse: separate" class="dxbebt" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td class="dx">
                                                                                                        <img alt="v" src="Images/autocomplete.gif">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <dx:ASPxCallback ID="xcpnOutBound" ClientInstanceName="xcpnOutBound" runat="server"
                                                                                HideContentOnCallback="true" OnCallback="xcpnOutBound_Callback" ClientSideEvents-BeginCallback="function(s,e){LoadingPanel.Show();}"
                                                                                ClientSideEvents-EndCallback="function(s,e){LoadingPanel.Hide();eval(s.cpPopup); s.cpPopup = '';}">
                                                                            </dx:ASPxCallback>
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="float: left">
                                                                            <dx:ASPxTextBox ID="txtShipto" runat="server" ClientInstanceName="txtShipto" MaxLength="10"
                                                                                Width="150px" ReadOnly="true" Text='<%# Eval("SSHIPTO") %>' Border-BorderColor="#cccccc">
                                                                                
                                                                            </dx:ASPxTextBox>
                                                                        </div>
                                                                        <div style="float: left">
                                                                            -
                                                                        </div>
                                                                        <div style="float: left">
                                                                            <dx:ASPxTextBox ID="txtShiptoName" runat="server" ClientInstanceName="txtShiptoName"
                                                                                MaxLength="10" Width="280px" ReadOnly="true" Border-BorderColor="#cccccc">
                                                                            </dx:ASPxTextBox>
                                                                        </div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <dx:ASPxTextBox ID="txtValue" runat="server" Width="100%" ClientInstanceName="txtValue"
                                                                            Text='<%# Eval("NVALUE") %>' ClientEnabled="false" Border-BorderColor="#cccccc">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                                                            Settings-ShowColumnHeaders="false" Style="margin-top: 0px" ClientInstanceName="sgvw"
                                                                            Width="100%" KeyFieldName="dtDrop" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                                                            SkinID="_gvw">
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn Caption="Drop" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="10%" FieldName="dtDrop">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="OutBound No." VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="20%" FieldName="dtDeliveryNo">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="Ship-to" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="18%" FieldName="dtShipto">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="Ship-toName" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="32%" FieldName="dtShiptoName">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                                                    Width="10%" FieldName="dtValue">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataColumn Width="10%" CellStyle-Cursor="hand" VisibleIndex="10">
                                                                                    <DataItemTemplate>
                                                                                        <dx:ASPxButton ID="imbedit" runat="server" SkinID="_delete" CausesValidation="False"
                                                                                            AutoPostBack="False">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('DelClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </DataItemTemplate>
                                                                                    <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataColumn>
                                                                            </Columns>
                                                                        </dx:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="right" width="50%">
                                                                                    <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" Text="ยืนยัน">
                                                                                        <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                                <td width="50%">
                                                                                    <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" Text="ปิด">
                                                                                        <%-- <ClientSideEvents Click="function (s,e){gvw3.CancelEdit() ;}" />--%>
                                                                                        <ClientSideEvents Click="function (s,e){xcpn.PerformCallback('Cancel');}" />
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <asp:SqlDataSource ID="sds3" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                        CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                        EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY P.CFIFO, P.SPLANDATE) AS ID1,CASE WHEN P.CFIFO IS NULL OR P.CFIFO = '0' THEN 'ประจำวัน'  ELSE 'FIFO' END AS STYPE,nvl(P.CFIFO,'0') AS CFIFO, P.NPLANID,P.NNO,P.SPLANDATE,P.SPLANTIME,TS.STERMINALNAME,P.STERMINALID,P.DDELIVERY,P.STIMEWINDOW,PL.NDROP,PL.SDELIVERYNO,PL.SSHIPTO,P.SHEADREGISTERNO,P.STRAILERREGISTERNO,PL.NVALUE,PL.SPLANLISTID,P.SREMARK,P.SEMPLOYEEID 
                                        FROM ((TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULEList pl ON P.NPLANID = PL.NPLANID) LEFT JOIN TTERMINAL t ON P.STERMINALID = T.STERMINALID) LEFT JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND to_char(P.DCREATE,'dd/MM/yyyy') = to_char(sysdate,'dd/MM/yyyy') AND P.STERMINALID LIKE '%' || CASE WHEN SUBSTR('' || :oTerminal,1,5) = '80000' THEN '' ELSE '' ||  :oTerminal END || '%' AND P.SHEADREGISTERNO||';'||P.STRAILERREGISTERNO||';'||PL.SDELIVERYNO LIKE '%'||:oSearch||'%'">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="oTerminal" SessionField="STERMINAL_DATA" />
                                            <asp:ControlParameter Name="oSearch" ControlID="txtSerachPlan" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                    <ClientSideEvents ActiveTabChanged="function (s, e) {gvw1.CancelEdit();gvw2.CancelEdit();gvw3.CancelEdit();}"></ClientSideEvents>
                </dx:ASPxPageControl>
                <dx:ASPxTextBox ID="txtCDAYTYPE" runat="server" Width="5px" Text="0" ClientVisible="false">
                </dx:ASPxTextBox>
                <dx:ASPxTextBox runat="server" Width="5px" ClientVisible="False" ID="txtCTYPE">
                </dx:ASPxTextBox>
                <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
                </dx:ASPxLoadingPanel>
                <asp:SqlDataSource ID="sdsTerminal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" />
                <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" />
                <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sdsTimeWindow" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" />
                <asp:SqlDataSource ID="sdsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <script type="text/javascript">
        setdatepicker();
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            setdatepicker();
        }
        function setdatepicker() {
            $(document).keypress(function (e) {
                if (e.which == 13) {
                    return false;
                }
            });
        }

    </script>
</asp:Content>
