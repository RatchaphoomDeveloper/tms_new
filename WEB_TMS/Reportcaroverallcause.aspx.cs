﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;

public partial class Reportcaroverallcause : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

        // SetData(truckID, reqID, reqType);
        //UpdateTotal();

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');
        string SUSERID = Session["UserID"] + "";
        string VENID = CommonFunction.ReplaceInjection(cboVendor.Value + "");

        switch (paras[0])
        {
            case "setcause":

                break;
        }
    }

    #region SetAutoComplete

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
          FROM ( 
                SELECT TVENDOR.SVENDORID,TVENDOR.SABBREVIATION FROM TVENDOR
                INNER JOIN 
                (
                    SELECT 
                       SVENDORID, TO_DATE(DEND) - TO_DATE(sysdate) as DateCountdown
                    FROM TCONTRACT
                    WHERE   TO_DATE(DEND) - TO_DATE(sysdate)  >= 0
                )O
                ON TVENDOR.SVENDORID = O.SVENDORID
                GROUP BY TVENDOR.SVENDORID,TVENDOR.SABBREVIATION)
         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    #endregion
}