﻿<%@ Page MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true" EnableViewState="true" CodeFile="Quarter_AlertVendor.aspx.cs" Inherits="Quarter_AlertVendor" StylesheetTheme="Aqua" EnableEventValidation="false" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Sys$_Application$addComponent(component) {
            /// <param name="component" type="Sys.Component"></param>
            var e = Function._validateParams(arguments, [
                { name: "component", type: Sys.Component }
            ]);
            if (e) throw e;


            var id = component.get_id();
            if (!id) throw Error.invalidOperation(Sys.Res.cantAddWithoutId);
            if (typeof (this._components[id]) !== 'undefined') throw Error.invalidOperation(String.format(Sys.Res.appDuplicateComponent, id));
            this._components[id] = component;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>
        </div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-4 text-right">
                        แจ้งผู้ขนส่ง :

                    </label>
                    <div class="col-md-4">
                        <asp:Label Text="text" runat="server" ID="lblSABBREVIATION" />
                        <asp:HiddenField ID="hidSVENDORID" runat="server" />
                        <asp:HiddenField ID="hidID" runat="server" />
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 text-right">
                        แจ้งผู้ขนส่ง :

                    </label>
                    <div class="col-md-4">
                        <asp:Label Text="text" runat="server" ID="lblYEAR" />
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 text-right">
                        ไตรมาส :

                    </label>
                    <div class="col-md-4">
                        <asp:Label Text="" runat="server" ID="lblQuarter" />
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 text-right">
                        เลขที่เอกสาร :

                    </label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtNo" runat="server" CssClass="form-control" />
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label class="col-md-4 text-right">
                        วันที่ :

                    </label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" />
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="clearfix"></div>

                <center>
                    <div class="form-group">
                        <input id="btnConfirm" runat="server" clientidmode="Static" type="button" value="แจ้งผู้ขนส่ง" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-default" />
                        &nbsp;&nbsp;
                     <input id="btnCancel" type="button" runat="server" clientidmode="Static" value="ยกเลิกเอกสาร" data-toggle="modal" data-target="#ModalConfirmCancel" class="btn btn-default" />
                    </div>
                    <div class="form-group">
                        <asp:Button Text="Preview" CssClass="btn btn-default" runat="server" OnClick="Preview_Click" ID="btnPreview" />
                        &nbsp;&nbsp;
                        <asp:Button Text="Export PDF" ID="btnPDF" CssClass="btn btn-default" runat="server" OnClick="brnPDF_Click" />
                        &nbsp;&nbsp;
                        <asp:Button Text="Export Word" ID="btnWord" CssClass="btn btn-default" runat="server" OnClick="btnWord_Click" />
                        &nbsp;&nbsp;
                        <asp:Button Text="ย้อนกลับ" ID="btnBack" CssClass="btn btn-default" runat="server" Visible="false"/>
                    </div>

                </center>


            </div>
            <p>&nbsp;</p>
            <div class="form-group">
            <div class="col-md-2">
            </div>
            <div class="">
                <div style="width: 980px;float:left" id="divBody" runat="server">
                </div>
            </div>
            <div class="col-md-2">
            </div>
                </div>
            <div class="clearfix"></div>
            


        </div>
    </div>
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK" TextTitle="ยืนยันการแจ้งผู้ขนส่ง" TextDetail="คุณต้องการแจ้งผู้ขนส่งใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmCancel" IDModel="ModalConfirmCancel" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmCancel_ClickOK" TextTitle="ยืนยันการยกเลิกเอกสาร" TextDetail="คุณต้องการยกเลิกเอกสารใช่หรือไม่ ?" />
</asp:Content>
