﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="EmailScheduleAdd.aspx.cs" Inherits="EmailScheduleAdd" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <br />
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblHeaderTab1" runat="server" Text="ตั้งค่าตารางส่งอีเมล์"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell style="text-align:right">
                                        <asp:Label ID="Label1" runat="server" Text="ประเภทอีเมล์ :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="ddlEmailType" runat="server" CssClass="form-control" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlEmailType_SelectedIndexChanged"></asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell style="text-align:right">
                                        <asp:Label ID="lblTemplateName" runat="server" Text="Template Name :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <%--<asp:TextBox ID="txtTemplateName" runat="server" CssClass="form-control" Width="600px"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlTemplate" runat="server" Width="600px" CssClass="form-control"></asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell style="text-align:right">
                                        <asp:Label ID="lblNo" runat="server" Text="ครั้งที่ :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtNo" runat="server" CssClass="form-control" Width="300px" style="text-align:center"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell style="text-align:right;">
                                        <asp:Label ID="lblDay" runat="server" Text="แจ้งเตือนล่วงหน้า (วัน) :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtDay" runat="server" CssClass="form-control" Width="300px" style="text-align:center"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell style="text-align:right; vertical-align:top">
                                        <asp:Label ID="lblStatus" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:RadioButtonList ID="radStatus" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave"/>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
