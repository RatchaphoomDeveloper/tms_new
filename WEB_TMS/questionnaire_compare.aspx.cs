﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity;
using OfficeOpenXml;

public partial class questionnaire_compare : PageBase
{
    string GroupPermission = "01";
    #region " Prop "
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected TQUESTIONNAIRE SearchCriteria
    {
        get { return (TQUESTIONNAIRE)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }

    protected DataTable VendorDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "VendorDDL"]; }
        set { ViewState[this.ToString() + "VendorDDL"] = value; }
    }
    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            InitForm();
            LoadMain();
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private bool ValidateSearch()
    {
        try
        {
            int? s = int.Parse(ddlYearSearchStart.SelectedValue);
            int? end = int.Parse(ddlYearSearchEnd.SelectedValue);

            if (s > end)
            {
                alertFail("ระบุปีไม่ถูกต้อง");
                return false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
            return false;
        }

        return true;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (ValidateSearch()) LoadMain();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        InitForm();
        LoadMain();
    }

    public void LoadMain()
    {

        try
        {
            string _err = string.Empty;
            SearchCriteria = new TQUESTIONNAIRE() { NYEAR = string.Empty, SVENDORID = string.Empty };
            int? startYear = 0;
            int? endYear = 0;
            if (ddlYearSearchStart.SelectedItem != null) startYear = int.Parse(ddlYearSearchStart.SelectedItem.Value);
            if (ddlYearSearchEnd.SelectedItem != null) endYear = int.Parse(ddlYearSearchEnd.SelectedItem.Value);
            if (ddlVendorSearch.SelectedItem != null) SearchCriteria.SVENDORID = ddlVendorSearch.SelectedItem.Value;
            bool OnlyComplete = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? true : false;
            SearhData = new QuestionnaireBLL().GetListScoreByYear(ref _err, startYear, endYear, SearchCriteria.SVENDORID, OnlyComplete);
            if (SearhData != null)
            {
                SearhData.Columns["ROWNUM"].ColumnName = "ลำดับ";
                SearhData.Columns["SVENDORNAME"].ColumnName = "ผู้ประกอบการขนส่ง";
            }
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    public void InitForm()
    {
        int StartYear = 2015;
        int cYear = DateTime.Today.Year;
        int endYear = cYear + 4;
        int index = 0;
        for (int i = StartYear; i <= endYear; i++)
        {
            ddlYearSearchStart.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
            ddlYearSearchEnd.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
        }

        // เลือกปีปัจจุบัน
        ListItem sel = ddlYearSearchStart.Items.FindByValue(StartYear.ToString());
        ListItem selEnd = ddlYearSearchEnd.Items.FindByValue(cYear.ToString());
        ddlYearSearchEnd.ClearSelection();
        ddlYearSearchEnd.ClearSelection();
        if (sel != null) sel.Selected = true;
        else ddlYearSearchStart.SelectedIndex = 0;

        if (selEnd != null) selEnd.Selected = true;
        else ddlYearSearchEnd.SelectedIndex = 0;

        ddlVendorSearch.SelectedIndex = 0;
        LoadVendor();
    }
    public void LoadVendor()
    {
        try
        {
            VendorDDL = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = VendorDDL.NewRow();
            dr["SABBREVIATION"] = "";
            VendorDDL.Rows.InsertAt(dr, 0);
            ddlVendorSearch.DataSource = VendorDDL;
            ddlVendorSearch.DataBind();

            if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
            {
                ddlVendorSearch.ClearSelection();
                ListItem lst = ddlVendorSearch.Items.FindByValue(user_profile.SVDID);
                if (lst != null) lst.Selected = true;
                ddlVendorSearch.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
        //    GotoDefault();
        //    return;
        //}
    }
    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Width = 50;
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Width = 50;
            e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;
            for (int i = 2; i < SearhData.Columns.Count; i++)
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
            }
        }
    }
    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        LoadMain();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (ValidateSearch()) ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {
            string _err = string.Empty;
            SearchCriteria = new TQUESTIONNAIRE() { NYEAR = string.Empty, SVENDORID = string.Empty };
            int? startYear = 0;
            int? endYear = 0;
            if (ddlYearSearchStart.SelectedItem != null) startYear = int.Parse(ddlYearSearchStart.SelectedItem.Value);
            if (ddlYearSearchEnd.SelectedItem != null) endYear = int.Parse(ddlYearSearchEnd.SelectedItem.Value);
            if (ddlVendorSearch.SelectedItem != null) SearchCriteria.SVENDORID = ddlVendorSearch.SelectedItem.Value;
            bool OnlyComplete = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? true : false;
            SearhData = new QuestionnaireBLL().GetListScoreByYear(ref _err, startYear, endYear, SearchCriteria.SVENDORID, OnlyComplete);
            if (SearhData != null)
            {
                SearhData.Columns["ROWNUM"].ColumnName = "ลำดับ";
                SearhData.Columns["SVENDORNAME"].ColumnName = "ผู้ประกอบการขนส่ง";
            }

            using (ExcelPackage pck = new ExcelPackage())
            {
                var ws = pck.Workbook.Worksheets.Add("audit_by_year");

                ws.Cells["A1"].LoadFromDataTable(SearhData, true);
                Response.Clear();
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=report_audit.xlsx");
                Response.End();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }
}