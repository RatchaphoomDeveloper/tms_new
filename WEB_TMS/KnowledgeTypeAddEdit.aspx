﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KnowledgeTypeAddEdit.aspx.cs" Inherits="KnowledgeTypeAddEdit" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info" id="divDetail" runat="server">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract6" id="acollapseFindContract6">รายละเอียด&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract6" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract6">
                    <div>
                        <br />
                        <asp:Table runat="server" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblKnowledgeTypet" runat="server" Text="KnowledgeType :&nbsp;"></asp:Label>
                                    <asp:Label ID="lblTypeRequire1" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:TextBox runat="server" ID="txtKnowledgeType" CssClass="form-control"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Width="15%" Style="text-align: right">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                    <asp:Label ID="lblTypeRequire2" runat="server" Text="*&nbsp;" ForeColor="Red"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="25%">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </asp:TableCell>
                                <asp:TableCell Width="10%">
                                        &nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <br />
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-success" Visible="false" Style="width: 120px;" data-toggle="modal"
                                data-target="#ModalConfirmBeforeSave" UseSubmitBehavior="false" />&nbsp;
                  <asp:Button ID="cmdCancelDoc" runat="server" Text="ยกเลิก"
                      CssClass="btn btn-md btn-hover btn-danger" Style="width: 120px;" data-toggle="modal"
                      UseSubmitBehavior="false" data-target="#ModalConfirmBeforeCancel" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmCancel" IDModel="ModalConfirmBeforeCancel"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmCancel_ClickOK"
        TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
