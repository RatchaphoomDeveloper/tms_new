﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="questionnaire_form_old.aspx.cs" Inherits="questionnaire_form" StylesheetTheme="Aqua" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.2" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .rdoListNoBorder {
            border: none;
        }

            .rdoListNoBorder tr td {
                border: none !important;
            }

        .marginTp {
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="cmbForm" />
                <asp:PostBackTrigger ControlID="cmdUpload" />
                <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnAddBy" />
                <asp:AsyncPostBackTrigger EventName="Click" ControlID="btnAddAS" />
            </Triggers>
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>บริษัทผู้ประกอบการขนส่ง</td>
                        <td>
                            <asp:DropDownList CssClass="form-control marginTp" ID="cmbVendor" runat="server" DataTextField="SABBREVIATION" DataValueField="SVENDORID"></asp:DropDownList></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" class="style24">วันที่ตรวจประเมิน
                        </td>
                        <td align="left">
                            <table width="250px" class="marginTp">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="dteDcheck" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="text-align: right;">สถานที่ตรวจประเมิน &nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" CssClass="form-control" runat="server" Width="260px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">รายชื่อคณะผู้ตรวจ
                        </td>
                        <td>
                            <table class="marginTp">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtBy" Width="180px" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;
                                        <asp:Button ID="btnAddBy" runat="server" Text="เพิ่ม" OnClick="btnAddd_Click" CssClass="btn btn-md btn-hover btn-info" Width="60px" />
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="grvBY" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowCommand="grvBY_RowCommand" ShowHeader="false"
                                DataKeyNames="NID"
                                CellPadding="4" GridLines="None" CssClass="table table-hover"
                                ShowHeaderWhenEmpty="false" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="ลำดับ">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>.
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNAME" runat="server" Text='<%# Eval("SNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="16px" Height="16px" Style="cursor: pointer" CommandName="Remove" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">รายชื่อผู้รับการตรวจ
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtAs" Width="180px" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;
                                        <asp:Button ID="btnAddAS" runat="server" OnClick="btnAddAS_Click" CssClass="btn btn-md btn-hover btn-info" Text="เพิ่ม" Width="60px" /></td>
                                </tr>
                            </table>
                            <asp:GridView ID="grvAS" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowCommand="grvAS_RowCommand" ShowHeader="false"
                                DataKeyNames="NID" CellPadding="4" GridLines="None" CssClass="table table-hover" ShowHeaderWhenEmpty="false" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                <Columns>
                                     <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="ลำดับ">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>.
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNAME" runat="server" Text='<%# Eval("SNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="16px" Height="16px" Style="cursor: pointer" CommandName="Remove" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </td>
                    </tr>
                    <tr>
                        <td valign="top">เอกสารแนบ
                        </td>
                        <td colspan="3" valign="top">
                            <div class="panel-body" style="padding: 0px;">
                                <div class="dataTable_wrapper">
                                    <div class="panel-body">
                                        <asp:Table ID="Table3" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell ColumnSpan="2">
                                                    <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                                        Width="350px" DataValueField="UPLOAD_ID">
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:FileUpload ID="fileUpload" runat="server" />
                                                </asp:TableCell><asp:TableCell>
                                                    <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" OnClick="cmdUpload_Click" Style="width: 100px" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                        <br />
                                        <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                            EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                            EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                            OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                            OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px" CausesValidation="True" Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>แบบฟอร์ม </td>
                        <td colspan="3">
                            <asp:DropDownList class="form-control selectpicker" ID="cmbForm" runat="server" Width="400" AutoPostBack="true" OnSelectedIndexChanged="cmbForm_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br />
                            <br />
                            <asp:Table ID="tblButtonHead" runat="server" Visible="false" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <center>
                                            <asp:Button ID="btnSaveTop" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" OnClick="btnSave_Click"
                                                Style="width: 120px" OnClientClick="return CheckAllChk();" />
                                            &nbsp;
                        <asp:Button ID="Button2" runat="server" Text="ปิด"
                            UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click"
                            Style="width: 100px" />
                                            <br />
                                        </center>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table style="width: 100%">
            <tr>
                <td colspan="4">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl1" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <% if (System.Net.Dns.GetHostName() == "OiRN-NB")
                                           { %>
                                        <asp:RadioButtonList ID="CheckAllRdo" runat="server" RepeatDirection="Horizontal" onchange="checkAll()"></asp:RadioButtonList>
                                        <% } %>
                                        <br />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab1" runat="server" Text="แบบทดสอบ 1"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label1" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint1" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView1" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnDataBound="GridView1_DataBound" HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" runat="server" CssClass="rdoListNoBorder" onchange="CallAll();"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl2" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab2" runat="server" Text="แบบทดสอบ 2"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label3" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint2" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView2" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" OnDataBound="GridView1_DataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" Text='<%# Eval("SVISITFORMNAME") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" runat="server" CssClass="rdoListNoBorder" onchange="CallAll();"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl3" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab3" runat="server" Text="แบบทดสอบ 3"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label5" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint3" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView3" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" OnDataBound="GridView1_DataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" CssClass="rdoListNoBorder" onchange="CallAll();" runat="server"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl4" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab4" runat="server" Text="แบบทดสอบ 4"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label9" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint4" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView4" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnDataBound="GridView1_DataBound"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" CssClass="rdoListNoBorder" onchange="CallAll();" runat="server"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl5" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab5" runat="server" Text="แบบทดสอบ 5"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label12" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint5" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView5" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" OnDataBound="GridView1_DataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" CssClass="rdoListNoBorder" onchange="CallAll();" runat="server"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl6" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab6" runat="server" Text="แบบทดสอบ 6"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label15" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint6" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView6" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" OnDataBound="GridView1_DataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" CssClass="rdoListNoBorder" onchange="CallAll();" runat="server"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl7" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab7" runat="server" Text="แบบทดสอบ 7"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label18" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint7" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView7" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" OnDataBound="GridView1_DataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" CssClass="rdoListNoBorder" onchange="CallAll();" runat="server"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl8" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab8" runat="server" Text="แบบทดสอบ 8"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label21" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint8" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView8" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" OnDataBound="GridView1_DataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" runat="server" onchange="CallAll();" CssClass="rdoListNoBorder"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl9" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab9" runat="server" Text="แบบทดสอบ 9"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label24" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint9" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView9" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound" OnDataBound="GridView1_DataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" CssClass="rdoListNoBorder" runat="server" onchange="CallAll();"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:Table ID="tbl10" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <asp:Label ID="lblHeaderTab10" runat="server" Text="แบบทดสอบ 10"></asp:Label>
                                                <div style="float: right;">
                                                    <asp:Label ID="Label27" runat="server" Text="คะแนน "></asp:Label>
                                                    <asp:Label ID="lblPoint10" runat="server" Text="0"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="dataTable_wrapper">
                                                    <div class="panel-body">
                                                        <asp:GridView ID="GridView10" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowDataBound="GridView1_RowDataBound"
                                                            CellPadding="4" GridLines="None" CssClass="table table-hover"
                                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnDataBound="GridView1_DataBound"
                                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="NTYPEVISITFORMID,NGROUPID,NVISITFORMID"
                                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="หัวข้อแบบประเมิน">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSVISITFORMNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ระดับคะแนน" HeaderStyle-Width="150px">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:RadioButtonList ID="RdoListPoint" CssClass="rdoListNoBorder" runat="server" onchange="CallAll();"></asp:RadioButtonList>
                                                                        <div style="display: none;">
                                                                            <asp:HiddenField ID="hdWEIGHT" runat="server" Value='<%# Eval("NWEIGHT") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="เหตุผลเพิ่มเติม">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRemark" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>รายละเอียดเพิ่มเติม
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <dx:ASPxMemo ID="txtDetail" runat="server" Height="100px" Width="400px">
                    </dx:ASPxMemo>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <table>
                        <tr>
                            <td>คะแนนรวม &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblQSumAll" ClientIDMode="Static" runat="server" Text="0"></asp:Label>
                            </td>
                            <td>&nbsp;คะแนน
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 50px;">
                <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13"></td>
                <td colspan="2" class="style13" style="padding-left: 5px;"></td>
            </tr>
        </table>

    </div>
    <center>
        <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" OnClick="btnSave_Click" Style="width: 120px" OnClientClick="return CheckAllChk();" />
        &nbsp;
        <asp:Button ID="btnClose" runat="server" Text="ปิด"
            UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click"
            Style="width: 100px" /><br />
        <br />
        <div style="display: none;">
            <asp:Button ID="btnClick" runat="server" Text="บันทึก"
                UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-info" OnClick="btnSave_Click"
                Style="width: 120px" />
        </div>
    </center>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>

            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    <script type="text/javascript">
        $(function () {
            CallAll(); // OnClientClick="return validate();"
        });

        function CheckAllChk() {
            var isValid = true;
            var rdoList = $("table[id*=RdoListPoint]");
            rdoList.each(function (key) {
                if (isValid) {
                    isValid = ($(this).find("input[type=radio]:checked").length > 0);
                    if (!isValid) {
                        alert("มีแบบสอบถามที่ยังกรอกคะแนนไม่ครบ");
                        return;
                    }
                }
            });
            return isValid;
        }

        function checkAll() {
            var radios = document.getElementsByName('<%=CheckAllRdo.ClientID %>');
            var val = 0;
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    val = radios[i].value;
                    break;
                }
            }

            $("input[type=radio][value=" + val + "]").attr('checked', 'checked');
            CallAll();
        }

        function CallAll() {
            var _sumAll = 0.0;

            for (i = 1; i <= 10; i++) {
                var _PointGrvCurr = 0.0;
                $("#cph_Main_GridView" + i.toString() + " input[type=radio]:checked").each(function () {
                    var _id = "cph_Main_GridView" + i.toString() + "_RdoListPoint";
                    var rdoId = $(this).attr("id");
                    var pieces = rdoId.replace(_id, "").split(/[\s_]+/);
                    var _hwId = pieces[pieces.length - 1];
                    var _hW = "cph_Main_GridView" + i.toString() + "_hdWEIGHT_" + _hwId.toString();
                    var _curr = parseFloat($("#" + _hW).val()) * parseFloat($(this).val());
                    _sumAll += _curr;
                    _PointGrvCurr += _curr;
                });
                $("#cph_Main_lblPoint" + i.toString()).text(_PointGrvCurr);
            }

            $("#lblQSumAll").text(_sumAll);
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            CallAll();
        });
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

