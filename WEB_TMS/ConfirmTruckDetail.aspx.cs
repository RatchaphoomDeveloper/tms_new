﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.ContractConfirm;

public partial class ConfirmTruckDetail : PageBase
{
    DataTable dt;
    DataSet ds;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        lblCSTANBY.Text = "จำนวนรถหมุนเวียน {0} คัน";
        lblCCONFIRM.Text = "จำนวนรถที่ยืนยัน {0} คัน";
        if (!IsPostBack)
        {
            //if (!(Permissions("97") || Permissions("7")))
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //}
            if (Session["CGROUP"] + string.Empty == "0")
            {
                //Response.Redirect("~/default.aspx");
                //return;
            }

            SetDrowDownList();
            if (Request.QueryString["str"] != null)
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                if (!string.IsNullOrEmpty(strQuery))
                {
                    string[] strData = strQuery.Split('&');
                    //SCONTRACTID & กลุ่มงาน & บริษัท & เลขที่สัญญา & คลัง & รถหมุนเวียน
                    hidSCONTRACTID.Value = strData[0];
                    txtGroup.Text = strData[1];
                    txtVendor.Text = strData[2];
                    txtContract.Text = strData[3];
                    ddlTTERMINAL.SelectedValue = strData[4];
                    cbCarRecycling.Checked = strData[5] == "N";
                    txtDate.Text = strData[6];
                    hidVendorID.Value = strData[7];
                    btnSearch_Click(null, null);
                }
            }
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //Response.Redirect("NotAuthorize.aspx");  
                btnSearch.Enabled = false;               
                btnExport.Enabled = false;
                btnCheckIVMS.Enabled = false;
                aConfirmCheckIVMS.HRef = "javascript:void(0);";

                btnSearch.CssClass = "btn btn-md bth-hover btn-info";
                btnExport.CssClass = "btn btn-md bth-hover btn-info";
                btnCheckIVMS.CssClass = "btn btn-md bth-hover btn-info";
                aConfirmCheckIVMS.Style.Add("cursor", "not-allowed");
                //gvData.Columns[11].Visible = false;
            }
            if (!CanWrite)
            {
                // gvw.Columns[0].Visible = false;
                //gvData.Columns[10].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {
        ddlTTERMINAL.DataTextField = "SABBREVIATIOn";
        ddlTTERMINAL.DataValueField = "STERMINALID";
        ddlTTERMINAL.DataSource = ContractBLL.Instance.TTERMINALSelect();
        ddlTTERMINAL.DataBind();
        ddlTTERMINAL.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        ddlTTERMINAL.SelectedIndex = 0;

        ddlCONFIRM_STATUS.DataValueField = "CAR_STATUS_ID";
        ddlCONFIRM_STATUS.DataTextField = "CAR_STATUS_NAME";
        ddlCONFIRM_STATUS.DataSource = ContractConfirmBLL.Instance.GetMcarConfirmStatus();
        ddlCONFIRM_STATUS.DataBind();
        ddlCONFIRM_STATUS.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
    }

    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txtDate.Text.Trim()))
            {
                alertFail("กรุณาเลือกวันที่ยืนยันรถ !!!");
                return;
            }
            GetData();
            gvData.DataSource = ds.Tables[0];
            gvData.DataBind();
            //gvTerminal.DataSource = ds.Tables[1];
            //gvTerminal.DataBind();
            lblItem.Text = ds.Tables[0].Rows.Count + string.Empty;
            //lblItemTerminal.Text = ds.Tables[1].Rows.Count + string.Empty;

            DataTerminal();
            DataIVMSStatus();
            DataGPS();
            DataStatus();
            VisibleColumnNow(false);
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        rblStatus.ClearSelection();
        txtTEXTSEARCH.Text = string.Empty;
        ddlTTERMINAL.SelectedIndex = 0;
        cbCarRecycling.Checked = false;
    }
    #endregion

    #region btnExport_Click
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            dt = DataTerminal();
            string I_LICENSE = string.Empty;
            DataSet dsIVMS = new DataSet();
            foreach (DataRow item in dt.Rows)
            {
                I_LICENSE = string.Empty;
                foreach (var itemLICENSE in ds.Tables[0].Select("STERMINALID = '" + item["STERMINALID"] + "'"))
                {
                    I_LICENSE += ",'" + itemLICENSE["SHEADREGISTERNO"] + "'";
                }
                if (!string.IsNullOrEmpty(I_LICENSE))
                {
                    I_LICENSE = I_LICENSE.Remove(0, 1);
                    dsIVMS = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(item["STERMINALID"] + string.Empty, I_LICENSE);
                    if (dsIVMS != null && dsIVMS.Tables.Count > 0 && dsIVMS.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow itemIVMS in dsIVMS.Tables[0].Rows)
                        {
                            DataRow[] drs = ds.Tables[0].Select(" SHEADREGISTERNO = '" + itemIVMS["VEHICLE_NUMBER"] + "'");
                            if (drs.Any())
                            {
                                DataRow dr = drs[0];
                                dr["IVMS_GPS_STAT_NOW"] = itemIVMS["GPS_STAT"] + string.Empty == "1" ? "Yes" : "No";
                                dr["IVMS_MDVR_STAT_NOW"] = itemIVMS["MDVR_STAT"] + string.Empty == "1" ? "Yes" : "No";
                                dr["IVMS_STATUS_NOW"] = itemIVMS["STATUS"];
                                dr["IVMS_RADIUS_KM_NOW"] = itemIVMS["Radius_Km"];
                                dr["IVMS_CHECKDATE_NOW"] = itemIVMS["DateTime"];
                            }
                        }
                    }
                }

            }

            //foreach (DataRow item in ds.Tables[0].Rows)
            //{
            //    dsIVMS = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(item["STERMINALID"] + string.Empty, item["SHEADREGISTERNO"] + string.Empty);
            //    if (dsIVMS != null && dsIVMS.Tables.Count > 0 && dsIVMS.Tables[0].Rows.Count > 0)
            //    {
            //        DataRow dr = dsIVMS.Tables[0].Rows[0];
            //        item["IVMS_GPS_STAT_NOW"] = dr["GPS_STAT"] + string.Empty == "1" ? "Yes" : "No";
            //        item["IVMS_MDVR_STAT_NOW"] = dr["MDVR_STAT"] + string.Empty == "1" ? "Yes" : "No";
            //        item["IVMS_STATUS_NOW"] = dr["STATUS"];
            //        item["IVMS_RADIUS_KM_NOW"] = dr["Radius_Km"];
            //        item["IVMS_CHECKDATE_NOW"] = dr["DateTime"];
            //    }
            //}
            VisibleColumnNow(true);
            lblItem.Text = ds.Tables[0].Rows.Count + string.Empty;

            DataTable dtIVMSStatus = DataIVMSStatus();
            DataTable dtIVMSStatusNow = DataIVMSStatusNow();
            DataTable dtGPS = DataGPS();
            DataTable dtStatus = DataStatus();

            gvData.DataSource = ds.Tables[0];
            gvData.DataBind();

            ds.Tables[0].Columns.Remove("ROWNUM");
            ds.Tables[0].Columns.Remove("CSTANBY");
            ds.Tables[0].Columns.Remove("CAR_STATUS_ID");
            ds.Tables[0].Columns.Remove("IVMS_GPS_STAT");
            ds.Tables[0].Columns.Remove("IVMS_MDVR_STAT");
            ds.Tables[0].Columns.Remove("CMA");
            ds.Tables[0].Columns.Remove("CCONFIRM");
            ds.Tables[0].Columns.Remove("STERMINALID");
            ds.Tables[0].Columns.Remove("STRUCKID");
            ds.Tables[0].Columns.Remove("CCONFIRM_SEND");
            ds.Tables[0].Columns.Remove("DCREATE");
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/ConfirmTruckDetailFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["ConfirmTruckDetail"];
            worksheet.InsertDataTable(ds.Tables[0], new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            ExcelWorksheet worksheet2 = workbook.Worksheets["ConfirmTruckIvmsStatus"];
            worksheet2.InsertDataTable(dtIVMSStatus, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            ExcelWorksheet worksheet3 = workbook.Worksheets["ConfirmTruckIvmsStatusPTT"];
            worksheet3.InsertDataTable(dtIVMSStatusNow, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            ExcelWorksheet worksheet4 = workbook.Worksheets["ConfirmTruckGPS"];
            worksheet4.InsertDataTable(dtGPS, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            dt.Columns.RemoveAt(0);
            dt.Columns.Remove("CCONFIRM");
            ExcelWorksheet worksheet5 = workbook.Worksheets["ConfirmTruckTerminal"];
            worksheet5.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            dtStatus.Columns.Remove("CCONFIRM");
            ExcelWorksheet worksheet6 = workbook.Worksheets["ConfirmTruckStatus"];
            worksheet6.InsertDataTable(dtStatus, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = "ConfirmTruckDetail_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region btnCheckIVMS_Click
    protected void btnCheckIVMS_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            dt = DataTerminal();
            string I_LICENSE = string.Empty;
            DataSet dsIVMS = new DataSet();
            foreach (DataRow item in dt.Rows)
            {
                I_LICENSE = string.Empty;
                foreach (var itemLICENSE in ds.Tables[0].Select("STERMINALID = '" + item["STERMINALID"] + "'"))
                {
                    I_LICENSE += ",'" + itemLICENSE["SHEADREGISTERNO"] + "'";
                }
                if (!string.IsNullOrEmpty(I_LICENSE))
                {
                    I_LICENSE = I_LICENSE.Remove(0, 1);
                    dsIVMS = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(item["STERMINALID"] + string.Empty, I_LICENSE);
                    if (dsIVMS != null && dsIVMS.Tables.Count > 0 && dsIVMS.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow itemIVMS in dsIVMS.Tables[0].Rows)
                        {
                            DataRow[] drs = ds.Tables[0].Select(" SHEADREGISTERNO = '" + itemIVMS["VEHICLE_NUMBER"] + "' AND STERMINALID = '" + itemIVMS["PLANTCODE"] + "'");
                            if (drs.Any())
                            {
                                DataRow dr = drs[0];
                                dr["IVMS_GPS_STAT_NOW"] = itemIVMS["GPS_STAT"] + string.Empty == "1" ? "Yes" : "No";
                                dr["IVMS_MDVR_STAT_NOW"] = itemIVMS["MDVR_STAT"] + string.Empty == "1" ? "Yes" : "No";
                                dr["IVMS_STATUS_NOW"] = itemIVMS["STATUS"];
                                dr["IVMS_RADIUS_KM_NOW"] = itemIVMS["Radius_Km"];
                                dr["IVMS_CHECKDATE_NOW"] = itemIVMS["DateTime"];
                            }
                        }
                    }
                }

            }

            //foreach (DataRow item in ds.Tables[0].Rows)
            //{
            //    dsIVMS = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(item["STERMINALID"] + string.Empty, item["SHEADREGISTERNO"] + string.Empty);
            //    if (dsIVMS != null && dsIVMS.Tables.Count > 0 && dsIVMS.Tables[0].Rows.Count > 0)
            //    {
            //        DataRow dr = dsIVMS.Tables[0].Rows[0];
            //        item["IVMS_GPS_STAT_NOW"] = dr["GPS_STAT"] + string.Empty == "1" ? "Yes" : "No";
            //        item["IVMS_MDVR_STAT_NOW"] = dr["MDVR_STAT"] + string.Empty == "1" ? "Yes" : "No";
            //        item["IVMS_STATUS_NOW"] = dr["STATUS"];
            //        item["IVMS_RADIUS_KM_NOW"] = dr["Radius_Km"];
            //        item["IVMS_CHECKDATE_NOW"] = dr["DateTime"];
            //    }
            //}
            VisibleColumnNow(true);
            lblItem.Text = ds.Tables[0].Rows.Count + string.Empty;

            DataIVMSStatus();
            DataIVMSStatusNow();
            DataGPS();
            DataStatus();

            gvData.DataSource = ds.Tables[0];
            gvData.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion
    #endregion

    #region VisibleColumnNow
    private void VisibleColumnNow(bool isVisible)
    {
        gvData.Columns[17].Visible = isVisible;
        gvData.Columns[18].Visible = isVisible;
        gvData.Columns[19].Visible = isVisible;
        gvData.Columns[20].Visible = isVisible;
        gvData.Columns[21].Visible = isVisible;
        gvGPS.Columns[4].Visible = isVisible;
        gvGPS.Columns[5].Visible = isVisible;
        gvGPS.Columns[6].Visible = isVisible;
        if (!isVisible)
        {
            gvIVMSStatusNow.DataSource = null;
            gvIVMSStatusNow.DataBind();
        }
    }
    #endregion

    #region GetData
    private void GetData()
    {
        ds = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailSelect(txtDate.Text.Trim(), ddlTTERMINAL.SelectedValue, hidSCONTRACTID.Value, txtTEXTSEARCH.Text.Trim(), cbCarRecycling.Checked ? "N" : "Y", ddlCONFIRM_STATUS.SelectedValue, rblStatus.SelectedValue, false);

        long CSTANBY = ds.Tables[0].AsEnumerable().Count(it => (it["CSTANBY"] + string.Empty).Trim() != "Y" && (it["CCONFIRM"] + string.Empty).Trim() == "1" && (it["CCONFIRM_SEND"] + string.Empty).Trim() == "1");
        long CCONFIRM = ds.Tables[0].AsEnumerable().Count(it => (it["CCONFIRM"] + string.Empty).Trim() == "1" && (it["CCONFIRM_SEND"] + string.Empty).Trim() == "1");
        lblCSTANBY.Text = string.Format(lblCSTANBY.Text, CSTANBY / 2);
        lblCCONFIRM.Text = string.Format(lblCCONFIRM.Text, CCONFIRM - (CSTANBY / 2));
    }
    #endregion

    #region DataTerminal
    private DataTable DataTerminal()
    {
        DataTable dtTerminal = new DataTable();
        dtTerminal.Columns.Add("STERMINALID", typeof(string));
        dtTerminal.Columns.Add("SABBREVIATION", typeof(string));
        dtTerminal.Columns.Add("CCONFIRM", typeof(string));
        dtTerminal.Columns.Add("CSTANBY_N", typeof(string));
        dtTerminal.Columns.Add("CSTANBY", typeof(string));
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            var query = from row in ds.Tables[0].AsEnumerable()
                        group row by new
                        {
                            SABBREVIATION = row["SABBREVIATION"] + string.Empty
                            ,
                            STERMINALID = row["STERMINALID"] + string.Empty
                        } into groupdata
                        orderby groupdata.Key.STERMINALID
                        select new
                        {
                            SABBREVIATION = groupdata.Key.SABBREVIATION,
                            STERMINALID = groupdata.Key.STERMINALID,
                            CCONFIRM = groupdata.Count(it => it["CCONFIRM"] + string.Empty == "1" && (it["CCONFIRM_SEND"] + string.Empty).Trim() == "1"),
                            CSTANBY_N = groupdata.Count(it => it["CCONFIRM"] + string.Empty == "1" && (it["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it["CSTANBY"] + string.Empty == "Y"),
                            CSTANBY = groupdata.Count(it => it["CCONFIRM"] + string.Empty == "1" && (it["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it["CSTANBY"] + string.Empty == "N"),
                        };
            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.SABBREVIATION))
                {
                    dtTerminal.Rows.Add(item.STERMINALID, item.SABBREVIATION, item.CCONFIRM, item.CSTANBY_N, item.CSTANBY);
                }

            }
            gvTerminal.DataSource = dtTerminal;
            gvTerminal.DataBind();
            lblItemTerminal.Text = dtTerminal.Rows.Count + string.Empty;
        }
        return dtTerminal;
    }
    #endregion

    #region DataIVMSStatus
    private DataTable DataIVMSStatus()
    {
        DataTable dtData = new DataTable();
        dtData.Columns.Add("IVMS_STATUS", typeof(string));
        dtData.Columns.Add("CCONFIRM", typeof(string));
        dtData.Columns.Add("IVMS_CHECKDATE", typeof(string));
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            var query = from row in ds.Tables[0].AsEnumerable()
                        group row by new
                        {
                            IVMS_STATUS = row["IVMS_STATUS"] + string.Empty
                        } into groupdata
                        orderby groupdata.Key.IVMS_STATUS
                        select new
                        {
                            IVMS_STATUS = groupdata.Key.IVMS_STATUS,
                            IVMS_CHECKDATE = groupdata.FirstOrDefault(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE"] + string.Empty)) == null ? string.Empty : groupdata.First(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE"] + string.Empty))["IVMS_CHECKDATE"] + string.Empty,
                            CCONFIRM = groupdata.Count(it => it["CCONFIRM"] + string.Empty == "1" && (it["CCONFIRM_SEND"] + string.Empty).Trim() == "1"),
                        };
            if (query != null)
            {
                foreach (var item in query)
                {
                    if (!string.IsNullOrEmpty(item.IVMS_STATUS))
                    {
                        dtData.Rows.Add(item.IVMS_STATUS, item.CCONFIRM, item.IVMS_CHECKDATE);
                    }
                }
            }

            gvIVMSStatus.DataSource = dtData;
            gvIVMSStatus.DataBind();
            lblItemIVMSStatus.Text = dtData.Rows.Count + string.Empty;
        }
        return dtData;
    }
    #endregion

    #region DataIVMSStatusNow
    private DataTable DataIVMSStatusNow()
    {
        DataTable dtData = new DataTable();
        dtData.Columns.Add("IVMS_STATUS_NOW", typeof(string));
        dtData.Columns.Add("CCONFIRM_NOW", typeof(string));
        dtData.Columns.Add("IVMS_CHECKDATE_NOW", typeof(string));
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            var query = from row in ds.Tables[0].AsEnumerable()
                        group row by new
                        {
                            IVMS_STATUS = row["IVMS_STATUS_NOW"] + string.Empty
                        } into groupdata
                        orderby groupdata.Key.IVMS_STATUS
                        select new
                        {
                            IVMS_STATUS = groupdata.Key.IVMS_STATUS,
                            IVMS_CHECKDATE = groupdata.FirstOrDefault(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE_NOW"] + string.Empty)) == null ? string.Empty : groupdata.First(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE_NOW"] + string.Empty))["IVMS_CHECKDATE_NOW"] + string.Empty,
                            CCONFIRM = groupdata.Count(it => it["CCONFIRM"] + string.Empty == "1" && (it["CCONFIRM_SEND"] + string.Empty).Trim() == "1"),
                        };
            foreach (var item in query)
            {
                if (!string.IsNullOrEmpty(item.IVMS_STATUS))
                {
                    dtData.Rows.Add(item.IVMS_STATUS, item.CCONFIRM, item.IVMS_CHECKDATE);
                }

            }
            gvIVMSStatusNow.DataSource = dtData;
            gvIVMSStatusNow.DataBind();
            lblItemIVMSStatusNow.Text = dtData.Rows.Count + string.Empty;
        }
        return dtData;
    }
    #endregion

    #region DataGPS
    private DataTable DataGPS()
    {
        DataTable dtData = new DataTable();
        dtData.Columns.Add("GPS_NAME", typeof(string));
        dtData.Columns.Add("CCONFIRM_YES", typeof(string));
        dtData.Columns.Add("CCONFIRM_NO", typeof(string));
        dtData.Columns.Add("IVMS_CHECKDATE", typeof(string));
        dtData.Columns.Add("CCONFIRM_YES_NOW", typeof(string));
        dtData.Columns.Add("CCONFIRM_NO_NOW", typeof(string));
        dtData.Columns.Add("IVMS_CHECKDATE_NOW", typeof(string));
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            DataRow dr = dtData.NewRow();
            dr["GPS_NAME"] = "กล้อง";
            int CCONFIRM_YES = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_MDVR_STAT") == "1" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            int CCONFIRM_NO = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_MDVR_STAT") != "1" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            DataRow drFirst = ds.Tables[0].AsEnumerable().FirstOrDefault(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE"] + string.Empty));
            string IVMS_CHECKDATE = string.Empty;
            if (drFirst != null)
            {
                IVMS_CHECKDATE = drFirst.Field<string>("IVMS_CHECKDATE");
            }
            int CCONFIRM_YES_NOW = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_MDVR_STAT_NOW") == "Yes" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            int CCONFIRM_NO_NOW = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_MDVR_STAT_NOW") != "Yes" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            drFirst = ds.Tables[0].AsEnumerable().FirstOrDefault(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE_NOW"] + string.Empty));
            string IVMS_CHECKDATE_NOW = string.Empty;
            if (drFirst != null)
            {
                IVMS_CHECKDATE_NOW = drFirst.Field<string>("IVMS_CHECKDATE_NOW");
            }
            dr["CCONFIRM_YES"] = CCONFIRM_YES + string.Empty;
            dr["CCONFIRM_NO"] = CCONFIRM_NO + string.Empty;
            dr["IVMS_CHECKDATE"] = IVMS_CHECKDATE + string.Empty;
            dr["CCONFIRM_YES_NOW"] = CCONFIRM_YES_NOW + string.Empty;
            dr["CCONFIRM_NO_NOW"] = CCONFIRM_NO_NOW + string.Empty;
            dr["IVMS_CHECKDATE_NOW"] = IVMS_CHECKDATE_NOW + string.Empty;
            dtData.Rows.Add(dr);


            dr = dtData.NewRow();
            dr["GPS_NAME"] = "GPS";
            CCONFIRM_YES = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_GPS_STAT") == "1" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            CCONFIRM_NO = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_GPS_STAT") != "1" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            //drFirst = ds.Tables[0].AsEnumerable().FirstOrDefault(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE"] + string.Empty));
            //IVMS_CHECKDATE = ds.Tables[0].AsEnumerable().First(it => !string.IsNullOrEmpty(it["IVMS_CHECKDATE"] + string.Empty)).Field<string>("IVMS_CHECKDATE");

            CCONFIRM_YES_NOW = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_GPS_STAT_NOW") == "Yes" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            CCONFIRM_NO_NOW = ds.Tables[0].AsEnumerable().GroupBy(it => it["STRUCKID"]).Count(it => it.First().Field<string>("IVMS_GPS_STAT_NOW") != "Yes" && (it.First()["CCONFIRM_SEND"] + string.Empty).Trim() == "1" && it.First()["CCONFIRM"] + string.Empty == "1");
            //IVMS_CHECKDATE_NOW = ds.Tables[0].Rows[0].Field<string>("IVMS_CHECKDATE_NOW");
            dr["CCONFIRM_YES"] = CCONFIRM_YES + string.Empty;
            dr["CCONFIRM_NO"] = CCONFIRM_NO + string.Empty;
            dr["IVMS_CHECKDATE"] = IVMS_CHECKDATE + string.Empty;
            dr["CCONFIRM_YES_NOW"] = CCONFIRM_YES_NOW + string.Empty;
            dr["CCONFIRM_NO_NOW"] = CCONFIRM_NO_NOW + string.Empty;
            dr["IVMS_CHECKDATE_NOW"] = DateTime.Now.ToString(DateTimeFormat);
            dtData.Rows.Add(dr);
            gvGPS.DataSource = dtData;
            gvGPS.DataBind();
            lblItemGPS.Text = dtData.Rows.Count + string.Empty;
        }
        return dtData;
    }
    #endregion

    #region DataStatus
    private DataTable DataStatus()
    {
        DataTable dtData = new DataTable();
        dtData.Columns.Add("STATUS_NAME", typeof(string));
        dtData.Columns.Add("CCONFIRM", typeof(string));
        dtData.Columns.Add("CSTANBY_N", typeof(string));
        dtData.Columns.Add("CSTANBY", typeof(string));
        dtData.Columns.Add("DCREATE", typeof(string));
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            DataRow dr = dtData.NewRow();
            dr["STATUS_NAME"] = "ยืนยัน";
            int CCONFIRM = ds.Tables[0].AsEnumerable().Count(it => it.Field<string>("CCONFIRM") == "1");
            int CSTANBY_N = ds.Tables[0].AsEnumerable().Count(it => it.Field<string>("CCONFIRM") == "1" && it["CSTANBY"] + string.Empty == "Y");
            int CSTANBY = ds.Tables[0].AsEnumerable().Count(it => it.Field<string>("CCONFIRM") == "1" && it["CSTANBY"] + string.Empty == "N") / 2;
            string DCREATE = ds.Tables[0].Rows[0].Field<DateTime>("DCREATE").ToString("dd/MM/yyyy HH:mm:ss");
            dr["CCONFIRM"] = CCONFIRM + string.Empty;
            dr["CSTANBY_N"] = CSTANBY_N + string.Empty;
            dr["CSTANBY"] = CSTANBY + string.Empty;
            dr["DCREATE"] = DCREATE + string.Empty;
            dtData.Rows.Add(dr);


            dr = dtData.NewRow();
            dr["STATUS_NAME"] = "ปฏิเสธ";
            CCONFIRM = ds.Tables[0].AsEnumerable().Count(it => it.Field<string>("CCONFIRM") != "1");
            CSTANBY_N = ds.Tables[0].AsEnumerable().Count(it => it.Field<string>("CCONFIRM") != "1" && (it["CSTANBY"] + string.Empty == "Y" || it["CSTANBY"] + string.Empty == string.Empty));
            CSTANBY = ds.Tables[0].AsEnumerable().Count(it => it.Field<string>("CCONFIRM") != "1" && it["CSTANBY"] + string.Empty == "N") / 2;
            dr["CCONFIRM"] = CCONFIRM + string.Empty;
            dr["CSTANBY_N"] = CSTANBY_N + string.Empty;
            dr["CSTANBY"] = CSTANBY + string.Empty;
            dr["DCREATE"] = DCREATE + string.Empty;
            dtData.Rows.Add(dr);
            gvStatus.DataSource = dtData;
            gvStatus.DataBind();
            lblItemStatus.Text = dtData.Rows.Count + string.Empty;
        }
        return dtData;
    }
    #endregion

    #region gvData_PageIndexChanging
    protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvData.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);
    }
    #endregion

    #region gvData_RowDataBound
    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;  
            HtmlAnchor aSHEADREGISTERNO = (HtmlAnchor)e.Row.FindControl("aSHEADREGISTERNO");
            if (aSHEADREGISTERNO != null)
            {
                string strQuery = aSHEADREGISTERNO.InnerText.Trim() + "&" + hidVendorID.Value;
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(strQuery);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                aSHEADREGISTERNO.HRef = "ConfirmTruckDetailCheckIVMS.aspx?str=" + encryptedValue;
            }
            if (drv["CCONFIRM"] + string.Empty != "1")
            {
                e.Row.ForeColor = Color.Red;

                foreach (TableCell item in e.Row.Cells)
                {
                    item.ForeColor = Color.Red;
                }
            }
            if ((drv["IVMS_GPS_STAT_NOW"] + string.Empty).ToUpper() == "NO")
            {
                e.Row.Cells[17].ForeColor = Color.Red;
            }
            if ((drv["IVMS_MDVR_STAT_NOW"] + string.Empty).ToUpper() == "NO")
            {
                e.Row.Cells[18].ForeColor = Color.Red;
            }
            if (!CanRead)
            {
                aSHEADREGISTERNO.HRef = "javascript:void(0);";
                aSHEADREGISTERNO.Style.Add("cursor", "not-allowed");
            }
        }
    }
    #endregion



}