﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using System.Globalization;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;

public partial class ReportCapacityCodition : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetCboYear();
            ListData();
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": ListData();
                break;
            case "save":

                int Rows = gvw.VisibleRowCount;
                for (int i = 0; i < Rows; i++)
                {
                    dynamic data = gvw.GetRowValues(i, "SDATE", "NAMEOFDATE");
                    ASPxTextBox txtMaxCapacity = gvw.FindRowCellTemplateControl(i,null, "txtMaxCapacity") as ASPxTextBox;
                    ASPxTextBox txtOUTSOURCE = gvw.FindRowCellTemplateControl(i, null, "txtOUTSOURCE") as ASPxTextBox;

                    string sDate = !string.IsNullOrEmpty(data[0] + "") ? DateTime.Parse(data[0] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
                  
                    if (!string.IsNullOrEmpty(txtMaxCapacity.Text) || !string.IsNullOrEmpty(txtOUTSOURCE.Text))
                    {
                        string checkData = "SELECT SDATE FROM TBL_RPT_CAPACITY WHERE TO_CHAR(SDATE,'dd/MM/yyyy') = '" + CommonFunction.ReplaceInjection(sDate) + "'";
                        int nCount = CommonFunction.Count_Value(conn, checkData);
                        if (nCount > 0)
                        {
                            string UPDATE = @"UPDATE TBL_RPT_CAPACITY 
                                              SET MAXCAPACITY = " + txtMaxCapacity.Text + @"
                                                  ,OUTSOURCE = " + txtOUTSOURCE.Text + @"
                                                  ,NAMEOFDATE = '" + data[1] + @"'
                                              WHERE TO_CHAR(SDATE,'dd/MM/yyyy') = '" + CommonFunction.ReplaceInjection(sDate) + "'";
                            AddTODB(UPDATE);
                        }
                        else
                        {
                            string Insert = "INSERT INTO TBL_RPT_CAPACITY (SDATE,MAXCAPACITY,OUTSOURCE,NAMEOFDATE) VALUES(TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + "','dd/MM/yyyy')," + txtMaxCapacity.Text + "," + txtOUTSOURCE.Text + ",'" + data[1] + "')";

                            AddTODB(Insert);
                        }
                    }

                }
                ListData();

                    break;
            case "back":
                break;
        }
    }

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    void ListData()
    {
        string sDate = "";
        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            sDate = "01/" + cboMonth.Value + "/" + cboYear.Value;
        }
        else
        {
            sDate = "01/" + (DateTime.Now.Month + "").PadLeft(2, '0') + "/" + DateTime.Now.Year;
        }

        string QUERY = @"
  SELECT  ROW_NUMBER() OVER (PARTITION BY  '' ORDER BY DL.SDATE)||'.' as NO,DL.SDATE , DL.NAMEOFDATE,CAP.MAXCAPACITY,CAP.OUTSOURCE
  FROM
  (
        select last_day(add_months(TO_DATE('" + sDate + "','dd/MM/yyyy'), -1))+1+rownum-1 as SDATE ,to_char(to_date((last_day(add_months(TO_DATE('" + sDate + @"','dd/MM/yyyy'), -1))+1+rownum-1),'dd/mm/yyyy'), 'DAY') as NAMEOFDATE
        from dual 
        connect by 
        rownum <= last_day(TO_DATE('" + sDate + "','dd/MM/yyyy'))-(last_day(add_months(TO_DATE('" + sDate + @"','dd/MM/yyyy'), -1))+1)+1
        ORDER BY last_day(add_months(TO_DATE('" + sDate + @"','dd/MM/yyyy'), -1))+1+rownum-1 ASC
  )DL 
  LEFT JOIN TBL_RPT_CAPACITY CAP
  ON TO_CHAR(CAP.SDATE,'dd/MM/yyyy') = TO_CHAR(DL.SDATE,'dd/MM/yyyy')    ";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        gvw.DataSource = dt;
        gvw.DataBind();
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion
}