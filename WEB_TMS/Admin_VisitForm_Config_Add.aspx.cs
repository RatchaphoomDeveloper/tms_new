﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_Entity;
using Utility;

public partial class Admin_VisitForm_Config_Add : PageBase
{
    #region " Prop "
    protected USER_PERMISSION Page_Status
    {
        get { return (USER_PERMISSION)ViewState[this.ToString() + "Page_Status"]; }
        set { ViewState[this.ToString() + "Page_Status"] = value; }
    }
    protected PAGE_ACTION PageAction
    {
        get { return (PAGE_ACTION)ViewState[this.ToString() + "PageAction"]; }
        set { ViewState[this.ToString() + "PageAction"] = value; }
    }

    protected string QDocID
    {
        get { return ViewState[this.ToString() + "QDocID"].ToString(); }
        set { ViewState[this.ToString() + "QDocID"] = value; }
    }
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        try
        {
            if (!IsPostBack)
            {
                user_profile = SessionUtility.GetUserProfileSession();
                CheckPermission();
                InitForm();
                if (Session["EditYEARID"] != null)
                {
                    PageAction = PAGE_ACTION.EDIT;
                    QDocID = Session["EditYEARID"].ToString();
                    Session["EditYEARID"] = null;
                    BindEdit();
                }
                else if (Session["ViewYEARID"] != null)
                {
                    PageAction = PAGE_ACTION.VIEW;
                    QDocID = Session["ViewYEARID"].ToString();
                    Session["ViewYEARID"] = null;
                    BindEdit();
                    SetViewMode();
                }
                else
                {
                    PageAction = PAGE_ACTION.ADD;
                    QDocID = string.Empty;

                }
                this.AssignAuthen();
            }
        }
        catch (Exception ex)
        {
            Session["ViewYEARID"] = null;
            Session["EditYEARID"] = null;
            alertFail(ex.Message);
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                
            }
            if (!CanWrite)
            {
                btnSave.Enabled = false;
                btnAdd.Enabled = false;
                btnSaveAction.Enabled = false;
                btnNotSave.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public void SetViewMode()
    {
        btnAdd.Enabled = false;
        btnAdd.Visible = false;
        ddlName.Enabled = false;
        ddlYear.Enabled = false;
        rdoOptionStatus.Enabled = false;
    }

    public void InitForm()
    {
        int StartYear = 2015;
        int cYear = DateTime.Today.Year;
        int endYear = cYear + 4;
        int index = 0;
        for (int i = StartYear; i <= endYear; i++)
        {
            ddlYear.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
        }

        ListItem sel = ddlYear.Items.FindByValue(cYear.ToString());
        if (sel != null) sel.Selected = true;
        else ddlYear.SelectedIndex = 0;

        BindDDLName();
    }

    public void BindDDLName()
    {
        string _err = string.Empty;
        DataTable dt = new QuestionnaireBLL().GetFormName(ref _err);
        DataRow dr = dt.NewRow();
        dr["STYPEVISITFORMNAME"] = "- เลือกข้อมูล -";
        dt.Rows.InsertAt(dr, 0);
        ddlName.DataSource = dt;
        ddlName.DataBind();
        ddlName.SelectedIndex = 0;
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_VisitForm_config.aspx");
    }
    public void BindEdit()
    {
        string _err = string.Empty;
        SearchFormConfig _search = new SearchFormConfig();
        _search.ID = QDocID;
        _search.CACTIVE = string.Empty;
        _search.STYPEVISITFORMNAME = string.Empty;
        _search.Y_CACTIVE = string.Empty;
        _search.YEAR = string.Empty;
        SearhData = new QuestionnaireBLL().GetConfig(ref _err, _search);

        if (SearhData != null)
        {
            string formName = SearhData.Rows[0]["FORMNAME"].ToString();
            string IS_ACTIVE = SearhData.Rows[0]["Y_CACTIVE"].ToString();
            string _strYear = SearhData.Rows[0]["YEAR"].ToString();
            string FormId = SearhData.Rows[0]["NTYPEVISITFORMID"].ToString();
            decimal percent = decimal.Parse(SearhData.Rows[0]["PERCENT"].ToString());
            ListItem sel = ddlYear.Items.FindByValue(_strYear);
            if (sel != null)
            {
                ddlYear.ClearSelection();
                sel.Selected = true;
            }

            ListItem selForm = ddlName.Items.FindByValue(FormId);
            ddlName.ClearSelection();
            if (selForm != null)
            {
                selForm.Selected = true;
            }
            else
            {
                ddlName.Items.Add(new ListItem() { Selected = true, Text = formName, Value = FormId });
            }

            ListItem selStatus = rdoOptionStatus.Items.FindByValue(IS_ACTIVE);
            if (selStatus != null)
            {
                rdoOptionStatus.ClearSelection();
                selStatus.Selected = true;
            }
            txtPercent.Text = percent.ToString("0.000");
        }
        else
        {
            alertSuccess(" ไม่พบข้อมูลที่เลือก", "admin_VisitForm_config.aspx");
        }
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }
    }

    public void GotoDefault()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>");
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(ddlYear.SelectedValue))
            {
                alertFail("กรุณาระบุปีที่ต้องการ");
                return;
            }

            if (string.IsNullOrEmpty(ddlName.SelectedItem.Value.ToString()))
            {
                alertFail("กรุณาเลือกชื่อฟอร์มที่ต้องการ");
                return;
            }

            if (string.IsNullOrEmpty(txtPercent.Text))
            {
                alertFail("กรุณาระบุเปอร์เซ็นต์คำนวณรายปี");
                return;
            }

            double Percent = 0;
            bool IsDecimal = double.TryParse(txtPercent.Text, out Percent);
            if ((!IsDecimal) || ((Percent != 0) && (Percent <= 0.0009)) || (Percent > 100))
            {
                alertFail("ข้อมูลคะแนนไม่ถูกต้อง (0.001-100)");
                return;
            }

            string _err = string.Empty;
            if (!string.IsNullOrEmpty(QDocID)) // Edit
            {
                //เช็คว่ามีการใช้งานหรือยัง 
                bool isUse = false;
                if (ddlName.SelectedValue.ToString() != SearhData.Rows[0]["NTYPEVISITFORMID"].ToString())
                {
                    if (ddlYear.SelectedValue == SearhData.Rows[0]["YEAR"].ToString())
                        isUse = new QuestionnaireBLL().IsFormInUse(ref _err, ddlYear.SelectedValue, int.Parse(SearhData.Rows[0]["NTYPEVISITFORMID"].ToString()));
                }
                if (isUse)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ssScript", "AlertCopy();", true);
                }
                else
                {
                    btnSaveAction_Click(null, null);
                }
            }
            else
            {
                TTYPEOFVISITFORM_YEARS item = new TTYPEOFVISITFORM_YEARS() { YEAR = int.Parse(ddlYear.SelectedItem.Value.Trim()), NTYPEVISITFORMID = int.Parse(ddlName.SelectedItem.Value.ToString()), ID = 0, IS_ACTIVE = rdoOptionStatus.SelectedValue,PERCENT= decimal.Parse(txtPercent.Text.Trim() )};
                new QuestionnaireBLL().AddEditConfig(ref _err, item, user_profile);
                if (!string.IsNullOrEmpty(_err))
                {
                    alertFail(_err);
                    return;
                }
                else
                {
                    alertSuccess("เพิ่มข้อมูลเรียบร้อยแล้ว", "admin_visitform_config.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnSaveAction_Click(object sender, EventArgs e)
    {
        string _err = string.Empty;
        TTYPEOFVISITFORM_YEARS item = new TTYPEOFVISITFORM_YEARS() { YEAR = int.Parse(ddlYear.SelectedItem.Value.Trim()), NTYPEVISITFORMID = int.Parse(ddlName.SelectedItem.Value.ToString()), ID = 0, IS_ACTIVE = rdoOptionStatus.SelectedValue, PERCENT = decimal.Parse(txtPercent.Text.Trim()) };
        item.ID = int.Parse(QDocID.ToString());
        new QuestionnaireBLL().AddEditConfig(ref _err, item, user_profile);
        if (!string.IsNullOrEmpty(_err))
        {
            alertFail(_err);
            return;
        }
        else
        {
            alertSuccess("บันทึกข้อมูลเรียบร้อย", "admin_visitform_config.aspx");
        }
    }
}