﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class Truck_Moving : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Event
        gvwTruckMove.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTruckMove_AfterPerformCallback);
        gvwTruckMove.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwTruckMove_HtmlRowPrepared);
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {
            if (!Permissions("52")) { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='admin_home.aspx';<script>"); return; }
            ClearSession();
            DataView dv = (DataView)sdsTRUCK.Select(new DataSourceSelectArguments());
            Session["dtTRUCK"] = dv.ToTable();
            Session["dtSource"] = dv.ToTable();
            gvwTruckMove.DataBind();
        }
    }
    protected void xcpnMain_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "SEARCH":
                PrepareData();
                break;
            case "SEARCH_CANCEL":
                txtTruckHNo.Text = "";
                txtTruckTNo.Text = "";
                PrepareData();
                break;
        }
    }
    protected void gvwTruckMove_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        if (eArgs.Length < 2) return;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default:

                break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "UNLOCK":
                UnlockTruck(visibleindex);
                break;
            case "UPDATE":
                UpdateTruck(visibleindex);
                break;
        }
    }
    protected void gvwTruckMove_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":
                ASPxButton btnLock = (ASPxButton)gvwTruckMove.FindRowCellTemplateControl(e.VisibleIndex, null, "btnLock");
                ASPxLabel lblHTRUCK = (ASPxLabel)gvwTruckMove.FindRowCellTemplateControl(e.VisibleIndex, null, "lblHTRUCK");
                ASPxLabel lblTTRUCK = (ASPxLabel)gvwTruckMove.FindRowCellTemplateControl(e.VisibleIndex, null, "lblTTRUCK");
                ASPxComboBox cmbHTRUCK = (ASPxComboBox)gvwTruckMove.FindRowCellTemplateControl(e.VisibleIndex, null, "cmbHTRUCK");
                ASPxComboBox cmbTTRUCK = (ASPxComboBox)gvwTruckMove.FindRowCellTemplateControl(e.VisibleIndex, null, "cmbTTRUCK");
                ASPxButton btnUpdate = (ASPxButton)gvwTruckMove.FindRowCellTemplateControl(e.VisibleIndex, null, "btnUpdate");

                lblHTRUCK.Text = e.GetValue("SHEADREGISTERNO").ToString();
                cmbHTRUCK.Text = e.GetValue("SHEADREGISTERNO").ToString();
                lblTTRUCK.Text = e.GetValue("STRAILERREGISTERNO").ToString();
                cmbTTRUCK.Text = e.GetValue("STRAILERREGISTERNO").ToString();

                string alert = "";

                if (e.GetValue("H_STRUCK").ToString().Trim() == "")
                {
                    btnLock.ImageUrl = "Images/unlock.png";
                    cmbHTRUCK.ClientInstanceName = "cmbHTRUCK_" + e.VisibleIndex;
                    lblHTRUCK.Visible = false;
                    cmbTTRUCK.Visible = false;
                    alert = @"if(cmbHTRUCK_" + e.VisibleIndex + @".GetValue()==null) 
                                { dxWarningRedirect('แจ้งเตือน','กรุณาระบุทะเบียนรถส่วนหัว',function(s,e){ cmbHTRUCK_" + e.VisibleIndex + ".Focus(); }); return; }";
                }
                else if (e.GetValue("T_STRUCK").ToString().Trim() == "")
                {
                    btnLock.ImageUrl = "Images/unlock.png";
                    cmbTTRUCK.ClientInstanceName = "cmbTTRUCK_" + e.VisibleIndex;
                    lblTTRUCK.Visible = false;
                    cmbHTRUCK.Visible = false;
                    alert = @"if(cmbTTRUCK_" + e.VisibleIndex + @".GetValue()==null) 
                                { dxWarningRedirect('แจ้งเตือน','กรุณาระบุทะเบียนรถส่วนหาง',function(s,e){ cmbTTRUCK_" + e.VisibleIndex + ".Focus(); }); return; }";
                }
                else
                {
                    btnLock.ImageUrl = "Images/lock.png";
                    btnLock.Cursor = "Pointer";
                    btnLock.ToolTip = "ยกเลิกการจับคู่";
                    btnLock.ClientSideEvents.Click = "function(s,e){ gvwTruckMove.PerformCallback('UNLOCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }";

                    cmbHTRUCK.Visible = false;
                    cmbTTRUCK.Visible = false;
                    btnUpdate.Enabled = false;
                }
                btnUpdate.ClientSideEvents.Click = @"function(s,e){ " + alert + @" var vis = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); 
                                                            dxConfirm('แจ้งเตือน','ท่านต้องการบันทึกเพื่อเปลี่ยนแปลงข้อมูลนี้ ใช่หรือไม่ ?'
                                                                ,function(s,e){ dxPopupConfirm.Hide(); gvwTruckMove.PerformCallback('UPDATE;'+vis); } 
                                                                ,function(s,e){ dxPopupConfirm.Hide(); }) 
                                                                }";
                if (Session["chkurl"] + "" == "1")
                {
                    btnLock.Enabled = false;
                    btnUpdate.Enabled = false;
                    cmbHTRUCK.ReadOnly = true;
                    cmbTTRUCK.ReadOnly = true;
                }
                break;
        }
    }
    protected void gvwTruckMove_PageIndexChanged(object sender, EventArgs e)
    {
        gvwTruckMove.DataBind();
    }
    protected void gvwTruckMove_DataBinding(object sender, EventArgs e)
    {
        gvwTruckMove.DataSource = (DataTable)Session["dtSource"];
    }
    protected void cmbHTRUCK_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        SqlDataSource sds = (SqlDataSource)source;
        sds.SelectCommand = @"SELECT STRUCKID,SHEADREGISTERNO FROM 
        (SELECT ROW_NUMBER()OVER(ORDER BY t.STRUCKID) AS RN , t.STRUCKID, t.SHEADREGISTERNO FROM TTRUCK t WHERE SCARTYPEID='3' and (t.STRUCKID LIKE :fillter or t.SHEADREGISTERNO LIKE :fillterN) )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sds.SelectParameters.Add("fillterN", TypeCode.String, String.Format("%{1}%", e.Filter));
        sds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sds;
        comboBox.DataBind();
    }
    protected void cmbTTRUCK_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        SqlDataSource sds = (SqlDataSource)source;
        sds.SelectCommand = @"SELECT STRUCKID,SHEADREGISTERNO FROM 
        (SELECT ROW_NUMBER()OVER(ORDER BY t.STRUCKID) AS RN , t.STRUCKID, t.SHEADREGISTERNO FROM TTRUCK t WHERE SCARTYPEID='4' and (t.STRUCKID LIKE :fillter or t.SHEADREGISTERNO LIKE :fillterN) )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sds.SelectParameters.Add("fillterN", TypeCode.String, String.Format("%{1}%", e.Filter));
        sds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sds;
        comboBox.DataBind();
    }
    private void UnlockTruck(int visibleindex)
    {
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];
        dynamic dydata = gvwTruckMove.GetRowValues(visibleindex, "TRID", "CONTRACT", "H_STRUCK", "T_STRUCK");

        if ("" + dydata[1] != "")//หากเป็นรถในสัญญา
        {
            string nTruck = CommonFunction.Get_Value(conn, @"SELECT NTRUCK FROM TCONTRACT WHERE SCONTRACTID='" + dydata[1] + "'");
            nTruck = nTruck == "" ? "0" : nTruck;
            int allTruck = dtTruck.Select("CONTRACT='" + dydata[1] + "'").Length;

            //ทำการตรวจสอบเผื่อกรณีรถในสัญญาเดียวกันถูกปลดล๊อกมาแล้ว เพื่อรถจะไม่ถูกปลดล๊อกเกินจำนวนที่ปลดได้
            DataTable dt = new DataTable();
            if (Session["NTRUCK"] != null)
            {
                dt = (DataTable)Session["NTRUCK"];
                foreach (DataRow dr in dt.Select("SCONTRACTID='" + dydata[1] + "'"))
                    allTruck -= int.Parse(dr["NTRUCK"] + "");
            }
            else
            {
                dt.Columns.Add("SCONTRACTID", typeof(int));
                dt.Columns.Add("NTRUCK", typeof(int));
            }

            if (int.Parse(nTruck) >= allTruck)
            {//กรณีที่รถในสัญญานี้ไม่เพียงพอหากมีการถอดถอนโยกย้าย
                CommonFunction.SetPopupOnLoad(gvwTruckMove, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','ไม่สามารถทำการปลดรถในสัญญานี้ได้ เนื่องจากจำนวนรถในสัญญานี้มีไม่เพียงพอต่อการถอดถอนหรือโยกย้าย');");
                return;
            }
            else if ("" + CommonFunction.Get_Value(conn, "SELECT REF_SCONTRACTID FROM TCONTRACT_TRUCK WHERE STRUCKID='" + dydata[2] + "' AND STRAILERID='" + dydata[3] + "'") != "")
            {//กรณีที่รถถูกโยกย้ายมาจากสัญญาอื่น
                CommonFunction.SetPopupOnLoad(gvwTruckMove, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','ไม่สามารถทำการปลดรถในสัญญานี้ได้ เนื่องจากรถคันนี้ถูกโยกย้ายมาจากสัญญาอื่น');");
                return;
            }
            else
            {
                if (dt.Select("SCONTRACTID='" + dydata[1] + "'").Length > 0)
                {
                    foreach (DataRow dr in dt.Select("SCONTRACTID='" + dydata[1] + "'"))
                    {
                        dr.BeginEdit();
                        dr["NTRUCK"] = int.Parse(dr["NTRUCK"] + "") + 1;
                        dr.EndEdit();
                    }
                }
                else
                    dt.Rows.Add(new object[] { dydata[1] + "", 1 });
            }
        }

        int TRID = int.Parse(dtTruck.Compute("MIN(TRID)", string.Empty) + "");
        foreach (DataRow dr in dtTruck.Select("TRID='" + dydata[0] + "'"))
        {
            for (int i = 1; i <= 2; i++)//i=1 หัว i=2 หาง
            {
                DataRow nr = dtTruck.NewRow();
                nr["TRID"] = "" + (TRID - i);
                nr["SCARTYPEID"] = "" + dr["SCARTYPEID"];
                if (i == 1)
                {
                    nr["SHEADREGISTERNO"] = "" + dr["SHEADREGISTERNO"];
                    nr["H_STRUCK"] = "" + dr["H_STRUCK"];
                    nr["T_STRUCK"] = "";
                    nr["STRAILERREGISTERNO"] = "";
                }
                else
                {
                    nr["SHEADREGISTERNO"] = "";
                    nr["H_STRUCK"] = "";
                    nr["T_STRUCK"] = "" + dr["T_STRUCK"];
                    nr["STRAILERREGISTERNO"] = "" + dr["STRAILERREGISTERNO"];
                }
                nr["CHANGED"] = "U";
                nr["CARTYPE"] = "" + dr["CARTYPE"];
                nr["CONTRACT"] = dr["CONTRACT"];
                dtTruck.Rows.Add(nr);
            }

            dtTruck.Rows.Remove(dr);
            break;
        }
        Session["dtTRUCK"] = dtTruck;
        PrepareData();
    }
    private void UpdateTruck(int visibleindex)
    {
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];
        dynamic upddata = gvwTruckMove.GetRowValues(visibleindex, "TRID", "H_STRUCK", "T_STRUCK", "CHANGED", "SHEADREGISTERNO", "STRAILERREGISTERNO", "CONTRACT");
        ASPxComboBox cmbHTRUCK = (ASPxComboBox)gvwTruckMove.FindRowCellTemplateControl(visibleindex, null, "cmbHTRUCK");
        ASPxComboBox cmbTTRUCK = (ASPxComboBox)gvwTruckMove.FindRowCellTemplateControl(visibleindex, null, "cmbTTRUCK");

        foreach (DataRow dr in dtTruck.Select("TRID='" + upddata[0] + "'"))
        {
            string check = "";
            if (!string.IsNullOrEmpty(upddata[1] + "") && string.IsNullOrEmpty(upddata[2] + ""))//เพิ่มหางให้กับหัว
            {
                #region Match หาง -> หัว
                // รถที่จะ Match กันได้คือรถที่ในส่วนของหางและหัว ทั้งคู่นั้นลอยอยู่ ดังนั้นหากมีการเปลี่ยนแปลงที่ส่วนหางแสดงว่า H_STRUCK ของหางมีค่าเป็นค่าว่าง
                check = dtTruck.Select("H_STRUCK<>'' AND T_STRUCK='" + cmbTTRUCK.Value + "'").Length + "";//ตรวจสอบว่ารถส่วนหางคันนี้ Match กับหัวคันไหนอยู่บ้าง ที่ไม่ใช่หัวตัวเอง(H_STRUCK='') 
                if (check == "0")
                {
                    //Match รถสองคันนี้เข้าด้วยกัน
                    dr.BeginEdit();
                    dr["T_STRUCK"] = cmbTTRUCK.Value;
                    dr["STRAILERREGISTERNO"] = cmbTTRUCK.Text.Trim();
                    dr["CHANGED"] = "M";
                    dr.EndEdit();

                    //ลบ Record ที่เป็นหางทิ้ง
                    foreach (DataRow r in dtTruck.Select("ISNULL(H_STRUCK,'')='' AND T_STRUCK='" + cmbTTRUCK.Value + "'"))
                        r.Delete();
                    dtTruck.AcceptChanges();

                    Session["dtTRUCK"] = dtTruck;
                    SaveChange();

                }
                else
                {
                    CommonFunction.SetPopupOnLoad(gvwTruckMove, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','ไม่สามารถทำการโยกรถได้! เนื่องจากรถส่วนหางได้มีการทำสัญญาหรือถูกจับคู่กับรถคันอื่นแล้ว กรุณาทำการปลดล็อก');");
                    return;
                }
                #endregion
            }
            else if (!string.IsNullOrEmpty(upddata[2] + "") && string.IsNullOrEmpty(upddata[1] + ""))//เพิ่มหัวให้กับหาง
            {
                #region Match หัว -> หาง
                // รถที่จะ Match กันได้คือรถที่ในส่วนของหางและหัว ทั้งคู่นั้นลอยอยู่ ดังนั้นหากมีการเปลี่ยนแปลงที่ส่วนหัวแสดงว่า T_STRUCK ของหัวมีค่าเป็นค่าว่าง
                check = dtTruck.Select("T_STRUCK<>'' AND H_STRUCK='" + cmbHTRUCK.Value + "'").Length + "";//ตรวจสอบว่ารถส่วนหัวคันนี้ Match กับหางคันไหนอยู่บ้าง ที่ไม่ใช่หางตัวเอง(T_STRUCK='') 
                if (check == "0")
                {
                    //Match รถสองคันนี้เข้าด้วยกัน
                    dr.BeginEdit();
                    dr["H_STRUCK"] = cmbHTRUCK.Value;
                    dr["SHEADREGISTERNO"] = cmbHTRUCK.Text.Trim();
                    dr["CHANGED"] = "M";
                    dr.EndEdit();

                    //ลบ Record ที่เป็นหัวทิ้ง
                    foreach (DataRow r in dtTruck.Select("ISNULL(T_STRUCK,'')='' AND H_STRUCK='" + cmbHTRUCK.Value + "'"))
                        r.Delete();
                    dtTruck.AcceptChanges();

                    Session["dtTRUCK"] = dtTruck;
                    SaveChange();

                }
                else
                {
                    CommonFunction.SetPopupOnLoad(gvwTruckMove, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','ไม่สามารถทำการโยกรถได้! เนื่องจากรถส่วนหัวได้มีการทำสัญญาหรือถูกจับคู่กับรถคันอื่นแล้ว กรุณาทำการปลดล็อก');");
                    return;
                }
                #endregion
            }
            break;
        }
    }
    private void SaveChange()
    {
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];

        foreach (DataRow dr in dtTruck.Select("CHANGED <> 'NULL'"))
        {
            if (dr["CHANGED"] + "" == "M")
            {
                //BackUp
                InsertBack(dr["H_STRUCK"] + "", dr["CONTRACT"] + "");
                InsertBack(dr["T_STRUCK"] + "", dr["CONTRACT"] + "");
                //Update Change Truck
                int carType = int.Parse(dr["SCARTYPEID"] + "");
                UpdateToDB(dr["H_STRUCK"] + "", dr["T_STRUCK"] + "", dr["STRAILERREGISTERNO"] + "", carType + "", dr["CONTRACT"] + "");
                UpdateToDB(dr["T_STRUCK"] + "", dr["H_STRUCK"] + "", dr["SHEADREGISTERNO"] + "", (carType + 1) + "", dr["CONTRACT"] + "");
            }
            else
            {
                if (dr["H_STRUCK"] + "" != "")
                {
                    //BackUp
                    InsertBack(dr["H_STRUCK"] + "", dr["CONTRACT"] + "");
                    //Update Change Truck
                    int carType = int.Parse(dr["SCARTYPEID"] + "");
                    UpdateToDB(dr["H_STRUCK"] + "", dr["T_STRUCK"] + "", dr["STRAILERREGISTERNO"] + "", carType + "", dr["CONTRACT"] + "");
                }
                else
                {
                    //BackUp
                    InsertBack(dr["T_STRUCK"] + "", dr["CONTRACT"] + "");
                    //Update Change Truck
                    int carType = int.Parse(dr["SCARTYPEID"] + "");
                    UpdateToDB(dr["T_STRUCK"] + "", dr["H_STRUCK"] + "", dr["SHEADREGISTERNO"] + "", (carType + 1) + "", dr["CONTRACT"] + "");
                }
            }
        }
        gvwTruckMove.JSProperties["cpRedirectTo"] = "Truck_Moving.aspx";
        //cmbTruckType.SelectedIndex = 0;
        //txtTruckHNo.Text = "";
        //txtTruckTNo.Text = "";
        //ClearSession();
        //DataView dv = (DataView)sdsTRUCK.Select(new DataSourceSelectArguments());
        //Session["dtTRUCK"] = dv.ToTable();
    }
    private void PrepareData()
    {
        DataTable dtTRUCK = (DataTable)Session["dtTRUCK"];

        if (dtTRUCK.Select("CHANGED<>'NULL'").Length <= 0)
        {
            DataView dv = (DataView)sdsTRUCK.Select(new DataSourceSelectArguments());
            dtTRUCK = dv.ToTable();
        }
        string sCondi = "";
        if (txtTruckHNo.Text.Trim() != "" && txtTruckTNo.Text.Trim() != "")
            sCondi = @"SHEADREGISTERNO like '%" + txtTruckHNo.Text + "%' OR STRAILERREGISTERNO like '%" + txtTruckTNo.Text + "%'";
        else if (txtTruckHNo.Text.Trim() != "")
            sCondi = @"SHEADREGISTERNO like '%" + txtTruckHNo.Text + "%'";
        else if (txtTruckTNo.Text.Trim() != "")
            sCondi = @"STRAILERREGISTERNO like '%" + txtTruckTNo.Text + "%'";
        Session["dtSource"] = CommonFunction.ArrayDataRowToDataTable(dtTRUCK, dtTRUCK.Select("" + sCondi + "", "TRID ASC"));
        gvwTruckMove.DataBind();
    }
    private void InsertBack(string STRUCKID, string CONTRACT)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {
            con.Open();
            string nitem = CommonFunction.Get_Value(conn, "SELECT MAX(NITEM)+1 FROM TTRUCK_HIS WHERE STRUCKID='" + STRUCKID + "'");
            if (string.IsNullOrEmpty(nitem))
                nitem = "1";

            //เก็บประวัติรถ
            string strSql = @"INSERT INTO TTRUCK_HIS
(STRUCKID,NITEM,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE,SHOLDERID,SHOLDERNAME,SWUPDATE,DWUPDATE,TRUCK_CATEGORY,VOL_UOM,VEH_TEXT,CARCATE_ID,SERVICE_DATE,ROUTE,DEPOT,TPPOINT,CLASSGRP) 

(SELECT STRUCKID,{1}
,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE,SHOLDERID,SHOLDERNAME,SWUPDATE,DWUPDATE,TRUCK_CATEGORY,VOL_UOM,VEH_TEXT,CARCATE_ID,SERVICE_DATE,ROUTE,DEPOT,TPPOINT,CLASSGRP 
FROM TTRUCK WHERE STRUCKID='{0}')";

            SystemFunction.SQLExecuteNonQuery(conn, string.Format(strSql, STRUCKID, nitem));

            //เก็บประวัติความจุแป้น
            strSql = @"INSERT INTO TTRUCK_COMPART_HIS(STRUCKID,NITEM,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE) 
                      (SELECT STRUCKID,{1},NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE FROM TTRUCK_COMPART WHERE STRUCKID='{0}')";

            SystemFunction.SQLExecuteNonQuery(conn, string.Format(strSql, STRUCKID, nitem));

            //เก็บประวัติเอกสารรถ
            strSql = @"INSERT INTO TTRUCK_DOC_HIS(DOCID,STRUCKID,NITEM,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) 
                     (SELECT DOCID,STRUCKID,{1},DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME FROM TTRUCK_DOC WHERE STRUCKID='{0}')";

            SystemFunction.SQLExecuteNonQuery(conn, string.Format(strSql, STRUCKID, nitem));

            //เก็บประวัติการประกันภัย
            strSql = @"INSERT INTO TTRUCK_INSURANCE_HIS(STRUCKID,NITEM,CTYPE,SCOMPANY,START_DURATION,END_DURATION,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE) 
                     (SELECT STRUCKID,{1},CTYPE,SCOMPANY,START_DURATION,END_DURATION,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE 
                        FROM TTRUCK_INSURANCE WHERE STRUCKID='{0}')";

            SystemFunction.SQLExecuteNonQuery(conn, string.Format(strSql, STRUCKID, nitem));

            //เก็บประวัติเอกสารประกันภัย
            strSql = @"INSERT INTO TTRUCK_INSUREDOC_HIS(DOCID,STRUCKID,NITEM,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) 
                     (SELECT DOCID,STRUCKID,{1},CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME FROM TTRUCK_INSUREDOC WHERE STRUCKID='{0}')";

            SystemFunction.SQLExecuteNonQuery(conn, string.Format(strSql, STRUCKID, nitem));

            if (CONTRACT != "")//กรณีที่เป็นรถในสัญญา
            {
                string GenID = CommonFunction.Gen_ID(con, "SELECT HISTORY_ID FROM (SELECT HISTORY_ID+0 As HISTORY_ID FROM TCONTRACT_TRUCK_HISTORY ORDER BY HISTORY_ID DESC)  WHERE ROWNUM <= 1");

                strSql = @"INSERT INTO TCONTRACT_TRUCK_HISTORY(HISTORY_ID,DHISTORY,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,DREJECT,SREJECT) 
                         (SELECT '{1}',SYSDATE,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,SYSDATE,'{2}' FROM TCONTRACT_TRUCK WHERE STRUCKID='{0}' OR STRAILERID='{0}')";

                SystemFunction.SQLExecuteNonQuery(conn, string.Format(strSql, STRUCKID, GenID, "" + Session["UserID"]));
            }
        }
    }
    private void UpdateToDB(string STRUCKID, string TrailerID, string RegNo, string ScartypeID, string CONTRACT)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {
            con.Open();
            if (ScartypeID == "1" || ScartypeID == "3")//หัวรถ
            {
                string strSql = @"UPDATE TTRUCK SET STRAILERID=:STRAILERID, STRAILERREGISTERNO=:STRAILERREGISTERNO, DUPDATE=SYSDATE, SUPDATE=:SUPDATE WHERE STRUCKID=:STRUCKID";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                    com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = TrailerID;
                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = RegNo;
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                    com.ExecuteNonQuery();
                }

                if (CONTRACT != "")//กรณีที่เป็นรถในสัญญา
                {
                    strSql = @"DELETE FROM TCONTRACT_TRUCK WHERE NVL(STRAILERID,'0')<>'0' AND (STRUCKID=:STRUCKID OR STRAILERID=:STRAILERID)";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                        com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = TrailerID;
                        com.ExecuteNonQuery();
                    }
                }
            }
            else//หางรถ
            {
                string strSql = @"UPDATE TTRUCK SET SHEADID=:SHEADID , STRAILERREGISTERNO=:STRAILERREGISTERNO, DUPDATE=SYSDATE,SUPDATE=:SUPDATE WHERE STRUCKID=:STRUCKID";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCKID;
                    com.Parameters.Add(":SHEADID", OracleType.VarChar).Value = TrailerID;
                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = RegNo;
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                    com.ExecuteNonQuery();
                }

                if (CONTRACT != "")//กรณีที่เป็นรถในสัญญา
                {
                    strSql = @"DELETE FROM TCONTRACT_TRUCK WHERE NVL(STRAILERID,'0')<>'0' AND (STRUCKID=:STRUCKID OR STRAILERID=:STRAILERID)";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = TrailerID;
                        com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = STRUCKID;
                        com.ExecuteNonQuery();
                    }
                }
            }
        }
    }
    private void ClearSession()
    {
        string[] Sesarr = new string[] { "dtTRUCK", "NTRUCK" };
        foreach (string arr in Sesarr)
            Session.Remove(arr);
    }
    private bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
}