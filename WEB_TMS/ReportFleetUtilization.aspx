﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ReportFleetUtilization.aspx.cs" Inherits="ReportFleetUtilization" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
    <script type="text/javascript">
        window.__OriginalDXUpdateRowCellsHandler = ASPxClientTableFixedColumnsHelper.prototype.ChangeCellsVisibility;
        ASPxClientTableFixedColumnsHelper.prototype.ChangeCellsVisibility = function (row, startIndex, endIndex, display) {
            if ((row.cells.length == 0) || (row.cells[0].getAttribute("ci") == null))
                window.__OriginalDXUpdateRowCellsHandler(row, startIndex, endIndex - 1, display); // base call
            else {
                //custom processing
                for (var i = startIndex; i <= endIndex; i++) {
                    var cell = FindCellWithColumnIndex(row, i);
                    if (cell != null)
                        cell.style.display = display;
                }
            }
        };

        function FindCellWithColumnIndex(row, colIndex) {
            for (var i = 0; i < row.cells.length; i++) {
                if (row.cells[i].getAttribute("ci") == colIndex)
                    return row.cells[i];
            }
            return null;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                 <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                <label  class="col-md-1 control-label">กลุ่มงาน</label>
                <div class="col-md-5">
					<%--OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged" AutoPostBack="false"--%>
                    <asp:DropDownList runat="server" ID="ddlGroup" CssClass="form-control" AutoPostBack="false">
                    </asp:DropDownList>
                </div>

                <%--<label  class="col-md-1 control-label">กลุ่มงาน</label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="txtWorkGroup" ReadOnly="true" CssClass="form-control" />
                </div>--%>
            </div>
            <div class="row form-group">
                <label  class="col-md-1 control-label">วันที่</label>
                <div class="col-md-5">
                    <asp:TextBox runat="server" ID="txtDateStart" CssClass="datepicker" />
                </div>
                <label  class="col-md-1 control-label">ถึง</label>
                <div class="col-md-5 ">
                    <asp:TextBox runat="server" ID="txtDateEnd" CssClass="datepicker" />
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    <center>
                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" /></center>
                </div>
                
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    <dx:ASPxGridView ID="gvReport" runat="server" AutoGenerateColumns="false"  SettingsPager-Mode="ShowAllRecords" OnAfterPerformCallback="gvReport_AfterPerformCallback" Settings-ShowHorizontalScrollBar="True" Width="100%">
                        <Columns>
                            
                            <dx:GridViewDataTextColumn FieldName="DDELIVERY" Caption="วันที่" ReadOnly="True" GroupIndex="1"  VisibleIndex="0">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                <Settings AllowGroup="True" GroupInterval="Value" SortMode="Value"/>
                            </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataTextColumn  FieldName="GROUPS_NAME" Caption="กลุ่มงาน" ReadOnly="True" VisibleIndex="1">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               <Settings />
                                        </dx:GridViewDataTextColumn>
                            <%--<dx:GridViewDataTextColumn  FieldName="WORKGROUP_NAME"  Caption="ลักษณะงาน" ReadOnly="True" >
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                               
                                        </dx:GridViewDataTextColumn>--%>
                            <dx:GridViewDataTextColumn  FieldName="TERMINAL_NAME" Caption="คลัง" ReadOnly="True" VisibleIndex="2">
                                <CellStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
								<Settings AllowGroup="false"/>
							</dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="SCONTRACTNO"  Caption="เลขที่สัญญา" ReadOnly="True" VisibleIndex="3">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />                               
                                        </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn  FieldName="VENDOR_NAME"  Caption="ผู้ขนส่ง" ReadOnly="True" VisibleIndex="4">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn  FieldName="M_AMOUNT" Caption="รถในสัญญา" ReadOnly="True" VisibleIndex="5">
                                <CellStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
								<Settings AllowGroup="false"/>
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn  FieldName="M_ALTERNATION" Caption="รถหมุนในสัญญา" ReadOnly="True" VisibleIndex="6">
								<CellStyle HorizontalAlign="Center" />
								<HeaderStyle HorizontalAlign="Center" />
								<Settings AllowGroup="false"/>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="เที่ยวที่ 1" VisibleIndex="7">
                                <Columns>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS_CONFIRM_1"  Caption="<center>รถพร้อมปฏิบัติงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />                               
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS1"  Caption="<center>รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
									</dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_C_NUMFLIGHTS1"  Caption="<center>Utilization รถพร้อม</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  ToolTip="xxx" FieldName="U_NUMFLIGHTS1"  Caption="<center>Utilization รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
							<dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="เที่ยวที่ 2" VisibleIndex="8">
                                <Columns>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS_CONFIRM_2"  Caption="<center>รถพร้อมปฏิบัติงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />                               
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS2"  Caption="<center>รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
									</dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_C_NUMFLIGHTS2"  Caption="<center>Utilization รถพร้อม</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_NUMFLIGHTS2"  Caption="<center>Utilization รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
							<dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="เที่ยวที่ 3" VisibleIndex="9">
                                <Columns>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS_CONFIRM_3"  Caption="<center>รถพร้อมปฏิบัติงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />                               
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS3"  Caption="<center>รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
									</dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_C_NUMFLIGHTS3"  Caption="<center>Utilization รถพร้อม</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_NUMFLIGHTS3"  Caption="<center>Utilization รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
							<dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="เที่ยวที่ 4" VisibleIndex="10">
                                <Columns>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS_CONFIRM_4"  Caption="<center>รถพร้อมปฏิบัติงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />                               
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="NUMFLIGHTS4"  Caption="<center>รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" >
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
									</dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_C_NUMFLIGHTS4"  Caption="<center>Utilization รถพร้อม</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_NUMFLIGHTS4"  Caption="<center>Utilization รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
							<dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center" Caption="Overall Utilization" VisibleIndex="11">
                                <Columns>
									<dx:GridViewDataTextColumn  FieldName="U_C_UNNUMFLIGHTS"  Caption="<center>Utilization รถพร้อม</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  FieldName="U_UNNUMFLIGHTS"  Caption="<center>Utilization รถรับงาน</center>" ReadOnly="True" PropertiesTextEdit-EncodeHtml="false" PropertiesTextEdit-DisplayFormatString="P2">
                                        <CellStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
                        </Columns>
                        <Paddings />
                    </dx:ASPxGridView>
                    <%--<asp:GridView runat="server" ID="gvReport" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader" DataKeyNames="SVENDORID">
                        <Columns>
                            <asp:BoundField DataField="SVENDORID" Visible="false" />
                            <asp:BoundField DataField="NAME" HeaderText="กลุ่มที่"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="WORKGROUPNAME" HeaderText="กลุ่มงาน"  HeaderStyle-HorizontalAlign="Center"/>
                            <asp:BoundField DataField="DDELIVERY" HeaderText="วันที่" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="SABBREVIATION" HeaderText="<center>ผู้ขนส่ง</center>" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HtmlEncode="false" />
                            <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                           
                        </Columns>
                    </asp:GridView>--%>
                </div>
            </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        $(document).ready(function () {

            updatedate();

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            updatedate();
        }
        function updatedate() {

            $("#<%=txtDateStart.ClientID %>").on('change', function (e) {
                $("#<%=txtDateEnd.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate($(this).val());
                var date = new Date($(this).val().split('/')[2], $(this).val().split('/')[1] - 1, $(this).val().split('/')[0]);
                date.setDate(date.getDate() + 6);
                //alert(date);
                $("#<%=txtDateEnd.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate(date);
            });
            $("#<%=txtDateEnd.ClientID %>").on('change', function (e) {
                $("#<%=txtDateStart.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate($(this).val());
                var date = new Date($(this).val().split('/')[2], $(this).val().split('/')[1] - 1, $(this).val().split('/')[0]);
                date.setDate(date.getDate() - 6);
                //alert(date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear());
                //alert(date);
                $("#<%=txtDateStart.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate(date);
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

