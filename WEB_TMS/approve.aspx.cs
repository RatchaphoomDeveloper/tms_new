﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using DevExpress.Web.ASPxEditors;

public partial class approve : PageBase//System.Web.UI.Page 
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return; }

        if (!IsPostBack)
        {
            string SUID = Session["UserID"] + "";
            string strsql = "SELECT SUID, SVENDORID FROM TUSER WHERE SUID ='" + CommonFunction.ReplaceInjection(SUID) + "'";
            DataTable dt = CommonFunction.Get_Data(conn, strsql);
            string SVENDORID = "";
            SVENDORID = dt.Rows[0]["SVENDORID"] + "";
            Session["SVENDORID"] = SVENDORID;
            cboReatype.DataBind();
            cboReatype.Items.Insert(0, new ListEditItem("- ระบุประเภท -", ""));
            cboReatype.SelectedIndex = 0;
            cboReatypeT2.DataBind();
            cboReatypeT2.Items.Insert(0, new ListEditItem("- ระบุประเภท -", ""));
            cboReatypeT2.SelectedIndex = 0;

            //DateTime sDate = DateTime.Now.AddMonths(-1);
            DateTime sDate = DateTime.Now.AddDays(-20);
            DateTime eDate = DateTime.Now;

            dedtStart.Text = sDate.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture);
            dedtEnd.Text = eDate.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture);
            dedtStartT2.Text = sDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            dedtEndT2.Text = eDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            
            Lisdata(SVENDORID);
            LisdataT2(SVENDORID);

            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {            
            if (!CanRead)
            {
               
                gvwWaiting.Columns[7].Visible = false;
                gvwtab2.Columns[8].Visible = false;
                btnSearch.Enabled = false;
                btnSearchT2.Enabled = false;
            }

            if (!CanWrite)
            {
                btnReq.Enabled = false;
                //int getCoumn = 0;
                //getCoumn = findGridColumnByCSS(ref gvwWaiting, "xxSearch")
                //
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        string SVENDORID = Session["SVENDORID"] + "";
        Lisdata(SVENDORID);
        LisdataT2(SVENDORID);
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string[] param = e.Parameter.Split(';');
            string SVENDORID = Session["SVENDORID"] + "";

            switch (param[0])
            {
                case "search":
                    Lisdata(SVENDORID);
                    break;
                case "searchT2":
                    LisdataT2(SVENDORID);
                    break;
                case "view":
                    dynamic datav = gvwtab2.GetRowValues(int.Parse(param[1]), "REQUEST_ID");
                    xcpn.JSProperties["cpRedirectTo"] = "approve_view.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + datav));
                    break;
                case "edit":
                   // dynamic status = gvwWaiting.GetRowValues(int.Parse(param[1]), "STATUS_FLAG");
                    dynamic data = gvwWaiting.GetRowValues(int.Parse(param[1]), "REQUEST_ID", "REQUEST_OTHER_FLAG", "REQTYPE_ID");
                    //if (status == "09")
                    //{
                    //    xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile_rk.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0])) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(status));
                    //}
                    //else
                    //{
                        xcpn.JSProperties["cpRedirectTo"] = "approve_add.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0])) + "&type=" + Server.UrlEncode(STCrypt.Encrypt("" + data[1]));

                    //}
                    break;

                case "newbill":
                    xcpn.JSProperties["cpRedirectTo"] = "service_add_other.aspx";
                    break;

            }

        }
    }

    private void Lisdata(string VENDOR_ID)
    {
        string CoditionAll = "";

        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            CoditionAll += " AND (TVEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%' OR TRUCK.SHEADREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%' OR TRUCK.STRAILERREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%')";
        }

        if (dedtStart.Text.Trim() != null && dedtEnd.Text.Trim() != null)
        {
            //DateTime datestart = DateTime.Parse(dedtStart.Text.ToString());
            //DateTime dateend = DateTime.Parse(dedtEnd.Text.ToString());

            CoditionAll += " AND (TRUNC(NVL(TREQ.REQUEST_DATE,sysdate)) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(dedtStart.Text.Trim()) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dedtEnd.Text.Trim()) + "','DD/MM/YYYY')) ";
        }

        if (!string.IsNullOrEmpty(cboReatype.Value + ""))
        {
            CoditionAll += " AND TREQTYPE.REQTYPE_ID = '" + CommonFunction.ReplaceInjection(cboReatype.Value + "") + "'";
        }

        string CoditionVendor = CommonFunction.ReplaceInjection(VENDOR_ID);

        string strsql = @"SELECT   TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
                        ,TREQ.Status_Flag,TREQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TVEN.SABBREVIATION
                        ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_ID,TREQTYPE.REQTYPE_NAME ,TREQ.REQUEST_OTHER_FLAG
                        FROM TBL_Request TREQ
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR  WHERE 1=1 --AND  SVENDORID = '" + CoditionVendor + @"'
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,
                            CASE  SCARTYPEID
                            WHEN 0 THEN SHEADREGISTERNO
                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
                            ELSE ''
                            END REGISTERNO
                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                            FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID 
                        WHERE 1=1 " + CoditionAll + @" AND TREQ.Status_Flag IN ('01','12') /* TREQ.Status_Flag = '01' */ AND  TREQ.RK_FLAG <> 'M' /* OR  TREQ.RK_FLAG = 'Y'  OR (TREQ.Status_Flag = '09' AND TREQ.RK_FLAG = 'Y')OR TREQ.Status_Flag = '12' */
                        ORDER BY 
                        CASE WHEN TREQ.Status_Flag IN ('10','11') THEN null ELSE TREQ.Status_Flag END NULLS LAST
                        ,TREQ.CREATE_DATE ASC";

        DataTable dt = CommonFunction.Get_Data(conn, strsql);
        // lblsss.Text = strsql;

        if (dt.Rows.Count > 0)
        {
            //lblsss.Text = dt.Rows.Count + "";
            gvwWaiting.DataSource = dt;
            gvwWaiting.DataBind();
        }
        else
        {

        }
        gvwWaiting.DataBind();
    }

    private void LisdataT2(string VENDOR_ID)
    {
        string CoditionAll = "";

        if (!string.IsNullOrEmpty(txtsearchT2.Text))
        {
            //CoditionAll += " AND TVEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtsearchT2.Text) + "%' AND TRUCK.SHEADREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(txtsearchT2.Text) + "%' AND TRUCK.STRAILERREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(txtsearchT2.Text) + "%'";
            CoditionAll += " AND (NVL(TVEN.SABBREVIATION,' ')||NVL(TRUCK.SHEADREGISTERNO,' ')||NVL(TRUCK.STRAILERREGISTERNO,' ')) LIKE '%" + CommonFunction.ReplaceInjection(txtsearchT2.Text) + "%'";
        }

        if (!string.IsNullOrEmpty(cboReatypeT2.Value + ""))
        {
            CoditionAll += " AND TREQTYPE.REQTYPE_ID = '" + CommonFunction.ReplaceInjection(cboReatypeT2.Value + "") + "'";
        }

        if (dedtStartT2.Text.Trim() != null && dedtEndT2.Text.Trim() != null)
        {
            //DateTime datestart = DateTime.Parse(dedtStartT2.Text.ToString());
            //DateTime dateend = DateTime.Parse(dedtEndT2.Text.ToString());

            CoditionAll += " AND (TRUNC(TREQ.REQUEST_DATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(dedtStartT2.Text.Trim()) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dedtEndT2.Text.Trim()) + "','DD/MM/YYYY')) ";
        }

        string CoditionVendor = CommonFunction.ReplaceInjection(VENDOR_ID);

        string strsql = @"SELECT   TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
                        ,TREQ.Status_Flag,TRUNC(TREQ.WATER_EXPIRE_DATE) as DWATEREXPIRE,TVEN.SABBREVIATION,TRUNC(TREQ.APPROVE_DATE) as APPROVE_DATE
                        ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_NAME ,TREQTYPE.REQTYPE_ID
                        FROM TBL_Request TREQ
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR  WHERE 1=1 --AND  SVENDORID = '" + CoditionVendor + @"'
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,
                            CASE  SCARTYPEID
                            WHEN 0 THEN SHEADREGISTERNO
                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
                            ELSE ''
                            END REGISTERNO
                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                            FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID 
                        WHERE 1=1 " + CoditionAll + @" AND TREQ.Status_Flag NOT IN ('01','09')/* TREQ.Status_Flag <> '09' AND TREQ.Status_Flag <> '01' */ AND TREQ.RK_FLAG <> 'M' /* OR TREQ.Status_Flag = '12' */
                        ORDER BY 
                        CASE WHEN TREQ.Status_Flag IN ('10','11') THEN null ELSE TREQ.Status_Flag END NULLS LAST
                        ,TREQ.APPROVE_DATE ASC";

        DataTable dt = CommonFunction.Get_Data(conn, strsql);
        gvwtab2.DataSource = dt;
        gvwtab2.DataBind();

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string SVENDORID = Session["SVENDORID"] + "";
        Lisdata(SVENDORID);
    }
    protected void btnSearchT2_Click(object sender, EventArgs e)
    {
        string SVENDORID = Session["SVENDORID"] + "";
        LisdataT2(SVENDORID);
    }
}