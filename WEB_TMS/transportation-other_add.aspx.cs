﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using System.Globalization;
using System.IO;

public partial class transportation_add : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/transportation-other/{0}/{2}/{1}/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["CheckPermission"] = null;
            string AddEdit = "";
            bool chkurl = false;
            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "23")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            Session["CheckPermission"] = AddEdit;

            dteDate.Value = DateTime.Now;

            var dt = new List<dt>();
            Session["dt"] = dt;

            if (Session["oNPROBLEMID"] != null)
            {
                ListData();
            }
        }

    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {




    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        if ("" + Session["CheckPermission"] == "1")
        {
            switch (paras[0])
            {
                case "Save":
                    if (cboDelivery.Value + "" == "" && cboVendor.Value + "" == "" && cboHeadRegist.Value + "" == "" && cmbEMPLOYEE.Value + "" == "")
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาระบุข้อมูลสำคัญ<br />เช่น เลข Outbound ,ผู้ประกอบการขนส่ง , ทะเบียนหัว,ชื่อพนักงานขับรถ !');");
                        return;
                    }

                    if (Session["oNPROBLEMID"] == null) //insert
                    {
                        string GenID;
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            GenID = CommonFunction.Gen_ID(con, "SELECT NPROBLEMID FROM (SELECT NPROBLEMID FROM TPROBLEM ORDER BY NPROBLEMID DESC)  WHERE ROWNUM <= 1");
                            string strsql = "INSERT INTO TPROBLEM(SNAME,SSOLUTION,NPROBLEMID,STYPEPROBLEM,SDELIVERYNO,SHEADREGISTERNO,STRUCKID,STRAILERREGISTERNO,SVENDORID,SEMPLOYEEID,DPROBLEMDATE,SPROBLEMPLACE,SPROBLEMDETAIL,NSCORECUT,CSTATUS,SCREATE,DCREATE,SUPDATE,DUPDATE) VALUES (:SNAME,:SSOLUTION,:NPROBLEMID,:STYPEPROBLEM,:SDELIVERYNO,:SHEADREGISTERNO,:STRUCKID,:STRAILERREGISTERNO,:SVENDORID,:SEMPLOYEEID,:DPROBLEMDATE,:SPROBLEMPLACE,:SPROBLEMDETAIL,:NSCORECUT,:CSTATUS,:SCREATE,SYSDATE,:SUPDATE,SYSDATE)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NPROBLEMID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":SNAME", OracleType.VarChar).Value = txtCreateName.Text;
                                com.Parameters.Add(":SSOLUTION", OracleType.VarChar).Value = txtSolution.Text;
                                com.Parameters.Add(":STYPEPROBLEM", OracleType.VarChar).Value = txtTypeProblem.Text;
                                com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerRegist.Text;
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbEMPLOYEE.Value + "";
                                com.Parameters.Add(":DPROBLEMDATE", OracleType.DateTime).Value = dteDate.Value + "";
                                com.Parameters.Add(":SPROBLEMPLACE", OracleType.VarChar).Value = txtProblemPlace.Text;
                                com.Parameters.Add(":SPROBLEMDETAIL", OracleType.VarChar).Value = txtProblemDetail.Text;
                                com.Parameters.Add(":NSCORECUT", OracleType.Number).Value = txtScore.Text;
                                com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"];
                                com.ExecuteNonQuery();
                            }


                            string strsql2 = "INSERT INTO TPROBLEMIMPORTFILE(NIMPORTID,NPROBLEMID,SEVIDENCENAME,SFILENAME,SGENFILENAME,SFILEPATH,SCREATE,DCREATE) VALUES(:NIMPORTID,:NPROBLEMID,:SEVIDENCENAME,:SFILENAME,:SGENFILENAME,:SFILEPATH,:SCREATE,SYSDATE)";

                            var listImport = (List<dt>)Session["dt"];

                            foreach (var list in listImport)
                            {
                                string GenIDImport = CommonFunction.Gen_ID(con, "SELECT NIMPORTID FROM (SELECT NIMPORTID FROM TPROBLEMIMPORTFILE ORDER BY NIMPORTID DESC)  WHERE ROWNUM <= 1");
                                using (OracleCommand com2 = new OracleCommand(strsql2, con))
                                {
                                    com2.Parameters.Clear();
                                    com2.Parameters.Add(":NIMPORTID", OracleType.Number).Value = GenIDImport;
                                    com2.Parameters.Add(":NPROBLEMID", OracleType.Number).Value = GenID;
                                    com2.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = list.dtEvidenceName;
                                    com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = list.dtFileName;
                                    com2.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = list.dtGenFileName;
                                    com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = list.dtFilePath;
                                    com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                    com2.ExecuteNonQuery();
                                }
                            }
                        }
                        LogUser("23", "I", "บันทึกข้อมูลหน้า ปัญหาอื่นๆ", GenID + "");
                    }
                    else //update
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strsql = "UPDATE TPROBLEM SET SNAME = :SNAME,SSOLUTION = :SSOLUTION, STYPEPROBLEM = :STYPEPROBLEM,SDELIVERYNO = :SDELIVERYNO,SHEADREGISTERNO = :SHEADREGISTERNO,STRUCKID = :STRUCKID,STRAILERREGISTERNO = :STRAILERREGISTERNO,SVENDORID = :SVENDORID,SEMPLOYEEID = :SEMPLOYEEID,DPROBLEMDATE = :DPROBLEMDATE,SPROBLEMPLACE = :SPROBLEMPLACE,SPROBLEMDETAIL = :SPROBLEMDETAIL,NSCORECUT = :NSCORECUT,CSTATUS = :CSTATUS,SUPDATE = :SUPDATE,DUPDATE = SYSDATE WHERE NPROBLEMID = :NPROBLEMID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NPROBLEMID", OracleType.Number).Value = Session["oNPROBLEMID"];
                                com.Parameters.Add(":SNAME", OracleType.VarChar).Value = txtCreateName.Text;
                                com.Parameters.Add(":SSOLUTION", OracleType.VarChar).Value = txtSolution.Text;
                                com.Parameters.Add(":STYPEPROBLEM", OracleType.VarChar).Value = txtTypeProblem.Text;
                                com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerRegist.Text;
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbEMPLOYEE.Value + "";
                                com.Parameters.Add(":DPROBLEMDATE", OracleType.DateTime).Value = dteDate.Value + "";
                                com.Parameters.Add(":SPROBLEMPLACE", OracleType.VarChar).Value = txtProblemPlace.Text;
                                com.Parameters.Add(":SPROBLEMDETAIL", OracleType.VarChar).Value = txtProblemDetail.Text;
                                com.Parameters.Add(":NSCORECUT", OracleType.Number).Value = txtScore.Text;
                                com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"];
                                com.ExecuteNonQuery();
                            }


                            string strDelImport = "DELETE FROM TPROBLEMIMPORTFILE WHERE NPROBLEMID = " + Session["oNPROBLEMID"];
                            using (OracleCommand comDelImport = new OracleCommand(strDelImport, con))
                            {
                                comDelImport.ExecuteNonQuery();
                            }


                            string strsql2 = "INSERT INTO TPROBLEMIMPORTFILE(NIMPORTID,NPROBLEMID,SEVIDENCENAME,SFILENAME,SGENFILENAME,SFILEPATH,SCREATE,DCREATE) VALUES(:NIMPORTID,:NPROBLEMID,:SEVIDENCENAME,:SFILENAME,:SGENFILENAME,:SFILEPATH,:SCREATE,SYSDATE)";

                            var listImport = (List<dt>)Session["dt"];

                            foreach (var list in listImport)
                            {
                                string GenIDImport = CommonFunction.Gen_ID(con, "SELECT NIMPORTID FROM (SELECT NIMPORTID FROM TPROBLEMIMPORTFILE ORDER BY NIMPORTID DESC)  WHERE ROWNUM <= 1");
                                using (OracleCommand com2 = new OracleCommand(strsql2, con))
                                {
                                    com2.Parameters.Clear();
                                    com2.Parameters.Add(":NIMPORTID", OracleType.Number).Value = GenIDImport;
                                    com2.Parameters.Add(":NPROBLEMID", OracleType.Number).Value = Session["oNPROBLEMID"];
                                    com2.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = list.dtEvidenceName;
                                    com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = list.dtFileName;
                                    com2.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = list.dtGenFileName;
                                    com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = list.dtFilePath;
                                    com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                    com2.ExecuteNonQuery();
                                }
                            }
                        }

                        LogUser("23", "E", "แก้ไขข้อมูลหน้า ปัญหาอื่นๆ", Session["oNPROBLEMID"] + "");
                    }


                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='transportation-other.aspx';});");

                    ClearControl();

                    break;

                case "Upload":
                    string dtr = txtEvidence.Text;
                    var addData = (List<dt>)Session["dt"];
                    int max = (addData.Count > 0) ? addData.Select(s => s.dtID).OrderByDescending(o => o).First() + 1 : 1;
                    var fileupload = (Session["FileUpload"] + "").Split(';');

                    if (fileupload.Count() == 3)
                    {
                        addData.Add(new dt
                        {
                            dtID = max,
                            dtEvidenceName = txtEvidence.Text,
                            dtFileName = fileupload[0],
                            dtGenFileName = fileupload[1],
                            dtFilePath = fileupload[2]
                        });

                        Session["dt"] = addData;
                        sgvw.DataSource = addData;
                        sgvw.DataBind();
                    }
                    break;

                case "DeleteList":

                    var deldt = (List<dt>)Session["dt"];
                    int index1 = Convert.ToInt32(paras[1]);
                    string FilePath1 = sgvw.GetRowValues(index1, "dtFilePath") + "";

                    if (File.Exists(Server.MapPath("./") + FilePath1.Replace("/", "\\")))
                    {
                        File.Delete(Server.MapPath("./") + FilePath1.Replace("/", "\\"));
                    }

                    deldt.RemoveAt(index1);

                    sgvw.DataSource = deldt;
                    sgvw.DataBind();
                    Session["dt"] = deldt;

                    break;

                case "ViewList":

                    int index = Convert.ToInt32(paras[1]);
                    string FilePath = sgvw.GetRowValues(index, "dtFilePath") + "";

                    xcpn.JSProperties["cpRedirectOpen"] = "openFile.aspx?str=" + FilePath;

                    break;

            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID,SCONTRACTID,SVENDORID FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,c.SVENDORID FROM (TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID AND nvl(CT.STRAILERID,'1') = nvl(T.STRAILERID,'1') 
WHERE t.SHEADREGISTERNO LIKE :fillter AND c.SVENDORID LIKE :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

        if (comboBox.SelectedIndex == -1)
        {
            txtTrailerRegist.Text = "";
            txtTruckID.Text = "";
        }

    }
    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsTruck.SelectCommand = @"SELECT SDELIVERYNO,SVENDORID,SHEADREGISTERNO, STRAILERREGISTERNO,STRUCKID FROM(SELECT pl.SDELIVERYNO, p.STRAILERREGISTERNO, tk.STRUCKID,p.SVENDORID,p.SHEADREGISTERNO,ROW_NUMBER()OVER(ORDER BY pl.SDELIVERYNO) AS RN 
FROM (TPLANSCHEDULEList pl LEFT JOIN TPLANSCHEDULE p ON PL.NPLANID = P.NPLANID  ) 
LEFT JOIN TTRUCK tk ON p.SHEADREGISTERNO = TK.SHEADREGISTERNO
WHERE pl.CACTIVE = '1' AND p.CACTIVE = '1' AND pl.SDELIVERYNO LIKE :fillter AND p.SVENDORID LIKE :fillter1 AND p.SHEADREGISTERNO LIKE :fillter2 ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsTruck.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", cboHeadRegist.Value + ""));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

    }
    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM ((((TVENDOR_SAP v LEFT JOIN TCONTRACT c ON V.SVENDORID = C.SVENDORID )LEFT JOIN TCONTRACTTYPE ct ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID) 
LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID)LEFT JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID)
WHERE  V.SVENDORNAME  like :fillter AND T.SHEADREGISTERNO  LIKE :fillter2
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", cboHeadRegist.Value + ""));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cmbEMPLOYEE_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID,SPERSONELNO,FULLNAME,STEL,SDRIVERNO FROM (SELECT E.SEMPLOYEEID,E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL, E.SDRIVERNO ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE E.SEMPLOYEEID || E.SPERSONELNO || E.INAME || ES.FNAME || ' ' || ES.LNAME LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbEMPLOYEE_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            string[] _Filename = e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                ASPxUploadControl upl = (ASPxUploadControl)sender;
                int count = _Filename.Count() - 1;

                string genName = "other" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
                {
                    string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                    e.CallbackData = data;

                    Session["FileUpload"] = e.UploadedFile.FileName + ";" + genName + "." + _Filename[1] + ";" + data;

                }
            }
            else
            {

                return;

            }
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }


    void ListData()
    {
        DataTable dt = CommonFunction.Get_Data(sql, "SELECT SNAME,SSOLUTION,STYPEPROBLEM,SDELIVERYNO,SHEADREGISTERNO,STRUCKID,STRAILERREGISTERNO,SVENDORID,SEMPLOYEEID,DPROBLEMDATE,SPROBLEMPLACE,SPROBLEMDETAIL,NSCORECUT,CSTATUS FROM TPROBLEM WHERE NPROBLEMID = " + Session["oNPROBLEMID"]);
        DataTable dt2 = CommonFunction.Get_Data(sql, "SELECT NIMPORTID,SEVIDENCENAME,SFILENAME,SGENFILENAME,SFILEPATH FROM TPROBLEMIMPORTFILE WHERE NPROBLEMID = " + Session["oNPROBLEMID"]);

        if (dt.Rows.Count > 0)
        {
            cboDelivery.Value = dt.Rows[0]["SDELIVERYNO"] + "";
            cboHeadRegist.Value = dt.Rows[0]["SHEADREGISTERNO"] + "";
            txtTruckID.Text = dt.Rows[0]["STRUCKID"] + "";
            txtTrailerRegist.Text = dt.Rows[0]["STRAILERREGISTERNO"] + "";

            cboVendor_OnItemsRequestedByFilterConditionSQL(cboVendor, new ListEditItemsRequestedByFilterConditionEventArgs(0, 10, dt.Rows[0]["SVENDORID"] + ""));
            cboVendor.Value = dt.Rows[0]["SVENDORID"] + "";

            cmbEMPLOYEE_OnItemsRequestedByFilterConditionSQL(cmbEMPLOYEE, new ListEditItemsRequestedByFilterConditionEventArgs(0, 10, dt.Rows[0]["SEMPLOYEEID"] + ""));
            cmbEMPLOYEE.Value = dt.Rows[0]["SEMPLOYEEID"] + "";
            dteDate.Date = Convert.ToDateTime(dt.Rows[0]["DPROBLEMDATE"] + "");
            txtProblemPlace.Text = dt.Rows[0]["SPROBLEMPLACE"] + "";
            txtProblemDetail.Text = dt.Rows[0]["SPROBLEMDETAIL"] + "";
            txtScore.Text = dt.Rows[0]["NSCORECUT"] + "";
            txtTypeProblem.Text = dt.Rows[0]["STYPEPROBLEM"] + "";
            txtCreateName.Text = dt.Rows[0]["SNAME"] + "";
            txtSolution.Text = dt.Rows[0]["SSOLUTION"] + "";

            if (dt2.Rows.Count > 0)
            {
                var Data = (List<dt>)Session["dt"];

                foreach (DataRow dr1 in dt2.Rows)
                {
                    Data.Add(new dt
                    {
                        dtID = Convert.ToInt32(dr1["NIMPORTID"]),
                        dtEvidenceName = dr1["SEVIDENCENAME"] + "",
                        dtFileName = dr1["SFILENAME"] + "",
                        dtGenFileName = dr1["SGENFILENAME"] + "",
                        dtFilePath = dr1["SFILEPATH"] + ""
                    });
                }
                sgvw.DataSource = Data;
                sgvw.DataBind();
                Session["dt"] = Data;
            }
        }
    }


    [Serializable]
    struct dt
    {
        public int dtID { get; set; }
        public string dtEvidenceName { get; set; }
        public string dtFileName { get; set; }
        public string dtGenFileName { get; set; }
        public string dtFilePath { get; set; }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }


}