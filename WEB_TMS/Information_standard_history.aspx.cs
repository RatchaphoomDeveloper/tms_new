﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;

public partial class Information_standard_history : System.Web.UI.Page
{

    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    static List<sInformation> listInformation = new List<sInformation>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                string[] param = strQuery[0].Split(';');

                ViewState["setdata"] = param[0];
                ViewState["Type"] = param[1];
            }
             #region เช็ค Permission
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("49", "R", "เปิดดูข้อมูลหน้า ประวัติข้อมูล Average Standard Trip", ViewState["setdata"] + "" + "," + ViewState["Type"] + "");

            #endregion
            //string Type = Request.QueryString["Type"];
            //string[] strQueryType;
            //if (!string.IsNullOrEmpty(Type))
            //{
            //    strQueryType = STCrypt.DecryptURL(Type);
            //    ViewState["Type"] = strQueryType[0];

            //}
            cboTruckType.DataBind();
            if (!string.IsNullOrEmpty(ViewState["Type"] + ""))
            {
                cboTruckType.Value = cboTruckType.Items.FindByValue(ViewState["Type"] + "").Value;
            }
            //cboTruckType.DataBind();
            //cboTruckType.SelectedIndex = 0;

        }
        setdata();
        Listgrid();
    }

    void setdata()
    {

        string sSTERMINALID = ViewState["setdata"] + "";

        string strsql = @" SELECT STERMINALID, SABBREVIATION, DCREATE, SCREATE, DUPDATE, SUPDATE, VALUESTTERMINAL, SPRODUCTTYPEID, SCARTYPEID
                           FROM TINFORMATION_STABDARD
                           WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(sSTERMINALID) + "'";



        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);
        if (dt.Rows.Count > 0)
        {
            #region AdddatatoList Employee
            if (listInformation.Count > 0)
            {
                listInformation.RemoveAt(0);
            }

            DateTime? sDATE_CREATED = null;
            DateTime? sDATE_UPDATED = null;

            if (!string.IsNullOrEmpty(dt.Rows[0]["DCREATE"] + ""))
            {
                sDATE_CREATED = DateTime.Parse(dt.Rows[0]["DCREATE"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DUPDATE"] + ""))
            {
                sDATE_UPDATED = DateTime.Parse(dt.Rows[0]["DUPDATE"] + "");
            }
            listInformation.Add(new sInformation
            {
                STERMINALID = dt.Rows[0]["STERMINALID"] + "",
                SABBREVIATION = dt.Rows[0]["SABBREVIATION"] + "",
                VALUESTTERMINAL = dt.Rows[0]["VALUESTTERMINAL"] + "",
                SPRODUCTTYPEID = dt.Rows[0]["SPRODUCTTYPEID"] + "",
                SCARTYPEID = dt.Rows[0]["SCARTYPEID"] + "",
                DCREATE = sDATE_CREATED,
                SCREATE = dt.Rows[0]["SCREATE"] + "",
                DUPDATE = sDATE_UPDATED,
                SUPDATE = dt.Rows[0]["SUPDATE"] + ""

            });
            #endregion
        }

    }

    void Listgrid()
    {
        string STERMINALID = ViewState["setdata"] + "";
        string sType = ViewState["Type"] + "";



        string Historysql = @"SELECT STERMINALID, NVERSION FROM TINFORMATION_STABDARD_HISTORY
                             WHERE STERMINALID = '" + CommonFunction.ReplaceInjection(STERMINALID) + @"'
                             AND SCARTYPEID ='" + CommonFunction.ReplaceInjection(sType) + @"' 
                             GROUP BY STERMINALID,NVERSION 
                             ORDER BY NVERSION DESC";
        string SCARTYPEID = "";
        if (!string.IsNullOrEmpty(cboTruckType.Value + ""))
        {
            SCARTYPEID = CommonFunction.ReplaceInjection(cboTruckType.SelectedItem.Value + "");
        }
        string Condition3 = "";
        string Condition4 = "";
        string Condition5 = "";

        DataTable dt2 = CommonFunction.Get_Data(conn, @" select SPRODUCTTYPENAME as FieldName,
                                                       SPRODUCTTYPEID AS FieldValue from TPRODUCTTYPE");
        if (dt2.Rows.Count > 0)
        {
            for (int i = 0; i <= dt2.Rows.Count - 1; i++)
            {
                Condition3 += "," + dt2.Rows[i]["FieldValue"] + "" + ".VALUESTTERMINAL AS " + dt2.Rows[i]["FieldValue"] + "";
                Condition4 += @"LEFT JOIN (SELECT * FROM TINFORMATION_STABDARD_HISTORY WHERE 1=1   AND SPRODUCTTYPEID='" + dt2.Rows[i]["FieldValue"] + "" + "' AND SCARTYPEID='" + CommonFunction.ReplaceInjection(SCARTYPEID) + "' AND STERMINALID='" + CommonFunction.ReplaceInjection(STERMINALID) + "') " + dt2.Rows[i]["FieldValue"] + "" + " ON  std.NVERSION = " + dt2.Rows[i]["FieldValue"] + "" + ".NVERSION ";
                Condition5 += @"LEFT JOIN (SELECT * FROM TINFORMATION_STABDARD WHERE 1=1   AND SPRODUCTTYPEID='" + dt2.Rows[i]["FieldValue"] + "" + "' AND SCARTYPEID='" + CommonFunction.ReplaceInjection(SCARTYPEID) + "' AND STERMINALID='" + CommonFunction.ReplaceInjection(STERMINALID) + "') " + dt2.Rows[i]["FieldValue"] + "" + " ON  std.STERMINALID = " + dt2.Rows[i]["FieldValue"] + "" + ".STERMINALID ";

            }

        }

//        string strsql = @"
//SELECT stds.STERMINALID,stds.SABBREVIATION,stds.SCARTYPEID ,stds.NVERSION,stds.O,stds.S,stds.L,stds.A,stds.B,stds.E,stds.I,stds.R,sDate.DUPDATE,CASE  WHEN sDate.SUPDATE IS NOT NULL  THEN  suser.SFIRSTNAME||' '||suser.SLASTNAME  ELSE NULL END  AS SUPDATE FROM
//(
//SELECT std.STERMINALID ,std.SABBREVIATION,std.SCARTYPEID ,std.NVERSION " + Condition3 + @"
//FROM 
//(
//SELECT STERMINALID, NVERSION,SABBREVIATION,SCARTYPEID 
//FROM TINFORMATION_STABDARD_HISTORY  std
//WHERE  std.STERMINALID='" + CommonFunction.ReplaceInjection(STERMINALID) + @"' GROUP BY  STERMINALID,NVERSION,SABBREVIATION,SCARTYPEID )std   
//" + Condition4 + @" 
//) stds
//LEFT JOIN (SELECT NVERSION,MAX(DUPDATE) AS DUPDATE,SUPDATE FROM TINFORMATION_STABDARD_HISTORY WHERE DUPDATE IS NOT NULL  GROUP BY NVERSION,SUPDATE) sDate
//ON stds.NVERSION = sDate.NVERSION
//LEFT JOIN  TUSER SUSER ON sDate.SUPDATE = SUSER.SUID WHERE stds.SCARTYPEID = '" + CommonFunction.ReplaceInjection(SCARTYPEID) + @"' ORDER BY stds.NVERSION ASC";

        string strsql = @"SELECT std.STERMINALID ,std.SABBREVIATION,std.SCARTYPEID ,std.NVERSION,std.DUPDATE,CASE  WHEN std.SUPDATE IS NOT NULL  THEN  suser.SFIRSTNAME||' '||suser.SLASTNAME  ELSE NULL END  AS SUPDATE " + Condition3 + @"
 FROM (SELECT STERMINALID, NVERSION,SABBREVIATION,MAX(DUPDATE) AS DUPDATE,SUPDATE,SCARTYPEID FROM TINFORMATION_STABDARD_HISTORY  std WHERE  std.STERMINALID='" + CommonFunction.ReplaceInjection(STERMINALID) + "' GROUP BY  STERMINALID,NVERSION,SABBREVIATION,SUPDATE,SUPDATE,SCARTYPEID )std " + Condition4 + " LEFT JOIN  TUSER SUSER ON std.SUPDATE = SUSER.SUID WHERE std.SCARTYPEID = '" + CommonFunction.ReplaceInjection(SCARTYPEID) + "' ORDER BY std.NVERSION ASC";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);

        List<string[]> sds = new List<string[]>();

        //DataTable dt = new DataTable();
        //dt = CommonFunction.Get_Data(conn, Historysql);

        string DateUpdate = "";
        string SUPDATE = "";
        string datachange = "";
        string dataold = "";
        string datanew = "";

        string DataPresent = @"SELECT std.STERMINALID,std.SABBREVIATION,std.SCARTYPEID ,std.DUPDATE,CASE  WHEN std.SUPDATE IS NOT NULL  THEN  suser.SFIRSTNAME||' '||suser.SLASTNAME  ELSE NULL END  AS SUPDATE " + Condition3 + @"
 FROM (SELECT STERMINALID,SABBREVIATION,MAX(DUPDATE) AS DUPDATE,SUPDATE,SCARTYPEID FROM TINFORMATION_STABDARD  std WHERE  std.STERMINALID='" + CommonFunction.ReplaceInjection(STERMINALID) + "' AND std.SCARTYPEID = '" + CommonFunction.ReplaceInjection(SCARTYPEID) + "' GROUP BY  STERMINALID,SABBREVIATION,SUPDATE,SUPDATE,SCARTYPEID )std " + Condition5 + " LEFT JOIN  TUSER SUSER ON std.SUPDATE = SUSER.SUID ";


        DataTable dtDataPresent = new DataTable();
        dtDataPresent = CommonFunction.Get_Data(conn, DataPresent);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {

                if (i + 1 != dt.Rows.Count)
                {
                    //เช็คว่าค่าใน TINFORMATION_STABDARD i+1 นั้น มีค่าไหม ถ้าไม่มีให้เอาของตัวแรก ซึ่งได้มาจากการแอดค่า Value ว่างเข้าไป่กอนในตอนเซฟข้อมูลครั้งแรก
                    if (!string.IsNullOrEmpty(dt.Rows[i + 1]["DUPDATE"] + ""))
                    {
                        DateUpdate = dt.Rows[i + 1]["DUPDATE"] + "";
                    }
                    else
                    {
                        DateUpdate = dt.Rows[i]["DUPDATE"] + "";
                    }

                    //SUPDATE = dt.Rows[i + 1]["SUPDATE"] + "";

                    if (!string.IsNullOrEmpty(dt.Rows[i + 1]["SUPDATE"] + ""))
                    {
                        SUPDATE = dt.Rows[i + 1]["SUPDATE"] + "";
                    }
                    else
                    {
                        SUPDATE = dt.Rows[i]["SUPDATE"] + "";
                    }

                    if (dt.Rows[i]["SABBREVIATION"] + "" != dt.Rows[i + 1]["SABBREVIATION"] + "")
                    {
                        datachange += "คลัง" + "<br>";
                        dataold += dt.Rows[i]["SABBREVIATION"] + "" + "<br>";
                        datanew += dt.Rows[i + 1]["SABBREVIATION"] + "" + "<br>";
                    }


                    for (int j = 0; j <= dt2.Rows.Count - 1; j++)
                    {
                        string Field = dt2.Rows[j]["FieldValue"] + "";
                        if (dt.Rows[i][Field] + "" != dt.Rows[i + 1][Field] + "")
                        {
                            datachange += dt2.Rows[j]["FieldName"] + "" + "<br>";
                            dataold += dt.Rows[i][Field] + "" + "<br>";
                            datanew += dt.Rows[i + 1][Field] + "" + "<br>";
                        }
                    }

                }
                else
                {
                    int nCount = dtDataPresent.Rows.Count;
                    DateUpdate = dtDataPresent.Rows[nCount - 1]["DUPDATE"] + "";
                    SUPDATE = dtDataPresent.Rows[nCount - 1]["SUPDATE"] + "";
                    if (dt.Rows[i]["SABBREVIATION"] + "" != dtDataPresent.Rows[nCount - 1]["SABBREVIATION"] + "")
                    {
                        datachange += "คลัง" + "<br>";
                        dataold += dt.Rows[i]["SABBREVIATION"] + "" + "<br>";
                        datanew += dtDataPresent.Rows[nCount - 1]["SABBREVIATION"] + "" + "<br>";
                    }
                    for (int j = 0; j <= dt2.Rows.Count - 1; j++)
                    {
                        string Field = dt2.Rows[j]["FieldValue"] + "";
                        if (dt.Rows[i][Field] + "" != dtDataPresent.Rows[nCount - 1][Field] + "")
                        {
                            datachange += dt2.Rows[j]["FieldName"] + "" + "<br>";
                            dataold += dt.Rows[i][Field] + "" + "<br>";
                            datanew += dtDataPresent.Rows[nCount - 1][Field] + "" + "<br>";
                        }
                    }


                }

                //if (!string.IsNullOrEmpty(DateUpdate) && !string.IsNullOrEmpty(SUPDATE) && !string.IsNullOrEmpty(datachange) && !string.IsNullOrEmpty(dataold) && !string.IsNullOrEmpty(datanew))
                //{
                sds.Add(new string[] { DateUpdate, SUPDATE, datachange, dataold, datanew });
                DateUpdate = "";
                SUPDATE = "";
                datachange = "";
                dataold = "";
                datanew = "";
                //}
            }
        }

        gvw.DataSource = sds.Select(s => new { DateUpdate = s[0], SUPDATE = s[1], datachange = s[2], dataold = s[3], datanew = s[4] }).OrderByDescending(o => o.DateUpdate);
        gvw.DataBind();
        gvw.Visible = true;
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //Setdata();

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {


    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    [Serializable]
    class sInformation
    {
        public string STERMINALID { get; set; }
        public string SABBREVIATION { get; set; }
        public string VALUESTTERMINAL { get; set; }
        public string SPRODUCTTYPEID { get; set; }
        public string SCARTYPEID { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }

    }
}