﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using System.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;
using System.Configuration;
using System.Globalization;
using System.Web.Configuration;

public partial class admin_committe_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        //gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText); 
        #endregion

        if (!IsPostBack)
        {            
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //Session["CheckPermission"] = AddEdit;

            Session["vs_Sentencer"] = null;
            if (Session["ss_SSENTENCERCODE"] != null)
            {
                ltr_SSENTENCERCODE.Text = "" + Session["ss_SSENTENCERCODE"];

            }
            BindData(ltr_SSENTENCERCODE.Text);
            this.AssignAuthen();
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    void BindData(string sentencercode)
    {
        #region has sentencercode
        if (sentencercode != "" && Session["vs_Sentencer"] == null)
        {
            DataView dvSentencers = (DataView)sdsSentencer.Select(DataSourceSelectArguments.Empty);
            dvSentencers.Sort = "NLIST ASC";
            DataTable dtSentencers = dvSentencers.ToTable();
            Session["vs_Sentencer"] = dtSentencers;
            dtSentencers.Dispose();
        }
        #endregion
        DataTable dtSentencer = new DataTable();
        if (Session["vs_Sentencer"] == null)
        {
            #region vs_Sentencer is null
            DataView dvSentencer = (DataView)sdsSentencer.Select(DataSourceSelectArguments.Empty);
            dtSentencer = dvSentencer.ToTable();
            dtSentencer.Clear();
            if (dtSentencer.Rows.Count == 0)
            {
                DataRow dr_new;
                string[] array_position = { "ประธานคณะกรรมการ", "รองประธานคณะกรรมการ", "เลขานุการ" };
                for (int idxNew = 0; idxNew <= 4; idxNew++)
                {//SSENTENCERCODE ,SSENTENCERID ,SSENTENCERNAME ,DSENTENCES ,DBEGINTERM ,DENDTERM ,CODE ,INAME ,FNAME ,LNAME ,SPOSITION ,SJOB ,CACTIVE ,NLIST 
                    dr_new = dtSentencer.NewRow();
                    dr_new["NLIST"] = "" + (idxNew + 1);
                    dr_new["SSENTENCERCODE"] = "";
                    dr_new["SSENTENCERID"] = "0";
                    dr_new["SSENTENCERNAME"] = "";
                    dr_new["DSENTENCES"] = DateTime.Now;
                    dr_new["DBEGINTERM"] = DateTime.Now;
                    dr_new["DENDTERM"] = DateTime.Now;
                    dr_new["CODE"] = "";
                    dr_new["INAME"] = "";
                    dr_new["FNAME"] = "";
                    dr_new["LNAME"] = "";
                    dr_new["SPOSITION"] = "";
                    dr_new["SJOB"] = "" + ((idxNew < (array_position.Length)) ? "" + array_position[idxNew] : "");
                    dr_new["CEXPIRE"] = "0";
                    dtSentencer.Rows.Add(dr_new);
                }
            }
            Session["vs_Sentencer"] = dtSentencer;
            #endregion
        }
        else
        {
            dtSentencer = (DataTable)Session["vs_Sentencer"];
            if (dtSentencer.Rows.Count > 0)
            {
                txtsnentencername.Text = dtSentencer.Rows[0]["SSENTENCERNAME"] + "";
                detdbeginterm.Value = dtSentencer.Rows[0]["DBEGINTERM"];
                detdendterm.Value = dtSentencer.Rows[0]["DENDTERM"];
                rblcActiveTerm.Value = (dtSentencer.Rows[0]["CEXPIRETERM"].ToString() == "0") ? "0" : "1";
                radType.Value = dtSentencer.Rows[0]["SENTENCER_TYPE_ID"].ToString();
            }
        }

        gvwSentencer.DataSource = dtSentencer;
        gvwSentencer.DataBind();
    }
    void BackupData()
    {
        DataTable dt_Sentencer = (DataTable)Session["vs_Sentencer"];
        dt_Sentencer.Clear();
        DataRow dr_Sentencer;
        for (int VisibleIndex = 0; VisibleIndex < gvwSentencer.VisibleRowCount; VisibleIndex++)
        {
            dynamic datasentencer = gvwSentencer.GetRowValues(VisibleIndex, "SSENTENCERCODE", "SSENTENCERID", "SSENTENCERNAME", "DBEGINTERM", "DENDTERM", "NLIST", "CODE", "SIFLNAME", "SPOSITION", "SJOB", "CEXPIRE");

            ASPxTextBox txtnlist = gvwSentencer.FindRowCellTemplateControl(VisibleIndex, (GridViewDataColumn)gvwSentencer.Columns[1], "txtnlist") as ASPxTextBox;
            ASPxTextBox txtcode = gvwSentencer.FindRowCellTemplateControl(VisibleIndex, (GridViewDataColumn)gvwSentencer.Columns[2], "txtcode") as ASPxTextBox;
            ASPxTextBox txtiflname = gvwSentencer.FindRowCellTemplateControl(VisibleIndex, (GridViewDataColumn)gvwSentencer.Columns[3], "txtiflname") as ASPxTextBox;
            ASPxTextBox txtsposition = gvwSentencer.FindRowCellTemplateControl(VisibleIndex, (GridViewDataColumn)gvwSentencer.Columns[4], "txtsposition") as ASPxTextBox;
            ASPxTextBox txtsjob = gvwSentencer.FindRowCellTemplateControl(VisibleIndex, (GridViewDataColumn)gvwSentencer.Columns[5], "txtsjob") as ASPxTextBox;
            ASPxCheckBox ckbCEXPIRE = gvwSentencer.FindRowCellTemplateControl(VisibleIndex, (GridViewDataColumn)gvwSentencer.Columns[6], "ckbCEXPIRE") as ASPxCheckBox;

            dr_Sentencer = dt_Sentencer.NewRow();
            dr_Sentencer["SSENTENCERCODE"] = "" + (ltr_SSENTENCERCODE.Text.Equals("") ? datasentencer[0] : ltr_SSENTENCERCODE.Text);
            dr_Sentencer["SSENTENCERID"] = "" + datasentencer[1];
            dr_Sentencer["SSENTENCERNAME"] = "" + txtsnentencername.Text.Trim();
            dr_Sentencer["DBEGINTERM"] = ("" + detdbeginterm.Value == "") ? "" + DateTime.Now : "" + detdbeginterm.Value;
            dr_Sentencer["DENDTERM"] = ("" + detdendterm.Value == "") ? "" + DateTime.Now : "" + detdendterm.Value;
            dr_Sentencer["NLIST"] = "" + txtnlist.Text.Trim();
            dr_Sentencer["CODE"] = "" + txtcode.Text.Trim();
            dr_Sentencer["SIFLNAME"] = "" + txtiflname.Text.Trim();
            dr_Sentencer["SPOSITION"] = "" + txtsposition.Text.Trim();
            dr_Sentencer["SJOB"] = "" + txtsjob.Text.Trim();
            dr_Sentencer["CEXPIRE"] = "" + (ckbCEXPIRE.Checked ? "0" : "1");
            dt_Sentencer.Rows.Add(dr_Sentencer);
        }
        Session["vs_Sentencer"] = dt_Sentencer;
    }
    protected void sdsSentencer_Modifying(object sender, SqlDataSourceCommandEventArgs e)
    {
        int idxGridNewing = gvwSentencer.EditingRowVisibleIndex;
        int idxGridEditing = gvwSentencer.EditingRowVisibleIndex >= 0 ? gvwSentencer.EditingRowVisibleIndex : idxGridNewing;
        string mode = gvwSentencer.EditingRowVisibleIndex >= 0 ? "new" : "edit";
        dynamic dataSentencer = gvwSentencer.GetRowValues(idxGridEditing, "CODE", "SSENTENCERID", "SPOSITION", "SJOB", "");

        QueryProcessing(e);

    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    protected void gvwSentencer_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string[] param = e.Args[0].Split('$');
        string CallbackName = "" + param[0];
        DataTable dt_Sentencer = (DataTable)Session["vs_Sentencer"];
        if (CanWrite)
        {
            switch (CallbackName.ToLower())
            {
                case "btnadd":
                    BackupData();
                    DataRow drnewRow = dt_Sentencer.NewRow();
                    drnewRow["SSENTENCERCODE"] = "";
                    drnewRow["SSENTENCERID"] = 0;
                    drnewRow["SSENTENCERNAME"] = "";
                    drnewRow["DSENTENCES"] = DateTime.Now;
                    drnewRow["DBEGINTERM"] = DateTime.Now;
                    drnewRow["DENDTERM"] = DateTime.Now;
                    drnewRow["NLIST"] = "" + (gvwSentencer.VisibleRowCount + 1);
                    drnewRow["CODE"] = "";
                    drnewRow["SIFLNAME"] = "";
                    drnewRow["SPOSITION"] = "";
                    drnewRow["SJOB"] = "";
                    drnewRow["CEXPIRE"] = "0";
                    dt_Sentencer.Rows.Add(drnewRow);
                    Session["vs_Sentencer"] = dt_Sentencer;
                    BindData(ltr_SSENTENCERCODE.Text);
                    break;
                case "btnsubmit":
                    string sSentencer_Code = (ltr_SSENTENCERCODE.Text != "") ? ltr_SSENTENCERCODE.Text : "";//ถ้า ltr_SSENTENCERCODE มีค่า เอาค่าใร ltr เพราะเป็นการEditมาจากหน้าหลัก
                    using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                    {
                        if (con.State == ConnectionState.Closed) con.Open();

                        for (int visibleindex = 0; visibleindex < gvwSentencer.VisibleRowCount; visibleindex++)
                        {
                            dynamic datasentencer = gvwSentencer.GetRowValues(visibleindex, "SSENTENCERCODE", "SSENTENCERID", "SSENTENCERNAME", "DBEGINTERM", "DENDTERM", "NLIST"
                                , "CODE", "SIFLNAME", "SPOSITION", "SJOB", "CEXPIRE");
                            ASPxTextBox txtnlist = gvwSentencer.FindRowCellTemplateControl(visibleindex, (GridViewDataColumn)gvwSentencer.Columns[1], "txtnlist") as ASPxTextBox;
                            ASPxTextBox txtcode = gvwSentencer.FindRowCellTemplateControl(visibleindex, (GridViewDataColumn)gvwSentencer.Columns[2], "txtcode") as ASPxTextBox;
                            ASPxTextBox txtiflname = gvwSentencer.FindRowCellTemplateControl(visibleindex, (GridViewDataColumn)gvwSentencer.Columns[3], "txtiflname") as ASPxTextBox;
                            ASPxTextBox txtsposition = gvwSentencer.FindRowCellTemplateControl(visibleindex, (GridViewDataColumn)gvwSentencer.Columns[4], "txtsposition") as ASPxTextBox;
                            ASPxTextBox txtsjob = gvwSentencer.FindRowCellTemplateControl(visibleindex, (GridViewDataColumn)gvwSentencer.Columns[5], "txtsjob") as ASPxTextBox;
                            ASPxCheckBox ckbCEXPIRE = gvwSentencer.FindRowCellTemplateControl(visibleindex, (GridViewDataColumn)gvwSentencer.Columns[6], "ckbCEXPIRE") as ASPxCheckBox;

                            sSentencer_Code = (datasentencer[0] + "" == "") ? sSentencer_Code : datasentencer[0] + "";//ถ้าในรายการเคยมีรหัสกลุ่มแล้ว ให้ใช้รหัสกลุ่ม
                            if ("" + txtiflname.Text.Trim() != "")
                            {//ถ้ามีการระบุชื่อ แต่ ถ้าไม่มีระบบจะไม่เก็บไห้
                                OracleCommand ora_cmd = new OracleCommand("IUTSENTENCER", con);
                                ora_cmd.CommandType = CommandType.StoredProcedure;
                                ora_cmd.Parameters.AddWithValue("S_SENTENCERCODE", sSentencer_Code == "" ? null : sSentencer_Code);
                                ora_cmd.Parameters.AddWithValue("S_SENTENCERNAME", txtsnentencername.Text.Trim());
                                ora_cmd.Parameters.AddWithValue("S_SENTENCERID", "" + datasentencer[1]);
                                ora_cmd.Parameters.AddWithValue("D_BEGINTERM", Convert.ToDateTime(detdbeginterm.Value, new CultureInfo("en-US")));
                                ora_cmd.Parameters.AddWithValue("D_ENDTERM", Convert.ToDateTime(detdendterm.Value, new CultureInfo("en-US")));
                                ora_cmd.Parameters.AddWithValue("S_CODE", txtcode.Text.Trim());
                                ora_cmd.Parameters.AddWithValue("S_FNAME", txtiflname.Text.Trim());
                                ora_cmd.Parameters.AddWithValue("S_POSITION", txtsposition.Text.Trim());
                                ora_cmd.Parameters.AddWithValue("S_JOB", txtsjob.Text.Trim());
                                ora_cmd.Parameters.AddWithValue("C_ACTIVE", (ckbCEXPIRE.Checked ? "1" : "0"));
                                ora_cmd.Parameters.AddWithValue("N_LIST", txtnlist.Text.Trim());
                                ora_cmd.Parameters.AddWithValue("C_EXPIRETERM", rblcActiveTerm.Value + "");
                                ora_cmd.Parameters.AddWithValue("I_SENTENCER_TYPE_ID", radType.Value.ToString() + "");


                                OracleParameter para = new OracleParameter();
                                para.ParameterName = "SENTENCER_CODE";
                                para.OracleType = OracleType.VarChar;
                                para.Size = 15;
                                para.Direction = ParameterDirection.Output;
                                ora_cmd.Parameters.Add(para);

                                int result = ora_cmd.ExecuteNonQuery();

                                if (sSentencer_Code == "") sSentencer_Code = ora_cmd.Parameters["SENTENCER_CODE"].Value.ToString();
                            }
                        }

                        LogUser("38", "I", "บันทึกข้อมูลหน้า คณะกรรมการพิจารณาอุทธรณ์", sSentencer_Code);
                    }
                    CommonFunction.SetPopupOnLoad(gvwSentencer, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_committe_lst.aspx';});");
                    break;
                case "btncancel":
                    gvwSentencer.JSProperties["cpRedirectTo"] = "admin_committe_lst.aspx";
                    break;
            }

        }
        else
        {
            CommonFunction.SetPopupOnLoad(gvwSentencer, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }
    protected void gvwSentencer_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {
        string mode = "";
        dynamic dyn_Sentencer = gvwSentencer.GetRowValues(e.VisibleIndex, "sKeyID", "NLIST", "SSENTENCERCODE", "SSENTENCERID");
        DataTable dt_Sentencer = new DataTable();
        if (Session["vs_Sentencer"] == null)
            return;
        else
            dt_Sentencer = (DataTable)Session["vs_Sentencer"];

        switch (e.ButtonID.ToLower())
        {
            case "cmdbtndel":
                BackupData();
                if (dt_Sentencer.Rows.Count > 0)
                {
                    foreach (DataRow dr_Sentencer in dt_Sentencer.Select("NLIST=" + dyn_Sentencer[1] + "")) dt_Sentencer.Rows[dt_Sentencer.Rows.IndexOf(dr_Sentencer)].Delete();

                    Session["vs_Sentencer"] = dt_Sentencer;
                    BindData(ltr_SSENTENCERCODE.Text);
                }
                break;
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}