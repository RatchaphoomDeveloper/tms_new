﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="SurpriseCheckContractAddEdit.aspx.cs" Inherits="SurpriseCheckContractAddEdit" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>


            <div class="form-horizontal">
                <ul class="nav nav-tabs" runat="server" id="tabtest">
                    <li class="active" id="liTab1" runat="server"><a href="#TabGeneral" data-toggle="tab" aria-expanded="true" runat="server" id="GeneralTab1">ข้อมูลเบื้องต้น</a></li>
                    <li class="" id="liTab2" visible="false" runat="server"><a href="#TabGeneral2" runat="server" id="GeneralTab2">ปัญหา</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="TabGeneral">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">ข้อมูลทั่วไป</a>
                                <asp:HiddenField runat="server" ID="hidcollapse1" />
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">

                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ชื่อผู้ขนส่ง<asp:Label ID="lblReqddlVendor" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="true" /></label>
                                        <div class="col-md-3">
                                            <asp:DropDownList runat="server" CssClass="form-control select2" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-md-2 control-label">เลขที่สัญญาจ้างขนส่ง<asp:Label ID="lblReqddlContract" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                        <div class="col-md-3">
                                            <asp:DropDownList runat="server" CssClass="form-control select2" ID="ddlContract">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ชนิดรถ<asp:Label ID="Label1" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="true" /></label>
                                        <div class="col-md-3">
                                            <div class="col-md-12">
                                                <asp:RadioButtonList runat="server" ID="rblTruckType" AutoPostBack="true" OnSelectedIndexChanged="rblTruckType_SelectedIndexChanged" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="รถเดี่ยว" Value="1" Selected="True" />
                                                    <asp:ListItem Text="รถกึ่งพ่วง" Value="2" />
                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">หมายเลขแชสซีย์(หัว)<asp:Label ID="lblReqddlWorkGroup" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="true" /></label>
                                        <div class="col-md-3">

                                            <div class="col-md-10 PaddingLeftRight0">
                                                <asp:TextBox runat="server" ID="txtHsChasis" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-1">
                                                <asp:Button ID="btnHsChasisSearch" ToolTip="ดึงทะเบียนหัว" runat="server" Text="+" Width="30px"
                                                    OnClick="btnHsChasisSearch_Click" />
                                            </div>
                                        </div>
                                        <label class="col-md-2 control-label">หมายเลขแชสซีย์(หาง)<asp:Label ID="lblReqtxtTsChasis" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                        <div class="col-md-3">
                                            <div class="col-md-10 PaddingLeftRight0">

                                                <asp:TextBox runat="server" ID="txtTsChasis" Enabled="false" CssClass="form-control" />
                                            </div>
                                            <div class="col-md-1">
                                                <asp:Button ID="btnTsChasis" ToolTip="ดึงทะเบียนหาง" runat="server" Text="+" Width="30px"
                                                    OnClick="btnTsChasis_Click" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ทะเบียนหัว</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtSHEADREGISTERNO" CssClass="form-control "></asp:TextBox>
                                        </div>
                                        <label class="col-md-2 control-label">ทะเบียนหาง</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtSTRAILERREGISTERNO" Enabled="false" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">วันที่นัดหมาย<asp:Label ID="Label2" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="true" /></label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtCHECK_DATE" CssClass="form-control datetimepicker" />
                                        </div>
                                        <label class="col-md-2 control-label">สถานที่<asp:Label ID="Label3" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="true" /></label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtPLACE" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-3 control-label">ผู้ยื่นคำขอ</label>
                                        <div class="col-md-3">
                                            <asp:TextBox runat="server" ID="txtUser" CssClass="form-control" ReadOnly="true" />
                                            <asp:HiddenField runat="server" ID="hidREQUEST_BY" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">เอกสารหลักฐานเพื่อพิจารณา</a>
                                <asp:HiddenField runat="server" ID="HiddenField1" />
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-12 text-center">
                                            <asp:Label Text="หมายเหตุ สามารถแนบเอกสารได้ภายหลัง" ForeColor="Red" runat="server" />
                                        </div>
                                        <div class="col-md-12">
                                            <asp:GridView ID="dgvRequestFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                                <Columns>

                                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            </asp:GridView>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col col-md-5 control-label">ประเภทไฟล์เอกสาร</label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" ID="ddlUploadType" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col col-md-5 control-label">เลือกไฟล์</label>
                                            <div class="col-md-3">
                                                <asp:FileUpload ID="fileUpload" runat="server" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:Button Text="Upload" ID="btnUpload" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUpload_Click" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView ID="dgvUploadFile" runat="server" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false"
                                                    CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH" OnRowDeleting="dgvUploadFile_RowDeleting"
                                                    OnRowUpdating="dgvUploadFile_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound" EmptyDataText="[ ไม่มีข้อมูล ]">
                                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="No.">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                        <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                        <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                        <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>

                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse8" id="acollapse8">เอกสารหลักฐานอื่นๆ</a>
                                <asp:HiddenField runat="server" ID="hidcollapse8" />
                            </div>
                            <div id="collapse8" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <label class="col col-md-5 control-label">ประเภทไฟล์เอกสาร</label>
                                        <div class="col-md-3">
                                            <asp:DropDownList runat="server" ID="ddlUploadTypeExpired" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col col-md-5 control-label">เลือกไฟล์</label>
                                        <div class="col-md-3">
                                            <asp:FileUpload ID="fileUploadExpired" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-4">
                                            <asp:Button Text="Upload" ID="btnUploadExpired" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUploadExpired_Click" />
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <asp:GridView ID="dgvUploadFileExpired" runat="server" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                                                HorizontalAlign="Center" AutoGenerateColumns="false"
                                                CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH" OnRowDeleting="dgvUploadFileExpired_RowDeleting"
                                                OnRowUpdating="dgvUploadFileExpired_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound" EmptyDataText="[ ไม่มีข้อมูล ]">
                                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="No.">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                    <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                    <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                    <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>

                                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse3" id="acollapse3">หมายเหตุ</a>
                                <asp:HiddenField runat="server" ID="HiddenField2" />
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" placeholder="หมายเหตุ" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--<div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">เอกสารแนบสำคัญ (ระบุวันหมดอายุ)</a>
                                <asp:HiddenField runat="server" ID="HiddenField2" />
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <asp:GridView ID="dgvRequestFileExpired" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                                <Columns>

                                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse8" id="acollapse8">เอกสารหลักฐานที่เกี่ยวข้อง</a>
                                <asp:HiddenField runat="server" ID="HiddenField3" />
                            </div>
                            <div id="collapse8" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    
                                </div>
                            </div>
                        </div>--%>
                        <div class="row form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4">
                                <input id="btnConfirm" runat="server" type="button" value="ยืนยัน" data-toggle="modal" data-target="#ModalConfirmAfterSave" class="btn btn-md bth-hover btn-info" />
                                <input id="btnCancel" runat="server" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalCancelAfterSave" class="btn btn-md bth-hover btn-info" />
                                <input id="btnSave" runat="server" type="button" value="บันทึก" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" />
                                <input id="btnEdit" runat="server" type="button" value="บันทึก" data-toggle="modal" data-target="#ModalConfirmEditSave" class="btn btn-md bth-hover btn-info" />
                                <asp:Button runat="server" ID="mpBack" CssClass="btn btn-danger" Text="กลับ" OnClick="mpBack_ClickOK" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hidID" />
            <asp:HiddenField runat="server" ID="hidIS_ACTIVE" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
            <asp:PostBackTrigger ControlID="btnUploadExpired" />

        </Triggers>
    </asp:UpdatePanel>
    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSave_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirm" IDModel="ModalConfirmAfterSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirm_ClickOK" TextTitle="ยืนยันการนัดหมาย" TextDetail="คุณต้องการยืนยันใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpCancel" IDModel="ModalCancelAfterSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpCancel_ClickOK" TextTitle="ยกเลิกการนัดหมาย" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpEdit" IDModel="ModalConfirmEditSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpEdit_ClickOK" TextTitle="ยกเลิกการนัดหมาย" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

