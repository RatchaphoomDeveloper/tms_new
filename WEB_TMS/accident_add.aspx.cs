﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Text;
using System.Configuration;
using DevExpress.Web.ASPxGridView;

public partial class accident_add : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/Accident/{0}/{2}/{1}/";

    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            // 
            bool chkurl = false;
            Session["CheckPermission"] = null;
            string AddEdit = "";

            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "7")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            Session["CheckPermission"] = AddEdit;

            var dt1 = new List<DT1>();
            Session["DT1"] = dt1;

            bool boolchk = false;

            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            dteDateAccident.Value = DateTime.Now;
            dteDateAccident.MaxDate = DateTime.Today.AddDays(-1);

            dteBEGINWORK.Value = DateTime.Now;
            dteBEGINWORK.MaxDate = DateTime.Today.AddDays(-1);

            dteLASTWORK.Value = DateTime.Now;
            dteLASTWORK.MaxDate = DateTime.Today.AddDays(-1);

            Session["addSACCIDENTID"] = null;
            if (Session["oSACCIDENTID"] != null)
            {
                Session["addSACCIDENTID"] = Session["oSACCIDENTID"];
                BindData();
                boolchk = true;
            }

            PageControl.TabPages[1].ClientEnabled = boolchk;
            PageControl.TabPages[2].ClientEnabled = boolchk;
            PageControl.TabPages[3].ClientEnabled = boolchk;

            

        }
    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        if ("" + Session["CheckPermission"] == "1")
        {
            switch (paras[0])
            {

                case "Save":

                    if (Session["addSACCIDENTID"] != null)
                    {

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = @"UPDATE TACCIDENT SET DACCIDENT = :DACCIDENT,TACCIDENTTIME = :TACCIDENTTIME,SCITIZENID = :SCITIZENID,SEMPLOYEEID = :SEMPLOYEEID,SHEADREGISTERNO = :SHEADREGISTERNO,STRAILERREGISTERNO = :STRAILERREGISTERNO
,STRUCKID = :STRUCKID,SACCIDENTLOCALE = :SACCIDENTLOCALE,CTRANSPORTPROCESS = :CTRANSPORTPROCESS,CEVENT = :CEVENT,SEVENTOTHER = :SEVENTOTHER,CNOLITIGANT = :CNOLITIGANT,SLITIGANT = :SLITIGANT,CFAULT = :CFAULT,CVALUE = :CVALUE
,NVALUE = :NVALUE,CVALUETERMINAL = :CVALUETERMINAL,NVALUETERMINAL = :NVALUETERMINAL,CPRODUCT = :CPRODUCT,SPRODUCT = :SPRODUCT,NAMOUNT = :NAMOUNT,CENVIRONMENT = :CENVIRONMENT,CWATER = :CWATER,CCOMMUNITY = :CCOMMUNITY
,CFARM = :CFARM,CFIRE = :CFIRE,CVEHICLEFIRE = :CVEHICLEFIRE,CCOMMUNITYFIRE = :CCOMMUNITYFIRE,CDAMAGEPRODUCT = :CDAMAGEPRODUCT,CCORPORATE = :CCORPORATE,CMEDIACOUNTRY = :CMEDIACOUNTRY,CMEDIACOMMUNITY = :CMEDIACOMMUNITY
,DUPDATE = SYSDATE,SUPDATE = :SCREATE,SVENDORID = :SVENDORID,SCONTRACTID = :SCONTRACTID,CPERSONAL = :CPERSONAL,NDAMAGEPRODUCT = :NDAMAGEPRODUCT WHERE SACCIDENTID = :SACCIDENTID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                int num = 0;
                                double doublenum = 0.0;

                                com.Parameters.Clear();
                                com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                com.Parameters.Add(":DACCIDENT", OracleType.DateTime).Value = dteDateAccident.Value;
                                com.Parameters.Add(":TACCIDENTTIME", OracleType.VarChar).Value = tdeAcccidentTime.Text;
                                com.Parameters.Add(":SCITIZENID", OracleType.VarChar).Value = cmbPersonalNo.Value + "";
                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = txtEmployeeID.Text;
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                                com.Parameters.Add(":SACCIDENTLOCALE", OracleType.VarChar).Value = txtACCIDENTLOCALE.Text;
                                com.Parameters.Add(":CTRANSPORTPROCESS", OracleType.Char).Value = rblTRANSPORTPROCESS.Value + "";
                                com.Parameters.Add(":CEVENT", OracleType.Char).Value = rblEVENT.Value + "";
                                com.Parameters.Add(":SEVENTOTHER", OracleType.VarChar).Value = ("" + rblEVENT.Value == "4") ? txtEVENTOTHER.Text + "" : "";
                                com.Parameters.Add(":CNOLITIGANT", OracleType.Char).Value = rblLITIGANT.Value + "";
                                com.Parameters.Add(":SLITIGANT", OracleType.VarChar).Value = ("" + rblLITIGANT.Value == "2") ? rblLITIGANT.Value + "" : "";
                                com.Parameters.Add(":CFAULT", OracleType.Char).Value = rblFAULT.Value + "";
                                com.Parameters.Add(":CVALUE", OracleType.Char).Value = (chkVALUE.Checked) ? "1" : "";
                                com.Parameters.Add(":NVALUE", OracleType.Number).Value = double.TryParse(txtVALUE.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CVALUETERMINAL", OracleType.Char).Value = (chkVALUETERMINAL.Checked) ? "1" : "";
                                com.Parameters.Add(":NVALUETERMINAL", OracleType.Number).Value = double.TryParse(txtVALUETERMINAL.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CPRODUCT", OracleType.Char).Value = (chkPRODUCT.Checked) ? "1" : "";
                                com.Parameters.Add(":SPRODUCT", OracleType.VarChar).Value = txtPRODUCT.Text;
                                com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = double.TryParse(txtAMOUNT.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CENVIRONMENT", OracleType.Char).Value = (chkENVIRONMENT.Checked) ? "1" : "";
                                com.Parameters.Add(":CWATER", OracleType.Char).Value = (chkWATER.Checked) ? "1" : "";
                                com.Parameters.Add(":CCOMMUNITY", OracleType.Char).Value = (chkCOMMUNITY.Checked) ? "1" : "";
                                com.Parameters.Add(":CFARM", OracleType.Char).Value = (chkFARM.Checked) ? "1" : "";
                                com.Parameters.Add(":CFIRE", OracleType.Char).Value = (chkFIRE.Checked) ? "1" : "";
                                com.Parameters.Add(":CVEHICLEFIRE", OracleType.Char).Value = (chkVEHICLEFIRE.Checked) ? "1" : "";
                                com.Parameters.Add(":CCOMMUNITYFIRE", OracleType.Char).Value = (chkCOMMUNITYFIRE.Checked) ? "1" : "";
                                com.Parameters.Add(":CDAMAGEPRODUCT", OracleType.Char).Value = (chkDAMAGEPRODUCT.Checked) ? "1" : "";
                                com.Parameters.Add(":CCORPORATE", OracleType.Char).Value = (chkCORPORATE.Checked) ? "1" : "";
                                com.Parameters.Add(":CMEDIACOUNTRY", OracleType.Char).Value = (chkMEDIACOUNTRY.Checked) ? "1" : "";
                                com.Parameters.Add(":CMEDIACOMMUNITY", OracleType.Char).Value = (chkMEDIACOMMUNITY.Checked) ? "1" : "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = txtVendorID.Text;
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = txtContractID.Text;
                                com.Parameters.Add(":CPERSONAL", OracleType.Char).Value = (chkPersonal.Checked) ? "1" : "";
                                com.Parameters.Add(":NDAMAGEPRODUCT", OracleType.Number).Value = double.TryParse(txtDAMAGEPRODUCT.Text + "", out doublenum) ? doublenum : 0;
                                com.ExecuteNonQuery();

                            }

                            using (OracleCommand comDelList = new OracleCommand("DELETE FROM TPERSONALACCIDENT WHERE SACCIDENTID = '" + Session["addSACCIDENTID"] + "'", con))
                            {
                                comDelList.ExecuteNonQuery();
                            }

                            var InsertPersonal = (List<DT1>)Session["DT1"];

                            strsql = @"INSERT INTO TMS.TPERSONALACCIDENT ( ID, SACCIDENTID, SGROUP,  SNAME, STYPE, SCREATE, DCREATE) 
                    VALUES (:ID,:SACCIDENTID,:SGROUP,:SNAME,:STYPE,:SCREATE,SYSDATE)";
                            foreach (var dr in InsertPersonal)
                            {
                                using (OracleCommand com1 = new OracleCommand(strsql, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":ID", OracleType.Number).Value = dr.dtsID;
                                    com1.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"];
                                    com1.Parameters.Add(":SGROUP", OracleType.VarChar).Value = dr.dtsGroup;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = dr.dtsName;
                                    com1.Parameters.Add(":STYPE", OracleType.VarChar).Value = dr.dtsType;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.ExecuteNonQuery();
                                }
                            }


                            using (OracleCommand comDelfile = new OracleCommand("DELETE FROM TACCIDENTIMPORTFILE WHERE SACCIDENTID = '" + Session["addSACCIDENTID"] + "' AND NID BETWEEN 1 AND 4", con))
                            {
                                comDelfile.ExecuteNonQuery();
                            }

                            string strsql1 = "INSERT INTO TACCIDENTIMPORTFILE(NID,SACCIDENTID,SDOCUMENT,SFILENAME,SGENFILENAME,SFILEPATH,SCREATE,DCREATE) VALUES (:NID,:SACCIDENTID,:SDOCUMENT,:SFILENAME,'',:SFILEPATH,:SCREATE,SYSDATE)";

                            string[] sDucumentName = new string[4];

                            sDucumentName[0] = "สำเนาบัตรประชาชน";
                            sDucumentName[1] = "สำเนาใบขับขี่";
                            sDucumentName[2] = "รูปสถานที่เกิดเหตุ (จากGPS)";
                            sDucumentName[3] = "รูปสถานที่เกิดเหตุ (รูปภาพสถานที่เกิดเหตุจริง)";

                            for (int i = 0; i < 4; i++)
                            {
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {

                                    ASPxTextBox txtFileName = (ASPxTextBox)PageControl.FindControl("txtFileName" + i);
                                    ASPxTextBox txtFilePath = (ASPxTextBox)PageControl.FindControl("txtFilePath" + i);

                                    if (txtFileName != null && txtFilePath != null)
                                    {
                                        if ("" + txtFilePath.Text != "")
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":NID", OracleType.Number).Value = i + 1;
                                            com1.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath.Text;
                                            com1.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = sDucumentName[i];
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }



                            using (OracleCommand com2 = new OracleCommand(strsql1, con))
                            {

                                    if ("" + txtFilePath7.Text != "")
                                    {
                                        com2.Parameters.Clear();
                                        com2.Parameters.Add(":NID", OracleType.Number).Value = 8;
                                        com2.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                        com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName7.Text;
                                        com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath7.Text;
                                        com2.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = "สำเนาใบ Invoice";
                                        com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                        com2.ExecuteNonQuery();
                                    }
                            }

                            using (OracleCommand com3 = new OracleCommand(strsql1, con))
                            {

                                    if ("" + txtFilePath8.Text != "")
                                    {
                                        com3.Parameters.Clear();
                                        com3.Parameters.Add(":NID", OracleType.Number).Value = 9;
                                        com3.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                        com3.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName8.Text;
                                        com3.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath8.Text;
                                        com3.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = "หนังสือยินยอมชดใช้ค่าเสียหาย(ปริมาณน้ำมัน)";
                                        com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                        com3.ExecuteNonQuery();
                                    }

                            }



                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                            VisibleControlUpload();
                        }
                        LogUser("20", "E", "แก้ไขข้อมูลหน้าอุบัติเหตุ", Session["addSACCIDENTID"] + "");
                    }
                    else
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string GenID = CommonFunction.Gen_ID(con, "SELECT SACCIDENTID FROM (SELECT SACCIDENTID FROM TACCIDENT ORDER BY SACCIDENTID DESC)  WHERE ROWNUM <= 1");

                            string strsql = @"INSERT INTO TACCIDENT(SACCIDENTID,DACCIDENT,TACCIDENTTIME,DDATENOTIFY,SCITIZENID,SEMPLOYEEID,SHEADREGISTERNO,STRAILERREGISTERNO
,STRUCKID,SACCIDENTLOCALE,CTRANSPORTPROCESS,CEVENT,SEVENTOTHER,CNOLITIGANT,SLITIGANT,CFAULT,CVALUE
,NVALUE,CVALUETERMINAL,NVALUETERMINAL,CPRODUCT,SPRODUCT,NAMOUNT,CENVIRONMENT,CWATER,CCOMMUNITY
,CFARM,CFIRE,CVEHICLEFIRE,CCOMMUNITYFIRE,CDAMAGEPRODUCT,CCORPORATE,CMEDIACOUNTRY,CMEDIACOMMUNITY
,DCREATE,SCREATE,DUPDATE,SUPDATE,CSENDBY,SVENDORID,SCONTRACTID,CPERSONAL,NDAMAGEPRODUCT,CSENDTOPK) 
VALUES(:SACCIDENTID,:DACCIDENT,:TACCIDENTTIME,sysdate,:SCITIZENID,:SEMPLOYEEID,:SHEADREGISTERNO,:STRAILERREGISTERNO
,:STRUCKID,:SACCIDENTLOCALE,:CTRANSPORTPROCESS,:CEVENT,:SEVENTOTHER,:CNOLITIGANT,:SLITIGANT,:CFAULT,:CVALUE
,:NVALUE,:CVALUETERMINAL,:NVALUETERMINAL,:CPRODUCT,:SPRODUCT,:NAMOUNT,:CENVIRONMENT,:CWATER,:CCOMMUNITY
,:CFARM,:CFIRE,:CVEHICLEFIRE,:CCOMMUNITYFIRE,:CDAMAGEPRODUCT,:CCORPORATE,:CMEDIACOUNTRY,:CMEDIACOMMUNITY
,SYSDATE,:SCREATE,SYSDATE,:SCREATE,'2',:SVENDORID,:SCONTRACTID,:CPERSONAL,:NDAMAGEPRODUCT,'1')";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                int num = 0;
                                double doublenum = 0.0;

                                com.Parameters.Clear();
                                com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":DACCIDENT", OracleType.DateTime).Value = dteDateAccident.Value;
                                com.Parameters.Add(":TACCIDENTTIME", OracleType.VarChar).Value = tdeAcccidentTime.Text;
                                com.Parameters.Add(":SCITIZENID", OracleType.VarChar).Value = cmbPersonalNo.Value + "";
                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = txtEmployeeID.Text;
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                                com.Parameters.Add(":SACCIDENTLOCALE", OracleType.VarChar).Value = txtACCIDENTLOCALE.Text;
                                com.Parameters.Add(":CTRANSPORTPROCESS", OracleType.Char).Value = rblTRANSPORTPROCESS.Value + "";
                                com.Parameters.Add(":CEVENT", OracleType.Char).Value = rblEVENT.Value + "";
                                com.Parameters.Add(":SEVENTOTHER", OracleType.VarChar).Value = ("" + rblEVENT.Value == "4") ? txtEVENTOTHER.Text + "" : "";
                                com.Parameters.Add(":CNOLITIGANT", OracleType.Char).Value = rblLITIGANT.Value + "";
                                com.Parameters.Add(":SLITIGANT", OracleType.VarChar).Value = ("" + rblLITIGANT.Value == "2") ? rblLITIGANT.Value + "" : "";
                                com.Parameters.Add(":CFAULT", OracleType.Char).Value = rblFAULT.Value + "";
                                com.Parameters.Add(":CVALUE", OracleType.Char).Value = (chkVALUE.Checked) ? "1" : "";
                                com.Parameters.Add(":NVALUE", OracleType.Number).Value = double.TryParse(txtVALUE.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CVALUETERMINAL", OracleType.Char).Value = (chkVALUETERMINAL.Checked) ? "1" : "";
                                com.Parameters.Add(":NVALUETERMINAL", OracleType.Number).Value = double.TryParse(txtVALUETERMINAL.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CPRODUCT", OracleType.Char).Value = (chkPRODUCT.Checked) ? "1" : "";
                                com.Parameters.Add(":SPRODUCT", OracleType.VarChar).Value = txtPRODUCT.Text;
                                com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = double.TryParse(txtAMOUNT.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CENVIRONMENT", OracleType.Char).Value = (chkENVIRONMENT.Checked) ? "1" : "";
                                com.Parameters.Add(":CWATER", OracleType.Char).Value = (chkWATER.Checked) ? "1" : "";
                                com.Parameters.Add(":CCOMMUNITY", OracleType.Char).Value = (chkCOMMUNITY.Checked) ? "1" : "";
                                com.Parameters.Add(":CFARM", OracleType.Char).Value = (chkFARM.Checked) ? "1" : "";
                                com.Parameters.Add(":CFIRE", OracleType.Char).Value = (chkFIRE.Checked) ? "1" : "";
                                com.Parameters.Add(":CVEHICLEFIRE", OracleType.Char).Value = (chkVEHICLEFIRE.Checked) ? "1" : "";
                                com.Parameters.Add(":CCOMMUNITYFIRE", OracleType.Char).Value = (chkCOMMUNITYFIRE.Checked) ? "1" : "";
                                com.Parameters.Add(":CDAMAGEPRODUCT", OracleType.Char).Value = (chkDAMAGEPRODUCT.Checked) ? "1" : "";
                                com.Parameters.Add(":CCORPORATE", OracleType.Char).Value = (chkCORPORATE.Checked) ? "1" : "";
                                com.Parameters.Add(":CMEDIACOUNTRY", OracleType.Char).Value = (chkMEDIACOUNTRY.Checked) ? "1" : "";
                                com.Parameters.Add(":CMEDIACOMMUNITY", OracleType.Char).Value = (chkMEDIACOMMUNITY.Checked) ? "1" : "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = txtVendorID.Text;
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = txtContractID.Text;
                                com.Parameters.Add(":CPERSONAL", OracleType.Char).Value = (chkPersonal.Checked) ? "1" : "";
                                com.Parameters.Add(":NDAMAGEPRODUCT", OracleType.Number).Value = double.TryParse(txtDAMAGEPRODUCT.Text + "", out doublenum) ? doublenum : 0;
                                com.ExecuteNonQuery();

                            }

                            using (OracleCommand comDelList = new OracleCommand("DELETE FROM TPERSONALACCIDENT WHERE SACCIDENTID = '" + GenID + "'", con))
                            {
                                comDelList.ExecuteNonQuery();
                            }

                            var InsertPersonal = (List<DT1>)Session["DT1"];

                            strsql = @"INSERT INTO TMS.TPERSONALACCIDENT ( ID, SACCIDENTID, SGROUP,  SNAME, STYPE, SCREATE, DCREATE) 
                    VALUES (:ID,:SACCIDENTID,:SGROUP,:SNAME,:STYPE,:SCREATE,SYSDATE)";
                            foreach (var dr in InsertPersonal)
                            {
                                using (OracleCommand com1 = new OracleCommand(strsql, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":ID", OracleType.Number).Value = dr.dtsID;
                                    com1.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = GenID;
                                    com1.Parameters.Add(":SGROUP", OracleType.VarChar).Value = dr.dtsGroup;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = dr.dtsName;
                                    com1.Parameters.Add(":STYPE", OracleType.VarChar).Value = dr.dtsType;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.ExecuteNonQuery();
                                }
                            }


                            using (OracleCommand comDelfile = new OracleCommand("DELETE FROM TACCIDENTIMPORTFILE WHERE SACCIDENTID = '" + Session["addSACCIDENTID"] + "' AND NID BETWEEN 1 AND 4", con))
                            {
                                comDelfile.ExecuteNonQuery();
                            }

                            string strsql1 = "INSERT INTO TACCIDENTIMPORTFILE(NID,SACCIDENTID,SDOCUMENT,SFILENAME,SGENFILENAME,SFILEPATH,SCREATE,DCREATE) VALUES (:NID,:SACCIDENTID,:SDOCUMENT,:SFILENAME,'',:SFILEPATH,:SCREATE,SYSDATE)";

                            string[] sDucumentName = new string[4];

                            sDucumentName[0] = "สำเนาบัตรประชาชน";
                            sDucumentName[1] = "สำเนาใบขับขี่";
                            sDucumentName[2] = "รูปสถานที่เกิดเหตุ (จากGPS)";
                            sDucumentName[3] = "รูปสถานที่เกิดเหตุ (รูปภาพสถานที่เกิดเหตุจริง)";

                            for (int i = 0; i < 4; i++)
                            {
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {

                                    ASPxTextBox txtFileName = (ASPxTextBox)PageControl.FindControl("txtFileName" + i);
                                    ASPxTextBox txtFilePath = (ASPxTextBox)PageControl.FindControl("txtFilePath" + i);

                                    if (txtFileName != null && txtFilePath != null)
                                    {
                                        if ("" + txtFilePath.Text != "")
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":NID", OracleType.Number).Value = i + 1;
                                            com1.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = GenID;
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath.Text;
                                            com1.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = sDucumentName[i];
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }



                            using (OracleCommand com2 = new OracleCommand(strsql1, con))
                            {

                                if ("" + txtFilePath7.Text != "")
                                {
                                    com2.Parameters.Clear();
                                    com2.Parameters.Add(":NID", OracleType.Number).Value = 8;
                                    com2.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = GenID;
                                    com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName7.Text;
                                    com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath7.Text;
                                    com2.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = "สำเนาใบ Invoice";
                                    com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com2.ExecuteNonQuery();
                                }
                            }

                            using (OracleCommand com3 = new OracleCommand(strsql1, con))
                            {

                                if ("" + txtFilePath8.Text != "")
                                {
                                    com3.Parameters.Clear();
                                    com3.Parameters.Add(":NID", OracleType.Number).Value = 9;
                                    com3.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = GenID;
                                    com3.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName8.Text;
                                    com3.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath8.Text;
                                    com3.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = "หนังสือยินยอมชดใช้ค่าเสียหาย(ปริมาณน้ำมัน)";
                                    com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com3.ExecuteNonQuery();
                                }

                            }

                            Session["addSACCIDENTID"] = GenID;
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                            VisibleControlUpload();
                        }
                        LogUser("20", "I", "บันทึกข้อมูลหน้าอุบัติเหตุ", Session["addSACCIDENTID"] + "");
                    }

                    break;

                case "Save1":

                    double doublenum1 = 0.0;
                    double nSTARTTODESTINATION = double.TryParse("" + txtSTARTTODESTINATION.Text, out doublenum1) ? doublenum1 : 0;
                    double nSTARTTOACCIDENT = double.TryParse("" + txtSTARTTOACCIDENT.Text, out doublenum1) ? doublenum1 : 0;
                    double nPARKTOACCIDENT = double.TryParse("" + txtPARKTOACCIDENT.Text, out doublenum1) ? doublenum1 : 0;

                    if (nSTARTTOACCIDENT > nSTARTTODESTINATION)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณใส่ข้อมูลระยะทางจากจุดเริ่มต้น-จุดเกิดเหตุ มากกว่า ระยะทางจากจุดเริ่มต้น-ปลายทาง');");
                        txtTabVisible.Text = "2";
                        txtSTARTTOACCIDENT.BackColor = System.Drawing.Color.Red;
                        return;
                    }
                    else if (nPARKTOACCIDENT > nSTARTTODESTINATION)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณใส่ข้อมูลระยะทางจากจุดจอดสุดท้าย-จุดเกิดเหตุ มากกว่า ระยะทางจากจุดเริ่มต้น-ปลายทาง');");
                        txtTabVisible.Text = "2";
                        txtPARKTOACCIDENT.BackColor = System.Drawing.Color.Red;
                        return;
                    }

                    if (Session["addSACCIDENTID"] != null)
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = @"UPDATE TACCIDENT SET CINSPECTIONHISTORY=:CINSPECTIONHISTORY,CMAINTENANCEHISTORY=:CMAINTENANCEHISTORY,DLASTWORK=:DLASTWORK,TLASTWORK=:TLASTWORK,DBEGINWORK=:DBEGINWORK,TBEGINWORK=:TBEGINWORK,SSTART=:SSTART,SEND=:SEND
,NSTARTTODESTINATION=:NSTARTTODESTINATION,NSTARTTOACCIDENT=:NSTARTTOACCIDENT,NDRIVEHOUR=:NDRIVEHOUR,CPARK=:CPARK,TPARKTIME=:TPARKTIME,NPARKTOACCIDENT=:NPARKTOACCIDENT,CAFTERONEHOUR=:CAFTERONEHOUR
,NBEFOREJOB=:NBEFOREJOB,NALCOHOLACCIDENT=:NALCOHOLACCIDENT,CDRUGACCIDENT=:CDRUGACCIDENT,CSIGHTCHECK=:CSIGHTCHECK,SSIGHTCHECK=:SSIGHTCHECK,CDISEASECHECK=:CDISEASECHECK,SDISEASECHECK=:SDISEASECHECK
,CILLNESS=:CILLNESS,CMEDICINE=:CMEDICINE,SMEDICINE=:SMEDICINE,CDDC=:CDDC,SNOTDDC=:SNOTDDC,NEXPERIENCEYEAR=:NEXPERIENCEYEAR,NEXPERIENCEMONTH=:NEXPERIENCEMONTH,CUSEPHONE=:CUSEPHONE,SUSEPHONE=:SUSEPHONE
,CSPEEDOVERLIMIT=:CSPEEDOVERLIMIT,NSPEEDOVERLIMIT=:NSPEEDOVERLIMIT,CURGENT=:CURGENT,SURGENT=:SURGENT,CWARNINGSIGN=:CWARNINGSIGN,SWARNINGSIGN=:SWARNINGSIGN,SACCIDENTHISTORY1=:SACCIDENTHISTORY1
,SACCIDENTHISTORY2=:SACCIDENTHISTORY2,SACCIDENTHISTORY3=:SACCIDENTHISTORY3,CPLACE=:CPLACE,CLIGHTING=:CLIGHTING,SLIGHTING=:SLIGHTING,CRAIN=:CRAIN,SRAIN=:SRAIN,CROUTINE=:CROUTINE,
DUPDATE = sysdate,SUPDATE = :SUPDATE WHERE SACCIDENTID = :SACCIDENTID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                int num = 0;
                                double doublenum = 0.0;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                com.Parameters.Add(":CINSPECTIONHISTORY", OracleType.Char).Value = rblINSPECTIONHISTORY.Value + "";
                                com.Parameters.Add(":CMAINTENANCEHISTORY", OracleType.Char).Value = rblMAINTENANCEHISTORY.Value + "";
                                com.Parameters.Add(":DLASTWORK", OracleType.DateTime).Value = dteLASTWORK.Value;
                                com.Parameters.Add(":TLASTWORK", OracleType.VarChar).Value = tedLASTWORK.Text;
                                com.Parameters.Add(":DBEGINWORK", OracleType.DateTime).Value = dteBEGINWORK.Value;
                                com.Parameters.Add(":TBEGINWORK", OracleType.VarChar).Value = tedBEGINWORK.Text;
                                com.Parameters.Add(":SSTART", OracleType.VarChar).Value = txtSTART.Text;
                                com.Parameters.Add(":SEND", OracleType.VarChar).Value = txtEND.Text;
                                com.Parameters.Add(":NSTARTTODESTINATION", OracleType.Number).Value = nSTARTTODESTINATION;
                                com.Parameters.Add(":NSTARTTOACCIDENT", OracleType.Number).Value = nSTARTTOACCIDENT;
                                com.Parameters.Add(":NDRIVEHOUR", OracleType.Number).Value = double.TryParse("" + txtDRIVEHOUR.Text, out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CPARK", OracleType.Char).Value = rblPARK.Value + "";
                                com.Parameters.Add(":TPARKTIME", OracleType.VarChar).Value = ("" + rblPARK.Value == "1") ? txtPARKTIME.Text : "";
                                com.Parameters.Add(":NPARKTOACCIDENT", OracleType.Number).Value = nPARKTOACCIDENT;
                                com.Parameters.Add(":CAFTERONEHOUR", OracleType.Char).Value = rblAFTERONEHOUR.Value + "";
                                com.Parameters.Add(":NBEFOREJOB", OracleType.Number).Value = double.TryParse("" + txtBEFOREJOB.Text, out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":NALCOHOLACCIDENT", OracleType.Number).Value = double.TryParse("" + txtALCOHOLACCIDENT.Text, out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CDRUGACCIDENT", OracleType.Char).Value = rblDRUGACCIDENT.Value + "";
                                com.Parameters.Add(":CSIGHTCHECK", OracleType.Char).Value = rblSIGHTCHECK.Value + "";
                                com.Parameters.Add(":SSIGHTCHECK", OracleType.VarChar).Value = txtSIGHTCHECK.Text;
                                com.Parameters.Add(":CDISEASECHECK", OracleType.Char).Value = rblDISEASECHECK.Value + "";
                                com.Parameters.Add(":SDISEASECHECK", OracleType.VarChar).Value = txtDISEASECHECK.Text;
                                com.Parameters.Add(":CILLNESS", OracleType.Char).Value = rblILLNESS.Value + "";
                                com.Parameters.Add(":CMEDICINE", OracleType.Char).Value = rblMEDICINE.Value + "";
                                com.Parameters.Add(":SMEDICINE", OracleType.VarChar).Value = ("" + rblMEDICINE.Value == "2") ? tedMEDICINE.Text + "" : "";
                                com.Parameters.Add(":CDDC", OracleType.Char).Value = rblDDC.Value + "";
                                com.Parameters.Add(":SNOTDDC", OracleType.VarChar).Value = ("" + rblDDC.Value == "2") ? txtNOTDDC.Text + "" : "";
                                com.Parameters.Add(":NEXPERIENCEYEAR", OracleType.Number).Value = double.TryParse("" + txtEXPERIENCEYEAR.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":NEXPERIENCEMONTH", OracleType.Number).Value = double.TryParse("" + txtEXPERIENCEMONTH.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CUSEPHONE", OracleType.Char).Value = rblUSEPHONE.Value + "";
                                com.Parameters.Add(":SUSEPHONE", OracleType.VarChar).Value = ("" + rblUSEPHONE.Value == "2") ? rblUSEPHONE.Value + "" : "";
                                com.Parameters.Add(":CSPEEDOVERLIMIT", OracleType.Char).Value = rblSPEEDOVERLIMIT.Value + "";
                                com.Parameters.Add(":NSPEEDOVERLIMIT", OracleType.Number).Value = double.TryParse("" + txtSPEEDOVERLIMIT.Text + "", out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CURGENT", OracleType.Char).Value = rblURGENT.Value + "";
                                com.Parameters.Add(":SURGENT", OracleType.VarChar).Value = ("" + rblURGENT.Value == "2") ? txtURGENT.Text : "";
                                com.Parameters.Add(":CWARNINGSIGN", OracleType.Char).Value = rblWARNINGSIGN.Value + "";
                                com.Parameters.Add(":SWARNINGSIGN", OracleType.VarChar).Value = ("" + rblWARNINGSIGN.Value == "2") ? txtWARNINGSIGN.Text : "";
                                com.Parameters.Add(":SACCIDENTHISTORY1", OracleType.VarChar).Value = txtACCIDENTHISTORY1.Text;
                                com.Parameters.Add(":SACCIDENTHISTORY2", OracleType.VarChar).Value = txtACCIDENTHISTORY2.Text;
                                com.Parameters.Add(":SACCIDENTHISTORY3", OracleType.VarChar).Value = txtACCIDENTHISTORY3.Text;
                                com.Parameters.Add(":CPLACE", OracleType.Char).Value = rblPARK.Value + "";
                                com.Parameters.Add(":CLIGHTING", OracleType.Char).Value = rblLIGHTING.Value + "";
                                com.Parameters.Add(":SLIGHTING", OracleType.VarChar).Value = ("" + rblLIGHTING.Value == "4") ? rblLIGHTING.Value + "" : "";
                                com.Parameters.Add(":CRAIN", OracleType.Char).Value = rblRAIN.Value + "";
                                com.Parameters.Add(":SRAIN", OracleType.VarChar).Value = ("" + rblRAIN.Value == "3") ? txtRAIN.Text : "";
                                com.Parameters.Add(":CROUTINE", OracleType.Char).Value = rblROUTINE.Value + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.ExecuteNonQuery();

                            }

                            using (OracleCommand comDelfile = new OracleCommand("DELETE FROM TACCIDENTIMPORTFILE WHERE SACCIDENTID = '" + Session["addSACCIDENTID"] + "' AND NID BETWEEN 5 AND 7", con))
                            {
                                comDelfile.ExecuteNonQuery();
                            }

                            string strsql1 = "INSERT INTO TACCIDENTIMPORTFILE(NID,SACCIDENTID,SDOCUMENT,SFILENAME,SGENFILENAME,SFILEPATH,SCREATE,DCREATE) VALUES (:NID,:SACCIDENTID,:SDOCUMENT,:SFILENAME,'',:SFILEPATH,:SCREATE,SYSDATE)";

                            string[] sDucumentName = new string[3];

                            sDucumentName[0] = "การตรวจสภาพรถ";
                            sDucumentName[1] = "หลักฐานการซ่อมบำรุง";
                            sDucumentName[2] = "ผลการตรวจสารเสพติด ณ จุดเกิดเหตุ";
                            int o = 0;
                            for (int i = 4; i < 7; i++)
                            {
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {

                                    ASPxTextBox txtFileName = (ASPxTextBox)PageControl.FindControl("txtFileName" + i);
                                    ASPxTextBox txtFilePath = (ASPxTextBox)PageControl.FindControl("txtFilePath" + i);

                                    if (txtFileName != null && txtFilePath != null)
                                    {
                                        if ("" + txtFilePath.Text != "")
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":NID", OracleType.Number).Value = i + 1;
                                            com1.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath.Text;
                                            com1.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = sDucumentName[o];
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com1.ExecuteNonQuery();
                                            o++;

                                        }
                                    }
                                }
                            }
                        }

                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                        // CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='vendor_accident_lst.aspx';});");
                    }
                    break;

                case "CheckScore":
                    double num1 = 0.0;
                    double sum1 = 0.0;
                    double c2 = double.TryParse("" + cmbOrganizationEffect.Value, out num1) ? num1 : 0;
                    double c3 = double.TryParse("" + cmbDriverEffect.Value, out num1) ? num1 : 0;
                    double c4 = double.TryParse("" + cmbValueEffect.Value, out num1) ? num1 : 0;
                    double c6 = double.TryParse("" + cmbEnvironmentEffect.Value, out num1) ? num1 : 0;
                    sum1 = c2 * c3 * c4 * c6;

                    txtScoreSubtract.Text = CommonFunction.Get_Value(sql, "SELECT NSCORE FROM LSTMULTIPLEIMPACT WHERE " + sum1 + " BETWEEN NSTART AND CASE WHEN NEND = 0 THEN " + sum1 + " ELSE NEND END");
                    break;

                case "Save2":

                    if (Session["addSACCIDENTID"] != null)
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = @"UPDATE TACCIDENT SET CFAULT = :CFAULT, CUSE = '1', NSCORESUBTRACT = :NSCORESUBTRACT, DUPDATE = sysdate,SUPDATE = :SUPDATE,NORGANIZATIONEFFECT = :CORGANIZATIONEFFECT,NDRIVEREFFECT = :CDRIVEREFFECT,NVALUEEFFECT = :CVALUEEFFECT,NENVIRONMENTEFFECT = :CENVIRONMENTEFFECT,NFORECASTDAMAGE = :NFORECASTDAMAGE,NPENALTY = :NPENALTY,SBEGINHUMAN = :SBEGINHUMAN,SCOMMENT = :SCOMMENT,SDETAILWRONGCONTRACT = :SDETAILWRONGCONTRACT,SMANAGEANDEDIT = :SMANAGEANDEDIT,SPROTECTION = :SPROTECTION WHERE SACCIDENTID = :SACCIDENTID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                int num = 0;
                                double doublenum = 0.0;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":CORGANIZATIONEFFECT", OracleType.Number).Value = Int32.TryParse(cmbOrganizationEffect.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":CDRIVEREFFECT", OracleType.Number).Value = Int32.TryParse(cmbDriverEffect.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":CVALUEEFFECT", OracleType.Number).Value = Int32.TryParse(cmbValueEffect.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":CENVIRONMENTEFFECT", OracleType.Number).Value = Int32.TryParse(cmbEnvironmentEffect.Value + "", out num) ? num : 0;
                                com.Parameters.Add(":NFORECASTDAMAGE", OracleType.Number).Value = Int32.TryParse(txtForecastDamage.Text, out num) ? num : 0;
                                com.Parameters.Add(":NPENALTY", OracleType.Number).Value = Int32.TryParse(txtPenalty.Text, out num) ? num : 0;
                                com.Parameters.Add(":SBEGINHUMAN", OracleType.VarChar).Value = txtBeginHuman.Text;
                                com.Parameters.Add(":SCOMMENT", OracleType.VarChar).Value = txtComment.Text;
                                com.Parameters.Add(":SDETAILWRONGCONTRACT", OracleType.VarChar).Value = txtDetailWrongContract.Text;
                                com.Parameters.Add(":SMANAGEANDEDIT", OracleType.VarChar).Value = txtManageAndEdit.Text;
                                com.Parameters.Add(":SPROTECTION", OracleType.VarChar).Value = txtProtection.Text;
                                com.Parameters.Add(":NSCORESUBTRACT", OracleType.Number).Value = double.TryParse(txtScoreSubtract.Text, out doublenum) ? doublenum : 0;
                                com.Parameters.Add(":CFAULT", OracleType.Char).Value = cbxTruePK.Value + "";
                                com.ExecuteNonQuery();

                            }



                            int count = CommonFunction.Count_Value(sql, "SELECT * FROM TREDUCEPOINT WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'");
                            if (count > 0)
                            {
                                using (OracleConnection con11 = new OracleConnection(sql))
                                {
                                    if (con11.State == ConnectionState.Closed) con11.Open();
                                    if (cbxTruePK.Value != "0")
                                    {
                                        using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET NPOINT = " + (("" + txtScoreSubtract.Text != "") ? ("" + txtScoreSubtract.Text) : "0") + " WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con11))
                                        {
                                            comUpdate.ExecuteNonQuery();
                                        }
                                    }
                                    else if (cbxTruePK.Value == "0")
                                    {
                                        using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con11))
                                        {
                                            comUpdate.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID    ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                                if (cbxTruePK.Value != "0")
                                    ReducePoint("06", "090", "อุบัติเหตุ", "", "", "", "" + (("" + txtScoreSubtract.Text != "") ? ("" + txtScoreSubtract.Text) : "0"), Session["addSACCIDENTID"] + "",
                                       txtContractID.Text, txtTruckID.Text, cboHeadRegist.Value + "", "", cboTrailerRegist.Value + "", txtEmployeeID.Text);

                            }

                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                            VisibleControlUpload();
                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาบันทึกข้อมูลทั่วไปก่อน');");
                    }

                    break;

                case "Save3":

                    if (Session["addSACCIDENTID"] != null)
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = @"UPDATE TACCIDENT SET DUPDATE = sysdate,SUPDATE = :SUPDATE,CUSE = :CUSE,CDELIVERY = :CDELIVERY,CSTATUS = :CSTATUS,CRETURNSCORE = :CRETURNSCORE,SRETURNSCOREREMARK = :SRETURNSCOREREMARK WHERE SACCIDENTID = :SACCIDENTID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                int num = 0;
                                double doublenum = 0.0;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":CUSE", OracleType.Char).Value = rblUse.Value + "";
                                com.Parameters.Add(":CDELIVERY", OracleType.Char).Value = rblDelivery.Value + "";
                                com.Parameters.Add(":CSTATUS", OracleType.Char).Value = rblStatus.Value + "";
                                com.Parameters.Add(":CRETURNSCORE", OracleType.Char).Value = (chkReturnScore.Checked) ? '1' : '0';
                                com.Parameters.Add(":SRETURNSCOREREMARK", OracleType.VarChar).Value = txtReturnScoreRemark.Text;
                                com.ExecuteNonQuery();

                            }


                            if ("" + rblUse.Value == "0")
                            {
                                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                                {
                                    comUpdate.ExecuteNonQuery();
                                }
                            }
                            else
                            {
                                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '1' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                                {
                                    comUpdate.ExecuteNonQuery();
                                }
                            }

                            if (chkReturnScore.Checked)
                            {

                                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                                {
                                    comUpdate.ExecuteNonQuery();
                                }

                                using (OracleCommand comUpdateAccident = new OracleCommand("UPDATE TACCIDENT SET NSCORESUBTRACT = 0 WHERE SACCIDENTID = '" + Session["addSACCIDENTID"] + "'", con))
                                {
                                    comUpdateAccident.ExecuteNonQuery();
                                }
                            }

                        }
                        //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO



                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='accident_lst.aspx';});");
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาบันทึกข้อมูลทั่วไปก่อน');");
                    }

                    break;

                case "deleteFile":

                    string FilePath = paras[1];

                    if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                    {
                        File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                    }

                    string cNo = paras[2];
                    if (cNo == "1")
                    {
                        txtFileName0.Text = "";
                        txtFilePath0.Text = "";
                    }
                    else if (cNo == "2")
                    {
                        txtFileName1.Text = "";
                        txtFilePath1.Text = "";
                    }
                    else if (cNo == "3")
                    {
                        txtFileName2.Text = "";
                        txtFilePath2.Text = "";
                    }
                    else if (cNo == "4")
                    {
                        txtFileName3.Text = "";
                        txtFilePath3.Text = "";
                    }
                    else if (cNo == "5")
                    {
                        txtFileName4.Text = "";
                        txtFilePath4.Text = "";
                    }
                    else if (cNo == "6")
                    {
                        txtFileName5.Text = "";
                        txtFilePath5.Text = "";
                    }
                    else if (cNo == "7")
                    {
                        txtFileName6.Text = "";
                        txtFilePath6.Text = "";
                    }
                    else if (cNo == "8")
                    {
                        txtFileName7.Text = "";
                        txtFilePath7.Text = "";
                    }
                    else if (cNo == "9")
                    {
                        txtFileName8.Text = "";
                        txtFilePath8.Text = "";
                    }

                    VisibleControlUpload();
                    break;

            }

        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRUCKID,SCONTRACTID,SCONTRACTNO,SVENDORID,SVENDORNAME
        FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,
        c.SCONTRACTNO,c.SVENDORID,vs.SVENDORNAME FROM ((TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) 
        INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID) LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID
        WHERE t.SHEADREGISTERNO LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

    }

    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboTrailerRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.STRAILERREGISTERNO) AS RN , 
        T.STRAILERREGISTERNO FROM (TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID 
        WHERE T.STRAILERREGISTERNO LIKE :fillter AND c.SVENDORID LIKE :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", txtVendorID.Text));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }

    protected void cboTrailerRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void sgvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":

                string[] para = e.Args[0].Split(';');

                switch (para[0] + "")
                {
                    case "Del":
                        if (sgvw.VisibleRowCount > 0)
                        {
                            var delsdt1 = (List<DT1>)Session["DT1"];
                            delsdt1.RemoveAt(Convert.ToInt32(para[1]));

                            sgvw.DataSource = delsdt1;
                            sgvw.DataBind();

                            Session["DT1"] = delsdt1;
                        }
                        break;

                    case "AddList":

                        var addData = (List<DT1>)Session["DT1"];
                        int max = (addData.Count > 0) ? addData.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                        addData.Add(new DT1
                        {
                            dtsID = max,
                            dtsName = txtName.Text,
                            dtsGroup = cmb1.Text,
                            dtsType = cmb2.Text

                        });

                        Session["DT1"] = addData;
                        sgvw.DataSource = addData;
                        sgvw.DataBind();

                        txtName.Text = "";
                        cmb1.SelectedIndex = 0;
                        cmb2.SelectedIndex = 0;

                        break;
                }
                break;
        }
    }

    private void BindData()
    {
        DataTable dt = new DataTable();
        string strsql = @"SELECT SACCIDENTID,DACCIDENT,TACCIDENTTIME,DDATENOTIFY,SCITIZENID,SEMPLOYEEID,SHEADREGISTERNO,STRAILERREGISTERNO,STRUCKID
,SACCIDENTLOCALE,CTRANSPORTPROCESS,CEVENT,SEVENTOTHER,CNOLITIGANT,SLITIGANT,CFAULT,CVALUE,NVALUE,NDAMAGEPRODUCT,CVALUETERMINAL,NVALUETERMINAL,CPRODUCT,SPRODUCT
,NAMOUNT,CENVIRONMENT,CWATER,CCOMMUNITY,CFARM,CFIRE,CVEHICLEFIRE,CCOMMUNITYFIRE,CDAMAGEPRODUCT,CCORPORATE,CMEDIACOUNTRY,CMEDIACOMMUNITY,SCONTRACTID,SVENDORID,CINSPECTIONHISTORY
,CMAINTENANCEHISTORY,DLASTWORK,TLASTWORK,DBEGINWORK,TBEGINWORK,SSTART,SEND,NSTARTTODESTINATION,NSTARTTOACCIDENT,NDRIVEHOUR,CPARK,TPARKTIME,NPARKTOACCIDENT
,CAFTERONEHOUR,NBEFOREJOB,NALCOHOLACCIDENT,CDRUGACCIDENT,CSIGHTCHECK,SSIGHTCHECK,CDISEASECHECK,SDISEASECHECK,CILLNESS,CMEDICINE,SMEDICINE,CDDC,SNOTDDC
,NEXPERIENCEYEAR,NEXPERIENCEMONTH,CUSEPHONE,SUSEPHONE,CSPEEDOVERLIMIT,NSPEEDOVERLIMIT,CURGENT,SURGENT,CWARNINGSIGN,SWARNINGSIGN,SACCIDENTHISTORY1,SACCIDENTHISTORY2
,SACCIDENTHISTORY3,CPLACE,CLIGHTING,SLIGHTING,CRAIN,SRAIN,CROUTINE,CPERSONAL,NORGANIZATIONEFFECT,NDRIVEREFFECT,NVALUEEFFECT,NENVIRONMENTEFFECT,NFORECASTDAMAGE,NPENALTY,
SBEGINHUMAN,SCOMMENT,SDETAILWRONGCONTRACT,SMANAGEANDEDIT,SPROTECTION
,CUSE,NSCORESUBTRACT,CDELIVERY,CRETURNSCORE,SRETURNSCOREREMARK,CSENDBY FROM TACCIDENT WHERE SACCIDENTID = '" + Session["oSACCIDENTID"] + "'";

        dt = CommonFunction.Get_Data(sql, strsql);
        if (dt.Rows.Count > 0)
        {
            txtDate.Text = dt.Rows[0]["DDATENOTIFY"] + "";
            txtACCIDENTHISTORY1.Text = dt.Rows[0]["SACCIDENTHISTORY1"] + "";
            txtACCIDENTHISTORY2.Text = dt.Rows[0]["SACCIDENTHISTORY2"] + "";
            txtACCIDENTHISTORY3.Text = dt.Rows[0]["SACCIDENTHISTORY3"] + "";
            txtACCIDENTLOCALE.Text = dt.Rows[0]["SACCIDENTLOCALE"] + "";
            txtALCOHOLACCIDENT.Text = dt.Rows[0]["NALCOHOLACCIDENT"] + "";
            txtAMOUNT.Text = dt.Rows[0]["NAMOUNT"] + "";
            txtBEFOREJOB.Text = dt.Rows[0]["NBEFOREJOB"] + "";
            txtContractID.Text = dt.Rows[0]["SCONTRACTID"] + "";
            txtDAMAGEPRODUCT.Text = dt.Rows[0]["NDAMAGEPRODUCT"] + "";
            txtDISEASECHECK.Text = dt.Rows[0]["CDISEASECHECK"] + "";
            txtDRIVEHOUR.Text = dt.Rows[0]["NDRIVEHOUR"] + "";
            txtEmployeeID.Text = dt.Rows[0]["SEMPLOYEEID"] + "";
            txtEND.Text = dt.Rows[0]["SEND"] + "";
            txtEVENTOTHER.Text = dt.Rows[0]["SEVENTOTHER"] + "";
            txtEXPERIENCEMONTH.Text = dt.Rows[0]["NEXPERIENCEMONTH"] + "";
            txtEXPERIENCEYEAR.Text = dt.Rows[0]["NEXPERIENCEYEAR"] + "";
            txtLIGHTING.Text = dt.Rows[0]["SLIGHTING"] + "";
            txtLITIGANT.Text = dt.Rows[0]["SLITIGANT"] + "";
            txtNOTDDC.Text = dt.Rows[0]["SNOTDDC"] + "";
            txtPARKTIME.Text = dt.Rows[0]["TPARKTIME"] + "";
            txtPARKTOACCIDENT.Text = dt.Rows[0]["NPARKTOACCIDENT"] + "";
            txtPRODUCT.Text = dt.Rows[0]["SPRODUCT"] + "";
            txtRAIN.Text = dt.Rows[0]["SRAIN"] + "";
            txtSIGHTCHECK.Text = dt.Rows[0]["SSIGHTCHECK"] + "";
            txtSPEEDOVERLIMIT.Text = dt.Rows[0]["NSPEEDOVERLIMIT"] + "";
            txtSTART.Text = dt.Rows[0]["SSTART"] + "";
            txtSTARTTOACCIDENT.Text = dt.Rows[0]["NSTARTTOACCIDENT"] + "";
            txtSTARTTODESTINATION.Text = dt.Rows[0]["NSTARTTODESTINATION"] + "";
            txtTruckID.Text = dt.Rows[0]["STRUCKID"] + "";
            txtURGENT.Text = dt.Rows[0]["SURGENT"] + "";
            txtUSEPHONE.Text = dt.Rows[0]["SUSEPHONE"] + "";
            txtVALUE.Text = dt.Rows[0]["NVALUE"] + "";
            txtVALUETERMINAL.Text = dt.Rows[0]["NVALUETERMINAL"] + "";
            txtVendorID.Text = dt.Rows[0]["SVENDORID"] + "";
            txtWARNINGSIGN.Text = dt.Rows[0]["SWARNINGSIGN"] + "";

            chkCOMMUNITY.Checked = ("" + dt.Rows[0]["CMEDIACOMMUNITY"] == "1") ? true : false;
            chkCOMMUNITYFIRE.Checked = ("" + dt.Rows[0]["CCOMMUNITYFIRE"] == "1") ? true : false;
            chkCORPORATE.Checked = ("" + dt.Rows[0]["CCORPORATE"] == "1") ? true : false;
            chkDAMAGEPRODUCT.Checked = ("" + dt.Rows[0]["CDAMAGEPRODUCT"] == "1") ? true : false;
            chkENVIRONMENT.Checked = ("" + dt.Rows[0]["CENVIRONMENT"] == "1") ? true : false;
            chkFARM.Checked = ("" + dt.Rows[0]["CFARM"] == "1") ? true : false;
            chkFIRE.Checked = ("" + dt.Rows[0]["CFIRE"] == "1") ? true : false;
            chkMEDIACOMMUNITY.Checked = ("" + dt.Rows[0]["CMEDIACOMMUNITY"] == "1") ? true : false;
            chkMEDIACOUNTRY.Checked = ("" + dt.Rows[0]["CMEDIACOUNTRY"] == "1") ? true : false;
            chkPersonal.Checked = ("" + dt.Rows[0]["CPERSONAL"] == "1") ? true : false;
            chkPRODUCT.Checked = ("" + dt.Rows[0]["CPRODUCT"] == "1") ? true : false;
            chkVALUE.Checked = ("" + dt.Rows[0]["CVALUE"] == "1") ? true : false;
            chkVALUETERMINAL.Checked = ("" + dt.Rows[0]["CVALUETERMINAL"] == "1") ? true : false;
            chkVEHICLEFIRE.Checked = ("" + dt.Rows[0]["CVEHICLEFIRE"] == "1") ? true : false;
            chkWATER.Checked = ("" + dt.Rows[0]["CWATER"] == "1") ? true : false;

            rblAFTERONEHOUR.Value = dt.Rows[0]["CAFTERONEHOUR"] + "";
            rblDDC.Value = dt.Rows[0]["CDDC"] + "";
            rblDISEASECHECK.Value = dt.Rows[0]["CDISEASECHECK"] + "";
            rblDRUGACCIDENT.Value = dt.Rows[0]["CDRUGACCIDENT"] + "";
            rblEVENT.Value = dt.Rows[0]["CEVENT"] + "";
            rblFAULT.Value = dt.Rows[0]["CFAULT"] + "";
            rblILLNESS.Value = dt.Rows[0]["CILLNESS"] + "";
            rblINSPECTIONHISTORY.Value = dt.Rows[0]["CINSPECTIONHISTORY"] + "";
            rblLIGHTING.Value = dt.Rows[0]["CLIGHTING"] + "";
            rblLITIGANT.Value = dt.Rows[0]["CNOLITIGANT"] + "";
            rblMAINTENANCEHISTORY.Value = dt.Rows[0]["CMAINTENANCEHISTORY"] + "";
            rblMEDICINE.Value = dt.Rows[0]["CMEDICINE"] + "";
            rblPARK.Value = dt.Rows[0]["CPARK"] + "";
            rblPLACE.Value = dt.Rows[0]["CPLACE"] + "";
            rblRAIN.Value = dt.Rows[0]["CRAIN"] + "";
            rblROUTINE.Value = dt.Rows[0]["CROUTINE"] + "";
            rblSIGHTCHECK.Value = dt.Rows[0]["CSIGHTCHECK"] + "";
            rblSPEEDOVERLIMIT.Value = dt.Rows[0]["CSPEEDOVERLIMIT"] + "";
            rblTRANSPORTPROCESS.Value = dt.Rows[0]["CTRANSPORTPROCESS"] + "";
            rblURGENT.Value = dt.Rows[0]["CURGENT"] + "";
            rblUSEPHONE.Value = dt.Rows[0]["CUSEPHONE"] + "";
            rblWARNINGSIGN.Value = dt.Rows[0]["CWARNINGSIGN"] + "";

            DateTime date;
            dteBEGINWORK.Value = DateTime.TryParse(dt.Rows[0]["DBEGINWORK"] + "", out date) ? date : DateTime.Now;
            dteDateAccident.Value = DateTime.TryParse(dt.Rows[0]["DACCIDENT"] + "", out date) ? date : DateTime.Now;
            dteLASTWORK.Value = DateTime.TryParse(dt.Rows[0]["DLASTWORK"] + "", out date) ? date : DateTime.Now;

            tedBEGINWORK.Text = dt.Rows[0]["TBEGINWORK"] + "";
            tedLASTWORK.Text = dt.Rows[0]["TLASTWORK"] + "";
            tedMEDICINE.Text = dt.Rows[0]["SMEDICINE"] + "";

            cmbPersonalNo.Value = dt.Rows[0]["SCITIZENID"] + "";
            cboHeadRegist.Value = dt.Rows[0]["SHEADREGISTERNO"] + "";
            cboTrailerRegist.Value = dt.Rows[0]["STRAILERREGISTERNO"] + "";



            cbxTruePK.Value = dt.Rows[0]["CFAULT"] + "";
            cmbOrganizationEffect.Value = dt.Rows[0]["NORGANIZATIONEFFECT"] + "";
            cmbDriverEffect.Value = dt.Rows[0]["NDRIVEREFFECT"] + "";
            cmbValueEffect.Value = dt.Rows[0]["NVALUEEFFECT"] + "";
            cmbEnvironmentEffect.Value = dt.Rows[0]["NENVIRONMENTEFFECT"] + "";
            txtForecastDamage.Text = dt.Rows[0]["NFORECASTDAMAGE"] + "";
            txtPenalty.Text = dt.Rows[0]["NPENALTY"] + "";
            txtBeginHuman.Text = dt.Rows[0]["SBEGINHUMAN"] + "";
            txtComment.Text = dt.Rows[0]["SCOMMENT"] + "";
            txtDetailWrongContract.Text = dt.Rows[0]["SDETAILWRONGCONTRACT"] + "";
            txtManageAndEdit.Text = dt.Rows[0]["SMANAGEANDEDIT"] + "";
            txtProtection.Text = dt.Rows[0]["SPROTECTION"] + "";
            rblUse.Value = dt.Rows[0]["CUSE"] + "";
            txtScoreSubtract.Text = dt.Rows[0]["NSCORESUBTRACT"] + "";
            rblDelivery.Value = dt.Rows[0]["CDELIVERY"] + "";

            chkReturnScore.Checked = ("" + dt.Rows[0]["CRETURNSCORE"] == "1") ? true : false;
            txtReturnScoreRemark.Text = dt.Rows[0]["SRETURNSCOREREMARK"] + "";

            rblStatus.Value = Session["sendStatus"] + "";

            if ((dt.Rows[0]["CSENDBY"] + "").Contains("1"))
            {
                rblDelivery.Value = "1";
                rblDelivery.ClientEnabled = false;
            }


            strsql = "SELECT * FROM TPERSONALACCIDENT WHERE SACCIDENTID = '" + Session["oSACCIDENTID"] + "'";
            DataTable dt1 = CommonFunction.Get_Data(sql, strsql);

            int num = 0;

            var ddd = new List<DT1>();
            if (dt1.Rows.Count > 0)
            {
                foreach (DataRow dr in dt1.Rows)
                {
                    ddd.Add(new DT1
                    {
                        dtsID = int.TryParse(dr["ID"] + "", out num) ? num : 0,
                        dtsGroup = dr["SGROUP"] + "",
                        dtsType = dr["STYPE"] + "",
                        dtsName = dr["SNAME"] + ""
                    });
                }

                sgvw.DataSource = ddd;
                sgvw.DataBind();
            }
            Session["DT1"] = ddd;


            DataTable dtFile = CommonFunction.Get_Data(sql, "SELECT NID, SFILENAME,SFILEPATH FROM TACCIDENTIMPORTFILE WHERE SACCIDENTID ='" + Session["oSACCIDENTID"] + "'");
            foreach (DataRow dr in dtFile.Rows)
            {
                switch ("" + dr["NID"])
                {
                    case "1":
                        txtFileName0.Text = dr["SFILENAME"] + "";
                        txtFilePath0.Text = dr["SFILEPATH"] + "";
                        break;
                    case "2":
                        txtFileName1.Text = dr["SFILENAME"] + "";
                        txtFilePath1.Text = dr["SFILEPATH"] + "";
                        break;
                    case "3":
                        txtFileName2.Text = dr["SFILENAME"] + "";
                        txtFilePath2.Text = dr["SFILEPATH"] + "";
                        break;
                    case "4":
                        txtFileName3.Text = dr["SFILENAME"] + "";
                        txtFilePath3.Text = dr["SFILEPATH"] + "";
                        break;
                    case "5":
                        txtFileName4.Text = dr["SFILENAME"] + "";
                        txtFilePath4.Text = dr["SFILEPATH"] + "";
                        break;
                    case "6":
                        txtFileName5.Text = dr["SFILENAME"] + "";
                        txtFilePath5.Text = dr["SFILEPATH"] + "";
                        break;
                    case "7":
                        txtFileName6.Text = dr["SFILENAME"] + "";
                        txtFilePath6.Text = dr["SFILEPATH"] + "";
                        break;
                    case "8":
                        txtFileName7.Text = dr["SFILENAME"] + "";
                        txtFilePath7.Text = dr["SFILEPATH"] + "";
                        break;
                    case "9":
                        txtFileName8.Text = dr["SFILENAME"] + "";
                        txtFilePath8.Text = dr["SFILEPATH"] + "";
                        break;
                }


            }

            VisibleControlUpload();
        }
    }

    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID, SPERSONELNO,FULLNAME FROM (SELECT E.SEMPLOYEEID, E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND E.SPERSONELNO LIKE :fillter AND e.STRANS_ID LIKE :vendorid) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        //sdsPersonal.SelectParameters.Add("vendorid", TypeCode.String, Session["SVDID"] + "");
        sdsPersonal.SelectParameters.Add("vendorid", TypeCode.String, String.Format("%{0}%", txtVendorID.Text));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "accident" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName;


            }
        }
        else
        {

            return;

        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void VisibleControlUpload()
    {
        bool visible = string.IsNullOrEmpty(txtFilePath0.Text);
        uplExcel0.ClientVisible = visible;
        txtFileName0.ClientVisible = !visible;
        btnView0.ClientEnabled = !visible;
        btnDelFile0.ClientEnabled = !visible;

        if (!(string.IsNullOrEmpty(txtFilePath0.Text)))
        {
            chkUpload1.Value = "1";
        }
        else
        {
            chkUpload1.Value = "";
        }

        visible = string.IsNullOrEmpty(txtFilePath1.Text);
        uplExcel1.ClientVisible = visible;
        txtFileName1.ClientVisible = !visible;
        btnView1.ClientEnabled = !visible;
        btnDelFile1.ClientEnabled = !visible;

        if (!(string.IsNullOrEmpty(txtFilePath1.Text)))
        {
            chkUpload2.Value = "1";
        }
        else
        {
            chkUpload2.Value = "";
        }

        visible = string.IsNullOrEmpty(txtFilePath2.Text);
        uplExcel2.ClientVisible = visible;
        txtFileName2.ClientVisible = !visible;
        btnView2.ClientEnabled = !visible;
        btnDelFile2.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath3.Text);
        uplExcel3.ClientVisible = visible;
        txtFileName3.ClientVisible = !visible;
        btnView3.ClientEnabled = !visible;
        btnDelFile3.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath4.Text);
        uplExcel4.ClientVisible = visible;
        txtFileName4.ClientVisible = !visible;
        btnView4.ClientEnabled = !visible;
        btnDelFile4.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath5.Text);
        uplExcel5.ClientVisible = visible;
        txtFileName5.ClientVisible = !visible;
        btnView5.ClientEnabled = !visible;
        btnDelFile5.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath6.Text);
        uplExcel6.ClientVisible = visible;
        txtFileName6.ClientVisible = !visible;
        btnView6.ClientEnabled = !visible;
        btnDelFile6.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath7.Text);
        uplExcel7.ClientVisible = visible;
        txtFileName7.ClientVisible = !visible;
        btnView7.ClientEnabled = !visible;
        btnDelFile7.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath8.Text);
        uplExcel8.ClientVisible = visible;
        txtFileName8.ClientVisible = !visible;
        btnView8.ClientEnabled = !visible;
        btnDelFile8.ClientEnabled = !visible;
    }

    protected bool ReducePoint(string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    {//SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection);
        repoint.NREDUCEID = "";
        //repoint.DREDUCE = "";
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = "" + Session["UserID"];
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];
        repoint.SDELIVERYNO = "" + sArrayParams[11];
        repoint.Insert();
        return IsReduce;
    }

    protected void InActiveReducePoint()
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();
            if (chkReturnScore.Checked)
            {

                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                {
                    comUpdate.ExecuteNonQuery();
                }
            }

        }

    }

    [Serializable]
    struct DT1
    {
        public int dtsID { get; set; }
        public string dtsGroup { get; set; }
        public string dtsType { get; set; }
        public string dtsName { get; set; }
    }


    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}