﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="SurpriseCheckContract.aspx.cs" Inherits="SurpriseCheckContract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="form-horizontal">

        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>ค้นหา
                    </div>
                    <div class="panel-body">
                        <div class="row form-group">
                            <label class="col-md-3 control-label">วันที่นัดหมาย</label>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDate" />
                            </div>
                            <label class="col-md-2 control-label">ถึง</label>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDateTo" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เลขที่สัญญา</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract" >
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ค้นหา</label>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" placeholder="ทะเบียนรถ" />
                            </div>
                            <label class="col-md-2 control-label">สถานะ</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlStatus" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8">

                                <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                                <asp:Button Text="เคลียร์" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                                <asp:Button Text="Export" runat="server" ID="btnExport" class="btn btn-md bth-hover btn-info" OnClick="btnExport_Click" Visible="false" />
                                <a href="SurpriseCheckContractAddEdit.aspx" target="_blank" runat="server" id="aAdd" class="btn btn-md bth-hover btn-info" style="color: white;">เพิ่ม
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>ผลการค้นหา จำนวน
                <asp:Label Text="0" runat="server" ID="lblItem" />
                        รายการ
                    </div>
                    <div class="panel-body">
                        <asp:GridView runat="server" ID="gvSurpriseCheckContract"
                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                            AllowPaging="True" OnRowUpdating="gvSurpriseCheckContract_RowUpdating" DataKeyNames="ID,IS_ACTIVE" OnPageIndexChanging="gvSurpriseCheckContract_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="วันที่นัดหมาย" DataField="CHECK_DATE" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" ItemStyle-HorizontalAlign="Center" />

                                <%--<asp:BoundField HeaderText="กลุ่มงาน" DataField="GROUPSNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />--%>
                                <asp:BoundField HeaderText="ชื่อผู้ขนส่ง" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="หมายเลขแชสซีย์(หัว)" DataField="CHASIS_H" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="หมายเลขแชสซีย์(หาง)" DataField="CHASIS_T" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="ทะเบียน(หัว)" DataField="SHEADREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="ทะเบียน(หาง)" DataField="STRAILERREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

                                <asp:BoundField HeaderText="สถานะ" DataField="STATUS_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="View" CommandName="update"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="gvSurpriseCheckContract" />
            </Triggers>
        </asp:UpdatePanel>

    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

