﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.ContractConfirm;

public partial class ConfirmTruckDetailCheckIVMS : PageBase
{
    DataTable dt;
    DataSet ds;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (!(Permissions("97") || Permissions("7")))
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //}
            //if (Session["CGROUP"] + string.Empty == "0")
            //{
            //    Response.Redirect("~/default.aspx");
            //    return;
            //}

            SetDrowDownList();
            if (Session["CGROUP"] + string.Empty == "0")
            {
                ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
                ddlVendor.Enabled = false;
                ddlVendor_SelectedIndexChanged(null, null);
                //mode = "view";
            }
            if (Request.QueryString["str"] != null)
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                if (!string.IsNullOrEmpty(strQuery))
                {
                    string[] strData = strQuery.Split('&');
                    //ทะเบียนรถ
                    ddlVendor.SelectedValue = strData[1];
                    ddlVendor_SelectedIndexChanged(null, null);
                    ddlSHEADREGISTERNO.Value = strData[0];
                    //txtSHEADREGISTERNO.Text = strData[0];
                }
            }
            this.AssignAuthen();
        }
        else
        {
            ddlVendor_SelectedIndexChanged(null, null);
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnCheckIVMS.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {
        ddlTTERMINAL.DataTextField = "SABBREVIATIOn";
        ddlTTERMINAL.DataValueField = "STERMINALID";
        ddlTTERMINAL.DataSource = ContractBLL.Instance.TTERMINALSelect();
        ddlTTERMINAL.DataBind();
        ddlTTERMINAL.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        ddlTTERMINAL.SelectedIndex = 0;

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });

        //ddlSHEADREGISTERNO.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem() { });
    }

    #endregion

    #region Btn_Cilck
    

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        
        if (Session["CGROUP"] + string.Empty == "0")
        {
            ddlSHEADREGISTERNO.SelectedIndex = -1;
        }
        else
        {
            ddlVendor.ClearSelection();
            ddlVendor_SelectedIndexChanged(null, null);
            ddlSHEADREGISTERNO.SelectedIndex = -1;
        }
        
        ddlTTERMINAL.SelectedIndex = 0;
    }
    #endregion

    #region btnCheckIVMS_Click
    protected void btnCheckIVMS_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlSHEADREGISTERNO.SelectedIndex < 0 || ddlTTERMINAL.SelectedIndex <= 0)
            {
                alertFail("กรุณาป้อนทะเบียนรถและคลังต้นทาง !!!");
                return;
            }

            DataSet dsIVMS = ConfirmTruckBLL.Instance.PTTConfirmTruckDetailCheckIVMSSelect(ddlTTERMINAL.SelectedValue, "'" + ddlSHEADREGISTERNO.Value.ToString().Trim() + "'");

            if (!dsIVMS.Tables[0].Columns.Contains("SHEADREGISTERNO"))
            {
                dsIVMS.Tables[0].Columns.Add("SHEADREGISTERNO");
            }
            if (!dsIVMS.Tables[0].Columns.Contains("SABBREVIATION"))
            {
                dsIVMS.Tables[0].Columns.Add("SABBREVIATION");
            }
            if (!dsIVMS.Tables[0].Columns.Contains("IVMS_GPS_STAT_NOW"))
            {
                dsIVMS.Tables[0].Columns.Add("IVMS_GPS_STAT_NOW");
            }
            if (!dsIVMS.Tables[0].Columns.Contains("IVMS_MDVR_STAT_NOW"))
            {
                dsIVMS.Tables[0].Columns.Add("IVMS_MDVR_STAT_NOW");
            }
            if (dsIVMS.Tables[0].Rows.Count > 0)
            {
                dsIVMS.Tables[0].Rows[0]["SHEADREGISTERNO"] = ddlSHEADREGISTERNO.Value.ToString().Trim();
                dsIVMS.Tables[0].Rows[0]["SABBREVIATION"] = ddlTTERMINAL.SelectedItem.Text;
                dsIVMS.Tables[0].Rows[0]["IVMS_GPS_STAT_NOW"] = dsIVMS.Tables[0].Rows[0]["GPS_STAT"] + string.Empty == "1" ? "Yes" : "No";
                dsIVMS.Tables[0].Rows[0]["IVMS_MDVR_STAT_NOW"] = dsIVMS.Tables[0].Rows[0]["MDVR_STAT"] + string.Empty == "1" ? "Yes" : "No";
            }
            gvData.DataSource = dsIVMS.Tables[0];
            gvData.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion
    #endregion

    #region gvData_PageIndexChanging
    protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvData.PageIndex = e.NewPageIndex;
       
    }
    #endregion

    #region gvData_RowDataBound
    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }
    }
    #endregion



    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddlSHEADREGISTERNO.DataSource = ConfirmTruckBLL.Instance.TruckByVendorSelect(ddlVendor.SelectedValue);
            ddlSHEADREGISTERNO.DataBind();

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
}