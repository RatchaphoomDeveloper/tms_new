﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Web.Configuration;
using DevExpress.XtraReports.UI;
using DevExpress.XtraCharts;

public partial class ReportReducePoint : System.Web.UI.Page
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            checkPermitionReport();

            for (int i = 1; i <= 12; i++)
            {
                string Month = DateTimeFormatInfo.GetInstance(new CultureInfo("th-TH")).GetMonthName(i);
                cmbStartMonth.Items.Add(new ListEditItem("เดือน " + Month, i));
                cmbEndMonth.Items.Add(new ListEditItem("เดือน " + Month, i));
            }

            cmbStartMonth.SelectedIndex = 0;
            cmbEndMonth.SelectedIndex = 11;

            string cYear = DateTime.Today.Year.ToString();
            int nYear = Convert.ToInt16(cYear);
            for (int i = 0; i <= 20; i++)
            {
                cmbYear.Items.Add(nYear + 543 + "", nYear);
                nYear -= 1;

            }
            cmbYear.SelectedIndex = 0;

        }
        else
        {
            CreateReport();
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }



    protected void BuildChart(XRChart chart)
    {

        string sqlquery = string.Format(@"SELECT V.SVENDORNAME,COUNT(O.NREDUCEID) AS COUNTREDUCE ,
ROUND((COUNT(O.NREDUCEID) / (SELECT COUNT(NREDUCEID) FROM TREDUCEPOINT WHERE TO_DATE('1/' || TO_CHAR(DREDUCE,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY'))) * 100,2) AS NPERCENT 
FROM ((
SELECT    nvl(r.SCONTRACTID,C.SCONTRACTID) AS SCONTRACTID,R.NREDUCEID
                                                          FROM           ((TREDUCEPOINT r LEFT JOIN
                                                                                    TTRUCK t ON R.SHEADREGISTERNO = T .SHEADREGISTERNO) LEFT JOIN
                                                                                    TCONTRACT_TRUCK ct ON T .STRUCKID = CT.STRUCKID) LEFT JOIN
                                                                                    TCONTRACT c ON NVL(CT.SCONTRACTID,r.SCONTRACTID) = C.SCONTRACTID
                                                                                    WHERE TO_DATE('1/' || TO_CHAR(r.DREDUCE,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY')  AND NVL(c.CACTIVE,'Y') = 'Y'
)o INNER JOIN TCONTRACT cc ON O.SCONTRACTID = CC.SCONTRACTID )LEFT JOIN TVENDOR_SAP v ON CC.SVENDORID = V.SVENDORID
   GROUP BY V.SVENDORNAME", "1/" + cmbStartMonth.Value + "/" + cmbYear.Value, "1/" + cmbEndMonth.Value + "/" + cmbYear.Value);

        DataTable dt1 = CommonFunction.Get_Data(sql, sqlquery);

        chart.DataSource = dt1;
        chart.Series[0].ArgumentScaleType = ScaleType.Qualitative;
        chart.Series[0].ArgumentDataMember = "SVENDORNAME";
        chart.Series[0].ValueDataMembers[0] = "COUNTREDUCE";

        chart.Series[1].ArgumentScaleType = ScaleType.Qualitative;
        chart.Series[1].ArgumentDataMember = "SVENDORNAME";
        chart.Series[1].ValueDataMembers[0] = "NPERCENT";

    }

    protected void checkPermitionReport()
    {
        // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;

    }

    protected void CreateReport()
    {
//        string sqlquery = string.Format(@"SELECT ROW_NUMBER () OVER(ORDER BY t.STOPICNAME) || ')' || t.STOPICNAME AS STOPICNAME, SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '01' THEN 1 ELSE 0 END) AS M1 ,
// SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '02' THEN 1 ELSE 0 END) AS M2 ,
//  SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '03' THEN 1 ELSE 0 END) AS M3 ,
//   SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '04' THEN 1 ELSE 0 END) AS M4 ,
//    SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '05' THEN 1 ELSE 0 END) AS M5 ,
//     SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '06' THEN 1 ELSE 0 END) AS M6 ,
//      SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '07' THEN 1 ELSE 0 END) AS M7 ,
//       SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '08' THEN 1 ELSE 0 END) AS M8 ,
//        SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '09' THEN 1 ELSE 0 END) AS M9 ,
//         SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '10' THEN 1 ELSE 0 END) AS M10 ,
//          SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '11' THEN 1 ELSE 0 END) AS M11 ,
//         SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '12' THEN 1 ELSE 0 END) AS M12 ,
//         COUNT(R.NREDUCEID) AS SUMREDUCE
//    FROM (SELECT NREDUCEID,DREDUCE,nvl(STOPICID,21) AS STOPICID FROM TREDUCEPOINT) r INNER JOIN TTOPIC t ON r.STOPICID = T.STOPICID WHERE T.CACTIVE = '1' AND TO_DATE('1/' || TO_CHAR(r.DREDUCE,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY')
//GROUP BY T.STOPICNAME", "1/" + cmbStartMonth.Value + "/" + cmbYear.Value, "1/" + cmbEndMonth.Value + "/" + cmbYear.Value);

        string sqlquery = string.Format(@"SELECT  ROW_NUMBER () OVER(ORDER BY t.STOPICNAME) || ')' || t.STOPICNAME AS STOPICNAME,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '01' THEN 1 ELSE 0 END) AS M1 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '02' THEN 1 ELSE 0 END) AS M2 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '03' THEN 1 ELSE 0 END) AS M3 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '04' THEN 1 ELSE 0 END) AS M4 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '05' THEN 1 ELSE 0 END) AS M5 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '06' THEN 1 ELSE 0 END) AS M6 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '07' THEN 1 ELSE 0 END) AS M7 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '08' THEN 1 ELSE 0 END) AS M8 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '09' THEN 1 ELSE 0 END) AS M9 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '10' THEN 1 ELSE 0 END) AS M10 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '11' THEN 1 ELSE 0 END) AS M11 ,
SUM(CASE WHEN TO_CHAR(R.DREDUCE,'MM') = '12' THEN 1 ELSE 0 END) AS M12 ,
COUNT(R.NREDUCEID) AS SUMREDUCE
FROM TTOPIC t  
LEFT JOIN 
(
   -- SELECT NREDUCEID,DREDUCE,nvl(STOPICID,21) AS STOPICID FROM TREDUCEPOINT
   -- WHERE  TO_DATE('1/' || TO_CHAR(DREDUCE,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY')
    SELECT  NREDUCEID,DREDUCE,nvl(STOPICID,21) AS STOPICID FROM TREDUCEPOINT tr
    LEFT JOIN TTRUCK t ON tr.SHEADREGISTERNO = T .SHEADREGISTERNO 
    LEFT JOIN TCONTRACT_TRUCK ct ON T .STRUCKID = CT.STRUCKID 
    LEFT JOIN TCONTRACT c ON NVL(CT.SCONTRACTID,tr.SCONTRACTID) = C.SCONTRACTID
    LEFT JOIN TVENDOR v ON C.SVENDORID = V.SVENDORID
    WHERE  TO_DATE('1/' || TO_CHAR(DREDUCE,'MM/YYYY'),'DD/MM/YYYY') BETWEEN TO_DATE('{0}','DD/MM/YYYY') AND TO_DATE('{1}','DD/MM/YYYY') AND NVL(c.CACTIVE,'Y') = 'Y'
) r
ON r.STOPICID = T.STOPICID 
WHERE T.CACTIVE = '1' 
GROUP BY  t.STOPICNAME
ORDER BY T.STOPICNAME", "1/" + cmbStartMonth.Value + "/" + cmbYear.Value, "1/" + cmbEndMonth.Value + "/" + cmbYear.Value);

        DataTable dt = CommonFunction.Get_Data(sql, sqlquery);

        ReducePointReport report = new ReducePointReport();

        report.DataSource = dt;
        report.Parameters["prYEAR"].Value = "ระหว่าง " + cmbStartMonth.Text + " ถึง " + cmbEndMonth.Text + " ปี " + cmbYear.Text;


        XRChart xrChart = report.FindControl("xrChart1", true) as XRChart;

        XRChart xrChart2 = report.FindControl("xrChart2", true) as XRChart;
        BuildChart(xrChart);

        xrChart2.DataSource = dt;
        xrChart2.Series[0].ArgumentScaleType = ScaleType.Qualitative;
        xrChart2.Series[0].ArgumentDataMember = "STOPICNAME";
        xrChart2.Series[0].ValueDataMembers[0] = "SUMREDUCE";

        report.Name = "สรุปข้อมูลการผิดสัญญา";
        this.ReportViewer1.Report = report;

    }
}