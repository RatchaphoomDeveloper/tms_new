﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="TruckWater_info.aspx.cs"
    Inherits="TruckWater_info" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnLoad="xcpn_Load" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <div>
                    <table width="100%">
                        <tr>
                            <td>
                                ทะเบียนหัว:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtsHeadRegisterNo" runat="server" Width="75px" ClientInstanceName="txtsHeadRegisterNo"
                                    NullText="ทะเบียนหัว">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                ทะเบียนท้าย:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtsTrailerRegisterNo" runat="server" Width="75px" ClientInstanceName="txtsTrailerRegisterNo"
                                    NullText="ทะเบียนท้าย">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                หมายเลขแชชซีย์ (หัว/ท้าย): </td>
                            <td>
                                <dx:ASPxTextBox ID="txtsCHASIS" runat="server" Width="110px" ClientInstanceName="txtsCHASIS" NullText="หมายเลขแชสซีย์">
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                ประเภทรถ: </td>
                            <td>
                                <dx:ASPxComboBox ID="cmbsCarTypeID" runat="Server" Width="100px" TextField="TypeName" ValueField="SCARTYPEID"
                                    DataSourceID="sdsCarType" SelectedIndex="0">
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="sdsCarType" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" SelectCommand="SELECT -1 AS ID,null AS SCARTYPEID, '- ทั้งหมด -' AS TypeName FROM TTRUCKTYPE WHERE ROWNUM <2 UNION SELECT SCARTYPEID AS ID,SCARTYPEID, SCARCATEGORY AS TypeName  FROM TTRUCKTYPE WHERE SCARTYPEID NOT IN ('2','4')">
                                </asp:SqlDataSource>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search">
                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('SEARCH;'); }" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxButton ID="btnClearSearch" ClientInstanceName="btnClearSearch" runat="server" SkinID="_clearsearch">
                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('SEARCH_CANCEL;'); }" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10">
                                &nbsp;</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td bgcolor="#0E4999">
                                <img src="images/spacer.GIF" width="250px" height="1px"></td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td>
                                <dx:ASPxGridView ID="gvwTruck" runat="server" ClientInstanceName="gvwTruck" SkinID="_gvw" Width="100%"
                                    Style="margin-top: 0px" AutoGenerateColumns="False" KeyFieldName="STRUCKID">
                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="LDUPDATE" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="วันที่อัพเดทล่าสุด">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy HH:mm">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนหัว">
                                            <DataItemTemplate>
                                                <dx:ASPxHyperLink ID="lbkViewHRegNo" ClientInstanceName="lbkViewHRegNo" runat="server" CausesValidation="False"
                                                    AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false" EnableTheming="false" CssClass="dxeLineBreakFix"
                                                    Text='<%# ""+Eval("SHEADREGISTERNO") %>'>
                                                    <ClientSideEvents Click="function (s, e) { gvwTruck.PerformCallback('VIEW;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                </dx:ASPxHyperLink>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนท้าย">
                                            <DataItemTemplate>
                                                <dx:ASPxHyperLink ID="lbkViewTRegNo" ClientInstanceName="lbkViewTRegNo" runat="server" CausesValidation="False"
                                                    AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false" EnableTheming="false" CssClass="dxeLineBreakFix"
                                                    Text='<%# ""+Eval("STRAILERREGISTERNO") %>'>
                                                    <ClientSideEvents Click="function (s, e) { gvwTruck.PerformCallback('VIEW;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                </dx:ASPxHyperLink>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCHASIS" Caption="หมายเลขแชสซีย์(หัว)">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="RCHASIS" Caption="หมายเลขแชสซีย์(ท้าย)">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCARCATEGORY" Caption="ประเภทรถ">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NTOTALCAPACITY" Caption="ปริมาตร">
                                            <PropertiesTextEdit DisplayFormatString="N0">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา">
                                            <DataItemTemplate>
                                                <dx:ASPxHyperLink ID="lbkViewContract" ClientInstanceName="lbkViewContract" runat="server" CausesValidation="False"
                                                    AutoPostBack="false" Cursor="pointer" EnableDefaultAppearance="false" EnableTheming="false" CssClass="dxeLineBreakFix"
                                                    Text='<%# ""+Eval("SCONTRACTNO") %>'>
                                                    <ClientSideEvents Click="function (s, e) { gvwTruck.PerformCallback('VIEWCONTRACT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                </dx:ASPxHyperLink>
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="สถานะ">
                                            <DataItemTemplate>
                                                <dx:ASPxLabel Text='<%# (Eval("CACTIVE").ToString().Trim()=="Y" ? "<font color=Green >ใช้งาน</font>" : (Eval("CACTIVE").ToString().Trim()=="N" ? "<font color=Orange >ระงับ</font>" : "<font color=Red >ยกเลิก</font>")) %>'
                                                    EncodeHtml="false" ID="xlbCATIVE" runat="Server" />
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="การจัดการ">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="btnEdit" runat="server" SkinID="_edit" Width="50px" CausesValidation="False" AutoPostBack="false">
                                                    <ClientSideEvents Click="function (s, e) { gvwTruck.PerformCallback('EDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center" Cursor="hand">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="RTRUCKID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="SCARTYPEID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="SVENDORID" Visible="false" />
                                    </Columns>
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="sdsTruck" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                    CacheKeyDependency="ckdTruck" SelectCommand="SELECT CASE WHEN T.DCREATE!=T.DUPDATE THEN 1 ELSE 0 END LDUPDATE,T.STRUCKID,T.SHEADREGISTERNO,T.SCHASIS,TR.STRUCKID RTRUCKID,TR.SHEADREGISTERNO STRAILERREGISTERNO,TR.SCHASIS RCHASIS,NVL(T.SCARTYPEID,TR.SCARTYPEID-1) SCARTYPEID,TT.SCARCATEGORY,NVL(T.CACTIVE,'Y') AS CACTIVE
,CASE WHEN T.SCARTYPEID='1' THEN NVL(T.NTOTALCAPACITY,0) + NVL(TR.NTOTALCAPACITY,0) WHEN T.SCARTYPEID='3' THEN TR.NTOTALCAPACITY ELSE T.NTOTALCAPACITY END AS NTOTALCAPACITY 
,T.DUPDATE,C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO 
FROM TTRUCK T 
FULL OUTER JOIN TTRUCK TR ON T.STRAILERID=TR.STRUCKID OR TR.SHEADID=T.STRUCKID 
LEFT JOIN TTRUCKTYPE TT ON T.SCARTYPEID=TT.SCARTYPEID 
LEFT JOIN TCONTRACT_TRUCK CT ON T.STRUCKID=CT.STRUCKID AND TR.STRUCKID=CT.STRAILERID 
LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID 
WHERE (T.SCARTYPEID NOT IN (2,4) OR TR.SCARTYPEID IN (2,4)) 
AND NVL(T.SHEADREGISTERNO,'$') LIKE '%'|| NVL(:sHRegNo,NVL(T.SHEADREGISTERNO,'$')) ||'%' 
AND NVL(TR.SHEADREGISTERNO,'$') LIKE '%'|| NVL(:sTRegNo,NVL(TR.SHEADREGISTERNO,'$')) ||'%' 
AND (NVL(T.SCHASIS,'$') LIKE '%'|| NVL(:sCHASISNo,NVL(T.SCHASIS,'$')) ||'%' OR NVL(TR.SCHASIS,'$') LIKE '%'|| NVL(:sCHASISNo,NVL(TR.SCHASIS,'$')) ||'%') 
AND (NVL(T.SCARTYPEID,'-1') LIKE '%'|| NVL(:sCarTypeNo,NVL(T.SCARTYPEID,'-1')) ||'%' OR NVL(TR.SCARTYPEID,'-1') LIKE '%'|| NVL(:sCarTypeNo+1,NVL(T.SCARTYPEID,'-1')) ||'%')
ORDER BY T.SCARTYPEID">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="txtsHeadRegisterNo" Name="sHRegNo" PropertyName="Text" />
                                        <asp:ControlParameter ControlID="txtsTrailerRegisterNo" Name="sTRegNo" PropertyName="Text" />
                                        <asp:ControlParameter ControlID="txtsCHASIS" Name="sCHASISNo" PropertyName="Text" />
                                        <asp:ControlParameter ControlID="cmbsCarTypeID" Name="sCarTypeNo" PropertyName="Value" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
