﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_committe_lst.aspx.cs" Inherits="admin_committe_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="75%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" NullText="กรุณาป้อนข้อมูลที่ต้องการค้นหา"
                                Style="margin-left: 0px" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="right">
                            ค้นหาจาก&nbsp;&nbsp; </td>
                        <td width="8%" class="style13">
                            <dx:ASPxComboBox ID="cbxGroup" runat="server" Width="90px" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="คณะกรรมการ" Value="0" />
                                    <dx:ListEditItem Text="ปี" Value="1" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp; </td>
                        <td colspan="2" align="right">
                            &nbsp; </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr style="height: 1px;">
                        <td bgcolor="#0E4999">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="SSENTENCERCODE"
                                SkinID="_gvw" DataSourceID="sds" SettingsPager-PageSize="10">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ปี" VisibleIndex="1" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ชื่อคณะกรรมการ" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                        Width="30%" FieldName="SSENTENCERNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ประเภท" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                        Width="30%" FieldName="SENTENCER_TYPE_NAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ดำรงตำแหน่ง" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        Width="20%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="จำนวน<br>กรรมการ" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="NAPPROVE" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SDTERM" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lblddd" runat="server" Text='<%# Eval("CACTIVE").ToString()== "1"?"หมดวาระ":"อยู่ในวาระ" %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="7">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn FieldName="SSENTENCERCODE" Visible="False" />
                                    <dx:GridViewDataColumn FieldName="DBEGINTERM" Visible="false" />
                                    <dx:GridViewDataColumn FieldName="DENDTERM" Visible="false" />
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser" OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" 
                                SelectCommand="SELECT TSENTENCER.SSENTENCERCODE,SSENTENCERNAME,DBEGINTERM,DENDTERM,NAPPROVE,CEXPIRETERM CACTIVE
      , M_SENTENCER_TYPE.SENTENCER_TYPE_NAME
 FROM TSENTENCER
 LEFT JOIN (
     SELECT SSENTENCERCODE,COUNT(SSENTENCERCODE) NAPPROVE FROM TSENTENCER GROUP BY SSENTENCERCODE
 )TSENTENCER_GRP ON TSENTENCER.SSENTENCERCODE=TSENTENCER_GRP.SSENTENCERCODE
 LEFT JOIN M_SENTENCER_TYPE ON TSENTENCER.SENTENCER_TYPE_ID = M_SENTENCER_TYPE.SENTENCER_TYPE_ID
 WHERE 1=1 
 AND  NVL(TSENTENCER.SSENTENCERCODE,'')||''||NVL(SSENTENCERNAME,'') LIKE '%'||NVL(:S_KEYWORD, NVL(TSENTENCER.SSENTENCERCODE,'')||''||NVL(SSENTENCERNAME,'')  )||'%'
 GROUP BY TSENTENCER.SSENTENCERCODE,SSENTENCERNAME,DBEGINTERM,DENDTERM,NAPPROVE,CEXPIRETERM, M_SENTENCER_TYPE.SENTENCER_TYPE_NAME
 " DeleteCommand="DELETE FROM TSENTENCER WHERE (SSENTENCERCODE = :S_SENTENCERCODE)"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                <DeleteParameters>
                                    <asp:SessionParameter Name="S_SENTENCERCODE" SessionField="ss_DelSSENTENCERCODE"
                                        Type="String" />
                                </DeleteParameters>
                                <SelectParameters>
                                    <asp:ControlParameter Name="S_KEYWORD" ControlID="txtSearch"  PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp; </td>
                        <td align="right">
                            &nbsp; </td>
                        <td align="right">
                            &nbsp; </td>
                        <td align="right">
                            &nbsp; </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
