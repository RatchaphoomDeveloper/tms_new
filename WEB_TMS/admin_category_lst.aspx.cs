﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class admin_category_lst : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();
            Session["oSCATEGORYID"] = null;

            LogUser("33", "R", "เปิดดูข้อมูลหน้า Category", "");
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnDel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

        switch (cbxGroup.SelectedIndex)
        {
            case 0:
                sds.SelectCommand = "SELECT  ROW_NUMBER () OVER (ORDER BY C.SCATEGORYID) AS ID1, C.SCATEGORYID, C.SDEFINE, C.NIMPACTLEVEL, C.SIMPACTLEVEL, C.SCATEGORYNAME,(SELECT SCATEGORYTYPENAME FROM TCATEGORYTYPE WHERE SCATEGORYTYPEID = CT.SHEADTYPEID ) AS CATEHEAD,CT. SCATEGORYTYPENAME, C.CACTIVE FROM TCATEGORY C  INNER JOIN  TCATEGORYTYPE CT ON C.SCATEGORYTYPEID = CT.SCATEGORYTYPEID WHERE C.SDEFINE LIKE '%' || :oSearch || '%'";
                break;
            case 1:
                sds.SelectCommand = "SELECT  ROW_NUMBER () OVER (ORDER BY C.SCATEGORYID) AS ID1, C.SCATEGORYID, C.SDEFINE, C.NIMPACTLEVEL, C.SIMPACTLEVEL, C.SCATEGORYNAME,(SELECT SCATEGORYTYPENAME FROM TCATEGORYTYPE WHERE SCATEGORYTYPEID = CT.SHEADTYPEID ) AS CATEHEAD,CT. SCATEGORYTYPENAME, C.CACTIVE FROM TCATEGORY C  INNER JOIN  TCATEGORYTYPE CT ON C.SCATEGORYTYPEID = CT.SCATEGORYTYPEID WHERE CT.SCATEGORYTYPENAME LIKE '%' || :oSearch || '%'";
                break;
        }
        sds.DataBind();
        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("ID1", "SCATEGORYID")
                    .Cast<object[]>()
                    .Select(s => new { ID1 = s[0].ToString(), SCATEGORYID = s[1].ToString() });
                string delid = "";
                foreach (var l in ld)
                {
                    
                    Session["delCategory"] = l.SCATEGORYID;
                    sds.Delete();

                    delid += l.SCATEGORYID + ",";
                }

                LogUser("33", "D", "ลบข้อมูลหน้า Category รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SCATEGORYID");
                string stmID = Convert.ToString(data);

                Session["oSCATEGORYID"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_category_add.aspx";

                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;

        if (!CanWrite)
        {
            imbedit.Enabled = false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_category_add.aspx");

    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}
