﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportGrade.aspx.cs" Inherits="ReportGrade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
<script type="text/javascript" src="Javascript/DevExpress/DevExpress.js" > </script>
    <table width="100%" cellspacing="2px" cellpadding="2px">
        <tr>
            <td width="20%">
             &nbsp;
            </td>
            <td width="30%">
            </td>
            <td width="50%"></td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                ชื่อผู้ประกอบการ :
            </td>
            <td>
                <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                    EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SVENDORID" Width="180px" nulltext="-- ทั้งหมด --">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus" ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){hideVendor.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORNAME'));};}" />
                    <Columns>
                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                    </Columns>
                </dx:ASPxComboBox>
                <dx:ASPxTextBox ID="hideVendor" ClientInstanceName="hideVendor" runat="server" Width="10px"
                    ClientVisible="false">
                </dx:ASPxTextBox>
                <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td width="20%" style="background-color: #D3D3D3">
                 ปี :
            </td>
            <td>
                <dx:ASPxComboBox ID="cmbYear" runat="server" Width="100px" SelectedIndex="0">
                </dx:ASPxComboBox>
            </td>
            <td>
               
            </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
              &nbsp;
            </td>
            <td><div style="float: left">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_search" OnClick="btnSubmit_Click">
                    </dx:ASPxButton>
                </div>
                <div style="float:left">&nbsp;</div>
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" SkinID="_close">
                        <ClientSideEvents Click="function(s,e){ window.location = 'Report.aspx'}" />
                    </dx:ASPxButton>
                </div></td>
            <td>
                
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <dx:ReportToolbar ID="ReportToolbar1" runat="server" ReportViewerID="ReportViewer1"
                    ShowDefaultButtons="False" Width="80%" >
                    <Items>
                        <dx:ReportToolbarButton ItemKind="Search" />
                        <dx:ReportToolbarSeparator />
                        <dx:ReportToolbarButton ItemKind="PrintReport" />
                        <dx:ReportToolbarButton ItemKind="PrintPage" />
                        <dx:ReportToolbarSeparator />
                        <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                        <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                        <dx:ReportToolbarLabel ItemKind="PageLabel" />
                        <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                        </dx:ReportToolbarComboBox>
                        <dx:ReportToolbarLabel ItemKind="OfLabel" />
                        <dx:ReportToolbarTextBox ItemKind="PageCount"  />
                        <dx:ReportToolbarButton ItemKind="NextPage" />
                        <dx:ReportToolbarButton ItemKind="LastPage" />
                        <dx:ReportToolbarSeparator />
                        <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                        <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                        <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                            <Elements>
                                <dx:ListElement Value="pdf" />
                                <dx:ListElement Value="xls" />
                                <dx:ListElement Value="xlsx" />
                                <dx:ListElement Value="rtf" />
                                <dx:ListElement Value="mht" />
                                <dx:ListElement Value="html" />
                                <dx:ListElement Value="txt" />
                                <dx:ListElement Value="csv" />
                                <dx:ListElement Value="png" />
                            </Elements>
                        </dx:ReportToolbarComboBox>
                    </Items>
                    <Styles>
                        <LabelStyle>
                            <Margins MarginLeft="3px" MarginRight="3px" />
                        </LabelStyle>
                    </Styles>
                </dx:ReportToolbar>
            </td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <dx:ReportViewer ID="ReportViewer1" runat="server">
                </dx:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
