﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Configuration;

public partial class truck_add_p4 : System.Web.UI.Page
{
    string cSAP_SYNC = WebConfigurationManager.AppSettings["SAP_SYNCOUT"].ToString();
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string UploadDirectory = "UploadFile/Truck/";
    //private static string REQ_ID = "";
    //private static string TTRUCK_INSUREDOC = "";
    //private static string TTRUCK_DOC = "";
    //private static string TTRUCK_MWATERDOC = "";

    private string REQ_ID { get { return Session["REQ_ID"] + ""; } set { Session["REQ_ID"] = value; } }
    private string TTRUCK_INSUREDOC { get { return Session["TTRUCK_INSUREDOC"] + ""; } set { Session["TTRUCK_INSUREDOC"] = value; } }
    private string TTRUCK_DOC { get { return Session["TTRUCK_DOC"] + ""; } set { Session["TTRUCK_DOC"] = value; } }
    private string TTRUCK_MWATERDOC { get { return Session["TTRUCK_MWATERDOC"] + ""; } set { Session["TTRUCK_MWATERDOC"] = value; } }
    string SMENUID = "60";
    private void ClearSessionStatic()
    {
        Session["REQ_ID"] = "";
        Session["TTRUCK_INSUREDOC"] = "";
        Session["TTRUCK_DOC"] = "";
        Session["TTRUCK_MWATERDOC"] = "";
    }

    private void NewLstStatic()
    {
        REQ_ID = "";
        TTRUCK_INSUREDOC = "";
        TTRUCK_DOC = "";
        TTRUCK_MWATERDOC = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        for (int i = 0; i < Session.Count; i++)
        {
            var crntSession = Session.Keys[i];
            Response.Write(string.Concat(crntSession, "=", Session[crntSession]) + "<br />");
        }
        #region Event
        gvwHCompart.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwHCompart_AfterPerformCallback);

        //gvwSTRUCKDoc1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc1_CustomColumnDisplayText);
        gvwSTRUCKDoc1.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc1_AfterPerformCallback);

        //gvwSTRUCKDoc2.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc2_CustomColumnDisplayText);
        gvwSTRUCKDoc2.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc2_AfterPerformCallback);

        //gvwSTRUCKDoc3.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc3_CustomColumnDisplayText);
        gvwSTRUCKDoc3.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc3_AfterPerformCallback);

        //gvwSTRUCKDoc4.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc4_CustomColumnDisplayText);
        gvwSTRUCKDoc4.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc4_AfterPerformCallback);

        //gvwSTRUCKDoc5.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc5_CustomColumnDisplayText);
        gvwSTRUCKDoc5.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc5_AfterPerformCallback);

        //gvwSTRUCKSDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKSDoc_CustomColumnDisplayText);
        gvwSTRUCKSDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKSDoc_AfterPerformCallback);

        //gvwSTRUCKIDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKIDoc_CustomColumnDisplayText);
        gvwSTRUCKIDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKIDoc_AfterPerformCallback);

        gvwRCompart.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRCompart_AfterPerformCallback);

        //gvwRTRUCKDoc1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc1_CustomColumnDisplayText);
        gvwRTRUCKDoc1.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc1_AfterPerformCallback);

        //gvwRTRUCKDoc2.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc2_CustomColumnDisplayText);
        gvwRTRUCKDoc2.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc2_AfterPerformCallback);

        //gvwRTRUCKDoc3.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc3_CustomColumnDisplayText);
        gvwRTRUCKDoc3.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc3_AfterPerformCallback);

        //gvwRTRUCKDoc4.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc4_CustomColumnDisplayText);
        gvwRTRUCKDoc4.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc4_AfterPerformCallback);

        //gvwRTRUCKDoc5.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc5_CustomColumnDisplayText);
        gvwRTRUCKDoc5.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc5_AfterPerformCallback);

        //gvwRTRUCKSDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKSDoc_CustomColumnDisplayText);
        gvwRTRUCKSDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKSDoc_AfterPerformCallback);

        //gvwRTRUCKIDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKIDoc_CustomColumnDisplayText);
        gvwRTRUCKIDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKIDoc_AfterPerformCallback);

        gvwSTRUCKMWaterDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKMWaterDoc_AfterPerformCallback);
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }

        if (!IsPostBack)
        {
            ClearSessionStatic();
            NewLstStatic();

            if (!Permissions("60"))
            { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }

            string str = Request.QueryString["str"];
            cmbHRoute.Value = TMSConstant.DefaultSAPRoute;
            cmbRRoute.Value = TMSConstant.DefaultSAPRoute;
            #region BindData
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = STCrypt.DecryptURL(str);
                ClearSession();
                SetRoundPanel("" + cmbCarType.Value);
                LogUser(SMENUID, "R", "เปิดดูข้อมูลหน้า ส่งคำร้องขอเพิ่มข้อมูลรถ", "");
                BindData("SET_DATA_CONTROL");
                //ChangeMode("" + QueryString[0]);
            }

            #endregion
        }
        TTRUCK_DOC = SystemFunction.QUERY_TTRUCK_DOC();
        TTRUCK_INSUREDOC = SystemFunction.QUERY_TTRUCK_INSUREDOC();
        TTRUCK_MWATERDOC = SystemFunction.QUERY_TTRUCK_MWATERDOC();
    }

    #region CB Panel
    protected void xcpnMain_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "FullData":
                string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("TRUCK&" + txtGlobal_STRUCKID.Text.Trim() + "&" + txtGlobal_RTRUCKID.Text.Trim()))
                        , sUrl = "Truck_History_Info.aspx?str=";
                xcpnMain.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                break;
            case "Save":
                if (cmbCarType.Value == "0" && Session["SHCompart"] != null && gvwHCompart.VisibleRowCount > 0)
                {
                    LogUser(SMENUID, "I", "บันทึกส่งคำร้องขอเพิ่มข้อมูลรถ", "" + txtGlobal_STRUCKID.Text);
                    SaveData();
                }
                else if (cmbCarType.Value == "3" && Session["SRCompart"] != null && gvwRCompart.VisibleRowCount > 0)
                {
                    LogUser(SMENUID, "I", "บันทึกส่งคำร้องขอเพิ่มข้อมูลรถ", "" + txtGlobal_STRUCKID.Text);
                    SaveData();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('Warning!','ไม่พบข้อมูลความจุ');");
                }
                break;
        }
    }
    protected void xcpnHead_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "Calc":
                DataTable dtCompart = (DataTable)Session["SHCompart"];
                xlbHnSlot.Text = dtCompart.Select("CDEL='0'").Length + "";
                int nTotalCapDel = 0;
                foreach (DataRow dr in dtCompart.Select("CDEL='0'"))
                {
                    DataTable dtComp = new DataTable();
                    dtComp.Columns.Add("Capacity", typeof(int));
                    for (int i = 1; i <= 3; i++)
                    {
                        if (dr["NPANLEVEL" + i] + "" != "")
                            dtComp.Rows.Add(new object[] { dr["NPANLEVEL" + i] });
                    }
                    nTotalCapDel += int.Parse(dtComp.Compute("Max(Capacity)", string.Empty) + "");
                }
                xlbHnTatolCapacity.Text = string.Format("{0:n0}", nTotalCapDel);
                if (nTotalCapDel + "" != "" && cmbHProdGRP.Value + "" != "")
                {
                    Double HnWeight = (txtHnWeight.Text != "" ? Double.Parse(txtHnWeight.Text) : 0.00)
                            , nProdWeight = Double.Parse(nTotalCapDel + "") * Double.Parse(cmbHProdGRP.Value + "");
                    xlbHnLoadWeight.Text = string.Format("{0:n0}", nProdWeight);
                    xlbHCalcWeight.Text = string.Format("{0:n0}", (HnWeight + nProdWeight));
                }
                break;
        }
    }
    protected void xcpnTrail_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "Calc":
                DataTable dtCompart = (DataTable)Session["SRCompart"];
                xlbRnSlot.Text = dtCompart.Select("CDEL='0'").Length + "";
                int nTotalCapDel = 0;
                foreach (DataRow dr in dtCompart.Select("CDEL='0'"))
                {
                    DataTable dtComp = new DataTable();
                    dtComp.Columns.Add("Capacity", typeof(int));
                    for (int i = 1; i <= 3; i++)
                    {
                        if (dr["NPANLEVEL" + i] + "" != "")
                            dtComp.Rows.Add(new object[] { dr["NPANLEVEL" + i] });
                    }
                    nTotalCapDel += int.Parse(dtComp.Compute("Max(Capacity)", string.Empty) + "");
                }
                xlbRnTatolCapacity.Text = string.Format("{0:n0}", nTotalCapDel);
                if (nTotalCapDel + "" != "" && cmbRProdGRP.Value + "" != "")
                {
                    Double HnWeight = (paras[1] != "" ? Double.Parse(paras[1]) : 0.00)
                            , RnWeight = (txtRnWeight.Text != "" ? Double.Parse(txtRnWeight.Text) : 0.00)
                            , nProdWeight = Double.Parse(nTotalCapDel + "") * Double.Parse(cmbRProdGRP.Value + "");
                    xlbRnLoadWeight.Text = string.Format("{0:n0}", nProdWeight);
                    xlbRCalcWeight.Text = string.Format("{0:n0}", (HnWeight + RnWeight + nProdWeight));
                }
                break;
        }
    }
    protected void xcpnMWater_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "AddWater":
                break;
            case "ViewWater":
                break;
        }
    }
    protected void xcpnPermit_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "Permit":
                break;
        }
    }
    #endregion

    #region Combobox
    protected void cmbCarcate_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch ("" + ((ASPxComboBox)sender).Value)
        {
            case "02":
                SetValidation(true);
                break;
            default:
                SetValidation(false);
                break;
        }

        BindData("SET_DATA_CONTROL");
    }
    protected void cmbCarType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetRoundPanel("" + ((ASPxComboBox)sender).Value);
        BindData("SET_DATA_CONTROL");
    }

    protected void cmbHsHolder_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbHsHolder_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORID||NVL(v.SVENDORNAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    protected void cmbHProdGRP_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbHProdGRP_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT PROD_ID,PROD_NAME,PROD_CATEGORY,NVL(DENSITY,0) AS DENSITY FROM (SELECT ROW_NUMBER()OVER(ORDER BY PROS.PROD_GRP) AS RN , PROS.PROD_ID,PROS.PROD_NAME,PRO.PROD_CATEGORY,PRO.DENSITY FROM TPRODUCT PRO
LEFT JOIN TPRODUCT_SAP PROS ON PRO.PROD_ID=PROS.PROD_ID WHERE PROS.PROD_ID||NVL(PROS.PROD_GRP_NAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }

    protected void cmbRsHolder_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbRsHolder_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORID||NVL(v.SVENDORNAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    protected void cmbRProdGRP_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbRProdGRP_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT PROD_ID,PROD_NAME,PROD_CATEGORY,NVL(DENSITY,0) AS DENSITY FROM (SELECT ROW_NUMBER()OVER(ORDER BY PROS.PROD_GRP) AS RN , PROS.PROD_ID,PROS.PROD_NAME,PRO.PROD_CATEGORY,PRO.DENSITY FROM TPRODUCT PRO
LEFT JOIN TPRODUCT_SAP PROS ON PRO.PROD_ID=PROS.PROD_ID WHERE PROS.PROD_ID||NVL(PROS.PROD_GRP_NAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    #endregion

    #region GridView
    protected void gvwHCompart_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        DataTable dtCompart;
        DataView dvCompart;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
                DataTable dtCompartSet = (DataTable)Session["SHCompart"];
                gvwHCompart.DataSource = dtCompartSet;
                gvwHCompart.DataBind();
                gvwHCompart.CancelEdit();
                break;
            case "CANCELEDIT":
                gvwHCompart.CancelEdit();
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwHCompart.StartEdit(visibleindex);
                dynamic dyData = gvwHCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox txtNCOMPARTNO = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox txtNPANLEVEL1 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox txtNPANLEVEL2 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox txtNPANLEVEL3 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL3");
                txtNCOMPARTNO.Text = "" + dyData[0];
                txtNPANLEVEL1.Text = "" + dyData[1];
                txtNPANLEVEL2.Text = "" + dyData[2];
                txtNPANLEVEL3.Text = "" + dyData[3];
                #endregion
                break;
            case "NEWCOMPART":
                gvwHCompart.AddNewRow();
                break;
            case "DELCOMPART":
                dynamic dyDataDel = gvwHCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                dtCompart = PrepareDataTable("SHCompart", "", "" + dyDataDel[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtCompart.Select("NCOMPARTNO='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCompart.Rows.IndexOf(_drDataDel);
                        if (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCompart.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCompart.Rows[idx_drTemp].BeginEdit();
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "1";
                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["SHCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL=0";
                gvwHCompart.DataSource = dvCompart.ToTable();
                gvwHCompart.DataBind();
                gvwHCompart.JSProperties["cpCalc"] = "Calc";
                break;
            case "SAVECOMPART":
                dynamic dyDataSubmit = gvwHCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                string Compart_mode = (visibleindex == -1) ? "Add" : "Edit";
                dtCompart = PrepareDataTable("SHCompart", "", "" + dyDataSubmit[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox save_txtNCOMPARTNO = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox save_txtNPANLEVEL1 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox save_txtNPANLEVEL2 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox save_txtNPANLEVEL3 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL3");

                switch (Compart_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */
                        if (dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "' AND CDEL='1'").Length > 0)
                        {
                            DataRow[] drData = dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "'");

                            if (drData.Length > 0)
                            {
                                int idx_drTemp = dtCompart.Rows.IndexOf(drData[0]);
                                dtCompart.Rows[idx_drTemp].BeginEdit();

                                dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                                dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                                dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                                dtCompart.Rows[idx_drTemp].EndEdit();
                            }

                        }
                        else
                        {
                            if (dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "'").Length > 0)
                            {
                                DataRow[] drData = dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "'");

                                if (drData.Length > 0)
                                {
                                    int idx_drTemp = dtCompart.Rows.IndexOf(drData[0]);
                                    dtCompart.Rows[idx_drTemp].BeginEdit();

                                    dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                    dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                    dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                    dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                                    dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                                    dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                                    dtCompart.Rows[idx_drTemp].EndEdit();
                                }
                            }
                            else
                            {
                                DataRow drNewRow = dtCompart.NewRow();
                                drNewRow["NCOMPARTNO"] = "" + save_txtNCOMPARTNO.Text;
                                drNewRow["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                drNewRow["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                drNewRow["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                drNewRow["CNEW"] = "1";
                                drNewRow["CCHANGE"] = "0";
                                drNewRow["CDEL"] = "0";
                                dtCompart.Rows.Add(drNewRow);
                            }
                        }
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        DataRow[] _drData = dtCompart.Select("NCOMPARTNO='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtCompart.Rows.IndexOf(_drData[0]);
                            dtCompart.Rows[idx_drTemp].BeginEdit();

                            dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                            dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                            dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                        #endregion
                        break;

                }
                Session["SHCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL=0";
                gvwHCompart.CancelEdit();
                gvwHCompart.DataSource = dvCompart.ToTable();
                gvwHCompart.DataBind();
                gvwHCompart.JSProperties["cpCalc"] = "Calc";
                break;
        }
    }

    protected void gvwSTRUCKDoc1_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");

        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc1.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC1");
                break;
            case "BIND_TRUCK_HINFODOC1":
                BindData("BIND_TRUCK_HINFODOC1");
                break;
        }
    }

    protected void gvwSTRUCKDoc2_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc2_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc2.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC2");
                break;
            case "BIND_TRUCK_HINFODOC2":
                BindData("BIND_TRUCK_HINFODOC2");
                break;
        }
    }

    protected void gvwSTRUCKDoc3_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc3_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc3.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC3");
                break;
            case "BIND_TRUCK_HINFODOC3":
                BindData("BIND_TRUCK_HINFODOC3");
                break;
        }
    }

    protected void gvwSTRUCKDoc4_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc4_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc4.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC4");
                break;
            case "BIND_TRUCK_HINFODOC4":
                BindData("BIND_TRUCK_HINFODOC4");
                break;
        }
    }

    protected void gvwSTRUCKDoc5_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc5_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc5.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC5");
                break;
            case "BIND_TRUCK_HINFODOC5":
                BindData("BIND_TRUCK_HINFODOC5");
                break;
        }
    }

    protected void gvwSTRUCKSDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKSDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKSDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                BindData("BIND_TRUCK_HSTATUTEDOC");
                break;
            case "BIND_TRUCK_HSTATUTEDOC":
                BindData("BIND_TRUCK_HSTATUTEDOC");
                break;
        }
    }

    protected void gvwSTRUCKIDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKIDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKIDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINSUREDOC");
                break;
            case "BIND_TRUCK_HINSUREDOC":
                BindData("BIND_TRUCK_HINSUREDOC");
                break;
        }
    }

    protected void gvwRCompart_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        DataTable dtCompart;
        DataView dvCompart;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
                DataTable dtCompartSet = (DataTable)Session["SRCompart"];
                gvwRCompart.DataSource = dtCompartSet;
                gvwRCompart.DataBind();
                gvwRCompart.CancelEdit();
                break;
            case "CANCELEDIT":
                gvwRCompart.CancelEdit();
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwRCompart.StartEdit(visibleindex);
                dynamic dyData = gvwRCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox txtNCOMPARTNO = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox txtNPANLEVEL1 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox txtNPANLEVEL2 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox txtNPANLEVEL3 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL3");
                txtNCOMPARTNO.Text = "" + dyData[0];
                txtNPANLEVEL1.Text = "" + dyData[1];
                txtNPANLEVEL2.Text = "" + dyData[2];
                txtNPANLEVEL3.Text = "" + dyData[3];
                #endregion
                break;
            case "NEWCOMPART":
                gvwRCompart.AddNewRow();
                break;
            case "DELCOMPART":
                dynamic dyDataDel = gvwRCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                dtCompart = PrepareDataTable("SRCompart", "", "" + dyDataDel[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtCompart.Select("NCOMPARTNO='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCompart.Rows.IndexOf(_drDataDel);
                        if (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCompart.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCompart.Rows[idx_drTemp].BeginEdit();
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "1";
                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["SRCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL =0";
                gvwRCompart.DataSource = dvCompart.ToTable();
                gvwRCompart.DataBind();
                gvwRCompart.JSProperties["cpCalc"] = "Calc";
                break;
            case "SAVECOMPART":
                dynamic dyDataSubmit = gvwRCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                string Compart_mode = (visibleindex == -1) ? "Add" : "Edit";
                dtCompart = PrepareDataTable("SRCompart", "", "" + dyDataSubmit[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox save_txtNCOMPARTNO = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox save_txtNPANLEVEL1 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox save_txtNPANLEVEL2 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox save_txtNPANLEVEL3 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL3");

                switch (Compart_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */
                        if (dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "' AND CDEL='1'").Length > 0)
                        {
                            DataRow[] drData = dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "'");

                            if (drData.Length > 0)
                            {
                                int idx_drTemp = dtCompart.Rows.IndexOf(drData[0]);
                                dtCompart.Rows[idx_drTemp].BeginEdit();

                                dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                                dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                                dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                                dtCompart.Rows[idx_drTemp].EndEdit();
                            }

                        }
                        else
                        {

                            if (dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "' ").Length > 0)
                            {
                                DataRow[] drData = dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "'");

                                if (drData.Length > 0)
                                {
                                    int idx_drTemp = dtCompart.Rows.IndexOf(drData[0]);
                                    dtCompart.Rows[idx_drTemp].BeginEdit();

                                    dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                    dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                    dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                    dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                                    dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                                    dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                                    dtCompart.Rows[idx_drTemp].EndEdit();
                                }

                            }
                            else
                            {
                                DataRow drNewRow = dtCompart.NewRow();
                                drNewRow["NCOMPARTNO"] = "" + save_txtNCOMPARTNO.Text;
                                drNewRow["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                drNewRow["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                drNewRow["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                drNewRow["CNEW"] = "1";
                                drNewRow["CCHANGE"] = "0";
                                drNewRow["CDEL"] = "0";
                                dtCompart.Rows.Add(drNewRow);
                            }
                        }
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        DataRow[] _drData = dtCompart.Select("NCOMPARTNO='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtCompart.Rows.IndexOf(_drData[0]);
                            dtCompart.Rows[idx_drTemp].BeginEdit();

                            dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                            dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                            dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                        #endregion
                        break;

                }
                Session["SRCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL=0";
                gvwRCompart.CancelEdit();
                gvwRCompart.DataSource = dvCompart.ToTable();
                gvwRCompart.DataBind();
                gvwRCompart.JSProperties["cpCalc"] = "Calc";
                break;
        }
    }

    protected void gvwRTRUCKDoc1_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc1.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC1");
                break;
            case "BIND_TRUCK_RINFODOC1":
                BindData("BIND_TRUCK_RINFODOC1");
                break;
        }
    }

    protected void gvwRTRUCKDoc2_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc2_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc2.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC2");
                break;
            case "BIND_TRUCK_RINFODOC2":
                BindData("BIND_TRUCK_RINFODOC2");
                break;
        }
    }

    protected void gvwRTRUCKDoc3_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc3_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc3.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC3");
                break;
            case "BIND_TRUCK_RINFODOC3":
                BindData("BIND_TRUCK_RINFODOC3");
                break;
        }
    }

    protected void gvwRTRUCKDoc4_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc4_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc4.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC4");
                break;
            case "BIND_TRUCK_RINFODOC4":
                BindData("BIND_TRUCK_RINFODOC4");
                break;
        }
    }

    protected void gvwRTRUCKDoc5_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc5_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc5.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC5");
                break;
            case "BIND_TRUCK_RINFODOC5":
                BindData("BIND_TRUCK_RINFODOC5");
                break;
        }
    }

    protected void gvwRTRUCKSDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKSDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKSDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RSTATUTEDOC");
                break;
            case "BIND_TRUCK_RSTATUTEDOC":
                BindData("BIND_TRUCK_RSTATUTEDOC");
                break;
        }
    }

    protected void gvwRTRUCKIDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKIDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKIDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINSUREDOC");
                break;
            case "BIND_TRUCK_RINSUREDOC":
                BindData("BIND_TRUCK_RINSUREDOC");
                break;
        }
    }

    protected void gvwSTRUCKMWaterDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[9] + "" + dyData[3];
                gvwSTRUCKMWaterDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("MWaterDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_MWATERDOC");
                break;
            case "BIND_TRUCK_MWATERDOC":
                BindData("BIND_TRUCK_MWATERDOC");
                break;
            case "DISPOSE":
                if ("" + eArgs[1] == "SHOW")
                    BindData("BIND_TRUCK_MWATERDOC");
                break;
        }
    }

    #endregion

    #region UploadFiles
    protected void uldHDoc01_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "1";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHDoc02_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "2";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHDoc03_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "3";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHDoc04_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "4";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHDoc05_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = (int)TMSEnum.TruckDocType.Others;
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHSDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/STATUTE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "STATUTE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HISDoc"] = dtTruckDoc;
    }
    protected void uldHIDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INSURANCE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "INSURANCE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HISDoc"] = dtTruckDoc;
    }
    protected void uldRDoc01_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "1";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRDoc02_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "2";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRDoc03_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "3";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRDoc04_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "4";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRDoc05_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = (int)TMSEnum.TruckDocType.Others;
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRSDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/STATUTE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "STATUTE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RISDoc"] = dtTruckDoc;
    }
    protected void uldRIDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INSURANCE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "INSURANCE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RISDoc"] = dtTruckDoc;
    }
    protected void uldMWaterDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_TRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/MWATER/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("MWaterDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["MWaterDoc"] = dtTruckDoc;
    }
    #endregion

    string GenTRUKCID()
    {
        return CommonFunction.Get_Value(conn, "SELECT FC_GENID_TTRUCK(0) FROM DUAL");
    }
    void SaveData()
    {

        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfoRedirect('Session Expire','" + "Please Login try again!',function(){ window.location='default.aspx';});");
            return;
        }

        string IsSuccessed = "", msg = "";
        string STRUCK = "", RTRUCK = "";
        string ScarType = "" + cmbCarType.Value;

        #region Condition Rule
        DataTable _dt;
        /*ตรวจสอบหมายเลข ทะเบียน ซ้ำ*/
        _dt = CommonFunction.Get_Data(conn, string.Format(@"SELECT * FROM (SELECT SHEADREGISTERNO FROM TTRUCK UNION 
SELECT SHEADREGISTERNO FROM TTRUCK_TEMP TT
INNER JOIN TREQ_DATACHANGE TDC
ON TT.REQ_ID = TDC.REQ_ID
WHERE TDC.STATUS IN ('0','4')) WHERE SHEADREGISTERNO LIKE '{0}'", CommonFunction.ReplaceInjection(txtHRegNo.Text)));
        if (_dt.Rows.Count > 0)
        {
            CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('แจ้งเตือน!','ขออภัยไม่สามารถทำการบันทึกข้อมูลรถได้ เนื่องจากท่านได้กรอกทะเบียนรถ(หัว)ซ้ำ กรุณาตรวจสอบ');"); return;
        }
        else if (ScarType == "3")
        {
            //ทะเบียนหัวซ้ำกับทะเบียนหาง
            if (txtHRegNo.Text.Trim() == txtRRegNo.Text.Trim())
            {
                CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('แจ้งเตือน!','ขออภัยไม่สามารถทำการบันทึกข้อมูลรถได้ เนื่องจากท่านได้กรอกทะเบียนรถ(หัว) ซ้ำกับ ทะเบียนรถ(หาง) กรุณาตรวจสอบ');"); return;
            }

            _dt = CommonFunction.Get_Data(conn, string.Format(@"SELECT * FROM  (SELECT SHEADREGISTERNO FROM TTRUCK UNION 
SELECT SHEADREGISTERNO FROM TTRUCK_TEMP TT
INNER JOIN TREQ_DATACHANGE TDC
ON TT.REQ_ID = TDC.REQ_ID
WHERE TDC.STATUS IN ('0','4'))  WHERE SHEADREGISTERNO LIKE '{0}'", CommonFunction.ReplaceInjection(txtRRegNo.Text)));
            if (_dt.Rows.Count > 0)
            {
                CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('แจ้งเตือน!','ขออภัยไม่สามารถทำการบันทึกข้อมูลรถได้ เนื่องจากท่านได้กรอกทะเบียนรถ(หาง)ซ้ำ กรุณาตรวจสอบ');"); return;
            }
        }
        /*ตรวจสอบหมายเลขแชสซีย์ ซ้ำ
        //Vehchicle
        _dt = CommonFunction.Get_Data(conn, string.Format("SELECT * FROM TTRUCK WHERE SCHASIS LIKE '{0}'", CommonFunction.ReplaceInjection(txtHsChasis.Text)));
        if (_dt.Rows.Count > 0)
        {
            CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('แจ้งเตือน!','ขออภัยไม่สามารถทำการบันทึกข้อมูลรถได้ เนื่องจากท่านได้กรอกหมายเลขแชสซีย์(หัว)ซ้ำ กรุณาตรวจสอบ');"); return;
        }
        else if (ScarType == "3")
        {
            //เลขแชสซีย์หัวซ้ำกับหาง
            if (txtHsChasis.Text.Trim() == txtRsChasis.Text.Trim())
            {
                CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('แจ้งเตือน!','ขออภัยไม่สามารถทำการบันทึกข้อมูลรถได้ เนื่องจากท่านได้กรอกหมายเลขแชสซีย์(หัว) ซ้ำกับ หมายเลขแชสซีย์(หาง) กรุณาตรวจสอบ');"); return;
            }

            _dt = CommonFunction.Get_Data(conn, string.Format("SELECT * FROM TTRUCK WHERE SCHASIS LIKE '{0}'", CommonFunction.ReplaceInjection(txtRsChasis.Text)));
            if (_dt.Rows.Count > 0)
            {
                CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('แจ้งเตือน!','ขออภัยไม่สามารถทำการบันทึกข้อมูลรถได้ เนื่องจากท่านได้กรอกหมายเลขแชสซีย์(หาง)ซ้ำ กรุณาตรวจสอบ');"); return;
            }
        }*/
        _dt.Dispose();
        #endregion


        try
        {
            #region ข้อมูลรถหรือข้อมูลรถส่วนหัว

            STRUCK = GenTRUKCID();

            string SREQ_ID = "";
            if (!string.IsNullOrEmpty(REQ_ID))
            {
                SREQ_ID = REQ_ID;
            }
            else
            {
                string SVENDORID = SystemFunction.GET_VENDORID(Session["UserID"] + "");
                string STRUCKREGIS = !string.IsNullOrEmpty(txtRRegNo.Text) ? (txtHRegNo.Text + " - " + txtRRegNo.Text) : txtHRegNo.Text;
                SREQ_ID = SystemFunction.AddToTREQ_DATACHANGE(STRUCK, "T", Session["UserID"] + "", "4", STRUCKREGIS, SVENDORID, "Y");
            }

            #region ข้อมูลรถ
            using (OracleConnection con = new OracleConnection(conn))
            {
                con.Open();
                OracleCommand cmd = new OracleCommand();
                string strSql = @"INSERT INTO TTRUCK_TEMP 
    (STRUCKID, SHEADREGISTERNO, SCARTYPEID, NSLOT, SHEADID, STRAILERID, STRAILERREGISTERNO, STRANSPORTID, STRANSPORTTYPE, SLAST_REQ_ID, SENGINE, SCHASIS, SBRAND, SMODEL, NWHEELS
    , NTOTALCAPACITY, NWEIGHT, NCALC_WEIGHT, NLOAD_WEIGHT, SVIBRATION, STANK_MATERAIL, STANK_MAKER, SLOADING_METHOD, DREGISTER, SPROD_GRP, SBLACKLISTTYPE, DBLACKLIST, DBLACKLIST2
    , BLACKLIST_CAUSE, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE, DSIGNIN, NSHAFTDRIVEN, POWERMOVER, PUMPPOWER, PUMPPOWER_TYPE, MATERIALOFPRESSURE, VALVETYPE, FUELTYPE
    , GPS_SERVICE_PROVIDER, SOWNER_NAME, TRUCK_CATEGORY, VOL_UOM, VEH_TEXT, CARCATE_ID, ROUTE, STERMINALID, DEPOT, TPPOINT, CLASSGRP,REQ_ID)
 VALUES 
    (:STRUCKID, :SHEADREGISTERNO, :SCARTYPEID, :NSLOT, :SHEADID, :STRAILERID, :STRAILERREGISTERNO, :STRANSPORTID, :STRANSPORTTYPE, :SLAST_REQ_ID, :SENGINE, :SCHASIS, :SBRAND, :SMODEL, :NWHEELS
    , :NTOTALCAPACITY, :NWEIGHT, :NCALC_WEIGHT, :NLOAD_WEIGHT, :SVIBRATION, :STANK_MATERAIL, :STANK_MAKER, :SLOADING_METHOD, :DREGISTER, :SPROD_GRP, :SBLACKLISTTYPE, :DBLACKLIST, :DBLACKLIST2
    , :BLACKLIST_CAUSE, :CACTIVE, SYSDATE, :SCREATE, SYSDATE, :SUPDATE, :DSIGNIN, :NSHAFTDRIVEN, :POWERMOVER, :PUMPPOWER, :PUMPPOWER_TYPE, :MATERIALOFPRESSURE, :VALVETYPE, :FUELTYPE
    , :GPS_SERVICE_PROVIDER, :SOWNER_NAME, :TRUCK_CATEGORY, :VOL_UOM, :VEH_TEXT, :CARCATE_ID, :ROUTE, :STERMINALID, :DEPOT, :TPPOINT, :CLASSGRP,:REQ_ID)";

                //string STRANSPORTID = CommonFunction.Get_Value(conn, string.Format("SELECT SVENDORID FROM TVENDOR WHERE SABBREVIATION LIKE '{0}'",
                //                         ("" + cmbHsHolder.Text != "" ? cmbHsHolder.Text : cmbRsHolder.Text)));

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = txtHRegNo.Text;
                    com.Parameters.Add(":SCARTYPEID", OracleType.Number).Value = ScarType;
                    if (xlbHnSlot.Text.Trim() != "" && ScarType != "3")
                        com.Parameters.Add(":NSLOT", OracleType.Number).Value = xlbHnSlot.Text.Trim();
                    else
                        com.Parameters.Add(":NSLOT", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":SHEADID", OracleType.VarChar).Value = OracleString.Null;
                    com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = OracleString.Null;
                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = OracleString.Null;
                    com.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = GET_VENDORID_US("" + Session["UserID"]);
                    //com.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = !string.IsNullOrEmpty(STRANSPORTID) ? STRANSPORTID : GET_VENDORID_US("" + Session["UserID"]);
                    com.Parameters.Add(":STRANSPORTTYPE", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":SLAST_REQ_ID", OracleType.VarChar).Value = OracleString.Null;
                    com.Parameters.Add(":SENGINE", OracleType.VarChar).Value = txtHsEngine.Text;
                    com.Parameters.Add(":SCHASIS", OracleType.VarChar).Value = txtHsChasis.Text;
                    com.Parameters.Add(":SBRAND", OracleType.VarChar).Value = cboHsBrand.Text + "";
                    com.Parameters.Add(":SMODEL", OracleType.VarChar).Value = txtHsModel.Text;
                    if (txtHnWheel.Text.Trim() != "")
                        com.Parameters.Add(":NWHEELS", OracleType.Number).Value = txtHnWheel.Text.Trim();
                    else
                        com.Parameters.Add(":NWHEELS", OracleType.Number).Value = OracleNumber.Null;
                    if (xlbHnTatolCapacity.Text.Trim() != "")
                        com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = xlbHnTatolCapacity.Text.Trim();
                    else
                        com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = OracleNumber.Null;
                    if (txtHnWeight.Text.Trim() != "")
                        com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = txtHnWeight.Text.Trim();
                    else
                        com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = OracleNumber.Null;
                    if (xlbHCalcWeight.Text.Trim() != "")
                        com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = xlbHCalcWeight.Text.Trim();
                    else
                        com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                    if (xlbHnLoadWeight.Text.Trim() != "")
                        com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = xlbHnLoadWeight.Text.Trim();
                    else
                        com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":SVIBRATION", OracleType.VarChar).Value = cboHsVibration.Value + "";
                    com.Parameters.Add(":STANK_MATERAIL", OracleType.VarChar).Value = cboHTankMaterial.Value + "";
                    com.Parameters.Add(":STANK_MAKER", OracleType.VarChar).Value = txtHTank_Maker.Text;
                    com.Parameters.Add(":SLOADING_METHOD", OracleType.VarChar).Value = cboHLoadMethod.Value + "";
                    com.Parameters.Add(":DSIGNIN", OracleType.DateTime).Value = Convert.ToDateTime(xDetdHRegNo.Value);
                    string[] arrProd = cmbHProdGRP.Text.ToString().Split('-');
                    com.Parameters.Add(":SPROD_GRP", OracleType.VarChar).Value = arrProd[0].Trim();
                    com.Parameters.Add(":SBLACKLISTTYPE", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":DBLACKLIST", OracleType.DateTime).Value = OracleDateTime.Null;
                    com.Parameters.Add(":DBLACKLIST2", OracleType.DateTime).Value = OracleDateTime.Null;
                    com.Parameters.Add(":BLACKLIST_CAUSE", OracleType.VarChar).Value = OracleString.Null;
                    com.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = (rblPermit.Value + "" == "" ? "Y" : rblPermit.Value + "");
                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                    com.Parameters.Add(":DREGISTER", OracleType.DateTime).Value = Convert.ToDateTime(xDetdHFRegNo.Value);
                    if (txtHnShaftDriven.Text.Trim() != "")//จำนวนเพลาขับเคลื่อน
                        com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = txtHnShaftDriven.Text.Trim();
                    else
                        com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = OracleNumber.Null;
                    if (txtHPowermover.Text.Trim() != "")//กำลังเครื่องยนต์
                        com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = txtHPowermover.Text.Trim();
                    else
                        com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":PUMPPOWER", OracleType.VarChar).Value = rblHPumpPower.Value + "";
                    com.Parameters.Add(":PUMPPOWER_TYPE", OracleType.VarChar).Value = cboHPumpPower_type.Value + "";
                    com.Parameters.Add(":MATERIALOFPRESSURE", OracleType.VarChar).Value = cboHMaterialOfPressure.Value + "";
                    com.Parameters.Add(":VALVETYPE", OracleType.VarChar).Value = rblHValveType.Value == "Other" ? txtHValveType.Text : rblHValveType.Value + "";
                    com.Parameters.Add(":FUELTYPE", OracleType.VarChar).Value = rblHFuelType.Value == "Other" ? txtHFuelType.Text : rblHFuelType.Value + "";
                    com.Parameters.Add(":GPS_SERVICE_PROVIDER", OracleType.VarChar).Value = cboHGPSProvider.Text + "";
                    com.Parameters.Add(":SOWNER_NAME", OracleType.VarChar).Value = ("" + cmbHsHolder.Text != "" ? cmbHsHolder.Text : cmbRsHolder.Text);
                    com.Parameters.Add(":TRUCK_CATEGORY", OracleType.VarChar).Value = "" + cmbHTru_Cate.Value;
                    com.Parameters.Add(":VOL_UOM", OracleType.VarChar).Value = txtVEH_Volume.Text;
                    com.Parameters.Add(":VEH_TEXT", OracleType.VarChar).Value = txtVEH_Text.Text;
                    com.Parameters.Add(":CARCATE_ID", OracleType.VarChar).Value = "" + cmbCarcate.Value;

                    com.Parameters.Add(":ROUTE", OracleType.Char).Value = "" + cmbHRoute.Value;
                    com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + cmbRDepot.Value;
                    com.Parameters.Add(":DEPOT", OracleType.Char).Value = "" + cmbHDepot.Value;
                    com.Parameters.Add(":TPPOINT", OracleType.Char).Value = "" + cmbHTPPoint.Value;
                    com.Parameters.Add(":CLASSGRP", OracleType.VarChar).Value = "" + cmbHClassGRP.Value;
                    com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;

                    com.ExecuteNonQuery();
                }
            }
            #endregion

            #region ข้อมูลความจุ
            if (ScarType != "3" && Session["SHCompart"] != null)
            {
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    DataTable dt = (DataTable)Session["SHCompart"];
                    //string DEL_COMPART = "Delete  FROM TTRUCK_COMPART_TEMP WHERE STRUCKID = '" + STRUCK + "' AND REQ_ID = '" + SREQ_ID + "'";
                    //AddTODB(DEL_COMPART);

                    string strSql = @"INSERT INTO TTRUCK_COMPART_TEMP(STRUCKID,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE,REQ_ID) Values
                                        (:STRUCKID,:NCOMPARTNO,:NPANLEVEL,:NCAPACITY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:REQ_ID)";

                    int nSlot = 0;
                    foreach (DataRow dr in dt.Select("NCOMPARTNO<>''", "NCOMPARTNO ASC"))
                    {
                        for (int i = 1; i <= 3; i++)
                        {
                            if (dr["NPANLEVEL" + i] + "" != "")
                            {
                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                                    com.Parameters.Add(":NCOMPARTNO", OracleType.Number).Value = dr["NCOMPARTNO"] + "";
                                    com.Parameters.Add(":NPANLEVEL", OracleType.Number).Value = i;
                                    com.Parameters.Add(":NCAPACITY", OracleType.Number).Value = dr["NPANLEVEL" + i] + "";
                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                    com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                        nSlot++;
                    }

                    strSql = string.Format("UPDATE TTRUCK_TEMP SET NSLOT='{1}' WHERE STRUCKID='{0}' AND REQ_ID = '" + CommonFunction.ReplaceInjection(SREQ_ID) + "'", STRUCK, nSlot);
                    SystemFunction.SQLExecuteNonQuery(conn, strSql);
                }
            }
            #endregion

            #region เอกสารข้อมูลรถ
            if (Session["HDoc"] != null)
            {
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    DataTable dt = (DataTable)Session["HDoc"];
                    string dest = UploadDirectory + "" + STRUCK + "/INFO/";
                    foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "DOC_TYPE ASC"))
                    {
                        string GenID = CommonFunction.Gen_ID(con, TTRUCK_DOC);

                        string strSql = @"INSERT INTO TTRUCK_DOC_TEMP(DOCID,STRUCKID,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME,REQ_ID) Values
                                            (:DOCID,:STRUCKID,:DOC_TYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME,:REQ_ID)";
                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                            com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = dr["DOC_TYPE"] + "";
                            com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                            com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                            com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                            com.ExecuteNonQuery();
                        }
                        ServerMoveFile("" + dr["SPATH"], dest, "" + dr["DOC_SYSNAME"]);
                    }
                }
            }
            #endregion

            #region ข้อมูลพรบ.
            using (OracleConnection con = new OracleConnection(conn))
            {
                con.Open();
                string strSql = @"INSERT INTO TTRUCK_INSURANCE_TEMP(STRUCKID,CTYPE,SCOMPANY,START_DURATION,END_DURATION,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE,REQ_ID) Values
                                    (:STRUCKID,:CTYPE,:SCOMPANY,:START_DURATION,:END_DURATION,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE,:REQ_ID)";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                    com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "STATUTE";
                    com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtHSCompany.Text + "";
                    if (xDetHSDuration_Start.Value + "" != "")
                        com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetHSDuration_Start.Value);
                    else
                        com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                    if (xDetHSDuration_End.Value + "" != "")
                        com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetHSDuration_End.Value);
                    else
                        com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                    com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemHSDetail.Text + "";
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                    com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                    com.ExecuteNonQuery();
                }
            }
            #endregion

            #region ข้อมูลประกันภัย.
            using (OracleConnection con = new OracleConnection(conn))
            {
                con.Open();
                string strSql = @"INSERT INTO TTRUCK_INSURANCE_TEMP(STRUCKID,CTYPE,SCOMPANY,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE,REQ_ID) Values
                                        (:STRUCKID,:CTYPE,:SCOMPANY,:INSURE_BUDGET,:INSURE_TYPE,:HOLDING,:NINSURE,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE,:REQ_ID)";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                    com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "INSURANCE";
                    com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtHICompany.Text + "";
                    if (txtHIBudget.Text + "" != "")
                        com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = txtHIBudget.Text + "";
                    else
                        com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":INSURE_TYPE", OracleType.VarChar).Value = cbmHIType.Value + "";
                    string sHolding = "";
                    foreach (ListEditItem items in chkHIHolding.Items)
                    {
                        if (items.Selected)
                            sHolding += "," + items.Value;
                    }
                    if (sHolding.Trim() != "")
                        sHolding = sHolding.Substring(1);
                    com.Parameters.Add(":HOLDING", OracleType.VarChar).Value = sHolding.ToString();
                    if (txtHInInsure.Text + "" != "")
                        com.Parameters.Add(":NINSURE", OracleType.Number).Value = txtHInInsure.Text + "";
                    else
                        com.Parameters.Add(":NINSURE", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemHIDetail.Text + "";
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                    com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                    com.ExecuteNonQuery();
                }
            }
            #endregion

            #region เอกสารข้อมูลพรบ. และประกันภัย
            if (Session["HISDoc"] != null)
            {
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    DataTable dt = (DataTable)Session["HISDoc"];
                    foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "CTYPE ASC"))
                    {
                        string dest = UploadDirectory + "" + STRUCK + "/" + dr["CTYPE"] + "/";
                        string GenID = CommonFunction.Gen_ID(con, TTRUCK_INSUREDOC);

                        string strSql = @"INSERT INTO TTRUCK_INSUREDOC_TEMP(DOCID,STRUCKID,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME,REQ_ID) Values
                                    (:DOCID,:STRUCKID,:CTYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME,:REQ_ID)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                            com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = dr["CTYPE"] + "";
                            com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                            com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                            com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                            com.ExecuteNonQuery();
                            ServerMoveFile("" + dr["SPATH"], dest, "" + dr["DOC_SYSNAME"]);
                        }
                    }
                }
            }
            #endregion

            #endregion

            #region ข้อมูลรถส่วนหาง

            if (ScarType == "3")
            {
                RTRUCK = GenTRUKCID();

                #region ข้อมูลรถ
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    string strSql = @"INSERT INTO TTRUCK_TEMP 
    (STRUCKID, SHEADREGISTERNO, SCARTYPEID, NSLOT, SHEADID, STRAILERID, STRAILERREGISTERNO, STRANSPORTID, STRANSPORTTYPE, SLAST_REQ_ID, SENGINE, SCHASIS, SBRAND, SMODEL, NWHEELS
    , NTOTALCAPACITY, NWEIGHT, NCALC_WEIGHT, NLOAD_WEIGHT, SVIBRATION, STANK_MATERAIL, STANK_MAKER, SLOADING_METHOD, DREGISTER, SPROD_GRP, SBLACKLISTTYPE, DBLACKLIST, DBLACKLIST2
    , BLACKLIST_CAUSE, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE, DSIGNIN, NSHAFTDRIVEN, POWERMOVER, PUMPPOWER, PUMPPOWER_TYPE, MATERIALOFPRESSURE, VALVETYPE, FUELTYPE
    , GPS_SERVICE_PROVIDER, SOWNER_NAME, TRUCK_CATEGORY, VOL_UOM, VEH_TEXT, CARCATE_ID, ROUTE, STERMINALID, DEPOT, TPPOINT, CLASSGRP,REQ_ID)
 VALUES 
    (:STRUCKID, :SHEADREGISTERNO, :SCARTYPEID, :NSLOT, :SHEADID, :STRAILERID, :STRAILERREGISTERNO, :STRANSPORTID, :STRANSPORTTYPE, :SLAST_REQ_ID, :SENGINE, :SCHASIS, :SBRAND, :SMODEL, :NWHEELS
    , :NTOTALCAPACITY, :NWEIGHT, :NCALC_WEIGHT, :NLOAD_WEIGHT, :SVIBRATION, :STANK_MATERAIL, :STANK_MAKER, :SLOADING_METHOD, :DREGISTER, :SPROD_GRP, :SBLACKLISTTYPE, :DBLACKLIST, :DBLACKLIST2
    , :BLACKLIST_CAUSE, :CACTIVE, SYSDATE, :SCREATE, SYSDATE, :SUPDATE, :DSIGNIN, :NSHAFTDRIVEN, :POWERMOVER, :PUMPPOWER, :PUMPPOWER_TYPE, :MATERIALOFPRESSURE, :VALVETYPE, :FUELTYPE
    , :GPS_SERVICE_PROVIDER, :SOWNER_NAME, :TRUCK_CATEGORY, :VOL_UOM, :VEH_TEXT, :CARCATE_ID, :ROUTE, :STERMINALID, :DEPOT, :TPPOINT, :CLASSGRP,:REQ_ID)";

                    //string STRANSPORTID = CommonFunction.Get_Value(conn, string.Format("SELECT SVENDORID FROM TVENDOR WHERE SABBREVIATION LIKE '{0}'",
                    //                                    ("" + cmbRsHolder.Text != "" ? cmbRsHolder.Text : cmbHsHolder.Text)));

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {

                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                        com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = txtRRegNo.Text;
                        com.Parameters.Add(":SCARTYPEID", OracleType.Number).Value = 4;
                        if (xlbRnSlot.Text.Trim() != "")
                            com.Parameters.Add(":NSLOT", OracleType.Number).Value = xlbRnSlot.Text.Trim();
                        else
                            com.Parameters.Add(":NSLOT", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":SHEADID", OracleType.VarChar).Value = STRUCK;
                        com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = OracleString.Null;
                        com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtHRegNo.Text;
                        com.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = GET_VENDORID_US("" + Session["UserID"]);
                        //com.Parameters.Add(":STRANSPORTID", OracleType.VarChar).Value = !string.IsNullOrEmpty(STRANSPORTID) ? STRANSPORTID : GET_VENDORID_US("" + Session["UserID"]);
                        com.Parameters.Add(":STRANSPORTTYPE", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":SLAST_REQ_ID", OracleType.VarChar).Value = OracleString.Null;
                        com.Parameters.Add(":SENGINE", OracleType.VarChar).Value = txtRsEngine.Text;
                        com.Parameters.Add(":SCHASIS", OracleType.VarChar).Value = txtRsChasis.Text;
                        com.Parameters.Add(":SBRAND", OracleType.VarChar).Value = cboRsBrand.Text + "";
                        com.Parameters.Add(":SMODEL", OracleType.VarChar).Value = txtRsModel.Text;
                        if (txtRnWheel.Text.Trim() != "")
                            com.Parameters.Add(":NWHEELS", OracleType.Number).Value = txtRnWheel.Text.Trim();
                        else
                            com.Parameters.Add(":NWHEELS", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbRnTatolCapacity.Text.Trim() != "")
                            com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = xlbRnTatolCapacity.Text.Trim();
                        else
                            com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = OracleNumber.Null;
                        if (txtRnWeight.Text.Trim() != "")
                            com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = txtRnWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbRCalcWeight.Text.Trim() != "")
                            com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = xlbRCalcWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbRnLoadWeight.Text.Trim() != "")
                            com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = xlbRnLoadWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":SVIBRATION", OracleType.VarChar).Value = cboRsVibration.Value + "";
                        com.Parameters.Add(":STANK_MATERAIL", OracleType.VarChar).Value = cboRTankMaterial.Value + "";
                        com.Parameters.Add(":STANK_MAKER", OracleType.VarChar).Value = txtRTank_Maker.Text;
                        com.Parameters.Add(":SLOADING_METHOD", OracleType.VarChar).Value = cboRLoadMethod.Value + "";
                        com.Parameters.Add(":DSIGNIN", OracleType.DateTime).Value = Convert.ToDateTime(xDetdRRegNo.Value);
                        string[] arrProd = cmbRProdGRP.Text.ToString().Split('-');
                        com.Parameters.Add(":SPROD_GRP", OracleType.VarChar).Value = arrProd[0].Trim();
                        com.Parameters.Add(":SBLACKLISTTYPE", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":DBLACKLIST", OracleType.DateTime).Value = OracleDateTime.Null;
                        com.Parameters.Add(":DBLACKLIST2", OracleType.DateTime).Value = OracleDateTime.Null;
                        com.Parameters.Add(":BLACKLIST_CAUSE", OracleType.VarChar).Value = OracleString.Null;
                        com.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = (rblPermit.Value + "" == "" ? "Y" : rblPermit.Value + "");
                        com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                        com.Parameters.Add(":DREGISTER", OracleType.DateTime).Value = Convert.ToDateTime(xDetdRFRegNo.Value);
                        if (txtRnShaftDriven.Text.Trim() != "")//จำนวนเพลาขับเคลื่อน
                            com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = txtRnShaftDriven.Text.Trim();
                        else
                            com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = OracleNumber.Null;
                        if (txtRPowermover.Text.Trim() != "")//กำลังเครื่องยนต์
                            com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = txtRPowermover.Text.Trim();
                        else
                            com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":PUMPPOWER", OracleType.VarChar).Value = rblRPumpPower.Value + "";
                        com.Parameters.Add(":PUMPPOWER_TYPE", OracleType.VarChar).Value = cboRPumpPower_type.Value + "";
                        com.Parameters.Add(":MATERIALOFPRESSURE", OracleType.VarChar).Value = cboRMaterialOfPressure.Value + "";
                        com.Parameters.Add(":VALVETYPE", OracleType.VarChar).Value = rblRValveType.Value == "Other" ? txtRValveType.Text : rblRValveType.Value + "";
                        com.Parameters.Add(":FUELTYPE", OracleType.VarChar).Value = rblRFuelType.Value == "Other" ? txtRFuelType.Text : rblRFuelType.Value + "";
                        com.Parameters.Add(":GPS_SERVICE_PROVIDER", OracleType.VarChar).Value = cboRGPSProvider.Text + "";
                        com.Parameters.Add(":SOWNER_NAME", OracleType.VarChar).Value = ("" + cmbRsHolder.Text != "" ? cmbRsHolder.Text : cmbHsHolder.Text);
                        com.Parameters.Add(":TRUCK_CATEGORY", OracleType.VarChar).Value = "" + cmbRTru_Cate.Value;
                        com.Parameters.Add(":VOL_UOM", OracleType.VarChar).Value = txtTU_Volume.Text;
                        com.Parameters.Add(":VEH_TEXT", OracleType.VarChar).Value = txtTU_Text.Text;
                        com.Parameters.Add(":CARCATE_ID", OracleType.VarChar).Value = "" + cmbCarcate.Value;

                        com.Parameters.Add(":ROUTE", OracleType.Char).Value = "" + cmbRRoute.Value;
                        com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + cmbRDepot.Value;
                        com.Parameters.Add(":DEPOT", OracleType.Char).Value = "" + cmbRDepot.Value;
                        com.Parameters.Add(":TPPOINT", OracleType.Char).Value = "" + cmbRTPPoint.Value;
                        com.Parameters.Add(":CLASSGRP", OracleType.VarChar).Value = "" + cmbRClassGRP.Value;
                        com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                        com.ExecuteNonQuery();
                    }
                }

                #region อัพเดทข้อมูลหางไปที่หัว เนื่องจากตอน Insert ข้อมูลหัวยังไม่มี ID ของหาง
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    string strSql = "UPDATE TTRUCK_TEMP SET STRAILERID=:STRAILERID ,STRAILERREGISTERNO=:STRAILERREGISTERNO WHERE STRUCKID=:STRUCKID AND REQ_ID = '" + CommonFunction.ReplaceInjection(SREQ_ID) + "'";
                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                        com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = RTRUCK;
                        com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtRRegNo.Text;
                        com.ExecuteNonQuery();
                    }
                }
                #endregion

                #endregion

                #region ข้อมูลความจุ
                if (Session["SRCompart"] != null)
                {
                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        con.Open();
                        DataTable dt = (DataTable)Session["SRCompart"];
                        string strSql = @"INSERT INTO TTRUCK_COMPART_TEMP(STRUCKID,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE,REQ_ID) Values
                                        (:STRUCKID,:NCOMPARTNO,:NPANLEVEL,:NCAPACITY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:REQ_ID)";

                        int nSlot = 0;
                        foreach (DataRow dr in dt.Select("NCOMPARTNO<>''", "NCOMPARTNO ASC"))
                        {
                            for (int i = 1; i <= 3; i++)
                            {
                                if (dr["NPANLEVEL" + i] + "" != "")
                                {
                                    using (OracleCommand com = new OracleCommand(strSql, con))
                                    {
                                        com.Parameters.Clear();
                                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                        com.Parameters.Add(":NCOMPARTNO", OracleType.Number).Value = dr["NCOMPARTNO"] + "";
                                        com.Parameters.Add(":NPANLEVEL", OracleType.Number).Value = i;
                                        com.Parameters.Add(":NCAPACITY", OracleType.Number).Value = dr["NPANLEVEL" + i] + "";
                                        com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                        com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                        com.ExecuteNonQuery();
                                    }
                                }
                            } nSlot++;
                        }
                        strSql = string.Format("UPDATE TTRUCK_TEMP SET NSLOT='{1}' WHERE STRUCKID='{0}' AND REQ_ID = '" + CommonFunction.ReplaceInjection(SREQ_ID) + "'", RTRUCK, nSlot);
                        SystemFunction.SQLExecuteNonQuery(conn, strSql);
                    }
                }
                #endregion

                #region เอกสารข้อมูลรถ
                if (Session["RDoc"] != null)
                {
                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        con.Open();
                        DataTable dt = (DataTable)Session["RDoc"];
                        string dest = UploadDirectory + "" + RTRUCK + "/INFO/";
                        foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "DOC_TYPE ASC"))
                        {
                            string GenID = CommonFunction.Gen_ID(con, TTRUCK_DOC);
                            string strSql = @"INSERT INTO TTRUCK_DOC_TEMP(DOCID,STRUCKID,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME,REQ_ID) Values
                                            (:DOCID,:STRUCKID,:DOC_TYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME,:REQ_ID)";
                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = dr["DOC_TYPE"] + "";
                                com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                                com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                com.ExecuteNonQuery();
                                ServerMoveFile("" + dr["SPATH"], dest, "" + dr["DOC_SYSNAME"]);
                            }
                        }
                    }
                }
                #endregion

                #region ข้อมูลพรบ.
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    string strSql = "";
                    strSql = @"INSERT INTO TTRUCK_INSURANCE_TEMP(STRUCKID,CTYPE,SCOMPANY,START_DURATION,END_DURATION,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE,REQ_ID) Values
                                    (:STRUCKID,:CTYPE,:SCOMPANY,:START_DURATION,:END_DURATION,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE,:REQ_ID)";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                        com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "STATUTE";
                        com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtRSCompany.Text + "";
                        if (xDetRSDuration_Start.Value + "" != "")
                            com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetRSDuration_Start.Value);
                        else
                            com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                        if (xDetRSDuration_End.Value + "" != "")
                            com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetRSDuration_End.Value);
                        else
                            com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                        com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemRSDetail.Text + "";
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                        com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                        com.ExecuteNonQuery();
                    }
                }
                #endregion

                #region ข้อมูลประกันภัย.
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    string strSql = "";
                    strSql = @"INSERT INTO TTRUCK_INSURANCE_TEMP(STRUCKID,CTYPE,SCOMPANY,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE,REQ_ID) Values
                                            (:STRUCKID,:CTYPE,:SCOMPANY,:INSURE_BUDGET,:INSURE_TYPE,:HOLDING,:NINSURE,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE,:REQ_ID)";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                        com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "INSURANCE";
                        com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtRICompany.Text + "";
                        if (txtRIBudget.Text + "" != "")
                            com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = txtRIBudget.Text + "";
                        else
                            com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":INSURE_TYPE", OracleType.VarChar).Value = cbmRIType.Value + "";
                        string sHolding = "";
                        foreach (ListEditItem items in chkRIHolding.Items)
                        {
                            if (items.Selected)
                                sHolding += "," + items.Value;
                        }
                        if (sHolding.Trim() != "")
                            sHolding = sHolding.Substring(1);
                        com.Parameters.Add(":HOLDING", OracleType.VarChar).Value = sHolding.ToString();
                        if ("" + txtRInInsure.Text != "")
                            com.Parameters.Add(":NINSURE", OracleType.Number).Value = txtRInInsure.Text + "";
                        else
                            com.Parameters.Add(":NINSURE", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemRIDetail.Text + "";
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                        com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                        com.ExecuteNonQuery();
                    }
                }
                #endregion

                #region เอกสารข้อมูลพรบ. และประกันภัย
                if (Session["RISDoc"] != null)
                {
                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        con.Open();
                        DataTable dt = (DataTable)Session["RISDoc"];
                        foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "CTYPE ASC"))
                        {
                            string dest = UploadDirectory + "" + STRUCK + "/" + dr["CTYPE"] + "/";
                            string GenID = CommonFunction.Gen_ID(con, TTRUCK_INSUREDOC);

                            string strSql = @"INSERT INTO TTRUCK_INSUREDOC_TEMP(DOCID,STRUCKID,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME,REQ_ID) Values
                                            (:DOCID,:STRUCKID,:CTYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME,:REQ_ID)";
                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = dr["CTYPE"] + "";
                                com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                                com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                com.ExecuteNonQuery();
                                ServerMoveFile("" + dr["SPATH"], dest, "" + dr["DOC_SYSNAME"]);
                            }
                        }
                    }
                }
                #endregion

            }

            #endregion

            #region ข้อมูลวัดน้ำ

            if (ScarType != "3" || !string.IsNullOrEmpty(RTRUCK))//ตรวจสอบสิทธิ์
            {
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();

                    string sQuery = @"UPDATE TTRUCK_TEMP SET PLACE_WATER_MEASURE=:PLACE_WATER_MEASURE,SLAST_REQ_ID=:SLAST_REQ_ID, DPREV_SERV=:DPREV_SERV, DWATEREXPIRE=:DWATEREXPIRE, SWUPDATE=:SWUPDATE, DWUPDATE=SYSDATE WHERE STRUCKID=:STRUCKID AND REQ_ID = '" + CommonFunction.ReplaceInjection(SREQ_ID) + "'";

                    using (OracleCommand com = new OracleCommand(sQuery, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = (ScarType == "3" ? RTRUCK : STRUCK);
                        com.Parameters.Add(":PLACE_WATER_MEASURE", OracleType.VarChar).Value = rblPlaceMWater.Value + "";
                        //วัดน้ำภายใน ปตท.
                        if (rblPlaceMWater.Value == "INTERNAL")
                        {
                            com.Parameters.Add(":SLAST_REQ_ID", OracleType.VarChar).Value = OracleString.Null;
                            com.Parameters.Add(":DPREV_SERV", OracleType.DateTime).Value = OracleDateTime.Null;
                            com.Parameters.Add(":DWATEREXPIRE", OracleType.DateTime).Value = OracleDateTime.Null;
                        }
                        else//วัดน้ำภายนอก ปตท.
                        {
                            com.Parameters.Add(":SLAST_REQ_ID", OracleType.VarChar).Value = txtEsCar_Num.Text;
                            com.Parameters.Add(":DPREV_SERV", OracleType.DateTime).Value = Convert.ToDateTime(xDetEDPREV_SERV.Value);
                            com.Parameters.Add(":DWATEREXPIRE", OracleType.DateTime).Value = Convert.ToDateTime(xDetEDNEXT_SERV.Value);
                        }
                        com.Parameters.Add(":SWUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                        com.ExecuteNonQuery();
                    }

                    #region เอกสารวัดน้ำ
                    if ("" + rblPlaceMWater.Value == "EXTERNAL")
                    {
                        //เอกสารวัดน้ำ สำหรับกรณีที่มีการวัดน้ำภายนอกปตท.
                        DataTable dt = (DataTable)Session["MWaterDoc"];
                        string dest = UploadDirectory + "" + (ScarType == "3" ? RTRUCK : STRUCK) + "/MWATER/";
                        if (dt != null)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["CNEW"] == "1")
                                {
                                    string GenID = CommonFunction.Gen_ID(con, TTRUCK_MWATERDOC);

                                    string strSql = @"INSERT INTO TTRUCK_MWATERDOC_TEMP(DOCID,STRUCKID,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME,REQ_ID) Values
                                                    (:DOCID,:STRUCKID,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME,:REQ_ID)";
                                    using (OracleCommand com = new OracleCommand(strSql, con))
                                    {
                                        com.Parameters.Clear();
                                        com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = (ScarType == "3" ? RTRUCK : STRUCK);
                                        com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                                        com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                                        com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                        com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                        com.ExecuteNonQuery();
                                        ServerMoveFile(dr["SPATH"].ToString(), dest, dr["DOC_SYSNAME"].ToString());
                                    }
                                }
                                else
                                {
                                    string strSql = @"UPDATE TTRUCK_MWATERDOC_TEMP SET DUPDATE = SYSDATE ,SUPDATE = :SUPDATE ,CACTIVE = :CACTIVE
                                                     WHERE DOCID=:DOCID AND STRUCKID=:STRUCKID AND REQ_ID=:REQ_ID";
                                    using (OracleCommand com = new OracleCommand(strSql, con))
                                    {
                                        com.Parameters.Clear();
                                        com.Parameters.Add(":DOCID", OracleType.Number).Value = dr["DOCID"];
                                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = ScarType == "3" ? RTRUCK : STRUCK;
                                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                        com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                        com.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }

            #endregion

            #region เคลียร์ไฟล์
            ServerClearFile(UploadDirectory + "Temp/" + STRUCK);
            if (RTRUCK != "")
                ServerClearFile(UploadDirectory + "Temp/" + RTRUCK);
            #endregion

            IsSuccessed = "1";
            msg += "<br /> บันทึกข้อมูล(TMS) : <img src=images/stat_check.gif />";
        }
        catch (Exception ect)
        {
            IsSuccessed = "0";
            msg += "<br /> บันทึกข้อมูล(TMS) : <img src=images/stat_unchecked.gif />";
        }

        if (IsSuccessed == "1")
        {

            string sMsg = "";

            //string VEH_NO = CommonFunction.Get_Value(new OracleConnection(conn), "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STRUCK + "'");

            //if (VEH_NO.Trim() != "" && cSAP_SYNC == "1") // cSAP_SYNC = 1 ให้มีการ Syncข้อมูลไปยัง SAP
            //{
            //    try
            //    {
            //        SAP_CREATE_VEH veh_syncout = new SAP_CREATE_VEH();
            //        veh_syncout.sVehicle = VEH_NO + "";
            //        sMsg = veh_syncout.CRT_Vehicle_SYNC_OUT_SI();
            //        if (sMsg == "")
            //            msg += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_check.gif />";
            //        else
            //            msg += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_unchecked.gif />" + sMsg.Replace(",", "<br/>");
            //    }
            //    catch (Exception ect)
            //    {
            //        msg += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_unchecked.gif />" + sMsg.Replace(",", "<br/>");
            //    }
            //}
            ClearSession();
            //CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + "ผลการบันทึกรายการ " + msg +
            //        "',function(){ window.location='Vendor_Detail.aspx';});");
            if (SendMailToVendorRk())
            {
                CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + "ผลการบันทึกรายการ " + msg + "',function(){ window.location='Vendor_Detail.aspx'; });");
            }
            else
            {
                CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + " <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้',function(){window.location='Vendor_Detail.aspx';});");
            }
        }
        else
            CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfo('Warning!','" + "ผลการบันทึกรายการ " + msg + "');");
    }
    void BindData(string mode)
    {
        DataTable dtTruckDoc, dt;
        string _sql = "";

        switch (mode)
        {
            case "SET_DATA_CONTROL":
                #region SET DDL
                //ยี่ห้อรถ
                _sql = @"SELECT 0,null BRANDID,' - เลือก - ' BRANDNAME FROM TTRUCK_BRAND UNION SELECt ROWNUM,BRANDID,BRANDNAME FROM TTRUCK_BRAND";
                dt = CommonFunction.Get_Data(conn, _sql);
                cboHsBrand.DataSource = dt;
                cboHsBrand.DataBind();
                cboRsBrand.DataSource = dt;
                cboRsBrand.DataBind();
                //ผู้ให้บริการ GPS
                _sql = @"SELECT 0,null GPSID,' - เลือก - ' GPSNAME FROM TTRUCK_GPS UNION SELECt ROWNUM,GPSID,GPSNAME FROM TTRUCK_GPS";
                dt = CommonFunction.Get_Data(conn, _sql);
                cboHGPSProvider.DataSource = dt;
                cboHGPSProvider.DataBind();
                cboRGPSProvider.DataSource = dt;
                cboRGPSProvider.DataBind();
                //กลุ่มผลิตภัณฑ์ที่บรรทุก
                _sql = @"SELECT PROS.PROD_ID,PROS.PROD_NAME,PRO.PROD_CATEGORY,PRO.DENSITY FROM TPRODUCT PRO LEFT  JOIN TPRODUCT_SAP PROS ON PRO.PROD_ID=PROS.PROD_ID";
                dt = CommonFunction.Get_Data(conn, _sql);
                cmbHProdGRP.DataSource = dt;
                cmbHProdGRP.DataBind();
                cmbRProdGRP.DataSource = dt;
                cmbRProdGRP.DataBind();
                dt.Dispose();
                #endregion
                break;

            case "BIND_TRUCK_HINFODOC1":
                #region แนบเอกสารใบจดทะเบียนรถ(หัว)
                if (Session["HDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='1'"));
                    gvwSTRUCKDoc1.DataSource = dtTruckDoc;
                    gvwSTRUCKDoc1.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC2":
                #region แนบหลักฐานการเสียภาษี(หัว)
                if (Session["HDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='2'"));
                    gvwSTRUCKDoc2.DataSource = dtTruckDoc;
                    gvwSTRUCKDoc2.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC3":
                #region แนบเอกสาร Compartment(หัว)
                if (Session["HDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='3'"));
                    gvwSTRUCKDoc3.DataSource = dtTruckDoc;
                    gvwSTRUCKDoc3.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC4":
                #region แนบเอกสารใบจดทะเบียนรถครั้งแรก(หัว)
                if (Session["HDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='4'"));
                    gvwSTRUCKDoc4.DataSource = dtTruckDoc;
                    gvwSTRUCKDoc4.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC5":
                #region แนบเอกสารอื่นๆ
                if (Session["HDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='" + (int)TMSEnum.TruckDocType.Others + "'"));
                    gvwSTRUCKDoc5.DataSource = dtTruckDoc;
                    gvwSTRUCKDoc5.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_HSTATUTEDOC":
                #region แนบเอกสารพรบ.(หัว)
                if (Session["HISDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HISDoc"], ((DataTable)Session["HISDoc"]).Select("CACTIVE='1' AND CTYPE='STATUTE'"));
                    gvwSTRUCKSDoc.DataSource = dtTruckDoc;
                    gvwSTRUCKSDoc.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_HINSUREDOC":
                #region แนบเอกสารประกันภัย(หัว)
                if (Session["HISDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HISDoc"], ((DataTable)Session["HISDoc"]).Select("CACTIVE='1' AND CTYPE='INSURANCE'"));
                    gvwSTRUCKIDoc.DataSource = dtTruckDoc;
                    gvwSTRUCKIDoc.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;

            case "BIND_TRUCK_RINFODOC1":
                #region แนบเอกสารใบจดทะเบียนรถ(หาง)
                if (Session["RDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='1'"));
                    gvwRTRUCKDoc1.DataSource = dtTruckDoc;
                    gvwRTRUCKDoc1.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC2":
                #region แนบหลักฐานการเสียภาษี(หาง)
                if (Session["RDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='2'"));
                    gvwRTRUCKDoc2.DataSource = dtTruckDoc;
                    gvwRTRUCKDoc2.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC3":
                #region แนบเอกสาร Compartment(หาง)
                if (Session["RDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='3'"));
                    gvwRTRUCKDoc3.DataSource = dtTruckDoc;
                    gvwRTRUCKDoc3.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC4":
                #region แนบเอกสารใบจดทะเบียนรถครั้งแรก(หาง)
                if (Session["RDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='4'"));
                    gvwRTRUCKDoc4.DataSource = dtTruckDoc;
                    gvwRTRUCKDoc4.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC5":
                #region แนบเอกสารใบจดทะเบียนรถครั้งแรก(หาง)
                if (Session["RDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='" + (int)TMSEnum.TruckDocType.Others + "'"));
                    gvwRTRUCKDoc5.DataSource = dtTruckDoc;
                    gvwRTRUCKDoc5.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_RSTATUTEDOC":
                #region แนบเอกสารพรบ.(หาง)
                if (Session["RISDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RISDoc"], ((DataTable)Session["RISDoc"]).Select("CACTIVE='1' AND CTYPE='STATUTE'"));
                    gvwRTRUCKSDoc.DataSource = dtTruckDoc;
                    gvwRTRUCKSDoc.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
            case "BIND_TRUCK_RINSUREDOC":
                #region แนบเอกสารประกันภัย(หาง)
                if (Session["RISDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RISDoc"], ((DataTable)Session["RISDoc"]).Select("CACTIVE='1' AND CTYPE='INSURANCE'"));
                    gvwRTRUCKIDoc.DataSource = dtTruckDoc;
                    gvwRTRUCKIDoc.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;

            case "BIND_TRUCK_MWATERDOC":
                #region แนบไฟล์ข้อมูลวัดน้ำ
                if (Session["MWaterDoc"] != null)
                {
                    dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["MWaterDoc"], ((DataTable)Session["MWaterDoc"]).Select("CACTIVE='1'"));
                    gvwSTRUCKMWaterDoc.DataSource = dtTruckDoc;
                    gvwSTRUCKMWaterDoc.DataBind();
                    dtTruckDoc.Dispose();
                }
                #endregion
                break;
        }
    }
    void SetRoundPanel(string cCarType)
    {
        string sCarType = "";
        switch (cCarType)
        {
            case "0":
                sCarType = "รถบรรทุก";
                dvHead.Visible = true;
                tblCapacity.Visible = true;
                rpnHInfomation.HeaderText = "ข้อมูล" + sCarType;
                rpnHStatute.HeaderText = "พรบ." + sCarType;
                rpnHInsurance.HeaderText = "การประกันภัย" + sCarType;
                dvTrail.Visible = false;
                break;
            case "3":
                sCarType = "รถหัวเทรลเลอร์";
                dvHead.Visible = true;
                tblCapacity.Visible = false;
                rpnHInfomation.HeaderText = "ข้อมูล" + sCarType;
                rpnHStatute.HeaderText = "พรบ." + sCarType;
                rpnHInsurance.HeaderText = "การประกันภัย" + sCarType;

                sCarType = "รถหางเทรลเลอร์";
                dvTrail.Visible = true;
                rpnTInfomation.HeaderText = "ข้อมูล" + sCarType;
                rpnTStatute.HeaderText = "พรบ." + sCarType;
                rpnTInsurance.HeaderText = "การประกันภัย" + sCarType;
                break;
        }
    }
    void ChangeMode(string mode)
    {
        bool V = false;
        //  string sColor = "#FFFFFF";
        string sColor, sColor2;
        sColor2 = "#EEEEEE";
        sColor = "#FFFFFF";
        if (mode == "View")
        {
            V = true;
            sColor = "#EEEEEE";
        }

        cmbCarcate.ReadOnly = V;
        cmbCarType.ReadOnly = V;

        rpnHInfomation.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cmbHsHolder.ReadOnly = V;
        xDetdHRegNo.ReadOnly = V;
        xDetdHFRegNo.ReadOnly = V;
        cmbHTru_Cate.ReadOnly = V;
        txtVEH_Text.ReadOnly = V;
        cmbHRoute.ReadOnly = V;
        cmbHClassGRP.ReadOnly = V;
        cmbHDepot.ReadOnly = V;
        cmbHTPPoint.ReadOnly = V;
        txtVEH_Volume.ReadOnly = V;
        txtHsChasis.ReadOnly = V;
        txtHsEngine.ReadOnly = V;
        cboHsBrand.ReadOnly = V;
        txtHsModel.ReadOnly = V;
        txtHnWheel.ReadOnly = V;
        txtHPowermover.ReadOnly = V;
        txtHnShaftDriven.ReadOnly = V;
        cboHsVibration.ReadOnly = V;
        rblHPumpPower.ReadOnly = V;
        cboHPumpPower_type.ReadOnly = V;
        cboHMaterialOfPressure.ReadOnly = V;
        rblHValveType.ReadOnly = V;
        if (rblHValveType.Value == "Other")
            txtHValveType.ClientEnabled = !V;
        rblHFuelType.ReadOnly = V;
        if (rblHFuelType.Value == "Other")
            txtHFuelType.ClientEnabled = !V;
        cboHGPSProvider.ReadOnly = V;
        txtHnWeight.ReadOnly = V;
        gvwHCompart.Columns[4].Visible = !V;
        tblHNewCap.Visible = !V;
        cboHTankMaterial.ReadOnly = V;
        cboHLoadMethod.ReadOnly = V;
        txtHTank_Maker.ReadOnly = V;
        cmbHProdGRP.ReadOnly = V;
        uldHDoc01.Enabled = !V;
        xbnHDoc01.Enabled = !V;
        gvwSTRUCKDoc1.Columns[2].Visible = !V;
        gvwSTRUCKDoc1.Columns[3].Visible = V;
        uldHDoc02.Enabled = !V;
        xbnHDoc02.Enabled = !V;
        gvwSTRUCKDoc2.Columns[2].Visible = !V;
        gvwSTRUCKDoc2.Columns[3].Visible = V;
        uldHDoc03.Enabled = !V;
        xbnHDoc03.Enabled = !V;
        gvwSTRUCKDoc3.Columns[2].Visible = !V;
        gvwSTRUCKDoc3.Columns[3].Visible = V;
        uldHDoc04.Enabled = !V;
        xbnHDoc04.Enabled = !V;
        gvwSTRUCKDoc4.Columns[2].Visible = !V;
        gvwSTRUCKDoc4.Columns[3].Visible = V;


        rpnHStatute.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtHSCompany.ReadOnly = V;
        xDetHSDuration_Start.ReadOnly = V;
        xDetHSDuration_End.ReadOnly = V;
        xMemHSDetail.ReadOnly = V;
        uldHSDoc.Enabled = !V;
        xbnHSDoc.Enabled = !V;
        gvwSTRUCKSDoc.Columns[2].Visible = !V;
        gvwSTRUCKSDoc.Columns[3].Visible = V;


        rpnHInsurance.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtHICompany.ReadOnly = V;
        txtHIBudget.ReadOnly = V;
        cbmHIType.ReadOnly = V;
        chkHIHolding.ReadOnly = V;
        txtHInInsure.ReadOnly = V;
        xMemHIDetail.ReadOnly = V;
        uldHIDoc.Enabled = !V;
        xbnHIDoc.Enabled = !V;
        gvwSTRUCKIDoc.Columns[2].Visible = !V;
        gvwSTRUCKIDoc.Columns[3].Visible = V;


        rpnTInfomation.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cmbRsHolder.ReadOnly = V;
        xDetdRFRegNo.ReadOnly = V;
        xDetdRRegNo.ReadOnly = V;
        cmbRTru_Cate.ReadOnly = V;
        txtTU_Text.ReadOnly = V;
        cmbRRoute.ReadOnly = V;
        cmbRClassGRP.ReadOnly = V;
        cmbRDepot.ReadOnly = V;
        cmbRTPPoint.ReadOnly = V;
        txtTU_Volume.ReadOnly = V;
        txtRsChasis.ReadOnly = V;
        txtRsEngine.ReadOnly = V;
        cboRsBrand.ReadOnly = V;
        txtRsModel.ReadOnly = V;
        txtRnWheel.ReadOnly = V;
        txtRPowermover.ReadOnly = V;
        txtRnShaftDriven.ReadOnly = V;
        cboRsVibration.ReadOnly = V;
        rblRPumpPower.ReadOnly = V;
        cboRPumpPower_type.ReadOnly = V;
        cboRMaterialOfPressure.ReadOnly = V;
        rblRValveType.ReadOnly = V;
        if (rblRValveType.Value == "Other")
            txtRValveType.ClientEnabled = !V;
        rblRFuelType.ReadOnly = V;
        if (rblRFuelType.Value == "Other")
            txtRFuelType.ClientEnabled = !V;
        cboRGPSProvider.ReadOnly = V;
        txtRnWeight.ReadOnly = V;
        gvwRCompart.Columns[4].Visible = !V;
        tblRNewCap.Visible = !V;
        cboRTankMaterial.ReadOnly = V;
        cboRLoadMethod.ReadOnly = V;
        txtRTank_Maker.ReadOnly = V;
        cmbRProdGRP.ReadOnly = V;
        uldRDoc01.Enabled = !V;
        xbnRDoc01.Enabled = !V;
        gvwRTRUCKDoc1.Columns[2].Visible = !V;
        gvwRTRUCKDoc1.Columns[3].Visible = V;
        uldRDoc02.Enabled = !V;
        xbnRDoc02.Enabled = !V;
        gvwRTRUCKDoc2.Columns[2].Visible = !V;
        gvwRTRUCKDoc2.Columns[3].Visible = V;
        uldRDoc03.Enabled = !V;
        xbnRDoc03.Enabled = !V;
        gvwRTRUCKDoc3.Columns[2].Visible = !V;
        gvwRTRUCKDoc3.Columns[3].Visible = V;
        uldRDoc04.Enabled = !V;
        xbnRDoc04.Enabled = !V;
        gvwRTRUCKDoc4.Columns[2].Visible = !V;
        gvwRTRUCKDoc4.Columns[3].Visible = V;


        rpnTStatute.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtRSCompany.ReadOnly = V;
        xDetRSDuration_Start.ReadOnly = V;
        xDetRSDuration_End.ReadOnly = V;
        xMemRSDetail.ReadOnly = V;
        uldRSDoc.Enabled = !V;
        xbnRSDoc.Enabled = !V;
        gvwRTRUCKSDoc.Columns[2].Visible = !V;
        gvwRTRUCKSDoc.Columns[3].Visible = V;


        rpnTInsurance.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtRICompany.ReadOnly = V;
        txtRIBudget.ReadOnly = V;
        cbmRIType.ReadOnly = V;
        chkRIHolding.ReadOnly = V;
        txtRInInsure.ReadOnly = V;
        xMemRIDetail.ReadOnly = V;
        uldRIDoc.Enabled = !V;
        xbnRIDoc.Enabled = !V;
        gvwRTRUCKIDoc.Columns[2].Visible = !V;
        gvwRTRUCKIDoc.Columns[3].Visible = V;


        rpnPermit.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        //rblPermit.ReadOnly = V;
        //rpnPermit.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor2);
        //rblPermit.ReadOnly = true;
        //rpnWater.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor2);
        //rblPlaceMWater

        xbnSubmit.Visible = !V;
        xbnCancel.Visible = !V;
    }
    void SetValidation(bool _b)
    {
        string sVal = "";

        if (_b)
            sVal = @"<font color=""#FF0000"">*</font>";

        vlddHRegNo.Text = sVal;
        xDetdHRegNo.ValidationSettings.RequiredField.IsRequired = _b;
        vldHsBrand.Text = sVal;
        cboHsBrand.ValidationSettings.RequiredField.IsRequired = _b;
        vldHnShaftDriven.Text = sVal;
        txtHnShaftDriven.ValidationSettings.RequiredField.IsRequired = _b;
        vldHGPSProvider.Text = sVal;
        cboHGPSProvider.ValidationSettings.RequiredField.IsRequired = _b;
        vldRRegNo.Text = sVal;
        xDetdRRegNo.ValidationSettings.RequiredField.IsRequired = _b;
        vldRsBrand.Text = sVal;
        cboRsBrand.ValidationSettings.RequiredField.IsRequired = _b;
        vldRnShaftDriven.Text = sVal;
        txtRnShaftDriven.ValidationSettings.RequiredField.IsRequired = _b;

    }
    void ClearSession()
    {
        string[] sSession = new string[] { "SCARTYPEID", "SHCompart", "SRCompart", "HDoc", "RDoc", "HISDoc", "RISDoc", "MWaterDoc" };
        foreach (string s in sSession)
            Session.Remove(s);
    }

    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
    protected DataTable PrepareDataTable(string ss_name, string IsDel, string struckid, params string[] fields)
    {
        DataTable dtData = new DataTable();
        if (Session[ss_name] != null)
        {
            dtData = (DataTable)Session[ss_name];
            if (IsDel == "0")
            {
                foreach (DataRow drDel in dtData.Select("CNEW='1' AND CACTIVE='0'"))
                {
                    int idx = dtData.Rows.IndexOf(drDel);
                    dtData.Rows[idx].Delete();
                }
            }
        }
        else
        {
            foreach (string field_type in fields)
            {
                dtData.Columns.Add(field_type + "");
            }
        }
        return dtData;
    }

    string UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string ServerMapPath = Server.MapPath("./") + pathFile.Replace("/", "\\");
        FileInfo File = new FileInfo(ServerMapPath + ful.FileName);
        string ResultFileName = ServerMapPath + File.Name;
        string sPath = Path.GetDirectoryName(ResultFileName)
                , sFileName = Path.GetFileNameWithoutExtension(ResultFileName)
                , sFileType = Path.GetExtension(ResultFileName);

        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(ServerMapPath))
            {
                Directory.CreateDirectory(ServerMapPath);
            }
            #endregion
            string fileName = (GenFileName + "" + sFileType.Trim());
            ful.SaveAs(ServerMapPath + fileName);

            return fileName + "$" + sFileName.Replace("$", "") + sFileType;
        }
        else
            return "$";
    }
    #region manage file on server
    //this event occur when press submit
    private void ServerMoveFile(string sourceFile, string destFile, string fileName)
    {
        if (!Directory.Exists(Server.MapPath(destFile)))//ก่อนจะย้าย ดูก่อนว่ามี directory ป่าว 
        {
            Directory.CreateDirectory(Server.MapPath(destFile));//สร้าง directory ขึนมาสำหรับจะย้ายไฟล์เข้า
        }
        if (isFileExist(sourceFile + fileName))//ก่อนจะย้าย เชคก่อนว่า มีไฟล์อยู่หรือป่าว?
        {
            Directory.Move(Server.MapPath(sourceFile) + fileName, Server.MapPath(destFile) + fileName);
        }
    }
    private void ServerClearFile(string pathFile)
    {
        if (Directory.Exists(Server.MapPath(pathFile)))
        {
            Directory.Delete(Server.MapPath(pathFile), true);
        }
    }
    private bool isFileExist(string checkPath)
    {
        if (File.Exists(Server.MapPath(checkPath)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private void DeleteFileNotUse(string pathFile)
    {
        if (isFileExist(pathFile))
        {
            File.Delete(Server.MapPath(pathFile));
        }
    }
    #endregion

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    private bool SendMailToVendorRk()
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "ขอเพิ่มข้อมูลรถ " + txtHRegNo.Text + (!string.IsNullOrEmpty(txtRRegNo.Text) ? (" - " + txtRRegNo.Text + "") : "");
        string VENDOR_NAME = "";
        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {

            DataTable dt_MAIL = CommonFunction.Get_Data(conn, @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('61')");
            for (int i = 0; i < dt_MAIL.Rows.Count; i++)
            {
                _to += ";" + dt_MAIL.Rows[i]["SEMAIL"] + "";
            }

            _to = _to.Remove(0, 1);
        }
        string SVENDOR_ID = SystemFunction.GET_VENDORID(Session["UserID"] + "");
        //หาชื่อบริษัท
        DataTable dt_NAME = CommonFunction.Get_Data(conn, "SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = '" + SVENDOR_ID + "' ");
        if (dt_NAME.Rows.Count > 0)
        {
            VENDOR_NAME = dt_NAME.Rows[0]["SABBREVIATION"] + "";
        }
        #region html

        sHTML = @" <table width='600px' cellpadding='3' cellspacing='1' border='0' >
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง ขอเพิ่มข้อมูลรถ
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน รข.</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   ทางบริษัท " + VENDOR_NAME + @" ได้ขอเพิ่มข้อมูลรถ  """ + txtHRegNo.Text + (!string.IsNullOrEmpty(txtRRegNo.Text) ? (" - " + txtRRegNo.Text + "") : "") + @""" เพื่อให้ข้อมูลเป็นปัจจุบันและถูกต้องยิ่งขึ้น</td>
            </tr>
              <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>จึงเรียนมาเพื่อโปรดทราบ
                    และดำเนินการต่อไป</td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>" + VENDOR_NAME + @"
                </td>
            </tr>
            </table>";

        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(conn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    private string GET_VENDORID_US(string SUID)
    {
        string Result = "";

        string Query = @"SELECT SUID, SVENDORID FROM TUSER WHERE SUID = '" + SUID + "' AND CGROUP = '0'";
        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            Result = dt.Rows[0]["SVENDORID"] + "";
        }

        return Result;
    }
}