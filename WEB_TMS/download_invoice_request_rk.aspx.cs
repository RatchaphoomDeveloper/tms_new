﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Data.OracleClient;
using System.Globalization;
using System.Drawing.Text;
using DevExpress.XtraReports.UI;
using System.Drawing;


public partial class download_invoice_request_rk : System.Web.UI.Page
{
    public static string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static bool cBindReport = false;
    private static string sRQID = "";
    private static DataTable dtReport = new DataTable();
    private static DataTable dtParameterReport = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string strRQID = Request.QueryString["strRQID"];
            if (!string.IsNullOrEmpty(strRQID + ""))
            {
                string[] arrRQID = STCrypt.DecryptURL(strRQID);
                sRQID = arrRQID[0];
                dtReport = new DataTable();
                dtParameterReport = new DataTable();
                cBindReport = false;
                //set data to list for bind report

                ListData_Request_Item(sRQID);
                ListData_Parameter_Report(sRQID);
                BindReport(dtReport, dtParameterReport, "Invoice", sRQID);
            }
            else
            {
                btnDowload.ClientVisible = false;
            }

        }

        if (cBindReport)
        {
            BindReport(dtReport, dtParameterReport, "Invoice", sRQID);
        }
    }

    private void BindReport(DataTable _dtData, DataTable _dtParameter, string sReportName, string sReqID)
    {
        rpt_Invoice_RQ report = new rpt_Invoice_RQ();
        report.Name = sReqID + "_" + sReportName;

        report.DataSource = _dtData;

        DataRow dr = null;
        if (_dtParameter.Rows.Count > 0)
        {
            dr = _dtParameter.Rows[0];

            report.Parameters["sReportNo"].Value = dr["REQUEST_ID"] + "";
            report.Parameters["sDay"].Value = DateTime.Now.Day + "";
            report.Parameters["sMonth"].Value = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
            report.Parameters["sYearTH"].Value = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
            report.Parameters["sCompanyName"].Value = dr["SABBREVIATION"] + "(" + dr["VENDOR_ID"] + ")";
            report.Parameters["sAddress"].Value = dr["SNO"] + " " + dr["SDISTRICT"] + " " + dr["SREGION"] + " " + dr["SPROVINCE"] + " " + dr["SPROVINCECODE"] + "";
            report.Parameters["sRegisterNo"].Value = dr["SREGISTRATION"] + "";
            report.Parameters["sChasisNo"].Value = dr["VEH_CHASSIS"] + "" + (dr["TU_CHASSIS"] + "" != "" ? ("/" + dr["TU_CHASSIS"]) : "");
        }

        float scaleFactor = 1;
        if (!IsReportPrinted())
        {
            scaleFactor = 1.3f;//0.835f;

            PrivateFontCollection fontColl = new PrivateFontCollection();
            //  fontColl.AddFontFile(Server.MapPath("~/font/THSarabun Bold.ttf"));
            fontColl.AddFontFile(Server.MapPath("~/font/Tahoma.ttf"));

            foreach (Band b in report.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    c.Font = new Font(fontColl.Families[0].Name, 8, c.Font.Style);// c.Font.Size  
                }
            }

            ((XRTableCell)report.FindControl("xrTableCell1", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell2", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell3", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell5", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell6", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell8", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell9", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell11", true)).Font = new Font(fontColl.Families[0].Name, 8);
            ((XRTableCell)report.FindControl("xrTableCell4", true)).Font = new Font(fontColl.Families[0].Name, 8);

            // โปรดเรียกเก็บเงินจาก xrLabel6
            ((XRLabel)report.FindControl("xrLabel6", true)).Text = "............................................................................................................................................................";

            // ที่อยู่ xrLabel10
            ((XRLabel)report.FindControl("xrLabel10", true)).Text = "......................................................................................................................................................................................................";

            // ผู้จ่ายค่าธรรมเนียม xrLabel15
            ((XRLabel)report.FindControl("xrLabel15", true)).Text = "................................................";

            // ผู้รับค่าธรรมเนียม xrLabel19
            ((XRLabel)report.FindControl("xrLabel19", true)).Text = "................................................";

            ((XRLabel)report.FindControl("xrLabel29", true)).Text = "";
            ((XRLabel)report.FindControl("xrLabel30", true)).Text = "";




        }
        else
        {
            scaleFactor = 1;

            PrivateFontCollection fontColl = new PrivateFontCollection();
            //  fontColl.AddFontFile(Server.MapPath("~/font/THSarabun Bold.ttf"));
            fontColl.AddFontFile(Server.MapPath("~/font/THSarabunNew.ttf"));

            foreach (Band b in report.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    c.Font = new Font(fontColl.Families[0].Name, 12, c.Font.Style);// c.Font.Size  
                }
            }

            ((XRTableCell)report.FindControl("xrTableCell1", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell2", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell3", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell5", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell6", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell8", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell9", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell11", true)).Font = new Font(fontColl.Families[0].Name, 12);
            ((XRTableCell)report.FindControl("xrTableCell4", true)).Font = new Font(fontColl.Families[0].Name, 12);

        }

        report.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        report.PageWidth = Convert.ToInt32(report.PageWidth * scaleFactor);
        report.PageHeight = Convert.ToInt32(report.PageHeight * scaleFactor);
        report.CreateDocument();
        report.PrintingSystem.Document.ScaleFactor = scaleFactor;

        rvw.Report = report;
        rvw.DataBind();
        cBindReport = true;
    }

    #region class
    [Serializable]
    class TReport_RQ
    {
        public string sID { get; set; }
        public string sDetail { get; set; }
        public decimal nPrice { get; set; }
    }
    #endregion

    protected bool IsReportPrinted()
    {
        //return true;
        return (Request.Params["__EVENTTARGET"] == rvw.UniqueID &&
            (Request.Params["__EVENTARGUMENT"].Contains("saveToDisk") ||
            Request.Params["__EVENTARGUMENT"].Contains("saveToWindow")));
    }

    private void ListData_Request_Item(string _sreqid)
    {
        //        string sql = @"SELECT TSM.SERVICE_ID,TSM.SERVICE_NAME as sDetail,TRI.NITEM,TRI.NPRICE,CASE WHEN TSM.SERVICE_ID = '00002' THEN 1 ELSE 0 END AS NSETORDER,TSM.FLAGE_SERVICECANCEL
        //                        FROM TBL_SERVICE_MASTERDATA TSM INNER JOIN TBL_REQUEST_ITEM TRI ON TSM.SERVICE_ID = TRI.SERVICE_ID
        //                        WHERE TRI.REQUEST_ID = '{0}'  AND TSM.ISACTIVE_FLAG = 'Y'
        //                        ORDER BY NSETORDER ASC,TSM.SERVICE_NAME ASC";
        string sql = @"SELECT ROWNUM as NROWS,TSM.SERVICE_ID,TRI.NITEM,
                        CASE WHEN TRI.FLAGE_SERVICECANCEL = 'Y' THEN  TSM.SERVICE_NAME||'(ยกเว้นค่าบริการ)' ELSE  TSM.SERVICE_NAME END  as sDetail,
                        CASE WHEN TRI.FLAGE_SERVICECANCEL = 'Y' THEN 0 ELSE TRI.NPRICE END AS NPRICE,
                        CASE WHEN TSM.SERVICE_ID = '00002' THEN 1 ELSE 0 END AS NSETORDER
                        FROM TBL_SERVICE_MASTERDATA TSM 
                        INNER JOIN TBL_REQUEST_ITEM TRI ON TSM.SERVICE_ID = TRI.SERVICE_ID
                        WHERE TRI.REQUEST_ID = '{0}'  AND TSM.ISACTIVE_FLAG = 'Y' AND NVL(TRI.CTYPE_ITEM,'99') <> '1'
                        ORDER BY NSETORDER ASC,TSM.SERVICE_NAME ASC";

        dtReport = new DataTable();
        dtReport = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(_sreqid)));

        if (dtReport.Rows.Count > 0)
        {
            DataRow dr = null; // เต็มที่ได้ 14 row
            int ncheck = (12 - dtReport.Rows.Count);
            for (int i = 0; i < ncheck; i++)
            {
                dr = dtReport.NewRow();
                dr["SERVICE_ID"] = "9999";
                dtReport.Rows.Add(dr);
            }
        }



    }

    private void ListData_Parameter_Report(string _sreqid)
    {
        //set parameters 
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
                        TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
                        CASE WHEN TRQ.TU_NO IS NOT NULL AND TRQ.TU_NO <> '' THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
                        TCC.CARCATE_NAME,TVD.SABBREVIATION,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
                        TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS
                         FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
                         LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
                         WHERE TRQ.REQUEST_ID = '{0}'";
        dtParameterReport = new DataTable();
        dtParameterReport = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(_sreqid)));
    }

    #region old code show
    /*
        float scaleFactor = 1;
        if (!IsReportPrinted())
        {
            scaleFactor = 0.835f;

        }
        else
        {
            scaleFactor = 1;
        }
        report.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        report.PageWidth = Convert.ToInt32(report.PageWidth * scaleFactor);
        report.PageHeight = Convert.ToInt32(report.PageHeight * scaleFactor);
        report.CreateDocument();
        report.PrintingSystem.Document.ScaleFactor = scaleFactor;
        */
    #endregion
}