﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;

public partial class vendor_accident_lst : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            Session["delSACCIDENTID"] = null;
            Session["oSACCIDENTID"] = null;
            Session["sendStatus"] = null;

            LogUser("7", "R", "เปิดดูข้อมูลหน้า รถเกิดอุบัติเหตุ", "");
        }
    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

        BindData();
        lblCarCount.Text = ((DataView)sds.Select(DataSourceSelectArguments.Empty)).ToTable().Rows.Count + "";

    }

    //กด แสดงเลข Record
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "Search":
                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;

            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SACCIDENTID", "CSENDTOPK", "STATUS");
                string stmID = data[0] + "";
                string STATUS = data[2] + "";

                string sendStatus = "";
                switch (STATUS)
                {
                    case "ดำเนินการ":
                        sendStatus = "1";
                        break;
                    case "อุทธรณ์":
                        sendStatus = "2";
                        break;
                    case "ปิดเรื่อง":
                        sendStatus = "3";
                        break;
                }

                Session["oSACCIDENTID"] = stmID;

                if ("" + data[1] != "1")
                {
                    xcpn.JSProperties["cpRedirectTo"] = "vendor_accident_add.aspx";
                }
                else
                {
                    xcpn.JSProperties["cpRedirectTo"] = "vendor_accident_view.aspx";
                    Session["sendStatus"] = sendStatus;
                }

                break;

            case "SendData":

                using (OracleConnection con = new OracleConnection(sql))
                {

                    con.Open();
                    string strsql = "UPDATE TACCIDENT SET CSENDTOPK = '1' WHERE SACCIDENTID = :SACCIDENTID";
                    using (OracleCommand com = new OracleCommand(strsql, con))
                    {

                        for (int i = 0; i <= gvw.VisibleRowCount - 1; i++)
                        {
                            if (gvw.Selection.IsRowSelected(i))
                            {
                                dynamic DATA = gvw.GetRowValues(i, "SACCIDENTID", "CSENDTOPK");
                                if ("" + DATA[1] != "1")
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = DATA[0] + "";
                                    com.ExecuteNonQuery();
                                }
                            }

                        }
                    }
                }

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");
                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                BindData();
                break;

        }

    }

    protected void gvw_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":


                if (!("" + e.GetValue("CSENDTOPK")).Equals("1"))
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                }
                else
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#CEECF5");
                }

                break;
        }
    }

    private void BindData()
    {
        string condition = "";
//        if (!IsPostBack)
//        {

//            string sql = @"SELECT ROW_NUMBER () OVER (ORDER BY a.DACCIDENT DESC) AS ID1,a.SACCIDENTID,A.DDATENOTIFY ,A.TACCIDENTTIME, a.DACCIDENT, A.SHEADREGISTERNO,A.STRAILERREGISTERNO,ES.FNAME || ' ' || ES.LNAME AS SEMPLOYEE, a.NVALUE,
//case when a.CFAULT = '0' then 'ฝ่ายถูก' else (case when a.CFAULT = '1' then 'ฝ่ายผิด' else 'ประมาทร่วม' end) end AS FAULT,a.CSENDTOPK,
//tt.SUMCAR,CASE WHEN CAST(7 - (SYSDATE - a.DCREATE) AS INT) <= 0 THEN 0 ELSE CAST(7 - (SYSDATE - a.DCREATE) AS INT) END  AS NOPERATTION,
//
//CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (SYSDATE - A.DCREATE)) > 0 THEN 'ดำเนินการ' ELSE 'ปิดเรื่อง' END  ELSE
//CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN 'อุทธรณ์' ELSE
//CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN 'ปิดเรื่อง'  END END END  As STATUS
//
//FROM (((TACCIDENT a LEFT JOIN TVENDOR_SAP v ON A.SVENDORID = V.SVENDORID) LEFT JOIN (SELECT SVENDORID,STRUCKID, COUNT(SHEADREGISTERNO) AS SUMCAR 
//FROM TACCIDENT GROUP BY SVENDORID,STRUCKID) tt ON tt.SVENDORID = A.SVENDORID AND tt.STRUCKID = A.STRUCKID)
//LEFT JOIN TAPPEAL ap ON A.SACCIDENTID = AP.SREFERENCEID AND AP.SAPPEALTYPE='090')LEFT JOIN TEMPLOYEE_SAP es ON ES.SEMPLOYEEID = A.SEMPLOYEEID
//
//WHERE (A.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR A.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR ES.FNAME || ' ' || ES.LNAME LIKE '%' || :oSearch || '%' OR V.SVENDORNAME LIKE '%' || :oSearch || '%' )  AND a.SVENDORID = :oVENDORID ";

//            sds.SelectCommand = sql;
//            sds.SelectParameters.Clear();
//            sds.SelectParameters.Add("oSearch", txtSearch.Text);
//            sds.SelectParameters.Add("oVENDORID", Session["SVDID"] + "");

//            sds.DataBind();
//            gvw.DataBind();
//        }
//        else
//        {
            if ("" + cboStatus.Value == "1")
            {
                condition = "AND ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - A.DCREATE)) > 0";
            }
            else if ("" + cboStatus.Value == "2")
            {
                condition = "AND (ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5')";
            }
            else if ("" + cboStatus.Value == "3")
            {
                condition = "AND ((ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - A.DCREATE)) <= 0) OR ap.CSTATUS = '6' OR ap.CSTATUS = '3')";
            }

            string sql = @"SELECT ROW_NUMBER () OVER (ORDER BY a.DACCIDENT DESC) AS ID1,a.SACCIDENTID,A.DDATENOTIFY ,A.TACCIDENTTIME, a.DACCIDENT, A.SHEADREGISTERNO,A.STRAILERREGISTERNO,ES.FNAME || ' ' || ES.LNAME AS SEMPLOYEE, a.NVALUE,
case when a.CFAULT = '0' then 'ฝ่ายถูก' else (case when a.CFAULT = '1' then 'ฝ่ายผิด' else 'ประมาทร่วม' end) end AS FAULT,a.CSENDTOPK,
tt.SUMCAR,CASE WHEN CAST(7 - (SYSDATE - a.DCREATE) AS INT) <= 0 THEN 0 ELSE CAST(7 - (SYSDATE - a.DCREATE) AS INT) END  AS NOPERATTION,

CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (SYSDATE - A.DCREATE)) > 0 THEN 'ดำเนินการ' ELSE 'ปิดเรื่อง' END  ELSE
CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN 'อุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN 'ปิดเรื่อง'  END END END  As STATUS

FROM (((TACCIDENT a LEFT JOIN TVENDOR_SAP v ON A.SVENDORID = V.SVENDORID) LEFT JOIN (SELECT SVENDORID,STRUCKID, COUNT(SHEADREGISTERNO) AS SUMCAR 
FROM TACCIDENT GROUP BY SVENDORID,STRUCKID) tt ON tt.SVENDORID = A.SVENDORID AND tt.STRUCKID = A.STRUCKID)
LEFT JOIN TAPPEAL ap ON A.SACCIDENTID = AP.SREFERENCEID AND AP.SAPPEALTYPE='090')LEFT JOIN TEMPLOYEE_SAP es ON ES.SEMPLOYEEID = A.SEMPLOYEEID

WHERE (A.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR A.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR ES.FNAME || ' ' || ES.LNAME  LIKE '%' || :oSearch || '%' OR V.SVENDORNAME LIKE '%' || :oSearch || '%' ) AND To_Date(a.DACCIDENT,'dd/MM/yyyy') BETWEEN To_Date(:dStart,'dd/MM/yyyy') AND To_Date(:dEnd,'dd/MM/yyyy') AND a.SVENDORID = :oVENDORID " + condition;

            sds.SelectCommand = sql;
            sds.SelectParameters.Clear();
            sds.SelectParameters.Add("oSearch", txtSearch.Text);
            sds.SelectParameters.Add("dStart", dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sds.SelectParameters.Add("dEnd", dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sds.SelectParameters.Add("oVENDORID", Session["SVDID"] + "");

            sds.DataBind();
            gvw.DataBind();
        }


    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}
