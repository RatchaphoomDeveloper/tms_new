﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Data;
using System.Collections;

public partial class admin_topic_lst : PageBase
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
       
        #endregion
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            Session["oTopicID"] = null;
            Session["oVersion"] = null;
            Session["DataTable"] = CommonFunction.Get_Data(sql, "SELECT SPROCESSID, SPROCESSNAME FROM TPROCESS WHERE (SPROCESSID LIKE '%0')");

            cmbProcess.DataBind();
            cmbProcess.Items.Insert(0, new ListEditItem(" - ทั้งหมด - ", ""));

            LogUser("34", "R", "เปิดดูข้อมูลหน้า หัวข้อตัดคะแนนประเมิน", "");
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnDel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {
        gvw.DataBind();
    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("ID1", "STOPICID", "STOPICNAME")
                      .Cast<object[]>()
                      .Select(s => new { ID1 = s[0].ToString(), STOPICID = s[1].ToString(), STOPICNAME = s[2].ToString() });

                string delid = "";
                foreach (var l in ld)
                {
                    Session["dTopicID"] = l.STOPICID;
                    delid += l.STOPICID + ",";

                    try
                    {
                        sds.Delete();
                    }
                    catch (Exception ex)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถลบรายการนี้ได้ รหัส " + Session["dTopicID"] + " เนื่องจากรายการนี้ถูกนำไปใช้แล้ว!!');");
                    }
                }

                LogUser("34", "D", "ลบข้อมูลหน้า หัวข้อตัดคะแนนประเมิน รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();

                break;
            case "edit":
                int indexx = int.Parse(e.Parameter.Split(';')[1]);
                dynamic data = gvw.GetRowValues(indexx, "STOPICID");
                dynamic data1 = gvw.GetRowValues(indexx, "SVERSION");
                Session["oTopicID"] = data + "";
                Session["oVersion"] = data1 + "";
                xcpn.JSProperties["cpRedirectTo"] = "admin_topic_add.aspx";

                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
       
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_topic_add.aspx");

    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "Process")
        {
            string[] splitProcess = e.GetValue("CPROCESS").ToString().Split(',');
            if (Session["DataTable"] != null)
            {
                DataTable dt = (DataTable)Session["DataTable"];
                DataRow[] dr;
                ASPxComboBox cmbProcess = (ASPxComboBox)gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "cmbProcess");
                for (int i = 0; i < splitProcess.Count(); i++)
                {
                    dr = dt.Select("SPROCESSID = '" + splitProcess[i] + "'");
                    if (dr.Count() > 0)
                    {
                        cmbProcess.Items.Add(dr[0]["SPROCESSNAME"] + "");
                    }
                }

                cmbProcess.SelectedIndex = 0;

            }
            ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;
            if (!CanWrite)
            {
                imbedit.Enabled = false;
            }

        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
